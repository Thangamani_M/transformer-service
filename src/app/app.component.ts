import { Component, HostListener, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router, RoutesRecognized } from '@angular/router';
import {
    agentLoginDataSelector, AgentLogoutDataRemoved, AppDataService, AuthenticationModuleApiSuffixModels, CrudService, DEFAULT_COMMON_DASHBOARD_NAVIGATION, IApplicationResponse,
    LoggedInUserModel, ModulesBasedApiSuffix, PageBasedPermissionsRemoved, retrieveAlreadyLoggedInAgentDetails, SharedModuleApiSuffixModels, SidebarDataRemoved, SignalRTriggers, UserService
} from '@app/shared';
import { OtherService, RxjsService } from '@app/shared/services';
import { HubConnection, HubConnectionBuilder, LogLevel } from '@aspnet/signalr';
import { environment } from '@environments/environment';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { DynamicEagerLoadingCreate, DynamicEagerLoadingRemove, expiresTimeInFullDate, isLoggedIn, loggedInUserData, Logout, StaticEagerLoadingRemove } from '@modules/others';
import { LeadCreationStepperParamsRemoveAction, LeadCreationUserDataRemoveAction, LeadHeaderDataRemoveAction, SalesModuleApiSuffixModels, ScheduleCallbackViewModalComponent, TelephonyApiSuffixModules } from '@modules/sales';
import { RebookSnoozeModalComponent } from '@modules/technical-management/components/rebook-snooze';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { select, Store } from '@ngrx/store';
import { MessageService } from 'primeng/api';
import { combineLatest, Observable } from 'rxjs';
import { filter, map, pairwise } from 'rxjs/operators';
import { AppState } from './reducers';
import { SignalrConnectionService } from './shared/services/signalr-connection.service';
@Component({
    selector: 'body',
    templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {
    isFormChangesDetected: boolean;
    loggedInUserData: LoggedInUserModel;
    agentExtensionNo;
    setInterval;
    customerContactDetail;
    rebookNotiList: any = [];
    rebookNotiIndex: number = 0;
    sound: any;

    constructor(private router: Router, private activatedRoute: ActivatedRoute, private titleService: Title,
        private store: Store<AppState>, private otherService: OtherService, private rxjsService: RxjsService,
        private appDataService: AppDataService, private userService: UserService, private signalrConnectionService: SignalrConnectionService,
        private crudService: CrudService, private dialog: MatDialog, private idle: Idle,
        private messageService: MessageService) {
        if (!this.otherService?.previousUrl?.includes('non-crm')) {
            this.idle.setIdle(1779); // how long can they be inactive before considered idle, in seconds
            this.idle.setTimeout(1); // how long can they be idle before considered timed out, in seconds
            this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES); // provide sources that will "interrupt" aka provide events indicating the user is active
        }
        this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
            this.agentExtensionNo = extension;
            if (this.agentExtensionNo) {
                // inbound and outbound call signalR hug config initiation
                this.hubConnectionForTelephonyAPIConfigs();
            }
        });
    }

    // Broadcast event to all the listeners ( multiple tabs ). If any change in the local storage it will trigger this listener in all the opened tabs
    // The below listener will start to listen the changes in localstorage only when more than one tab is opened
    @HostListener('window:storage', ['$event'])
    onLocalStorageValueChanges(event) {
        if ((localStorage?.hasOwnProperty('encodedJWTToken') && localStorage.encodedJWTToken) ||
            (!localStorage && !this.userService.encodedJWTToken) ||
            (localStorage?.length == 0)) {
            if (!this.userService.encodedJWTToken) {
                if (this.otherService?.previousUrl?.includes('non-crm') ||
                    window.location.href.includes('generate-password')) {
                    return;
                };
                this.removeAllApplicationData();
            }
        }
    }
    // When the user switches to another tab (the same fidelity application) then the user idle time will be stopped and resumes the start timer again when the user is back to the tab
    @HostListener('document:visibilitychange')
    onTabVisibilityChange() {
        if (this.appDataService.openedTabsCount > 1) {
            if (document.hidden) {
                this.idle.stop();
            } else {
                this.loadUserIdleFunctions();
            }
        }
        else if (this.appDataService.openedTabsCount == 0) {
            // let currentStorageTime = new Date().getTime();
            // if (this.appDataService.storageTime && currentStorageTime - this.appDataService.storageTime > 10) {
            //     // this.removeAllApplicationData();
            //     // this.router.navigateByUrl('login');
            // }
            // else {
            this.appDataService.openedTabsCount = 1;
            // }
        }
    }

    // Prepare number of opened tab counts of the fidelity CRM
    @HostListener("window:load", ['$event'])
    onTabLoad(e) {
        // return the function without tab increment for non crm components (which doesn't require authentication)
        if (this.otherService?.previousUrl?.includes('non-crm')) return;
        let openedTabsCount = this.appDataService.openedTabsCount;
        if (Number.isNaN(openedTabsCount)) {
            this.appDataService.openedTabsCount = 1;
        }
        else {
            this.appDataService.openedTabsCount = openedTabsCount + 1;
        }
    }

    // Prepare number of closed tab counts of the fidelity CRM
    @HostListener("window:beforeunload", ['$event'])
    onTabUnLoad(e) {
        // return the function without tab decrement for non crm components (which doesn't require authentication)
        if (this.otherService?.previousUrl?.includes('non-crm')) return;
        let openedTabsCount = this.appDataService.openedTabsCount;
        if (openedTabsCount) {
            this.appDataService.openedTabsCount = openedTabsCount > 0 ? openedTabsCount - 1 : openedTabsCount;
        }
        else {
            this.appDataService.openedTabsCount = 0;
        }
        // this.appDataService.storageTime = new Date().getTime();
    }

    @HostListener('document:keyup')
    @HostListener('document:keydown')
    @HostListener('document:keypress')
    @HostListener('document:click')
    @HostListener('document:wheel')
    resetIdleTime() {
        this.loadUserIdleFunctions();
    }

    ngOnInit(): void {
        this.store.select(expiresTimeInFullDate).subscribe((expiresTimeInFullDate) => {
            if (!expiresTimeInFullDate) return;
            // Clear all user data if the user closes the tab without logging out the application and comes back again to access some of the pages directly
            let expiryDateTimePlusFiveMins = new Date(expiresTimeInFullDate);
            // extend the minutes upto 30 from the current token expiry time then compare the the current time exceeds expiry time plus 30 minutes
            expiryDateTimePlusFiveMins.setMinutes(new Date(expiresTimeInFullDate).getMinutes() + 30);
            // if (new Date().getTime() >= new Date(expiresTimeInFullDate).getTime()) {
            //     this.userService.removeAllStorageDataAndAppData();
            // }
        });
        this.rxjsService.getCustomerContactNumber().subscribe((customerContactDetail) => {
            if (!customerContactDetail) {
                return;
            }
            this.customerContactDetail = customerContactDetail;
        });
        this.sound = document.getElementById("audio");
        // this.appDataService.storageTime = null;
        combineLatest([
            this.store.select(loggedInUserData)
        ]).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
        });
        this.store.pipe(select(isLoggedIn)).subscribe((isLoggedIn: boolean) => {
            if (isLoggedIn == true) {
                //Tmp funcion added to capture the latest noticiations
                // this.setInterval = setInterval(() => {
                //     this.rxjsService.setGlobalLoaderProperty(false);
                //     this.crudService.get(ModulesBasedApiSuffix.COMMON_API, SharedModuleApiSuffixModels.NOTIFICATIONS, this.loggedInUserData.userId, false, prepareGetRequestHttpParams(null, null, {
                //         isDefaultLoaderDisabled: true
                //     }))
                //         .subscribe((response: IApplicationResponse) => {
                //             if (response.isSuccess && response.statusCode == 200 && response.resources) {
                //                 this.rxjsService.setNotifications({ notifications: response.resources.notifications, count: response.resources.notificationCount });
                //             }
                //             this.rxjsService.setGlobalLoaderProperty(false);
                //         });
                // }, 600000); // 10 mins

            }
            else if (isLoggedIn == false) {
                this.idle.stop();
                clearInterval(this.setInterval);
            }
        });
        this.router.events.pipe(
            filter(event => event instanceof NavigationEnd),
            map((event: NavigationEnd) => {
                // prevent the user to go to login page and navigates to common dashboard whenever already logged in in another tab
                if (event.url == '/login' && this.loggedInUserData?.userId) {
                    if (!this.otherService?.previousUrl?.includes(AuthenticationModuleApiSuffixModels.GENERATE_PASSWORD)) {
                        let navigateableUrl = (this.otherService.previousUrl == '/login') ? DEFAULT_COMMON_DASHBOARD_NAVIGATION :
                            this.otherService.previousUrl ? this.otherService.previousUrl : DEFAULT_COMMON_DASHBOARD_NAVIGATION;
                        this.router.navigateByUrl(navigateableUrl);
                    }
                }
                let child = this.activatedRoute.firstChild;
                while (child) {
                    if (child.firstChild) {
                        child = child.firstChild;
                    } else if (child.snapshot.data && child.snapshot.data['title']) {
                        return child.snapshot.data['title'];
                    } else {
                        return null;
                    }
                }
                return null;
            })).subscribe((data: any) => {
                this.rxjsService.setFormChangesDetectionPropertyForPageReload(false);
                if (data !== 'Advanced Search List') {
                    this.rxjsService.setFormChangeDetectionProperty(false);
                }
                this.rxjsService.setFormSubmittedSuccessfullyProperty(false);
                if (data && this.otherService.title != data) {
                    this.otherService.title = data + ' - Fidelity';
                    this.titleService.setTitle(this.otherService.title);
                }
            });
        // find previous url logic
        this.router.events.pipe(filter((routerEvent: any) => routerEvent instanceof RoutesRecognized), pairwise())
            .subscribe((routerEvents: RoutesRecognized[]) => {
                this.otherService.previousUrl = routerEvents[0].urlAfterRedirects;
                this.otherService.currentUrl = routerEvents[1].urlAfterRedirects;
            });
        this.rxjsService.setFromUrl(this.appDataService.fromUrlOfLeadLifecycle)
        if (this.loggedInUserData.userId) {
            this.getUpdatedSalesNotifications(this.loggedInUserData.userId);
            this.hubConnectionForSalesAPIConfigs(this.loggedInUserData.userId);
            this.hubConnectionForChatAPIConfigs();
        }
        // prevent unsaved data to be lost on browser reload some technics
        this.rxjsService.getFormChangesDetectionPropertyForPageReload().subscribe((isFormChangesDetected: boolean) => {
            this.isFormChangesDetected = isFormChangesDetected;
        });
        // when open another tab it presists its default url
        // combineLatest(this.store.select(defaultRedirectRouteUrl),this.store.select(sidebarDataSelector$)).subscribe((observer)=>{
        //     if (window.history.length === 2 && observer[0]!==null) {
        //         this.router.navigateByUrl(observer[0]);
        //     }
        //     if(observer[1] && window.history.length!==50){
        //         this.router.navigate(['common-dashboard'], { state: { isAuthorized: (observer[1].length > 0 ? true : false) } });
        //     }
        // });
        this.loadUserIdleFunctions();
        /* If the agent is already logged In the same system and the same user ID ( event with different browsers ) then call the api to fetch the 
agent details rather than logging in again */
        if (this.loggedInUserData.userId) {
            retrieveAlreadyLoggedInAgentDetails(this.crudService, this.userService, this.loggedInUserData, this.store);
        }
    }

    hubConnectionInstanceForSalesAPI: HubConnection;
    hubConnectionForSalesAPIConfigs(loggedInUserId?: string) {
        let signalrConnectionService = this.signalrConnectionService.hubConnectionInitializationForSalesAPIPromise();
        signalrConnectionService.then(() => {
            this.hubConnectionInstanceForSalesAPI = this.signalrConnectionService.salesAPIHubConnectionBuiltInstance();
            this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.ScheduleCallbackTrigger, data => {
                if (this.loggedInUserData.userId.toLowerCase() == data.createdUserId.toLowerCase()) {
                    data.triggerName = SignalRTriggers.ScheduleCallbackTrigger;
                    data.fromModule = 'Lead';
                    data.type = "Schedule Callback";
                    this.rxjsService.setDialogOpenProperty(true);
                    this.dialog.open(ScheduleCallbackViewModalComponent, { disableClose: true, data, width: '750px' });
                }
            });
            this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.CustomerAgentCallbackTrigger, data => {
                if (this.loggedInUserData.userId.toLowerCase() == data.assignedToUserId.toLowerCase()) {
                    data.triggerName = SignalRTriggers.CustomerAgentCallbackTrigger;
                    data.fromModule = 'Customer';
                    data.type = "Schedule Callback";
                    this.rxjsService.setDialogOpenProperty(true);
                    this.dialog.open(ScheduleCallbackViewModalComponent, { disableClose: true, data, width: '750px' });
                }
            });
            //receive new messages
            this.hubConnectionInstanceForSalesAPI.on('newMessage', (receivedMessage) => {
                this.messageService.add({ severity: 'info', summary: receivedMessage.NewMessage.Sender, detail: receivedMessage.NewMessage.Text });
            });
            // to accept the queue based incoming calls
            this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.CallbackAcceptNotificationTrigger, data => {
                if (this.loggedInUserData.userId.toLowerCase() == data.createdUserId.toLowerCase() && data.extensionNumber == this.agentExtensionNo) {
                    if (data.customerId) {
                        data.triggerName = SignalRTriggers.CallbackAcceptNotificationTrigger;
                        data.fromModuleForSubmit = 'Openscape';
                        this.rxjsService.setDialogOpenProperty(true);
                        this.rxjsService.setscheduleCallbackDetails(data);
                        this.rxjsService.setExpandOpenScape(true);
                    }
                }
            });
            this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.DynamicDataChangeTrigger, data => {
                if (data) {
                    this.getDynamicAppData();
                }
            });
            // notifications signalR trigger
            this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.NotificationTrigger, ({ UserId }) => {
                if (UserId == loggedInUserId) {
                    this.getUpdatedSalesNotifications(loggedInUserId);
                }
            });
            this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.RebookSnoozeTrigger, data => {
                let exist = data.filter(x => x.UserId == this.loggedInUserData.userId);
                if (exist?.length) {
                    let rebookDialogAlreadyExist = [];
                    exist?.filter(x => {
                        this.dialog.openDialogs?.forEach((el: any) => {
                            if (x.UserId == this.loggedInUserData.userId && x.CallInitiationRebookId == el?.componentInstance?.popupData?.CallInitiationRebookId) {
                                rebookDialogAlreadyExist.push(x.CallInitiationRebookId);
                            }
                        })
                    });
                    if (rebookDialogAlreadyExist?.length) {
                        return;
                    }
                    this.rebookNotiList = exist;
                    this.rxjsService.setDialogOpenProperty(true);
                    this.openRebookNotificationDialog(this.rebookNotiList[0]);
                }
            });
            this.hubConnectionInstanceForSalesAPI.onclose((data) => {
                this.hubConnectionInstanceForSalesAPI = undefined;
                //this.hubConnectionTelephonyConfigs();
            });
        })
            .catch();
    }

    hubConnectionInstanceForChatAPI: HubConnection;
    hubConnectionForChatAPIConfigs() {
        let signalrConnectionService = this.signalrConnectionService.hubConnectionInitializationForChatAPIPromise();
        signalrConnectionService.then(() => {
            this.hubConnectionInstanceForChatAPI = this.signalrConnectionService.chatAPIHubConnectionBuiltInstance();
            this.hubConnectionInstanceForChatAPI.on('newMessage', (receivedMessage) => {
                console.log("newMessage");//, JSON.stringify(receivedMessage)
                // this.onNewMessageReceived(receivedMessage);
                this.sound.play();
                this.messageService.add({ severity: 'info', summary: receivedMessage.NewMessage.Sender, detail: receivedMessage.NewMessage.Text });
            });
            this.hubConnectionInstanceForChatAPI.onclose(() => {
                console.log('Chat app disconnected');
                this.hubConnectionForChatAPIConfigs();
            });
        }).catch();
    }

    openRebookNotificationDialog(exist) {
        const ref = this.dialog.open(RebookSnoozeModalComponent, {
            width: '650px',
            data: exist,
            disableClose: true
        });
        ref.afterClosed().subscribe((result) => {
            // if (result) {
            if (this.rebookNotiIndex < (this.rebookNotiList.length - 1)) {
                this.rebookNotiIndex++;
                this.openRebookNotificationDialog(this.rebookNotiList[this.rebookNotiIndex])
            }
            // }
        });
    }

    hubConnectionInstanceForTelephonyAPI: HubConnection;
    hubConnectionForTelephonyAPIConfigs() {
        this.hubConnectionInstanceForTelephonyAPI = new HubConnectionBuilder()
            .configureLogging(LogLevel.Critical)
            .withUrl(environment.telephonyApi)
            .build();
        this.hubConnectionInstanceForTelephonyAPI.keepAliveIntervalInMilliseconds = 30000000;
        this.hubConnectionInstanceForTelephonyAPI.start().
            then(() => console.log('Telephony API Hub Connection is started..!!'))
            .catch((error) => {
                console.log('Telephony API Hub Connection is Failed because of : ', error);
            });
        /* Openscape Inbound Events */
        // Inbound call delivered
        this.hubConnectionInstanceForTelephonyAPI.on(SignalRTriggers.InboundCallDeliveredTrigger, data => {
            if (this.agentExtensionNo == data.ExtensionNumber) {
                data.triggerName = SignalRTriggers.InboundCallDeliveredTrigger;
                this.rxjsService.setInboundCallDetails(data);
            }
        });
        // Inbound call received
        this.hubConnectionInstanceForTelephonyAPI.on(SignalRTriggers.InboundCallNotificationsTrigger, data => {
            if (this.agentExtensionNo == data.ExtensionNumber) {
                data.triggerName = SignalRTriggers.InboundCallNotificationsTrigger;
                this.rxjsService.setInboundCallDetails(data);
            }
        });
        // Inbound call disconnect
        this.hubConnectionInstanceForTelephonyAPI.on(SignalRTriggers.InboundCallDisconnectedNotificationsTrigger, data => {
            if (this.agentExtensionNo == data.ExtensionNumber) {
                data.triggerName = SignalRTriggers.InboundCallDisconnectedNotificationsTrigger;
                this.rxjsService.setInboundCallDetails(data);
            }
        });
        // Inbound call failed
        this.hubConnectionInstanceForTelephonyAPI.on(SignalRTriggers.InboundCallFailedNotificationsTrigger, data => {
            if (this.agentExtensionNo == data.ExtensionNumber) {
                data.triggerName = SignalRTriggers.InboundCallFailedNotificationsTrigger;
                this.rxjsService.setInboundCallDetails(data);
            }
        });
        /* End Of Openscape Inbound Events */

        /* Openscape Outbound Events */
        // Outbound call is created by the customer
        this.hubConnectionInstanceForTelephonyAPI.on(SignalRTriggers.OutboundCallDeliveredTrigger, data => {
            if (this.agentExtensionNo == data.ExtensionNumber) {
                data.triggerName = SignalRTriggers.OutboundCallDeliveredTrigger;
                this.rxjsService.setUniqueCallId(data);
            }
        });
        // Outbound call is received by the customer
        this.hubConnectionInstanceForTelephonyAPI.on(SignalRTriggers.OutboundCallNotificationsTrigger, data => {
            if (this.agentExtensionNo == data.ExtensionNumber) {
                data.fromContactNumber = this.agentExtensionNo;
                data.toContactNumber = this.customerContactDetail?.customerContactNumber?.toString().replace(/\s/g, "");
                data.customerId = this.customerContactDetail?.customerId;
                data.customerAddressId = this.customerContactDetail?.siteAddressId;
                data.occurrenceBookId = this.customerContactDetail?.occurrenceBookId;
                this.onCreateEventModuleCallLogsForOpenscape('connect', data);
                data.triggerName = SignalRTriggers.OutboundCallNotificationsTrigger;
                this.rxjsService.setUniqueCallId(data);
            }
        });
        // Outbound call disconnect
        this.hubConnectionInstanceForTelephonyAPI.on(SignalRTriggers.OutboundCallDisconnectedNotificationsTrigger, data => {
            if (this.agentExtensionNo == data.ExtensionNumber) {
                this.onCreateEventModuleCallLogsForOpenscape('disconnect', data);
                data.triggerName = SignalRTriggers.OutboundCallDisconnectedNotificationsTrigger;
                this.rxjsService.setUniqueCallId(data);
            }
        });
        // Outbound call failed
        this.hubConnectionInstanceForTelephonyAPI.on(SignalRTriggers.OutboundCallFailedNotificationsTrigger, data => {
            if (this.agentExtensionNo == data.ExtensionNumber) {
                this.onCreateEventModuleCallLogsForOpenscape('fail', data);
                data.triggerName = SignalRTriggers.OutboundCallFailedNotificationsTrigger;
                this.rxjsService.setUniqueCallId(data);
            }
        });
        /* End Of Openscape Outbound Events */

        /* Openscape Common (Inbound/Outbound) Events */
        // Call hold for both inbound and outbound
        this.hubConnectionInstanceForTelephonyAPI.on(SignalRTriggers.CallHoldNotificationsTrigger, data => {
            if (this.agentExtensionNo == data.ExtensionNumber) {
                data.triggerName = SignalRTriggers.CallHoldNotificationsTrigger;
                this.rxjsService.setUniqueCallId(data);
            }
        });
        // Call transfer for both inbound and outbound
        this.hubConnectionInstanceForTelephonyAPI.on(SignalRTriggers.CallTransferredNotificationsTrigger, data => {
            if (this.agentExtensionNo == data.ExtensionNumber) {
                data.triggerName = SignalRTriggers.CallTransferredNotificationsTrigger;
                this.rxjsService.setUniqueCallId(data);
            }
        });
        // /* End Of Openscape Common (Inbound/Outbound) Events */
        this.hubConnectionInstanceForTelephonyAPI.onclose((data) => {
            this.hubConnectionInstanceForTelephonyAPI = undefined;
            //this.hubConnectionTelephonyConfigs();
        });
    }

    count = 0;
    loadUserIdleFunctions() {
        this.count = 0;
        this.idle.watch();
        this.idle.onTimeout.subscribe((countdown: number) => {
            this.count++;
            if (this.count == 1 && this.idle.isIdling()) {
                this.onApplicationLogout();
            }
        });
    }

    onCreateEventModuleCallLogsForOpenscape(type: string, payload) {
        let crudService: Observable<IApplicationResponse> = type == 'connect' ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OUTBOUND_CALL_CONNECTED_LOG, payload) :
            type == 'disconnect' ? this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OUTBOUND_CALL_DISCONNECTED_LOG, payload) :
                this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.OUTBOUND_CALL_FAILED_LOG, payload);
        crudService.subscribe((response: IApplicationResponse) => {
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    onApplicationLogout() {
        if (!this.loggedInUserData.userId) return;
        this.userService.onLogout('observable')?.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.removeAllApplicationData();
            }
        });
    }

    removeAllApplicationData() {
        this.router.navigateByUrl('login');
        this.store.dispatch(new Logout());
        this.store.dispatch(new SidebarDataRemoved());
        this.store.dispatch(new AgentLogoutDataRemoved());
        this.store.dispatch(new StaticEagerLoadingRemove());
        this.store.dispatch(new DynamicEagerLoadingRemove());
        this.store.dispatch(new LeadCreationUserDataRemoveAction());
        this.store.dispatch(new LeadCreationStepperParamsRemoveAction());
        this.store.dispatch(new LeadHeaderDataRemoveAction());
        this.store.dispatch(new PageBasedPermissionsRemoved());
        this.rxjsService.setUniqueCallId(null);
        this.rxjsService.setCustomerContactNumber(null);
        this.rxjsService.setscheduleCallbackDetails(null);
        this.rxjsService.setAnyPropertyValue(null);
        this.signalrConnectionService.closeAllHubConnections();
        localStorage.clear();
        sessionStorage.clear();
        // close all opened angular material popups thoughout the entire application
        this.dialog.closeAll();
        this.count = 0;
        this.appDataService.openedTabsCount = 0;
        this.userService.onLogout();
    }

    getUpdatedSalesNotifications(loggedInUserId: string) {
        this.crudService.get(ModulesBasedApiSuffix.COMMON_API, SharedModuleApiSuffixModels.NOTIFICATIONS, loggedInUserId)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200 && response.resources) {
                    this.rxjsService.setNotifications({ notifications: response.resources.notifications, count: response.resources.notificationCount });
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    getDynamicAppData() {
        this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_DYNAMIC, undefined)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200 && response.resources) {
                    this.store.dispatch(new DynamicEagerLoadingCreate({ dynamicEagerLoadingData: response.resources }));
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    ngOnDestroy() {
    }

    gotoTop(): void {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    }
}