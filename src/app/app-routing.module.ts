import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonDashboardComponent, LoginComponent, PageNotFoundComponent } from '@app/modules/others';
import { AuthenticationGuard, LayoutComponent, OnDemandPreloadStrategy } from './shared';

const routes: Routes = [
  {
    path: 'login', component: LoginComponent, data: { title: 'Login' }
  },
  {
    path: 'generate-password/:id', loadChildren: () => import('../app/modules/others/reset-password/reset-password.module').then(m => m.ResetPasswordModule), data: { title: 'Reset Password' }
  },
  {
    path: 'forgotpwd', loadChildren: () => import('../app/modules/others/forgot-password/forgot-password.module').then(m => m.ForgotPasswordModule), data: { title: 'Forgot Password' }
  },
  {
    path: "",
    component: LayoutComponent,
    canActivate: [AuthenticationGuard],
    children: [
      {
        path: 'common-dashboard', component: CommonDashboardComponent, data: { title: 'Common Dashboard' }, canActivate: [AuthenticationGuard]
      },
      {
        path: 'user', loadChildren: () => import('../app/modules/user/user.module').then(m => m.UserModule), data: { preload: true }
      },
      {
        path: 'customer', loadChildren: () => import('../app/modules/customer/customer.module').then(m => m.CustomerModule), data: { preload: true }
      },
      {
        path: 'lss', loadChildren: () => import('../app/modules/lss/lss.module').then(m => m.LssModule)
      },
      {
        path: 'sales', loadChildren: () => import('../app/modules/sales/').then(m => m.SalesModule), data: { preload: true }
      },
      {
        path: 'boundary-management', loadChildren: () => import('../app/modules/boundary-management').then(m => m.BoundaryManagementModule)
      },
      {
        path: 'technical-management', loadChildren: () => import('../app/modules/technical-management/technical-management.module').then(m => m.TechnicalManagementModule), data: { preload: true }
      },
      {
        path: "inventory", loadChildren: () => import('../app/modules/inventory/inventory.module').then(m => m.InventoryModule)
      },
      {
        path: 'signal-management', loadChildren: () => import('../app/modules/signal-management/signal-management.module').then(m => m.SignalManagementModule), data: { preload: true }
      },
      {
        path: 'chat-room', loadChildren: () => import('../app/modules/chat-room/chat-room.module').then(m => m.ChatRoomModule)
      },
      {
        path: 'faq', loadChildren: () => import('../app/modules/faq/faq.module').then(m => m.FaqModule)
      },
      {
        path: 'my-tasks', loadChildren: () => import('../app/modules/my-tasks').then(m => m.MyTasksModule)
      },
      {
        path: 'event-management', loadChildren: () => import('../app/modules/event-management/event-management.module').then(m => m.EventManagementModule), data: { preload: true }
      },
      {
        path: 'signal-translator', loadChildren: () => import('../app/modules/signal-translator/signal-translator.module').then(m => m.SignalTranslatorModule)
      },
      {
        path: 'dealer', loadChildren: () => import('../app/modules/dealer/dealer.module').then(m => m.DealerModule)
      },
      {
        path: 'collection', loadChildren: () => import('../app/modules/collection/collection.module').then(m => m.CollectionModule), data: { preload: true }
      },
      {
        path: 'calls', loadChildren: () => import('../app/modules/calls/calls.module').then(m => m.CallsModule)
      },
      {
        path: 'data-maintenance', loadChildren: () => import('../app/modules/data-maintenance/data-maintenance-main-menu.module').then(m => m.DataMaintenanceMainMenuModule)
      },
      {
        path: 'billing', loadChildren: () => import('../app/modules/billing-management/billing-management.module').then(m => m.BillingManagementModule)
      },
      {
        path: "skill-configuration", loadChildren: () => import('../app/modules/others/configuration/configuration.module').then(m => m.ConfigurationModule)
      },
      {
        path: "configuration", loadChildren: () => import('../app/modules/others/configuration/configuration.module').then(m => m.ConfigurationModule)
      },
      {
        path: 'out-of-office', loadChildren: () => import('../app/modules/out-of-office/out-of-office.module').then(m => m.OutOfOfficeModule)
      },
      {
        path: 'calender-demo', loadChildren: () => import('../app/modules/calender-demo/calender-demo.module').then(m => m.CalenderDemoModule)
      },
      { path: 'reports', loadChildren: () => import('./modules/reports/reports.module').then(m => m.ReportsModule) },
      { path: 'panic-app', loadChildren: () => import('../app/modules/panic-app/panic-app.module').then(m => m.PanicAppModule) },
      {
        path: 'campaign-management', loadChildren: () => import('../app/modules/campaign-management/campaign-management.module').then(m => m.CampaignManagementModule), data: { preload: true }
      },
      {
        path: 'notifications', loadChildren: () => import('../app/modules/others/notifications/notifications.module').then(m => m.NotificationsModule), data: { title: 'Notifications' }, canActivate: [AuthenticationGuard]
      },
      {
        path: 'radio-removal', loadChildren: () => import('../app/modules/radio-removal/radio-removal.module').then(m => m.RadioRemovalModule), data: { title: 'Notifications' }, canActivate: [AuthenticationGuard]
      },
      {
        path: 'risk-watch', loadChildren: () => import('../app/modules/riskwatch/riskwatch.module').then(m => m.RiskwatchModule), data: { title: 'Risk Watch' }, canActivate: [AuthenticationGuard]
      },
      {
        path: 'openscape', loadChildren: () => import('../app/modules/others/openscape-logged-agents/openscape-logged-agents-list.module').then(m => m.OpenscapeLoggedAgentsModule)
      }
    ]
  },
  {
    path: 'non-crm', loadChildren: () => import('../app/modules/others/non-crm/non-crm.module').then(m => m.NonCRMModule)
  },
  {
    path: 'non-crm-rfr', loadChildren: () => import('../app/modules/others/rfr-quote-acceptence/rfr-quote-acceptence.module').then(m => m.RfrQuoteAcceptenceModule)
  },
  {
    path: 'cancellation-request-form', loadChildren: () => import('../app/modules/others/cancellation-request-form/cancellation-request-form.module').then(m => m.CancellationRequestFormModule)
  },
  {
    path: '**', component: PageNotFoundComponent, data: { title: 'Page Not Found' }
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: OnDemandPreloadStrategy })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
