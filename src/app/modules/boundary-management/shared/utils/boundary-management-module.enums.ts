enum Boundary_Management_Components {
    ROLLBACK_PROCESS = 'Rollback Process',
    OPERATIONAL_BOUNDARIES='Operations Boundaries',
    TECHNICAL_BOUNDARIES='Technical Boundaries',
    SALES_BOUNDARIES='Sales Boundaries',
    GIS_DASHBOARD='GIS Dashboard'
  }

  export default Boundary_Management_Components;