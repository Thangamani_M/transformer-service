import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    GISDashboardComponent, ImportOtherBoundariesComponent,
    RollbackDetailComponent,
    RollbackListComponent, ViewOperationsBoundariesListComponent, ViewOtherBoundariesListComponent, ViewSalesBoundariesListComponent, ViewTechnicalBoundariesListComponent
} from '@app/modules/boundary-management';
import { BoundaryDetailNotificationComponent, GISBoundaryDetailPageComponent } from '@app/shared';

import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    {
        path: 'view', children: [
            {
                path: 'sales-boundaries', component: ViewSalesBoundariesListComponent, data: { title: 'View Sales Boundaries' }, canActivate: [AuthGuard]
            },
            {
                path: 'sales-boundaries/add-edit', component: GISBoundaryDetailPageComponent, data: { title: 'Sales Boundaries Add/Edit' }, canActivate: [AuthGuard]
            },
            {
                path: 'technical-boundaries', component: ViewTechnicalBoundariesListComponent, data: { title: 'View Technical Boundaries' }, canActivate: [AuthGuard]
            },
            {
                path: 'technical-boundaries/add-edit', component: GISBoundaryDetailPageComponent, data: { title: 'Technical Boundaries Add/Edit' }, canActivate: [AuthGuard]
            },
            {
                path: 'operations-boundaries', component: ViewOperationsBoundariesListComponent, data: { title: 'View Operations Boundaries' }, canActivate: [AuthGuard]
            },
            {
                path: 'operations-boundaries/add-edit', component: GISBoundaryDetailPageComponent, data: { title: 'Operations Boundaries Add/Edit' }, canActivate: [AuthGuard]
            },
            {
                path: 'other-boundaries', component: ViewOtherBoundariesListComponent, data: { title: 'View Other Boundaries' }, canActivate: [AuthGuard]
            },
            {
                path: 'other-boundaries/detail', component: ViewOtherBoundariesListComponent, data: { title: 'View Other Boundaries Detail' }, canActivate: [AuthGuard]
            },
            {
                path: 'other-boundaries/import', component: ImportOtherBoundariesComponent, data: { title: 'Import Other Boundaries' }, canActivate: [AuthGuard]
            },
            {
                path: 'rollbacks', component: RollbackListComponent, data: { title: 'Rollbacks List' }, canActivate: [AuthGuard]
            },
            {
                path: 'rollbacks/add-edit', component: RollbackDetailComponent, data: { title: 'Rollbacks Add/Edit' }, canActivate: [AuthGuard]
            }
        ]
    },
    {
        path: 'gis-dashboard', component: GISDashboardComponent, data: { title: 'GIS Dashboard' }, canActivate: [AuthGuard]
    },
    {
        path: 'notification', component: BoundaryDetailNotificationComponent, data: { title: 'Import Other Boundaries' }, canActivate: [AuthGuard]
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)]
})

export class BoundaryManagementRoutingModule { }
