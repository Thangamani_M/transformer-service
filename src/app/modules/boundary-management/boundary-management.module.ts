import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatMenuModule } from '@angular/material';
import {
  BoundaryManagementRoutingModule, ChangeOwnerPopupComponent, GISDashboardComponent, ImportOtherBoundariesComponent, RollbackDetailComponent, RollbackListComponent, ViewBoundariesListComponent, ViewOperationsBoundariesListComponent, ViewOtherBoundariesListComponent, ViewSalesBoundariesListComponent,
  ViewTechnicalBoundariesListComponent
} from '@app/modules/boundary-management';
import { BoundaryDetailNotificationComponent, GISBoundaryDetailPageComponent, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { RequestSearchListComponent } from '@modules/my-tasks/components/boundary-management/gis/request-search-list.component';
@NgModule({
  declarations: [ViewBoundariesListComponent, ViewSalesBoundariesListComponent, ViewTechnicalBoundariesListComponent,
    ViewOperationsBoundariesListComponent, ViewOtherBoundariesListComponent, ImportOtherBoundariesComponent,
    GISDashboardComponent, RollbackListComponent, RollbackDetailComponent,GISBoundaryDetailPageComponent,
    RequestSearchListComponent,BoundaryDetailNotificationComponent,ChangeOwnerPopupComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule, MatMenuModule,
    BoundaryManagementRoutingModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents: [ChangeOwnerPopupComponent],
  exports: [ViewBoundariesListComponent,RequestSearchListComponent]
})
export class BoundaryManagementModule { }
