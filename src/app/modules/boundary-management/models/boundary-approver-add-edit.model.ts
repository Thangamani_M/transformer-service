class BoundaryApproverAddEditFormModel {
  constructor(model?: BoundaryApproverAddEditFormModel) {
    this.boundaryApprovalConfigId =
      model == undefined
        ? ""
        : model.boundaryApprovalConfigId == undefined
          ? ""
          : model.boundaryApprovalConfigId;
    this.boundaryTypeId =
      model == undefined
        ? ""
        : model.boundaryTypeId == undefined
          ? ""
          : model.boundaryTypeId;
    this.boundaryLayerId =
      model == undefined
        ? ""
        : model.boundaryLayerId == undefined
          ? ""
          : model.boundaryLayerId;
    this.description =
      model == undefined
        ? ""
        : model.description == undefined
          ? ""
          : model.description;
    this.divisionIds =
      model == undefined
        ? []
        : model.divisionIds == undefined
          ? []
          : model.divisionIds;
    this.branchIds =
      model == undefined
        ? []
        : model.branchIds == undefined
          ? []
          : model.branchIds;
    this.approvalConfigLevelList =
      model == undefined
        ? []
        : model.approvalConfigLevelList == undefined
          ? []
          : model.approvalConfigLevelList;
  }
  boundaryApprovalConfigId?: string;
  boundaryTypeId?: string;
  boundaryLayerId?: string;
  description?: string;
  divisionIds?: [];
  branchIds?: [];
  approvalConfigLevelList: ApprovalConfigLevelListModel[];
}
class ApprovalConfigLevelListModel {
  constructor(model?: ApprovalConfigLevelListModel) {
    this.boundaryApprovalConfigLevelId =
      model == undefined
        ? ""
        : model.boundaryApprovalConfigLevelId == undefined
          ? ""
          : model.boundaryApprovalConfigLevelId;
    this.level =
      model == undefined ? null : model.level == undefined ? null : model.level;
    this.roleId =
      model == undefined ? "" : model.roleId == undefined ? "" : model.roleId;
    this.noOfEmployees =
      model == undefined
        ? null
        : model.noOfEmployees == undefined
          ? null
          : model.noOfEmployees;
    this.roles =
      model == undefined ? [] : model.roles == undefined ? [] : model.roles;
    this.showEmployeeCountContainer =
      model == undefined
        ? true
        : model.showEmployeeCountContainer == undefined
          ? true
          : model.showEmployeeCountContainer;
  }
  boundaryApprovalConfigLevelId?: string; 
  level?: number;
  roleId?: string;
  noOfEmployees?: number;
  roles?;
  showEmployeeCountContainer: boolean;
}

export { BoundaryApproverAddEditFormModel, ApprovalConfigLevelListModel };
