class UploadOtherBoundaryModel {
    constructor(uploadOtherBoundaryModel?: UploadOtherBoundaryModel) {
        this.file = uploadOtherBoundaryModel == undefined ? "" : uploadOtherBoundaryModel.file == undefined ? "" : uploadOtherBoundaryModel.file;
        this.description = uploadOtherBoundaryModel == undefined ? "" : uploadOtherBoundaryModel.description == undefined ? "" : uploadOtherBoundaryModel.description;
        this.boundaryLayerId = uploadOtherBoundaryModel == undefined ? 11 : uploadOtherBoundaryModel.boundaryLayerId == undefined ? 11 : uploadOtherBoundaryModel.boundaryLayerId;
    }
    file?: string;
    description?: string;
    boundaryLayerId?:number;
}

export { UploadOtherBoundaryModel }