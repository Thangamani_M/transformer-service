import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { selectStaticEagerLoadingBoundaryTypesState$, UploadOtherBoundaryModel } from '@app/modules';
import { AppState } from '@app/reducers';
import { BoundaryTypes, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, ResponseMessageTypes, setRequiredValidator, SnackbarService } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'import-other-boundaries',
  templateUrl: './import-other-boundary.component.html'
})
export class ImportOtherBoundariesComponent implements OnInit {
  uploadForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  layers = [];
  selectedFile;

  constructor(private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.combineLatestNgrxStoreData();
  }

  ngOnInit(): void {
    this.createUploadForm();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingBoundaryTypesState$)]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = response[0];
      this.layers = response[1].filter(b => b.boundaryTypeId == BoundaryTypes.OTHERS).map((boundaryTypes) =>
        boundaryTypes.boundaryLayers
      )[0];
    });
  }

  createUploadForm() {
    let uploadOtherBoundaryModel = new UploadOtherBoundaryModel();
    this.uploadForm = this.formBuilder.group({});
    Object.keys(uploadOtherBoundaryModel).forEach((key) => {
      this.uploadForm.addControl(key, new FormControl(key == 'boundaryLayerId' ? this.layers[0]['boundaryLayerId'] : uploadOtherBoundaryModel[key]));
    });
    this.uploadForm = setRequiredValidator(this.uploadForm, ["file", "boundaryLayerId"]);
  }

  uploadFile(files: FileList): void {
    this.selectedFile = '';
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.selectedFile = files.item(0);
    const fileExtension = this.selectedFile.name.slice(this.selectedFile.name.length - 4, this.selectedFile.name.length);
    if (fileExtension !== '.csv') {
      this.snackbarService.openSnackbar("Please select .csv file extension only", ResponseMessageTypes.WARNING);
      return;
    }
    this.uploadForm.get('file').setValue(this.selectedFile.name);
  }

  onSubmit() {
    if (this.uploadForm.invalid) {
      return;
    }
    let formData = new FormData();
    formData.append("csv_file", this.selectedFile);
    formData.append("createdUserId", this.loggedInUserData.userId);
    formData.append("description", this.uploadForm.value.description);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.OTHER_BOUNDARIES_FILE_IMPORT, formData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.cancel();
        }
      });
  }

  cancel() {
    this.router.navigate(['/boundary-management/view/other-boundaries']);
  }
}
