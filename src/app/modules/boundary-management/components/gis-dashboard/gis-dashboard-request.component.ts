import { Component, OnInit } from '@angular/core';
import { AppState } from '@app/reducers';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData, selectStaticEagerLoadingBoundaryTypesState$ } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-gis-dashboard-request',
  templateUrl: './gis-dashboard-request.component.html'
})
export class GISDashboardComponent implements OnInit {
  loggedInUserData: LoggedInUserModel;
  response;
  statsByDivisions = [];
  boundaryTypes = [];
  layers = [];
  searchGISRequestsCriteria = {
    fromDate: "",
    toDate: "",
    layerId: "",
    boundaryTypeId: ""
  };
  isGISRequestSearchDisabled = true;

  constructor(private store: Store<AppState>, private rxjsService: RxjsService, private momentService: MomentService,
    private crudService: CrudService) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingBoundaryTypesState$)]
    ).subscribe((response) => {
      this.loggedInUserData = response[0];
      this.boundaryTypes = response[1];
    });
  }

  ngOnInit() {
    this.getGISRequests();
  }

  getGISRequests(otherParams?: object) {
    if (!otherParams) {
      otherParams = {};
      otherParams['maximumRows'] = 10;
      otherParams['pageIndex'] = 0;
    }
    let params = { ...this.searchGISRequestsCriteria, ...otherParams };
    params['fromDate'] = this.searchGISRequestsCriteria.fromDate ? this.momentService.localToUTC(this.searchGISRequestsCriteria.fromDate) : "";
    params['toDate'] = this.searchGISRequestsCriteria.toDate ? this.momentService.localToUTC(this.searchGISRequestsCriteria.toDate) : "";
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.BOUNDARIES_GIS_DASHBOARD, undefined, false, prepareRequiredHttpParams(params), 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.response = response;
          this.statsByDivisions = response.resources.divisionsCount;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onGISSearchRequestsByKeywords(btnActionType: string) {
    if (btnActionType == 'clear') {
      this.searchGISRequestsCriteria = { boundaryTypeId: "", layerId: "", fromDate: "", toDate: "" }
      this.isGISRequestSearchDisabled = true;
    }
    this.getGISRequests();
  }

  onOptionsSelected() {
    this.layers = this.boundaryTypes.filter(b => b.boundaryTypeId == this.searchGISRequestsCriteria.boundaryTypeId).map((boundaryTypes) =>
      boundaryTypes.boundaryLayers
    )[0];
    if (this.searchGISRequestsCriteria.boundaryTypeId || this.searchGISRequestsCriteria.layerId || this.searchGISRequestsCriteria.fromDate ||
      this.searchGISRequestsCriteria.toDate) {
      this.isGISRequestSearchDisabled = false;
    }
    else {
      this.isGISRequestSearchDisabled = true;
    }
  }

  onModelChanged() {
    setTimeout(() => {
      if (this.searchGISRequestsCriteria.boundaryTypeId || this.searchGISRequestsCriteria.layerId || this.searchGISRequestsCriteria.fromDate ||
        this.searchGISRequestsCriteria.toDate) {
        this.isGISRequestSearchDisabled = false;
      }
      else {
        this.isGISRequestSearchDisabled = true;
      }
    });
  }

  onEmittedEvent(emittedObject) {
    delete emittedObject.searchColumns;
    this.getGISRequests(emittedObject);
  }
}
