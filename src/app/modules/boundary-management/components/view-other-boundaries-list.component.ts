import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  IApplicationResponse, LoggedInUserModel,
  ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
declare let L;
declare var $;
@Component({
  selector: 'view-other-boundaries-list',
  templateUrl: './view-other-boundaries-list.component.html'
})
export class ViewOtherBoundariesListComponent implements OnInit {
  columns = [];
  polygonArray = [];
  polyLayers = [];
  loggedInUserData: LoggedInUserModel;
  response;
  boundaries = [];
  selectedEmployeeOption = {};
  employees = [];
  changeOwnerDetail = {};
  boundaryId = '';
  boundaryLayerId;

  constructor(private store: Store<AppState>, private rxjsService: RxjsService, private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private momentService: MomentService) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = response[0];
      this.boundaryLayerId = response[1] && Object.keys(response[1]).length == 0 ? null : +response[1]['boundaryLayerId'];
    });
  }

  ngOnInit() {
    if (this.boundaryLayerId) {
      this.getDetailedOtherBoundaries();
      this.getGisBoundaryDetailsByBoundaryId();
    }
    else {
      this.getOtherBoundaries();
    }
  }

  afterDataRenderedInMapAction(e) {
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getDetailedOtherBoundaries(otherParams?: object) {
    if (!otherParams) {
      otherParams = {};
      otherParams['maximumRows'] = 10;
      otherParams['pageIndex'] = 0;
    }
    let params = {};
    if (otherParams['searchColumns']) {
      Object.keys(otherParams['searchColumns']).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          params[key] = this.momentService.localToUTC(otherParams['searchColumns'][key]);
        }
        else {
          params[key] = otherParams['searchColumns'][key]['value'] ? otherParams['searchColumns'][key]['value'] :
            otherParams['searchColumns'][key];
        }
      });
    }
    delete otherParams['searchColumns'];
    params = { ...otherParams, ...params };
    params['boundaryLayerId'] = this.boundaryLayerId;
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.OTHER_BOUNDARY_IMPORT_DETAIL, undefined, false, prepareRequiredHttpParams(params))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.response = response;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getOtherBoundaries(otherParams?: object) {
    if (!otherParams) {
      otherParams = {};
      otherParams['maximumRows'] = 10;
      otherParams['pageIndex'] = 0;
    }
    let params = {};
    if (otherParams['searchColumns']) {
      Object.keys(otherParams['searchColumns']).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          params[key] = this.momentService.localToUTC(otherParams['searchColumns'][key]);
        }
        else {
          params[key] = otherParams['searchColumns'][key]['value'] ? otherParams['searchColumns'][key]['value'] :
            otherParams['searchColumns'][key];
        }
      });
    }
    delete otherParams['searchColumns'];
    params = { ...otherParams, ...params };
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.OTHER_BOUNDARIES_IMPORT, undefined, false, prepareRequiredHttpParams(params))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.response = response;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  emitSelectedOwnerDetails({ changeOwnerDetail }) {
    this.boundaryId = changeOwnerDetail.boundaryId;
  }

  getGisBoundaryDetailsByBoundaryId() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.BOUNDARIES_MAP_VIEW, undefined, false,
      prepareRequiredHttpParams({
        layerId: this.boundaryLayerId
      }), 1).subscribe((response) => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.boundaries = response.resources;
          if (this.boundaries.length > 0) {
            let data = [];
            this.boundaries.forEach((boundary) => {
              let str = boundary.geometry.coordinates;
              str = str.replace(/^"(.*)"$/, '$1');
              str = str.replace(/[()]/g, '')
              let coordinates = JSON.parse(str);
              coordinates[0].forEach((latLong, index) => {
                let latLongArr = JSON.parse(JSON.stringify(latLong));
                if (Math.sign(latLongArr[0]) == 1) {
                  latLong[0] = latLongArr[1];
                  latLong[1] = latLongArr[0];
                }
              });
              let obj = { coordinates, properties: { Id: boundary.boundaryId, Name: boundary.boundaryName } };
              data.push(obj);
            })
            this.polyLayers = [];
            data.forEach((feature) => {
              this.polyLayers.push(L.polygon(feature.coordinates, feature.properties));
            });
            setTimeout(() => {
              this.polygonArray = this.polyLayers;
            }, 1000);
          } else {
            this.polygonArray = [];
          }
        }
      });
  }

  onEmittedEvent(emittedObject) {
    if (this.boundaryLayerId) {
      this.getDetailedOtherBoundaries(emittedObject);
    }
    else {
      this.getOtherBoundaries(emittedObject);
    }
  }

  eventToOpenPopup(popupNativeElement) {
    $(popupNativeElement).modal('show');
  }
}
