import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import {
  BoundaryTypes, formConfigs, IApplicationResponse, LoggedInUserModel,
  ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData, selectStaticEagerLoadingBoundaryTypesState$ } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { ChangeOwnerPopupComponent } from './change-owner-popup.component';
declare let L;
declare var $;
@Component({
  selector: 'view-sales-boundaries-list',
  templateUrl: './view-sales-boundaries-list.component.html'
})
export class ViewSalesBoundariesListComponent implements OnInit {
  columns = [];
  polygonArray = [];
  polyLayers = [];
  loggedInUserData: LoggedInUserModel;
  response = { resources: [], totalCount: 0 };
  layerId = "";
  regionId = "";
  regions = [];
  layers = [];
  boundaries = [];
  isSearchBtnDisabled = true;
  boundaryId = '';
  isANewRequest = false;

  constructor(private store: Store<AppState>, private rxjsService: RxjsService, private dialog: MatDialog,
    private crudService: CrudService, private momentService: MomentService) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingBoundaryTypesState$),]
    ).subscribe((response) => {
      this.loggedInUserData = response[0];
      this.layers = response[1]?.filter(b => b.boundaryTypeId == BoundaryTypes.SALES).map((boundaryTypes) =>
        boundaryTypes.boundaryLayers
      )[0];
    });
  }

  ngOnInit() {
    this.getForkJoinRequests();
    this.getRegiondByCountryId();
  }

  afterDataRenderedInMapAction(e) {
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS, prepareRequiredHttpParams({
        countryId: formConfigs.countryId
      }), 1)])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess) {
            switch (ix) {
              case 0:
                this.regions = respObj.resources;
                break;
            }
          }
        });
      });
  }

  getSalesBoundaries(otherParamsCopy?: object) {
    if (!this.layerId || !this.regionId) {
      return;
    }
    let otherParams = otherParamsCopy ? JSON.parse(JSON.stringify(otherParamsCopy)) : undefined;
    if (!otherParams) {
      otherParams = {};
      otherParams['maximumRows'] = 10;
      otherParams['pageIndex'] = 0;
    }
    let params = {};
    if (otherParams['searchColumns']) {
      Object.keys(otherParams['searchColumns']).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          params[key] = this.momentService.localToUTC(otherParams['searchColumns'][key]);
        }
        else {
          params[key] = otherParams['searchColumns'][key]['value'] ? otherParams['searchColumns'][key]['value'] :
            otherParams['searchColumns'][key];
        }
      });
    }
    delete otherParams['searchColumns'];
    params = { ...otherParams, ...params };
    params['layerId'] = this.layerId;
    params['regionId'] = this.regionId;
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_BOUNDARIES_IMPORT, undefined, false, prepareRequiredHttpParams(params), 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          if (Object.keys(params)?.length > 4) {
            response['requestType'] = 'fieldLevelSearch';
            this.response = response;
          }
          else {
            this.response = response;
          }
        }
        if (Object.keys(params)?.length > 4 || otherParamsCopy) {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onSearchRequestsByKeywords(btnActionType: string) {
    if (btnActionType == 'clear') {
      this.layerId = "";
      this.regionId = "";
      this.response = { resources: [], totalCount: 0 };
      this.polygonArray = [];
      this.isSearchBtnDisabled = true;
    }
    else {
      this.isANewRequest = true;
      this.getSalesBoundaries();
      this.getGisBoundaryDetailsByBoundaryId();
    }
  }

  onOptionsSelected() {
    if (this.layerId && this.regionId) {
      this.isSearchBtnDisabled = false;
    }
  }

  emitSelectedOwnerDetails({ changeOwnerDetail }) {
    let data: any = {};
    data.boundaryId = changeOwnerDetail.boundaryId;
    this.boundaryId = data.boundaryId;
    data.userId = this.loggedInUserData.userId;
    data.changeOwnerDetail = changeOwnerDetail;
    this.dialog.open(ChangeOwnerPopupComponent, {
      width: "1200px",
      data,
      disableClose: true
    });
  }

  getGisBoundaryDetailsByBoundaryId(type = 'default') {
    if (!this.layerId || !this.regionId) {
      return;
    }
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.BOUNDARIES_MAP_VIEW, undefined, false,
      prepareRequiredHttpParams({
        layerId: this.layerId,
        regionId: this.regionId
      })).subscribe((response) => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.boundaries = response.resources;
          if (this.boundaries.length > 0) {
            let data = [];
            this.boundaries.forEach((boundary) => {
              if (!boundary.geometry) {
                return;
              }
              let str = boundary.geometry.coordinates;
              str = str.replace(/^"(.*)"$/, '$1');
              str = str.replace(/[()]/g, '')
              let coordinates = JSON.parse(str);
              coordinates[0].forEach((latLong, index) => {
                let latLongArr = JSON.parse(JSON.stringify(latLong));
                if (Math.sign(latLongArr[0]) == 1) {
                  latLong[0] = latLongArr[1];
                  latLong[1] = latLongArr[0];
                }
              });
              let obj = { coordinates, properties: { Id: boundary.boundaryId, Name: boundary.boundaryName } };
              data.push(obj);
            })
            this.polyLayers = [];
            data.forEach((feature) => {
              this.polyLayers.push(L.polygon(feature.coordinates, feature.properties));
            });
            setTimeout(() => {
              this.polygonArray = this.polyLayers;
            }, 1000);
          } else {
            this.polygonArray = [];
          }
        }
        if (type == 'refresh') {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onEmittedEvent(emittedObject) {
    if (typeof emittedObject === 'boolean') {
      this.getGisBoundaryDetailsByBoundaryId('refresh');
    }
    else {
      this.getSalesBoundaries(emittedObject);
    }
  }

  eventToOpenPopup(popupNativeElement) {
    $(popupNativeElement).modal('show');
  }

  getRegiondByCountryId(): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS, prepareRequiredHttpParams({
      countryId: formConfigs.countryId
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.regions = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
}
