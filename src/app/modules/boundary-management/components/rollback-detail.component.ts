import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  currentComponentPageBasedPermissionsSelector$, formConfigs, IApplicationResponse, LoggedInUserModel,
  ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData, selectStaticEagerLoadingBoundaryTypesState$ } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { ReusablePrimeNGTableFeatureService } from '../../../shared/services/reusable-primeng-table-features.service';
import Boundary_Management_Components from '../shared/utils/boundary-management-module.enums';

declare let L;
declare var $;
@Component({
  selector: 'rollback-detail',
  templateUrl: './rollback-detail.component.html'
})
export class RollbackDetailComponent implements OnInit {
  columns = [];
  polygonArray = [];
  polyLayers = [];
  loggedInUserData: LoggedInUserModel;
  response;
  boundaries = [];
  selectedEmployeeOption = {};
  employees = [];
  changeOwnerDetail = {};
  boundaryId = '';
  boundaryBackupId;
  boundaryTypeId;
  layerId = "";
  regionId = "";
  regions = [];
  boundaryTypes = [];
  isRollbackBtnDisabled = true;
  isInitiateRollbackAccessDenined = false;

  constructor(private store: Store<AppState>, private rxjsService: RxjsService, private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private momentService: MomentService, private snackbarService: SnackbarService,
    private router: Router, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingBoundaryTypesState$),
      this.activatedRoute.queryParams,
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = response[0];
      this.boundaryTypes = response[1];
      this.boundaryBackupId = response[2]['boundaryBackupId'];
      this.boundaryTypeId = response[2]['boundaryTypeId'];
      let componentPermissions = [];
      componentPermissions = response[3][Boundary_Management_Components.ROLLBACK_PROCESS];
      this.isInitiateRollbackAccessDenined = componentPermissions?.find(cP => cP.menuName === 'Initiate Rollback') ? false : true;
    });
  }


  ngOnInit() {
    if (this.boundaryBackupId) {
      this.getRollbackDetail();
      this.getRegiondByCountryId();
    }
    else {
      this.getRollbacks();
    }
  }

  afterDataRenderedInMapAction(e) {
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getRegiondByCountryId(): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS, prepareRequiredHttpParams({
      countryId: formConfigs.countryId
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.regions = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRollbackDetail(otherParams?: object) {
    if (!otherParams) {
      otherParams = {};
      otherParams['maximumRows'] = 10;
      otherParams['pageIndex'] = 0;
    }
    let params = {};
    if (otherParams['searchColumns']) {
      Object.keys(otherParams['searchColumns']).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          params[key] = this.momentService.localToUTC(otherParams['searchColumns'][key]);
        }
        else {
          params[key] = otherParams['searchColumns'][key]['value'] ? otherParams['searchColumns'][key]['value'] :
            otherParams['searchColumns'][key];
        }
      });
    }
    delete otherParams['searchColumns'];
    params = { ...otherParams, ...params };
    params['boundaryBackupId'] = this.boundaryBackupId;
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.ROLLBACK_BOUNDARIES_DETAIL, undefined, false, prepareRequiredHttpParams(params))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.response = response;
          this.isRollbackBtnDisabled = response.resources.statusName == 'Active' ? false : true;
          if (this.boundaryTypeId) {
            this.response.boundaryTypeNameForReference = this.boundaryTypes.find(b => b['boundaryTypeId'] == this.boundaryTypeId).boundaryTypeName;
          }
          this.boundaries = response.resources.boundaries;
          this.prepareDataForLeafLetMap();
        }
      });
  }

  getRollbacks(otherParamsCopy?: object) {
    let otherParams = otherParamsCopy ? JSON.parse(JSON.stringify(otherParamsCopy)) : undefined;
    if (!otherParams) {
      otherParams = {};
      otherParams['maximumRows'] = 10;
      otherParams['pageIndex'] = 0;
    }
    let params = {};
    if (otherParams['searchColumns']) {
      Object.keys(otherParams['searchColumns']).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          params[key] = this.momentService.localToUTC(otherParams['searchColumns'][key]);
        }
        else {
          params[key] = otherParams['searchColumns'][key]['value'] ? otherParams['searchColumns'][key]['value'] :
            otherParams['searchColumns'][key];
        }
      });
    }
    delete otherParams['searchColumns'];
    params = { ...otherParams, ...params };
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.ROLLBACK_BOUNDARIES, undefined, false, prepareRequiredHttpParams(params))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.response = response;
        }
        if (Object.keys(params)?.length > 2 || otherParamsCopy) {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  emitSelectedOwnerDetails({ changeOwnerDetail }) {
    this.boundaryId = changeOwnerDetail.boundaryId;
  }

  prepareDataForLeafLetMap() {
    if (this.boundaries.length > 0) {
      let data = [];
      this.boundaries.forEach((boundary) => {
        let str = boundary.geometry.coordinates;
        str = str.replace(/^"(.*)"$/, '$1');
        str = str.replace(/[()]/g, '')
        let coordinates = JSON.parse(str);
        coordinates[0].forEach((latLong) => {
          let latLongArr = JSON.parse(JSON.stringify(latLong));
          if (Math.sign(latLongArr[0]) == 1) {
            latLong[0] = latLongArr[1];
            latLong[1] = latLongArr[0];
          }
        });
        let obj = { coordinates, properties: { Id: boundary.boundaryId, Name: boundary.boundaryName } };
        data.push(obj);
      })
      this.polyLayers = [];
      data.forEach((feature) => {
        this.polyLayers.push(L.polygon(feature.coordinates, feature.properties));
      });
      setTimeout(() => {
        this.polygonArray = this.polyLayers;
      }, 1000);
    } else {
      this.polygonArray = [];
    }
  }

  onEmittedEvent(emittedObject) {
    if (this.boundaryBackupId) {
      this.getRollbackDetail(emittedObject);
    }
    else {
      this.getRollbacks(emittedObject);
    }
  }

  eventToOpenPopup(popupNativeElement) {
    $(popupNativeElement).modal('show');
  }

  onInitiateRollback() {
    if (this.isInitiateRollbackAccessDenined) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    else {
      this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you wish to <strong>initiate rollback</strong>?`).
        onClose?.subscribe(dialogResult => {
          if (dialogResult) {
            this.onSubmit();
          }
        });
    }
  }

  onSubmit() {
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.INITIATE_ROLLBACK, {
      boundaryBackupId: this.boundaryBackupId,
      createdUserId: this.loggedInUserData.userId
    }).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.router.navigateByUrl("my-tasks/boundary-management/view/rollbacks");
      }
    });
  }
}