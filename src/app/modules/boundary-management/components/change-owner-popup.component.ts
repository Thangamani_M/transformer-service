import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {
  CrudService, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse,
  ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, SnackbarService
} from '@app/shared';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-change-owner-popup',
  templateUrl: './change-owner-popup.component.html'
})
export class ChangeOwnerPopupComponent implements OnInit {
  formGroup: FormGroup;
  getLoopableCDMObjectRequestObservable: Observable<IApplicationResponse>;

  constructor(@Inject(MAT_DIALOG_DATA) public changeOwnerData, public snackbarService: SnackbarService,
    private rxjsService: RxjsService, public dialog: MatDialog,
    private crudService: CrudService, private httpCancelService: HttpCancelService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.createFormGroup();
    this.onEmployeeFullNameFormControlValueChanges()
  }

  createFormGroup(): void {
    this.formGroup = this.formBuilder.group({
      fullName: ['', [Validators.required]]
    });
  }

  onEmployeeFullNameFormControlValueChanges(): void {
    this.formGroup
      .get("fullName")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((search: IApplicationResponse) => {
          this.getLoopableCDMObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
            UserModuleApiSuffixModels.GET_EMPLOYEE_SEARCH, null, true, prepareRequiredHttpParams({ search }));
        });
  }

  onSelectedItemOption(selectedObject): void {
    if (!selectedObject) return;
    setTimeout(() => {
      this.changeOwnerData.changeOwnerDetail = selectedObject;
      this.formGroup.patchValue({
        fullName: selectedObject.fullName
      }, { emitEvent: false, onlySelf: true });
    }, 200);
  }

  clearFormControlValues() {
    this.changeOwnerData.changeOwnerDetail = null;
  }

  onSubmit() {
    this.formGroup.get('fullName').markAllAsTouched();
    if (this.formGroup.invalid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.BOUNDARIES_OWNER_DETAILS, {
      boundaryId: this.changeOwnerData.boundaryId,
      assignToId: this.changeOwnerData.changeOwnerDetail?.['employeeId'],
      modifiedUserId: this.changeOwnerData.userId
    }).subscribe((response: IApplicationResponse) => {
      if (response.statusCode == 200 && response.isSuccess) {
        this.dialog.closeAll();
      }
    });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
