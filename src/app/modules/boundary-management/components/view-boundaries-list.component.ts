import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CommonPaginationConfig,
  CrudType, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  prepareRequiredHttpParams, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, SnackbarService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes
} from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../shared/models/prime-ng-table-list-component-variables.model';
import { ReusablePrimeNGTableFeatureService } from '../../../shared/services/reusable-primeng-table-features.service';
@Component({
  selector: 'view-boundaries-list',
  templateUrl: './view-boundaries-list.component.html'
})
export class ViewBoundariesListComponent extends PrimeNgTableVariablesModel implements OnInit {
  @Input() response;
  @Input() columns = [];
  @Input() type = 'others';
  @Input() boundaryId = '';
  @Input() isANewRequest = false;
  @Input() keyNameOfSidebar = '';
  @Output() emittedEvent = new EventEmitter();
  @Output() emitSelectedOwnerDetails = new EventEmitter();
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  searchColumns: any;
  primengTableConfigProperties;
  selectedMenuItemRowData = {};
  changeOwnerDetail = {};
  selectedPageSize = CommonPaginationConfig.defaultPageSize;

  constructor(
    private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private tableFilterFormService: TableFilterFormService,
    private router: Router, private momentService: MomentService, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private crudService: CrudService, private rxjsService: RxjsService, private store: Store<AppState>) {
    super();
    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});
    this.primengTableConfigProperties = {
      tableCaption: "",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "",
            dataKey: this.type == 'rollbacks' ? 'boundaryBackupId ' : 'boundaryId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [],
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: SalesModuleApiSuffixModels.BOUNDARIES,
            moduleName: ModulesBasedApiSuffix.SALES,
          }]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns);
    this.columnFilterRequest();
    this.rxjsService.setGlobalLoaderProperty(true);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      if (response[0][this.keyNameOfSidebar]) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, response[0][this.keyNameOfSidebar]);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
      this.loggedInUserData = new LoggedInUserModel(response[1]);
    });
  }

  ngOnChanges() {
    if (this.response) {
      // clear all search keywords in all the fields based on the conditions where included in the respective components
      // 'isANewRequest' property and 'response.requestType' is 'fieldLevelSearch' are added in the respective components whichever is reused this component
      if (this.isANewRequest && this.response?.requestType !== 'fieldLevelSearch') {
        this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns);
        this.columnFilterRequest();
      }
      this.dataList = this.response.resources?.boundaries ? this.response.resources.boundaries : this.response.resources;
      this.totalRecords = this.response.resources?.boundaries ? this.response.resources.boundaries.length : this.response.totalCount;
    }
    if (this.type) {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].datakey = (this.type == 'boundary configuration dashboard' ||
        this.type == 'boundary approval dashboard') ? 'boundaryApprovalConfigId' : 'boundaryId';
      switch (this.type.toLowerCase()) {
        case 'sales':
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
            { field: 'boundaryRequestRefNo', header: 'Request ID' },
            { field: 'boundaryName', header: 'Boundary' },
            { field: 'description', header: 'Desc' },
            { field: 'assignTo', header: 'Boundary Owner' },
            { field: 'modifiedDate', header: 'Last Modified' },
            { field: 'areaPartition', header: 'Area Partition' },
            { field: 'comments', header: 'Comments' },
            { field: 'levelIndicator', header: 'Level Indicator' }, { field: 'size', header: 'Size' },
            { field: 'isActive', header: 'Status', width: '200px' },
            { field: 'options', header: 'Options' }];
          break;
        case 'technical':
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
            { field: 'boundaryRequestRefNo', header: 'Request ID' },
            { field: 'boundaryName', header: 'Boundary' },
            { field: 'provinceName', header: 'Province' },
            { field: 'regionName', header: 'Region' },
            { field: 'divisionName', header: 'Division' },
            { field: 'districtName', header: 'District' },
            { field: 'branchName', header: 'Branch' },
            { field: 'assignTo', header: 'Boundary Owner' },
            { field: 'modifiedDate', header: 'Last Modified' },
            { field: 'areaPartition', header: 'Area Partition' },
            { field: 'comments', header: 'Comments' },
            { field: 'levelIndicator', header: 'Level Indicator' }, { field: 'size', header: 'Size' },
            { field: 'isActive', header: 'Status', width: '200px' },
            { field: 'options', header: 'Options' }];
          break;
        case 'operations':
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
            { field: 'boundaryRequestRefNo', header: 'Request ID' },
            { field: 'boundaryName', header: 'Area Name' }, { field: 'mainAreaName', header: 'Main Area' },
            { field: 'mainAreaCode', header: 'Main Area ID' },
            { field: 'provinceName', header: 'Province' },
            { field: 'regionName', header: 'Region' },
            { field: 'divisionName', header: 'Division' },
            { field: 'districtName', header: 'District' },
            { field: 'branchName', header: 'Branch' },
            { field: 'subBranchName', header: 'Sub Branch' },
            { field: 'assignTo', header: 'Boundary Owner' },
            { field: 'modifiedDate', header: 'Last Modified' }, { field: 'businessArea', header: 'Business Area' },
            { field: 'opsManagementTypeName', header: 'OpsMgmt' }, { field: 'sapContract', header: 'SAP Contract' },
            { field: 'isOutsideResponse', header: 'Outside Response' },
            { field: 'isOutsideResponse', header: 'RecordID' },
            { field: 'areaPartition', header: 'Area Partition' },
            { field: 'comments', header: 'Comments' },
            { field: 'levelIndicator', header: 'Level Indicator' }, { field: 'size', header: 'Size' },
            { field: 'isActive', header: 'Status', width: '200px' },
            { field: 'options', header: 'Options' }];
          break;
        case 'others':
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
            { field: 'documentName', header: 'File Name' },
            { field: 'boundaryLayerName', header: 'Layer Name' },
            { field: 'createdDate', header: 'Date And Time' },
            { field: 'createdUser', header: 'Uploader' },
            { field: 'description', header: 'Description' },
            { field: 'statusName', header: 'Status' }];
          break;
        case 'others detail':
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
            { field: 'boundaryName', header: 'Station' },
            { field: 'phoneNo1', header: 'Phone 1' },
            { field: 'phoneNo2', header: 'Phone 2' },
            { field: 'email', header: 'Email' }];
          break;
        case 'boundary approval dashboard':
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
            { field: 'boundaryRequestRefNo', header: 'Request ID' },
            { field: 'boundaryName', header: 'Boundary Name' },
            { field: 'createdDate', header: 'Date And Time' },
            { field: 'boundaryTypeName', header: 'Boundary Type' },
            { field: 'divisionName', header: 'Division' }, { field: 'districtName', header: 'District' },
            { field: 'boundaryStatusName', header: 'Status' }];
          break;
        case 'boundary configuration dashboard':
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
            { field: 'description', header: 'Name' },
            { field: 'boundaryTypeName', header: 'Boundary Type' },
            { field: 'boundaryLayerName', header: 'Layer' },
            { field: 'divisionName', header: 'Division' }, { field: 'branchName', header: 'Branch' }, { field: 'totalApprovals', header: 'Total Approvals' }];
          break;
        case 'rollbacks':
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
            { field: 'boundaryBackupRefNo', header: 'Backup ID' },
            { field: 'boundaryLayerName', header: 'Layer' },
            { field: 'boundaryName', header: 'Boundary Name' },
            { field: 'createdDate', header: 'Creation Date' },
            { field: 'goLiveDate', header: 'Go Live Date' },
            { field: 'expiryDate', header: 'Expiry Date' },
            { field: 'statusName', header: 'Status' }];
          break;
        case 'rollback detail':
          if (this.response?.boundaryTypeNameForReference) {
            this.prepareTableColumns(this.response.boundaryTypeNameForReference);
          }
          break;
      }
    }
    if (this.keyNameOfSidebar !== '') {
      this.combineLatestNgrxStoreData();
    }
  }

  prepareTableColumns(loopableCase: string) {
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableFieldsSearch = false;
    switch (loopableCase.toLowerCase()) {
      case 'sales':
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
          { field: 'boundaryRequestRefNo', header: 'Request ID' },
          { field: 'boundaryName', header: 'Boundary Name' },
          { field: 'description', header: 'Desc' },
          { field: 'assignTo', header: 'Boundary Owner' },
          { field: 'modifiedDate', header: 'Last Modified' },
          { field: 'areaPartition', header: 'Area Partition' },
          { field: 'comments', header: 'Comments' },
          { field: 'levelIndicator', header: 'Level Indicator' }, { field: 'size', header: 'Size' }];
        break;
      case 'technical':
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
          { field: 'boundaryRequestRefNo', header: 'Request ID' },
          { field: 'boundaryName', header: 'Boundary' },
          { field: 'provinceName', header: 'Province' },
          { field: 'regionName', header: 'Region' },
          { field: 'divisionName', header: 'Division' },
          { field: 'districtName', header: 'District' },
          { field: 'branchName', header: 'Branch' },
          { field: 'assignTo', header: 'Boundary Owner' },
          { field: 'modifiedDate', header: 'Last Modified' },
          { field: 'areaPartition', header: 'Area Partition' },
          { field: 'comments', header: 'Comments' },
          { field: 'levelIndicator', header: 'Level Indicator' }, { field: 'size', header: 'Size' }];
        break;
      case 'operations':
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
          { field: 'boundaryName', header: 'Area Name' }, { field: 'boundaryRequestRefNo', header: 'Request ID' },
          { field: 'mainAreaName', header: 'Main Area' }, { field: 'mainAreaCode', header: 'Main Area ID' },
          { field: 'provinceName', header: 'Province' },
          { field: 'regionName', header: 'Region' },
          { field: 'divisionName', header: 'Division' },
          { field: 'districtName', header: 'District' },
          { field: 'branchName', header: 'Branch' },
          { field: 'subBranchName', header: 'Sub Branch' },
          { field: 'assignTo', header: 'Boundary Owner' },
          { field: 'modifiedDate', header: 'Last Modified' }, { field: 'businessArea', header: 'Business Area' },
          { field: 'opsManagementTypeName', header: 'OpsMgmt' }, { field: 'sapContract', header: 'SAP Contract' },
          { field: 'isOutsideResponse', header: 'Outside Response' },
          { field: 'isOutsideResponse', header: 'RecordID' },
          { field: 'areaPartition', header: 'Area Partition' },
          { field: 'comments', header: 'Comments' },
          { field: 'levelIndicator', header: 'Level Indicator' }, { field: 'size', header: 'Size' }];
        break;
      case 'lss':
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [{ field: 'areaId', header: 'Area ID' },
        { field: 'boundaryName', header: 'Boundary Name' },
        { field: 'description', header: 'Desc' },
        { field: 'assignTo', header: 'Boundary Owner' },
        { field: 'modifiedDate', header: 'Last Modified' },
        { field: 'areaPartition', header: 'Area Partition' },
        { field: 'comments', header: 'Comments' },
        { field: 'levelIndicator', header: 'Level Indicator' }, { field: 'size', header: 'Size' }];
        break;
      case 'others':
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [
          { field: 'documentName', header: 'File Name' },
          { field: 'boundaryLayerName', header: 'Layer Name' },
          { field: 'createdDate', header: 'Date And Time' },
          { field: 'createdUser', header: 'Uploader' },
          { field: 'description', header: 'Description' },
          { field: 'statusName', header: 'Status' }];
        break;
    }
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns);
    this.columnFilterRequest();
  }

  loadPaginationLazy(event): void {
    //store selected page size
    this.selectedPageSize = event.rows ? event.rows : CommonPaginationConfig.defaultPageSize;
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["maximumRows"] = event.rows;
    if (event.sortField) {
      row["sortOrderColumn"] = event.sortField;
    }
    if (event.sortField) {
      row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    }
    if (!row['searchColumns']) {
      row['searchColumns'] = this.searchColumns;
    }
    else {
      row['searchColumns'] = event.filters;
    }
    this.onCRUDRequested(CrudType.GET, row);
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          let row = {};
          row['searchColumns'] = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, row));
        })
      )
      .subscribe();
  }

  onMenuItemClicked(type: string, rowData) {
    switch (type) {
      case 'change owner':
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0]?.canChangeOwner) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[0]?.canChangeOwner == true) {
          this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.BOUNDARIES_OWNER_DETAILS, null, true,
            prepareRequiredHttpParams({ boundaryId: rowData['boundaryId'] }))
            .subscribe((response: IApplicationResponse) => {
              if (response.resources && response.statusCode == 200 && response.isSuccess) {
                response.resources.boundaryId = rowData['boundaryId'];
                this.emitSelectedOwnerDetails.emit({ changeOwnerDetail: response.resources });
              }
              else {
                this.emitSelectedOwnerDetails.emit({ changeOwnerDetail: { boundaryId: rowData['boundaryId'] } });
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
        break;
      case 'edit boundary':
        this.router.navigate([`/my-tasks/boundary-management/view-request/add-edit`], {
          queryParams: {
            boundaryRequestId: rowData['boundaryRequestId'],
            boundaryRequestRefNo: rowData['boundaryRequestRefNo'],
            fromUrl: this.type
          }
        });
        break;
    }
  }

  onChangeStatus(rowData, index) {
    this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
      this.primengTableConfigProperties, rowData)?.onClose?.subscribe((result) => {
        if (result) {
          this.emittedEvent.emit(true);
        }
        else if (!result) {
          this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object): void {
    let otherParams = {};
    if (this.searchForm.value.searchKeyword) {
      otherParams["search"] = this.searchForm.value.searchKeyword;
    }

    if (CrudType.GET === type && Object.keys(row).length > 0) {
      if (row['searchColumns']) {
        Object.keys(row['searchColumns']).forEach((key) => {
          otherParams[key] = row['searchColumns'][key]['value'] ? row['searchColumns'][key]['value'] :
            row['searchColumns'][key];
          if (key.toLowerCase().includes('date')) {
            otherParams[key] = this.momentService.localToUTC(row['searchColumns'][key]);
          }
          else {
            otherParams[key] = row['searchColumns'][key];
          }
        });
      }
      otherParams['sortOrder'] = row['sortOrder'];
      if (row['sortOrderColumn']) {
        otherParams['sortOrderColumn'] = row['sortOrderColumn'];
      }
    }
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      case CrudType.GET:
        this.openAddEditPage(CrudType.GET, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?): void {
    switch (type) {
      case CrudType.CREATE:
        if (this.type == 'others') {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canUpload) {
            this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canUpload) {
            this.router.navigateByUrl("/boundary-management/view/other-boundaries/import");
          }
        }
        else if (this.type == 'boundary configuration dashboard') {
          this.router.navigateByUrl("/configuration/boundary-management/boundary-approvers/add-edit");
        }
        break;
      case CrudType.EDIT:
        switch (this.type) {
          case "others":
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canUpload) {
              this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canUpload) {
              this.router.navigate(["/boundary-management/view/other-boundaries/detail"],
                { queryParams: { boundaryLayerId: editableObject['boundaryLayerId'] } });
            }
          case "boundary configuration dashboard":
            this.router.navigate(["/configuration/boundary-management/boundary-approvers/add-edit"],
              { queryParams: { boundaryApprovalConfigId: editableObject['boundaryApprovalConfigId'] } });
            break;
          case "boundary approval dashboard":
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
              this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            } else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
              this.router.navigate([`/my-tasks/boundary-management/approval-dashboard/add-edit`], {
                queryParams: {
                  boundaryRequestId: editableObject['boundaryRequestId'],
                  boundaryRequestRefNo: editableObject['boundaryRequestRefNo'],
                  fromUrl: 'DOAApprovalDashboard'
                }
              });
            }
            break;
          case "rollbacks":
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
              this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            } else if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
             this.router.navigate(["boundary-management/view/rollbacks/add-edit"],
              { queryParams: { boundaryBackupId: editableObject['boundaryBackupId'], boundaryTypeId: editableObject['boundaryTypeId'] } });
            }
            break;
          default:
            this.onMenuItemClicked('edit boundary', editableObject);
            break;
        }
        break;
      case CrudType.GET:
        if (!editableObject['maximumRows']) {
          editableObject['maximumRows'] = this.selectedPageSize;
          editableObject['pageIndex'] = CommonPaginationConfig.defaultPageIndex;
        }
        this.emittedEvent.emit(editableObject);
        break;
    }
  }
}