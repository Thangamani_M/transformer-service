import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DayWeekMonthComponent } from './day-week-month/day-week-month.component';


const routes: Routes = [

    { path: '', redirectTo: 'day-week-month-calender', pathMatch: 'full' },
    { path: 'day-week-month-calender', component: DayWeekMonthComponent, data: { title: 'day-week-month-calender' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CalenderDemoRoutingModule { }
