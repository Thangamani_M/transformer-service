import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, destructureAfrigisObjectAddressComponents, formConfigs, getFullFormatedAddress, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { CorporateConractAddEditModel, CorporateConractAddressModel } from '@modules/panic-app/models/corporate-contract.model';
import { PanicAppApiSuffixModels } from '@modules/panic-app/shared';
import { NewAddressPopupComponent, SalesModuleApiSuffixModels } from '@modules/sales';
import { Store, select } from '@ngrx/store';

@Component({
  selector: 'app-corporate-contract-add-edit',
  templateUrl: './corporate-contract-add-edit.component.html'
})
export class CorporateContractAddEditComponent implements OnInit {

  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  alphaNumericConfig = new CustomDirectiveConfig({
    isAnAlphaNumericOnly: true,
  });
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({
    isAlphaNumericSomeSpecialCharterOnly: true,
  });
  countryCodes = countryCodes;

  isNew = 1;
  physicalAddressData = [];
  postalAddressData = [];
  corporateAddEditForm: FormGroup
  loggedUser: UserLogin
  corporateUserId = ""
  constructor(private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private router: Router, private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParams.subscribe(param => {
      this.corporateUserId = param['id'];
    })
  }

  ngOnInit() {
    this.createCorprateContractAddEditForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.corporateUserId) {
      this.getDetails()
      this.isNew = 0;
    }
  }

  createCorprateContractAddEditForm() {
    let corporateModel = new CorporateConractAddEditModel();
    this.corporateAddEditForm = this.formBuilder.group({});
    Object.keys(corporateModel).forEach((key) => {
      switch (key) {
        case "corporateContractPhysicalAddress":
          this.corporateAddEditForm.addControl(key, this.createAddressModel());
          break;
        case "corporateContractPostalAddress":
          this.corporateAddEditForm.addControl(key, this.createAddressModel());
          break;
        default:
          this.corporateAddEditForm.addControl(key, new FormControl(corporateModel[key]));
          break;
      }
    })
    this.corporateAddEditForm.get("createdUserId").setValue(this.loggedUser?.userId)
    this.corporateAddEditForm.get("modifiedUserId").setValue(this.loggedUser?.userId)
    this.corporateAddEditForm.get("createdDate").setValue(new Date().toDateString())
    this.corporateAddEditForm = setRequiredValidator(this.corporateAddEditForm,
      ["companyName", "registrationNumber", "vatNumber", "emailId", "mobileNo"]);

  }
  createAddressModel(basicInfo?: CorporateConractAddressModel): FormGroup {
    let addressModel = new CorporateConractAddressModel(basicInfo);
    let formControls = {};
    Object.keys(addressModel).forEach((key) => {
      formControls[key] = new FormControl(addressModel[key]);
    });

    return this.formBuilder.group(formControls);
  }
  get corporateContractPhysicalAddress(): FormGroup {
    if (!this.corporateAddEditForm) return;
    return this.corporateAddEditForm.get("corporateContractPhysicalAddress") as FormGroup;
  }
  get corporateContractPostalAddress(): FormGroup {
    if (!this.corporateAddEditForm) return;
    return this.corporateAddEditForm.get("corporateContractPostalAddress") as FormGroup;
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.CORPORATE_CONTRACT, this.corporateUserId).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        response.resources.corporateContractPhysicalAddress.createdDate = new Date()
        response.resources.corporateContractPostalAddress.createdDate = new Date()
        if (response.resources.corporateContract.psiraExpiryDate) {
          response.resources.corporateContract.psiraExpiryDate = new Date(response.resources.corporateContract.psiraExpiryDate)
        }
        this.corporateContractPhysicalAddress.get("addressData").setValue(
          {
            description: response.resources.corporateContractPhysicalAddress.formatedAddress
          }, { emitEvent: false, onlySelf: true }
        );
        this.corporateContractPostalAddress.get("addressData").setValue(
          {
            description: response.resources.corporateContractPostalAddress.formatedAddress
          }, { emitEvent: false, onlySelf: true }
        );
        this.corporateContractPhysicalAddress.patchValue(response.resources.corporateContractPhysicalAddress)
        this.corporateContractPostalAddress.patchValue(response.resources.corporateContractPostalAddress)

        this.corporateAddEditForm.patchValue(response.resources.corporateContract);
        this.rxjsService.setGlobalLoaderProperty(false)

      }
    })
  }


  filterSingle(event, type) {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS,
      null,
      true,
      prepareRequiredHttpParams({
        searchText: event.query,
        isAfrigisSearch: true,
      })
    ).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        if (type == 'physical') {
          this.physicalAddressData = response.resources;
        } else {
          this.postalAddressData = response.resources;

        }
      }
      this.rxjsService.setGlobalLoaderProperty(false)

    })

  }
  selectAddress(event, type) {
    this.getAddressFullDetails(event?.seoid, type)
  }

  getAddressFullDetails(seoid: string, type: string): void {
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_ADDRESS_DETAILS,
        undefined,
        false,
        prepareRequiredHttpParams({ seoid })
      )
      .subscribe((response: IApplicationResponse) => {
        if (
          response.isSuccess &&
          response.statusCode === 200 &&
          response.resources
        ) {
          this.rxjsService.setGlobalLoaderProperty(false)
          if (type == "physical") {
            this.corporateAddEditForm.controls["corporateContractPhysicalAddress"]
              .get("jsonObject")
              .setValue(JSON.stringify(response.resources));
            response.resources.addressDetails.forEach((addressObj) => {
              this.corporateContractPhysicalAddress
                .get("addressConfidentLevelId")
                .setValue(addressObj.confidence);
              this.patchAddressFormGroupValues(
                "physical",
                addressObj,
                addressObj["address_components"]
              );
            });
            this.corporateContractPhysicalAddress.updateValueAndValidity();
          } else {
            this.corporateAddEditForm.controls["corporateContractPostalAddress"]
              .get("jsonObject")
              .setValue(JSON.stringify(response.resources));
            response.resources.addressDetails.forEach((addressObj) => {
              this.corporateContractPhysicalAddress
                .get("addressConfidentLevelId")
                .setValue(addressObj.confidence);
              this.patchAddressFormGroupValues(
                "postal",
                addressObj,
                addressObj["address_components"]
              );
            });
            this.corporateContractPhysicalAddress.updateValueAndValidity();
          }
        }
      })
  }
  patchAddressFormGroupValues(
    type: string,
    addressObj,
    addressComponents: Object[]
  ) {
    addressObj.geometry.location.lat = addressObj.geometry.location.lat
      ? addressObj.geometry.location.lat
      : "";
    addressObj.geometry.location.lng = addressObj.geometry.location.lng
      ? addressObj.geometry.location.lng
      : "";

    if (addressObj.geometry.location.lat && addressObj.geometry.location.lng) {
      addressObj.geometry.location.latLong = `${addressObj.geometry.location.lat}, ${addressObj.geometry.location.lng}`;
    } else {
      addressObj.geometry.location.latLong = "";
    }

    let {
      suburbName,
      cityName,
      provinceName,
      postalCode,
      streetName,
      streetNo,
      buildingNo,
      buildingName,
      estateName,
      estateStreetName,
      estateStreetNo,
    } = destructureAfrigisObjectAddressComponents(addressComponents);
    if (type == "physical") {
      this.corporateContractPhysicalAddress.patchValue(
        {
          latitude: addressObj.geometry.location.lat,
          longitude: addressObj.geometry.location.lng,
          latLong: addressObj.geometry.location.latLong,
          buildingName,
          buildingNo,
          estateName,
          estateStreetName,
          estateStreetNo,
          suburbName,
          cityName,
          provinceName,
          postalCode,
          streetName,
          streetNo,
        },
        { emitEvent: false, onlySelf: true }
      );
    } else {
      this.corporateContractPostalAddress.patchValue(
        {
          latitude: addressObj.geometry.location.lat,
          longitude: addressObj.geometry.location.lng,
          latLong: addressObj.geometry.location.latLong,
          buildingName,
          buildingNo,
          estateName,
          estateStreetName,
          estateStreetNo,
          suburbName,
          cityName,
          provinceName,
          postalCode,
          streetName,
          streetNo,
        },
        { emitEvent: false, onlySelf: true }
      );
    }
    this.setPostalAddress();
  }

  isPhysicalAddressSameAsPostalAddress() {
    if (this.corporateAddEditForm.get('isSameAsPhysicalAddress').value) {
      this.corporateContractPostalAddress.setValue(this.corporateContractPhysicalAddress.value);
      this.corporateContractPostalAddress.disable()
    } else {
      this.corporateContractPostalAddress.enable()

      this.corporateContractPostalAddress.reset()
    }
  }

  setPostalAddress() {
    if (this.corporateAddEditForm.get('isSameAsPhysicalAddress').value) {
      this.corporateContractPostalAddress.setValue(this.corporateContractPhysicalAddress.value);
    }
  }


  openAddressPopup(type): void {
    const dialogReff = this.dialog.open(NewAddressPopupComponent, {
      width: "750px",
      disableClose: true,
      data: { ...this.corporateAddEditForm.value, createdUserId: this.loggedUser.userId },
    });

    dialogReff.afterClosed().subscribe((result) => {
      if (result) {
      }
    });
    dialogReff.componentInstance.outputData.subscribe((result) => {
      if (result) {
        let addressObj = {
          formatedAddress: getFullFormatedAddress(result),
          addressData: { description: getFullFormatedAddress(result) },
          addressConfidentLevelId: result.addressConfidentLevelId,
          // addressId:result.addressId,
          suburbName: result.suburbName,
          cityName: result.cityName,
          provinceName: result.provinceName,
          postalCode: result.postalCode,
          streetName: result.streetName,
          streetNo: result.streetNo,
          buildingNo: result.buildingNo,
          buildingName: result.buildingName,
          estateName: result.estateName,
          estateStreetName: result.estateStreetName,
          estateStreetNo: result.estateStreetNo,
        }

        if (type == "physical") {
          this.corporateContractPhysicalAddress.patchValue(addressObj);
        } else {
          this.corporateContractPostalAddress.patchValue(addressObj);
        }
        this.rxjsService.setFormChangeDetectionProperty(true);
      }
      else {
      }
    });
  }

  onSubmit() {
    if (this.corporateAddEditForm.invalid) return '';



    let obj = this.corporateAddEditForm.getRawValue();

    if (obj.mobileNo) {
      obj.mobileNo = obj.mobileNo.toString().replace(/\s/g, "");
    }
    if (obj.psiraExpiryDate) {
      obj.psiraExpiryDate = obj.psiraExpiryDate.toDateString();
    }
    let api = this.corporateUserId ? this.crudService.update(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.CORPORATE_CONTRACT, obj) : this.crudService.create(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.CORPORATE_CONTRACT, obj)

    api.subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        if (response.resources) {
          this.corporateUserId = response.resources;
          this.goNext();
        }
      }
    })
  }

  goBack() {
    this.router.navigate(['/panic-app/corporate-contract'])
  }
  goNext() {
    if (this.corporateUserId) {
      this.router.navigate(['panic-app/corporate-contract/plan'], { queryParams: { id: this.corporateUserId, isNew: this.isNew } })
    }
  }

}
