import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PanicAppApiSuffixModels } from '@modules/panic-app';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-corporate-contract-details',
  templateUrl: './corporate-contract-details.component.html'
})
export class CorporateContractDetailsComponent implements OnInit {
  corporateId: any
  viewData: any = []
  detailsData = []
  creditControlData = []
  primengTableConfigProperties: any = {
    tableCaption: "Corporate Contract",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Panic App', relativeRouterUrl: '' }, { displayName: 'Corporate Contract', relativeRouterUrl: '/panic-app/corporate-contract' }, {
      displayName: 'View Corporate Contract'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'View Corporate Contract',
          enableBreadCrumb: true,
          enableAction: true,
          enableViewBtn: true
        }]
    }
  }

  constructor(
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private router: Router) {
    this.corporateId = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    if (this.corporateId) {
      this.getDetailsById()
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Corporate Contract"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.CORPORATE_CONTRACT_TARIFF_DETAILS, this.corporateId).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.viewData = [
          {
            name: 'COMPANY DETAILS', columns: [
              { name: 'Company Name', value: response?.resources?.corporateContractDetails?.companyName },
              { name: 'Company PRISA Register number', value: response?.resources?.corporateContractDetails?.psiraRegistrationNumber },
              { name: 'Company Register Number', value: response?.resources?.corporateContractDetails?.registrationNumber },
              { name: 'Comapany PRISA Expiry Date', value: response?.resources?.corporateContractDetails?.psiraExpiryDate },
              { name: 'Company SPOC Name', value: response?.resources?.corporateContractDetails?.spocName },
              { name: 'Company Email ID', value: response?.resources?.corporateContractDetails?.emailId },
              { name: 'Account Email ID', value: response?.resources?.corporateContractDetails?.accountsEmailId },
              { name: 'Company Phone Number', value: response?.resources?.corporateContractDetails?.mobileNoCountryCode + " " + response?.resources?.corporateContractDetails?.mobileNo },
              { name: 'Physical Address', value: response?.resources?.corporateContractDetails?.physicalAddress },
              { name: 'Postal Address', value: response?.resources?.corporateContractDetails?.postalAddress },
            ]
          },
          {
            name: 'PLAN / TARIFF INFO', columns: [
              { name: 'Plan Name', value: response?.resources?.corporateUsersTariffPlanDetails?.personalPanicTariffName },
              { name: 'Number of Licenses / Users', value: response?.resources?.corporateUsersTariffPlanDetails?.numberOfLicence },
            ]
          },
        ]

      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/panic-app/corporate-contract/add-edit'], { queryParams: { id: this.corporateId } });
        break;
    }
  }


  goBack() {
    this.router.navigate(['/panic-app/corporate-contract']);
  }
}
