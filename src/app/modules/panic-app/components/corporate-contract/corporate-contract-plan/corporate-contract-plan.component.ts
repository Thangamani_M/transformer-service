import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { CorporateConractPlanAddEditModel, PanicAppApiSuffixModels } from '@modules/panic-app';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-corporate-contract-plan',
  templateUrl: './corporate-contract-plan.component.html'
})
export class CorporateContractPlanComponent implements OnInit {

  constructor(private rxjsService: RxjsService, private crudService: CrudService, private formBuilder: FormBuilder,
    private store: Store<AppState>, private router: Router, private activatedRoute: ActivatedRoute) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.activatedRoute.queryParams.subscribe(param => {
      this.corporateUserId = param['id']
      this.isNew = param['isNew']
    })

  }
  corporateUserId = ""
  planDropdownList = []
  corporatePlanForm: FormGroup
  loggedUser: UserLogin
  isShowEmail = false;
  formData = new FormData()
  isNew = 1;
  ngOnInit() {
    this.createCorporateContractAddForm()
    this.getPlanDropdown();
    this.onFormControlChange();
    if(this.isNew!=1){
      this.getDetails();
    }
  }


  createCorporateContractAddForm(): void {
    let model = new CorporateConractPlanAddEditModel();
    this.corporatePlanForm = this.formBuilder.group({});
    Object.keys(model).forEach((key) => {
      this.corporatePlanForm.addControl(key, (key == 'createdUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(model[key]));
    });
    this.corporatePlanForm = setRequiredValidator(this.corporatePlanForm, ["personalPanicTariffId",'emailId']);

    // this.corporatePlanForm.get("corporateUserTariffId").setValue(this.corporateUserId)
    this.corporatePlanForm.get("createdDate").setValue(new Date())
  }

  onFormControlChange() {
    this.corporatePlanForm.get('isGenerateLicenceIdAndSendEmail').valueChanges.subscribe(status => {
      if (status) {
        this.isShowEmail = true;
      } else {
        this.isShowEmail = false;
      }
    })
  }
  getDetails(){
    this.crudService.get(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.CORPORATE_CONTRACT_TARIFF_DETAILS, this.corporateUserId).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
       this.isNew = response.resources?.corporateUsersTariffPlanDetails?.personalPanicTariffId ? 0:1
        this.corporatePlanForm.patchValue(response.resources.corporateUsersTariffPlanDetails);
      }else{
        this.isNew = 1;
      }
    })
  }
  onChangePlan() {
    let plan = this.planDropdownList.find(item => item.id === this.corporatePlanForm.get("personalPanicTariffId").value);
    if (plan) this.corporatePlanForm.get("personalPanicTariffName").setValue(plan.planName);
  }
  uploadFiles(event) {
    this.formData = new FormData()
    this.formData.append('file', event.target.files[0]);
  }
  emitUploadedFiles(uploadedFile) {
    if (!uploadedFile || uploadedFile?.length == 0) {
      return;
    }
    this.formData = new FormData()
    this.formData.append('file', uploadedFile);
    let file_Name = uploadedFile['name'];
    this.corporatePlanForm.get("uploadUserFileName").setValue(file_Name)
  }


  onSubmit() {
    this.corporatePlanForm.get("corporateUserId").setValue(this.corporateUserId)
    if (this.corporatePlanForm.invalid) return '';
    if (this.corporatePlanForm.get("isGenerateLicenceIdAndSendEmail").value) {
      this.crudService.create(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.CORPORATE_CONTRACT_TARIFF_PLAN, this.corporatePlanForm.value).subscribe(response => {
        if (response.isSuccess && response.statusCode ==200) {
          this.goNext();
        }
      })
    } else {

      if (this.corporatePlanForm.invalid) return '';

      this.formData.delete("data");
      this.formData.append("data", JSON.stringify(this.corporatePlanForm.value));
      this.crudService.create(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.CORPORATE_CONTRACT_TARIFF_PROCESS_FILE, this.formData).subscribe(response => {
        if (response.isSuccess && response.statusCode ==200) {

          this.goNext()
        }
      })
    }
  }


  goBack() {
    this.router.navigate(['panic-app/corporate-contract/add-edit'], { queryParams: { id: this.corporateUserId } })
  }
  goNext() {
    this.router.navigate(['panic-app/corporate-contract'])
  }

  getPlanDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.UX_CORPORATE_CONTRACT_TARIFF_PLAN).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.planDropdownList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false)

    })
  }

}
