import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CorporateContractAddEditComponent } from './corporate-contract-add-edit/corporate-contract-add-edit.component';
import { CorporateContractDetailsComponent } from './corporate-contract-details/corporate-contract-details.component';
import { CorporateContractPlanComponent } from './corporate-contract-plan/corporate-contract-plan.component';
import { CorporateContractComponent } from './corporate-contract.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


const routes: Routes = [
  { path: '', component: CorporateContractComponent, canActivate: [AuthGuard], data: { title: 'Customer Corporate' } },
  { path: 'add-edit', component: CorporateContractAddEditComponent, canActivate: [AuthGuard], data: { title: 'Customer Corporate Add/Edit' } },
  { path: 'plan', component: CorporateContractPlanComponent, canActivate: [AuthGuard], data: { title: 'Customer Corporate Plan' } },
  { path: 'view', component: CorporateContractDetailsComponent, canActivate: [AuthGuard], data: { title: 'Customer Corporate Details' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class CorporateContractRoutingModule { }
