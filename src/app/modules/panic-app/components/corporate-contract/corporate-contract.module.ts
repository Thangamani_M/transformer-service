import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NewAddressModule } from '@modules/sales/components/task-management/lead-info/new-address/new-address.module';
import { CorporateContractAddEditComponent } from './corporate-contract-add-edit/corporate-contract-add-edit.component';
import { CorporateContractDetailsComponent } from './corporate-contract-details/corporate-contract-details.component';
import { CorporateContractHeaderComponent } from './corporate-contract-header/corporate-contract-header.component';
import { CorporateContractPlanComponent } from './corporate-contract-plan/corporate-contract-plan.component';
import { CorporateContractRoutingModule } from './corporate-contract-routing.module';
import { CorporateContractComponent } from './corporate-contract.component';

@NgModule({
  declarations: [CorporateContractComponent,CorporateContractAddEditComponent,CorporateContractHeaderComponent,CorporateContractPlanComponent,CorporateContractDetailsComponent],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule, LayoutModule, MaterialModule, SharedModule,
    NewAddressModule,
    CorporateContractRoutingModule
  ],
  entryComponents:[CorporateContractHeaderComponent],
  exports:[CorporateContractHeaderComponent]
})
export class CorporateContractModule { }
