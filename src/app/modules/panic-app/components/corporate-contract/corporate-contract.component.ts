import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse,  ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { PanicAppApiSuffixModels } from "@modules/panic-app/shared";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';


@Component({
  selector: 'app-corporate-contract',
  templateUrl: './corporate-contract.component.html'
})
export class CorporateContractComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  constructor(
    private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService

  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Corporate Contract",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Panic App', relativeRouterUrl: '' }, { displayName: 'Corporate Contract', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Corporate Contract",
            dataKey: "corporateUserId",
            enableBreadCrumb: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "companyName", header: "Company Name", width: "150px" },
              { field: "emailId", header: "Company Email Address", width: "200px" },
              { field: "registrationNumber", header: "Registration Number", width: "200px" },
              { field: "psiraRegistrationNumber", header: "PSIRA Register Number", width: "200px" },
              { field: "spocName", header: "Spoc Name", width: "150px" },
              { field: "suburbName", header: "Suburb Name", width: "150px" },
              { field: "cityName", header: "City Name", width: "150px" },
              { field: "isActive", header: "Status", width: "150px" }
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: PanicAppApiSuffixModels.CORPORATE_CONTRACT,
            moduleName: ModulesBasedApiSuffix.PANIC_APP
          }
        ],
      },
    };

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Corporate Contract"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object
  ) {
    this.loading = true;
    let panicAppModuleApiSuffixModels: PanicAppApiSuffixModels;
    panicAppModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[
        this.selectedTabIndex
      ].apiSuffixModel;

    this.crudService
      .get(
        ModulesBasedApiSuffix.PANIC_APP,
        panicAppModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      )
      .subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList = data.resources;;
          this.totalRecords = 0;
        }
      });
  }
  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(["panic-app/corporate-contract/add-edit"]);
        break;
      case CrudType.VIEW:
        this.router.navigate(["panic-app/corporate-contract/view"], { queryParams: { id: editableObject['corporateUserId'] } });
        break;
    }
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

}
