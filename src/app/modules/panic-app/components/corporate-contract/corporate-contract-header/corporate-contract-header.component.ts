import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-corporate-contract-header',
  templateUrl: './corporate-contract-header.component.html',
  styleUrls: ['./corporate-contract-header.component.scss']
})
export class CorporateContractHeaderComponent implements OnInit {

  @Input() pageHeading :any
  @Input() currentPage :any = 1
  constructor() { }
  stepperArray = []
  totalSteps = 2;

  ngOnInit() {
    for (let index = 1; index <= this.totalSteps; index++) {
      let isActive = index <= this.currentPage
      this.stepperArray.push({
        index: index,
        active: isActive
      })
    }
  }

}
