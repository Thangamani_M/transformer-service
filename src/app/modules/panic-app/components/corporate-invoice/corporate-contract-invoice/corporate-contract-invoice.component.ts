import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, fileUrlDownload,ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { PanicAppApiSuffixModels } from "@modules/panic-app/shared";
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CommonChangeStatusModelComponent } from '../common-change-status-model/common-change-status-model.component';
@Component({
  selector: 'app-corporate-contract-invoice',
  templateUrl: './corporate-contract-invoice.component.html',
})
export class CorporateContractInvoiceComponent extends PrimeNgTableVariablesModel  implements OnInit {
  primengTableConfigProperties: any;
  statuses: any = [];
  constructor(  private crudService: CrudService,
    private dialog: MatDialog,private rxjsService: RxjsService,  private store: Store<AppState>, private snackbarService: SnackbarService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Corporate Contract Invoice",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Panic App', relativeRouterUrl: '' }, { displayName: 'Corporate Contract Invoice', relativeRouterUrl: '' }],
      tableComponentConfigs: {
          tabsList: [
              {
                  caption: "",
                  dataKey: "invoiceId",
                  enableBreadCrumb: true,
                  enableReset: false,
                  enableAction: true,
                  enableGlobalSearch: false,
                  reorderableColumns: false,
                  resizableColumns: false,
                  enableScrollable: true,
                  checkBox: false,
                  enableRowDelete: false,
                  enableFieldsSearch: true,
                  enableHyperLink: false,
                  cursorLinkIndex: 0,
                  columns: [
                      { field: "invoiceRefNo", header: "Invoice Number", width: "150px" },
                      { field: "companyName", header: "Company Name", width: "150px" },
                      { field: "registrationNumber", header: "Company Registration Number", width: "150px" },
                      { field: "vatNumber", header: "Company VAT No", width: "150px" },
                      { field: "emailId", header: "Company Email ID", width: "150px" },
                      { field: "billingPlan", header: "Billing Plan", width: "150px" },
                      { field: "planName", header: "Plan Name", width: "150px" },
                      { field: "invoiceAmount", header: "Invoice Amount", width: "150px" },
                      { field: "status", header: "Status", width: "150px",isStatusClick:true },
                      { field: "documentPath", header: "Download", width: "150px",isDownloadLink: true, nofilter: true,nosort:true },
                  ],
                  enableAddActionBtn: false,
                  shouldShowDeleteActionBtn: false,
                  shouldShowCreateActionBtn: false,
                  areCheckboxesRequired: false,
                  isDateWithTimeRequired: true,
                  apiSuffixModel: PanicAppApiSuffixModels.CORPORATE_CONTRACT_INVOICE,
                  moduleName: ModulesBasedApiSuffix.PANIC_APP
              }
          ],
      },
  };
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Corporate Invoice"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.PANIC_APP,
      PanicAppApiSuffixModels.CORPORATE_CONTRACT_INVOICE,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredListData(
          row["pageIndex"], row["pageSize"], otherParams);
        break;
      case CrudType.EDIT:
        // if (otherParams == "documentPath" && row['documentPath']) {
        //   window.open(row['documentPath'])
        // }
        if (otherParams == "documentPath" && row['documentPath']) {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canDownload) {
	          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
          fileUrlDownload(row['documentPath'], row['documentPath'])
        }
        if(otherParams == 'status'){
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
	          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
          this.changeStatus(row);
        }
        break;
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  changeStatus(data) {
    if (data.status) {
        const dataObj = { rowData: data, status: this.statuses };
        const dialogReff = this.dialog.open(CommonChangeStatusModelComponent, { width: '700px', disableClose: true, data: dataObj });
        dialogReff.afterClosed().subscribe(result => {
            if (result) return;
            this.getRequiredListData();
        });
        this.rxjsService.setGlobalLoaderProperty(false);
    } else {
        this.snackbarService.openSnackbar("Please try selecting any other status", ResponseMessageTypes.WARNING);
    }
}
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
