import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from "@app/reducers";
import {  IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { commonchangestatusListModel } from '@modules/panic-app/models/common-change-status.model';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from "@ngrx/store";
import { HttpCancelService } from '@app/shared';
import { PanicAppApiSuffixModels } from '@modules/panic-app/shared';
@Component({
  selector: 'app-common-change-status-model',
  templateUrl: './common-change-status-model.component.html',
})
export class CommonChangeStatusModelComponent implements OnInit {
  userId = "";
  changeStatusForm: FormGroup;
  Statuses = [];
  constructor(private httpService: CrudService,private dialog: MatDialog, private httpCancelService: HttpCancelService, private rxjsService: RxjsService,private formBuilder: FormBuilder, private store: Store<AppState>,@Inject(MAT_DIALOG_DATA) public data: any) {
      this.changeStatusForm = this.formBuilder.group({});
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
          if (!userData) {
            return;
          }
          this.userId = userData["userId"];
        });
       
     }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createChangeStatusForm();
    this.getStatuses();
   
  }
  createChangeStatusForm(): void {
    let changeStatusModel = new commonchangestatusListModel();
    this.changeStatusForm = this.formBuilder.group({});
    Object.keys(changeStatusModel).forEach((key) => {
      this.changeStatusForm.addControl(key, new FormControl(changeStatusModel[key]));
    });
    this.changeStatusForm.controls['modifiedUserId'].setValue(this.userId);
    this.changeStatusForm.controls['invoiceId'].setValue(this.data?.rowData?.invoiceId);
    this.changeStatusForm = setRequiredValidator(this.changeStatusForm, ['paymentStatusId']);
  }

  getStatuses(): void {
      this.httpService.dropdown(ModulesBasedApiSuffix.BILLING,BillingModuleApiSuffixModels.UX_PAYMENT_STATUS).subscribe(
        (response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
          this.Statuses = response.resources;
          }
      });
  }
  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

  submit() {
    if (this.changeStatusForm.invalid) {
      return
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let reqObj = {
      ...this.changeStatusForm.value,
      invoiceId: this.data?.rowData?.invoiceId
    }
      reqObj = {
        invoiceId: this.data?.rowData?.invoiceId,
        paymentStatusId: this.changeStatusForm.get('paymentStatusId').value,
        modifiedUserId: this.changeStatusForm.get('modifiedUserId').value,
      }
      this.httpService.update(ModulesBasedApiSuffix.PANIC_APP,
        PanicAppApiSuffixModels.CORPORATE_CONTRACT_INVOICE_PAYMENT, reqObj).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setDialogOpenProperty(false);
        this.dialog.closeAll();
      }
    })
  }
  cancel() {
    this.dialog.closeAll();
  }

}
