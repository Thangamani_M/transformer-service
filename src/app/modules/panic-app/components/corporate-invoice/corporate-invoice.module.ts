import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CorporateContractInvoiceComponent } from './corporate-contract-invoice/corporate-contract-invoice.component';
import { CorporateInvoiceRoutingModule } from './corporate-invoice-routing.module';
import { CommonChangeStatusModelComponent } from './common-change-status-model/common-change-status-model.component';

@NgModule({
  declarations: [CorporateContractInvoiceComponent,CommonChangeStatusModelComponent],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule, LayoutModule, MaterialModule, SharedModule,
    CorporateInvoiceRoutingModule
  ],
  entryComponents:[CommonChangeStatusModelComponent],
})
export class CorporateInvoiceModule { }
