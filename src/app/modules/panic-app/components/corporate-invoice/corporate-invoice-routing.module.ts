import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CorporateContractInvoiceComponent } from './corporate-contract-invoice/corporate-contract-invoice.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: CorporateContractInvoiceComponent, canActivate: [AuthGuard], data: { title: 'Corporate Invoice' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class CorporateInvoiceRoutingModule { }
