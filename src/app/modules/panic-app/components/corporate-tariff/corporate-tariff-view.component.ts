import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { PanicAppApiSuffixModels } from "@modules/panic-app/shared";
import { IT_MANAGEMENT_COMPONENT } from "@modules/user";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";


@Component({
  selector: "app-corporate-tariff-view",
  templateUrl: "./corporate-tariff-view.component.html"
})
export class CorporateTariffViewComponent implements OnInit {

  viewData = []
  personalPanicTariffId: any
  primengTableConfigProperties: any = {
    tableCaption: "Tariff Corporate Tariff Config",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Panic App', relativeRouterUrl: '' }, { displayName: 'Corporate Tariff Config', relativeRouterUrl: '/panic-app/corporate-tariff' }, {
      displayName: 'View Corporate Tariff Config'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'View Corporate Tariff Config',
          enableBreadCrumb: true,
          enableAction: true,
          enableViewBtn: true
        }]
    }
  }
  constructor(private rxjsService: RxjsService, private crudService: CrudService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private snackbarService: SnackbarService) {

    this.activatedRoute.queryParams.subscribe(param => {
      this.personalPanicTariffId = param['id']
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Tariff Creation"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getDetails()
    this.viewData = [
      { name: 'Tariff Code', value: "-", order: 1 },
      { name: 'Tariff Description', value: "-", order: 2 },
      { name: 'Invoice Period', value: "-", order: 3 },
      { name: 'Valid From', value: "-", order: 4 },
      { name: 'Valid Till', value: "-", order: 5 },
      { name: 'Subscription Group', value: "-", order: 6 },
      { name: 'Status', value: "-", order: 7 },
    ]

  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.TARIFF_DETAILS, undefined, false, prepareRequiredHttpParams({ personalpanictariffId: this.personalPanicTariffId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.viewData = [
          { name: 'Tariff Code', value: response.resources.tariffCode, order: 1 },
          { name: 'Tariff Description', value: response.resources.tariffDescription, order: 2 },
          { name: 'Invoice Period', value: response.resources.billingIntervalName, order: 3 },
          { name: 'Valid From', value: response.resources.validFrom, order: 4 },
          { name: 'Valid Till', value: response.resources.validTill, order: 5 },
          { name: 'Subscription Group', value: response.resources.tariffGroup, order: 6 },
          {
            name: 'Status',
            value: response.resources?.isActive == true ? 'Active' : 'InActive',
            statusClass: response.resources?.cssClass ? response.resources.cssClass :
              (response.resources?.isActive == true ? 'status-label-green' : 'status-label-pink'),
            order: 7
          }
        ]

      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }
  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/panic-app/corporate-tariff/add-edit'], { queryParams: { id: this.personalPanicTariffId } });
        break;
    }
  }
  goBack() {
    this.router.navigate(['/panic-app/corporate-tariff']);
  }

}
