import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { PanicAppApiSuffixModels } from "@modules/panic-app/shared";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: "app-corporate-tariff-list",
  templateUrl: "./corporate-tariff-list.component.html"
})
export class CorporateTariffListComponent extends PrimeNgTableVariablesModel{
  primengTableConfigProperties: any;
  constructor(
    private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService

  ) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Corporate Tariff Config",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Panic App', relativeRouterUrl: '' }, { displayName: 'Corporate Tariff Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Call Escalation Config",
            dataKey: "personalPanicTariffId",
            enableBreadCrumb: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "tariffCode", header: "Tariff Code", width: "150px" },
              { field: "tariffDescription", header: "Tariff Description", width: "150px" },
              { field: "billingIntervalName", header: "Invoice Period", width: "150px" },
              { field: "validFrom", header: "Valid From", width: "150px", isDate:true },
              { field: "validTill", header: "Valid Till", width: "150px" , isDate:true},
              { field: "tariffGroup", header: "Subscription Group", width: "150px" },
              { field: "isActive", header: "Status", width: "150px" }
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: PanicAppApiSuffixModels.TRAIFF_LIST,
            moduleName: ModulesBasedApiSuffix.PANIC_APP
          }
        ],
      },
    };

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Tariff Creation"];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object
  ) {
    this.loading = true;
    let panicAppModuleApiSuffixModels: PanicAppApiSuffixModels;
    panicAppModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[
        this.selectedTabIndex
      ].apiSuffixModel;

    this.crudService
      .get(
        ModulesBasedApiSuffix.PANIC_APP,
        panicAppModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      )
      .subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.dataList = data.resources;;
          this.totalRecords = data.totalCount;
        } else {
          this.dataList =  data.resources;
          this.totalRecords = 0;
        }
      });
  }
  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(["panic-app/corporate-tariff/add-edit"]);
        break;
      case CrudType.VIEW:
        this.router.navigate(["panic-app/corporate-tariff/view"], { queryParams: { id: editableObject['personalPanicTariffId'] } });
        break;
    }
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

}
