import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CorporateTariffAddEditComponent, CorporateTariffListComponent, CorporateTariffViewComponent } from '@modules/panic-app/components/corporate-tariff';
import { CorporateTariffRoutingModule } from './corporate-tariff-routing.module';

@NgModule({
  declarations: [CorporateTariffListComponent, CorporateTariffAddEditComponent, CorporateTariffViewComponent],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule, LayoutModule, MaterialModule, SharedModule,
    CorporateTariffRoutingModule
  ]
})
export class CorporateTariffModule { }
