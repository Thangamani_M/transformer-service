import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CorporateTariffAddEditComponent } from './corporate-tariff-add-edit.component';
import { CorporateTariffListComponent } from './corporate-tariff-list.component';
import { CorporateTariffViewComponent } from './corporate-tariff-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: CorporateTariffListComponent,canActivate:[AuthGuard], data: {title: 'Corporate Tariff Config'}},
  { path: 'add-edit', component: CorporateTariffAddEditComponent,canActivate:[AuthGuard], data: {title: 'Tariff Creation'}},
  { path: 'view', component: CorporateTariffViewComponent, canActivate:[AuthGuard],data: {title: 'View - Corporate Tariff Config'}},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class CorporateTariffRoutingModule { }
