import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { PanicAppModule } from '@modules/panic-app';
import { TariffDefinitionAddEditComponent, TariffDefinitionListComponent } from '@modules/panic-app/components/tariff-definition-list';
import { TariffDefinitionListRoutingModule } from './tariff-definition-list-routing.module';


@NgModule({
  declarations: [TariffDefinitionListComponent, TariffDefinitionAddEditComponent],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule, LayoutModule, MaterialModule, SharedModule,
    TariffDefinitionListRoutingModule,
    PanicAppModule
  ]
})
export class TariffDefinitionListModule { }
