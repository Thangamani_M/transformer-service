import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { PanicAppApiSuffixModels, TariffDefinitionAddEditModel, TariffDefinitionConfigAddModel, TariffDefinitionConfigFeeAddModel } from '@modules/panic-app';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-tariff-definition-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.scss']
})
export class TariffDefinitionAddEditComponent implements OnInit {
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  loggedUser: UserLogin
  personalPanicTariffId = ''
  tariffDropdown = []
  tariffCategory = []
  chargeTypeList = []
  groupList = []
  operatorList = []
  billMethodTypeList = []
  valueTypeList = []
  tariffConfigFeeDetails = []
  tariffDefinitionForm: FormGroup
  tariffConfigFee: FormArray
  personalPanicConfigDetails: any = {}
  isDisabled = false
  primengTableConfigProperties: any = {
    tableCaption: "Add Tariff Definition",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Panic App', relativeRouterUrl: '' }, { displayName: 'Tariff Definition', relativeRouterUrl: '/panic-app/tariff-definition' }, {
      displayName: 'Add/Edit Tariff Definition'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Tariff Definition',
          enableBreadCrumb: true,
          enableAction: true,
        }]
    }
  }

  constructor(private rxjsService: RxjsService, private crudService: CrudService, private formBuilder: FormBuilder,
    private store: Store<AppState>, private router: Router, private activatedRoute: ActivatedRoute) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParams.subscribe(param => {
      this.personalPanicTariffId = param['id']
    })

  }

  ngOnInit(): void {

    this.getTariffCategory()
    this.getChargeTypeList()
    this.createTariffAddForm()
    this.getSubscriptionGroupDrodown()
    if (this.personalPanicTariffId) {
      this.getDetails()
    } else {
      this.getTariffCodeDropdown();
    }
    this.getOperatorDropdown();
    this.valueTypeList = [
      { id: 'd209cb2e-47c6-4adb-bcbd-34bc83c6ba01', displayName: 'less than' },
      { id: 'c409ca08-04d7-4c90-b39d-d3810ae5ae84', displayName: 'between' },
      { id: '054c4fcc-478c-4233-b9d3-a477b02d487e', displayName: 'greater than' },
    ];
    this.billMethodTypeList = [
      { id: true, displayName: 'Fixed' },
      { id: false, displayName: 'Per Unit' },
    ]
  }

  getDetails() {

    this.crudService.get(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.TARIFF_DEFINITION_DETAILS, undefined, false, prepareRequiredHttpParams({ PersonalPanicTariffConfigId: this.personalPanicTariffId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.personalPanicConfigDetails = response.resources;
        this.tariffConfigFeeDetails = this.personalPanicConfigDetails.tariffConfigFeeDetails

        this.tariffConfigFee = this.getTariffConfigFee
        this.tariffDropdown = [{
          id: this.personalPanicConfigDetails.tariffConfigDetails.personalPanicTariffId,
          displayName: this.personalPanicConfigDetails.tariffConfigDetails.tariffCode
        }]
        this.isDisabled = true;
        this.selectedCategory = this.personalPanicConfigDetails.tariffConfigFeeDetails[0].personalPanicTariffCategoryName
        this.selectedCategoryId = this.personalPanicConfigDetails.tariffConfigFeeDetails[0].personalPanicTariffCategoryId
        this.tariffDefinitionForm.controls["tariffConfig"].patchValue(this.personalPanicConfigDetails.tariffConfigDetails)

        for (let index = 0; index < (this.personalPanicConfigDetails.tariffConfigFeeDetails.length); index++) {
          this.tariffConfigFee.push(this.createTariffDefinitionConfigFeeFormArray(this.personalPanicConfigDetails.tariffConfigFeeDetails[index]));
        }
      }
    })
  }
  personalPanicTariffChange(id) {
    let filter = this.tariffDropdown.find(item => item.id == id)
    if (filter) {
      this.tariffDefinitionForm.controls["tariffConfig"].get('personalPanicTariffGroupId').setValue(filter?.tariffGroupId)
    }
  }
  createTariffAddForm(): void {
    let tariffDefinitionModel = new TariffDefinitionAddEditModel();
    this.tariffDefinitionForm = this.formBuilder.group({});
    let obj = {
      createdUserId: this.loggedUser.userId,
      createdDate: new Date()
    }
    this.tariffDefinitionForm.addControl('tariffConfig', this.createTariffDefinitionConfigForm(obj));
    this.tariffDefinitionForm.addControl("tariffConfigFee", this.formBuilder.array([])
    )

  }
  get getTariffConfigFee(): FormArray {
    if (!this.tariffDefinitionForm) return;
    return this.tariffDefinitionForm.get("tariffConfigFee") as FormArray;
  }

  get getTariffConfig(): FormGroup {
    if (!this.tariffDefinitionForm) return;
    return this.tariffDefinitionForm.get("getTariffConfig") as FormGroup;
  }

  createTariffDefinitionConfigForm(config?: TariffDefinitionConfigAddModel | any): FormGroup {
    let configModel = new TariffDefinitionConfigAddModel(config);
    let formControls = {};
    Object.keys(configModel).forEach((key) => {
      if (key == "personalPanicTariffConfigId" || key == 'tariffGroupId') {
        formControls[key] = new FormControl(configModel[key]);
      } else {
        formControls[key] = new FormControl(configModel[key], [
          Validators.required,
        ]);
      }
    });
    return this.formBuilder.group(formControls);
  }
  createTariffDefinitionConfigFeeFormArray(configFee?: TariffDefinitionConfigFeeAddModel): FormGroup {
    let configFeeModel = new TariffDefinitionConfigFeeAddModel(configFee);
    let formControls = {};
    Object.keys(configFeeModel).forEach((key) => {
      if (this.selectedCategory == "Utilization" && (key == "personalPanicTariffTypeId" || key == "personalPanicTariffCategoryId" || key == 'isFixed' || key == "fromUnit" || key == "toUnit" || key == "operatorId")) {

        formControls[key] = new FormControl(configFeeModel[key], [Validators.required]);
      } else if (this.selectedCategory == "Per Unit" && (key == "personalPanicTariffTypeId" || key == "personalPanicTariffCategoryId")) {

        formControls[key] = new FormControl(configFeeModel[key], [Validators.required]);
      } else if (this.selectedCategory == "Slab" && (key == "personalPanicTariffTypeId" || key == "personalPanicTariffCategoryId" || key == "fromUnit" || key == "toUnit" || key == "unitCost")) {

        formControls[key] = new FormControl(configFeeModel[key], [Validators.required]);
      } else if (this.selectedCategory == "Fixed Billing" && (key == "personalPanicTariffTypeId" || key == "personalPanicTariffCategoryId" || key == "fromUnit" || key == "toUnit" || key == "unitCost" || key == "fixedCost")) {
        formControls[key] = new FormControl(configFeeModel[key], [Validators.required]);
      }
      else {
        formControls[key] = new FormControl(configFeeModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  getTariffCodeDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.UX_TARIFF_DEFINITION).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.tariffDropdown = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }
  getSubscriptionGroupDrodown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.TARIFF_GROUP).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.groupList = response.resources
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }
  getTariffCategory() {
    this.crudService.get(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.TARIFF_DEFINITION_CATEGORY).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.tariffCategory = response.resources;
        if (this.tariffCategory.length != 0 && this.selectedCategory == "") {
          this.selectTariffCategory(this.tariffCategory[0])
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }
  getChargeTypeList() {
    this.crudService.get(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.TARIFF_DEFINITION_CHARGE_TYPES).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.chargeTypeList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }
  getOperatorDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_OPERATOR).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        // this.valueTypeList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }

  receiveEmitedData(data) {
    while (this.getTariffConfigFee.length !== 0) {
      this.getTariffConfigFee.removeAt(0)
    }
    this.tariffConfigFee = this.getTariffConfigFee
    for (const object of data) {
      object.fromUnit = +object.fromUnit
      object.toUnit = +object.toUnit
      object.utilizationCharges = +object.utilizationCharges
      object.utilizationPercentage = +object.utilizationPercentage
      object.personalPanicTariffCategoryId = this.selectedCategoryId
      this.tariffConfigFee.push(this.createTariffDefinitionConfigFeeFormArray(object));
    }
  }

  selectedCategory = ""
  selectedCategoryId = ""
  selectTariffCategory(category) {
    this.selectedCategory = category.personalPanicTariffCategoryName
    this.selectedCategoryId = category.personalPanicTariffCategoryId
  }
  onSubmit() {

    if (this.tariffDefinitionForm.invalid) return "";
    if (this.selectedCategory == "Utilization") {
      if (typeof this.tariffDefinitionForm.value?.isFixed == "string") {
        this.tariffDefinitionForm.value.isFixed == "false" ? false : true
      }
    }
    this.crudService.create(ModulesBasedApiSuffix.PANIC_APP, PanicAppApiSuffixModels.TARIFF_DEFINITION, this.tariffDefinitionForm.getRawValue()).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goBack()
      }
    })
  }
  goBack() {
    this.router.navigate(['/panic-app/tariff-definition']);
  }
}
