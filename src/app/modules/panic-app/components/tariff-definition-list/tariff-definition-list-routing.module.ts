import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TariffDefinitionListComponent, TariffDefinitionAddEditComponent } from '@modules/panic-app/components/tariff-definition-list';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: '', component: TariffDefinitionListComponent, canActivate: [AuthGuard], data: { title: 'Tariff Definition List' } },
  { path: 'add-edit', component: TariffDefinitionAddEditComponent, canActivate: [AuthGuard], data: { title: 'Add Tariff Definition' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class TariffDefinitionListRoutingModule { }
