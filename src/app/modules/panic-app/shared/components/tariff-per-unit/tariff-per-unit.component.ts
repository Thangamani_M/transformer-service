import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TariffDefinitionConfigFeeAddModel } from '@modules/panic-app';

@Component({
  selector: 'app-painc-app-shared-tariff-per-unit',
  templateUrl: './tariff-per-unit.component.html',
  styleUrls: ['./tariff-per-unit.component.scss']
})
export class TariffPerUnitComponent implements OnChanges {

  constructor(private formBuilder: FormBuilder) { }
  tariffDefinitionForm: FormGroup
  @Input() chargeTypeList = []
  @Input() tariffConfigFeeDetails :any = [];

  @Input() selectedCategoryId = ""
  @Output() emitData = new EventEmitter<any>();

  tariffConfigFee: FormArray

  ngOnInit(): void {
    this.createTariffAddForm()
    this.tariffDefinitionForm.get("tariffConfigFee").valueChanges.subscribe(item => {
      this.emitData.emit(this.tariffDefinitionForm.get("tariffConfigFee").value);
    })
  }
  ngOnChanges(changes: SimpleChanges) {
    if(changes['tariffConfigFeeDetails']) {
       this.createTariffAddForm()
      }
  }

  createTariffAddForm(): void {
    this.tariffDefinitionForm = this.formBuilder.group({});
    this.tariffDefinitionForm.addControl("tariffConfigFee", this.formBuilder.array([])
    )
    this.tariffConfigFee = this.getTariffConfigFee

    if (this.tariffConfigFeeDetails.length != 0) {
      for (const obj of this.tariffConfigFeeDetails) {
        this.tariffConfigFee.push(this.createTariffDefinitionConfigFeeFormArray(obj));
      }
    } else {
      this.tariffConfigFee.push(this.createTariffDefinitionConfigFeeFormArray());
      this.tariffConfigFee.push(this.createTariffDefinitionConfigFeeFormArray());
    }
  }
  get getTariffConfigFee(): FormArray {
    if (!this.tariffDefinitionForm) return;
    return this.tariffDefinitionForm.get("tariffConfigFee") as FormArray;
  }
  createTariffDefinitionConfigFeeFormArray(configFee?: TariffDefinitionConfigFeeAddModel): FormGroup {
    let configFeeModel = new TariffDefinitionConfigFeeAddModel(configFee);
    let formControls = {};
    Object.keys(configFeeModel).forEach((key) => {
      if (key == "personalPanicTariffTypeId" || key == "unitCost") {
        formControls[key] = new FormControl(configFeeModel[key], [Validators.required]);
      } else {
        formControls[key] = new FormControl(configFeeModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

}
