

import { Component, EventEmitter, Input, OnInit, Output,OnChanges,SimpleChanges } from '@angular/core';
import { FormArray, FormGroup, FormBuilder, FormControl, ValidatorFn, Validators } from '@angular/forms';
import { SnackbarService, ResponseMessageTypes } from '@app/shared';
import { TariffDefinitionConfigFeeAddModel } from '@modules/panic-app';

@Component({
  selector: 'app-painc-app-shared-tariff-fixed-billing',
  templateUrl: './tariff-fixed-billing.component.html',
  styleUrls: ['./tariff-fixed-billing.component.scss']
})
export class TariffFixedBillingComponent implements OnChanges {

  @Input() chargeTypeList = []
  @Input() tariffConfigFeeDetails :any = [];

  @Input() selectedCategoryId = ""
  @Output() emitData = new EventEmitter<any>();

  tariffConfigFee: FormArray
  tariffDefinitionForm: FormGroup

  constructor(private formBuilder: FormBuilder, private snackbarService: SnackbarService) { }

  ngOnInit(): void {
    this.createTariffAddForm()
    this.tariffDefinitionForm.get("tariffConfigFee").valueChanges.subscribe(item => {
      if (!this.tariffDefinitionForm.invalid) {
        this.emitData.emit(this.tariffDefinitionForm.get("tariffConfigFee").value);
      }
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes['tariffConfigFeeDetails']) {
       this.createTariffAddForm()
      }
  }
  createTariffAddForm(): void {
    this.tariffDefinitionForm = this.formBuilder.group({});
    this.tariffDefinitionForm.addControl("tariffConfigFee", this.formBuilder.array([])
    )
    this.tariffConfigFee = this.getTariffConfigFee
    // this.tariffConfigFee.push(this.createTariffDefinitionConfigFeeFormArray());
    if (this.tariffConfigFeeDetails.length != 0) {
      for (const obj of this.tariffConfigFeeDetails) {
        this.tariffConfigFee.push(this.createTariffDefinitionConfigFeeFormArray(obj));
      }
    } else {
      this.tariffConfigFee.push(this.createTariffDefinitionConfigFeeFormArray());
    }
    this.addValidators()
  }
  get getTariffConfigFee(): FormArray {
    if (!this.tariffDefinitionForm) return;
    return this.tariffDefinitionForm.get("tariffConfigFee") as FormArray;
  }

  addNewRow() {
    if (this.getTariffConfigFee.invalid) return ""

    this.tariffConfigFee.push(this.createTariffDefinitionConfigFeeFormArray());
  }

  createTariffDefinitionConfigFeeFormArray(configFee?: TariffDefinitionConfigFeeAddModel): FormGroup {
    let configFeeModel = new TariffDefinitionConfigFeeAddModel(configFee);
    let formControls = {};
    Object.keys(configFeeModel).forEach((key) => {
      if (key == "personalPanicTariffTypeId" || key == "fromUnit" || key == "toUnit" || key == "unitCost" || key == "fixedCost") {
        formControls[key] = new FormControl(configFeeModel[key], [Validators.required]);
      } else {
        formControls[key] = new FormControl(configFeeModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }




  onChangeToUnit(index) {
    let fromUnit = this.getTariffConfigFee.controls[index].get('fromUnit').value;
    let toUnit = this.getTariffConfigFee.controls[index].get('toUnit').value;

    if (fromUnit > toUnit) {
      this.getTariffConfigFee.controls[index].get('toUnit').setValue(null);
      return this.snackbarService.openSnackbar("TO Unit must be gratherthen from unit", ResponseMessageTypes.WARNING)
    }
  }
  addValidators() {
    // Get the form array and append a validator (again code split)
    (this.tariffDefinitionForm.get('tariffConfigFee') as FormArray).setValidators(this.formArrayValidator());
  }
  // Form validator
  errosObj = {}
  formArrayValidator(): ValidatorFn {
    // The validator is on the array, so the AbstractControl is of type FormArray
    return (group: FormArray) => {
      // Create an object of errors to return
      const errors = {};
      // Get the list of controls in the array (which are FormGroups)
      const controls = group.controls;
      // Iterate over them
      for (let i = 1; i < controls.length; i++) {
        // Get references to controls to compare them (split code again)

        let boolean = this.validateFromUnitAndToUnit(i);
        if (boolean) {
          errors[i + 'greaterThan' + (i - 1)] = true;
          this.errosObj[i] = "ERROR"
        } else {
          this.errosObj[i] = null
        }
      }

      return errors === {} ? null : errors;
    }
  }

  validateFromUnitAndToUnit = (index) => {
    const findRange = this.getTariffConfigFee.controls.find((item, i) => {
      if (i != index) {
        if (this.getTariffConfigFee.controls[index].get("personalPanicTariffTypeId").value == item.get("personalPanicTariffTypeId").value
        ) {
          return (item.get("fromUnit").value >= +this.getTariffConfigFee.controls[index].get('fromUnit').value &&
            item.get("toUnit").value <= +this.getTariffConfigFee.controls[index].get('toUnit').value) ||
            (item.get("fromUnit").value <= +this.getTariffConfigFee.controls[index].get('fromUnit').value &&
              item.get("toUnit").value >= +this.getTariffConfigFee.controls[index].get('toUnit').value) ||
            (item.get("fromUnit").value <= +this.getTariffConfigFee.controls[index].get('toUnit').value &&
              item.get("toUnit").value >= +this.getTariffConfigFee.controls[index].get('fromUnit').value) ||
            (item.get("fromUnit").value >= +this.getTariffConfigFee.controls[index].get('toUnit').value &&
              item.get("toUnit").value <= +this.getTariffConfigFee.controls[index].get('fromUnit').value)
        }
      }
    })

    if (findRange) {
      return true
    }

    return false
  }




}
