import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ResponseMessageTypes, SnackbarService } from '@app/shared';
import { TariffDefinitionConfigFeeAddModel } from '@modules/panic-app';

@Component({
  selector: 'app-painc-app-shared-tariff-utilization',
  templateUrl: './tariff-utilization.component.html',
  styleUrls: ['./tariff-utilization.component.scss']
})
export class TariffUtilizationComponent implements OnChanges {

  @Input() chargeTypeList = []
  @Input() selectedCategoryId = ""
  @Input() billMethodTypeList = []
  @Input() valueTypeList = []
  @Input() tariffConfigFeeDetails :any = [];
  @Output() emitData = new EventEmitter<any>();

  tariffConfigFee: FormArray
  isUtilizationPercentage: boolean = true
  utilizationPercentage: number
  utilizationCharges: number

  tariffDefinitionForm: FormGroup

  constructor(private formBuilder: FormBuilder, private snackbarService: SnackbarService) { }

  ngOnInit(): void {
    this.createTariffAddForm()
    this.tariffDefinitionForm.get("tariffConfigFee").valueChanges.subscribe(item => {
      if (!this.tariffDefinitionForm.invalid) {

        this.emitData.emit(this.tariffDefinitionForm.get("tariffConfigFee").value);
      }
    })
    if (this.tariffConfigFeeDetails.length != 0) {
      for (let index = 0; index < this.tariffConfigFeeDetails.length; index++) {
        const element = this.tariffConfigFeeDetails[index];
        this.tariffConfigFee.push(this.createTariffDefinitionConfigFeeFormArray(element));
        this.onChangeValueType(index, {target:{value:element.operatorId}})

      }
    }
  }
  ngOnChanges(changes: SimpleChanges) {
    if(changes['tariffConfigFeeDetails']) {
       this.createTariffAddForm()
      }
  }
  createTariffAddForm(): void {
    this.tariffDefinitionForm = this.formBuilder.group({});
    this.tariffDefinitionForm.addControl("tariffConfigFee", this.formBuilder.array([])
    )
    this.tariffConfigFee = this.getTariffConfigFee
    if (this.tariffConfigFeeDetails.length == 0) {
      this.tariffConfigFee.push(this.createTariffDefinitionConfigFeeFormArray({ personalPanicTariffCategoryId: this.selectedCategoryId }));
    }
    // this.addValidators()
  }
  get getTariffConfigFee(): FormArray {
    if (!this.tariffDefinitionForm) return;
    return this.tariffDefinitionForm.get("tariffConfigFee") as FormArray;
  }

  addNewRow() {
    if (this.getTariffConfigFee.invalid) return ""

    this.tariffConfigFee.push(this.createTariffDefinitionConfigFeeFormArray({ personalPanicTariffCategoryId: this.selectedCategoryId }));
  }

  createTariffDefinitionConfigFeeFormArray(configFee?: TariffDefinitionConfigFeeAddModel): FormGroup {
    let configFeeModel = new TariffDefinitionConfigFeeAddModel(configFee);
    let formControls = {};
    Object.keys(configFeeModel).forEach((key) => {
      if (key == "personalPanicTariffTypeId" || key == "personalPanicTariffCategoryId" || key == 'isFixed' || key == "fromUnit" || key == "toUnit" || key == "operatorId") {
        formControls[key] = new FormControl(configFeeModel[key], [Validators.required]);
      } else {
        formControls[key] = new FormControl(configFeeModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }


  onChangeValueType(index, event) {
    let valueType = this.valueTypeList.find(item => item.id == event.target.value)

    if (valueType.displayName.toLowerCase() == "less than") {
      this.getTariffConfigFee.controls[index].get('fromUnit').disable()
      this.getTariffConfigFee.controls[index].get('toUnit').enable()
      this.getTariffConfigFee.controls[index].get('fromUnit').setValue('0')
      this.getTariffConfigFee.controls[index].get('fromUnit').clearAsyncValidators()
      this.getTariffConfigFee.updateValueAndValidity()
      this.getTariffConfigFee.controls[index].get('isFixed').setValue(true)
    } else if (valueType.displayName == "greater than") {
      this.getTariffConfigFee.controls[index].get('fromUnit').enable()
      this.getTariffConfigFee.controls[index].get('toUnit').disable()
      this.getTariffConfigFee.controls[index].get('toUnit').setValue('0')
      this.getTariffConfigFee.controls[index].get('toUnit').clearAsyncValidators()
      this.getTariffConfigFee.updateValueAndValidity()
      this.getTariffConfigFee.controls[index].get('isFixed').setValue(true)
    } else {
      this.getTariffConfigFee.controls[index].get('isFixed').setValue(false)
      this.getTariffConfigFee.controls[index].get('toUnit').enable()
      this.getTariffConfigFee.controls[index].get('fromUnit').enable()
    }
  }

  onChangeToUnit(index) {

    let fromUnit = +this.getTariffConfigFee.controls[index].get('fromUnit').value;
    let toUnit = +this.getTariffConfigFee.controls[index].get('toUnit').value;
    // 10  > 100
    if (fromUnit > toUnit) {
      this.getTariffConfigFee.controls[index].get('toUnit').setValue(null);
      return this.snackbarService.openSnackbar("To Unit must be gratherthen from unit", ResponseMessageTypes.WARNING)
    }
    this.addValidators(index)
  }
  addValidators(index) {
    let valueType = this.valueTypeList.find(value => value.id == this.getTariffConfigFee.controls[index].get("operatorId").value)
    if (valueType.displayName.toLowerCase() == "less than" || valueType.displayName.toLowerCase() == "greater than") return ''

    this.errosObj = {}
    let boolean = this.validateFromUnitAndToUnit(index);
    if (boolean) {
      // errors[i + 'greaterThan' + (i - 1)] = true;
      this.errosObj[index] = "ERROR"
    } else {
      this.errosObj[index] = null
    }

    // Get the form array and append a validator (again code split)
    // (this.tariffDefinitionForm.get('tariffConfigFee') as FormArray).setValidators(this.formArrayValidator());
  }
  // Form validator
  errosObj = {}
  formArrayValidator(): ValidatorFn {
    // The validator is on the array, so the AbstractControl is of type FormArray
    return (group: FormArray) => {
      // Create an object of errors to return
      const errors = {};
      // Get the list of controls in the array (which are FormGroups)
      const controls = group.controls;
      // Iterate over them
      for (let i = 1; i < controls.length; i++) {
        // Get references to controls to compare them (split code again)



      }

      return errors === {} ? null : errors;
    }
  }

  validateFromUnitAndToUnit = (index) => {
    const findRange = this.getTariffConfigFee.controls.find((item, i) => {
      if (i != index) {
        let valueType = this.valueTypeList.find(value => value.id == item.get("operatorId").value)
        if (this.getTariffConfigFee.controls[index].get("personalPanicTariffTypeId").value == item.get("personalPanicTariffTypeId").value
          && (valueType.displayName.toLowerCase() == "between")) {
          return (item.get("fromUnit").value >= +this.getTariffConfigFee.controls[index].get('fromUnit').value &&
            item.get("toUnit").value <= +this.getTariffConfigFee.controls[index].get('toUnit').value) ||
            (item.get("fromUnit").value <= +this.getTariffConfigFee.controls[index].get('fromUnit').value &&
              item.get("toUnit").value >= +this.getTariffConfigFee.controls[index].get('toUnit').value) ||
            (item.get("fromUnit").value <= +this.getTariffConfigFee.controls[index].get('toUnit').value &&
              item.get("toUnit").value >= +this.getTariffConfigFee.controls[index].get('fromUnit').value) ||
            (item.get("fromUnit").value >= +this.getTariffConfigFee.controls[index].get('toUnit').value &&
              item.get("toUnit").value <= +this.getTariffConfigFee.controls[index].get('fromUnit').value)
        }
      }
    })

    if (findRange) {
      return true
    }

    return false
  }


  onChangeUtilization() {
    this.getTariffConfigFee.controls.find((item, i) => {
      item.get('utilizationCharges').setValue(!this.isUtilizationPercentage ? this.utilizationCharges : '0')
      item.get('isUtilizationPercentage').setValue(this.isUtilizationPercentage)
      item.get('utilizationPercentage').setValue(this.isUtilizationPercentage ? this.utilizationPercentage : '')

    })
  }


}
