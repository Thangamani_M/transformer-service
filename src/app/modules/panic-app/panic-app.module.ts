import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TariffFixedBillingComponent, TariffPerUnitComponent, TariffSlabComponent, TariffUtilizationComponent } from '@modules/panic-app/shared';
import { PanicAppRoutingModule } from './panic-app-routing.module';

@NgModule({
  declarations: [TariffUtilizationComponent, TariffFixedBillingComponent, TariffSlabComponent, TariffPerUnitComponent],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    MatDatepickerModule, LayoutModule, MaterialModule, SharedModule,
    PanicAppRoutingModule
  ],
  exports: [TariffUtilizationComponent, TariffFixedBillingComponent, TariffSlabComponent, TariffPerUnitComponent],
  entryComponents: []
})
export class PanicAppModule { }
