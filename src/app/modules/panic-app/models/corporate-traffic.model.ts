class CorporateTrafficAddModel {
  constructor(corporateTrafficModel?: CorporateTrafficAddModel) {
    this.personalPanicTariffId = corporateTrafficModel ? corporateTrafficModel.personalPanicTariffId == undefined ? '' : corporateTrafficModel.personalPanicTariffId : '';
    this.tariffCode = corporateTrafficModel == undefined ? null : corporateTrafficModel.tariffCode == undefined ? null : corporateTrafficModel.tariffCode;
    this.billingIntervalId = corporateTrafficModel ? corporateTrafficModel.billingIntervalId == undefined ? "" : corporateTrafficModel.billingIntervalId : "";
    this.isActive = corporateTrafficModel ? corporateTrafficModel.isActive == undefined ? true : corporateTrafficModel.isActive : true;
    this.tariffDescription = corporateTrafficModel ? corporateTrafficModel.tariffDescription == undefined ? "" : corporateTrafficModel.tariffDescription : "";
    this.personalPanicTariffId = corporateTrafficModel ? corporateTrafficModel.personalPanicTariffId == undefined ? "" : corporateTrafficModel.personalPanicTariffId : "";
    this.modifiedUserId = corporateTrafficModel ? corporateTrafficModel.modifiedUserId == undefined ? "" : corporateTrafficModel.modifiedUserId : "";
    this.validFrom = corporateTrafficModel ? corporateTrafficModel.validFrom == undefined ? "" : corporateTrafficModel.validFrom : "";
    this.validTill = corporateTrafficModel ? corporateTrafficModel.validTill == undefined ? "" : corporateTrafficModel.validTill : "";
    this.createdUserId = corporateTrafficModel ? corporateTrafficModel.createdUserId == undefined ? "" : corporateTrafficModel.createdUserId : "";
    this.tariffGroupId = corporateTrafficModel ? corporateTrafficModel.tariffGroupId == undefined ? "" : corporateTrafficModel.tariffGroupId : "";
  }

  personalPanicTariffId?: string;
  tariffCode?: string;
  tariffDescription?: string;
  billingIntervalId?: string;
  validFrom?: string;
  validTill?: string;
  isActive?: boolean;
  createdUserId?: string;
  modifiedUserId?: string;
  tariffGroupId?: string
}

class TariffDefinitionConfigAddModel {
  constructor(tariffDefinitionConfigAddModel?: TariffDefinitionConfigAddModel) {
    this.personalPanicTariffId = tariffDefinitionConfigAddModel ? tariffDefinitionConfigAddModel.personalPanicTariffId == undefined ? '' : tariffDefinitionConfigAddModel.personalPanicTariffId : '';
    this.personalPanicTariffConfigId = tariffDefinitionConfigAddModel == undefined ? null : tariffDefinitionConfigAddModel.personalPanicTariffConfigId == undefined ? null : tariffDefinitionConfigAddModel.personalPanicTariffConfigId;
    this.freeDays = tariffDefinitionConfigAddModel ? tariffDefinitionConfigAddModel.freeDays == undefined ? null : tariffDefinitionConfigAddModel.freeDays : null;
    this.isActive = tariffDefinitionConfigAddModel ? tariffDefinitionConfigAddModel.isActive == undefined ? true : tariffDefinitionConfigAddModel.isActive : true;
    this.freeResponse = tariffDefinitionConfigAddModel ? tariffDefinitionConfigAddModel.freeResponse == undefined ? null : tariffDefinitionConfigAddModel.freeResponse : null;
    this.createdUserId = tariffDefinitionConfigAddModel ? tariffDefinitionConfigAddModel.createdUserId == undefined ? "" : tariffDefinitionConfigAddModel.createdUserId : "";
    this.tariffGroupId = tariffDefinitionConfigAddModel ? tariffDefinitionConfigAddModel.tariffGroupId == undefined ? "" : tariffDefinitionConfigAddModel.tariffGroupId : "";
    this.personalPanicTariffGroupId = tariffDefinitionConfigAddModel ? tariffDefinitionConfigAddModel.personalPanicTariffGroupId == undefined ? "" : tariffDefinitionConfigAddModel.personalPanicTariffGroupId : "";
    this.createdDate = tariffDefinitionConfigAddModel ? tariffDefinitionConfigAddModel.createdDate == undefined ? "" : tariffDefinitionConfigAddModel.createdDate : "";
  }

  personalPanicTariffId?: string;
  personalPanicTariffConfigId?: string;
  freeResponse?: number;
  freeDays?: number;
  isActive?: boolean;
  createdUserId?: string;
  createdDate?: string;
  tariffGroupId?: string;
  personalPanicTariffGroupId?:string
}


class TariffDefinitionConfigFeeAddModel {
  constructor(tariffDefinitionConfigFeeAddModel?: TariffDefinitionConfigFeeAddModel) {
    this.personalPanicTariffConfigFeeId = tariffDefinitionConfigFeeAddModel ? tariffDefinitionConfigFeeAddModel.personalPanicTariffConfigFeeId == undefined ? '' : tariffDefinitionConfigFeeAddModel.personalPanicTariffConfigFeeId : '';
    this.personalPanicTariffConfigId = tariffDefinitionConfigFeeAddModel == undefined ? null : tariffDefinitionConfigFeeAddModel.personalPanicTariffConfigId == undefined ? null : tariffDefinitionConfigFeeAddModel.personalPanicTariffConfigId;
    this.unitCost = tariffDefinitionConfigFeeAddModel ? tariffDefinitionConfigFeeAddModel.unitCost == undefined ? null : tariffDefinitionConfigFeeAddModel.unitCost : null;
    this.isUtilizationPercentage = tariffDefinitionConfigFeeAddModel ? tariffDefinitionConfigFeeAddModel.isUtilizationPercentage == undefined ? true : tariffDefinitionConfigFeeAddModel.isUtilizationPercentage : true;
    this.isFixed = tariffDefinitionConfigFeeAddModel ? tariffDefinitionConfigFeeAddModel.isFixed == undefined ? false : tariffDefinitionConfigFeeAddModel.isFixed : false;
    this.fromUnit = tariffDefinitionConfigFeeAddModel ? tariffDefinitionConfigFeeAddModel.fromUnit == undefined ? null : tariffDefinitionConfigFeeAddModel.fromUnit : null;
    this.personalPanicTariffCategoryId = tariffDefinitionConfigFeeAddModel ? tariffDefinitionConfigFeeAddModel.personalPanicTariffCategoryId == undefined ? "" : tariffDefinitionConfigFeeAddModel.personalPanicTariffCategoryId : "";
    this.personalPanicTariffTypeId = tariffDefinitionConfigFeeAddModel ? tariffDefinitionConfigFeeAddModel.personalPanicTariffTypeId == undefined ? "" : tariffDefinitionConfigFeeAddModel.personalPanicTariffTypeId : "";
    this.toUnit = tariffDefinitionConfigFeeAddModel ? tariffDefinitionConfigFeeAddModel.toUnit == undefined ? null : tariffDefinitionConfigFeeAddModel.toUnit : null;
    this.maximum = tariffDefinitionConfigFeeAddModel ? tariffDefinitionConfigFeeAddModel.maximum == undefined ? 0 : tariffDefinitionConfigFeeAddModel.maximum : 0;
    this.minimum = tariffDefinitionConfigFeeAddModel ? tariffDefinitionConfigFeeAddModel.minimum == undefined ? 0 : tariffDefinitionConfigFeeAddModel.minimum : 0;
    this.fixedCost = tariffDefinitionConfigFeeAddModel ? tariffDefinitionConfigFeeAddModel.fixedCost == undefined ? null : tariffDefinitionConfigFeeAddModel.fixedCost : null;
    this.utilizationCharges = tariffDefinitionConfigFeeAddModel ? tariffDefinitionConfigFeeAddModel.utilizationCharges == undefined ? null : tariffDefinitionConfigFeeAddModel.utilizationCharges : null;
    this.utilizationPercentage = tariffDefinitionConfigFeeAddModel ? tariffDefinitionConfigFeeAddModel.utilizationPercentage == undefined ? null : tariffDefinitionConfigFeeAddModel.utilizationPercentage : null;
    this.isActive = tariffDefinitionConfigFeeAddModel ? tariffDefinitionConfigFeeAddModel.isActive == undefined ? true : tariffDefinitionConfigFeeAddModel.isActive : true;
    this.operatorId = tariffDefinitionConfigFeeAddModel == undefined ? null : tariffDefinitionConfigFeeAddModel.operatorId == undefined ? null : tariffDefinitionConfigFeeAddModel.operatorId;
    this.createdUserId = tariffDefinitionConfigFeeAddModel == undefined ? null : tariffDefinitionConfigFeeAddModel.createdUserId == undefined ? null : tariffDefinitionConfigFeeAddModel.createdUserId;
    this.createdDate = tariffDefinitionConfigFeeAddModel == undefined ? null : tariffDefinitionConfigFeeAddModel.createdDate == undefined ? null : tariffDefinitionConfigFeeAddModel.createdDate;
    this.billMethod = tariffDefinitionConfigFeeAddModel == undefined ? "" : tariffDefinitionConfigFeeAddModel.billMethod == undefined ? "" : tariffDefinitionConfigFeeAddModel.billMethod;

  }

  personalPanicTariffConfigFeeId?: string;
  personalPanicTariffConfigId?: string;
  fromUnit?: number;
  toUnit?: number;
  maximum?: number;
  minimum?: number;
  fixedCost?: number;
  unitCost?: number;
  utilizationCharges?: number;
  utilizationPercentage?: number;
  isUtilizationPercentage?: boolean;
  isFixed?: boolean;
  isActive?: boolean;
  personalPanicTariffCategoryId?: string;
  personalPanicTariffTypeId?: string;
  createdUserId?: string;
  createdDate?: string;
  billMethod?: string;
  operatorId?: string;
}

class TariffDefinitionAddEditModel {
  constructor(tariffDefinitionAddEditModel?: TariffDefinitionAddEditModel) {
    this.tariffConfig = tariffDefinitionAddEditModel ? tariffDefinitionAddEditModel.tariffConfig == undefined ? {} : tariffDefinitionAddEditModel.tariffConfig : {};
    this.tariffConfigFee = tariffDefinitionAddEditModel ? tariffDefinitionAddEditModel.tariffConfigFee == undefined ? [] : tariffDefinitionAddEditModel.tariffConfigFee : [];
  }
  tariffConfig?: TariffDefinitionConfigAddModel;
  tariffConfigFee?: TariffDefinitionConfigFeeAddModel[];
}





export { CorporateTrafficAddModel, TariffDefinitionAddEditModel, TariffDefinitionConfigAddModel, TariffDefinitionConfigFeeAddModel };
