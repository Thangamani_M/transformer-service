


class CorporateConractAddressModel {
  constructor(corporateConractAddressModel?: CorporateConractAddressModel) {
    this.addressId = corporateConractAddressModel ? corporateConractAddressModel.addressId == undefined ? null : corporateConractAddressModel.addressId : null;
    this.buildingName = corporateConractAddressModel == undefined ? null : corporateConractAddressModel.buildingName == undefined ? null : corporateConractAddressModel.buildingName;
    this.buildingNo = corporateConractAddressModel ? corporateConractAddressModel.buildingNo == undefined ? "" : corporateConractAddressModel.buildingNo : "";
    this.streetNo = corporateConractAddressModel ? corporateConractAddressModel.streetNo == undefined ? '' : corporateConractAddressModel.streetNo : '';
    this.streetName = corporateConractAddressModel ? corporateConractAddressModel.streetName == undefined ? "" : corporateConractAddressModel.streetName : "";
    this.estateName = corporateConractAddressModel ? corporateConractAddressModel.estateName == undefined ? "" : corporateConractAddressModel.estateName : "";
    this.estateStreetNo = corporateConractAddressModel ? corporateConractAddressModel.estateStreetNo == undefined ? "" : corporateConractAddressModel.estateStreetNo : "";
    this.estateStreetName = corporateConractAddressModel ? corporateConractAddressModel.estateStreetName == undefined ? "" : corporateConractAddressModel.estateStreetName : "";
    this.formatedAddress = corporateConractAddressModel ? corporateConractAddressModel.formatedAddress == undefined ? "" : corporateConractAddressModel.formatedAddress : "";
    this.createdUserId = corporateConractAddressModel ? corporateConractAddressModel.createdUserId == undefined ? "" : corporateConractAddressModel.createdUserId : "";
    this.jsonObject = corporateConractAddressModel ? corporateConractAddressModel.jsonObject == undefined ? "" : corporateConractAddressModel.jsonObject : "";
    this.seoId = corporateConractAddressModel ? corporateConractAddressModel.seoId == undefined ? "" : corporateConractAddressModel.seoId : "";
    this.provinceId = corporateConractAddressModel ? corporateConractAddressModel.provinceId == undefined ? "" : corporateConractAddressModel.provinceId : "";
    this.cityId = corporateConractAddressModel ? corporateConractAddressModel.cityId == undefined ? "" : corporateConractAddressModel.cityId : "";
    this.suburbId = corporateConractAddressModel ? corporateConractAddressModel.suburbId == undefined ? "" : corporateConractAddressModel.suburbId : "";
    this.postalCode = corporateConractAddressModel ? corporateConractAddressModel.postalCode == undefined ? "" : corporateConractAddressModel.postalCode : "";
    this.latitude = corporateConractAddressModel ? corporateConractAddressModel.latitude == undefined ? "" : corporateConractAddressModel.latitude : "";
    this.longitude = corporateConractAddressModel ? corporateConractAddressModel.longitude == undefined ? "" : corporateConractAddressModel.longitude : "";
    this.addressConfidentLevelId = corporateConractAddressModel ? corporateConractAddressModel.addressConfidentLevelId == undefined ? "" : corporateConractAddressModel.addressConfidentLevelId : "";
    this.isAddressComplex = corporateConractAddressModel ? corporateConractAddressModel.isAddressComplex == undefined ? false : corporateConractAddressModel.isAddressComplex : false;
    this.isAddressExtra = corporateConractAddressModel ? corporateConractAddressModel.isAddressExtra == undefined ? false : corporateConractAddressModel.isAddressExtra : false;
    this.afriGISAddressId = corporateConractAddressModel ? corporateConractAddressModel.afriGISAddressId == undefined ? "" : corporateConractAddressModel.afriGISAddressId : "";
    this.suburbName = corporateConractAddressModel ? corporateConractAddressModel.suburbName == undefined ? "" : corporateConractAddressModel.suburbName : "";
    this.cityName = corporateConractAddressModel ? corporateConractAddressModel.cityName == undefined ? "" : corporateConractAddressModel.cityName : "";
    this.provinceName = corporateConractAddressModel ? corporateConractAddressModel.provinceName == undefined ? "" : corporateConractAddressModel.provinceName : "";
    this.createdUserId = corporateConractAddressModel ? corporateConractAddressModel.createdUserId == undefined ? "" : corporateConractAddressModel.createdUserId : "";
    this.createdDate = corporateConractAddressModel ? corporateConractAddressModel.createdDate == undefined ? "" : corporateConractAddressModel.createdDate : "";
    this.addressData = corporateConractAddressModel ? corporateConractAddressModel.addressData == undefined ? "" : corporateConractAddressModel.addressData : "";
  }

  addressId: string
  buildingName: string;
  buildingNo: string;
  streetNo: string;
  streetName: string;
  estateName: string;
  estateStreetNo: string;
  estateStreetName: string;
  formatedAddress: string;
  jsonObject: string;
  seoId: string;
  provinceId: string;
  cityId: string;
  suburbId: string;
  postalCode: string;
  latitude: string;
  longitude: string;
  addressConfidentLevelId: string;
  isAddressComplex: boolean;
  isAddressExtra: boolean;
  afriGISAddressId: string;
  suburbName: string;
  cityName: string;
  provinceName: string;
  createdUserId: string;
  createdDate: string;
  addressData: string;
}
class CorporateConractAddEditModel {
  constructor(corporateConractAddEditModel?: CorporateConractAddEditModel) {
    this.corporateUserId = corporateConractAddEditModel ? corporateConractAddEditModel.corporateUserId == undefined ? '' : corporateConractAddEditModel.corporateUserId : '';
    this.companyName = corporateConractAddEditModel == undefined ? null : corporateConractAddEditModel.companyName == undefined ? null : corporateConractAddEditModel.companyName;
    this.registrationNumber = corporateConractAddEditModel ? corporateConractAddEditModel.registrationNumber == undefined ? "" : corporateConractAddEditModel.registrationNumber : "";
    this.vatNumber = corporateConractAddEditModel ? corporateConractAddEditModel.vatNumber == undefined ? '' : corporateConractAddEditModel.vatNumber : '';
    this.psiraRegistrationNumber = corporateConractAddEditModel ? corporateConractAddEditModel.psiraRegistrationNumber == undefined ? "" : corporateConractAddEditModel.psiraRegistrationNumber : "";
    this.psiraExpiryDate = corporateConractAddEditModel ? corporateConractAddEditModel.psiraExpiryDate == undefined ? "" : corporateConractAddEditModel.psiraExpiryDate : "";
    this.emailId = corporateConractAddEditModel ? corporateConractAddEditModel.emailId == undefined ? "" : corporateConractAddEditModel.emailId : "";
    this.mobileNoCountryCode = corporateConractAddEditModel ? corporateConractAddEditModel.mobileNoCountryCode == undefined ? "+27" : corporateConractAddEditModel.mobileNoCountryCode : "+27";
    this.mobileNo = corporateConractAddEditModel ? corporateConractAddEditModel.mobileNo == undefined ? "" : corporateConractAddEditModel.mobileNo : "";
    this.spocName = corporateConractAddEditModel ? corporateConractAddEditModel.spocName == undefined ? "" : corporateConractAddEditModel.spocName : "";
    this.accountsEmailId = corporateConractAddEditModel ? corporateConractAddEditModel.accountsEmailId == undefined ? "" : corporateConractAddEditModel.accountsEmailId : "";
    this.physicalAddressId = corporateConractAddEditModel ? corporateConractAddEditModel.physicalAddressId == undefined ? "" : corporateConractAddEditModel.physicalAddressId : "";
    this.postalAddressId = corporateConractAddEditModel ? corporateConractAddEditModel.postalAddressId == undefined ? "" : corporateConractAddEditModel.postalAddressId : "";
    this.isSameAsPhysicalAddress = corporateConractAddEditModel ? corporateConractAddEditModel.isSameAsPhysicalAddress == undefined ? false : corporateConractAddEditModel.isSameAsPhysicalAddress : false;
    this.isActive = corporateConractAddEditModel ? corporateConractAddEditModel.isActive == undefined ? false : corporateConractAddEditModel.isActive : false;
    this.createdDate = corporateConractAddEditModel ? corporateConractAddEditModel.createdDate == undefined ? "" : corporateConractAddEditModel.createdDate : "";
    this.createdUserId = corporateConractAddEditModel ? corporateConractAddEditModel.createdUserId == undefined ? "" : corporateConractAddEditModel.createdUserId : "";
    this.modifiedUserId = corporateConractAddEditModel ? corporateConractAddEditModel.modifiedUserId == undefined ? "" : corporateConractAddEditModel.modifiedUserId : "";
    this.corporateContractPostalAddress = corporateConractAddEditModel ? corporateConractAddEditModel.corporateContractPostalAddress == undefined ? {} : corporateConractAddEditModel.corporateContractPostalAddress : {};
    this.corporateContractPhysicalAddress = corporateConractAddEditModel ? corporateConractAddEditModel.corporateContractPhysicalAddress == undefined ? {} : corporateConractAddEditModel.corporateContractPhysicalAddress : {};
  }

  corporateUserId: string;
  companyName: string;
  registrationNumber: string;
  vatNumber: string;
  psiraRegistrationNumber: string;
  psiraExpiryDate: string;
  emailId: string;
  mobileNoCountryCode: string;
  mobileNo: string;
  spocName: string;
  accountsEmailId: string;
  physicalAddressId: string;
  postalAddressId: string;
  isSameAsPhysicalAddress: boolean;
  isActive: boolean;
  createdDate: string;
  createdUserId: string;
  modifiedUserId?: string;

  corporateContractPostalAddress: CorporateConractAddressModel | any;
  corporateContractPhysicalAddress: CorporateConractAddressModel | any;
}

class CorporateConractPlanAddEditModel {
  constructor(corporateConractPlanAddEditModel?: CorporateConractPlanAddEditModel) {
    this.corporateUserId = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.corporateUserId == undefined ? '' : corporateConractPlanAddEditModel.corporateUserId : '';
    this.corporateUserTariffId = corporateConractPlanAddEditModel == undefined ? null : corporateConractPlanAddEditModel.corporateUserTariffId == undefined ? null : corporateConractPlanAddEditModel.corporateUserTariffId;
    this.personalPanicTariffId = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.personalPanicTariffId == undefined ? "" : corporateConractPlanAddEditModel.personalPanicTariffId : "";
    this.numberOfLicence = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.numberOfLicence == undefined ? 0 : corporateConractPlanAddEditModel.numberOfLicence : 0;
    this.emailId = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.emailId == undefined ? "" : corporateConractPlanAddEditModel.emailId : "";
    this.isGenerateLicenceIdAndSendEmail = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.isGenerateLicenceIdAndSendEmail == undefined ? false : corporateConractPlanAddEditModel.isGenerateLicenceIdAndSendEmail : false;
    this.emailId = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.emailId == undefined ? "" : corporateConractPlanAddEditModel.emailId : "";
    this.uploadUserFileName = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.uploadUserFileName == undefined ? "" : corporateConractPlanAddEditModel.uploadUserFileName : "";
    this.uploadUserFilePath = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.uploadUserFilePath == undefined ? "" : corporateConractPlanAddEditModel.uploadUserFilePath : "";
    this.isEmailSent = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.isEmailSent == undefined ? false : corporateConractPlanAddEditModel.isEmailSent : false;
    this.isUploadUserInfo = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.isUploadUserInfo == undefined ? false : corporateConractPlanAddEditModel.isUploadUserInfo : false;
    this.isActive = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.isActive == undefined ? false : corporateConractPlanAddEditModel.isActive : false;
    this.createdDate = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.createdDate == undefined ? "" : corporateConractPlanAddEditModel.createdDate : "";
    this.createdUserId = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.createdUserId == undefined ? "" : corporateConractPlanAddEditModel.createdUserId : "";
    this.modifiedUserId = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.modifiedUserId == undefined ? "" : corporateConractPlanAddEditModel.modifiedUserId : "";
    this.personalPanicTariffName = corporateConractPlanAddEditModel ? corporateConractPlanAddEditModel.personalPanicTariffName == undefined ? "" : corporateConractPlanAddEditModel.personalPanicTariffName : "";
   }

  corporateUserTariffId: string;
  corporateUserId: string;
  personalPanicTariffId: string;
  numberOfLicence: number;
  emailId: string;
  isGenerateLicenceIdAndSendEmail: boolean;
  uploadUserFileName:string;
  uploadUserFilePath: string;
  isEmailSent: boolean;
  isUploadUserInfo: boolean;
  isActive:boolean;
  createdUserId: string;
  modifiedUserId: string;

  createdDate: string;
  personalPanicTariffName: string;
}



export { CorporateConractAddressModel, CorporateConractAddEditModel,CorporateConractPlanAddEditModel }
