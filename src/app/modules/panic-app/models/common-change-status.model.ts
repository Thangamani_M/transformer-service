class commonchangestatusListModel {
    invoiceId?: string;
    modifiedUserId?: string;
    paymentStatusId?: string

    constructor(techAreaManagerWorkListModel?: commonchangestatusListModel) {
      this.modifiedUserId = techAreaManagerWorkListModel == undefined ? null : techAreaManagerWorkListModel.modifiedUserId == undefined ? null : techAreaManagerWorkListModel.modifiedUserId;
      this.paymentStatusId = techAreaManagerWorkListModel == undefined ? null : techAreaManagerWorkListModel.paymentStatusId == undefined ? null : techAreaManagerWorkListModel.paymentStatusId;
      this.invoiceId = techAreaManagerWorkListModel == undefined ? null : techAreaManagerWorkListModel.invoiceId == undefined ? null : techAreaManagerWorkListModel.invoiceId;

    }
  }
  export { commonchangestatusListModel };