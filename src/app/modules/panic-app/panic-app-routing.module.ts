import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  { path: 'corporate-tariff', loadChildren: () => import('./components/corporate-tariff/corporate-tariff.module').then(m => m.CorporateTariffModule) },
  { path: 'tariff-definition', loadChildren: () => import('./components/tariff-definition-list/tariff-definition-list.module').then(m => m.TariffDefinitionListModule) },
  { path: 'corporate-contract', loadChildren: () => import('./components/corporate-contract/corporate-contract.module').then(m => m.CorporateContractModule) },
  { path: 'corporate-Invoice', loadChildren: () => import('./components/corporate-invoice/corporate-invoice.module').then(m => m.CorporateInvoiceModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class PanicAppRoutingModule { }
