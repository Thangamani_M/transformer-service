import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { SalesDashboardRoutingModule } from '@sales/components/dashboard';
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SalesDashboardRoutingModule,
    MaterialModule,
    SharedModule,
  ],
  entryComponents: [],
})
export class SalesDashboardModule { }
