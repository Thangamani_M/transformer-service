import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalesDashboardComponent } from '@sales/components';

const salesDashboardModuleRoutes: Routes = [
    { path: '', component: SalesDashboardComponent, data: { title: 'Sales Dashboard' } }
];
@NgModule({
    imports: [RouterModule.forChild(salesDashboardModuleRoutes)]
})

export class SalesDashboardRoutingModule { }
