import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { LeadLifecycleBreadCrumbIconActionsHeaderComponent } from "./lead-lifecycle-breadcrumb-icon-actions-header";
import { LeadLifecycleHeaderContainerComponent } from "./lead-lifecycle-header-container";

@NgModule({
    declarations: [LeadLifecycleHeaderContainerComponent,LeadLifecycleBreadCrumbIconActionsHeaderComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
        SharedModule,
        LayoutModule
    ],
    exports: [LeadLifecycleHeaderContainerComponent,LeadLifecycleBreadCrumbIconActionsHeaderComponent],
    entryComponents: [],
})
export class LeadLifecycleReusableComponentsModule { }