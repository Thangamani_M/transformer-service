import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { BreadCrumbModel, CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from "@app/shared";
import { RxjsService } from "@app/shared/services/rxjs.services";
import { CUSTOMER_COMPONENT } from "@modules/customer/shared/utils/customer-module.enums";
import { MY_TASK_COMPONENT } from "@modules/my-tasks/shared";
import { AssignedToModalComponent } from "@modules/sales/components/lead-notes/assigned-to-modal.component";
import { CustomerCallModalComponent } from "@modules/sales/components/lead-notes/customer-call.component";
import { EmailModalComponent } from "@modules/sales/components/lead-notes/email-modal.component";
import { LeadNoteModalComponent } from "@modules/sales/components/lead-notes/lead-notes-modal.component";
import { ScheduleCallbackAddEditModalComponent } from "@modules/sales/components/lead-notes/schedule-callback-add-edit-modal.component";
import { SMSModalComponent } from "@modules/sales/components/lead-notes/sms-modal.component";
import { selectLeadHeaderDataState$ } from "@modules/sales/components/task-management/lead-creation-ngrx-files/lead-creation.selectors";
import { LeadHeaderData } from "@modules/sales/models/lead-creation-stepper.model";
import { ScheduleCallBackAddEditModel } from "@modules/sales/models/lead-sms-notification.model";
import { LeadOutcomeStatusNames, SalesModuleApiSuffixModels, Sales_Leads_Components } from "@modules/sales/shared/utils/sales-module.enums";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs/internal/observable/combineLatest";
@Component({
  selector: "app-lead-lifecycle-breadcrumb-icon-actions-header",
  templateUrl: "./lead-lifecycle-breadcrumb-icon-actions-header.component.html",
  host: {
    "(window:click)": "QuickMenuOnClick()"
  }
})

export class LeadLifecycleBreadCrumbIconActionsHeaderComponent implements OnInit {
  @Input() shouldShowHeaderContainer = true;
  @Input() shouldShowHeaderContainerOnly = false;
  @Input() breadCrumb: BreadCrumbModel;
  @Output() onEmittedEvent = new EventEmitter();
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  leadHeaderData: LeadHeaderData;
  isMenuOpen = false;
  requiredLeadProps;
  filteredSubComponentsPermissions = [];

  constructor(private router: Router, private dialog: MatDialog, private store: Store<AppState>,
    private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService) {
  }

  toggleMenu($event) {
    $event.stopPropagation();
    this.isMenuOpen = !this.isMenuOpen;
  }

  QuickMenuOnClick() {
    this.isMenuOpen = false;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getLeadDetailsById();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadHeaderDataState$), this.rxjsService.getFromUrl()]
    ).subscribe((response) => {
      this.leadHeaderData = new LeadHeaderData(response[0]);
      this.requiredLeadProps = this.leadHeaderData;
      this.combineLatestNgrxStoreDataForPermissions();
    });
  }

  combineLatestNgrxStoreDataForPermissions() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let componentPermissions = [];
      if (this.leadHeaderData.fromUrl == "Leads List") {
        componentPermissions = response[0][Sales_Leads_Components.LEADS];
      }
      else if (this.leadHeaderData.fromUrl == "My Leads List") {
        componentPermissions = response[0][MY_TASK_COMPONENT.MY_SALES];
        componentPermissions = componentPermissions?.filter(cP => cP.menuName === 'My Leads').map((fS => fS['subMenu']))[0];
      }
      else if(this.leadHeaderData.fromUrl == "Customer Management"){
        let customer = response[0][CUSTOMER_COMPONENT.CUSTOMER]?.find(el => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD);
          let sales = customer?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.SALES);
          componentPermissions  = sales?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.LEADS)['subMenu']
      }
      if (componentPermissions) {
        this.filteredSubComponentsPermissions = componentPermissions?.filter(cP => cP.menuName === Sales_Leads_Components.LEAD_LANDING_PAGE).map((fS => fS['subMenu']))[0];
      }
    });
  }

  getLeadDetailsById(): void {
    if (!this.leadHeaderData.leadId) return;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEADS_LANDING, this.leadHeaderData.leadId)
      .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          this.requiredLeadProps = { ...this.leadHeaderData, ...resp.resources };
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onActionIconSelected(actionType: string) {
    let modalComponent;
    let data: ScheduleCallBackAddEditModel = this.leadHeaderData;
    switch (actionType) {
      case "Email":
        modalComponent = EmailModalComponent;
        break;
      case "SMS":
        modalComponent = SMSModalComponent;
        break;
      case "AssignReassign":
        modalComponent = AssignedToModalComponent;
        break;
      case "Schedule Call Back":
        modalComponent = ScheduleCallbackAddEditModalComponent;
        data = new ScheduleCallBackAddEditModel(this.requiredLeadProps);
        data.isCustomer = false;
        data.contactPerson = this.requiredLeadProps.customerName;
        if (this.requiredLeadProps.mobile1) {
          data.contactNumberCountryCode = this.requiredLeadProps.mobile1.slice(0, 3);
          data.contactNumber = this.requiredLeadProps.mobile1.slice(4);
        }
        break;
      case "Dial":
        modalComponent = CustomerCallModalComponent;
        break;
      case "Notes":
        modalComponent = LeadNoteModalComponent;
        break;
      default:
        let urlPrefix = this.leadHeaderData.fromUrl == 'Leads List' ? '/sales/task-management/leads/' :
          '/my-tasks/my-sales/my-leads/';
        let urlSuffix = actionType == "Sales Documents" ? 'sales-support-documents' :
          'other-documents';
        let subMenus = this.filteredSubComponentsPermissions?.find(fSC => fSC.menuName === actionType)?.subMenu;
        let isAccess = subMenus?.find(sM => sM['menuName'] === PermissionTypes.LIST);
        if (isAccess) {
          let queryParams = { queryParams: { leadId: this.leadHeaderData.leadId } };
          this.router.navigate([`${urlPrefix}${urlSuffix}`], queryParams);
        }
        else {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        break;
    }
    if (actionType !== "Sales Documents" && actionType !== "Other Documents") {
      let isAccessAvailable = this.getCreatePermissionByActionIconType(actionType);
      if (isAccessAvailable) {
        const dialogReff = this.dialog.open(modalComponent, {
          data,
          width: "700px",
          disableClose: true,
        });
        dialogReff.afterClosed().subscribe((result) => { });
        dialogReff.componentInstance?.['outputData']?.subscribe((ele) => {
          if (ele) {
            this.onEmittedEvent.emit();
          }
        });
      }
      else {
        this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
    }
  }

  getCreatePermissionByActionIconType(actionTypeMenuName): boolean {
    let foundObj = this.filteredSubComponentsPermissions?.find(fSC => fSC.menuName === actionTypeMenuName);
    if (foundObj) {
      return foundObj.subMenu.length == 0 ? false : true;
    }
    else {
      return false;
    }
  }

  onAnchorClicked(urlSegment: string) {
    if (urlSegment == 'landingPage') {
      if (this.leadHeaderData.fromUrl == 'Leads List') {
        this.router.navigate(['/sales/task-management/leads/lead-info/view'], { queryParams: { leadId: this.leadHeaderData.leadId } });
      }
      else{
        this.router.navigate(['/my-tasks/my-sales/my-leads/lead-info/view'], { queryParams: { leadId: this.leadHeaderData.leadId } });
      }
    }
    else {
      this.router.navigateByUrl(urlSegment);
    }
  }
}
