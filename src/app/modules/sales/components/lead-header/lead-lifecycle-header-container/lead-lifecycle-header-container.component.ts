import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { selectLeadCreationStepperParamsState$, selectLeadHeaderDataState$ } from "@modules/sales/components/task-management/lead-creation-ngrx-files/lead-creation.selectors";
import { LeadCreationUserDataModel } from '@modules/sales/models/lead';
import { LeadHeaderData } from "@modules/sales/models/lead-creation-stepper.model";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs/internal/observable/combineLatest";
@Component({
  selector: "app-lead-lifecycle-header-container",
  templateUrl: "./lead-lifecycle-header-container.component.html"
})

export class LeadLifecycleHeaderContainerComponent {
  leadHeaderData: LeadHeaderData;
  @Input() shouldShowLeadRefNoLink = true;
  leadCreationUserDataModel: LeadCreationUserDataModel;

  constructor(private router: Router, private store: Store<AppState>) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadHeaderDataState$),
      this.store.select(selectLeadCreationStepperParamsState$)]
    ).subscribe((response) => {
      this.leadHeaderData = new LeadHeaderData(response[0]);
      this.leadCreationUserDataModel = new LeadCreationUserDataModel(
        response[1]
      );
    });
  }

  redirectToLeadLandingPage() {
    if (this.leadHeaderData.fromUrl == 'Leads List') {
      this.router.navigate(['/sales/task-management/leads/lead-info/view'], { queryParams: { leadId: this.leadHeaderData.leadId } });
    }
    else {
      this.router.navigate(['/my-tasks/my-sales/my-leads/lead-info/view'], { queryParams: { leadId: this.leadHeaderData.leadId } });
    }
  }
}
