import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { AddNotesModel, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from "@app/reducers";
import {
  formConfigs, setRequiredValidator
} from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { LoggedInUserModel, ModulesBasedApiSuffix } from '@app/shared/utils';
import { loggedInUserData } from "@modules/others/auth.selectors";
import { Store } from "@ngrx/store";
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-add-notes-modal',
  templateUrl: './add-notes-modal.component.html',
  styleUrls: ['./raw-lead.component.scss']
})

export class AddRawLeadsNotesModalComponent implements OnInit {
  formConfigs = formConfigs;
  leadNotes;
  loggedInUserData: LoggedInUserModel;
  addNotesForm: FormGroup;

  constructor(private crudService: CrudService, private rxjsService: RxjsService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data) {
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.combineLatestNgrxStoreData();
    this.createAddNotesForm();
    this.getNotes();
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([this.store.select(loggedInUserData)]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  createAddNotesForm(): void {
    let addNotesModel = new AddNotesModel();
    this.addNotesForm = this.formBuilder.group({});
    Object.keys(addNotesModel).forEach((key) => {
      this.addNotesForm.addControl(key, new FormControl(addNotesModel[key]));
    });
    this.addNotesForm.controls['modifiedUserId'].setValue(this.loggedInUserData.userId);
    this.addNotesForm.controls['rawLeadId'].setValue(this.data.rawLeadId);
    this.addNotesForm.controls['createdUserId'].setValue(this.loggedInUserData.userId);
    this.addNotesForm = setRequiredValidator(this.addNotesForm, ['comments']);
  }

  getNotes() {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_CREATE_RAW_LEAD_NOTES, this.data.rawLeadId, false, null).subscribe((response) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.leadNotes = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  createNotes() {
    if (this.addNotesForm.invalid) {
      return;
    }
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_CREATE_RAW_LEAD_NOTES, this.addNotesForm.value, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getNotes();
        this.createAddNotesForm();
      }
    });
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}