import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ChangeStatusModel, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from "@app/reducers";
import { addFormControls, prepareGetRequestHttpParams, removeFormControls, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { LoggedInUserModel, ModulesBasedApiSuffix } from '@app/shared/utils';
import { loggedInUserData } from "@modules/others/auth.selectors";
import { Store } from "@ngrx/store";
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-change-status-modal',
  templateUrl: './change-status-modal.component.html',
  styleUrls: ['./raw-lead.component.scss']
})

export class ChangeStatusModalComponent implements OnInit {
  changeStatusForm: FormGroup;
  rawLeadStatuses = [];
  assignToUsers = [];
  userGroups = [];
  checkedInArray = {};
  loggedInUserData: LoggedInUserModel;
  show: boolean = false;

  constructor(private httpService: CrudService, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private store: Store<AppState>,
    private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public statusObj: any) {
    this.changeStatusForm = this.formBuilder.group({});
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.combineLatestNgrxStoreData();
    this.createChangeStatusForm();
    this.getStatuses();
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([this.store.select(loggedInUserData)]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  createChangeStatusForm(): void {
    let changeStatusModel = new ChangeStatusModel();
    this.changeStatusForm = this.formBuilder.group({});
    Object.keys(changeStatusModel).forEach((key) => {
      this.changeStatusForm.addControl(key, new FormControl(changeStatusModel[key]));
    });
    this.changeStatusForm.controls['modifiedUserId'].setValue(this.loggedInUserData.userId);
    this.changeStatusForm = setRequiredValidator(this.changeStatusForm, ['rawLeadStatusId', 'assignedTo', 'userGroupId']);

    this.changeStatusForm.patchValue(this.statusObj);
    if (this.statusObj['userGroupId']) {
      this.changeStatusForm.controls['assignedTo'].setValue(this.statusObj['assignedTo']);
      this.onChangeUserGroup(this.statusObj['userGroupId'])
    } else {
      this.changeStatusForm.controls['assignedTo'].setValue("");
      this.changeStatusForm.controls['userGroupId'].setValue("");
    }
  }

  getRawLeadStatusDetails(): void {
    this.httpService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALS_API_LEAD_STATUS, this.statusObj['rawLeadId'], false, null, 1).subscribe((response) => {
        this.statusObj = response.resources;
        this.changeStatusForm.patchValue(response.resources);
        if (this.statusObj['userGroupId']) {
          this.changeStatusForm.controls['assignedTo'].setValue(this.statusObj['assignedTo']);
          this.onChangeUserGroup(this.statusObj['userGroupId'])
        } else {
          this.changeStatusForm.controls['assignedTo'].setValue("");
          this.changeStatusForm.controls['userGroupId'].setValue("");
        }
      });
  }

  onChangeUserGroup(groupId): void {
    this.httpService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_USERS_FROM_GROUP, undefined, false,
      prepareGetRequestHttpParams(undefined, undefined,
        { groupId: this.changeStatusForm.value.userGroupId })).subscribe((response) => {
          if (!groupId) {
            this.changeStatusForm.controls['assignedTo'].setValue("");
          }
          this.assignToUsers = response.resources;
        });
  }

  getStatuses(): void {
    this.httpService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_STATUSES, null, false, null).subscribe((response) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.rawLeadStatuses = response.resources;
          this.onChangeStatus(this.statusObj['rawLeadStatusId']);
          this.checkedInArray = this.rawLeadStatuses.find(e => e.id == this.statusObj['rawLeadStatusId'])
          if (!this.checkedInArray) {
            this.changeStatusForm.controls['rawLeadStatusId'].setValue("");
          } else {
            this.changeStatusForm.controls['rawLeadStatusId'].setValue(this.statusObj['rawLeadStatusId']);
          }
          this.getUserGroups();
        }
      });
  }

  onChangeStatus(e): void {
    let found = this.rawLeadStatuses.filter(i => i.id == e);
    if (found.length > 0) {
      if (found[0].displayName == "Allocated") {
        this.show = true;
        this.changeStatusForm = addFormControls(this.changeStatusForm, ["userGroupId", "assignedTo"]);
        this.changeStatusForm = setRequiredValidator(this.changeStatusForm, ["userGroupId", "assignedTo"]);
      } else {
        this.show = false;
        this.changeStatusForm = removeFormControls(this.changeStatusForm, ["userGroupId", "assignedTo"]);
      }
    }
  }

  getUserGroups(): void {
    this.httpService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      SalesModuleApiSuffixModels.RAW_LEAD_USER_GROUPS, null, false, null).subscribe((response) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.userGroups = response.resources;
        }
      });
  }

  changeStatus(): void {
    if (this.changeStatusForm.invalid) {
      return;
    }
    this.httpService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_RAW_LEAD_STATUS, this.changeStatusForm.value, 1).subscribe((res) => {
      if (res.isSuccess && res.statusCode === 200) {
        this.dialog.closeAll();
      }
    });
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}