import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  loggedInUserData, RawLeadManualAddEditModel, SalesModuleApiSuffixModels, selectDynamicEagerLoadingLeadCatgoriesState$,
  selectDynamicEagerLoadingSourcesState$, selectStaticEagerLoadingSiteTypesState$, selectStaticEagerLoadingTitlesState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, FindDuplicatePipe, formConfigs, setRequiredValidator, SnackbarService } from '@app/shared';
import { RawLeadStatus, ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import {
  addNullOrStringDefaultFormControlValue,
  countryCodes,
  debounceTimeForSearchkeyword,
  destructureAfrigisObjectAddressComponents, disableFormControls, emailPattern, enableFormControls, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams,
  removeFormControlError
} from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-raw-lead-add-edit',
  templateUrl: './raw-lead-add-edit.component.html',
  styleUrls: ['./raw-lead.component.scss']
})

export class RawLeadAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  rawLeadForm: FormGroup;
  rawLeadId = "";
  titleList = [];
  sourceList = [];
  siteTypeList = [];
  rawLeadCategoriesList = [];
  rawLeadCategories: any = [];
  countryCodes = countryCodes;
  showReferral: boolean = false;
  fromUrl = '';
  breadCrumb: BreadCrumbModel;
  rawLeadDetails;
  loggedInUserData: LoggedInUserModel;
  selectedAddressOption;
  isFormSubmitted = false;
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;

  constructor(private activatedRoute: ActivatedRoute, private crudService: CrudService, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private router: Router, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createRawLeadManualAddForm();
    this.rawLeadForm.get('mobile2CountryCode').valueChanges.subscribe((mobile2CountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(mobile2CountryCode);
    });
    this.rawLeadForm.get('mobile2').valueChanges.subscribe(() => {
      this.setPhoneNumberLengthByCountryCode(this.rawLeadForm.get('mobile2CountryCode').value);
    });
    this.onFormControlChanges();
    if (this.rawLeadId) {
      this.getRawLeadById().subscribe((response: IApplicationResponse) => {
        this.rawLeadDetails = response.resources;
        let rawLeadManualModel = new RawLeadManualAddEditModel(response.resources);
        if (response.resources.leadCategoryId) {
          for (var i = 0; i < response.resources.leadCategoryId.split(",").length; i++) {
            this.rawLeadCategories.push(({'id':parseInt(response.resources.leadCategoryId.split(",")[i]),'displayName':response.resources.leadCategoryName.split(",")[i]}));
          }
          rawLeadManualModel.rawLeadCategories = this.rawLeadCategories;
          rawLeadManualModel.leadCategoryId = this.rawLeadCategories;
        }
        rawLeadManualModel.mobile1CountryCode = rawLeadManualModel.mobile1CountryCode ? rawLeadManualModel.mobile1CountryCode : "+27";
        rawLeadManualModel.phoneNumberType = rawLeadManualModel.phoneNumberType ? rawLeadManualModel.phoneNumberType : "M";
        rawLeadManualModel.officeNoCountryCode = rawLeadManualModel.officeNoCountryCode ? rawLeadManualModel.officeNoCountryCode : "+27";
        rawLeadManualModel.mobile2CountryCode = rawLeadManualModel.mobile2CountryCode ? rawLeadManualModel.mobile2CountryCode : "+27";
        rawLeadManualModel.premisesNoCountryCode = rawLeadManualModel.premisesNoCountryCode ? rawLeadManualModel.premisesNoCountryCode : "+27";
        if (rawLeadManualModel.formatedAddress) {
          rawLeadManualModel.formatedAddress = rawLeadManualModel.formatedAddress;
        }
        if (rawLeadManualModel.latitude && rawLeadManualModel.longitude) {
          rawLeadManualModel.latLong = rawLeadManualModel.latitude + "," + rawLeadManualModel.longitude;
        }
        if (rawLeadManualModel.isAddressComplex || rawLeadManualModel.isAddressExtra) {
          this.selectedAddressOption = {};
          this.selectedAddressOption["description"] = rawLeadManualModel.formatedAddress;
        }
        if (rawLeadManualModel['refereeComments'] == "" || rawLeadManualModel['refereeComments'] == null) {
          this.showReferral = false;
          this.rawLeadForm.get('refereeComments').clearValidators()
          this.rawLeadForm.get('refereeComments').updateValueAndValidity();
        }
        else {
          this.showReferral = true;
        }
        this.rawLeadForm.setValue(rawLeadManualModel);
        this.rawLeadForm.get("leadCategoryId").setValue(this.rawLeadCategories);
        this.prepareDynamicBreadCrumbs();
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
    else {
      this.prepareDynamicBreadCrumbs();
      this.rawLeadForm.controls['mobile1CountryCode'].setValue("+27");
      this.rawLeadForm.controls['premisesNoCountryCode'].setValue("+27");
      this.rawLeadForm.controls['mobile2CountryCode'].setValue("+27");
      this.rawLeadForm.controls['officeNoCountryCode'].setValue("+27");
      this.rawLeadForm.controls['phoneNumberType'].setValue("M");
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(selectDynamicEagerLoadingSourcesState$),
      this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(selectDynamicEagerLoadingLeadCatgoriesState$),
      this.store.select(selectStaticEagerLoadingSiteTypesState$),
      this.rxjsService.getFromUrl(),
      this.activatedRoute.queryParams,
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.sourceList = response[0];
      this.titleList = response[1];
      let leadCategoryTmpArray = response[2];
      this.siteTypeList = response[3];
      if (leadCategoryTmpArray) {
        this.rawLeadCategoriesList = leadCategoryTmpArray;
      }
      this.fromUrl = response[4];
      this.rawLeadId = response[5]['id'];
      this.loggedInUserData = new LoggedInUserModel(response[6]);
    });
  }

  prepareDynamicBreadCrumbs() {
    if (this.fromUrl == 'Raw Leads List') {
      this.breadCrumb = {
        pageTitle: { key: 'Raw Lead Info', value: this.rawLeadDetails?.rawLeadRefNo },
        items: [{ key: 'Sales' }, { key: 'Raw Leads', routeUrl: '/sales/raw-lead' }, { key: '', routeUrl: '' }, { key: this.rawLeadId ? 'Update' : 'Create' }]
      };
    }
    else if (this.fromUrl == 'My Raw Leads List') {
      this.breadCrumb = {
        pageTitle: { key: 'My Raw Lead Info', value: this.rawLeadDetails?.rawLeadRefNo },
        items: [{ key: 'My Tasks' }, { key: 'My Sales' }, { key: 'My Raw Leads' }, { key: '', routeUrl: '' }, { key: this.rawLeadId ? 'Update' : 'Create' }]
      };
    }
  }

  onFormControlChanges() {
    this.rawLeadForm.get('isAddressComplex').valueChanges.subscribe((isAddressComplex: boolean) => {
      if (isAddressComplex) {
        this.rawLeadForm = setRequiredValidator(this.rawLeadForm as FormGroup, ["formatedAddress"]);
        this.rawLeadForm.get('formatedAddress').markAllAsTouched();
      }
      else {
        this.rawLeadForm = removeFormControlError(this.rawLeadForm, 'required', 'formatedAddress');
        this.rawLeadForm = addNullOrStringDefaultFormControlValue(this.rawLeadForm, [{ controlName: 'buildingNo', defaultValue: null },
        { controlName: 'buildingName', defaultValue: null }]);
      }
    });
    this.rawLeadForm.get('isAddressExtra').valueChanges.subscribe((isAddressExtra: boolean) => {
      if (isAddressExtra) {
        this.rawLeadForm = enableFormControls(this.rawLeadForm, ["estateStreetNo", "estateStreetName", "estateName"]);
        this.rawLeadForm = setRequiredValidator(this.rawLeadForm as FormGroup, ["formatedAddress"]);
        this.rawLeadForm.get('formatedAddress').markAllAsTouched();
      }
      else {
        this.rawLeadForm = removeFormControlError(this.rawLeadForm, 'required', 'formatedAddress');
        this.rawLeadForm = disableFormControls(this.rawLeadForm, ["estateStreetNo", "estateStreetName", "estateName"]);
        this.rawLeadForm = addNullOrStringDefaultFormControlValue(this.rawLeadForm, [{ controlName: 'estateStreetNo', defaultValue: null },
        { controlName: 'estateStreetName', defaultValue: null }, { controlName: 'estateName', defaultValue: null }]);
      }
    });
    this.rawLeadForm.get('isAfrigisSearch').valueChanges.subscribe((isAfrigisSearch: boolean) => {
      if (isAfrigisSearch) {
        this.rawLeadForm = setRequiredValidator(this.rawLeadForm as FormGroup, ["formatedAddress"]);
        this.rawLeadForm.get('formatedAddress').markAllAsTouched();
      }
      else {
        this.rawLeadForm = removeFormControlError(this.rawLeadForm, 'required', 'formatedAddress');
      }
    });
    this.onFormatedAddressFormControlValueChanges();
  }

  onFormatedAddressFormControlValueChanges(): void {
    this.rawLeadForm
      .get("formatedAddress")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((searchtext) => {
          this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS, null, true,
            prepareRequiredHttpParams({
              searchtext,
              isAfrigisSearch: this.rawLeadForm.get("isAfrigisSearch").value
            }));
        });
  }

  clearFormControlValues(): void {
    this.rawLeadForm.patchValue({
      latitude: null,
      longitude: null,
      latLong: null,
      suburb: null,
      city: null,
      province: null,
      postalCode: null,
      streetName: null,
      streetNo: null,
      buildingNo: null,
      buildingName: null,
      estateName: null,
      estateStreetName: null,
      estateStreetNo: null,
    });
  }

  getAddressFullDetails(seoid: string): void {
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_ADDRESS_DETAILS, undefined, false,
        prepareRequiredHttpParams({ seoid })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200 && response.resources) {
            response.resources.addressDetails.forEach((addressObj) => {
              this.patchAddressFormGroupValues("address", addressObj, addressObj["address_components"]);
            });
            this.rawLeadForm.updateValueAndValidity();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  patchAddressFormGroupValues(
    type: string,
    addressObj,
    addressComponents: Object[]
  ) {
    addressObj.geometry.location.lat = addressObj.geometry.location.lat
      ? addressObj.geometry.location.lat : "";
    addressObj.geometry.location.lng = addressObj.geometry.location.lng
      ? addressObj.geometry.location.lng : "";
    if (addressObj.geometry.location.lat && addressObj.geometry.location.lng) {
      addressObj.geometry.location.latLong = `${addressObj.geometry.location.lat}, ${addressObj.geometry.location.lng}`;
    } else {
      addressObj.geometry.location.latLong = "";
    }
    let {
      suburbName,
      cityName,
      provinceName,
      postalCode,
      streetName,
      streetNo,
      buildingNo,
      buildingName,
      estateName,
      estateStreetName,
      estateStreetNo,
    } = destructureAfrigisObjectAddressComponents(addressComponents);
    if (type == "address") {
      this.rawLeadForm.patchValue(
        {
          latitude: addressObj.geometry.location.lat,
          longitude: addressObj.geometry.location.lng,
          latLong: addressObj.geometry.location.latLong,
          buildingName,
          buildingNo,
          estateName,
          estateStreetName,
          estateStreetNo,
          suburb: suburbName,
          city: cityName,
          province: provinceName,
          postalCode,
          streetName,
          streetNo,
        },
        { emitEvent: false, onlySelf: true }
      );
    }
  }

  onSelectedItemOption(selectedObject): void {
    if (!selectedObject) return;
    this.selectedAddressOption = selectedObject;
    if (this.rawLeadForm.get('isAfrigisSearch').value) {
      this.getAddressFullDetails(selectedObject["seoid"]);
    } else {
      selectedObject["longitude"] = selectedObject["longitude"]
        ? selectedObject["longitude"] : "";
      selectedObject["latitude"] = selectedObject["latitude"]
        ? selectedObject["latitude"] : "";
      if (selectedObject["latitude"] && selectedObject["longitude"]) {
        selectedObject["latLong"] = `${selectedObject["latitude"]}, ${selectedObject["longitude"]}`;
      } else {
        selectedObject["latLong"] = "";
      }
      this.rawLeadForm.patchValue(selectedObject, { emitEvent: false, onlySelf: true });
    }
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.rawLeadForm.get('mobile2').setValidators([Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        break;
      default:
        this.rawLeadForm.get('mobile2').setValidators([Validators.minLength(formConfigs.indianContactNumberMaxLength),
        Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        break;
    }
  }

  changeLeadSource(value) {
    const selectedIndex = value.target.selectedIndex;
    const optGroupLabel = value.target.options[selectedIndex].parentNode['label'];
    if (optGroupLabel.includes('refer') || optGroupLabel.includes('referral') || optGroupLabel.includes('reference')
      || optGroupLabel.includes('Refer') || optGroupLabel.includes('Referral') || optGroupLabel.includes('Reference')
      || optGroupLabel.includes('referrals') || optGroupLabel.includes('Referrals')) {
      this.showReferral = true;
      this.rawLeadForm.addControl('refereeComments', new FormControl(''));
      this.rawLeadForm.get('refereeComments').setValidators([Validators.required])
      this.rawLeadForm.get('refereeComments').updateValueAndValidity();
    } else {
      this.showReferral = false;
      this.rawLeadForm.controls['refereeComments'].setValue('');
      this.rawLeadForm.get('refereeComments').clearValidators()
      this.rawLeadForm.get('refereeComments').updateValueAndValidity();
    }
  }

  createRawLeadManualAddForm(): void {
    let rawLeadManualAddEditModel = new RawLeadManualAddEditModel();
    this.rawLeadForm = this.formBuilder.group({});
    Object.keys(rawLeadManualAddEditModel).forEach((key) => {
      if (key == "email") {
        this.rawLeadForm.addControl(key, new FormControl(rawLeadManualAddEditModel[key], Validators.compose([Validators.email, emailPattern]))
        );
      }
      else {
        this.rawLeadForm.addControl(key, new FormControl(rawLeadManualAddEditModel[key]));
      }
    });
    this.rawLeadForm = setRequiredValidator(this.rawLeadForm, ["mobile1", "lastName", "sourceId"]);
  }

  getComponentDemandedData(): Observable<any> {
    return forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_TITLES, undefined, true, null, 1),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SOURCE_TYPES, undefined, true, null, 1),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_LEAD_CATEGORY, undefined, true, null, 1),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SITE_TYPES, undefined, true, null, 1)]
    );
  }

  getRawLeadById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_RAW_LEADS,
      this.rawLeadId, false, null);
  }

  validateAtleastOneFieldToBeMandatory(): void {
    if ((this.rawLeadForm.get("isAddressComplex").value && !this.rawLeadForm.get("buildingNo").value && !this.rawLeadForm.get("buildingName").value) ||
      (this.rawLeadForm.get("isAddressExtra").value && !this.rawLeadForm.get("estateName").value &&
        !this.rawLeadForm.get("estateStreetName").value && !this.rawLeadForm.get("estateStreetNo").value)) {
      this.setAtleastOneFieldRequiredError();
    }
    else {
      this.rawLeadForm = removeFormControlError(this.rawLeadForm, "atleastOneOfTheFieldsIsRequired");
    }
  }

  setAtleastOneFieldRequiredError() {
    if (this.rawLeadForm.get("isAddressComplex").value) {
      this.rawLeadForm.get("buildingNo").setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.rawLeadForm.get("buildingName").setErrors({ atleastOneOfTheFieldsIsRequired: true });
    }
    if (this.rawLeadForm.get("isAddressExtra").value) {
      this.rawLeadForm.get("estateName").setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.rawLeadForm.get("estateStreetNo").setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.rawLeadForm.get("estateStreetName").setErrors({ atleastOneOfTheFieldsIsRequired: true });
    }
  }

  onSubmit(): void {
    this.isFormSubmitted = true;
    this.validateAtleastOneFieldToBeMandatory();
    if (this.rawLeadForm.invalid) {
      return;
    }
    if ((this.rawLeadForm.controls.isAddressComplex.value || this.rawLeadForm.controls.isAddressExtra.value ||
      this.rawLeadForm.controls.isAfrigisSearch.value) && (!this.rawLeadForm.value.province ||
        !this.rawLeadForm.value.city)) {
      this.rawLeadForm.get('formatedAddress').markAllAsTouched();
      this.snackbarService.openSnackbar("Province Name / City Name is required..!!", ResponseMessageTypes.ERROR);
      return;
    }
    const isMobile1Duplicate = new FindDuplicatePipe().transform(this.rawLeadForm.get('mobile1').value,
      this.rawLeadForm, 'mobile1');
    const isMobile2Duplicate = new FindDuplicatePipe().transform(this.rawLeadForm.get('mobile2').value,
      this.rawLeadForm, 'mobile2', this.rawLeadForm.get('mobile2CountryCode').value);
    const isOfficeNoDuplicate = new FindDuplicatePipe().transform(this.rawLeadForm.get('officeNo').value,
      this.rawLeadForm, 'officeNo');
    const isPremisesNoDuplicate = new FindDuplicatePipe().transform(this.rawLeadForm.get('premisesNo').value,
      this.rawLeadForm, 'premisesNo');
    if (this.rawLeadForm.invalid || isMobile1Duplicate || isMobile2Duplicate || isOfficeNoDuplicate || isPremisesNoDuplicate) {
      this.rawLeadForm.markAllAsTouched();
      return;
    }
    if (this.rawLeadForm.value.latLong) {
      this.rawLeadForm.value.latitude = this.rawLeadForm.value.latLong.split(",")[0];
      this.rawLeadForm.value.longitude = this.rawLeadForm.value.latLong.split(",")[1];
    }
    this.rawLeadForm.value.rawLeadStatusId = RawLeadStatus.New;
    this.rawLeadForm.value.rawLeadId = this.rawLeadId;
    let ContactNumber = this.rawLeadForm.value.mobile1;
    this.rawLeadForm.value.contactCountryCode = "";
    this.rawLeadForm.value.mobile1 = ContactNumber;
    if (this.rawLeadId) {
      this.rawLeadForm.value.modifiedUserId = this.loggedInUserData.userId;
    } else {
      this.rawLeadForm.value.createdUserId = this.loggedInUserData.userId;
    }
    if (this.rawLeadForm.value.mobile1) {
      this.rawLeadForm.value.mobile1 = this.rawLeadForm.value.mobile1.toString().replace(/\s/g, "");
    }
    if (this.rawLeadForm.value.mobile2) {
      this.rawLeadForm.value.mobile2 = this.rawLeadForm.value.mobile2.toString().replace(/\s/g, "");
    }
    if (this.rawLeadForm.value.officeNo) {
      this.rawLeadForm.value.officeNo = this.rawLeadForm.value.officeNo.toString().replace(/\s/g, "");
    }
    if (this.rawLeadForm.value.premisesNo) {
      this.rawLeadForm.value.premisesNo = this.rawLeadForm.value.premisesNo.toString().replace(/\s/g, "");
    }
    if (this.rawLeadForm.value.refereeComments == null) {
      this.rawLeadForm.get('refereeComments').setValue('');
      this.rawLeadForm.get('refereeComments').clearValidators()
      this.rawLeadForm.get('refereeComments').updateValueAndValidity();
    }
    if(this.rawLeadForm.value.leadCategoryId){
      let selectedleadCategoryId = [];
      this.rawLeadForm.value?.leadCategoryId?.forEach(result=>{
       selectedleadCategoryId.push(result?.id);
      })
      this.rawLeadForm.value.leadCategoryId = selectedleadCategoryId;
    }
    this.rawLeadForm.value.premisesNo = this.rawLeadForm.value.premisesNo.toString().replace(/\s/g, "");
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.rawLeadId ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_RAW_LEADS, this.rawLeadForm.value, 1) :
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_RAW_LEADS, this.rawLeadForm.value, 1)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        if (this.fromUrl == 'Raw Leads List' || this.fromUrl == 'Leads List') {
          this.router.navigate(['sales/raw-lead/view'], { queryParams: { id: response.resources } });
        }
        else if (this.fromUrl == 'My Raw Leads List' || this.fromUrl == 'My Leads List') {
          this.router.navigate(['my-tasks/my-sales/my-raw-leads/view'], { queryParams: { id: response.resources } });
        }
      }
    });
  }

  onRedirectToRawLeadsList() {
    if (this.fromUrl == 'Raw Leads List') {
      this.router.navigateByUrl('/sales/raw-lead');
    }
    else if (this.fromUrl == 'My Raw Leads List') {
      this.router.navigate(['my-tasks/my-sales'],
        { queryParams: { tab: 0 } });
    }
  }
}