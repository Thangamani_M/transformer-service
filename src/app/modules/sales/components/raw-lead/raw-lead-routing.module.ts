import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RawLeadAddEditComponent, RawLeadImportFailedListCompenent, RawLeadListComponent, RawLeadUploadComponent, RawLeadViewComponent } from '@sales/components/raw-lead';
import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services/authguards';

const creditNoteModuleRoutes: Routes = [
    { path: '', component: RawLeadListComponent, data: { title: 'Raw Lead' }, canActivate: [AuthGuard] },
    { path: 'add-edit', component: RawLeadAddEditComponent, data: { title: 'Raw Lead Add/Edit' }, canActivate: [AuthGuard] },
    { path: 'view', component: RawLeadViewComponent, data: { title: 'Raw Lead View' }, canActivate: [AuthGuard] },
    { path: 'upload', component: RawLeadUploadComponent, data: { title: 'Raw Lead upload' }, canActivate: [AuthGuard] },
    { path: 'import-failed', component: RawLeadImportFailedListCompenent, data: { title: 'Raw Lead Import Failed' }, canActivate: [AuthGuard] },
];

@NgModule({
    imports: [RouterModule.forChild(creditNoteModuleRoutes)]
})

export class RawLeadRoutingModule { }
