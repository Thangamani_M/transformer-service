import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { loggedInUserData, RawLeadFailedModel, RawLeadUploadAddEditModel, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, ReusablePrimeNGTableFeatureService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { environment } from '@environments/environment';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-raw-lead-upload',
  templateUrl: './raw-lead-upload.component.html',
  styleUrls: ['./raw-lead.component.scss']
})

export class RawLeadUploadComponent implements OnInit {
  rawLeadForm: FormGroup;
  rawLeadArray: FormArray;
  rawLeadFailedArray: FormArray;
  rawLeadAllDataArray: FormArray;
  rawLeadFailureArray: RawLeadFailedModel[] = [];
  rawLeadSuccessArray: RawLeadFailedModel[] = [];
  rawLeadFailureCount: Number = 0;
  rawLeadSuccessCount: Number = 0;
  duplicatingEmailCount: number = 0;
  duplicatingContactNumberCount: number = 0;
  invalidLeadCategory: boolean = false;
  sourceList = [];
  siteTypeList = [];
  rawLeadCategoryList = [];
  showFailedImport: boolean = false;
  leadCategoryList = [,];
  leadEmailList: Array<string> = [];
  leadEmailDuplicating: Array<boolean> = [];
  leadContactNumberList: Array<string> = [];
  leadContactNumberDuplicating: Array<boolean> = [];
  leadContactNumberValid: Array<boolean> = [];
  processedData: RawLeadUploadAddEditModel[];
  initialBatchSize: number = 50;
  batchSize: number = 15;
  displayedRowCount: number = 0;
  initialFailedBatchSize: number = 50;
  failedBatchSize: number = 15;
  displayedFailedRowCount: number = 0;
  mobileNumberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  throttle = 50;
  scrollDistance = 2;
  scrollUpDistance = 2;
  loggedInUserData: LoggedInUserModel;
  breadCrumb: BreadCrumbModel = {
    pageTitle: { key: 'Raw Lead Upload' },
    items: [{ key: 'Sales' }, { key: 'Raw Leads', routeUrl: '/sales/raw-lead' }, { key: 'File Upload' }]
  };
  uploadedFile;
  uploadResponse;
  showRecordCount = false;

  constructor(private crudService: CrudService, private http: HttpClient, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private router: Router, private httpCancelService: HttpCancelService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService) {
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.combineLatestNgrxStoreData();
    this.createRawLeadUploadAddForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      });
  }

  createRawLeadUploadAddForm() {
    this.rawLeadForm = this.formBuilder.group({
      fileName: '',
      rawLeadArray: this.formBuilder.array([])
      , rawLeadFailedArray: this.formBuilder.array([])
      , rawLeadAllDataArray: this.formBuilder.array([])
    });
  }

  emitUploadedFiles(uploadedFile) {
    if (!uploadedFile || uploadedFile?.length == 0) {
      return;
    }
    this.showFailedImport = false;
    this.rawLeadFailureCount = 0;
    this.rawLeadSuccessCount = 0;
    this.rawLeadFailureArray = [];
    this.rawLeadSuccessArray = [];
    this.uploadedFile = uploadedFile;
    this.rawLeadForm.get('fileName').setValue(uploadedFile.name);
  }

  saveData = (function () {
    var a = document.createElement("a");
    document.body.appendChild(a);
    return function (data, fileName) {
      var json = JSON.stringify(data),
        blob = new Blob([json], { type: "octet/stream" }),
        url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = fileName;
      a.click();
      window.URL.revokeObjectURL(url);
    };
  }());

  getErroredFiles(batchId) {
    let obj = prepareGetRequestHttpParams(null, null, {
      batchId: batchId
    });
    let url = environment.SALES_API + SalesModuleApiSuffixModels.SALES_API_GET_DOWNLOADABLE_FILES + "?" + obj
    this.http.get(url, {
      responseType: 'blob',
      withCredentials: false,
      headers: new HttpHeaders({
        'Content-Type': 'application/octet-stream'
      })
    }).subscribe(response => {
      let link = document.createElement('a');
      let blob = new Blob([response], { type: 'application/octet-stream' });
      link.href = URL.createObjectURL(blob);
      link.download = 'file.xlsx';
      link.click();
      URL.revokeObjectURL(link.href);
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  upload() {
    let formData = new FormData();
    formData.append("raw_lead_file", this.uploadedFile);
    formData.append("createdUserId", this.loggedInUserData.userId);
    this.crudService.fileUpload(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.RAW_LEADS_PROCESS_FILES, formData).subscribe((response) => {
      if (response.isSuccess && response.resources && response.statusCode == 200) {
        this.uploadResponse = response.resources;
        this.showRecordCount = true;
        if (response.resources.failedRecordCount > 0) {
          this.getErroredFiles(response.resources.batchId);
        }
      }
      else {
        this.rawLeadForm.get('fileName').setValue("");
      }
    });
  }

  get getRawLeadArrays(): FormArray {
    if (!this.rawLeadForm) return;
    return this.rawLeadForm.get('rawLeadArray') as FormArray;
  }

  get getRawLeadAllDataArrays(): FormArray {
    if (!this.rawLeadForm) return;
    return this.rawLeadForm.get('rawLeadAllDataArray') as FormArray;
  }

  get getRawLeadFailedArrays(): FormArray {
    if (!this.rawLeadForm) return;
    return this.rawLeadForm.get('rawLeadFailedArray') as FormArray;
  }

  createRawLeadItems(rawLeadItems: RawLeadUploadAddEditModel): FormGroup | undefined {
    let formControls = {};
    Object.keys(rawLeadItems).forEach((key) => {
      formControls[key] = [rawLeadItems[key]]
    });
    return this.formBuilder.group(formControls);
  }

  createRawLeadFailedItems(rawLeadFailedItems: RawLeadFailedModel): FormGroup | undefined {
    let array = new RawLeadFailedModel(rawLeadFailedItems);
    let formControls = {};
    Object.keys(array).forEach((key) => {
      formControls[key] = [array[key]]
    });
    return this.formBuilder.group(formControls);
  }

  removeRawLeadItem(i) {
    if (this.getRawLeadArrays.length === 1) {
      this.snackbarService.openSnackbar("Atleast one Raw lead item is required", ResponseMessageTypes.WARNING);
      return;
    }
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, undefined,
      DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          this.getRawLeadArrays.removeAt(i);
          this.getRawLeadAllDataArrays.removeAt(i);
          this.leadCategoryList.splice(i, 1);
          this.leadEmailList.splice(i, 1);
          this.leadEmailDuplicating.splice(i, 1);
          this.leadContactNumberList.splice(i, 1);
          this.leadContactNumberDuplicating.splice(i, 1);
          this.leadContactNumberValid.splice(i, 1);
          this.findDuplicationCount();
        }
      });
  }

  onSubmit() {
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rawLeadFailureCount = 0;
    this.rawLeadSuccessCount = 0;
    this.rawLeadFailureArray = [];
    this.rawLeadSuccessArray = [];
    this.invalidLeadCategory = false;
    let invalidContactNumbers = false;
    this.getRawLeadAllDataArrays.value.forEach((element, i) => {
      element.leadCategoryId = this.leadCategoryList[i];
      if (!this.invalidLeadCategory && element.leadCategoryId.length == 0) this.invalidLeadCategory = true;
      if (this.leadContactNumberValid[i] === false && invalidContactNumbers === false) invalidContactNumbers = true;
    });
    if (this.rawLeadForm.invalid || this.getRawLeadArrays.length === 0 || this.duplicatingEmailCount > 0 || this.duplicatingContactNumberCount > 0 || this.invalidLeadCategory || invalidContactNumbers) {
      return;
    }
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.RAW_LEADS_IMPORT, this.rawLeadForm.value.rawLeadAllDataArray)
      .subscribe((response: IApplicationResponse) => {
        if (!response.isSuccess) {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        }
        else {
          if (response.resources.length >= 2) {
            let resultSuccess = response.resources[0];
            this.rawLeadSuccessCount = resultSuccess.length;
            let arraySuccess: RawLeadFailedModel[] = resultSuccess.map(item => new RawLeadFailedModel(item));
            this.rawLeadSuccessArray = arraySuccess;
            let resultFailure = response.resources[1];
            this.rawLeadFailureCount = resultFailure.length;
            let arrayFailure: RawLeadFailedModel[] = resultFailure.map(item => new RawLeadFailedModel(item));
            this.rawLeadFailureArray = arrayFailure;
            if (this.rawLeadSuccessCount > 0 && this.rawLeadFailureCount == 0) {
              this.snackbarService.openSnackbar("Raw leads imported successfully", ResponseMessageTypes.SUCCESS);
              this.router.navigateByUrl('/sales/raw-lead');
            }
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  removeUploadedFile() {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to remove the selected file?`, undefined,
      DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        this.showRecordCount = false;
        this.showFailedImport = false;
        this.rawLeadFailureCount = 0;
        this.rawLeadSuccessCount = 0;
        this.rawLeadFailureArray = [];
        this.rawLeadSuccessArray = [];
        this.duplicatingContactNumberCount = 0;
        this.duplicatingEmailCount = 0;
        this.leadEmailList = [];
        this.leadEmailDuplicating = [];
        this.leadContactNumberList = [];
        this.leadContactNumberDuplicating = [];
        this.leadContactNumberValid = [];
        if (this.rawLeadArray !== undefined) this.rawLeadArray.clear();
        if (this.rawLeadAllDataArray !== undefined) this.rawLeadAllDataArray.clear();
        if (this.rawLeadFailedArray !== undefined) this.rawLeadFailedArray.clear();
        this.rawLeadForm.get('fileName').setValue("");
        this.snackbarService.openSnackbar("File has been removed", ResponseMessageTypes.SUCCESS);
      });
  }

  showHideFailedImportList() {
    this.rxjsService.setGlobalLoaderProperty(true);
    if (!this.showFailedImport) {
      this.displayedFailedRowCount = 0;
      this.showFailedImport = true;
      this.rawLeadFailedArray = this.getRawLeadFailedArrays;
      this.getRawLeadFailedArrays.clear();
      var newBatch = this.rawLeadFailureArray.slice(0, this.initialBatchSize);
      if (newBatch.length > 0) {
        newBatch.forEach((itm) => {
          let obj = this.createRawLeadFailedItems(itm);
          this.rawLeadFailedArray.push(obj);
        });
        this.displayedFailedRowCount += newBatch.length;
      }
    }
    else {
      this.hideFailedImportList();
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  hideFailedImportList() {
    this.showFailedImport = false;
  }

  onLeadCategoryItemChanged(LeadCategoryItem: Array<string>, i: number) {
    this.leadCategoryList[i] = LeadCategoryItem;
  }

  findDuplicationCount() {
    this.findDuplicationEmail();
    this.findDuplicationContactNumber();
  }

  findDuplicationEmail() {
    this.duplicatingEmailCount = 0;
    var emailDuplicate = this.leadEmailList.reduce((a, c) => (a[c] = (a[c] || 0) + 1, a), Object.create(null));
    this.leadEmailDuplicating = this.leadEmailDuplicating.map(item => {
      item = false
      return item;
    });
    Object.keys(emailDuplicate).forEach(key => {
      if (key !== '' && emailDuplicate[key] > 1) {
        this.duplicatingEmailCount += emailDuplicate[key];
        this.leadEmailList.forEach((item, i) => {
          if (this.leadEmailList[i] == key)
            this.leadEmailDuplicating[i] = true;
        });
      }
    });
  }

  findDuplicationContactNumber() {
    this.duplicatingContactNumberCount = 0;
    var contactNumberDuplicate = this.leadContactNumberList.reduce((a, c) => (a[c] = (a[c] || 0) + 1, a), Object.create(null));
    this.leadContactNumberDuplicating = this.leadContactNumberDuplicating.map(item => {
      item = false
      return item;
    });
    Object.keys(contactNumberDuplicate).forEach(key => {
      if ((key !== '' && key !== '+27' && key !== '+91' && key !== '+45') && contactNumberDuplicate[key] > 1) {
        this.duplicatingContactNumberCount += contactNumberDuplicate[key];
        this.leadContactNumberList.forEach((item, i) => {
          if (this.leadContactNumberList[i] == key)
            this.leadContactNumberDuplicating[i] = true;
        });
      }
    });
  }

  validateContactNumber(i) {
    let tempContactNumber = this.leadContactNumberList[i];
    tempContactNumber = tempContactNumber.replace('+27', '').replace('+91', '').replace('+45', '');
    this.leadContactNumberValid[i] = false;
    if (tempContactNumber.length <= 10 && tempContactNumber.length >= 9)
      this.leadContactNumberValid[i] = true;
  }

  onScrollDownRawLeadArray() {
    var newBatch = this.processedData.slice(this.displayedRowCount, this.displayedRowCount + this.batchSize);
    if (newBatch.length > 0) {
      newBatch.forEach((itm, i) => {
        let obj = this.createRawLeadItems(itm);
        this.createNewRowInRawLead(obj, (this.displayedRowCount + i));
      });
      this.displayedRowCount += newBatch.length;
    }
  }

  createNewRowInRawLead(rowItem: FormGroup, index: number) {
    this.rawLeadArray.push(rowItem);
    var ctrls = this.rawLeadArray.controls[index];
    ctrls.get("email").valueChanges.subscribe((EmailId: string) => {
      this.leadEmailList[index] = EmailId;
      this.findDuplicationEmail();
    });
    ctrls.get("contactNumber").valueChanges.subscribe((ContactNumber: string) => {
      const contactNumberCountryCode = ctrls.get("contactNumberCountryCode").value;
      let ContactNumberWithCC = (contactNumberCountryCode + ' ' + ContactNumber);
      ContactNumberWithCC = ContactNumberWithCC.replace(' ', '').replace(' ', '').replace(' ', '').replace(' ', '');
      this.leadContactNumberList[index] = ContactNumberWithCC;
      this.findDuplicationContactNumber();
      this.validateContactNumber(index);
    });
    ctrls.get("contactNumberCountryCode").valueChanges.subscribe((ContactNumberCountryCode: string) => {
      const contactNumber = ctrls.get("contactNumber").value;
      let ContactNumberWithCC = (ContactNumberCountryCode + ' ' + contactNumber);
      ContactNumberWithCC = ContactNumberWithCC.replace(' ', '').replace(' ', '').replace(' ', '').replace(' ', '');
      this.leadContactNumberList[index] = ContactNumberWithCC;
      this.findDuplicationContactNumber();
      this.validateContactNumber(index);
    });
  }

  onScrollDownRawLeadFailureArray() {
    var newBatch = this.rawLeadFailureArray.slice(this.displayedFailedRowCount, this.displayedFailedRowCount + this.failedBatchSize);
    if (newBatch.length > 0) {
      newBatch.forEach((itm, i) => {
        let obj = this.createRawLeadFailedItems(itm);
        this.rawLeadFailedArray.push(obj);
      });
      this.displayedFailedRowCount += newBatch.length;
    }
  }

  downloadSampleTemplate() {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', '../../../../../assets/files/raw_lead_upload_sample.xlsx');
    link.setAttribute('download', `raw_lead_upload_sample.xlsx`);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }
}
