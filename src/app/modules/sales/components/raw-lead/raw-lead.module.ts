import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import {
    AddRawLeadsNotesModalComponent, RawLeadAddEditComponent, RawLeadImportFailedListCompenent, RawLeadListComponent,
    RawLeadRoutingModule, RawLeadUploadComponent, RawLeadViewComponent
} from '@sales/components/raw-lead';
@NgModule({
  declarations: [RawLeadListComponent, RawLeadAddEditComponent, RawLeadViewComponent,
    RawLeadUploadComponent, RawLeadImportFailedListCompenent, AddRawLeadsNotesModalComponent],
  imports: [
    CommonModule, MaterialModule, SharedModule, ReactiveFormsModule, FormsModule,
    RawLeadRoutingModule,
  ],
  entryComponents: [AddRawLeadsNotesModalComponent],
  exports: []
})
export class RawLeadModule { }
