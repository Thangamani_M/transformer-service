import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels, Sales_Leads_Components } from '@app/modules';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
    selector: 'app-raw-lead-view',
    templateUrl: './raw-lead-view.component.html',
    styleUrls: ['./raw-lead.component.scss']
})
export class RawLeadViewComponent implements OnInit {
    rawLeadId = "";
    status = "";
    fromUrl = '';
    rawLeadRefNo = "";
    breadCrumb: BreadCrumbModel;
    viewData: any = [
        {
            name: 'BASIC INFORMATION', columns: [
                { name: 'Lead ID', value: "" },
                { name: 'Title', value: "" },
                { name: 'Name', value: "" },
                { name: 'Surname', value: "" },
                { name: 'Phone Number', value: "" },
                { name: 'Email Address', value: "" },
                { name: 'Mobile Number', value: "" },
                { name: 'Office Number', value: "" },
                { name: 'Lead Source', value: "" },
                { name: 'Status', value: "" },
                { name: 'Lead Category', value: "" },
                { name: 'Load Date', value: "" }]
        },
        {
            name: 'ADDRESS INFORMATION', columns: [
                { name: 'Building No', value: "" },
                { name: 'Building Name', value: "" },
                { name: 'Street Number', value: "" }, { name: 'Location Pin', value: "" },
                { name: 'Street Address', value: "" }, { name: 'Suburb', value: "" },
                { name: 'City', value: "" }, { name: 'Province', value: "" }, { name: 'Zip Code', value: "" },
            ]
        }
    ];
    componentPermissions = [];

    constructor(private router: Router, private activatedRoute: ActivatedRoute,
        private rxjsService: RxjsService, private store: Store<AppState>, private snackbarService: SnackbarService,
        private crudService: CrudService) {
    }

    ngOnInit() {
        this.combineLatestNgrxStoreData();
        this.prepareDynamicBreadCrumbs();
        this.crudService.get(ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.SALES_API_RAW_LEADS, this.rawLeadId, false, null).subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode == 200 && response.isSuccess) {
                    this.rawLeadRefNo = response.resources?.rawLeadRefNo;
                    this.viewData = [
                        {
                            name: 'BASIC INFORMATION', columns: [
                                { name: 'Raw Lead ID', value: response.resources?.rawLeadRefNo },
                                { name: 'Title', value: response.resources?.title },
                                { name: 'Name', value: response.resources?.firstName },
                                { name: 'Surname', value: response.resources?.lastName },
                                { name: 'Phone Number', value: (response.resources?.mobile1CountryCode + " " + response.resources?.mobile1), isFullNumber: true },
                                { name: 'Email Address', value: response.resources?.email },
                                { name: 'Mobile Number', value: response.resources?.mobile2 ? (response.resources.mobile2CountryCode + " " + response.resources.mobile2) : '-', isFullNumber: true },
                                { name: 'Office Number', value: response.resources?.officeNo ? (response.resources.officeNoCountryCode + " " + response.resources.officeNo) : '-', isFullNumber: true },
                                { name: 'Lead Source', value: response.resources?.leadSource },
                                {
                                    name: 'Status', value: response.resources?.leadStatusName,
                                    statusClass: response.resources?.statusClass
                                },
                                { name: 'Lead Category', value: response.resources?.leadCategoryName },
                                { name: 'Load Date', value: response.resources?.createdDate, isDateTime: true }]
                        },
                        {
                            name: 'ADDRESS INFORMATION', columns: [
                                { name: 'Building No', value: response.resources?.buildingNo },
                                { name: 'Building Name', value: response.resources?.buildingName },
                                { name: 'Street Number', value: response.resources?.streetNo },
                                { name: 'Street Address', value: response.resources?.streetName }, { name: 'Suburb', value: response.resources?.suburb },
                                { name: 'City', value: response.resources?.city }, { name: 'Province', value: response.resources?.province }, { name: 'Zip Code', value: response.resources?.postalCode },
                                { name: 'Location Pin', value: response.resources?.latitude ? (response.resources?.latitude + " " + response.resources?.longitude) : '-' },
                            ]
                        }
                    ]
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    combineLatestNgrxStoreData(): void {
        combineLatest([
            this.activatedRoute.queryParams, this.rxjsService.getFromUrl(),
            this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
            this.rawLeadId = response[0]['id'];
            this.status = response[0]['leadStatus'];
            this.fromUrl = response[1];
            if (response[2][Sales_Leads_Components.RAW_LEAD]) {
                this.componentPermissions = response[2][Sales_Leads_Components.RAW_LEAD];
            }
        });
    }

    prepareDynamicBreadCrumbs() {
        if (this.fromUrl == 'Raw Leads List') {
            this.breadCrumb = {
                items: [{ key: 'Sales' }, { key: 'Raw Leads', routeUrl: '/sales/raw-lead' },
                { key: 'View' }]
            };
        }
        else if (this.fromUrl == 'My Raw Leads List') {
            this.breadCrumb = {
                items: [{ key: 'My Tasks' }, { key: 'My Sales' }, { key: 'My Raw Leads', routeUrl: 'my-tasks/my-sales?tab=0' },
                { key: 'View' }]
            };
        }
    }

    onEditButtonClicked() {
        if (!this.componentPermissions.find(cF => cF.menuName === PermissionTypes.EDIT)) {
            this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        else {
            let queryParams = { queryParams: { id: this.rawLeadId, status: this.status } };
            if (this.fromUrl == 'Raw Leads List') {
                this.router.navigate(['sales/raw-lead/add-edit'], queryParams);
            }
            else if (this.fromUrl == 'My Raw Leads List') {
                this.router.navigate(['my-tasks/my-sales/my-raw-leads/add-edit'], queryParams);
            }
        }
    }

    onRedirectToRawLeadsList() {
        if (this.fromUrl == 'Raw Leads List') {
            this.router.navigateByUrl('/sales/raw-lead');
        }
        else if (this.fromUrl == 'My Raw Leads List') {
            this.router.navigate(['my-tasks/my-sales'],
                { queryParams: { tab: 0 } })
        }
    }
}