import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { RawLeadFailedModel } from '@app/modules';
import { BreadCrumbModel } from '@app/shared';
import { RxjsService } from '@app/shared/services';
@Component({
  selector: 'app-raw-lead-import-failed-list',
  templateUrl: './raw-lead-import-failed-list.component.html',
  styleUrls: ['./raw-lead.component.scss']
})

export class RawLeadImportFailedListCompenent implements OnInit {
  rawLeadForm: FormGroup;
  rawLeadArray: FormArray;
  rawLeadFailedArray: RawLeadFailedModel[] = [];
  breadCrumb: BreadCrumbModel;
  @ViewChild('fileInput', null) fileInput: ElementRef;

  constructor(private rxjsService: RxjsService,
    private formBuilder: FormBuilder) {
    this.breadCrumb = {
      pageTitle: { key: 'Import Failed List' },
      items: [{ key: 'Sales' }, { key: 'Raw Leads', routeUrl: '/sales/raw-lead' }, { key: 'Add New Raw Leads', routeUrl: '/sales/raw-lead/upload' },
      { key: 'Import Failed List' }]
    };
  }

  ngOnInit(): void {
    this.createRawLeadUploadAddForm();
    this.rawLeadArray = this.getRawLeadArrays;
    while (this.getRawLeadArrays.length) {
      this.getRawLeadArrays.removeAt(this.getRawLeadArrays.length - 1);
    }
    for (let i = 0; i < this.rawLeadFailedArray.length; i++) {
      let resObj = this.rawLeadFailedArray[i];
      this.rawLeadArray.push(this.createRawLeadItems(resObj, i));
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createRawLeadUploadAddForm() {
    this.rawLeadForm = this.formBuilder.group({
      rawLeadArray: this.formBuilder.array([])
    });
  }

  get getRawLeadArrays(): FormArray {
    return this.rawLeadForm.get('rawLeadArray') as FormArray;
  }

  createRawLeadItems(rawLeadFailedItems: RawLeadFailedModel, index: number): FormGroup | undefined {
    let rawLeadArray = new RawLeadFailedModel(rawLeadFailedItems);
    let formControls = {};
    Object.keys(rawLeadArray).forEach((key) => {
      formControls[key] = [rawLeadArray[key]]
    });
    return this.formBuilder.group(formControls);
  }
}
