import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { RawLeadStatuses, SalesModuleApiSuffixModels, Sales_Leads_Components, selectStaticEagerLoadingRawLeadStatusState$, selectStaticEagerLoadingSiteTypesState$ } from '@app/modules';
import { AppState } from "@app/reducers";
import {
  agentLoginDataSelector,
  BreadCrumbModel, CommonPaginationConfig, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, DATE_FORMAT_TYPES, debounceTimeForSearchkeyword, ExtensionModalComponent, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, NO_DATA_FOUND_MESSAGE, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  ResponseMessageTypes,
  RxjsService,
  SnackbarService
} from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { select, Store } from '@ngrx/store';
import { AddRawLeadsNotesModalComponent, ChangeStatusModalComponent } from '@sales/components/raw-lead';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-raw-lead-list',
  templateUrl: './raw-lead-list.component.html',
  styleUrls: ['./raw-lead.component.scss'],
})

export class RawLeadListComponent extends PrimeNgTableVariablesModel {
  columnFilterForm: FormGroup;
  row = {};
  searchColumns;
  agentExtensionNo;
  searchForm: FormGroup;
  otherParams = {};
  breadCrumb: BreadCrumbModel;
  siteTypes;
  primengTableConfigProperties: any = {
    tableCaption: "Raw Leads",
    breadCrumbItems: [{ displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Raw Lead Dashboard' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Raw Leads',
          dataKey: 'rawLeadId',
          enableAddActionBtn: true,
          reorderableColumns: false,
          resizableColumns: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'rawLeadRefNo', header: 'Raw Lead ID', width: '200px' },
          { field: 'client', header: 'Client Name', width: '200px' },
          { field: 'email', header: 'Email Address', width: '200px' },
          { field: 'phoneNumber', header: 'Phone Number', width: '200px' },
          { field: 'sourceCode', header: 'Source Code', width: '200px' },
          { field: 'suburb', header: 'Suburb', width: '200px' },
          { field: 'callAttempts', header: 'Call Attempts', width: '200px' },
          { field: 'siteType', header: 'Site Type', width: '200px' },
          { field: 'loadDate', header: 'Load Date', width: '200px' },
          { field: 'iconClass', header: 'Notes', width: '200px' },
          { field: 'status', header: 'Status', width: '200px' }],
          apiSuffixModel: InventoryModuleApiSuffixModels.ITEM_BRAND,
          moduleName: ModulesBasedApiSuffix.INVENTORY,
        },
      ]
    }
  }
  DATE_FORMAT_TYPES = DATE_FORMAT_TYPES;
  NO_DATA_FOUND_MESSAGE=NO_DATA_FOUND_MESSAGE;

  constructor(
    private crudService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    private snackbarService: SnackbarService,
    private router: Router,
    private store: Store<AppState>,
    private _fb: FormBuilder,
    private momentService: MomentService,
    private dialog: MatDialog, private rxjsService: RxjsService) {
    super();
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.breadCrumb = {
      items: [{ key: 'Sales' }, { key: 'Raw Leads Dashboard' }]
    };
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    this.getRequiredListData();
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingSiteTypesState$),
      this.store.select(selectStaticEagerLoadingRawLeadStatusState$),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.siteTypes = response[1];
      this.status = response[2];
      if (response[3][Sales_Leads_Components.RAW_LEAD]) {
        let prepareDynamicTableTabsFromPermissionsObj =
          prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, response[3][Sales_Leads_Components.RAW_LEAD]);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  loadPaginationLazy(event) {
    this.otherParams = {};
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onTabSelected(e): void {
    this.selectedTabIndex = e;
  }

  addNotes(data): void {
    this.dialog.open(AddRawLeadsNotesModalComponent, { width: '700px', disableClose: true, data })
  }

  changeStatus(data) {
    if (data.rawLeadStatusId == RawLeadStatuses.Address_Verification_Pending || data.rawLeadStatusId == RawLeadStatuses.Archive ||
      data.rawLeadStatusId == RawLeadStatuses.Lead_Created) {
      return;
    }
    else if (data.status) {
      const dialogReff = this.dialog.open(ChangeStatusModalComponent, { width: '700px', disableClose: true, data });
      dialogReff.afterClosed().subscribe(result => {
        if (result) return;
        this.getRequiredListData();
      });
    }
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.otherParams = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  onCRUDRequested(type: CrudType | string, row?: object): void {
    if (this.otherParams) {
      Object.keys(this.otherParams).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          this.otherParams[key] = this.momentService.localToUTC(this.otherParams[key]);
        } else {
          this.otherParams[key] = this.otherParams[key];
        }
      });
    }
    if (CrudType.GET === type && Object.keys(row).length > 0) {
      Object.keys(row['searchColumns']).forEach((key) => {
        if (key == 'status') {
          let status = row['searchColumns'][key]['value'];
          key = 'rawLeadStatusId';
          this.otherParams[key] = status.id;
        } else if (key == 'siteType') {
          let siteType = row['searchColumns'][key]['value'];
          key = 'siteTypeId';
          this.otherParams[key] = siteType.id;
        }
        else {
          this.otherParams[key] = row['searchColumns'][key]['value'];
        }
      });
      this.otherParams['sortOrder'] = row['sortOrder'];
      if (row['sortOrderColumn']) {
        this.otherParams['sortOrderColumn'] = row['sortOrderColumn'];
      }
    }
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], this.otherParams);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      case CrudType.ICON_POPUP:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        else {
          this.addNotes(row);
        }
        break;
      case CrudType.STATUS_POPUP:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        else {
          this.changeStatus(row);
        }
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    this.rxjsService.setFromUrl('Raw Leads List');
    switch (type) {
      case CrudType.CREATE:
        if (editableObject == 'upload') {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canUpload) {
            this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          else {
            this.router.navigateByUrl("/sales/raw-lead/upload");
          }
        }
        else {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
            this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          else {
            this.router.navigateByUrl("/sales/raw-lead/add-edit");
          }
        }
        break;
      case CrudType.EDIT:
        this.router.navigate(['sales/raw-lead/view'], { queryParams: { id: editableObject['rawLeadId'], leadStatus: editableObject['rawLeadStatusName'] } });
        break;
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_RAW_LEADS;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      salesModuleApiSuffixModels,
      undefined, false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          response.resources.forEach((resp) => {
            resp['iconClass'] = 'icon icon-inventory';
          });
          this.dataList = response.resources;
          this.totalRecords = response.totalCount;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  contactNumber(contact) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    else if (this.selectedTabIndex == 0) {
      if (!this.agentExtensionNo) {
        this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
        this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
      } else {
        let data = {
          customerContactNumber: contact.phoneNumber,
          rawLeadId: contact.rawLeadId,
          clientName: contact.client
        }
        this.rxjsService.setCustomerContactNumber(data);
        this.rxjsService.setExpandOpenScape(true);
      }
    }
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }
}






// import { Component } from '@angular/core';
// import { FormBuilder, FormGroup } from '@angular/forms';
// import { MatDialog } from '@angular/material';
// import { Router } from '@angular/router';
// import { RawLeadStatuses, SalesModuleApiSuffixModels, Sales_Leads_Components, selectStaticEagerLoadingRawLeadStatusState$, selectStaticEagerLoadingSiteTypesState$ } from '@app/modules';
// import { AppState } from "@app/reducers";
// import {
//   agentLoginDataSelector,
//   BreadCrumbModel, CommonPaginationConfig, createOrUpdateFilteredFieldKeyValues, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, DATE_FORMAT_TYPES, debounceTimeForSearchkeyword, ExtensionModalComponent, filterFormControlNamesWhichHasValues, findPropsDifference, HttpCancelService, IApplicationResponse,
//   IPatchTableFilteredFields,
//   LoggedInUserModel, ModulesBasedApiSuffix, OtherService, patchPersistedNgrxFieldValuesToForm, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
//   preparePTableLoadDataConfiguration,
//   ResponseMessageTypes,
//   RxjsService,
//   SnackbarService,
//   SplitSearchColumnFieldsFromObject,
//   tableFilteredDataSelector$
// } from '@app/shared';
// import { TableFilterFormService } from '@app/shared/services/create-form.services';
// import { MomentService } from '@app/shared/services/moment.service';
// import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
// import { loggedInUserData } from '@modules/others';
// import { select, Store } from '@ngrx/store';
// import { AddRawLeadsNotesModalComponent, ChangeStatusModalComponent } from '@sales/components/raw-lead';
// import { combineLatest, of } from 'rxjs';
// import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
// import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
// @Component({
//   selector: 'app-raw-lead-list',
//   templateUrl: './raw-lead-list.component.html',
//   styleUrls: ['./raw-lead.component.scss'],
// })

// export class RawLeadListComponent extends PrimeNgTableVariablesModel {
//   columnFilterForm: FormGroup;
//   row = {};
//   searchColumns;
//   agentExtensionNo;
//   searchForm: FormGroup;
//   breadCrumb: BreadCrumbModel;
//   siteTypes;
//   pageSize = CommonPaginationConfig.defaultPageSize;
//   primengTableConfigProperties: any = {
//     tableCaption: "Raw Leads",
//     breadCrumbItems: [{ displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Raw Lead Dashboard' }],
//     tableComponentConfigs: {
//       tabsList: [
//         {
//           caption: 'Raw Leads',
//           dataKey: 'rawLeadId',
//           enableAddActionBtn: true,
//           reorderableColumns: false,
//           resizableColumns: false,
//           enableFieldsSearch: true,
//           enableHyperLink: true,
//           cursorLinkIndex: 0,
//           columns: [{ field: 'rawLeadRefNo', header: 'Raw Lead ID', width: '200px' },
//           { field: 'client', header: 'Client Name', width: '200px' },
//           { field: 'email', header: 'Email Address', width: '200px' },
//           { field: 'phoneNumber', header: 'Phone Number', width: '200px' },
//           { field: 'sourceCode', header: 'Source Code', width: '200px' },
//           { field: 'suburb', header: 'Suburb', width: '200px' },
//           { field: 'callAttempts', header: 'Call Attempts', width: '200px' },
//           { field: 'siteType', header: 'Site Type', width: '200px' },
//           { field: 'loadDate', header: 'Load Date', width: '200px' },
//           { field: 'iconClass', header: 'Notes', width: '200px' },
//           { field: 'status', header: 'Status', width: '200px' }],
//           apiSuffixModel: InventoryModuleApiSuffixModels.ITEM_BRAND,
//           moduleName: ModulesBasedApiSuffix.INVENTORY,
//         },
//       ]
//     }
//   }
//   DATE_FORMAT_TYPES = DATE_FORMAT_TYPES;

//   constructor(
//     private crudService: CrudService,
//     private tableFilterFormService: TableFilterFormService,
//     private snackbarService: SnackbarService,
//     private router: Router, private otherService: OtherService,
//     private store: Store<AppState>,
//     private _fb: FormBuilder, private httpCancelService: HttpCancelService,
//     private momentService: MomentService,
//     private dialog: MatDialog, private rxjsService: RxjsService) {
//     super();
//     this.searchForm = this._fb.group({ searchKeyword: "" });
//     this.columnFilterForm = this._fb.group({});
//     this.breadCrumb = {
//       items: [{ key: 'Sales' }, { key: 'Raw Leads Dashboard' }]
//     };
//   }

//   ngOnInit(): void {
//     this.combineLatestNgrxStoreData();
//     this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
//     this.columnFilterRequest();
//     this.getRequiredListData();
//     this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
//       this.agentExtensionNo = extension;
//     });
//     // start logic table fields search persistant
//     combineLatest([
//       this.store.select(tableFilteredDataSelector$)
//     ]).pipe(take(1)).subscribe((observables) => {
//       if (!observables[0]) return;
//       this.tableFilteredFields = observables[0];
//       setTimeout(() => {
//         // Give some milliseconds to render with proper history of fields search values and tabl results
//         if (Object.keys(this.tableFilteredFields?.[this.otherService.currentUrl] ?
//           this.tableFilteredFields[this.otherService.currentUrl] : {}).length > 2) {
//           let { columnFilterForm, row }: IPatchTableFilteredFields = patchPersistedNgrxFieldValuesToForm(this.columnFilterForm, this.tableFilteredFields, this.row, this.otherService,
//             this.httpCancelService, this.areColumnFiltersEdited);
//           this.columnFilterForm = columnFilterForm;
//           this.row = row;
//           let keyValuesObj = filterFormControlNamesWhichHasValues({ ...this.columnFilterForm.value });
//           if (Object.keys(keyValuesObj).length > 0) {
//             this.onCRUDRequested(CrudType.GET, this.row, false);
//           }
//         }
//       });
//     });
//     // end logic table fields search persistant
//   }

//   combineLatestNgrxStoreData(): void {
//     combineLatest([
//       this.store.select(loggedInUserData),
//       this.store.select(selectStaticEagerLoadingSiteTypesState$),
//       this.store.select(selectStaticEagerLoadingRawLeadStatusState$),
//       this.store.select(currentComponentPageBasedPermissionsSelector$)]
//     ).subscribe((response) => {
//       this.loggedInUserData = new LoggedInUserModel(response[0]);
//       this.siteTypes = response[1]?.map(item => {
//         return { label: item.displayName, value: item.id };
//       });
//       this.status = response[2]?.map(item => {
//         return { label: item.displayName, value: item.id };
//       });
//       if (response[3][Sales_Leads_Components.RAW_LEAD]) {
//         let prepareDynamicTableTabsFromPermissionsObj =
//           prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, response[3][Sales_Leads_Components.RAW_LEAD]);
//         this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
//       }
//     });
//   }

//   loadPaginationLazy(event) {
//     // start logic table fields search persistant
//     this.row = preparePTableLoadDataConfiguration(event, this.searchColumns, this.tableFilteredFields, this.otherService, this.row);
//     this.onCRUDRequested(CrudType.GET, this.row);
//     // end logic table fields search persistant
//   }

//   addNotes(data): void {
//     this.dialog.open(AddRawLeadsNotesModalComponent, { width: '700px', disableClose: true, data })
//   }

//   changeStatus(data) {
//     if (data.rawLeadStatusId == RawLeadStatuses.Address_Verification_Pending || data.rawLeadStatusId == RawLeadStatuses.Archive ||
//       data.rawLeadStatusId == RawLeadStatuses.Lead_Created) {
//       return;
//     }
//     else if (data.status) {
//       const dialogReff = this.dialog.open(ChangeStatusModalComponent, { width: '700px', disableClose: true, data });
//       dialogReff.afterClosed().subscribe(result => {
//         if (result) return;
//         this.getRequiredListData();
//       });
//     }
//   }

//   columnFilterRequest() {
//     this.columnFilterForm.valueChanges
//       .pipe(
//         debounceTime(debounceTimeForSearchkeyword),
//         distinctUntilChanged(),
//         switchMap(obj => {
//           Object.keys(obj).forEach(key => {
//             if (obj[key] === "") {
//               delete obj[key]
//             }
//           });
//           this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
//           // start logic table fields search persistant
//           if (this.tableFilteredFields[this.otherService.currentUrl]) {
//             this.row = { ...this.tableFilteredFields[this.otherService.currentUrl] };
//             let sourceObj = SplitSearchColumnFieldsFromObject(this.tableFilteredFields[this.otherService.currentUrl]);
//             // Delete properties from the source object with comparing the destination object and return the source object
//             let filteredSearchColumns = findPropsDifference(sourceObj, this.searchColumns);
//             this.row['searchColumns'] = { ...filteredSearchColumns, ...this.searchColumns };
//           }
//           else {
//             this.row['searchColumns'] = this.searchColumns;
//             this.row['pageIndex'] = +CommonPaginationConfig.defaultPageIndex;
//             this.row['pageSize'] = +CommonPaginationConfig.defaultPageSize;
//           }
//           // set to true to make sure the column filters are changed by the user
//           this.areColumnFiltersEdited = true;
//           // end logic table fields search persistant
//           return of(this.onCRUDRequested(CrudType.GET, this.row));
//         })
//       )
//       .subscribe();
//   }

//   onCRUDRequested(type: CrudType | string, row?: object, shouldDispatchNgrxAction = true) {
//     switch (type) {
//       case CrudType.CREATE:
//         this.openAddEditPage(CrudType.CREATE, row);
//         break;
//       case CrudType.GET:
//         let otherParams = {};
//         if (Object.keys(this.row).length > 0) {
//           // logic for split columns and its values to key value pair
//           if (row['searchColumns']) {
//             Object.keys(row['searchColumns']).forEach((key) => {
//               if (key.toLowerCase().includes('date') && row['searchColumns'][key] instanceof Date) {
//                 otherParams[key] = this.momentService.localToUTCDateTime(row['searchColumns'][key]);
//               }
//               else if (key == 'status') {
//                 let value = row['searchColumns']?.[key]?.['id'] ?
//                   row['searchColumns'][key]['id'] : row['searchColumns'][key] ?
//                     row['searchColumns'][key] : row['searchColumns']['rawLeadStatusId'];
//                 otherParams['rawLeadStatusId'] = value;
//                 otherParams[key] = value;
//               } else if (key == 'siteType') {
//                 let value = row['searchColumns']?.[key]?.['id'] ?
//                   row['searchColumns'][key]['id'] : row['searchColumns'][key] ?
//                     row['searchColumns'][key] : row['searchColumns']['siteTypeId'];
//                 otherParams['siteTypeId'] = value;
//                 otherParams[key] = value;
//               }
//               else {
//                 otherParams[key] = row['searchColumns'][key];
//               }
//             });
//           }
//           if (row['sortOrderColumn']) {
//             otherParams['sortOrder'] = row['sortOrder'];
//             otherParams['sortOrderColumn'] = row['sortOrderColumn'];
//           }
//         }
//         if (!row['pageIndex'] && !row['pageSize']) {
//           row['pageIndex'] = CommonPaginationConfig.defaultPageIndex;
//           row['pageSize'] = CommonPaginationConfig.defaultPageSize;
//         }
//         // start logic table fields search persistant
//         if ((row['pageIndex'] || row['pageIndex'] == 0) && row['pageSize']) {
//           otherParams['pageIndex'] = row['pageIndex'];
//           otherParams['pageSize'] = row['pageSize'];
//         }
//         // start logic table fields search persistant
//         createOrUpdateFilteredFieldKeyValues(this.tableFilteredFields, [], otherParams, this.areColumnFiltersEdited, this.otherService, this.store, shouldDispatchNgrxAction);
//         //row = {...otherParams};
//         if ((otherParams['pageIndex'] || otherParams['pageIndex'] == 0) && otherParams['pageSize']) {
//           row['pageIndex'] = otherParams['pageIndex'];
//           row['pageSize'] = otherParams['pageSize'];
//           delete otherParams['pageIndex'];
//           delete otherParams['pageSize'];
//         }
//         if (row['searchColumns']) {
//           otherParams = { ...otherParams, ...row['searchColumns'] };
//         }
//         setTimeout(() => {
//           this.first = parseInt(row['pageIndex']) * parseInt(row['pageSize']);
//           this.pageSize = row['pageSize'];
//           this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams);
//         });
//         // end logic table fields search persistant
//         break;
//       case CrudType.EDIT:
//         this.openAddEditPage(CrudType.EDIT, row);
//         break;
//       case CrudType.ICON_POPUP:
//         if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
//           this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
//         }
//         else {
//           this.addNotes(row);
//         }
//         break;
//       case CrudType.STATUS_POPUP:
//         if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
//           this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
//         }
//         else {
//           this.changeStatus(row);
//         }
//         break;
//     }
//   }

//   openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
//     this.rxjsService.setFromUrl('Raw Leads List');
//     switch (type) {
//       case CrudType.CREATE:
//         if (editableObject == 'upload') {
//           if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canUpload) {
//             this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
//           }
//           else {
//             this.router.navigateByUrl("/sales/raw-lead/upload");
//           }
//         }
//         else {
//           if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
//             this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
//           }
//           else {
//             this.router.navigateByUrl("/sales/raw-lead/add-edit");
//           }
//         }
//         break;
//       case CrudType.EDIT:
//         this.router.navigate(['sales/raw-lead/view'], { queryParams: { id: editableObject['rawLeadId'], leadStatus: editableObject['rawLeadStatusName'] } });
//         break;
//     }
//   }

//   getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
//     let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
//     salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_RAW_LEADS;
//     this.crudService.get(
//       ModulesBasedApiSuffix.SALES,
//       salesModuleApiSuffixModels,
//       undefined, false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
//         if (response.isSuccess && response.statusCode === 200 && response.resources) {
//           response.resources.forEach((resp) => {
//             resp['iconClass'] = 'icon icon-inventory';
//           });
//           this.dataList = response.resources;
//           this.totalRecords = response.totalCount;
//         }
//         this.rxjsService.setGlobalLoaderProperty(false);
//       });
//   }

//   contactNumber(contact) {
//     if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
//       this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
//     }
//     else if (this.selectedTabIndex == 0) {
//       if (!this.agentExtensionNo) {
//         this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
//         this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
//       } else {
//         let data = {
//           customerContactNumber: contact.phoneNumber,
//           rawLeadId: contact.rawLeadId,
//           clientName: contact.client
//         }
//         this.rxjsService.setCustomerContactNumber(data);
//         this.rxjsService.setExpandOpenScape(true);
//       }
//     }
//   }

//   onBreadCrumbClick(breadCrumbItem: object): void {
//     if (breadCrumbItem.hasOwnProperty('queryParams')) {
//       this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
//         { queryParams: breadCrumbItem['queryParams'] })
//     }
//     else {
//       this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
//     }
//   }
// }