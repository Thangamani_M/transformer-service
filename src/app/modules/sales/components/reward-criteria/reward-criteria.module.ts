import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { RewardCriteriaComponent, RewardCriteriaItemInfoComponent, RewardCriteriaRoutingModule, RewardCriteriaServiceInfoComponent, RewardCriteriaViewComponent } from '@sales/components/reward-criteria';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
@NgModule({
  declarations: [RewardCriteriaComponent, RewardCriteriaItemInfoComponent, RewardCriteriaServiceInfoComponent, RewardCriteriaViewComponent],
  imports: [
    CommonModule, MaterialModule, SharedModule,
    ReactiveFormsModule, FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    RewardCriteriaRoutingModule,
  ],
  exports: [],
  entryComponents: []

})
export class RewardCriteriaModule { }
