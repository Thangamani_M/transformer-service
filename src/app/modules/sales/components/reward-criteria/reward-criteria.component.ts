import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams } from '@app/shared/utils';
import { SalesModuleApiSuffixModels, Sales_Leads_Components } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-reward-criteria',
  templateUrl: './reward-criteria.component.html'
})
export class RewardCriteriaComponent extends PrimeNgTableVariablesModel implements OnInit {
  rewardPartnershipDetails;
  primengTableConfigProperties: any = {
    tableCaption: "Reward Partnerships",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Reward Partenerships' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Reward Partnerships',
          dataKey: 'rewardPartnerId',
          enableBreadCrumb: true,
          enableAction: false,
          enableAddActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'rewardPartnershipRefNo', header: 'Partnership ID' },
            { field: 'rewardPartnershipName', header: 'Reward Partnership Name' },
            { field: 'rewardPartnershipCompanyName', header: 'Reward Partnership Company Name' },
            {
              field: 'isPreferenceRateApplicable', header: 'Preference Rate', type: 'dropdown', placeholder: 'Select Preference Rate',
              options: [{ label: 'Yes', value: true }, { label: 'No', value: false }]
            },
            {
              field: 'isComplimentaryMonths', header: 'Complementary Service', type: 'dropdown', placeholder: 'Select Preference Rate',
              options: [{ label: 'Yes', value: true }, { label: 'No', value: false }]
            },
            {
              field: 'isInstallationDiscount', header: 'Installation Discount', type: 'dropdown', placeholder: 'Select Is Installation Discount',
              options: [{ label: 'Yes', value: true }, { label: 'No', value: false }]
            },
            { field: 'startDate', header: 'Start Date', isDate: true },
            { field: 'endDate', header: 'End Date', isDate: true },
            { field: 'isActive', header: 'Status' }
          ],
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_REWARD_PARTNERSHIPS,
          moduleName: ModulesBasedApiSuffix.SALES,
        }
      ]
    }
  }

  constructor(
    private rxjsService: RxjsService, private store: Store<AppState>, private snackbarService: SnackbarService,
    private httpService: CrudService, private router: Router) {
    super();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRewardCriterias();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      if (response[0][Sales_Leads_Components.REWARD_PARTNERSHIP]) {
        let prepareDynamicTableTabsFromPermissionsObj =
          prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, response[0][Sales_Leads_Components.REWARD_PARTNERSHIP]);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRewardCriterias(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.httpService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_REWARD_PARTNERSHIPS, undefined, false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          response.resources?.rewardPartnerships.forEach((rewardPartnership) => {
            rewardPartnership.isPreferenceRateApplicable = rewardPartnership.isPreferenceRateApplicable == true ? 'Yes' : 'No';
            rewardPartnership.isComplimentaryMonths = rewardPartnership.isComplimentaryMonths == true ? 'Yes' : 'No';
            rewardPartnership.isInstallationDiscount = rewardPartnership.isInstallationDiscount == true ? 'Yes' : 'No';
          });
          this.rewardPartnershipDetails = response.resources;
          this.dataList = response.resources.rewardPartnerships;
          this.totalRecords = response.resources.totalCount;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRewardCriterias(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        else {
          this.router.navigate(["/sales/reward-partnerships/view"]);
        }
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        else {
          this.router.navigate(["/sales/reward-partnerships/view"], { queryParams: { id: editableObject['rewardPartnershipId'] } });
        }
        break;
    }
  }
}
