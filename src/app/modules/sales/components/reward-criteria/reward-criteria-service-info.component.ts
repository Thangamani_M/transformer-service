import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, ReusablePrimeNGTableFeatureService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { addFormControls, clearFormControlValidators, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControlError, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { RewardPartnershipModel, RewardPartnershipServiceInfoModel, RewardPartnershipServicesModel } from '@modules/sales/models/reward-criteria';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-reward-criteria-service-info',
  templateUrl: './reward-criteria-service-info.component.html'
})
export class RewardCriteriaServiceInfoComponent implements OnInit {
  rewardData;
  formConfigs = formConfigs;
  rewardPartnershipId = "";
  rewardPartnerShipServiceInfoForm: FormGroup;
  rewardPartnershipServices: FormArray;
  rewardDataModel: RewardPartnershipModel;
  loggedInUserData: LoggedInUserModel;
  serviceCategories = [];
  @ViewChildren('input') rows: QueryList<any>;
  serviceCategoryId = "";
  disabled: boolean = false;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  servicesBasedOnServiceCategoryId = {};

  constructor(
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private router: Router, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private store: Store<AppState>) {
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.activatedRoute.queryParams]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.rewardPartnershipId = response[1]?.id;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.forkJoinRequests();
    this.createRewardCriteriaServiceForm();
    this.onFormControlChanges();
  }

  getServicesById(serviceCategoryId): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_SERVICES, undefined, false, prepareGetRequestHttpParams(null, null, { serviceCategoryId }));
  }

  getServiceDetailsById(id): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_SERVICE, id, false, null);
  }

  onChangeServiceCategory(serviceCategoryId) {
    this.serviceCategoryId = serviceCategoryId;
    this.getServicesById(serviceCategoryId).subscribe((response: IApplicationResponse) => {
      if (response.resources && response.isSuccess && response.statusCode == 200) {
        this.servicesBasedOnServiceCategoryId[this.serviceCategoryId] = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onChangeService(serviceId, i) {
    this.getServiceDetailsById(serviceId).subscribe((response: IApplicationResponse) => {
      if (response.resources && response.isSuccess && response.statusCode == 200) {
        this.getServiceListItemsArray.controls[i].get("serviceRefNo").setValue(response.resources.serviceNumber);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onChangeDiscountFee() {
    if (this.rewardPartnerShipServiceInfoForm.get('isDiscountOnMonthlyFee').value) {
      this.rewardPartnershipServices.clear();
      this.rewardPartnershipServices = null;
    } else {
      if (this.rewardPartnershipServices && this.rewardPartnershipServices.value.length > 0) {
      } else {
        this.rewardPartnershipServices = this.getServiceListItemsArray;
        this.rewardPartnershipServices.push(this.createServiceItemList());
        this.getServiceListItemsArray.controls[0].get("discountAmount").setValidators([Validators.required]);
        this.getServiceListItemsArray.controls[0].get("discountAmount").updateValueAndValidity();
      }
    }
  }

  onDiscountPercentageChanges() {
    this.rewardPartnerShipServiceInfoForm.get('serviceDiscountPercentage').valueChanges.subscribe((levyPercentage: string) => {
      if (levyPercentage == '0' || levyPercentage == '00' || levyPercentage == '0.0' ||
        levyPercentage == '0.00' || levyPercentage == '00.0' || levyPercentage == '00.00' ||
        levyPercentage == '00.' || levyPercentage == '0.' || levyPercentage == '100' || levyPercentage.includes('99.')) {
        this.rewardPartnerShipServiceInfoForm.get('serviceDiscountPercentage').setErrors({ 'invalid': true });
      } else {
        this.rewardPartnerShipServiceInfoForm = removeFormControlError(this.rewardPartnerShipServiceInfoForm as FormGroup, 'invalid');
      }
    });
  }

  onPercentageChanges(i, str) {
    if (str == 'amt') {
      this.getServiceListItemsArray.controls[i].get('discountAmount').valueChanges.subscribe((levyPercentage: string) => {
        if (levyPercentage == '0' || levyPercentage == '00' || levyPercentage == '0.0' ||
          levyPercentage == '0.00' || levyPercentage == '00.0' || levyPercentage == '00.00' ||
          levyPercentage == '00.' || levyPercentage == '0.') {
          this.getServiceListItemsArray.controls[i].get('discountAmount').setErrors({ 'invalid': true });
        }
        else {
          this.getServiceListItemsArray.controls[i] = removeFormControlError(this.getServiceListItemsArray.controls[i] as FormGroup, 'invalid');
        }
      });
    } else {
      this.getServiceListItemsArray.controls[i].get('discountPercentage').valueChanges.subscribe((levyPercentage: string) => {
        if (levyPercentage == '0' || levyPercentage == '00' || levyPercentage == '0.0' ||
          levyPercentage == '0.00' || levyPercentage == '00.0' || levyPercentage == '00.00' ||
          levyPercentage == '00.' || levyPercentage == '0.' || levyPercentage == '100' || levyPercentage.includes('99.')) {
          this.getServiceListItemsArray.controls[i].get('discountPercentage').setErrors({ 'invalid': true });
        }
        else {
          this.getServiceListItemsArray.controls[i] = removeFormControlError(this.getServiceListItemsArray.controls[i] as FormGroup, 'invalid');
        }
      });
    }
  }

  onFormControlChanges(): void {
    this.rewardPartnerShipServiceInfoForm.get('isDiscountOnMonthlyFee').valueChanges.subscribe((serv: boolean) => {
      this.disabled = serv;
      if (!this.disabled) {
        this.rewardPartnerShipServiceInfoForm.get('isFixedServiceDiscountPercentage').setValue(false);
      } else {
        this.rewardPartnerShipServiceInfoForm = addFormControls(this.rewardPartnerShipServiceInfoForm, [{ controlName: "isFixedServiceDiscountPercentage" }, { controlName: "serviceDiscountAmount" }, { controlName: "serviceDiscountPercentage" }]);
        this.rewardPartnerShipServiceInfoForm.get('isFixedServiceDiscountPercentage').setValue(false);
      }
    });
    this.rewardPartnerShipServiceInfoForm.get('isFixedServiceDiscountPercentage').valueChanges.subscribe((serv: boolean) => {
      if (!serv) {
      } else {
        this.rewardPartnerShipServiceInfoForm = addFormControls(this.rewardPartnerShipServiceInfoForm, [{ controlName: "serviceDiscountAmount" }, { controlName: "serviceDiscountPercentage" }]);
      }
    });
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  addServiceInfoItems(): void {
    if (this.getServiceListItemsArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.rewardPartnershipServices = this.getServiceListItemsArray;
    let rewardPartnershipServicesModel = new RewardPartnershipServicesModel();
    this.rewardPartnershipServices.insert(0, this.createServiceItemList(rewardPartnershipServicesModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.getServiceListItemsArray.controls[0].get("discountAmount").setValidators([Validators.required]);
    this.getServiceListItemsArray.controls[0].get("discountAmount").updateValueAndValidity();
  }

  getRewardCriteriaById(): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_REWARD_PARTNERSHIPS,
      this.rewardPartnershipId, false, null);
  }

  createServiceItemList(serviceListModel?: RewardPartnershipServicesModel): FormGroup {
    let rewardPartnershipServicesModel = new RewardPartnershipServicesModel(serviceListModel);
    let formControls = {};
    rewardPartnershipServicesModel.rewardPartnershipId = this.rewardPartnershipId;
    rewardPartnershipServicesModel.createdUserId = this.loggedInUserData.userId;
    Object.keys(rewardPartnershipServicesModel).forEach((key) => {
      formControls[key] = [{ value: rewardPartnershipServicesModel[key], disabled: false }, (key !== 'serviceRefNo') ? [Validators.required] : []]
      if (key == 'discountPercentage' || key == 'discountAmount' || key == 'rewardPartnershipId' ||
        key == 'rewardPartnershipServiceId' || key == 'serviceName') {
        formControls[key] = [{ value: rewardPartnershipServicesModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  get getServiceListItemsArray(): FormArray {
    if (!this.rewardPartnerShipServiceInfoForm) return;
    return this.rewardPartnerShipServiceInfoForm.get("rewardPartnershipServices") as FormArray;
  }

  removServiceListItem(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getServiceListItemsArray.controls[i].value.rewardPartnershipServiceId) {
            this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_REWARD_PARTNERSHIP_SERVICES, undefined,
              prepareRequiredHttpParams({
                modifiedUserId: this.loggedInUserData.userId,
                ids: this.getServiceListItemsArray.controls[i].value.rewardPartnershipServiceId,
                isDeleted: true
              })).subscribe((res) => {
                if (res.isSuccess && res.statusCode == 200) {
                  this.getServiceListItemsArray.removeAt(i);
                }
                if (this.getServiceListItemsArray.length === 0) {
                  this.addServiceInfoItems();
                };
              });
          } else {
            this.getServiceListItemsArray.removeAt(i);
          }
        }
      });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  changePercentage(index) {
    if (this.getServiceListItemsArray.controls[index].get("isPercentage").value == false) {
      this.getServiceListItemsArray.controls[index].get("discountAmount").setValidators([Validators.required]);
      this.getServiceListItemsArray.controls[index].get("discountAmount").updateValueAndValidity();
      this.getServiceListItemsArray.controls[index] = clearFormControlValidators((this.getServiceListItemsArray.controls[index] as FormGroup), ['discountPercentage']);
    } else {
      this.getServiceListItemsArray.controls[index].get("discountPercentage").setValidators([Validators.required]);
      this.getServiceListItemsArray.controls[index].get("discountPercentage").updateValueAndValidity();
      this.getServiceListItemsArray.controls[index] = clearFormControlValidators((this.getServiceListItemsArray.controls[index] as FormGroup), ['discountAmount']);
    }
  }

  createRewardCriteriaServiceForm() {
    let rewardPartnershipServiceInfoModel = new RewardPartnershipServiceInfoModel();
    this.rewardPartnerShipServiceInfoForm = this.formBuilder.group({
      rewardPartnershipServices: this.formBuilder.array([])
    });
    Object.keys(rewardPartnershipServiceInfoModel).forEach((key) => {
      if (key == 'serviceDiscountAmount') {
        this.rewardPartnerShipServiceInfoForm.addControl(key, new FormControl(rewardPartnershipServiceInfoModel[key]));
      } else if (key == 'serviceDiscountPercentage') {
        this.rewardPartnerShipServiceInfoForm.addControl(key, new FormControl(rewardPartnershipServiceInfoModel[key]));
      }
      else if (key == 'isPreferenceRateApplicable') {
        this.rewardPartnerShipServiceInfoForm.addControl(key, new FormControl(rewardPartnershipServiceInfoModel[key]));
      }
      else {
        this.rewardPartnerShipServiceInfoForm.addControl(key, new FormControl(rewardPartnershipServiceInfoModel[key]));
      }
    });
    this.rewardPartnerShipServiceInfoForm.get('modifiedUserId').setValue(this.loggedInUserData.userId);
    this.rewardPartnerShipServiceInfoForm.get('createdUserId').setValue(this.loggedInUserData.userId);
    this.rewardPartnerShipServiceInfoForm = setRequiredValidator(this.rewardPartnerShipServiceInfoForm, ["isDiscountOnMonthlyFee", "serviceDiscountPercentage", "serviceDiscountAmount"]);
  }

  getRewardServicesById(): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_REWARD_PARTNERSHIP_SERVICES,
      this.rewardPartnershipId, false, null);
  }

  forkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SERVICE_CATEGORY, undefined, true, null),
      this.getRewardCriteriaById(), this.getRewardServicesById()])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp.resources && resp.isSuccess && resp.statusCode === 200) {
            switch (ix) {
              case 0:
                this.serviceCategories = resp.resources;
                break;
              case 1:
                this.rewardData = resp.resources;
                break;
              case 2:
                this.rewardPartnerShipServiceInfoForm.patchValue(resp.resources);
                if (!this.rewardPartnerShipServiceInfoForm.get('isFixedServiceDiscountPercentage').value) {
                  this.rewardPartnerShipServiceInfoForm.get('isFixedServiceDiscountPercentage').setValue(false);
                }
                if (!this.rewardPartnerShipServiceInfoForm.get('isDiscountOnMonthlyFee').value) {
                  this.rewardPartnerShipServiceInfoForm.get('isDiscountOnMonthlyFee').setValue(false);
                } else {
                  this.rewardPartnerShipServiceInfoForm.get('isFixedServiceDiscountPercentage').setValue(resp.resources.isFixedServiceDiscountPercentage);
                  this.rewardPartnerShipServiceInfoForm.get('serviceDiscountAmount').setValue(resp.resources.serviceDiscountAmount);
                  this.rewardPartnerShipServiceInfoForm.get('serviceDiscountPercentage').setValue(resp.resources.serviceDiscountPercentage);
                }
                if (!this.rewardPartnerShipServiceInfoForm.get('isPreferenceRateApplicable').value) {
                  this.rewardPartnerShipServiceInfoForm.get('isPreferenceRateApplicable').setValue(false);
                }
                if (!this.rewardPartnerShipServiceInfoForm.get('isSpecificServices').value) {
                  this.rewardPartnerShipServiceInfoForm.get('isSpecificServices').setValue(false);
                }
                if (!this.rewardPartnerShipServiceInfoForm.get('serviceDiscountAmount').value) {
                  this.rewardPartnerShipServiceInfoForm.get('serviceDiscountAmount').setValue("");
                }
                if (!this.rewardPartnerShipServiceInfoForm.get('serviceDiscountPercentage').value) {
                  this.rewardPartnerShipServiceInfoForm.get('serviceDiscountPercentage').setValue("");
                }
                this.rewardPartnershipServices = this.getServiceListItemsArray;
                if (resp.resources.rewardPartnershipServices.length > 0) {
                  resp.resources.rewardPartnershipServices.forEach((serviceListModel) => {
                    this.rewardPartnershipServices.push(this.createServiceItemList(serviceListModel));
                  });
                  this.rewardPartnershipServices.value.forEach((element, index) => {
                    this.getServicesById(element.serviceCategoryId).subscribe((response: IApplicationResponse) => {
                      if (response.resources && response.isSuccess && response.statusCode == 200) {
                        this.servicesBasedOnServiceCategoryId[element.serviceCategoryId] = response.resources;
                      }
                      if (index == this.rewardPartnershipServices.length - 1) {
                        this.rxjsService.setGlobalLoaderProperty(false);
                      }
                    });
                  });
                  this.onChangeDiscountFee();
                } else {
                  this.rewardPartnershipServices.push(this.createServiceItemList());
                  this.onChangeDiscountFee();
                  this.rxjsService.setGlobalLoaderProperty(false);
                }
                break;
            }
          }
        });
      });
  }

  submitted: boolean = false;
  onSubmit() {
    this.rewardPartnerShipServiceInfoForm.get("isSpecificServices").setValue((this.rewardPartnerShipServiceInfoForm.value.isDiscountOnMonthlyFee) ? false : true);
    if (this.rewardPartnerShipServiceInfoForm.value.isSpecificServices) {
      this.rewardPartnerShipServiceInfoForm = clearFormControlValidators(this.rewardPartnerShipServiceInfoForm, ['serviceDiscountPercentage', 'serviceDiscountAmount']);
    } else if (!this.rewardPartnerShipServiceInfoForm.value.isSpecificServices) {
      if (this.rewardPartnerShipServiceInfoForm.value.isFixedServiceDiscountPercentage) {
        this.rewardPartnerShipServiceInfoForm.get("serviceDiscountPercentage").setValidators([Validators.required]);
        this.rewardPartnerShipServiceInfoForm.get("serviceDiscountPercentage").updateValueAndValidity();
        this.rewardPartnerShipServiceInfoForm = clearFormControlValidators(this.rewardPartnerShipServiceInfoForm, ['serviceDiscountAmount']);
      } else {
        this.rewardPartnerShipServiceInfoForm.get("serviceDiscountAmount").setValidators([Validators.required]);
        this.rewardPartnerShipServiceInfoForm.get("serviceDiscountAmount").updateValueAndValidity();
        this.rewardPartnerShipServiceInfoForm = clearFormControlValidators(this.rewardPartnerShipServiceInfoForm, ['serviceDiscountPercentage']);
      }
    }
    if (this.rewardPartnerShipServiceInfoForm.invalid) {
      return;
    }
    let payload = this.rewardPartnerShipServiceInfoForm.value;
    payload.isSpecificServices = payload.isDiscountOnMonthlyFee ? false : true;
    if (payload.rewardPartnershipServices.length == 0) {
      payload.rewardPartnershipServices = null;
    } else {
      payload.serviceDiscountAmount = ""
    }
    if (payload.isSpecificServices) {
      payload.isFixedServiceDiscountPercentage = false;
      payload.serviceDiscountPercentage = null;
      payload.serviceDiscountAmount = null;
    } else if (!payload.isSpecificServices) {
      if (payload.isFixedServiceDiscountPercentage) {
        payload.serviceDiscountAmount = null;
      } else {
        payload.serviceDiscountPercentage = null;
      }
    }
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_REWARD_PARTNERSHIP_SERVICES, payload)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200 && resp.resources) {
          if (resp.resources.rewardPartnershipServices) {
            resp.resources.rewardPartnershipServices.forEach((rewardPartnershipService, index: number) => {
              if (!this.rewardPartnershipServices.controls[index].get('rewardPartnershipServiceId').value) {
                this.rewardPartnershipServices.controls[index].get('rewardPartnershipServiceId').setValue(rewardPartnershipService.rewardPartnershipServiceId);
              }
            });
          }
          this.submitted = true;
        }
      });
  }

  onArrowClick(type: string) {
    if (type == 'back') {
      this.router.navigate(["/sales/reward-partnerships/view"], { queryParams: { id: this.rewardData['rewardPartnershipId'] } });
    }
    else {
      this.router.navigate(["/sales/reward-partnerships/item-info"], { queryParams: { id: this.rewardData['rewardPartnershipId'] } });
    }
  }

  cancel() {
    this.router.navigate(["/sales/reward-partnerships"]);
  }
}
