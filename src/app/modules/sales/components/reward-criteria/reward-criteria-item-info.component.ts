import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, ReusablePrimeNGTableFeatureService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { addFormControls, addNullOrStringDefaultFormControlValue, clearFormControlValidators, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControls } from '@app/shared/utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { RewardPartnershipItemInfoModel, RewardPartnershipItemServicesModel } from '@modules/sales/models/reward-criteria';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-reward-criteria-item-info',
  templateUrl: './reward-criteria-item-info.component.html'
})
export class RewardCriteriaItemInfoComponent implements OnInit {
  rewardData;
  formConfigs = formConfigs;
  rewardPartnershipId = "";
  rewardPartnerShipItemInfoForm: FormGroup;
  installationDiscountItems: FormArray;
  @ViewChildren('input') rows: QueryList<any>;
  serviceCategoryId = "";
  serviceDetails;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  servicesBasedOnServiceCategoryId = {};
  installationDiscountItemsArray = [];
  loggedInUserData: LoggedInUserModel;
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private formBuilder: FormBuilder,
    private router: Router,
    private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createRewardCriteriaItemForm();
    this.getRewardItemById().subscribe((response: IApplicationResponse) => {
      this.installationDiscountItemsArray = response.resources.installationDiscountItems;
      this.rewardPartnerShipItemInfoForm.patchValue(response.resources);
      this.installationDiscountItems = this.getServiceListItemsArray;
      if (response.resources.installationDiscountItems.length > 0) {
        response.resources.installationDiscountItems.forEach((serviceListModel) => {
          this.installationDiscountItems.push(this.createItemList(serviceListModel));
        });
        this.addFormControlValueChangesToFormArrayControls();
      } else {
        this.installationDiscountItems.push(this.createItemList());
      }
      if (this.rewardPartnerShipItemInfoForm.get('isFixedItemDiscountPercentage').value == false) {
        this.rewardPartnerShipItemInfoForm.get('isFixedItemDiscountPercentage').setValue(false);
      }
      if (this.rewardPartnerShipItemInfoForm.get('isOverAllItemDiscount').value == false) {
        this.rewardPartnerShipItemInfoForm.get('isOverAllItemDiscount').setValue(false);
      }
      if (this.rewardPartnerShipItemInfoForm.get('isSpecialDiscount').value == false) {
        this.rewardPartnerShipItemInfoForm.get('isSpecialDiscount').setValue(false);
      }
      if (!this.rewardPartnerShipItemInfoForm.get('itemDiscountAmount').value) {
        this.rewardPartnerShipItemInfoForm.get('itemDiscountAmount').setValue('');
      }
      if (!this.rewardPartnerShipItemInfoForm.get('itemDiscountPercentage').value) {
        this.rewardPartnerShipItemInfoForm.get('itemDiscountPercentage').setValue('');
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    this.getRewardCriteriaById().subscribe((response: IApplicationResponse) => {
      this.rewardData = response.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.activatedRoute.queryParams]).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.rewardPartnershipId = response[1]?.id;
      });
  }

  createRewardCriteriaItemForm() {
    let rewardPartnershipItemInfoModel = new RewardPartnershipItemInfoModel();
    this.rewardPartnerShipItemInfoForm = this.formBuilder.group({
      installationDiscountItems: this.formBuilder.array([])
    });
    Object.keys(rewardPartnershipItemInfoModel).forEach((key) => {
      this.rewardPartnerShipItemInfoForm.addControl(key, new FormControl(rewardPartnershipItemInfoModel[key]));
    });
    this.rewardPartnerShipItemInfoForm.get('createdUserId').setValue(this.loggedInUserData.userId);
  }

  getServicesById(): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_SERVICES,
      undefined, false,
      prepareGetRequestHttpParams(null, null, { serviceCategoryId: this.serviceCategoryId }));
  }

  getServiceDetailsById(id): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_SERVICE, id, false, null);
  }

  onChangeServiceCategory(serviceCategoryId) {
    this.serviceCategoryId = serviceCategoryId;
    this.getServicesById().subscribe((response: IApplicationResponse) => {
      this.servicesBasedOnServiceCategoryId[this.serviceCategoryId] = response.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onChangeService(serviceId, i) {
    this.getServiceDetailsById(serviceId).subscribe((response: IApplicationResponse) => {
      if (response.resources && response.statusCode == 200 && response.isSuccess) {
        this.serviceDetails = response.resources;
        this.getServiceListItemsArray.controls[i].get("serviceNumber").setValue(this.serviceDetails.serviceNumber);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onFormControlChanges(): void {
    this.rewardPartnerShipItemInfoForm.get('isFixedItemDiscountPercentage').valueChanges.subscribe((serv: boolean) => {
      if (!serv) {
      } else {
        this.rewardPartnerShipItemInfoForm = addFormControls(this.rewardPartnerShipItemInfoForm, [{ controlName: "itemDiscountAmount" }, { controlName: "itemDiscountPercentage" }]);
      }
    });
    this.rewardPartnerShipItemInfoForm.get('isOverAllItemDiscount').valueChanges.subscribe((serv: boolean) => {
      if (!serv) {
        this.rewardPartnerShipItemInfoForm.get('isFixedItemDiscountPercentage').setValue(false);
      } else {
        if (this.rewardPartnerShipItemInfoForm.get('isSpecialDiscount').value == false) {
          if (this.installationDiscountItems) {
            this.installationDiscountItemsArray = null;
            this.installationDiscountItems.clear();
            this.installationDiscountItems = null;
          }
        }
        this.rewardPartnerShipItemInfoForm = addFormControls(this.rewardPartnerShipItemInfoForm, [{ controlName: "isFixedItemDiscountPercentage" }, { controlName: "itemDiscountAmount" }, { controlName: "itemDiscountPercentage" }]);
      }
    });
    this.rewardPartnerShipItemInfoForm.get('isSpecialDiscount').valueChanges.subscribe((isSpecialDiscount: boolean) => {
      if (!isSpecialDiscount) {
        if (!this.installationDiscountItems) {
        } else {
          if (!this.installationDiscountItems) {
          } else {
            this.installationDiscountItemsArray = null;
            this.installationDiscountItems.clear();
            this.installationDiscountItems = null;
          }
        }
      } else {
        if (this.installationDiscountItemsArray && this.installationDiscountItemsArray.length > 0) {
        } else {
          this.installationDiscountItems = this.getServiceListItemsArray;
          this.installationDiscountItems.push(this.createItemList());
          this.addFormControlValueChangesToFormArrayControls();
        }
      }
    });
  }

  onChangeDiscountFee() {
    if (!this.rewardPartnerShipItemInfoForm.get('isOverAllItemDiscount').value) {
      this.rewardPartnerShipItemInfoForm = addNullOrStringDefaultFormControlValue(this.rewardPartnerShipItemInfoForm, [{ controlName: 'itemDiscountAmount', defaultValue: '' }, { controlName: 'itemDiscountPercentage', defaultValue: '' }]);
    }
  }

  selectedOptions: any = [];
  onSelectedItemOption(selectedObject, index: number): void {
    if (!selectedObject) return;
    this.getServiceListItemsArray.controls[index].get("stockName").setValue(selectedObject.displayName);
    this.getServiceListItemsArray.controls[index].get("itemId").setValue(selectedObject.id);
  }

  clearInitiatorFormControlValues(index: number) {
    this.getServiceListItemsArray.controls[index].get("stockName").setValue("");
    this.getServiceListItemsArray.controls[index].get("itemId").setValue("");
  }

  redirect(str) {
    if (str == 'backward') {
      this.router.navigate(["/sales/reward-partnerships/service-info"], { queryParams: { id: this.rewardData['rewardPartnershipId'] } });
    } else if (this.rewardPartnerShipItemInfoForm.get('rewardPartnershipId').value) {
      this.router.navigate(["/sales/reward-partnerships/item-info"], { queryParams: { id: this.rewardData['rewardPartnershipId'] } });
    }
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  submitted: boolean = false;
  addNewItem() {
    this.submitted = true;
    if (this.getServiceListItemsArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.installationDiscountItems = this.getServiceListItemsArray;
    let rewardPartnershipItemServicesModel = new RewardPartnershipItemServicesModel();
    this.installationDiscountItems.insert(0, this.createItemList(rewardPartnershipItemServicesModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.addFormControlValueChangesToFormArrayControls();
  }

  addFormControlValueChangesToFormArrayControls() {
    this.installationDiscountItems.controls.forEach((getServiceListItemObj, index: number) => {
      this.installationDiscountItems.controls[index].get('stockCode').valueChanges.subscribe((itemCode: string) => {
        if (!itemCode) {
          this.getServiceListItemsArray.controls[index].get("stockName").setValue("");
          this.getServiceListItemsArray.controls[index].get("itemId").setValue("");
        }
        else {
          this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM, undefined, true, prepareGetRequestHttpParams(
            null, null, { itemCode }));
        }
      });
    });
  }

  getRewardCriteriaById(): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_REWARD_PARTNERSHIPS, this.rewardPartnershipId, false, null);
  }

  createItemList(serviceListModel?: RewardPartnershipItemServicesModel): FormGroup {
    let rewardPartnershipItemServicesModel = new RewardPartnershipItemServicesModel(serviceListModel);
    let formControls = {};
    rewardPartnershipItemServicesModel.createdUserId = this.loggedInUserData.userId;
    Object.keys(rewardPartnershipItemServicesModel).forEach((key) => {
      formControls[key] = [{ value: rewardPartnershipItemServicesModel[key], disabled: false }, key !== 'stockName' ? [Validators.required] : []]
      if (key == 'rewardPartnershipItemId') {
        formControls[key] = [{ value: rewardPartnershipItemServicesModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  get getServiceListItemsArray(): FormArray {
    if (!this.rewardPartnerShipItemInfoForm) return;
    return this.rewardPartnerShipItemInfoForm.get("installationDiscountItems") as FormArray;
  }

  removeItem(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getServiceListItemsArray.controls[i].value.rewardPartnershipItemId != '') {
            this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_REAWARD_ITEM_INFO, undefined,
              prepareRequiredHttpParams({
                ids: this.getServiceListItemsArray.controls[i].value.rewardPartnershipItemId,
                isDeleted: true,
                modifiedUserId: this.loggedInUserData.userId
              }), 1).subscribe((res) => {
                if (res.isSuccess) {
                  this.getServiceListItemsArray.removeAt(i);
                }
                if (this.getServiceListItemsArray.length === 0) {
                  this.addNewItem();
                };
              });
          } else {
            this.getServiceListItemsArray.removeAt(i);
            this.rxjsService.setFormChangeDetectionProperty(true);
          }
        }
      });
  }

  changePercentage() {
    this.rewardPartnerShipItemInfoForm.get('itemDiscountAmount').setValue(null);
    this.rewardPartnerShipItemInfoForm.get('itemDiscountPercentage').setValue(null);
    this.rewardPartnerShipItemInfoForm.get('itemDiscountAmount').setErrors({ 'invalid': false });
    this.rewardPartnerShipItemInfoForm.get('itemDiscountAmount').markAsUntouched();
    this.rewardPartnerShipItemInfoForm.get('itemDiscountPercentage').setErrors({ 'invalid': false });
    this.rewardPartnerShipItemInfoForm.get('itemDiscountPercentage').markAsUntouched();
    this.rewardPartnerShipItemInfoForm = clearFormControlValidators(this.rewardPartnerShipItemInfoForm, ['itemDiscountPercentage', 'itemDiscountAmount']);
  }

  getRewardItemById(): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_REAWARD_ITEM_INFO, this.rewardPartnershipId, false, null);
  }

  onSubmit() {
    if (this.rewardPartnerShipItemInfoForm.value.isOverAllItemDiscount) {
      if (this.rewardPartnerShipItemInfoForm.value.isFixedItemDiscountPercentage) {
        this.rewardPartnerShipItemInfoForm.get("itemDiscountPercentage").setValidators([Validators.required]);
        this.rewardPartnerShipItemInfoForm.get("itemDiscountPercentage").updateValueAndValidity();
        this.rewardPartnerShipItemInfoForm = clearFormControlValidators(this.rewardPartnerShipItemInfoForm, ['itemDiscountAmount']);
      } else {
        this.rewardPartnerShipItemInfoForm.get("itemDiscountAmount").setValidators([Validators.required]);
        this.rewardPartnerShipItemInfoForm.get("itemDiscountAmount").updateValueAndValidity();
        this.rewardPartnerShipItemInfoForm = clearFormControlValidators(this.rewardPartnerShipItemInfoForm, ['itemDiscountPercentage']);
      }
    } else {
      this.rewardPartnerShipItemInfoForm = clearFormControlValidators(this.rewardPartnerShipItemInfoForm, ['itemDiscountAmount', 'itemDiscountPercentage']);
      this.rewardPartnerShipItemInfoForm = removeFormControls(this.rewardPartnerShipItemInfoForm, ["itemDiscountAmount", "itemDiscountPercentage"]);
    }
    if (this.rewardPartnerShipItemInfoForm.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    }
    let payload = this.rewardPartnerShipItemInfoForm.getRawValue();
    payload.itemDiscountAmount = payload.itemDiscountAmount ? payload.itemDiscountAmount : 0;
    payload.itemDiscountPercentage = payload.itemDiscountPercentage ? payload.itemDiscountPercentage : 0;
    if (!payload.isSpecialDiscount) {
      payload.installationDiscountItems = null;
    }
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_REAWARD_ITEM_INFO, payload)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200 && resp.resources) {
          if (!this.rewardPartnerShipItemInfoForm.value.rewardPartnershipId) {
            this.rewardPartnerShipItemInfoForm.get('rewardPartnershipId').setValue(resp.resources);
          }
        }
      });
  }

  cancel() {
    this.router.navigate(["/sales/reward-partnerships"]);
  }
}
