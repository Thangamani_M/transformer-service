import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RewardCriteriaComponent, RewardCriteriaServiceInfoComponent, RewardCriteriaViewComponent } from '@sales/components/reward-criteria';
import { RewardCriteriaItemInfoComponent } from './reward-criteria-item-info.component';
import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: RewardCriteriaComponent, data: { title: 'Reward Criteria' }, canActivate: [AuthGuard] },
    { path: 'view', component: RewardCriteriaViewComponent, data: { title: 'Reward Criteria View' }, canActivate: [AuthGuard] },
    { path: 'service-info', component: RewardCriteriaServiceInfoComponent, data: { title: 'Reward Criteria Add edit' }, canActivate: [AuthGuard] },
    { path: 'item-info', component: RewardCriteriaItemInfoComponent, data: { title: 'Reward Criteria Add edit' }, canActivate: [AuthGuard] },
];
@NgModule({
    imports: [RouterModule.forChild(routes)]
})

export class RewardCriteriaRoutingModule { }
