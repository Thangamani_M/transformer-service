import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService } from '@app/shared/services';
import { disableFormControls, disableWithDefaultFormControlValues, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import {
  loggedInUserData, selectStaticEagerLoadingAnnualNetworkFeePeriodsState$, selectStaticEagerLoadingComplimentaryMonthsState$,
  selectStaticEagerLoadingFixedContractFeePeriodsState$
} from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { RewardPartnershipModel } from '@modules/sales/models/reward-criteria';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-reward-criteria-view',
  templateUrl: './reward-criteria-view.component.html'
})
export class RewardCriteriaViewComponent implements OnInit {
  rewardData;
  minDate = new Date();
  minDateEqualsOrGreaterStartDate = new Date();
  rewardPartnershipId = "";
  rewardPartnerShipForm: FormGroup;
  rewardDataModel: RewardPartnershipModel;
  complementaryMonths = [];
  fixedContractFeePeriods = [];
  annualNetworkFeePeriods = [];
  loggedInUserData: LoggedInUserModel;
  submitted: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private router: Router, private datePipe: DatePipe,
    private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.forkJoinRequests();
    this.createRewardPartnershipForm();
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(selectStaticEagerLoadingFixedContractFeePeriodsState$),
      this.store.select(selectStaticEagerLoadingAnnualNetworkFeePeriodsState$),
      this.store.select(selectStaticEagerLoadingComplimentaryMonthsState$),
      this.store.select(loggedInUserData),
      this.activatedRoute.queryParams]
    ).pipe(take(1)).subscribe((response) => {
      this.fixedContractFeePeriods = response[0];
      this.annualNetworkFeePeriods = response[1];
      this.complementaryMonths = response[2];
      this.loggedInUserData = new LoggedInUserModel(response[3]);
      this.rewardPartnershipId = response[4]?.id;
    });
  }

  onFormControlChanges() {
    this.rewardPartnerShipForm.get('isComplimentaryMonths').valueChanges.subscribe((isComplimentaryMonths: boolean) => {
      if (isComplimentaryMonths) {
        this.rewardPartnerShipForm.get('complimentaryMonthId').enable();
      } else {
        this.rewardPartnerShipForm = disableWithDefaultFormControlValues(this.rewardPartnerShipForm, [{ formControlName: 'complimentaryMonthId', value: null }]);
      }
    });
    this.rewardPartnerShipForm.get('isWaiverAnnualNetworkFee').valueChanges.subscribe((isWaiverAnnualNetworkFee: boolean) => {
      if (isWaiverAnnualNetworkFee) {
        this.rewardPartnerShipForm.get('annualNetworkFeePeriodId').enable();
      } else {
        this.rewardPartnerShipForm = disableWithDefaultFormControlValues(this.rewardPartnerShipForm, [{ formControlName: 'annualNetworkFeePeriodId', value: null }]);
      }
    });
    this.rewardPartnerShipForm.get('isFixedContractFee').valueChanges.subscribe((isFixedContractFee: boolean) => {
      if (isFixedContractFee) {
        this.rewardPartnerShipForm.get('fixedContractFeePeriodId').enable();
      } else {
        this.rewardPartnerShipForm = disableWithDefaultFormControlValues(this.rewardPartnerShipForm, [{ formControlName: 'fixedContractFeePeriodId', value: null }]);
      }
    });
  }

  onDateChange() {
    this.minDateEqualsOrGreaterStartDate = new Date(this.rewardPartnerShipForm.controls.startDate.value);
  }

  createRewardPartnershipForm() {
    this.rewardDataModel = new RewardPartnershipModel();
    this.rewardPartnerShipForm = this.formBuilder.group({});
    Object.keys(this.rewardDataModel).forEach((key) => {
      this.rewardPartnerShipForm.addControl(key, new FormControl(this.rewardDataModel[key]));
      if (key == 'complimentaryMonthId' || key == 'fixedContractFeePeriodId' || key == 'annualNetworkFeePeriodId') {
        this.rewardPartnerShipForm.addControl(key, new FormControl({ value: this.rewardDataModel[key], disabled: true }));
      }
    });
    this.rewardPartnerShipForm.controls.createdUserId.setValue(this.loggedInUserData.userId)
    this.rewardPartnerShipForm = setRequiredValidator(this.rewardPartnerShipForm, ["rewardPartnershipName", "rewardPartnershipCompanyName", "endDate", "startDate", "complimentaryMonthId", "annualNetworkFeePeriodId", "fixedContractFeePeriodId"]);
    this.rewardPartnerShipForm = disableFormControls(this.rewardPartnerShipForm, ['complimentaryMonthId', 'fixedContractFeePeriodId', 'annualNetworkFeePeriodId']);
  }

  getRewardCriteriaById() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_REWARD_PARTNERSHIPS,
      this.rewardPartnershipId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.rewardData = response.resources;
          this.rewardData['startDate'] = new Date(this.rewardData['startDate']);
          this.rewardData['endDate'] = new Date(this.rewardData['endDate']);
          this.rewardPartnerShipForm.patchValue(this.rewardData);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  forkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ANNUAL_COMPLI_PERIODS, undefined, true, null),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_FIXED_PERIODS, undefined, true, null),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ANNUAL_PERIODS, undefined, true, null)]
    ).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.complementaryMonths = resp.resources;
              break;
            case 1:
              this.fixedContractFeePeriods = resp.resources;
              break;
            case 2:
              this.annualNetworkFeePeriods = resp.resources;
              break;
          }
          if (ix == response.length - 1) {
            if (this.rewardPartnershipId) {
              this.getRewardCriteriaById();
            }
            else {
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          }
        }
      });
    });
  }

  submit() {
    if (this.rewardPartnerShipForm.invalid) {
      return;
    }
    this.rewardPartnerShipForm.value.startDate = this.datePipe.transform(this.rewardPartnerShipForm.value.startDate, 'yyyy-MM-dd');
    this.rewardPartnerShipForm.value.endDate = this.datePipe.transform(this.rewardPartnerShipForm.value.endDate, 'yyyy-MM-dd');
    this.crudService.create(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_REWARD_PARTNERSHIPS, this.rewardPartnerShipForm.value)
      .subscribe((response: IApplicationResponse) => {
        this.submitted = true;
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.rewardPartnerShipForm.get('rewardPartnershipId').setValue(response.resources);
          this.rewardPartnershipId = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onArrowClick() {
    if (this.rewardPartnerShipForm.get('rewardPartnershipId').value) {
      this.router.navigate(["/sales/reward-partnerships/service-info"], { queryParams: { id: this.rewardPartnershipId } });
    }
  }

  cancel() {
    this.router.navigate(["/sales/reward-partnerships"]);
  }
}
