import { Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { LeadNotesModel } from '@modules/sales/models/lead-notes';
import { Store } from '@ngrx/store';
import { SalesModuleApiSuffixModels } from '@sales/shared';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'lead-notes-modal-component',
  templateUrl: './lead-notes-modal.component.html',
  styleUrls: ['./lead-notes.scss'],
})
export class LeadNoteModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  leadId = "";
  leadNoteForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  fileList: File[] = [];
  buttonDisable = false;
  @ViewChild('fileInput', null) myFileInputField: ElementRef;

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService, private crudService: CrudService,
    private dialog: MatDialog, private store: Store<AppState>) {
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.combineLatestNgrxStoreData();
    this.createLeadNoteForm();
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.leadId = response[1]?.leadId;
    });
  }

  createLeadNoteForm(): void {
    let emailNotificationModel = new LeadNotesModel();
    this.leadNoteForm = this.formBuilder.group({
    });
    Object.keys(emailNotificationModel).forEach((key) => {
      this.leadNoteForm.addControl(key, new FormControl(emailNotificationModel[key]));
    });
    this.leadNoteForm = setRequiredValidator(this.leadNoteForm, ["comments"]);
  }

  emitUploadedFiles(uploadedFiles: any[]) {
    this.fileList = uploadedFiles;
  }

  onSubmit(): void {
    if (this.leadNoteForm.invalid) {
      this.leadNoteForm.markAllAsTouched();
      return;
    }
    this.buttonDisable = true;
    var formValue = this.leadNoteForm.value;
    formValue.leadId = this.data.leadId;
    formValue.createdUserId = this.loggedInUserData.userId;
    let formData = new FormData();
    formData.append('comment', JSON.stringify(formValue));
    this.fileList.forEach(file => {
      formData.append('file', file);
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEAD_NOTES, formData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.rxjsService.setLeadNotesUpdateStatus(true);
          this.rxjsService.setDialogOpenProperty(false);
          this.dialog.closeAll();
          this.outputData.emit(true);
          this.buttonDisable = false;
        }
      });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
