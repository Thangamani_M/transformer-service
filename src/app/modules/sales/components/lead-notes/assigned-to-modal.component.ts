import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareAutoCompleteListFormatForMultiSelection, prepareGetRequestHttpParams, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { AssignToModel } from '@modules/sales/models/assign-to-model';
import { Store } from '@ngrx/store';
import { SalesModuleApiSuffixModels } from '@sales/shared';
import { UserModuleApiSuffixModels } from '@user/shared';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-assigned-to-modal-component',
  templateUrl: './assigned-to-modal.component.html',
  styleUrls: ['./lead-notes.scss'],
})
export class AssignedToModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  userGroups = [];
  assignedToList = [];
  assignToForm: FormGroup;
  leadId = '';
  loggedInUserData: LoggedInUserModel;

  constructor(private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data, private store: Store<AppState>,
    private rxjsService: RxjsService, private crudService: CrudService, private formBuilder: FormBuilder,
    private httpCancelService: HttpCancelService, private activatedRoute: ActivatedRoute) {
  }
  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.combineLatestNgrxStoreData();
    this.getUserGroup();
    this.createAssignToManualAddForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.leadId = response[1]['leadId'];
    });
  }

  getUserGroup() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_GROUPS, undefined, true).subscribe((response) => {
      if (response.statusCode == 200 && response.isSuccess && response.resources) {
        this.userGroups = prepareAutoCompleteListFormatForMultiSelection(response.resources);
      }
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  onUserGroupChange(groupId) {
    if (groupId?.length == 0) {
      return;
    }
    this.assignToForm.get('groupId').setValue(groupId);
    this.assignedToList = [];
    this.assignToForm.get('assignedTo').setValue("");
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_USERS, prepareGetRequestHttpParams(null, null, { groupId })).subscribe((response) => {
      if (response.statusCode == 200 && response.isSuccess && response.resources) {
        this.assignedToList = response.resources;
      }
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  createAssignToManualAddForm(): void {
    let assignToAddModel = new AssignToModel();
    this.assignToForm = this.formBuilder.group({});
    Object.keys(assignToAddModel).forEach((key) => {
      this.assignToForm.addControl(key, new FormControl(assignToAddModel[key]));
    });
    this.assignToForm = setRequiredValidator(this.assignToForm, ["groupId", "assignedTo"]);
  }

  onSubmit() {
    this.assignToForm.get('groupId').markAllAsTouched();
    this.assignToForm.get('groupId').markAsTouched();
    this.assignToForm.get('groupId').markAsDirty();
    this.assignToForm.get('groupId').updateValueAndValidity();
    this.assignToForm.updateValueAndValidity();
    if (this.assignToForm.invalid) {
      return;
    }
    this.assignToForm.value.leadId = this.data.leadId;
    this.assignToForm.value.createdUserId = this.loggedInUserData.userId;
    this.assignToForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEAD_ASSIGNED_USER, this.assignToForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.rxjsService.setLeadNotesUpdateStatus(true);
          this.outputData.emit(true);
          this.dialog.closeAll();
        }
      });
  }

  cancel() {
    this.outputData.emit(false);
    this.dialog.closeAll();
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
