import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  prepareAutoCompleteListFormatForMultiSelection, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService,
  setRequiredValidator
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { UpgradeModel } from '@modules/customer/shared/utils/customer-module.models';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
@Component({
  selector: 'app-partition-search-modal',
  templateUrl: './customer-upgrade-modal.component.html',
  styleUrls: ['./lead-notes.scss']
})

export class CustomerUpgradeModalComponent implements OnInit {
  customerUpgradeForm: FormGroup;
  customerAddressList = [];
  activeSubscriptionData: any = [];
  installedSystem = [];
  loggedInUserData: LoggedInUserModel;
  leadCategories = [];
  sourceTypes = [];

  constructor(@Inject(MAT_DIALOG_DATA) public upgradeModel: UpgradeModel, private router: Router, private dialog: MatDialog,
    private crudservice: CrudService, private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private httpCancelService: HttpCancelService, private store: Store<AppState>,) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getForkJoinRequests();
    this.createUpgradeModal();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getForkJoinRequests(): void {
    let currentAddressRequestObservable = this.upgradeModel.fromComponent == 'customer detail' ? this.crudservice.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      SalesModuleApiSuffixModels.CURRENT_ADDRESS_INFO, undefined, false, prepareRequiredHttpParams({
        customerId: this.upgradeModel.customerId
      })) :
      this.crudservice.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        SalesModuleApiSuffixModels.CURRENT_ADDRESS_INFO_UPGRADE, undefined, false, prepareRequiredHttpParams({
          customerId: this.upgradeModel.customerId, callInitiationId: this.upgradeModel.callInitiationId
        }));
    forkJoin([
      this.crudservice.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SOURCE_TYPES, undefined, true, null),
      this.crudservice.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_LEAD_CATEGORY, undefined, false, undefined),
      currentAddressRequestObservable]
    ).subscribe((response: IApplicationResponse[]) => {
      response.forEach((respObj: IApplicationResponse, ix: number) => {
        if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
          switch (ix) {
            case 0:
              this.sourceTypes = respObj.resources;
              break;
            case 1:
              this.leadCategories = prepareAutoCompleteListFormatForMultiSelection(respObj.resources, true);
              break;
            case 2:
              if (respObj.resources.length > 0) {
                this.customerAddressList = respObj.resources;
                let addressId = this.upgradeModel.addressId; //set the address which exists as deafult
                this.customerUpgradeForm.get('addressId').setValue(addressId);
                if (this.customerAddressList[0]?.leadCategories) {
                  let array = this.customerAddressList[0]?.leadCategories.split(",");
                  this.customerUpgradeForm.get('leadCategoryId').setValue(array);
                }
                if (this.customerAddressList[0]?.sourceId) {
                  this.customerUpgradeForm.get('sourceId').setValue(this.customerAddressList[0]?.sourceId);
                }
                this.activeSubscriptionData = this.customerAddressList[0]?.activeSubscriptions;
                this.installedSystem = this.customerAddressList[0]?.installedSystems;
                this.customerUpgradeForm.get('titleId').setValue(this.customerAddressList[0]?.titleId);
                this.customerUpgradeForm.get('firstName').setValue(this.customerAddressList[0]?.firstName);
                this.customerUpgradeForm.get('lastName').setValue(this.customerAddressList[0]?.lastName);
              }
          }
        }
        if (response.length - 1 == ix) {
          this.rxjsService.setPopupLoaderProperty(false);
        }
      });
    });
  }

  createUpgradeModal() {
    let upgradeModel = new UpgradeModel(this.upgradeModel);
    this.customerUpgradeForm = this.formBuilder.group({});
    Object.keys(upgradeModel).forEach((key) => {
      this.customerUpgradeForm.addControl(key, new FormControl(upgradeModel[key]));
    });
    this.customerUpgradeForm = setRequiredValidator(this.customerUpgradeForm, ["addressId", "leadCategoryId", "sourceId", "notes"]);
  }

  onSubmit() {
    if (this.customerUpgradeForm.invalid) {
      this.customerUpgradeForm.markAllAsTouched();
      return;
    }
    this.customerUpgradeForm.value.createdUserId = this.loggedInUserData.userId;
    this.customerUpgradeForm.value.customerId = this.upgradeModel.customerId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudservice.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.LEAD_UPGRADE, this.customerUpgradeForm.value)
      .subscribe({
        next: response => {
          if (response.isSuccess && response.statusCode == 200 && response.resources) {
            this.redirectToLeads(response.resources);
            this.dialog.closeAll();
          }
          this.rxjsService.setDialogOpenProperty(false);
        },
      });
  }

  redirectToLeads(leadId) {
    this.router.navigate(['/sales/task-management/leads/lead-info/view'], { queryParams: { leadId: leadId, selectedIndex: 1 } });
  }

  selectedAddress() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    let found = this.customerAddressList.find(e => e.addressId == this.upgradeModel.addressId);
    this.activeSubscriptionData = found?.activeSubscriptions;
    this.installedSystem = found?.installedSystems;
    if (found.leadCategories) {
      found.leadCategoryId = found.leadCategories.split(",");
    }
    found.leadCategoryId.forEach((element, i) => {
      let x = parseInt(element);
      found.leadCategoryId[i] = x;
    });
    this.customerUpgradeForm.patchValue(found);
  }

  getActiveSubscription() {
    this.crudservice.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.LEAD_UPGRADE, undefined, false,
      prepareGetRequestHttpParams('0', '100', {
        customerId: this.upgradeModel.customerId,
        addressId: this.upgradeModel.addressId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.activeSubscriptionData = response.resources.subscription;
          this.installedSystem = this.activeSubscriptionData.installedSystem;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
