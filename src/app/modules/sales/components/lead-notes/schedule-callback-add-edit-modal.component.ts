import { Component, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { addFormControls, countryCodes, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, removeFormControls, setRequiredValidator, SharedModuleApiSuffixModels } from '@app/shared/utils';
import { MediaTypes } from '@modules/my-tasks/components/email-dashboard/media-type.enums';
import { loggedInUserData } from '@modules/others';
import { ScheduleCallBackAddEditModel } from '@modules/sales/models/lead-sms-notification.model';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-schedule-callback-add-edit',
  templateUrl: './schedule-callback-add-edit-modal.component.html',
  styleUrls: ['./lead-notes.scss'],
})
export class ScheduleCallbackAddEditModalComponent implements OnInit {
  scheduleCallbackForm: FormGroup;
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  loggedInUserData: LoggedInUserModel;
  ticketReferences = [];
  customerData: any = [];
  todayDate = new Date();
  @Output() outputData = new EventEmitter<any>();
  queues = [];
  agentExtensionNo = '';
  @ViewChild("input", { static: false }) row;

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public scheduleCallBackAddEditModel: ScheduleCallBackAddEditModel,
    private crudService: CrudService, private dialog: MatDialog,
    private store: Store<AppState>, private rxjsService: RxjsService) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.getCustomerDate()
      .subscribe(data => {
        this.customerData = data
        if (this.scheduleCallBackAddEditModel.isCustomer) {
          this.rxjsService.setLeadNotesUpdateStatus(false);
          if (data?.customerId) {
            this.getTicketReferences(data.customerId);
          }
        }
      });
    this.createScheduleAddForm();
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = response[0];
    });
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
  }

  onFormControlChanges(): void {
    this.scheduleCallbackForm.get('isSelf').valueChanges.subscribe((isSelf: boolean) => {
      if (isSelf) {
        this.scheduleCallbackForm = removeFormControls(this.scheduleCallbackForm, ['queueId']);
      }
      else {
        this.getQueues();
        this.scheduleCallbackForm = addFormControls(this.scheduleCallbackForm, [{
          controlName: 'queueId', defaultValue: ''
        }]);
        this.scheduleCallbackForm = setRequiredValidator(this.scheduleCallbackForm, ['queueId']);
      }
    });
    this.scheduleCallbackForm
      .get("contactNumberCountryCode")
      .valueChanges.subscribe((contactNumberCountryCode: string) => {
        this.setPhoneNumberLengthByCountryCode(contactNumberCountryCode);
        setTimeout(() => {
          this.row.nativeElement.focus();
          this.row.nativeElement.blur();
        });
      });
    this.scheduleCallbackForm
      .get("contactNumber")
      .valueChanges.subscribe((mobileNumber2: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.scheduleCallbackForm.get("contactNumberCountryCode").value
        );
      });
  }

  getQueues() {
    this.crudService.get(
      ModulesBasedApiSuffix.SHARED,
      SharedModuleApiSuffixModels.UX_QUEUES,
      undefined,
      false,
      prepareGetRequestHttpParams(null, null, { mediaTypeId: MediaTypes.CALLBACK }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.queues = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createScheduleAddForm(): void {
    this.scheduleCallBackAddEditModel.ticketId = null;
    let scheduleCallBackAddEditModel = new ScheduleCallBackAddEditModel(this.scheduleCallBackAddEditModel);
    this.scheduleCallbackForm = this.formBuilder.group({});
    Object.keys(scheduleCallBackAddEditModel).forEach((key) => {
      this.scheduleCallbackForm.addControl(key, new FormControl(scheduleCallBackAddEditModel[key]));
    });
    this.scheduleCallbackForm = setRequiredValidator(this.scheduleCallbackForm, ["callbackDateTime", "contactNumber",
      "isSelf"]);
  }

  setPhoneNumberLengthByCountryCode(contactNumberCountryCode: string) {
    switch (contactNumberCountryCode) {
      case "+27":
        this.scheduleCallbackForm.get('contactNumber').setValidators([Validators.required, Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        break;
      default:
        this.scheduleCallbackForm.get('contactNumber').setValidators([Validators.required, Validators.minLength(formConfigs.indianContactNumberMaxLength),
        Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        break;
    }
  }

  getTicketReferences(id) {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKETS_CUSTOMER, null, null, prepareGetRequestHttpParams(null, null, { customerId: id }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.ticketReferences = response.resources;
        }
      });
  }

  onSubmit(): void {
    if (this.scheduleCallbackForm.invalid) {
      return;
    }
    let payload = this.scheduleCallbackForm.value;
    payload.contactNumber =
      payload.contactNumber.replace(/[a-z]/gi, '').replace(/ /g, '');
    payload.createdUserId = this.loggedInUserData.userId;
    var date = new Date(payload.callbackDateTime);
    var dateStr =
      ("00" + (date.getMonth() + 1)).slice(-2) + "/" +
      ("00" + date.getDate()).slice(-2) + "/" +
      date.getFullYear() + " " +
      ("00" + date.getHours()).slice(-2) + ":" +
      ("00" + date.getMinutes()).slice(-2) + ":" +
      ("00" + date.getSeconds()).slice(-2);
    payload.callbackDateTime = dateStr;
    payload.queueId = payload?.queueId?.id;
    if (payload.ticketId && payload.ticketId == 'null') {
      payload.ticketId = null;
    }
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.SALES,
      this.scheduleCallBackAddEditModel.isCustomer ?
        SalesModuleApiSuffixModels.CUSTOMER_CALLBACK : SalesModuleApiSuffixModels.SALES_API_LEAD_CALLBACK,
      payload)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setLeadNotesUpdateStatus(true);
        this.outputData.emit(true);
        this.dialog.closeAll();
      }
    });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
