import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { CrudService, RxjsService } from '@app/shared';
import { ModulesBasedApiSuffix } from '@app/shared/utils/enums.utils';
import { TelephonyApiSuffixModules } from '@modules/sales/shared/utils/telephony.module.enum';
@Component({
  selector: 'callback-customer-list',
  templateUrl: './callback-customers-list-popup.component.html',
  styleUrls: ['./lead-notes.scss'],
})
export class callBackCustomerListModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  customerData: any = [];
  selectedCustomerAddressDetails;
  customerAddressList = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private crudService: CrudService,
    private router: Router, private dialog: MatDialog, private rxjsService: RxjsService) {
  }

  ngOnInit() {
    this.customerData = this.data;
  }

  customerDetails(selectedCustomerAddressDetails) {
    this.selectedCustomerAddressDetails = selectedCustomerAddressDetails;
    this.customerAddressList = selectedCustomerAddressDetails.address;
  }

  redirectToCustomerPage(customerObj) {
    customerObj.key = 'openscape module';    // key to differentiate openscape related logics wherever the observers are subscribed
    this.rxjsService.setAnyPropertyValue(customerObj);
    let payload = { customerId: '', uniqueCallId: '', modifiedUserId: '' }
    payload.customerId = customerObj.CustomerId;
    payload.uniqueCallId = this.customerData.UniqueCallid;
    payload.modifiedUserId = this.customerData.UserId;
    this.router.navigate(["/customer/manage-customers/customer-verification"], {
      queryParams: {
        customerId: customerObj.CustomerId,
        siteAddressId: customerObj?.address[0]?.AddressId
      }
    });
    this.outputData.emit(true);
    this.dialog.closeAll();
    this.crudService.update(ModulesBasedApiSuffix.TELEPHONY, TelephonyApiSuffixModules.AGENT_UPDATE_CUSTOMER, payload).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
      }
    });
  }
}
