import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import {
  convertTwelevHoursToTwentyFourHours, countryCodes, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  prepareRequiredHttpParams,
  removeProperyFromAnObjectWithDeepCopy,
  setRequiredValidator
} from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { selectLeadCreationUserDataState$, selectLeadHeaderDataState$ } from '@modules/sales/components/task-management/lead-creation-ngrx-files/lead-creation.selectors';
import { BookAppointmentModel } from '@modules/sales/models/book-appointment-model';
import { LeadHeaderData } from '@modules/sales/models/lead-creation-stepper.model';
import { Store } from '@ngrx/store';
import { AppointmentStatus, LeadOutcomeStatusNames, SalesModuleApiSuffixModels } from '@sales/shared';
import { combineLatest, Observable } from 'rxjs';
import { AppointmentEscalationPopupComponent } from './appointment-escalation/appointment-escalation-pop-up.component';
@Component({
  selector: 'app-book-appoinment-modal-component',
  templateUrl: './book-appoinment-modal.component.html',
  styleUrls: ['./lead-notes.scss'],
})
export class BookAppoinmentModalComponent implements OnInit {
  bookAppointmentForm: FormGroup;
  tempTimeslots = [];
  timeslots = [];
  countryCodes = countryCodes;
  todayDate = new Date();
  loggedInUserData: LoggedInUserModel;
  caption = 'Book Appointment';
  shouldDisableAllFields = false;
  resultedTimeSlotDetailInitialState = {
    sellerName: '', sellerAppointmentDuration: '', availableTime: '', contactNumber: '',
    email: '', sellerSlotId: null
  };
  resultedTimeSlotDetail = {
    sellerName: '', sellerAppointmentDuration: '', availableTime: '', contactNumber: '', email: '',
    sellerSlotId: null
  };
  alternateSellers;
  alternateSellerTime;
  selectedSellerDetails = {};
  isArrayUnShifted = false;
  isArrayShifted = false;
  leadHeaderData: LeadHeaderData;
  @Output() outputData = new EventEmitter();
  isAtleastOneSellerAvailableTimeEqualsUserTime = true;
  yearRange = '';

  constructor(private dialog: MatDialog, private store: Store<AppState>, private datePipe: DatePipe,
    private rxjsService: RxjsService, private crudService: CrudService, private formBuilder: FormBuilder,
    private httpCancelService: HttpCancelService,
    @Inject(MAT_DIALOG_DATA) public data, private snackbarService: SnackbarService) {
    let date = new Date().getFullYear();
    this.yearRange = `${date}:${date + 1}`;
    this.rxjsService.setDialogOpenProperty(true);
    if (this.data && this.data.fromUrl && this.data.fromUrl == 'Appointment History') {
      if (this.data.appointmentStatusName == AppointmentStatus.BOOKED) {
        this.caption = 'ReSchedule Appointment';
      }
      else if (this.data.appointmentStatusName !== AppointmentStatus.BOOKED) {
        this.caption = 'View Booked Appointment';
      }
      this.getAppointmentDetailById();
    }
    this.combineLatestNgrxStoreData();
  }

  ngOnInit() {
    this.createBookAppointmentManualAddForm();
    this.bookAppointmentForm.get('appointmentDate').valueChanges.subscribe(() => {
      this.bookAppointmentForm.get('sellerSlotId').setValue(null);
      this.bookAppointmentForm.get('isAlternateSeller').setValue(false);
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectLeadCreationUserDataState$),
      this.store.select(selectLeadHeaderDataState$)
    ]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      if (response[1]?.['leadStatusName'] == LeadOutcomeStatusNames.NO_SALE ||
        response[1]['leadStatusName'] == LeadOutcomeStatusNames.SALE_COMPLETED || (this.data && this.data.appointmentStatusName !== AppointmentStatus.BOOKED)) {
        this.shouldDisableAllFields = true;
      }
      this.leadHeaderData = new LeadHeaderData(response[2]);
    });
  }

  onFormControlValueChanges(type: string) {
    switch (type) {
      case "appointmentDate":
        if (this.bookAppointmentForm.get('appointmentDate').value) {
          this.getSellerNames();
        }
        break;
      case "isAlternateSeller":
        if (this.bookAppointmentForm.get(type).value) {
          this.alternateSellers = [];
          this.timeslots = this.tempTimeslots;
          this.resultedTimeSlotDetail = {
            availableTime: "", contactNumber: "", email: "", sellerAppointmentDuration: "",
            sellerName: "", sellerSlotId: ""
          };
          this.bookAppointmentForm.get('sellerSlotId').setValue(null, { emitEvent: false, onlySelf: true });
          this.bookAppointmentForm.get('alternateSellerTime').setValue(null);
          this.bookAppointmentForm = setRequiredValidator(this.bookAppointmentForm, ['sellerSlotId']);
          if (this.bookAppointmentForm.get('alternateSellerTime').value) {
            this.setSelectedSellerDetails(this.timeslots[0]);
          }
        }
        else {
          this.getSellerNames();
          this.setSelectedSellerDetails(this.timeslots[0]);
          this.bookAppointmentForm.get('sellerSlotId').clearValidators();
          this.bookAppointmentForm.get('sellerSlotId').updateValueAndValidity();
        }
        break;
      case "alternateSellerTime":
        if (this.bookAppointmentForm.get('alternateSellerTime').value) {
          this.getSellerNames();
        }
    }
  }

  getAppointmentDetailById(): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.APPOINTMENTS, this.data.appointmentId).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.selectedSellerDetails = response.resources;
        let bookAppointmentModel: BookAppointmentModel = new BookAppointmentModel(response.resources);
        if (bookAppointmentModel.isAlternateSeller) {
          let alternateSellerDateTime = new Date();
          let [hours, minutes, seconds] = bookAppointmentModel.alternateSellerTime.split(':');
          alternateSellerDateTime.setHours(+hours);
          alternateSellerDateTime.setMinutes(+minutes);
          alternateSellerDateTime.setSeconds(+seconds);
          bookAppointmentModel.alternateSellerTime = alternateSellerDateTime;
        }
        let bookAppointmentModelWithoutAppointmentDate = removeProperyFromAnObjectWithDeepCopy(bookAppointmentModel, ["appointmentDate"]);
        this.bookAppointmentForm.patchValue(bookAppointmentModelWithoutAppointmentDate);
        this.bookAppointmentForm.get('appointmentDate').setValue(new Date(bookAppointmentModel.appointmentDate));
        if (bookAppointmentModel.appointmentDate && bookAppointmentModel.alternateSellerTime) {
          this.onFormControlValueChanges('alternateSellerTime');
        }
        else if (bookAppointmentModel.appointmentDate) {
          this.onFormControlValueChanges('appointmentDate')
        }
      }
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  createBookAppointmentManualAddForm(): void {
    let bookAppointmentModel = new BookAppointmentModel();
    this.bookAppointmentForm = this.formBuilder.group({});
    Object.keys(bookAppointmentModel).forEach((key) => {
      this.bookAppointmentForm.addControl(key, new FormControl(bookAppointmentModel[key]));
    });
    this.bookAppointmentForm = setRequiredValidator(this.bookAppointmentForm, ["appointmentDate"]);
  }

  OnChangeSeller(value) {
    if (value) {
      if (this.data && this.data.fromUrl && this.data.fromUrl == 'Appointment History') {
        if (this.data.appointmentStatusName == AppointmentStatus.BOOKED && !this.isArrayShifted) {
          let filteredSameSeller = this.alternateSellers.find(as => as['sellerSlotId'] == this.selectedSellerDetails['sellerSlotId']);
          if (!filteredSameSeller) {
            this.alternateSellers = this.alternateSellers.filter(as => as['sellerSlotId'] !== this.selectedSellerDetails['sellerSlotId']);
            this.isArrayShifted = true;
          }
        }
      }
      this.timeslots = this.tempTimeslots;
      this.resultedTimeSlotDetail = this.timeslots.find(x => x.sellerSlotId == value);
      this.setSelectedSellerDetails(this.resultedTimeSlotDetail);
    }
  }

  setSelectedSellerDetails(resultedTimeSlotDetail) {
    if (!resultedTimeSlotDetail) return;
    this.bookAppointmentForm.patchValue({
      fullName: resultedTimeSlotDetail.sellerName,
      appointmentDuration: resultedTimeSlotDetail.sellerAppointmentDuration,
      appointmentTimeFrom: resultedTimeSlotDetail.availableTime,
      contactNoDuplicate: resultedTimeSlotDetail.contactNumber, email: resultedTimeSlotDetail.email,
      sellerSlotId: resultedTimeSlotDetail.sellerSlotId ? resultedTimeSlotDetail.sellerSlotId : null
    })
    this.resultedTimeSlotDetail = resultedTimeSlotDetail;
  }

  changeTimeSlot(i, obj) {
    this.timeslots.forEach((element, index) => {
      element.isChecked = (index == i) ? true : false;
    });
    if (!this.bookAppointmentForm.value.isAlternateSeller) {
      this.resultedTimeSlotDetail = obj;
      this.setSelectedSellerDetails(this.resultedTimeSlotDetail);
    }
  }

  getSellerNames() {
    let appointmentDate = this.datePipe.transform(this.bookAppointmentForm.get('appointmentDate').value, 'yyyy-MM-dd');
    if (this.bookAppointmentForm.get('alternateSellerTime').value instanceof Date) {
      this.alternateSellerTime = this.datePipe.transform(this.bookAppointmentForm.get('alternateSellerTime').value, 'HH:mm:ss');
    }
    else {
      this.alternateSellerTime = this.bookAppointmentForm.get('alternateSellerTime').value ?
        this.bookAppointmentForm.get('alternateSellerTime').value : "";
    }
    let payload = this.bookAppointmentForm.get('isAlternateSeller').value ? {
      appointmentDate,
      leadId: this.leadHeaderData.leadId,
      appointmentTime: this.alternateSellerTime
    } : { appointmentDate, leadId: this.leadHeaderData.leadId }
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.APPOINTMENT_SELLER, undefined, true,
      prepareRequiredHttpParams(payload)).subscribe((response: IApplicationResponse) => {
        this.timeslots = [];
        this.alternateSellers = [];
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          if (this.shouldDisableAllFields) {
            this.alternateSellers.push(response.resources[0]);
            this.setSelectedSellerDetails(response.resources[0]);
          }
          else {
            this.timeslots = response.resources;
            this.tempTimeslots = JSON.parse(JSON.stringify(this.timeslots));
            if (this.bookAppointmentForm.get('alternateSellerTime').value) {
              this.alternateSellers = this.timeslots;
            }
            else {
              this.timeslots.forEach((element, index) => {
                element.isChecked = index == 0 ? true : false;
                if (index > 2) {
                  if (this.data && this.data.fromUrl && this.data.fromUrl == 'Appointment History') {
                    if (this.data.appointmentStatusName == AppointmentStatus.BOOKED && !this.isArrayUnShifted && this.bookAppointmentForm.get('isAlternateSeller').value) {
                      this.alternateSellers.push(this.selectedSellerDetails);
                      this.isArrayUnShifted = true;
                    }
                  }
                  this.alternateSellers.push(element);
                }
              });
            }
            if (this.timeslots.length > 0 && !this.bookAppointmentForm.get('isAlternateSeller').value) {
              let filteredObject = this.bookAppointmentForm.get('sellerSlotId').value ?
                this.timeslots.find(ts => ts['sellerSlotId'] == this.bookAppointmentForm.get('sellerSlotId').value) : this.timeslots[0];
              if (this.data?.appointmentStatusName !== AppointmentStatus.BOOKED && filteredObject && !this.timeslots.find(ts => ts['isChecked'] == true)) {
                filteredObject.isChecked = true;
              }
              this.setSelectedSellerDetails(filteredObject);
            }
            else if (this.timeslots.length > 0 && this.bookAppointmentForm.get('isAlternateSeller').value) {
              let filteredObject = this.bookAppointmentForm.get('sellerSlotId').value ?
                this.timeslots.find(ts => ts['sellerSlotId'] == this.bookAppointmentForm.get('sellerSlotId').value) : {};
              this.setSelectedSellerDetails(filteredObject);
            }
            else if (this.timeslots.length == 0 && this.bookAppointmentForm.get('isAlternateSeller').value) {
              this.bookAppointmentForm.get('sellerSlotId').setValue(null);
              this.setSelectedSellerDetails(this.resultedTimeSlotDetailInitialState);
            }
            if (!this.timeslots.find(t => t.isChecked == true) && this.data) {
              if (this.bookAppointmentForm.get('sellerSlotId').value) {
                this.timeslots.find(ts => ts['sellerSlotId'] == this.bookAppointmentForm.get('sellerSlotId').value).isChecked = true;
              }
              else {
                this.timeslots[0].isChecked = true;
              }
            }
          }
          if (this.timeslots.length == 1) {
            this.setSelectedSellerDetails(this.timeslots[0]);
          }
          if (this.timeslots.length > 0) {
            this.timeslots.forEach((timeslot) => {
              timeslot.availableTimeInRailwayTime = convertTwelevHoursToTwentyFourHours(timeslot.availableTimeFrom);
            });
            if (this.bookAppointmentForm.get('alternateSellerTime').value) {
              let userAppointmentTime = this.datePipe.transform(new Date(this.bookAppointmentForm.get('alternateSellerTime').value), 'HH:mm');
              this.isAtleastOneSellerAvailableTimeEqualsUserTime = this.timeslots.find(ts => ts['availableTimeInRailwayTime'] == userAppointmentTime) ? true : false;
            }
          }
          else {
            this.isAtleastOneSellerAvailableTimeEqualsUserTime = false;
          }
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  escalate() {
    let data = {
      createdUserId: this.loggedInUserData.userId,
      leadId: this.leadHeaderData.leadId,
      appointmentDate: this.datePipe.transform(new Date(this.bookAppointmentForm.get('appointmentDate').value), 'yyyy-MM-dd'),
      appointmentTime: this.datePipe.transform(new Date(this.bookAppointmentForm.get('alternateSellerTime').value), 'HH:mm')
    }
    this.dialog.closeAll();
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(AppointmentEscalationPopupComponent, {
      width: "850px",
      disableClose: true,
      data,
    });
    dialogReff.componentInstance.outputData.subscribe((result) => {
      if (result == true) {
        this.outputData.emit(true);
        this.dialog.closeAll();
      }
    });
  }

  onSubmit() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.bookAppointmentForm.invalid) {
      return;
    }
    if (!this.bookAppointmentForm.get('isAlternateSeller').value && this.timeslots.find(ts => ts['isChecked'] && ts['isChecked'] == true) == undefined) {
      this.snackbarService.openSnackbar('Time slot should be selected..!!', ResponseMessageTypes.WARNING);
      return;
    }
    if (!this.bookAppointmentForm.get('isAlternateSeller').value) {
      this.setSelectedSellerDetails(this.resultedTimeSlotDetail);
    }
    this.bookAppointmentForm.value.appointmentDate = this.datePipe.transform(this.bookAppointmentForm.value.appointmentDate, 'yyyy-MM-dd');
    this.bookAppointmentForm.value.leadId = this.leadHeaderData.leadId;
    this.bookAppointmentForm.value.createdUserId = this.loggedInUserData.userId;
    this.bookAppointmentForm.value.alternateSellerTime = this.alternateSellerTime;
    this.bookAppointmentForm.value.fullName = this.leadHeaderData.customerName;
    this.bookAppointmentForm.value.customerId = this.leadHeaderData.customerId;
    this.bookAppointmentForm.value.sellerId = this.timeslots.find(ts => ts['sellerSlotId'] == this.resultedTimeSlotDetail.sellerSlotId).sellerId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse>;
    if (this.data && this.data.fromUrl && this.data.fromUrl == 'Appointment History') {
      this.bookAppointmentForm.value['reminderTime'] = null;
      crudService = this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.APPOINTMENTS_RESCHEDULE,
        { ...this.bookAppointmentForm.getRawValue(), ...this.bookAppointmentForm.value });
    }
    else {
      crudService =
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.APPOINTMENT_SELLER_SAVE,
          { ...this.bookAppointmentForm.getRawValue(), ...this.bookAppointmentForm.value });
    }
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.rxjsService.setLeadNotesUpdateStatus(true);
        this.outputData.emit(true);
        this.dialog.closeAll();
      }
    });
  }

  ngDestory() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
