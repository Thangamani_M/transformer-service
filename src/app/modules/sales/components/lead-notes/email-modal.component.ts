import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService } from '@app/shared/services';
import { emailPattern, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { LeadEmailNotificationtModel } from '@modules/sales/models/lead-email-notification.model';
import { Store } from '@ngrx/store';
import { SalesModuleApiSuffixModels } from '@sales/shared';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-email-modal-component',
  templateUrl: './email-modal.component.html',
  styleUrls: ['./lead-notes.scss'],
})
export class EmailModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  leadId: string;
  emailNotificationForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  fileList: File[] = [];
  customerId = "";

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private dialog: MatDialog, private store: Store<AppState>) {
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.combineLatestNgrxStoreData();
    this.createEmailNotificationAddForm();
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.leadId = response[1]?.leadId;
    });
  }

  createEmailNotificationAddForm(): void {
    let emailNotificationModel = new LeadEmailNotificationtModel();
    this.emailNotificationForm = this.formBuilder.group({});
    Object.keys(emailNotificationModel).forEach((key) => {
      if (key == 'notificationCC') {
        this.emailNotificationForm.addControl(key,
          new FormControl(emailNotificationModel[key], Validators.compose([Validators.email, emailPattern])));
      }
      else {
        this.emailNotificationForm.addControl(key, new FormControl(emailNotificationModel[key]));
      }
    });
    this.emailNotificationForm.get('notificationTo').setValue(this.data.email);
    this.emailNotificationForm = setRequiredValidator(this.emailNotificationForm, ["notificationTo", "notificationSubject", "notificationText"]);
  }

  emitUploadedFiles(uploadedFiles: any[]) {
    this.fileList = uploadedFiles;
  }

  onSubmit(): void {
    if (this.emailNotificationForm.invalid) {
      this.emailNotificationForm.markAllAsTouched();
      return;
    }
    var formValue = this.emailNotificationForm.value;
    formValue.leadId = this.data.leadId;
    formValue.createdUserId = this.loggedInUserData.userId;
    formValue.customerId = this.data.customerId;
    let formData = new FormData();
    formData.append('email', JSON.stringify(formValue));
    this.fileList.forEach(file => {
      formData.append('file', file);
    });
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEAD_NOTIFICATIONS_EMAIL, formData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.rxjsService.setLeadNotesUpdateStatus(true);
          this.rxjsService.setDialogOpenProperty(false);
          this.outputData.emit(true);
          this.dialog.closeAll();
        }
      });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
