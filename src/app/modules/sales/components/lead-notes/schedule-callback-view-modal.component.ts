import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes } from '@app/shared/enums';
import { agentLoginDataSelector } from '@app/shared/layouts/app-header/state/agent-login.selector';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { CallbackStatus, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { ScheduleCallBackViewModel } from '@modules/sales/models/lead-sms-notification.model';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-schedule-callback-view',
  templateUrl: './schedule-callback-view-modal.component.html',
  styleUrls: ['./lead-notes.scss'],
})
export class ScheduleCallbackViewModalComponent implements OnInit {
  loggedInUserData: LoggedInUserModel;
  agentExtensionNo = '';

  constructor(
    private snackbarService: SnackbarService, private crudService: CrudService,
    private dialog: MatDialog, private store: Store<AppState>, private rxjsService: RxjsService,
    @Inject(MAT_DIALOG_DATA) public scheduleCallbackData: ScheduleCallBackViewModel
  ) {
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.combineLatestNgrxStoreData();
    if (this.scheduleCallbackData.fromModule == 'Lead' || this.scheduleCallbackData.fromModule == 'Customer') {
      this.getScheduleCallbackById();
      this.rxjsService.setPopupLoaderProperty(true);
    }
    this.store.pipe(select(agentLoginDataSelector)).subscribe((agentExtensionNo: string) => {
      this.agentExtensionNo = agentExtensionNo;
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = response[0];
    });
  }

  getScheduleCallbackById() {
    let apiSuffix, retrievableId;
    if (this.scheduleCallbackData.fromModule == 'Lead') {
      apiSuffix = SalesModuleApiSuffixModels.SALES_API_LEAD_CALLBACK;
      retrievableId = this.scheduleCallbackData.leadCallbackId;
    }
    else if (this.scheduleCallbackData.fromModule == 'Customer') {
      apiSuffix = SalesModuleApiSuffixModels.CUSTOMER_CALLBACK;
      retrievableId = this.scheduleCallbackData.customerCallbackId;
    }
    this.crudService.get(ModulesBasedApiSuffix.SALES, apiSuffix, retrievableId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.scheduleCallbackData = { ...this.scheduleCallbackData, ...response.resources };
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  onSubmit(type) {
    let crudService: Observable<IApplicationResponse>, moduleName = ModulesBasedApiSuffix.SALES;
    // lead callback trigger
    if (this.scheduleCallbackData.triggerName == 'ScheduleCallbackTrigger') {
      switch (type) {
        case 'snooze':
          crudService = this.crudService.create(moduleName, SalesModuleApiSuffixModels.SALES_API_LEAD_CALLBACK_SNOOZE, {
            leadCallbackId: this.scheduleCallbackData.leadCallbackId,
            createdUserId: this.loggedInUserData.userId
          });
          break;
        case 'accept':
          if (this.agentExtensionNo) {
            crudService = this.crudService.update(moduleName, SalesModuleApiSuffixModels.SALES_API_LEAD_CALLBACK__ACCEPT, {
              leadCallbackId: this.scheduleCallbackData.leadCallbackId,
              modifiedUserId: this.loggedInUserData.userId,
              callbackStatusId: CallbackStatus.COMPLETED
            });
            this.rxjsService.setscheduleCallbackDetails(this.scheduleCallbackData);
            this.rxjsService.setExpandOpenScape(true);
            this.dialog.closeAll();
          } else {
            this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
          }
          break;
        case 'cancel':
          crudService = this.crudService.create(moduleName, SalesModuleApiSuffixModels.SALES_API_LEAD_CALLBACK__CANCEL, {
            leadCallbackId: this.scheduleCallbackData.leadCallbackId,
            modifiedUserId: this.loggedInUserData.userId,
            callbackStatusId: CallbackStatus.CANCELLED
          });
          break;
      }
    }
    // customer callback trigger
    else if (this.scheduleCallbackData.triggerName == 'CustomerAgentCallbackTrigger') {
      switch (type) {
        case 'snooze':
          crudService = this.crudService.create(moduleName, SalesModuleApiSuffixModels.CUSTOMER_CALLBACK_SNOOZE, {
            customerCallbackId: this.scheduleCallbackData.customerCallbackId,
            createdUserId: this.loggedInUserData.userId
          });
          break;
        case 'accept':
          if (this.agentExtensionNo) {
            crudService = this.crudService.update(moduleName, SalesModuleApiSuffixModels.CUSTOMER_CALLBACK_ACCEPT, {
              customerCallbackId: this.scheduleCallbackData.customerCallbackId,
              modifiedUserId: this.loggedInUserData.userId,
              callbackStatusId: CallbackStatus.COMPLETED
            });
            this.rxjsService.setscheduleCallbackDetails(this.scheduleCallbackData);
            this.rxjsService.setExpandOpenScape(true);
            this.dialog.closeAll();
          } else {
            this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
          }
          break;
        case 'cancel':
          crudService = this.crudService.create(moduleName, SalesModuleApiSuffixModels.CUSTOMER_CALLBACK_CANCEL, {
            customerCallbackId: this.scheduleCallbackData.customerCallbackId,
            modifiedUserId: this.loggedInUserData.userId,
            callbackStatusId: CallbackStatus.CANCELLED
          });
          break;
      }
    }
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.dialog.closeAll();
      }
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
