import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { forkJoin } from 'rxjs';
import { SalesModuleApiSuffixModels } from '../../../../../modules/sales/shared/utils/sales-module.enums';
import { ConfirmDialogPopupComponent } from '../../../../../shared/components/confirm-dialog-popup/confirm-dialog-popup.component';
import { CrudService } from '../../../../../shared/services/crud.service';
import { HttpCancelService } from '../../../../../shared/services/http-cancel.service';
import { RxjsService } from '../../../../../shared/services/rxjs.services';
import { ModulesBasedApiSuffix } from '../../../../../shared/utils/enums.utils';
import { prepareRequiredHttpParams, setRequiredValidator } from '../../../../../shared/utils/functions.utils';
import { IApplicationResponse } from '../../../../../shared/utils/interfaces.utils';
import { formConfigs } from '../../../../../shared/utils/variables.utils';
@Component({
    selector: 'app-appointment-escalation',
    templateUrl: './appointment-escalation-pop-up.component.html'
})

export class AppointmentEscalationPopupComponent implements OnInit {
    formConfigs = formConfigs;
    reactiveFormGroup: FormGroup;
    escalationReasons = [];
    escalationTo = [];
    isFormSubmitted = false;
    @Output() outputData = new EventEmitter();
    appointmentDateTimeModel = '';

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        private dialog: MatDialog,
        private crudService: CrudService,
        public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private rxjsService: RxjsService) {
    }

    ngOnInit(): void {
        this.createQuotationForm();
        this.getForkJoinRequests();
        this.appointmentDateTimeModel = this.data.appointmentDate + " " + this.data.appointmentTime;
    }

    getForkJoinRequests(): void {
        forkJoin([this.getEscalationTo(), this.getEscalationReasons()]).subscribe((resp: IApplicationResponse[]) => {
            resp.forEach((res: IApplicationResponse, ix: number) => {
                if (res.isSuccess && res.statusCode === 200) {
                    switch (ix) {
                        case 0:
                            this.escalationTo = res.resources;
                            break;
                        case 1:
                            this.escalationReasons = res.resources;
                            break;
                    }
                }
            });
            this.rxjsService.setPopupLoaderProperty(false);
        });
    }

    getEscalationTo() {
        return this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.APPOINTMENT_ESCALATION_ROLE_LEVELS, undefined, false, prepareRequiredHttpParams({
                leadId: this.data.leadId
            }));
    }

    getEscalationReasons() {
        return this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.SELLER_APPOINTMENT_ESCALATION_REASONS_LIST, undefined, false, null);
    }

    createQuotationForm(): void {
        let model = {
            escalationUserId: '', escalationReasonId: '', notes: ''
        };
        this.reactiveFormGroup = this.formBuilder.group({});
        Object.keys(model).forEach((key) => {
            this.reactiveFormGroup.addControl(key,
                new FormControl(model[key]));
        });
        this.reactiveFormGroup = setRequiredValidator(this.reactiveFormGroup, ["escalationUserId", "escalationReasonId"]);
    }

    onSubmit(): void {
        this.isFormSubmitted = true;
        if (this.reactiveFormGroup.invalid) return;
        this.reactiveFormGroup.value.appointmentDateTime = this.appointmentDateTimeModel;
        this.reactiveFormGroup.value.leadId = this.data.leadId;
        this.reactiveFormGroup.value.createdUserId = this.data.createdUserId;
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ESCALATION_APPOINTMENTS, this.reactiveFormGroup.value).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.dialog.closeAll();
                this.outputData.emit(true);
            }
        });
    }

    dialogClose(): void {
        this.dialogRef.close(false);
        this.outputData.emit(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }
}
