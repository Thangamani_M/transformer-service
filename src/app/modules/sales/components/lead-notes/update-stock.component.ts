import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { CommonPaginationConfig, debounceTimeForSearchkeyword, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared/utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-update-stock-component',
  templateUrl: './update-stock.component.html',
  styleUrls: ['./lead-notes.scss'],
})
export class UpdateStockModalComponent extends PrimeNgTableVariablesModel implements OnInit {
  leadId: string;
  userData;
  @Output() outputData = new EventEmitter<any>();
  componentList = [];
  componentGroupList = [];
  itemBrandList = [];
  details;
  quantity = null;
  primengTableConfigProperties: any = {
    tableCaption: "Stock Items",
    breadCrumbItems: [{ displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Raw Lead List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Stock Items',
          dataKey: 'itemId',
          ebableAddActionBtn: false,
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: true,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'stockCode', header: 'Stock Code' },
          { field: 'stockDescription', header: 'Description' },
          { field: 'unitPrice', header: 'Unit Price' },
          { field: 'qty', header: 'Quantity' },
          { field: 'subTotal', header: 'Sub Total' },
          { field: 'vat', header: 'vat ' },
          { field: 'total', header: 'Total' },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_STOCK_ITEMS_DATA,
          moduleName: ModulesBasedApiSuffix.SALES,
        },
      ]
    }
  }
  systemTypes;
  updateStockForm: FormGroup;
  columnFilterForm: FormGroup;
  displayName;
  selectedOption = [];
  tempSelectedRows = [];
  searchColumns;
  row;
  selectedPageSize = CommonPaginationConfig.defaultPageSize;
  actualTempSelectedRows = [];

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    @Inject(MAT_DIALOG_DATA) public data, private crudService: CrudService, private dialog: MatDialog,
    private store: Store<AppState>, private tableFilterFormService: TableFilterFormService,
    private momentService: MomentService) {
    super();
    this.leadId = this.activatedRoute.snapshot.queryParams.leadId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.data.data.leadItems.forEach((leadItem) => {
      leadItem.isChecked = true;
    });
    this.columnFilterForm = this.formBuilder.group({});
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns);
    this.columnFilterRequest();
    this.updateStockForm = this.formBuilder.group({
      component: [''],
      componentGroup: [''],
      brand: [''],
    });
    this.getForkJoinRequests();
  }

  getForkJoinRequests(): void {
    forkJoin([this.getLeadStockItems(), this.crudService
      .dropdown(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.UX_SYSTEM_TYPES
      )]).subscribe(
        (response: IApplicationResponse[]) => {
          response.forEach((respObj: IApplicationResponse, ix: number) => {
            if (respObj.isSuccess && respObj.statusCode === 200) {
              switch (ix) {
                case 0:
                  if (respObj.resources.componentList.length > 0) {
                    this.componentList = respObj.resources.componentList;
                    this.componentList.forEach(element => {
                      element.id = element.id.toString();
                    });
                  }
                  if (respObj.resources.componentGroupList.length > 0) {
                    this.componentGroupList = respObj.resources.componentGroupList;
                    this.componentGroupList.forEach(element => {
                      element.id = element.id.toString();
                    });
                  }
                  if (respObj.resources.itemBrandList.length > 0) {
                    this.itemBrandList = respObj.resources.itemBrandList;
                  }
                  break;
                case 1:
                  this.systemTypes = respObj.resources;
                  this.details = this.systemTypes.filter(e => e.id == this.data.data.systemTypeId)
                  this.displayName = this.details[0]['displayName']
                  break;
              }
              if (ix == response.length - 1) {
                this.getModalData();
              }
            }
          });
        }
      );
  }

  resetForm() {
    this.updateStockForm.get('componentGroup').setValue('');
    this.updateStockForm.get('component').setValue('');
    this.updateStockForm.get('brand').setValue('');
    this.totalRecords = 0;
    this.dataList = [];
    this.getModalData('0', '10', this.searchColumns);
  }

  userListOption = [];
  componentGroupChange() {
    let isstechid = this.updateStockForm.get('componentGroup').value;
    if (isstechid.length != 0) {
      this.getModalData();
    } else if (isstechid != "") {
      this.getModalData();
    }
  }

  componentChange() {
    let isstechid = this.updateStockForm.get('component').value;
    if (isstechid.length != 0) {
      this.getModalData();
    } else if (isstechid != "") {
      this.getModalData();
    }
  }

  itemBrandChange() {
    let isstechid = this.updateStockForm.get('brand').value;
    if (isstechid.length != 0) {
      this.getModalData();
    } else if (isstechid != "") {
      this.getModalData();
    }
  }

  disbledAdd: boolean = false;
  getModalData(pageIndex?: string, pageSize?: string,
    otherParams?: object) {
    this.loading = true;
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setGlobalLoaderProperty(false);
    setTimeout(() => {
      this.rxjsService.setPopupLoaderProperty(true);
    });
    let obj1 = {
      systemTypeId: this.data.data.systemTypeId,
      districtId: this.data.leadData.districtId,
      leadId: this.data.leadData.leadId,
      leadGroupId: this.data.leadData.leadGroupId,
      componentGroupId: this.updateStockForm.value.componentGroup,
      componentId: this.updateStockForm.value.component,
      itemBrandId: this.updateStockForm.value.brand
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    if (this.searchColumns) {
      otherParams = { ...otherParams, ...this.searchColumns };
    }
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_STOCK_ITEMS_DATA;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      salesModuleApiSuffixModels, undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        this.selectedRows = [];
        this.totalRecords = response.totalCount;
        if (response.resources && response.isSuccess) {
          this.dataList = response.resources;
          this.dataList.forEach(element => {
            element.isChecked = false;
            this.data.data.leadItems.forEach(item => {
              if (item.stockCode == element.stockCode) {
                element.isChecked = true;
                element.qty = item.qty;
                element.subTotal = item.subTotal;
                element.vat = item.vat;
                element.total = item.total;
              }
            });
          });
          this.dataList.forEach((element) => {
            this.tempSelectedRows.forEach(item => {
              if (item.stockCode == element.stockCode) {
                element.isChecked = item.isChecked;
                element.qty = item.qty;
                element.subTotal = item.subTotal;
                element.vat = item.vat;
                element.total = item.total;
              }
            }
            );
          });
          this.dataList.forEach(element => {
            element['unitTaxPrice'] = element.subTotal * element['taxPercentage'] / 100;
            element.vat = element['unitTaxPrice'];
            element.tax = element.vat;
            if (element.isChecked) {
              this.disbledAdd = true;
              this.selectedRows.push(element)
            }
          });
          this.tempSelectedRows = this.retrieveUniqueObjects([...this.selectedRows, ...this.tempSelectedRows]);
        }
        else {
          this.disbledAdd = false;
          this.dataList = [];
        }
        this.rxjsService.setPopupLoaderProperty(false);
        this.loading = false;
      });
  }

  onChecked(data?) {
    if (data) {
      data.isChecked = !data.isChecked
      if (data.isChecked) {
        this.disbledAdd = true;
        this.tempSelectedRows.push(data);
      } else {
        this.selectedRows = this.selectedRows.filter(item => item.itemId !== data.itemId);
        this.data.data.leadItems = this.data.data.leadItems.filter(item => item.itemId !== data.itemId);
        this.tempSelectedRows = this.tempSelectedRows.filter(item => item.itemId !== data.itemId);
        if (this.tempSelectedRows.length == 0) {
          this.disbledAdd = false;
        }
      }
    } else {
      this.dataList.forEach(element => {
        element.tax = element.vat;
      });
      if (this.selectedRows.length > 0) {
        this.disbledAdd = true;
      } else {
        this.disbledAdd = false;
      }
      this.tempSelectedRows = this.selectedRows;
    }
    this.actualTempSelectedRows = this.retrieveUniqueObjects([...this.data.data.leadItems, ...this.tempSelectedRows]).filter(f => f['isChecked'] && f['isChecked'] == true);
  }

  onQuantityChange(type, serviceSubItem: object): void {
    serviceSubItem["qty"] =
      type === "plus"
        ? serviceSubItem["qty"] + 1
        : serviceSubItem["qty"] === 1
          ? serviceSubItem["qty"]
          : serviceSubItem["qty"] - 1;
    serviceSubItem['qty'] = serviceSubItem['qty']
    serviceSubItem['unitPrice'] = serviceSubItem['unitPrice']
    serviceSubItem['tax'] = serviceSubItem['tax']
    serviceSubItem['vat'] = serviceSubItem['vat']
    serviceSubItem['subTotal'] = serviceSubItem['qty'] * serviceSubItem['unitPrice']
    serviceSubItem['vat'] = serviceSubItem['qty'] * serviceSubItem['tax']
    serviceSubItem['total'] = serviceSubItem['subTotal'] + (serviceSubItem['vat']
    );
  }

  retrieveUniqueObjects = (list: Array<object>): Array<object> => {
    return [...new Set(list.map(obj => obj['itemId']))].map(itemId => {
      return list.find(obj => obj['itemId'] === itemId)
    });
  }

  otherParams = {};
  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {})
          let row = {};
          row['searchColumns'] = this.searchColumns;
          return of(this.onCRUDRequested(row));
        })
      )
      .subscribe();
  }

  loadPaginationLazy(event): void {
    this.selectedPageSize = event.rows ? event.rows : CommonPaginationConfig.defaultPageSize;
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["maximumRows"] = event.rows;
    if (event.sortField) {
      row["sortOrderColumn"] = event.sortField;
    }
    if (event.sortField) {
      row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    }
    if (!row['searchColumns']) {
      row['searchColumns'] = this.searchColumns;
    }
    else {
      row['searchColumns'] = event.filters;
    }
    this.tempSelectedRows = this.retrieveUniqueObjects([...this.selectedRows, ...this.tempSelectedRows]);
    this.onCRUDRequested(row);
  }

  onCRUDRequested(row?: object): void {
    let otherParams = {};
    if (Object.keys(row).length > 0 && row['searchColumns']) {
      Object.keys(row['searchColumns']).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          otherParams[key] = this.momentService.localToUTC(row['searchColumns'][key]);
        }
        else if (key == 'leadStatusName') {
          otherParams['leadStatusId'] = row['searchColumns'][key]['id'];
        } else if (key == 'siteTypeName') {
          otherParams['siteTypeId'] = row['searchColumns'][key]['id'];
        } else if (key == 'leadGroupName') {
          otherParams['leadGroupId'] = row['searchColumns'][key]['id'];
        }
        else {
          otherParams[key] = row['searchColumns'][key]['id'] ?
            row['searchColumns'][key]['id'] : row['searchColumns'][key];
        }
      });
    }
    let pageIndex, maximumRows;
    if (!row['maximumRows']) {
      maximumRows = this.selectedPageSize;
      pageIndex = CommonPaginationConfig.defaultPageIndex;
    }
    else {
      maximumRows = row["maximumRows"];
      pageIndex = row["pageIndex"];
    }
    delete row['maximumRows'] && row['maximumRows'];
    delete row['pageIndex'] && row['pageIndex'];
    delete row['searchColumns'];
    this.getModalData(pageIndex, maximumRows, { ...otherParams, ...row });
  }

  quantityChange(data) {
    this.dataList.find(v => v.itemId == data.itemId).subTotal = data.qty * data.unitPrice;
    this.dataList.find(v => v.itemId == data.itemId).total = this.dataList.find(v => v.itemId == data.itemId).subTotal + data.vat;
  }

  trim(arr, key) {
    var values = {};
    return arr.filter(function (item) {
      var val = item[key];
      var exists = values[val];
      values[val] = true;
      return !exists;
    });
  }

  submit() {
    this.outputData.emit(this.actualTempSelectedRows);
    this.dialog.closeAll();
  }

  getLeadStockItems(pageIndex?: string, pageSize?: string): Observable<IApplicationResponse> {
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_STOCK_ITEMS_DROPDOWNS;
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      salesModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, {
        SystemTypeId: this.data.data.systemTypeId
      }));
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}