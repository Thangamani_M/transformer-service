import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { LeadSMSNotificationtModel } from '@modules/sales/models/lead-sms-notification.model';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-sms-modal-component',
  templateUrl: './sms-modal.component.html',
  styleUrls: ['./lead-notes.scss'],
})
export class SMSModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  smsNotificationForm: FormGroup;
  loggedInUserData: LoggedInUserModel;

  constructor(@Inject(MAT_DIALOG_DATA) public data, private formBuilder: FormBuilder, private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService, private crudService: CrudService, private dialog: MatDialog,
    private store: Store<AppState>) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createSMSNotificationAddForm();
    this.smsNotificationForm.get('notificationToCountryCode').valueChanges.subscribe((notificationToCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(notificationToCountryCode);
    });

    this.smsNotificationForm.get('notificationTo').valueChanges.subscribe((notificationTo: string) => {
      this.setPhoneNumberLengthByCountryCode(this.smsNotificationForm.get('notificationToCountryCode').value);
    });
    this.rxjsService.setDialogOpenProperty(true);
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([this.store.select(loggedInUserData)]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  createSMSNotificationAddForm(): void {
    let smsNotificationModel = new LeadSMSNotificationtModel();
    this.smsNotificationForm = this.formBuilder.group({});
    Object.keys(smsNotificationModel).forEach((key) => {
      this.smsNotificationForm.addControl(key, new FormControl(smsNotificationModel[key]));
    });
    this.smsNotificationForm.controls['notificationToCountryCode'].setValue("+27");
    this.smsNotificationForm = setRequiredValidator(this.smsNotificationForm, ["notificationTo", "notificationText"]);
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.smsNotificationForm.get('notificationTo').setValidators([Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        break;
      default:
        this.smsNotificationForm.get('notificationTo').setValidators([Validators.minLength(formConfigs.indianContactNumberMaxLength),
        Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        break;
    }
  }

  onSubmit(): void {
    if (this.smsNotificationForm.invalid) {
      return;
    }
    //assign hidden field values
    this.smsNotificationForm.value.notificationTo = this.smsNotificationForm.value.notificationTo.replace(/\D/g, '');
    this.smsNotificationForm.value.leadId = this.data.leadId;
    this.smsNotificationForm.value.createdUserId = this.loggedInUserData.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEAD_NOTIFICATIONS_SMS, this.smsNotificationForm.value, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.rxjsService.setLeadNotesUpdateStatus(true);
          this.outputData.emit(true)
          this.dialog.closeAll();
        }
      });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
