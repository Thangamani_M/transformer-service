import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes } from '@app/shared';
import { FindDuplicatePipe } from '@app/shared/pipes/find-duplicate.pipe';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { countryCodes, currentOrGreaterMonths, formConfigs, generateCurrentYearToNext99Years, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControlError, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData, selectStaticEagerLoadingCustomerTypesState$, selectStaticEagerLoadingTitlesState$ } from '@modules/others';
import { ContactInfoModel, CustomerContactEditModel } from '@modules/sales/models/schedule-callback.model';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-customer-contact-add-edit',
  templateUrl: './customer-contact-edit-modal.component.html',
  styleUrls: ['./lead-notes.scss'],
})
export class CustomerContactAddEditModalComponent implements OnInit {
  customerContactForm: FormGroup;
  @ViewChild('input', { static: false }) row;
  formConfigs = formConfigs;
  countryCodeList = countryCodes;
  ticketReferenceList = [];
  moduleName = "";
  customerData;
  popUpClose = true;
  months = [];
  years = generateCurrentYearToNext99Years();
  customerSiteType;
  customerTypes = [];
  titles = [];
  loggedInUserData: LoggedInUserModel;
  copyOfContactInfoFormGroupControls;
  saIdPermissions;
  passportPermissions;
  hideSaid = true
  hidePassport = true
  isEditPassport = false;
  isEditSaId = false;

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data,
    private crudService: CrudService, private dialog: MatDialog, private router: Router,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectStaticEagerLoadingCustomerTypesState$), this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingTitlesState$)]
    ).subscribe((response) => {
      this.customerTypes = response[0];
      this.loggedInUserData = new LoggedInUserModel(response[1]);
      this.titles = response[2];
    });
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.getQuickActionComponent()
      .subscribe(moduleName => {
        this.moduleName = moduleName;
      })
    this.rxjsService.getCustomerDate()
      .subscribe(data => {
        this.customerData = data;
      });
    this.createCustomerContactForm();
    this.getDetails();
    this.onFormControlChanges();
    this.saIdPermissions = this.data.permissions?.subMenu.find(fSC => fSC.menuName == "SA ID");
    this.passportPermissions = this.data.permissions?.subMenu.find(fSC => fSC.menuName == "Passport");

    this.isEditPassport = this.passportPermissions?.subMenu.find(fSC => fSC.menuName == "Edit");
    this.isEditSaId = this.saIdPermissions?.subMenu.find(fSC => fSC.menuName == "Edit");

  }

  validateAtleastOneFieldToBeMandatory(type: string): void {
    if (type === 'contactInfo') {
      if (!this.contactInfoFormGroupControls.get('mobile2').value && !this.contactInfoFormGroupControls.get('officeNo').value &&
        !this.contactInfoFormGroupControls.get('premisesNo').value) {
        this.setAtleastOneFieldRequiredError(type);
      }
      else {
        this.customerContactForm.controls['contactInfo'] = removeFormControlError(this.customerContactForm.controls['contactInfo'] as FormGroup, 'atleastOneOfTheFieldsIsRequired');
      }
    }
  }

  setAtleastOneFieldRequiredError(type: string): void {
    this.contactInfoFormGroupControls.get('mobile2').setErrors({ atleastOneOfTheFieldsIsRequired: true });
    this.contactInfoFormGroupControls.get('officeNo').setErrors({ atleastOneOfTheFieldsIsRequired: true });
    this.contactInfoFormGroupControls.get('premisesNo').setErrors({ atleastOneOfTheFieldsIsRequired: true });
  }

  createContactInfoForm(contactInfo?: ContactInfoModel): FormGroup {
    let contactInfoModel = new ContactInfoModel(contactInfo);
    let formControls = {};
    Object.keys(contactInfoModel).forEach((key) => {
      formControls[key] = new FormControl({ value: contactInfoModel[key], disabled: true });
    });
    return this.formBuilder.group(formControls);
  }

  createCustomerContactForm(): void {
    let customerContactEditModel = new CustomerContactEditModel();
    this.customerContactForm = this.formBuilder.group({});
    Object.keys(customerContactEditModel).forEach((key) => {
      if (key == 'contactInfo') {
        this.customerContactForm.addControl(key, this.createContactInfoForm());
      } else {
        this.customerContactForm.addControl(key, new FormControl({ value: customerContactEditModel[key], disabled: true }));
      }
    });
    this.customerContactForm = setRequiredValidator(this.customerContactForm, ["firstName", "lastName", "titleId", "email"])
    this.customerContactForm.controls['contactInfo'] = setRequiredValidator(this.customerContactForm.controls['contactInfo'] as FormGroup, ['mobile1']);
  }

  onFormControlChanges() {
    this.contactInfoFormGroupControls.get('mobile2CountryCode').valueChanges.subscribe((mobile2CountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(mobile2CountryCode);
      setTimeout(() => {
        this.row?.nativeElement?.focus();
        this.row?.nativeElement?.blur();
      });
    });
    this.contactInfoFormGroupControls.get('mobile2').valueChanges.subscribe((mobileNumber2: string) => {
      this.setPhoneNumberLengthByCountryCode(this.contactInfoFormGroupControls.get('mobile2CountryCode').value);
    });
    this.customerContactForm.get('isPassport').valueChanges.subscribe((isPassport: boolean) => {
      if (this.customerSiteType.id == 2 || isPassport == null) {
        return;
      }
      if (isPassport) {
        this.customerContactForm = setRequiredValidator(this.customerContactForm, ['passportNo', 'passportExpiryMonth', 'passportExpiryYear']);
        this.customerContactForm.get('saId').clearValidators();
        this.customerContactForm.get('saId').updateValueAndValidity();
        this.customerContactForm.get('saId').setValue(null);
        this.customerContactForm.get('passportNo').setValue(this.copyOfContactInfoFormGroupControls?.passportNo || null);
      }
      else {
        this.customerContactForm = setRequiredValidator(this.customerContactForm, ['saId']);
        this.customerContactForm.get('passportNo').clearValidators();
        this.customerContactForm.get('passportNo').updateValueAndValidity();
        this.customerContactForm.get('passportNo').setValue(null);
        this.customerContactForm.get('saId').setValue(this.copyOfContactInfoFormGroupControls?.saId || null);
        this.customerContactForm.get('passportExpiryMonth').clearValidators();
        this.customerContactForm.get('passportExpiryMonth').updateValueAndValidity();
        this.customerContactForm.get('passportExpiryMonth').setValue(null);
        this.customerContactForm.get('passportExpiryYear').clearValidators();
        this.customerContactForm.get('passportExpiryYear').updateValueAndValidity();
        this.customerContactForm.get('passportExpiryYear').setValue(null);
      }
    });
    this.customerContactForm.get('passportExpiryYear').valueChanges.subscribe((passportExpiryYear: string) => {
      this.customerContactForm.get('passportExpiryMonth').setValue(null);
      this.months = currentOrGreaterMonths(passportExpiryYear);
    });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.contactInfoFormGroupControls.get('mobile2').setValidators([Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        break;
      default:
        this.contactInfoFormGroupControls.get('mobile2').setValidators([Validators.minLength(formConfigs.indianContactNumberMaxLength),
        Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        break;
    }
  }

  get contactInfoFormGroupControls(): FormGroup {
    if (!this.customerContactForm) return;
    return this.customerContactForm.get('contactInfo') as FormGroup;
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.CUSTOMER_CONTACT__GET_UPDATE, this.customerData['customerId'], null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          this.customerSiteType = this.customerTypes.find(cT => cT['id'] == response.resources.customerTypeId);
          if (response.resources.isPassport) {
            response.resources['passportExpiryMonth'] = response.resources.passportExpiryDate.split("/")[0];
            response.resources['passportExpiryYear'] = response.resources.passportExpiryDate.split("/")[1];
          }
          if (!response.resources.saId && !response.resources.isPassport) {
            response.resources.isPassport = null;
          }
          this.copyOfContactInfoFormGroupControls = response.resources;
          this.customerContactForm.patchValue(response.resources);
          if (response.resources.isPassport) {
            this.customerContactForm.get('passportExpiryMonth').setValue(response.resources.passportExpiryDate.split("/")[0]);
          }
          this.contactInfoFormGroupControls.patchValue(this.copyOfContactInfoFormGroupControls);

        }
      });
  }

  getTicketReferenceDropdoewData(id) {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKETS_CUSTOMER, null, null, prepareGetRequestHttpParams(null, null, { customerId: id }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.ticketReferenceList = response.resources;
        }
      });
  }

  isFormSubmitted = false
  onSubmit(): void {
    this.isFormSubmitted = true;
    this.validateAtleastOneFieldToBeMandatory('contactInfo');
    const isMobile1Duplicate = new FindDuplicatePipe().transform(this.contactInfoFormGroupControls.get('mobile1').value,
      this.contactInfoFormGroupControls, 'mobile1');
    const isMobile2Duplicate = this.contactInfoFormGroupControls.get('mobile2').value ? new FindDuplicatePipe().transform(this.contactInfoFormGroupControls.get('mobile2').value,
      this.contactInfoFormGroupControls, 'mobile2', this.contactInfoFormGroupControls.get('mobile2CountryCode').value) : false;
    const isOfficeNoDuplicate = this.contactInfoFormGroupControls.get('officeNo').value ? new FindDuplicatePipe().transform(this.contactInfoFormGroupControls.get('officeNo').value,
      this.contactInfoFormGroupControls, 'officeNo') : false;
    const isPremisesNoDuplicate = this.contactInfoFormGroupControls.get('premisesNo').value ? new FindDuplicatePipe().transform(this.contactInfoFormGroupControls.get('premisesNo').value,
      this.contactInfoFormGroupControls, 'premisesNo') : false;
    if (this.customerContactForm.invalid || isMobile1Duplicate || isMobile2Duplicate || isOfficeNoDuplicate || isPremisesNoDuplicate) {
      return;
    }
    if (this.customerContactForm.invalid) {
      return;
    }
    if (!this.contactInfoFormGroupControls.get('mobile2').value) {
      delete this.contactInfoFormGroupControls.value.mobile2CountryCode;
    }
    if (!this.contactInfoFormGroupControls.get('premisesNo').value) {
      delete this.contactInfoFormGroupControls.value.premisesNoCountryCode;
    }
    if (!this.contactInfoFormGroupControls.get('officeNo').value) {
      delete this.contactInfoFormGroupControls.value.officeNoCountryCode;
    }
    let obj = this.customerContactForm.value;
    if (typeof obj.contactInfo.mobile1 === "string") {
      obj.contactInfo.mobile1 = obj.contactInfo.mobile1.split(" ").join("")
      obj.mobile1 = obj.contactInfo.mobile1.replace(/"/g, "");
    }
    if (typeof obj.contactInfo.mobile2 === "string") {
      obj.contactInfo.mobile2 = obj.contactInfo.mobile2.split(" ").join("")
      obj.mobile2 = obj.contactInfo.mobile2.replace(/"/g, "");
    }
    if (typeof obj.contactInfo.officeNo === "string") {
      obj.contactInfo.officeNo = obj.contactInfo.officeNo.split(" ").join("")
      obj.officeNo = obj.contactInfo.officeNo.replace(/"/g, "");
    }
    if (typeof obj.contactInfo.premisesNo === "string") {
      obj.contactInfo.premisesNo = obj.contactInfo.premisesNo.split(" ").join("")
      obj.premisesNo = obj.contactInfo.premisesNo.replace(/"/g, "");
    }
    obj.modifiedUserId = this.loggedInUserData.userId;
    obj.modifiedDate = new Date();
    obj.isLeadPage = true
    if (obj.isPassport) {
      obj.passportExpiryDate = `${obj.passportExpiryMonth}/${obj.passportExpiryYear}`;
    }
    delete obj.passportExpiryMonth;
    delete obj.passportExpiryYear;
    this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.CUSTOMER_CONTACT_UPDATE, obj, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.dialog.closeAll();
          this.popUpClose = false;
          this.rxjsService.setContactUpdateDetails(true);
        }
      });
  }

  onEdit() {
    let access = this.data.permissions['subMenu'].find(fSC => fSC.menuName == "Edit");
    if (!access) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.activatedRoute?.snapshot?.queryParams?.feature_name) {
      this.onAfterEdit();
    } else {
      this.rxjsService.setGlobalLoaderProperty(true);
      let filterObj = { userId: this.loggedInUserData?.userId, customerId: this.customerData?.customerId, siteAddressId: this.data?.addressId, uniqueCallId: this.data?.uniqueCallId ? this.data?.uniqueCallId : null };
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, this.data?.api, null, false,
        prepareRequiredHttpParams(filterObj))
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.onAfterEdit();
          } else {
            this.dialog.closeAll();
            this.popUpClose = false;
            this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: this.customerData?.customerId, siteAddressId: this.data?.addressId, tab: this.data?.tab } })
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  onAfterEdit() {
    this.customerContactForm.enable({onlySelf:true,emitEvent:false});
    this.customerContactForm.get('contactInfo.mobile1CountryCode').disable();
    this.customerContactForm.get('contactInfo.mobile2CountryCode').disable();
    this.customerContactForm.get('contactInfo.officeNoCountryCode').disable();
    this.customerContactForm.get('contactInfo.premisesNoCountryCode').disable();
  }

  showPassport() {
    let access = this.passportPermissions?.subMenu.find(fSC => fSC.menuName == "Edit");
    if (!access) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.hidePassport = !this.hidePassport
  }

  showSaid() {
    let access = this.saIdPermissions?.subMenu.find(fSC => fSC.menuName == "Edit");
    if (!access) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.hideSaid = !this.hideSaid;
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
