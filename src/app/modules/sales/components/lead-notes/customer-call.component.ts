import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, ExtensionModalComponent, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { formConfigs, ModulesBasedApiSuffix, prepareRequiredHttpParams } from '@app/shared/utils';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'app-customer-call-component',
  templateUrl: './customer-call.component.html',
  styleUrls: ['./lead-notes.scss'],
})
export class CustomerCallModalComponent implements OnInit {
  leadId: string;
  formConfigs = formConfigs;
  dialedNumbers;
  premisesNumber;
  mobileNumber1;
  mobileNumber2;
  officeNumber;
  agentExtensionNo;

  constructor(private snackbarService: SnackbarService, private rxjsService: RxjsService,
    @Inject(MAT_DIALOG_DATA) public data: any, private crudService: CrudService, private dialog: MatDialog,
    private store: Store<AppState>) {
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_CONTACT_NUMBER, undefined, false,
      prepareRequiredHttpParams({
        leadId: this.data.leadId
      })).subscribe(resp => {
        this.dialedNumbers = resp.resources;
        if (resp.isSuccess && resp.statusCode == 200 && resp.resources) {
          this.premisesNumber = resp.resources.premisesNumber;
          this.mobileNumber1 = resp.resources.mobileNumber1;
          this.mobileNumber2 = resp.resources.mobileNumber2;
          this.officeNumber = resp.resources.officeNumber;
          this.rxjsService.setDialogOpenProperty(false);
        }
      });
  }

  callDail(e): void {
    if (!this.agentExtensionNo) {
      this.dialog.closeAll();
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    } else {
      let data = {
        customerContactNumber: e,
        leadId: this.data.leadId,
        clientName: this.dialedNumbers.customerName
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
    }
  }
}
