import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { LoggedInUserModel, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services/crud.service';
import { ModulesBasedApiSuffix } from '@app/shared/utils/enums.utils';
import { loggedInUserData } from '@modules/others';
import { TelephonyApiSuffixModules } from '@modules/sales/shared/utils/telephony.module.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'callback-customer-info',
  templateUrl: './callback-customers-info-popup.component.html',
  styleUrls: ['./lead-notes.scss'],
})
export class callBackCustomerInfoModalComponent implements OnInit {
  customerData: any = [];
  customerAddressDetails;
  customerAddressList = [];
  selectedCustomer;
  selectedAddress;
  loggedInUserData: LoggedInUserModel;

  constructor(@Inject(MAT_DIALOG_DATA) public data, private crudService: CrudService, private router: Router,
    private dialog: MatDialog, private store: Store<AppState>, private rxjsService: RxjsService) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.customerData = this.data;
    this.customerData['Customers'].forEach((element, index) => {
      if (index == 0) {
        this.selectedCustomer = element;
        element.isChecked = true;
      } else {
        element.isChecked = false;
      }
    });
    this.selectedCustomer['address']?.forEach((element, index) => {
      if (index == 0) {
        element.isChecked = true;
        this.selectedAddress = element;
      } else {
        element.isChecked = false;
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  selectCustomer(i, selectedCustomer) {
    this.selectedCustomer = selectedCustomer;
    this.customerData['Customers'].forEach((customerObj, index) => {
      customerObj.isChecked = (index == i) ? true : false;
      customerObj['address'].forEach((address, index) => {
        address.isChecked = (index == 0) ? true : false;
      });
    });
  }

  redirectToCustomerDetailPage(customer) {
    this.rxjsService.setViewCustomerData({
      customerId: customer.CustomerId,
      addressId: customer.address[this.getIndexOSelectedCustomerAddress()].AddressId,
      customerTab: 3,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
    this.dialog.closeAll();
  }

  getIndexOSelectedCustomerAddress(): number {
    return this.selectedCustomer['address']?.findIndex(el => el.isChecked);
  }

  selectAddress(i, selectedAddress) {
    this.selectedAddress = selectedAddress;
    this.selectedCustomer['address'].forEach((addressObj, index) => {
      addressObj.isChecked = (index == i) ? true : false;
    });
  }

  customerDetails(e) {
    this.customerAddressDetails = e;
    this.customerAddressList = e.address;
  }

  redirectToSearch() {
    this.router.navigate(['customer/advanced-search']);
  }

  redirectToStack(customer) {
    let routeFragment = (this.loggedInUserData.roleName == 'Operator') ? '/event-management/stack-operator' :
      '/event-management/stack-dispatcher';
    this.router.navigate([routeFragment], {
      queryParams: {
        occurrenceBookId: customer?.OccurrenceBookId
      }
    });
    this.dialog.closeAll();
  }

  redirectToCustomerPage() {
    let payload = {
      customerId: (this.customerData.Customers.length == 1) ? this.customerData.Customers[0].CustomerId :
        this.selectedCustomer.CustomerId,
      uniqueCallId: this.customerData.UniqueCallid,
      modifiedUserId: this.customerData.UserId
    }
    this.crudService.update(ModulesBasedApiSuffix.TELEPHONY, TelephonyApiSuffixModules.AGENT_UPDATE_CUSTOMER, payload)
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.rxjsService.setViewCustomerData({
            customerId: this.selectedCustomer.CustomerId,
            addressId: this.selectedAddress?.addressId,
            customerTab: 3,
            monitoringTab: null,
          })
          this.rxjsService.navigateToViewCustomerPage();
          this.dialog.closeAll();
        }
      });
  }
}
