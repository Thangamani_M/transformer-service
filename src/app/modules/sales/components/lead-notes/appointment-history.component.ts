import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { loggedInUserData, selectStaticEagerLoadingAppointmentStatusState$ } from '@modules/others';
import { BookAppoinmentModalComponent, LeadHeaderData, selectLeadHeaderDataState$ } from '@modules/sales';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-appointment-history',
  templateUrl: './appointment-history.component.html'
})
export class AppointmentHistoryComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties;
  leadHeaderData: LeadHeaderData;
  leadId = "";
  editObj;
  showAppointmentOption = false;
  showCancelOption = false;
  breadCrumb: BreadCrumbModel;

  constructor(
    private dialog: MatDialog, private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private rxjsService: RxjsService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Appointment History",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Appointment History',
            dataKey: 'appointmentId',
            enableBreadCrumb: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'appointmentRefNo', header: 'Appointment Id' },
            { field: 'appointmentDate', header: 'Appointment Date', isDate: true },
            { field: 'appointmentTime', header: 'Time Slot' },
            { field: 'sellerName', header: 'Seller' },
            { field: 'appointmentStatusName', header: 'Status', type: 'dropdown', options: [], placeholder: 'Select Appointment Status' }
            ],
          }]
      }
    }
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectLeadHeaderDataState$), this.store.select(selectStaticEagerLoadingAppointmentStatusState$),
      this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.leadHeaderData = new LeadHeaderData(response[1]);
      if (response[2]) {
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns[4].options = response[2] = response[2].map(item => {
          return { label: item.displayName, value: item.id };
        });
      }
      this.leadId = response[3]['leadId'];
      this.breadCrumb = {
        pageTitle: { key: 'Appointment History', value: this.leadHeaderData.leadRefNo },
        items: [{
          key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
        }, { key: 'Appointment History' }]
      };
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].caption = `Appointment History  ${this.leadHeaderData.leadRefNo}`;
    });
  }

  ngOnInit(): void {
    this.getAppointments();
  }

  getAppointments(pageIndex?: string, pageSize?: string, otherParams?: object) {
    let obj1 = { leadId: this.leadId };
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.APPOINTMENTS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  openAddEditPage(type: CrudType | string, data?: object | string): void {
    data['fromUrl'] = 'Appointment History';
    switch (type) {
      case CrudType.EDIT:
        if (data['appointmentStatusName'] == 'Booked') {
          this.showAppointmentOption = true;
          this.editObj = data;
        }
        break;
    }
  }

  cancel() {
    this.showAppointmentOption = false;
    this.showCancelOption = true;
  }

  reschedule() {
    this.showAppointmentOption = false;
    let data: any;
    data = this.editObj;
    const dialogReff = this.dialog.open(BookAppoinmentModalComponent, { width: '900px', disableClose: true, data });
    dialogReff.afterClosed().subscribe(result => {
      if (!result) {
        this.getAppointments();
      }
    });
  }

  cancelAppointment() {
    let obj = {
      appointmentId: this.editObj['appointmentId'],
      modifiedUserId: this.loggedInUserData.userId
    }
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.APPOINTMENT_CANCEL, obj).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.showCancelOption = false;
        this.getAppointments();
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    if (row['appointmentStatusName'] == 'Rescheduled') {
      return;
    }
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        if (CrudType.GET === type && Object.keys(row).length > 0) {
          if (row['searchColumns']) {
            Object.keys(row['searchColumns']).forEach((key) => {
              if (key == 'appointmentStatusName') {
                searchObj['appointmentStatusId'] = row['searchColumns']['appointmentStatusName'];
              }
            });
          }
        }
        this.getAppointments(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
    }
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}