import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel,
  countryCodes, CrudService, emailPattern, formConfigs, IApplicationResponse, LoggedInUserModel,
  ModulesBasedApiSuffix, removeFormControlError, RxjsService, setRequiredValidator
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { StopAndKnockRemoveEquipmentModel } from '@modules/sales/models/stop-knock-model';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-stop-and-knock-remove-equipment',
  templateUrl: './stop-and-knock-remove-equipment.component.html',
  styleUrls: ['./stop-and-knock-add-edit.component.scss']
})
export class StopAndKnockRemoveEquipmentComponent implements OnInit {
  stopKnockForm: FormGroup;
  formConfigs = formConfigs;
  stopAndKnockId = "";
  loggedInUserData: LoggedInUserModel;
  countryCodes = countryCodes;
  todayDate = new Date();
  contactPersonTypes = [];
  addressInfo = { formatedAddress: "", movingDate: "", technicalArea: "", serviceTerminatedDate: "" };
  @ViewChild('input', { static: false }) input;
  fromUrl = '';
  breadCrumb: BreadCrumbModel;

  constructor(private rxjsService: RxjsService,
    private formBuilder: FormBuilder,
    private crudService: CrudService, private datePipe: DatePipe,
    private activatedRoute: ActivatedRoute, private router: Router,
    private store: Store<AppState>) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.activatedRoute.queryParams, this.rxjsService.getFromUrl()
    ]).pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.stopAndKnockId = response[1]['stopAndKnockId'];
        this.fromUrl = response[2];
      });
  }

  ngOnInit(): void {
    this.createStockKnockForm();
    this.getForkJoinRequests();
    this.onFormControlChanges();
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_CONTACT_TYPE_PERSONS),
      this.getEquipmentRemovalDetailsById()]
    )
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200 && respObj.isSuccess) {
            switch (ix) {
              case 0:
                this.contactPersonTypes = respObj.resources;
                break;
              case 1:
                let stopAndKnockModel = new StopAndKnockRemoveEquipmentModel(respObj.resources);
                this.addressInfo = respObj.resources.addressInfo;
                this.addressInfo.serviceTerminatedDate = respObj.resources.serviceTerminatedDate;
                this.addressInfo.movingDate = respObj.resources.movingDate;
                this.addressInfo.technicalArea = respObj.resources.technicalArea;
                stopAndKnockModel.removalDate = new Date(stopAndKnockModel.removalDate);
                this.stopKnockForm.patchValue(stopAndKnockModel, { emitEvent: false, onlySelf: true });
                this.prepareDynamicBreadCrumbs();
                if (stopAndKnockModel.mobile1 && stopAndKnockModel.mobile1 != "") {
                  // to remove min length errors when masking the phone numbers
                  this.stopKnockForm = removeFormControlError(this.stopKnockForm, 'minlength');
                  setTimeout(() => {
                    this.focusInAndOutFormArrayFields();
                  }, 100);
                  break;
                }
            }
          }
        });
        setTimeout(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }, 100);
      });
  }

  prepareDynamicBreadCrumbs() {
    if (this.fromUrl == 'Stop And Knock List') {
      this.breadCrumb = {
        pageTitle: { key: 'Stop And Knock Remove Equipment', value: this.stopKnockForm.get('stopAndKnockRefNo').value },
        items: [{ key: 'Sales' }, { key: 'Stop And Knock Dashboard', routeUrl: '/sales/stop-and-knock' }]
      };
    }
    else if (this.fromUrl == 'My Stop And Knock List') {
      this.breadCrumb = {
        pageTitle: { key: 'My Stop And Knock Remove Equipment', value: this.stopKnockForm.get('stopAndKnockRefNo').value },
        items: [{ key: 'My Tasks' }, { key: 'My Sales' }, { key: 'My Stop And Knock Dashboard', routeUrl: '/my-tasks/my-sales?tab=1' }]
      };
    }
  }

  createStockKnockForm(): void {
    let stopKnockModel = new StopAndKnockRemoveEquipmentModel();
    this.stopKnockForm = this.formBuilder.group({
    });
    Object.keys(stopKnockModel).forEach((key) => {
      if (key == 'email') {
        this.stopKnockForm.addControl(key,
          new FormControl(stopKnockModel[key], Validators.compose([Validators.email, emailPattern])));
      }
      else {
        this.stopKnockForm.addControl(key, new FormControl(stopKnockModel[key]));
      }
    });
    this.stopKnockForm = setRequiredValidator(this.stopKnockForm, ['firstName', 'lastName', 'mobile1', 'officeNo', 'notes', 'removalDate']);
  }

  onFormControlChanges() {
    this.stopKnockForm.get('officeNoCountryCode').valueChanges.subscribe((officeNoCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(officeNoCountryCode);
      setTimeout(() => {
        this.input.nativeElement.focus();
        this.input.nativeElement.blur();
      });
    });
    this.stopKnockForm.get('officeNo').valueChanges.subscribe((officeNo: string) => {
      this.setPhoneNumberLengthByCountryCode(this.stopKnockForm.get('officeNoCountryCode').value);
    });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.stopKnockForm.get('officeNo').setValidators([Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        break;
      default:
        this.stopKnockForm.get('officeNo').setValidators([Validators.minLength(formConfigs.indianContactNumberMaxLength),
        Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        break;
    }
  }

  focusInAndOutFormArrayFields(): void {
    this.input.nativeElement.focus();
    this.input.nativeElement.blur();
  }

  getEquipmentRemovalDetailsById() {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.STOP_AND_KNOCK_REMOVE_EQUIPMENT,
      this.stopAndKnockId)
  }

  onSubmit() {
    if (this.stopKnockForm.invalid) {
      return;
    }
    if (!this.stopKnockForm.get('officeNo').value) {
      delete this.stopKnockForm.value.officeNoCountryCode;
    }
    this.stopKnockForm.value.mobile1 = this.stopKnockForm.value.mobile1.toString().replace(/\s/g, "");
    this.stopKnockForm.value.officeNo = this.stopKnockForm.value.officeNo.toString().replace(/\s/g, "");
    this.stopKnockForm.value.stopAndKnockId = this.stopAndKnockId;
    this.stopKnockForm.value.createdUserId = this.loggedInUserData.userId;
    this.stopKnockForm.value.removalDate = this.datePipe.transform(this.stopKnockForm.value.removalDate, 'yyyy-MM-dd');
    this.crudService.create(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.STOP_AND_KNOCK_REMOVE_EQUIPMENT, this.stopKnockForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.redirectToListPage('addEdit');
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  redirectToListPage(type = 'addEdit') {
    if (type == 'addEdit') {
      if (this.fromUrl == 'Stop And Knock List') {
        this.router.navigate(['/sales/stop-and-knock/add-edit'], { queryParams: { stopAndKnockId: this.stopAndKnockId } });
      }
      else {
        this.router.navigate(['/my-tasks/my-sales/my-stop-and-knocks/add-edit'], { queryParams: { stopAndKnockId: this.stopAndKnockId } });
      }
    }
    else {
      if (this.fromUrl == 'Stop And Knock List') {
        this.router.navigateByUrl("/sales/stop-and-knock");
      }
      else {
        this.router.navigate(['my-tasks/my-sales'],
          { queryParams: { tab: 3, }, skipLocationChange: true })
      }
    }
  }
}