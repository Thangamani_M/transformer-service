import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams, ResponseMessageTypes, SnackbarService
} from '@app/shared';
import { RxjsService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels, Sales_Leads_Components } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-stop-and-knock-list',
  templateUrl: './stop-and-knock-list.component.html',
  styleUrls: ['./stop-and-knock-add-edit.component.scss']
})
export class StopAndKnockListComponent extends PrimeNgTableVariablesModel implements OnInit {
  SearchText: string = '';
  loading = false;
  fromUrl = 'Stop And Knock List';

  primengTableConfigProperties: any = {
    tableCaption: "Stop And Knock",
    shouldShowBreadCrumb: true,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Sales ', relativeRouterUrl: '' }, { displayName: 'Stop And Knock Dashboard' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Stop And Knock',
          dataKey: 'stopAndKnockId',
          enableBreadCrumb: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          reorderableColumns: false,
          resizableColumns: false,
          cursorLinkIndex: 0,
          columns: [{ field: 'stopAndKnockRefNo', header: 'SK ID' },
          { field: 'suburbName', header: 'Suburb' },
          { field: 'cityName', header: 'City' },
          { field: 'reconnectionArea', header: 'Reconn Area' },
          { field: 'agent', header: 'Agent' },
          { field: 'agentMobileNumber', header: 'Agent Mobile Number', isPhoneMask: true },
          { field: 'agentFollowupDate', header: 'Agent Followup Date' },
          { field: 'seller', header: 'Seller' },
          { field: 'sellerFollowupDate', header: 'Seller Followup Date' },
          { field: 'contactPersonType', header: 'Contact Person Type' },
          { field: 'mobileNo1', header: 'MobileNo1', isPhoneMask: true },
          { field: 'createdDate', header: 'Created Date', isDateTime: true }
          ]
        }]
    }
  }

  constructor(
    private router: Router, private snackbarService: SnackbarService,
    private crudService: CrudService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private rxjsService: RxjsService) {
    super();
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.getStopAndKnocks();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.rxjsService.getFromUrl(),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.fromUrl = response[1];
      if (response[2][Sales_Leads_Components.STOP_AND_KNOCK]) {
        let prepareDynamicTableTabsFromPermissionsObj =
          prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, response[2][Sales_Leads_Components.STOP_AND_KNOCK]);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getStopAndKnocks(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.STOP_KNOCK,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
      }
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, otherParams?): void {
    switch (type) {
      case CrudType.GET:
        this.getStopAndKnocks(
          row["pageIndex"], row["pageSize"], otherParams);
        break;
      case CrudType.VIEW:
        this.onOpenNextPage(row);
        break;
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onOpenNextPage(editableObject) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    else {
      this.rxjsService.setFromUrl('Stop And Knock List');
      this.router.navigate(['/sales/stop-and-knock/add-edit'], { queryParams: { stopAndKnockId: editableObject['stopAndKnockId'] } })
    }
  }
}