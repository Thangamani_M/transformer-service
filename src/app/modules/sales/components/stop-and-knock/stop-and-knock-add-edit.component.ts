import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  addFormControls, BreadCrumbModel, countryCodes, CrudService, CrudType, CustomDirectiveConfig, debounceTimeForSearchkeyword, emailPattern, filterListByKey, FindDuplicatePipe,
  formConfigs, IApplicationResponse, LeadGroupTypes, LoggedInUserModel,
  ModulesBasedApiSuffix, prepareAutoCompleteListFormatForMultiSelection, prepareRequiredHttpParams,
  removeFormControlError, removeFormControls, RxjsService, SERVER_REQUEST_DATE_TRANSFORM, setRequiredValidator, TableFilterFormService
} from '@app/shared';
import {
  loggedInUserData, selectDynamicEagerLoadingLeadCatgoriesState$, selectDynamicEagerLoadingSourcesState$,
  selectStaticEagerLoadingCustomerTypesState$, selectStaticEagerLoadingSiteTypesState$, selectStaticEagerLoadingTitlesState$
} from '@modules/others';
import { ContactInfoModel, SalesModuleApiSuffixModels } from '@modules/sales';
import {
  StopAndKnockAdditionalOptionsModel, StopAndKnockAddressModel, StopAndKnockBasicInfoModel,
  StopAndKnockContactInfoModel, StopAndKnockModel
} from '@modules/sales/models/stop-knock-model';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
declare var $;
@Component({
  selector: 'app-stop-and-knock-add-edit',
  templateUrl: './stop-and-knock-add-edit.component.html',
  styleUrls: ['./stop-and-knock-add-edit.component.scss']
})
export class StopAndKnockAddEditComponent implements OnInit {
  stopKnockForm: FormGroup;
  stopKnockAdditionalOptionsForm: FormGroup;
  formConfigs = formConfigs;
  stopAndKnockId = "";
  customerTypes = [];
  titles = [];
  leadCategories = [];
  leadSources = [];
  siteTypes = [];
  customerTypeName;
  loggedInUserData: LoggedInUserModel;
  countryCodes = countryCodes;
  commercialCustomerType = 'Commercial';
  residentialCustomerType = 'Residential';
  isFormSubmitted = false;
  stopAndKnockNotes = [];
  contactPersonTypes = [];
  @ViewChild('all_options_modal', { static: false }) all_options_modal: ElementRef;
  @ViewChild('input', { static: false }) row;
  popupCaption = '';
  todayDate = new Date();
  userGroups = [];
  agentGroupedUsers = [];
  sellerGroupedUsers = [];
  currentAgentName;
  currentSellerName;
  customerMobileNumbersObj;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  fromUrl = 'Stop And Knock List';
  breadCrumb: BreadCrumbModel;
  movingDateVar;
  isScheduleFollowupBtnDisabled = false;
  btnSelectionType = "";
  @ViewChild("existing_stop_and_knock_modal", { static: false })
  existing_stop_and_knock_modal: ElementRef<any>;
  primengTableConfigProperties = {
    tableCaption: "Template",
    tableComponentConfigs: {
      tabsList: [
        {
          caption: "Found Existing Customer Profile",
          dataKey: "index",
          resizableColumns: false,
          enableFieldsSearch: true,
          columns: [
            { field: "isChecked", header: "Select" },
            { field: "customerRefNo", header: "Customer ID" },
            { field: "firstName", header: "First Name" },
            { field: "lastName", header: "Last Name" },
            { field: "email", header: "Email" },
            { field: "mobile1", header: "Mobile No" },
            { field: "customerTypeName", header: "Customer Type" },
            { field: "fullAddress", header: "Address" },
            { field: "isOpenLeads", header: "Open Lead" },
          ],
        },
      ],
    },
  };
  selectedExistingCustomerProfile: any = { isOpenLeads: false };
  columnFilterForm: FormGroup;
  dataList = [];
  dataListSecondCopy = [];
  dataListCopy = [];
  totalRecords;
  isResetBtnDisabled = true;
  isExistingLeadModalClosed = false;
  isNewProfileBtnDisabled = false;
  feature = '';
  featureIndex = '';
  searchColumns;
  selectedRows: string[] = [];
  pageSize: number = 10;
  loading: boolean;

  constructor(private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private tableFilterFormService: TableFilterFormService,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private router: Router,
    private store: Store<AppState>, private datePipe: DatePipe) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(selectStaticEagerLoadingSiteTypesState$), this.store.select(selectStaticEagerLoadingCustomerTypesState$),
      this.activatedRoute.queryParams,
      this.rxjsService.getFromUrl()
    ]).pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.titles = response[1];
        this.siteTypes = response[2];
        this.customerTypes = response[3];
        this.stopAndKnockId = response[4]['stopAndKnockId'];
        this.feature = response[4]?.["feature_name"];
        this.featureIndex = response[4]?.["featureIndex"];
        this.fromUrl = response[5];
      });
  }

  prepareDynamicBreadCrumbs() {
    if (this.fromUrl == 'Stop And Knock List') {
      this.breadCrumb = {
        pageTitle: { key: 'Update Stop And Knock', value: this.stopKnockForm.get('stopAndKnockRefNo').value },
        items: [{ key: 'Sales' }, { key: 'Stop And Knock Dashboard' }, { key: 'Stop And Knock Dashboard', routeUrl: '/sales/stop-and-knock' }]
      };
    }
    else if (this.fromUrl == 'My Stop And Knock List') {
      this.breadCrumb = {
        pageTitle: { key: 'Update My Stop And Knock', value: this.stopKnockForm.get('stopAndKnockRefNo').value },
        items: [{ key: 'My Tasks' }, { key: 'My Sales' }, { key: 'My Stop And Knock Dashboard', routeUrl: '/my-tasks/my-sales?tab=1' }]
      };
    }
  }

  ngOnInit() {
    this.combineLatestNgrxDynamicData();
    this.createStockKnockForm();
    this.createStockKnockAddtionalOptionsForm();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns);
    this.columnFilterRequest();
    this.onFormControlChanges();
    this.getForkJoinRequests();
    this.onBootstrapModalEventChanges();
  }

  loadPaginationLazy(event) {
    this.rxjsService.setPopupLoaderProperty(true);
    if (event.sortField && event.sortOrder) {
      this.dataList.sort((a, b) => {
        let fieldA = a[event.sortField].toUpperCase();
        var fieldB = b[event.sortField].toUpperCase();
        if (event.sortOrder == 1) {
          if (fieldA < fieldB) {
            return -1;
          }
          if (fieldA > fieldB) {
            return 1;
          }
        } else {
          if (fieldB < fieldA) {
            return -1;
          }
          if (fieldB > fieldA) {
            return 1;
          }
        }
      });
    }
    setTimeout(() => {
      this.rxjsService.setPopupLoaderProperty(false);
    }, 500);
  }

  combineLatestNgrxDynamicData() {
    combineLatest([
      this.store.select(selectDynamicEagerLoadingLeadCatgoriesState$),
      this.store.select(selectDynamicEagerLoadingSourcesState$)])
      .subscribe((response) => {
        this.leadCategories = response[0];
        this.leadSources = response[1];
      });
  }

  onBootstrapModalEventChanges() {
    $("#all_options_modal").on("shown.bs.modal", () => {
      delete this.stopKnockAdditionalOptionsForm;
      this.createStockKnockAddtionalOptionsForm();
      setTimeout(() => {
        this.rxjsService.setFormSubmittedSuccessfullyProperty(false);
      }, 1000);
      this.currentAgentName = "";
      this.currentSellerName = "";
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#all_options_modal").on("hidden.bs.modal", () => {
      this.rxjsService.setFormSubmittedSuccessfullyProperty(true);
      this.rxjsService.setDialogOpenProperty(false);
      this.popupCaption = "";
    });
    $("#existing_stop_and_knock_modal").on("shown.bs.modal", (e) => {
      this.searchColumns = "";
      this.contactInfoFormGroupControls.get("email").markAsPristine();
      this.contactInfoFormGroupControls.get("mobile1").markAsPristine();
      this.contactInfoFormGroupControls.updateValueAndValidity();
      this.basicInfoFormGroupControls.get("firstName").markAsPristine();
      this.basicInfoFormGroupControls.get("lastName").markAsPristine();
      this.basicInfoFormGroupControls.updateValueAndValidity();
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.isExistingLeadModalClosed = false;
      this.dataList.map((obj) => (obj["isChecked"] = false));
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#existing_stop_and_knock_modal").on("hidden.bs.modal", (e) => {
      this.isExistingLeadModalClosed = true;
      this.addressInfoFormGroupControls.get("formatedAddress").enable({ emitEvent: false, onlySelf: true });
      setTimeout(() => {
        this.isExistingLeadModalClosed = false;
      }, 100);
      this.rxjsService.setDialogOpenProperty(false);
      this.onResetFilter('modalClose');
    });
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap((obj) => {
          Object.keys(obj).forEach((key) => {
            if (obj[key] === "") {
              delete obj[key];
            }
          });
          this.searchColumns = Object.entries(obj).reduce(
            (a, [k, v]) => (v == null ? a : ((a[k] = v), a)),
            {}
          );
          let row = {};
          row["searchColumns"] = this.searchColumns;
          return of(this.searchColumns);
        })
      )
      .subscribe((searchedColumnKeyValues) => {
        this.rxjsService.setPopupLoaderProperty(true);
        this.dataList = this.dataListSecondCopy;
        this.dataListCopy = this.dataListSecondCopy;
        if (Object.keys(searchedColumnKeyValues).length == 0) {
          this.isResetBtnDisabled = true;
          this.dataList = this.dataListSecondCopy;
          this.dataListCopy = this.dataListSecondCopy;
          this.totalRecords = this.dataListSecondCopy.length;
        } else {
          this.isResetBtnDisabled = false;
          Object.keys(searchedColumnKeyValues).forEach((key, ix) => {
            if (Object.keys(searchedColumnKeyValues).length == 1) {
              this.dataListCopy = filterListByKey(this.dataListCopy, key, searchedColumnKeyValues[key]);
            } else {
              this.dataListCopy = filterListByKey(this.dataListCopy, key, searchedColumnKeyValues[key]);
            }
            if (Object.keys(searchedColumnKeyValues).length == ix + 1) {
              this.dataList = this.dataListCopy;
              this.totalRecords = this.dataListCopy.length;
            }
          });
        }
        setTimeout(() => {
          this.rxjsService.setPopupLoaderProperty(false);
        }, 500);
      });
  }

  getForkJoinRequests() {
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_CONTACT_TYPE_PERSONS), this.getStopKnockDetailsById()]
    )
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200 && respObj.isSuccess) {
            switch (ix) {
              case 0:
                this.contactPersonTypes = respObj.resources;
                break;
              case 1:
                let stopAndKnockModel = new StopAndKnockModel(respObj.resources);
                this.movingDateVar = respObj.resources.movingDate ? new Date(respObj.resources.movingDate) : null;
                if (stopAndKnockModel.serviceTerminatedDate) {
                  stopAndKnockModel.serviceTerminatedDate = new Date(stopAndKnockModel.serviceTerminatedDate);
                }
                else {
                  stopAndKnockModel.serviceTerminatedDate = new Date();
                }
                stopAndKnockModel.addressInfo.latLong = stopAndKnockModel.addressInfo.latitude + "," + stopAndKnockModel.addressInfo.longitude;
                stopAndKnockModel.basicInfo.titleId = stopAndKnockModel.basicInfo.titleId == 0 ? null : stopAndKnockModel.basicInfo.titleId;
                stopAndKnockModel.contactPersonTypeId = stopAndKnockModel.contactPersonTypeId == 0 ? null : stopAndKnockModel.contactPersonTypeId;
                let selectedleadCategoryId = [];
                if (stopAndKnockModel.leadCategoryId) {
                  stopAndKnockModel?.leadCategoryId?.forEach(result =>{
                    selectedleadCategoryId.push(this.leadCategories.find(l => l['id'] == result));
                  })
                }
                this.stopKnockForm.patchValue(stopAndKnockModel);
                this.stopKnockForm.get("leadCategoryId").setValue(selectedleadCategoryId);
                this.prepareDynamicBreadCrumbs();
                this.contactInfoFormGroupControls.patchValue(new StopAndKnockContactInfoModel(stopAndKnockModel.contactInfo));
                this.stopKnockForm.controls['contactInfo'] = removeFormControlError(this.stopKnockForm.controls['contactInfo'] as FormGroup, 'minlength');
                this.stopAndKnockNotes = respObj.resources.notes;
                break;
            }
          }
        });
        setTimeout(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }, 100);
      });
  }

  onFormControlChanges() {
    this.contactInfoFormGroupControls.get('mobile2CountryCode').valueChanges.subscribe((mobile2CountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(mobile2CountryCode);
      setTimeout(() => {
        this.row.nativeElement.focus();
        this.row.nativeElement.blur();
      });
    });
    this.contactInfoFormGroupControls.get('mobile2').valueChanges.subscribe((mobileNumber2: string) => {
      this.setPhoneNumberLengthByCountryCode(this.contactInfoFormGroupControls.get('mobile2CountryCode').value);
    });
    this.onCustomerTypeChanges();
  }

  onResetFilter(type) {
    if (type == 'modalClose') {
      return;
    }
    this.columnFilterForm.patchValue({
      customerRefNo: "",
      customerTypeName: "",
      email: "",
      firstName: "",
      fullAddress: "",
      lastName: "",
      mobile1: "",
    });
    this.selectedExistingCustomerProfile = { isOpenLeads: false };
    this.getExistingLeads(type);
  }

  getExistingLeads(actionType: string = 'get'): void {
    const emailFormControl = this.contactInfoFormGroupControls.get("email");
    const mobile1FormControl = this.contactInfoFormGroupControls.get("mobile1");
    const firstNameFormControl = this.basicInfoFormGroupControls.get("firstName");
    const lastNameFormControl = this.basicInfoFormGroupControls.get("lastName");
    if (actionType == 'get') {
      if (((!emailFormControl.value || !firstNameFormControl.value || !lastNameFormControl.value || !mobile1FormControl.value) ||
        ((mobile1FormControl?.value?.replace(/\s/g, "").length !== 9 || ((emailFormControl.value !== '' && emailFormControl.invalid) ||
          (emailFormControl.value == '' && emailFormControl.valid))))) ||
        (!emailFormControl.dirty && !mobile1FormControl.dirty && !firstNameFormControl.dirty && !lastNameFormControl.dirty) ||
        this.isExistingLeadModalClosed) {
        return;
      }
    }
    this.addressInfoFormGroupControls.get("formatedAddress").disable({ emitEvent: false, onlySelf: true });
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_DUPLICATE_LEADS,
        null,
        null,
        prepareRequiredHttpParams({
          firstName: firstNameFormControl.value,
          lastName: lastNameFormControl.value,
          email: emailFormControl.value,
          mobile1: mobile1FormControl.value.toString().replace(/\s/g, ""),
          leadId: ''
        })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            response.resources.forEach((resp, index) => {
              resp.index = index;
              resp.isChecked = false;
            });
            this.dataList = response.resources;
            this.dataListCopy = response.resources;
            this.dataListSecondCopy = response.resources;
            this.totalRecords = response.resources.length;
            this.isNewProfileBtnDisabled = this.totalRecords == 0 ? true : false;
            if (response.resources.length > 0) {
              $(this.existing_stop_and_knock_modal.nativeElement).modal("show");
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  onChangeCustomerSelection(rowData) {
    this.isResetBtnDisabled = false;
    this.selectedExistingCustomerProfile = rowData;
    if (
      this.contactInfoFormGroupControls.get("email").value ==
      rowData["email"] &&
      this.contactInfoFormGroupControls
        .get("mobile1")
        .value.replace(/\s/g, "") == rowData["mobile1"] &&
      this.basicInfoFormGroupControls.get("firstName").value ==
      rowData["firstName"] &&
      this.basicInfoFormGroupControls.get("lastName").value ==
      rowData["lastName"]
    ) {
      this.isNewProfileBtnDisabled = true;
    } else {
      this.isNewProfileBtnDisabled = false;
    }
    this.dataList.forEach((dataObj) => {
      dataObj["isChecked"] =
        rowData["index"] == dataObj["index"] ? true : false;
    });
  }

  onExistingCustomerPopupBtnClick(type) {
    this.stopKnockForm.get("leadValidationType").setValue(type);
    let selectedExistingCustomerProfileCopy = JSON.parse(
      JSON.stringify(this.selectedExistingCustomerProfile)
    );
    switch (type) {
      case "Use Existing Customer":
        delete selectedExistingCustomerProfileCopy.isOpenLeads;
        this.contactInfoFormGroupControls.patchValue(
          new ContactInfoModel(selectedExistingCustomerProfileCopy)
        );
        this.basicInfoFormGroupControls.patchValue(
          this.selectedExistingCustomerProfile
        );
        this.stopKnockForm
          .get("customerId")
          .setValue(this.selectedExistingCustomerProfile["customerId"]);
        this.rxjsService.setFormChangeDetectionProperty(true);
        break;
      case "Use Existing Lead":
        this.router.navigate(["/sales/task-management/leads/lead-info/view"], {
          queryParams: {
            leadId: this.selectedExistingCustomerProfile["leadId"],
          },
        });
        break;
      case "Use Existing Customer Detail":
        let queryParams = { addressId: this.selectedExistingCustomerProfile['addressId'] };
        if (this.feature && this.featureIndex) {
          queryParams['feature_name'] = this.feature;
          queryParams['featureIndex'] = this.featureIndex;
        }
        this.router.navigate(["customer/manage-customers/view/" + this.selectedExistingCustomerProfile["customerId"]], { queryParams: queryParams }
        );
        break;
    }
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.contactInfoFormGroupControls.get('mobile2').setValidators([Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        break;
      default:
        this.contactInfoFormGroupControls.get('mobile2').setValidators([Validators.minLength(formConfigs.indianContactNumberMaxLength),
        Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        break;
    }

  }

  createStockKnockForm() {
    let stopKnockModel = new StopAndKnockModel();
    this.stopKnockForm = this.formBuilder.group({
    });
    Object.keys(stopKnockModel).forEach((key) => {
      switch (key) {
        case "addressInfo":
          this.stopKnockForm.addControl(key, this.createAndGetAddressForm());
          break;
        case "contactInfo":
          this.stopKnockForm.addControl(key, this.createContactInfoForm());
          break;
        case "basicInfo":
          this.stopKnockForm.addControl(key, this.createBasicInfoForm());
          break;
        default:
          this.stopKnockForm.addControl(key, new FormControl(stopKnockModel[key]));
          break;
      }
    });
  }

  createStockKnockAddtionalOptionsForm() {
    let stopAndKnockAdditionalOptionsModel = new StopAndKnockAdditionalOptionsModel();
    this.stopKnockAdditionalOptionsForm = this.formBuilder.group({});
    Object.keys(stopAndKnockAdditionalOptionsModel).forEach((key) => {
      this.stopKnockAdditionalOptionsForm.addControl(key, new FormControl(stopAndKnockAdditionalOptionsModel[key]));
    });
  }

  createAndGetAddressForm(stopAndKnockAddressModel?: StopAndKnockAddressModel): FormGroup {
    let addressModel = new StopAndKnockAddressModel(stopAndKnockAddressModel);
    let formControls = {};
    Object.keys(addressModel).forEach((key) => {
      formControls[key] = new FormControl({ value: addressModel[key], disabled: true });
    });
    return this.formBuilder.group(formControls);
  }

  createBasicInfoForm(basicInfo?: StopAndKnockBasicInfoModel): FormGroup {
    let basicInfoModel = new StopAndKnockBasicInfoModel(basicInfo);
    let formControls = {};
    Object.keys(basicInfoModel).forEach((key) => {
      formControls[key] = new FormControl(basicInfoModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createContactInfoForm(contactInfo?: StopAndKnockContactInfoModel): FormGroup {
    let contactInfoModel = new StopAndKnockContactInfoModel(contactInfo);
    let formControls = {};
    Object.keys(contactInfoModel).forEach((key) => {
      if (key == 'email') {
        formControls[key] = new FormControl(contactInfoModel[key], Validators.compose([Validators.email, emailPattern]));
      }
      else {
        formControls[key] = new FormControl(contactInfoModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  onCustomerTypeChanges() {
    this.basicInfoFormGroupControls.get('customerTypeId').valueChanges.subscribe((customerTypeId: string) => {
      if (!customerTypeId) return;
      this.customerTypeName = this.customerTypes.find(s => s['id'] == customerTypeId).displayName;
      if (this.customerTypeName === this.commercialCustomerType) {
        this.stopKnockForm.controls['basicInfo'] = addFormControls(this.stopKnockForm.controls['basicInfo'] as FormGroup, ["companyRegNo", "companyName"]);
        this.stopKnockForm.controls['basicInfo'] = setRequiredValidator(this.stopKnockForm.controls['basicInfo'] as FormGroup, ["companyName"]);
        this.stopKnockForm.controls['basicInfo'] = removeFormControls(this.stopKnockForm.controls['basicInfo'] as FormGroup, ["said"]);
      }
      else {
        this.stopKnockForm.controls['basicInfo'] = addFormControls(this.stopKnockForm.controls['basicInfo'] as FormGroup, ["said"]);
        this.stopKnockForm.controls['basicInfo'] = removeFormControls(this.stopKnockForm.controls['basicInfo'] as FormGroup, ["companyRegNo", "companyName"]);
      }
    });
  }

  onUserGroupChanges() {
    this.stopKnockAdditionalOptionsForm.get('agentUserGroupId').valueChanges.subscribe((agentUserGroupId: string) => {
      if (!agentUserGroupId) return;
      this.getUsersByUserGroupId('agent', agentUserGroupId);
    });
    this.stopKnockAdditionalOptionsForm.get('sellerUserGroupId').valueChanges.subscribe((sellerUserGroupId: string) => {
      if (!sellerUserGroupId) return;
      this.getUsersByUserGroupId('seller', sellerUserGroupId);
    });
    this.stopKnockAdditionalOptionsForm.get('agentUserId').valueChanges.subscribe((agentUserId: string) => {
      if (!agentUserId) return;
      this.currentAgentName = this.agentGroupedUsers.find(g => g['id'] == agentUserId).displayName;
    });
    this.stopKnockAdditionalOptionsForm.get('sellerUserId').valueChanges.subscribe((sellerUserId: string) => {
      if (!sellerUserId) return;
      this.currentSellerName = this.sellerGroupedUsers.find(g => g['id'] == sellerUserId).displayName;
    });
  }

  getUsersByUserGroupId(fromType = 'agent', groupId: string) {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_USERS,
      prepareRequiredHttpParams({ groupId })).subscribe((res) => {
        if (res.resources && res.statusCode == 200 && res.isSuccess) {
          if (fromType == 'agent') {
            this.agentGroupedUsers = res.resources;
            if (res.resources.length == 0) {
              this.stopKnockAdditionalOptionsForm.get('agentUserId').setValue("");
            }
          }
          else if (fromType == 'seller') {
            this.sellerGroupedUsers = res.resources;
            if (res.resources.length == 0) {
              this.stopKnockAdditionalOptionsForm.get('sellerUserId').setValue("");
            }
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  get addressInfoFormGroupControls(): FormGroup {
    if (!this.stopKnockForm) return;
    return this.stopKnockForm.get('addressInfo') as FormGroup;
  }

  get basicInfoFormGroupControls(): FormGroup {
    if (!this.stopKnockForm) return;
    return this.stopKnockForm.get('basicInfo') as FormGroup;
  }

  get contactInfoFormGroupControls(): FormGroup {
    if (!this.stopKnockForm) return;
    return this.stopKnockForm.get('contactInfo') as FormGroup;
  }

  getStopKnockDetailsById() {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.STOP_KNOCK,
      this.stopAndKnockId);
  }

  onBtnClick(type: string) {
    $(this.all_options_modal.nativeElement).modal('show');
    this.isScheduleFollowupBtnDisabled = false;
    switch (type) {
      case 'notes':
        this.popupCaption = 'Add Notes';
        this.stopKnockAdditionalOptionsForm = removeFormControls(this.stopKnockAdditionalOptionsForm, ["followupDateAndTime",
          "notes", "contactNo", "contactNoCountryCode", "agentUserGroupId", "agentUserId", "sellerUserGroupId", "sellerUserId"]);
        this.stopKnockAdditionalOptionsForm = addFormControls(this.stopKnockAdditionalOptionsForm, [{ controlName: "notes" }]);
        this.stopKnockAdditionalOptionsForm = setRequiredValidator(this.stopKnockAdditionalOptionsForm, ['notes']);
        break;
      case 'followup':
        this.popupCaption = 'Schedule Follow-Up';
        this.getCustomerMobileNumbers();
        this.stopKnockAdditionalOptionsForm = removeFormControls(this.stopKnockAdditionalOptionsForm, ["notes", "agentUserGroupId", "agentUserId", "sellerUserGroupId", "sellerUserId"]);
        this.stopKnockAdditionalOptionsForm = addFormControls(this.stopKnockAdditionalOptionsForm, ["followupDateAndTime",
          "notes", "contactNo", "contactNoCountryCode"]);
        this.stopKnockAdditionalOptionsForm = setRequiredValidator(this.stopKnockAdditionalOptionsForm, ["followupDateAndTime"]);
        break;
      case 'agent':
        this.popupCaption = 'Assign Agent';
        this.getUserGroups();
        this.stopKnockAdditionalOptionsForm = removeFormControls(this.stopKnockAdditionalOptionsForm, ["followupDateAndTime",
          "notes", "contactNo", "contactNoCountryCode"]);
        this.stopKnockAdditionalOptionsForm = addFormControls(this.stopKnockAdditionalOptionsForm, ["agentUserGroupId", "agentUserId", "sellerUserGroupId", "sellerUserId"]);
        this.stopKnockAdditionalOptionsForm = setRequiredValidator(this.stopKnockAdditionalOptionsForm, ["agentUserGroupId", "agentUserId"]);
        this.onUserGroupChanges();
        break;
      case 'equipment':
        $(this.all_options_modal.nativeElement).modal('hide');
        if (this.fromUrl == 'Stop And Knock List') {
          this.router.navigate(['/sales/stop-and-knock/add-edit/equipment-removal'], { queryParams: { stopAndKnockId: this.stopAndKnockId } })
        }
        else {
          this.router.navigate(['/my-tasks/my-sales/my-stop-and-knocks/add-edit/equipment-removal'], { queryParams: { stopAndKnockId: this.stopAndKnockId } })
        }
        break;
    }
  }

  getUserGroups() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_GROUPS, undefined, true).subscribe((res) => {
      if (res.resources && res.statusCode == 200 && res.isSuccess) {
        this.userGroups = res.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getCustomerMobileNumbers() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.STOP_AND_KNOCK_CUSTOMER, this.stopAndKnockId).subscribe((res) => {
      if (res.resources && res.statusCode == 200 && res.isSuccess) {
        this.customerMobileNumbersObj = res.resources;
        this.customerMobileNumbersObj.isMobile1Selected = true;
        this.customerMobileNumbersObj.isMobile2Selected = false;
        this.customerMobileNumbersObj.isOfficeSelected = false;
        this.customerMobileNumbersObj.isPremisesSelected = false;
        this.objectValueChanges(this.customerMobileNumbersObj, 'isMobile1Selected');
        this.isScheduleFollowupBtnDisabled = this.popupCaption == 'Schedule Follow-Up' && !res.resources.mobile1 &&
          !res.resources.mobile2 && !res.resources.office && !res.resources.premises ? true : false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  validateAtleastOneFieldToBeMandatory(type: string) {
    if (type === 'contactInfo') {
      if (!this.contactInfoFormGroupControls.get('mobile2').value && !this.contactInfoFormGroupControls.get('officeNo').value &&
        !this.contactInfoFormGroupControls.get('premisesNo').value) {
        this.setAtleastOneFieldRequiredError(type);
      }
      else {
        this.stopKnockForm.controls['contactInfo'] = removeFormControlError(this.stopKnockForm.controls['contactInfo'] as FormGroup, 'atleastOneOfTheFieldsIsRequired');
      }
    }
    else {
      if (this.addressInfoFormGroupControls.get('isAddressExtra').value && !this.addressInfoFormGroupControls.get('estateName').value && !this.addressInfoFormGroupControls.get('estateStreetName').value &&
        !this.addressInfoFormGroupControls.get('estateStreetNo').value) {
        this.setAtleastOneFieldRequiredError(type);
      }
      else {
        this.stopKnockForm.controls['addressInfo'] = removeFormControlError(this.stopKnockForm.controls['addressInfo'] as FormGroup, 'atleastOneOfTheFieldsIsRequired');
      }
    }
  }

  setAtleastOneFieldRequiredError(type: string) {
    this.contactInfoFormGroupControls.get('mobile2').setErrors({ atleastOneOfTheFieldsIsRequired: true });
    this.contactInfoFormGroupControls.get('officeNo').setErrors({ atleastOneOfTheFieldsIsRequired: true });
    this.contactInfoFormGroupControls.get('premisesNo').setErrors({ atleastOneOfTheFieldsIsRequired: true });
  }

  objectValueChanges(object, trueBooleanProperty): Object {
    for (let key in object) {
      if (key == trueBooleanProperty) {
        object[key] = true;
      }
      else if (typeof object[key] === 'boolean') {
        object[key] = false;
      }
    }
    return object;
  }

  onSubmit(type: string) {
    this.btnSelectionType = type;
    if (this.popupCaption == '') {
      this.isFormSubmitted = true;
      this.validateAtleastOneFieldToBeMandatory('contactInfo');
      this.validateAtleastOneFieldToBeMandatory('addressInfo');
      const isMobile1Duplicate = new FindDuplicatePipe().transform(this.contactInfoFormGroupControls.get('mobile1').value,
        this.contactInfoFormGroupControls, 'mobile1');
      const isMobile2Duplicate = new FindDuplicatePipe().transform(this.contactInfoFormGroupControls.get('mobile2').value,
        this.contactInfoFormGroupControls, 'mobile2', this.contactInfoFormGroupControls.get('mobile2CountryCode').value);
      const isOfficeNoDuplicate = new FindDuplicatePipe().transform(this.contactInfoFormGroupControls.get('officeNo').value,
        this.contactInfoFormGroupControls, 'officeNo');
      const isPremisesNoDuplicate = new FindDuplicatePipe().transform(this.contactInfoFormGroupControls.get('premisesNo').value,
        this.contactInfoFormGroupControls, 'premisesNo');
      if (type == 'reconnection') {
        this.stopKnockForm = setRequiredValidator(this.stopKnockForm, ['siteTypeId', 'leadCategoryId', "sourceId", "contactPersonTypeId"]);
        this.stopKnockForm.controls['basicInfo'] = setRequiredValidator(this.stopKnockForm.controls['basicInfo'] as FormGroup, ['customerTypeId',
          'firstName', 'lastName', 'titleId']);
        this.stopKnockForm.controls['contactInfo'] = setRequiredValidator(this.stopKnockForm.controls['contactInfo'] as FormGroup, ['mobile1', 'email']);
        if (this.stopKnockForm.invalid || this.stopKnockForm.controls['contactInfo'].invalid ||
          this.stopKnockForm.controls['basicInfo'].invalid) {
          return;
        }
      }
      else {
        this.stopKnockForm = removeFormControlError(this.stopKnockForm, "required", "sourceId");
        this.stopKnockForm.get('sourceId').markAllAsTouched();
        this.stopKnockForm.value.basicInfo.titleId = this.stopKnockForm.value.basicInfo.titleId == null ? 0 : this.stopKnockForm.value.basicInfo.titleId;
      }
      if (type !== 'update') {
        if (this.stopKnockForm.invalid || isMobile1Duplicate || isMobile2Duplicate || isOfficeNoDuplicate || isPremisesNoDuplicate) {
          this.stopKnockForm.get('leadCategoryId').markAllAsTouched();
          return;
        }
        if (this.stopKnockForm.invalid) {
          return;
        }
      }
      else {
        this.stopKnockForm.clearValidators();
        this.stopKnockForm.updateValueAndValidity();
        if (!this.stopKnockForm.value.contactInfo.mobile1 || this.stopKnockForm.value.contactInfo.mobile1 == '') {
          this.stopKnockForm.value.contactInfo.mobile1 = 0;
        }
      }
      if (!this.contactInfoFormGroupControls.get('mobile2').value) {
        delete this.stopKnockForm.value.contactInfo.mobile2CountryCode;
      }
      if (!this.contactInfoFormGroupControls.get('premisesNo').value) {
        delete this.stopKnockForm.value.contactInfo.premisesNoCountryCode;
      }
      if (!this.contactInfoFormGroupControls.get('officeNo').value) {
        delete this.stopKnockForm.value.contactInfo.officeNoCountryCode;
      }
      this.stopKnockForm.value.contactInfo.mobile1 = this.stopKnockForm.value.contactInfo.mobile1.toString().replace(/\s/g, "");
      if (this.stopKnockForm.value.contactInfo.mobile2) {
        this.stopKnockForm.value.contactInfo.mobile2 = this.stopKnockForm.value.contactInfo.mobile2.toString().replace(/\s/g, "");
      }
      if (this.stopKnockForm.value.contactInfo.officeNo) {
        this.stopKnockForm.value.contactInfo.officeNo = this.stopKnockForm.value.contactInfo.officeNo.toString().replace(/\s/g, "");
      }
      if (this.stopKnockForm.value.contactInfo.premisesNo) {
        this.stopKnockForm.value.contactInfo.premisesNo = this.stopKnockForm.value.contactInfo.premisesNo.toString().replace(/\s/g, "");
      }
      this.stopKnockForm.value.modifiedUserId = this.loggedInUserData.userId;
    }
    else if (this.popupCaption !== '' && this.stopKnockAdditionalOptionsForm.invalid) {
      return;
    }
    if (this.stopKnockForm.get('serviceTerminatedDate').value) {
      this.stopKnockForm.value.serviceTerminatedDate = this.datePipe.transform(this.stopKnockForm.value.serviceTerminatedDate, SERVER_REQUEST_DATE_TRANSFORM);
    }
    if(this.stopKnockForm.value.leadCategoryId){
      let selectedleadCategoryId = [];
      this.stopKnockForm.value?.leadCategoryId?.forEach(result=>{
       selectedleadCategoryId.push(result?.id);
      })
      this.stopKnockForm.value.leadCategoryId = selectedleadCategoryId;
    }
    let crudService: Observable<IApplicationResponse>;
    switch (type) {
      case "update":
        crudService = this.crudService.update(ModulesBasedApiSuffix.SALES,
          SalesModuleApiSuffixModels.STOP_KNOCK, this.stopKnockForm.value);
        break;
      case "reconnection":
        this.stopKnockForm.value.leadGroupId = LeadGroupTypes.RECONNECTION;
        crudService = this.crudService.create(ModulesBasedApiSuffix.SALES,
          SalesModuleApiSuffixModels.LEADS_RECONNECTION, this.stopKnockForm.value);
        break;
      case "Add Notes":
        crudService = this.crudService.create(ModulesBasedApiSuffix.SALES,
          SalesModuleApiSuffixModels.STOP_AND_KNOCK_ADD_NOTES, this.stopKnockAdditionalOptionsForm.value);
        break;
      case 'Schedule Follow-Up':
        var followupDate = new Date(this.stopKnockAdditionalOptionsForm.value.followupDateAndTime);
        let followupTime =
          ("00" + followupDate.getHours()).slice(-2) + ":" +
          ("00" + followupDate.getMinutes()).slice(-2) + ":" +
          ("00" + followupDate.getSeconds()).slice(-2);
        this.stopKnockAdditionalOptionsForm.value.followupDate = followupDate;
        this.stopKnockAdditionalOptionsForm.value.followupTime = followupTime;
        firstLoop: for (let key in this.customerMobileNumbersObj) {
          if (typeof this.customerMobileNumbersObj[key] === 'boolean' && this.customerMobileNumbersObj[key]) {
            secondLoop: for (let nestedKey in this.customerMobileNumbersObj) {
              if (key.toLowerCase().includes(nestedKey)) {
                this.stopKnockAdditionalOptionsForm.value.contactNo = this.customerMobileNumbersObj[nestedKey];
                this.stopKnockAdditionalOptionsForm.value.contactNoCountryCode = this.customerMobileNumbersObj[nestedKey + 'CountryCode'];
                break secondLoop;
              }
            }
            break firstLoop;
          }
        }
        crudService = this.crudService.create(ModulesBasedApiSuffix.SALES,
          SalesModuleApiSuffixModels.STOP_AND_KNOCK_SCHEDULE_CALLBACK, this.stopKnockAdditionalOptionsForm.value);
        break;
      case "Assign Agent":
        crudService = this.crudService.create(ModulesBasedApiSuffix.SALES,
          SalesModuleApiSuffixModels.STOP_AND_KNOCK_ASSIGN_AGENT, this.stopKnockAdditionalOptionsForm.value);
        break;
    }
    this.stopKnockAdditionalOptionsForm.value.createdUserId = this.loggedInUserData.userId;
    this.stopKnockAdditionalOptionsForm.value.stopAndKnockId = this.stopAndKnockId;
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.popupCaption = type;
        if (this.popupCaption == 'Add Notes') {
          this.stopAndKnockNotes.unshift(response.resources);
        }
        if (this.popupCaption == 'reconnection' || this.popupCaption == 'update') {
          this.redirectToListPage();
        }
        $(this.all_options_modal.nativeElement).modal('hide');
        this.popupCaption = "";
      }
      else if (!response.isSuccess && response.statusCode === 409 && response.exceptionMessage === 'Already customer profile exist, Please link customer') {
        $(this.existing_stop_and_knock_modal.nativeElement).modal("show");
        response.resources.forEach((resp, index) => {
          resp.index = index;
        });
        this.dataList = [];
        this.dataList = response.resources;
        this.selectedExistingCustomerProfile = this.dataList.find((d) => d["isChecked"]);
        this.isNewProfileBtnDisabled = true;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  redirectToListPage() {
    if (this.fromUrl == 'Stop And Knock List') {
      this.router.navigateByUrl("/sales/stop-and-knock");
    }
    else {
      this.router.navigate(['my-tasks/my-sales'],
        { queryParams: { tab: 3, } })
    }
  }

  openModal() {}
  
  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {} 
}