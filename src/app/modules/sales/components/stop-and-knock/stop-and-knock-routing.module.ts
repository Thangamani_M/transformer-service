import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StopAndKnockAddEditComponent } from './stop-and-knock-add-edit.component';
import { StopAndKnockListComponent } from './stop-and-knock-list.component';
import { StopAndKnockRemoveEquipmentComponent } from './stop-and-knock-remove-equipment.component';
import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: 'add-edit', component: StopAndKnockAddEditComponent, data: { title: 'Stop and Knock Add Edit' }, canActivate: [AuthGuard] },
  { path: '', component: StopAndKnockListComponent, data: { title: 'Stop and Knock List' }, canActivate: [AuthGuard] },
  { path: 'add-edit/equipment-removal', component: StopAndKnockRemoveEquipmentComponent, data: { title: 'Stop and Knock Equipment Removal' }, canActivate: [AuthGuard] }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class StopAndKnockRoutingModule { }
