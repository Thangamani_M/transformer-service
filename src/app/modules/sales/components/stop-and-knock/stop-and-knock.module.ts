import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { StopAndKnockAddEditComponent } from './stop-and-knock-add-edit.component';
import { StopAndKnockListComponent } from './stop-and-knock-list.component';
import { StopAndKnockRemoveEquipmentComponent } from './stop-and-knock-remove-equipment.component';
import { StopAndKnockRoutingModule } from './stop-and-knock-routing.module';
@NgModule({
  declarations: [StopAndKnockAddEditComponent, StopAndKnockListComponent, StopAndKnockRemoveEquipmentComponent],
  imports: [
    CommonModule,
    StopAndKnockRoutingModule,
    CommonModule, MaterialModule, SharedModule,
    ReactiveFormsModule, FormsModule
  ],
  entryComponents: [],
  exports: []
})
export class StopAndKnockModule { }
