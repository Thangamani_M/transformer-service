class SmsCampaignsmsCampaignStageOneModel {
    constructor(smsCampaignStageOneModel?: SmsCampaignsmsCampaignStageOneModel) {
        this.marketingCampaignId = smsCampaignStageOneModel ? smsCampaignStageOneModel.marketingCampaignId == undefined ? '' : smsCampaignStageOneModel.marketingCampaignId : '';
        this.startDate = smsCampaignStageOneModel ? smsCampaignStageOneModel.startDate == undefined ? '' : smsCampaignStageOneModel.startDate : '';
        this.endDate = smsCampaignStageOneModel ? smsCampaignStageOneModel.endDate == undefined ? '' : smsCampaignStageOneModel.endDate : '';

        this.regionIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.regionIds == undefined ? [] : smsCampaignStageOneModel.regionIds : [];
        this.divisionIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.divisionIds == undefined ? [] : smsCampaignStageOneModel.divisionIds : [];
        this.districtIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.districtIds == undefined ? [] : smsCampaignStageOneModel.districtIds : [];
        this.branchIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.branchIds == undefined ? [] : smsCampaignStageOneModel.branchIds : [];
        this.salesAreaIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.salesAreaIds == undefined ? [] : smsCampaignStageOneModel.salesAreaIds : [];
        this.subBranchIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.subBranchIds == undefined ? [] : smsCampaignStageOneModel.subBranchIds : [];
        this.mainAreaIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.mainAreaIds == undefined ? '' : smsCampaignStageOneModel.mainAreaIds : '';
        this.subAreaIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.subAreaIds == undefined ? '' : smsCampaignStageOneModel.subAreaIds : '';
        this.originIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.originIds == undefined ? [] : smsCampaignStageOneModel.originIds : [];

        this.installOriginIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.installOriginIds == undefined ? [] : smsCampaignStageOneModel.installOriginIds : [];
        this.technicalInstallationAreaIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.technicalInstallationAreaIds == undefined ? [] : smsCampaignStageOneModel.technicalInstallationAreaIds : [];
        this.technicalServiceAreaIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.technicalServiceAreaIds == undefined ? [] : smsCampaignStageOneModel.technicalServiceAreaIds : [];
        this.itemOwnershipTypeIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.itemOwnershipTypeIds == undefined ? [] : smsCampaignStageOneModel.itemOwnershipTypeIds : [];

        this.customerTypeIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.customerTypeIds == undefined ? [] : smsCampaignStageOneModel.customerTypeIds : [];
        this.siteTypeIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.siteTypeIds == undefined ? [] : smsCampaignStageOneModel.siteTypeIds : [];
        this.lssIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.lssIds == undefined ? [] : smsCampaignStageOneModel.lssIds : [];
        this.sourceIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.sourceIds == undefined ? [] : smsCampaignStageOneModel.sourceIds : [];
        this.serviceCategoryIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.serviceCategoryIds == undefined ? [] : smsCampaignStageOneModel.serviceCategoryIds : [];
        this.serviceIds = smsCampaignStageOneModel ? smsCampaignStageOneModel.serviceIds == undefined ? [] : smsCampaignStageOneModel.serviceIds : [];

        this.isDealerRented = smsCampaignStageOneModel ? smsCampaignStageOneModel.isDealerRented == undefined ? false : smsCampaignStageOneModel.isDealerRented : false;
        this.isDealerOwned = smsCampaignStageOneModel ? smsCampaignStageOneModel.isDealerOwned == undefined ? false : smsCampaignStageOneModel.isDealerOwned : false;
        this.isCPAProtected = smsCampaignStageOneModel ? smsCampaignStageOneModel.isCPAProtected == undefined ? false : smsCampaignStageOneModel.isCPAProtected : false;

        this.contractPeriodId = smsCampaignStageOneModel ? smsCampaignStageOneModel.contractPeriodId == undefined ? '' : smsCampaignStageOneModel.contractPeriodId : '';
        this.discountTypeId = smsCampaignStageOneModel ? smsCampaignStageOneModel.discountTypeId == undefined ? '' : smsCampaignStageOneModel.discountTypeId : '';
        this.motivationReasonId = smsCampaignStageOneModel ? smsCampaignStageOneModel.motivationReasonId == undefined ? '' : smsCampaignStageOneModel.motivationReasonId : '';
        this.startDate = smsCampaignStageOneModel ? smsCampaignStageOneModel.startDate == undefined ? '' : smsCampaignStageOneModel.startDate : '';
        this.startDateOperatorId = smsCampaignStageOneModel ? smsCampaignStageOneModel.startDateOperatorId == undefined ? '' : smsCampaignStageOneModel.startDateOperatorId : '';
        this.endDate = smsCampaignStageOneModel ? smsCampaignStageOneModel.endDate == undefined ? '' : smsCampaignStageOneModel.endDate : '';
        this.endDateOperatorId = smsCampaignStageOneModel ? smsCampaignStageOneModel.endDateOperatorId == undefined ? '' : smsCampaignStageOneModel.endDateOperatorId : '';
        this.birthdayDate = smsCampaignStageOneModel ? smsCampaignStageOneModel.birthdayDate == undefined ? '' : smsCampaignStageOneModel.birthdayDate : '';

        this.createdUserId = smsCampaignStageOneModel ? smsCampaignStageOneModel.createdUserId == undefined ? null : smsCampaignStageOneModel.createdUserId : null;
        this.eventCode = smsCampaignStageOneModel ? smsCampaignStageOneModel.eventCode == undefined ? '' : smsCampaignStageOneModel.eventCode : '';
    }
    marketingCampaignId?: string;
    regionIds?: any;
    divisionIds?: any;
    districtIds?: any;
    branchIds?: any;
    salesAreaIds?: any;
    subBranchIds?: any;
    mainAreaIds?: any;
    // marketingCampaignMediumIds?: any;
    subAreaIds?: any;
    originIds?: any;
    installOriginIds: any
    technicalInstallationAreaIds: any;
    technicalServiceAreaIds: any;
    isDealerRented: boolean
    isDealerOwned: boolean
    itemOwnershipTypeIds: any
    customerTypeIds: any
    lssIds: any
    siteTypeIds: any
    sourceIds: any
    isCPAProtected: boolean
    contractPeriodId:string
    serviceCategoryIds:any
    serviceIds:any
    discountTypeId:string
    motivationReasonId:string
    startDate:any
    startDateOperatorId:string
    endDate:any
    endDateOperatorId:string
    birthdayDate:any
    createdUserId?: string;
    eventCode:string
}


export { SmsCampaignsmsCampaignStageOneModel }
