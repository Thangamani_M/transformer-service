import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SmsCampaignRoutingModule } from './sms-campaign-routing.module';
import { SmsCampaignListComponent } from './sms-campaign-list.component';
import { SharedModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SmsCampaignStageOneComponent } from './sms-campaign-stage-one.component';
import { SmsCampaignStageTwoComponent } from './sms-campaign-stage-two.component';
import { SmsCampaignStageThreeComponent } from './sms-campaign-stage-three.component';
import { SmsCampaignStageOneViewComponent } from './sms-campaign-stage-one-view.component';
import { SmsCampaignStageThreeViewComponent } from './sms-campaign-stage-three-view.component';


@NgModule({
  declarations: [SmsCampaignListComponent, SmsCampaignStageOneComponent, SmsCampaignStageTwoComponent, SmsCampaignStageThreeComponent, SmsCampaignStageOneViewComponent, SmsCampaignStageThreeViewComponent],
  imports: [
    CommonModule,
    SmsCampaignRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SmsCampaignModule { }
