import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-sms-campaign-stage-three-view',
  templateUrl: './sms-campaign-stage-three-view.component.html'
})
export class SmsCampaignStageThreeViewComponent implements OnInit {
  marketingCampaignId = "";
  smsCampaignDetails;
  smsCampaignId = "";

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService,
    private crudService: CrudService) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.getdelayDurationDetailsById(this.smsCampaignId);
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([this.activatedRoute.queryParams]
    ).subscribe((response) => {
      if (response[0]) {
        this.marketingCampaignId = response[0].id;
        this.smsCampaignId = response[0].smsCampaignId;
      }
    });
  }

  getdelayDurationDetailsById(smsCampaignId: string) {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SMS_CAMPAIGN_SCHEDULES, smsCampaignId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.smsCampaignDetails = response.resources;
          this.smsCampaignId = response.resources?.smsCampaignId;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  onArrowClick(type: string) {
    if (type == 'previous') {
      this.router.navigate(['sales/sms-campaign/stage-two'], { queryParams: { id: this.marketingCampaignId, smsCampaignId: this.smsCampaignId, isSubmitted: true } });
    }
  }
}
