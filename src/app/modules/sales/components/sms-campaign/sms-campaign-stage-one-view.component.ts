
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-sms-campaign-stage-one-view',
  templateUrl: './sms-campaign-stage-one-view.component.html'
})
export class SmsCampaignStageOneViewComponent implements OnInit {
  marketingCampaignId = "";
  smsCampaignDetails;
  smsCampaignId = "";

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService,
     private crudService: CrudService) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([this.activatedRoute.queryParams]
    ).subscribe((response) => {
      if (response[0]) {
        this.marketingCampaignId = response[0]?.id;
        this.getdelayDurationDetailsById();
      }
    });
  }

  getdelayDurationDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SMS_CAMPAIGN, this.marketingCampaignId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.smsCampaignDetails = response.resources;
          this.smsCampaignId = response.resources?.smsCampaignId;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onArrowClick() {
    this.router.navigate(['sales/sms-campaign/stage-two'], { queryParams: { id: this.marketingCampaignId, smsCampaignId: this.smsCampaignId, isSubmitted: true } });
  }
}