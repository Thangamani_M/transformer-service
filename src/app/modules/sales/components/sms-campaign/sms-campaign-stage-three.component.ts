import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, countryCodes, CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-sms-campaign-stage-three',
  templateUrl: './sms-campaign-stage-three.component.html'
})
export class SmsCampaignStageThreeComponent implements OnInit {
  isNumericOnly = new CustomDirectiveConfig({ isANumberWithZero: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  smsCampaignStageThreeForm: FormGroup;
  departmentDropDown = [];
  roleDropDown = [];
  userDropDown = [];
  loggedInUserData: LoggedInUserModel;
  countryCodes = countryCodes;
  todayDate: any = new Date()
  todayStartTime: any = new Date()
  todayEndTime: any = new Date()
  marketingCampaignId = ""
  smsCampaignId = "";

  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private momentService: MomentService, private httpCancelService: HttpCancelService, private router: Router,
    private store: Store<AppState>, private _fb: FormBuilder, private rxjsService: RxjsService) {
  }

  ngOnInit(): void {
    this.todayStartTime = this.momentService.startOfDayTime(new Date())
    this.todayEndTime = this.momentService.endOfDayTime(new Date())
    this.combineLatestNgrxStoreData();
    this.getForkJoinRequests();
    this.smsCampaignStageThreeForm = this._fb.group({
      smsCampaignId: [this.smsCampaignId, Validators.required],
      totalRecords: [0, Validators.required],
      scheduleSendDate: ['', Validators.required],
      scheduleSendTime: ['', Validators.required],
      messsage: ['', Validators.required],
      isBatch: [false],
      isResponse: [false],
      isAllocateResponse: [false],
      batchSize: [''],
      frequency: [''],
      batchStartTime: [''],
      batchEndTime: [''],
      endTimeMinDate: [''],
      responseEndDate: [''],
      responseEndTime: [''],
      departmentId: [''],
      roleId: [''],
      userId: [''],
      createdUserId: [this.loggedInUserData.userId, Validators.required],
      creaedUserName: [this.loggedInUserData.displayName]
    });
    this.smsCampaignStageThreeForm.get('isBatch').valueChanges.subscribe((isBatch: boolean) => {
      if (isBatch) {
        this.smsCampaignStageThreeForm = setRequiredValidator(this.smsCampaignStageThreeForm, ['batchSize', 'frequency', 'batchStartTime', 'batchEndTime']);
      }
      else {
        this.smsCampaignStageThreeForm = clearFormControlValidators(this.smsCampaignStageThreeForm, ['batchSize', 'frequency', 'batchStartTime', 'batchEndTime']);
      }
    });
    this.smsCampaignStageThreeForm.get('isResponse').valueChanges.subscribe((isResponse: boolean) => {
      if (isResponse) {
        this.smsCampaignStageThreeForm = setRequiredValidator(this.smsCampaignStageThreeForm, ['responseEndDate', 'responseEndTime']);
      }
      else {
        this.smsCampaignStageThreeForm = clearFormControlValidators(this.smsCampaignStageThreeForm, ['responseEndDate', 'responseEndTime']);
      }
    });
    this.smsCampaignStageThreeForm.get('isAllocateResponse').valueChanges.subscribe((isAllocateResponse: boolean) => {
      if (isAllocateResponse) {
        this.smsCampaignStageThreeForm = setRequiredValidator(this.smsCampaignStageThreeForm, ['departmentId', 'roleId', 'userId']);
      }
      else {
        this.smsCampaignStageThreeForm = clearFormControlValidators(this.smsCampaignStageThreeForm, ['departmentId', 'roleId', 'userId']);
      }
    });

    this.smsCampaignStageThreeForm.get('batchStartTime').valueChanges.subscribe((batchStartTime: any) => {
      if (!batchStartTime) {
        return
      }

      let endTimeMinTime = this.momentService.toMoment(this.momentService.addMinits(batchStartTime, 1)).seconds(0).format();
      this.smsCampaignStageThreeForm.get('endTimeMinDate').setValue(endTimeMinTime, { emitEvent: false });
      this.smsCampaignStageThreeForm.get('batchEndTime').setValue(null, { emitEvent: false });
      this.smsCampaignStageThreeForm.updateValueAndValidity()
    });
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      if (response[1]) {
        this.marketingCampaignId = response[1].id;
        this.smsCampaignId = response[1].smsCampaignId;
        this.getScheduleDetailsById();
      }
    });
  }

  getForkJoinRequests() {
    forkJoin([this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DEPARTMENTS, null, false, null),
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_ROLES, null, false, null),
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DEPARTMENT_ROLE_USERS, null, false, null)])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
            switch (ix) {
              case 0:
                this.departmentDropDown = respObj.resources;
                break;
              case 1:
                this.roleDropDown = respObj.resources;
                break;
              case 2:
                this.userDropDown = respObj.resources;
                break;
            }
            if (ix == response.length - 1) {
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          }
        });
      });
  }

  getScheduleDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SMS_CAMPAIGN_SCHEDULES, this.smsCampaignId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          response.resources.isBatch = response.resources.isBatch ? 'true' : 'false'
          response.resources.isAllocateResponse = response.resources.isAllocateResponse ? 'true' : 'false'
          response.resources.isResponse = response.resources.isResponse ? 'true' : 'false'
          response.resources.scheduleSendDate = response.resources.scheduleSendDate ? new Date(response.resources.scheduleSendDate) : null
          response.resources.responseEndDate = response.resources.responseEndDate ? new Date(response.resources.responseEndDate) : null
          response.resources.scheduleSendTime = response.resources.scheduleSendTime ? this.momentService.setTimetoRequieredDate(new Date(), response.resources.scheduleSendTime) : null
          response.resources.batchEndTime = response.resources.batchEndTime ? this.momentService.setTimetoRequieredDate(new Date(), response.resources.batchEndTime) : null
          response.resources.batchStartTime = response.resources.batchStartTime ? this.momentService.setTimetoRequieredDate(new Date(), response.resources.batchStartTime) : null
          response.resources.responseEndTime = response.resources.responseEndTime ? this.momentService.setTimetoRequieredDate(new Date(), response.resources.responseEndTime) : null
          this.smsCampaignStageThreeForm.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.smsCampaignStageThreeForm.invalid) {
      return;
    }
    let payload = this.smsCampaignStageThreeForm.value;
    payload.scheduleSendDate = payload.scheduleSendDate.toDateString();
    payload.scheduleSendTime = payload.scheduleSendTime ? this.momentService.toFormateType(payload.scheduleSendTime, 'HH:mm') : null;
    payload.batchStartTime = payload.batchStartTime ? this.momentService.toFormateType(payload.batchStartTime, 'HH:mm') : null;
    payload.batchEndTime = payload.batchEndTime ? this.momentService.toFormateType(payload.batchEndTime, 'HH:mm') : null;
    payload.responseEndDate = payload.responseEndDate ? payload.responseEndDate.toDateString() : null;
    payload.responseEndTime = payload.responseEndTime ? this.momentService.toFormateType(payload.responseEndTime, 'HH:mm') : null;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SMS_CAMPAIGN_SCHEDULES, payload)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.router.navigateByUrl('/sales/sms-campaign');
      }
    });
  }

  onArrowClick() {
    this.router.navigate(['/sales/sms-campaign/stage-two'], { queryParams: { id: this.marketingCampaignId, smsCampaignId: this.smsCampaignId } });
  }
}

