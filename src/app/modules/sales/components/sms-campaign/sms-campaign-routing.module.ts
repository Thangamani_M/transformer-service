import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SmsCampaignListComponent } from './sms-campaign-list.component';
import { SmsCampaignStageOneViewComponent } from './sms-campaign-stage-one-view.component';
import { SmsCampaignStageOneComponent } from './sms-campaign-stage-one.component';
import { SmsCampaignStageThreeViewComponent } from './sms-campaign-stage-three-view.component';
import { SmsCampaignStageThreeComponent } from './sms-campaign-stage-three.component';
import { SmsCampaignStageTwoComponent } from './sms-campaign-stage-two.component';

const routes: Routes = [
  { path:'', component: SmsCampaignListComponent, data:{title:'SMS Campaign List'} },
  { path:'stage-one', component: SmsCampaignStageOneComponent, data:{title:'SMS Campaign Stage One'} },
  { path:'stage-two', component: SmsCampaignStageTwoComponent, data:{title:'SMS Campaign Stage Two'} },
  { path:'stage-three', component: SmsCampaignStageThreeComponent, data:{title:'SMS Campaign Stage Three'} },
  { path:'stage-one-view', component: SmsCampaignStageOneViewComponent, data:{title:' View SMS Campaign Stage One'} },
  { path:'stage-three-view', component: SmsCampaignStageThreeViewComponent, data:{title:' View SMS Campaign Stage Three'} },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class SmsCampaignRoutingModule { }
