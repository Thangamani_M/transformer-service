import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CAMPAIGN_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/campaign-mngt-component';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-sms-campaign-list',
  templateUrl: './sms-campaign-list.component.html'
})
export class SmsCampaignListComponent extends PrimeNgTableVariablesModel implements OnInit {
  row: any = {};

  constructor(private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>,private snackbarService: SnackbarService,
    private datePipe: DatePipe, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "SMS Campaign",
      breadCrumbItems: [{ displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Marketing', relativeRouterUrl: '' }, { displayName: 'SMS Campaign', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'SMS Campaign',
            dataKey: 'marketingCampaignId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'campaignRefNo', header: 'Register Number', width: '200px' },
            { field: 'campaignName', header: 'Name Of Campaign', width: '200px' },
            { field: 'districtName', header: 'District', width: '200px' },
            { field: 'branchName', header: 'Branch', width: '200px' },
            { field: 'departmentName', header: 'Department', width: '200px' },
            { field: 'startDate', header: 'Valid From', width: '200px' },
            { field: 'endDate', header: 'Valid To', width: '200px' },
            { field: 'contactPerson', header: 'Contact', width: '200px' },
            { field: 'progress', header: 'Progress', width: '200px' },
            ],
            apiSuffixModel: SalesModuleApiSuffixModels.SMS_CAMPAIGN,
            moduleName: ModulesBasedApiSuffix.SALES,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          }
        ]
      }
    }
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getMarketingDataList();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CAMPAIGN_MANAGEMENT_COMPONENT.SMS_CAMPAIGN]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });

  }


  getMarketingDataList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.startDate = this.datePipe.transform(val?.startDate, 'dd-MM-yyyy, h:mm:ss a');
          val.endDate = this.datePipe.transform(val?.endDate, 'dd-MM-yyyy, h:mm:ss a');
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      if (data.isSuccess && data.statusCode == 200 && data.resources) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      }
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getMarketingDataList(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
    }
  }

  loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
    switch (type) {
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            if (editableObject?.isSubmitted) {
              this.router.navigate(["sales/sms-campaign/stage-one-view"], { queryParams: { id: editableObject['marketingCampaignId'] } });
            } else {
              this.router.navigate(["sales/sms-campaign/stage-one"], { queryParams: { id: editableObject['marketingCampaignId'] } });
            }
            break;
        }
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    this.loadActionTypes(null, false, editableObject, type);
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}