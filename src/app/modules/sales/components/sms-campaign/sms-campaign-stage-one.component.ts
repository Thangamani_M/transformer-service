import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { selectDynamicEagerLoadingMotivationReasonTypesState$, selectDynamicEagerLoadingServiceCategoriesState$, selectDynamicEagerLoadingSourcesState$, selectStaticEagerLoadingContractPeriodsState$, selectStaticEagerLoadingCustomerTypesState$, selectStaticEagerLoadingItemOwnerShipTypesState$, selectStaticEagerLoadingLssSchemeTypesState$, selectStaticEagerLoadingSiteTypesState$, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  CrudService, formConfigs, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { ItManagementApiSuffixModels, loggedInUserData } from '@modules/others';
import { ReasonForMotivationTypes } from '@modules/others/configuration/models/reason-for-motivation-model';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';
import { SmsCampaignsmsCampaignStageOneModel } from './models/sms-campaign-stage-one.model';
@Component({
  selector: 'app-sms-campaign-stage-one',
  templateUrl: './sms-campaign-stage-one.component.html'
})
export class SmsCampaignStageOneComponent implements OnInit {
  smsCampaignStageOneForm: FormGroup;
  marketingCampaignId = "";
  smsCampaignId = "";
  regionList = [];
  divisionList = []
  districtList = [];
  branchList = [];
  mainAreaList = [];
  salesAreaList = [];
  subAreaList = [];
  subBranchList = [];
  originList = [];
  installOriginList = [];
  itemOwnershipTypeList = [];
  customerTypeList = [];
  lssList = [];
  operatorList = [];
  discountList = [];
  technicianInstallationAreaList = [];
  technicalServiceAreaList = [];
  siteTypeList = [];
  sourceList = [];
  contractPeriodList = [];
  serviceTypeList = [];
  serviceList = [];
  motivationList = [];
  reasonList = [];
  todayDate = new Date();
  todayStartDate = new Date()
  formConfigs = formConfigs;
  loggedInUserData: LoggedInUserModel;

  constructor(private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private crudService: CrudService, private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreDynamicData();
    this.combineLatestNgrxStoreData();
    this.getForkJoinRequestsOne();
    this.createStageOneForm();
  }

  combineLatestNgrxStoreDynamicData() {
    combineLatest([
      this.store.select(selectDynamicEagerLoadingSourcesState$),
      this.store.select(selectStaticEagerLoadingItemOwnerShipTypesState$),
      this.store.select(selectDynamicEagerLoadingMotivationReasonTypesState$)
    ]).subscribe((response) => {
      this.sourceList = response[0];
      this.itemOwnershipTypeList = response[1];
      if (response[2]) {
        this.motivationList = response[2]?.filter(r => r.displayName == ReasonForMotivationTypes.GENERAL_DISCOUNT).map(mR => mR.motivationReasons)[0];
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingSiteTypesState$),
      this.store.select(selectStaticEagerLoadingCustomerTypesState$),
      this.store.select(selectStaticEagerLoadingContractPeriodsState$),
      this.store.select(selectStaticEagerLoadingItemOwnerShipTypesState$),
      this.store.select(selectStaticEagerLoadingLssSchemeTypesState$),
      this.store.select(selectDynamicEagerLoadingServiceCategoriesState$),
      this.activatedRoute.queryParams
    ]).pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.siteTypeList = response[1];
        this.customerTypeList = response[2];
        this.contractPeriodList = response[3];
        this.itemOwnershipTypeList = response[4];
        this.serviceTypeList = response[6]
        if (response[7]) {
          this.marketingCampaignId = response[7].id;
          this.getCampaignDetailsById();
        }
      });
  }

  getForkJoinRequestsOne() {
    forkJoin([this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS, prepareGetRequestHttpParams(null, null, { CountryId: formConfigs.countryId })),
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_ORIGINS, null, false), this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_INSTALL_ORIGIN, null, false),
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_OPERATORS, null, false),
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_DISCOUNT_TYPE, null, false),
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_EVENT_CODES, null, false)])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess) {
            switch (ix) {
              case 0:
                this.regionList = respObj.resources;
                break;
              case 1:
                this.originList = respObj.resources;
                break;
              case 2:
                this.installOriginList = respObj.resources;
                break;
              case 3:
                this.operatorList = respObj.resources;
                break;
              case 4:
                this.discountList = respObj.resources;
                break;
              case 5:
                this.reasonList = respObj.resources;
                break;
            }
          }
          if (ix == response.length - 1) {
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
      });
  }

  getSource(event): void {
    let otherParams = {}
    otherParams['SourceName'] = event.query
    otherParams['SourceGroupName'] = 'Campaign'
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_SOURCE_TYPES, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.resources) {
          if (response.resources.length > 0) {
            this.sourceList = []
            response.resources.forEach(element => {
              if (element.sources) {
                element.sources.forEach(element1 => {
                  this.sourceList.push(element1)
                })
              }
            })
            this.sourceList = [...this.sourceList]
          } else {
            this.smsCampaignStageOneForm.get('sourceIds').setValue([])
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.WARNING)
            this.sourceList = [];
          }
        } else {
          if (this.smsCampaignStageOneForm.get('sourceIds').value == 0) {
            this.smsCampaignStageOneForm.get('sourceIds').setValue([])
          }
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.WARNING)
          this.sourceList = [];
        }
      });
  }

  getCampaignDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SMS_CAMPAIGN, this.marketingCampaignId, false, null, 1)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.resources && response.statusCode == 200) {
          this.smsCampaignId = response.resources?.smsCampaignId
          response.resources.startDate = response.resources.startDate ? new Date(response.resources.startDate) : null
          response.resources.endDate = response.resources.endDate ? new Date(response.resources.endDate) : null
          response.resources.birthdayDate = response.resources.birthdayDate ? new Date(response.resources.birthdayDate) : null
          response.resources.isCPAProtected = response.resources.cpaProtected ? 'true' : 'false'
          response.resources.isDealerOwned = response.resources.dealerOwned ? 'true' : 'false'
          response.resources.isDealerRented = response.resources.dealerRented ? 'true' : 'false'
          response.resources.districtIds = this.setItems(response.resources.districtIds)
          response.resources.divisionIds = this.setItems(response.resources.divisionIds)
          response.resources.regionIds = this.setItems(response.resources.regionIds)
          response.resources.branchIds = this.setItems(response.resources.branchIds)
          response.resources.salesAreaIds = this.setItems(response.resources.salesAreaIds)
          response.resources.subBranchIds = this.setItems(response.resources.subBranchIds)
          response.resources.mainAreaIds = this.setItems(response.resources.mainAreaIds)
          response.resources.subAreaIds = this.setItems(response.resources.subAreaIds)
          response.resources.originIds = this.setItems(response.resources.originIds)
          response.resources.installOriginIds = this.setItems(response.resources.installOriginIds)
          response.resources.technicalInstallationAreaIds = this.setItems(response.resources.technicalInstallationAreaIds)
          response.resources.technicalServiceAreaIds = this.setItems(response.resources.technicalServiceAreaIds)
          response.resources.itemOwnershipTypeIds = this.setItems(response.resources.itemOwnershipTypeIds)
          response.resources.customerTypeIds = this.setItems(response.resources.customerTypeIds)
          response.resources.lssIds = this.setItems(response.resources.lssIds)
          response.resources.siteTypeIds = this.setItems(response.resources.siteTypeIds)
          response.resources.sourceName = response.resources.sourceName ? response.resources.sourceName.split(',') : null
          if (response.resources?.sourceIds?.length > 0) {
            let itesmIds = []
            response.resources.sourceIds.forEach((element, index) => {
              let data: any = {}
              data.id = element
              data.displayName = response.resources.sourceName[index]
              itesmIds.push(data)
            });
            response.resources.sourceIds = itesmIds
          }
          response.resources.serviceCategoryIds = this.setItems(response.resources.serviceCategoryIds)
          response.resources.serviceIds = this.setItems(response.resources.serviceIds)
          if (response.resources.serviceName) {
            // response.resources.serviceName.split(',')
            let serviceList = []
            response.resources.serviceIds.forEach((element, index) => {
              serviceList.push({ id: element.id, displayName: response.resources.serviceName.split(',')[index] })
            });
            this.serviceList = serviceList
          }
          response.resources.createdUserId = response.resources.createdUserId ? response.resources.createdUserId : this.loggedInUserData.userId
          this.smsCampaignStageOneForm.patchValue(response.resources)
          this.onServiceCategoryChange()
          this.smsCampaignStageOneForm.get('serviceIds').setValue(response.resources.serviceIds)
        }
      });
  }

  setItems(itemList) {
    if (!itemList) return
    let itesmIds = []
    itemList.forEach(element => {
      itesmIds.push({ id: element })
    });
    return itesmIds;
  }

  setItemsPost(itemList) {
    if (!itemList) return
    let itesmIds = []
    itemList.forEach(element => {
      itesmIds.push(element.id)
    });
    return itesmIds;
  }

  createStageOneForm(): void {
    let smsCampaignsmsCampaignStageOneModel = new SmsCampaignsmsCampaignStageOneModel();
    this.smsCampaignStageOneForm = this.formBuilder.group({});
    Object.keys(smsCampaignsmsCampaignStageOneModel).forEach((key) => {
      this.smsCampaignStageOneForm.addControl(key, new FormControl(smsCampaignsmsCampaignStageOneModel[key]));
    });
    this.smsCampaignStageOneForm = setRequiredValidator(this.smsCampaignStageOneForm, ["regionIds", "divisionIds", "districtIds", "branchIds"]);
    this.smsCampaignStageOneForm.get('createdUserId').setValue(this.loggedInUserData.userId)
    this.smsCampaignStageOneForm.get('isDealerOwned').valueChanges.subscribe(val => {
      if (val === 'true') {
        this.smsCampaignStageOneForm.get('isDealerRented').setValue('false');
      }
    });
    this.smsCampaignStageOneForm.get('isDealerRented').valueChanges.subscribe(val => {
      if (val === 'true') {
        this.smsCampaignStageOneForm.get('isDealerOwned').setValue('false');
      }
    });
    this.smsCampaignStageOneForm.get('startDate').valueChanges.subscribe(val => {
      if (val) {
        this.smsCampaignStageOneForm.get('endDate').setValue(null);
        this.smsCampaignStageOneForm.get('endDate').setValidators([Validators.required]);
        let date = new Date(val);
        date.setDate(date.getDate() + 1)
        this.todayStartDate = date;
      }
    });

    this.smsCampaignStageOneForm.get('regionIds').valueChanges.subscribe(regionId => {
      if (!regionId) return;
      if (regionId.length == 0) return;
      let regionIds = [];
      regionId.forEach(element => {
        regionIds.push(element.id)
      });
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { ids: regionIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          this.divisionList = [];
          if (response && response.resources && response.isSuccess) {
            this.divisionList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.smsCampaignStageOneForm.get('divisionIds').valueChanges.subscribe(divisionId => {
      if (!divisionId) return;
      if (divisionId.length == 0) return;
      let divisionIds = []
      divisionId.forEach(element => {
        divisionIds.push(element.id)
      });
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionId: divisionIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          this.districtList = [];
          if (response && response.resources && response.isSuccess) {
            this.districtList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    });
    this.smsCampaignStageOneForm.get('districtIds').valueChanges.subscribe(districtId => {
      if (!districtId) return;
      if (districtId.length == 0) return;
      let districtIds = []
      districtId.forEach(element => {
        districtIds.push(element.id)
      });
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null,
          { districtId: districtIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          this.branchList = [];
          if (response && response.resources && response.isSuccess) {
            this.branchList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.smsCampaignStageOneForm.get('branchIds').valueChanges.subscribe(branchId => {
      if (!branchId) return;
      if (branchId.length == 0) return;
      let branchIds = []
      branchId.forEach(element => {
        branchIds.push(element.id)
      });
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_SUB_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null,
          { branchId: branchIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.subBranchList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SALES_BOUNDARIES, null, false,
        prepareGetRequestHttpParams(null, null,
          { branchIds: branchIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          this.salesAreaList = []
          if (response && response.resources && response.isSuccess) {
            response.resources.forEach(element => {
              element.id = element.boundaryId
              element.displayName = element.boundaryName
            });
            this.salesAreaList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TECHAREA_BOUNDARIES, null, false,
        prepareGetRequestHttpParams(null, null,
          { BoundaryLayerId: 4, branchIds: branchIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          this.technicianInstallationAreaList = []
          if (response && response.resources && response.isSuccess) {
            this.technicianInstallationAreaList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TECHAREA_BOUNDARIES, null, false,
        prepareGetRequestHttpParams(null, null,
          { BoundaryLayerId: 3, branchIds: branchIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          this.technicalServiceAreaList = []
          if (response && response.resources && response.isSuccess) {
            this.technicalServiceAreaList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_LSS, null, false,
        prepareGetRequestHttpParams(null, null,
          { branchId: branchIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.lssList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    });
    this.smsCampaignStageOneForm.get('subBranchIds').valueChanges.subscribe(subBranchId => {
      if (!subBranchId) return;
      if (subBranchId.length == 0) return;
      let subBranchIds = []
      subBranchId.forEach(element => {
        subBranchIds.push(element.id)
      });
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_MAIN_AREA_BY_SUB_BRANCH, null, false,
        prepareGetRequestHttpParams(null, null,
          { subBranchIds: subBranchIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          this.mainAreaList = [];
          if (response && response.resources && response.isSuccess) {
            this.mainAreaList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    });
    this.smsCampaignStageOneForm.get('mainAreaIds').valueChanges.subscribe(mainAreaId => {
      if (!mainAreaId) return;
      if (mainAreaId.length == 0) return;
      let mainAreaIds = []
      mainAreaId.forEach(element => {
        mainAreaIds.push(element.id)
      });
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SUB_AREA, null, false,
        prepareGetRequestHttpParams(null, null,
          { mainAreaIds: mainAreaIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          this.subAreaList = [];
          if (response && response.resources && response.isSuccess) {
            this.subAreaList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
  }

  onServiceCategoryChange(event?) {
    let serviceCategoryId = this.smsCampaignStageOneForm.get('serviceCategoryIds').value
    if (!serviceCategoryId) {
      this.serviceList = []
      this.smsCampaignStageOneForm.get('serviceIds').setValue(null)
      return;
    }
    if (serviceCategoryId.length == 0) {
      this.serviceList = []
      this.smsCampaignStageOneForm.get('serviceIds').setValue(null)
      return;
    }
    var serviceList = []

    serviceCategoryId.forEach(element => {
      if (element.hasOwnProperty('services')) {
        if (element.services) {
          element.services.forEach(element1 => {
            serviceList.push(element1)
          });
        }
      } else {
        let exist = this.serviceTypeList.find(x => x.id == element.id)
        if (exist) {
          if (exist.services) {
            exist.services.forEach(element1 => {
              serviceList.push(element1)
            });
          }
        }
      }
    });
    let unicId = this.getUniqueListBy(serviceList, 'id')
    this.serviceList = [...unicId]
    if (event) {
      let serviceIds = this.smsCampaignStageOneForm.get('serviceIds').value
      let serviceIdsCatAlt = this.setItemsPost(this.serviceList)
      let serviceIdsAlt = this.setItemsPost(serviceIds)
      var result = serviceIdsCatAlt.filter(e => serviceIdsAlt.indexOf(e) !== -1);
      let fServiceIds = this.setItems(result)
      this.smsCampaignStageOneForm.get('serviceIds').setValue(fServiceIds)
    }
  }

  public checkValidity(): void {
    Object.keys(this.smsCampaignStageOneForm.controls).forEach((key) => {
      this.smsCampaignStageOneForm.controls[key].markAsDirty();
    });
  }

  onSubmit(): void {
    if (this.smsCampaignStageOneForm.invalid) {
      this.checkValidity()
      return;
    }
    let formValue = this.smsCampaignStageOneForm.getRawValue()
    formValue.isCPAProtected = (formValue.isCPAProtected == true || formValue.isCPAProtected == "true") ? true : false
    formValue.isDealerOwned = (formValue.isDealerOwned == true || formValue.isDealerOwned == "true") ? true : false
    formValue.isDealerRented = (formValue.isDealerRented == true || formValue.isDealerRented == "true") ? true : false
    formValue.districtIds = this.setItemsPost(formValue.districtIds)
    formValue.divisionIds = this.setItemsPost(formValue.divisionIds)
    formValue.regionIds = this.setItemsPost(formValue.regionIds)
    formValue.branchIds = this.setItemsPost(formValue.branchIds)
    formValue.salesAreaIds = this.setItemsPost(formValue.salesAreaIds)
    formValue.subBranchIds = this.setItemsPost(formValue.subBranchIds)
    formValue.mainAreaIds = this.setItemsPost(formValue.mainAreaIds)
    formValue.subAreaIds = this.setItemsPost(formValue.subAreaIds)
    formValue.originIds = this.setItemsPost(formValue.originIds)
    formValue.installOriginIds = this.setItemsPost(formValue.installOriginIds)
    formValue.technicalInstallationAreaIds = this.setItemsPost(formValue.technicalInstallationAreaIds)
    formValue.technicalServiceAreaIds = this.setItemsPost(formValue.technicalServiceAreaIds)
    formValue.itemOwnershipTypeIds = this.setItemsPost(formValue.itemOwnershipTypeIds)
    formValue.customerTypeIds = this.setItemsPost(formValue.customerTypeIds)
    formValue.lssIds = this.setItemsPost(formValue.lssIds)
    formValue.siteTypeIds = this.setItemsPost(formValue.siteTypeIds)
    formValue.sourceIds = this.setItemsPost(formValue.sourceIds)
    formValue.serviceCategoryIds = this.setItemsPost(formValue.serviceCategoryIds)
    formValue.serviceIds = this.setItemsPost(formValue.serviceIds)
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SMS_CAMPAIGN, formValue).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.smsCampaignId = response.resources;
        this.router.navigate(['sales/sms-campaign/stage-two'], { queryParams: { id: this.marketingCampaignId, smsCampaignId: this.smsCampaignId } });
      }
    });
  }

  getUniqueListBy(arr, key) {
    return [...new Map(arr.map(item => [item[key], item])).values()]
  }

  onArrowClick() {
    if ((this.marketingCampaignId || this.smsCampaignStageOneForm.get('marketingCampaignId').value) && this.smsCampaignId) {
      this.router.navigate(['sales/sms-campaign/stage-two'], { queryParams: { id: this.marketingCampaignId, smsCampaignId: this.smsCampaignId } });
    }
  }
}
