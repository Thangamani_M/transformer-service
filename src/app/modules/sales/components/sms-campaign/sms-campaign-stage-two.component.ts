import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  CrudService, CrudType, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService
} from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { btnActionTypes } from '@modules/sales';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { combineLatest } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-sms-campaign-stage-two',
  templateUrl: './sms-campaign-stage-two.component.html'
})

export class SmsCampaignStageTwoComponent extends PrimeNgTableVariablesModel implements OnInit {
  campaignStageTwoForm: FormGroup;
  marketingCampaignId = '';
  smsCampaignId = '';
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isSubmitted: boolean;
  pageSize:any = 10
  isOpenMatchedCriteriaDialog: boolean = false

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private router: Router, private formBuilder: FormBuilder, private datePipe: DatePipe) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "SMS Campaign",
      breadCrumbItems: [{ displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Marketing', relativeRouterUrl: '' }, { displayName: 'SMS Campaign', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'SMS Campaign',
            dataKey: 'smsCampaignCustomerId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'customerRefNo', header: 'Account Number', width: '200px' },
            { field: 'firstName', header: 'Customer Name', width: '200px' },
            { field: 'lastName', header: 'Surname', width: '200px' },
            { field: 'mobileNumber', header: 'Mobile Number', width: '200px' },
            { field: 'lastCommunicationReceived', header: 'Last Communication Received', width: '200px' },
            { field: 'lastCommunicationDate', header: 'Last Communication Date', width: '200px' },
            { field: 'lastCommunicationResult', header: 'Last Communication Result', width: '200px' },
            ],
            apiSuffixModel: SalesModuleApiSuffixModels.SMS_CAMPAIGN_CUSTOMERS,
            moduleName: ModulesBasedApiSuffix.SALES,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createStageOneForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.activatedRoute.queryParams]).pipe(take(1))
      .subscribe((response) => {
        if (response[0]) {
          this.marketingCampaignId = response[0].id;
          this.smsCampaignId = response[0].smsCampaignId;
          this.isSubmitted = response[0].isSubmitted == 'true' ? true : false;
          this.getMarketingDataList();
        }
      });
  }

  createStageOneForm() {
    this.campaignStageTwoForm = this.formBuilder.group({
      totalRecords: [0],
      sampleSize: ['',Validators.required],
      matchedCriteria: [''],
    });
  }

  getMarketingDataList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (!otherParams) {
      otherParams = {}
      otherParams['smsCampaignId'] = this.smsCampaignId
    } else {
      otherParams['smsCampaignId'] = this.smsCampaignId
    }
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources?.data) {
        res?.resources?.data?.forEach(val => {
          val.lastCommunicationDate = this.datePipe.transform(val?.lastCommunicationDate, 'dd-MM-yyyy, h:mm:ss a');
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      if (data.isSuccess && data.statusCode == 200 && data.resources) {
        this.dataList = data.resources?.data;
        this.campaignStageTwoForm.get('matchedCriteria').setValue(data.resources?.matchedCriteria);
        this.totalRecords = data.resources.totalcount;
      } else {
        this.totalRecords = 0;
      }
      this.campaignStageTwoForm.get('totalRecords').setValue(this.totalRecords);
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getMarketingDataList(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
    switch (type) {
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            if (editableObject?.isSubmitted) {
              this.router.navigate(["sales/sms-campaign/stage-one-view"], { queryParams: { id: editableObject['marketingCampaignId'] } });
            } else {
              this.router.navigate(["sales/sms-campaign/stage-one"], { queryParams: { id: editableObject['marketingCampaignId'] } });
            }
            break;
        }
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {});
    }
  }

  onArrowClick(type: string) {
    if (this.isSubmitted == true) {
      if (type == 'previous') {
        this.router.navigate(['/sales/sms-campaign/stage-one-view'], { queryParams: { id: this.marketingCampaignId } });
      }
      else {
        this.router.navigate(['/sales/sms-campaign/stage-three-view'], { queryParams: { id: this.marketingCampaignId, smsCampaignId: this.smsCampaignId } });
      }
    } else {
      if (type == 'previous') {
        this.router.navigate(['/sales/sms-campaign/stage-one'], { queryParams: { id: this.marketingCampaignId } });
      }
      else {
        this.router.navigate(['/sales/sms-campaign/stage-three'], { queryParams: { id: this.marketingCampaignId, smsCampaignId: this.smsCampaignId } });
      }
    }
  }

  openMatchedCriteriaDialog(){
    if(!this.campaignStageTwoForm.get('matchedCriteria').value) return
    this.isOpenMatchedCriteriaDialog = true
  }

  onSubmit(): void {
    if (this.campaignStageTwoForm.invalid) {
      return;
    }
    this.getMarketingDataList('0', this.campaignStageTwoForm.get('sampleSize').value);
    this.pageSize = this.campaignStageTwoForm.get('sampleSize').value
  }

  nextOrPrevStepper(type?: string): void {
    if (type === btnActionTypes.SUBMIT) {
      this.router.navigate(['/sales/sms-campaign/stage-three'], { queryParams: { id: this.marketingCampaignId, smsCampaignId: this.smsCampaignId } });
    }
    else if (type === btnActionTypes.NEXT) {
      this.router.navigate(['/sales/sms-campaign/stage-three'], { queryParams: { id: this.marketingCampaignId, smsCampaignId: this.smsCampaignId } });
    }
    else if (type === btnActionTypes.PREVIOUS) {
      this.router.navigate(['/sales/sms-campaign/stage-one'], { queryParams: { id: this.marketingCampaignId } });
    }
  }
}