import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, SnackbarService } from '@app/shared/services';
import { DebitOrderCode } from '@modules/inventory/models/debitorderruncode.model';
import { combineLatest, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-debitorder-run-code-add-edit',
  templateUrl: './debitorder-run-code-add-edit.component.html'
})
export class DebitorderRunCodeAddEditComponent implements OnInit {
  debitOrderRunCodeId = "";
  DebitOrderCodeAddEditForm: FormGroup;
  debitorderRunCode: FormArray;
  errorMessage: string;
  isButtondisabled = false;
  alphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  formConfigs = formConfigs;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private crudService: CrudService,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createDebitOrderRunCodeManualAddForm();
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([this.activatedRoute.queryParams]
    ).pipe(take(1)).subscribe((response) => {
      if (response[0]) {
        this.debitOrderRunCodeId = response[0].id;
        this.getdebitOrderRunCodeById().subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.isSuccess) {
            this.debitorderRunCode = this.getdebitorderRunCode;
            this.debitorderRunCode.push(this.createDebitorderRunCodeFormGroup(response.resources));
          }
        });
      } else {
        this.debitorderRunCode = this.getdebitorderRunCode;
        this.debitorderRunCode.push(this.createDebitorderRunCodeFormGroup());
      }
    });
  }

  createDebitOrderRunCodeManualAddForm(): void {
    this.DebitOrderCodeAddEditForm = this.formBuilder.group({
      debitorderRunCode: this.formBuilder.array([])
    });
  }

  get getdebitorderRunCode(): FormArray {
    if (!this.DebitOrderCodeAddEditForm) return;
    return this.DebitOrderCodeAddEditForm.get("debitorderRunCode") as FormArray;
  }

  addDebitOrderRunCode(): void {
    if (this.DebitOrderCodeAddEditForm.invalid) return;
    this.debitorderRunCode = this.getdebitorderRunCode;
    let debitorderRunCodeData = new DebitOrderCode();
    this.debitorderRunCode.push(this.createDebitorderRunCodeFormGroup(debitorderRunCodeData));
  }

  createDebitorderRunCodeFormGroup(debitorderRunCode?: DebitOrderCode): FormGroup {
    let DebitOrderCodeData = new DebitOrderCode(debitorderRunCode ? debitorderRunCode : undefined);
    let formControls = {};
    Object.keys(DebitOrderCodeData).forEach((key) => {
      formControls[key] = [{ value: DebitOrderCodeData[key], disabled: debitorderRunCode && (key == '') && DebitOrderCodeData[key] !== '' ? true : false },
      (key === 'debitOrderRunCodeName' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  removedebitorderruncode(i: number): void {
    if (this.debitorderRunCode.controls[i].value.debitOrderRunCodeId.length > 1) {
      this.crudService.delete(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.DEBITORDERRUNCODES,
        this.debitorderRunCode.controls[i].value.debitOrderRunCodeId).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.debitorderRunCode.removeAt(i);
          }
          if (this.debitorderRunCode.length === 0) {
            this.addDebitOrderRunCode();
          };
        });
    }
    else if (this.debitorderRunCode.length === 1) {
      this.snackbarService.openSnackbar("Atleast one Debit Order Run Code required", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.debitorderRunCode.removeAt(i);
    }
  }

  onChange(value) {
    if (this.getdebitorderRunCode.length > 1) {
      var lengthNew = this.getdebitorderRunCode.length - 1;
      var findIndex = this.getdebitorderRunCode.getRawValue().findIndex(x => x.debitOrderRunCodeName == value);
      if (lengthNew != findIndex) {
        this.snackbarService.openSnackbar("Debit order Run Code Name  already exist", ResponseMessageTypes.WARNING);
        return false;
      }
    }
  }

  getdebitOrderRunCodeById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.DEBITORDERRUNCODES,
      this.debitOrderRunCodeId
    );
  }

  onClicked(): void {
    this.router.navigate(['configuration/payment-configuration/debitor-order-run-view'], { queryParams: { id: this.debitOrderRunCodeId }, skipLocationChange: true })
  }

  submit() {
    if (this.debitorderRunCode.invalid) {
      return;
    }
    if (this.debitOrderRunCodeId) {
      this.isButtondisabled = true;
      this.crudService.update(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.DEBITORDERRUNCODES, this.DebitOrderCodeAddEditForm.value["debitorderRunCode"])
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/payment-configuration'], { queryParams: { tab: 1 }, skipLocationChange: true });
            } else {
              this.isButtondisabled = false;
            }
          }
        });
    } else {
      this.isButtondisabled = true;
      this.crudService.create(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.DEBITORDER_BULK, this.DebitOrderCodeAddEditForm.value["debitorderRunCode"])
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/configuration/payment-configuration'], { queryParams: { tab: 1 }, skipLocationChange: true });
            } else {
              this.isButtondisabled = false;
            }
          }
        });
    }
  }
}
