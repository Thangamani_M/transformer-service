import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
    selector: 'app-debitorder-run-code-view',
    templateUrl: './debitorder-run-code-view.component.html'
})

export class DebitorderRunCodeViewComponent implements OnInit {
    debitOrderRunCodeId;
    debitOrderRunCodeDetail;
    primengTableConfigProperties: any;

    constructor(private router: Router, private activatedRoute: ActivatedRoute, private httpService: CrudService,
        private rxjsService: RxjsService) {
        this.primengTableConfigProperties = {
            tableCaption: "View Debit Order Run Code",
            breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' },
            { displayName: 'Billing', relativeRouterUrl: '' },
            { displayName: 'Debit Order Run Code List', relativeRouterUrl: '/configuration/payment-configuration', queryParams: { tab: 1 } }, { displayName: 'View Debit Order Run Code' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableEditActionBtn: true,
                        enableClearfix: true,
                    }]
            }
        }
    }

    combineLatestNgrxStoreData(): void {
        combineLatest([this.activatedRoute.queryParams]
        ).pipe(take(1)).subscribe((response) => {
            if (response[0]?.id) {
                this.debitOrderRunCodeId = response[0].id;
                this.httpService.get(ModulesBasedApiSuffix.BILLING,
                    SalesModuleApiSuffixModels.DEBITORDERRUNCODES, this.debitOrderRunCodeId, false, null).subscribe((response: IApplicationResponse) => {
                        if (response.resources && response.isSuccess && response.statusCode == 200) {
                            this.debitOrderRunCodeDetail = response.resources;
                        }
                        this.rxjsService.setGlobalLoaderProperty(false);
                    });
            }
        });
    }

    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
    }

    onCRUDRequested(type: CrudType | string): void {
        switch (type) {
            case CrudType.EDIT:
                this.onEditButtonClicked();
                break;
        }
    }

    onEditButtonClicked(): void {
        this.router.navigate(['configuration/payment-configuration/debitor-order-run-add-edit'], { queryParams: { id: this.debitOrderRunCodeId } })
    }
}
