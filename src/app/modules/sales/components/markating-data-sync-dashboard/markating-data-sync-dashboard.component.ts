import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CommonPaginationConfig, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService
} from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CAMPAIGN_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/campaign-mngt-component';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-markating-data-sync-dashboard',
  templateUrl: './markating-data-sync-dashboard.component.html'
})
export class MarkatingDataSyncDashboardComponent extends PrimeNgTableVariablesModel implements OnInit {

  constructor(private router: Router,private snackbarService: SnackbarService, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private datePipe: DatePipe) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Marketing Data Sync Dashboard",
      breadCrumbItems: [{ displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Marketing', relativeRouterUrl: '' }, 
      { displayName: 'Marketing Data Sync Config Dashboard', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Marketing Data Sync Dashboard',
            dataKey: 'marketingDataSyncMaintainanceId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'testRunPeriodName', header: 'Test Run Period Name', width: '200px' },
            { field: 'marketingDataSyncMaintainanceUpdateTypeName', header: 'Marketing DataSync Name', width: '200px',shouldHideCalendar: true },
            { field: 'dayName', header: 'Day Name', width: '200px' },
            { field: 'startDate', header: 'StartDate', width: '200px' },
            { field: 'startTime', header: 'StartTime', width: '200px', nofilter: true },
            { field: 'host', header: 'Host', width: '200px' },
            { field: 'port', header: 'Port', width: '200px' },
            ],
            apiSuffixModel: SalesModuleApiSuffixModels.MARKETING_DATA_SYNC_DASHBOARD,
            moduleName: ModulesBasedApiSuffix.SALES,
            enableAddActionBtn: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getMarketingDataList();
  }

 
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CAMPAIGN_MANAGEMENT_COMPONENT.MARKETING_DATA_SYNC_CONFIG]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }

    });

  }


  getMarketingDataList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let collectionModuleApiSuffixModels: CollectionModuleApiSuffixModels;
    collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex ? pageIndex : CommonPaginationConfig.defaultPageIndex, pageSize ? pageSize : CommonPaginationConfig.defaultPageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.startDate = this.datePipe.transform(val?.startDate, 'dd-MM-yyyy, h:mm:ss a');
          return val;
        });
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      if (data.isSuccess && data.statusCode == 200 && data.resources) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.totalRecords = 0;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getMarketingDataList(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
    }
  }

  loadActionTypes(api, filter = true, editableObject?: any, type?: any) {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("sales/marketing-data/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["sales/marketing-data/view"], { queryParams: { id: editableObject['marketingDataSyncMaintainanceId'] } });
            break;
        }
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    this.loadActionTypes(null, false, editableObject, type);
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
