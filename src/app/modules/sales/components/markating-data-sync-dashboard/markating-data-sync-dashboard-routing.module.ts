import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MarkatingDataSyncDashboardAddEditComponent } from './markating-data-sync-dashboard-add-edit/markating-data-sync-dashboard-add-edit.component';
import { MarkatingDataSyncDashboardViewComponent } from './markating-data-sync-dashboard-view/markating-data-sync-dashboard-view.component';
import { MarkatingDataSyncDashboardComponent } from './markating-data-sync-dashboard.component';

const routes: Routes = [
  { path: '', component: MarkatingDataSyncDashboardComponent, data: { title: 'Marketing Dashboard' } },
  { path: 'add-edit', component: MarkatingDataSyncDashboardAddEditComponent, data: { title: 'Add Marketing Dashboard' } },
  { path: 'view', component: MarkatingDataSyncDashboardViewComponent, data: { title: ' View Marketing Dashboard' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class MarkatingDataSyncDashboardRoutingModule { }
