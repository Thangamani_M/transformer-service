import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { MarkatingDataSyncDashboardAddEditComponent } from './markating-data-sync-dashboard-add-edit/markating-data-sync-dashboard-add-edit.component';
import { MarkatingDataSyncDashboardRoutingModule } from './markating-data-sync-dashboard-routing.module';
import { MarkatingDataSyncDashboardViewComponent } from './markating-data-sync-dashboard-view/markating-data-sync-dashboard-view.component';
import { MarkatingDataSyncDashboardComponent } from './markating-data-sync-dashboard.component';
@NgModule({
  declarations: [MarkatingDataSyncDashboardComponent, MarkatingDataSyncDashboardAddEditComponent, MarkatingDataSyncDashboardViewComponent],
  imports: [
    CommonModule,
    MarkatingDataSyncDashboardRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class MarkatingDataSyncDashboardModule { }
