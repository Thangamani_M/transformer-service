import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CAMPAIGN_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/campaign-mngt-component';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-markating-data-sync-dashboard-view',
  templateUrl: './markating-data-sync-dashboard-view.component.html'
})
export class MarkatingDataSyncDashboardViewComponent implements OnInit {
  marketingDataSyncMaintainanceId = "";
  primengTableConfigProperties: any;
  viewDetailedData = [];
  componentPermissions = [];

  constructor(private activatedRoute: ActivatedRoute, private store: Store<AppState>, private router: Router, private rxjsService: RxjsService,
    private crudService: CrudService, private snackbarService: SnackbarService) {
    this.primengTableConfigProperties = {
      tableCaption: "View Marketing Data Sync Resubmission",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Sales', relativeRouterUrl: '' },
      { displayName: 'Marketing', relativeRouterUrl: '' }, { displayName: 'View Marketing Data Sync Resubmission' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'View Marketing Data Sync Resubmission',
            dataKey: 'marketingDataSyncMaintainanceId',
            enableBreadCrumb: true,
            enableAction: true,
            enableViewBtn: true
          }]
      }
    }
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([this.activatedRoute.queryParams,
    this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).pipe(take(1)).subscribe((response) => {
      if (response[0]) {
        this.marketingDataSyncMaintainanceId = response[0].id;
        this.getDelayedDurationDetailsById();
      }
      if (response[1]) {
        let permission = response[1][CAMPAIGN_MANAGEMENT_COMPONENT.MARKETING_DATA_SYNC_CONFIG]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      }
    });
  }

  getDelayedDurationDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.MARKETING_DATA_SYNC_DASHBOARD, this.marketingDataSyncMaintainanceId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.viewDetailedData = [
            { name: 'Day Name', value: response.resources?.dayName },
            { name: 'Host', value: response.resources?.host },
            { name: 'StartDate', value: response.resources?.startDate, isDate: true },
            { name: 'startTime', value: response.resources.startTime },
            { name: 'UserName', value: response.resources.userName },
            { name: 'Test Run Period Name', value: response.resources?.testRunPeriodName }
          ]
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  navigateToList() {
    this.router.navigate(["/sales/marketing-data"]);
  }

  onCRUDRequested(type: CrudType | string): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        else {
          this.router.navigate(["/sales/marketing-data/add-edit"], { queryParams: { id: this.marketingDataSyncMaintainanceId } });
        }
        break;
    }
  }
}
