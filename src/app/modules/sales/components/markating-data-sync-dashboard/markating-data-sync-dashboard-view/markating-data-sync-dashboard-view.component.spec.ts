import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkatingDataSyncDashboardViewComponent } from './markating-data-sync-dashboard-view.component';

describe('MarkatingDataSyncDashboardViewComponent', () => {
  let component: MarkatingDataSyncDashboardViewComponent;
  let fixture: ComponentFixture<MarkatingDataSyncDashboardViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkatingDataSyncDashboardViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkatingDataSyncDashboardViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
