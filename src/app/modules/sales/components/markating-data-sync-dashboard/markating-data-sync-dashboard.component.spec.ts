import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkatingDataSyncDashboardComponent } from './markating-data-sync-dashboard.component';

describe('MarkatingDataSyncDashboardComponent', () => {
  let component: MarkatingDataSyncDashboardComponent;
  let fixture: ComponentFixture<MarkatingDataSyncDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkatingDataSyncDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkatingDataSyncDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
