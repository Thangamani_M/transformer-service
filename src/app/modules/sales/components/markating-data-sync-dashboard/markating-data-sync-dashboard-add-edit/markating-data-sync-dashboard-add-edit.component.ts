import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, disableWithDefaultFormControlValues, enableWithDefaultFormControlValues, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-markating-data-sync-dashboard-add-edit',
  templateUrl: './markating-data-sync-dashboard-add-edit.component.html'
})
export class MarkatingDataSyncDashboardAddEditComponent implements OnInit {
  marketingDataSyncMaintainanceId = "";
  daysDropDetails = [];
  startTodayDate = new Date;
  uploadedFile: File;
  marketingFrom: FormGroup;
  maintainceData = [];
  maintainceDataTestRun = [];
  maintainceDataViewDetails;
  days = [{ id: '1', displayName: 'Monday' }, { id: '2', displayName: 'Tuesday' }, { id: '3', displayName: 'Wednesday' },
  { id: '4', displayName: 'Thursday' }, { id: '5', displayName: 'Friday' }, { id: '6', displayName: 'Saturday' }]
  loggedInUserData: LoggedInUserModel;
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  formConfigs = formConfigs;
  selectedFrequencyName = '';

  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private store: Store<AppState>, private rxjsService: RxjsService,
    private crudService: CrudService, private momentService: MomentService) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.getForkJoinRequests();
    this.createMarketingForm();
    this.onFormControlValueChanges();
  }

  onFormControlValueChanges() {
    this.marketingFrom.get('testRunPeriodId').valueChanges.subscribe((testRunPeriodId: string) => {
      this.selectedFrequencyName = this.maintainceDataTestRun.find(mDT => mDT['id'] == testRunPeriodId)?.displayName;
      if (this.selectedFrequencyName == 'Daily') {
        this.marketingFrom = disableWithDefaultFormControlValues(this.marketingFrom, [{ formControlName: 'dayId', value: null }]);
      }
      else if (this.marketingFrom.get('dayId').disabled) {
        this.marketingFrom = enableWithDefaultFormControlValues(this.marketingFrom, [{ formControlName: 'dayId', value: null, isRequired: true }]);
      }
    });
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      if (response[1]) {
        this.marketingDataSyncMaintainanceId = response[1].id;
      }
    });
  }

  getForkJoinRequests() {
    forkJoin([this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_MARKETING_DAYS, null, false, null),
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_MARKETING_MAINTAINCE_DATA, null, false, null),
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_TEST_RUN_PEROID, null, false, null)])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
            switch (ix) {
              case 0:
                this.daysDropDetails = respObj.resources;
                break;
              case 1:
                this.maintainceData = respObj.resources;
                break;
              case 2:
                this.maintainceDataTestRun = respObj.resources;
                break;
            }
            if (ix == response.length - 1 && this.marketingDataSyncMaintainanceId) {
              this.getMarketingMaintainceData();
            }
            else if (ix == response.length - 1 && !this.marketingDataSyncMaintainanceId) {
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          }
        });
      });
  }

  createMarketingForm(): void {
    this.marketingFrom = new FormGroup({
      'dayId': new FormControl(null),
      'testRunPeriodId': new FormControl(null),
      'testRunPeriodName': new FormControl(null),
      'startDate': new FormControl(null),
      'startTime': new FormControl(null),
      'marketingDataSyncMaintainanceUpdateTypeId': new FormControl(null),
      'userName': new FormControl(null),
      'host': new FormControl(null),
      'port': new FormControl(null),
      'striataFolderPath': new FormControl(null),
      'certificatePath': new FormControl(null),
      'marketingDataSyncMaintainanceId': new FormControl(null),
      'createdDate': new FormControl(null),
      'createdUserId': new FormControl(this.loggedInUserData.userId),
      'modifiedUserId': new FormControl(null),
      'documentPath': new FormControl(null)
    })
    this.marketingFrom = setRequiredValidator(this.marketingFrom, ["dayId", "startDate", "certificatePath",
      "testRunPeriodId", "userName", "port", "striataFolderPath", "startTime", "marketingDataSyncMaintainanceUpdateTypeId"]);
    this.marketingFrom.get('host').setValidators([Validators.required]);
  }

  getMarketingMaintainceData() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.MARKETING_DATA_SYNC_DASHBOARD, this.marketingDataSyncMaintainanceId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.maintainceDataViewDetails = response.resources;
          let startTime = this.momentService.convertNormalToRailayTimeOnly(response.resources.startTime)
          let startTimeArray: any = startTime.split(':');
          let startTimeDate = (new Date).setHours(startTimeArray[0], startTimeArray[1]);
          response.resources.startTime = new Date(startTimeDate);
          response.resources.startDate = new Date(response.resources.startDate);
          this.marketingFrom.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  emitUploadedFiles(uploadedFile) {
    if (!uploadedFile || uploadedFile?.length == 0) {
      return;
    }
    this.uploadedFile = uploadedFile;
    this.marketingFrom.get('certificatePath').setValue(uploadedFile['name']);
  }

  onSubmit(): void {
    this.marketingFrom.get('certificatePath').markAllAsTouched();
    if (this.marketingFrom.invalid) {
      return;
    }
    let formGroupAbstractControlValue = this.marketingFrom.getRawValue();
    formGroupAbstractControlValue.startDate = this.momentService.convertToUTC(this.marketingFrom.get('startDate').value);
    if (typeof this.marketingFrom.get('startTime').value !== 'string') {
      let startTime = this.momentService.convertNormalToRailayTimeOnly(this.marketingFrom.get('startTime').value)
      formGroupAbstractControlValue.startTime = startTime;
    }
    formGroupAbstractControlValue.marketingDataSyncMaintainanceId = this.marketingDataSyncMaintainanceId;
    if (this.marketingDataSyncMaintainanceId) {
      formGroupAbstractControlValue.modifiedUserId = this.loggedInUserData.userId;
    }
    let find = this.maintainceDataTestRun.find(item => item.id == formGroupAbstractControlValue.testRunPeriodId)
    formGroupAbstractControlValue.testRunPeriodName = find.displayName;
    let payload = new FormData();
    payload.append("data", JSON.stringify(formGroupAbstractControlValue));
    if (this.uploadedFile) {
      payload.append("file", this.uploadedFile, this.uploadedFile['name']);
    }
    let crudService: Observable<IApplicationResponse> = (!this.marketingDataSyncMaintainanceId) ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.MARKETING_DATA_SYNC_DASHBOARD, payload) :
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.MARKETING_DATA_SYNC_DASHBOARD, payload)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.router.navigateByUrl('/sales/marketing-data');
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
}
