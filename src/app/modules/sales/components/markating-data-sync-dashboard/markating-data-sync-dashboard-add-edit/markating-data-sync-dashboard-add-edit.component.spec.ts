import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkatingDataSyncDashboardAddEditComponent } from './markating-data-sync-dashboard-add-edit.component';

describe('MarkatingDataSyncDashboardAddEditComponent', () => {
  let component: MarkatingDataSyncDashboardAddEditComponent;
  let fixture: ComponentFixture<MarkatingDataSyncDashboardAddEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkatingDataSyncDashboardAddEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkatingDataSyncDashboardAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
