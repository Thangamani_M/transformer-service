import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RawLeadNewAddressRequestAddEditComponent, RawLeadNewAddressRequestComponent } from '.';
import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    {
        path: 'dashboard', children: [
            { path: '', component: RawLeadNewAddressRequestComponent, data: { title: 'Raw Lead New Address Request List' }, canActivate: [AuthGuard] },
            { path: 'add-edit', component: RawLeadNewAddressRequestAddEditComponent, data: { title: 'Raw Lead New Address Request Add Edit' }, canActivate: [AuthGuard] }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)]
})

export class RawLeadNewAddressRequestRoutingModule { }
