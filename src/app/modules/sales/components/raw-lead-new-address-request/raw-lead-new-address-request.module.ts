import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { RawLeadNewAddressRequestAddEditComponent, RawLeadNewAddressRequestComponent, RawLeadNewAddressRequestRoutingModule } from ".";
@NgModule({
  declarations: [RawLeadNewAddressRequestComponent,RawLeadNewAddressRequestAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RawLeadNewAddressRequestRoutingModule,
    MaterialModule,
    SharedModule,
  ],
})
export class RawLeadNewAddressRequestModule {}
