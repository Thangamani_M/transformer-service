import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-raw-lead-new-address-request',
  templateUrl: './raw-lead-new-address-request.component.html'
})
export class RawLeadNewAddressRequestComponent extends PrimeNgTableVariablesModel {
  addressRequestStatus = [
    { label: 'Pending', value: "Pending" },
    { label: 'Approved', value: "Approved" },
    { label: 'Declined', value: "Declined" },
  ];
  primengTableConfigProperties: any = {
    tableCaption: "New Address Request",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Sales ', relativeRouterUrl: '' }, { displayName: 'New Address list' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'New Address Request',
          dataKey: 'rawLeadId',
          enableBreadCrumb: true,
          enableAction: true,
          enableAddActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'newAddressRequestRefNo', header: 'Request ID' },
          { field: 'rawLeadRefNo', header: 'Raw Lead No' },
          { field: 'fullAddress', header: 'Address' },
          { field: 'status', header: 'Status', type: 'dropdown', options: this.addressRequestStatus }],
        }]
    }
  }

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private crudService: CrudService, private rxjsService: RxjsService) {
    super();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRawLeadNewAddressRequests();
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRawLeadNewAddressRequestStatuses() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.RAW_LEADS_NEW_ADDRESS_REQUESTS,
      undefined,
      false).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.addressRequestStatus = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRawLeadNewAddressRequests(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.RAW_LEADS_NEW_ADDRESS_REQUESTS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  navigateToList(): void {
    this.router.navigate(['/sales/task-management/leads/lead-info/view'], { queryParams: { selectedIndex: 0, leadId: "" }, skipLocationChange: true })
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.GET:
        this.getRawLeadNewAddressRequests(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, data?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/sales/new-address-approval/dashboard/add-edit'],
          { queryParams: { rawLeadId: data['rawLeadId'] } })
        break;
    }
  }
}
