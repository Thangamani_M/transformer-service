import { Component, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData, SalesModuleApiSuffixModels, Sales_Leads_Components } from '@app/modules';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR } from '@app/shared/utils';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

declare let $;
@Component({
  selector: 'raw-lead-new-address-request-add-edit',
  templateUrl: './raw-lead-new-address-request-add-edit.component.html'
})

export class RawLeadNewAddressRequestAddEditComponent {
  addressRequestModel: any = {};
  rawLeadId = "";
  declineReason = "";
  message = "";
  loggedInUserData: LoggedInUserModel;
  @ViewChild('decline_reason', { static: false }) decline_reason: ElementRef<any>;
  componentPermissions = [];

  constructor(private router: Router, private crudService: CrudService, private store: Store<AppState>, private snackbarService: SnackbarService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(loggedInUserData), this.activatedRoute.queryParams,
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0])
      this.rawLeadId = response[1]['rawLeadId'];
      this.componentPermissions = response[2][Sales_Leads_Components.NEW_ADDRESS_APPROVAL];
      this.getNewAddressRequestDetailsById();
    });
  }

  ngOnInit() {
    this.onBootstrapModalEventChanges();
  }

  onBootstrapModalEventChanges(): void {
    $("#decline_reason").on("shown.bs.modal", (e) => {
      this.declineReason = "";
    });
    $("#decline_reason").on("hidden.bs.modal", (e) => {
    });
  }

  getNewAddressRequestDetailsById(): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_NEW_ADDRESS, this.rawLeadId, false, null, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.addressRequestModel = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(submitStatus: string): void {
    if (submitStatus == 'approve') {
      if (!this.componentPermissions.find(cF => cF.menuName === PermissionTypes.APPROVE)) {
        this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      else {
        this.message = submitStatus == 'approve' ?
          `Are you sure you want to approve this new address and add it to the address library ?` :
          `Are you sure you want to decline this new address request ?`;
        this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(this.message).
          onClose?.subscribe(dialogResult => {
            if (!dialogResult) return;
            this.onFormSubmit(submitStatus);
          });
      }
    }
    else if (submitStatus == 'decline') {
      if (!this.componentPermissions.find(cF => cF.menuName === PermissionTypes.DECLINE)) {
        this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      else {
        $(this.decline_reason.nativeElement).modal('show');
      }
    }
  }

  onFormSubmit(submitStatus) {
    let payload = {};
    payload['isApproved'] = submitStatus == 'approve' ? true : false;
    payload['comments'] = this.declineReason;
    payload['rawLeadId'] = this.rawLeadId
    payload['modifiedUserId'] = this.loggedInUserData.userId;
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_NEW_ADDRESS,
      payload).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.redirectToListPage();
        }
      });
  }

  redirectToListPage(): void {
    this.router.navigateByUrl('/sales/new-address-approval/dashboard');
  }
}