import { Component } from "@angular/core";
import { Router } from '@angular/router';
import { AppState } from "@app/reducers";
import {
  BreadCrumbModel, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ReusablePrimeNGTableFeatureService, SnackbarService, standardHeightForPscrollContainer
} from "@app/shared";
import { ModuleName, ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService } from "@app/shared/services";
import { MY_TASK_COMPONENT } from "@modules/my-tasks/shared";
import { loggedInUserData } from '@modules/others';
import {
  LeadCreationStepperParamsCreateAction, LeadCreationUserDataModel, LeadHeaderData, LeadHeaderDataCreateAction, LeadOutcomeStatusNames, SalesModuleApiSuffixModels,
  Sales_Leads_Components,
  selectLeadCreationUserDataState$, selectLeadHeaderDataState$, ServiceAgreementStepsParams, StepperParams
} from "@modules/sales";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { take } from "rxjs/operators";
@Component({
  selector: "app-latest-quotes-info",
  templateUrl: "./latest-quotes-info.component.html",
  styleUrls: ["./latest-quotes-info.component.scss"]
})
export class LatestQuotesInfoComponent {
  params: StepperParams;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  loggedInUserData: LoggedInUserModel;
  serviceAgreementStepsParams;
  leadHeaderData: LeadHeaderData;
  quotationSystemTypes = [];
  items;
  quotationServiceCategories = [];
  lssContributionDetails = [];
  services;
  isEnableQuoteGenerateButton = false;
  terms;
  detailsObj;
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  quoteDetails: any = {};
  exceptionMessage = "";
  payload = {};
  breadCrumb: BreadCrumbModel;
  standardHeightForPscrollContainer = standardHeightForPscrollContainer;
  filteredSubComponentsPermissions = [];

  constructor(
    private crudService: CrudService, private snackbarService: SnackbarService,
    private router: Router, public rxjsService: RxjsService,
    private store: Store<AppState>, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService
  ) {
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.getQuotations();
    this.getTermsAndConditions();
    this.combineLatestPageLevelAccessData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationUserDataState$),
      this.store.select(loggedInUserData), this.store.select(selectLeadHeaderDataState$)
    ]
    ).pipe(take(1)).subscribe((response) => {
      this.leadCreationUserDataModel = new LeadCreationUserDataModel(response[0]);
      this.loggedInUserData = new LoggedInUserModel(response[1]);
      this.leadHeaderData = response[2];
      this.breadCrumb = {
        pageTitle: { key: 'Generate Quotation', value: this.leadHeaderData.leadRefNo },
        items: [{
          key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
        }, { key: 'Generate Quotation' }]
      };
    });
  }

  combineLatestPageLevelAccessData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)
    ]).subscribe((response) => {
      let componentPermissions = [];
      if (this.leadHeaderData.fromUrl == "Leads List") {
        componentPermissions = response[0][Sales_Leads_Components.LEADS];
      }
      else {
        componentPermissions = response[0][MY_TASK_COMPONENT.MY_SALES];
        componentPermissions = componentPermissions?.filter(cP => cP.menuName === 'My Leads').map((fS => fS['subMenu']))[0];
      }
      if (componentPermissions) {
        this.filteredSubComponentsPermissions = componentPermissions?.filter(cP => cP.menuName === Sales_Leads_Components.STAGE_FOUR)?.map((fS => fS['subMenu']))[0];
      }
    });
  }

  validatePreQuoteGeneration() {
    let serviceCategoryIds = [];
    this.quotationServiceCategories.forEach(element => {
      serviceCategoryIds.push(element.serviceCategoryId);
    });
    let systemTypeIds = [];
    this.quotationSystemTypes.forEach(element => {
      systemTypeIds.push(element.systemTypeId);
    });
    let dt = new Date();
    dt.setDate(dt.getDate() + 21);
    this.payload = {
      "serviceCategoryIds": serviceCategoryIds,
      "systemTypeIds": systemTypeIds,
      "createdUserId": this.loggedInUserData.userId,
      "leadId": this.leadHeaderData.leadId,
      "adminFee": this.quoteDetails['adminFee'],
      "annualNetworkFee": this.quoteDetails['annualNetworkFee'],
      "expiryDate": dt
    }
    this.payload['isConfirmGenerateQuotation'] = false;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_GET_QUOTATIONS, this.payload)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 409 && Array.isArray(response.resources) &&
          response.message.includes('Required services/items are not added. Please check stage 2 & 3 to add the required services/items')) {
          this.exceptionMessage = response.message;
        }
      });
  }

  onArrowClick(str) {
    if (str == 'backward') {
      if (this.leadHeaderData.fromUrl == 'Leads List') {
        this.router.navigate(['/sales/task-management/leads/marketing-preference']);
      }
      else {
        this.router.navigate(['/my-tasks/my-sales/my-leads/marketing-preference']);
      }
    }
    else {
      if (this.leadHeaderData.fromUrl == 'Leads List') {
        this.router.navigate(['/sales/task-management/leads/quotation/view']);
      }
      else {
        this.router.navigate(['/my-tasks/my-sales/my-leads/quotation/view']);
      }
    }
  }

  adjustCost() {
    let isAccessAllowed = this.filteredSubComponentsPermissions?.find(fSC => fSC.menuName === 'Adjust Costing');
    if (isAccessAllowed) {
      if (this.leadHeaderData.fromUrl == 'Leads List') {
        this.router.navigate(['/sales/task-management/leads/lead-quotations/adjust-info']);
      }
      else {
        this.router.navigate(['/my-tasks/my-sales/my-leads/lead-quotations/adjust-info']);
      }
    }
    else {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
  }

  clickAggrement(str) {
    if (str == 'service') {
      this.detailsObj = this.terms[0];
    } else {
      this.detailsObj = this.terms[1];
    }
  }

  getTermsAndConditions() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_TERMS, undefined, false, null)
      .subscribe((response) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.terms = response.resources;
        }
      });
  }

  reviseQuotes(type: string) {
    if (type == 'services') {
      if (this.leadHeaderData.fromUrl == 'Leads List') {
        this.router.navigate(['/sales/task-management/leads/lead-services/add-edit']);
      }
      else {
        this.router.navigate(['/my-tasks/my-sales/my-leads/lead-services/add-edit']);
      }
    }
    else if (type == 'items') {
      if (this.leadHeaderData.fromUrl == 'Leads List') {
        this.router.navigate(['/sales/task-management/leads/lead-items/add-edit']);
      }
      else{
        this.router.navigate(['/my-tasks/my-sales/my-leads/lead-items/add-edit']);
      }
    }
  }

  generateQuote() {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`${this.exceptionMessage} <br>
    Are you sure you want to Generate quote?`).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          this.payload['isConfirmGenerateQuotation'] = true;
          this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_GET_QUOTATIONS, this.payload, 1)
            .subscribe((response: IApplicationResponse) => {
              if (response.statusCode == 200 && response.isSuccess) {
                this.getLeadHeaderDataByLeadId();
                this.isEnableQuoteGenerateButton = false;
              }
            });
        }
      });
  }

  getLeadHeaderDataByLeadId() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEADS_HEADER, this.leadHeaderData.leadId, false, undefined, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        let leadHeaderData = new LeadHeaderData(response.resources);
        this.store.dispatch(new LeadHeaderDataCreateAction({ leadHeaderData }));
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getQuotations() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_GET_QUOTATIONS, this.leadHeaderData.leadId, false, null, 1).subscribe((response) => {
      if (response.isSuccess && response.resources && response.statusCode == 200) {
        this.quoteDetails = response.resources;
        this.serviceAgreementStepsParams = new ServiceAgreementStepsParams();
        this.serviceAgreementStepsParams['quotationVersionId'] = this.quoteDetails['quotationVersionId'];
        this.store.dispatch(new LeadCreationStepperParamsCreateAction({ serviceAgreementStepsParams: this.serviceAgreementStepsParams }));
        this.isEnableQuoteGenerateButton = response.resources.isEnableQuoteGenerateButton;
        if (response.resources.items && response.resources.items.quotationSystemTypes) {
          this.items = response.resources.items;
          this.quotationSystemTypes = response.resources.items.quotationSystemTypes;
        }
        if (response.resources.services && response.resources.services.quotationServiceCategories) {
          this.services = response.resources.services;
          this.quotationServiceCategories = response.resources.services.quotationServiceCategories;
        }
        if (response.resources.lssContributionDetails) {
          this.lssContributionDetails.push(response.resources.lssContributionDetails)
        }
        this.validatePreQuoteGeneration();
      }
      else {
        this.isEnableQuoteGenerateButton = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
}
