class PendingQuotationModel {
    constructor(pendingQuotationModel?: PendingQuotationModel) {
        this.quotationOutcomeId = pendingQuotationModel ? pendingQuotationModel.quotationOutcomeId == undefined ? null : pendingQuotationModel.quotationOutcomeId : null;
        this.leadCallBackId = pendingQuotationModel ? pendingQuotationModel.leadCallBackId == undefined ? null : pendingQuotationModel.leadCallBackId : null;
        this.leadId = pendingQuotationModel ? pendingQuotationModel.leadId == undefined ? '' : pendingQuotationModel.leadId : '';
        this.callbackDateTime = pendingQuotationModel ? pendingQuotationModel.callbackDateTime == undefined ? '' : pendingQuotationModel.callbackDateTime : '';
        this.contactNumber = pendingQuotationModel ? pendingQuotationModel.contactNumber == undefined ? '' : pendingQuotationModel.contactNumber : '';
        this.contactNumberCountryCode = pendingQuotationModel ? pendingQuotationModel.contactNumberCountryCode == undefined ? '' : pendingQuotationModel.contactNumberCountryCode : '';
        this.comments = pendingQuotationModel ? pendingQuotationModel.comments == undefined ? '' : pendingQuotationModel.comments : '';
        this.createdUserId = pendingQuotationModel ? pendingQuotationModel.createdUserId == undefined ? '' : pendingQuotationModel.createdUserId : '';
        this.probabilityOfClosureId = pendingQuotationModel ? pendingQuotationModel.probabilityOfClosureId == undefined ? '' : pendingQuotationModel.probabilityOfClosureId : '';
    }
    quotationOutcomeId: string;
    leadCallBackId: string;
    leadId: string;
    callbackDateTime: string;
    contactNumber: string;
    contactNumberCountryCode: string;
    comments: string;
    createdUserId: string;
    probabilityOfClosureId?:string;
}

class NoSalesQuotationModel {
    constructor(noSalesQuotationModel?: NoSalesQuotationModel) {
        this.quotationOutcomeId = noSalesQuotationModel == undefined ? '' : noSalesQuotationModel.quotationOutcomeId == undefined ? '' : noSalesQuotationModel.quotationOutcomeId;
        this.quotationVersionId = noSalesQuotationModel == undefined ? '' : noSalesQuotationModel.quotationVersionId == undefined ? '' : noSalesQuotationModel.quotationVersionId;
        this.outcomeId = noSalesQuotationModel == undefined ? '' : noSalesQuotationModel.outcomeId == undefined ? '' : noSalesQuotationModel.outcomeId;
        this.isCustomerAcceptCompetitorOffer = noSalesQuotationModel == undefined ? true : noSalesQuotationModel.isCustomerAcceptCompetitorOffer == undefined ? true : noSalesQuotationModel.isCustomerAcceptCompetitorOffer;
        this.competitorName = noSalesQuotationModel == undefined ? '' : noSalesQuotationModel.competitorName == undefined ? '' : noSalesQuotationModel.competitorName;
        this.outcomeReasonId = noSalesQuotationModel == undefined ? '' : noSalesQuotationModel.outcomeReasonId == undefined ? '' : noSalesQuotationModel.outcomeReasonId;
        this.isCustomerNotBuyFromCompetitor = noSalesQuotationModel == undefined ? false : noSalesQuotationModel.isCustomerNotBuyFromCompetitor == undefined ? false : noSalesQuotationModel.isCustomerNotBuyFromCompetitor;
        this.isInstallation = noSalesQuotationModel == undefined ? false : noSalesQuotationModel.isInstallation == undefined ? false : noSalesQuotationModel.isInstallation;
        this.isService = noSalesQuotationModel == undefined ? false : noSalesQuotationModel.isService == undefined ? false : noSalesQuotationModel.isService;
        this.promotionalOfferNewsLetterTypeId = noSalesQuotationModel == undefined ? '' : noSalesQuotationModel.promotionalOfferNewsLetterTypeId == undefined ? '' : noSalesQuotationModel.promotionalOfferNewsLetterTypeId;
        this.comments = noSalesQuotationModel == undefined ? '' : noSalesQuotationModel.comments == undefined ? '' : noSalesQuotationModel.comments;
        this.newsLetterTypeId = noSalesQuotationModel == undefined ? '' : noSalesQuotationModel.newsLetterTypeId == undefined ? '' : noSalesQuotationModel.newsLetterTypeId;
        this.probabilityOfClosureId = noSalesQuotationModel == undefined ? null : noSalesQuotationModel.probabilityOfClosureId == undefined ? null : noSalesQuotationModel.probabilityOfClosureId;
        this.leadcallbackId = noSalesQuotationModel == undefined ? null : noSalesQuotationModel.leadcallbackId == undefined ? null : noSalesQuotationModel.leadcallbackId;
        this.timingOfDeal = noSalesQuotationModel == undefined ? null : noSalesQuotationModel.timingOfDeal == undefined ? null : noSalesQuotationModel.timingOfDeal;
        this.createdUserId = noSalesQuotationModel == undefined ? '' : noSalesQuotationModel.createdUserId == undefined ? '' : noSalesQuotationModel.createdUserId;

    }
    quotationOutcomeId: string;
    quotationVersionId: string;
    outcomeId: string;
    isCustomerAcceptCompetitorOffer: boolean;
    competitorName: string;
    outcomeReasonId: string;
    isCustomerNotBuyFromCompetitor: boolean;
    isInstallation: boolean;
    isService: boolean;
    promotionalOfferNewsLetterTypeId: string;
    comments: string;
    newsLetterTypeId: string;
    probabilityOfClosureId: number;
    leadcallbackId: string;
    timingOfDeal: string;
    createdUserId: string;
}

export {PendingQuotationModel, NoSalesQuotationModel}