import { defaultCountryCode } from "@app/shared";

class MarketingPreferenceModel {
    constructor(marketingPreferenceModel?: MarketingPreferenceModel) {
        this.leadId = marketingPreferenceModel == undefined ? '' : marketingPreferenceModel.leadId == undefined ? '' : marketingPreferenceModel.leadId;
        this.email = marketingPreferenceModel == undefined ? '' : marketingPreferenceModel.email == undefined ? '' : marketingPreferenceModel.email;
        this.smsNumber = marketingPreferenceModel == undefined ? '' : marketingPreferenceModel.smsNumber == undefined ? '' : marketingPreferenceModel.smsNumber;
        this.smsNumberCountryCode = marketingPreferenceModel == undefined ? defaultCountryCode : marketingPreferenceModel.smsNumberCountryCode == undefined ? defaultCountryCode : marketingPreferenceModel.smsNumberCountryCode;
        this.mobileNumber = marketingPreferenceModel == undefined ? '' : marketingPreferenceModel.mobileNumber == undefined ? '' : marketingPreferenceModel.mobileNumber;
        this.mobileNumberCountryCode = marketingPreferenceModel == undefined ? defaultCountryCode : marketingPreferenceModel.mobileNumberCountryCode == undefined ? defaultCountryCode : marketingPreferenceModel.mobileNumberCountryCode;
        this.landLineNumber = marketingPreferenceModel == undefined ? '' : marketingPreferenceModel.landLineNumber == undefined ? '' : marketingPreferenceModel.landLineNumber;
        this.landLineNumberCountryCode = marketingPreferenceModel == undefined ? defaultCountryCode : marketingPreferenceModel.landLineNumberCountryCode == undefined ? defaultCountryCode : marketingPreferenceModel.landLineNumberCountryCode;
        this.address1 = marketingPreferenceModel == undefined ? '' : marketingPreferenceModel.address1 == undefined ? '' : marketingPreferenceModel.address1;
        this.address2 = marketingPreferenceModel == undefined ? '' : marketingPreferenceModel.address2 == undefined ? '' : marketingPreferenceModel.address2;
        this.address3 = marketingPreferenceModel == undefined ? '' : marketingPreferenceModel.address3 == undefined ? '' : marketingPreferenceModel.address3;
        this.address4 = marketingPreferenceModel == undefined ? '' : marketingPreferenceModel.address4 == undefined ? '' : marketingPreferenceModel.address4;
        this.isEmail = marketingPreferenceModel == undefined ? false : marketingPreferenceModel.isEmail == undefined ? false : marketingPreferenceModel.isEmail;
        this.isSMS = marketingPreferenceModel == undefined ? false : marketingPreferenceModel.isSMS == undefined ? false : marketingPreferenceModel.isSMS;
        this.isMobileNumber = marketingPreferenceModel == undefined ? false : marketingPreferenceModel.isMobileNumber == undefined ? false : marketingPreferenceModel.isMobileNumber;
        this.isLandLineNumber = marketingPreferenceModel == undefined ? false : marketingPreferenceModel.isLandLineNumber == undefined ? false : marketingPreferenceModel.isLandLineNumber;
        this.isPostal = marketingPreferenceModel == undefined ? false : marketingPreferenceModel.isPostal == undefined ? false : marketingPreferenceModel.isPostal;
        this.isNoMarketing = marketingPreferenceModel == undefined ? false : marketingPreferenceModel.isNoMarketing == undefined ? false : marketingPreferenceModel.isNoMarketing;
        this.isMorningBefore12 = marketingPreferenceModel == undefined ? null : marketingPreferenceModel.isMorningBefore12 == undefined ? null : marketingPreferenceModel.isMorningBefore12;
        this.isAfterNoonAfter12 = marketingPreferenceModel == undefined ? null : marketingPreferenceModel.isAfterNoonAfter12 == undefined ? null : marketingPreferenceModel.isAfterNoonAfter12;
        this.isLaterAfterNoonAfter16 = marketingPreferenceModel == undefined ? null : marketingPreferenceModel.isLaterAfterNoonAfter16 == undefined ? null : marketingPreferenceModel.isLaterAfterNoonAfter16;
        this.isSameAsSiteAddress = marketingPreferenceModel == undefined ? null : marketingPreferenceModel.isSameAsSiteAddress == undefined ? null : marketingPreferenceModel.isSameAsSiteAddress;
        this.postalCode = marketingPreferenceModel == undefined ? null : marketingPreferenceModel.postalCode == undefined ? null : marketingPreferenceModel.postalCode;
        this.leadMarketingPreferenceId = marketingPreferenceModel == undefined ? '' : marketingPreferenceModel.leadMarketingPreferenceId == undefined ? '' : marketingPreferenceModel.leadMarketingPreferenceId;
    }
    leadId: string;
    email: string;
    smsNumber: string;
    smsNumberCountryCode: string;
    mobileNumber: string;
    mobileNumberCountryCode: string;
    landLineNumber: string;
    landLineNumberCountryCode: string;
    address1:string;
    address2:string;
    address3:string;
    address4:string;
    isEmail: boolean;
    isSMS: boolean;
    isMobileNumber: boolean;
    isLandLineNumber: boolean;
    isPostal: boolean;
    isNoMarketing: boolean;
    isMorningBefore12: boolean;
    isAfterNoonAfter12: boolean;
    isLaterAfterNoonAfter16: boolean;
    isSameAsSiteAddress:boolean;
    postalCode: number;
    leadMarketingPreferenceId:string;
}

export { MarketingPreferenceModel };