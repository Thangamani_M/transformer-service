class PartitionCreationModel {
    constructor(partitionCreationModel?: PartitionCreationModel) {
        this.isPartitionedSystem = partitionCreationModel == undefined ? true : partitionCreationModel.isPartitionedSystem == undefined ? true : partitionCreationModel.isPartitionedSystem;
        this.paymentTypeId = partitionCreationModel == undefined ? null : partitionCreationModel.paymentTypeId == undefined ? null : partitionCreationModel.paymentTypeId;
        this.partitionTypeId = partitionCreationModel == undefined ? null : partitionCreationModel.partitionTypeId == undefined ? null : partitionCreationModel.partitionTypeId;
        this.noOfPartitions = partitionCreationModel == undefined ? 0 : partitionCreationModel.noOfPartitions == undefined ? 0 : partitionCreationModel.noOfPartitions;
        this.primaryLeadId = partitionCreationModel == undefined ? null : partitionCreationModel.primaryLeadId == undefined ? null : partitionCreationModel.primaryLeadId;
        this.customerId = partitionCreationModel == undefined ? null : partitionCreationModel.customerId == undefined ? null : partitionCreationModel.customerId;
        this.createdUserId = partitionCreationModel == undefined ? null : partitionCreationModel.createdUserId == undefined ? null : partitionCreationModel.createdUserId;
        this.partitions = partitionCreationModel == undefined ? [] : partitionCreationModel.partitions == undefined ? [] : partitionCreationModel.partitions;
    }
    isPartitionedSystem: boolean;
    paymentTypeId?: number;
    partitionTypeId?: number;
    noOfPartitions?: number;
    primaryLeadId?: string;
    customerId?: string;
    createdUserId?: string;
    partitions: Partitions[];
}
class Partitions {
    constructor(partitions?: Partitions) {
        this.partitionLeadId = partitions == undefined ? null : partitions.partitionLeadId == undefined ? null : partitions.partitionLeadId;
        this.partitionCode = partitions == undefined ? null : partitions.partitionCode == undefined ? null : partitions.partitionCode;
        this.partitionName = partitions == undefined ? null : partitions.partitionName == undefined ? null : partitions.partitionName;
        this.partitionId = partitions == undefined ? null : partitions.partitionId == undefined ? null : partitions.partitionId;
        this.isPrimary = partitions == undefined ? false : partitions.isPrimary == undefined ? false : partitions.isPrimary;
        this.isChecked = partitions == undefined ? false : partitions.isChecked == undefined ? false : partitions.isChecked;
        this.serialNumber = partitions == undefined ? 0 : partitions.serialNumber == undefined ? 0 : partitions.serialNumber;
        this.index = partitions == undefined ? 0 : partitions.index == undefined ? 0 : partitions.index;
        this.currentCursorIndex = partitions == undefined ? 0 : partitions.currentCursorIndex == undefined ? 0 : partitions.currentCursorIndex;
    }
    partitionLeadId: string;
    partitionCode?: string;
    partitionName: string;
    partitionId?: string;
    isPrimary?: boolean;
    isChecked?: boolean;
    serialNumber?: number;
    index?: number;
    currentCursorIndex: number;
}
class ServiceSubscriptionModel {
    constructor(serviceSubscriptionModel?: ServiceSubscriptionModel) {
        this.serviceCategoryId = serviceSubscriptionModel == undefined ? null : serviceSubscriptionModel.serviceCategoryId == undefined ? null : serviceSubscriptionModel.serviceCategoryId;
        this.suburbId = serviceSubscriptionModel == undefined ? null : serviceSubscriptionModel.suburbId == undefined ? null : serviceSubscriptionModel.suburbId;
        this.districtId = serviceSubscriptionModel == undefined ? null : serviceSubscriptionModel.districtId == undefined ? null : serviceSubscriptionModel.districtId;
        this.districtName = serviceSubscriptionModel == undefined ? null : serviceSubscriptionModel.districtName == undefined ? null : serviceSubscriptionModel.districtName;
        this.siteTypeId = serviceSubscriptionModel == undefined ? null : serviceSubscriptionModel.siteTypeId == undefined ? null : serviceSubscriptionModel.siteTypeId;
        this.leadId = serviceSubscriptionModel == undefined ? null : serviceSubscriptionModel.leadId == undefined ? null : serviceSubscriptionModel.leadId;
        this.customerId = serviceSubscriptionModel == undefined ? null : serviceSubscriptionModel.customerId == undefined ? null : serviceSubscriptionModel.customerId;
    }
    serviceCategoryId?: string;
    suburbId?: string;
    districtId?: string;
    siteTypeId?: string;
    leadId?: string;
    customerId?: string;
    districtName?: string;
}
class ServicesModel {
    constructor(serviceInfoModel?: ServicesModel) {
        this.leadServiceId = serviceInfoModel == undefined ? "" : serviceInfoModel.leadServiceId == undefined ? "" : serviceInfoModel.leadServiceId;
        this.servicePriceId = serviceInfoModel == undefined ? "" : serviceInfoModel.servicePriceId == undefined ? "" : serviceInfoModel.servicePriceId;
        this.serviceCode = serviceInfoModel == undefined ? "" : serviceInfoModel.serviceCode == undefined ? "" : serviceInfoModel.serviceCode;
        this.unitPrice = serviceInfoModel == undefined ? 0 : serviceInfoModel.unitPrice == undefined ? 0 : serviceInfoModel.unitPrice;
        this.serviceName = serviceInfoModel == undefined ? "" : serviceInfoModel.serviceName == undefined ? "" : serviceInfoModel.serviceName;
        this.unitTaxPrice = serviceInfoModel == undefined ? 0 : serviceInfoModel.unitTaxPrice == undefined ? 0 : serviceInfoModel.unitTaxPrice;
        this.qty = serviceInfoModel == undefined ? 0 : serviceInfoModel.qty == undefined ? 0 : serviceInfoModel.qty;
        this.isApproved = serviceInfoModel == undefined ? false : serviceInfoModel.isApproved == undefined ? false : serviceInfoModel.isApproved;
        this.taxPrice = serviceInfoModel == undefined ? 0 : serviceInfoModel.taxPrice == undefined ? 0 : serviceInfoModel.taxPrice;
        this.taxPercentage = serviceInfoModel == undefined ? 0 : serviceInfoModel.taxPercentage == undefined ? 0 : serviceInfoModel.taxPercentage;
        this.totalPrice = serviceInfoModel == undefined ? 0 : serviceInfoModel.totalPrice == undefined ? 0 : serviceInfoModel.totalPrice;
        this.servicePrice = serviceInfoModel == undefined ? 0 : serviceInfoModel.servicePrice == undefined ? 0 : serviceInfoModel.servicePrice;
        this.isQtyAvailable = serviceInfoModel == undefined ? false : serviceInfoModel.isQtyAvailable == undefined ? false : serviceInfoModel.isQtyAvailable;
        this.isChecked = serviceInfoModel == undefined ? false : serviceInfoModel.isChecked == undefined ? false : serviceInfoModel.isChecked;
    }
    leadServiceId: string;
    serviceName: string;
    serviceCode: string;
    servicePriceId: string;
    qty: number;
    unitPrice: number;
    unitTaxPrice: number;
    taxPrice: number;
    taxPercentage: number;
    totalPrice: number;
    servicePrice: number;
    isApproved: boolean;
    isQtyAvailable: boolean;
    isChecked: boolean;
}
class ServiceInfoFormModel {
    constructor(serviceInfoFormModel?: ServiceInfoFormModel) {
        this.customerId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.customerId == undefined ? null : serviceInfoFormModel.customerId;
        this.leadId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.leadId == undefined ? null : serviceInfoFormModel.leadId;
        this.billingIntervalId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.billingIntervalId == undefined ? null : serviceInfoFormModel.billingIntervalId;
        this.contractPeriodId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.contractPeriodId == undefined ? null : serviceInfoFormModel.contractPeriodId;
        this.paymentMethodId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.paymentMethodId == undefined ? null : serviceInfoFormModel.paymentMethodId;
        this.paymentTypeId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.paymentTypeId == undefined ? null : serviceInfoFormModel.paymentTypeId;
        this.createdUserId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.createdUserId == undefined ? null : serviceInfoFormModel.createdUserId;
        this.secondaryPartition = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.secondaryPartition == undefined ? null : serviceInfoFormModel.secondaryPartition;
        this.services = serviceInfoFormModel == undefined ? [] : serviceInfoFormModel.services == undefined ? [] : serviceInfoFormModel.services;
        this.partitionTypeId = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.partitionTypeId == undefined ? null : serviceInfoFormModel.partitionTypeId;
        this.noOfPartitions = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.noOfPartitions == undefined ? null : serviceInfoFormModel.noOfPartitions;
        this.isPartitionedSystem = serviceInfoFormModel == undefined ? false : serviceInfoFormModel.isPartitionedSystem == undefined ? false : serviceInfoFormModel.isPartitionedSystem;
        this.secondaryPartitionCode = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.secondaryPartitionCode == undefined ? null : serviceInfoFormModel.secondaryPartitionCode;
        this.primaryPartition = serviceInfoFormModel == undefined ? null : serviceInfoFormModel.primaryPartition == undefined ? null : serviceInfoFormModel.primaryPartition;
    }
    customerId?: string;
    leadId: string;
    billingIntervalId?: number;
    contractPeriodId?: number;
    paymentMethodId?: number;
    paymentTypeId?: number;
    createdUserId: string;
    secondaryPartition?: {
        partitionLeadId: string,
        partitionId: string,
        partitionTypeId: string,
    };
    services: ServicesModel[];
    partitionTypeId?: number;
    noOfPartitions?: number;
    isPartitionedSystem?: boolean;
    secondaryPartitionCode?: string;
    primaryPartition?: {
        partitionId: string,
        customerName: string,
        partitionCode: string,
        address: string
    };
}
class PriceAdjustModel {
    constructor(priceAdjustModel?: PriceAdjustModel) {
        this.leadPriceAdjustmentId = priceAdjustModel == undefined ? "" : priceAdjustModel.leadPriceAdjustmentId == undefined ? "" : priceAdjustModel.leadPriceAdjustmentId;
        this.leadId = priceAdjustModel == undefined ? "" : priceAdjustModel.leadId == undefined ? "" : priceAdjustModel.leadId;
        this.licenseTypeId = priceAdjustModel == undefined ? null : priceAdjustModel.licenseTypeId == undefined ? null : priceAdjustModel.licenseTypeId;

        this.quotationVersionId = priceAdjustModel == undefined ? "" : priceAdjustModel.quotationVersionId == undefined ? "" : priceAdjustModel.quotationVersionId;
        this.isGeneralDiscount = priceAdjustModel == undefined ? false : priceAdjustModel.isGeneralDiscount == undefined ? false : priceAdjustModel.isGeneralDiscount;
        this.isRewardPartnership = priceAdjustModel == undefined ? false : priceAdjustModel.isRewardPartnership == undefined ? false : priceAdjustModel.isRewardPartnership;
        this.rewardPartnershipId = priceAdjustModel == undefined ? "" : priceAdjustModel.rewardPartnershipId == undefined ? "" : priceAdjustModel.rewardPartnershipId;
        this.isMembershipNumberRequired = priceAdjustModel == undefined ? false : priceAdjustModel.isMembershipNumberRequired == undefined ? false : priceAdjustModel.isMembershipNumberRequired;
        this.rewardPartnershipRefNo = priceAdjustModel == undefined ? "" : priceAdjustModel.rewardPartnershipRefNo == undefined ? "" : priceAdjustModel.rewardPartnershipRefNo;
        this.rewardId = priceAdjustModel == undefined ? "" : priceAdjustModel.rewardId == undefined ? "" : priceAdjustModel.rewardId;
        this.marketingCampaignId = priceAdjustModel == undefined ? "" : priceAdjustModel.marketingCampaignId == undefined ? "" : priceAdjustModel.marketingCampaignId;
        this.isLSSPreferentialRate = priceAdjustModel == undefined ? false : priceAdjustModel.isLSSPreferentialRate == undefined ? false : priceAdjustModel.isLSSPreferentialRate;
        this.isCampaign = priceAdjustModel == undefined ? false : priceAdjustModel.isCampaign == undefined ? false : priceAdjustModel.isCampaign;
        this.isGeneralDiscount = priceAdjustModel == undefined ? false : priceAdjustModel.isGeneralDiscount == undefined ? false : priceAdjustModel.isGeneralDiscount;
        this.isRewardPartnership = priceAdjustModel == undefined ? false : priceAdjustModel.isRewardPartnership == undefined ? false : priceAdjustModel.isRewardPartnership;
        this.isLSS = priceAdjustModel == undefined ? false : priceAdjustModel.isLSS == undefined ? false : priceAdjustModel.isLSS;
        this.isCamp = priceAdjustModel == undefined ? false : priceAdjustModel.isCamp == undefined ? false : priceAdjustModel.isCamp;
        this.isGene = priceAdjustModel == undefined ? false : priceAdjustModel.isGene == undefined ? false : priceAdjustModel.isGene;
        this.isReward = priceAdjustModel == undefined ? false : priceAdjustModel.isReward == undefined ? false : priceAdjustModel.isReward;
        this.motivationReasonId = priceAdjustModel == undefined ? "" : priceAdjustModel.motivationReasonId == undefined ? "" : priceAdjustModel.motivationReasonId;
        this.isComplimentaryMonths = priceAdjustModel == undefined ? false : priceAdjustModel.isComplimentaryMonths == undefined ? false : priceAdjustModel.isComplimentaryMonths;
        this.complimentaryMonthId = priceAdjustModel == undefined ? "" : priceAdjustModel.complimentaryMonthId == undefined ? "" : priceAdjustModel.complimentaryMonthId;
        this.annualNetworkFeePeriodId = priceAdjustModel == undefined ? "" : priceAdjustModel.annualNetworkFeePeriodId == undefined ? "" : priceAdjustModel.annualNetworkFeePeriodId;
        this.isWaiverAdminFee = priceAdjustModel == undefined ? false : priceAdjustModel.isWaiverAdminFee == undefined ? false : priceAdjustModel.isWaiverAdminFee;
        this.isFixedContractFee = priceAdjustModel == undefined ? false : priceAdjustModel.isFixedContractFee == undefined ? false : priceAdjustModel.isFixedContractFee;
        this.fixedContractFeePeriodId = priceAdjustModel == undefined ? "" : priceAdjustModel.fixedContractFeePeriodId == undefined ? "" : priceAdjustModel.fixedContractFeePeriodId;
        this.itemDiscountAmount = priceAdjustModel == undefined ? 0 : priceAdjustModel.itemDiscountAmount == undefined ? 0 : priceAdjustModel.itemDiscountAmount;
        this.itemDiscountPercentage = priceAdjustModel == undefined ? 0 : priceAdjustModel.itemDiscountPercentage == undefined ? 0 : priceAdjustModel.itemDiscountPercentage;
        this.createdUserId = priceAdjustModel == undefined ? "" : priceAdjustModel.createdUserId == undefined ? "" : priceAdjustModel.createdUserId;
        this.serviceDiscountAmount = priceAdjustModel == undefined ? 0 : priceAdjustModel.serviceDiscountAmount == undefined ? 0 : priceAdjustModel.serviceDiscountAmount;
        this.serviceDiscountPercentage = priceAdjustModel == undefined ? 0 : priceAdjustModel.serviceDiscountPercentage == undefined ? 0 : priceAdjustModel.serviceDiscountPercentage;
        this.isWaiverProRateCharges = priceAdjustModel == undefined ? false : priceAdjustModel.isWaiverProRateCharges == undefined ? false : priceAdjustModel.isWaiverProRateCharges;
        this.leadItemPriceAdjustments = priceAdjustModel == undefined ? [] : priceAdjustModel.leadItemPriceAdjustments == undefined ? [] : priceAdjustModel.leadItemPriceAdjustments;
        this.leadServicePriceAdjustments = priceAdjustModel == undefined ? [] : priceAdjustModel.leadServicePriceAdjustments == undefined ? [] : priceAdjustModel.leadServicePriceAdjustments;
        this.comments = priceAdjustModel == undefined ? "" : priceAdjustModel.comments == undefined ? "" : priceAdjustModel.comments;

    }
    leadPriceAdjustmentId: string;
    comments: string;
    leadId: string;
    licenseTypeId: string;
    quotationVersionId: string;
    rewardPartnershipId: string;
    rewardId: string;
    marketingCampaignId: string;
    isMembershipNumberRequired: boolean;
    rewardPartnershipRefNo: string;
    isLSSPreferentialRate: boolean;
    isCampaign: boolean;
    isGeneralDiscount: boolean;
    isRewardPartnership: boolean;
    isLSS: boolean;
    isCamp: boolean;
    isGene: boolean;
    isReward: boolean;
    motivationReasonId: string;
    isComplimentaryMonths: boolean;
    complimentaryMonthId: string;
    annualNetworkFeePeriodId: string;
    isWaiverProRateCharges: boolean;
    isWaiverAdminFee: boolean;
    isFixedContractFee: boolean;
    fixedContractFeePeriodId: string;
    itemDiscountAmount: number;
    itemDiscountPercentage: number;
    createdUserId: string;
    serviceDiscountAmount: number;
    serviceDiscountPercentage: number;
    leadItemPriceAdjustments: LeadItemPriceAdjustments[];
    leadServicePriceAdjustments: LeadServicePriceAdjustments[];
}
class LeadItemPriceAdjustments {
    constructor(leadItemPriceAdjustments?: LeadItemPriceAdjustments) {
        this.leadItemPriceAdjustmentId = leadItemPriceAdjustments == undefined ? null : leadItemPriceAdjustments.leadItemPriceAdjustmentId == undefined ? null : leadItemPriceAdjustments.leadItemPriceAdjustmentId;
        this.systemTypeId = leadItemPriceAdjustments == undefined ? null : leadItemPriceAdjustments.systemTypeId == undefined ? null : leadItemPriceAdjustments.systemTypeId;
        this.systemTypeName = leadItemPriceAdjustments == undefined ? null : leadItemPriceAdjustments.systemTypeName == undefined ? null : leadItemPriceAdjustments.systemTypeName;
        this.isDiscountAmount = leadItemPriceAdjustments == undefined ? null : leadItemPriceAdjustments.isDiscountAmount == undefined ? null : leadItemPriceAdjustments.isDiscountAmount;
        this.isDiscountPercentage = leadItemPriceAdjustments == undefined ? true : leadItemPriceAdjustments.isDiscountPercentage == undefined ? true : leadItemPriceAdjustments.isDiscountPercentage;
        this.discountValue = leadItemPriceAdjustments == undefined ? 0 : leadItemPriceAdjustments.discountValue == undefined ? 0 : leadItemPriceAdjustments.discountValue;
        this.subTotal = leadItemPriceAdjustments == undefined ? 0 : leadItemPriceAdjustments.subTotal == undefined ? 0 : leadItemPriceAdjustments.subTotal;
        this.discountAmount = leadItemPriceAdjustments == undefined ? 0 : leadItemPriceAdjustments.discountAmount == undefined ? 0 : leadItemPriceAdjustments.discountAmount;
        this.adjustedTotal = leadItemPriceAdjustments == undefined ? 0 : leadItemPriceAdjustments.adjustedTotal == undefined ? 0 : leadItemPriceAdjustments.adjustedTotal;
        this.vatAmount = leadItemPriceAdjustments == undefined ? 0 : leadItemPriceAdjustments.vatAmount == undefined ? 0 : leadItemPriceAdjustments.vatAmount;
        this.totalAmount = leadItemPriceAdjustments == undefined ? 0 : leadItemPriceAdjustments.totalAmount == undefined ? 0 : leadItemPriceAdjustments.totalAmount;
        this.itemPercentage = leadItemPriceAdjustments == undefined ? 0 : leadItemPriceAdjustments.itemPercentage == undefined ? 0 : leadItemPriceAdjustments.itemPercentage;
        this.taxPercentage = leadItemPriceAdjustments == undefined ? 0 : leadItemPriceAdjustments.taxPercentage == undefined ? 0 : leadItemPriceAdjustments.taxPercentage;

    }
    leadItemPriceAdjustmentId: string;
    systemTypeId?: string;
    systemTypeName: string;
    isDiscountAmount?: boolean;
    itemPercentage?: number;
    isDiscountPercentage?: boolean;
    discountValue?: number;
    subTotal?: number;
    discountAmount: number;
    adjustedTotal: number;
    vatAmount: number;
    totalAmount: number;
    taxPercentage: number;
}
class LeadServicePriceAdjustments {
    constructor(leadServicePriceAdjustments?: LeadServicePriceAdjustments) {
        this.leadServicePriceAdjustmentId = leadServicePriceAdjustments == undefined ? null : leadServicePriceAdjustments.leadServicePriceAdjustmentId == undefined ? null : leadServicePriceAdjustments.leadServicePriceAdjustmentId;
        this.serviceCategoryId = leadServicePriceAdjustments == undefined ? null : leadServicePriceAdjustments.serviceCategoryId == undefined ? null : leadServicePriceAdjustments.serviceCategoryId;
        this.serviceCategoryName = leadServicePriceAdjustments == undefined ? null : leadServicePriceAdjustments.serviceCategoryName == undefined ? null : leadServicePriceAdjustments.serviceCategoryName;
        this.isDiscountAmount = leadServicePriceAdjustments == undefined ? null : leadServicePriceAdjustments.isDiscountAmount == undefined ? null : leadServicePriceAdjustments.isDiscountAmount;
        this.isDiscountPercentage = leadServicePriceAdjustments == undefined ? true : leadServicePriceAdjustments.isDiscountPercentage == undefined ? true : leadServicePriceAdjustments.isDiscountPercentage;
        this.discountValue = leadServicePriceAdjustments == undefined ? 0 : leadServicePriceAdjustments.discountValue == undefined ? 0 : leadServicePriceAdjustments.discountValue;
        this.subTotal = leadServicePriceAdjustments == undefined ? 0 : leadServicePriceAdjustments.subTotal == undefined ? 0 : leadServicePriceAdjustments.subTotal;
        this.discountAmount = leadServicePriceAdjustments == undefined ? 0 : leadServicePriceAdjustments.discountAmount == undefined ? 0 : leadServicePriceAdjustments.discountAmount;
        this.adjustedTotal = leadServicePriceAdjustments == undefined ? 0 : leadServicePriceAdjustments.adjustedTotal == undefined ? 0 : leadServicePriceAdjustments.adjustedTotal;
        this.vatAmount = leadServicePriceAdjustments == undefined ? 0 : leadServicePriceAdjustments.vatAmount == undefined ? 0 : leadServicePriceAdjustments.vatAmount;
        this.totalAmount = leadServicePriceAdjustments == undefined ? 0 : leadServicePriceAdjustments.totalAmount == undefined ? 0 : leadServicePriceAdjustments.totalAmount;
        this.servicePercentage = leadServicePriceAdjustments == undefined ? 0 : leadServicePriceAdjustments.servicePercentage == undefined ? 0 : leadServicePriceAdjustments.servicePercentage;
        this.taxPercentage = leadServicePriceAdjustments == undefined ? 0 : leadServicePriceAdjustments.taxPercentage == undefined ? 0 : leadServicePriceAdjustments.taxPercentage;
    }
    leadServicePriceAdjustmentId: string;
    serviceCategoryId?: string;
    serviceCategoryName: string;
    isDiscountAmount?: boolean;
    isDiscountPercentage?: boolean;
    discountValue?: number;
    subTotal?: number;
    discountAmount: number;
    adjustedTotal: number;
    vatAmount: number;
    totalAmount: number;
    servicePercentage: number;
    taxPercentage: number;
}
class BOCLinkToCustomerModel {
    constructor(bOCLinkToCustomerModel?: BOCLinkToCustomerModel) {
        this.address = bOCLinkToCustomerModel == undefined ? "" : bOCLinkToCustomerModel.address == undefined ? "" : bOCLinkToCustomerModel.address;
        this.customerId = bOCLinkToCustomerModel == undefined ? "" : bOCLinkToCustomerModel.customerId == undefined ? "" : bOCLinkToCustomerModel.customerId;
        this.customerName = bOCLinkToCustomerModel == undefined ? "" : bOCLinkToCustomerModel.customerName == undefined ? "" : bOCLinkToCustomerModel.customerName;
        this.addressId = bOCLinkToCustomerModel == undefined ? "" : bOCLinkToCustomerModel.addressId == undefined ? "" : bOCLinkToCustomerModel.addressId;
        this.contractId = bOCLinkToCustomerModel == undefined ? "" : bOCLinkToCustomerModel.contractId == undefined ? "" : bOCLinkToCustomerModel.contractId;
        this.contractRefNo = bOCLinkToCustomerModel == undefined ? "" : bOCLinkToCustomerModel.contractRefNo == undefined ? "" : bOCLinkToCustomerModel.contractRefNo;
        this.balanceOfContractTransferOptionId = bOCLinkToCustomerModel == undefined ? "" : bOCLinkToCustomerModel.balanceOfContractTransferOptionId == undefined ? "" : bOCLinkToCustomerModel.balanceOfContractTransferOptionId;
    }
    address?: string;
    customerId?: string;
    customerName?:string;
    addressId?: string;
    contractId?: string;
    contractRefNo?:string;
    balanceOfContractTransferOptionId?:string;
}

export {
    PartitionCreationModel, Partitions, ServiceSubscriptionModel, ServicesModel, ServiceInfoFormModel, PriceAdjustModel,
    LeadItemPriceAdjustments, LeadServicePriceAdjustments, BOCLinkToCustomerModel
}