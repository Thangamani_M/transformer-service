import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketingPreferenceInfoComponent } from './marketing-preference-info.component';

describe('MarketingPreferenceInfoComponent', () => {
  let component: MarketingPreferenceInfoComponent;
  let fixture: ComponentFixture<MarketingPreferenceInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarketingPreferenceInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketingPreferenceInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
