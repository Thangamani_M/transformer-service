import { Component } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material";
import { Router } from '@angular/router';
import { AppState } from "@app/reducers";
import {
  BreadCrumbModel, debounceTimeForSearchkeyword, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  prepareRequiredHttpParams, ResponseMessageTypes, setRequiredValidator,
  SnackbarService
} from "@app/shared";
import { ModuleName } from '@app/shared/enums';
import { CrudService, RxjsService } from "@app/shared/services";
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData, selectDynamicEagerLoadingStructureTypesState$ } from '@modules/others';
import {
  ItemInfoFormModel, ItemInfoModel,
  LeadCreationStepperParamsChangeAction, LeadCreationUserDataCreateAction, LeadCreationUserDataModel, LeadHeaderData, SalesModuleApiSuffixModels,
  selectLeadCreationUserDataState$, selectLeadHeaderDataState$, StepperParams, UpdateStockModalComponent
} from "@modules/sales";
import { LeadOutcomeStatusNames } from "@modules/sales/shared";
import { Store } from "@ngrx/store";
import { combineLatest, forkJoin, Observable, of } from "rxjs";
import { catchError, debounceTime, distinctUntilChanged, switchMap, take } from "rxjs/operators";
import { ReusablePrimeNGTableFeatureService } from '../../../../../../shared/services/reusable-primeng-table-features.service';

declare var $;
@Component({
  selector: "app-latest-installable-item-info",
  templateUrl: "./latest-installable-item-info.component.html"
})
export class LatestInstallableItemInfoComponent {
  params: StepperParams;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  itemInfoForm: FormGroup;
  captureForm: FormGroup
  systemTypes = [];
  itemListSummary = {};
  availableItemsCount = 0;
  objectKeys = Object.keys;
  subTotalSummary = {
    unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0,
    levyPercentageSubTotal: 0, levyPercentageVat: 0, levyPercentageTotal: 0,
    TotalSubTotal: 0, TotalVat: 0, TotalPrice: 0,
  };
  selectedItemsWithKeyValue = {};
  loggedInUserData: LoggedInUserModel;
  selectedItemsWithKey = [];
  isFormChangeDetected = false;
  captureInstructions = false;
  leadHeaderData = new LeadHeaderData();
  structureTypes = [];
  roofTypes = [];
  details;
  systemTypesData = [];
  selectedSystemId;
  structureTypeId = "";
  levyInfo;
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  breadCrumb: BreadCrumbModel;
  isLeadItems = false;
  tempArr = [];
  isAddOptionBtnDisabled = true;

  constructor(
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private router: Router, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    public rxjsService: RxjsService,
    private store: Store<AppState>,
    private dialog: MatDialog, private snackbarService: SnackbarService
  ) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD)
  }

  ngOnInit(): void {
    this.combineLatestNgrxDynamicData();
    this.createItemInfoForm();
    this.createCaptureForm();
    this.getPartitionCount().subscribe(res => {
      this.partitionsInfo = res.resources;
    });
    this.onBootstrapModalEventChanges();
    this.forkJoinRequests();
    this.onFormControlChanges();
  }

  combineLatestNgrxDynamicData() {
    combineLatest([
      this.store.select(selectDynamicEagerLoadingStructureTypesState$)])
      .subscribe((response) => {
        this.structureTypes = response[0];
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      [this.store.select(selectLeadCreationUserDataState$),
      this.store.select(loggedInUserData), this.store.select(selectLeadHeaderDataState$)]
    ).pipe(take(1)).subscribe((response) => {
      this.leadCreationUserDataModel = new LeadCreationUserDataModel(
        response[0]
      );
      this.loggedInUserData = new LoggedInUserModel(response[1]);
      this.leadHeaderData = new LeadHeaderData(response[2]);
      this.breadCrumb = {
        pageTitle: { key: 'Lead No', value: this.leadHeaderData.leadRefNo },
        items: [{
          key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
        }, { key: 'Add Stock Items' }]
      };
    });
  }

  openCapturePopUp() {
    this.captureInstructions = true;
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setGlobalLoaderProperty(false);
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.INSTALLATION_INSTRUCTIONS, this.leadHeaderData.leadId, false, null).subscribe(res => {
          if (res.resources && res.isSuccess && res.isSuccess) {
            this.captureForm.patchValue(res.resources);
          }
          this.rxjsService.setPopupLoaderProperty(false);
        });
  }

  onSubmitInstructions(type?: string) {
    if (type == 'cancel') {
      this.captureInstructions = false;
      this.rxjsService.setDialogOpenProperty(false);
    }
    else {
      if (this.captureForm.invalid) {
        return;
      }
      this.crudService.create(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.INSTALLATION_INSTRUCTIONS,
        this.captureForm.value).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.captureInstructions = false;
            this.rxjsService.setDialogOpenProperty(false);
          }
        });
    }
  }

  onDelete(index, obj) {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (obj.systemTypeId) {
            this.crudService
              .delete(
                ModulesBasedApiSuffix.SALES,
                SalesModuleApiSuffixModels.DELETE_SYSTEM_TYPE,
                undefined,
                prepareRequiredHttpParams({
                  leadId: this.leadHeaderData.leadId,
                  ids: obj.systemTypeId,
                  isDeleted: true,
                  modifiedUserId: this.loggedInUserData.userId
                })).subscribe((response: IApplicationResponse) => {
                  if (response.isSuccess && response.statusCode === 200) {
                    this.systemTypesData.splice(index, 1);
                    if (this.tempArr) {
                      this.tempArr = this.tempArr.filter(e => e != obj.systemTypeId);
                    }
                    if (obj.systemTypeId) {
                      let fitr = this.systemTypes.find(e => e.id == obj.systemTypeId);
                      fitr.isDisabled = false;
                    }
                  }
                });
          } else {
            this.systemTypesData.splice(index, 1);
            if (this.tempArr) {
              this.tempArr = this.tempArr.filter(e => e != obj.systemTypeId);
            }
            if (obj.systemTypeId) {
              let fitr = this.systemTypes.find(e => e.id == obj.systemTypeId);
              fitr.isDisabled = false;
            }
          }
        }
      });
  }

  openUpdateStock(dt, i) {
    if (!dt.systemTypeId) {
      this.snackbarService.openSnackbar("System Type is required", ResponseMessageTypes.WARNING);
      return;
    }
    this.selectedSystemId = dt.systemTypeId;
    const details = { ...dt, ...this.leadHeaderData };
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(UpdateStockModalComponent, { width: '950px', data: { data: details, index: i, leadData: this.leadHeaderData }, disableClose: true });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      this.systemTypes.forEach(e => {
        if (e.id == this.selectedSystemId) {
          e.isDisabled = true;
        }
      })
      this.systemTypesData.find(e => parseInt(e.systemTypeId) == this.selectedSystemId).leadItems = ele;
      this.calculateTotalAmountSummary();
    });
  }

  addOption() {
    let obj = {
      'leadItems': [],
      'systemTypeId': null,
      'systemTypes': this.systemTypes.filter(e => !e.isDisabled),
      'noOfPartitions': 0
    }
    if (this.systemTypesData.length > 0) {
      if (this.systemTypesData[0].leadItems.length > 0) {
        this.systemTypesData.splice(0, 0, obj);
      }
    } else if (this.systemTypes?.length > 0) {
      this.systemTypesData.push(obj);
    }
    else {
      this.snackbarService.openSnackbar('System Types didnot load properly', ResponseMessageTypes.WARNING);
    }
  }

  calculateRoundOff(value, tax) {
    value = parseFloat(value.toFixed(2));
    var num = parseFloat(((value * (tax / 100)) * 100).toFixed(2));
    return (Math.round(num) / 100 * Math.sign(num));
  }

  onQuantityChange(type, serviceSubItem: object): void {
    serviceSubItem["qty"] =
      type === "plus"
        ? serviceSubItem["qty"] + 1
        : serviceSubItem["qty"] === 1
          ? serviceSubItem["qty"]
          : serviceSubItem["qty"] - 1;
    serviceSubItem['subTotal'] = serviceSubItem['qty'] * serviceSubItem['unitPrice'];
    // serviceSubItem['unitTaxPrice'] =serviceSubItem['unitPrice'] * serviceSubItem['taxPercentage'] /100;
    serviceSubItem['unitTaxPrice'] = this.calculateRoundOff(serviceSubItem['unitPrice'], serviceSubItem['taxPercentage']);
    serviceSubItem['vat'] = this.calculateRoundOff((serviceSubItem['qty'] * serviceSubItem['unitPrice']), serviceSubItem['taxPercentage']);
    serviceSubItem['total'] = serviceSubItem['subTotal'] + serviceSubItem['vat'];
    serviceSubItem['total'] = serviceSubItem['total'];
    serviceSubItem['subTotal'] = serviceSubItem['subTotal']
    serviceSubItem['vat'] = serviceSubItem['vat']
    serviceSubItem['total'] = serviceSubItem['total']
    this.isFormChangeDetected = true;
    let serviceAgreementStepsParams = { ...this.params };
    serviceAgreementStepsParams.isFormChangeDetected = this.isFormChangeDetected;
    this.store.dispatch(new LeadCreationStepperParamsChangeAction({ serviceAgreementStepsParams }));
    this.calculateTotalAmountSummary();

  }

  calculateTotalAmountSummary(): void {
    this.subTotalSummary = {
      unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0, levyPercentageSubTotal: 0, levyPercentageVat: 0,
      levyPercentageTotal: 0,
      TotalSubTotal: 0, TotalVat: 0, TotalPrice: 0
    };
    this.objectKeys(this.systemTypesData).forEach((systemTypeId, ix: number) => {
      this.systemTypesData[systemTypeId]["totalAmount"] = { unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0 };
      this.subTotalSummary = {
        unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0, levyPercentageSubTotal: 0, levyPercentageVat: 0, levyPercentageTotal: 0,
        TotalSubTotal: 0, TotalVat: 0, TotalPrice: 0
      };
      this.systemTypesData[systemTypeId]["leadItems"].forEach((obj) => {
        this.subTotalSummary.subTotal += obj.subTotal;
        this.subTotalSummary.vat += obj.vat;
        this.subTotalSummary.unitPrice += obj.unitPrice;
        this.subTotalSummary.totalPrice += obj.total;
        this.subTotalSummary.totalPrice = this.subTotalSummary.totalPrice;
        this.systemTypesData[systemTypeId]["totalAmount"] = this.subTotalSummary;
      });
    });

  }

  saveDetails() {
    if (this.itemInfoForm.invalid) {
      return;
    }
    if (!this.details) {
      this.details = {};
    }
    this.isLeadItems = false;
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.isFormChangeDetected = true;
    let serviceAgreementStepsParams = { ...this.params };
    serviceAgreementStepsParams.isFormChangeDetected = this.isFormChangeDetected;
    this.store.dispatch(new LeadCreationStepperParamsChangeAction({ serviceAgreementStepsParams }));
    this.details['levyConfigId'] = this.itemInfoForm.controls.levyConfigId.value;
    this.details['createdUserId'] = this.loggedInUserData.userId
    this.details['systemTypes'] = this.systemTypesData;
    this.details['leadId'] = this.leadHeaderData.leadId;
    let payload = [];
    payload = JSON.parse(JSON.stringify(this.details));
    if (payload['systemTypes'].length > 0) {
      payload['systemTypes'].forEach(element => {
        if (element.leadItems.length == 0) {
          this.isLeadItems = true;
        }
        element.leadItems.forEach(item => {
          item.subTotal = item.subTotal.toFixed(2);
          item.tax = item.tax.toFixed(2);
          item.total = item.total.toFixed(2);
          item.unitPrice = item.unitPrice.toFixed(2);
          item.unitTaxPrice = item.unitTaxPrice.toFixed(2);
          item.vat = item.vat.toFixed(2);
        });
      });
    } else {
      this.isLeadItems = true;
    }
    if (!this.isLeadItems) {
    } else {
      this.snackbarService.openSnackbar("Option can't be empty", ResponseMessageTypes.ERROR);
      return;
    }
    if (payload['systemTypes'].length > 0) {
      payload['systemTypes'].forEach((element, i) => {
        let levyObj = {
          leadItemId: null,
          itemId: this.levyInfo['itemId'],
          itemPricingConfigId: "",
          qty: 1,
          vat: '0.00',
          isMiscellaneous: true,
          total: '0.00',
          miscellaneousConfigId: this.levyInfo['miscellaneousConfigId']
        }
        levyObj['itemPricingConfigId'] = element.leadItems[0].itemPricingConfigId;
        levyObj['subTotal'] = this.levyInfo['levyPercentage'] ? (element.totalAmount.subTotal * (this.levyInfo['levyPercentage'] / 100)).toFixed(2) : '0.00';
        levyObj['unitPrice'] = levyObj['subTotal'] ? levyObj['subTotal'] : '0.00';
        levyObj['vat'] = this.levyInfo['levyPercentage'] ? (element.totalAmount.vat * (this.levyInfo['levyPercentage'] / 100)).toFixed(2) : '0.00';
        levyObj['total'] = ((this.levyInfo['levyPercentage'] ? (element.totalAmount.subTotal * (this.levyInfo['levyPercentage'] / 100)) : 0) +
          (this.levyInfo['levyPercentage'] ? (element.totalAmount.vat * (this.levyInfo['levyPercentage'] / 100)) : 0)).toFixed(2);
        element.leadItems[element.leadItems.length] = levyObj;
      });
    }
    if (!this.isLeadItems) {
      let crudService: Observable<IApplicationResponse> = this.crudService.create(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.LEAD_ITEM, payload);
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.getItemsByLeadId();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    } else {
      this.snackbarService.openSnackbar("Option can't be empty", ResponseMessageTypes.WARNING);
    }
  }

  onBootstrapModalEventChanges(): void {
    $("#items_info_modal").on("shown.bs.modal", () => {
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#items_info_modal").on("hidden.bs.modal", () => {
      this.availableItemsCount = 0;
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  onFormControlChanges(): void {
    this.itemInfoForm
      .get("systemTypeId")
      .valueChanges.subscribe(() => {
        this.selectedItemsWithKey = [];
      });
    this.itemInfoForm.get('search').valueChanges.pipe(debounceTime(debounceTimeForSearchkeyword),
      distinctUntilChanged(), switchMap(() =>
        of(this.filterItemTypeDetailsBySearchOptions())
      )).subscribe();
  }

  partitionsInfo;
  forkJoinRequests(): void {
    forkJoin([this.getSystemTypes().pipe(catchError(error => of(error))),
    this.getMiscellaneousPriceTYpes().pipe(catchError(error => of(error)))]).subscribe(
      (resp: IApplicationResponse[]) => {
        resp.forEach((response: IApplicationResponse, ix: number) => {
          if (response.isSuccess && response.statusCode === 200) {
            switch (ix) {
              case 0:
                this.systemTypes = response.resources;
                this.systemTypes.forEach(element => {
                  element.isDisabled = false;
                });
                this.getItemsByLeadId();
                break;
              case 1:
                this.levyInfo = response.resources;
                break;
            }
            if (resp.length == ix + 1) {
              this.isAddOptionBtnDisabled = false;
            }
          }
        });
      }
    );
  }

  structureTypeChange(e) {
    this.structureTypeId = e;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ROOFTYPE, undefined, false, prepareGetRequestHttpParams(null, null, {
      structureTypeId: this.structureTypeId
    }), 1).subscribe(resp => {
      this.roofTypes = resp.resources;
      if (this.details) {
        this.roofTypeChange(this.details.levyConfigId)
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    });
  }

  roofTypeChange(e) {
    let found = this.roofTypes.find(item => item.levyConfigId == e);
    if (found && this.levyInfo) {
      this.levyInfo['levyPercentage'] = found.levyPercentage;
    }
  }

  calculate(sum, percent) {
    if (sum && percent) {
      let dt = sum * (percent / 100);
      return dt;
    } else {
      return 0;
    }
  }

  calculateGrantTotal(sum, percent) {
    sum=parseFloat(sum.toFixed(2));
    if (sum && percent) {
      let dt = this.calculateRoundOff(sum, percent);
      return sum + dt;
    } else {
      if (!sum) {
        return 0;
      } else if (!percent) {
        return sum;
      }
    }
  }

  onPrepareItemsByLeadIdResponse(response: IApplicationResponse): void {
    if (response.isSuccess && response.statusCode === 200) {
      if (this.objectKeys(response.resources).length > 0) {
        response.resources.forEach((obj) => {
          this.itemListSummary[obj.systemTypeId] = { values: [] };
          this.itemListSummary[obj.systemTypeId]["isChecked"] = true;
          this.itemListSummary[obj.systemTypeId]["systemTypeName"] = obj.systemTypeName;
          this.itemListSummary[obj.systemTypeId]["totalAmount"] = { unitPrice: 0, itemPrice: 0, taxPrice: 0, totalPrice: 0 };
          obj['leadItems'].forEach((leadItemObj: ItemInfoModel) => {
            this.itemListSummary[obj.systemTypeId]["totalAmount"]["unitPrice"] =
              this.itemListSummary[obj.systemTypeId]["totalAmount"]["unitPrice"] + leadItemObj.unitPrice;
            this.itemListSummary[obj.systemTypeId]["totalAmount"]["itemPrice"] =
              this.itemListSummary[obj.systemTypeId]["totalAmount"]["itemPrice"] + leadItemObj.itemPrice;
            this.itemListSummary[obj.systemTypeId]["totalAmount"]["taxPrice"] =
              this.itemListSummary[obj.systemTypeId]["totalAmount"]["taxPrice"] + leadItemObj.unitTaxPrice;
            this.itemListSummary[obj.systemTypeId]["totalAmount"]["totalPrice"] =
              this.itemListSummary[obj.systemTypeId]["totalAmount"]["totalPrice"] + (leadItemObj.itemPrice + leadItemObj.unitTaxPrice);

            this.itemListSummary[obj.systemTypeId]['values'].push(leadItemObj)
          });
        });
      }
      this.calculateTotalAmountSummary();
    }
  }

  getItemsByLeadId(): void {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.LEAD_ITEM, this.leadHeaderData.leadId, false, null)
      .subscribe((response: IApplicationResponse) => {
        let leadCreationUserDataModel = { ...this.leadCreationUserDataModel };
        leadCreationUserDataModel.itemsCount = response.resources ? response.resources.systemTypes.length : 0;
        this.leadCreationUserDataModel.itemsCount = leadCreationUserDataModel.itemsCount;
        this.store.dispatch(new LeadCreationUserDataCreateAction({ leadCreationUserDataModel }));
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.details = response.resources;
          if (this.details && this.details.levyConfigId) {
            this.itemInfoForm.controls.levyConfigId.setValue(this.details.levyConfigId)
          }
          if (this.details && this.details.structureTypeId) {
            this.itemInfoForm.controls.structureTypeId.setValue(this.details.structureTypeId)
            this.structureTypeChange(this.details.structureTypeId);
          }
          if (this.details && this.details.systemTypes) {
            this.systemTypesData = this.details.systemTypes;
            this.systemTypesData.forEach((element, index) => {
              element.index = index + 1;
              element.leadItems.forEach((e, index) => {
                e['unitTaxPrice'] = e.subTotal * e['taxPercentage'] / 100;
                e.vat = e['unitTaxPrice'];
                e.total = e.subTotal + e.vat;
                e.tax = e.vat;
                if (e.isMiscellaneous) {
                  element.leadItems.splice(index, 1);
                }
              });
              this.systemTypes.forEach(e => {
                if (e.id == element.systemTypeId) {
                  e.isDisabled = true;
                }
              })
              element.systemTypes = this.systemTypes.filter(e => e.id == element.systemTypeId);
            });
            this.systemTypesData.forEach((element, index) => {
              element.leadItems.forEach((e, index) => {
                e['unitTaxPrice'] = this.calculateRoundOff(e.subTotal, e['taxPercentage']);
                e.vat = e['unitTaxPrice'];
                e.total = e.subTotal + e.vat;
                e.tax = e.vat;
              });
            });
            this.calculateTotalAmountSummary();
          } else {
            this.details = {};
          }
        }
        setTimeout(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }, 1000);
      });
  }

  changeSystemType(obj) {
    this.systemTypes.forEach(e => {
      if (e.id == obj.systemTypeId) {
        e.isDisabled = true;
      }
    });
  }

  createItemInfoForm(): void {
    let itemInfoFormModel = new ItemInfoFormModel();
    this.itemInfoForm = this.formBuilder.group({});
    this.objectKeys(itemInfoFormModel).forEach((key) => {
      this.itemInfoForm.addControl(key, new FormControl(itemInfoFormModel[key]));
    });
    this.itemInfoForm = setRequiredValidator(this.itemInfoForm, [
      "levyConfigId", "structureTypeId"
    ]);
  }

  createCaptureForm(): void {
    let captureFormModel = {
      "installationInstructionId": "",
      "createdUserId": "",
      "leadId": "",
      "installationNotes": ""
    }
    this.captureForm = this.formBuilder.group({});
    this.objectKeys(captureFormModel).forEach((key) => {
      this.captureForm.addControl(
        key,
        new FormControl(captureFormModel[key])
      );
    });
    this.captureForm = setRequiredValidator(this.captureForm, [
      "installationNotes"
    ]);
    this.captureForm.get('createdUserId').setValue(this.loggedInUserData.userId);
    this.captureForm.get('leadId').setValue(this.leadHeaderData.leadId);
  }

  getSystemTypes(): Observable<IApplicationResponse> {
    return this.crudService
      .dropdown(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.UX_SYSTEM_TYPES
      )
  }

  getPartitionCount(): Observable<IApplicationResponse> {
    return this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.PARTITIONS_COUNT,
        undefined,
        false,
        prepareRequiredHttpParams({
          leadId: this.leadHeaderData.leadId
        })
      )
  }

  getMiscellaneousPriceTYpes(): Observable<IApplicationResponse> {
    return this.crudService
      .dropdown(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_MISCELLANEOUS_CONFIG_PRICE, null, 1
      )
  }

  onArrowClick(str) {
    if (str == 'backward') {
      if (this.leadHeaderData.fromUrl == 'Leads List') {
        this.router.navigate(['/sales/task-management/leads/lead-services/add-edit']);
      }
      else {
        this.router.navigate(['/my-tasks/my-sales/my-leads/lead-services/add-edit']);
      }
    }
    else {
      if (this.leadHeaderData.fromUrl == 'Leads List') {
        this.router.navigate(['/sales/task-management/leads/marketing-preference']);
      }
      else {
        this.router.navigate(['/my-tasks/my-sales/my-leads/marketing-preference']);
      }
    }
  }

  onDeleteItem(item, stock) {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (stock["leadItemId"]) {
            this.crudService
              .delete(
                ModulesBasedApiSuffix.SALES,
                SalesModuleApiSuffixModels.LEAD_ITEM,
                undefined,
                prepareRequiredHttpParams({
                  leadId: this.leadHeaderData.leadId,
                  ids: stock["leadItemId"],
                  isDeleted: true,
                  modifiedUserId: this.loggedInUserData.userId
                }))
              .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                  this.systemTypesData.forEach(element => {
                    if (element.systemTypeId == item.systemTypeId) {
                      element.leadItems.forEach((stockItem, index) => {
                        if (stockItem.stockCode == stock.stockCode) {
                          element.leadItems.splice(index, 1)
                        }
                      });
                    }
                  });
                  this.calculateTotalAmountSummary();
                }
              });
          } else {
            this.systemTypesData.forEach(element => {
              if (element.systemTypeId == item.systemTypeId) {
                element.leadItems.forEach((stockItem, index) => {
                  if (stockItem.stockCode == stock.stockCode) {
                    element.leadItems.splice(index, 1)
                  }
                });
              }
            });
            this.calculateTotalAmountSummary();
          }
        }
      });
  }

  onResponse(response: IApplicationResponse): void {
    response.resources.forEach((respObj) => {
      if (this.objectKeys(this.itemListSummary).length > 0) {
        if (this.itemListSummary.hasOwnProperty(+this.itemInfoForm.value.systemTypeId)) {
          this.itemListSummary[this.itemInfoForm.value.systemTypeId]['values'].forEach((localObj) => {
            if (localObj['itemId'] === respObj['itemId']) {
              respObj["isChecked"] = true;
              respObj["qty"] = localObj['qty'];
            }
          });
        }
      }
    });
    // isQtyAvailable qty calculation
    response.resources.forEach((itemInfoModel: ItemInfoModel) => {
      if (itemInfoModel['isQtyAvailable']) {
        itemInfoModel.qty = itemInfoModel.qty ? itemInfoModel.qty : 1;
        itemInfoModel.itemPrice = +((itemInfoModel.qty * itemInfoModel.itemPrice).toFixed(2));
        itemInfoModel.taxPrice = +((itemInfoModel.qty * itemInfoModel.taxPrice).toFixed(2));
        itemInfoModel.totalPrice = +((itemInfoModel.itemPrice + itemInfoModel.taxPrice).toFixed(2));
      }
      else {
        itemInfoModel.totalPrice = +((itemInfoModel.itemPrice + itemInfoModel.taxPrice).toFixed(2));
      }
    });
    if (this.selectedItemsWithKey.length != 0) {
      response.resources.forEach(x => x.isChecked = (this.selectedItemsWithKey.find(y => y.itemId == x.itemId) != null ? true : x.isChecked));
    }
  }

  filterItemTypeDetailsBySearchOptions(pageIndex?: string, pageSize?: string): void {
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.NEW_LEAD_ITEM_GET, undefined,
        false, prepareGetRequestHttpParams(pageIndex, pageSize, {
          systemTypeId: this.itemInfoForm.get('systemTypeId').value,
          districtId: this.leadCreationUserDataModel.districtId,
          leadId: this.leadCreationUserDataModel.leadId,
          leadGroupId: this.leadCreationUserDataModel.leadGroupId,
          search: this.itemInfoForm.get('search').value
        }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.isSuccess) {
          this.availableItemsCount = response.resources.length;
          this.onResponse(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSystemTypeName(systemTypeId?: string): string {
    const constSystemTypeId = systemTypeId ? systemTypeId : this.itemInfoForm.value.systemTypeId;
    return this.systemTypes.find(s => s.id == constSystemTypeId).displayName;
  }

  removeItem(selectedSystemTypeId: string, itemObj: object) {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) return;
        if (itemObj["leadItemId"]) {
          this.crudService
            .delete(
              ModulesBasedApiSuffix.SALES,
              SalesModuleApiSuffixModels.LEAD_ITEM,
              itemObj["leadItemId"]).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                  this.objectKeys(this.itemListSummary).forEach((systemTypeId: string) => {
                    if (systemTypeId === selectedSystemTypeId) {
                      for (const [ix, val] of this.itemListSummary[systemTypeId]['values'].entries()) {
                        if (
                          val["leadItemId"] === itemObj["leadItemId"]
                        ) {
                          this.itemListSummary[systemTypeId]['values'].splice(ix, 1);
                          if (this.itemListSummary[systemTypeId]['values'].length === 0) {
                            delete this.itemListSummary[systemTypeId];
                          }
                          this.calculateTotalAmountSummary();
                        }
                      }
                    }
                  });
                  this.selectedItemsWithKeyValue = { ...this.itemListSummary };
                }
              });
        } else {
          this.objectKeys(this.itemListSummary).forEach((systemTypeId: string) => {
            if (systemTypeId === selectedSystemTypeId) {
              for (const [ix, val] of this.itemListSummary[systemTypeId]['values'].entries()) {
                if (val["itemId"] === itemObj["itemId"]) {
                  this.itemListSummary[systemTypeId]['values'].splice(ix, 1);
                  if (this.itemListSummary[systemTypeId]['values'].length === 0) {
                    delete this.itemListSummary[systemTypeId];
                  }
                  this.calculateTotalAmountSummary();
                }
              }
            }
          });
          this.selectedItemsWithKeyValue = { ...this.itemListSummary };
        }
      });
  }

  canDeactivate(): boolean {
    return true;
    if (this.isFormChangeDetected) {
      return confirm("You have unsaved changes.Are you sure to leave?");
    }
    else {
      return true;
    }
  }

  ngOnDestroy() {
    this.rxjsService.setFormChangesDetectionPropertyForPageReload(this.isFormChangeDetected);
  }
}
