import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from "@app/reducers";
import {
  BreadCrumbModel, clearFormControlValidators, CustomDirectiveConfig, enableFormControls, HttpCancelService,
  IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  prepareRequiredHttpParams, removeFormControlError,
  removeProperyFromAnObjectWithDeepCopy,
  ResponseMessageTypes, setRequiredValidator, SnackbarService
} from "@app/shared";
import { CrudService, RxjsService } from "@app/shared/services";
import { loggedInUserData, selectDynamicEagerLoadingMotivationReasonTypesState$, selectDynamicEagerLoadingRewardPartnershipsState$, selectStaticEagerLoadingFixedContractFeePeriodsState$ } from '@modules/others';
import { CampaignModuleApiSuffixModels } from "@modules/others/configuration/components/campaign-management/shared/enums/campaign.enum";
import { ReasonForMotivationTypes } from "@modules/others/configuration/models/reason-for-motivation-model";
import {
  btnActionTypes,
  LeadItemPriceAdjustments,
  LeadServicePriceAdjustments, PriceAdjustModel, SalesModuleApiSuffixModels,
  selectLeadCreationStepperParamsState$, selectLeadHeaderDataState$, ServiceAgreementStepsParams, StepperParams
} from "@modules/sales";
import { Store } from "@ngrx/store";
import { combineLatest, forkJoin, Observable } from "rxjs";
import { ReusablePrimeNGTableFeatureService } from '../../../../../../shared/services/reusable-primeng-table-features.service';
@Component({
  selector: "app-adjust-cost-info",
  templateUrl: "./adjust-cost-info.component.html"
})
export class AdjustCostInfoComponent {
  @Input() params: StepperParams;
  @Output() emittedEvent = new EventEmitter<StepperParams>();
  @ViewChild('itemModal', { static: false }) itemModal: ElementRef;
  leadServicePriceAdjustments: FormArray;
  leadItemPriceAdjustments: FormArray;
  loggedInUserData: LoggedInUserModel;
  priceAdjustmentForm: FormGroup;
  leadId;
  leadHeaderData;
  levyInfo = {};
  complmPeriods = [];
  fixedPeriods = [];
  annualPeriods = [];
  detailsData;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  rewardPartneships = [];
  motivationReasons = [];
  isGene: boolean = false;
  isLSS: boolean = false;
  isCamp: boolean = false;
  isReward: boolean = false;
  isLicenseTypeEnabled: boolean = false;
  rewardId: string = '';
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  doaDetails = {
    doaApprovalLevel: 0,
    doaLevelConfigId: ''
  };
  licenceTypes = [];
  breadCrumb: BreadCrumbModel;
  campaignList = []
  marketingCampaignId;
  extraClassNames = "";

  constructor(
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private httpCancelService: HttpCancelService
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.leadId = params.leadId;
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createPriceAdjustmentForm();
    this.forkJoinRequests();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectLeadHeaderDataState$),
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectStaticEagerLoadingFixedContractFeePeriodsState$),
      this.store.select(selectDynamicEagerLoadingRewardPartnershipsState$),
      this.store.select(selectDynamicEagerLoadingMotivationReasonTypesState$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.leadHeaderData = response[1];
      this.leadId = this.leadHeaderData.leadId;
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[2]);
      this.extraClassNames = this.serviceAgreementStepsParams.quotationVersionId ? 'disabled' : "";
      this.breadCrumb = {
        pageTitle: { key: 'Adjust Cost', value: this.leadHeaderData.leadRefNo },
        items: [{
          key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
        }, { key: 'Adjust Cost' }]
      };
      this.fixedPeriods = response[3];
      this.rewardPartneships = response[4];
      this.motivationReasons = response[5]?.filter(r => r.displayName == ReasonForMotivationTypes.GENERAL_DISCOUNT).map(mR => mR.motivationReasons)[0];
    });
  }

  changeServicePercentage(index) {
    if (this.getleadServicePriceAdjustments.controls[index].get("isDiscountPercentage").value == true) {
      this.getleadServicePriceAdjustments.controls[index].get("isDiscountAmount").setValue(false)
      this.getleadServicePriceAdjustments.controls[index].get("discountAmount").setValue("");
      this.getleadServicePriceAdjustments.controls[index].get("discountAmount").setValidators([Validators.max(100)]);
      this.getleadServicePriceAdjustments.controls[index].get("discountAmount").updateValueAndValidity();
    }
    this.onPercentageChanges(index, 'service');
  }

  changeServiceAmount(index) {
    if (this.getleadServicePriceAdjustments.controls[index].get("isDiscountAmount").value == true) {
      this.getleadServicePriceAdjustments.controls[index].get("isDiscountPercentage").setValue(false);
      this.getleadServicePriceAdjustments.controls[index].get("discountAmount").setValue("");
      this.getleadServicePriceAdjustments.controls[index].get("discountAmount").clearValidators();
      this.getleadServicePriceAdjustments.controls[index].get("discountAmount").updateValueAndValidity();
    }
    this.onPercentageChanges(index, 'service');
  }

  changeAmount(index) {
    if (this.getleadItemPriceAdjustments.controls[index].get("isDiscountAmount").value == true) {
      this.getleadItemPriceAdjustments.controls[index].get("isDiscountPercentage").setValue(false);
      this.getleadItemPriceAdjustments.controls[index].get("discountAmount").setValue("");
      this.getleadServicePriceAdjustments.controls[index].get("discountAmount").clearValidators();
      this.getleadServicePriceAdjustments.controls[index].get("discountAmount").updateValueAndValidity();
    }
    this.onPercentageChanges(index, 'item');
  }

  changePercentage(index) {
    if (this.getleadItemPriceAdjustments.controls[index].get("isDiscountPercentage").value == true) {
      this.getleadItemPriceAdjustments.controls[index].get("isDiscountAmount").setValue(false);
      this.getleadItemPriceAdjustments.controls[index].get("discountAmount").setValue("");
      this.getleadItemPriceAdjustments.controls[index].get("discountAmount").setValidators([Validators.max(100)]);
      this.getleadItemPriceAdjustments.controls[index].get("discountAmount").updateValueAndValidity();
    }
    this.onPercentageChanges(index, 'item');
  }

  onPercentageChanges(i, str) {
    if (str == 'item') {
      this.getleadItemPriceAdjustments.controls[i].get('discountAmount').valueChanges.subscribe((levyPercentage: string) => {
        let isDiscountAmount = this.getleadItemPriceAdjustments.controls[i].get('isDiscountAmount').value;
        let isDiscountPercentage = this.getleadItemPriceAdjustments.controls[i].get('isDiscountPercentage').value;
        let discountValue = this.getleadItemPriceAdjustments.controls[i].get('discountAmount').value;
        if (!discountValue) {
          discountValue = 0;
        }
        let subtotal = this.getleadItemPriceAdjustments.controls[i].get('subTotal').value;
        if (isDiscountAmount) {
          this.getleadItemPriceAdjustments.controls[i].get("discountAmount").setValidators([Validators.max(subtotal)]);
        }
        let totalAmount = this.getleadItemPriceAdjustments.controls[i].get('totalAmount').value;
        let vatAmount = this.getleadItemPriceAdjustments.controls[i].get('vatAmount').value;
        let adjustedTotal = this.getleadItemPriceAdjustments.controls[i].get('adjustedTotal').value;
        if (isDiscountPercentage) {
          discountValue = (discountValue * parseFloat(subtotal)) / 100;
        }
        adjustedTotal = subtotal - discountValue;
        vatAmount = (adjustedTotal * this.detailsData?.taxPercentage) / 100;
        totalAmount = parseFloat(adjustedTotal) + parseFloat(vatAmount);
        this.getleadItemPriceAdjustments.controls[i].get('discountValue').setValue(parseFloat(discountValue).toFixed(2));
        this.getleadItemPriceAdjustments.controls[i].get('totalAmount').setValue(parseFloat(totalAmount).toFixed(2));
        this.getleadItemPriceAdjustments.controls[i].get('vatAmount').setValue(parseFloat(vatAmount).toFixed(2));
        this.getleadItemPriceAdjustments.controls[i].get('adjustedTotal').setValue(parseFloat(adjustedTotal).toFixed(2));
        if (levyPercentage == '0' || levyPercentage == '00' || levyPercentage == '0.0' ||
          levyPercentage == '0.00' || levyPercentage == '00.0' || levyPercentage == '00.00' ||
          levyPercentage == '00.' || levyPercentage == '0.') {
          this.getleadItemPriceAdjustments.controls[i].get('discountAmount').setErrors({ 'invalid': true });
        }
        else {
          this.getleadItemPriceAdjustments.controls[i] = removeFormControlError(this.getleadItemPriceAdjustments.controls[i] as FormGroup, 'invalid');
        }
      });
    } else {
      let isDiscountPercentage = this.getleadServicePriceAdjustments.controls[i].get('isDiscountPercentage').value;
      let isDiscountAmount = this.getleadServicePriceAdjustments.controls[i].get('isDiscountAmount').value;
      let discountValue = this.getleadServicePriceAdjustments.controls[i].get('discountAmount').value;
      if (!discountValue) {
        discountValue = 0;
      }
      let subtotal = this.getleadServicePriceAdjustments.controls[i].get('subTotal').value;
      if (isDiscountAmount) {
        this.getleadServicePriceAdjustments.controls[i].get("discountAmount").setValidators([Validators.max(subtotal)]);
      }
      let totalAmount = this.getleadServicePriceAdjustments.controls[i].get('totalAmount').value;
      let vatAmount = this.getleadServicePriceAdjustments.controls[i].get('vatAmount').value;
      let adjustedTotal = this.getleadServicePriceAdjustments.controls[i].get('adjustedTotal').value;
      if (isDiscountPercentage) {
        discountValue = (discountValue * parseFloat(subtotal)) / 100;
      }
      adjustedTotal = subtotal - discountValue;
      vatAmount = (adjustedTotal * this.detailsData?.taxPercentage) / 100;
      totalAmount = parseFloat(adjustedTotal) + parseFloat(vatAmount);
      this.getleadServicePriceAdjustments.controls[i].get('discountValue').setValue(parseFloat(discountValue).toFixed(2));
      this.getleadServicePriceAdjustments.controls[i].get('totalAmount').setValue(parseFloat(totalAmount).toFixed(2));
      this.getleadServicePriceAdjustments.controls[i].get('vatAmount').setValue(parseFloat(vatAmount).toFixed(2));
      this.getleadServicePriceAdjustments.controls[i].get('adjustedTotal').setValue(parseFloat(adjustedTotal).toFixed(2));
    }
  }

  forkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ANNUAL_COMPLI_PERIODS, undefined, true, null),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ANNUAL_PERIODS, undefined, true, null),
      this.crudService.get(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.UX_MARKETING_CAMPAIGN, undefined, true, null),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_LICENCE_TYPES, undefined, true, prepareRequiredHttpParams({
        districtId: this.leadHeaderData.districtId
      }))]
    ).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.complmPeriods = resp.resources;
              break;
            case 1:
              this.annualPeriods = resp.resources;
              break;
            case 2:
              this.campaignList = resp.resources;
              break;
            case 3:
              this.licenceTypes = resp.resources;
              this.getPriceAdjustmentByLeadId();
              break;
          }
        }
      });
    });
  }

  createPriceAdjustmentForm() {
    let priceAdjustModel = new PriceAdjustModel();
    this.priceAdjustmentForm = this.formBuilder.group({
      leadServicePriceAdjustments: this.formBuilder.array([]),
      leadItemPriceAdjustments: this.formBuilder.array([])
    });
    Object.keys(priceAdjustModel).forEach((key) => {
      this.priceAdjustmentForm.addControl(key, new FormControl(priceAdjustModel[key]));
    });
    this.priceAdjustmentForm = setRequiredValidator(this.priceAdjustmentForm, ["licenseTypeId", "motivationReasonId"]);
  }

  get getleadServicePriceAdjustments(): FormArray {
    if (!this.priceAdjustmentForm) return;
    return this.priceAdjustmentForm.get("leadServicePriceAdjustments") as FormArray;
  }

  get getleadItemPriceAdjustments(): FormArray {
    if (!this.priceAdjustmentForm) return;
    return this.priceAdjustmentForm.get("leadItemPriceAdjustments") as FormArray;
  }

  createleadServicePriceAdjustments(leadServicePriceAdjustments?: LeadServicePriceAdjustments): FormGroup {
    let leadServicePriceAdjustmentsModel = new LeadServicePriceAdjustments(leadServicePriceAdjustments);
    let formControls = {};
    Object.keys(leadServicePriceAdjustmentsModel).forEach((key) => {
      if (key === 'discountAmount') {
        formControls[key] = [{ value: leadServicePriceAdjustmentsModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: leadServicePriceAdjustmentsModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  createleadItemPriceAdjustments(leadItemPriceAdjustments?: LeadItemPriceAdjustments): FormGroup {
    let leadItemPriceAdjustmentsFormControlModel = new LeadItemPriceAdjustments(leadItemPriceAdjustments);
    let formControls = {};
    Object.keys(leadItemPriceAdjustmentsFormControlModel).forEach((key) => {
      if (key === 'discountAmount') {
        formControls[key] = [{ value: leadItemPriceAdjustmentsFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: leadItemPriceAdjustmentsFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  getLeadDetailsByLeadId(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEADS_HEADER, this.leadId, false, null, 1)
  }

  convertTodecimal(obj) {
    obj.adjustedTotal = obj.adjustedTotal ? parseFloat(obj.adjustedTotal).toFixed(2) : 0;
    obj.discountValue = obj.discountValue ? parseFloat(obj.discountValue).toFixed(2) : 0;
    obj.subTotal = obj.subTotal ? parseFloat(obj.subTotal).toFixed(2) : 0;
    obj.totalAmount = obj.totalAmount ? parseFloat(obj.totalAmount).toFixed(2) : 0;
    obj.vatAmount = obj.vatAmount ? parseFloat(obj.vatAmount).toFixed(2) : 0;
    return obj;
  }

  getPriceAdjustmentByLeadId() {
    if (this.leadItemPriceAdjustments) {
      this.leadItemPriceAdjustments.clear();
    }
    if (this.leadServicePriceAdjustments) {
      this.leadServicePriceAdjustments.clear();
    }
    let prepareGetRequestHttpParamsObj = {
      leadId: this.leadId,
      quotationVersionId: this.serviceAgreementStepsParams?.quotationVersionId,
      isRewardPartnership: this.priceAdjustmentForm.get('isReward').value,
      rewardPartnershipId: this.priceAdjustmentForm.get('rewardId').value,
      isCampaign: this.priceAdjustmentForm.get('isCamp').value,
      marketingCampaignId: this.priceAdjustmentForm.get('isCamp').value == true ? this.priceAdjustmentForm.get('marketingCampaignId').value : "",
      isLSSPreferentialRate: this.priceAdjustmentForm.get('isLSS').value
    }
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_PRICE_ADJUSTMENTS, undefined, false,
      prepareGetRequestHttpParams(null, null, prepareGetRequestHttpParamsObj)).subscribe(resp => {
        if (this.leadServicePriceAdjustments) {
          this.leadServicePriceAdjustments.clear();
        }
        if (this.leadItemPriceAdjustments) {
          this.leadItemPriceAdjustments.clear();
        }
        this.detailsData = resp.resources;
        if (this.detailsData) {
          if (this.detailsData.isLicenseTypeEnabled) {
            this.isLicenseTypeEnabled = true;
            let licenseTypeId = this.licenceTypes?.find(lT => lT['licenseTypeName'] == 'Default')?.licenseTypeId;
            if(licenseTypeId){
              this.detailsData.licenseTypeId = licenseTypeId;
            }
          } else {
            this.isLicenseTypeEnabled = false;
            this.priceAdjustmentForm.get('licenseTypeId').enable();
            this.priceAdjustmentForm = clearFormControlValidators(this.priceAdjustmentForm, ['licenseTypeId']);
          }
          this.doaDetails['doaApprovalLevel'] = this.detailsData.doaApprovalLevel;
          this.doaDetails['doaLevelConfigId'] = this.detailsData.doaLevelConfigId;
          if (!this.detailsData.annualNetworkFeePeriodId) {
            this.detailsData.annualNetworkFeePeriodId = "";
          }
          if (!this.detailsData.complimentaryMonthId) {
            this.detailsData.complimentaryMonthId = "";
          }
          if (!this.detailsData.fixedContractFeePeriodId) {
            this.detailsData.fixedContractFeePeriodId = "";
          }
          if (!this.detailsData.motivationReasonId) {
            this.detailsData.motivationReasonId = "";
          }
          if (!this.detailsData.rewardPartnershipId) {
            this.detailsData.rewardPartnershipId = "";
          }
          if (!this.detailsData.marketingCampaignId) {
            this.detailsData.marketingCampaignId = "";
          }
          if (this.detailsData.isRewardPartnership == false && this.detailsData.isLSSPreferentialRate == false && this.detailsData.isCampaign == false) {
            this.detailsData.isGeneralDiscount = true;
          }
          let priceAdjustModel = new PriceAdjustModel(this.detailsData);
          let copiedDetails = removeProperyFromAnObjectWithDeepCopy(priceAdjustModel, ['rewardPartnershipRefNo']);
          this.priceAdjustmentForm.patchValue(copiedDetails);
          this.priceAdjustmentForm.get('isCamp').setValue(this.detailsData.isCampaign);
          this.priceAdjustmentForm.get('isGene').setValue(this.detailsData.isGeneralDiscount);
          this.priceAdjustmentForm.get('isLSS').setValue(this.detailsData.isLSSPreferentialRate);
          this.priceAdjustmentForm.get('isReward').setValue(this.detailsData.isRewardPartnership);
          this.priceAdjustmentForm.get('rewardId').setValue(this.detailsData.rewardPartnershipId);
          if (this.isCamp) {
            this.priceAdjustmentForm.get('isCamp').setValue(this.isCamp);
            this.priceAdjustmentForm.get('isLSS').setValue(this.isLSS);
            this.priceAdjustmentForm.get('isReward').setValue(this.isReward);
            this.priceAdjustmentForm.get('isGene').setValue(this.isGene);
            this.priceAdjustmentForm.get('rewardId').setValue('');
          }
          if (this.isLSS) {
            this.priceAdjustmentForm.get('isCamp').setValue(this.isCamp);
            this.priceAdjustmentForm.get('isLSS').setValue(this.isLSS);
            this.priceAdjustmentForm.get('isReward').setValue(this.isReward);
            this.priceAdjustmentForm.get('isGene').setValue(this.isGene);
            this.priceAdjustmentForm.get('rewardId').setValue('');
          }
          if (this.isReward) {
            this.priceAdjustmentForm.get('isCamp').setValue(this.isCamp);
            this.priceAdjustmentForm.get('isLSS').setValue(this.isLSS);
            this.priceAdjustmentForm.get('isReward').setValue(this.isReward);
            this.priceAdjustmentForm.get('isGene').setValue(this.isGene);
            this.disbleFields();
          }
          if (this.isGene) {
            this.priceAdjustmentForm.get('isCamp').setValue(this.isCamp);
            this.priceAdjustmentForm.get('isLSS').setValue(this.isLSS);
            this.priceAdjustmentForm.get('isReward').setValue(this.isReward);
            this.priceAdjustmentForm.get('isGene').setValue(this.isGene);
            this.priceAdjustmentForm.get('rewardId').setValue('');
          }
          if (this.rewardId) {
            this.priceAdjustmentForm.get('rewardId').setValue(this.rewardId);
          }
          if (this.marketingCampaignId && this.priceAdjustmentForm.get('isCamp').value == true) {
            this.priceAdjustmentForm.get('marketingCampaignId').setValue(this.marketingCampaignId);
          }
          if (this.detailsData.leadServicePriceAdjustments && this.detailsData.leadServicePriceAdjustments.length > 0) {
            this.leadServicePriceAdjustments = this.getleadServicePriceAdjustments;
            this.detailsData.leadServicePriceAdjustments.forEach((obj) => {
              this.convertTodecimal(obj);
              this.leadServicePriceAdjustments.push(this.createleadServicePriceAdjustments(obj));
            });
          }
          if (this.detailsData.leadItemPriceAdjustments && this.detailsData.leadItemPriceAdjustments.length > 0) {
            this.leadItemPriceAdjustments = this.getleadItemPriceAdjustments;
            this.detailsData.leadItemPriceAdjustments.forEach((obj) => {
              this.convertTodecimal(obj);
              this.leadItemPriceAdjustments.push(this.createleadItemPriceAdjustments(obj));
            });
          }
          if (this.detailsData.isRewardPartnership) {
            this.changeisReward();
          }
          if (this.detailsData.isCampaign) {
            this.changeisCamp();
          }
          if (this.detailsData.isLSSPreferentialRate) {
            if (!this.isReward) {
              this.disableReward();
            }
          }
          if (this.detailsData.isGeneralDiscount) {
            if (!this.isReward) {
              this.disableReward();
            }
          }
          this.changeMonths();
          this.changeisFixedContractFee();
          if (this.serviceAgreementStepsParams?.quotationVersionId) {
            this.disbleFields();
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  disbleFields() {
    this.priceAdjustmentForm.get('complimentaryMonthId').disable({ emitEvent: false });
    this.priceAdjustmentForm.get('isComplimentaryMonths').disable({ emitEvent: false });
    this.priceAdjustmentForm.get('fixedContractFeePeriodId').disable({ emitEvent: false });
    this.priceAdjustmentForm.get('isFixedContractFee').disable({ emitEvent: false });
    this.priceAdjustmentForm.get('isWaiverAdminFee').disable({ emitEvent: false });
    this.priceAdjustmentForm.get('isWaiverProRateCharges').disable({ emitEvent: false });
    this.priceAdjustmentForm.get('annualNetworkFeePeriodId').disable({ emitEvent: false });
  }

  enableFields() {
    this.priceAdjustmentForm = enableFormControls(this.priceAdjustmentForm, ['complimentaryMonthId', 'isComplimentaryMonths', 'fixedContractFeePeriodId',
      'isFixedContractFee', 'isWaiverAdminFee', 'isWaiverProRateCharges', 'annualNetworkFeePeriodId']);
  }

  disableReward() {
    this.priceAdjustmentForm.get('rewardId').disable();
    this.priceAdjustmentForm.get('rewardPartnershipRefNo').disable();
    this.priceAdjustmentForm.get('rewardId').clearValidators();
    this.priceAdjustmentForm.get('rewardId').updateValueAndValidity();
  }

  changeMonths() {
    if (this.priceAdjustmentForm.get('isComplimentaryMonths').value) {
      this.priceAdjustmentForm.get('complimentaryMonthId').enable();
      this.priceAdjustmentForm.get('complimentaryMonthId').setValidators([Validators.required])
      this.priceAdjustmentForm.get('complimentaryMonthId').updateValueAndValidity();
    } else {
      this.priceAdjustmentForm.get('complimentaryMonthId').markAsUntouched();
      this.priceAdjustmentForm.get('complimentaryMonthId').setValue('')
      this.priceAdjustmentForm.get('complimentaryMonthId').disable({ emitEvent: false });
      this.priceAdjustmentForm.get('complimentaryMonthId').clearValidators();
      this.priceAdjustmentForm.get('complimentaryMonthId').updateValueAndValidity();
    }
  }

  changeisFixedContractFee() {
    if (this.priceAdjustmentForm.get('isFixedContractFee').value) {
      this.priceAdjustmentForm.get('fixedContractFeePeriodId').enable();
      this.priceAdjustmentForm.get('fixedContractFeePeriodId').setValidators([Validators.required])
      this.priceAdjustmentForm.get('fixedContractFeePeriodId').updateValueAndValidity();
    } else {
      this.priceAdjustmentForm.get('fixedContractFeePeriodId').disable();
      this.priceAdjustmentForm.get('fixedContractFeePeriodId').setValue('');
      this.priceAdjustmentForm.get('fixedContractFeePeriodId').clearValidators();
      this.priceAdjustmentForm.get('fixedContractFeePeriodId').updateValueAndValidity();
    }
  }

  changeisReward() {
    if (this.priceAdjustmentForm.get('isReward').value) {
      this.isReward = true;
      this.isCamp = false;
      this.isGene = false;
      this.isLSS = false;
      this.priceAdjustmentForm.get('motivationReasonId').clearValidators();
      this.priceAdjustmentForm.get('motivationReasonId').updateValueAndValidity();
      this.priceAdjustmentForm.get('isGene').setValue(false);
      this.priceAdjustmentForm.get('isLSS').setValue(false);
      this.priceAdjustmentForm.get('isCamp').setValue(false);
      this.priceAdjustmentForm.get('rewardId').enable();
      this.priceAdjustmentForm.get('rewardId').setValidators([Validators.required])
      this.priceAdjustmentForm.get('rewardId').updateValueAndValidity();
      this.priceAdjustmentForm.get('marketingCampaignId').setValue("");
      this.priceAdjustmentForm.get('marketingCampaignId').clearValidators();
      this.priceAdjustmentForm.get('marketingCampaignId').updateValueAndValidity();
      this.disbleFields();
    } else {
      this.enableFields();
    }
  }

  changeisCamp() {
    if (this.priceAdjustmentForm.get('isCamp').value) {
      this.isCamp = true;
      this.isGene = false;
      this.isLSS = false;
      this.isReward = false;
      this.rewardId = '';
      this.priceAdjustmentForm.get('isReward').setValue(false);
      this.priceAdjustmentForm.get('isGene').setValue(false);
      this.priceAdjustmentForm.get('rewardId').setValue("");
      this.priceAdjustmentForm.get('isLSS').setValue(false);
      this.priceAdjustmentForm.get('marketingCampaignId').setValidators([Validators.required])
      this.priceAdjustmentForm.get('marketingCampaignId').updateValueAndValidity();
      this.priceAdjustmentForm.get('motivationReasonId').clearValidators();
      this.priceAdjustmentForm.get('motivationReasonId').updateValueAndValidity();
      this.disableReward();
    } else {
      this.enableFields();
      this.priceAdjustmentForm.get('marketingCampaignId').setValue("");
      this.priceAdjustmentForm.get('marketingCampaignId').clearValidators();
      this.priceAdjustmentForm.get('marketingCampaignId').updateValueAndValidity();
    }
    this.priceAdjustmentForm.get('rewardPartnershipRefNo').setValue(null);
  }

  changeisGene() {
    if (this.priceAdjustmentForm.get('isGene').value) {
      this.isGene = true;
      this.isLSS = false;
      this.isReward = false;
      this.isCamp = false;
      this.rewardId = '';
      this.priceAdjustmentForm.get('isReward').setValue(false);
      this.priceAdjustmentForm.get('isLSS').setValue(false);
      this.priceAdjustmentForm.get('isCamp').setValue(false);
      this.priceAdjustmentForm.get('rewardId').setValue("");
      this.priceAdjustmentForm.get('motivationReasonId').setValidators([Validators.required])
      this.priceAdjustmentForm.get('motivationReasonId').updateValueAndValidity();
      this.priceAdjustmentForm.get('marketingCampaignId').setValue("");
      this.priceAdjustmentForm.get('marketingCampaignId').clearValidators();
      this.priceAdjustmentForm.get('marketingCampaignId').updateValueAndValidity();
      this.disableReward();
      this.enableFields();
      this.disableReward();
      this.getPriceAdjustmentByLeadId();
    }
    this.priceAdjustmentForm.get('rewardPartnershipRefNo').setValue(null);
  }

  changeisLSS() {
    if (this.priceAdjustmentForm.get('isLSS').value) {
      this.isLSS = true;
      this.isReward = false;
      this.isGene = false;
      this.isCamp = false;
      this.priceAdjustmentForm.get('motivationReasonId').clearValidators();
      this.priceAdjustmentForm.get('motivationReasonId').updateValueAndValidity();
      this.rewardId = '';
      this.priceAdjustmentForm.get('isReward').setValue(false);
      this.priceAdjustmentForm.get('rewardId').setValue("");
      this.priceAdjustmentForm.get('isGene').setValue(false);
      this.priceAdjustmentForm.get('isCamp').setValue(false);
      this.enableFields();
      this.disableReward();
      this.getPriceAdjustmentByLeadId();
      this.priceAdjustmentForm.get('marketingCampaignId').setValue("");
      this.priceAdjustmentForm.get('marketingCampaignId').clearValidators();
      this.priceAdjustmentForm.get('marketingCampaignId').updateValueAndValidity();
    } else {
      this.enableFields();
    }
    this.priceAdjustmentForm.get('rewardPartnershipRefNo').setValue(null);
  }

  rewardPartnershipIdChange(e) {
    let obj = this.priceAdjustmentForm.get('rewardId').value;
    this.rewardId = obj;
    this.priceAdjustmentForm.get('rewardPartnershipId').setValue(obj)
    let found = this.rewardPartneships.find(e => e.id == obj);
    if (found) {
      this.priceAdjustmentForm.get('isMembershipNumberRequired').setValue(found.isMembershipNumberRequired);
    }
    if (this.priceAdjustmentForm.get('isReward').value) {
      this.priceAdjustmentForm.get('rewardPartnershipRefNo').enable();
    }
    this.getPriceAdjustmentByLeadId();
  }

  campaignIdChange(e) {
    let obj = this.priceAdjustmentForm.get('marketingCampaignId').value;
    this.marketingCampaignId = obj
    this.getPriceAdjustmentByLeadId()
  }

  addServicesAmt() {
    let dt = this.priceAdjustmentForm.get('leadServicePriceAdjustments').value;
    let sum = 0;
    if (dt && dt.length > 0) {
      if (!this.priceAdjustmentForm.value.rewardId && this.priceAdjustmentForm.value.isReward) {
        sum = 0;
      } else {
        dt.forEach(element => {
          sum = sum + parseFloat(element.discountValue);
        });
      }
      this.priceAdjustmentForm.get('serviceDiscountAmount').setValue(JSON.parse(JSON.stringify(sum.toFixed(2))));
      return 'R ' + (sum).toFixed(2);
    } else {
      return 'R ' + 0;
    }
  }

  addServicesPerccent() {
    let dt = this.priceAdjustmentForm.get('leadServicePriceAdjustments').value;
    let sum = 0;
    let adjust: number = 0;
    let finalTotal: number = 0;
    if (dt && dt.length > 0) {
      dt.forEach(element => {
        if (this.priceAdjustmentForm.value.isGene) {
          sum = sum + parseFloat(element.discountValue);
          adjust = adjust + parseFloat(element.subTotal);
          finalTotal = (sum / adjust) * 100;
        } else {
          if (!this.priceAdjustmentForm.value.rewardId && this.priceAdjustmentForm.value.isReward) {
            finalTotal = 0;
          } else {
            finalTotal = finalTotal + parseFloat(element.servicePercentage);
          }
        }
      });
      this.priceAdjustmentForm.get('serviceDiscountPercentage').setValue((finalTotal).toFixed(2));
      return (finalTotal).toFixed(2) + ' %';
    } else {
      return 0 + ' %';
    }
  }

  addItemAmt() {
    let dt = this.priceAdjustmentForm.get('leadItemPriceAdjustments').value;
    let sum = 0;
    if (dt && dt.length > 0) {
      if (!this.priceAdjustmentForm.value.rewardId && this.priceAdjustmentForm.value.isReward) {
        sum = 0;
      } else {
        dt.forEach(element => {
          sum = sum + parseFloat(element.discountValue);
        });
        this.priceAdjustmentForm.get('itemDiscountAmount').setValue(JSON.parse(JSON.stringify(sum.toFixed(2))));
      }
      return 'R ' + (sum).toFixed(2);
    } else {
      return 'R ' + 0;
    }
  }

  addItemPerccent() {
    let dt = this.priceAdjustmentForm.get('leadItemPriceAdjustments').value;
    let sum = 0;
    let adjust: number = 0;
    let finalTotal: number = 0;
    if (dt && dt.length > 0) {
      dt.forEach(element => {
        if (this.priceAdjustmentForm.value.isGene) {
          sum = sum + parseFloat(element.discountValue);
          adjust = adjust + parseFloat(element.subTotal);
          finalTotal = (sum / adjust) * 100;
        } else {
          if (!this.priceAdjustmentForm.value.rewardId && this.priceAdjustmentForm.value.isReward) {
            finalTotal = 0;
          } else {
            finalTotal = finalTotal + parseFloat(element.itemPercentage);
          }
        }
      });
      this.priceAdjustmentForm.get('itemDiscountPercentage').setValue((finalTotal).toFixed(2));
      return (finalTotal).toFixed(2) + ' %';
    } else {
      return 0 + ' %';
    }
  }

  getDoaConfig() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DOA_LEVEL_PRICE_ADJUSTMENT, undefined, false, prepareGetRequestHttpParams(null, null, {
      leadId: this.leadId,
      quotationVersionId: this.serviceAgreementStepsParams.selectedQuotationVersionIds.split(","),
      serviceDiscount: this.priceAdjustmentForm.value.serviceDiscountPercentage,
      itemDiscount: this.priceAdjustmentForm.value.itemDiscountPercentage,
      servicePrice: this.priceAdjustmentForm.value.serviceDiscountAmount,
      itemPrice: this.priceAdjustmentForm.value.itemDiscountAmount,
    })).subscribe(resp => {
      this.doaDetails = resp.resources;
      this.rxjsService.setGlobalLoaderProperty(false)
    });
  }

  submit() {
    let isLss = this.priceAdjustmentForm.get('isLSS').value
    let isCamp = this.priceAdjustmentForm.get('isCamp').value
    let isReward = this.priceAdjustmentForm.get('isReward').value
    let isGene = this.priceAdjustmentForm.get('isGene').value
    let rewardId = this.priceAdjustmentForm.get('rewardId').value
    this.priceAdjustmentForm.get('isCampaign').setValue(isCamp)
    this.priceAdjustmentForm.get('isGeneralDiscount').setValue(isGene)
    this.priceAdjustmentForm.get('isLSSPreferentialRate').setValue(isLss)
    this.priceAdjustmentForm.get('isRewardPartnership').setValue(isReward)
    this.priceAdjustmentForm.get('rewardPartnershipId').setValue(rewardId)
    this.priceAdjustmentForm.get('createdUserId').setValue(this.loggedInUserData.userId)
    if (isLss) {
      this.priceAdjustmentForm.get('rewardPartnershipId').setValue(null)
      this.priceAdjustmentForm.get('rewardPartnershipRefNo').setValue(null)
    }
    if (this.priceAdjustmentForm.invalid) {
      return;
    }
    let obj = this.priceAdjustmentForm.getRawValue();
    if (!obj.leadServicePriceAdjustments && !obj.leadItemPriceAdjustments.length) {
      this.snackbarService.openSnackbar("Service price or ItemPrice either one is mandatory", ResponseMessageTypes.WARNING);
      return;
    }
    if (obj.leadServicePriceAdjustments.length == 0 && obj.leadItemPriceAdjustments.length == 0) {
      this.snackbarService.openSnackbar("Service price or ItemPrice either one is mandatory", ResponseMessageTypes.WARNING);
      return;
    }
    obj.rewardPartnershipRefNo = obj.rewardPartnershipRefNo ? obj.rewardPartnershipRefNo : null;
    obj.doaLevel = this.doaDetails?.doaApprovalLevel;
    obj.doaLevelConfigId = this.doaDetails?.doaLevelConfigId;
    let leadServicePriceAdjustments = obj.leadServicePriceAdjustments.filter(e => e.discountAmount != "")
    let leadItemPriceAdjustments = obj.leadItemPriceAdjustments.filter(e => e.discountAmount != "")
    obj.leadServicePriceAdjustments = leadServicePriceAdjustments;
    obj.leadItemPriceAdjustments = leadItemPriceAdjustments;
    obj.quotationVersionId = this.detailsData?.quotationVersionId;
    obj['isConfirmGenerateQuotation'] = true;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Generate Quote with Adjusted Cost<br>
    Are you sure you want to Generate Quote with Adjusted Cost?`).onClose?.subscribe(dialogResult => {
      if (dialogResult) {
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_PRICE_ADJUSTMENTS, obj).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.redirectToTab(btnActionTypes.SUBMIT);
          }
        });
      }
    });
  }

  redirectToTab(type?: btnActionTypes | string): void {
    if (type === btnActionTypes.SUBMIT || type === btnActionTypes.CANCEL) {
      if (this.leadHeaderData.fromUrl == 'Leads List') {
        this.router.navigate(["sales/task-management/leads/lead-items/quotes-info"], { queryParams: { leadId: this.leadHeaderData.leadId } });
      }
      else{
        this.router.navigate(["/my-tasks/my-sales/my-leads/lead-items/quotes-info"], { queryParams: { leadId: this.leadHeaderData.leadId } });
      }
    }
  }

  ngOndestroy() {
    this.rxjsService.setGlobalLoaderProperty(false);
  }
}
