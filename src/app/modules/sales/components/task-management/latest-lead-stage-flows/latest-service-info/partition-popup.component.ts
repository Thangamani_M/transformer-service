import { Component, Inject, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import {
  CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse,
  ModulesBasedApiSuffix, onBlurActionOnDuplicatedFormControl, onFormControlChangesToFindDuplicate, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { PartitionCreationModel, Partitions, SalesModuleApiSuffixModels } from '@modules/sales';
import { tap } from 'rxjs/operators';
import { DynamicConfirmByMessageConfirmationType, ReusablePrimeNGTableFeatureService } from '../../../../../../shared/services/reusable-primeng-table-features.service';
@Component({
  selector: 'app-partition-popup',
  templateUrl: './partition-popup.component.html'
})

export class PartitionPopupComponent implements OnInit {
  formConfigs = formConfigs;
  partitionCreationForm: FormGroup;
  alphaNumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  partitions: FormArray;
  allValidAndDisabledArrayValuesLength = 1;
  paymentTypeId: number;
  @ViewChildren('input') rows: QueryList<any>;

  constructor(@Inject(MAT_DIALOG_DATA) public partitionData,
    private dialog: MatDialog, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private crudService: CrudService, private snackbarService: SnackbarService) {
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createPartitionForm();
  }

  getPartitions(form): Array<any> {
    return form.controls.partitions.controls;
  }

  createPrimaryBasedSecondaryPartitionsForm(formGroup: FormGroup): FormGroup {
    formGroup = this.formBuilder.group({
      allPartitions: new FormArray([])
    });
    return formGroup;
  }

  get partitionsArray(): FormArray {
    if (!this.partitionCreationForm) return;
    return this.partitionCreationForm.get('partitions') as FormArray;
  }

  createPartitionForm(): void {
    this.partitionData.primaryLeadId = this.partitionData.leadId;
    let partitionCreationModel = new PartitionCreationModel(this.partitionData);
    this.partitionCreationForm = this.formBuilder.group({});
    Object.keys(partitionCreationModel).forEach((key) => {
      if (key === 'partitions') {
        this.partitionCreationForm.addControl(key, new FormArray([]));
      }
      else {
        this.partitionCreationForm.addControl(key, new FormControl(partitionCreationModel[key]));
      }
    });
    this.generateOrGetAllPartitions();
  }

  generateOrGetAllPartitions(noOfPartitions?: string): void {
    this.partitions = this.partitionsArray;
    if (this.partitions.invalid && noOfPartitions) {
      this.focusInAndOutFormArrayFields();
      return;
    }
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.PARTITIONS_GENERATE, undefined,
      false, prepareRequiredHttpParams(
        {
          noOfPartitions: noOfPartitions ? noOfPartitions : this.partitionCreationForm.get('noOfPartitions').value,
          leadId: this.partitionCreationForm.get('primaryLeadId').value,
          isSingle: noOfPartitions ? true : false
        }), 1)
      .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
          if (noOfPartitions) {
            this.partitions.insert(0, this.addNewPartition(resp.resources[0], 0, 'create'));
          }
          else {
            if (this.partitions) {
              while (this.partitions.length) {
                this.partitions.removeAt(this.partitions.length - 1);
              }
            }
            resp.resources.forEach((partitions: Partitions, index: number) => {
              this.partitions.push(this.addNewPartition(partitions, index, 'update'));
            });
          }
          this.prepareIsCheckedPropValues();
          // if the isChecked length zero save button is disabled logic
          for (let i = 0; i < this.partitionsArray.length; i++) {
            this.partitionsArray.controls[i].get('isChecked').valueChanges.subscribe((isChecked: boolean) => {
              const isCheckedArrayLength = this.partitions.value.filter(p => p['isChecked'] || p['isChecked'] == undefined).length;
              this.allValidAndDisabledArrayValuesLength = (isCheckedArrayLength == 1 && !isChecked) ? 0 : isCheckedArrayLength + 1;
            });
          }
          if (this.paymentTypeId == 2) {
            this.allValidAndDisabledArrayValuesLength = (this.partitionsArray.length == this.partitions.value.filter(p => p['isChecked'] || p['isPrimary']).length) ? 0 : 1;
          }
        }
      });
  }

  prepareIsCheckedPropValues() {
    this.paymentTypeId = +this.partitionCreationForm.get('paymentTypeId').value;
    this.partitions.controls.forEach((partitionAbstractControl, index: number) => {
      switch (this.paymentTypeId) {
        // pay for all
        case 1:
          partitionAbstractControl.get('isChecked').setValue(true);
          partitionAbstractControl.get('isChecked').disable();
          break;
        // pay partially
        case 2:
          if (partitionAbstractControl.get('isPrimary').value) {
            partitionAbstractControl.get('isChecked').setValue(true);
            partitionAbstractControl.get('isChecked').disable();
          }
          else if (partitionAbstractControl.get('partitionLeadId').value) {
            partitionAbstractControl.get('isChecked').setValue(true);
            partitionAbstractControl.get('isChecked').enable();
          }
          else if (!partitionAbstractControl.get('partitionLeadId').value) {
            partitionAbstractControl.get('isChecked').setValue(false);
            partitionAbstractControl.get('isChecked').enable();
          }
          break;
        // pay for standalone alias primary
        case 3:
          if (partitionAbstractControl.get('isPrimary').value) {
            partitionAbstractControl.get('isChecked').setValue(true);
          }
          else {
            partitionAbstractControl.get('isChecked').setValue(false);
          }
          partitionAbstractControl.get('isChecked').disable();
          break;
      }
    })
  }

  onChange() {
    if (this.paymentTypeId == 2) {
      this.allValidAndDisabledArrayValuesLength = (this.partitionsArray.length == this.partitions.value.filter(p => p['isChecked'] || p['isPrimary']).length) ? 0 : 1;
    }
  }

  addNewPartition(partitionModel: Partitions, index: number, type: string): FormGroup {
    if (type == 'create') {
      partitionModel.partitionLeadId = this.partitionData.leadId;
    }
    let partitionsModel = new Partitions(partitionModel);
    let formControls = {};
    Object.keys(partitionsModel).forEach((key) => {
      formControls[key] = [{
        value: key == 'index' ? index : partitionsModel[key],
        disabled: (key === 'partitionCode') ? true : false
      },
      key === 'partitionName' ?
        [Validators.required] : []];
    });
    return this.formBuilder.group(formControls);
  }

  onFormControlChanges(formControlName: string, formArray: FormArray, formArrayName: string, currentCursorIndex: number): void {
    onFormControlChangesToFindDuplicate(formControlName, formArray, formArrayName, currentCursorIndex, this.partitionCreationForm);
  }

  onBlur(formControlName: string, formArray: FormArray, e, index: number) {
    onBlurActionOnDuplicatedFormControl(formControlName, formArray, e, index);
  }

  addPartition(): void {
    if (this.partitionCreationForm.get('paymentTypeId').value == '3') {
      return;
    }
    if (this.partitions.length > 15) {
      this.snackbarService.openSnackbar("Maximum Partitions Limit is exceeded..!!", ResponseMessageTypes.WARNING);
      return;
    }
    this.generateOrGetAllPartitions('1');
  }

  removePartition(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, undefined,
      DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) return;
        if (this.partitions.controls[i].value.partitionId && this.partitions.length > 1) {
          this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.PARTITIONS, undefined,
            prepareRequiredHttpParams({
              ids: this.partitions.controls[i].value.partitionId,
              leadId: this.partitionData.leadId,
              isDeleted: true
            }), 1)
            .subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                this.partitions.removeAt(i);
              }
              if (this.partitions.length === 0) {
                this.addPartition();
              };
            });
        }
        else {
          this.partitions.removeAt(i);
        }
      });
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    });
  }

  onSubmit(): void {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.focusInAndOutFormArrayFields();
    let partitionsRawArray = [...this.partitions.getRawValue()];
    this.partitionCreationForm.value.partitions = partitionsRawArray;
    if (this.partitions.length === 0 || this.partitionCreationForm.invalid) return;
    this.partitionCreationForm.value.paymentTypeId = +this.partitionCreationForm.value.paymentTypeId;
    this.partitionCreationForm.value.contractPeriodId = this.partitionData.contractPeriodId;
    this.partitionCreationForm.value.billingIntervalId = this.partitionData.billingIntervalId;
    this.partitionCreationForm.value.paymentMethodId = this.partitionData.paymentMethodId;
    this.partitionCreationForm.value.addressId = this.partitionData.addressId;
    this.partitionCreationForm.value.partitions.forEach((partition) => {
      partition.partitionLeadId = partition.isChecked ? this.partitionData.leadId : null
    });
    this.crudService.create(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.PARTITIONS, this.partitionCreationForm.value).pipe(tap(() => {
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dialog.closeAll();
        }
      });
  }

  ngOnDestroy(): void {
    this.rxjsService.setPropertyValue(this.partitions.length);
    this.rxjsService.setDialogOpenProperty(false);
  }
}
