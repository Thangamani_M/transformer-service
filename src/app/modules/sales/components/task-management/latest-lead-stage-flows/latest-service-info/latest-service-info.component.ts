
import { ChangeDetectorRef, Component, ElementRef, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  addFormControls, BreadCrumbModel, ComponentProperties, CustomDirectiveConfig, formConfigs,
  IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, removeFormControlError, removeFormControls, setRequiredValidator, SnackbarService
} from '@app/shared';
import { ModuleName, ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import {
  loggedInUserData, selectStaticEagerLoadingBillingIntervalsState$,
  selectStaticEagerLoadingContractPeriodsState$, selectStaticEagerLoadingPartitionTypesState$,
  selectStaticEagerLoadingPaymentMethodsState$, selectStaticEagerLoadingPaymentTypesState$
} from '@modules/others';
import {
  BalanceOfContractLeadStageTwoPopupComponent,
  btnActionTypes, LeadCreationUserDataCreateAction, LeadCreationUserDataModel,

  LeadOutcomeStatusNames, SalesModuleApiSuffixModels, selectLeadCreationStepperParamsState$, selectLeadCreationUserDataState$,
  selectLeadHeaderDataState$, ServiceInfoFormModel, ServicesModel
} from '@modules/sales';
import { LeadHeaderData } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { DynamicConfirmByMessageConfirmationType, ReusablePrimeNGTableFeatureService } from '../../../../../../shared/services/reusable-primeng-table-features.service';
import { PartitionPopupComponent } from './partition-popup.component';
import { ServicePopupComponent } from './service-popup.component';

declare let $;
@Component({
  selector: 'app-latest-service-info',
  templateUrl: './latest-service-info.component.html'
})
export class LatestServiceInfoComponent {
  primaryContractPeriods = [];
  allContractPeriods = [];
  paymentFrequencies = [];
  lssContributionDetails;
  partitionTypes = [];
  paymentMethods = [];
  paymentTypes = [];
  secondaryPartitions = [];
  selectedPartitionType = '';
  selectedServiceOption = {};
  secondaryPartition = null;
  @ViewChildren('input') rows: QueryList<any>;
  noOfPartitionsBtn = 'Capture';
  breadCrumb: BreadCrumbModel;
  selectedItemsWithKeyValue = {};
  @ViewChild('serviceModal', { static: false }) serviceModal: ElementRef;
  formConfigs = formConfigs;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  leadHeaderData = new LeadHeaderData();
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  serviceInfoAddEditForm: FormGroup;
  serviceObj = {};
  componentProperties: ComponentProperties;
  serviceItemsLength = 0;
  otherServicesTotalAmount = {
    servicePrice: 0,
    taxPrice: 0,
    totalPrice: 0
  };
  valueAddedServicesTotalAmount = {
    servicePrice: 0,
    taxPrice: 0,
    totalPrice: 0
  };
  VAServices = 'Value Added Services';
  VAS = 'VAS';
  objectKeys = Object.keys;
  loggedInUserData: LoggedInUserModel;
  isFormChangeDetected = false;
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  @ViewChild('ngForm', { static: false }) ngForm;
  availablePartitions = [];
  serviceCategories;
  shouldDisableBOCButton = true;
  leadLandingPageDetails;
  resultAfterBOCIsApplied;
  areBOCRelatedServicesApplied = false;

  constructor(private crudService: CrudService,
    public rxjsService: RxjsService, private store: Store<AppState>, private dialog: MatDialog,
    private formBuilder: FormBuilder, private router: Router, private httpCancelService: HttpCancelService,
    private snackbarService: SnackbarService, private cdr: ChangeDetectorRef, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(selectLeadCreationUserDataState$), this.store.select(loggedInUserData),
    this.store.select(selectStaticEagerLoadingContractPeriodsState$), this.store.select(selectStaticEagerLoadingBillingIntervalsState$),
    this.store.select(selectStaticEagerLoadingPartitionTypesState$), this.store.select(selectStaticEagerLoadingPaymentMethodsState$),
    this.store.select(selectStaticEagerLoadingPaymentTypesState$),
    this.store.select(selectLeadHeaderDataState$), this.store.select(selectLeadCreationStepperParamsState$)])
      .pipe(take(1))
      .subscribe((response) => {
        this.leadCreationUserDataModel = new LeadCreationUserDataModel(response[0]);
        this.loggedInUserData = new LoggedInUserModel(response[1]);
        this.allContractPeriods = response[2];
        this.primaryContractPeriods = this.allContractPeriods?.filter(res => res['isPrimary']);
        this.paymentFrequencies = response[3];
        this.partitionTypes = response[4];
        this.paymentMethods = response[5];
        this.paymentTypes = response[6];
        this.leadHeaderData = new LeadHeaderData(response[7]);
        this.getServicesByLeadId();
        this.breadCrumb = {
          pageTitle: { key: 'Lead No', value: this.leadHeaderData.leadRefNo },
          items: [{
            key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
          }, { key: 'Add Services' }]
        };
      });
  }

  canDeactivate(boolean) {
    return boolean;
  }

  ngOnInit(): void {
    this.getForkJoinRequests();
    this.createServiceInfoForm();
    this.onFormControlChanges();
  }

  getForkJoinRequests(): void {
    if (!this.leadCreationUserDataModel.leadId) return;
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEADS_LANDING, this.leadCreationUserDataModel.leadId, false,
        undefined)]).subscribe((resp: IApplicationResponse[]) => {
          resp.forEach((res: IApplicationResponse, ix: number) => {
            if (res.isSuccess && res.statusCode === 200) {
              switch (ix) {
                case 0:
                  this.leadLandingPageDetails = new LeadHeaderData(res.resources);
                  break;
              }
            }
          });
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    });
  }

  onFormControlChanges(): void {
    this.serviceInfoAddEditForm.get('isPartitionedSystem').valueChanges.subscribe((isPartitionedSystem: boolean) => {
      if (isPartitionedSystem) {
        this.serviceInfoAddEditForm = addFormControls(this.serviceInfoAddEditForm, [{
          controlName: "partitionTypeId",
          defaultValue: this.serviceInfoAddEditForm.get('partitionTypeId') ? this.serviceInfoAddEditForm.get('partitionTypeId').value : null
        },
        { controlName: "paymentTypeId", defaultValue: this.serviceInfoAddEditForm.get('paymentTypeId') ? this.serviceInfoAddEditForm.get('paymentTypeId').value : null }]);
        this.serviceInfoAddEditForm = setRequiredValidator(this.serviceInfoAddEditForm, ["partitionTypeId", "paymentTypeId"]);
        this.serviceInfoAddEditForm.get('partitionTypeId').valueChanges.subscribe((partitionTypeId: string) => {
          if (!partitionTypeId) {
            this.selectedPartitionType = '';
            return;
          };
          this.selectedPartitionType = this.partitionTypes.find(p => p['id'] == +partitionTypeId).displayName;
          if (this.selectedPartitionType == 'Primary') {
            if (!this.serviceInfoAddEditForm.get('noOfPartitions').value) {
              this.serviceInfoAddEditForm.get('noOfPartitions').enable();
            }
            this.serviceInfoAddEditForm = setRequiredValidator(this.serviceInfoAddEditForm, ["noOfPartitions"]);
            this.serviceInfoAddEditForm.get('noOfPartitions').valueChanges.subscribe((noOfPartitions: string) => {
              if (noOfPartitions == '1' || +noOfPartitions > 15) {
                this.serviceInfoAddEditForm.get('noOfPartitions').setErrors({ invalid: true });
              }
              else {
                this.serviceInfoAddEditForm = removeFormControlError(this.serviceInfoAddEditForm, 'invalid');
              }
            });
            this.serviceInfoAddEditForm = removeFormControls(this.serviceInfoAddEditForm, ['secondaryPartitionCode']);
            this.serviceInfoAddEditForm = addFormControls(this.serviceInfoAddEditForm,
              [{ controlName: "paymentTypeId", defaultValue: null }]);
            this.serviceInfoAddEditForm = setRequiredValidator(this.serviceInfoAddEditForm, ["paymentTypeId"]);
          }
          else {
            this.serviceInfoAddEditForm = addFormControls(this.serviceInfoAddEditForm,
              ["secondaryPartitionCode"]);
            this.serviceInfoAddEditForm = setRequiredValidator(this.serviceInfoAddEditForm, ["secondaryPartitionCode"]);
            this.serviceInfoAddEditForm = removeFormControls(this.serviceInfoAddEditForm, ['paymentTypeId']);
          }
        });
      }
      else {
        this.serviceInfoAddEditForm = removeFormControls(this.serviceInfoAddEditForm, ["partitionTypeId", "paymentTypeId"]);
        this.serviceInfoAddEditForm.get('noOfPartitions').disable({ emitEvent: false, onlySelf: true });
        this.serviceInfoAddEditForm.get('noOfPartitions').setValue(null);
      }
    });
  }

  onOpenModal() {
    this.serviceInfoAddEditForm.value.leadId = this.leadHeaderData.leadId;
    this.serviceInfoAddEditForm.value.customerId = this.leadHeaderData.customerId;
    this.serviceInfoAddEditForm.value.createdUserId = this.loggedInUserData.userId;
    this.serviceInfoAddEditForm.value.addressId = this.leadLandingPageDetails.addressId;
    const dialogRef = this.dialog.open(PartitionPopupComponent, {
      width: "700px",
      minHeight: '300px',
      data: { ...this.serviceInfoAddEditForm.getRawValue(), ...this.serviceInfoAddEditForm.value },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe((dialogResult) => {
      if (dialogResult) return;
      if (this.noOfPartitionsBtn = 'Capture') {
        this.getAvailablePartitions();
      }
      this.noOfPartitionsBtn = 'Edit';
      this.rxjsService.getPropertyValue().subscribe((noOfPartitions) => {
        this.serviceInfoAddEditForm.get('noOfPartitions').setValue(noOfPartitions);
      });
    });
  }

  getSecondaryPartitionsBySearchKeyword(searchKeyword): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_PARTITION_SEARCH, null, false,
      prepareRequiredHttpParams({
        partitionCode: searchKeyword, leadId: this.leadHeaderData.leadId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.secondaryPartition = response.resources;
          this.serviceInfoAddEditForm.get('secondaryPartitionCode').setErrors({ invalid: false });
          this.serviceInfoAddEditForm.get('secondaryPartitionCode').updateValueAndValidity();
          this.serviceInfoAddEditForm.updateValueAndValidity();
          this.focusInAndOutFormArrayFields();
        }
        else {
          this.secondaryPartition = {};
          this.serviceInfoAddEditForm.get('secondaryPartitionCode').setErrors({ invalid: true });
          this.focusInAndOutFormArrayFields();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSecondarySearch() {
    if (!this.serviceInfoAddEditForm.get('secondaryPartitionCode').value) {
      return;
    }
    this.getSecondaryPartitionsBySearchKeyword(this.serviceInfoAddEditForm.get('secondaryPartitionCode').value);
  }

  onAddServices(): void {
    if (this.serviceInfoAddEditForm.invalid) return;
    let data = { ...this.serviceInfoAddEditForm.value, ...this.leadHeaderData };
    data['serviceObj'] = this.serviceObj;
    data['lssName'] = this.leadHeaderData.lssName;
    data['createdUserId'] = this.loggedInUserData.userId;
    const dialogRef = this.dialog.open(ServicePopupComponent, {
      width: "1200px",
      data,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (typeof dialogResult === 'boolean' && dialogResult) {
        this.isFormChangeDetected = false;
        return;
      }
      if (dialogResult.fromComponent && dialogResult.fromComponent == 'services') {
        this.calculateTotalAmountSummary();
      }
      else if (dialogResult.fromComponent && dialogResult.fromComponent == 'lssContribution') {
        this.getServicesByLeadId('fromPopup');
      }
    });
    let self = this;
    dialogRef.componentInstance.outputData.subscribe(ele => {
      self.serviceObj = ele.serviceObj;
      this.isFormChangeDetected = true;
    });
  }

  getServicesByLeadId(type = 'default'): void {
    if (!this.leadHeaderData.leadId || !this.leadHeaderData.customerId) return;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEAD_SERVICES_BY_ID, null,
      false, prepareRequiredHttpParams({ leadId: this.leadHeaderData.leadId, customerId: this.leadHeaderData.customerId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.resultAfterBOCIsApplied = null;
          this.shouldDisableBOCButton = !response.resources?.isBalanceOfContract;
          this.prepareDataToBeBoundAfterServerResponse(response, type);
          if (!response.resources?.contractPeriodId) {
            this.serviceInfoAddEditForm.get('contractPeriodId').setValue(this.primaryContractPeriods.find(c => c.isDefault).id);
          }
          if (!response.resources?.billingIntervalId) {
            this.serviceInfoAddEditForm.get('billingIntervalId').setValue(this.paymentFrequencies.find(p => p.isDefault).id);
          }
          if (!response.resources?.paymentMethodId) {
            this.serviceInfoAddEditForm.get('paymentMethodId').setValue(this.paymentMethods.find(p => p.isDefault).id);
          }
        }
      });
  }

  prepareDataToBeBoundAfterServerResponse(response: IApplicationResponse, type?: string) {
    let serviceInfoFormModel: ServiceInfoFormModel;
    let leadCreationUserDataModel = { ...this.leadCreationUserDataModel };
    leadCreationUserDataModel.servicesCount = response.resources.serviceCategories ? response.resources.serviceCategories.length : 0;
    this.store.dispatch(new LeadCreationUserDataCreateAction({ leadCreationUserDataModel }));
    if (type == 'fromPopup') {
      serviceInfoFormModel = new ServiceInfoFormModel({ ...this.serviceInfoAddEditForm.value, ...this.serviceInfoAddEditForm.getRawValue() });
      this.serviceObj = {};
    }
    else {
      serviceInfoFormModel = new ServiceInfoFormModel(response.resources);
    }
    if (serviceInfoFormModel.primaryPartition) {
      this.secondaryPartition = {
        customerName: serviceInfoFormModel.primaryPartition.customerName,
        partitionCode: serviceInfoFormModel.primaryPartition.partitionCode,
        address: serviceInfoFormModel.primaryPartition.address,
        partitionLeadId: this.leadCreationUserDataModel.leadId
      }
      this.serviceInfoAddEditForm.patchValue(serviceInfoFormModel);
      this.serviceInfoAddEditForm.get('secondaryPartitionCode').setValue(serviceInfoFormModel.secondaryPartitionCode, { emitEvent: false, onlySelf: true });
    }
    else {
      this.serviceInfoAddEditForm.patchValue(serviceInfoFormModel);
      if (serviceInfoFormModel.paymentTypeId && this.serviceInfoAddEditForm.get('paymentTypeId')) {
        this.serviceInfoAddEditForm.get('paymentTypeId').setValue(serviceInfoFormModel.paymentTypeId);
      }
    }
    if (serviceInfoFormModel.noOfPartitions) {
      this.noOfPartitionsBtn = 'Edit';
      this.serviceInfoAddEditForm.get('noOfPartitions').disable();
    }
    this.serviceInfoAddEditForm.get('isPartitionedSystem').setValue(serviceInfoFormModel.isPartitionedSystem);
    this.serviceCategories = response.resources.serviceCategories;
    if (response.resources.serviceCategories) {
      response.resources.serviceCategories.forEach((respObj) => {
        if (!type) {
          if (this.serviceObj[respObj['serviceCategoryName']]) {
            this.serviceObj[respObj['serviceCategoryName']]['values'] = [...this.serviceObj[respObj['serviceCategoryName']]['values'], ...respObj['services']];
          }
          else {
            this.serviceObj[respObj['serviceCategoryName']] = {};
            this.serviceObj[respObj['serviceCategoryName']]['values'] = respObj['services'];
            this.serviceObj[respObj['serviceCategoryName']]['serviceCategoryId'] = respObj['serviceCategoryId'];
          }
        }
        else {
          this.serviceObj[respObj['serviceCategoryName']] = {};
          this.serviceObj[respObj['serviceCategoryName']]['values'] = respObj['services'];
          this.serviceObj[respObj['serviceCategoryName']]['serviceCategoryId'] = respObj['serviceCategoryId'];
        }
      });
      this.calculateTotalAmountSummary();
    }
    if (this.availablePartitions && this.availablePartitions.length > 0) {
      this.servicePartitionsDataBindLogic();
    }
    else {
      this.getAvailablePartitions();
    }
    if (response.resources.lssContributionDetails) {
      this.serviceObj['LSS Scheme Name'] = {};
      this.serviceObj['LSS Scheme Name']['values'] = [];
      this.serviceObj['LSS Scheme Name']['totalAmount'] = {
        servicePrice: response.resources.lssContributionDetails.unitPrice,
        taxPrice: response.resources.lssContributionDetails.unitTaxPrice,
        totalPrice: response.resources.lssContributionDetails.totalPrice
      };
      let lssContributionObj = {
        taxPrice: response.resources.lssContributionDetails.unitTaxPrice,
        serviceName: response.resources.lssContributionDetails.lssName,
        serviceCode: response.resources.lssContributionDetails.lssRefNo,
        servicePrice: response.resources.lssContributionDetails.unitPrice
      }
      this.serviceObj['LSS Scheme Name']['values'].push({ ...response.resources.lssContributionDetails, ...lssContributionObj });
      this.calculateTotalAmountSummary();
    }
    setTimeout(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    }, 500);
  }

  createServiceInfoForm(): void {
    let serviceInfoFormModel = new ServiceInfoFormModel();
    this.serviceInfoAddEditForm = this.formBuilder.group({});
    Object.keys(serviceInfoFormModel).forEach((key) => {
      this.serviceInfoAddEditForm.addControl(key, new FormControl(serviceInfoFormModel[key]));
    });
    this.serviceInfoAddEditForm = setRequiredValidator(this.serviceInfoAddEditForm, [
      "contractPeriodId", "billingIntervalId", "paymentMethodId"
    ]);
  }

  getAvailablePartitions() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_SERVICE_PARTITIONS, null, false,
      prepareRequiredHttpParams({
        leadId: this.leadHeaderData.leadId,
        customerId: this.leadHeaderData.customerId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.availablePartitions = response.resources.map((obj) => {
            return { display: obj.partitionName, value: obj.partitionId };
          });
          this.servicePartitionsDataBindLogic();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  servicePartitionsDataBindLogic() {
    this.serviceCategories?.forEach((respObj) => {
      this.serviceObj[respObj['serviceCategoryName']]['values'].forEach((serviceSubObj) => {
        if (serviceSubObj['servicePartitions'] && serviceSubObj['servicePartitions'].length > 0) {
          serviceSubObj['selectedPartitionIds'] = serviceSubObj['servicePartitions']?.map(pO => pO['partitionId']);
        }
        else {
          serviceSubObj['selectedPartitionIds'] = [];
        }
      });
    });
  }

  getServicesAfterBOCIsApplied(payload): void {
    this.rxjsService.setDialogOpenProperty(false);
    this.rxjsService.setGlobalLoaderProperty(true);
    payload.leadId = this.leadHeaderData.leadId;
    payload.createdUserId = this.loggedInUserData.userId;
    payload.siteAddressId = this.leadLandingPageDetails.addressId;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.LEAD_BALANCE_OF_CONTRACT, payload)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.resultAfterBOCIsApplied = response.resources;
          // if BOC option is BOC Initiated then balanceOfContractTransferOptionId value is 1
          // if BOC option is Existing Contract then balanceOfContractTransferOptionId value is 2 
          this.areBOCRelatedServicesApplied = payload?.balanceOfContractTransferOptionId == 2 ? true : false;
          this.prepareDataToBeBoundAfterServerResponse(response);
          this.shouldDisableBOCButton = true;
        }
      });
  }

  onOpenBOCPopup() {
    const dialogReff = this.dialog.open(BalanceOfContractLeadStageTwoPopupComponent, {
      width: "750px",
      disableClose: true,
      data: {leadId:this.leadHeaderData.leadId},
    });
    dialogReff.componentInstance.outputData.subscribe((result) => {
      if (result.balanceOfContractTransferOptionId) {
        this.getServicesAfterBOCIsApplied(result);
      }
    });
  }

  onPartitionSelectionChanged(serviceSubItem, selectedPartitionIds) {
    if (selectedPartitionIds) {
      serviceSubItem.selectedPartitionIds = selectedPartitionIds;
    }
  }

  removeItem(serviceItemKey: string, serviceItemObj: object) {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, undefined,
      DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) return;
        let areAllServicesRemoved = false;
        if (serviceItemObj['leadServiceId']) {
          this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEAD_SERVICES_BY_ID, undefined,
            prepareRequiredHttpParams({
              ids: serviceItemObj['leadServiceId'],
              isDeleted: true,
              modifiedUserId: this.loggedInUserData.userId
            })).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                Object.keys(this.serviceObj).forEach((key: string) => {
                  if (key === serviceItemKey) {
                    for (const [ix, val] of this.serviceObj[key]['values'].entries()) {
                      if (val['leadServiceId'] === serviceItemObj['leadServiceId']) {
                        this.serviceObj[key]['values'].splice(ix, 1);
                        if (this.serviceObj[key]['values'].length === 0) {
                          delete this.serviceObj[key];
                          areAllServicesRemoved = true;
                        }
                        else {
                          areAllServicesRemoved = false;
                        }
                        this.calculateTotalAmountSummary();
                      }
                    }
                  }
                });
                let leadCreationUserDataModel = { ...this.leadCreationUserDataModel };
                leadCreationUserDataModel.servicesCount = areAllServicesRemoved ? 0 : 1;
                this.store.dispatch(new LeadCreationUserDataCreateAction({ leadCreationUserDataModel }));
                this.selectedItemsWithKeyValue = { ...this.serviceObj };
              }
            });
        }
        else if (serviceItemObj['lssId']) {
          this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.LSS_CONTRIBUTION, undefined,
            prepareRequiredHttpParams({
              ids: serviceItemObj['lssId'],
              leadId: serviceItemObj['leadId'],
              isDeleted: true,
              modifiedUserId: this.loggedInUserData.userId
            })).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                delete this.serviceObj['LSS Scheme Name'];
                this.calculateTotalAmountSummary();
              }
            });
        }
        else {
          Object.keys(this.serviceObj).forEach((key: string) => {
            if (key === serviceItemKey) {
              for (const [ix, val] of this.serviceObj[key]['values'].entries()) {
                if (val['servicePriceId'] === serviceItemObj['servicePriceId']) {
                  this.serviceObj[key]['values'].splice(ix, 1);
                  if (this.serviceObj[key]['values'].length === 0) {
                    delete this.serviceObj[key];
                    areAllServicesRemoved = true;
                  }
                  else {
                    areAllServicesRemoved = false;
                  }
                  this.calculateTotalAmountSummary();
                }
              }
            }
          });
          let leadCreationUserDataModel = { ...this.leadCreationUserDataModel };
          leadCreationUserDataModel.servicesCount = areAllServicesRemoved ? 0 : 1;
          this.store.dispatch(new LeadCreationUserDataCreateAction({ leadCreationUserDataModel }));
          this.selectedItemsWithKeyValue = { ...this.serviceObj };
        }
        this.selectedItemsWithKeyValue = JSON.parse(JSON.stringify(this.serviceObj));
      });
  }

  onArrowClick(type: string) {
    if (this.resultAfterBOCIsApplied) {
      this.confirmationForUnsavedBOCServices(type);
    }
    else {
      this.canDeactivate(true);
      if (type == 'previous') {
        this.rxjsService.navigateToLeadViewPage();
      }
      else {
        if (this.leadHeaderData.fromUrl == 'Leads List') {
          this.router.navigate(['/sales/task-management/leads/lead-items/add-edit']);
        }
        else {
          this.router.navigate(['/my-tasks/my-sales/my-leads/lead-items/add-edit']);
        }
      }
    }
  }

  calculateTotalAmountSummary(): void {
    this.valueAddedServicesTotalAmount = { taxPrice: 0, totalPrice: 0, servicePrice: 0 };
    this.otherServicesTotalAmount = { taxPrice: 0, totalPrice: 0, servicePrice: 0 };
    Object.keys(this.serviceObj).forEach((key) => {
      this.serviceObj[key]["totalAmount"] = { taxPrice: 0, totalPrice: 0, servicePrice: 0 };
      this.serviceObj[key]['isQtyAvailable'] = this.serviceObj[key]['values'].find(s => s['isQtyAvailable'] == true) ? true : false;
      if (this.serviceObj[key]['isQtyAvailable']) {
        this.serviceObj[key]['values'].forEach((obj: ServicesModel) => {
          this.valueAddedServicesTotalAmount.servicePrice = this.valueAddedServicesTotalAmount.servicePrice + (obj['servicePrice']);
          this.valueAddedServicesTotalAmount.taxPrice = this.valueAddedServicesTotalAmount.taxPrice + (obj['taxPrice']);
          this.valueAddedServicesTotalAmount.totalPrice = this.valueAddedServicesTotalAmount.totalPrice +
            (obj['servicePrice'] + obj['taxPrice']);
          this.serviceObj[key]["totalAmount"]['servicePrice'] = this.serviceObj[key]["totalAmount"]['servicePrice'] + (obj['servicePrice']);
          this.serviceObj[key]["totalAmount"]['taxPrice'] = this.serviceObj[key]["totalAmount"]['taxPrice'] + (obj['taxPrice']);
          this.serviceObj[key]["totalAmount"]['totalPrice'] = this.serviceObj[key]["totalAmount"]['totalPrice'] + (obj['servicePrice'] + obj['taxPrice']);
        });
      }
      else {
        this.serviceObj[key]['values'].forEach((obj: ServicesModel) => {
          this.otherServicesTotalAmount.servicePrice = this.otherServicesTotalAmount.servicePrice + obj['unitPrice'];
          this.otherServicesTotalAmount.taxPrice = this.otherServicesTotalAmount.taxPrice + obj['unitTaxPrice'];
          this.otherServicesTotalAmount.totalPrice = this.otherServicesTotalAmount.totalPrice + (obj['unitPrice'] + obj['unitTaxPrice']);
          this.serviceObj[key]["totalAmount"]['servicePrice'] = this.serviceObj[key]["totalAmount"]['servicePrice'] + (obj['unitPrice']);
          this.serviceObj[key]["totalAmount"]['taxPrice'] = this.serviceObj[key]["totalAmount"]['taxPrice'] + (obj['unitTaxPrice']);
          this.serviceObj[key]["totalAmount"]['totalPrice'] = this.serviceObj[key]["totalAmount"]['totalPrice'] + (obj['unitPrice'] + obj['unitTaxPrice']);
        });
      }
    });
    this.serviceItemsLength = this.objectKeys(this.serviceObj).length;
    $('#services_modal').modal('hide');
  }

  onQuantityChange(type, serviceSubItem: object): void {
    if (type === 'plus') {
      serviceSubItem['qty'] = serviceSubItem['qty'] + 1;
    }
    else {
      serviceSubItem['qty'] = (serviceSubItem['qty'] === 1) ? serviceSubItem['qty'] : serviceSubItem['qty'] - 1;
    }
    serviceSubItem['servicePrice'] = (serviceSubItem['qty'] * serviceSubItem['unitPrice']);
    serviceSubItem['taxPrice'] = (serviceSubItem['qty'] * serviceSubItem['unitTaxPrice']);
    serviceSubItem['totalPrice'] = (serviceSubItem['servicePrice'] + serviceSubItem['taxPrice']);
    this.calculateTotalAmountSummary();
    this.isFormChangeDetected = (type == 'minus' && serviceSubItem['qty'] === 1) ? false : true;
  }

  exitLead() {
    if (this.resultAfterBOCIsApplied) {
      this.confirmationForUnsavedBOCServices('exit lead');
    }
    else {
      this.canDeactivate(true);
      this.rxjsService.navigateToLeadListPage();
    }
  }

  confirmationForUnsavedBOCServices(type = 'arrow') {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog('Are you sure you want to discard the unsaved changes?').
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) {
          this.canDeactivate(true);
          return;
        };
        this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.LEAD_BALANCE_OF_CONTRACT, undefined,
          prepareRequiredHttpParams({
            ids: this.leadHeaderData.leadId, isDeleted: true,
            modifiedUserId: this.loggedInUserData.userId
          })).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              this.canDeactivate(false);
              if (type == 'previous') {
                this.rxjsService.navigateToLeadViewPage();
              }
              else if (type == 'exit lead') {
                this.rxjsService.navigateToLeadListPage();
              }
              else {
                if (this.leadHeaderData.fromUrl == 'Leads List') {
                  this.router.navigate(['/sales/task-management/leads/lead-items/add-edit']);
                }
                else {
                  this.router.navigate(['/my-tasks/my-sales/my-leads/lead-items/add-edit']);
                }
              }
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
      });
  }

  onSubmit(): void {
    let areServicesDuplicated = false;
    if (this.serviceObj.hasOwnProperty('LSS Scheme Name') && this.objectKeys(this.serviceObj).length == 1 ||
      !this.serviceObj.hasOwnProperty('LSS Scheme Name') && this.objectKeys(this.serviceObj).length == 0) {
      this.snackbarService.openSnackbar("Atleast One Service Subscription is required..!!", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.serviceInfoAddEditForm.invalid) {
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.serviceInfoAddEditForm.value.services = [];
    Object.keys(this.serviceObj).forEach((key) => {
      if (this.serviceObj.hasOwnProperty(key) && key !== 'LSS Scheme Name') {    // change if the key is changed
        this.serviceObj[key]['values'].forEach((service) => {
          this.serviceInfoAddEditForm.value.services.push(service);
          areServicesDuplicated = this.serviceInfoAddEditForm.value.services.filter((s => s['serviceCode'] == service['serviceCode']))?.length > 1 ? true : false;
        });
      }
      for (const [ix, val] of this.serviceObj[key]['values'].entries()) {
        val['partitions'] = [];
        if (val['selectedPartitionIds'] && val['selectedPartitionIds'].length > 0) {
          val['selectedPartitionIds'].forEach((partitionId) => {
            val['partitions'].push({
              leadServicePartitionId: (val['servicePartitions']?.find(sPC => sPC['partitionId'] == partitionId)?.leadServicePartitionId ?
                val['servicePartitions'].find(sPC => sPC['partitionId'] == partitionId).leadServicePartitionId : null),
              partitionId
            });
          });
        }
        delete val['servicePartitions'];
        delete val['selectedPartitionIds'];
      }
    });
    this.serviceInfoAddEditForm.value.leadId = this.leadHeaderData.leadId;
    this.serviceInfoAddEditForm.value.customerId = this.leadHeaderData.customerId;
    this.serviceInfoAddEditForm.value.createdUserId = this.loggedInUserData.userId;
    if (this.secondaryPartition) {
      this.secondaryPartition.partitionLeadId = this.leadHeaderData.leadId;
      this.serviceInfoAddEditForm.value.secondaryPartition = this.secondaryPartition;
      this.secondaryPartition.partitionTypeId = this.serviceInfoAddEditForm.value.partitionTypeId;
    }
    if (this.secondaryPartition) {
      this.secondaryPartition.partitionLeadId = this.leadHeaderData.leadId;
      this.serviceInfoAddEditForm.value.secondaryPartition = this.secondaryPartition;
      this.secondaryPartition.partitionTypeId = this.serviceInfoAddEditForm.value.partitionTypeId;
    }
    if (areServicesDuplicated) {
      this.snackbarService.openSnackbar("Duplicate services exist, Please remove..!!", ResponseMessageTypes.WARNING);
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LEAD_SERVICES_BY_ID, this.serviceInfoAddEditForm.value);
    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.isFormChangeDetected = false;
        let leadCreationUserDataModel = { ...this.leadCreationUserDataModel };
        leadCreationUserDataModel.servicesCount = response.message.toLowerCase().includes("created") ? 1 : this.serviceInfoAddEditForm.value.services.length;
        this.store.dispatch(new LeadCreationUserDataCreateAction({ leadCreationUserDataModel }));
        this.getServicesByLeadId();
        this.nextOrPrevStepper(btnActionTypes.SUBMIT);
      }
    });
  }

  nextOrPrevStepper(type?: btnActionTypes | string): void {
    if (type == btnActionTypes.EXIT_LEAD) {
    }
  }

  ngOnDestroy() {
    this.rxjsService.setFormChangesDetectionPropertyForPageReload(this.isFormChangeDetected);
  }
}