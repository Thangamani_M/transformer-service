import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import {
  CrudService, disableFormControls, enableFormControls, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix, removeFormControlError, RxjsService, setRequiredValidator
} from '@app/shared';
import { loggedInUserData, selectStaticEagerLoadingBOCOptionsState$ } from '@modules/others';
import { BOCLinkToCustomerModel, SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-balance-of-contract-in-lead-stage-two-popup',
  templateUrl: './balance-of-contract-in-lead-stage-two-popup.component.html'
})

export class BalanceOfContractLeadStageTwoPopupComponent implements OnInit {
  BOCCreationForm: FormGroup;
  @Output() outputData = new EventEmitter<any>();
  loggedInUserData: LoggedInUserModel;
  selectedContractDetail;
  bocOptions = [];
  contracts = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data,
    private dialog: MatDialog, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private store: Store<AppState>,
    private crudService: CrudService) {
    this.rxjsService.setDialogOpenProperty(true);
    this.combineLatestNgrxStoreDataOne();
  }

  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingBOCOptionsState$)
    ]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.bocOptions = response[1];
    });
  }

  ngOnInit() {
    this.createBOCCreationForm();
    this.getBOCDetails();
    this.onFormControlChanges();
  }

  onFormControlChanges() {
    this.BOCCreationForm.get('balanceOfContractTransferOptionId').valueChanges.subscribe((balanceOfContractTransferOptionId: string) => {
      if (!balanceOfContractTransferOptionId) return;
      // if BOC option is BOC Initiated
      if (balanceOfContractTransferOptionId == '1') {
        this.BOCCreationForm = removeFormControlError(this.BOCCreationForm, 'required', "contractId");
        this.BOCCreationForm = disableFormControls(this.BOCCreationForm, ["contractId"]);
        this.BOCCreationForm.get('contractId').setValue("");
        this.selectedContractDetail = null;
      }
      // if BOC option is Existing Contract
      else {
        this.BOCCreationForm = enableFormControls(this.BOCCreationForm, ["contractId"]);
        this.BOCCreationForm = setRequiredValidator(this.BOCCreationForm, ["contractId"]);
      }
    });
    this.BOCCreationForm.get('contractId').valueChanges.subscribe((contractId: string) => {
      if (contractId) {
        this.selectedContractDetail = null;
        this.getContractDetailByContractId();
      };
    });
  }

  getBOCDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.BOC_DETAIL, this.data?.leadId).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.rxjsService.setPopupLoaderProperty(true);
        this.BOCCreationForm.patchValue(response.resources?.customerAddress);
        this.contracts = response.resources?.contracts;
      }
    });
  }

  createBOCCreationForm() {
    let bOCLinkToCustomerModel = new BOCLinkToCustomerModel();
    this.BOCCreationForm = this.formBuilder.group({});
    Object.keys(bOCLinkToCustomerModel).forEach((key) => {
      this.BOCCreationForm.addControl(key, new FormControl(bOCLinkToCustomerModel[key]));
    });
    this.BOCCreationForm = setRequiredValidator(this.BOCCreationForm, ["balanceOfContractTransferOptionId"]);
  }

  getContractDetailByContractId() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.LEAD_BOC_CONTRACT, this.BOCCreationForm.get('contractId').value)
      .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
          this.selectedContractDetail = resp.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  onSubmit() {
    if (this.BOCCreationForm.invalid) {
      return;
    }
    if (this.BOCCreationForm.value.balanceOfContractTransferOptionId == 2) {
      this.BOCCreationForm.value.contractRefNo = this.contracts?.find(c => c.contractId == this.BOCCreationForm.get('contractId').value)?.contractRefNo;
    }
    else {
      delete this.BOCCreationForm.value.contractRefNo;
    }
    this.outputData.emit(this.BOCCreationForm.value);
    this.dialog.closeAll();
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
