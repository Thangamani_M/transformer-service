import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, CustomDirectiveConfig, formConfigs, IApplicationResponse,
  ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, SnackbarService
} from '@app/shared';
import { selectDynamicEagerLoadingServiceCategoriesState$ } from '@modules/others';
import { SalesModuleApiSuffixModels, ServicesModel } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs/internal/observable/combineLatest';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-service-popup',
  templateUrl: './service-popup.component.html'
})
export class ServicePopupComponent extends PrimeNgTableVariablesModel implements OnInit {
  serviceCategoryId = null;
  serviceCategories = [];
  VAServices = 'Value Added Services';
  VAS = 'VAS';
  serviceCategoryName = '';
  tempSelectedRows = [];
  serviceSubscriptionForm: FormGroup;
  serviceSubscriptions = [];
  tempServiceObj = {};
  serviceSubscriptionObj;
  objectKeys = Object.keys;
  isLssInfoContainerShown = false;
  lssContributionDetails;
  caption = 'Services';
  contributionAmount = "";
  formConfigs = formConfigs;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  primengTableConfigProperties: any = {
    tableCaption: "Services",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [],
    tableComponentConfigs: {
      tabsList: [
        {
          dataKey: 'servicePriceId',
          resizableColumns: false,
          columns: [{ field: 'serviceName', header: 'Service Description' },
          { field: 'serviceCode', header: 'Service Code' },
          { field: 'servicePrice', header: 'Sub Total' }, { field: 'taxPrice', header: 'VAT' },
          { field: 'totalPrice', header: 'Total' }
          ]
        }]
    }
  }
  @Output() outputData = new EventEmitter<any>();
  pageSize: number = 10;

  constructor(@Inject(MAT_DIALOG_DATA) public serviceSubscription, public snackbarService: SnackbarService,
    private rxjsService: RxjsService, public dialogRef: MatDialogRef<ServicePopupComponent>,
    private crudService: CrudService, private store: Store<AppState>) {
    super();
    this.serviceSubscriptionObj = JSON.parse(JSON.stringify(this.serviceSubscription));
  }

  getServiceCategoryName(): string {
    return this.serviceCategories.find(s => s['id'] == this.serviceCategoryId).displayName;
  }

  ngOnInit() {
    this.combineLatestNgrxDynamicData();
    this.rxjsService.setDialogOpenProperty(true);
    Object.keys(this.serviceSubscriptionObj.serviceObj).forEach((serviceCategoryKey: string) => {
      this.serviceSubscriptionObj.serviceObj[serviceCategoryKey]['values'].forEach((serviceObj) => {
        serviceObj.isChecked = true;
      });
    });
    this.tempServiceObj = JSON.parse(JSON.stringify(this.serviceSubscriptionObj.serviceObj));
  }

  combineLatestNgrxDynamicData() {
    combineLatest([
      this.store.select(selectDynamicEagerLoadingServiceCategoriesState$)])
      .subscribe((response) => {
        this.serviceCategories = response[0];
      });
  }

  onResponse(response: IApplicationResponse) {
    this.serviceCategoryName = this.getServiceCategoryName();
    this.dataList = response.resources;
    this.totalRecords = response.totalCount;
    this.selectedRows = [];
    this.selectedRows = [...this.tempSelectedRows, ...this.selectedRows];
    response.resources.forEach((responseObj: ServicesModel) => {
      if (responseObj.isQtyAvailable && responseObj.isQtyAvailable == true) {
        responseObj.qty = responseObj.qty ? responseObj.qty : 1;
        responseObj.servicePrice = +((responseObj.qty * responseObj.servicePrice).toFixed(2));
        responseObj.taxPrice = +((responseObj.qty * responseObj.taxPrice).toFixed(2));
        responseObj.totalPrice = +((responseObj.servicePrice + responseObj.taxPrice).toFixed(2));
      }
      else {
        responseObj.totalPrice = +((responseObj.servicePrice + responseObj.taxPrice).toFixed(2));
      }
    });
    if (this.serviceSubscriptionObj.serviceObj.hasOwnProperty(this.serviceCategoryName)) {
      this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'].forEach((localObj: ServicesModel) => {
        response.resources.forEach((serverResponseObj: ServicesModel) => {
          if (serverResponseObj.servicePriceId === localObj.servicePriceId) {
            if (serverResponseObj.isChecked) {
              localObj.isChecked = serverResponseObj.isChecked;
              this.selectedRows.push(serverResponseObj);
            }
            else if (localObj.isChecked) {
              serverResponseObj.isChecked = localObj.isChecked;
              this.selectedRows.push(localObj);
            }
            if (serverResponseObj.isQtyAvailable && localObj.isQtyAvailable) {
              serverResponseObj['qty'] = localObj['qty'];
              serverResponseObj['servicePrice'] = localObj['servicePrice'];
              serverResponseObj['taxPrice'] = localObj['taxPrice'];
              serverResponseObj['totalPrice'] = localObj['totalPrice'];
            }
          }
        });
      });
    }
    if (this.dataList.find(d => d['isQtyAvailable'] == true)) {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [{ field: 'serviceName', header: 'Service Description' },
      { field: 'serviceCode', header: 'Service Code' }, { field: 'qty', header: 'Qty' },
      { field: 'servicePrice', header: 'Sub Total' }, { field: 'taxPrice', header: 'VAT' },
      { field: 'totalPrice', header: 'Total' }]
    }
    else {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = [{ field: 'serviceName', header: 'Service Description' },
      { field: 'serviceCode', header: 'Service Code' },
      { field: 'servicePrice', header: 'Sub Total' }, { field: 'taxPrice', header: 'VAT' },
      { field: 'totalPrice', header: 'Total' }]
    }
    this.prepareKeyValueData('response');
    setTimeout(() => {
      this.rxjsService.setPopupLoaderProperty(false);
    }, 200);
  }

  onClick(type: string, rowData) {
    if (type === 'minus') {
      rowData.qty = rowData.qty > 1 ?
        rowData.qty - 1 : 1;
    }
    else {
      rowData.qty = rowData.qty + 1;
    }
    let serviceCategoryKey = (this.serviceCategoryName === this.VAServices) ? this.VAServices :
      (this.serviceCategoryName === this.VAS) ? this.VAS : this.serviceCategoryName;
    if (!this.serviceSubscriptionObj.serviceObj[serviceCategoryKey]) {
      this.serviceSubscriptionObj.serviceObj[serviceCategoryKey] = {};
      this.serviceSubscriptionObj.serviceObj[serviceCategoryKey]['values'] = [];
    }
    rowData['servicePrice'] = +((rowData['qty'] * rowData['unitPrice']).toFixed(2));
    rowData['taxPrice'] = +((rowData['qty'] * rowData['unitTaxPrice']).toFixed(2));
    rowData['totalPrice'] = +((rowData['servicePrice'] + rowData['taxPrice']).toFixed(2));
    this.serviceSubscriptionObj.serviceObj[serviceCategoryKey]['values'].forEach((localObj, ix: number) => {
      if (localObj['servicePriceId'] === rowData['servicePriceId']) {
        this.serviceSubscriptionObj.serviceObj[serviceCategoryKey]['values'][ix] = rowData;
      }
    });
    let quantityChangedServiceObj = this.selectedRows.find(s => s['servicePriceId'] == rowData['servicePriceId']);
    if (quantityChangedServiceObj) {
      quantityChangedServiceObj.qty = rowData.qty;
      quantityChangedServiceObj.servicePrice = rowData['servicePrice'];
      quantityChangedServiceObj.taxPrice = rowData['taxPrice'];
      quantityChangedServiceObj.totalPrice = rowData['totalPrice'];
    }
    this.prepareKeyValueData('quantity');
  }

  onClickLssInfo() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.LSS_LEAD_SERVICES, undefined, false,
      prepareRequiredHttpParams({
        leadId: this.serviceSubscriptionObj.leadId,
      }), 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.lssContributionDetails = response.resources;
          this.caption = this.lssContributionDetails.isFixedContribution ? 'LSS Fixed Contribution' : 'LSS Voluntary Contribution';
          this.contributionAmount = this.lssContributionDetails.contributionAmount;
          this.isLssInfoContainerShown = true;
        }
      });
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.onAddServices(row["pageIndex"], row["pageSize"]);
    this.tempSelectedRows = this.retrieveUniqueObjects([...this.selectedRows, ...this.tempSelectedRows]);
  }

  onCRUDRequest(type) {
    this.serviceCategoryName = this.getServiceCategoryName();
    switch (type) {
      case CrudType.CHECK:
        this.prepareKeyValueData('check');
        break;
    }
  }

  prepareKeyValueData(type: string) {
    this.selectedRows.forEach((selectedRow) => {
      selectedRow.isChecked = true;
    });
    if (!this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]) {
      this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName] = {};
    }
    this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'] = this.retrieveUniqueObjects([...this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'] ?
      this.retrieveUniqueObjects(this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values']) : [],
    ...this.retrieveUniqueObjects([...this.tempSelectedRows, ...this.selectedRows])]).filter(v => v['isChecked']);
    this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'] = this.retrieveUniqueObjects(this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'] ?
      this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'] : []).filter(v => v['isChecked']);
    if (this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'].length === 0) {
      delete this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName];
    }
    this.outputData.emit(this.serviceSubscriptionObj);
  }

  retrieveUniqueObjects = (list: Array<object>): Array<object> => {
    return [...new Set(list.map(obj => obj['servicePriceId']))].map(servicePriceId => {
      return list.find(obj => obj['servicePriceId'] === servicePriceId)
    });
  }

  onChange(rowData?) {
    if (rowData) {
      rowData.isChecked = !rowData.isChecked;
      if (Object.keys(this.serviceSubscriptionObj.serviceObj).length > 0) {
        if (this.serviceSubscriptionObj.serviceObj.hasOwnProperty(this.serviceCategoryName)) {
          let serviceArray = this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'];
          var foundIndex = serviceArray.findIndex(v => v.servicePriceId === rowData.servicePriceId);
          serviceArray[foundIndex] = rowData;
        }
      }
    }
    else {
      if (Object.keys(this.serviceSubscriptionObj.serviceObj).length > 0) {
        if (this.serviceSubscriptionObj.serviceObj.hasOwnProperty(this.serviceCategoryName)) {
          this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'].forEach((localObj, ix: number) => {
            localObj['isChecked'] = !this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'][ix]['isChecked'];
          });
        }
      }
    }
    this.prepareKeyValueData('checkbox');
  }

  onAddServices(pageIndex?: string, pageSize?: string) {
    this.serviceCategoryName = this.getServiceCategoryName();
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEAD_SERVICES, undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, {
        serviceCategoryId: this.serviceCategoryId,
        suburbId: this.serviceSubscriptionObj.suburbId,
        districtId: this.serviceSubscriptionObj.districtId,
        districtName: this.serviceSubscriptionObj.districtName,
        siteTypeId: this.serviceSubscriptionObj.siteTypeId,
        leadId: this.serviceSubscriptionObj.leadId,
        customerId: this.serviceSubscriptionObj.customerId
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.onResponse(response);
        }
        else {
          this.selectedRows = [];
          this.dataList = [];
          this.totalRecords = 0;
        }
      });
  }

  onSubmit(type: string) {
    if (type == 'services') {
      if (!this.serviceCategoryId) {
        return;
      }
      this.prepareKeyValueData('submit');
      this.dialogRef.close({ fromComponent: 'services' });
    }
    else {
      let payload = {
        leadLSSContributionId: this.lssContributionDetails.leadLSSContributionId ? this.lssContributionDetails.leadLSSContributionId : null,
        leadId: this.serviceSubscriptionObj.leadId,
        lssId: this.lssContributionDetails.lssId,
        contributionAmount: this.contributionAmount,
        createdUserId: this.serviceSubscription.createdUserId
      }
      this.crudService.create(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.LEAD_SERVICES_LSS, payload).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.dialogRef.close({ fromComponent: 'lssContribution' });
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  onClose(type?: string) {
    if (type == 'services') {
      let isTempServiceObjHasOwnProp = this.tempServiceObj.hasOwnProperty(this.serviceCategoryName);
      let isServiceObjHasOwnProp = this.serviceSubscriptionObj.serviceObj.hasOwnProperty(this.serviceCategoryName);
      if (isTempServiceObjHasOwnProp && isServiceObjHasOwnProp && (this.dataList.length !== this.selectedRows.length)) {
        let serviceObjValues = this.retrieveUniqueObjects([
          ...this.tempServiceObj[this.serviceCategoryName]['values'], ...this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values']]);
        serviceObjValues.forEach((localObj, localObjIndex: number) => {
          this.tempServiceObj[this.serviceCategoryName]['values'].forEach((tempLocalObj) => {
            if (tempLocalObj.servicePriceId !== localObj['servicePriceId'] && !tempLocalObj.isApproved) {
              serviceObjValues.splice(localObjIndex, 1);
            }
          });
        });
        this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'] = serviceObjValues;
      }
      else if ((isTempServiceObjHasOwnProp && !isServiceObjHasOwnProp) ||
        (isTempServiceObjHasOwnProp && isServiceObjHasOwnProp && (this.dataList.length === this.selectedRows.length))) {
        this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName] = { values: [] };
        this.tempServiceObj[this.serviceCategoryName]['values'].forEach((tempLocalObj) => {
          this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'].push(tempLocalObj);
        });
      }
      else if (!isTempServiceObjHasOwnProp && isServiceObjHasOwnProp) {
        delete this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName];
      }
    }
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
