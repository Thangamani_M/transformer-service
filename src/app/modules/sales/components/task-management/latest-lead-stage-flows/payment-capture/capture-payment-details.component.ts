import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { adjustDateFormatAsPerPCalendar, BreadCrumbModel, CrudService, debounceTimeForDeepNestedObjectSearchkeyword, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, removeFormControlError, RxjsService, SERVER_REQUEST_DATE_TIME_TRANSFORM, SERVER_REQUEST_DATE_TRANSFORM, setRequiredValidator, SnackbarService } from '@app/shared';
import { ModuleName, ResponseMessageTypes } from '@app/shared/enums';
import { loggedInUserData, selectStaticEagerLoadingPaymentMethodsState$, selectStaticEagerLoadingPaymentOptionsState$ } from '@modules/others';
import { LeadCreationUserDataModel, selectLeadCreationStepperParamsState$, selectLeadCreationUserDataState$, selectLeadHeaderDataState$ } from '@modules/sales';
import { InstallationFeeModel, InstallationPriceModel, LeadHeaderData, PaymentModel, ServiceAgreementStepsParams, ServiceFeeModel } from '@modules/sales/models';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';

enum PaymentMethods {
  ON_INVOICE = 1,
  DEBIT_ORDER = 2
}
@Component({
  selector: 'capture-payment',
  templateUrl: './capture-payment-details.component.html',
  styleUrls: ['./capture-payment-details.component.scss']
})

export class CapturePaymentComponent {
  leadId = "";
  leadHeaderData: LeadHeaderData;
  yearArray = [];
  showModalFirst = false;
  showModalSecond = false;
  showModalThird = false;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  loggedInUserData: LoggedInUserModel;
  serviceFeeForm: FormGroup;
  installationFeeForm: FormGroup;
  selected = new FormControl(0);
  detailsInfo;
  processSaleId = "";
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  days = [];
  paymentDetails: FormArray;
  installationPaymentOptions: FormArray;
  paymentModel: PaymentModel;
  installationsTypes = [];
  data;
  paymentMethods = [];
  showSummary: boolean = false;
  summary;
  serviceFeeSaved: boolean = false;
  installationFeeSaved: boolean = false;
  breadCrumb: BreadCrumbModel;
  todayDate = new Date();
  servicefeeDetails;
  isInstallationFeeFormSubmitted = false;
  paymentDate$ = new Subject();
  selectedIndex = 0;
  installationDetail;
  serviceDetail;
  isItemAndOnInvoiceOnly = false;
  dateFormat = adjustDateFormatAsPerPCalendar();

  constructor(private crudService: CrudService,
    private datePipe: DatePipe,
    private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private rxjsService: RxjsService,
    private router: Router, private store: Store<AppState>) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationUserDataState$),
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(loggedInUserData),
      this.store.select(selectLeadHeaderDataState$),
      this.store.select(selectStaticEagerLoadingPaymentMethodsState$),
      this.store.select(selectStaticEagerLoadingPaymentOptionsState$)]
    ).pipe(take(1)).subscribe((response) => {
      this.leadCreationUserDataModel = new LeadCreationUserDataModel(
        response[0]
      );
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[1]);
      this.loggedInUserData = new LoggedInUserModel(response[2]);
      this.leadHeaderData = response[3];
      this.paymentMethods = response[4];
      this.installationsTypes = response[5];
      this.breadCrumb = {
        pageTitle: { key: 'Lead No', value: this.leadHeaderData.leadRefNo },
        items: [{
          key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
        }, { key: 'Process Payment' }]
      };
    });
  }

  ngOnInit() {
    this.yearArray = this.generateArrayOfYears();
    this.days = this.generateArrayOfDays();
    this.getDetails();
    this.createServicFeeForm();
    this.createInstallationFeeForm();
    this.paymentDate$.pipe(
      debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
      distinctUntilChanged(),
      switchMap(searchText => {
        return of(searchText);
      })).subscribe((selectedPaymentDate: string) => {
        // payment method is debit order then we need validate the selected date and time
        if (this.installationPaymentOptions.controls[this.selectedIndex].get('paymentMethodId').value == PaymentMethods.DEBIT_ORDER) {
          this.validateSelectedDate(selectedPaymentDate);
        }
      });
  }

  validateSelectedDate(selectedPaymentDate) {
    let paymentDate = this.datePipe.transform(selectedPaymentDate, SERVER_REQUEST_DATE_TRANSFORM);
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.PAYMENT_DATE_VALIDATE, undefined, false, prepareGetRequestHttpParams(null, null, {
      leadId: this.leadHeaderData.leadId,
      paymentDate
    })).subscribe((resp) => {
      this.installationPaymentOptions.controls[this.selectedIndex].get('isSelectedDateValid').setValue(resp.isSuccess);
      this.installationPaymentOptions.controls[this.selectedIndex].get('invalidMessage').setValue(resp.message);
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  getSummary() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUMMARY, undefined, false, prepareGetRequestHttpParams(null, null, {
      processSaleId: this.processSaleId
    })).subscribe((resp) => {
      if (resp.isSuccess && resp.statusCode == 200 && resp.resources) {
        this.summary = resp.resources;
        this.summary.installationFees?.forEach(element => {
          element.paymentDate = this.datePipe.transform(element.paymentDate, 'dd-MM-yyyy');
        });
      }
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  onTabChanged(e) {
    this.rxjsService.setPopupLoaderProperty(true);
    if (e == 0) {
      if (this.paymentDetails) {
        this.paymentDetails.clear();
      }
      if (this.processSaleId) {
        this.getServiceFeeDetails(this.processSaleId);
      }
    } else {
      this.installationFeeForm.get('paymentOptionId').setValue("");
      if (this.installationPaymentOptions) {
        this.installationPaymentOptions.clear();
      }
      if (this.processSaleId) {
        this.getInstallationFeeDetailsByprocessSaleId(this.processSaleId, 'tab changed');
      }
    }
    setTimeout(() => {
      this.rxjsService.setPopupLoaderProperty(false);
    }, 500);
  }

  changeServicePurchaseOrder() {
    this.serviceFeeForm.get('isPurchaseOrderExist').valueChanges.subscribe(isPurchaseOrderExist => {
      if (isPurchaseOrderExist) {
        this.serviceFeeForm.get('purchaseOrderNo').enable();
        this.serviceFeeForm.get('purchaseOrderDate').enable();
        this.serviceFeeForm.get('purchaseOrderNo').setValidators([Validators.required]);
        this.serviceFeeForm.get('purchaseOrderDate').setValidators([Validators.required]);
        this.serviceFeeForm.get('purchaseOrderNo').updateValueAndValidity();
        this.serviceFeeForm.get('purchaseOrderDate').updateValueAndValidity()
      } else {
        this.serviceFeeForm.get('purchaseOrderNo').disable();
        this.serviceFeeForm.get('purchaseOrderDate').disable();
        this.serviceFeeForm.get('purchaseOrderNo').setValue('');
        this.serviceFeeForm.get('purchaseOrderDate').setValue('');
        this.serviceFeeForm.get('purchaseOrderDate').clearValidators();
        this.serviceFeeForm.get('purchaseOrderDate').updateValueAndValidity();
        this.serviceFeeForm.get('purchaseOrderNo').clearValidators();
        this.serviceFeeForm.get('purchaseOrderNo').updateValueAndValidity();
        this.serviceFeeForm = removeFormControlError(this.serviceFeeForm as FormGroup, 'invalid');
      }
    });
  }

  changePurchaseOrder() {
    if (this.installationFeeForm.value.isPurchaseOrderExist) {
      this.installationFeeForm.get('purchaseOrderNo').enable();
      this.installationFeeForm.get('purchaseOrderDate').enable();
      this.installationFeeForm.get('purchaseOrderNo').setValidators([Validators.required]);
      this.installationFeeForm.get('purchaseOrderDate').setValidators([Validators.required]);
      this.installationFeeForm.get('purchaseOrderNo').updateValueAndValidity();
      this.installationFeeForm.get('purchaseOrderDate').updateValueAndValidity();
    } else {
      this.installationFeeForm.get('purchaseOrderNo').disable();
      this.installationFeeForm.get('purchaseOrderNo').setValue('');
      this.installationFeeForm.get('purchaseOrderDate').setValue('');
      this.installationFeeForm.get('purchaseOrderDate').disable();
      this.installationFeeForm.get('purchaseOrderNo').clearValidators();
      this.installationFeeForm.get('purchaseOrderNo').updateValueAndValidity();
      this.installationFeeForm.get('purchaseOrderDate').clearValidators();
      this.installationFeeForm.get('purchaseOrderDate').updateValueAndValidity();
      this.installationFeeForm = removeFormControlError(this.installationFeeForm as FormGroup, 'invalid');
    }
  }

  get getPaymentDetils(): FormArray {
    if (!this.serviceFeeForm) return;
    return this.serviceFeeForm.get("paymentDetails") as FormArray;
  }

  get getInstllationOptionDetils(): FormArray {
    if (!this.installationFeeForm) return;
    return this.installationFeeForm.get("installationPaymentOptions") as FormArray;
  }

  createPaymentDetails(paymentModel?: PaymentModel): FormGroup {
    this.paymentModel = new PaymentModel(paymentModel);
    let formControls = {};
    Object.keys(this.paymentModel).forEach((key) => {
      formControls[key] = [this.paymentModel[key], []]
    });
    return this.formBuilder.group(formControls);
  }

  createServicFeeForm() {
    let serviceFeeModel = new ServiceFeeModel();
    this.serviceFeeForm = this.formBuilder.group({
      paymentDetails: this.formBuilder.array([]),
    });
    Object.keys(serviceFeeModel).forEach((key) => {
      this.serviceFeeForm.addControl(
        key,
        new FormControl(serviceFeeModel[key])
      );
    });
    this.serviceFeeForm = setRequiredValidator(this.serviceFeeForm, [
      "purchaseOrderNo", "purchaseOrderDate", "paymentDay"
    ]);
  }

  createInstallationPaymentOptions(paymentModel?: InstallationPriceModel): FormGroup {
    let installationPriceModel = new InstallationPriceModel(paymentModel);
    let formControls = {};
    Object.keys(installationPriceModel).forEach((key) => {
      formControls[key] = [installationPriceModel[key], []];
      if (key == 'paymentMethodId') {
        formControls[key] = [{ value: installationPriceModel[key], disabled: this.isItemAndOnInvoiceOnly ? true : false }, [Validators.required]];
      }
      else if (key == 'paymentDate') {
        formControls[key] = [{ value: installationPriceModel[key], disabled: false }, [Validators.required]];
      }
      else if (key == 'shouldDisable' && this.isItemAndOnInvoiceOnly) {
        formControls[key] = [{ value: false, disabled: false }, [Validators.required]];
      }
    });
    return this.formBuilder.group(formControls);
  }

  createInstallationFeeForm() {
    let installationFeeModel = new InstallationFeeModel();
    this.installationFeeForm = this.formBuilder.group({
      installationPaymentOptions: this.formBuilder.array([])
    });
    Object.keys(installationFeeModel).forEach((key) => {
      this.installationFeeForm.addControl(key, new FormControl(installationFeeModel[key]));
    });
    this.installationFeeForm = setRequiredValidator(this.installationFeeForm, [
      "purchaseOrderNo", "purchaseOrderDate", "paymentOptionId",
    ]);
  }

  onPaymentMethodChanged(index: number) {
    this.installationPaymentOptions.controls[index].get('shouldDisable').setValue(false);
    if (this.installationPaymentOptions.controls[this.selectedIndex].get('paymentMethodId').value == PaymentMethods.DEBIT_ORDER &&
      this.installationPaymentOptions.controls[index].get('paymentDate').value) {
      this.validateSelectedDate(this.installationPaymentOptions.controls[index].get('paymentDate').value);
    }
  }

  onChangeInitialDebit(obj, i) {
    if (obj.value.isInitialDeposit) {
      this.getPaymentDetils.controls[i].get("isFirstBillDate").setValue(false);
      this.getPaymentDetils.controls[i].get("isSpecialDebitOrderAfterFirstSignal").setValue(false);
    }
  }

  onChangeFirstDo(obj, i) {
    if (obj.value.isFirstBillDate) {
      this.getPaymentDetils.controls[i].get("isInitialDeposit").setValue(false);
      this.getPaymentDetils.controls[i].get("isSpecialDebitOrderAfterFirstSignal").setValue(false);
    }
  }

  onChangeSpecialDo(obj, i) {
    if (obj.value.isSpecialDebitOrderAfterFirstSignal) {
      this.getPaymentDetils.controls[i].get("isInitialDeposit").setValue(false);
      this.getPaymentDetils.controls[i].get("isFirstBillDate").setValue(false);
    }
  }

  hide() {
    this.showModalFirst = false;
    this.showModalSecond = true;
  }

  navigateToList(): void {
    this.router.navigate(['/sales/task-management/leads']);
  }

  openModal() {
    this.showSummary = false;
    this.selected.setValue(0);
    let payload = {
      createdUserId: this.loggedInUserData.userId,
      quotationVersionId: this.serviceAgreementStepsParams.selectedQuotationVersionIds.split(",")
    }
    if (!this.processSaleId) {
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_PROCESS_SALES, payload).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200 && response.isSuccess) {
          this.processSaleId = response.resources;
          this.rxjsService.setDialogOpenProperty(true);
          this.showModalFirst = true;
          if (this.detailsInfo.processSalesServiceFees && this.detailsInfo.processSalesServiceFees.length > 0) {
            this.getServiceFeeDetails(this.processSaleId);
          }
          if (this.detailsInfo.processSalesInstallationFees && this.detailsInfo.processSalesInstallationFees.length > 0) {
            this.getInstallationFeeDetailsByprocessSaleId(this.processSaleId);
          }
          this.rxjsService.setPopupLoaderProperty(false);
        }
      });
    } else {
      this.showModalFirst = true;
      if (this.detailsInfo.processSalesServiceFees && this.detailsInfo.processSalesServiceFees.length > 0) {
        this.getServiceFeeDetails(this.processSaleId)
      }
      if (this.detailsInfo.processSalesInstallationFees && this.detailsInfo.processSalesInstallationFees.length > 0) {
        this.getInstallationFeeDetailsByprocessSaleId(this.processSaleId)
      }
    }
  }

  closePaymentPopup() {
    this.showModalFirst = false;
    this.showSummary = false;
    this.installationFeeSaved = false;
    this.serviceFeeSaved = false;
    if (this.paymentDetails) {
      this.paymentDetails.clear();
    }
    if (this.installationPaymentOptions) {
      this.installationPaymentOptions.clear();
    }
    this.serviceFeeForm.reset();
    this.installationFeeForm.reset();
  }

  processClick() {
    this.showModalFirst = false;
    this.showModalSecond = false;
    this.showModalThird = true;
  }

  generateArrayOfYears() {
    var min = new Date().getFullYear();
    var max = min + 50;
    var years = [];
    for (var i = min; i <= max; i++) {
      years.push(i);
    }
    return years;
  }

  generateArrayOfDays() {
    var min = 0;
    var max = 28;
    var days = [];
    for (var i = min; i <= max; i++) {
      days.push(i);
    }
    return days;
  }

  onSubmitServiceFeeFormAndMoveToInstallationFeeTab() {
    let payload = this.serviceFeeForm.getRawValue();
    if (!payload.isPurchaseOrderExist) {
      this.serviceFeeForm.get('purchaseOrderDate').clearValidators();
      this.serviceFeeForm.get('purchaseOrderDate').updateValueAndValidity();
      this.serviceFeeForm.get('purchaseOrderNo').clearValidators();
      this.serviceFeeForm.get('purchaseOrderNo').updateValueAndValidity();
    }
    if (this.serviceFeeForm.invalid) {
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    payload.createdUserId = this.loggedInUserData.userId;
    if (this.serviceFeeForm.value.paymentMethodId == PaymentMethods.ON_INVOICE) {
      this.serviceFeeForm.get('isProFormaInvoiceRequired').setValue(true);
      payload.paymentDay = null;
    }
    if (!payload.purchaseOrderNo) {
      payload.purchaseOrderNo = null;
      payload.purchaseOrderDate = null;
    }
    payload.purchaseOrderDate = this.datePipe.transform(payload.purchaseOrderDate, SERVER_REQUEST_DATE_TRANSFORM);
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_GET_SERVICE_FEE, payload).subscribe((response) => {
      this.selected.setValue(0);
      if (response.isSuccess && response.resources && response.statusCode == 200) {
        if (this.installationPaymentOptions?.length > 0) {
          this.installationPaymentOptions.controls.forEach((installationPaymentOptionAbstractControl) => {
            installationPaymentOptionAbstractControl.get('shouldDisable').setValue(false);
            installationPaymentOptionAbstractControl.get('isSelectedDateValid').setValue(true);
            installationPaymentOptionAbstractControl.get('invalidMessage').setValue("");
          });
        }
        this.serviceFeeForm.get("serviceFeeId").setValue(response.resources);
        this.serviceFeeSaved = true;
        if (this.detailsInfo.processSalesInstallationFees && this.detailsInfo.processSalesInstallationFees.length > 0) {
          this.selected.setValue(1);
        } else {
          this.showSummary = true;
          this.getSummary();
        }
      }
    });
  }

  onSubmitInstallationFeeForm() {
    this.isInstallationFeeFormSubmitted = true;
    let payload = this.installationFeeForm.getRawValue();
    if (!payload.isPurchaseOrderExist) {
      this.installationFeeForm.get('purchaseOrderDate').clearValidators();
      this.installationFeeForm.get('purchaseOrderDate').updateValueAndValidity();
      this.installationFeeForm.get('purchaseOrderNo').clearValidators();
      this.installationFeeForm.get('purchaseOrderNo').updateValueAndValidity();
    }
    else {
      payload.purchaseOrderDate = this.datePipe.transform(payload.purchaseOrderDate, SERVER_REQUEST_DATE_TRANSFORM);
    }
    if (this.installationFeeForm.invalid) {
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    payload.createdUserId = this.loggedInUserData.userId;
    payload.processSaleId = this.processSaleId;
    if (payload.installationPaymentOptions) {
      payload.installationPaymentOptions.forEach(element => {
        element.paymentDate = this.datePipe.transform(element.paymentDate, SERVER_REQUEST_DATE_TIME_TRANSFORM);
        element.createdUserId = this.loggedInUserData.userId;
        element.paymentOptionId = payload.paymentOptionId;
      });
    }
    let selectedDateValidCount = 0;
    this.installationPaymentOptions.controls.filter(iPO => iPO.value.paymentMethodId == PaymentMethods.DEBIT_ORDER).forEach((abstractControl: AbstractControl) => {
      selectedDateValidCount = (abstractControl.get('isSelectedDateValid').value == true) ? selectedDateValidCount + 1 : (selectedDateValidCount == 0 ? 0 : selectedDateValidCount - 1);
    });
    if (selectedDateValidCount !== this.installationPaymentOptions.controls.filter(iPO => iPO.value.paymentMethodId == PaymentMethods.DEBIT_ORDER).length) {
      if (this.installationPaymentOptions.controls.find(iPO => iPO.value.invalidMessage?.includes("weekend"))) {
        this.snackbarService.openSnackbar("Selected date is a weekend.Kindly choose another date", ResponseMessageTypes.ERROR);
      }
      else {
        this.snackbarService.openSnackbar("Amount cannot be debited on this date .Kindly choose another date", ResponseMessageTypes.ERROR);
      }
      return;
    }
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_GET_INSTLLATION_FEE, payload).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.showSummary = true;
        this.installationFeeSaved = true;
        this.getSummary();
      }
    });
  }

  saleOrderPaymentInsert(obj) {
    let finalOb = {
      referenceId: this.serviceAgreementStepsParams.selectedQuotationVersionIds,
      paymentMethodId: obj?.paymentMethodId,
      amount: obj?.installationFee,
      paymentOptionId: obj?.paymentOptionId,
      paymentStatusId: 2,
      createdUserId: this.loggedInUserData.userId,
    }
    let paymentInsertDTOs = [];
    if (obj && obj.installationPaymentOptions && obj.installationPaymentOptions.length > 0) {
      obj.installationPaymentOptions.forEach(element => {
        let ele = {
          paymentId: this.loggedInUserData.userId,
          paymentTransactionInvoiceId: this.loggedInUserData.userId,
          paymentTransactionId: this.loggedInUserData.userId,
          paymentRefNo: null,
          paymentMethodId: element.paymentMethodId,
          paidAmount: obj.installationFee,
          paymentStatusId: 2,
          paymentPercentageId: element.paymentPercentageId,
          paymentExcessAmountId: null,
          isActive: true,
          createdUserId: this.loggedInUserData.userId,
          debitDate: element.paymentDate,
          paymentOptionId: element.paymentOptionId
        }
        paymentInsertDTOs.push(ele);
        finalOb['paymentInsertDTOs'] = paymentInsertDTOs;
      });
    }
    this.crudService.create(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.SALES_ORDER_PAYMENT_Insert, finalOb).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setDialogOpenProperty(false);
      }
    });
  }

  backFromSummaryToInstallationFeeTab() {
    if (this.installationPaymentOptions) {
      this.installationPaymentOptions.clear();
      this.installationFeeForm.get('paymentOptionId').setValue("");
    }
    this.getInstallationFeeDetailsByprocessSaleId(this.processSaleId, 'back');
    this.selected.setValue(1);
    this.showSummary = false;
  }

  processPayment() {
    let payload = {
      processSaleId: this.processSaleId,
      createdUserId: this.loggedInUserData.userId
    }
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_ORDER, payload).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.showSummary = false;
        this.showModalFirst = false;
        this.router.navigate(['/sales/task-management/leads/quotation/view']);
        this.rxjsService.setDialogOpenProperty(false);
      }
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  backFromInstallationToServiceFeeTab() {
    this.selected.setValue(1);
    this.paymentDetails.clear();
    this.selected.setValue(0);
    this.getServiceFeeDetails(this.processSaleId);
  }

  getServiceFeeDetails(processSaleId) {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_GET_SERVICE_FEE, undefined, false,
      prepareGetRequestHttpParams(null, null, { processSaleId })).subscribe(resp => {
        this.serviceDetail = resp.resources;
        if (this.paymentDetails) {
          this.paymentDetails.clear();
        }
        this.paymentDetails = this.getPaymentDetils;
        if (resp.resources) {
          this.servicefeeDetails = resp.resources;
          if (this.servicefeeDetails.monthlyFee == 0) {
            this.servicefeeDetails.monthlyFee = Number(this.servicefeeDetails.monthlyFee).toFixed(2)
          }
          if (this.servicefeeDetails?.isPurchaseOrderExist == true) {
            this.servicefeeDetails['purchaseOrderDate'] = new Date(this.servicefeeDetails['purchaseOrderDate']);
          }
          this.serviceFeeForm.patchValue(new ServiceFeeModel(this.servicefeeDetails));
          if (!resp.resources.paymentDay) {
            this.serviceFeeForm.get('paymentDay').setValue(0);
          }
          if (resp.resources?.debtorDetail) {
            let serviceFeeFormData = resp.resources.debtorDetail;
            if (serviceFeeFormData.accountNo) {
              let len = serviceFeeFormData.accountNo.length - 4;
              let first = serviceFeeFormData.accountNo.substring(0, len);
              let last = serviceFeeFormData.accountNo.substring(len, serviceFeeFormData.accountNo.length);
              let str = ''
              for (let i = 0; i < first.length; i++) {
                str = str + '*';
              }
              serviceFeeFormData.accountNo = str + '' + last;
            }
            this.serviceFeeForm.patchValue(serviceFeeFormData);
          }
          resp.resources?.paymentDetails.forEach((obj) => {
            this.paymentDetails.push(this.createPaymentDetails(obj));
          });
        }
        if (this.serviceFeeForm.value.paymentMethodId == PaymentMethods.ON_INVOICE) {
          this.serviceFeeForm.get('isProFormaInvoiceRequired').setValue(true);
          this.serviceFeeForm.get('paymentDay').setValue(0);
        }
        this.changeServicePurchaseOrder();
        setTimeout(() => {
          this.rxjsService.setPopupLoaderProperty(false);
        }, 500);
      });
  }

  getInstallationFeeDetailsByprocessSaleId(processSaleId, type?: string) {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_GET_INSTALLATION_FEE, processSaleId, false, null).subscribe(resp => {
      this.installationPaymentOptions = this.getInstllationOptionDetils;
      if (resp.resources && resp.statusCode == 200 && resp.isSuccess) {
        this.isItemAndOnInvoiceOnly = resp.resources.isItemAndOnInvoiceOnly;
        this.installationDetail = resp.resources;
        if (this.installationDetail.installationFee == 0) {
          this.installationDetail.installationFee = Number(this.installationDetail.installationFee).toFixed(2)
        }
        if (this.installationDetail.accountNo) {
          let len = this.installationDetail.accountNo.length - 4;
          let first = this.installationDetail.accountNo.substring(0, len);
          let last = this.installationDetail.accountNo.substring(len, this.installationDetail.accountNo.length);
          let str = ''
          for (let i = 0; i < first.length; i++) {
            str = str + '*';
          }
          this.installationDetail.accountNo = str + '' + last;
        }
        this.installationFeeForm.patchValue(this.installationDetail);
        let paymentMethod = this.paymentMethods.find(e => e.id == this.installationFeeForm.value.paymentMethodId);
        this.installationFeeForm.get('paymentMethodName').setValue(paymentMethod.displayName);
        if (resp.resources?.paymentInstallationFees) {
          if (resp.resources.paymentInstallationFees?.isPurchaseOrderExist == true) {
            resp.resources.paymentInstallationFees['purchaseOrderDate'] = new Date(resp.resources.paymentInstallationFees['purchaseOrderDate']);
          }
          if (resp.resources?.paymentInstallationFees?.installationFeeId) {
            resp.resources?.paymentInstallationFees?.installationPaymentOptions.forEach((obj) => {
              if (type == 'back' || type == 'tab changed') {
                obj.shouldDisable = false;
                obj.isSelectedDateValid = true;
                obj.invalidMessage = "";
              }
              Object.keys(obj).forEach((key) => {
                if (key == 'paymentDate') {
                  obj[key] = new Date(obj[key]);
                }
              });
              this.installationPaymentOptions.push(this.createInstallationPaymentOptions(obj));
            });
          } else {
            // if paymentMethodId is 1 then 'On invoice' else for 2 'Debit Order'
            if (paymentMethod.id == 1 && resp.resources?.isExistService == false && !resp.resources?.isItemAndOnInvoiceOnly) {
              this.installationFeeForm.get('paymentOptionId').disable();
              this.installationFeeForm.get('paymentOptionId').setValue(1);
              let option = this.installationsTypes[0].paymentPercentage[0];
              this.installationPaymentOptions.push(this.createInstallationPaymentOptions(option));
            }
            // Else paymentMethodId is 2 then 'Debit Order' so the user will asked to select the payment frequencies (or)
            // items are only purchased (for this case by default payment method is 1 (on invoice)) then enable the user to select the payment frequencies
            else {
              let dt = {};
              dt['createdUserId'] = this.loggedInUserData.userId;
              dt['paymentMethodId'] = this.installationFeeForm.value.paymentMethodId;
              this.installationPaymentOptions = this.getInstllationOptionDetils;
              this.installationPaymentOptions.push(this.createInstallationPaymentOptions());
            }
          }
        } else {
          // if paymentMethodId is 1 then 'On invoice' else for 2 'Debit Order'
          if (paymentMethod.id == 1 && resp.resources?.isExistService == false && !resp.resources?.isItemAndOnInvoiceOnly) {
            this.installationFeeForm.get('paymentOptionId').disable();
            this.installationFeeForm.get('paymentOptionId').setValue(1);
            let option = this.installationsTypes[0].paymentPercentage[0];
            this.installationPaymentOptions.push(this.createInstallationPaymentOptions(option));
          }
          // Else paymentMethodId is 2 then 'Debit Order' so the user will asked to select the payment frequencies (or)
          // items are only purchased (for this case by default payment method is 1 (on invoice)) then enable the user to select the payment frequencies 
          else {
            let dt = {};
            dt['createdUserId'] = this.loggedInUserData.userId;
            dt['paymentMethodId'] = this.installationFeeForm.value.paymentMethodId;
            this.installationPaymentOptions = this.getInstllationOptionDetils;
            this.installationPaymentOptions.push(this.createInstallationPaymentOptions());
          }
        }
      } else {
        this.installationPaymentOptions = this.getInstllationOptionDetils;
        this.installationPaymentOptions.push(this.createInstallationPaymentOptions());
      }
      if (resp.resources.paymentInstallationFees) {
        this.installationFeeForm.patchValue(new InstallationFeeModel(resp.resources.paymentInstallationFees));
      }
      this.changePurchaseOrder();
      setTimeout(() => {
        this.rxjsService.setPopupLoaderProperty(false);
      }, 500);
    });
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_PROCESS_SALES, undefined, false, prepareGetRequestHttpParams(null, null, {
      quotationVersionIds: this.serviceAgreementStepsParams.selectedQuotationVersionIds
    })).subscribe((resp) => {
      if (resp.resources && resp.isSuccess && resp.statusCode == 200) {
        this.detailsInfo = resp.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onChangePaymentOption() {
    let payment = this.installationFeeForm.value.paymentOptionId;
    let found;
    this.installationsTypes.forEach(element => {
      if (element.paymentOptionId == payment) {
        found = element.paymentPercentage;
      }
    });
    this.installationPaymentOptions = this.getInstllationOptionDetils;
    if (found.length > 0) {
      this.installationPaymentOptions.clear();
      found.forEach((element: InstallationPriceModel) => {
        let elementCopy = { ...element };
        // if payment method id is 1 then by default it is oninvoice
        elementCopy.paymentMethodId = this.isItemAndOnInvoiceOnly ? '1' : "";
        this.installationPaymentOptions.push(this.createInstallationPaymentOptions(elementCopy));
      });
    }
  }

  backwardForwardClik(str) {
    if (str == 'backward') {
      this.router.navigate(['/sales/task-management/leads/quotation/view'], { queryParams: { leadId: this.leadId } });
    }
  }
}