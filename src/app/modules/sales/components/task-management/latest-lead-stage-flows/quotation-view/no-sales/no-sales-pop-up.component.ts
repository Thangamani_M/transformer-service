import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, NoSalesQuotationModel, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
    ConfirmDialogPopupComponent, CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
    RxjsService, setRequiredValidator
} from '@app/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
    selector: 'no-sales-up-pop-up',
    templateUrl: './no-sales-pop-up.component.html'
})

export class NoSalesPopUpComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    noSalesQuotationForm: FormGroup;
    loggedInUserData: LoggedInUserModel;
    outcomeReasonId = "";
    isCometetorShow = true;
    promotionOfferShow = false;
    newsLetterShow = false;
    motivations = [];
    newsLetterTypes = [];

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        private dialog: MatDialog,
        private crudService: CrudService,
        public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
        private formBuilder: FormBuilder,
        private store: Store<AppState>, private rxjsService: RxjsService) {
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
        this.combineLatestNgrxStoreData();
        this.createLssResourceForm();
        this.getMotivation();
    }

    combineLatestNgrxStoreData() {
        combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
        });
    }

    getMotivation() {
        this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.SALES_API_UX_MOTIVATION_REASON,
            undefined, false, null).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200 && response.resources) {
                    this.motivations = response.resources;
                    this.getNewsLetterTypes();
                }
            });
    }

    radioChange(e) {
        if (e.value) {
            this.isCometetorShow = true;
            this.noSalesQuotationForm.controls['competitorName'].setValidators([Validators.required]);
            this.noSalesQuotationForm.controls['outcomeReasonId'].setValidators([Validators.required]);
            this.noSalesQuotationForm.controls['isCustomerNotBuyFromCompetitor'].setValue(false);
        } else {
            this.isCometetorShow = false;
            this.noSalesQuotationForm.controls['competitorName'].setValue(null);
            this.noSalesQuotationForm.controls['competitorName'].clearValidators();
            this.noSalesQuotationForm.controls['competitorName'].updateValueAndValidity();
            this.noSalesQuotationForm.controls['outcomeReasonId'].setValue(null);
            this.noSalesQuotationForm.controls['outcomeReasonId'].clearValidators();
            this.noSalesQuotationForm.controls['outcomeReasonId'].updateValueAndValidity();
            this.noSalesQuotationForm.controls['isCustomerNotBuyFromCompetitor'].setValue(true);
        }
    }

    showOptions(e) {
        if (e.checked) {
            this.promotionOfferShow = true;
            this.noSalesQuotationForm.controls['promotionalOfferNewsLetterTypeId'].setValidators([Validators.required]);
        } else {
            this.promotionOfferShow = false;
            this.noSalesQuotationForm.controls['promotionalOfferNewsLetterTypeId'].setValue(null);
            this.noSalesQuotationForm.controls['promotionalOfferNewsLetterTypeId'].clearValidators();
            this.noSalesQuotationForm.controls['promotionalOfferNewsLetterTypeId'].updateValueAndValidity();
        }
    }

    showNewsOptions(e) {
        if (e.checked) {
            this.newsLetterShow = true;
            this.noSalesQuotationForm.controls['newsLetterTypeId'].setValidators([Validators.required]);
        } else {
            this.newsLetterShow = false;
            this.noSalesQuotationForm.controls['newsLetterTypeId'].setValue(null);
            this.noSalesQuotationForm.controls['newsLetterTypeId'].clearValidators();
            this.noSalesQuotationForm.controls['newsLetterTypeId'].updateValueAndValidity()
        }
    }

    getNewsLetterTypes() {
        this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.SALES_API_UX_NEWS_LETTER_TYPES,
            undefined, false, null).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200 && response.resources) {
                    this.newsLetterTypes = response.resources;
                }
                this.rxjsService.setPopupLoaderProperty(false);
            });
    }

    createLssResourceForm(): void {
        let noSalesQuotationModel = new NoSalesQuotationModel();
        if (noSalesQuotationModel.outcomeReasonId) {
            this.outcomeReasonId = noSalesQuotationModel.outcomeReasonId;
        }
        this.noSalesQuotationForm = this.formBuilder.group({});
        Object.keys(noSalesQuotationModel).forEach((key) => {
            this.noSalesQuotationForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedInUserData.userId) :
                new FormControl(noSalesQuotationModel[key]));
        });
        this.noSalesQuotationForm = setRequiredValidator(this.noSalesQuotationForm, ["competitorName", "outcomeReasonId"]);
    }

    onSubmit(): void {
        if (this.noSalesQuotationForm.invalid) {
            return;
        }
        this.noSalesQuotationForm.value.leadId = this.data.leadId;
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_QUOTATION_OUTCOME_NO_SALE,
            this.noSalesQuotationForm.value, 1).subscribe((response) => {
                if (response.isSuccess && response.statusCode === 200) {
                    this.dialog.closeAll();
                    this.noSalesQuotationForm.reset();
                    this.outputData.emit(true);
                }
            });
    }

    dialogClose(): void {
        this.dialogRef.close(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }
}
