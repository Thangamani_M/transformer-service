import { DatePipe, ViewportScroller } from '@angular/common';
import { Component, ElementRef, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatRadioChange } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { addIsCheckedProperty, addNullOrStringDefaultFormControlValue, adjustDateFormatAsPerPCalendar, BreadCrumbModel, clearFormControlValidators, CrudService, getNthDate, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, ReusablePrimeNGTableFeatureService, RxjsService, SERVER_REQUEST_DATE_TIME_TRANSFORM, SERVER_REQUEST_DATE_TRANSFORM, setRequiredValidator, SnackbarService } from '@app/shared';
import { ModuleName, ResponseMessageTypes } from '@app/shared/enums';
import { loggedInUserData, selectStaticEagerLoadingOutcomeStatusState$ } from '@modules/others';
import { LeadCreationStepperParamsCreateAction, LeadCreationUserDataModel, LeadHeaderData, NoSalesPopUpComponent, selectLeadCreationUserDataState$, selectLeadHeaderDataState$, ServiceAgreementStepsParams, TechnicalAppointmentFormModel } from '@modules/sales';
import { ContractTypes, LeadOutcomeStatusNames, QuotationOutcomeStatus, SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { TechnicalAppointmentStatus, TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import moment from 'moment';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { LeadHeaderDataCreateAction, selectLeadCreationStepperParamsState$ } from '../../lead-creation-ngrx-files';
import { QuotationPendingPopUpComponent } from './quotation-pending';
import { QuoteDeclinePopUpComponent } from './quote-decline/quote-decline-pop-up.component';

declare var $;
@Component({
  selector: 'app-quotation-view',
  templateUrl: './quotation-view.component.html',
  styleUrls: ['./quotation-view.component.scss']
})

export class QuotationViewComponent {
  quotationSummary = [];
  selectedQuotation;
  salesOrders = [];
  outComeId = "";
  appointment;
  isProceedPaymentBtnDisabled = true;
  isTechnicalApptBtnDisabled = true;
  @ViewChild('technical_appointment', { static: false }) technical_appointment: ElementRef;
  @ViewChild('handover_certificate', { static: false }) handover_certificate: ElementRef;
  @ViewChildren('buttons') buttons: QueryList<ElementRef>;
  @ViewChild('terms_and_conditions_modal', { static: false }) terms_and_conditions_modal: ElementRef<any>;
  callIdNo = '';
  detailsObj;
  technicalAppointmentDetails;
  selectedHandoverCertificateDetails;
  shouldShowNoSaleBtn = true;
  isSaleCompletedBtnDisabled = true;
  selectedOrder;
  noOfHours;
  acceptedQuote;
  IsInstallationDebtor: any;
  technicalAppointmentAlertMessage = '';
  technicalAppointmentStatus = TechnicalAppointmentStatus;
  preferenceForm: FormGroup;
  leadHeaderData: LeadHeaderData;
  outcomeStatus = [];
  acceptedQuotations = [];
  leadCreationUserDataModel: LeadCreationUserDataModel;
  loggedInUserData: LoggedInUserModel;
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  technicalAppointmentForm: FormGroup;
  handoverCeritificateForm: FormGroup;
  isNoSaleBtnDisabled = true;
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  handOverCertificates = [];
  handOverCertificateCategory = [];
  handOverCertificateSubCategories: FormArray;
  isFormSubmitted = false;
  isCustomerSignedOff = false;
  description = '';
  breadCrumb: BreadCrumbModel;
  isAcceptTermsandConditions = false;
  contractTermsandConditions = [];
  isContractBtnDisabled = true;
  parseHtml;
  selectedTermsAndConditions = [];
  isAcceptTermsAndConditionsBtnDisabled = true;
  selectedQuoteFromDropDown;
  tomorrowDate= getNthDate(new Date(),1);
  dateFormat = adjustDateFormatAsPerPCalendar();

  constructor(public rxjsService: RxjsService, private dialog: MatDialog,
    private crudService: CrudService, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private router: Router, private snackbarService: SnackbarService,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private sanitizer: DomSanitizer, private _vps: ViewportScroller, private datePipe: DatePipe) {
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.onBootstrapModalEventChanges();
    this.rxjsService.getUniqueCallId()
      .subscribe(data => {
        if (data && data.UniqueCallid) {
          this.callIdNo = data.UniqueCallid;
        }
      });
    this.createMarketingpreferenceForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationUserDataState$),
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingOutcomeStatusState$),
      this.store.select(selectLeadHeaderDataState$)]
    ).pipe(take(1)).subscribe((response) => {
      this.leadCreationUserDataModel = new LeadCreationUserDataModel(
        response[0]
      );
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(
        response[1]
      );
      this.loggedInUserData = new LoggedInUserModel(response[2]);
      this.outcomeStatus = response[3];
      this.leadHeaderData = response[4];
      this.breadCrumb = {
        pageTitle: { key: 'Lead No', value: this.leadHeaderData.leadRefNo },
        items: [{
          key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
        }, { key: 'Process Quotation' }]
      };
      this.getLeadHeaderDataByLeadId();
      this.getQuotations();
      this.getHandOverCertificates();
    });
  }

  getLeadHeaderDataByLeadId() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEADS_HEADER, this.leadHeaderData.leadId, false, undefined, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        let leadHeaderData = new LeadHeaderData(response.resources);
        leadHeaderData.fromUrl = this.leadHeaderData.fromUrl;
        this.store.dispatch(new LeadHeaderDataCreateAction({ leadHeaderData }));
        this.isSaleCompletedBtnDisabled = leadHeaderData.isSaleCompleted ? false : true;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onBootstrapModalEventChanges() {
    $("#technical_appointment").on("shown.bs.modal", () => {
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#technical_appointment").on("hidden.bs.modal", () => {
      this.rxjsService.setDialogOpenProperty(false);
      //this.technicalAppointmentForm.reset();
    });
    $("#handover_certificate").on("shown.bs.modal", () => {
      this.handOverCertificateCategory = [];
      this.isCustomerSignedOff = false;
      this.callIdNo = null;
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#handover_certificate").on("hidden.bs.modal", () => {
      this.rxjsService.setDialogOpenProperty(false);
    });
    $("#terms_and_conditions_modal").on("shown.bs.modal", () => {
      this.isAcceptTermsandConditions = false;
      this.callIdNo = "";
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#terms_and_conditions_modal").on("hidden.bs.modal", () => {
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  createMarketingpreferenceForm() {
    this.preferenceForm = this.formBuilder.group({
      leadId: [this.leadHeaderData.leadId, Validators.required],
      isEmail: [false, Validators.required],
      isSMS: [false, Validators.required],
      isMobileNumber: [false, Validators.required],
      isLandLineNumber: [false, Validators.required],
      isPostal: [false, Validators.required],
      isNoMarketing: [false, Validators.required],
      isMorningBefore12: [false, Validators.required],
      isAfterNoonAfter12: [false, Validators.required],
      isLaterAfterNoonAfter16: [false, Validators.required],
      email: ['', Validators.required],
      smsNumber: ['', Validators.required],
      mobileNumber: ['', Validators.required],
      landLineNumber: ['', Validators.required],
      address: ['', Validators.required],
      postalCode: ['', Validators.required],
      createdUserId: [this.loggedInUserData.userId, Validators.required],
    });
  }

  onTermsAndConditionsClicked(type?: string) {
    let tempArray = [];
    this.contractTermsandConditions.forEach((value, index) => {
      if (!value.parentTermsandConditionId || value.parentTermsandConditionId === null) {
        this.contractTermsandConditions.splice(index, 1);
      }
      tempArray = this.contractTermsandConditions;
    });
    this.isContractBtnDisabled = (tempArray.filter(ct => !ct['isChecked']).length == 0 && this.isAcceptTermsandConditions &&
      tempArray.length > 0) ? false : true;
    this.isAcceptTermsAndConditionsBtnDisabled = this.contractTermsandConditions.filter(c => c['isChecked']).length !== this.contractTermsandConditions.length;
    if (type == 'final accept' && this.isAcceptTermsandConditions || type == undefined && this.isAcceptTermsandConditions) {
      this.isAcceptTermsAndConditionsBtnDisabled = true;
    }
  }

  getContractTermsAndConditions() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        this.acceptedQuote.monthlyFees ? SalesModuleApiSuffixModels.SERVICE_AGREEMENT_CONTRACT_TERMS_AND_CONDITIONS :
          SalesModuleApiSuffixModels.INSTALLATION_AGREEMENT_CONTRACT_TERMS_AND_CONDITIONS,
        undefined,
        false,
        prepareRequiredHttpParams({
          leadId: this.leadHeaderData.leadId
        }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.callIdNo = response.resources.callId;
          response.resources.contractTermsandConditions.forEach((contractTermsandConditions) => {
            contractTermsandConditions.isCheckboxDisabled = true;
            contractTermsandConditions.isTermsAndConditionsRead = false;
          })
          this.contractTermsandConditions = response.resources.contractTermsandConditions;
          this.contractTermsandConditions.forEach((contractTermsandCondition, index: number) => {
            if (index == 0) {
              this.selectedTermsAndConditions = contractTermsandCondition;
            }
            if (contractTermsandCondition.parentTermsandConditionId === null || !contractTermsandCondition.parentTermsandConditionId) {
              this.parseHtml = JSON.stringify(contractTermsandCondition.termsandConditionDescription);
              this.parseHtml = JSON.parse(this.parseHtml);
              this.parseHtml = this.sanitizer.bypassSecurityTrustHtml(this.parseHtml);
            }
            contractTermsandCondition.isChecked = false;
          })
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  triggerBtnClickEvent(index, accordionType, handOverCertificateCategory) {
    if (accordionType == 'collapse') {
      handOverCertificateCategory.isCollapsed = false;
      handOverCertificateCategory.isExpanded = true;
    }
    else {
      handOverCertificateCategory.isCollapsed = true;
      handOverCertificateCategory.isExpanded = false;
    }
    this.buttons.forEach((item, ix) => {
      if (ix == index) {
        item.nativeElement.click();
      }
    });
  }

  getQuotations() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_GET_QUOTATIONS,
      undefined,
      false,
      prepareRequiredHttpParams({
        leadId: this.leadHeaderData.leadId
      })).subscribe((response: IApplicationResponse) => {
        this.detailsObj = response.resources
        if (response.isSuccess && response.resources && response.statusCode === 200) {
          if (response.resources.quotations) {
            this.quotationSummary = response.resources.quotations;
            this.quotationSummary && this.quotationSummary.forEach(element => {
              element.isDisabled = (element.outcomeId == 3 || element.outcomeId == null) ? false : true;
              if (element.quotationStatusName === 'Accept') {
                this.shouldShowNoSaleBtn = false;
              }
            });
            this.isNoSaleBtnDisabled = (this.quotationSummary.filter(qS => qS['outcomeId'] == QuotationOutcomeStatus.DECLINE).length ==
              this.quotationSummary.length) ? false : true;
          }
          if (response.resources.acceptedQuotations) {
            var selectedQuotation = response.resources.acceptedQuotations.find(a => a['quotationVersionId'] == this.serviceAgreementStepsParams.quotationVersionId);
            if (selectedQuotation) {
              selectedQuotation.isAcceptedQuoteSelected = true;
              this.selectedQuotation = selectedQuotation;
            }
            response.resources.acceptedQuotations.forEach((resource, index) => {
              if (!selectedQuotation && index == 0) {
                resource.isAcceptedQuoteSelected = true;
                this.selectedQuotation = resource;
              }
              else if (selectedQuotation?.quotationVersionId !== resource.quotationVersionId) {
                resource.isAcceptedQuoteSelected = false;
              }
              else if (!selectedQuotation && index !== 0) {
                resource.isAcceptedQuoteSelected = false;
              }
            });
            this.acceptedQuotations = addIsCheckedProperty(response.resources.acceptedQuotations);
          }
          if (response.resources.salesOrders) {
            this.salesOrders = addIsCheckedProperty(response.resources.salesOrders);
            this.salesOrders.forEach((salesOrder) => {
              salesOrder.isDisabled = ((salesOrder.technicalAppointmentStatus &&
                salesOrder.technicalAppointmentStatus == this.technicalAppointmentStatus.BOOKED) || !salesOrder.technicalAppointmentStatus) ? true : false;
            });
        //    this.isSaleCompletedBtnDisabled = this.leadHeaderData.isSaleCompleted ? false : true;
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getHandOverCertificates() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LEAD_HAND_OVER_CERTIFICATE_DOCUMENT,
      undefined,
      false,
      prepareRequiredHttpParams({
        leadId: this.leadHeaderData.leadId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources && response.statusCode === 200) {
          this.handOverCertificates = [];
          this.handOverCertificates = response.resources;
          if (this.handOverCertificates.length > 0) {
            let createdHandoverCertificatesCount = 0;
            this.handOverCertificates.forEach((handOverCertificate) => {
              if (handOverCertificate.handOverCertificateRefNo) {
                createdHandoverCertificatesCount += 1;
              }
            });
          }
        }
        this.rxjsService.setDialogOpenProperty(false);
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getHandOverCertificateQuestionsById() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LEAD_HAND_OVER_CERTIFICATE,
      undefined,
      false,
      prepareRequiredHttpParams({
        leadId: this.leadHeaderData.leadId,
        systemTypeId: this.selectedHandoverCertificateDetails.systemTypeId ? this.selectedHandoverCertificateDetails.systemTypeId : "",
        saleOrderId: this.selectedHandoverCertificateDetails.saleOrderId,
        qutationVersionId: this.selectedHandoverCertificateDetails.qutationVersionId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources && response.statusCode === 200) {
          this.handOverCertificateCategory = response.resources.handOverCertificateCategory;
          // add a property to handle expand collapse actions
          this.handOverCertificateCategory.forEach((handOverCertificateCategory, index: number) => {
            handOverCertificateCategory['isExpanded'] = index == 0 ? true : false;
            handOverCertificateCategory['isCollapsed'] = index == 0 ? false : true;
            handOverCertificateCategory['areMandatoryFilled'] = false;
            let mandatoryFieldsCount = 0;
            handOverCertificateCategory.handOverCertificateSubCategories.forEach((handOverCertificateSubCategory, index) => {
              handOverCertificateSubCategory.handOverCertificateFlag = handOverCertificateSubCategory.handOverCertificateFlag == 'YES' ? true :
                handOverCertificateSubCategory.handOverCertificateFlag == 'NO' ? false : handOverCertificateSubCategory.handOverCertificateFlag;
              handOverCertificateSubCategory.tempHandOverCertificateFlag = handOverCertificateSubCategory.handOverCertificateFlag;
              if (handOverCertificateSubCategory.handOverCertificateFlag !== undefined && (handOverCertificateSubCategory.handOverCertificateFlag ||
                handOverCertificateSubCategory.handOverCertificateFlag == false)) {
                mandatoryFieldsCount += 1;
              }
              if (index == handOverCertificateCategory.handOverCertificateSubCategories.length - 1) {
                handOverCertificateCategory.areMandatoryFilled = (mandatoryFieldsCount == handOverCertificateCategory.handOverCertificateSubCategories.length) ? true : false
                return;
              }
            });
          });
        }
        this.rxjsService.setDialogOpenProperty(false);
      });
  }

  get HandOverCertificateCategoriesArray(): FormArray {
    if (!this.handoverCeritificateForm) return;
    return this.handoverCeritificateForm.get("handOverCertificateCategory") as FormArray;
  }

  get HandOverCertificateSubCategoriesArray(): FormArray {
    if (!this.handoverCeritificateForm) return;
    return this.handoverCeritificateForm.get("handOverCertificateSubCategories") as FormArray;
  }

  scrollFn(anchor: string) {
    this._vps.scrollToAnchor(anchor);
  }

  onOpenTermsAndConditionsIndex(selectionTypeVia: string, index: number, contractTermsandCondition, anchor: string) {
    if (selectionTypeVia == 'link') {
      contractTermsandCondition.isCheckboxDisabled = contractTermsandCondition.isChecked ? true : false;
      contractTermsandCondition.isTermsAndConditionsRead = true;
    }
    else if (selectionTypeVia == 'checkbox') {
      contractTermsandCondition.isChecked = true;
      contractTermsandCondition.isCheckboxDisabled = true;
    }
    let element = document.getElementById(anchor);
    if (element) {
      element.scrollIntoView(); // scroll to a particular element
    }
    this.onTermsAndConditionsClicked();
  }

  onChangeQuoteStatus(quotationOutcomeStatus?, quote?, index?) {
    this.selectedQuoteFromDropDown = quote;
    if (quotationOutcomeStatus == QuotationOutcomeStatus.ACCEPT) {
      this.acceptedQuote = quote;
      this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog('Do you want to accept this quote ?').
        onClose?.subscribe(dialogResult => {
          if (!dialogResult) {
            quote.outcomeId = null;
            return;
          }
          this.shouldShowNoSaleBtn = false;
          $(this.terms_and_conditions_modal.nativeElement).modal('show');
          this.getContractTermsAndConditions();
        });
    }
    else {
      let data = {};
      if (quotationOutcomeStatus !== QuotationOutcomeStatus.NO_SALE) {
        this.outComeId = quote.outcomeId;
        data = quote;
        data['selectedOutcomeId'] = quotationOutcomeStatus;
      }
      data['leadId'] = this.leadHeaderData.leadId;
      let openablePopupComponent: any = quotationOutcomeStatus == QuotationOutcomeStatus.DECLINE ? QuoteDeclinePopUpComponent :
        quotationOutcomeStatus == QuotationOutcomeStatus.PENDING ? QuotationPendingPopUpComponent :
          quotationOutcomeStatus == QuotationOutcomeStatus.NO_SALE ? NoSalesPopUpComponent : "";
      this.rxjsService.setDialogOpenProperty(true);
      const dialogReff1 = this.dialog.open(openablePopupComponent, { width: '700px', data, disableClose: false });
      dialogReff1.componentInstance['outputData'].subscribe(result => {
        if (!result) {
          quote.outcomeId = null;
          return;
        }
        this.getQuotations();
        this.getLeadHeaderDataByLeadId();
      });
      dialogReff1.afterClosed().subscribe(dialogResult => {
        if (dialogResult && quotationOutcomeStatus == QuotationOutcomeStatus.DECLINE) {
          quote.outcomeId = null;
          return;
        };
        switch (quotationOutcomeStatus) {
          case QuotationOutcomeStatus.DECLINE:
            if (dialogResult && dialogResult.isSubmitted && dialogResult.isSubmitted == true) {
              let selectedQuoteDetails = this.quotationSummary.find(qS => qS['quotationVersionId'] == quote['quotationVersionId']);
              if (selectedQuoteDetails) {
                selectedQuoteDetails.outcomeId = QuotationOutcomeStatus.DECLINE;
              }
              this.isNoSaleBtnDisabled = (this.quotationSummary.filter(qS => qS['outcomeId'] == QuotationOutcomeStatus.DECLINE).length ==
                this.quotationSummary.length) ? false : true;
            }
            break;
        }
      });
    }
  }

  selectAcceptedQuotation(selectedAcceptQuotation, index: number) {
    this.acceptedQuotations.forEach((acceptedQuotation, i) => {
      acceptedQuotation.isAcceptedQuoteSelected = i == index ? true : false;
    });
    this.selectedQuotation = selectedAcceptQuotation;
  }

  onSelectionChanged(type: string, salesOrder) {
    salesOrder.isChecked = !salesOrder.isChecked;
    if (type == 'accepted quotes') {
      this.isProceedPaymentBtnDisabled = this.acceptedQuotations.find(a => a.isChecked) ? false : true;
    }
    else {
      this.isTechnicalApptBtnDisabled = this.salesOrders.find(so => so.isChecked) ? false : true;
    }
    this.salesOrders.forEach(element => {
      element.isChecked = salesOrder.saleOrderId == element.saleOrderId ? element.isChecked : false;
    });
  }

  onBtnClick(type, selectedHandoverCertificateDetails?) {
    switch (type) {
      case 'payment':
        this.serviceAgreementStepsParams.selectedQuotationVersionIds = "";
        this.serviceAgreementStepsParams.selectedQuotationNumbers = "";
        this.acceptedQuotations.filter(a => a.isChecked).forEach((filteredQuotation) => {
          this.serviceAgreementStepsParams.selectedQuotationNumbers = this.serviceAgreementStepsParams.selectedQuotationNumbers ? this.serviceAgreementStepsParams.selectedQuotationNumbers + ',' + filteredQuotation.quotationNumber : filteredQuotation.quotationNumber;
          this.serviceAgreementStepsParams.selectedQuotationVersionIds = this.serviceAgreementStepsParams.selectedQuotationVersionIds ? this.serviceAgreementStepsParams.selectedQuotationVersionIds + ',' + filteredQuotation.quotationVersionId : filteredQuotation.quotationVersionId;
        });
        this.onValidateDebtor();
        break;
      case 'technical':
        this.createTechnicalAppointmentForm();
        this.onFormControlChanges();
        this.getTechnicianDetailsBySaleOrderId();
        break;
      case 'certificate':
        this.selectedHandoverCertificateDetails = selectedHandoverCertificateDetails;
        $(this.handover_certificate.nativeElement).modal('show');
        this.getHandOverCertificateQuestionsById();
        break;
    }
  }

  getTechnicianDetailsBySaleOrderId() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.APPOINTMENTS_TECHNICIAN_DETAILS,
      undefined,
      false,
      prepareRequiredHttpParams({
        saleOrderId: this.salesOrders.find(so => so.isChecked).saleOrderId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources && response.statusCode === 200) {
          this.technicalAppointmentDetails = response.resources;
          this.technicalAppointmentDetails?.appointmentDates.forEach((appointmentDateObj, index) => {
            if (index == 0) {
              appointmentDateObj.isChecked = true;
              this.selectAppointmentDate(appointmentDateObj, index);
            }
            else {
              appointmentDateObj.isChecked = false;
            }
          });
          this.technicalAppointmentForm.get('isSpecialAppointment').setValue(response.resources.isSpecialAppointment);
          if (response.resources.isSpecialAppointment) {
            this.technicalAppointmentForm = setRequiredValidator(this.technicalAppointmentForm, ['isUrgent']);
          }
          else {
            this.technicalAppointmentForm.get('isUrgent').setValidators(null);
            this.technicalAppointmentForm = addNullOrStringDefaultFormControlValue(this.technicalAppointmentForm, [{
              controlName: 'isCode50',
              defaultValue: null
            }, { controlName: 'isUrgent', defaultValue: null }]);
          }
          if (response.resources.alertMessage) {
            this.technicalAppointmentAlertMessage = response.resources.alertMessage;
            this.snackbarService.openSnackbar(response.resources.alertMessage, ResponseMessageTypes.WARNING);
          }
          $(this.technical_appointment.nativeElement).modal('show');
          if (response.resources.specialAppointmentDate) {
            response.resources.specialAppointmentDate = new Date(response.resources.specialAppointmentDate);
            this.tomorrowDate = new Date(response.resources.specialAppointmentDate);
          }
          else {
            let tomorrowDate=getNthDate(new Date(),1);
            response.resources.specialAppointmentDate = tomorrowDate;
            this.tomorrowDate =tomorrowDate;
          }
          this.technicalAppointmentForm.patchValue(new TechnicalAppointmentFormModel(response.resources));
          this.noOfHours = response.resources.estimatedTime ? moment(response.resources.estimatedTime, "HH:mm").format("HH:mm") : '00:00';
          if (!response.resources.isUrgent && !response.resources.isCode50) {
            this.technicalAppointmentForm.get('isUrgent').setValue(null);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onValidateDebtor() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_VALIDATE_DEBTOR,
      undefined,
      false,
      prepareRequiredHttpParams({
        quotationVersionIds: this.serviceAgreementStepsParams.selectedQuotationVersionIds
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && !response.exceptionMessage && response.statusCode === 200) {
          this.store.dispatch(new LeadCreationStepperParamsCreateAction({ serviceAgreementStepsParams: this.serviceAgreementStepsParams }));
          this.onRedirectToCapturePaymentPage();
        }
      });
  }

  onFormControlChanges() {
    this.technicalAppointmentForm.get('isSpecialAppointment').valueChanges.subscribe((isSpecialAppointment: boolean) => {
      if (isSpecialAppointment) {
        this.technicalAppointmentForm = setRequiredValidator(this.technicalAppointmentForm, ['isUrgent', "comments", "specialAppointmentDate"]);
      }
      else {
        this.technicalAppointmentForm = clearFormControlValidators(this.technicalAppointmentForm, ['isUrgent', 'comments', 'specialAppointmentDate']);
        this.technicalAppointmentForm = addNullOrStringDefaultFormControlValue(this.technicalAppointmentForm, [{
          controlName: 'specialAppointmentDate',
          defaultValue: null
        }, { controlName: 'isUrgent', defaultValue: null }, { controlName: 'isCode50', defaultValue: null }]);
      }
    });
  }

  createTechnicalAppointmentForm() {
    let technicalAppointmentFormModel = new TechnicalAppointmentFormModel();
    this.technicalAppointmentForm = this.formBuilder.group({});
    Object.keys(technicalAppointmentFormModel).forEach((key) => {
      this.technicalAppointmentForm.addControl(
        key,
        new FormControl(technicalAppointmentFormModel[key])
      );
    });
  }

  selectAppointmentDate(technicianDetails, i) {
    technicianDetails.isChecked = true;
    this.technicalAppointmentDetails?.appointmentDates.forEach((appointmentDateObj, j) => {
      if (i !== j) {
        appointmentDateObj.isChecked = false;
      }
    });
  }

  onTechnicalAppointmentSubmit() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.technicalAppointmentForm.invalid) {
      return;
    }
    if (this.technicalAppointmentForm.value.isSpecialAppointment) {
      this.technicalAppointmentForm.value.isCode50 = !this.technicalAppointmentForm.value.isUrgent;
      this.technicalAppointmentForm.value.specialAppointmentDate = this.datePipe.transform(this.technicalAppointmentForm.value.specialAppointmentDate, SERVER_REQUEST_DATE_TRANSFORM);
    }
    else {
      this.technicalAppointmentForm.value.isCode50 = false;
      this.technicalAppointmentForm.value.isUrgent = false;
    }
    let isCheckedPropObj = this.technicalAppointmentDetails?.appointmentDates?.find(el => el.isChecked) ?
      this.technicalAppointmentDetails?.appointmentDates.find(el => el.isChecked) : null;
    if (isCheckedPropObj) {
      this.technicalAppointmentForm.value.appointmentDate = this.datePipe.transform(isCheckedPropObj.appointmentDate, SERVER_REQUEST_DATE_TIME_TRANSFORM);
      this.technicalAppointmentForm.value.technicianId = isCheckedPropObj.technicianId;
    }
    else {
      this.technicalAppointmentForm.value.appointmentDate = null;
      this.technicalAppointmentForm.value.technicianId = null;
    }
    this.technicalAppointmentForm.value.createdUserId = this.serviceAgreementStepsParams.createdUserId ? this.serviceAgreementStepsParams.createdUserId : this.loggedInUserData.userId;
    this.technicalAppointmentForm.value.appointmentId = this.technicalAppointmentForm.value.appointmentId ? this.technicalAppointmentForm.value.appointmentId : null;
    this.technicalAppointmentForm.value.quotationversionId = this.technicalAppointmentDetails.quotationVersionId;
    this.technicalAppointmentForm.value.addressId = this.technicalAppointmentDetails.addressId;
    this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.APPOINTMENTS_TECHNICIAN, this.technicalAppointmentForm.value).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          $(this.technical_appointment.nativeElement).modal('hide');
          this.getQuotations();
          this.getLeadHeaderDataByLeadId();
          this.salesOrders.forEach((saleOrder) => {
            saleOrder.isChecked = false;
          });
        }
      });
  }

  onRadioOptionSelected($event: MatRadioChange, handOverCertificateSubCategory, selectedMainCategoryIndex, handOverCertificateCat) {
    handOverCertificateSubCategory.handOverCertificateFlag = $event.value == true ? 'YES' :
      $event.value == false ? 'NO' : $event.value == undefined ? 'NA' : $event.value;
    let mandatoryFieldsCount = 0;
    this.handOverCertificateCategory[selectedMainCategoryIndex].handOverCertificateSubCategories.forEach((handOverCertificateSubCategory, index) => {
      if (handOverCertificateSubCategory.handOverCertificateFlag !== undefined && (handOverCertificateSubCategory.handOverCertificateFlag ||
        handOverCertificateSubCategory.handOverCertificateFlag == false)) {
        mandatoryFieldsCount += 1;
      }
      if (index == this.handOverCertificateCategory[selectedMainCategoryIndex].handOverCertificateSubCategories.length - 1) {
        handOverCertificateCat.areMandatoryFilled = (mandatoryFieldsCount == this.handOverCertificateCategory[selectedMainCategoryIndex].handOverCertificateSubCategories.length) ? true : false
        return;
      }
    });
  }

  onHandOverCertificateFormSubmit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.isFormSubmitted = true;
    if (this.isCustomerSignedOff) {
      for (let i = 0; i < this.handOverCertificateCategory.length; i++) {
        for (let j = 0; j < this.handOverCertificateCategory[i].handOverCertificateSubCategories.length; j++) {
          if (!this.handOverCertificateCategory[i].areMandatoryFilled) {
            return;
          }
        }
      }
    }
    if (this.description == '' && this.handOverCertificateCategory[this.handOverCertificateCategory.length - 1]['handOverCertificateSubCategories']
    [this.handOverCertificateCategory[this.handOverCertificateCategory.length - 1]['handOverCertificateSubCategories'].length - 1]['tempHandOverCertificateFlag'] == true) {
      return;
    }
    let payload = { leadHandOverCertificates: [] };
    payload['leadId'] = this.leadHeaderData.leadId;
    payload['sellerUserId'] = this.loggedInUserData.userId;
    payload['createdUserId'] = this.loggedInUserData.userId;
    payload['isCustomerSignedOff'] = this.isCustomerSignedOff;
    payload['callIdNo'] = this.callIdNo;
    payload['saleOrderId'] = this.selectedHandoverCertificateDetails.saleOrderId;
    payload['systemTypeId'] = this.selectedHandoverCertificateDetails.systemTypeId;
    payload['qutationVersionId'] = this.selectedHandoverCertificateDetails.qutationVersionId;
    this.handOverCertificateCategory.forEach((handOverCertificateCategory, i) => {
      handOverCertificateCategory.handOverCertificateSubCategories.forEach((handOverCertificateSubCategory, j) => {
        if (handOverCertificateSubCategory.handOverCertificateFlag !== undefined && handOverCertificateSubCategory.handOverCertificateFlag !== null) {
          if (j == handOverCertificateCategory.handOverCertificateSubCategories.length - 1) {
            handOverCertificateSubCategory.description = this.description;
          }
          if (handOverCertificateSubCategory.handOverCertificateFlag !== 'YES' && handOverCertificateSubCategory.handOverCertificateFlag !== 'NO' &&
            handOverCertificateSubCategory.handOverCertificateFlag !== 'NA') {
            handOverCertificateSubCategory.handOverCertificateFlag = (handOverCertificateSubCategory.handOverCertificateFlag == true) ? 'YES' :
              (handOverCertificateSubCategory.handOverCertificateFlag == false) ? 'NO' : (handOverCertificateSubCategory.handOverCertificateFlag == 'NA') ? 'NA' :
                handOverCertificateSubCategory.handOverCertificateFlag == 'tru' ? 'YES' : null;
          }
          payload['leadHandOverCertificates'].push(handOverCertificateSubCategory);
        }
      });
    });
    this.crudService.create(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LEAD_HAND_OVER_CERTIFICATE, payload).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          $(this.handover_certificate.nativeElement).modal('hide');
          this.getHandOverCertificates();
          this.getLeadDetailsByLeadId();
          this.isFormSubmitted = false;
        }
      });
  }

  getLeadDetailsByLeadId(): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEADS_HEADER, this.leadHeaderData.leadId, false, undefined, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        let leadHeaderData = new LeadHeaderData(response.resources);
        this.isSaleCompletedBtnDisabled = leadHeaderData.isSaleCompleted ? false : true;
      }
    });
  }

  onCustomerSignedOff() {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.isCustomerSignedOff) {
      this.crudService.get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_LEAD_HAND_OVER_CERTIFICATE_CALL_ID,
        undefined,
        false,
        prepareRequiredHttpParams({
          leadId: this.leadHeaderData.leadId
        })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.resources && response.statusCode === 200) {
            this.callIdNo = response.resources.callIdNo;
            this.isCustomerSignedOff = this.callIdNo ? true : false;
            if (!this.callIdNo) {
              this.snackbarService.openSnackbar("Customer must be connected to a call in order to agree sign off", ResponseMessageTypes.WARNING);
            }
          }
          this.rxjsService.setDialogOpenProperty(false);
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    else {
      this.callIdNo = "";
    }
  }

  getAgreementTabs(acceptedQuoteObj, contractTypeObj, contractType: string) {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.AGREEMENT_TABS,
      undefined,
      false,
      prepareRequiredHttpParams({
        leadId: this.leadHeaderData.leadId,
        quotationVersionId: this.selectedQuotation.quotationVersionId,
        contractTypeId: contractType,
        partitionId: acceptedQuoteObj.partitionId ? acceptedQuoteObj.partitionId : ""
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources && response.statusCode === 200) {
          this.rxjsService.setIsInstallationDebtor(false);
          if (this.acceptedQuotations.length > 0 && contractType == ContractTypes.SERVICE_AGREEMENT &&
            this.detailsObj.isOldServiceAgreementExist && !contractTypeObj.contractId) {
            this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Do you want to copy the existing details?`).
              onClose?.subscribe(dialogResult => {
                this.serviceAgreementStepsParams.isExist = dialogResult ? true : false;
                this.prepareNgrxDataForAgreementTabs(response, contractTypeObj, contractType, acceptedQuoteObj);
              });
          }
          else {
            this.serviceAgreementStepsParams.isExist = false;
            this.prepareNgrxDataForAgreementTabs(response, contractTypeObj, contractType, acceptedQuoteObj);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  prepareAgreementTabs(response: IApplicationResponse) {
    response.resources.forEach((tabObj) => {
      if (this.leadHeaderData.fromUrl == 'Leads List' && tabObj.navigationUrl) {
        tabObj.navigationUrl = '/sales/task-management/leads' + tabObj.navigationUrl;
      }
      else if (tabObj.navigationUrl) {
        tabObj.navigationUrl = '/my-tasks/my-sales/my-leads' + tabObj.navigationUrl;
      }
    });
    return response.resources;
  }

  prepareNgrxDataForAgreementTabs(response, contractTypeObj, contractType, acceptedQuoteObj) {
    this.serviceAgreementStepsParams.leadId = this.leadHeaderData.leadId;
    this.serviceAgreementStepsParams.partitionId = acceptedQuoteObj.partitionId;
    this.serviceAgreementStepsParams.customerId = this.leadHeaderData.customerId;
    this.serviceAgreementStepsParams.quotationVersionId = this.selectedQuotation.quotationVersionId;
    this.serviceAgreementStepsParams.agreementTabs = this.prepareAgreementTabs(response);
    this.serviceAgreementStepsParams.contractId = contractTypeObj.contractId;
    this.serviceAgreementStepsParams.contractType = contractType;
    this.serviceAgreementStepsParams.modifiedUserId = this.loggedInUserData.userId;
    this.serviceAgreementStepsParams.createdUserId = this.loggedInUserData.userId;
    this.serviceAgreementStepsParams.addressId = this.leadHeaderData.addressId;
    this.store.dispatch(new LeadCreationStepperParamsCreateAction({ serviceAgreementStepsParams: this.serviceAgreementStepsParams }));
    if (contractType == ContractTypes.SERVICE_AGREEMENT) {
      let firstAgreementTabObj = response.resources.find(s => s['indexNo'] == 1);
      this.router.navigateByUrl(firstAgreementTabObj.navigationUrl);
    } else {
      if (this.leadHeaderData.fromUrl == 'Leads List') {
        this.router.navigateByUrl('/sales/task-management/leads/installation-agreement/insta-site-hazard');
      }
      else {
        this.router.navigateByUrl('/my-tasks/my-sales/my-leads/installation-agreement/insta-site-hazard');
      }
    }
  }

  onRedirectToCapturePaymentPage() {
    if (this.leadHeaderData.fromUrl == 'Leads List') {
      this.router.navigate(['/sales/task-management/leads/capture-payment']);
    }
    else {
      this.router.navigate(['/my-tasks/my-sales/my-leads/capture-payment']);
    }
  }

  onArrowClick() {
    if (this.leadHeaderData.fromUrl == 'Leads List') {
      this.router.navigate(['/sales/task-management/leads/lead-items/quotes-info']);
    }
    else {
      this.router.navigate(['/my-tasks/my-sales/my-leads/lead-items/quotes-info']);
    }
  }

  onSaleCompleted() {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Do you want to complete the sale?`).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) return;
        this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALE_COMPLETED, {
          leadId: this.leadHeaderData.leadId, modifiedUserId: this.loggedInUserData.userId
        }).subscribe((response) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.rxjsService.navigateToLeadListPage();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      });
  }

  qrCodeModalOPen(order) {
    this.selectedOrder = order;
  }

  sendEmail() {
    let obj = {
      saleOrderId: this.selectedOrder.saleOrderId
    }
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.INSTALLATION_FEES_EMAIL, obj, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        $('#qr_code_popup').modal('hide');
      }
    });
  }

  onSubmitToAcceptQuotation(type = 'submit') {
    if (type == 'cancel') {
      this.quotationSummary.find(q => q['quotationVersionId'] == this.selectedQuoteFromDropDown['quotationVersionId']).outcomeId = null;
    }
    else {
      let formDataPayload = new FormData();
      let payload = {
        quotationVersionId: this.acceptedQuote.quotationVersionId,
        modifiedUserId: this.loggedInUserData.userId,
        isAcceptTermsandConditions: true,
        callId: this.callIdNo,
        termsandConditionTypeId: this.acceptedQuote.monthlyFees ? ContractTypes.SERVICE_AGREEMENT : ContractTypes.INSTALLATION_AGREEMENTS
      };
      formDataPayload.append('payload', JSON.stringify(payload));
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_QUOTATION_OUTCOME_ACCEPT, formDataPayload).subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          $(this.terms_and_conditions_modal.nativeElement).modal('hide');
          this.getQuotations();
          this.getLeadHeaderDataByLeadId();
        }
      });
    }
  }
}
