import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
    ConfirmDialogPopupComponent, CrudService, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService
} from '@app/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
    selector: 'quotaion-follow-up-pop-up',
    templateUrl: './quotation-follow-up-pop-up.component.html'
})

export class QuotationFollowUpPopUpComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    loggedInUserData: LoggedInUserModel;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        private dialog: MatDialog,
        private crudService: CrudService,
        public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
        private store: Store<AppState>, private rxjsService: RxjsService) {
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
        this.combineLatestNgrxStoreData();
    }

    combineLatestNgrxStoreData() {
        combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
        });
    }

    onSubmit(): void {
        let payload = {
            quotationVersionId: this.data.quotationVersionId,
            modifiedUserId: this.loggedInUserData.userId,
        };
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_QUOTATION_OUTCOME_ACCEPT, payload).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.dialog.closeAll();
                this.outputData.emit(true);
            }
        });
    }

    dialogClose(): void {
        this.dialogRef.close(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }
}
