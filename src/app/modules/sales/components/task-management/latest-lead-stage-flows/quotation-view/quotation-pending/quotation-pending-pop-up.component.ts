import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, PendingQuotationModel, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
    ConfirmDialogPopupComponent, CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
    RxjsService, setRequiredValidator
} from '@app/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
    selector: 'quotaion-pending-pop-up',
    templateUrl: './quotation-pending-pop-up.component.html'
})

export class QuotationPendingPopUpComponent implements OnInit {
    pendingQuotationForm: FormGroup;
    loggedInUserData: LoggedInUserModel;
    probabilityOfClosures = [];
    contactDetails: any;
    conactError = false;
    @Output() outputData = new EventEmitter<any>();
    todayDate=new Date();

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        private dialog: MatDialog,
        private crudService: CrudService,
        public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
        private formBuilder: FormBuilder,
        private store: Store<AppState>, private rxjsService: RxjsService) {
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
        this.combineLatestNgrxStoreData();
        this.createQuotationForm();
        this.getProbabilityOfClosures();
        this.getContactNumbers();
    }

    combineLatestNgrxStoreData() {
        combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
        });
    }

    getProbabilityOfClosures() {
        this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.SALES_API_PROBABILITY_CLOSURE,
            undefined, false, null).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                    this.probabilityOfClosures = response.resources;
                }
                this.rxjsService.setPopupLoaderProperty(false);
            });
    }

    getContactNumbers() {
        this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.SALES_API_LEAD_CONTACTS,
            this.data.leadId, false, null).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                    this.contactDetails = response.resources;
                }
                this.rxjsService.setPopupLoaderProperty(false);
            });
    }

    radioChange(e, type) {
        this.pendingQuotationForm.controls['contactNumber'].setValue(e);
        this.pendingQuotationForm.controls['contactNumberCountryCode'].setValue(type);
    }

    createQuotationForm(): void {
        let pendingQuoatationModel = new PendingQuotationModel();
        this.pendingQuotationForm = this.formBuilder.group({});
        Object.keys(pendingQuoatationModel).forEach((key) => {
            this.pendingQuotationForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedInUserData.userId) :
                new FormControl(pendingQuoatationModel[key]));
        });
        this.pendingQuotationForm = setRequiredValidator(this.pendingQuotationForm, ["probabilityOfClosureId", "contactNumber", "contactNumberCountryCode", "callbackDateTime", "comments"]);
    }

    onSubmit(): void {
        this.pendingQuotationForm.value.leadId = this.data.leadId;
        this.conactError = (this.pendingQuotationForm.controls['contactNumber'].status === 'INVALID') ? true : false;
        if (this.pendingQuotationForm.invalid) return;
        this.pendingQuotationForm.value.quotationVersionId = this.data.quotationVersionId;
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_QUOTATION_OUTCOME_PENDING, this.pendingQuotationForm.value).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.dialog.closeAll();
                this.pendingQuotationForm.reset();
                this.outputData.emit(true);
            }
        });
    }

    dialogClose(): void {
        this.dialogRef.close(false);
        this.outputData.emit(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }
}