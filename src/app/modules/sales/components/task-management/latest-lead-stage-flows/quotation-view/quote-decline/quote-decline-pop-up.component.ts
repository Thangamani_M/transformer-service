import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
    ConfirmDialogPopupComponent, CrudService, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
    RxjsService
} from '@app/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
    selector: 'quote-decline-pop-up',
    templateUrl: './quote-decline-pop-up.component.html'
})

export class QuoteDeclinePopUpComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    formConfigs = formConfigs;
    declinedReasonAddEditForm: FormGroup;
    loggedInUserData: LoggedInUserModel;
    outComeReasons = [];

    constructor(@Inject(MAT_DIALOG_DATA) public data,
        private dialog: MatDialog,
        private crudService: CrudService,
        public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
        public declineDialogRef: MatDialogRef<QuoteDeclinePopUpComponent>,
        private formBuilder: FormBuilder,
        private store: Store<AppState>, private rxjsService: RxjsService) {
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
        this.combineLatestNgrxStoreData();
        this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.SALES_API_UX_OUTCOME_REASON,
            this.data.selectedOutcomeId, false, null).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200 && response.resources) {
                    this.outComeReasons = response.resources;
                }
                this.rxjsService.setPopupLoaderProperty(false);
            });
        this.declinedReasonAddEditForm = this.formBuilder.group({
            outcomeReasonId: ['', Validators.required],
            comments: ['']
        });
    }

    combineLatestNgrxStoreData() {
        combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
        });
    }

    onSubmit(): void {
        if (this.declinedReasonAddEditForm.invalid) return;
        this.declinedReasonAddEditForm.value.outcomeId = this.data.selectedOutcomeId;
        this.declinedReasonAddEditForm.value.createdUserId = this.loggedInUserData.userId;
        this.declinedReasonAddEditForm.value.quotationVersionId = this.data.quotationVersionId;
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_QUOTATION_OUTCOME_DECLINE, this.declinedReasonAddEditForm.value, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.declineDialogRef.close({ isSubmitted: true });
                this.dialog.closeAll();
                this.outputData.emit(true);
                this.declinedReasonAddEditForm.reset();
            }
        });
    }

    dialogClose(): void {
        this.dialogRef.close(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }
}
