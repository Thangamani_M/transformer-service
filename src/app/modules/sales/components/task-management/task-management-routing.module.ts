import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services/authguards';
import {
  AdjustCostInfoComponent, CapturePaymentComponent, LatestInstallableItemInfoComponent, LatestQuotesInfoComponent, LatestServiceInfoComponent, LeadInfoComponent,
  LeadInfoViewComponent, LeadsListComponent, LeadSupportDocComponent,
  OtherDocumentAddEditUploadComponent, PartitionInformationComponent,
  UpdatePartitionInformationComponent, ViewPartitionInformationComponent
} from '@sales/components/task-management';
import { AppointmentHistoryComponent } from '../lead-notes/appointment-history.component';
import { MarketingPreferenceInfoComponent } from './latest-lead-stage-flows/marketing-preference-info/marketing-preference-info.component';
import { QuotationViewComponent } from './latest-lead-stage-flows/quotation-view/quotation-view.component';

const taskMangementModuleRoutes: Routes = [
  {
    path: 'partition-search', children: [
      { path: '', component: PartitionInformationComponent, data: { title: 'Search Partition' }, canActivate: [AuthGuard] },
      { path: 'view', component: ViewPartitionInformationComponent, data: { title: 'View Partition' }, canActivate: [AuthGuard] },
      { path: 'update', component: UpdatePartitionInformationComponent, data: { title: 'Update Partition' }, canActivate: [AuthGuard] }]
  },
  {
    path: 'leads',
    children: [
      { path: '', component: LeadsListComponent, data: { title: 'Leads List' }, canActivate: [AuthGuard] },
      { path: 'lead-info/add-edit', component: LeadInfoComponent, data: { title: 'Lead Info Add Edit' }, canActivate: [AuthGuard] },
      { path: 'lead-info/view', component: LeadInfoViewComponent, data: { title: 'Lead Info View' }, canActivate: [AuthGuard] },
      { path: 'lead-services/add-edit', component: LatestServiceInfoComponent, data: { title: 'Lead Services View' }, canActivate: [AuthGuard] },
      { path: 'lead-items/add-edit', component: LatestInstallableItemInfoComponent, data: { title: 'Lead Items View' }, canActivate: [AuthGuard] },
      { path: 'quotation/view', component: QuotationViewComponent, data: { title: 'Lead Quotation view' }, canActivate: [AuthGuard] },
      { path: 'marketing-preference', component: MarketingPreferenceInfoComponent, data: { title: 'Marketing Preference' }, canActivate: [AuthGuard] },
      { path: 'capture-payment', component: CapturePaymentComponent, data: { title: 'Capture Payment' }, canActivate: [AuthGuard] },
      { path: 'lead-items/quotes-info', component: LatestQuotesInfoComponent, data: { title: 'Lead Items View' }, canActivate: [AuthGuard] },
      { path: 'lead-quotations/adjust-info', component: AdjustCostInfoComponent, data: { title: 'Lead Items View' }, canActivate: [AuthGuard] },
      { path: 'appointment-history', component: AppointmentHistoryComponent, data: { title: 'Appointment History' }, canActivate: [AuthGuard] },
      { path: 'sales-support-documents', component: LeadSupportDocComponent, data: { title: 'Sales Support Document' }, canActivate: [AuthGuard] },
      {
        path: 'other-documents', loadChildren: () => import('../../../../shared/components/other-documents/other-documents-list.module').then(m => m.OtherDocumentsListModule)
      },
      { path: 'other-documents/add-edit', component: OtherDocumentAddEditUploadComponent, data: { title: 'Other Document' }, canActivate: [AuthGuard] }
    ]
  },
  // for my tasks path
  { path: 'lead-info/add-edit', component: LeadInfoComponent, data: { title: 'Lead Info Add Edit' }, canActivate: [AuthGuard] },
  { path: 'lead-info/view', component: LeadInfoViewComponent, data: { title: 'Lead Info View' }, canActivate: [AuthGuard] },
  { path: 'lead-services/add-edit', component: LatestServiceInfoComponent, data: { title: 'Lead Services View' }, canActivate: [AuthGuard] },
  { path: 'lead-items/add-edit', component: LatestInstallableItemInfoComponent, data: { title: 'Lead Items View' }, canActivate: [AuthGuard] },
  { path: 'quotation/view', component: QuotationViewComponent, data: { title: 'Lead Quotation view' }, canActivate: [AuthGuard] },
  { path: 'capture-payment', component: CapturePaymentComponent, data: { title: 'Capture Payment' }, canActivate: [AuthGuard] },
  { path: 'lead-items/quotes-info', component: LatestQuotesInfoComponent, data: { title: 'Lead Items View' }, canActivate: [AuthGuard] },
  { path: 'lead-quotations/adjust-info', component: AdjustCostInfoComponent, data: { title: 'Lead Items View' }, canActivate: [AuthGuard] },
  { path: 'appointment-history', component: AppointmentHistoryComponent, data: { title: 'Appointment History' }, canActivate: [AuthGuard] },
  { path: 'sales-support-documents', component: LeadSupportDocComponent, data: { title: 'Sales Support Document' }, canActivate: [AuthGuard] },
  {
    path: 'other-documents', loadChildren: () => import('../../../../shared/components/other-documents/other-documents-list.module').then(m => m.OtherDocumentsListModule)
  },
  { path: 'other-documents/add-edit', component: OtherDocumentAddEditUploadComponent, data: { title: 'Other Document' }, canActivate: [AuthGuard] },
  { path: 'marketing-preference', component: MarketingPreferenceInfoComponent, data: { title: 'Marketing Preference' }, canActivate: [AuthGuard] },
  // end for my tasks path
  // for my tasks path
  { path: 'marketing-preference', component: MarketingPreferenceInfoComponent, data: { title: 'Marketing Preference' }, canActivate: [AuthGuard] },
  {
    path: 'service-agreement', loadChildren: () => import('./service-installation-agreement/service-sales-installation-agreement.module').then(m => m.ServiceSalesInstallationAgreementModule)
  },
  {
    path: 'installation-agreement', loadChildren: () => import('./service-installation-agreement/service-sales-installation-agreement.module').then(m => m.ServiceSalesInstallationAgreementModule)
  },
  // end for my tasks path
  {
    path: 'leads/service-agreement', loadChildren: () => import('./service-installation-agreement/service-sales-installation-agreement.module').then(m => m.ServiceSalesInstallationAgreementModule)
  },
  {
    path: 'leads/installation-agreement', loadChildren: () => import('./service-installation-agreement/service-sales-installation-agreement.module').then(m => m.ServiceSalesInstallationAgreementModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(taskMangementModuleRoutes)],

})

export class TaskManagementRoutingModule { }
