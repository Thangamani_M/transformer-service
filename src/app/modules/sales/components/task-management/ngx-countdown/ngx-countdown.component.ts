import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { timer } from 'rxjs';

enum showTime {
  HOURS = 'HOURS',
  MINUTES = 'MINUTES',
  SECONDS = 'SECONDS',
}
@Component({
  selector: 'ngx-countdown',
  templateUrl: './ngx-countdown.component.html'
})
export class NgxCountdownComponent implements OnInit {
  @Input() interval: number;
  @Input() intervalTime: number;
  @Input() showTimeType=showTime.HOURS;
  @Input() countDownSuffix: string;
  @Output() onComplete = new EventEmitter();
  @Output() change = new EventEmitter();

  public countdown: string;
  private completed: boolean;

  constructor() { }

  ngOnInit() {
    this.countdown = this.getTime();
    timer(1000, 1000).subscribe(val => {
      this.manipulateInterval();
      this.countdown = this.getTime();
      if (this.interval === 0) {
        this.countdownCompleted();
      }
    });
  }

  private getTime(): string {
    if (this.interval < 0) {
      this.interval = Math.abs(this.interval);
      this.completed = true;
    }
    const hours = Math.floor(this.interval / 3600);
    const minutes = Math.floor((this.interval - (hours * 3600)) / 60);
    const seconds = (this.interval - (hours * 3600) - (minutes * 60));
    switch (this.showTimeType) {
      case showTime.SECONDS:
        return this.interval.toString();
      default:
        return `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
    }
  }

  private manipulateInterval() {
    if (this.completed) {
      this.interval++;
    } else {
      this.interval--;
    }
    if (this.intervalTime == 0) {
      this.onComplete.emit();
    }
    this.intervalTime--;
  }

  countdownCompleted() {
    this.completed = true;
    this.onComplete.emit();
    this.countdown = this.getTime();
  }
}
