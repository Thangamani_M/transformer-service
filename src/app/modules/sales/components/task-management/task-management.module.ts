import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import {  MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import {
  AdjustCostInfoComponent,
  BalanceOfContractLeadStageTwoPopupComponent,
  CapturePaymentComponent, LatestInstallableItemInfoComponent, LatestQuotesInfoComponent, LatestServiceInfoComponent, LeadCreationEffects,
  leadCreationStepperParamsReducer, leadCreationUserDataReducer, leadHeaderDataReducer, LeadInfoComponent,
  LeadInfoViewComponent,
  LeadsListComponent,
  LeadSupportDocComponent,
  LeadTaskListTaskComponent,
  NoSalesPopUpComponent,
  OtherDocumentAddEditUploadComponent, PartitionInformationComponent,
  PartitionPopupComponent,
  QuotationFollowUpPopUpComponent,
  QuotationPendingPopUpComponent,
  QuotationViewComponent,
  QuoteDeclinePopUpComponent,
  ServicePopupComponent,
  TaskManagementRoutingModule, TaskMgmtPartitionNameModalComponent,
  UpdatePartitionInformationComponent, ViewPartitionInformationComponent
} from '@sales/components/task-management';
import { GaugeChartModule } from 'angular-gauge-chart';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { RatingModule } from 'primeng/rating';
import { SalesModule } from '../../sales.module';
import { LeadLifecycleReusableComponentsModule } from '../lead-header/lead-lifecycle-reusable-components.module';
import { UpdateStockModalComponent } from '../lead-notes';
import { AppointmentEscalationPopupComponent } from '../lead-notes/appointment-escalation/appointment-escalation-pop-up.component';
import { AppointmentHistoryComponent } from '../lead-notes/appointment-history.component';
import { MarketingPreferenceInfoComponent } from './latest-lead-stage-flows/marketing-preference-info/marketing-preference-info.component';
import { NewAddressModule } from './lead-info/new-address/new-address.module';
@NgModule({
  declarations: [AppointmentHistoryComponent, AdjustCostInfoComponent,
    LeadInfoComponent, LatestQuotesInfoComponent,
    TaskMgmtPartitionNameModalComponent,
    AppointmentEscalationPopupComponent,
    LeadsListComponent,
    LeadSupportDocComponent,
    OtherDocumentAddEditUploadComponent,
    PartitionInformationComponent, UpdatePartitionInformationComponent, ViewPartitionInformationComponent, TaskMgmtPartitionNameModalComponent,
    LeadInfoViewComponent, LatestServiceInfoComponent, QuotationViewComponent, CapturePaymentComponent, LatestInstallableItemInfoComponent, ServicePopupComponent,
    PartitionPopupComponent, QuotationPendingPopUpComponent, QuotationFollowUpPopUpComponent, NoSalesPopUpComponent,
    QuoteDeclinePopUpComponent, LeadTaskListTaskComponent, BalanceOfContractLeadStageTwoPopupComponent,
    MarketingPreferenceInfoComponent, UpdateStockModalComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, TaskManagementRoutingModule, MatNativeDatetimeModule,
    MaterialModule, OwlDateTimeModule, RatingModule,
    OwlNativeDateTimeModule,
    StoreModule.forFeature('leadCreationUserData', leadCreationUserDataReducer),
    StoreModule.forFeature('leadCreationStepperParams', leadCreationStepperParamsReducer),
    StoreModule.forFeature('leadHeaderData', leadHeaderDataReducer),
    EffectsModule.forFeature([LeadCreationEffects]),
     GaugeChartModule, SalesModule, NewAddressModule,
    LeadLifecycleReusableComponentsModule
  ],
  exports: [],
  providers: [],
  entryComponents: [TaskMgmtPartitionNameModalComponent, ServicePopupComponent,
    PartitionPopupComponent, AppointmentHistoryComponent, QuotationPendingPopUpComponent, QuotationFollowUpPopUpComponent,
    NoSalesPopUpComponent, QuoteDeclinePopUpComponent, BalanceOfContractLeadStageTwoPopupComponent, AppointmentEscalationPopupComponent, UpdateStockModalComponent]
})
export class TaskManagementModule { }
