import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatMenuItem } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, ExtensionModalComponent } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { EnableDisable } from '@app/shared/models';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { CrudType, debounceTimeForSearchkeyword, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { loggedInUserData, selectStaticEagerLoadingLeadGroupsState$, selectStaticEagerLoadingSiteTypesState$ } from '@modules/others';
import { LeadCreationUserDataCreateAction } from '@modules/sales';
import { LeadCreationUserDataModel, Tasklead, TaskRawlead } from '@modules/sales/models';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { select, Store } from '@ngrx/store';
import { AddRawLeadsNotesModalComponent, ChangeStatusModalComponent } from '@sales/components/raw-lead';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
@Component({
  selector: 'app-leadss-task-list',
  templateUrl: './lead-task-list.component.html',
  styleUrls: ['./lead-task-list.component.scss']
})
export class LeadTaskListTaskComponent implements OnInit {
  taskrawlead: TaskRawlead[];
  tasklead: Tasklead[];
  selection = new SelectionModel<TaskRawlead>(true, []);
  enableDisable = new EnableDisable();
  applicationResponse: IApplicationResponse;
  errorMessage: string;
  limit: number = 25;
  skip: number = 0;
  rawLeadstatus: any;
  totalLength; totalLengthCalls;
  totalLengthLeads;
  pageIndex: string;
  SearchText: any = { search: '', userId: '' }
  SearchTextCalls = {}
  ids: string[] = [];
  param: any = { email: '', phoneNumber: '' };
  customerId: string = '';
  rating: number = 3;
  starCount: number = 5;
  queryParams: any;
  agentExtensionNo: any;
  clickOnContact = false;
  dataSourceData = false;
  leadstatus: any;
  tasksObservable;
  selectedTabIndex = 0;
  observableResponse: any;
  otherParams = {};
  onRowClick: any;
  onSearchInputChange: any;
  pageSize: number = 10;
  dataList: any
  deletConfirm: boolean = false
  addConfirm: boolean = false
  loading: boolean = false;
  public bradCrum: MatMenuItem[];
  status: any = [];
  selectedRows: string[] = [];
  selectedRow: any;
  selectedColumns: any[];
  totalRecords: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  columnFilterForm: FormGroup;
  row: any = {}
  searchColumns: any;
  searchForm: FormGroup;
  siteTypes: any;
  group: any;
  loggedInUserData: LoggedInUserModel;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  primengTableConfigProperties: any = {
    tableCaption: "My Tasks ",
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Sales Configuration' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'My Raw Leads',
          dataKey: 'rawLeadId',
          enableBreadCrumb: true,
          enableReset: false,
          enbableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'rawLeadRefNo', header: 'Raw Lead ID', width: '200px' },
          { field: 'client', header: 'Client', width: '200px' },
          { field: 'email', header: 'Email Address', width: '300px' },
          { field: 'phoneNumber', header: 'Phone Number', width: '200px' },
          { field: 'sourceCode', header: 'Source Code', width: '200px' },
          { field: 'suburb', header: 'Suburb ', width: '200px' },
          { field: 'callAttempts', header: 'Call Attempts', width: '200px' },
          { field: 'siteType', header: 'Site Type', width: '300px' },
          { field: 'loadDate', header: 'Load Date', width: '200px' },
          { field: 'iconClass', header: 'Notes', width: '200px' },
          { field: 'status', header: 'Status', width: '200px' },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_LEAD_CATEGORIES,
          moduleName: ModulesBasedApiSuffix.SALES,
        },
        {
          caption: 'My Leads',
          dataKey: 'leadId',
          enbableAddActionBtn: true,
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'leadRefNo', header: 'Lead Number' },
          { field: 'clientName', header: 'Name' },
          { field: 'suburbName', header: 'Suburb' },
          { field: 'leadGroupName', header: 'Lead Group' },
          { field: 'siteTypeName', header: 'Site Type' },
          { field: 'callDateTime', header: 'Call Time' },
          { field: 'leadStatusName', header: 'Status' }],
          shouldShowDeleteActionBtn: false,
          areCheckboxesRequired: false,
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_LEADS_MY_LEADS,
          moduleName: ModulesBasedApiSuffix.SALES,
        },
        {
          caption: 'My Quote Follow-Up',
          dataKey: 'leadCallbackId',
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enbableAddActionBtn: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableFieldsSearch: true,
          enableHyperLink: false,
          columns: [{ field: 'leadCallbackRefNo', header: 'FollowUp ID' },
          { field: 'leadNumber', header: 'Lead Number' },
          { field: 'quotationNumber', header: 'Quote Number' },
          { field: 'probabilityOfClosureName', header: 'Probability of Closure', width: "250px" },
          { field: 'timeOfDeal', header: 'Time of Deal' },
          { field: 'callbackDateTime', header: 'Follow-up Date and Time', width: "250px" },
          { field: 'notes', header: 'Notes' }],
          shouldShowDeleteActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_QUOTE_FOLLOW_UPS,
          moduleName: ModulesBasedApiSuffix.SALES,
        },
        {
          caption: 'My Stop And Knock',
          dataKey: 'stopAndKnockId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          enableSecondHyperLink: false,
          cursorLinkIndex: 0,
          cursorSecondLinkIndex: 3,
          columns: [{ field: 'stopAndKnockRefNo', header: 'SK ID' },
          { field: 'suburbName', header: 'Suburb' },
          { field: 'cityName', header: 'City' },
          { field: 'reconnectionArea', header: 'Reconn Area', width: '250px' },
          { field: 'agent', header: 'Agent' },
          { field: 'agentFollowupDate', header: 'Agent Followup Date' },
          { field: 'seller', header: 'Seller' },
          { field: 'sellerFollowupDate', header: 'Seller Followup Date' },
          { field: 'contactPersonType', header: 'Contact Person Type' },
          { field: 'mobileNo1', header: 'MobileNo1' },
          { field: 'createdDate', header: 'Created Date' }
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.STOP_KNOCK,
          moduleName: ModulesBasedApiSuffix.SALES
        },
        {
          caption: 'DOA Dashboard',
          dataKey: 'doaApprovalId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          enableSecondHyperLink: false,
          cursorLinkIndex: 0,
          cursorSecondLinkIndex: 3,
          columns: [{ field: 'requestNumber', header: 'Request Nr' },
          { field: 'requestDate', header: 'Req Date' },
          { field: 'requestor', header: 'Requestor' },
          { field: 'requestorRole', header: 'Req Role' },
          { field: 'requestType', header: 'Req Type' },
          { field: 'customerName', header: 'Customer Name' },
          { field: 'suburbName', header: 'Suburb' },
          { field: 'approvalValue', header: 'Approval Value' },
          { field: 'reference', header: 'Reference' },

          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.DOA_DASHBOARD_LIST,
          moduleName: ModulesBasedApiSuffix.SALES
        }
      ]
    }
  }

  constructor(private httpService: CrudService, private tableFilterFormService: TableFilterFormService,
    private dialog: MatDialog, private activatedRoute: ActivatedRoute, private crudService: CrudService,
    private rxjsService: RxjsService, private _fb: FormBuilder, private momentService: MomentService,
    private snackbarService: SnackbarService, private router: Router, private store: Store<AppState>) {
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.activatedRoute.queryParams.subscribe(params => {
      if (Object.keys(params).length > 0) {
        this.selectedTabIndex = +params.tab
        this.onCRUDRequested(CrudType.GET, {});
      }
    });
    this.combineLatestNgrxStoreData();
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
    if (this.selectedTabIndex == 1) {
      this.getleadStatuses();
    } else if (this.selectedTabIndex == 0) {
      this.getrawStatuses();
    }
    let data = null;
    this.rxjsService.setCustomerContactNumber(data);
    this.prepareTabBasedServerRequests(this.row, this.SearchTextCalls);
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingLeadGroupsState$),
      this.store.select(selectStaticEagerLoadingSiteTypesState$),
    ]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.group = response[1];
      this.siteTypes = response[2];
    });
  }

  checkIfCustomerDetails(customerNumber, details) {
    if (!customerNumber && details.length != 0) {
      return true;
    } else {
      return false;
    }
  }

  getrawStatuses() {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_RAW_LEAD_STATUSES, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        this.status = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getleadStatuses() {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LEAD_STATUSES, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        this.status = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSiteTypes() {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_SITE_TYPES, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        this.siteTypes = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getLeadGroup() {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALEA_API_GROUPS, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        this.group = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  loadPaginationLazy(event) {
    this.otherParams = {};
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }



  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.otherParams = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  private gettaskrawlead(pageIndex?, pageSize?, params?) {
    this.httpService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_MY_RAW_LEADS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        ...params
      }), 1
    ).subscribe(resp => {
      resp.resources.forEach((resp) => {
        resp['iconClass'] = 'icon icon-inventory';
      });
      this.observableResponse = resp;
      this.dataList = this.observableResponse.resources;
      this.totalRecords = resp.totalCount;
      this.rxjsService.setGlobalLoaderProperty(false);
    });

  }

  onCRUDRequested(type: CrudType | string, row?: object, cursorLinkIndex = 0, field = null): void {
    if (field) {
      switch (field) {
        case "leadCallbackRefNo":

          break;
        case "leadNumber":
          this.router.navigate(['/sales/task-management/leads/lead-info/view'], { queryParams: { leadId: row['leadId'] } });
          break;
        case "quotationNumber":
          window.open(row['documentPath'], '_blank');
          break;
      }
    }
    else {
      switch (type) {
        case CrudType.CREATE:
          this.openAddEditPage(CrudType.CREATE, row);
          break;
        case CrudType.GET:
          if (this.otherParams) {
            Object.keys(this.otherParams).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                this.otherParams[key] = this.momentService.localToUTC(this.otherParams[key]);
              } else {
                this.otherParams[key] = this.otherParams[key];
              }
            });
          }
          if (Object.keys(this.row).length > 0) {
            if (this.row['searchColumns']) {
              Object.keys(this.row['searchColumns']).forEach((key) => {
                if (key == 'leadStatusName' || key == 'status') {
                  let status = row['searchColumns'][key]['value'];
                  if (key == 'status') {
                    key = 'rawLeadStatusId';
                  } else {
                    key = 'LeadStatusId';
                  }
                  this.otherParams[key] = status.id;
                } else if (key == 'siteType' || key == 'siteTypeName') {
                  let siteType = row['searchColumns'][key]['value'];
                  key = 'siteTypeId';
                  this.otherParams[key] = siteType.id;
                } else if (key == 'leadGroupName') {
                  let siteType = row['searchColumns'][key]['value'];
                  key = 'LeadGroupId';
                  this.otherParams[key] = siteType.id;
                }
                else {
                  this.otherParams[key] = row['searchColumns'][key]['value'];
                }
              });
            }
            if (this.row['sortOrderColumn']) {
              this.otherParams['sortOrder'] = this.row['sortOrder'];
              this.otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
            }
          }
          this.prepareTabBasedServerRequests(row, this.otherParams);
          break;
        case CrudType.EDIT:
          this.openAddEditPage(CrudType.EDIT, row, cursorLinkIndex);
          break;
        case CrudType.ICON_POPUP:
          this.addNotes(row);
          break;
        case CrudType.STATUS_POPUP:
          this.changeStatus(row);
          break;
      }
    }
  }

  prepareTabBasedServerRequests(rowObj?, otherParamsObj?) {
    let row = rowObj ? rowObj : this.row;
    let otherParams = otherParamsObj ? otherParamsObj : this.otherParams;
    otherParams['userId'] = this.loggedInUserData.userId;
    switch (this.selectedTabIndex) {
      case 0:
        this.gettaskrawlead(row["pageIndex"], row["pageSize"], otherParams);
        break;
      case 1:
        this.gettasklead(row["pageIndex"], row["pageSize"], otherParams);
        break;
      case 2:
        otherParams.createdUserId = this.loggedInUserData.userId;
        this.gettaskQuoteFollowUp(row["pageIndex"], row["pageSize"], otherParams);
        break;
      case 3:
        this.getStopAndKnocks(row["pageIndex"], row["pageSize"], otherParams);
        break;
      case 4:

        this.getDoaDashboardList(row["pageIndex"], row["pageSize"], otherParams);


        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, cursorLinkIndex = 0): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['/sales/raw-lead/add-edit']);
            break;
          case 1:
            this.router.navigate(['/sales/task-management/leads/lead-info/add-edit']);
            break;
        }
        break;
      case CrudType.EDIT:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['/sales/task-management/leads/lead-info/add-edit'], { queryParams: { id: editableObject['rawLeadId'] } });
            break;
          case 1:
            this.router.navigate(['/sales/task-management/tasks/lead-info/view'], { queryParams: { leadId: editableObject['leadId'], selectedIndex: 1 } });
            this.rxjsService.setFromUrl("My Leads List");
            break;
          case 3:
            this.router.navigate(['/sales/stop-and-knock/add-edit'], { queryParams: { stopAndKnockId: editableObject['stopAndKnockId'] } })
            this.rxjsService.setFromUrl("My Leads List");
            break;
          case 4:
            this.router.navigate(['/sales/task-management/doa-dashboard-details'], { queryParams: { id: editableObject['doaApprovalId'] } })

            break;
        }
        break;
    }
  }

  gettasklead(pageIndex?, pageSize?, otherParams?: object) {
    this.httpService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LEADS_MY_LEADS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        ...otherParams
      }), 1
    ).subscribe(resp => {
      this.observableResponse = resp;
      this.dataList = this.observableResponse.resources;
      this.totalRecords = resp.totalCount;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  gettaskQuoteFollowUp(pageIndex?, pageSize?, otherParams?: object) {
    let params: any
    params = otherParams;
    this.httpService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_QUOTE_FOLLOW_UPS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        ...otherParams
      }), 1
    ).subscribe(resp => {
      this.observableResponse = resp;
      this.dataList = this.observableResponse.resources;
      this.totalRecords = resp.totalCount;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getStopAndKnocks(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.STOP_KNOCK,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response;
        this.dataList = this.observableResponse.resources;
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getDoaDashboardList(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    // otherParams['userId'] = "AE7F85D9-D2B8-4121-A851-078D25D527FC";
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.DOA_DASHBOARD_LIST,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response;
        this.dataList = this.observableResponse.resources;
        this.totalRecords = response.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onTabChange(e): void {
    this.selectedTabIndex = e.index;
    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[e.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.dataList = null;
    if (this.selectedTabIndex == 0) {
      this.getrawStatuses();
    }
    else if (this.selectedTabIndex == 1) {
      this.store.dispatch(new LeadCreationUserDataCreateAction({ leadCreationUserDataModel: new LeadCreationUserDataModel() }));
      this.getleadStatuses();
    }
    setTimeout(() => {
      this.prepareTabBasedServerRequests(this.row, this.SearchTextCalls);
    })
  };

  addNotes(data) {
    if (this.selectedTabIndex == 0) {
      this.dialog.open(AddRawLeadsNotesModalComponent, { width: '700px', disableClose: false, data })
    }
  }

  changeStatus(data) {
    if (data.rawLeadStatusName) {
      const dialogReff = this.dialog.open(ChangeStatusModalComponent, { width: '700px', disableClose: false, data });
      dialogReff.afterClosed().subscribe(result => {
        if (result) return;
        this.gettaskrawlead(this.SearchTextCalls);
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  contactNumber(contact) {
    if (this.selectedTabIndex == 0) {
      if (!this.agentExtensionNo) {
        this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
        this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
      } else {
        let data = {
          customerContactNumber: contact.phoneNumber,
          rawLeadId: contact.rawLeadId,
          clientName: contact.clientName
        }
        this.rxjsService.setCustomerContactNumber(data);
        this.rxjsService.setExpandOpenScape(true);
      }
    }
  }
}