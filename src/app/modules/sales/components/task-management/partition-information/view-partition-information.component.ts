import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { PartitionSearch } from '@modules/sales/models/partition-search';
import { SalesModuleApiSuffixModels, Sales_Leads_Components } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-partition-search-info-view',
  templateUrl: './view-partition-information.component.html',
  styleUrls: ['./partition-information.component.scss']
})
export class ViewPartitionInformationComponent implements OnInit {
  primaryLeadId = "";
  partitionSearch: PartitionSearch;
  secondaryPartitions = [];
  liablePartitions = [];
  componentPermissions = [];

  constructor(private activatedRoute: ActivatedRoute,
    private crudservice: CrudService,
    private router: Router,
    private rxjsService: RxjsService, private store: Store<AppState>, private snackbarService: SnackbarService
  ) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.combineLatestPageLevelAccessData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.activatedRoute.queryParams]).pipe(take(1))
      .subscribe((response) => {
        this.primaryLeadId = response[0].id;
        this.getPartitionPrimaryLeadId(this.primaryLeadId);
      });
  }

  combineLatestPageLevelAccessData(): void {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      if (response[0][Sales_Leads_Components.PARTITION_SEARCH]) {
        this.componentPermissions = response[0][Sales_Leads_Components.PARTITION_SEARCH];
      }
    });
  }

  getPartitionPrimaryLeadId(primaryLeadId: string) {
    this.crudservice.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.PARTITIONS, primaryLeadId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.partitionSearch = new PartitionSearch(response.resources);
          this.liablePartitions = response.resources.liablePartitions && response.resources.liablePartitions;
          this.secondaryPartitions = response.resources.secondaryPartitions;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  navigateToEdit() {
    if (!this.componentPermissions.find(cF => cF.menuName === PermissionTypes.EDIT)) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    else if (this.componentPermissions.find(cF => cF.menuName === PermissionTypes.EDIT)) {
      this.router.navigate(["sales/task-management/partition-search/update"], { queryParams: { id: this.primaryLeadId } });
    }
  }

  redirectToListPage(): void {
    this.router.navigateByUrl("sales/task-management/partition-search");
  }
}
