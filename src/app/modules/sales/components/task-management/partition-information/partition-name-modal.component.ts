import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-task-mgmt-partition-search-modal',
  templateUrl: './partition-name-modal.component.html',
  styleUrls: ['./partition-information.component.scss']
})

export class TaskMgmtPartitionNameModalComponent implements OnInit {
  partitionData;
  @Output() outputData = new EventEmitter<any>();
  @Output() selectedItemCode = new EventEmitter<any>();
  partitionNameModalForm: FormGroup;
  insertData;
  formValue;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private dialog: MatDialog,
    private crudservice: CrudService, private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService) { }

  ngOnInit(): void {
    this.partitionNameModalForm = this.formBuilder.group({
      createdUserId: this.data.createdUserId,
      primaryLeadId: this.data.primaryLeadId,
      partitionName: ['', Validators.required],
    });
    this.partitionData = this.data;
  }

  goToPartitionSearch() {
    this.formValue = this.partitionNameModalForm.value;
    this.rxjsService.setDialogOpenProperty(true);
    this.getPartitionInsert().subscribe((response: IApplicationResponse) => {
      if (response.resources && response.statusCode == 200 && response.isSuccess) {
        this.insertData = response.resources;
        this.dialog.closeAll();
        this.outputData.emit(this.insertData);
        if (this.insertData.isSuccess === true) {
          this.snackbarService.openSnackbar(this.insertData.message, ResponseMessageTypes.SUCCESS);
        }
        if (this.insertData.isSuccess === false) {
          this.snackbarService.openSnackbar(this.insertData.message, ResponseMessageTypes.WARNING);
        }
      }
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  getPartitionInsert(): Observable<IApplicationResponse> {
    if (this.partitionNameModalForm.invalid) {
      return;
    }
    return this.crudservice.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.PARTITION_INSERT, undefined, false, prepareRequiredHttpParams({
        createdUserId: this.formValue.createdUserId, primaryLeadId: this.formValue.primaryLeadId, partitionName: this.formValue.partitionName
      }));
  }

  ngOnDestroy(){
    this.rxjsService.setDialogOpenProperty(false);
  }
}