import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, filterFormControlNamesWhichHasValues, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { SalesModuleApiSuffixModels, Sales_Leads_Components } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-partition-search-info',
  templateUrl: './partition-information.component.html'
})

export class PartitionInformationComponent extends PrimeNgTableVariablesModel implements OnInit {
  searchPartitionForm: FormGroup;
  selectedOption;
  formConfigs = formConfigs;
  breadCrumb:BreadCrumbModel;
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;
  primengTableConfigProperties: any = {
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Advance Search List',
          dataKey: 'id',
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableAddActionBtn: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'partitionCode', header: 'Partition Code' },
          { field: 'customerNumber', header: 'Customer ID' },
          { field: 'customerName', header: 'Customer Name' },
          { field: 'address', header: 'Site Address' },
          { field: 'noOfPartition', header: 'Partition' }],
          apiSuffixModel: SalesModuleApiSuffixModels.PARTITIONS_SEARCH,
          moduleName: ModulesBasedApiSuffix.SALES
        }]
    }
  };
  isSearchBtnDisabled = true;
  isClearBtnDisabled = true;
  isSearchBtnPermissionDenied = false;

  constructor(private crudService: CrudService,
    private router: Router,private store: Store<AppState>,
    private formBuilder: FormBuilder,private snackbarService:SnackbarService,
    private rxjsService: RxjsService) {
    super();
    this.totalRecords = 0;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.createForm();
    this.onAddressFormControlValueChanges();
    this.searchPartitionForm.valueChanges
      .pipe(map((object) => filterFormControlNamesWhichHasValues(object)))
      .subscribe((keyValuesObj) => {
        this.isSearchBtnDisabled = (Object.keys(keyValuesObj).length == 0 || this.searchPartitionForm.invalid) ? true : false;
        this.isClearBtnDisabled = Object.keys(keyValuesObj).length > 0 ? false : true;
      });
      this.breadCrumb = {
        pageTitle: { key: "Search Partition" },items: [{ key: "Sales" },{ key: "Partition Search" }]};
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let componentPermissions = response[0][Sales_Leads_Components.PARTITION_SEARCH];
      if (componentPermissions) {
        this.isSearchBtnPermissionDenied = componentPermissions.find(cF => cF.menuName === PermissionTypes.FILTER) ? false : true;
      }
    });
  }

  createForm() {
    this.searchPartitionForm = this.formBuilder.group({
      address: [''],
      customerNumber: [''],
      customerName: [''],
      partitionCode: [''],
      latLang: ['']
    });
  }

  onAddressFormControlValueChanges(): void {
    this.searchPartitionForm
      .get("address")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((searchtext) => {
          this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS, null, true,
            prepareRequiredHttpParams({ searchtext, isAfrigisSearch: false }))
        });
  }

  onSelectedItemOption(selectedObject) {
    if (!selectedObject) return;
    this.selectedOption = selectedObject;
  }

  getPartitionsByKeywords(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    if(!this.isSearchBtnDisabled&&this.isSearchBtnPermissionDenied){
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    else{
      this.loading = true;
      let payload = {};
      if (this.selectedOption?.addressId) {
        this.searchPartitionForm.value.address = this.selectedOption.addressId;
      }
      payload = { ...this.searchPartitionForm.value, ...otherParams };
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.PARTITIONS_SEARCH, undefined, false,
        prepareGetRequestHttpParams(pageIndex, pageSize, payload)).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200 && response.resources) {
            this.dataList = response.resources
            this.totalRecords = response.totalCount;
            this.loading = false;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(row);
        break;
      case CrudType.GET:
        this.getPartitionsByKeywords(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        this.router.navigate(["sales/task-management/partition-search/view"], { queryParams: { id: row['primaryLeadId'] } });
        break;
    }
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  openAddEditPage(editableObject?: object | string): void {
    this.router.navigate(["sales/task-management/partition-search/view"], { queryParams: { id: editableObject['primaryLeadId'] } });
  }

  clearSearch() {
    this.searchPartitionForm.reset();
  }

  redirectToListPage(): void {
    this.router.navigateByUrl("sales/task-management/partition-search");
  }
}
