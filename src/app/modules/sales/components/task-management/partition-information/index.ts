export * from './partition-information.component';
export * from './update-partition-information.component';
export * from './view-partition-information.component';
export * from './partition-name-modal.component'