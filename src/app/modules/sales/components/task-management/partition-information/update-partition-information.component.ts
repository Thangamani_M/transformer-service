import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { PartitionSearch } from '@modules/sales/models/partition-search';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { TaskMgmtPartitionNameModalComponent } from './partition-name-modal.component';
@Component({
  selector: 'app-partition-search-add-edit',
  templateUrl: './update-partition-information.component.html',
  styleUrls: ['./partition-information.component.scss']
})
export class UpdatePartitionInformationComponent implements OnInit {
  primaryLeadId = "";
  partitionSearch: PartitionSearch;
  partitionForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  editedData = [];
  editedSecondaryData = [];
  secondaryPartitionData;
  liablePartitions = [];
  insertedData;
  editedSecondaryPartition;
  comparePartitionName;
  isItemCode = false;
  partitionLeadId = "";
  breadCrumb: BreadCrumbModel;

  constructor(
    private activatedRoute: ActivatedRoute,
    private crudservice: CrudService,
    private router: Router,
    private rjxService: RxjsService,
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createRewardCriteriaForm();
    this.breadCrumb = {
      pageTitle: { key: "Update Search Partition" }, items: [{ key: "Sales" }, { key: "Partition Search", routeUrl: "/sales/task-management/partition-search" },
      { key: 'View Partition', routeUrl: `/sales/task-management/partition-search/view?id=${this.primaryLeadId}` },
      { key: 'Update Partition' }]
    };
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.activatedRoute.queryParams]).pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.primaryLeadId = response[1].id;
        this.getPartitionPrimaryLeadId(this.primaryLeadId);
      });
  }

  createRewardCriteriaForm(): void {
    let partitionSearchModel = new PartitionSearch();
    this.partitionForm = this.formBuilder.group({});
    Object.keys(partitionSearchModel).forEach((key) => {
      this.partitionForm.addControl(key, new FormControl(partitionSearchModel[key]));
    });
    this.partitionForm = setRequiredValidator(this.partitionForm, ['partitionName']);
  }

  addSecondaryPartition() {
    this.rjxService.setDialogOpenProperty(true);
    let data = {
      createdUserId: this.loggedInUserData.userId,
      primaryLeadId: this.primaryLeadId
    }
    const dialogReff = this.dialog.open(TaskMgmtPartitionNameModalComponent, { width: '700px', disableClose: true, data: data });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (!ele) return;
      this.insertedData = ele;
      if (this.insertedData.isSuccess === true) {
        this.getPartitionPrimaryLeadId(this.primaryLeadId);
      }
    });
  }

  getPartitionPrimaryLeadId(primaryLeadId: string) {
    this.crudservice.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.PARTITIONS, primaryLeadId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.partitionSearch = response.resources;
          this.partitionLeadId = response.resources.partitionLeadId;
          this.comparePartitionName = this.partitionSearch && this.partitionSearch.partitionName;
          this.secondaryPartitionData = response.resources.secondaryPartitions ? response.resources.secondaryPartitions : [];
          this.liablePartitions = response.resources.liablePartitions ? response.resources.liablePartitions : [];
          this.secondaryPartitionData && this.secondaryPartitionData.forEach((key) => {
            key.isItemCode = false;
          });
          this.partitionForm.patchValue(response.resources);
        }
        this.rjxService.setGlobalLoaderProperty(false);
      });
  }

  partitionNameUpdate(e, event, i) {
    this.partitionForm.markAsDirty();
    if (!event.target.value) {
      this.isItemCode = true;
      this.secondaryPartitionData[i].isItemCode = true
    } else {
      this.secondaryPartitionData[i].isItemCode = false
      this.isItemCode = false
    }
    if (this.editedSecondaryData.find(x => x.partitionId === e.partitionId) != null) {
      this.editedSecondaryData.find(x => { x.partitionId === e.partitionId ? (x.partitionName = event.target.value) : (x.partitionName = x.partitionName) })
    }
    else {
      this.editedSecondaryPartition = {
        partitionId: e.partitionId,
        primaryLeadId: e.primaryLeadId,
        partitionLeadId: null,
        partitionCode: e.partitionCode,
        partition: e.partition,
        partitionName: event.target.value,
        createdUserId: this.loggedInUserData.userId,
        modifiedUserId: this.loggedInUserData.userId
      }
      this.editedSecondaryData.push(this.editedSecondaryPartition);
    }
  }

  onSubmit() {
    if (this.partitionForm.invalid) {
      return
    }
    let formValue = this.partitionForm.getRawValue();
    let primaryParams = {
      partitionId: formValue.partitionId,
      primaryLeadId: formValue.primaryLeadId,
      partitionLeadId: this.partitionLeadId ? this.partitionLeadId : null,
      partitionCode: formValue.partitionCode,
      partition: formValue.partition,
      partitionName: this.partitionForm.value.partitionName,
      createdUserId: this.loggedInUserData.userId,
      modifiedUserId: this.loggedInUserData.userId
    }
    if (this.comparePartitionName !== this.partitionForm.value.partitionName) {
      if (primaryParams.partitionName) {
        this.editedData.push(primaryParams);
      }
    }
    if (this.editedSecondaryData) {
      this.editedSecondaryData.forEach((key) => {
        if (!key.partitionName) {
          return
        }
        if (key.partitionName) {
          this.editedData.push(key)
        }
      });
    }
    if (this.editedData.length < 1) {
      return
    }
    this.editedData.forEach((key) => {
      if (key.partitionName !== '') {
      }
    });
    if (this.editedSecondaryData.filter(x => x.partitionName === '')?.length > 0) {
      return
    } else {
      this.crudservice.update(
        ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.PARTITIONS,
        this.editedData).subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.isSuccess) {
            this.redirectToListPage();
          }
        });
    }
  }

  enableDisablePartition(e, objecData) {
    const payload = {
      ids: objecData.partitionId,
      isActive: e.target.checked,
      modifiedUserId: this.loggedInUserData.userId
    }
    this.crudservice.update(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.PARTITIONS_ENABLED_DISABLED, payload)
      .subscribe(() => {
      });
  }

  redirectToListPage(): void {
    this.router.navigateByUrl("sales/task-management/partition-search");
  }

  redirectToViewPage(): void {
    this.router.navigateByUrl("sales/task-management/partition-search/view?id=" + this.primaryLeadId);
  }
}
