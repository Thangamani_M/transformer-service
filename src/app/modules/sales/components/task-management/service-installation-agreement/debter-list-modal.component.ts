import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { SalesModuleApiSuffixModels } from '@modules/sales';
@Component({
  selector: 'app-debter-list-modal',
  templateUrl: './debter-list-modal.component.html'
})
export class DebterListComponent {
  @Output() outputData1 = new EventEmitter<any>();
  selectedDebtor;

  constructor(private crudService: CrudService, private snackBarService: SnackbarService, @Inject(MAT_DIALOG_DATA)
  public existedDebtors, private dialog: MatDialog, private rxjsService: RxjsService) { }


  search(data) {
    if (data) {
      Object.keys(data).forEach((key) => (data[key] == null) && delete data[key]);
      data['isLinkDebtor'] = true;
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_SEARCH_DEBTER, undefined, false, prepareGetRequestHttpParams(null, null, data), 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          if (response.resources.length > 0) {
            this.snackBarService.openSnackbar("No Data Found", ResponseMessageTypes.SUCCESS);
          }
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
    } else {
      this.snackBarService.openSnackbar("Atleast One search criteria is required", ResponseMessageTypes.WARNING);
    }
  }

  select(i, existedDebtor) {
    this.selectedDebtor = existedDebtor;
    this.existedDebtors.forEach((element, index) => {
      element.isChecked = (index == i) ? true : false;
    });
  }

  linkDebtor() {
    this.dialog.closeAll();
    this.outputData1.emit(this.selectedDebtor);
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
