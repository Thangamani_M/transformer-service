import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel, CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, standardHeightForPscrollContainer
} from '@app/shared';
import {
  CreditVettingInfo, LeadHeaderData,
  SalesModuleApiSuffixModels, selectLeadCreationStepperParamsState$,
  selectLeadHeaderDataState$, ServiceAgreementStepsParams
} from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'credit-vetting-result',
  templateUrl: './credit-vetting-result.component.html',
  styleUrls: ['./service-installation-agreement.scss']
})

export class CreditVettingResultComponent implements OnInit {
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  leadHeaderData: LeadHeaderData;
  details:any={debtorInformation:{dob:""}};
  creditVettingInfoForm: FormGroup;
  IsInstallationDebtor: boolean = false;
  loggedInUserData: LoggedInUserModel;
  needleValue: Number = 0;
  bottomLabel;
  options = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 1000,
    arcColors: ["rgb(255,84,84)", "rgb(239,214,19)", "rgb(61,204,91)"],
    arcDelimiters: [35, 70],
    rangeLabel: ['0', '900'],
    needleStartValue: 10,
  }
  customerId = "";
  debtorId = "";
  debtorAccountDetailId = "";
  breadCrumb: BreadCrumbModel;
  uploadedFile: File;
  standardHeightForPscrollContainer=standardHeightForPscrollContainer;

  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private router: Router) {
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectLeadHeaderDataState$), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      this.leadHeaderData = response[1];
      if (response[2]) {
        this.customerId = response[2].customerId;
        this.debtorId = response[2].debtorId;
        this.debtorId = response[2].debtorId;
        this.debtorAccountDetailId = response[2].debtorAccountDetailId ? response[2].debtorAccountDetailId : '';
      }
    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.rxjsService.getIsInstallationDebtor().subscribe((IsInstallationDebtor: boolean) => {
      this.IsInstallationDebtor = IsInstallationDebtor;
      this.breadCrumb = {
        pageTitle: { key: 'Credit Vetting Result' },
        items: [{
          key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
        }, { key: this.IsInstallationDebtor ? 'Sales & Installation Agreement' : 'Service Agreement' }, { key: 'Credit Vetting Result' }]
      };
    });
    this.getDetails();
    this.createDebterForm();
    this.onFormControlChanges();
  }

  createDebterForm() {
    let debtorBankingDetailsModel = new CreditVettingInfo();
    this.creditVettingInfoForm = this.formBuilder.group({});
    Object.keys(debtorBankingDetailsModel).forEach((key) => {
      this.creditVettingInfoForm.addControl(key,
        new FormControl(debtorBankingDetailsModel[key]));
    });
  }

  onFormControlChanges() {
    this.creditVettingInfoForm.get('isPassportCust').valueChanges.subscribe((isPassportCust: boolean) => {
      if (!isPassportCust) {
        this.creditVettingInfoForm.get('passportNoCustomer').setValidators([Validators.required])
        this.creditVettingInfoForm.get('passportNoCustomer').updateValueAndValidity();
        this.creditVettingInfoForm.get('expiryCustDate').setValidators([Validators.required])
        this.creditVettingInfoForm.get('expiryCustDate').updateValueAndValidity();
        this.creditVettingInfoForm.get('expiryCustMonth').setValidators([Validators.required])
        this.creditVettingInfoForm.get('expiryCustMonth').updateValueAndValidity();
      } else {
        this.creditVettingInfoForm.get('passportNoCustomer').clearValidators();
        this.creditVettingInfoForm.get('passportNoCustomer').updateValueAndValidity();
        this.creditVettingInfoForm.get('expiryCustDate').clearValidators();
        this.creditVettingInfoForm.get('expiryCustDate').updateValueAndValidity();
        this.creditVettingInfoForm.get('expiryCustMonth').clearValidators();
        this.creditVettingInfoForm.get('expiryCustMonth').updateValueAndValidity();
      }
    });
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.CREDIT_RESULT, null, false,
      prepareGetRequestHttpParams(null, null,
        {
          customerId: this.customerId,
          debtorAccountDetailId: this.debtorAccountDetailId,
          debtorId: this.debtorId,
          quotationVersionId: this.serviceAgreementStepsParams.quotationVersionId
        }))
      .subscribe((response: IApplicationResponse) => {
        this.details = response.resources;
        if (this.details && this.details.debtorInformation) {
          if(this.details.debtorInformation.expiryDate){
            this.details.debtorInformation.expiryDate=new Date(this.details.debtorInformation.expiryDate);
          }
          if(this.details.debtorInformation.dob){
            this.details.debtorInformation.dob=new Date(this.details.debtorInformation.dob);
          }
          let rating = "";
          if (this.details.debtorInformation.isDynamicRating) {
            this.options.rangeLabel = ['0', '5'];
            rating = 'Dynamic';
            this.bottomLabel = rating + ' Score :' + this.details.debtorInformation.creditScore;
            this.needleValue = (parseFloat(this.details.debtorInformation.creditScore) / 5) * 100;
          }
          if (this.details.debtorInformation.isSME) {
            this.options.rangeLabel = ['0', '900'];
            rating = 'SME'
            this.bottomLabel = rating + ' Score :' + this.details.debtorInformation.creditScore;
            this.needleValue = (parseFloat(this.details.debtorInformation.creditScore) / 900) * 100;
          }
          if (!this.details.debtorInformation.isSME && !this.details.debtorInformation.isDynamicRating) {
            this.options.rangeLabel = ['0', '900'];
            this.bottomLabel = rating + ' Score :' + this.details.debtorInformation.creditScore;
            this.needleValue = (parseFloat(this.details.debtorInformation.creditScore) / 900) * 100;
          }
        }
        if (this.details && this.details.debtorBankingInformation && this.details.debtorBankingInformation.accountNo) {
          let len = this.details.debtorBankingInformation.accountNo.length - 4;
          let first = this.details.debtorBankingInformation.accountNo.substring(0, len);
          let last = this.details.debtorBankingInformation.accountNo.substring(len, this.details.debtorBankingInformation.accountNo.length);
          let str = ''
          for (let i = 0; i < first.length; i++) {
            str = str + '*';
          }
          this.details.debtorBankingInformation.accountNo = str + '' + last;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  emitUploadedFiles(uploadedFile) {
    if (!uploadedFile || uploadedFile?.length == 0) {
      return;
    }
    this.uploadedFile = uploadedFile;
  }

  onSubmit() {
    let obj = {
      'customerId': this.customerId
    }
    let formData = new FormData();
    formData.append('data', JSON.stringify(obj));
    if (this.uploadedFile) {
      formData.append('file', this.uploadedFile);
    }
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.CREDIT_VETTING_PAYMENT_PROOF, formData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
        }
      });
  }

  redirectToDebtorBankingDetails() {
    let urlSuffix = this.IsInstallationDebtor ? 'installation-agreement/debtor-banking-details' :
      'service-agreement/debtor-banking-details';
    if (this.serviceAgreementStepsParams.isOnInvoicePayment) {
      urlSuffix = this.IsInstallationDebtor ? 'installation-agreement/debtor-creation' :
        'service-agreement/debtor-creation';
    }
    if (this.leadHeaderData.fromUrl == 'Leads List') {
      this.router.navigate([`/sales/task-management/leads/${urlSuffix}`]);
    }
    else{
      this.router.navigate([`/my-tasks/my-sales/my-leads/${urlSuffix}`]);
    }
  }
}
