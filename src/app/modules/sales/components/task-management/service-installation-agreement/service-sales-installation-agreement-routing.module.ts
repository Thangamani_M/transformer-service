import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  AccessToPremisesComponent, CellPanicComponent, ContractCreationComponent,
  CreditVettingInfoComponent, CreditVettingResultComponent, DebtorBankingDetailsComponent, DebtorCreationComponent,
  DomesticWorkersComponent,
  FinduComponent, InstallationSiteHazardComponent,
  KeyholderInfoComponent,
  OpenAndCloseMonitoringComponent,
  PasswordFlowComponent,
  SiteHazardComponent
} from '@sales/components/task-management/service-installation-agreement';
import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: 'keyholder-information', component: KeyholderInfoComponent, data: { title: 'Keyholder Information' }, canActivate: [AuthGuard] },
  { path: 'domestic-workers', component: DomesticWorkersComponent, data: { title: 'Domestic Workers' }, canActivate: [AuthGuard] },
  { path: 'open-close-monitoring', component: OpenAndCloseMonitoringComponent, data: { title: 'Open And Close Monitoring' }, canActivate: [AuthGuard] },
  { path: 'password-special-instructions', component: PasswordFlowComponent, data: { title: 'Password And Special Instructions' }, canActivate: [AuthGuard] },
  { path: 'site-hazard', component: SiteHazardComponent, data: { title: 'Access to Premises' }, canActivate: [AuthGuard] },
  { path: 'findu', component: FinduComponent, data: { title: 'Findu' }, canActivate: [AuthGuard] },
  { path: 'checklist', component: ContractCreationComponent, data: { title: 'Check List' }, canActivate: [AuthGuard] },
  { path: 'cell-panic', component: CellPanicComponent, data: { title: 'Cell Panic' }, canActivate: [AuthGuard] },
  { path: 'access-to-premises', component: AccessToPremisesComponent, data: { title: 'Access to Premises' }, canActivate: [AuthGuard] },
  { path: 'debtor-creation', component: DebtorCreationComponent, data: { title: ' Debtor Creation' }, canActivate: [AuthGuard] },
  { path: 'debtor-banking-details', component: DebtorBankingDetailsComponent, data: { title: ' Debtor Banking' }, canActivate: [AuthGuard] },
  { path: 'credit-vetting-info', component: CreditVettingInfoComponent, data: { title: 'Credit vetting info' }, canActivate: [AuthGuard] },
  { path: 'credit-vetting-result', component: CreditVettingResultComponent, data: { title: 'Credit vetting info' }, canActivate: [AuthGuard] },
  { path: 'insta-site-hazard', component: InstallationSiteHazardComponent, data: { title: ' Site Hazard' }, canActivate: [AuthGuard] }
];
@NgModule({
  imports: [RouterModule.forChild(routes)]
})

export class ServiceSalesInstallationAgreementRoutingModule { }
