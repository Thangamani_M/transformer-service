import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, DynamicConfirmByMessageConfirmationType, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, SignalrConnectionService, SnackbarService } from '@app/shared';
import { ModuleName } from '@app/shared/enums';
import { CrudService, RxjsService } from '@app/shared/services';
import { generateCurrentYearToNext99Years, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, monthsByNumber, prepareRequiredHttpParams, setRequiredValidator, SignalRTriggers } from '@app/shared/utils';
import { HubConnection } from '@aspnet/signalr';
import { loggedInUserData, selectStaticEagerLoadingCustomerTypesState$ } from '@modules/others';
import {
  ContractTypes,
  SalesModuleApiSuffixModels, selectLeadCreationStepperParamsState$
} from '@modules/sales';
import { CustomerIdentificationModel, LeadCreationUserDataModel, LeadHeaderData, ServiceAgreementStepsParams } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { selectLeadHeaderDataState$ } from '../lead-creation-ngrx-files';
declare var $;
@Component({
  selector: 'app-contract-creation',
  templateUrl: './contract-creation.component.html',
  styleUrls: ['./service-installation-agreement.scss']
})

export class ContractCreationComponent implements OnInit {
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  availableTabs = [];
  isContractValid = true;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  leadHeaderData: LeadHeaderData;
  currentAgreementTabObj;
  IsInstallationDebtor = false;
  contractTermsandConditions = [];
  @ViewChild('customer_identification_modal', { static: false }) customer_identification_modal: ElementRef<any>;
  breadCrumb: BreadCrumbModel;
  customerIdentificationForm: FormGroup;
  customerTypes = [];
  customerSiteType;
  loggedInUserData: LoggedInUserModel;
  months = monthsByNumber;
  years = generateCurrentYearToNext99Years();

  constructor(private crudService: CrudService, private store: Store<AppState>, private signalrConnectionService: SignalrConnectionService,
    private rxjsService: RxjsService, private router: Router, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private snackbarService: SnackbarService, private formBuilder: FormBuilder) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectLeadHeaderDataState$),
      this.store.select(selectStaticEagerLoadingCustomerTypesState$), this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      this.leadHeaderData = response[1];
      this.customerTypes = response[2];
      this.loggedInUserData = new LoggedInUserModel(response[3]);
      this.currentAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['agreementTabName'].toLowerCase().includes('contract'));
    });
  }

  ngOnInit(): void {
    this.hubConnectionForSalesAPIConfigs();
    this.rxjsService.getIsInstallationDebtor().subscribe((IsInstallationDebtor: boolean) => {
      this.IsInstallationDebtor = IsInstallationDebtor;
      this.breadCrumb = {
        pageTitle: { key: 'Contract Creation' },
        items: [{
          key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
        }, { key: this.IsInstallationDebtor ? 'Sales & Installation Agreement' : 'Service Agreement' }, { key: 'Contract Creation' }]
      };
    });
    this.getServiceAndOrInstallationTabsConfig();
    this.onBootstrapModalEventChanges();
    this.createCustomerIdentificationForm();
    this.onFormControlChanges();
  }

  createCustomerIdentificationForm(): void {
    let customerIdentificationModel = new CustomerIdentificationModel();
    this.customerIdentificationForm = this.formBuilder.group({});
    Object.keys(customerIdentificationModel).forEach((key) => {
      this.customerIdentificationForm.addControl(key,
        new FormControl(customerIdentificationModel[key]));
    });
  }

  hubConnectionInstanceForSalesAPI: HubConnection;
  hubConnectionForSalesAPIConfigs() {
    let signalrConnectionService = this.signalrConnectionService.hubConnectionInitializationForSalesAPIPromise();
    signalrConnectionService.then(() => {
      this.hubConnectionInstanceForSalesAPI = this.signalrConnectionService.salesAPIHubConnectionBuiltInstance();
      // Get agreement tab data using signalR (service agreement)
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.ServiceContractCheckListRefreshTrigger, ({ QuotationVersionId, ServiceAgreements }) => {
        if (QuotationVersionId == this.serviceAgreementStepsParams.quotationVersionId && !this.IsInstallationDebtor) {
          this.manipulateAgreementTabs(JSON.parse(ServiceAgreements));
        }
      });
      // Get agreement tab data using signalR (installation agreement)
      this.hubConnectionInstanceForSalesAPI.on(SignalRTriggers.InstallationContractCheckListRefreshTrigger, ({ QuotationVersionId, InstallationAgreements }) => {
        if (QuotationVersionId == this.serviceAgreementStepsParams.quotationVersionId && this.IsInstallationDebtor) {
          this.manipulateAgreementTabs(JSON.parse(InstallationAgreements));
        }
      });
    })
      .catch(error => console.error(error, "Sales API Error from Contract creation component..!!"));
  }

  onBootstrapModalEventChanges(): void {
    $("#customer_identification_modal").on("shown.bs.modal", () => {
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#customer_identification_modal").on("hidden.bs.modal", () => {
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  onFormControlChanges() {
    this.customerIdentificationForm.get('isPassport').valueChanges.subscribe((isPassport: boolean) => {
      if (this.customerSiteType.id == 2) {
        return;
      }
      if (isPassport) {
        this.customerIdentificationForm = setRequiredValidator(this.customerIdentificationForm, ['passportNo', 'passportExpiryMonth', 'passportExpiryYear']);
        this.customerIdentificationForm.get('SAID').clearValidators();
        this.customerIdentificationForm.get('SAID').updateValueAndValidity();
        this.customerIdentificationForm.get('SAID').setValue(null);
      }
      else {
        this.customerIdentificationForm = setRequiredValidator(this.customerIdentificationForm, ['SAID']);
        this.customerIdentificationForm.get('passportNo').clearValidators();
        this.customerIdentificationForm.get('passportNo').updateValueAndValidity();
        this.customerIdentificationForm.get('passportNo').setValue(null);
        this.customerIdentificationForm.get('passportExpiryMonth').clearValidators();
        this.customerIdentificationForm.get('passportExpiryMonth').updateValueAndValidity();
        this.customerIdentificationForm.get('passportExpiryMonth').setValue(null);
        this.customerIdentificationForm.get('passportExpiryYear').clearValidators();
        this.customerIdentificationForm.get('passportExpiryYear').updateValueAndValidity();
        this.customerIdentificationForm.get('passportExpiryYear').setValue(null);
      }
    });
  }

  getServiceAndOrInstallationTabsConfig(): void {
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.AGREEMENT_TABS,
        undefined,
        false,
        prepareRequiredHttpParams({
          leadId: this.serviceAgreementStepsParams.leadId,
          quotationVersionId: this.serviceAgreementStepsParams.quotationVersionId,
          contractTypeId: this.serviceAgreementStepsParams.contractType,
          partitionId: this.serviceAgreementStepsParams.partitionId
        }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.manipulateAgreementTabs(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCustomerIdentification(): void {
    $(this.customer_identification_modal.nativeElement).modal('show');
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.CUSTOMER_IDENTIFICATION,
        this.leadHeaderData.leadId
      )
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.customerSiteType = this.customerTypes.find(cT => cT['id'] == response.resources.customerTypeId);
          if (this.customerSiteType.id == 2) {
            this.customerIdentificationForm = setRequiredValidator(this.customerIdentificationForm, ['companyRegNo']);
          }
          if (response.resources.isPassport) {
            response.resources['passportExpiryMonth'] = response.resources.passportExpiryDate.split("/")[0];
            response.resources['passportExpiryYear'] = response.resources.passportExpiryDate.split("/")[0];
          }
          this.customerIdentificationForm.patchValue(response.resources);
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  manipulateAgreementTabs(resources) {
    this.availableTabs = this.prepareAgreementTabs(resources).filter(r => r.agreementTabName !== 'Contract Creation');
    // Skip the debtor banking information if the payment method type is oninvoice only (either if services or items are mandatory)
    if (this.serviceAgreementStepsParams.isOnInvoicePayment || this.serviceAgreementStepsParams.isItemAndOnInvoiceOnly) {
      this.availableTabs = this.availableTabs.filter(item => item.agreementTabName != "Debtor Banking Details")
    }
    this.validateRequiredTabsData();
  }

  prepareAgreementTabs(resources) {
    resources.forEach((tabObj, index) => {
      if (this.leadHeaderData.fromUrl == 'Leads List' && tabObj.navigationUrl) {
        tabObj.navigationUrl = '/sales/task-management/leads' + tabObj.navigationUrl;
      }
      else if (tabObj.navigationUrl) {
        tabObj.navigationUrl = '/my-tasks/my-sales/my-leads' + tabObj.navigationUrl;
      }
    });
    return resources;
  }

  validateRequiredTabsData(): void {
    let isMandatoryButNotFilledCount = 0;
    this.availableTabs.filter(a => a['isMandatory']).forEach((tabObj) => {
      const agreementTabName = tabObj.agreementTabName.replace(/\s/g, "").toLowerCase();
      if (this.serviceAgreementStepsParams.contractType == ContractTypes.SERVICE_AGREEMENT) {
        if (agreementTabName.includes('keyholder') || agreementTabName.includes('domestic') || agreementTabName.includes('special') ||
          agreementTabName.includes('arming') || agreementTabName.includes('access') || agreementTabName.includes('cellpanic') || agreementTabName.includes('site') ||
          agreementTabName.includes('debtorcreation') || agreementTabName.includes('debtorbanking') || agreementTabName.includes('findu')
          || agreementTabName.includes('onlinedebtoracceptance') || agreementTabName.includes('kycverification') ||
          agreementTabName.includes('adjustcosting') || agreementTabName.includes('customeridentification')) {
          if (!tabObj.isFilled) {
            isMandatoryButNotFilledCount = 1;
          }
        }
      }
      else {
        if (agreementTabName.includes('installationsitehazards') || agreementTabName.includes('debtorcreation') ||
          agreementTabName.includes('debtorbankingdetails') || agreementTabName.includes('onlinedebtoracceptance') ||
          agreementTabName.includes('adjustcosting') || agreementTabName.includes('kycverification') || agreementTabName.includes('customeridentification')) {
          if (!tabObj.isFilled) {
            isMandatoryButNotFilledCount = 1;
          }
        }
      }
    });
    this.isContractValid = isMandatoryButNotFilledCount == 0 ? true : false;
  }

  onArrowClick() {
    let urlPrefix = this.leadHeaderData.fromUrl == 'Leads List' ? '/sales/task-management/leads/' :
      '/my-tasks/my-sales/my-leads/';
    if (this.IsInstallationDebtor) {
      if (this.serviceAgreementStepsParams.isOnInvoicePayment || this.serviceAgreementStepsParams.isItemAndOnInvoiceOnly) {
        return this.router.navigate([`${urlPrefix}` + 'installation-agreement/debtor-creation']);
      }
      else {
        this.router.navigate([`${urlPrefix}` + 'installation-agreement/debtor-banking-details']);
      }
    } else {
      let indexNo = this.serviceAgreementStepsParams.isOnInvoicePayment ? 2 : 1;
      let previousAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - indexNo));
      this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
    }
  }

  onSubmitCustomerIdentification() {
    if (this.customerIdentificationForm.invalid) {
      return;
    }
    this.customerIdentificationForm.value.leadId = this.leadHeaderData.leadId;
    this.customerIdentificationForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.customerIdentificationForm.value.partitionId = this.serviceAgreementStepsParams.partitionId;
    this.customerIdentificationForm.value.quotationVersionId = this.serviceAgreementStepsParams.quotationVersionId;
    this.customerIdentificationForm.value.contractTypeId = this.serviceAgreementStepsParams.contractType;
    if (this.customerIdentificationForm.value.isPassport) {
      this.customerIdentificationForm.value.passportExpiryDate = `${this.customerIdentificationForm.value.passportExpiryMonth}/${this.customerIdentificationForm.value.passportExpiryYear}`
    }
    delete this.customerIdentificationForm.value.passportExpiryMonth;
    delete this.customerIdentificationForm.value.passportExpiryYear;
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.CUSTOMER_IDENTIFICATION, this.customerIdentificationForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        $(this.customer_identification_modal.nativeElement).modal('hide');
      }
    });
  }

  onSubmit(): void {
    if (!this.isContractValid) {
      this.snackbarService.openSnackbar("Mandatory Tabs Needs To Be Filled..!!", ResponseMessageTypes.WARNING);
      return;
    }
    let formData = new FormData();
    let payload = {
      leadId: this.serviceAgreementStepsParams.leadId,
      createdUserId: this.serviceAgreementStepsParams.createdUserId,
      contractTypeId: this.serviceAgreementStepsParams.contractType,
      quotationVersionId: this.serviceAgreementStepsParams.quotationVersionId,
      customerId: this.serviceAgreementStepsParams.customerId,
      partitionId: this.serviceAgreementStepsParams.partitionId,
      contractId: this.serviceAgreementStepsParams.contractId
    }
    formData.append('payload', JSON.stringify(payload));
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to generate contract?`, null, DynamicConfirmByMessageConfirmationType.WARNING).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) return;
        let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.SALES,
          SalesModuleApiSuffixModels.SALES_API_CONTRACTS, formData);
        crudService.pipe(tap(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.redirectToTab();
          }
        });
      });
  }

  redirectToTab(): void {
    this.rxjsService.navigateToProcessQuotationPage();
  }

  ngOnDestroy() {
    this.hubConnectionInstanceForSalesAPI?.off(SignalRTriggers.ServiceContractCheckListRefreshTrigger);
    this.hubConnectionInstanceForSalesAPI?.off(SignalRTriggers.InstallationContractCheckListRefreshTrigger);
  }
}