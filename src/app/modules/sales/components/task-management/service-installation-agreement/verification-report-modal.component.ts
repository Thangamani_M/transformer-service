import { Component, OnInit, Inject } from '@angular/core';
import { CrudService, ModulesBasedApiSuffix, IApplicationResponse, RxjsService } from '@app/shared';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
@Component({
  selector: 'app-verification-report-modal',
  templateUrl: './verification-report-modal.component.html',
  styleUrls: ['./service-installation-agreement.scss']
})
export class VerificationReportModalComponent implements OnInit {
  verifications;

  constructor(private crudService: CrudService, @Inject(MAT_DIALOG_DATA) public leadId: any,
    private rxjsService: RxjsService) { }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.getVerificationsByLeadId();
  }

  getVerificationsByLeadId(): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.CUSTOMER_CREDIT_VETTING_REPORT, this.leadId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.verifications = response.resources;
        }
      });
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
