import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatInputModule } from '@angular/material';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import {
    AccessToPremisesComponent,
    CellPanicComponent, ContractCreationComponent,
    CreditVettingInfoComponent, CreditVettingModalComponent,
    CreditVettingResultComponent, DebterListComponent, DebtorBankingDetailsComponent, DebtorCreationComponent, DomesticWorkersComponent,
    FinduComponent,
    InstallationSiteHazardComponent,
    KeyholderInfoComponent,
    OpenAndCloseMonitoringComponent,
    PasswordFlowComponent, SearchServiceDebterComponent, ServiceSalesInstallationAgreementRoutingModule,
    SiteHazardComponent, VerificationReportModalComponent
} from '@sales/components/task-management/service-installation-agreement';
import { GaugeChartModule } from 'angular-gauge-chart';
import { OwlDateTimeModule } from 'ng-pick-datetime';
import { SalesModule } from '../../../sales.module';
import { LeadLifecycleReusableComponentsModule } from '../../lead-header';

@NgModule({
  declarations: [
    KeyholderInfoComponent, DomesticWorkersComponent, DebtorCreationComponent, DebtorBankingDetailsComponent,
    AccessToPremisesComponent, PasswordFlowComponent, SiteHazardComponent, CellPanicComponent,
    OpenAndCloseMonitoringComponent, ContractCreationComponent, CreditVettingModalComponent, VerificationReportModalComponent,
    FinduComponent, InstallationSiteHazardComponent, CreditVettingInfoComponent,
    CreditVettingResultComponent, DebterListComponent, SearchServiceDebterComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, ServiceSalesInstallationAgreementRoutingModule,
    MatInputModule,
    OwlDateTimeModule,
    LeadLifecycleReusableComponentsModule,
     GaugeChartModule, SalesModule
  ],
  exports: [KeyholderInfoComponent, DomesticWorkersComponent, OpenAndCloseMonitoringComponent,
    AccessToPremisesComponent, PasswordFlowComponent, SiteHazardComponent],
  providers: [],
  entryComponents: [CreditVettingModalComponent, VerificationReportModalComponent,
    DebterListComponent, SearchServiceDebterComponent]
})
export class ServiceSalesInstallationAgreementModule { }
