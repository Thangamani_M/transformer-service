import { ServiceAgreementInstallationActions, ServiceAgreementInstallationActionTypes } from '@sales/components/task-management';
import { ServiceAgreementStepsParams } from '@modules/sales/models';

export function serviceAgreementReducer(state =new ServiceAgreementStepsParams(),
  action: ServiceAgreementInstallationActions): ServiceAgreementStepsParams {
  switch (action.type) {
    case ServiceAgreementInstallationActionTypes.ServiceAgreementInstallationParamsChangeAction:
      return action.payload.serviceAgreementStepperParams;

      case ServiceAgreementInstallationActionTypes.ServiceAgreementInstallationParamsRemoveAction:
        return new ServiceAgreementStepsParams();

    default:
      return state;
  }
}