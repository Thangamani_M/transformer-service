import {Action} from '@ngrx/store';
import { ServiceAgreementStepsParams } from '@modules/sales/models';

export enum ServiceAgreementInstallationActionTypes {
  ServiceAgreementInstallationParamsChangeAction = '[ServiceAgreementInstallation] Params Change Request Action',
  ServiceAgreementInstallationParamsRemoveAction = '[ServiceAgreementInstallation] Params Remove Action',
}

export class ServiceAgreementInstallationParamsChange implements Action {

  readonly type = ServiceAgreementInstallationActionTypes.ServiceAgreementInstallationParamsChangeAction;

  constructor(public payload: { serviceAgreementStepperParams:ServiceAgreementStepsParams }) {
  }

}

export class ServiceAgreementInstallationParamsRemove implements Action {

  readonly type = ServiceAgreementInstallationActionTypes.ServiceAgreementInstallationParamsRemoveAction;

}

export type ServiceAgreementInstallationActions = ServiceAgreementInstallationParamsChange | ServiceAgreementInstallationParamsRemove;
  



