import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {
  ServiceAgreementInstallationActionTypes, ServiceAgreementInstallationParamsChange,
  ServiceAgreementInstallationParamsRemove
} from '@sales/components/task-management/service-installation-agreement';
import { tap } from 'rxjs/operators';
import { defer, of } from 'rxjs';
import { AppDataService } from '@app/shared/services';

@Injectable()
export class ServiceAgreementEffects {

  @Effect({ dispatch: false })
  dataChange$ = this.actions$.pipe(
    ofType<ServiceAgreementInstallationParamsChange>(ServiceAgreementInstallationActionTypes.ServiceAgreementInstallationParamsChangeAction),
    tap(action =>
      this.appDataService.ServiceAgreementUserData = action.payload.serviceAgreementStepperParams)
  );

  @Effect({ dispatch: false })
  removeData$ = this.actions$.pipe(
    ofType<ServiceAgreementInstallationParamsRemove>(ServiceAgreementInstallationActionTypes.ServiceAgreementInstallationParamsRemoveAction),
    tap(() =>
      this.appDataService.ServiceAgreementUserData = null
    )
  );

  @Effect()
  init$ = defer(() => {
    const serviceAgreementStepperParams = this.appDataService.ServiceAgreementUserData;
    if (serviceAgreementStepperParams) {
      return of(new ServiceAgreementInstallationParamsChange({ serviceAgreementStepperParams }));
    }
    else {
      return <any>of(new ServiceAgreementInstallationParamsRemove());
    }
  });

  constructor(private actions$: Actions, private appDataService: AppDataService) {

  }

}
