import {createSelector, createFeatureSelector} from '@ngrx/store';
import { ServiceAgreementStepsParams } from '@modules/sales/models';


export const selectServiceAgreementStepperState = createFeatureSelector<ServiceAgreementStepsParams>("serviceAgreement");

export const selectServiceAgreementStepperState$ = createSelector(
  selectServiceAgreementStepperState,
  selectServiceAgreementStepperState=>selectServiceAgreementStepperState
);


