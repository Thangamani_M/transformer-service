import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  addFormControls,
  BreadCrumbModel, countryCodes, CrudService, CustomDirectiveConfig, defaultCountryCode, emailPattern, FindDuplicatePipe, formConfigs, IApplicationResponse, ModulesBasedApiSuffix,
  prepareGetRequestHttpParams, removeFormControlError, removeFormControls, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { ModuleName } from '@app/shared/enums';
import { selectStaticEagerLoadingTitlesState$ } from '@modules/others';
import {
  LeadCreationStepperParamsCreateAction, LeadHeaderData,
  SearchDebterModel, selectLeadCreationStepperParamsState$,
  selectLeadHeaderDataState$, ServiceAgreementStepsParams, ServiceDebterModel
} from '@modules/sales';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { DebterListComponent } from './debter-list-modal.component';
import { SearchServiceDebterComponent } from './search-service-debter.component';
@Component({
  selector: 'installation-debtor-creation',
  templateUrl: './installation-debtor-creation.component.html',
})

export class DebtorCreationComponent implements OnInit {
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  leadHeaderData: LeadHeaderData;
  formConfigs = formConfigs;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  debtorForm: FormGroup;
  details;
  titles = [];
  linkedDebtorData;
  currentAgreementTabObj;
  IsInstallationDebtor: boolean = false;
  breadCrumb: BreadCrumbModel;
  isOnInvoicePayment: boolean = false;
  isItemAndOnInvoiceOnly = false;
  countryCodes = countryCodes;
  searchDebterModel = new SearchDebterModel();

  constructor(private crudService: CrudService, private snackbarService: SnackbarService, private router: Router,
    private formBuilder: FormBuilder, private store: Store<AppState>,
    public rxjsService: RxjsService, private dialog: MatDialog, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectStaticEagerLoadingTitlesState$), this.store.select(selectLeadHeaderDataState$)]
    ).subscribe((response) => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      this.titles = response[1];
      this.leadHeaderData = response[2]
      this.currentAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['agreementTabName'].toLowerCase().includes('debtor creat'));
    });
  }

  ngOnInit() {
    this.createDebtorForm();
    this.getDebtorDetailsById();
    this.onFormControlChanges();
    this.rxjsService.getIsInstallationDebtor().subscribe((IsInstallationDebtor: boolean) => {
      this.IsInstallationDebtor = IsInstallationDebtor;
      if (this.IsInstallationDebtor) {
        this.breadCrumb = {
          pageTitle: { key: 'Installation Debtor Creation' },
          items: [{
            key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
          }, { key: 'Sales & Installation Agreement' }, { key: 'Installation Debtor Creation' }]
        };
      }
      else {
        this.breadCrumb = {
          pageTitle: { key: 'Debtor Creation', value: this.leadHeaderData.leadRefNo },
          items: [{
            key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
          }, { key: 'Service Agreement' }, { key: 'Debtor Creation' }]
        };
      }
    });
  }

  createDebtorForm() {
    let serviceDebterModel = new ServiceDebterModel();
    this.debtorForm = this.formBuilder.group({});
    Object.keys(serviceDebterModel).forEach((key) => {
      if (key == 'alternateEmail') {
        this.debtorForm.addControl(key,
          new FormControl(serviceDebterModel[key], Validators.compose([Validators.email, emailPattern])));
      }
      else {
        this.debtorForm.addControl(key,
          new FormControl(serviceDebterModel[key]));
      }
    });
    this.debtorForm = setRequiredValidator(this.debtorForm, ["titleId", "firstName",
      "lastName", "email", "addressLine1", "addressLine2", "postalCode", "mobile1",]);
  }

  onFormControlChanges() {
    this.debtorForm.get('debtorTypeId').valueChanges.subscribe((debtorTypeId: number) => {
      if (!this.disableFlag && !this.debtorForm.value.isExistingDebtor) {
        if (debtorTypeId == 1) {
          this.debtorForm = addFormControls(this.debtorForm, ["companyRegNo", "companyName"]);
          this.debtorForm = setRequiredValidator(this.debtorForm, ["companyName", "companyRegNo"]);
          this.debtorForm = removeFormControls(this.debtorForm, ["said"]);
          if (this.debtorForm.value.isDebtorSameAsCustomer) {
            this.debtorForm.get('companyName').setValue(this.details.leadBasicInfo.companyName);
            this.debtorForm.get('companyRegNo').setValue(this.details.leadBasicInfo.companyRegNo);
          }
        } else if (debtorTypeId == 2) {
          this.debtorForm = addFormControls(this.debtorForm, ["said"]);
          this.debtorForm = setRequiredValidator(this.debtorForm, ["said"]);
          this.debtorForm = removeFormControls(this.debtorForm, ["companyRegNo", "companyName"]);
          if (this.debtorForm.value.isDebtorSameAsCustomer) {
            this.debtorForm.get('said').setValue(this.details.leadBasicInfo.said);
          }
        }
      }
      this.serviceAgreementStepsParams.debtorTypeId = debtorTypeId;
    });
    this.debtorForm
      .get("mobile2")
      .valueChanges.subscribe((mobileNumber2: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.debtorForm.get("mobile2CountryCode").value
        );
      });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.debtorForm
          .get("mobile2")
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
        break;
      default:
        this.debtorForm
          .get("mobile2")
          .setValidators([
            Validators.minLength(formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),
          ]);
        break;
    }
  }

  debtorChange: boolean = false;
  changeIsDebtorSame() {
    this.debtorChange = true;
    if (this.debtorForm.value.isDebtorSameAsCustomer) {
      this.debtorForm.patchValue({
        titleId: this.details.leadBasicInfo.titleId,
        isExistingDebtor: false,
        firstName: this.details.leadBasicInfo.firstName,
        email: this.details.leadBasicInfo.email,
        lastName: this.details.leadBasicInfo.lastName,
        mobile1: this.details.leadBasicInfo.mobile1,
        mobile1CountryCode: this.details.leadBasicInfo.mobile1CountryCode ? this.details.leadBasicInfo.mobile1CountryCode : '+27',
        mobile2CountryCode: this.details.leadBasicInfo.mobile2CountryCode ? this.details.leadBasicInfo.mobile2CountryCode : '+27',
        mobile2: this.details.leadBasicInfo.mobile2,
        officeNoCountryCode: this.details.leadBasicInfo.officeNoCountryCode ? this.details.leadBasicInfo.officeNoCountryCode : '+27',
        officeNo: this.details.leadBasicInfo.officeNo,
        premisesNoCountryCode: this.details.leadBasicInfo.premisesNoCountryCode ? this.details.leadBasicInfo.premisesNoCountryCode : '+27',
        premisesNo: this.details.leadBasicInfo.premisesNo,
        contactId: this.details.leadBasicInfo.contactId,
        said: this.details.leadBasicInfo.said,
        debtorTypeId: this.details.debtorTypeId,
        debtorId: this.details.leadBasicInfo.debtorId,
        debtorContactId: this.details.leadBasicInfo.debtorContactId,
        debtorAdditionalInfoId: this.details.leadBasicInfo.debtorAdditionalInfoId,
        linkContractDebtor: null
      });
    } else {
      this.debtorForm.patchValue({
        titleId: "",
        firstName: "",
        email: "",
        lastName: "",
        mobile2: "",
        mobile1: "",
        contactId: null,
        said: "",
        debtorId: "",
        debtorContactId: "",
        debtorAdditionalInfoId: "",
        linkContractDebtor: false
      });
    }
  }

  changeIsBillingAddress() {
    if (this.debtorForm.value.isBillingSameAsSiteAddress) {
      this.debtorForm.patchValue({
        addressLine1: this.details.leadAddress.addressLine1,
        addressLine2: this.details.leadAddress.addressLine2,
        addressLine3: this.details.leadAddress.addressLine3,
        addressLine4: this.details.leadAddress.addressLine4,
        postalCode: this.details.leadAddress.postalCode,
      });
    } else {
      this.debtorForm.patchValue({
        addressLine1: "",
        addressLine2: "",
        addressLine3: "",
        addressLine4: "",
        postalCode: "",
      });
    }
  }

  changeIsExisting() {
    this.debtorChange = true;
    if (this.debtorForm.value.isExistingDebtor) {
      this.debtorForm.reset({ debtorTypeId: this.debtorForm.value.debtorTypeId });
      this.debtorForm.patchValue({
        isExistingDebtor: true,
        linkContractDebtor: false, isDebtorSameAsCustomer: false
      });
      this.debtorForm = removeFormControlError(this.debtorForm, 'invalid');
      this.openModal();
    } else {
      this.disableFlag = false;
      this.debtorForm.reset();
      this.debtorForm.get('isExistingDebtor').setValue(false);
      this.debtorForm.get('debtorTypeId').setValue(this.details.debtorTypeId);
    }
    this.debtorForm.get('titleId').setValue("");
    this.resetPhoneNumbersToDefaultCountryCode();
  }

  linkExistingAgreementDebtor() {
    this.debtorChange = false;
    this.debtorForm.reset();
    this.debtorForm.patchValue({
      linkContractDebtor: true,
      titleId: "", isDebtorSameAsCustomer: false,
      isExistingDebtor: false, debtorTypeId: this.details.debtorTypeId
    });
    this.resetPhoneNumbersToDefaultCountryCode();
    this.debtorForm = removeFormControlError(this.debtorForm, 'invalid');
    this.getExistingDebtorDetails(this.details?.linkContractDebtor?.debtorId, this.details?.linkContractDebtor?.debtorAccountDetailId,
      this.details?.linkContractDebtor?.isLinkDebtor);
  }

  disableFlag: boolean = false;
  getExistingDebtorDetails(debtorId, debtorAccountDetailId, isLinkDebtor = true) {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_DEBTER_DETAILS, undefined, false, prepareGetRequestHttpParams(null, null, {
      debtorId,
      debtorAccountDetailId: debtorAccountDetailId ? debtorAccountDetailId : '',
      isLinkDebtor
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.linkedDebtorData = response.resources;
        this.disableFlag = true;
        if (this.details.address) {
          this.linkedDebtorData.addressLine1 = this.linkedDebtorData.addressLine1 ? this.linkedDebtorData.addressLine1.replace(/,/g, ' ') : '';
          this.linkedDebtorData.addressLine2 = this.linkedDebtorData.addressLine2 ? this.linkedDebtorData.addressLine2.replace(/,/g, ' ') : '';
          this.linkedDebtorData.addressLine3 = this.linkedDebtorData.addressLine3 ? this.linkedDebtorData.addressLine3.replace(/,/g, ' ') : '';
          this.linkedDebtorData.addressLine4 = this.linkedDebtorData.addressLine4 ? this.linkedDebtorData.addressLine4.replace(/,/g, ' ') : '';
        }
        this.debtorForm.patchValue(response.resources);
        if (this.linkedDebtorData.premisesNo == 0) {
          this.debtorForm.get('premisesNo').setValue('');
        }
        this.debtorForm.patchValue({
          premisesNoCountryCode: defaultCountryCode,
          officeNoCountryCode: defaultCountryCode,
          isExistingDebtor: true, isBillingSameAsSiteAddress: true
        });
        if (this.debtorForm.get('isExistingDebtor').value || this.debtorForm.get('linkContractDebtor').value) {
          this.resetPhoneNumbersToDefaultCountryCode();
        } else {
          this.disableFlag = false;
          this.debtorForm.reset();
          this.resetPhoneNumbersToDefaultCountryCode();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  openDebterExistModal(data) {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(DebterListComponent, { width: '960px', data: data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData1.subscribe(ele => {
      if (ele) {
        this.getExistingDebtorDetails(ele.debtorId, ele.debtorAccountDetailId);
      }
    });
  }

  openModal() {
    this.rxjsService.setDialogOpenProperty(true);
    let data = { debtorTypeId: this.debtorForm.get('debtorTypeId').value };
    const dialogReff = this.dialog.open(SearchServiceDebterComponent, { width: '960px', disableClose: true, data });
    dialogReff.afterClosed().subscribe(result => {
      this.debtorForm.get('isExistingDebtor').setValue(false);
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.openDebterExistModal(ele);
      }
    });
  }

  onArrowClick(type: string) {
    let urlPrefix = this.leadHeaderData.fromUrl == 'Leads List' ? '/sales/task-management/leads/' :
      '/my-tasks/my-sales/my-leads/';
    let nextIndex = this.currentAgreementTabObj['indexNo'] + 1;
    let nextUrl = "";
    if (this.isOnInvoicePayment || this.isItemAndOnInvoiceOnly) { // Either On-Invoice Payment Method if service is mandatory ( items are optional ) or For On-Invoice Payment Method if items are mandatory ( Services must not be purchased )
      if (this.IsInstallationDebtor) {
        nextUrl = `${urlPrefix}` + 'installation-agreement/checklist'; // to skip debtor account details and Online acceptance
      } else {
        nextIndex += 1 // to skip debtor account details
      }
    }
    let previousAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (nextIndex));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
      }
      else {
        if (!this.currentAgreementTabObj.isMandatory || (((this.details && this.details.debtorDetails) || this.debtorForm.value?.debtorAdditionalInfoId) && this.currentAgreementTabObj.isMandatory)) {
          this.router.navigateByUrl(nextUrl ? nextUrl : nextAgreementTabObj.navigationUrl);
        }
      }
    }
  }

  getDebtorDetailsById(type?: string) {
    if (type == 'revert changes') {
      this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to revert the changes?").
        onClose?.subscribe(dialogResult => {
          if (dialogResult) {
            this.getDebtorDetailsByIdAfterTypeChecking();
          }
        });
    }
    else {
      this.getDebtorDetailsByIdAfterTypeChecking();
    }
  }

  getDebtorDetailsByIdAfterTypeChecking() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_DEBTOR_LEADS, undefined, false, prepareGetRequestHttpParams(null, null, {
      leadId: this.serviceAgreementStepsParams.leadId,
      contractTypeId: this.serviceAgreementStepsParams.contractType,
      quotationVersionId: this.serviceAgreementStepsParams.quotationVersionId
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.details = response.resources;
        this.isOnInvoicePayment = response.resources?.isOnInvoicePayment;
        this.isItemAndOnInvoiceOnly = response.resources?.isItemAndOnInvoiceOnly;
        this.serviceAgreementStepsParams.isOnInvoicePayment = this.isOnInvoicePayment;
        this.serviceAgreementStepsParams.isItemAndOnInvoiceOnly = this.isItemAndOnInvoiceOnly;
        if (this.details.address) {
          this.details.leadAddress.addressLine1 = this.details.leadAddress.addressLine1 ? this.details.leadAddress.addressLine1.replace(/,/g, ' ') : '';
          this.details.leadAddress.addressLine2 = this.details.leadAddress.addressLine2 ? this.details.leadAddress.addressLine2.replace(/,/g, ' ') : '';
          this.details.leadAddress.addressLine3 = this.details.leadAddress.addressLine3 ? this.details.leadAddress.addressLine3.replace(/,/g, ' ') : '';
          this.details.leadAddress.addressLine4 = this.details.leadAddress.addressLine4 ? this.details.leadAddress.addressLine4.replace(/,/g, ' ') : '';
        }
        this.debtorForm.get('debtorTypeId').setValue(response.resources.debtorTypeId);
        if (response.resources.debtorDetails) {
          this.serviceAgreementStepsParams.debtorTypeId = response.resources.debtorTypeId;
          this.serviceAgreementStepsParams.debtorId = response.resources.debtorDetails.debtorId;
          this.serviceAgreementStepsParams.debtorAdditionalInfoId = response.resources.debtorDetails.debtorAdditionalInfoId;
        }
        else {
          this.serviceAgreementStepsParams.debtorTypeId = response.resources.debtorTypeId;
        }
        this.store.dispatch(new LeadCreationStepperParamsCreateAction({ serviceAgreementStepsParams: this.serviceAgreementStepsParams }));
        if (this.details.linkContractDebtor) {
          this.debtorChange = false;
          this.debtorForm.get('linkContractDebtor').setValue(true);
        }
        else {
          this.debtorForm.get('linkContractDebtor').setValue(false);
        }
        if (this.details.debtorDetails) {
          this.debtorForm.patchValue(this.details.debtorDetails);
        }
        if (this.debtorForm.value.isExistingDebtor) {
          this.resetPhoneNumbersToDefaultCountryCode();
          this.debtorForm.get('isExistingDebtor').setValue(true);
        } else {
          this.disableFlag = false;
          this.resetPhoneNumbersToDefaultCountryCode();
        }
        if (this.details.basicInfo) {
          this.debtorForm.patchValue(this.details.basicInfo);
        }
        if (this.details.address) {
          this.debtorForm.patchValue(this.details.address);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  resetPhoneNumbersToDefaultCountryCode() {
    this.debtorForm.get('premisesNoCountryCode').setValue('+27');
    this.debtorForm.get('mobile2CountryCode').setValue('+27');
    this.debtorForm.get('mobile1CountryCode').setValue('+27');
    this.debtorForm.get('officeNoCountryCode').setValue('+27');
  }

  kycDetails() {
    if (this.serviceAgreementStepsParams.debtorId && this.serviceAgreementStepsParams.customerId) {
      let queryParams = { queryParams: { customerId: this.leadHeaderData.customerId, debtorId: this.serviceAgreementStepsParams.debtorId, debtorAccountDetailId: '' } };
      let urlSuffix = this.IsInstallationDebtor ? 'installation-agreement/credit-vetting-info' :
        'service-agreement/credit-vetting-info';
      if (this.leadHeaderData.fromUrl == 'Leads List') {
        this.router.navigate([`/sales/task-management/leads/${urlSuffix}`], queryParams);
      }
      else {
        this.router.navigate([`/my-tasks/my-sales/my-leads/${urlSuffix}`], queryParams);
      }
    } else {
      this.snackbarService.openSnackbar("Debtor ID Or Debtor Account details ID is required", ResponseMessageTypes.WARNING);
    }
  }

  viewResult() {
    if (this.serviceAgreementStepsParams.debtorId &&
      this.serviceAgreementStepsParams.customerId) {
      let queryParams = { queryParams: { customerId: this.leadHeaderData.customerId, debtorId: this.serviceAgreementStepsParams.debtorId, debtorAccountDetailId: '' } };
      let urlSuffix = this.IsInstallationDebtor ? 'installation-agreement/credit-vetting-result' :
        'service-agreement/credit-vetting-result';
      if (this.leadHeaderData.fromUrl == 'Leads List') {
        this.router.navigate([`/sales/task-management/leads/${urlSuffix}`], queryParams);
      }
      else {
        this.router.navigate([`/my-tasks/my-sales/my-leads/${urlSuffix}`], queryParams);
      }
    } else {
      this.snackbarService.openSnackbar("Debtor ID Or Debtor Account details ID is required", ResponseMessageTypes.WARNING);
    }
  }

  validateAtleastOneFieldToBeMandatory(): void {
    if (
      !this.debtorForm.get("mobile2").value &&
      !this.debtorForm.get("officeNo").value &&
      !this.debtorForm.get("premisesNo").value
    ) {
      this.setAtleastOneFieldRequiredError();
    } else {
      this.debtorForm = removeFormControlError(this.debtorForm, "atleastOneOfTheFieldsIsRequired");
    }
  }

  setAtleastOneFieldRequiredError(): void {
    this.debtorForm
      .get("mobile2")
      .setErrors({ atleastOneOfTheFieldsIsRequired: true });
    this.debtorForm
      .get("officeNo")
      .setErrors({ atleastOneOfTheFieldsIsRequired: true });
    this.debtorForm
      .get("premisesNo")
      .setErrors({ atleastOneOfTheFieldsIsRequired: true });
  }

  onSubmit() {
    this.validateAtleastOneFieldToBeMandatory();
    const isMobile1Duplicate = new FindDuplicatePipe().transform(this.debtorForm.get("mobile1").value, this.debtorForm, "mobile1");
    const isMobile2Duplicate = new FindDuplicatePipe().transform(this.debtorForm.get("mobile2").value, this.debtorForm, "mobile2");
    const isOfficeNoDuplicate = new FindDuplicatePipe().transform(this.debtorForm.get("officeNo").value, this.debtorForm, "officeNo");
    const isPremisesNoDuplicate = new FindDuplicatePipe().transform(this.debtorForm.get("premisesNo").value, this.debtorForm, "premisesNo");
    if (
      this.debtorForm.invalid ||
      isMobile1Duplicate ||
      isMobile2Duplicate ||
      isOfficeNoDuplicate ||
      isPremisesNoDuplicate
    ) {
      return;
    }
    let payload = this.debtorForm.getRawValue();
    payload.createdUserId = this.serviceAgreementStepsParams.createdUserId;
    payload.leadId = this.serviceAgreementStepsParams.leadId;
    payload.quotationVersionId = this.serviceAgreementStepsParams.quotationVersionId;
    payload.contractTypeId = this.serviceAgreementStepsParams.contractType;
    payload.customerId = this.leadHeaderData['customerId'];
    payload.siteTypeId = this.leadHeaderData['siteTypeId'];
    if (typeof payload.mobile1 == 'string') {
      payload.mobile1 = payload.mobile1.split(" ").join("")
    }
    if (typeof payload.mobile2 == 'string') {
      payload.mobile2 = payload.mobile2.split(" ").join("")
    }
    if (typeof payload.premisesNo == 'string') {
      payload.premisesNo = payload.premisesNo.split(" ").join("")
    }
    if (typeof payload.officeNo == 'string') {
      payload.officeNo = payload.officeNo.split(" ").join("")
    }
    if (!payload.isBillingSameAsSiteAddress) {
      payload.isBillingSameAsSiteAddress = false;
    }
    if (!payload.isDebtorSameAsCustomer) {
      payload.isDebtorSameAsCustomer = false;
      payload.contactId = null;
    } else {
      payload.contactId = this.details.leadBasicInfo ? this.details.leadBasicInfo.contactId : null;
    }
    if (!payload.isEmailCommunication) {
      payload.isEmailCommunication = false;
    }
    if (!payload.isExistingDebtor) {
      payload.isExistingDebtor = false;
    }
    if (!payload.isPhoneCommunication) {
      payload.isPhoneCommunication = false;
    }
    if (!payload.isPostCommunication) {
      payload.isPostCommunication = false;
    }
    if (!payload.isSMSCommunication) {
      payload.isSMSCommunication = false;
    }
    if (this.debtorChange) {
      payload.debtorId = null;
      payload.debtorRefNo = null;
      payload.bdiNumber = null;
      payload.billingAddressId = null;
      payload.debtorContactId = this.details.debtorDetails ? this.details.debtorDetails.debtorContactId : null;
      payload.debtorAdditionalInfoId = this.debtorForm.get('debtorAdditionalInfoId').value ?
        this.debtorForm.get('debtorAdditionalInfoId').value : this.details.debtorDetails ? this.details.debtorDetails.debtorAdditionalInfoId : null;
      payload.debtorAccountDetailId = null
      payload.contactId = this.details.leadBasicInfo ? this.details.leadBasicInfo.contactId : null;;
    } else {
      payload.debtorId = this.details.debtorDetails ? this.details.debtorDetails.debtorId : null;
      payload.debtorContactId = this.details.debtorDetails ? this.details.debtorDetails.debtorContactId : null;
      payload.debtorAdditionalInfoId = this.debtorForm.get('debtorAdditionalInfoId').value ?
        this.debtorForm.get('debtorAdditionalInfoId').value : this.details.debtorDetails ? this.details.debtorDetails.debtorAdditionalInfoId : null;
      payload.debtorAccountDetailId = this.details.debtorDetails ? this.details.debtorDetails.debtorAccountDetailId : null;
    }
    if (payload.isExistingDebtor && this.linkedDebtorData) {
      payload.billingAddressId = this.linkedDebtorData.billingAddressId ? this.linkedDebtorData.billingAddressId : null;
      payload.isBillingSameAsSiteAddress = false;
      payload.contactId = this.linkedDebtorData.contactId ? this.linkedDebtorData.contactId : null;
      payload.debtorId = this.linkedDebtorData.debtorId ? this.linkedDebtorData.debtorId : null;
      payload.debtorRefNo = this.linkedDebtorData.debtorRefNo ? this.linkedDebtorData.debtorRefNo : null;
      payload.bdiNumber = this.linkedDebtorData.bdiNumber ? this.linkedDebtorData.bdiNumber : null;
      payload.debtorAccountDetailId = this.linkedDebtorData.debtorAccountDetailId ? this.linkedDebtorData.debtorAccountDetailId : null;
    }
    payload.officeNoCountryCode = this.details.basicInfo ? this.details.basicInfo.officeNoCountryCode : null;
    payload.officeNoCountryCode = payload.officeNoCountryCode ? payload.officeNoCountryCode : '+27'
    payload.mobile1CountryCode = this.details.basicInfo ? this.details.basicInfo.mobile1CountryCode : null;
    payload.mobile1CountryCode = payload.mobile1CountryCode ? payload.mobile1CountryCode : '+27'
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_CREATE_DEBTER, payload, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        if (!this.serviceAgreementStepsParams.debtorAdditionalInfoId) {
          this.getDebtorDetailsById();
        }
        this.serviceAgreementStepsParams.debtorAdditionalInfoId = response.resources;
        this.debtorForm.get('debtorAdditionalInfoId').setValue(response.resources);
        this.store.dispatch(new LeadCreationStepperParamsCreateAction({ serviceAgreementStepsParams: this.serviceAgreementStepsParams }));
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }
}
