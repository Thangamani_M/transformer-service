import { Component, ElementRef, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  btnActionTypes, CustomerModuleApiSuffixModels, CustomerVerificationType, DomesticWorkersAddEditModel,
  loggedInUserData, SalesModuleApiSuffixModels, selectLeadCreationStepperParamsState$,
  selectLeadHeaderDataState$, selectStaticEagerLoadingTypeOfWorkersState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  addFormControls, BreadCrumbModel, countryCodes, CrudService, CustomDirectiveConfig,
  DynamicConfirmByMessageConfirmationType,
  findTheDifferenceBetweenTwoArrays,
  formConfigs, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControlError, removeFormControls,
  removeFormValidators,
  ReusablePrimeNGTableFeatureService,
  RxjsService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { ModuleName, ResponseMessageTypes } from '@app/shared/enums';
import { LeadHeaderData, ServiceAgreementStepsParams, SiteSpecificInputObjectModel } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { Guid } from 'guid-typescript';
import { combineLatest, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-domestic-workers',
  templateUrl: './domestic-workers.component.html',
  styleUrls: ['./service-installation-agreement.scss']
})

export class DomesticWorkersComponent implements OnInit {
  @Input() inputObject = new SiteSpecificInputObjectModel();
  @Input() permission;
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  @ViewChildren('input') arrayList: QueryList<ElementRef>;
  formConfigs = formConfigs;
  leadHeaderData: LeadHeaderData;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  selectedDomesticTypeWorkerName: string = "Domestic";
  initialWorkingDays = [{ isChecked: false, name: "Monday" },
  { isChecked: false, name: "Tuesday" }, { isChecked: false, name: "Wednesday" }, { isChecked: false, name: "Thursday" },
  { isChecked: false, name: "Friday" }, { isChecked: false, name: "Saturday" }, { isChecked: false, name: "Sunday" }];
  tempWorkingDays = this.initialWorkingDays;
  domesticWorkersArray: DomesticWorkersAddEditModel[] | any = [];
  domesticWorkersArrayCopy: DomesticWorkersAddEditModel[] = [];
  domesticWorkersForm: FormGroup;
  domesticWorkers = [];
  selectedItemIndex = -1;
  countryCodes = countryCodes;
  selectedTypeOfWorkersName = "";
  currentAgreementTabObj;
  isDisabled = false;
  loggedInUserData: LoggedInUserModel;
  breadCrumb: BreadCrumbModel;
  uniqueCallId;
  pScrollStyleHeight;
  selectedDaysOnDuty = [];

  constructor(private crudService: CrudService, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private store: Store<AppState>, private router: Router,
    private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService
  ) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
  }

  ngOnInit(): void {
    this.isDisabled = this.inputObject.fromComponentType == 'lead' ? false : true;
    this.pScrollStyleHeight = this.inputObject.fromComponentType == 'lead' ? '41vh' : 'auto';
    this.createDomesticWorkersForm();
    this.createCheckBoxGroup();
    this.onDomesticWorkerTypeChanges();
    this.getDomesticWorkersById();
    this.onFormControlChanges();
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onFormControlChanges(): void {
    this.domesticWorkersForm.get('identityNo').valueChanges.subscribe((identityNo: string) => {
      if (this.domesticWorkersArray.find(d => d.identityNo == identityNo) && identityNo && this.selectedItemIndex == -1) {
        this.domesticWorkersForm.get('identityNo').setErrors({ duplicate: true });
      }
      else if (this.selectedItemIndex == -1) {
        this.domesticWorkersForm = removeFormControlError(this.domesticWorkersForm, 'duplicate');
      }
    });
  }

  onBlur() {
    if (this.domesticWorkersArray.find(d => d.identityNo == this.domesticWorkersForm.get('identityNo').value) &&
      this.domesticWorkersForm.get('identityNo').value && this.selectedItemIndex == -1) {
      this.domesticWorkersForm.get('identityNo').setErrors({ duplicate: true });
    }
    else if (this.selectedItemIndex == -1) {
      this.domesticWorkersForm = removeFormControlError(this.domesticWorkersForm, 'duplicate');
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectStaticEagerLoadingTypeOfWorkersState$), this.store.select(selectLeadHeaderDataState$),
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      this.domesticWorkers = response[1];
      this.leadHeaderData = response[2];
      this.loggedInUserData = new LoggedInUserModel(response[3]);
      this.currentAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['agreementTabName'].toLowerCase().includes('domestic'));
      if (this.leadHeaderData) {
        this.breadCrumb = {
          pageTitle: { key: 'Domestic Workers' },
          items: [{
            key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
          }, { key: 'Service Agreement' }, { key: 'Domestic Workers' }]
        };
      }
    });
  }

  createCheckBoxGroup(): void {
    let checkboxGroup = new FormArray(this.tempWorkingDays.map(item => new FormGroup({
      name: new FormControl(item.name),
      isChecked: new FormControl(item.isChecked),
    })));
    let hiddenControl = new FormControl(this.mapItems(checkboxGroup.value), Validators.required);
    checkboxGroup.valueChanges.subscribe((v) => {
      hiddenControl.setValue(this.mapItems(v));
    });
    this.domesticWorkersForm.addControl("tempWorkingDays", checkboxGroup);
    this.domesticWorkersForm.addControl("selectedItems", hiddenControl);
    this.domesticWorkersForm.updateValueAndValidity({ emitEvent: false, onlySelf: true });
  }

  mapItems(items) {
    this.selectedDaysOnDuty = items.filter((item) => item.isChecked).map((item) => item.name);
    this.domesticWorkersForm.get('selectedItems') &&
      this.domesticWorkersForm.get('selectedItems').setErrors((this.selectedDaysOnDuty && this.selectedDaysOnDuty.length > 0) ? null : { invalid: true });
    return this.selectedDaysOnDuty.length ? this.selectedDaysOnDuty : null;
  }

  onDomesticWorkerTypeChanges(): void {
    this.domesticWorkersForm.get('domesticWorkerTypeId').valueChanges.subscribe((domesticWorkerTypeId: string) => {
      if (domesticWorkerTypeId == "" || !domesticWorkerTypeId) return;
      this.getTempWorkingDaysArray.markAsUntouched();
      this.selectedDomesticTypeWorkerName = this.domesticWorkers.find(d => d['id'] == domesticWorkerTypeId).displayName;
      this.domesticWorkersForm.get('domesticWorkerTypeName').patchValue(this.selectedDomesticTypeWorkerName);
      switch (this.selectedDomesticTypeWorkerName) {
        case "Domestic Employees":
          this.domesticWorkersForm = addFormControls(this.domesticWorkersForm, [{ controlName: "isSleepIn", defaultValue: false }]);
          break;
        case "Gardener":
          this.domesticWorkersForm = addFormControls(this.domesticWorkersForm, [{ controlName: "isSleepIn", defaultValue: false }]);
          break;
        case "On-site Service Providers":
          this.domesticWorkersForm = removeFormControls(this.domesticWorkersForm, ["isSleepIn"]);
          break;
        default:
          this.domesticWorkersForm = removeFormControls(this.domesticWorkersForm, ["isSleepIn"]);
          break;
      }
    });
  }

  getDomesticWorkersById(): void {
    let resultObservable = this.inputObject.fromComponentType == 'lead' ?
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_DOMESTIC_DETAILS, undefined, false,
        prepareGetRequestHttpParams(undefined, undefined, {
          leadId: this.serviceAgreementStepsParams.leadId,
          customerId: this.serviceAgreementStepsParams.customerId,
          partitionId: this.serviceAgreementStepsParams.partitionId,
          quotationVersionId: this.serviceAgreementStepsParams.quotationVersionId,
          isExist: this.serviceAgreementStepsParams.isExist
        })) :
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.DOMESTIC_WORKERS_DETAILS, undefined, false,
        prepareGetRequestHttpParams(undefined, undefined, {
          customerId: this.inputObject.customerId,
          addressId: this.inputObject.addressId,
          partitionId: this.inputObject.partitionId,
          quotationVersionId: this.inputObject.quotationVersionId
        }));
    resultObservable.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        response.resources.forEach((domesticWorkersAddEditModel: DomesticWorkersAddEditModel) => {
          if (typeof domesticWorkersAddEditModel.isSleepIn === 'string') {
            domesticWorkersAddEditModel.isSleepIn = (domesticWorkersAddEditModel.isSleepIn.toString().toLowerCase() === "true") ? true :
              (domesticWorkersAddEditModel.isSleepIn.toString().toLowerCase() === "false") ? false : '-';
          }
        });
        response.resources.forEach((resource) => {
          if (resource.contactNo) {
            resource.contactNo = resource.contactNo.toString();
          }
          resource.tempGUID = Guid.create()['value'];
        });
        this.domesticWorkersArray = response.resources;
        this.domesticWorkersArrayCopy = JSON.parse(JSON.stringify(response.resources));
      }
      if (this.inputObject.fromComponentType == 'customer') {
        this.serviceAgreementStepsParams = new ServiceAgreementStepsParams({
          leadId: this.inputObject.leadId,
          customerId: this.inputObject.customerId,
          quotationVersionId: this.inputObject.quotationVersionId,
          createdUserId: this.loggedInUserData.userId,
          modifiedUserId: this.loggedInUserData.userId,
          partitionId: this.inputObject.partitionId
        });
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createDomesticWorkersForm(): void {
    let domesticWorkersAddEditModel = new DomesticWorkersAddEditModel();
    this.domesticWorkersForm = this.formBuilder.group({});
    Object.keys(domesticWorkersAddEditModel).forEach((key) => {
      if (key === 'tempWorkingDays') return;
      this.domesticWorkersForm.addControl(key,
        new FormControl(domesticWorkersAddEditModel[key]));
    });
    this.domesticWorkersForm = setRequiredValidator(this.domesticWorkersForm, ["domesticWorkerTypeId", "employeeName"]);
  }

  onChange(event): void {
    let workingDaysObj = this.tempWorkingDays.find(w => w.name === event.source.value);
    workingDaysObj.isChecked = event.checked ? true : false;
  }

  focusInAndOutFormArrayFields(): void {
    setTimeout(() => {
      this.arrayList.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      })
    }, 100);
  }

  get getTempWorkingDaysArray() {
    if (!this.domesticWorkersForm) { return; }
    return this.domesticWorkersForm.get('tempWorkingDays') as FormArray;
  }

  addItem(): void {
    this.getTempWorkingDaysArray?.markAsTouched();
    if (this.domesticWorkersForm.invalid) {
      if (this.selectedDaysOnDuty?.length == 0) {
        this.domesticWorkersForm.get('selectedItems').setErrors({ invalid: true });
      }
      this.focusInAndOutFormArrayFields();
      return;
    };
    if (this.selectedDaysOnDuty?.length == 0) {
      this.domesticWorkersForm.get('selectedItems').setErrors({ invalid: true });
      return;
    }
    let workingDaysCommaSeperated = [];
    this.getTempWorkingDaysArray.controls.forEach((control: AbstractControl) => {
      if (control.value['isChecked']) {
        workingDaysCommaSeperated.push(control.value);
      }
    });
    let days = workingDaysCommaSeperated.map(obj => obj.name).join();
    this.domesticWorkersForm.patchValue({ days });
    if (this.domesticWorkersForm.value.contactNo) {
      this.domesticWorkersForm.value.contactNo = this.domesticWorkersForm.value.contactNo.replace(/\s/g, "");
    }
    if (!this.domesticWorkersForm.value.tempGUID) {
      this.domesticWorkersForm.value.tempGUID = Guid.create.toString();
      this.domesticWorkersArray.push(this.domesticWorkersForm.value);
    }
    else {
      let foundIndex = this.domesticWorkersArray.findIndex((domesticWorkersObj: DomesticWorkersAddEditModel) => domesticWorkersObj.tempGUID == this.domesticWorkersForm.value.tempGUID);
      this.domesticWorkersArray[foundIndex] = this.domesticWorkersForm.value;
    }
    this.domesticWorkersForm.reset(new DomesticWorkersAddEditModel(), { emitEvent: false, onlySelf: true });
    this.selectedItemIndex = -1;
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.selectedDaysOnDuty.length = 0;
  }

  onItemClicked(index: number, domesticWorkersAddEditModel?: DomesticWorkersAddEditModel): void {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.selectedItemIndex = index;
    if (domesticWorkersAddEditModel.isSleepIn == true || domesticWorkersAddEditModel.isSleepIn == false) {
      domesticWorkersAddEditModel.isSleepIn =
        (domesticWorkersAddEditModel.isSleepIn.toString().toLowerCase() === "true") ? true :
          (domesticWorkersAddEditModel.isSleepIn.toString().toLowerCase() === "false") ? false : '-';
    }
    else {
      domesticWorkersAddEditModel.isSleepIn = '-';
    }
    this.domesticWorkersForm.patchValue(domesticWorkersAddEditModel);
    this.getTempWorkingDaysArray.controls.forEach((control: AbstractControl) => {
      let daysSplitArray = !Array.isArray(domesticWorkersAddEditModel.days) ? domesticWorkersAddEditModel.days :
        domesticWorkersAddEditModel.days.join();
      if (daysSplitArray.includes(control.value['name'])) {
        control.patchValue({ isChecked: true });
      }
      else {
        control.patchValue({ isChecked: false });
      }
    });
    this.focusInAndOutFormArrayFields();
  }

  removeItem(index: number, domesticWorkersObj?: DomesticWorkersAddEditModel) {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, null, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) return;
        if (domesticWorkersObj.domesticWorkerId) {
          let resultObservable = this.inputObject.fromComponentType == 'lead' ?
            this.crudService.delete(ModulesBasedApiSuffix.SALES,
              SalesModuleApiSuffixModels.SALES_API_DOMESTIC_WORKERS, undefined,
              prepareRequiredHttpParams({
                ids: domesticWorkersObj.domesticWorkerId,
                isDeleted: true,
                modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
              })) :
            this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
              CustomerModuleApiSuffixModels.DOMESTIC_WORKERS, undefined,
              prepareRequiredHttpParams({
                ids: domesticWorkersObj.domesticWorkerId,
                isDeleted: true,
                modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
              }));
          resultObservable.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              this.domesticWorkersArray.splice(index, 1);
            }
          })
        }
        else {
          this.domesticWorkersArray.splice(index, 1);
          this.rxjsService.setFormChangeDetectionProperty(true);
        }
      });
  }

  onSubmit() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.domesticWorkersForm = removeFormValidators(this.domesticWorkersForm, ['domesticWorkerTypeId', 'employeeName']);
    this.domesticWorkersForm = removeFormControls(this.domesticWorkersForm, ['tempWorkingDays']);
    let arrayDifference = findTheDifferenceBetweenTwoArrays(this.domesticWorkersArrayCopy, this.domesticWorkersArray, ["contactNo", "domesticWorkerTypeId", "identityNo", "employeeName"]);
    if ((this.domesticWorkersArray && this.domesticWorkersArray.length === 0) || !this.domesticWorkersArray ||
      (arrayDifference.length == 0 && this.domesticWorkersArray.length == this.domesticWorkersArrayCopy.length && !this.domesticWorkersArray[0].domesticWorkerId) && !this.serviceAgreementStepsParams.isExist) {
      return;
    }
    this.domesticWorkersArray.forEach((obj) => {
      if (obj['isSleepIn'] === '-') {
        delete obj['isSleepIn'];
      }
      delete obj['selectedItems'];
      delete obj['tempWorkingDays'];
      delete obj['tempGUID'];
      obj['leadId'] = this.serviceAgreementStepsParams.leadId;
      obj['customerId'] = this.serviceAgreementStepsParams.customerId;
      obj['partitionId'] = this.serviceAgreementStepsParams.partitionId;
      obj['quotationVersionId'] = this.serviceAgreementStepsParams.quotationVersionId;
      obj['contactNo'] = obj['contactNo'] ? obj['contactNo'].toString().replace(/\s/g, "") : null;
      obj['createdUserId'] = this.serviceAgreementStepsParams.createdUserId ? this.serviceAgreementStepsParams.createdUserId : this.loggedInUserData.userId;
      obj['modifiedUserId'] = this.loggedInUserData.userId;
      if (!Array.isArray(obj['days'])) {
        obj['days'] = obj['days'].split(',');
      }
      obj.addressId = (this.inputObject.fromComponentType == 'customer') ? this.inputObject.addressId :
        this.serviceAgreementStepsParams.addressId;
    });
    const payload = {
      sharedDomesticWorkerList: this.domesticWorkersArray
    }
    let crudService: Observable<IApplicationResponse> = this.inputObject.fromComponentType == 'lead' ?
      this.crudService.create(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_DOMESTIC_WORKERS, payload) :
      this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.DOMESTIC_WORKERS, payload);
    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.domesticWorkersForm = setRequiredValidator(this.domesticWorkersForm, ["domesticWorkerTypeId", "employeeName"]);
        this.createCheckBoxGroup();
        this.domesticWorkersForm.get('selectedItems').setErrors({ invalid: true });
        if (response.resources.sharedDomesticWorkerList) {
          response.resources.sharedDomesticWorkerList.forEach((domesticWorkersAddEditModel: DomesticWorkersAddEditModel, index: number) => {
            this.domesticWorkersArray[index].domesticWorkerId = domesticWorkersAddEditModel.domesticWorkerId;
            this.domesticWorkersArray[index].tempGUID = Guid.create()['value'];
          });
        }
        this.redirectToTab();
      }
      this.domesticWorkersArrayCopy = JSON.parse(JSON.stringify(this.domesticWorkersArray));
    });
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToProcessQuotationPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else {
        if (!this.currentAgreementTabObj.isMandatory || (this.domesticWorkersArray[0]?.domesticWorkerId &&
          this.currentAgreementTabObj.isMandatory)) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
    }
  }

  redirectToTab(type: btnActionTypes | string = btnActionTypes.SUBMIT): void {
    if (this.inputObject.fromComponentType == 'lead' && type == btnActionTypes.BACK_TO_LEAD) {
      this.rxjsService.navigateToProcessQuotationPage();
    }
    else if (this.inputObject.fromComponentType == 'customer' && type == btnActionTypes.BACK_TO_LEAD) {
      this.isDisabled = true
      this.getDomesticWorkersById();
    }
  }

  onEditBtnClicked() {
    if (this.inputObject.fromComponentType == "customer" && !this.permission.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.activatedRoute?.snapshot?.queryParams?.feature_name) {
      this.isDisabled = false;
      if (this.domesticWorkersArray.value[0]?.domesticWorkerId) {
        this.focusInAndOutFormArrayFields();
      }
    } else {
      this.rxjsService.setGlobalLoaderProperty(true);
      let filterObj = { userId: this.loggedInUserData?.userId, customerId: this.inputObject?.customerId, siteAddressId: this.inputObject?.addressId, uniqueCallId: this.uniqueCallId ? this.uniqueCallId : null };
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CHECK, null, false,
        prepareRequiredHttpParams(filterObj))
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.isDisabled = false;
            if (this.domesticWorkersArray?.value?.[0]?.domesticWorkerId) {
              this.focusInAndOutFormArrayFields();
            }
          } else {
            this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: this.inputObject?.customerId, siteAddressId: this.inputObject?.addressId, tab: CustomerVerificationType.DOMESTIC_WORKER, customerTab: 7, monitoringTab: 0, } })
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }
}
