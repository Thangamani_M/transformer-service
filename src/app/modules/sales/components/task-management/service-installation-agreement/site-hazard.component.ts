import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  btnActionTypes, CustomerModuleApiSuffixModels, CustomerVerificationType, loggedInUserData, SalesModuleApiSuffixModels,
  selectLeadCreationStepperParamsState$, selectLeadHeaderDataState$, SiteHazardAddEditModel
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  addFormControls,
  BreadCrumbModel,
  countryCodes, CrudService, CustomDirectiveConfig, defaultCountryCode, formConfigs, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControls,
  RxjsService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { ModuleName, ResponseMessageTypes } from '@app/shared/enums';
import { LeadHeaderData, ServiceAgreementStepsParams, SiteSpecificInputObjectModel } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
@Component({
  selector: 'app-site-hazard',
  templateUrl: './site-hazard.component.html'
})

export class SiteHazardComponent implements OnInit {
  @Input() inputObject = new SiteSpecificInputObjectModel();
  @Input() permission;
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  leadHeaderData: LeadHeaderData;
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  siteHazardAddEditForm: FormGroup;
  countryCodes = countryCodes;
  currentAgreementTabObj;
  loggedInUserData: LoggedInUserModel;
  isDisabled = false;
  breadCrumb: BreadCrumbModel;
  uniqueCallId;
  pScrollStyleHeight;

  constructor(private crudService: CrudService, private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute,
    public rxjsService: RxjsService, private store: Store<AppState>, private router: Router,private snackbarService : SnackbarService) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectLeadHeaderDataState$), this.store.select(loggedInUserData)]
    ).pipe(take(1)).subscribe((response) => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      this.leadHeaderData = response[1];
      this.loggedInUserData = new LoggedInUserModel(response[2]);
      this.currentAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['agreementTabName'].toLowerCase().includes('site'));
      if (this.leadHeaderData) {
        this.breadCrumb = {
          pageTitle: { key: 'Site Hazard' },
          items: [{
            key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
          }, { key: 'Service Agreement' }, { key: 'Site Hazard' }]
        };
      }
    });
  }

  ngOnInit(): void {
    this.isDisabled = this.inputObject.fromComponentType == 'lead' ? false : true;
    this.pScrollStyleHeight = this.inputObject.fromComponentType == 'lead' ? '41vh' : 'auto';
    this.createSiteHazardForm();
    this.onFormControlChanges();
  }

  onFormControlChanges(): void {
    this.siteHazardAddEditForm.get('isPremiseVisible').valueChanges.subscribe((isPremiseVisible: boolean) => {
      if (isPremiseVisible) {
        this.siteHazardAddEditForm = removeFormControls(this.siteHazardAddEditForm, ["description"]);
      }
      else {
        this.siteHazardAddEditForm = addFormControls(this.siteHazardAddEditForm, [{ controlName: "description" }]);
        this.siteHazardAddEditForm = setRequiredValidator(this.siteHazardAddEditForm, ['description']);
      }
    });
    this.siteHazardAddEditForm.get('isSwimmingPool').valueChanges.subscribe((isSwimmingPool: boolean) => {
      if (isSwimmingPool) {
        this.siteHazardAddEditForm = addFormControls(this.siteHazardAddEditForm, [{ controlName: "swimmingPoolLocation" }]);
        this.siteHazardAddEditForm = setRequiredValidator(this.siteHazardAddEditForm, ['swimmingPoolLocation']);
      }
      else {
        this.siteHazardAddEditForm = removeFormControls(this.siteHazardAddEditForm, ["swimmingPoolLocation"]);
      }
    });
    this.siteHazardAddEditForm.get('isSecurityOfficers').valueChanges.subscribe((isSecurityOfficers: boolean) => {
      if (isSecurityOfficers) {
        this.siteHazardAddEditForm = addFormControls(this.siteHazardAddEditForm, [
          { controlName: "noOfSecurityOfficers" }, { controlName: 'securityServiceProviderName' }, { controlName: "isSecurityProviderControlRoomOnSite", defaultValue: true },
          { controlName: 'controlRoomPhoneNoCountryCode', defaultValue: defaultCountryCode }, { controlName: "controlRoomPhoneNo" }]);
        this.siteHazardAddEditForm = setRequiredValidator(this.siteHazardAddEditForm, ['noOfSecurityOfficers',
          'securityServiceProviderName', "controlRoomPhoneNo"]);
      }
      else {
        this.siteHazardAddEditForm = removeFormControls(this.siteHazardAddEditForm, [
          "noOfSecurityOfficers", 'securityServiceProviderName', "controlRoomPhoneNoCountryCode", "controlRoomPhoneNo",
          "isSecurityProviderControlRoomOnSite"]);
      }
    });
  }

  getSiteHazardById(): void {
    let resultObservable = this.inputObject.fromComponentType == 'lead' ?
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SITE_HAZARD_SERVICES, undefined, false,
        prepareGetRequestHttpParams(undefined, undefined, {
          leadId: this.serviceAgreementStepsParams.leadId,
          quotationVersionId: this.serviceAgreementStepsParams.quotationVersionId,
          customerId: this.serviceAgreementStepsParams.customerId,
          partitionId: this.serviceAgreementStepsParams.partitionId,
          isExist: this.serviceAgreementStepsParams.isExist
        })) :
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SERVICE_SITE_HAZARD, undefined, false,
        prepareGetRequestHttpParams(undefined, undefined, {
          customerId: this.inputObject.customerId,
          addressId: this.inputObject.addressId,
          partitionId: this.inputObject.partitionId,
          quotationVersionId: this.inputObject.quotationVersionId
        }));
    resultObservable.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        if (Object.keys(response.resources ? response.resources : {}).length === 0) return;
        const siteHazardAddEditModel = new SiteHazardAddEditModel(response.resources);
        this.siteHazardAddEditForm.patchValue(siteHazardAddEditModel);
      }
      if (this.inputObject.fromComponentType == 'customer') {
        this.serviceAgreementStepsParams = new ServiceAgreementStepsParams({
          leadId: this.inputObject.leadId,
          customerId: this.inputObject.customerId,
          quotationVersionId: this.inputObject.quotationVersionId,
          createdUserId: this.loggedInUserData.userId,
          modifiedUserId: this.loggedInUserData.userId,
          partitionId: this.inputObject.partitionId,
        });
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createSiteHazardForm(): void {
    let siteHazardAddEditModel = new SiteHazardAddEditModel();
    this.siteHazardAddEditForm = this.formBuilder.group({});
    Object.keys(siteHazardAddEditModel).forEach((key) => {
      this.siteHazardAddEditForm.addControl(key, new FormControl(siteHazardAddEditModel[key]));
    });
    this.siteHazardAddEditForm = setRequiredValidator(this.siteHazardAddEditForm, ["swimmingPoolLocation",
      "noOfSecurityOfficers", 'securityServiceProviderName', "controlRoomPhoneNo"]);
    this.getSiteHazardById();
  }

  onSubmit(): void {
    if (this.siteHazardAddEditForm.invalid) {
      return;
    }
    this.siteHazardAddEditForm.value.createdUserId = this.serviceAgreementStepsParams.createdUserId;
    this.siteHazardAddEditForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.siteHazardAddEditForm.value.leadId = this.serviceAgreementStepsParams.leadId;
    this.siteHazardAddEditForm.value.customerId = this.serviceAgreementStepsParams.customerId;
    this.siteHazardAddEditForm.value.quotationVersionId = this.serviceAgreementStepsParams.quotationVersionId;
    this.siteHazardAddEditForm.value.partitionId = this.serviceAgreementStepsParams.partitionId;
    this.siteHazardAddEditForm.value.addressId = (this.inputObject.fromComponentType == 'customer') ? this.inputObject.addressId :
    this.serviceAgreementStepsParams.addressId;
    if (this.siteHazardAddEditForm.value.controlRoomPhoneNo) {
      this.siteHazardAddEditForm.value.controlRoomPhoneNo = this.siteHazardAddEditForm.value.controlRoomPhoneNo.toString().replace(/\s/g, "");
    }
    let crudService: Observable<IApplicationResponse> = this.inputObject.fromComponentType == 'lead' ?
      this.crudService.create(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_SITE_HAZARD_SERVICES, this.siteHazardAddEditForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.SERVICE_SITE_HAZARD, this.siteHazardAddEditForm.value);
    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        if (!this.siteHazardAddEditForm.value.siteHazardId) {
          this.siteHazardAddEditForm.get('siteHazardId').setValue(response.resources);
        }
        this.redirectToTab(btnActionTypes.NEXT);
      }
    });
  }

  redirectToTab(type: btnActionTypes | string = btnActionTypes.SUBMIT): void {
    if (this.inputObject.fromComponentType == 'lead' && type == btnActionTypes.BACK_TO_LEAD) {
      this.rxjsService.navigateToProcessQuotationPage();
    }
    else if (this.inputObject.fromComponentType == 'customer' && type == btnActionTypes.BACK_TO_LEAD) {
      this.isDisabled = true;
      this.getSiteHazardById();
    }
  }

  onEditBtnClicked() {
    if(this.inputObject.fromComponentType =="customer" && !this.permission.canEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.activatedRoute?.snapshot?.queryParams?.feature_name) {
      this.isDisabled = false;
    } else {
      this.rxjsService.setGlobalLoaderProperty(true);
      let filterObj = { userId: this.loggedInUserData?.userId, customerId: this.inputObject?.customerId, siteAddressId: this.inputObject?.addressId, uniqueCallId: this.uniqueCallId ? this.uniqueCallId : null };
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CHECK, null, false,
        prepareRequiredHttpParams(filterObj))
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.isDisabled = false;
          } else {
            this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: this.inputObject?.customerId, siteAddressId: this.inputObject?.addressId, tab: CustomerVerificationType.SITE_HAZARD, customerTab: 7, monitoringTab: 0, } })
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToProcessQuotationPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else {
        if ((this.siteHazardAddEditForm.get('siteHazardId')?.value &&
          this.currentAgreementTabObj.isMandatory) || !this.currentAgreementTabObj.isMandatory) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
    }
  }
}
