import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel, CrudService, debounceTimeForSearchkeyword, disableFormControls, disableWithDefaultFormControlValues, enableWithDefaultFormControlValues, formConfigs, generateCurrentYearToNext99Years, IApplicationResponse, ModulesBasedApiSuffix,
  monthsByNumber,
  prepareGetRequestHttpParams, removeProperyFromAnObjectWithDeepCopy, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { ModuleName, ResponseMessageTypes } from '@app/shared/enums';
import { selectStaticEagerLoadingCardTypesState$, selectStaticEagerLoadingTitlesState$ } from '@modules/others';
import {
  DebtorBankingDetailsModel,
  LeadCreationStepperParamsCreateAction, LeadHeaderData,
  SalesModuleApiSuffixModels, selectLeadCreationStepperParamsState$,
  selectLeadHeaderDataState$, ServiceAgreementStepsParams
} from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'installation-debtor-banking',
  templateUrl: './installation-debtor-banking-details.component.html',
  styleUrls: ['./service-installation-agreement.scss']
})

export class DebtorBankingDetailsComponent implements OnInit {
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  leadHeaderData: LeadHeaderData;
  currentAgreementTabObj;
  isExistingDebtor = false;
  isNewBankAccountCreated = false;
  details: any;
  monthsByNumber = monthsByNumber;
  years = generateCurrentYearToNext99Years();
  titles = [];
  banks = [];
  bankAccountTypes = [];
  cardTypes = [];
  selectedBranch;
  debtorBankingDetailsForm: FormGroup;
  formConfigs = formConfigs;
  IsInstallationDebtor: boolean = false;
  breadCrumb: BreadCrumbModel;
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;
  selectedBranchOption;

  constructor(private crudService: CrudService, private store: Store<AppState>,
    private formBuilder: FormBuilder, public rxjsService: RxjsService, private snackbarService: SnackbarService, private router: Router,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
    this.rxjsService.getIsInstallationDebtor().subscribe((IsInstallationDebtor: boolean) => {
      this.IsInstallationDebtor = IsInstallationDebtor;
      this.breadCrumb = {
        pageTitle: { key: 'Debtor Banking Details' },
        items: [{
          key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
        }, { key: this.IsInstallationDebtor ? 'Sales & Installation Agreement' : 'Service Agreement' }, { key: 'Debtor Banking Details' }]
      };
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectLeadHeaderDataState$), this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(selectStaticEagerLoadingCardTypesState$)]
    ).subscribe((response) => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      this.leadHeaderData = response[1];
      this.titles = response[2];
      this.cardTypes = response[3];
      this.currentAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['agreementTabName'].toLowerCase().includes('debtor banking details'));
    });
  }

  ngOnInit() {
    this.createDebtorForm();
    this.getForkJoinRequests();
    this.onFormControlChanges();
    if (this.serviceAgreementStepsParams.debtorTypeId == '1') {
      this.debtorBankingDetailsForm = setRequiredValidator(this.debtorBankingDetailsForm, ["companyName"]);
    }
    else {
      this.debtorBankingDetailsForm = disableWithDefaultFormControlValues(this.debtorBankingDetailsForm,
        [{ formControlName: 'companyName', value: '', shouldRemoveAllValidators: true }]);
    }
  }

  getForkJoinRequests() {
    forkJoin([this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.SALES_API_BANKS, undefined, false, prepareGetRequestHttpParams(null, null, {
    })), this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_ACCOUNTTYPE, undefined, false)])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
            switch (ix) {
              case 0:
                this.banks = respObj.resources;
                break;
              case 1:
                this.bankAccountTypes = respObj.resources;
                break;
            }
            if (ix == response.length - 1 && this.serviceAgreementStepsParams.debtorAdditionalInfoId) {
              this.getDebtorBankingDetailsById();
            }
            else if (ix == response.length - 1 && !this.serviceAgreementStepsParams.debtorAdditionalInfoId) {
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          }
        });
      });
  }

  kycDetails() {
    if (this.serviceAgreementStepsParams.debtorId && this.serviceAgreementStepsParams.customerId && this.debtorBankingDetailsForm.get('debtorAccountDetailId').value) {
      let queryParams = { queryParams: { customerId: this.leadHeaderData.customerId, debtorId: this.serviceAgreementStepsParams.debtorId, debtorAccountDetailId: this.debtorBankingDetailsForm.value.debtorAccountDetailId } };
      let urlSuffix = this.IsInstallationDebtor ? 'installation-agreement/credit-vetting-info' :
        'service-agreement/credit-vetting-info';
      if (this.leadHeaderData.fromUrl == 'Leads List') {
        this.router.navigate([`/sales/task-management/leads/${urlSuffix}`], queryParams);
      }
      else {
        this.router.navigate([`/my-tasks/my-sales/my-leads/${urlSuffix}`], queryParams);
      }
    } else {
      this.snackbarService.openSnackbar("Debtor ID Or Debtor Account details ID is required", ResponseMessageTypes.WARNING);
    }
  }

  viewResult() {
    if (this.serviceAgreementStepsParams.debtorId && this.debtorBankingDetailsForm.get('debtorAccountDetailId').value &&
      this.serviceAgreementStepsParams.customerId) {
      let queryParams = { queryParams: { customerId: this.leadHeaderData.customerId, debtorId: this.serviceAgreementStepsParams.debtorId, debtorAccountDetailId: this.debtorBankingDetailsForm.value.debtorAccountDetailId } };
      let urlSuffix = this.IsInstallationDebtor ? 'installation-agreement/credit-vetting-result' :
        'service-agreement/credit-vetting-result';
      if (this.leadHeaderData.fromUrl == 'Leads List') {
        this.router.navigate([`/sales/task-management/leads/${urlSuffix}`], queryParams);
      }
      else {
        this.router.navigate([`/my-tasks/my-sales/my-leads/${urlSuffix}`], queryParams);
      }
    } else {
      this.snackbarService.openSnackbar("Debtor ID Or Debtor Account details ID is required", ResponseMessageTypes.WARNING);
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  createDebtorForm() {
    let debtorBankingDetailsModel = new DebtorBankingDetailsModel();
    this.debtorBankingDetailsForm = this.formBuilder.group({});
    Object.keys(debtorBankingDetailsModel).forEach((key) => {
      this.debtorBankingDetailsForm.addControl(key,
        new FormControl(debtorBankingDetailsModel[key]));
    });
    this.debtorBankingDetailsForm = setRequiredValidator(this.debtorBankingDetailsForm, ["titleId", "firstName", "lastName", "accountNo", "bankId", "bankBranchId",
      "accountTypeId"]);
  }

  disabledKyc() {
    if (!this.isNewBankAccountCreated) {
      if (this.details?.systemOwnershipId) {
        if (this.details?.isDebtorAccountVetted && this.details?.isDebtorVetted) {
          return false;
        } else {
          return true;
        }
      } else {
        if (this.details?.isDebtorAccountVetted) {
          return false;
        } else {
          return true;
        }
      }
    } else {
      return true;
    }
  }

  getDebtorBankingDetailsById(type?: string) {
    if (type == 'revert changes') {
      this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to revert the changes?").
        onClose?.subscribe(dialogResult => {
          if (dialogResult) {
            this.getDebtorDetailsByIdAfterTypeChecking();
          }
        });
    }
    else {
      this.getDebtorDetailsByIdAfterTypeChecking();
    }
  }

  getDebtorDetailsByIdAfterTypeChecking() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_DEBTOR_BANKING, undefined, false, prepareGetRequestHttpParams(null, null, {
      debtorAdditionalInfoId: this.serviceAgreementStepsParams.debtorAdditionalInfoId ? this.serviceAgreementStepsParams.debtorAdditionalInfoId : '',
      leadId: this.serviceAgreementStepsParams.leadId,
      customerId: this.serviceAgreementStepsParams.customerId,
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.details = response.resources;
        this.serviceAgreementStepsParams.debtorId = this.details.debtorId;
        this.serviceAgreementStepsParams.isDebtorAccountVetted = this.details.isDebtorAccountVetted;
        this.serviceAgreementStepsParams.isDebtorVetted = this.details.isDebtorVetted;
        this.store.dispatch(new LeadCreationStepperParamsCreateAction({ serviceAgreementStepsParams: this.serviceAgreementStepsParams }));
        let copiedDetails = removeProperyFromAnObjectWithDeepCopy(this.details, ['isCardSelected', 'isBankAccount']);
        this.debtorBankingDetailsForm.patchValue(copiedDetails);
        if (this.details && this.details.debtorAccountDetailId && this.details.debtorAccountDetailId) {
          if (this.details.isExistingDebtor) {
            this.isExistingDebtor = true;
          }
          if (this.details.expiryDate) {
            let expiryDate = this.details.expiryDate.split("/");
            this.details.selectedMonth = expiryDate[0];
            this.details.selectedYear = expiryDate[1];
          }
          this.debtorBankingDetailsForm.get('debtorAdditionalInfoId').setValue(this.details.debtorAdditionalInfoId);
        }
        if (this.details && this.details.isBankAccount) {
          this.debtorBankingDetailsForm.get('isCardSelected').setValue(false);
          this.debtorBankingDetailsForm.get('isBankAccount').setValue(true);
          this.debtorBankingDetailsForm = disableFormControls(this.debtorBankingDetailsForm, ['cardTypeId', 'cardNo', 'selectedMonth', 'selectedYear']);
          this.debtorBankingDetailsForm = enableWithDefaultFormControlValues(this.debtorBankingDetailsForm, [{ formControlName: 'accountNo', isRequired: true, value: response.resources?.accountNo },
          { formControlName: 'bankId', isRequired: true, value: response.resources?.bankId }, { formControlName: 'bankBranchId', isRequired: true, value: null }, { formControlName: 'accountTypeId', value: response.resources?.accountTypeId, isRequired: true }])
          this.selectedBranchOption = { displayName: response.resources?.bankBranchName, id: response.resources?.bankBranchId };
          this.debtorBankingDetailsForm.get('bankBranchId').setValue(response.resources?.bankBranchName, { emitEvent: false, onlySelf: true });
        } else {
          this.debtorBankingDetailsForm.get('isCardSelected').setValue(true);
          this.debtorBankingDetailsForm = disableFormControls(this.debtorBankingDetailsForm, ['accountNo', 'bankId', 'bankBranchId', 'accountTypeId']);
          this.debtorBankingDetailsForm = enableWithDefaultFormControlValues(this.debtorBankingDetailsForm, [{ formControlName: 'cardTypeId', isRequired: true, value: response.resources?.cardTypeId },
          { formControlName: 'cardNo', isRequired: true, value: response.resources?.cardNo }, { formControlName: 'selectedMonth', isRequired: true, value: this.details.selectedMonth }, { formControlName: 'selectedYear', isRequired: true, value: this.details.selectedYear }])
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onFormControlChanges(): void {
    this.getBranchesByBankIdAndBranchKeyword();
    this.debtorBankingDetailsForm.get('isBankAccount').valueChanges.subscribe((isBankAccount: boolean) => {
      if (isBankAccount) {
        this.prepareBankingRelatedFormControls('isBankAccount');
      }
    });
    this.debtorBankingDetailsForm.get('isCardSelected').valueChanges.subscribe((isCardSelected: boolean) => {
      if (isCardSelected) {
        this.prepareBankingRelatedFormControls('isCardSelected');
      }
    });
  }

  getBranchesByBankIdAndBranchKeyword(): void {
    this.debtorBankingDetailsForm.get('bankBranchId')
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((searchtext) => {
          if (!searchtext) {
            this.getLoopableObjectRequestObservable = of();
          }
          else {
            this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_BANK_BRANCHES, undefined, false,
              prepareGetRequestHttpParams(null, null, { bankId: this.debtorBankingDetailsForm.get('bankId').value, searchText: searchtext }));
          }
        });
  }

  onSelectedBranchOption(selectedObject): void {
    if (!selectedObject) return;
    this.selectedBranchOption = selectedObject;
  }

  prepareBankingRelatedFormControls(typeOfBankingDetails: string) {
    if (typeOfBankingDetails == 'isBankAccount') {
      this.debtorBankingDetailsForm.get('isCardSelected').setValue(false);
      this.debtorBankingDetailsForm = disableWithDefaultFormControlValues(this.debtorBankingDetailsForm,
        [{ formControlName: 'cardTypeId', value: null, shouldRemoveAllValidators: true }, { formControlName: 'cardNo', value: '', shouldRemoveAllValidators: true },
        { formControlName: 'selectedMonth', value: null, shouldRemoveAllValidators: true }, { formControlName: 'selectedYear', value: null, shouldRemoveAllValidators: true }]);
      this.debtorBankingDetailsForm = enableWithDefaultFormControlValues(this.debtorBankingDetailsForm, [{ formControlName: 'accountNo', isRequired: true, value: null },
      { formControlName: 'bankId', isRequired: true, value: null }, { formControlName: 'bankBranchId', isRequired: true, value: null }, { formControlName: 'accountTypeId', isRequired: true, value: null }])
    }
    else {
      this.debtorBankingDetailsForm.get('isBankAccount').setValue(false);
      this.debtorBankingDetailsForm = disableWithDefaultFormControlValues(this.debtorBankingDetailsForm,
        [{ formControlName: 'accountNo', value: '', shouldRemoveAllValidators: true }, { formControlName: 'bankId', value: null, shouldRemoveAllValidators: true },
        { formControlName: 'bankBranchId', value: null, shouldRemoveAllValidators: true },
        { formControlName: 'accountTypeId', value: null, shouldRemoveAllValidators: true }]);
      this.debtorBankingDetailsForm = enableWithDefaultFormControlValues(this.debtorBankingDetailsForm, [{ formControlName: 'cardTypeId', isRequired: true, value: null },
      { formControlName: 'cardNo', isRequired: true, value: null }, { formControlName: 'selectedMonth', isRequired: true, value: null }, { formControlName: 'selectedYear', isRequired: true, value: null }])
    }
  }

  createNewBankAccount() {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to add new bank account?").
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) {
          this.isExistingDebtor = true;
          return;
        }
        this.debtorBankingDetailsForm.reset();
        this.isExistingDebtor = false;
        this.isNewBankAccountCreated = true;
        this.debtorBankingDetailsForm.get('isBankAccount').setValue(true);
      });
  }

  onSubmit() {
    this.debtorBankingDetailsForm.get('isInvoiceRequired').setValue(this.debtorBankingDetailsForm.get('isInvoiceRequired').value);
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.debtorBankingDetailsForm.invalid) {
      return;
    }
    if (!this.serviceAgreementStepsParams.debtorAdditionalInfoId) {
      this.snackbarService.openSnackbar("Debtor ID is not present", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.debtorBankingDetailsForm.value.isExistingDebtor && this.isNewBankAccountCreated) {
      this.debtorBankingDetailsForm.get('debtorAccountDetailId').setValue(null);
    }
    if (this.debtorBankingDetailsForm.get('isBankAccount').value) {
      this.debtorBankingDetailsForm.value.bankBranchId = this.selectedBranchOption.id;
    }
    this.debtorBankingDetailsForm.value.cardNo = this.debtorBankingDetailsForm.value.cardNo && this.debtorBankingDetailsForm.value.cardNo.replace(/\s/g, '');
    this.debtorBankingDetailsForm.value.leadId = this.serviceAgreementStepsParams.leadId;
    this.debtorBankingDetailsForm.value.debtorId = this.serviceAgreementStepsParams.debtorId;
    this.debtorBankingDetailsForm.value.createdUserId = this.serviceAgreementStepsParams.createdUserId;
    this.debtorBankingDetailsForm.value.customerId = this.serviceAgreementStepsParams.customerId;
    this.debtorBankingDetailsForm.value.debtorTypeId = this.serviceAgreementStepsParams.debtorTypeId;
    this.debtorBankingDetailsForm.value.contractTypeId = this.serviceAgreementStepsParams.contractType;
    this.debtorBankingDetailsForm.value.debtorAdditionalInfoId = this.serviceAgreementStepsParams.debtorAdditionalInfoId;
    this.debtorBankingDetailsForm.value.quotationVersionId = this.serviceAgreementStepsParams.quotationVersionId;
    if (this.debtorBankingDetailsForm.value.isCardSelected) {
      this.debtorBankingDetailsForm.value.expiryDate = `${this.debtorBankingDetailsForm.value.selectedMonth}/${this.debtorBankingDetailsForm.value.selectedYear}`
    }
    let payload = this.debtorBankingDetailsForm.value;
    payload.systemOwnershipId = this.details.systemOwnershipId;
  
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_DEBTOR_BANKING, payload).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        if (!this.debtorBankingDetailsForm.value.debtorAccountDetailId) {
          this.debtorBankingDetailsForm.get('debtorAccountDetailId').setValue(response.resources.debtorAccountDetailId);
        }
        this.debtorBankingDetailsForm.get('isOnlineDebtorAccepted').setValue(true);
      }
    });
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
      }
      else {
        if (!this.currentAgreementTabObj.isMandatory) {
          this.prepareContractCreationUrl();
        }
        else if ((this.debtorBankingDetailsForm.get('debtorAccountDetailId').value &&
          this.currentAgreementTabObj.isMandatory)) {
          if (this.debtorBankingDetailsForm.get('isOnlineDebtorAccepted').value) {
            this.prepareContractCreationUrl();
          }
          else {
            this.snackbarService.openSnackbar('Kindly click Save to continue', ResponseMessageTypes.WARNING);
          }
        }
      }
    }
  }

  prepareContractCreationUrl() {
    let urlSuffix = this.IsInstallationDebtor ? 'installation-agreement/checklist' :
      'service-agreement/checklist';
    if (this.leadHeaderData.fromUrl == 'Leads List') {
      this.router.navigateByUrl(`/sales/task-management/leads/${urlSuffix}`);
    }
    else {
      this.router.navigateByUrl(`/my-tasks/my-sales/my-leads/${urlSuffix}`);
    }
  }
}