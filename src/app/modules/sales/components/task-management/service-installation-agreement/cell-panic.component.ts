import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
  loggedInUserData, SalesModuleApiSuffixModels,
  selectLeadCreationStepperParamsState$, selectLeadHeaderDataState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel, countryCodes, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, formConfigs, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  onBlurActionOnDuplicatedFormControl,
  onFormControlChangesToFindDuplicate,
  prepareGetRequestHttpParams, prepareRequiredHttpParams,
  ReusablePrimeNGTableFeatureService,
  RxjsService
} from '@app/shared';
import { ModuleName } from '@app/shared/enums';
import { CellPanicAddEditModel, LeadHeaderData, ServiceAgreementStepsParams } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'cell-panic',
  templateUrl: './cell-panic.component.html',
  styleUrls: ['./service-installation-agreement.scss']
})

export class CellPanicComponent implements OnInit {
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  leadHeaderData: LeadHeaderData;
  formConfigs = formConfigs;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  cellPanicAddEditForm: FormGroup;
  countryCodes = countryCodes;
  currentAgreementTabObj;
  loggedInUserData: LoggedInUserModel;
  isDisabled = false;
  cellPanicId = "";
  cellPanicArray: FormArray;
  breadCrumb: BreadCrumbModel;
  @ViewChildren('input') rows: QueryList<any>;
  @ViewChildren('contactRows',) contactRows: QueryList<ElementRef>;

  constructor(private crudService: CrudService, private formBuilder: FormBuilder, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    public rxjsService: RxjsService, private store: Store<AppState>, private router: Router) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectLeadHeaderDataState$), this.store.select(loggedInUserData)]
    ).pipe(take(1)).subscribe((response) => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      this.leadHeaderData = response[1];
      this.loggedInUserData = new LoggedInUserModel(response[2]);
      this.currentAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['agreementTabName'].toLowerCase().includes('cell panic'));
      this.breadCrumb = {
        pageTitle: { key: 'Cell Panic' },
        items: [{
          key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
        }, { key: 'Service Agreement' }, { key: 'Cell Panic' }]
      };
    });
  }

  ngOnInit(): void {
    this.createSiteHazardForm();
    this.cellPanicArray = this.getCellPanic;
    this.cellPanicArray.push(this.createPanicArray());
    this.getCellPanicById().subscribe((response: IApplicationResponse) => {
      if (response.resources && response.isSuccess && response.statusCode == 200) {
        if (response.resources.length > 0) {
          this.focusAllFields();
        }
        let cellPanic = new CellPanicAddEditModel(response.resources);
        this.cellPanicAddEditForm.patchValue(cellPanic);
        this.cellPanicArray = this.getCellPanic;
        if (response.resources.length) {
          this.cellPanicArray.removeAt(this.cellPanicArray.length - 1);
        }
        response.resources.forEach((cellPanicAddEditModel: CellPanicAddEditModel) => {
          this.cellPanicArray.push(this.createPanicArray(cellPanicAddEditModel));
        });
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  focusAllFields() {
    setTimeout(() => {
      this.focusInAndOutFormArrayFields();
    }, 100);
  }

  createSiteHazardForm(): void {
    this.cellPanicAddEditForm = this.formBuilder.group({
      cellPanicArray: this.formBuilder.array([]),
    });
  }

  get getCellPanic(): FormArray {
    if (this.cellPanicAddEditForm) {
      return this.cellPanicAddEditForm.get("cellPanicArray") as FormArray;
    }
  }

  createPanicArray(usefulNumberContactListModel?: CellPanicAddEditModel): FormGroup {
    let usefulNumbersContactListFormControlModel = new CellPanicAddEditModel(usefulNumberContactListModel);
    let formControls = {};
    Object.keys(usefulNumbersContactListFormControlModel).forEach((key) => {
      if (key === 'userName' || key === 'mobileNo') {
        formControls[key] = [{ value: usefulNumbersContactListFormControlModel[key], disabled: false }, [Validators.required]]
      } else if (this.cellPanicId) {
        formControls[key] = [{ value: usefulNumbersContactListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: usefulNumbersContactListFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  focusInAndOutFormArrayFields(): void {
    this.contactRows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    });
  }

  addCellPanic() {
    if (this.getCellPanic.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    if (this.cellPanicAddEditForm.invalid) return;
    this.cellPanicArray = this.getCellPanic;
    let salesChannel = new CellPanicAddEditModel();
    this.cellPanicArray.insert(0, this.createPanicArray(salesChannel));
  }

  onFormControlChanges(formControlName: string, formArray: FormArray, formArrayName: string, currentCursorIndex: number): void {
    onFormControlChangesToFindDuplicate(formControlName, formArray, formArrayName, currentCursorIndex, this.cellPanicAddEditForm);
  }

  onBlur(formControlName: string, formArray: FormArray, e, index: number) {
    onBlurActionOnDuplicatedFormControl(formControlName, formArray, e, index);
  }

  removeCellPanic(i: number): void {
    if (!this.cellPanicArray.controls[i].value.cellPanicId) {
      this.cellPanicArray.removeAt(i);
      return;
    }
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, null, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) return;
        if (this.cellPanicArray.controls[i].value.cellPanicId && this.cellPanicArray.length > 1) {
          this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_CELL_PANIC, undefined,
            prepareRequiredHttpParams({
              id: this.cellPanicArray.controls[i].value.cellPanicId,
              isDeleted: true,
              modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
            }), 1)
            .subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                this.cellPanicArray.removeAt(i);
                this.rxjsService.setGlobalLoaderProperty(false);
              }
              if (this.cellPanicArray.length === 0) {
                this.addCellPanic();
              };
            });
        }
        else {
          this.cellPanicArray.removeAt(i);
        }
      });
  }

  getCellPanicById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_CELL_PANIC,
      undefined,
      null,
      prepareGetRequestHttpParams(null, null, {
        leadId: this.serviceAgreementStepsParams.leadId,
        quotationVersionId: this.serviceAgreementStepsParams.quotationVersionId,
        customerId: this.serviceAgreementStepsParams.customerId,
      })
    );
  }

  onSubmit(): void {
    if (this.cellPanicAddEditForm.invalid) {
      return;
    }
    this.cellPanicAddEditForm.value.cellPanicArray.forEach(element => {
      element.createdUserId = this.serviceAgreementStepsParams.createdUserId;
      element.modifiedUserId = this.loggedInUserData.userId;
      element.partitionId = this.serviceAgreementStepsParams.partitionId;
      element.customerId = this.serviceAgreementStepsParams.customerId;
      element.quotationVersionId = this.serviceAgreementStepsParams.quotationVersionId;
      element.contractId = this.serviceAgreementStepsParams.contractId;
      if (element.mobileNo) {
        element.mobileNo = element.mobileNo.toString().replace(/\s/g, "");
      }
    });
    this.crudService.create(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_CELL_PANIC, this.cellPanicAddEditForm.value.cellPanicArray, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          if (response.resources) {
            response.resources.forEach((cellPanicAddEditModel: CellPanicAddEditModel, index: number) => {
              this.cellPanicArray.controls[index].get('cellPanicId').setValue(cellPanicAddEditModel.cellPanicId);
            });
          }
        }
      });
  }

  redirectToTab(): void {
    this.rxjsService.navigateToProcessQuotationPage();
  }

  onEditBtnClicked() {
    this.isDisabled = false;
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToProcessQuotationPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else {
        if (!this.currentAgreementTabObj.isMandatory || (this.cellPanicArray.controls[0].get('cellPanicId').value &&
          this.currentAgreementTabObj.isMandatory)) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
    }
  }
}
