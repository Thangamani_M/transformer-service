import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AccessToPremisesAddEditModel, btnActionTypes, CustomerModuleApiSuffixModels, CustomerVerificationType, SalesModuleApiSuffixModels,
  selectLeadCreationStepperParamsState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel, clearFormControlValidators, DynamicConfirmByMessageConfirmationType, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams,
  prepareRequiredHttpParams, ReusablePrimeNGTableFeatureService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { ModuleName, ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { DigiPadModel, LeadHeaderData, LockBoxesModel, ServiceAgreementStepsParams, SiteSpecificInputObjectModel } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { selectLeadHeaderDataState$ } from '../lead-creation-ngrx-files';
@Component({
  selector: 'app-access-to-premises',
  templateUrl: './access-to-premises.component.html',
  styleUrls: ['./service-installation-agreement.scss']
})

export class AccessToPremisesComponent implements OnInit {
  @Input() inputObject = new SiteSpecificInputObjectModel();
  @Input() permission;
  isDisabled = false;
  formConfigs = formConfigs;
  accessPremisesForm: FormGroup;
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  leadHeaderData: LeadHeaderData;
  @ViewChildren('input') rows: QueryList<any>;
  details;
  lockBoxes: FormArray;
  digipads: FormArray;
  currentAgreementTabObj;
  loggedInUserData: LoggedInUserModel;
  breadCrumb: BreadCrumbModel;
  uniqueCallId;
  pScrollStyleHeight;

  constructor(private crudService: CrudService, private router: Router, private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    public rxjsService: RxjsService, private store: Store<AppState>, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService
  ) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD)
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectLeadHeaderDataState$), this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      this.leadHeaderData = response[1];
      this.loggedInUserData = new LoggedInUserModel(response[2]);
      this.currentAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['agreementTabName'].toLowerCase().includes('access'));
      if (this.leadHeaderData) {
        this.breadCrumb = {
          pageTitle: { key: 'Access To Premises' },
          items: [{
            key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
          }, { key: 'Service Agreement' }, { key: 'Access To Premises' }]
        };
      }
    });
  }

  ngOnInit(): void {
    this.isDisabled = this.inputObject.fromComponentType == 'lead' ? false : true;
    this.pScrollStyleHeight = this.inputObject.fromComponentType == 'lead' ? '43vh' : 'auto';
    this.getAccessPremisesByLeadAndPartitionId();
    this.createAccessPremisesForm();
    this.onFormControlChanges();
  }

  onFormControlChanges(): void {
    this.accessPremisesForm.get('isArmedResponsePadlock').valueChanges.subscribe((isPadlock: boolean) => {
      if (isPadlock) {
        this.accessPremisesForm.get("isPadlockOnGate").setValue(null);
        this.accessPremisesForm.get("isPadlockOnGate").enable();
        this.accessPremisesForm.get("padlockDescription").enable();
        this.accessPremisesForm = setRequiredValidator(this.accessPremisesForm, ['isPadlockOnGate']);
      }
      else {
        this.accessPremisesForm.get("isPadlockOnGate").disable({ emitEvent: false });
        this.accessPremisesForm.get("padlockDescription").disable();
        this.accessPremisesForm.get("isPadlockOnGate").setValue(null);
        this.accessPremisesForm.get("padlockDescription").setValue(null);
        this.accessPremisesForm = clearFormControlValidators(this.accessPremisesForm, ['isPadlockOnGate']);
      }
    });
    this.accessPremisesForm.get('isBarrelLockFitted').valueChanges.subscribe((isBarrelLock: boolean) => {
      if (isBarrelLock) {
        this.accessPremisesForm.get("isBarrelLockFittedOnGate").setValue(null);
        this.accessPremisesForm.get("barrelLockFittedDescription").enable();
        this.accessPremisesForm.get("isBarrelLockFittedOnGate").enable();
        this.accessPremisesForm = setRequiredValidator(this.accessPremisesForm, ['isBarrelLockFittedOnGate']);
      }
      else {
        this.accessPremisesForm.get("isBarrelLockFittedOnGate").disable({ emitEvent: false });
        this.accessPremisesForm.get("barrelLockFittedDescription").disable();
        this.accessPremisesForm.get("isBarrelLockFittedOnGate").setValue(null);
        this.accessPremisesForm.get("barrelLockFittedDescription").setValue(null);
        this.accessPremisesForm = clearFormControlValidators(this.accessPremisesForm, ['isBarrelLockFittedOnGate']);
      }
    });
    this.accessPremisesForm.get('isDigipadOnGate').valueChanges.subscribe((isDigipad: boolean) => {
      if (isDigipad) {
        if (this.digipads != null) {
          this.digipads.clear();
          this.digipads = this.getDigiPadListArray;
          this.digipads.push(this.createDigipadListModel());
        }
        this.accessPremisesForm.get("digipadDescription").enable();
      }
      else {
        if (this.digipads != null) {
          this.digipads.clear();
        }
        this.accessPremisesForm.get("digipadDescription").disable();
      }
    });
    this.accessPremisesForm.get('isLockBox').valueChanges.subscribe((isLockBox: boolean) => {
      if (isLockBox) {
        if (this.lockBoxes != null) {
          this.lockBoxes.clear();
          this.lockBoxes = this.getLockBoxesListArray;
          this.lockBoxes.push(this.createLockBoxesListModel());
        }
        this.accessPremisesForm.get("lockBoxDescription").enable();
      }
      else {
        if (this.lockBoxes != null) {
          this.lockBoxes.clear();
        }
        this.accessPremisesForm.get("lockBoxDescription").disable();
      }
    });
    this.accessPremisesForm.get('isOpenAccess').valueChanges.subscribe((isOpenAccess: boolean) => {
      if (isOpenAccess) {
        this.accessPremisesForm.get("openAccessDescription").enable();
      } else {
        this.accessPremisesForm.get("openAccessDescription").disable();
      }
    });
    this.accessPremisesForm.get('isSecureGate').valueChanges.subscribe((isSecureGate: boolean) => {
      if (isSecureGate) {
        this.accessPremisesForm.get("secureGateDescription").enable();
        this.accessPremisesForm.get("transmitterId").enable();
      } else {
        this.accessPremisesForm.get("secureGateDescription").disable();
        this.accessPremisesForm.get("transmitterId").disable();
      }
    });
    this.accessPremisesForm.get('isArmedResponseReceiverOnGate').valueChanges.subscribe((isArmedResponseReceiverOnGate: boolean) => {
      if (isArmedResponseReceiverOnGate) {
        this.accessPremisesForm.get("armedResponseReceiverOnGateDescription").enable();
        this.accessPremisesForm.get("receiverId").enable();
      } else {
        this.accessPremisesForm.get("armedResponseReceiverOnGateDescription").disable();
        this.accessPremisesForm.get("receiverId").disable();
      }
    });
    this.accessPremisesForm.get('isAuthroziedNoAccess').valueChanges.subscribe((isAuthroziedNoAccess: boolean) => {
      if (isAuthroziedNoAccess) {
        this.accessPremisesForm.get("authroziedNoAccessDescription").enable();
      } else {
        this.accessPremisesForm.get("authroziedNoAccessDescription").disable();
      }
    });
    this.accessPremisesForm.get('isUnProtectWall').valueChanges.subscribe((isUnProtectWall: boolean) => {
      if (isUnProtectWall) {
        this.accessPremisesForm.get("unProtectWallDescription").enable();
      } else {
        this.accessPremisesForm.get("unProtectWallDescription").disable();
      }
    });
  }

  getAccessPremisesByLeadAndPartitionId(): void {
    let resultObservable = this.inputObject.fromComponentType == 'lead' ?
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ACCESS_PREMISES_LEAD_DETAILS, undefined, false,
        prepareGetRequestHttpParams(undefined, undefined, {
          leadId: this.serviceAgreementStepsParams.leadId,
          quotationVersionId: this.serviceAgreementStepsParams.quotationVersionId,
          customerId: this.serviceAgreementStepsParams.customerId,
          partitionId: this.serviceAgreementStepsParams.partitionId,
          isExist: this.serviceAgreementStepsParams.isExist
        })) :
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.ACCESS_TO_PREMISES, undefined, false,
        prepareGetRequestHttpParams(undefined, undefined, {
          customerId: this.inputObject.customerId,
          addressId: this.inputObject.addressId,
          partitionId: this.inputObject.partitionId,
          quotationVersionId: this.inputObject.quotationVersionId
        }));
    resultObservable.subscribe((response: IApplicationResponse) => {
      if (response.statusCode == 200 && response.resources && response.isSuccess) {
        this.details = response.resources;
        if (!this.details.isArmedResponsePadlock) {
          this.details.isPadlockOnGate = null;
        }
        if (!this.details.isBarrelLockFitted) {
          this.details.isBarrelLockFittedOnGate = null;
        }
        if (!this.details.digipads) {
          this.details.digipads = [];
        }
        if (!this.details.lockBoxes) {
          this.details.lockBoxes = [];
        }
        if (this.details.accessPremiseId || this.details.customerId) {
          this.accessPremisesForm.patchValue(this.details);
          this.digipads = this.getDigiPadListArray;
          this.lockBoxes = this.getLockBoxesListArray;
          if (response.resources.digipads.length > 0) {
            response.resources.digipads.forEach((dg) => {
              this.digipads.push(this.createDigipadListModel(dg));
            });
          }
          else {
            this.digipads.push(this.createDigipadListModel());
          }
          if (response.resources.lockBoxes.length > 0) {
            response.resources.lockBoxes.forEach((md) => {
              this.lockBoxes.push(this.createLockBoxesListModel(md));
            });
          }
          else {
            this.lockBoxes.push(this.createLockBoxesListModel());
          }
        } else {
          this.digipads = this.getDigiPadListArray;
          this.digipads.push(this.createDigipadListModel());

          this.lockBoxes = this.getLockBoxesListArray;
          this.lockBoxes.push(this.createLockBoxesListModel());
        }
      } else {
        this.digipads = this.getDigiPadListArray;
        this.digipads.push(this.createDigipadListModel());

        this.lockBoxes = this.getLockBoxesListArray;
        this.lockBoxes.push(this.createLockBoxesListModel());
      }
      if (this.inputObject.fromComponentType == 'customer') {
        this.serviceAgreementStepsParams = new ServiceAgreementStepsParams({
          leadId: this.inputObject.leadId,
          customerId: this.inputObject.customerId,
          quotationVersionId: this.inputObject.quotationVersionId,
          createdUserId: this.loggedInUserData.userId,
          modifiedUserId: this.loggedInUserData.userId,
          partitionId: this.inputObject.partitionId,
        });
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createAccessPremisesForm(): void {
    let accessToPremisesAddEditModel = new AccessToPremisesAddEditModel();
    this.accessPremisesForm = this.formBuilder.group({
      digipads: this.formBuilder.array([]),
      lockBoxes: this.formBuilder.array([])
    });
    Object.keys(accessToPremisesAddEditModel).forEach((key) => {
      if (key == 'lockBoxDescription' || key == 'armedResponseReceiverOnGateDescription' || key == 'authroziedNoAccessDescription' || key == 'unProtectWallDescription' || key == 'lockBoxDescription' || key == 'openAccessDescription' || key == 'secureGateDescription' || key == 'digipadDescription' || key == 'barrelLockFittedDescription' || key == 'padlockDescription') {
        this.accessPremisesForm.addControl(key, new FormControl({ value: accessToPremisesAddEditModel[key], disabled: true }));
      } else {
        this.accessPremisesForm.addControl(key, new FormControl(accessToPremisesAddEditModel[key]));
      }
    });
    this.accessPremisesForm.get("isPadlockOnGate").setValue(null);
    this.accessPremisesForm.get("isBarrelLockFittedOnGate").setValue(null);
    this.accessPremisesForm.get("isPadlockOnGate").disable();
    this.accessPremisesForm.get("isBarrelLockFittedOnGate").disable()
  }

  addLockBox() {
    if (this.lockBoxes.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.lockBoxes = this.getLockBoxesListArray;
    let lockBoxesModel = new LockBoxesModel();
    this.lockBoxes.insert(0, this.createLockBoxesListModel(lockBoxesModel));
  }

  addDigipad() {
    if (this.digipads.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.digipads = this.getDigiPadListArray;
    let digiPadModel = new DigiPadModel();
    this.digipads.insert(0, this.createDigipadListModel(digiPadModel));
  }

  get getDigiPadListArray(): FormArray {
    if (!this.accessPremisesForm) { return };
    return this.accessPremisesForm.get("digipads") as FormArray;
  }

  //Create FormArray controls
  createDigipadListModel(digipad?: DigiPadModel): FormGroup {
    let digipadFormControlModel = new DigiPadModel(digipad);
    let formControls = {};
    Object.keys(digipadFormControlModel).forEach((key) => {
      if (key != 'accessPremiseDigipadId') {
        formControls[key] = [{ value: digipadFormControlModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: digipadFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  get getLockBoxesListArray(): FormArray {
    if (!this.accessPremisesForm) return;
    return this.accessPremisesForm.get("lockBoxes") as FormArray;
  }

  removeLockBox(i) {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, null, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) return;
        if (this.getLockBoxesListArray.controls[i].value.accessPremiseLockBoxId && this.getLockBoxesListArray.length > 1) {
          let resultObservable = this.inputObject.fromComponentType == 'lead' ?
            this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_DELETE_LOCKBOX,
              undefined,
              prepareRequiredHttpParams({
                Ids: this.getLockBoxesListArray.controls[i].value.accessPremiseLockBoxId,
                IsDeleted: false,
                modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
              })) :
            this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.LOCKBOX,
              undefined,
              prepareRequiredHttpParams({
                Ids: this.getLockBoxesListArray.controls[i].value.accessPremiseLockBoxId,
                IsDeleted: false,
                modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
              }));
          resultObservable.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getLockBoxesListArray.removeAt(i);
            }
            if (this.getLockBoxesListArray.length === 0) {
              this.addLockBox();
            };
          });
        }
        else {
          this.getLockBoxesListArray.removeAt(i);
          this.rxjsService.setFormChangeDetectionProperty(true);
        }
      });
  }

  removeDigipad(i) {
    if (!this.permission?.canRowDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, null, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) return;
        if (this.getDigiPadListArray.controls[i].value.accessPremiseDigipadId && this.getDigiPadListArray.length > 1) {
          let resultObservable = this.inputObject.fromComponentType == 'lead' ?
            this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_DELETE_DIGIPAD,
              undefined,
              prepareRequiredHttpParams({
                Ids: this.getDigiPadListArray.controls[i].value.accessPremiseDigipadId,
                IsDeleted: false,
                modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
              })) :
            this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.DIGIPAD,
              undefined,
              prepareRequiredHttpParams({
                Ids: this.getDigiPadListArray.controls[i].value.accessPremiseDigipadId,
                IsDeleted: false,
                modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
              }));
          resultObservable.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getDigiPadListArray.removeAt(i);
            }
            if (this.getDigiPadListArray.length === 0) {
              this.addDigipad();
            };
          });
        }
        else {
          this.getDigiPadListArray.removeAt(i);
          this.rxjsService.setFormChangeDetectionProperty(true);
        }
      });
  }

  //Create FormArray controls
  createLockBoxesListModel(lockBoxesModel?: LockBoxesModel): FormGroup {
    let lockBoxesModelFormControlModel = new LockBoxesModel(lockBoxesModel);
    let formControls = {};
    Object.keys(lockBoxesModelFormControlModel).forEach((key) => {
      if (key != 'accessPremiseLockBoxId') {
        formControls[key] = [{ value: lockBoxesModelFormControlModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: lockBoxesModelFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  onSubmit(): void {
    if (!this.accessPremisesForm.value.isDigipadOnGate) {
      if (this.digipads) {
        this.digipads.clear();
      }
    }
    if (!this.accessPremisesForm.value.isLockBox) {
      if (this.lockBoxes) {
        this.lockBoxes.clear();
      }
    }
    if (this.accessPremisesForm.invalid) {
      return;
    }
    this.accessPremisesForm.value.createdUserId = this.serviceAgreementStepsParams.createdUserId;
    this.accessPremisesForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.accessPremisesForm.value.leadId = this.leadHeaderData?.leadId ? this.leadHeaderData.leadId :
      this.serviceAgreementStepsParams.leadId;
    this.accessPremisesForm.value.customerId = this.serviceAgreementStepsParams.customerId;
    this.accessPremisesForm.value.quotationVersionId = this.serviceAgreementStepsParams.quotationVersionId;
    this.accessPremisesForm.value.partitionId = this.serviceAgreementStepsParams.partitionId;
    this.accessPremisesForm.value.addressId = (this.inputObject.fromComponentType == 'customer') ? this.inputObject.addressId :
      this.serviceAgreementStepsParams.addressId;
    let payload = this.accessPremisesForm.value;
    if (!payload.isDigipadOnGate) {
      payload.digipads = null;
    }
    if (!payload.accessPremiseId) {
      this.rxjsService.setFormChangeDetectionProperty(true);
    }
    if (!payload.isLockBox) {
      payload.lockBoxes = null;
    }
    if (!payload.isArmedResponsePadlock && !payload.isPadlockOnGate && !payload.isBarrelLockFitted && !payload.isArmedResponsePadlock && !payload.isBarrelLockFittedOnGate &&
      !payload.isSecureGate && !payload.isArmedResponseReceiverOnGate && !payload.isUnProtectWall && !payload.isOpenAccess && !payload.isAuthroziedNoAccess && !payload.isDigipadOnGate &&
      !payload.isLockBox && !payload.siteInstructionForAROfficer) {
      this.snackbarService.openSnackbar("Atleast one of the above should be selected/Filled..!!", ResponseMessageTypes.WARNING);
      return;
    }
    let resultObservable = this.inputObject.fromComponentType == 'lead' ?
      this.crudService.create(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.ACCESS_PREMISES, payload) :
      this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.ACCESS_TO_PREMISES, payload);
    resultObservable.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        if (!this.accessPremisesForm.value.accessPremiseId) {
          this.accessPremisesForm.get('accessPremiseId').setValue(response.resources);
        }
      }
    });
  }

  redirectToTab(type: btnActionTypes | string = btnActionTypes.SUBMIT): void {
    if (this.inputObject.fromComponentType == 'lead' && type == btnActionTypes.BACK_TO_LEAD) {
      this.rxjsService.navigateToProcessQuotationPage();
    }
    else if (this.inputObject.fromComponentType == 'customer' && type == btnActionTypes.BACK_TO_LEAD) {
      this.isDisabled = true;
      this.getAccessPremisesByLeadAndPartitionId();
    }
  }

  onEditBtnClicked() {
    if (this.inputObject.fromComponentType == "customer" && !this.permission.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.activatedRoute?.snapshot?.queryParams?.feature_name) {
      this.isDisabled = false;
    } else {
      this.rxjsService.setGlobalLoaderProperty(true);
      let filterObj = { userId: this.loggedInUserData?.userId, customerId: this.inputObject?.customerId, siteAddressId: this.inputObject?.addressId, uniqueCallId: this.uniqueCallId ? this.uniqueCallId : null };
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CHECK, null, false,
        prepareRequiredHttpParams(filterObj))
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.isDisabled = false;
          } else {
            this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: this.inputObject?.customerId, siteAddressId: this.inputObject?.addressId, tab: CustomerVerificationType.ACCESS_TO_PREMISES, customerTab: 7, monitoringTab: 0, } })
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToProcessQuotationPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else {
        if ((this.accessPremisesForm.get('accessPremiseId')?.value &&
          this.currentAgreementTabObj.isMandatory) || !this.currentAgreementTabObj.isMandatory) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
    }
  }
}
