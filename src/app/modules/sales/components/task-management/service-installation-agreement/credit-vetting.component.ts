import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel, CrudService, CustomDirectiveConfig, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService, standardHeightForPscrollContainer
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import {
  CreditVettingInfo,
  CreditVettingModalComponent, LeadHeaderData,
  SalesModuleApiSuffixModels, selectLeadCreationStepperParamsState$,
  selectLeadHeaderDataState$, ServiceAgreementStepsParams
} from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'credit-vetting-info',
  templateUrl: './credit-vetting.component.html',
  styleUrls: ['./service-installation-agreement.scss']
})

export class CreditVettingInfoComponent implements OnInit {
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  leadHeaderData: LeadHeaderData;
  details;
  creditVettingInfoForm: FormGroup;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  IsInstallationDebtor: boolean = false;
  loggedInUserData: LoggedInUserModel;
  minDate = new Date();
  successMsg: boolean = false;
  customerId = "";
  debtorId = "";
  debtorAccountDetailId = "";
  breadCrumb: BreadCrumbModel;
  standardHeightForPscrollContainer=standardHeightForPscrollContainer;

  constructor(private crudService: CrudService, private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private router: Router) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectLeadHeaderDataState$), this.store.pipe(select(loggedInUserData)), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      this.leadHeaderData = response[1];
      this.loggedInUserData = response[2];
      if (response[3]) {
        this.customerId = response[3].customerId;
        this.debtorId = response[3].debtorId;
        this.debtorAccountDetailId = response[3].debtorAccountDetailId;
      }
    });
  }

  ngOnInit() {
    this.rxjsService.getIsInstallationDebtor().subscribe((IsInstallationDebtor: boolean) => {
      this.IsInstallationDebtor = IsInstallationDebtor;
      this.breadCrumb = {
        pageTitle: { key: 'Credit Vetting' },
        items: [{
          key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
        }, { key: this.IsInstallationDebtor ? 'Sales & Installation Agreement' : 'Service Agreement' }, { key: 'Credit Vetting' }]
      };
    });
    this.getDetails();
    this.createDebterForm();
    this.onFormControlChanges();
  }

  onFormControlChanges() {
    this.creditVettingInfoForm.get('isPassportDebtor').valueChanges.subscribe((isPassportDebtor: boolean) => {
      if (isPassportDebtor) {
        this.creditVettingInfoForm.get('passportNoDebtor').setValidators([Validators.required])
        this.creditVettingInfoForm.get('passportNoDebtor').updateValueAndValidity();
        this.creditVettingInfoForm.get('passportExpiryDateDebt').setValidators([Validators.required])
        this.creditVettingInfoForm.get('passportExpiryDateDebt').updateValueAndValidity();
        this.creditVettingInfoForm.get('saidDebtor').clearValidators();
        this.creditVettingInfoForm.get('saidDebtor').updateValueAndValidity();
        this.creditVettingInfoForm.get('passportExpiryDateDebt').enable();
      } else {
        this.creditVettingInfoForm.get('passportNoDebtor').clearValidators();
        this.creditVettingInfoForm.get('passportNoDebtor').updateValueAndValidity();
        this.creditVettingInfoForm.get('passportExpiryDateDebt').clearValidators();
        this.creditVettingInfoForm.get('passportExpiryDateDebt').updateValueAndValidity();
        this.creditVettingInfoForm.get('saidDebtor').setValidators([Validators.required])
        this.creditVettingInfoForm.get('saidDebtor').updateValueAndValidity();
        this.creditVettingInfoForm.get('passportExpiryDateDebt').disable();
      }
    })
  }

  validate() {
    if (this.details.debtorInformation.debtorTypeId == 1) {
      this.creditVettingInfoForm.get('companyNameDebt').setValidators([Validators.required])
      this.creditVettingInfoForm.get('companyNameDebt').updateValueAndValidity();
      this.creditVettingInfoForm.get('companyRegNoDebt').setValidators([Validators.required])
      this.creditVettingInfoForm.get('companyRegNoDebt').updateValueAndValidity();
    } else {
      this.creditVettingInfoForm.get('companyNameDebt').clearValidators();
      this.creditVettingInfoForm.get('companyNameDebt').updateValueAndValidity();
      this.creditVettingInfoForm.get('companyRegNoDebt').clearValidators();
      this.creditVettingInfoForm.get('companyRegNoDebt').updateValueAndValidity();
    }
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.CREDIT_VETTING, null, false,
      prepareGetRequestHttpParams(null, null,
        {
          customerId: this.customerId,
          debtorId: this.debtorId,
          debtorAccountDetailId: this.debtorAccountDetailId,
          quotationVersionId: this.serviceAgreementStepsParams.quotationVersionId
        }))
      .subscribe((response: IApplicationResponse) => {
        this.details = response.resources;
        if (this.details) {
          if (this.details.debtorInformation) {
            if (this.details.debtorInformation.passportExpiryDateDebt) {
              this.details.debtorInformation.passportExpiryDateDebt = new Date(this.details.debtorInformation.passportExpiryDateDebt);
            }
            this.details.debtorInformation.dob = new Date(this.details.debtorInformation.dob);
            if (this.details.debtorInformation.debtorTypeId == 1) {
              this.creditVettingInfoForm.get('companyNameDebt').setValidators([Validators.required])
              this.creditVettingInfoForm.get('companyNameDebt').updateValueAndValidity();
              this.creditVettingInfoForm.get('companyRegNoDebt').setValidators([Validators.required])
              this.creditVettingInfoForm.get('companyRegNoDebt').updateValueAndValidity();
              this.creditVettingInfoForm.get('passportNoDebtor').clearValidators();
              this.creditVettingInfoForm.get('passportNoDebtor').updateValueAndValidity();
              this.creditVettingInfoForm.get('passportExpiryDateDebt').clearValidators();
              this.creditVettingInfoForm.get('passportExpiryDateDebt').updateValueAndValidity();
              this.creditVettingInfoForm.get('saidDebtor').clearValidators();
              this.creditVettingInfoForm.get('saidDebtor').updateValueAndValidity();
              this.creditVettingInfoForm.get('dobDebt').clearValidators();
              this.creditVettingInfoForm.get('dobDebt').updateValueAndValidity();
            } else {
              this.creditVettingInfoForm.get('companyNameDebt').clearValidators();
              this.creditVettingInfoForm.get('companyNameDebt').updateValueAndValidity();
              this.creditVettingInfoForm.get('companyRegNoDebt').clearValidators();
              this.creditVettingInfoForm.get('companyRegNoDebt').updateValueAndValidity();
              this.creditVettingInfoForm.get('passportNoDebtor').setValidators([Validators.required])
              this.creditVettingInfoForm.get('passportNoDebtor').updateValueAndValidity();
              this.creditVettingInfoForm.get('passportExpiryDateDebt').setValidators([Validators.required])
              this.creditVettingInfoForm.get('passportExpiryDateDebt').updateValueAndValidity();
              this.creditVettingInfoForm.get('saidDebtor').setValidators([Validators.required])
              this.creditVettingInfoForm.get('saidDebtor').updateValueAndValidity();
              this.creditVettingInfoForm.get('dobDebt').setValidators([Validators.required])
              this.creditVettingInfoForm.get('dobDebt').updateValueAndValidity();
            }
            if (!this.details.debtorInformation.companyRegNo && !this.details.debtorInformation.said) {
              this.details.debtorInformation.isPassport = true;
            } else {
              this.details.debtorInformation.isPassport = false;
            }
            this.creditVettingInfoForm.patchValue({
              isPassportDebtor: this.details.debtorInformation.isPassport,
              dobDebt: this.details.debtorInformation.dob,
              passportExpiryDateDebt: this.details.debtorInformation.passportExpiryDate,
              passportNoDebtor: this.details.debtorInformation.passportNo,
              saidDebtor: this.details.debtorInformation.said,
              systemOwnershipId: this.details.debtorInformation.systemOwnershipId,
              companyNameDebt: this.details.debtorInformation.companyName ? this.details.debtorInformation.companyName : null,
              companyRegNoDebt: this.details.debtorInformation.companyRegNo,
            })
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  createDebterForm() {
    let debtorBankingDetailsModel = new CreditVettingInfo();
    this.creditVettingInfoForm = this.formBuilder.group({});
    Object.keys(debtorBankingDetailsModel).forEach((key) => {
      this.creditVettingInfoForm.addControl(key,
        new FormControl(debtorBankingDetailsModel[key]));
    });
    this.creditVettingInfoForm = setRequiredValidator(this.creditVettingInfoForm, ["dobDebt", "saidDebtor"]);
  }

  validateDebtor() {
    const dialogReff = this.dialog.open(CreditVettingModalComponent, { width: '960px', data: this.details.debtorInformation, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData1.subscribe(ele => {
      if (ele) {
        this.details.debtorInformation.itNumber = ele.itNumber;
      }
    });
  }

  onSubmit() {
    this.validate();
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.details.debtorInformation.debtorTypeId == 1) {
      this.creditVettingInfoForm.get('saidDebtor').setValue(this.creditVettingInfoForm.value.companyRegNoDebt)
    }
    if (this.creditVettingInfoForm.invalid) {
      return;
    }
    if (this.details.debtorInformation.debtorTypeId == 1) {
      if (!this.details.debtorInformation.itNumber) {
        this.snackbarService.openSnackbar("Please click Validate debtor to get IT number before proceeding credit vetting", ResponseMessageTypes.ERROR);
        return;
      }
    }
    if (this.creditVettingInfoForm.value.isPassportDebtor) {
      this.creditVettingInfoForm.get('saidDebtor').setValue(null)
    } else {
      this.creditVettingInfoForm.get('passportNoDebtor').setValue(null)
      this.creditVettingInfoForm.get('passportExpiryDateDebt').setValue(null)
    }
    let data = this.creditVettingInfoForm.getRawValue();
    let obj = {
      'debtorInformation': {},
      'debtorBankingInformation': {},
      'createdUserId': this.loggedInUserData.userId
    };
    obj.debtorInformation = {
      "debtorId": this.debtorId,
      "customerId": this.details.debtorInformation ? this.details.debtorInformation.customerId : null,
      "leadId": this.details.debtorInformation ? this.details.debtorInformation.leadId : null,
      "debtorRefNo": this.details.debtorInformation ? this.details.debtorInformation.debtorRefNo : null,
      "debtorName": this.details.debtorInformation ? this.details.debtorInformation.debtorName : null,
      "debtorType": this.details.debtorInformation ? this.details.debtorInformation.debtorType : null,
      "itNumber": this.details.debtorInformation ? this.details.debtorInformation.itNumber : null,
      "systemOwnershipId": this.details.debtorInformation ? this.details.debtorInformation.systemOwnershipId : null,
      "customerAddress": this.details.debtorInformation ? this.details.debtorInformation.customerAddress : null,
      "firstName": this.details.debtorInformation ? this.details.debtorInformation.firstName : null,
      "lastName": this.details.debtorInformation ? this.details.debtorInformation.lastName : null,
      "said": data.saidDebtor,
      "mobile1": this.details.debtorInformation ? this.details.debtorInformation.mobile1 : '',
      "mobile1CountryCode": this.details.debtorInformation ? this.details.debtorInformation.mobile1CountryCode : '',
      "officeNo": null,
      "officeNoCountryCode": null,
      "foreName1": this.details.debtorInformation ? this.details.debtorInformation.foreName1 : null,
      "foreName2": this.details.debtorInformation ? this.details.debtorInformation.foreName2 : null,
      "buildingNumber": this.details.debtorInformation ? this.details.debtorInformation.buildingNumber : null,
      "streetNumber": this.details.debtorInformation ? this.details.debtorInformation.streetNumber : null,
      "isPassport": data.isPassportDebtor,
      "dob": data.dobDebt,
      "passportNo": data.passportNoDebtor,
      "companyName": data.companyNameDebt,
      "companyRegNo": data.companyRegNoDebt,
      "passportExpiryDate": data.passportExpiryDateDebt,
      "debtorTypeId": this.details.debtorInformation ? this.details.debtorInformation.debtorTypeId : null,
      "isDebtorSameAsCustomer": this.details.debtorInformation ? this.details.debtorInformation.isDebtorSameAsCustomer : false,
      'systemOwnership': data.systemOwnership,
      "isDebtorVetted": this.details.debtorInformation ? this.details.debtorInformation.isDebtorVetted : false,
    }
    obj.debtorBankingInformation = {
      "accountHolderName": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.accountHolderName : null,
      "companyName": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.companyName : null,
      "isBankAccount": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.isBankAccount : false,
      "bankName": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.bankName : null,
      "bankBranchName": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.bankBranchName : null,
      "bankBranchCode": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.bankBranchCode : null,
      "accountNo": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.accountNo : null,
      "accountTypeName": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.accountTypeName : null,
      "cardNo": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.cardNo : null,
      "expiryDate": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.expiryDate : null,
      "cardTypeName": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.cardTypeName : null,
      "accountTypeDescription": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.accountTypeDescription : null,
      "accountTypeId": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.accountTypeId : null,
      "isDebtorAccountVetted": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.isDebtorAccountVetted : false,
      "debtorAccountDetailId": this.details.debtorBankingInformation ? this.details.debtorBankingInformation.debtorAccountDetailId : null,
    }
    let formData = new FormData();
    formData.append('data', JSON.stringify(obj));
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.CREDIT_VETTING, formData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.successMsg = true;
        }
      });
  }

  redirectToDebtorBankingDetails() {
    let queryParams = { queryParams: { customerId: this.leadHeaderData.customerId, debtorId: this.serviceAgreementStepsParams.debtorId, debtorAccountDetailId: this.details?.debtorBankingInformation?.debtorAccountDetailId } };
    let urlSuffix = this.IsInstallationDebtor ? 'installation-agreement/credit-vetting-result' :
      'service-agreement/credit-vetting-result';
    if (this.leadHeaderData.fromUrl == 'Leads List') {
      this.router.navigate([`/sales/task-management/leads/${urlSuffix}`], queryParams);
    }
    else{
      this.router.navigate([`/my-tasks/my-sales/my-leads/${urlSuffix}`], queryParams);
    }
  }

  back() {
    let queryParams = { queryParams: { customerId: this.leadHeaderData.customerId, debtorId: this.serviceAgreementStepsParams.debtorId, debtorAccountDetailId: this.details.debtorBankingInformation?.debtorAccountDetailId } };
    let urlSuffix = this.IsInstallationDebtor ? 'installation-agreement/debtor-banking-details' :
      'service-agreement/debtor-banking-details';
    if (this.serviceAgreementStepsParams.isOnInvoicePayment) {
      urlSuffix = this.IsInstallationDebtor ? 'installation-agreement/debtor-creation' :
        'service-agreement/debtor-creation';
    }
    if (this.leadHeaderData.fromUrl == 'Leads List') {
      this.router.navigate([`/sales/task-management/leads/${urlSuffix}`], queryParams);
    }
    else{
      this.router.navigate([`/my-tasks/my-sales/my-leads/${urlSuffix}`], queryParams);
    }
  }

  cancel() {
    this.successMsg = false;
  }
}
