import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
  loggedInUserData, SalesModuleApiSuffixModels, selectLeadCreationStepperParamsState$,
  selectLeadHeaderDataState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, ReusablePrimeNGTableFeatureService, SnackbarService } from '@app/shared';
import { ModuleName, ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService } from '@app/shared/services';
import {
  formConfigs, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix,
  prepareGetRequestHttpParams, setRequiredValidator
} from '@app/shared/utils';
import { DocTypes, InstallationHazardModel, LeadHeaderData, ServiceAgreementStepsParams } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'installation-app-site-hazard',
  templateUrl: './installation-site-hazard.component.html'
})

export class InstallationSiteHazardComponent implements OnInit {
  params: ServiceAgreementStepsParams;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  InstallationHazardForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  leadHeaderData: LeadHeaderData;
  dogTypes: FormArray;
  @ViewChildren('input') rows: QueryList<any>;
  dogTypesList;
  details;
  show: boolean = false;
  showPlus: boolean = false;
  currentAgreementTabObj;
  breadCrumb: BreadCrumbModel;

  constructor(private crudService: CrudService, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private router: Router,
    private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    public rxjsService: RxjsService, private store: Store<AppState>
  ) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
    this.rxjsService.setIsInstallationDebtor(true);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectLeadHeaderDataState$)]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[1]);
      this.leadHeaderData = response[2];
      this.currentAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['agreementTabName'].toLowerCase().includes('installation site hazards'));
      this.breadCrumb = {
        pageTitle: { key: 'Site Hazard' },
        items: [{
          key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
        }, { key: 'Sales & Installation Agreement' }, { key: 'Site Hazard' }]
      };
    });
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToProcessQuotationPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else {
        if ((this.InstallationHazardForm.get('siteHazardId').value &&
          this.currentAgreementTabObj.isMandatory) || !this.currentAgreementTabObj.isMandatory) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
    }
  }

  ngOnInit(): void {
    this.createInstallationHazardForm();
    this.getInstallationHazardByLeadAndPartitionId();
    this.getDogTypes();
    this.onFormControlChanges();
  }

  validateFields() {
    this.InstallationHazardForm.get('noOfDogs').setValidators(Validators.required);
    this.InstallationHazardForm.get('noOfDogs').updateValueAndValidity();
  }

  applyValidation() {
    this.InstallationHazardForm.get('asbestosDescription').setValidators(Validators.required);
    this.InstallationHazardForm.get('asbestosDescription').updateValueAndValidity();
    this.InstallationHazardForm.get('confinedSpaceDescription').setValidators(Validators.required);
    this.InstallationHazardForm.get('confinedSpaceDescription').updateValueAndValidity();
    this.InstallationHazardForm.get('constructionSiteDescription').setValidators(Validators.required);
    this.InstallationHazardForm.get('constructionSiteDescription').updateValueAndValidity();
    this.InstallationHazardForm.get('extendedLadderDescription').setValidators(Validators.required);
    this.InstallationHazardForm.get('extendedLadderDescription').updateValueAndValidity();
    this.InstallationHazardForm.get('harmfulChemicalDescription').setValidators(Validators.required);
    this.InstallationHazardForm.get('harmfulChemicalDescription').updateValueAndValidity();
    this.InstallationHazardForm.get('loudNoiseAreaDescription').setValidators(Validators.required);
    this.InstallationHazardForm.get('loudNoiseAreaDescription').updateValueAndValidity();
    this.InstallationHazardForm.get('viciousDogsDescription').setValidators(Validators.required);
    this.InstallationHazardForm.get('viciousDogsDescription').updateValueAndValidity();
  }

  removeValidation() {
    this.InstallationHazardForm.get('asbestosDescription').clearValidators();
    this.InstallationHazardForm.get('asbestosDescription').updateValueAndValidity();
    this.InstallationHazardForm.get('confinedSpaceDescription').clearValidators()
    this.InstallationHazardForm.get('confinedSpaceDescription').updateValueAndValidity();
    this.InstallationHazardForm.get('constructionSiteDescription').clearValidators()
    this.InstallationHazardForm.get('constructionSiteDescription').updateValueAndValidity();
    this.InstallationHazardForm.get('extendedLadderDescription').clearValidators()
    this.InstallationHazardForm.get('extendedLadderDescription').updateValueAndValidity();
    this.InstallationHazardForm.get('harmfulChemicalDescription').clearValidators()
    this.InstallationHazardForm.get('harmfulChemicalDescription').updateValueAndValidity();
    this.InstallationHazardForm.get('loudNoiseAreaDescription').clearValidators()
    this.InstallationHazardForm.get('loudNoiseAreaDescription').updateValueAndValidity();
    this.InstallationHazardForm.get('viciousDogsDescription').clearValidators();
    this.InstallationHazardForm.get('viciousDogsDescription').updateValueAndValidity();
  }

  novalidateFields() {
    this.InstallationHazardForm.get('noOfDogs').clearValidators()
    this.InstallationHazardForm.get('noOfDogs').updateValueAndValidity();
  }

  changeViciousDogs() {
    if (this.InstallationHazardForm.get('isViciousDogs').value) {
      this.showDogs = true;
      this.InstallationHazardForm.get('viciousDogsDescription').setValidators(Validators.required);
      this.InstallationHazardForm.get('viciousDogsDescription').updateValueAndValidity();
      if (this.dogTypes?.length == 0) {
        this.dogTypes = this.getdogTypesListArray;
        this.dogTypes.insert(0, this.createDogTypesListModel());
      }
    } else {
      this.showDogs = false;
      this.InstallationHazardForm.get('viciousDogsDescription').clearValidators()
      this.InstallationHazardForm.get('viciousDogsDescription').updateValueAndValidity();
    }
  }

  changeInstallation() {
    let value = this.InstallationHazardForm.get('isInstallationRequired').value;
    if (value) {
      if (this.dogTypes?.length == 0) {
        this.dogTypes = this.getdogTypesListArray;
        this.dogTypes.insert(0, this.createDogTypesListModel());
      }
    } else {
      this.removeValidation();
      this.novalidateFields();
    }
  }

  onFormControlChanges(): void {
    this.InstallationHazardForm.get('isViciousDogs').valueChanges.subscribe((isDogOnSite: boolean) => {
      if (!isDogOnSite) {
        if (this.details.siteHazardId) {
          if (this.dogTypes?.length > 0) {
            this.snackbarService.openSnackbar("Please delete existing dogs", ResponseMessageTypes.WARNING);
            setTimeout(() => {
              this.InstallationHazardForm.get('isViciousDogs').setValue(true)
            }, 10)
          }
        }
      }
    });
    this.InstallationHazardForm.get('isConfinedSpace').valueChanges.subscribe((noOfDogs: boolean) => {
      if (noOfDogs) {
        this.InstallationHazardForm.get('confinedSpaceDescription').setValidators(Validators.required);
        this.InstallationHazardForm.get('confinedSpaceDescription').updateValueAndValidity();
      }
      else {
        this.InstallationHazardForm.get('confinedSpaceDescription').clearValidators();
        this.InstallationHazardForm.get('confinedSpaceDescription').updateValueAndValidity();
        this.InstallationHazardForm.get('confinedSpaceDescription').setValue(null);
      }
    });
    this.InstallationHazardForm.get('isExtendedLadders').valueChanges.subscribe((noOfDogs: boolean) => {
      if (noOfDogs) {
        this.InstallationHazardForm.get('extendedLadderDescription').setValidators(Validators.required);
        this.InstallationHazardForm.get('extendedLadderDescription').updateValueAndValidity();
      }
      else {
        this.InstallationHazardForm.get('extendedLadderDescription').clearValidators();
        this.InstallationHazardForm.get('extendedLadderDescription').updateValueAndValidity();
        this.InstallationHazardForm.get('extendedLadderDescription').setValue(null);
      }
    });
    this.InstallationHazardForm.get('isConstructionSite').valueChanges.subscribe((noOfDogs: boolean) => {
      if (noOfDogs) {
        this.InstallationHazardForm.get('constructionSiteDescription').setValidators(Validators.required);
        this.InstallationHazardForm.get('constructionSiteDescription').updateValueAndValidity();
      }
      else {
        this.InstallationHazardForm.get('constructionSiteDescription').clearValidators();
        this.InstallationHazardForm.get('constructionSiteDescription').updateValueAndValidity();
        this.InstallationHazardForm.get('constructionSiteDescription').setValue(null);
      }
    });
    this.InstallationHazardForm.get('isUnsafe').valueChanges.subscribe((noOfDogs: boolean) => {
      if (noOfDogs) {
        this.InstallationHazardForm.get('unsafeDescription').setValidators(Validators.required);
        this.InstallationHazardForm.get('unsafeDescription').updateValueAndValidity();
      }
      else {
        this.InstallationHazardForm.get('unsafeDescription').clearValidators();
        this.InstallationHazardForm.get('unsafeDescription').updateValueAndValidity();
        this.InstallationHazardForm.get('unsafeDescription').setValue(null);
      }
    });
    this.InstallationHazardForm.get('isConfinedSpaceInstalled').valueChanges.subscribe((noOfDogs: boolean) => {
      if (noOfDogs) {
        this.InstallationHazardForm.get('confinedSpaceInstalledDescription').setValidators(Validators.required);
        this.InstallationHazardForm.get('confinedSpaceInstalledDescription').updateValueAndValidity();
      }
      else {
        this.InstallationHazardForm.get('confinedSpaceInstalledDescription').clearValidators();
        this.InstallationHazardForm.get('confinedSpaceInstalledDescription').updateValueAndValidity();
        this.InstallationHazardForm.get('confinedSpaceInstalledDescription').setValue(null);
      }
    });
    this.InstallationHazardForm.get('isHarmfulChemical').valueChanges.subscribe((noOfDogs: boolean) => {
      if (noOfDogs) {
        this.InstallationHazardForm.get('harmfulChemicalDescription').setValidators(Validators.required);
        this.InstallationHazardForm.get('harmfulChemicalDescription').updateValueAndValidity();
      }
      else {
        this.InstallationHazardForm.get('harmfulChemicalDescription').clearValidators();
        this.InstallationHazardForm.get('harmfulChemicalDescription').updateValueAndValidity();
        this.InstallationHazardForm.get('harmfulChemicalDescription').setValue(null);
      }
    });
    this.InstallationHazardForm.get('isLoudNoiseArea').valueChanges.subscribe((noOfDogs: boolean) => {
      if (noOfDogs) {
        this.InstallationHazardForm.get('loudNoiseAreaDescription').setValidators(Validators.required);
        this.InstallationHazardForm.get('loudNoiseAreaDescription').updateValueAndValidity();
      }
      else {
        this.InstallationHazardForm.get('loudNoiseAreaDescription').clearValidators();
        this.InstallationHazardForm.get('loudNoiseAreaDescription').updateValueAndValidity();
        this.InstallationHazardForm.get('loudNoiseAreaDescription').setValue(null);
      }
    });
    this.InstallationHazardForm.get('isAsbestos').valueChanges.subscribe((noOfDogs: boolean) => {
      if (noOfDogs) {
        this.InstallationHazardForm.get('asbestosDescription').setValidators(Validators.required);
        this.InstallationHazardForm.get('asbestosDescription').updateValueAndValidity();
      }
      else {
        this.InstallationHazardForm.get('asbestosDescription').clearValidators();
        this.InstallationHazardForm.get('asbestosDescription').updateValueAndValidity();
        this.InstallationHazardForm.get('asbestosDescription').setValue(null);
      }
    });
    this.changeViciousDogs();
    this.changeInstallation();
  }

  getDogTypes() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_DOG_TYPES, undefined, false, undefined, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dogTypesList = response.resources.dogTypes;
      }
    });
  }

  createInstallationHazardForm(): void {
    let passwordsAddEditModel = new InstallationHazardModel();
    this.InstallationHazardForm = this.formBuilder.group({
      dogTypes: this.formBuilder.array([]),
    });
    Object.keys(passwordsAddEditModel).forEach((key) => {
      this.InstallationHazardForm.addControl(key, new FormControl(passwordsAddEditModel[key]));
    });
    this.InstallationHazardForm = setRequiredValidator(this.InstallationHazardForm, ["noOfDogs",
      "asbestosDescription", "confinedSpaceDescription", "constructionSiteDescription",
      "extendedLadderDescription", "harmfulChemicalDescription", "loudNoiseAreaDescription",
      "viciousDogsDescription", "isExtendedLadders", "isLoudNoiseArea", "isAsbestos", "isConstructionSite", "isConfinedSpace", "isHarmfulChemical", "isUnsafe", "unsafeDescription", "isConfinedSpaceInstalled", "confinedSpaceInstalledDescription"])
  }

  get getdogTypesListArray(): FormArray {
    if (!this.InstallationHazardForm) return;
    return this.InstallationHazardForm.get("dogTypes") as FormArray;
  }

  showDogs: boolean = false;
  showButtons: boolean = false;
  getInstallationHazardByLeadAndPartitionId() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SITE_HAZARD_INSTALLATION, undefined, false,
      prepareGetRequestHttpParams(undefined, undefined, {
        customerId: this.serviceAgreementStepsParams.customerId,
        quotationVersionId: this.serviceAgreementStepsParams.quotationVersionId,
        leadId: this.serviceAgreementStepsParams.leadId,
        partitionId: this.serviceAgreementStepsParams.partitionId,
        isExist: this.serviceAgreementStepsParams.isExist
      }))
      .subscribe((response: IApplicationResponse) => {
        if (this.dogTypes) {
          this.dogTypes.clear();
        }
        this.dogTypes = this.getdogTypesListArray;
        if (response && response.resources && response.isSuccess) {
          this.details = response.resources;
          this.InstallationHazardForm.patchValue(response.resources);
          if (this.details.contractId) {
            this.showDogs = true;
            this.showButtons = false;
            this.InstallationHazardForm.get('isViciousDogs').disable({ emitEvent: false });
            this.InstallationHazardForm.get('noOfDogs').disable();
            if (this.details.isViciousDogs) {
              if (this.details.dogTypes && this.details.dogTypes.length > 0) {
                response.resources.dogTypes.forEach((dg) => {
                  this.dogTypes.push(this.createDogTypesListModel(dg));
                });
                (<FormArray>this.InstallationHazardForm.get('dogTypes'))
                  .controls
                  .forEach(control => {
                    control.disable();
                  });
              }
              this.InstallationHazardForm.get('isViciousDogs').setValue(true);
            } else {
              this.InstallationHazardForm.get('isViciousDogs').disable({ emitEvent: false });
            }
          } else {
            this.showButtons = true;
            this.showDogs = true;
            if (!this.details.siteHazardId && !this.details.specialInstructionId) {
              this.InstallationHazardForm.get('noOfDogs').setValue(1);
              this.dogTypes.push(this.createDogTypesListModel());
              this.InstallationHazardForm.patchValue({
                isConfinedSpace: null,
                isExtendedLadders: null,
                isConstructionSite: null,
                isUnsafe: null,
                isConfinedSpaceInstalled: null,
                isHarmfulChemical: null,
                isLoudNoiseArea: null,
                isAsbestos: null,
              })
            } else {
              if (response.resources.dogTypes && response.resources.dogTypes.length > 0) {
                response.resources.dogTypes.forEach((dg) => {
                  this.dogTypes.push(this.createDogTypesListModel(dg));
                });
              } else {
                this.InstallationHazardForm.get('noOfDogs').setValue(1);
                this.dogTypes.push(this.createDogTypesListModel());
              }
            }
          }
        }
        else {
          this.showButtons = true;
          this.showDogs = true;
          this.InstallationHazardForm.get('noOfDogs').setValue(1);
          this.dogTypes.push(this.createDogTypesListModel());
          this.InstallationHazardForm.patchValue({
            isConfinedSpace: null,
            isExtendedLadders: null,
            isConstructionSite: null,
            isUnsafe: null,
            isConfinedSpaceInstalled: null,
            isHarmfulChemical: null,
            isLoudNoiseArea: null,
            isAsbestos: null,
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  deleteDogType(i: number) {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, null, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) return;
        this.getdogTypesListArray.removeAt(i);
        this.details.dogTypes.splice(i, 1);
        this.InstallationHazardForm.get('noOfDogs').setValue(this.getdogTypesListArray.length);
        this.rxjsService.setFormChangeDetectionProperty(true);
      });
  }

  //Create FormArray controls
  createDogTypesListModel(docTypes?: DocTypes): FormGroup {
    let docTypesFormControlModel = new DocTypes(docTypes);
    let formControls = {};
    Object.keys(docTypesFormControlModel).forEach((key) => {
      if (key == 'dogTypeId') {
        formControls[key] = [{ value: docTypesFormControlModel[key], disabled: false }, [Validators.required]]

      } else {
        formControls[key] = [{ value: docTypesFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  addItem(event): void {
    this.dogTypes = this.getdogTypesListArray;
    let docTypesFormControlModel = new DocTypes();
    let dogs = parseInt(event.target.value);
    if (event.target.value) {
      if (this.details && this.InstallationHazardForm.value.isViciousDogs && dogs < this.details?.dogTypes.length) {
        this.snackbarService.openSnackbar("Please delete existing dogs", ResponseMessageTypes.WARNING);
        return;
      }
      else if (this.details && this.InstallationHazardForm.value.isViciousDogs && dogs >= this.details?.dogTypes.length) {
        this.dogTypes.clear();
        this.details.dogTypes.forEach((dg) => {
          this.dogTypes.push(this.createDogTypesListModel(dg));
        });
        for (var i = this.details?.dogTypes.length; i < dogs; i++) {
          this.dogTypes.insert(i, this.createDogTypesListModel(docTypesFormControlModel));
        }
      }
      else {
        this.dogTypes.clear();
        for (var j = 0; j < this.InstallationHazardForm.value.noOfDogs; j++) {
          this.dogTypes.insert(j, this.createDogTypesListModel(docTypesFormControlModel));
        }
      }
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onSubmit() {
    let payload = this.InstallationHazardForm.getRawValue();
    if (payload.isInstallationRequired) {
      if (payload.isViciousDogs) {
      } else {
        this.dogTypes.clear();
      }
    } else {
      this.dogTypes.clear();
      payload.isConfinedSpace = false;
      payload.isExtendedLadders = false;
      payload.isConstructionSite = false;
      payload.isHarmfulChemical = false;
      payload.isAsbestos = false;
      payload.isLoudNoiseArea = false;
      payload.isUnsafe = false,
        payload.isConfinedSpaceInstalled = false,
        this.InstallationHazardForm.get('isConfinedSpace').setValue(false);
      this.InstallationHazardForm.get('isExtendedLadders').setValue(false);
      this.InstallationHazardForm.get('isConstructionSite').setValue(false);
      this.InstallationHazardForm.get('isHarmfulChemical').setValue(false);
      this.InstallationHazardForm.get('isUnsafe').setValue(false);
      this.InstallationHazardForm.get('isConfinedSpaceInstalled').setValue(false);
      this.InstallationHazardForm.get('isAsbestos').setValue(false);
      this.InstallationHazardForm.get('isLoudNoiseArea').setValue(false);
    }
    if (this.InstallationHazardForm.invalid) {
      return;
    }
    if (!this.InstallationHazardForm.value.siteHazardId) {
      this.rxjsService.setFormChangeDetectionProperty(true);
    }
    if (!payload.isInstallationRequired) {
      payload.noOfDogs = null;
      payload.dogTypes = null;
    }
    if (!payload.isViciousDogs) {
      payload.noOfDogs = null;
      payload.dogTypes = null;
    }
    payload.createdUserId = this.loggedInUserData.userId;
    payload.modifiedUserId = this.loggedInUserData.userId;
    payload.leadId = this.serviceAgreementStepsParams.leadId;
    payload.customerId = this.serviceAgreementStepsParams.customerId;
    payload.quotationVersionId = this.serviceAgreementStepsParams.quotationVersionId;
    payload.partitionId = this.serviceAgreementStepsParams.partitionId;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SITE_HAZARD_INSTALLATION, payload).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        if (!this.InstallationHazardForm.value.siteHazardId) {
          this.InstallationHazardForm.get('siteHazardId').setValue(response.resources.siteHazardId);
        }
      }
    });
  }
}
