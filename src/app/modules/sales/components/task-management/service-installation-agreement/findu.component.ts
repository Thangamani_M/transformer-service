import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
  SalesModuleApiSuffixModels, selectLeadCreationStepperParamsState$, selectLeadHeaderDataState$, selectStaticEagerLoadingTitlesState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, CustomDirectiveConfig, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { ModuleName } from '@app/shared/enums';
import { CrudService, RxjsService } from '@app/shared/services';
import {
  countryCodes, formConfigs, getPDropdownData, IApplicationResponse,
  ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, setRequiredValidator
} from '@app/shared/utils';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { AdditionalUsers, FinduSubscriberModel, LeadHeaderData, ServiceAgreementStepsParams } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-findu',
  templateUrl: './findu.component.html',
  styleUrls: ['./service-installation-agreement.scss']
})

export class FinduComponent implements OnInit {
  finduForm: FormGroup;
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  leadHeaderData: LeadHeaderData;
  additionalUsers: FormArray;
  maxDate = new Date();
  titles = [];
  currentAgreementTabObj;
  findUdetails;
  breadCrumb: BreadCrumbModel;
  vehicleMakeList;
  isPasswordShow = false;
  buildList = []
  raceList = []
  languageList = []
  heightList = []
  vehicleColors = [];
  countryList = [];
  dateRange = "";
  todayDate = new Date()
  isAnAlphaNumericOnly= new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });


  constructor(private crudService: CrudService, private router: Router, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder,
    public rxjsService: RxjsService, private store: Store<AppState>
  ) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(selectLeadHeaderDataState$)]
    ).pipe(take(1)).subscribe((response) => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      this.titles = this.customSelectKeyAndValue(response[1]);
      this.leadHeaderData = response[2];
      this.currentAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['agreementTabName'].toLowerCase().includes('find'));
      this.breadCrumb = {
        pageTitle: { key: 'FindU Subscribers' },
        items: [{
          key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
        }, { key: 'Service Agreement' }, { key: 'FindU Subscribers' }]
      };
    });
  }

  isEmailExist = true;
  checkDuplication() {
    setTimeout(() => {
      if (!this.finduForm?.get('email').value||this.finduForm?.get('email').invalid) {
        return;
      }
      this.rxjsService.setNotificationProperty(false);
      this.crudService
        .get(
          ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
          SalesModuleApiSuffixModels.FINDU_VALIDATION,
          undefined,
          false,
          prepareRequiredHttpParams({
            email: this.finduForm.value.email,
          }))
        .subscribe((response: IApplicationResponse) => {
          this.isEmailExist = response.isSuccess;
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    });
  }

  getvehicleMakeList() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_MAKE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.vehicleMakeList = getPDropdownData(response.resources, "displayName", "displayName");
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createFindUForm(): void {
    let finduSubscriberModel = new FinduSubscriberModel();
    this.finduForm = this.formBuilder.group({
      additionalUsers: this.formBuilder.array([]),
    });
    Object.keys(finduSubscriberModel).forEach((key) => {
      this.finduForm.addControl(key,
        new FormControl(finduSubscriberModel[key]));
    });
    this.finduForm = setRequiredValidator(this.finduForm, ["mobileNo", "email", "firstName", "lastName", "dob",
      "title", "password"]);
    this.finduForm.get("mobileNoCountryCode").setValue("+27");
  }

  get getAdditionalUsers(): FormArray {
    if (!this.finduForm) return;
    return this.finduForm.get("additionalUsers") as FormArray;
  }

  //Create FormArray controls
  createUserModel(userModel?: AdditionalUsers): FormGroup {
    let userModelFormControlModel = new AdditionalUsers(userModel);
    let formControls = {};
    Object.keys(userModelFormControlModel).forEach((key) => {
      if (key == 'finduUserId') {
        formControls[key] = [{ value: userModelFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: userModelFormControlModel[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formBuilder.group(formControls);
  }

  getFindUDetailsByLeadId(): void {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_FINDU, undefined, false, prepareRequiredHttpParams({
      leadId: this.serviceAgreementStepsParams.leadId,
      quotationVersionId: this.serviceAgreementStepsParams.quotationVersionId
    })).subscribe((response: IApplicationResponse) => {
      this.findUdetails = response.resources;
      if (response.isSuccess && response.resources && response.statusCode === 200) {
        this.findUdetails.mobileNoCountryCode = this.findUdetails.mobileNoCountryCode ? this.findUdetails.mobileNoCountryCode : '+27';
        this.findUdetails.additionalUsers = this.findUdetails.additionalUsers ? this.findUdetails.additionalUsers : [];
        if (response.resources.dob) {
          response.resources.dob = new Date(response.resources.dob);
        }
        this.finduForm.patchValue(response.resources);
        this.additionalUsers = this.getAdditionalUsers;
        if (response.resources.finduUserId) {
          if (response.resources.additionalUsers.length > 0) {
            response.resources.additionalUsers.forEach((dg) => {
              this.additionalUsers.push(this.createUserModel(dg));
            });
          }
        } else {
          if (response.resources.noOfUserCount > 0) {
            for (var i = 0; i < response.resources.noOfUserCount; i++) {
              this.additionalUsers.insert(i, this.createUserModel());
            }
          }
        }
        this.finduForm.get('noOfUserCount').setValue(response.resources.noOfUserCount);
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    });
  }

  ngOnInit(): void {
    this.createFindUForm();
    this.getDropDownList()
    this.getFindUDetailsByLeadId();
    this.getFinduDropdownsData();
    let date = new Date().getFullYear()
    this.dateRange =  `${date-90}:${date}`;
  }

  isDuplicate: boolean = false;
  OnChange(index): boolean {
    if (this.additionalUsers.length > 1) {
      this.isDuplicate = false;
      var cloneArray = this.additionalUsers.value.slice();
      cloneArray.splice(index, 1);
      cloneArray.forEach((obj) => {
        if (typeof obj.mobileNo === "string") {
          obj.mobileNo = obj.mobileNo.split(" ").join("")
          obj.mobileNo = obj.mobileNo.replace(/"/g, "");
        }
      });
      this.additionalUsers.value.forEach((obj) => {
        if (typeof obj.mobileNo === "string") {
          obj.mobileNo = obj.mobileNo.split(" ").join("")
          obj.mobileNo = obj.mobileNo.replace(/"/g, "");
        }
      });
      var findIndex = cloneArray.some(x => x.mobileNo == this.additionalUsers.value[index].mobileNo);
      if (findIndex) {
        this.snackbarService.openSnackbar("Contact number already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      } else {
        this.isDuplicate = false;
      }
    }
  }

  onSubmit() {
    if (this.finduForm.invalid) {
      return;
    }
    if (!this.isEmailExist) {
      this.snackbarService.openSnackbar("Email already exist", ResponseMessageTypes.ERROR);
      return;
    }
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Contact number already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.finduForm.value.customerId = this.serviceAgreementStepsParams.customerId;
    this.finduForm.value.quotationVersionId = this.serviceAgreementStepsParams.quotationVersionId;
    this.finduForm.value.partitionId = this.serviceAgreementStepsParams.partitionId;
    this.finduForm.value.leadId = this.serviceAgreementStepsParams.leadId;

    if(this.finduForm?.value?.dob){
      this.finduForm.value.dob = new Date(this.finduForm?.value?.dob).toDateString()
    }
    this.finduForm.value.height = this.heightList.find(item => item.id == this.finduForm.value.heightId)?.displayName;
    this.finduForm.value.createdUserId = this.serviceAgreementStepsParams.createdUserId;
    const mobile = this.finduForm.get('mobileNo').value.toString().replace(/\s/g, '');
    this.finduForm.value.mobileNo = mobile;
    let additionalUsers = this.finduForm.value.additionalUsers;
    additionalUsers.forEach(element => {
      if (typeof element.mobileNo === "string") {
        element.mobileNo = element.mobileNo.split(" ").join("")
        element.mobileNo = element.mobileNo.replace(/\s/g, '').trim();
      }
    });
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_FINDU, this.finduForm.value, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        if (!this.finduForm.get('finduUserId').value) {
          this.finduForm.get('finduUserId').setValue(response.resources);
        }
      }
    });
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToProcessQuotationPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else {
        if ((this.finduForm.get('finduUserId').value &&
          this.currentAgreementTabObj.isMandatory) || !this.currentAgreementTabObj.isMandatory) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
    }
  }


  getFinduDropdownsData() {
    this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.UX_FINDU_USERS).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        let country = []
        response.resources?.nationality.forEach(item=>{
          country.push({
            id: item.displayName,
            displayName: item.displayName,
          })
        })
        this.countryList = country;
        // this.vehicleColors = getPDropdownData(response.resources?.vehicleColor, "displayName", "displayName");
        // this.vehicleMakeList = getPDropdownData(response.resources?.vehicleMake, "displayName", "displayName");
        this.languageList = this.customSelectKeyAndValue(response.resources?.language);
        this.buildList = this.customSelectKeyAndValue(response.resources?.build);
        this.raceList = this.customSelectKeyAndValue(response.resources?.race)
        this.heightList = response.resources?.height;
      }
    });
  }

  customSelectKeyAndValue(arr){
     let selectArr = []
    arr.forEach(item=>{
      selectArr.push({id : item.displayName, displayName: item.displayName})
    })
    return selectArr;
  }
  vehicleModelList = []
  vehicleShapeList= []
  originalMakeList = []
  originalShapeList = []
  originalColorList = []
  originalModelList = []

  // veichle
  getDropDownList() {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_MAKE, null, false, null),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_MODEL, null, false, null),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_SHAPE, null, false, null),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_COLOR, null, false, null)],
    ).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
                this.originalMakeList = resp.resources;
                this.vehicleMakeList = getPDropdownData(resp.resources, "displayName", "displayName");
                break;
            case 1:
                this.originalModelList = resp.resources
                break;
            case 2:
                this.originalShapeList = resp.resources
                break;
            case 3:
                this.originalColorList =resp.resources
                break;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    })
  }
  selectedVModelID = ""
  selectedVNameID = ""
  selectedVShapeID = ""
  onElementChange(eve?,type?,action?){

    if(eve.originalEvent?.type == "input") return "";

    console.log("eve",eve)
    if(type == "vehicleMakeName"){
      this.finduForm.get('vehicleModel').reset();
      this.finduForm.get('vehicleBodyType').reset();
      this.finduForm.get('vehicleColour').reset();
      this.vehicleModelList = [];
      this.vehicleShapeList = [];
      this.vehicleColors = [];
      if(eve?.value){
        let selectedVName =  this.originalMakeList?.find(x => x.displayName == eve.value);
        console.log("selectedVName",selectedVName)
        this.selectedVNameID =  selectedVName != undefined &&  selectedVName?.id || "-1";
          this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_MODEL, this.selectedVNameID, false)
            .subscribe((response) => {
              this.vehicleModelList = getPDropdownData(response.resources, "displayName", "displayName");
            })

      }
    }
    else if(type == "vehicleModelName"){
      this.finduForm.get('vehicleBodyType').reset();
      this.finduForm.get('vehicleColour').reset();
      this.vehicleShapeList = [];
      this.vehicleColors = [];
      if(eve?.value){
        let selectedVModel =  this.originalModelList?.find(x => x.displayName == eve?.value);
        this.selectedVModelID =  selectedVModel != undefined &&  selectedVModel?.id || "-1";
          this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_SHAPE, this.selectedVModelID, false)
            .subscribe((response) => {
              this.vehicleShapeList = getPDropdownData(response.resources, "displayName", "displayName");
            })

      }
    }
    else if(type == "vehicleShapeName"){
      this.finduForm.get('vehicleColour').reset();
      let selectColorList = [];
      if(eve?.value){
          let selectedVShape =  this.originalShapeList?.find(x => x.displayName == eve?.value);
          this.selectedVShapeID =  selectedVShape != undefined &&  selectedVShape?.id || "-1";

          let allIds= this.selectedVNameID + "/" + this.selectedVModelID + "/" + this.selectedVShapeID;
          this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_COLORS,allIds).subscribe((response) => {
            response?.resources?.forEach(result =>{
              selectColorList.push(this.originalColorList?.find(x => x.displayName == result));
            })
            if(this.selectedVNameID == "-1" && this.selectedVModelID == "-1" && this.selectedVShapeID == "-1"){
              this.vehicleColors = getPDropdownData(this.vehicleColors, "displayName", "displayName");
            }
            else{
              this.vehicleColors = getPDropdownData(selectColorList, "displayName", "displayName");
            }
          })

      }
    }
    this.rxjsService.setGlobalLoaderProperty(false);

  }
}
