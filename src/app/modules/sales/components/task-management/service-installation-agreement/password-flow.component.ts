import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import {
  btnActionTypes, CustomerModuleApiSuffixModels, CustomerVerificationType, PasswordsAddEditModel, SalesModuleApiSuffixModels,
  selectLeadCreationStepperParamsState$, selectStaticEagerLoadingDogTypesState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, SnackbarService
} from '@app/shared';
import { ModuleName } from '@app/shared/enums';
import { CrudService, RxjsService } from '@app/shared/services';
import {
  clearFormControlValidators,
  formConfigs, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareGetRequestHttpParams, prepareRequiredHttpParams, setRequiredValidator
} from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { DocTypes, LeadHeaderData, MedicalConditions, ServiceAgreementStepsParams, SiteSpecificInputObjectModel } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { selectLeadHeaderDataState$ } from '../lead-creation-ngrx-files';
@Component({
  selector: 'app-password-flow',
  templateUrl: './password-flow.component.html',
  styleUrls: ['./service-installation-agreement.scss']
})

export class PasswordFlowComponent implements OnInit {
  @Input() inputObject = new SiteSpecificInputObjectModel();
  @Input() permission;
  isDisabled = false;
  params: ServiceAgreementStepsParams;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  passwordsForm: FormGroup;
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  leadHeaderData: LeadHeaderData;
  dogTypes: FormArray;
  medicalConditions: FormArray;
  @ViewChildren('input') rows: QueryList<any>;
  dogTypesList = [];
  details;
  show: boolean = false;
  showPlus: boolean = false;
  currentAgreementTabObj;
  loggedInUserData: LoggedInUserModel;
  isPasswordShow = false;
  isPasswordShowForDistressWord = false;
  breadCrumb: BreadCrumbModel;
  pScrollStyleHeight;
  uniqueCallId;

  constructor(private crudService: CrudService, private snackbarService: SnackbarService, private dialog: MatDialog, private router: Router,
    private formBuilder: FormBuilder, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    public rxjsService: RxjsService, private store: Store<AppState>,
  ) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectLeadHeaderDataState$), this.store.select(selectStaticEagerLoadingDogTypesState$),
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      this.leadHeaderData = response[1];
      this.dogTypesList = response[2];
      this.loggedInUserData = new LoggedInUserModel(response[3]);
      this.currentAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['agreementTabName'].toLowerCase().includes('special'));
      if (this.leadHeaderData) {
        this.breadCrumb = {
          pageTitle: { key: 'Password & Special Instructions' },
          items: [{
            key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
          }, { key: 'Service Agreement' }, { key: 'Password & Special Instructions' }]
        };
      }
    });
  }

  ngOnInit(): void {
    this.isDisabled = this.inputObject.fromComponentType == 'lead' ? false : true;
    this.pScrollStyleHeight = this.inputObject.fromComponentType == 'lead' ? '41vh' : 'auto';
    this.createPasswordForm();
    this.getSpecialInstructionsByLeadAndPartitionId();
    this.onFormControlChanges();
  }

  showNoOfDogs: boolean = false;
  onFormControlChanges(): void {
    this.passwordsForm.get('isDogs').valueChanges.subscribe((isDogOnSite: boolean) => {
      if (!isDogOnSite) {
        if (this.details.specialInstructionId) {
          if (this.dogTypes && this.dogTypes.length > 0) {
            this.snackbarService.openSnackbar("Please delete existing dogs", ResponseMessageTypes.WARNING);
            setTimeout(() => {
              this.passwordsForm.get('isDogs').setValue(true);
            }, 10)
          }
        }
      }
    });
    this.passwordsForm.get('noOfDogs').valueChanges.subscribe((noOfDogs: any) => {
      if (noOfDogs > 0)
        this.passwordsForm = setRequiredValidator(this.passwordsForm, ["dogDescription"]);
      else
        this.passwordsForm = clearFormControlValidators(this.passwordsForm, ["dogDescription"]);
      if (noOfDogs) {
        this.showPlus = true;
      }
      else {
        this.showPlus = false;
        this.show = false;
      }
    });
  }

  getSpecialInstructionsByLeadAndPartitionId(): void {
    let resultObservable = this.inputObject.fromComponentType == 'lead' ?
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SPECIAL_INSTRUCTIONS, undefined, false,
        prepareGetRequestHttpParams(undefined, undefined, {
          leadId: this.serviceAgreementStepsParams.leadId,
          quotationVersionId: this.serviceAgreementStepsParams.quotationVersionId,
          customerId: this.serviceAgreementStepsParams.customerId,
          partitionId: this.serviceAgreementStepsParams.partitionId,
          isExist: this.serviceAgreementStepsParams.isExist
        })) :
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SPECIAL_INSTRUCTIONS, undefined, false,
        prepareGetRequestHttpParams(undefined, undefined, {
          customerId: this.inputObject.customerId,
          addressId: this.inputObject.addressId,
          partitionId: this.inputObject.partitionId,
          quotationVersionId: this.inputObject.quotationVersionId
        }));
    resultObservable.subscribe((response: IApplicationResponse) => {
      if (response.statusCode == 200 && response.resources && response.isSuccess) {
        this.details = response.resources;
        if (this.dogTypes) {
          this.dogTypes.clear();
        }
        if (this.details.contractId || this.details.customerId) {
          this.passwordsForm.patchValue(response.resources);
          this.passwordsForm.get('isDogs').setValue(this.details.isDogs);
          this.dogTypes = this.getdogTypesListArray;
          this.medicalConditions = this.getMedicalConditionListArray;
          if (response.resources.dogTypes.length > 0) {
            this.show = true;
            response.resources.dogTypes.forEach((dg) => {
              this.dogTypes.push(this.createDogTypesListModel(dg));
            });
          };
          if (this.details.contractId) {
            this.passwordsForm.get('isDogs').disable();
          }
          this.medicalConditions = this.getMedicalConditionListArray;
          while (this.getMedicalConditionListArray.length) {
            this.getMedicalConditionListArray.removeAt(this.getMedicalConditionListArray.length - 1);
          }
          if (response.resources.medicalConditions.length > 0) {
            response.resources.medicalConditions.forEach((md) => {
              this.medicalConditions.push(this.createMedicalConditionListModel(md));
            });
          }
          else {
            this.medicalConditions.push(this.createMedicalConditionListModel());
          }
        } else {
          this.medicalConditions = this.getMedicalConditionListArray;
          this.medicalConditions.push(this.createMedicalConditionListModel());
        }
      } else {
        this.medicalConditions = this.getMedicalConditionListArray;
        this.medicalConditions.push(this.createMedicalConditionListModel());
      }
      if (this.inputObject.fromComponentType == 'customer') {
        this.serviceAgreementStepsParams = new ServiceAgreementStepsParams({
          leadId: this.inputObject.leadId,
          customerId: this.inputObject.customerId,
          quotationVersionId: this.inputObject.quotationVersionId,
          createdUserId: this.loggedInUserData.userId,
          modifiedUserId: this.loggedInUserData.userId,
          partitionId: this.inputObject.partitionId
        });
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createPasswordForm(): void {
    let passwordsAddEditModel = new PasswordsAddEditModel();
    this.passwordsForm = this.formBuilder.group({
      dogTypes: this.formBuilder.array([]),
      medicalConditions: this.formBuilder.array([])
    });
    Object.keys(passwordsAddEditModel).forEach((key) => {
      this.passwordsForm.addControl(key, new FormControl(passwordsAddEditModel[key]));
    });
    this.passwordsForm = setRequiredValidator(this.passwordsForm, ["alarmCancellationPassword", "distressWord"]);
  }

  get getdogTypesListArray(): FormArray {
    if (!this.passwordsForm) return;
    return this.passwordsForm.get("dogTypes") as FormArray;
  }

  createDogTypesListModel(docTypes?: DocTypes): FormGroup {
    let docTypesFormControlModel = new DocTypes(docTypes);
    let formControls = {};
    Object.keys(docTypesFormControlModel).forEach((key) => {
      if (key == 'dogTypeId') {
        formControls[key] = [{ value: docTypesFormControlModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: docTypesFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  get getMedicalConditionListArray(): FormArray {
    if (!this.passwordsForm) return;
    return this.passwordsForm.get("medicalConditions") as FormArray;
  }

  createMedicalConditionListModel(medicalConditions?: MedicalConditions): FormGroup {
    let medicalConditionsFormControlModel = new MedicalConditions(medicalConditions);
    let formControls = {};
    Object.keys(medicalConditionsFormControlModel).forEach((key) => {
      formControls[key] = [{ value: medicalConditionsFormControlModel[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  addItem(event): void {
    this.dogTypes = this.getdogTypesListArray;
    let docTypesFormControlModel = new DocTypes();
    let dogs = parseInt(event.target.value);
    if (event.target.value) {
      if (this.details && this.details?.isDogs && dogs < this.details?.dogTypes.length) {
        this.snackbarService.openSnackbar("Please delete existing dogs", ResponseMessageTypes.WARNING);
        this.passwordsForm.get('noOfDogs').setValue(this.details?.dogTypes.lengthh);
        return;
      }
      else if (this.details && this.details?.isDogs && dogs >= this.details?.dogTypes.length) {
        this.dogTypes.clear();
        this.details.dogTypes.forEach((dg) => {
          this.dogTypes.push(this.createDogTypesListModel(dg));
        });
        for (var i = this.details?.dogTypes.length; i < dogs; i++) {
          this.dogTypes.insert(i, this.createDogTypesListModel(docTypesFormControlModel));
        }
      } else {
        this.dogTypes.clear();
        for (var j = 0; j < this.passwordsForm.value.noOfDogs; j++) {
          this.dogTypes.insert(j, this.createDogTypesListModel(docTypesFormControlModel));
        }
      }
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  addMedicalItem(): void {
    this.medicalConditions = this.getMedicalConditionListArray;
    this.medicalConditions.insert(0, this.createMedicalConditionListModel());
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  deleteDogType(i: number) {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, null, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) return;
        if (this.getdogTypesListArray.controls[i].value.dogTypeDetailId && this.getdogTypesListArray.length > 0) {
          let resultObservable = this.inputObject.fromComponentType == 'lead' ?
            this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_DELETE_DOG_TYPES,
              undefined,
              prepareRequiredHttpParams({
                Ids: this.getdogTypesListArray.controls[i].value.dogTypeDetailId,
                IsDeleted: false,
                modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
              })) :
            this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.DOG_TYPES,
              undefined,
              prepareRequiredHttpParams({
                Ids: this.getdogTypesListArray.controls[i].value.dogTypeDetailId,
                IsDeleted: false,
                modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
              }));
          resultObservable.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.getdogTypesListArray.removeAt(i);
              this.details.dogTypes.splice(i, 1);
            }
            this.passwordsForm.get('noOfDogs').setValue(this.getdogTypesListArray.length);
          });
        }
        else {
          this.getdogTypesListArray.removeAt(i);
          this.passwordsForm.get('noOfDogs').setValue(this.getdogTypesListArray.length);
          this.rxjsService.setFormChangeDetectionProperty(true);
        }
      });
  }

  deleteMedical(i: number) {
    if (this.getMedicalConditionListArray.controls[i].value.medicalDetailId == '') {
      this.getMedicalConditionListArray.removeAt(i);
      return;
    }
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, null, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) return;
        if (this.getMedicalConditionListArray.controls[i].value.medicalDetailId && this.getMedicalConditionListArray.length > 1) {
          let resultObservable = this.inputObject.fromComponentType == 'lead' ?
            this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_DELETE_MEDICAL,
              undefined,
              prepareRequiredHttpParams({
                Ids: this.getMedicalConditionListArray.controls[i].value.medicalDetailId,
                IsDeleted: false,
                modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
              })) :
            this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.MEDICAL_CONDITIONS,
              undefined,
              prepareRequiredHttpParams({
                Ids: this.getMedicalConditionListArray.controls[i].value.medicalDetailId,
                IsDeleted: false,
                modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
              }));
          resultObservable.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.getMedicalConditionListArray.removeAt(i);
            }
            if (this.getMedicalConditionListArray.length === 0) {
              this.addMedicalItem();
            };
          });
        }
        else {
          this.getdogTypesListArray.removeAt(i);
          this.rxjsService.setFormChangeDetectionProperty(true);
        }
      });
  }

  onSubmit(): void {
    let payload = this.passwordsForm.getRawValue();
    if (!payload.isDogs) {
      if (this.dogTypes) {
        this.dogTypes.clear();
      }
    }
    if (this.passwordsForm.invalid) {
      return;
    }
    payload.createdUserId = this.serviceAgreementStepsParams.createdUserId;
    payload.modifiedUserId = this.loggedInUserData.userId;
    payload.leadId = this.serviceAgreementStepsParams.leadId;
    payload.customerId = this.serviceAgreementStepsParams.customerId;
    payload.quotationVersionId = this.serviceAgreementStepsParams.quotationVersionId;
    payload.partitionId = this.serviceAgreementStepsParams.partitionId;
    payload.addressId = (this.inputObject.fromComponentType == 'customer') ? this.inputObject.addressId :
      this.serviceAgreementStepsParams.addressId;
    let data = payload.dogTypes;
    let detailId;
    if (data.length > 0) {
      data.forEach(element => {
        detailId = element.dogDetailId
      });
    }
    if (detailId) {
      if (data.length > 0) {
        data.forEach(element => {
          element.dogDetailId = detailId;
        });
      }
    }
    payload.dogTypes = data;
    if (!payload.isDogs) {
      payload.dogTypes = null;
      payload.noOfDogs = null;
    }
    let resultObservable = this.inputObject.fromComponentType == 'lead' ?
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SPECIAL_INSTRUCTIONS, payload) :
      this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SPECIAL_INSTRUCTIONS, payload);
    resultObservable.subscribe(resp => {
      if (resp.resources && resp.statusCode == 200 && resp.isSuccess) {
        this.getSpecialInstructionsByLeadAndPartitionId();
        if (!this.passwordsForm.get('specialInstructionId').value) {
          this.passwordsForm.get('specialInstructionId').setValue(resp.resources);
        }
      }
    });
  }

  redirectToTab(type: btnActionTypes | string = btnActionTypes.SUBMIT): void {
    if (this.inputObject.fromComponentType == 'lead' && type == btnActionTypes.BACK_TO_LEAD) {
      this.rxjsService.navigateToProcessQuotationPage();
    }
    else if (this.inputObject.fromComponentType == 'customer' && type == btnActionTypes.BACK_TO_LEAD) {
      this.isDisabled = true;
      this.getSpecialInstructionsByLeadAndPartitionId();
    }
  }

  onEditBtnClicked() {
    if(this.inputObject.fromComponentType =="customer" && !this.permission.canEdit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    let filterObj = { userId: this.loggedInUserData?.userId, customerId: this.inputObject?.customerId, siteAddressId: this.inputObject?.addressId, uniqueCallId: this.uniqueCallId ? this.uniqueCallId : null };
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CHECK, null, false,
      prepareRequiredHttpParams(filterObj))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.isDisabled = false;
          this.focusInAndOutFormArrayFields();
        } else {
          this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: this.inputObject?.customerId, siteAddressId: this.inputObject?.addressId, tab: CustomerVerificationType.SPECIAL_INSTRUCTIONS, customerTab: 7, monitoringTab: 0, } })
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToProcessQuotationPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else {
        if ((this.passwordsForm.value?.specialInstructionId &&
          this.currentAgreementTabObj.isMandatory) || !this.currentAgreementTabObj.isMandatory) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
    }
  }
}
