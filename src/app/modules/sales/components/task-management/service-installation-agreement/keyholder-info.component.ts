import { Component, ElementRef, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  btnActionTypes, KeyHolderInfoAddEditModel,
  loggedInUserData,
  SalesModuleApiSuffixModels, selectLeadCreationStepperParamsState$, selectLeadHeaderDataState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel, countryCodes, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR,
  prepareGetRequestHttpParams,
  prepareRequiredHttpParams, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService
} from '@app/shared';
import { ModuleName, ResponseMessageTypes } from '@app/shared/enums';
import { CustomerModuleApiSuffixModels, CustomerVerificationType } from '@modules/customer';
import { EmergencyContactModel, KeyholderObjectModel, LeadHeaderData, ServiceAgreementStepsParams, SiteSpecificInputObjectModel } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
@Component({
  selector: 'app-keyholder-info',
  templateUrl: './keyholder-info.component.html',
  styleUrls: ['./service-installation-agreement.scss']
})

export class KeyholderInfoComponent implements OnInit {
  @Input() inputObject = new SiteSpecificInputObjectModel();
  @Input() permission;
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  formConfigs = formConfigs;
  keyHolderInfoForm: FormGroup;
  keyholders: FormArray;
  emergencyContacts: FormArray;
  countryCodes = countryCodes;
  leadHeaderData: LeadHeaderData;
  currentAgreementTabObj;
  @ViewChildren('keyholderRows') keyholderRows: QueryList<ElementRef>;
  @ViewChildren('keyholderInput') keyholderInput: QueryList<ElementRef>;
  @ViewChildren('keyholderInputPassword') keyholderInputPassword: QueryList<ElementRef>;
  @ViewChildren('emergencyContactRows') emergencyContactRows: QueryList<ElementRef>;
  @ViewChildren('emergencyContactInput') emergencyContactInput: QueryList<ElementRef>;
  isDisabled = false;
  loggedInUserData: LoggedInUserModel;
  breadCrumb: BreadCrumbModel;
  uniqueCallId;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  pScrollStyleHeight;

  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router, public rxjsService: RxjsService, private store: Store<AppState>,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private snackbarService: SnackbarService
  ) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD)
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
  }

  ngOnInit(): void {
    this.isDisabled = this.inputObject.fromComponentType == 'lead' ? false : true;
    this.pScrollStyleHeight = this.inputObject.fromComponentType == 'lead' ? '41vh' : 'auto';
    this.createKeyHolderInfoForm();
    this.getKeyholderById();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectLeadHeaderDataState$),
      this.store.select(loggedInUserData)]
    ).pipe(take(1)).subscribe((response) => {
      if (!response) return;
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      this.leadHeaderData = response[1];
      this.loggedInUserData = new LoggedInUserModel(response[2]);
      this.currentAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['agreementTabName'].toLowerCase().includes('key holder'));
      if (this.leadHeaderData) {
        this.breadCrumb = {
          pageTitle: { key: 'Keyholder Information' },
          items: [{
            key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
          }, { key: 'Service Agreement' }, { key: 'Keyholder Information' }]
        };
      }
    });
  }

  getKeyholderById(): void {
    let resultObservable = this.inputObject.fromComponentType == 'lead' ?
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_KEY_HOLDERS, undefined, false,
        prepareGetRequestHttpParams(undefined, undefined, {
          leadId: this.serviceAgreementStepsParams.leadId,
          quotationVersionId: this.serviceAgreementStepsParams.quotationVersionId,
          customerId: this.serviceAgreementStepsParams.customerId,
          partitionId: this.serviceAgreementStepsParams.partitionId,
          isExist: this.serviceAgreementStepsParams.isExist
        })) :
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.KEYHOLDER_DETAILS, undefined, false,
        prepareGetRequestHttpParams(undefined, undefined, {
          customerId: this.inputObject.customerId,
          addressId: this.inputObject.addressId,
          partitionId: this.inputObject.partitionId,
          quotationVersionId: this.inputObject.quotationVersionId
        }));
    resultObservable.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.keyholders = this.keyHoldersArray;
        while (this.keyHoldersArray.length) {
          this.keyHoldersArray.removeAt(this.keyHoldersArray.length - 1);
        }
        this.emergencyContacts = this.emergencyContactsArray;
        while (this.emergencyContactsArray.length) {
          this.emergencyContactsArray.removeAt(this.emergencyContactsArray.length - 1);
        }
        if (response.resources.keyholders.length == 0) {
          this.keyHoldersArray.push(this.createNewItem());
          this.keyHoldersArray.push(this.createNewItem());
          this.emergencyContactsArray.push(this.createNewItem('emergencyContacts'));
        }
        else {
          response.resources.keyholders.forEach((keyholderPersonObj: KeyholderObjectModel) => {
            this.keyHoldersArray.push(this.createNewItem('keyholders', keyholderPersonObj));
          });
          response.resources.emergencyContacts.forEach((emergencyContactModel: EmergencyContactModel) => {
            this.emergencyContactsArray.push(this.createNewItem('emergencyContacts', emergencyContactModel));
          });
        }
        if (response.resources.emergencyContacts.length == 0 && response.resources.keyholders.length > 0) {
          this.emergencyContactsArray.push(this.createNewItem('emergencyContacts'));
        }
        this.keyholders = this.keyHoldersArray;
        this.emergencyContacts = this.emergencyContactsArray;
        const keyHolderInfoAddEditModel = new KeyHolderInfoAddEditModel(response.resources);
        this.keyHolderInfoForm.patchValue(keyHolderInfoAddEditModel);
        if (response.resources.keyholders.length > 0 && response.resources.emergencyContacts.length > 0) {
          this.focusAllFields();
        };
        if (this.inputObject.fromComponentType == 'customer') {
          this.serviceAgreementStepsParams = new ServiceAgreementStepsParams({
            leadId: this.inputObject.leadId,
            customerId: this.inputObject.customerId,
            quotationVersionId: this.inputObject.quotationVersionId,
            createdUserId: this.loggedInUserData.userId,
            modifiedUserId: this.loggedInUserData.userId,
            partitionId: this.inputObject.partitionId
          });
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  focusAllFields() {
    setTimeout(() => {
      this.focusInAndOutFormArrayFields();
      this.focusInAndOutFormArrayFields('emergencyContacts');
    }, 100);
  }

  createKeyHolderInfoForm(): void {
    let keyHolderInfoAddEditModel = new KeyHolderInfoAddEditModel();
    this.keyHolderInfoForm = this.formBuilder.group({});
    Object.keys(keyHolderInfoAddEditModel).forEach((key) => {
      this.keyHolderInfoForm.addControl(key, new FormArray([]));
    });
  }

  createNewItem(itemType = 'keyholders', object?): FormGroup {
    let objectModel = itemType == 'keyholders' ? new KeyholderObjectModel(object) : new EmergencyContactModel(object);
    let formControls = {};
    if (itemType == 'keyholders') {
      Object.keys(objectModel).forEach((key) => {
        formControls[key] = [objectModel[key],
        (key === 'keyHolderId' || key === 'keyholderPersonId' || key == 'quotationVersionId' ||
          key === 'password' || key === 'createdUserId' || key === 'partitionId' || key == 'leadId') ? [] : [Validators.required]]
      });
    }
    else {
      Object.keys(objectModel).forEach((key) => {
        formControls[key] = [objectModel[key],
        (key === 'emergencyContactId' || key === 'createdUserId' || key === 'partitionId' || key == 'quotationVersionId' || key == 'leadId') ? [] : [Validators.required]]
      });
    }
    delete formControls['modifiedUserId'];
    formControls['createdUserId'][0] = this.loggedInUserData.userId;
    formControls['leadId'][0] = this.inputObject.fromComponentType == 'customer' ? this.inputObject.leadId : this.serviceAgreementStepsParams.leadId;
    formControls['partitionId'][0] = this.inputObject.fromComponentType == 'lead' ? this.serviceAgreementStepsParams.partitionId :
      this.inputObject.partitionId;
    formControls['customerId'][0] = this.inputObject.fromComponentType == 'lead' ? this.serviceAgreementStepsParams.customerId :
      this.inputObject.customerId;
    formControls['quotationVersionId'][0] = this.inputObject.fromComponentType == 'lead' ? this.serviceAgreementStepsParams.quotationVersionId :
      this.inputObject.quotationVersionId;
    return this.formBuilder.group(formControls);
  }

  focusInAndOutFormArrayFields(itemType = 'keyholders'): void {
    if (itemType == 'keyholders') {
      this.keyholderRows.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      });
      this.keyholderInput.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      });
    }
    else {
      this.emergencyContactRows.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      });
      this.emergencyContactInput.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      });
    }
  }

  addItem(itemType = 'keyholders'): void {
    if (itemType == 'keyholders') {
      if (this.keyHoldersArray.invalid) {
        this.focusInAndOutFormArrayFields(itemType);
        return;
      };
      this.keyholders = this.keyHoldersArray;
      let keyholderObjectModel = new KeyholderObjectModel();
      this.keyholders.insert(0, this.createNewItem(itemType, keyholderObjectModel));
    }
    else {
      if (this.emergencyContactsArray.invalid) {
        this.focusInAndOutFormArrayFields(itemType);
        return;
      };
      this.emergencyContacts = this.emergencyContactsArray;
      let emergencyContactModel = new EmergencyContactModel();
      this.emergencyContacts.insert(0, this.createNewItem(itemType, emergencyContactModel));
    }
  }

  removeItem(itemType = 'keyholders', i: number): void {
    if (itemType == 'keyholders') {
      if (!this.keyHoldersArray.controls[i].value.keyHolderId) {
        this.keyHoldersArray.removeAt(i);
        return;
      }
    }
    else {
      if (!this.emergencyContactsArray.controls[i].value.emergencyContactId) {
        this.emergencyContactsArray.removeAt(i);
        return;
      }
    }
    if (itemType == 'keyholders') {
      this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, null, DynamicConfirmByMessageConfirmationType.DANGER).
        onClose?.subscribe(dialogResult => {
          if (!dialogResult) return;
          if (this.keyHoldersArray.controls[i].value.keyHolderId && this.keyHoldersArray.length > 1) {
            let resultObservable = this.inputObject.fromComponentType == 'lead' ?
              this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_KEY_HOLDERS, undefined,
                prepareRequiredHttpParams({
                  ids: this.keyHoldersArray.controls[i].value.keyHolderId,
                  isDeleted: true,
                  modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
                })) :
              this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.KEYHOLDERS, undefined,
                prepareRequiredHttpParams({
                  ids: this.keyHoldersArray.controls[i].value.keyHolderId,
                  isDeleted: true,
                  modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
                }));
            resultObservable.subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                this.keyHoldersArray.removeAt(i);
              }
              if (this.keyHoldersArray.length === 0) {
                this.addItem();
              };
            });
          }
          else {
            this.keyHoldersArray.removeAt(i);
          }
        });
    }
    else {
      this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, null, DynamicConfirmByMessageConfirmationType.DANGER).
        onClose?.subscribe(dialogResult => {
          if (!dialogResult) return;
          if (this.emergencyContactsArray.controls[i].value.emergencyContactId && this.emergencyContactsArray.length > 1) {
            let resultObservable = this.inputObject.fromComponentType == 'lead' ?
              this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_EMERGENCY_CONTACTS, undefined,
                prepareRequiredHttpParams({
                  ids: this.emergencyContactsArray.controls[i].value.emergencyContactId,
                  isDeleted: true,
                  modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
                })) :
              this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.EMERGENCY_CONTACTS, undefined,
                prepareRequiredHttpParams({
                  ids: this.emergencyContactsArray.controls[i].value.emergencyContactId,
                  isDeleted: true,
                  modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
                }))
            resultObservable.subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                this.emergencyContactsArray.removeAt(i);
              }
              if (this.emergencyContactsArray.length === 0) {
                this.addItem('emergencyContacts');
              };
            });
          }
          else {
            this.emergencyContactsArray.removeAt(i);
          }
        });
    }
  }

  get keyHoldersArray(): FormArray {
    if (!this.keyHolderInfoForm) {
      return;
    }
    return this.keyHolderInfoForm.get("keyholders") as FormArray;
  }

  get emergencyContactsArray(): FormArray {
    if (!this.keyHolderInfoForm) return;
    return this.keyHolderInfoForm.get("emergencyContacts") as FormArray;
  }

  getKeyHolders(form): Array<any> {
    return form.controls.keyholders.controls;
  }

  getEmergencyContacts(form): Array<any> {
    return form.controls.emergencyContacts.controls;
  }

  onSubmit(): void {
    if (this.keyHolderInfoForm.invalid) {
      return;
    }
    this.keyHolderInfoForm.value.keyholders.forEach((keyholderObj: KeyholderObjectModel, index: number) => {
      // Because of some duplication form control logics we directly fetch the value from the nativeElement 'this.keyholderRows'
      keyholderObj.contactNo = this.keyholderRows?.['_results'][index].nativeElement.value.replace(/\s/g, "");
      keyholderObj.password = this.keyholderInputPassword?.['_results'][index].nativeElement.value.replace(/\s/g, "");
      keyholderObj.partitionId = this.serviceAgreementStepsParams.partitionId;
      keyholderObj.quotationVersionId = this.serviceAgreementStepsParams.quotationVersionId;
      keyholderObj['addressId'] = (this.inputObject.fromComponentType == 'customer') ? this.inputObject.addressId :
        this.serviceAgreementStepsParams.addressId;
    });
    this.keyHolderInfoForm.value.emergencyContacts.forEach((emergencyContactObj: EmergencyContactModel, index: number) => {
      // Because of some duplication form control logics we directly fetch the value from the nativeElement 'this.emergencyContactRows'
      emergencyContactObj.contactNo = this.emergencyContactRows?.['_results'][index].nativeElement.value.replace(/\s/g, "");
      emergencyContactObj.quotationVersionId = this.serviceAgreementStepsParams.quotationVersionId;
      emergencyContactObj.partitionId = this.serviceAgreementStepsParams.partitionId;
      emergencyContactObj['addressId'] = (this.inputObject.fromComponentType == 'customer') ? this.inputObject.addressId :
        this.serviceAgreementStepsParams.addressId;
    });
    let crudService: Observable<IApplicationResponse> = this.inputObject.fromComponentType == 'lead' ? this.crudService.create(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_KEY_HOLDERS, this.keyHolderInfoForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.KEYHOLDERS, this.keyHolderInfoForm.value);
    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.redirectToTab(btnActionTypes.NEXT);
        if (response.resources.keyholders) {
          response.resources.keyholders.forEach((keyholderPersonObj: KeyholderObjectModel, index: number) => {
            this.keyHoldersArray.controls[index].get('keyHolderId').setValue(keyholderPersonObj.keyHolderId);
          });
        }
        if (response.resources.emergencyContacts) {
          response.resources.emergencyContacts.forEach((emergencyContactModel: EmergencyContactModel, index: number) => {
            this.emergencyContactsArray.controls[index].get('emergencyContactId').setValue(emergencyContactModel.emergencyContactId);
          });
        }
      }
    });
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToProcessQuotationPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else {
        if (!this.currentAgreementTabObj.isMandatory || (this.keyHoldersArray.controls[0].get('keyHolderId').value &&
          this.emergencyContactsArray.controls[0].get('emergencyContactId').value &&
          this.currentAgreementTabObj.isMandatory)) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
    }
  }

  redirectToTab(type?: btnActionTypes | string): void {
    if (type == btnActionTypes.BACK_TO_LEAD && this.inputObject.fromComponentType == 'lead') {
      this.rxjsService.navigateToProcessQuotationPage();
    }
    else if (this.inputObject.fromComponentType == 'customer' && type == btnActionTypes.BACK_TO_LEAD) {
      this.isDisabled = true
      this.getKeyholderById();
    }
  }

  onEditBtnClicked() {
    if (this.inputObject.fromComponentType == "customer" && !this.permission.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.activatedRoute?.snapshot?.queryParams?.feature_name) {
      this.isDisabled = false;
      if (this.keyholders.value[0]?.keyHolderId) {
        this.focusAllFields();
      }
    } else {
      this.rxjsService.setGlobalLoaderProperty(true);
      let filterObj = { userId: this.loggedInUserData?.userId, customerId: this.inputObject?.customerId, siteAddressId: this.inputObject?.addressId, uniqueCallId: this.uniqueCallId ? this.uniqueCallId : null };
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CHECK, null, false,
        prepareRequiredHttpParams(filterObj))
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.isDisabled = false;
            if (this.keyholders.value[0]?.keyHolderId) {
              this.focusAllFields();
            }
          } else {
            this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: this.inputObject?.customerId, siteAddressId: this.inputObject?.addressId, tab: CustomerVerificationType.KEY_HOLDER_INFORMATION, customerTab: 7, monitoringTab: 0, } })
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }
}