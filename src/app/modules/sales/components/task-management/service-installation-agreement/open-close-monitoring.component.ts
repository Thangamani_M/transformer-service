import { Component, ElementRef, Input, OnInit, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  btnActionTypes, CustomerModuleApiSuffixModels,
  CustomerVerificationType,
  loggedInUserData,
  OpenCloseMonitoringAddEditModel, SalesModuleApiSuffixModels, selectLeadCreationStepperParamsState$,
  selectLeadHeaderDataState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel, countryCodes, CrudService, DynamicConfirmByMessageConfirmationType,
  emailPattern, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  onBlurActionOnDuplicatedFormControl,
  onFormControlChangesToFindDuplicate,
  PERMISSION_RESTRICTION_ERROR,
  prepareGetRequestHttpParams,
  prepareRequiredHttpParams, ResponseMessageTypes,
  ReusablePrimeNGTableFeatureService,
  RxjsService,
  SnackbarService
} from '@app/shared';
import { ModuleName } from '@app/shared/enums';
import { MomentService } from '@app/shared/services/moment.service';
import { LeadHeaderData, OpenCloseContact, ServiceAgreementStepsParams, SiteSpecificInputObjectModel } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
@Component({
  selector: 'app-open-close-monitoring',
  templateUrl: './open-close-monitoring.component.html',
  styleUrls: ['./service-installation-agreement.scss']
})

export class OpenAndCloseMonitoringComponent implements OnInit {
  @Input() inputObject = new SiteSpecificInputObjectModel();
  @Input() permission;
  isDisabled = false;
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  formConfigs = formConfigs;
  openCloseMonitoringForm: FormGroup;
  countryCodes = countryCodes;
  leadHeaderData: LeadHeaderData;
  openAndCloseMonitoringId;
  contacts: FormArray;
  currentAgreementTabObj;
  loggedInUserData: LoggedInUserModel;
  @ViewChildren('contactRows') contactRows: QueryList<ElementRef>;
  breadCrumb: BreadCrumbModel;
  uniqueCallId;
  pScrollStyleHeight;

  constructor(private crudService: CrudService, private activateRoute: ActivatedRoute,
    private formBuilder: FormBuilder, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    public rxjsService: RxjsService, private store: Store<AppState>,
    private snackbarService: SnackbarService, private router: Router, private momentService: MomentService) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectLeadHeaderDataState$), this.store.select(loggedInUserData)]
    ).pipe(take(1)).subscribe((response) => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      this.leadHeaderData = response[1];
      this.loggedInUserData = new LoggedInUserModel(response[2]);
      this.currentAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['agreementTabName'].toLowerCase().includes('arming'));
      if (this.leadHeaderData) {
        this.breadCrumb = {
          pageTitle: { key: 'Arming & Disarming Time' },
          items: [{
            key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage'
          }, { key: 'Service Agreement' }, { key: 'Arming & Disarming Time' }]
        };
      }
    });
  }

  ngOnInit(): void {
    this.isDisabled = this.inputObject.fromComponentType == 'lead' ? false : true;
    this.pScrollStyleHeight = this.inputObject.fromComponentType == 'lead' ? '41vh' : 'auto';
    this.createOpenCloseMonitoringForm();
    this.getArmingAndDisarmingByLeadAndPartitionId();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      this.getArmingAndDisarmingByLeadAndPartitionId()
    }
  }

  getSelectedValue(event) {
    if (!event) return;
    this.toggleAddDays({ checked: this.openCloseMonitoringForm.get('allDays').value })
  }

  focusInAndOutFormArrayFields(): void {
    setTimeout(() => {
      this.contactRows.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      })
    }, 100);
  }

  getContactControl(form): Array<any> {
    return form.controls.contacts.controls;
  }

  addItem() {
    if (this.contacts.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.contacts = this.getContacts;
    let openCloseContactModel = new OpenCloseContact();
    this.contacts.insert(0, this.createContact(openCloseContactModel));
  }

  onFormControlChanges(formControlName: string, formArray: FormArray, formArrayName: string, currentCursorIndex: number): void {
    onFormControlChangesToFindDuplicate(formControlName, formArray, formArrayName, currentCursorIndex, this.openCloseMonitoringForm);
  }

  onBlur(formControlName: string, formArray: FormArray, e, index: number) {
    onBlurActionOnDuplicatedFormControl(formControlName, formArray, e, index);
  }

  createOpenCloseMonitoringForm(): void {
    let openCloseMonitoringAddEditModel = new OpenCloseMonitoringAddEditModel();
    this.openCloseMonitoringForm = this.formBuilder.group({
      contacts: this.formBuilder.array([]),
    });
    Object.keys(openCloseMonitoringAddEditModel).forEach((key) => {
      if (key == 'monthlyReportEmailTo') {
        this.openCloseMonitoringForm.addControl(key,
          new FormControl(openCloseMonitoringAddEditModel[key], Validators.compose([Validators.email, emailPattern])));
      }
      else {
        this.openCloseMonitoringForm.addControl(key,
          new FormControl(openCloseMonitoringAddEditModel[key]));
      }
    });
    this.openCloseMonitoringForm.get('mondayArmingTime').valueChanges.subscribe(val => {
      this.setFormControlValidators(val, 'mondayDisarmingTime');
    });
    this.openCloseMonitoringForm.get('tuesdayArmingTime').valueChanges.subscribe(val => {
      this.setFormControlValidators(val, 'tuesdayDisarmingTime');
    });
    this.openCloseMonitoringForm.get('wednesdayArmingTime').valueChanges.subscribe(val => {
      this.setFormControlValidators(val, 'wednesdayDisarmingTime');
    });
    this.openCloseMonitoringForm.get('thursdayArmingTime').valueChanges.subscribe(val => {
      this.setFormControlValidators(val, 'thursdayDisarmingTime');
    });
    this.openCloseMonitoringForm.get('fridayArmingTime').valueChanges.subscribe(val => {
      this.setFormControlValidators(val, 'fridayDisarmingTime');
    });
    this.openCloseMonitoringForm.get('saturdayArmingTime').valueChanges.subscribe(val => {
      this.setFormControlValidators(val, 'saturdayDisarmingTime');
    });
    this.openCloseMonitoringForm.get('sundayArmingTime').valueChanges.subscribe(val => {
      this.setFormControlValidators(val, 'sundayDisarmingTime');
    });
    this.openCloseMonitoringForm.get('publicHolidayArmingTime').valueChanges.subscribe(val => {
      this.setFormControlValidators(val, 'publicHolidayDisarmingTime');
    });
  }

  setFormControlValidators(val, formControlName: string) {
    if (!val) return;
    this.openCloseMonitoringForm.get(formControlName).setValue(null);
    this.openCloseMonitoringForm.get(formControlName).setValidators([Validators.required]);
    this.openCloseMonitoringForm.get(formControlName).updateValueAndValidity();
  }

  get getContacts(): FormArray {
    if (!this.openCloseMonitoringForm) return;
    return this.openCloseMonitoringForm.get("contacts") as FormArray;
  }

  createContact(openCloseContact?: OpenCloseContact): FormGroup {
    let openCloseContactModel = new OpenCloseContact(openCloseContact);
    let formControls = {};
    Object.keys(openCloseContactModel).forEach((key) => {
      formControls[key] = [openCloseContactModel[key],
      (key === 'smsNo') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  toggleAddDays(event): void {
    let mondayArmingTime = this.openCloseMonitoringForm.get('mondayArmingTime').value;
    let mondayDisarmingTime = this.openCloseMonitoringForm.get('mondayDisarmingTime').value;
    if (event.checked && (mondayArmingTime || mondayDisarmingTime)) {
      this.openCloseMonitoringForm.patchValue({
        "tuesdayArmingTime": mondayArmingTime,
        "wednesdayArmingTime": mondayArmingTime,
        "thursdayArmingTime": mondayArmingTime,
        "fridayArmingTime": mondayArmingTime,
      })
      setTimeout(() => {
        this.openCloseMonitoringForm.patchValue({
          "tuesdayDisarmingTime": mondayDisarmingTime,
          "wednesdayDisarmingTime": mondayDisarmingTime,
          "thursdayDisarmingTime": mondayDisarmingTime,
          "fridayDisarmingTime": mondayDisarmingTime,
        })
      }, 0);
    }
  }

  removeItem(i: number): void {
    if (!this.contacts.controls[i].value.openAndCloseMonitoringContactId) {
      this.contacts.removeAt(i);
      return;
    }
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, null, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) return;
        if (this.contacts.controls[i].value.openAndCloseMonitoringContactId && this.contacts.length > 1) {
          let resultObservable = this.inputObject.fromComponentType == 'lead' ?
            this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DELETE_OPENCLOSE_CONTACT, undefined,
              prepareRequiredHttpParams({
                ids: this.contacts.controls[i].value.openAndCloseMonitoringContactId,
                isDeleted: true,
                modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
              })) :
            this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.ARMING_DISARMING_CONTACTS, undefined,
              prepareRequiredHttpParams({
                ids: this.contacts.controls[i].value.openAndCloseMonitoringContactId,
                modifiedUserId: this.serviceAgreementStepsParams.modifiedUserId
              }));
          resultObservable.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              this.contacts.removeAt(i);
            }
            if (this.contacts.length === 0) {
              this.addItem();
            };
          });
        }
        else {
          this.contacts.removeAt(i);
          this.rxjsService.setFormChangeDetectionProperty(true);
        }
      });
  }

  todayEndTime(dateTime) {
    if (!dateTime) return;
    let date = new Date(dateTime).setHours(23, 59);
    return new Date(date);
  }

  formateTime(time) {
    if (!time) return
    let timeToRailway = this.momentService.convertNormalToRailayTimeOnly(time);
    let TimeArray: any = timeToRailway.split(':');
    let date = (new Date).setHours(TimeArray[0], TimeArray[1]);
    return new Date(date);
  }

  getArmingAndDisarmingByLeadAndPartitionId(): void {
    if (this.inputObject.fromComponentType == 'lead') {
      if (!this.serviceAgreementStepsParams?.quotationVersionId) {
        return;
      }
    } else {
      if (!this.inputObject?.quotationVersionId) {
        return;
      }
    }
    let resultObservable = this.inputObject.fromComponentType == 'lead' ?
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.GET_OPEN_CLOSE_MONITORING, undefined, false,
        prepareGetRequestHttpParams(undefined, undefined, {
          leadId: this.serviceAgreementStepsParams.leadId,
          partitionId: this.serviceAgreementStepsParams.partitionId,
          quotationVersionId: this.serviceAgreementStepsParams.quotationVersionId,
          customerId: this.serviceAgreementStepsParams.customerId,
          isExist: this.serviceAgreementStepsParams.isExist
        })) :
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.ARMING_AND_DISARMING, undefined, false,
        prepareGetRequestHttpParams(undefined, undefined, {
          customerId: this.inputObject.customerId,
          addressId: this.inputObject.addressId,
          partitionId: this.inputObject.partitionId,
          quotationVersionId: this.inputObject.quotationVersionId
        }));
    resultObservable.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        if (response.resources && (response.resources.openAndCloseMonitoringId || response.resources.customerId)) {
          response.resources.mondayArmingTime = this.formateTime(response.resources.mondayArmingTime)
          response.resources.mondayDisarmingTime = this.formateTime(response.resources.mondayDisarmingTime)
          response.resources.tuesdayArmingTime = this.formateTime(response.resources.tuesdayArmingTime)
          response.resources.tuesdayDisarmingTime = this.formateTime(response.resources.tuesdayDisarmingTime)
          response.resources.thursdayDisarmingTime = this.formateTime(response.resources.thursdayDisarmingTime)
          response.resources.thursdayArmingTime = this.formateTime(response.resources.thursdayArmingTime)
          response.resources.wednesdayArmingTime = this.formateTime(response.resources.wednesdayArmingTime)
          response.resources.wednesdayDisarmingTime = this.formateTime(response.resources.wednesdayDisarmingTime)
          response.resources.fridayArmingTime = this.formateTime(response.resources.fridayArmingTime)
          response.resources.fridayDisarmingTime = this.formateTime(response.resources.fridayDisarmingTime)
          response.resources.saturdayArmingTime = this.formateTime(response.resources.saturdayArmingTime)
          response.resources.saturdayDisarmingTime = this.formateTime(response.resources.saturdayDisarmingTime)
          response.resources.sundayArmingTime = this.formateTime(response.resources.sundayArmingTime)
          response.resources.sundayDisarmingTime = this.formateTime(response.resources.sundayDisarmingTime)
          response.resources.publicHolidayArmingTime = this.formateTime(response.resources.publicHolidayArmingTime)
          response.resources.publicHolidayDisarmingTime = this.formateTime(response.resources.publicHolidayDisarmingTime)
          this.contacts = this.getContacts;
          while (this.contacts.length) {
            this.contacts.removeAt(this.contacts.length - 1);
          }
          response.resources.contacts.forEach((obj) => {
            if (typeof obj.smsNo === "string") {
              obj.smsNo = obj.smsNo.split(" ").join("")
              obj.smsNo = obj.smsNo.replace(/"/g, "");
            }
            this.contacts.push(this.createContact(obj));
          });
          this.openAndCloseMonitoringId = response.resources.openAndCloseMonitoringId
          this.openCloseMonitoringForm.patchValue(response.resources);
        }
        else {
          this.getContacts.clear();
          this.contacts = this.getContacts;
          this.contacts.push(this.createContact());
        }
        if (response.resources && response.resources.contacts.length > 0) {
          setTimeout(() => {
            this.focusInAndOutFormArrayFields();
          }, 10);
        }
      }
      if (this.inputObject.fromComponentType == 'customer') {
        this.serviceAgreementStepsParams = new ServiceAgreementStepsParams({
          leadId: this.inputObject.leadId,
          customerId: this.inputObject.customerId,
          quotationVersionId: this.inputObject.quotationVersionId,
          createdUserId: this.loggedInUserData.userId,
          modifiedUserId: this.loggedInUserData.userId,
          partitionId: this.inputObject.partitionId
        });
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  ifEmpty(val) {
    if (!val && !this.openCloseMonitoringForm.get('allDays').value)
      this.snackbarService.openSnackbar('Select Start Time', ResponseMessageTypes.WARNING);
  }

  onSubmit(): void {
    if (this.openCloseMonitoringForm.invalid) return;
    const openCloseMonitoringAddEditModel = new OpenCloseMonitoringAddEditModel(this.openCloseMonitoringForm.value);
    Object.keys(openCloseMonitoringAddEditModel).forEach((key) => {
      if (openCloseMonitoringAddEditModel[key]) {
        if (key == "createdUserId" || key == "contacts" || key == "customerId" || key == "customerId" || key == "partitionId" || key == "quotationVersionId" || key == "openAndCloseMonitoringId" || key == "leadId" || key == "countryCode" ||
          key == "smsNumber" || key == "smsNumberCountryCode" || key == "monthlyReportEmailTo") {
          openCloseMonitoringAddEditModel[key] = openCloseMonitoringAddEditModel[key]
        } else {
          openCloseMonitoringAddEditModel[key] = this.momentService.convertNormalToRailayTime(openCloseMonitoringAddEditModel[key])
        }
      }
    });
    let contacts = this.openCloseMonitoringForm.value.contacts;
    contacts.forEach(element => {
      if (typeof element.smsNo === "string") {
        element.smsNo = element.smsNo.split(" ").join("")
        element.smsNo = element.smsNo.replace(/"/g, "");
      }
    });
    openCloseMonitoringAddEditModel.contacts = contacts;
    openCloseMonitoringAddEditModel.createdUserId = this.serviceAgreementStepsParams.createdUserId;
    openCloseMonitoringAddEditModel.modifiedUserId = this.loggedInUserData.userId;
    openCloseMonitoringAddEditModel.leadId = this.serviceAgreementStepsParams.leadId;
    openCloseMonitoringAddEditModel.partitionId = this.serviceAgreementStepsParams.partitionId;
    openCloseMonitoringAddEditModel.addressId = (this.inputObject.fromComponentType == 'customer') ? this.inputObject.addressId :
      this.serviceAgreementStepsParams.addressId;
    openCloseMonitoringAddEditModel.customerId = this.serviceAgreementStepsParams.customerId ? this.serviceAgreementStepsParams.customerId : this.activateRoute.snapshot.paramMap.get('id');
    openCloseMonitoringAddEditModel.quotationVersionId = this.serviceAgreementStepsParams.quotationVersionId;
    openCloseMonitoringAddEditModel.monthlyReportEmailTo = openCloseMonitoringAddEditModel.monthlyReportEmailTo ? openCloseMonitoringAddEditModel.monthlyReportEmailTo : null;
    openCloseMonitoringAddEditModel.openAndCloseMonitoringId = this.openCloseMonitoringForm.get('openAndCloseMonitoringId').value ?
      this.openCloseMonitoringForm.get('openAndCloseMonitoringId').value : this.openAndCloseMonitoringId;
    let crudService: Observable<IApplicationResponse> = this.inputObject.fromComponentType == 'lead' ?
      this.crudService.create(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.GET_OPEN_CLOSE_MONITORING, openCloseMonitoringAddEditModel) :
      this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.ARMING_AND_DISARMING, openCloseMonitoringAddEditModel);
    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        if (response.message.toLowerCase().includes('created') || response.message.toLowerCase().includes('updated')) {
          response.resources.contacts.forEach((openCloseMonitoringAddEditModel, index: number) => {
            this.contacts.controls[index].get('openAndCloseMonitoringContactId').setValue(openCloseMonitoringAddEditModel.openAndCloseMonitoringContactId);
          });
          this.openCloseMonitoringForm.get('openAndCloseMonitoringId').setValue(response.resources.openAndCloseMonitoringId);
        }
      }
    });
  }

  redirectToTab(type: btnActionTypes | string = btnActionTypes.SUBMIT): void {
    if (this.inputObject.fromComponentType == 'lead' && type == btnActionTypes.BACK_TO_LEAD) {
      this.rxjsService.navigateToProcessQuotationPage();
    }
    else if (this.inputObject.fromComponentType == 'customer' && type == btnActionTypes.BACK_TO_LEAD) {
      this.isDisabled = true;
      this.getArmingAndDisarmingByLeadAndPartitionId();
    }
  }

  onEditBtnClicked() {
    if (this.inputObject.fromComponentType == "customer" && !this.permission.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.activateRoute?.snapshot?.queryParams?.feature_name) {
      this.isDisabled = false;
      if (this.contacts.value[0].openAndCloseMonitoringId) {
        this.focusInAndOutFormArrayFields();
      }
    } else {
      this.rxjsService.setGlobalLoaderProperty(true);
      let filterObj = { userId: this.loggedInUserData?.userId, customerId: this.inputObject?.customerId, siteAddressId: this.inputObject?.addressId, uniqueCallId: this.uniqueCallId ? this.uniqueCallId : null };
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CHECK, null, false,
        prepareRequiredHttpParams(filterObj))
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.isDisabled = false;
            if (this.contacts.value[0].openAndCloseMonitoringId) {
              this.focusInAndOutFormArrayFields();
            }
          } else {
            this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: this.inputObject?.customerId, siteAddressId: this.inputObject?.addressId, tab: CustomerVerificationType.ARMING_AND_DISARMING_TIME, customerTab: 7, monitoringTab: 0, } })
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
  }

  onArrowClick(type: string) {
    let previousAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] - 1));
    let nextAgreementTabObj = this.serviceAgreementStepsParams.agreementTabs.find(s => s['indexNo'] == (this.currentAgreementTabObj['indexNo'] + 1));
    if (this.currentAgreementTabObj) {
      if (type == 'previous') {
        if (this.currentAgreementTabObj.indexNo == 1) {
          this.rxjsService.navigateToProcessQuotationPage();
        }
        else {
          this.router.navigateByUrl(previousAgreementTabObj.navigationUrl);
        }
      }
      else {
        if ((this.openCloseMonitoringForm.value?.openAndCloseMonitoringId &&
          this.currentAgreementTabObj.isMandatory) || !this.currentAgreementTabObj.isMandatory) {
          this.router.navigateByUrl(nextAgreementTabObj.navigationUrl);
        }
      }
    }
  }
}
