import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import {
  CrudService, CustomDirectiveConfig, filterFormControlNamesWhichHasValues, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { SalesModuleApiSuffixModels, SearchDebterModel, selectLeadCreationStepperParamsState$, ServiceAgreementStepsParams } from '@modules/sales';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-search-service-modal',
  templateUrl: './search-service-debter.component.html',
  styleUrls: ['./service-installation-agreement.scss']
})
export class SearchServiceDebterComponent implements OnInit {
  searchDebterModel = new SearchDebterModel();
  @Output() outputData = new EventEmitter<any>();
  searchServiceForm: FormGroup
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  banks = [];
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  formConfigs = formConfigs;
  isSearchBtnDisabled=true;

  constructor(private crudService: CrudService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder, private snackBarService: SnackbarService, @Inject(MAT_DIALOG_DATA) public debtorSearchModel: any,
    private dialog: MatDialog, private rxjsService: RxjsService) {
    this.store.select(selectLeadCreationStepperParamsState$).subscribe(res => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(res);
    });
  }

  ngOnInit(): void {
    this.createSearchForm();
    this.getBanks();
    this.searchServiceForm.valueChanges
    .pipe(
      map((object) => filterFormControlNamesWhichHasValues(object)))
    .subscribe((keyValuesObj) => {
      this.isSearchBtnDisabled = (Object.keys(keyValuesObj).length == 0 || this.searchServiceForm.invalid) ? true : false;
    });
  }

  createSearchForm() {
    let searchDebterModel = new SearchDebterModel();
    this.searchServiceForm = this.formBuilder.group({
    });
    Object.keys(searchDebterModel).forEach((key) => {
      this.searchServiceForm.addControl(key, new FormControl(searchDebterModel[key]));
    });
  }

  getBanks() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.SALES_API_BANKS, undefined, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.banks = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  search() {
    if (this.searchServiceForm.invalid) {
      return;
    }
    let payload = this.searchServiceForm.value;
    payload['debtorTypeId'] = this.debtorSearchModel?.debtorTypeId;
    payload.quotationVersionId = this.serviceAgreementStepsParams.quotationVersionId;
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_SEARCH_DEBTER, undefined, false,
      prepareGetRequestHttpParams(null, null, payload)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          if (response.resources.length == 0) {
            this.snackBarService.openSnackbar("No Data Found", ResponseMessageTypes.SUCCESS);
          } else {
            this.dialog.closeAll();
            this.outputData.emit(response.resources);
          }
        }
      });
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
