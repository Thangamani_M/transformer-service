import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { CrudService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams } from '@app/shared/utils';
@Component({
  selector: 'app-credit-vetting',
  templateUrl: './credit-vetting-modal.component.html',
})

export class CreditVettingModalComponent implements OnInit {
  creditVettingReport: any;
  @Output() outputData1 = new EventEmitter<any>();
  listData: any;
  searchType = "";
  isFormSubmitted = false;

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService, @Inject(MAT_DIALOG_DATA) public data,
    private dialog: MatDialog) {
    this.data['tradingNo'] = '';
  }

  selectedDebtor: any;
  select(e, i, dt) {
    this.selectedDebtor = dt;
    this.listData.forEach((element, index) => {
      if (index == i) {
        element.isChecked = true;
      } else {
        element.isChecked = false;
      }
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
  }

  onSubmit() {
    this.outputData1.emit(this.selectedDebtor);
    this.dialog.closeAll();
  }

  getDetails() {
    this.isFormSubmitted = true;
    if ((!this.searchType && this.searchType !== 'TradingNo') || (!this.data.tradingNo && this.searchType == 'TradingNo')) {
      return;
    }
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.VALIDATE_CUSTOMER, null, false,
      prepareRequiredHttpParams(
        {
          searchType: this.searchType,
          subjectName: this.searchType == 'Name' ? this.data.companyName : '',
          tradingNumber: this.searchType == 'TradingNo' ? this.data.tradingNo : '',
          registrationNo: this.searchType == 'RegNo' ? this.data.companyRegNo : '',
        }
      ))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.listData = response.resources;
          this.listData.forEach(element => {
            element.isChecked = false;
          });
        }
      });
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}