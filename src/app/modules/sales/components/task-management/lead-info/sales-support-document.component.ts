import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { LeadCreationStepperParamsCreateAction, LeadHeaderData, LeadNoteModalComponent, SalesModuleApiSuffixModels, selectLeadCreationStepperParamsState$, selectLeadHeaderDataState$, ServiceAgreementStepsParams } from '@app/modules';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, IApplicationResponse, LoggedInUserModel, ModuleName, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
declare var $: any;
@Component({
  selector: 'sales-support-document',
  templateUrl: './sales-support-document.component.html',
  styleUrls: ['./lead-info.component.scss']
})
export class LeadSupportDocComponent implements OnInit {
  leadId = "";
  selectedIndex = 0;
  leadNote = [];
  supportDocumentObj: any;
  showList = false;
  quotationVersionId = '';
  quotationList = [];
  serviceAgreements = [];
  supportDocLength;
  installationAgreements = [];
  loggedInUserData: LoggedInUserModel;
  leadHeaderData: LeadHeaderData;
  breadCrumb: BreadCrumbModel;

  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private dialog: MatDialog,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.leadId = params.leadId;
      this.selectedIndex = +params.selectedIndex;
    });
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectLeadHeaderDataState$)]
    ).subscribe((response) => {
      this.loggedInUserData = response[0];
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[1]);
      this.leadHeaderData = new LeadHeaderData(response[2]);
      this.breadCrumb = {
        pageTitle: { key: 'Support Documents', value: this.leadHeaderData.leadRefNo },
        items: [{ key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage' }, { key: 'Support Documents' }]
      };
    });
  }

  ngOnInit(): void {
    this.getLeadNote();
  }

  test(e) {
    $('#sticky-sidebar').toggleClass('hide');
  }

  closeNav() {
    document.getElementById("quickaction_sidebar").style.width = "0";
    $('.main-container').css({ width: '100%' });
    $('.quick-action-menu-con').css({ right: '0', visibility: 'hidden' });
  }

  openNav() {
    document.getElementById("quickaction_sidebar").style.width = "250px";
    $('.main-container').css({ width: 'calc(100% - 250px)' });
    $('.quick-action-menu-con').css({ right: '12px', visibility: 'visible' });
  }

  getLeadNote() {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LEAD_NOTES, this.leadId, false, null, 1).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.leadNote = response.resources;
          this.getSupportDocuments();
        }
      });
  }

  onTabClicked(tab): void {
    this.selectedIndex = tab.index;
  }

  teleSaleCall(): void {
    this.router.navigate(['/sales/task-management/leads/lead-services/add-edit'], { queryParams: { leadId: this.leadId, customerId: this.leadHeaderData.customerId } });
  }

  updateLeadInfo(): void {
    this.router.navigate(['/sales/task-management/leads/lead-info/add-edit'], { queryParams: { leadId: this.leadId } });
  }

  navigateToList(e): void {
    this.router.navigate(['/sales/task-management/leads']);
  }

  clickDoc(doc) {
    this.quotationVersionId = doc.quotationVersionId;
    this.showList = !this.showList;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_SUPPORT_DOCUMENT, null, false,
      prepareGetRequestHttpParams(null, null,
        { quoatationVersionId: this.quotationVersionId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.resources && response.isSuccess) {
          this.quotationList = response.resources.quotations;
          this.serviceAgreements = response.resources.contracts && response.resources.contracts.filter(t => t.contractTypeName === 'Service Agreement');
          this.installationAgreements = response.resources.contracts && response.resources.contracts.filter(t => t.contractTypeName === 'Installation Agreement');
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSupportDocuments() {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_SUPPORT_FOLDER_LIST, null, false, prepareRequiredHttpParams({ leadId: this.leadId })).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.supportDocumentObj = response.resources;
          this.supportDocLength = response.resources.quotations.length;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  ngOnDestroy() {
    let serviceAgreementStepsParams = new ServiceAgreementStepsParams({
      createdUserId: this.loggedInUserData.userId,
      fromUrl: this.leadHeaderData.fromUrl
    });
    this.store.dispatch(new LeadCreationStepperParamsCreateAction({ serviceAgreementStepsParams }));
  }

  openLeadDialog(): void {
    const dialogReff = this.dialog.open(LeadNoteModalComponent, { width: '60vw', data: { leadId: this.leadId }, disableClose: true });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getLeadNote();
      }
    });
  }

  redirectToLandingPage() {
    this.router.navigate(['sales/task-management/leads/lead-info/view'], { queryParams: { leadId: this.leadId, setindex: 1 }, skipLocationChange: false })
  }
}
