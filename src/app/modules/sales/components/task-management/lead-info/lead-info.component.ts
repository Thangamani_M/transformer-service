import { Component, ElementRef, EventEmitter, Output, ViewChild } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { MatDialog } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import {
  btnActionTypes, CUSTOMER_COMPONENT, LeadCreationUserDataCreateAction,
  LeadHeaderDataCreateAction,
  NewAddressPopupComponent,
  SalesModuleApiSuffixModels, selectDynamicEagerLoadingLeadCatgoriesState$,
  selectDynamicEagerLoadingSourcesState$,
  selectLeadCreationUserDataState$,
  selectLeadHeaderDataState$,
  selectStaticEagerLoadingCustomerTypesState$,
  selectStaticEagerLoadingSiteTypesState$,
  selectStaticEagerLoadingTitlesState$
} from "@app/modules";
import { AppState } from "@app/reducers";
import {
  addFormControls, BreadCrumbModel,
  countryCodes,
  CrudService,
  CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword,
  destructureAfrigisObjectAddressComponents,
  disableFormControls,
  enableFormControls,
  filterListByKey,
  FindDuplicatePipe,
  formConfigs,
  getFullFormatedAddress,
  HttpCancelService,
  IApplicationResponse, LeadGroupTypes,
  LoggedInUserModel, ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareAutoCompleteListFormatForMultiSelection,
  prepareRequiredHttpParams,
  removeFormControlError,
  removeFormControls,
  ResponseMessageTypes,
  RxjsService,
  setRequiredValidator,
  SnackbarService
} from "@app/shared";
import { LeafLetFullMapViewModalComponent } from "@app/shared/components/leaf-let";
import { TableFilterFormService } from "@app/shared/services/create-form.services";
import { MY_TASK_COMPONENT } from "@modules/my-tasks/shared";
import { loggedInUserData } from "@modules/others";
import {
  AddressModel,
  BasicInfoModel,
  ContactInfoModel,
  LeadCreationUserDataModel,
  LeadHeaderData,
  LeadInfoModel
} from "@modules/sales/models";
import { LeadOutcomeStatusNames, Sales_Leads_Components } from "@modules/sales/shared";
import { Store } from "@ngrx/store";
import { combineLatest, Observable, of } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  switchMap,
  take
} from "rxjs/operators";
declare var $;
@Component({
  selector: "app-lead-info",
  templateUrl: "./lead-info.component.html",
  styleUrls: ["./lead-info.component.scss"],
})
export class LeadInfoComponent {
  formConfigs = formConfigs;
  leadForm: FormGroup;
  rawLeadId: string;
  leadId: string;
  customerId: string;
  addressList = [];
  searchColumns;
  @Output() outputData = new EventEmitter<any>();
  pageSize: number = 10;
  selectedAddressOption = {};
  loggedInUserData: LoggedInUserModel;
  siteType: string;
  pageTitle: string = "Create";
  btnName = "Next";
  fromPage: string;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  lssSchemes = [];
  customerTypes = [];
  leadCategories = [];
  titles = [];
  siteTypes = [];
  latLongList = [];
  selectedColumns: [];
  selectedRows: string[] = [];
  countryCodes = countryCodes;
  customerTypeName: string;
  commercialCustomerType = "Commercial"; // edit here
  residentialCustomerType = "Residential"; // edit here
  addressModalMessage = "";
  existingAddresses = [];
  installedRentedItems = [];
  existingCustomerAddress;
  selectedInstalledRentedItems = [];
  sourceTypes = [];
  isFormSubmitted = false;
  dataList = [];
  dataListCopy = [];
  dataListSecondCopy = [];
  totalRecords;
  loading: boolean;
  existingLeadsObservableResponse: IApplicationResponse;
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  primengTableConfigProperties = {
    tableCaption: "Template",
    tableComponentConfigs: {
      tabsList: [
        {
          caption: "Found Existing Customer Profile",
          dataKey: "index",
          resizableColumns: false,
          enableFieldsSearch: true,
          columns: [
            { field: "isChecked", header: "Select" },
            { field: "customerRefNo", header: "Customer ID" },
            { field: "firstName", header: "First Name" },
            { field: "lastName", header: "Last Name" },
            { field: "email", header: "Email" },
            { field: "mobile1", header: "Mobile No" },
            { field: "customerTypeName", header: "Customer Type" },
            { field: "fullAddress", header: "Address" },
            { field: "isOpenLeads", header: "Open Lead" },
          ],
        },
      ],
    },
  };
  secondPrimengTableConfigProperties = {
    tableCaption: "",
    tableComponentConfigs: {
      tabsList: [
        {
          caption: "Address Confirmation: Reconnection",
          dataKey: "index",
          resizableColumns: false,
          enableFieldsSearch: false,
          columns: [
            { field: "isChecked", header: "Select" },
            { field: "customerRefNo", header: "Customer ID" },
            { field: "firstName", header: "First Name" },
            { field: "lastName", header: "Last Name" },
            { field: "isSiteSpecific", header: "Service Status Active" },
            { field: "isRelocationNoticeRaised", header: "Relocation Raised" },
            { field: "isMovingCancellations", header: "Moving Cancellation (12 M)" },
          ],
        },
      ],
    },
  };
  leadHeaderData: LeadHeaderData;
  fromUrl = "Leads List";
  isLeadGroupUpgrade = false;
  reconnectionMessage = "";
  selectedExistingCustomerProfile: any = { isOpenLeads: false };
  isDataFetchedFromSelectedCustomerProfile = false;
  breadCrumb: BreadCrumbModel;
  areFieldsDisabled = false;
  columnFilterForm: FormGroup;
  columnFilterFormTwo: FormGroup;
  isResetBtnDisabled = true;
  isExistingLeadModalClosed = false;
  isNewProfileBtnDisabled = false;
  leadGroupTypes = LeadGroupTypes;
  selectedReconnCustomerProfile = { isCreateNewlead: false, isProceedAsReconnectionLead: false, customerId: "" };
  shouldShowLocationPinBtn = false;
  latLongObj;
  isCustomAddressSaved = false;
  afrigisAddressInfo;
  fromModule = "";
  addressId = "";
  feature: string;
  featureIndex: string;
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;
  filteredSubComponentsPermissions = [];

  @ViewChild("existing_address_modal_installed_items", { static: false })
  existing_address_modal_installed_items: ElementRef<any>;
  @ViewChild("existing_lead_modal", { static: false })
  existing_lead_modal: ElementRef<any>;
  @ViewChild("existing_address_footprint_modal", { static: false })
  existing_address_footprint_modal: ElementRef<any>;
  @ViewChild("confirm_reconnection_modal", { static: false })
  confirm_reconnection_modal: ElementRef<any>;
  @ViewChild("reconn_new_customer_modal", { static: false })
  reconn_new_customer_modal: ElementRef<any>;
  @ViewChild("input", { static: false }) row;
  @ViewChild("location", { static: false }) location: ElementRef<any>;;

  constructor(
    private crudService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    private httpCancelService: HttpCancelService,
    private snackbarService: SnackbarService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private rxjsService: RxjsService,
    private router: Router,
    private store: Store<AppState>
  ) {
    this.rxjsService.getFromUrl().subscribe((fromUrl: string) => {
      this.fromUrl = fromUrl ? fromUrl : "My Leads List";
    });
    this.combineLatestNgrxStoreDataOne();
    this.combineLatestNgrxStoreDataTwo();
  }

  getLeadDetailsByLeadId(): void {
    if (!this.leadId) return;
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_LEADS_HEADER,
        this.leadId, false, undefined)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.leadHeaderData = new LeadHeaderData(response.resources);
          this.store.dispatch(
            new LeadHeaderDataCreateAction({
              leadHeaderData: this.leadHeaderData,
            })
          );
        }
      });
  }

  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(selectLeadCreationUserDataState$),
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(selectStaticEagerLoadingSiteTypesState$),
      this.store.select(selectStaticEagerLoadingCustomerTypesState$),
    ]).pipe(take(1)).subscribe((response) => {
      this.leadCreationUserDataModel = new LeadCreationUserDataModel(
        response[0]
      );
      this.loggedInUserData = new LoggedInUserModel(response[1]);
      this.titles = response[2];
      this.siteTypes = response[3];
      this.customerTypes = response[4];
    });
  }

  combineLatestNgrxStoreDataTwo() {
    combineLatest([
      this.store.select(selectLeadHeaderDataState$),
      this.activatedRoute.queryParams,
    ]).pipe(take(1)).subscribe((response) => {
      this.leadHeaderData = response[0];
      if (!response[0]) {
        this.getLeadDetailsByLeadId();
      } else {
        this.leadHeaderData = new LeadHeaderData(response[0]);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rawLeadId = response[1]["rawLeadId"];
      this.leadId = response[1]["leadId"];
      this.customerId = response[1]["customerId"];
      this.fromPage = response[1]["pageFrom"];
      this.fromModule = response[1]?.["fromModule"];
      this.addressId = response[1]?.["addressId"];
      this.feature = response[1]?.["feature_name"];
      this.featureIndex = response[1]?.["featureIndex"];
      if (this.rawLeadId) {
        this.breadCrumb = {
          pageTitle: { key: "Create My Lead" },
          items: [
            { key: "My Tasks" },
            { key: "My Sales" },
            {
              key: "My Leads",
              routeUrl: `/my-tasks/my-sales?leadId=${this.leadHeaderData?.leadId}&tab=1`,
            },
            { key: "Create" },
          ],
        };
      } else if (this.fromUrl == "Leads List") {
        this.breadCrumb = {
          pageTitle: {
            key: this.leadHeaderData.leadId
              ? "Update My Lead"
              : "Create My Lead",
          },
          items: this.leadHeaderData.leadRefNo ? [
            { key: this.leadHeaderData.leadRefNo },
            {
              key: "Leads",
              routeUrl: "/sales/task-management/leads",
            },
            { key: "Update" },
          ] : [
            {
              key: "Leads",
              routeUrl: "/sales/task-management/leads",
            },
            { key: "Create" },
          ],
        };
      } else if (this.fromUrl == "My Leads List" && this.leadId) {
        this.breadCrumb = {
          pageTitle: {
            key: this.leadHeaderData.leadId
              ? "Update My Lead"
              : "Create My Lead",
            value: this.leadId ? this.leadHeaderData.leadRefNo : "",
          },
          items: [
            { key: "My Tasks" },
            { key: "My Sales" },
            {
              key: "My Leads",
              routeUrl: `/my-tasks/my-sales?leadId=${this.leadHeaderData.leadId}&tab=1`,
            },
            {
              key: this.leadId ? this.leadHeaderData.leadRefNo : "",
              routeUrl: "landingPage",
            },
            { key: this.leadId ? "Update" : "Create" },
          ],
        };
      }
      else if (this.fromUrl == "My Leads List" && !this.leadId) {
        this.breadCrumb = {
          pageTitle: { key: "Create My Lead" },
          items: [
            { key: "My Tasks" },
            { key: "My Sales" },
            {
              key: "My Leads",
              routeUrl: `/my-tasks/my-sales?leadId=${this.leadHeaderData.leadId}&tab=1`,
            },
            { key: "Create" },
          ],
        }
      }
      this.getRawLeadOrLeadOrCustomerById("ngOnInit");
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxDynamicData();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0]
        .columns
    );
    this.columnFilterFormTwo = this.tableFilterFormService.createFormGroup(
      this.secondPrimengTableConfigProperties.tableComponentConfigs.tabsList[0]
        .columns
    );
    this.columnFilterRequest();
    this.columnFilterRequest('two');
    this.rxjsService.setGlobalLoaderProperty(false);
    this.createLeadInfoForm();
    this.onBootstrapModalEventChanges();
    this.onFormControlChanges();
    if (this.leadId) {
      this.pageTitle = "Update";
    }
  }


  combineLatestNgrxDynamicData() {
    combineLatest([
      this.store.select(selectDynamicEagerLoadingLeadCatgoriesState$),
      this.store.select(selectDynamicEagerLoadingSourcesState$),
      this.store.select(currentComponentPageBasedPermissionsSelector$)
    ]).subscribe((response) => {
      this.leadCategories = response[0];
      this.sourceTypes = response[1];
      let componentPermissions = [];
      if (this.fromUrl == "Leads List") {
        componentPermissions = response[2][Sales_Leads_Components.LEADS];
      }
      else if (this.fromUrl == "My Leads List") {
        componentPermissions = response[2][MY_TASK_COMPONENT.MY_SALES];
        componentPermissions = componentPermissions?.filter(cP => cP.menuName === 'My Leads').map((fS => fS['subMenu']))[0];
      }
      else if (this.fromUrl == "Customer Management") {
        let customer = response[0][CUSTOMER_COMPONENT.CUSTOMER]?.find(el => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD);
        let sales = customer?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.SALES);
        componentPermissions = sales?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.LEADS)['subMenu']
      }
      if (componentPermissions) {
        this.filteredSubComponentsPermissions = componentPermissions?.filter(cP => cP.menuName === Sales_Leads_Components.STAGE_ONE).map((fS => fS['subMenu']))[0];
      }
    });
  }

  columnFilterRequest(type = 'one') {
    let columnFilterFormGroup = type == 'one' ? this.columnFilterForm : this.columnFilterFormTwo;
    columnFilterFormGroup.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap((obj) => {
          Object.keys(obj).forEach((key) => {
            if (obj[key] === "") {
              delete obj[key];
            }
          });
          this.searchColumns = Object.entries(obj).reduce(
            (a, [k, v]) => (v == null ? a : ((a[k] = v), a)),
            {}
          );
          let row = {};
          row["searchColumns"] = this.searchColumns;
          return of(this.searchColumns);
        })
      )
      .subscribe((searchedColumnKeyValues) => {
        this.rxjsService.setPopupLoaderProperty(true);
        this.dataList = this.dataListSecondCopy;
        this.dataListCopy = this.dataListSecondCopy;
        if (Object.keys(searchedColumnKeyValues).length == 0) {
          this.isResetBtnDisabled = true;
          this.dataList = this.dataListSecondCopy;
          this.dataListCopy = this.dataListSecondCopy;
          this.totalRecords = this.dataListSecondCopy.length;
        } else {
          this.isResetBtnDisabled = false;
          Object.keys(searchedColumnKeyValues).forEach((key, ix) => {
            if (Object.keys(searchedColumnKeyValues).length == 1) {
              this.dataListCopy = filterListByKey(this.dataListCopy,key,searchedColumnKeyValues[key]);
            } else {
              this.dataListCopy = filterListByKey(this.dataListCopy,key,searchedColumnKeyValues[key]);
            }
            if (Object.keys(searchedColumnKeyValues).length == ix + 1) {
              this.dataList = this.dataListCopy;
              this.totalRecords = this.dataListCopy.length;
            }
          });
        }
        setTimeout(() => {
          this.rxjsService.setPopupLoaderProperty(false);
        }, 500);
      });
  }

  onResetFilter(type) {
    if (type == 'modalClose') {
      return;
    }
    this.columnFilterForm.patchValue({
      customerRefNo: "",
      customerTypeName: "",
      email: "",
      firstName: "",
      fullAddress: "",
      lastName: "",
      mobile1: "",
    });
    this.selectedExistingCustomerProfile = { isOpenLeads: false };
    this.getExistingLeads(type);
  }

  loadPaginationLazy(event) {
    this.rxjsService.setPopupLoaderProperty(true);
    if (event.sortField && event.sortOrder) {
      this.dataList.sort((a, b) => {
        let fieldA = a[event.sortField].toUpperCase();
        var fieldB = b[event.sortField].toUpperCase();
        if (event.sortOrder == 1) {
          if (fieldA < fieldB) {
            return -1;
          }
          if (fieldA > fieldB) {
            return 1;
          }
        } else {
          if (fieldB < fieldA) {
            return -1;
          }
          if (fieldB > fieldA) {
            return 1;
          }
        }
      });
    }
    setTimeout(() => {
      this.rxjsService.setPopupLoaderProperty(false);
    }, 500);
  }

  onCRUDRequested(type: CrudType | string, field?: string, row?: object): void {
    switch (type) {
      case CrudType.EDIT:
        $(this.reconn_new_customer_modal.nativeElement).modal("hide");
        if (field == 'customerRefNo') {
          let queryParams = { addressId: row['addressId'] };
          if (this.feature && this.featureIndex) {
            queryParams['feature_name'] = this.feature;
            queryParams['featureIndex'] = this.featureIndex;
          }
          this.router.navigate(
            ["customer/manage-customers/view/" +
              row["customerId"]], { queryParams: queryParams }
          );
        }
        else if (field == 'leadId') {
          window.open(
            `/sales/task-management/lead-creation?leadId=${row["leadId"]}`,
            "_blank"
          );
        }
        break;
      case CrudType.GET:
        break;
    }
  }

  onBootstrapModalEventChanges(): void {
    $("#existing_lead_modal").on("shown.bs.modal", (e) => {
      this.searchColumns = "";
      this.contactInfoFormGroupControls.get("email").markAsPristine();
      this.contactInfoFormGroupControls.get("mobile1").markAsPristine();
      this.contactInfoFormGroupControls.updateValueAndValidity();
      this.basicInfoFormGroupControls.get("firstName").markAsPristine();
      this.basicInfoFormGroupControls.get("lastName").markAsPristine();
      this.basicInfoFormGroupControls.updateValueAndValidity();
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.isExistingLeadModalClosed = false;
      this.dataList.map((obj) => (obj["isChecked"] = false));
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#existing_lead_modal").on("hidden.bs.modal", (e) => {
      this.isExistingLeadModalClosed = true;
      this.addressInfoFormGroupControls.get("formatedAddress").enable({ emitEvent: false, onlySelf: true });
      setTimeout(() => {
        this.isExistingLeadModalClosed = false;
      }, 100);
      this.rxjsService.setDialogOpenProperty(false);
      this.onResetFilter('modalClose');
    });
    $("#existing_address_footprint_modal").on("shown.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#existing_address_footprint_modal").on("hidden.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(false);
    });
    $("#confirm_reconnection_modal").on("shown.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#confirm_reconnection_modal").on("hidden.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(false);
    });
    $("#reconn_new_customer_modal").on("shown.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#reconn_new_customer_modal").on("hidden.bs.modal", (e) => {
      this.searchColumns = "";
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  createLeadInfoForm(): void {
    let leadInfoModel = new LeadInfoModel();
    this.leadForm = this.formBuilder.group({});
    Object.keys(leadInfoModel).forEach((key) => {
      switch (key) {
        case "basicInfo":
          this.leadForm.addControl(key, this.createBasicInfoForm());
          break;
        case "contactInfo":
          this.leadForm.addControl(key, this.createContactInfoForm());
          break;
        case "addressInfo":
          this.leadForm.addControl(key, this.createAndGetAddressForm());
          break;
        default:
          this.leadForm.addControl(key, new FormControl(leadInfoModel[key]));
          break;
      }
    });
    this.leadForm = setRequiredValidator(this.leadForm, [
      "leadCategoryId",
      "siteTypeId",
      "sourceId"
    ]);
    this.leadForm.controls["basicInfo"] = setRequiredValidator(
      this.leadForm.controls["basicInfo"] as FormGroup,
      ["customerTypeId", "titleId", "firstName", "lastName"]
    );
    this.leadForm.controls["contactInfo"] = setRequiredValidator(
      this.leadForm.controls["contactInfo"] as FormGroup,
      ["email", "mobile1"]
    );
    this.leadForm.controls["addressInfo"] = setRequiredValidator(
      this.leadForm.controls["addressInfo"] as FormGroup,
      ["formatedAddress", 'latLong']
    );
  }

  createBasicInfoForm(basicInfo?: BasicInfoModel): FormGroup {
    let basicInfoModel = new BasicInfoModel(basicInfo);
    let formControls = {};
    Object.keys(basicInfoModel).forEach((key) => {
      formControls[key] = new FormControl(basicInfoModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createContactInfoForm(contactInfo?: ContactInfoModel): FormGroup {
    let contactInfoModel = new ContactInfoModel(contactInfo);
    let formControls = {};
    Object.keys(contactInfoModel).forEach((key) => {
      formControls[key] = new FormControl(contactInfoModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createAndGetAddressForm(address?: AddressModel): FormGroup {
    let addressModel = new AddressModel(address);
    let formControls = {};
    Object.keys(addressModel).forEach((key) => {
      if (
        key === "streetName" ||
        key === "suburbName" ||
        key === "cityName" ||
        key === "provinceName" ||
        key === "postalCode" ||
        key === "estateStreetNo" ||
        key === "estateStreetName" ||
        key === "estateName" ||
        key === "addressConfidentLevel"
      ) {
        formControls[key] = new FormControl({
          value: addressModel[key],
          disabled: true,
        });
      } else {
        formControls[key] = new FormControl(addressModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  onFormControlChanges(): void {
    this.leadForm.valueChanges.subscribe(() => {
      this.isFormSubmitted = false;
    });
    this.addressInfoFormGroupControls.get('isAfrigisSearch').valueChanges.subscribe((isAfrigisSearch: boolean) => {
      this.addressInfoFormGroupControls.get('formatedAddress').setValue("");
      if (isAfrigisSearch) {
        this.addressInfoFormGroupControls.get('addressId').setValue(this.addressInfoFormGroupControls.get('addressId').value ?
          this.addressInfoFormGroupControls.get('addressId').value : "");
        this.addressInfoFormGroupControls.get('isAddressExtra').setValue(false);
        this.clearAddressFormGroupValues('afrigis search checkbox');
      }
      this.addressList = [];
    });
    this.addressInfoFormGroupControls.get('formatedAddress').valueChanges.subscribe((formatedAddress) => {
      this.leadForm.get('isReconnectionAddressConfirmation').setValue(false);
    });
    this.addressInfoFormGroupControls.get('buildingNo').valueChanges.subscribe((buildingNo) => {
      this.leadForm.get('isReconnectionAddressConfirmation').setValue(false);
    });
    this.addressInfoFormGroupControls.get('buildingName').valueChanges.subscribe((buildingName) => {
      this.leadForm.get('isReconnectionAddressConfirmation').setValue(false);
    });
    this.addressInfoFormGroupControls.get('streetNo').valueChanges.subscribe((streetNo) => {
      this.leadForm.get('isReconnectionAddressConfirmation').setValue(false);
    });
    this.addressInfoFormGroupControls.get('estateStreetNo').valueChanges.subscribe((estateStreetNo) => {
      this.leadForm.get('isReconnectionAddressConfirmation').setValue(false);
    });
    this.addressInfoFormGroupControls.get('estateStreetName').valueChanges.subscribe((estateStreetName) => {
      this.leadForm.get('isReconnectionAddressConfirmation').setValue(false);
    });
    this.addressInfoFormGroupControls.get('estateName').valueChanges.subscribe((estateName) => {
      this.leadForm.get('isReconnectionAddressConfirmation').setValue(false);
    });
    this.onFormatedAddressFormControlValueChanges();
    this.onCustomerTypeChanges();
    this.contactInfoFormGroupControls
      .get("mobile2CountryCode")
      .valueChanges.subscribe((mobile2CountryCode: string) => {
        this.setPhoneNumberLengthByCountryCode(mobile2CountryCode);
        setTimeout(() => {
          this.row.nativeElement.focus();
          this.row.nativeElement.blur();
        });
      });
    this.contactInfoFormGroupControls
      .get("mobile2")
      .valueChanges.subscribe((mobileNumber2: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.contactInfoFormGroupControls.get("mobile2CountryCode").value
        );
      });
    this.addressInfoFormGroupControls
      .get("isAddressComplex")
      .valueChanges.subscribe((isAddressComplex: boolean) => {
        if (!isAddressComplex) {
          this.addressInfoFormGroupControls.get("buildingNo").setValue(null);
          this.addressInfoFormGroupControls.get("buildingName").setValue(null);
        }
      });
    this.addressInfoFormGroupControls
      .get("isAddressExtra")
      .valueChanges.subscribe((isAddressExtra: boolean) => {
        if (isAddressExtra) {
          this.leadForm.controls["addressInfo"] = enableFormControls(
            this.leadForm.controls["addressInfo"] as FormGroup,
            ["estateStreetNo", "estateStreetName", "estateName"]
          );
        } else {
          this.leadForm.controls["addressInfo"] = disableFormControls(
            this.leadForm.controls["addressInfo"] as FormGroup,
            ["estateStreetNo", "estateStreetName", "estateName"]
          );
          this.addressInfoFormGroupControls
            .get("estateStreetNo")
            .setValue(null);
          this.addressInfoFormGroupControls
            .get("estateStreetName")
            .setValue(null);
          this.addressInfoFormGroupControls.get("estateName").setValue(null);
        }
      });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.contactInfoFormGroupControls
          .get("mobile2")
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
        break;
      default:
        this.contactInfoFormGroupControls
          .get("mobile2")
          .setValidators([
            Validators.minLength(formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),
          ]);
        break;
    }
  }

  onCustomerTypeChanges(): void {
    this.basicInfoFormGroupControls
      .get("customerTypeId")
      .valueChanges.subscribe((customerTypeId: string) => {
        if (!customerTypeId) return;
        this.customerTypeName = this.customerTypes.find(
          (s) => s["id"] == customerTypeId
        ).displayName;
        if (this.customerTypeName === this.commercialCustomerType) {
          this.leadForm.controls["basicInfo"] = addFormControls(
            this.leadForm.controls["basicInfo"] as FormGroup,
            ["companyRegNo", "companyName"]
          );
          this.leadForm.controls["basicInfo"] = setRequiredValidator(
            this.leadForm.controls["basicInfo"] as FormGroup,
            ["companyName"]
          );
          this.leadForm.controls["basicInfo"] = removeFormControls(
            this.leadForm.controls["basicInfo"] as FormGroup,
            ["said"]
          );
        } else {
          this.leadForm.controls["basicInfo"] = addFormControls(
            this.leadForm.controls["basicInfo"] as FormGroup,
            ["said"]
          );
          this.leadForm.controls["basicInfo"] = removeFormControls(
            this.leadForm.controls["basicInfo"] as FormGroup,
            ["companyRegNo", "companyName"]
          );
        }
      });
  }

  get addressInfoFormGroupControls(): FormGroup {
    if (!this.leadForm) return;
    return this.leadForm.get("addressInfo") as FormGroup;
  }

  get basicInfoFormGroupControls(): FormGroup {
    if (!this.leadForm) return;
    return this.leadForm.get("basicInfo") as FormGroup;
  }

  get contactInfoFormGroupControls(): FormGroup {
    if (!this.leadForm) return;
    return this.leadForm.get("contactInfo") as FormGroup;
  }

  onFormatedAddressFormControlValueChanges(): void {
    this.addressInfoFormGroupControls
      .get("formatedAddress")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((searchtext) => {
          if (!searchtext || this.isCustomAddressSaved == true) {
            this.getLoopableObjectRequestObservable = of();
          }
          else {
            this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS, null, true,
              prepareRequiredHttpParams({
                searchtext,
                isAfrigisSearch: this.addressInfoFormGroupControls.get("isAfrigisSearch").value
              }));
          }
        });
  }

  openFindPinMap() {
    this.openFullMapViewWithConfig();
  }

  openFullMapViewWithConfig() {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(LeafLetFullMapViewModalComponent, {
      width: "100vw",
      maxHeight: "100vh",
      disableClose: true,
      data: {
        fromUrl: 'Two Table Architecture Custom Address',
        boundaryRequestId: null,
        boundaryRequestRefNo: null,
        isDisabled: false,
        boundaryRequestDetails: {},
        boundaries: [],
        latLong: this.latLongObj,
        shouldShowLegend: false
      },
    });
    dialogReff.afterClosed().subscribe((result) => {
      if (result?.hasOwnProperty('latLong')) {
        this.latLongObj = result.latLong;
        this.addressInfoFormGroupControls
          .get("latLong").setValue(`${result.latLong.lat.toFixed(4)}, ${result.latLong.lng.toFixed(4)}`)
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  clearAddressFormGroupValues(fromType = 'template'): void {
    this.addressInfoFormGroupControls.patchValue({
      latitude: null,
      longitude: null,
      latLong: null,
      suburbName: null,
      cityName: null,
      provinceName: null,
      postalCode: null,
      streetName: null,
      streetNo: null,
      buildingNo: null,
      buildingName: null,
      estateName: null,
      estateStreetName: null,
      estateStreetNo: null,
    });
    if (fromType == 'template') {
      this.focusAndBlurLocationField();
    }
  }

  getExistingLeads(actionType: string = 'get'): void {
    const emailFormControl = this.contactInfoFormGroupControls.get("email");
    const mobile1FormControl = this.contactInfoFormGroupControls.get("mobile1");
    const firstNameFormControl = this.basicInfoFormGroupControls.get("firstName");
    const lastNameFormControl = this.basicInfoFormGroupControls.get("lastName");
    if (actionType == 'get') {
      if (
        emailFormControl.invalid ||
        firstNameFormControl.invalid ||
        lastNameFormControl.invalid ||
        mobile1FormControl.invalid ||
        (!emailFormControl.dirty && !mobile1FormControl.dirty && !firstNameFormControl.dirty && !lastNameFormControl.dirty) ||
        this.isExistingLeadModalClosed
      ) {
        return;
      }
    }
    this.addressInfoFormGroupControls.get("formatedAddress").disable({ emitEvent: false, onlySelf: true });
    let leadId = this.leadId ? this.leadId : "";
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_DUPLICATE_LEADS,
        null,
        null,
        prepareRequiredHttpParams({
          firstName: firstNameFormControl.value,
          lastName: lastNameFormControl.value,
          email: emailFormControl.value,
          mobile1: mobile1FormControl.value.toString().replace(/\s/g, ""),
          leadId
        })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          response.resources.forEach((resp, index) => {
            resp.index = index;
            resp.isChecked = false;
          });
          this.dataList = response.resources;
          this.dataListCopy = response.resources;
          this.dataListSecondCopy = response.resources;
          this.totalRecords = response.resources.length;
          this.isNewProfileBtnDisabled = this.totalRecords == 0 ? true : false;
          if (response.resources.length > 0) {
            $(this.existing_lead_modal.nativeElement).modal("show");
          }
          else {
            this.addressInfoFormGroupControls.get("formatedAddress").enable({ emitEvent: false, onlySelf: true });
          }
        }
        else {
          this.addressInfoFormGroupControls.get("formatedAddress").enable({ emitEvent: false, onlySelf: true });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onChangeCustomerSelection(rowData) {
    this.isResetBtnDisabled = false;
    this.selectedExistingCustomerProfile = rowData;
    if (
      this.contactInfoFormGroupControls.get("email").value ==
      rowData["email"] &&
      this.contactInfoFormGroupControls
        .get("mobile1")
        .value.replace(/\s/g, "") == rowData["mobile1"] &&
      this.basicInfoFormGroupControls.get("firstName").value ==
      rowData["firstName"] &&
      this.basicInfoFormGroupControls.get("lastName").value ==
      rowData["lastName"]
    ) {
      this.isNewProfileBtnDisabled = true;
    } else {
      this.isNewProfileBtnDisabled = false;
    }
    this.dataList.forEach((dataObj) => {
      dataObj["isChecked"] =
        rowData["index"] == dataObj["index"] ? true : false;
    });
  }

  onExistingCustomerPopupBtnClick(type) {
    this.leadForm.get("leadValidationType").setValue(type);
    let selectedExistingCustomerProfileCopy = JSON.parse(
      JSON.stringify(this.selectedExistingCustomerProfile)
    );
    switch (type) {
      case "Use Existing Customer":
        delete selectedExistingCustomerProfileCopy.isOpenLeads;
        this.contactInfoFormGroupControls.patchValue(
          new ContactInfoModel(selectedExistingCustomerProfileCopy)
        );
        this.basicInfoFormGroupControls.patchValue(
          this.selectedExistingCustomerProfile
        );
        this.leadForm
          .get("customerId")
          .setValue(this.selectedExistingCustomerProfile["customerId"]);
        this.isDataFetchedFromSelectedCustomerProfile = true;
        this.rxjsService.setFormChangeDetectionProperty(true);
        break;
      case "Use Existing Lead":
        this.router.navigate(["/sales/task-management/leads/lead-info/view"], {
          queryParams: {
            leadId: this.selectedExistingCustomerProfile["leadId"],
          },
        });
        break;
      case "Use Existing Customer Detail":
        let queryParams = { addressId: this.selectedExistingCustomerProfile['addressId'] };
        if (this.feature && this.featureIndex) {
          queryParams['feature_name'] = this.feature;
          queryParams['featureIndex'] = this.featureIndex;
        }
        this.router.navigate(["customer/manage-customers/view/" + this.selectedExistingCustomerProfile["customerId"]], { queryParams: queryParams }
        );
        break;
    }
  }

  onSelectExistingCustomerAddress(existingCustomerAddressObj): void {
    this.existingCustomerAddress = existingCustomerAddressObj;
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.ADDRESS_ITEMS,
        this.existingCustomerAddress["addressId"]
      )
      .subscribe((response: IApplicationResponse) => {
        if (
          response.isSuccess &&
          response.statusCode === 200 &&
          response.resources
        ) {
          this.installedRentedItems = response.resources;
          this.installedRentedItems.forEach((installedRentedItem) => {
            installedRentedItem.isChecked = false;
          });
        }
      });
  }

  onChangeInstalledItems(installedItemObj): void {
    installedItemObj.isChecked = !installedItemObj.isChecked;
    if (installedItemObj.isChecked) {
      this.selectedInstalledRentedItems.push(installedItemObj);
    } else {
      this.selectedInstalledRentedItems.splice(
        this.selectedInstalledRentedItems.indexOf(installedItemObj.itemId),
        1
      );
    }
  }

  onLinkExistingAddress(): void {
    $(this.existing_address_footprint_modal.nativeElement).modal("hide");
    if (this.installedRentedItems.length > 0) {
      $(this.existing_address_modal_installed_items.nativeElement).modal(
        "show"
      );
    }
  }

  onSelectedInstalledItems(): void {
    $(this.existing_address_modal_installed_items.nativeElement).modal("hide");
  }

  getAddressFullDetails(seoid: string): void {
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_ADDRESS_DETAILS,
        undefined,
        false,
        prepareRequiredHttpParams({ seoid })
      )
      .subscribe((response: IApplicationResponse) => {
        if (
          response.isSuccess &&
          response.statusCode === 200 &&
          response.resources
        ) {
          this.leadForm.controls["addressInfo"]
            .get("jsonObject")
            .setValue(JSON.stringify(response.resources));
          this.addressInfoFormGroupControls
            .get("addressConfidentLevel")
            .setValue(response.resources.addressConfidenceLevel);
          response.resources.addressDetails.forEach((addressObj) => {
            this.addressInfoFormGroupControls
              .get("addressConfidentLevelId")
              .setValue(addressObj.confidence);
            this.patchAddressFormGroupValues(
              addressObj,
              addressObj["address_components"]
            );
          });
          this.addressInfoFormGroupControls.updateValueAndValidity();
          this.afrigisAddressInfo = {
            buildingName: this.addressInfoFormGroupControls.value.buildingName,
            buildingNo: this.addressInfoFormGroupControls.value.buildingNo,
            streetNo: this.addressInfoFormGroupControls.value.streetNo,
          };
        }
        this.focusAndBlurLocationField();
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  patchAddressFormGroupValues(
    addressObj,
    addressComponents: Object[]
  ) {
    addressObj.geometry.location.lat = addressObj.geometry.location.lat
      ? addressObj.geometry.location.lat
      : "";
    addressObj.geometry.location.lng = addressObj.geometry.location.lng
      ? addressObj.geometry.location.lng
      : "";
    if (addressObj.geometry.location.lat && addressObj.geometry.location.lng) {
      addressObj.geometry.location.latLong = `${addressObj.geometry.location.lat}, ${addressObj.geometry.location.lng}`;
    } else {
      addressObj.geometry.location.latLong = "";
    }
    let {
      suburbName,
      cityName,
      provinceName,
      postalCode,
      streetName,
      streetNo,
      buildingNo,
      buildingName,
      estateName,
      estateStreetName,
      estateStreetNo,
    } = destructureAfrigisObjectAddressComponents(addressComponents);
    this.addressInfoFormGroupControls.patchValue(
      {
        latitude: addressObj.geometry.location.lat,
        longitude: addressObj.geometry.location.lng,
        latLong: addressObj.geometry.location.latLong,
        buildingName,
        buildingNo,
        estateName,
        estateStreetName,
        estateStreetNo,
        suburbName,
        cityName,
        provinceName,
        postalCode,
        streetName,
        streetNo,
      },
      { emitEvent: false, onlySelf: true }
    );
  }

  onSelectedItemOption(selectedObject): void {
    if (!selectedObject) return;
    this.selectedAddressOption = selectedObject;
    if (this.addressInfoFormGroupControls.get('isAfrigisSearch').value) {
      this.leadForm.controls["addressInfo"]
        .get("seoid")
        .setValue(selectedObject["seoid"]);
      this.getAddressFullDetails(selectedObject["seoid"]);
      this.addressInfoFormGroupControls.get('addressId').setValue(this.addressInfoFormGroupControls.get('addressId').value ? this.addressInfoFormGroupControls.get('addressId').value : null);
    } else {
      selectedObject["longitude"] = selectedObject["longitude"] ? selectedObject["longitude"] : "";
      selectedObject["latitude"] = selectedObject["latitude"] ? selectedObject["latitude"] : "";
      if (selectedObject["latitude"] && selectedObject["longitude"]) {
        selectedObject["latLong"] = `${selectedObject["latitude"]}, ${selectedObject["longitude"]}`;
      } else {
        selectedObject["latLong"] = "";
      }
      this.addressInfoFormGroupControls.patchValue(
        {
          addressId: selectedObject['addressId'],
          seoid: "",
          jsonObject: "",
          latitude: selectedObject["latitude"],
          longitude: selectedObject["longitude"],
          latLong: selectedObject["latLong"],
          suburbName: selectedObject["suburbName"],
          cityName: selectedObject["cityName"],
          provinceName: selectedObject["provinceName"],
          postalCode: selectedObject["postalCode"],
          streetName: selectedObject["streetName"],
          streetNo: selectedObject["streetNo"],
          estateName: selectedObject["estateName"],
          estateStreetName: selectedObject["estateStreetName"],
          estateStreetNo: selectedObject["estateStreetNo"],
          buildingName: selectedObject["buildingName"],
          buildingNo: selectedObject["buildingNo"],
          addressConfidentLevel: selectedObject["addressConfidentLevelName"],
          addressConfidentLevelId: selectedObject["addressConfidentLevelId"]
        }, { emitEvent: false, onlySelf: true });
      this.focusAndBlurLocationField();
    }
  }

  focusAndBlurLocationField() {
    this.location.nativeElement.focus();
    this.location.nativeElement.blur();
  }

  getRawLeadOrLeadOrCustomerById(type: string): void {
    if (!this.rawLeadId && !this.leadId && !this.customerId) return;
    let salesModuleApiSuffixModels = this.leadId
      ? SalesModuleApiSuffixModels.SALES_API_LEADS
      : this.rawLeadId
        ? SalesModuleApiSuffixModels.SALES_API_RAW_LEADS_CONVERT_LEAD
        : SalesModuleApiSuffixModels.GET_LEAD_BY_CUSTOMER_ID;
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        salesModuleApiSuffixModels,
        this.leadId
          ? this.leadId
          : this.rawLeadId
            ? this.rawLeadId
            : this.customerId,
        false,
        undefined,
        1
      )
      .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          if (!this.rawLeadId) {
            // disable fields once created and when come for an update
            this.leadForm.controls["basicInfo"] = disableFormControls(this.leadForm.controls["basicInfo"] as FormGroup, ['titleId', 'firstName', 'lastName']);
            this.leadForm.controls["contactInfo"] = disableFormControls(this.leadForm.controls["contactInfo"] as FormGroup, ['mobile1', 'email']);
          }
          if (resp.resources?.basicInfo?.customerTypeId) {
            this.customerTypeName = this.customerTypes.find(
              (s) => s["id"] == resp.resources.basicInfo.customerTypeId
            ).displayName;
          }
          if (this.customerTypeName == this.residentialCustomerType && resp.resources?.basicInfo?.said) {
            this.leadForm.controls["basicInfo"].get('said').disable();
          }
          else if (this.customerTypeName == this.commercialCustomerType) {
            if (resp.resources?.basicInfo?.companyRegNo) {
              this.leadForm.controls["basicInfo"].get('companyRegNo').disable();
            }
            if (resp.resources?.basicInfo?.companyName) {
              this.leadForm.controls["basicInfo"].get('companyName').disable();
            }
          }
          // end of disable fields once created and when come for an update
          if (this.customerId) {
            this.isDataFetchedFromSelectedCustomerProfile = true;
          }
          let leadInfoModel = new LeadInfoModel(resp.resources);
          this.isLeadGroupUpgrade =
            +leadInfoModel.leadGroupId == this.leadGroupTypes.UPGRADE ? true : false;
          if (!this.customerId) {
            if (
              leadInfoModel.addressInfo.latitude &&
              leadInfoModel.addressInfo.longitude
            ) {
              leadInfoModel.addressInfo.latLong =
                leadInfoModel.addressInfo.latitude +
                "," +
                leadInfoModel.addressInfo.longitude;
            }
          } else {
            leadInfoModel.addressInfo.latitude = leadInfoModel.addressInfo
              .latitude
              ? leadInfoModel.addressInfo.latitude
              : " ";
            leadInfoModel.addressInfo.longitude = leadInfoModel.addressInfo
              .longitude
              ? leadInfoModel.addressInfo.longitude
              : " ";

            leadInfoModel.addressInfo = new AddressModel(
              leadInfoModel.addressInfo
            );
          }
          this.leadForm.patchValue(leadInfoModel, {
            emitEvent: false,
            onlySelf: true,
          });
          this.basicInfoFormGroupControls
            .get("customerTypeId")
            .setValue(leadInfoModel.basicInfo.customerTypeId);
          if (this.rawLeadId && resp.resources.basicInfo.titleId == 0) {
            this.basicInfoFormGroupControls.get("titleId").setValue(null);
          }
          let contactInfo = new ContactInfoModel(leadInfoModel.contactInfo);
          if (this.rawLeadId) {
            if (resp.resources.addressInfo.buildingNo) {
              resp.resources.addressInfo.isAddressComplex = true;
            }
            if (resp.resources.addressInfo.estateStreetNo) {
              resp.resources.addressInfo.isAddressExtra = true;
            }
          }
          this.contactInfoFormGroupControls.patchValue(contactInfo);
          if (!this.customerId) {
            this.addressInfoFormGroupControls.patchValue(
              resp.resources.addressInfo,
              { emitEvent: false, onlySelf: true }
            );
            this.addressInfoFormGroupControls
              .get("isAddressComplex")
              .setValue(resp.resources.addressInfo.isAddressComplex, { emitEvent: false, onlySelf: true });
            this.addressInfoFormGroupControls
              .get("isAddressExtra")
              .setValue(resp.resources.addressInfo.isAddressExtra);
          }
          this.leadCreationUserDataModel = new LeadCreationUserDataModel(
            resp.resources
          );
          if (!this.customerId) {
            this.selectedAddressOption["description"] =
              leadInfoModel.addressInfo.formatedAddress;
          }
          this.leadCreationUserDataModel.suburbId = this.leadForm.value.suburbId;
          this.leadCreationUserDataModel.districtId = this.leadForm.value.districtId;
          this.leadCreationUserDataModel.leadId = leadInfoModel.leadId
            ? leadInfoModel.leadId
            : resp.resources;
          this.leadCreationUserDataModel.leadStatusName = this
            .leadCreationUserDataModel.leadStatusName
            ? this.leadCreationUserDataModel.leadStatusName
            : this.leadHeaderData
              ? this.leadHeaderData.leadStatusName
              : LeadOutcomeStatusNames.ACCEPT;
          this.areFieldsDisabled =
            (this.leadCreationUserDataModel.leadStatusName ==
              this.leadOutcomeStatusNames.NO_SALE ||
              this.leadCreationUserDataModel?.leadStatusName ==
              this.leadOutcomeStatusNames.SALE_COMPLETED ||
              this.leadCreationUserDataModel?.leadStatusName ==
              this.leadOutcomeStatusNames.QUOTATION_ACCEPTED || this.leadCreationUserDataModel?.leadStatusName ==
              this.leadOutcomeStatusNames.QUOTATION_PENDING || this.leadCreationUserDataModel?.leadStatusName ==
              this.leadOutcomeStatusNames.QUOTATION_DECLINED || this.leadCreationUserDataModel?.leadStatusName ==
              this.leadOutcomeStatusNames.QUOTATION_EXPIRED || this.leadCreationUserDataModel?.leadStatusName ==
              this.leadOutcomeStatusNames.QUOTATION_ISSUED || this.leadCreationUserDataModel?.leadStatusName ==
              this.leadOutcomeStatusNames.SALE_ORDER_CREATED) &&
              this.leadId
              ? true
              : false;
              let selectedleadCategoryId = [];
              if(leadInfoModel.leadCategoryId){
                leadInfoModel?.leadCategoryId.forEach(result=>{
                  selectedleadCategoryId.push(this.leadCategories.find(l => l['id'] == result));
                })
                this.leadForm.get("leadCategoryId").setValue(selectedleadCategoryId);
              }
          this.store.dispatch(
            new LeadCreationUserDataCreateAction({
              leadCreationUserDataModel: this.leadCreationUserDataModel,
            })
          );
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  validateAtleastOneFieldToBeMandatory(type: string): void {
    if (type === "contactInfo") {
      if (
        !this.contactInfoFormGroupControls.get("mobile2").value &&
        !this.contactInfoFormGroupControls.get("officeNo").value &&
        !this.contactInfoFormGroupControls.get("premisesNo").value
      ) {
        this.setAtleastOneFieldRequiredError(type);
      } else {
        this.leadForm.controls["contactInfo"] = removeFormControlError(
          this.leadForm.controls["contactInfo"] as FormGroup,
          "atleastOneOfTheFieldsIsRequired"
        );
      }
    } else {
      if (
        (this.addressInfoFormGroupControls.get("isAddressComplex").value &&
          !this.addressInfoFormGroupControls.get("buildingNo").value &&
          !this.addressInfoFormGroupControls.get("buildingName").value) ||
        (
          this.addressInfoFormGroupControls.get("isAddressExtra").value &&
          !this.addressInfoFormGroupControls.get("estateName").value &&
          !this.addressInfoFormGroupControls.get("estateStreetName").value &&
          !this.addressInfoFormGroupControls.get("estateStreetNo").value
        )
      ) {
        this.setAtleastOneFieldRequiredError(type);
      }
      else {
        this.leadForm.controls["addressInfo"] = removeFormControlError(
          this.leadForm.controls["addressInfo"] as FormGroup,
          "atleastOneOfTheFieldsIsRequired"
        );
      }
    }
  }

  setAtleastOneFieldRequiredError(type: string): void {
    if (type === "contactInfo") {
      this.contactInfoFormGroupControls
        .get("mobile2")
        .setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.contactInfoFormGroupControls
        .get("officeNo")
        .setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.contactInfoFormGroupControls
        .get("premisesNo")
        .setErrors({ atleastOneOfTheFieldsIsRequired: true });
    } else {
      if (this.addressInfoFormGroupControls.get("isAddressComplex").value) {
        this.addressInfoFormGroupControls
          .get("buildingNo")
          .setErrors({ atleastOneOfTheFieldsIsRequired: true });
        this.addressInfoFormGroupControls
          .get("buildingName")
          .setErrors({ atleastOneOfTheFieldsIsRequired: true });
      }
      if (this.addressInfoFormGroupControls.get("isAddressExtra").value) {
        this.addressInfoFormGroupControls
          .get("estateName")
          .setErrors({ atleastOneOfTheFieldsIsRequired: true });
        this.addressInfoFormGroupControls
          .get("estateStreetNo")
          .setErrors({ atleastOneOfTheFieldsIsRequired: true });
        this.addressInfoFormGroupControls
          .get("estateStreetName")
          .setErrors({ atleastOneOfTheFieldsIsRequired: true });
      }
    }
  }

  onChangeReconnNewCustomerSelection(selectedReconnCustomerProfile) {
    this.selectedReconnCustomerProfile = selectedReconnCustomerProfile;
    this.dataList.forEach((data) => {
      data.isChecked = data.index == selectedReconnCustomerProfile.index ? true : false;
    });
  }

  onSubmit(type = 'others'): void {
    if (type == 'new lead') {
      this.rxjsService.setGlobalLoaderProperty(true);
    }
    this.validateAtleastOneFieldToBeMandatory("contactInfo");
    this.validateAtleastOneFieldToBeMandatory("addressInfo");
    const isMobile1Duplicate = new FindDuplicatePipe().transform(
      this.contactInfoFormGroupControls.get("mobile1").value,
      this.contactInfoFormGroupControls,
      "mobile1"
    );
    const isMobile2Duplicate = new FindDuplicatePipe().transform(
      this.contactInfoFormGroupControls.get("mobile2").value,
      this.contactInfoFormGroupControls,
      "mobile2",
      this.contactInfoFormGroupControls.get("mobile2CountryCode").value
    );
    const isOfficeNoDuplicate = new FindDuplicatePipe().transform(
      this.contactInfoFormGroupControls.get("officeNo").value,
      this.contactInfoFormGroupControls,
      "officeNo"
    );
    const isPremisesNoDuplicate = new FindDuplicatePipe().transform(
      this.contactInfoFormGroupControls.get("premisesNo").value,
      this.contactInfoFormGroupControls,
      "premisesNo"
    );
    this.leadForm.get("leadCategoryId").markAllAsTouched();
    this.addressInfoFormGroupControls.get("formatedAddress").markAllAsTouched();
    if (
      this.leadForm.invalid ||
      isMobile1Duplicate ||
      isMobile2Duplicate ||
      isOfficeNoDuplicate ||
      isPremisesNoDuplicate
    ) {
      let addressFormGroupRawValues = this.addressInfoFormGroupControls.getRawValue();
      if (
        !addressFormGroupRawValues.provinceName ||
        !addressFormGroupRawValues.suburbName ||
        !addressFormGroupRawValues.cityName
      ) {
        this.snackbarService.openSnackbar(
          "Province Name / City Name is required..!!",
          ResponseMessageTypes.ERROR
        );
        return;
      }
    }
    this.leadForm.value.createdUserId = this.loggedInUserData.userId;
    this.leadForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.leadForm.value.leadGroupId = this.leadForm.value.leadGroupId ? this.leadForm.value.leadGroupId : this.leadGroupTypes.NEW; //remove later
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.leadForm.value.addressInfo = (this.leadForm.controls['addressInfo'] as FormGroup).getRawValue();
    this.leadForm.value.contactInfo = (this.leadForm.controls['contactInfo'] as FormGroup).getRawValue();
    this.leadForm.value.basicInfo = (this.leadForm.controls['basicInfo'] as FormGroup).getRawValue();
    if (!this.contactInfoFormGroupControls.get("mobile2").value) {
      delete this.leadForm.value.contactInfo.mobile2CountryCode;
    }
    if (!this.contactInfoFormGroupControls.get("premisesNo").value) {
      delete this.leadForm.value.contactInfo.premisesNoCountryCode;
    }
    if (!this.contactInfoFormGroupControls.get("officeNo").value) {
      delete this.leadForm.value.contactInfo.officeNoCountryCode;
    }
    this.leadForm.value.contactInfo.mobile1 = this.leadForm.value.contactInfo.mobile1.toString().replace(/\s/g, "");
  if (this.leadForm.value.contactInfo.mobile2) {
    this.leadForm.value.contactInfo.mobile2 = this.leadForm.value.contactInfo.mobile2.toString().replace(/\s/g, "");
  }
  if (this.leadForm.value.contactInfo.officeNo) {
    this.leadForm.value.contactInfo.officeNo = this.leadForm.value.contactInfo.officeNo.toString().replace(/\s/g, "");
  }
  if (this.leadForm.value.contactInfo.premisesNo) {
    this.leadForm.value.contactInfo.premisesNo = this.leadForm.value.contactInfo.premisesNo.toString().replace(/\s/g, "");
  }
    if (this.leadId !== "" && this.leadId) {
      this.leadForm.value.isReconnectionLead = false;
    } else {
      this.leadForm.value.isReconnectionLead =
        this.reconnectionMessage !== "" ? true : false;
    }
    this.leadForm.value.customerId = this.leadForm.value.customerId
      ? this.leadForm.value.customerId
      : this.customerId;
    if (this.addressInfoFormGroupControls.get('isAfrigisSearch').value) {
      this.leadForm.value.afrigisAddressInfo = this.afrigisAddressInfo;
    }
    else {
      this.leadForm.value.afrigisAddressInfo = null;
    }
    if (this.leadForm.value.addressInfo.latLong) {
      this.leadForm.value.addressInfo.latitude = this.leadForm.value.addressInfo.latLong.split(",")[0];
      this.leadForm.value.addressInfo.longitude = this.leadForm.value.addressInfo.latLong.split(",")[1];
    }
    if(this.leadForm.value.leadCategoryId){
      let selectedleadCategoryId = [];
      this.leadForm.value?.leadCategoryId?.forEach(result=>{
       selectedleadCategoryId.push(result?.id);
      })
      this.leadForm.value.leadCategoryId = selectedleadCategoryId;
    }
    let crudService: Observable<IApplicationResponse> = this.crudService.create(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.LEAD,
      this.leadForm.value);
    if (type == 'new customer reconnection') {
      this.leadForm.value.isReconnectionAddressConfirmation = true;
      this.rxjsService.setDialogOpenProperty(false);
    }
    else if (type == 'new lead') {
      this.leadForm.value.isReconnectionAddressConfirmation = true;
      this.leadForm.value.customerId = this.selectedReconnCustomerProfile.customerId ?
        this.selectedReconnCustomerProfile.customerId : this.leadForm.value.customerId;
    }
    this.isFormSubmitted = true;
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.leadId = response.resources;
        this.btnName = "Next";
        this.nextOrPrevStepper("submit");
      } else if (
        !response.isSuccess &&
        response.statusCode == 409 &&
        response.message &&
        response.message.includes("Lead This address exists in")
      ) {
        $(this.confirm_reconnection_modal.nativeElement).modal("show");
        this.reconnectionMessage = response.message;
      } else if (
        !response.isSuccess &&
        response.statusCode == 409 &&
        response.exceptionMessage &&
        response.exceptionMessage.includes(
          "Already customer profile exist, Please link customer"
        )
      ) {
        $(this.existing_lead_modal.nativeElement).modal("show");
        response.resources.forEach((resp, index) => {
          resp.index = index;
        });
        this.dataList = [];
        this.dataList = response.resources;
        this.selectedExistingCustomerProfile = this.dataList.find(
          (d) => d["isChecked"]
        );
        this.isNewProfileBtnDisabled = true;
      }
      else if (
        !response.isSuccess &&
        response.statusCode == 409 &&
        response.exceptionMessage &&
        response.exceptionMessage.includes(
          "List of Customer Associated with this Address"
        )
      ) {
        $(this.reconn_new_customer_modal.nativeElement).modal("show");
        response.resources.forEach((dataObj, index) => {
          dataObj["index"] = index;
          dataObj["isChecked"] = false;
        });
        this.dataList = response.resources;
      }
      this.isFormSubmitted = false;
    });
  }

  nextOrPrevStepper(type?: string): void {
    if (type === btnActionTypes.SUBMIT) {
      this.navigateToLeadViewPage();
    } else if (type === btnActionTypes.NEXT) {
    } else if (type === btnActionTypes.PREVIOUS) {
      if (this.fromModule && this.fromModule == 'Customer') {
        this.rxjsService.setViewCustomerData({
          customerId: this.customerId,
          addressId: this.addressId,
          customerTab: 0,
          monitoringTab: null,
          feature_name: this.feature,
          featureIndex: this.featureIndex,
        })
        this.rxjsService.navigateToViewCustomerPage();
      }
      else if (this.leadId) {
        this.navigateToLeadViewPage();
      } else {
        if (this.fromUrl == "Leads List") {
          this.router.navigateByUrl("/sales/task-management/leads");
        } else if (this.fromUrl == "My Leads List") {
          this.router.navigate(["my-tasks/my-sales"], {
            queryParams: { leadId: this.leadId, tab: 1 },
          });
        } else if (this.fromUrl == "My Raw Leads List") {
          this.router.navigate(["my-tasks/my-sales"], {
            queryParams: { leadId: this.leadId, tab: 0 },
          });
        }
      }
    }
  }

  navigateToLeadViewPage() {
    if (this.fromUrl == "Leads List" || this.fromUrl == "Raw Leads List") {
      this.router.navigate(["/sales/task-management/leads/lead-info/view"], {
        queryParams: { leadId: this.leadId },
      });
    } else if (this.fromUrl == "My Leads List" || this.fromUrl == "My Raw Leads List") {
      this.router.navigate(["/my-tasks/my-sales/my-leads/lead-info/view"], {
        queryParams: { leadId: this.leadId },
      });
    }
  }

  navigateToList(e): void {
    this.router.navigate(["/sales/task-management/leads"]);
  }

  openAddressPopup() {
    let isAccessAllowed = this.filteredSubComponentsPermissions?.find(fSC => fSC.menuName === 'New Address Flow');
    if (isAccessAllowed) {
      this.leadForm.value.createdUserId = this.loggedInUserData.userId;
      this.leadForm.value.modifiedUserId = this.loggedInUserData.userId;
      let leadForm = { ...this.leadForm.value, ...this.leadForm.getRawValue() };
      const dialogReff = this.dialog.open(NewAddressPopupComponent, {
        width: "750px",
        disableClose: true,
        data: leadForm,
      });
      dialogReff.afterClosed().subscribe((result) => {
        if (result) {
          this.isCustomAddressSaved = false;
        }
      });
      dialogReff.componentInstance.outputData.subscribe((result) => {
        if (result) {
          this.isCustomAddressSaved = true;
          this.shouldShowLocationPinBtn = result.isAddressType == false ? true : false;
          this.addressInfoFormGroupControls.patchValue({
            formatedAddress: getFullFormatedAddress(result),
            addressConfidentLevelId: result.addressConfidentLevelId,
            suburbName: result.suburbName,
            cityName: result.cityName,
            provinceName: result.provinceName,
            postalCode: result.postalCode,
            streetName: result.streetName,
            streetNo: result.streetNo,
            buildingNo: result.buildingNo,
            buildingName: result.buildingName,
            estateName: result.estateName,
            estateStreetName: result.estateStreetName,
            estateStreetNo: result.estateStreetNo,
          });
          this.rxjsService.setFormChangeDetectionProperty(true);
        }
        else {
          this.isCustomAddressSaved = false;
        }
      });
    }
    else {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
  }
}
