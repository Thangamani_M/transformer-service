import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import {
  BookAppoinmentModalComponent, CustomerModuleApiSuffixModels, CUSTOMER_COMPONENT, LeadCreationStepperParamsCreateAction,
  LeadCreationUserDataCreateAction, LeadCreationUserDataModel, LeadHeaderData, LeadHeaderDataCreateAction, LeadNoteModalComponent, SalesModuleApiSuffixModels, Sales_Leads_Components, ScheduleCallbackViewModalComponent, selectLeadCreationStepperParamsState$,
  selectLeadCreationUserDataState$, ServiceAgreementStepsParams
} from '@app/modules';
import { AppState } from '@app/reducers';
import { AppDataService, BreadCrumbModel, IApplicationResponse, LoggedInUserModel, ModuleName, ModulesBasedApiSuffix, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { currentComponentPageBasedPermissionsSelector$ } from '@app/shared/layouts';
import { CrudService, RxjsService } from '@app/shared/services';
import { PERMISSION_RESTRICTION_ERROR } from '@app/shared/utils';
import { MY_TASK_COMPONENT } from '@modules/my-tasks/shared';
import { loggedInUserData } from '@modules/others';
import { ScheduleCallBackViewModel } from '@modules/sales/models/lead-sms-notification.model';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';

declare let $;
@Component({
  selector: 'app-lead-info-view',
  templateUrl: './lead-info-view.component.html',
  styleUrls: ['./lead-info.component.scss']
})
export class LeadInfoViewComponent implements OnInit {
  breadCrumb: BreadCrumbModel;
  leadDetails: LeadHeaderData;
  leadId = "";
  leadNotes;
  fromUrl = 'Leads List';
  loggedInUserData: LoggedInUserModel;
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  urlPrefix = '';
  leadInfo: any;
  isCpaExempt = true;
  @ViewChild('exempt_modal', { static: false }) exempt_modal: ElementRef<any>;
  leadNotesDataRefEnum = {
    LEAD_NOTES: 'Lead Notes',
    EMAIL_NOTIFICATIONS: 'Email Notifications',
    SMS_NOTIFICATIONS: 'SMS Notifications',
    NEW_QUOTE_REQUEST: 'New Quote Request',
    LEAD_CALLBACK: 'Lead Callback',
    LEAD_ASSIGNED_USERS: 'Lead Assigned Users',
    APPOINTMENTS: 'Appointments',
    APPOINTMENT_ESCALATION: 'Appointment Escalation',
  }
  filteredSubComponentsPermissions = [];

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private dialog: MatDialog,
    private appDataService: AppDataService, private snackbarService: SnackbarService,
    public rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectLeadCreationStepperParamsState$), this.store.select(selectLeadCreationUserDataState$),
      this.activatedRoute.queryParams]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = response[0];
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[1]);
      this.leadCreationUserDataModel = new LeadCreationUserDataModel(
        response[2]
      );
      this.leadId = response[3]['leadId'];
      this.getLeadNote();
      this.getLeadDetailsByLeadId();
    });
  }

  ngOnInit(): void {
    // this.router.routeReuseStrategy.shouldReuseRoute = function () {
    //   return false;
    // };
    this.rxjsService.getFromUrl().subscribe((fromUrl: string) => {
      this.fromUrl = fromUrl ? fromUrl : 'My Leads List';
      this.appDataService.fromUrlOfLeadLifecycle = this.fromUrl;
      this.urlPrefix = this.fromUrl == 'Leads List' ? '/sales/task-management/leads/' : '/my-tasks/my-sales/my-leads/';
    });
    this.combineLatestNgrxStoreDataForPermissions();
  }

  combineLatestNgrxStoreDataForPermissions() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let componentPermissions = [];
      if (this.fromUrl == "Leads List") {
        componentPermissions = response[0][Sales_Leads_Components.LEADS];
      }
      else if (this.fromUrl == "My Leads List" || this.fromUrl == "My Raw Leads List") {
        componentPermissions = response[0][MY_TASK_COMPONENT.MY_SALES];
        componentPermissions = componentPermissions?.filter(cP => cP.menuName === 'My Leads').map((fS => fS['subMenu']))[0];
      }
      else if (this.fromUrl == "Customer Management") {
        let customer = response[0][CUSTOMER_COMPONENT.CUSTOMER]?.find(el => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD);
        let sales = customer?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.SALES);
        componentPermissions = sales?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.LEADS)['subMenu']
      }
      if (componentPermissions) {
        this.filteredSubComponentsPermissions = componentPermissions?.filter(cP => cP.menuName === Sales_Leads_Components.LEAD_LANDING_PAGE).map((fS => fS['subMenu']))[0];
      }
    });
    this.getForkJoinRequests();
    this.onBootstrapModalEventChanges();
  }

  onBootstrapModalEventChanges(): void {
    $("#exempt_modal").on("shown.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#exempt_modal").on("hidden.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  test(e) {
    $('#sticky-sidebar').toggleClass('hide');
  }

  closeNav() {
    document.getElementById("quickaction_sidebar").style.width = "0";
    $('.main-container').css({ width: '100%' });
    $('#quickaction_sidebar .quick-action-menu-con').css({ right: '0', visibility: 'hidden' });
  }

  openNav() {
    document.getElementById("quickaction_sidebar").style.width = "250px";
    $('.main-container').css({ width: 'calc(100% - 250px)' });
    $('#quickaction_sidebar .quick-action-menu-con').css({ right: '12px', visibility: 'visible' });
  }

  getLeadDetailsByLeadId(): void {
    if (!this.leadId) return;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEADS_HEADER, this.leadId, false, undefined, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        let leadHeaderData = new LeadHeaderData(response.resources);
        leadHeaderData.fromUrl = this.fromUrl;
        this.store.dispatch(new LeadHeaderDataCreateAction({ leadHeaderData }));
        this.leadCreationUserDataModel.leadStatusName = leadHeaderData.leadStatusName;
        this.leadCreationUserDataModel = { ...this.leadCreationUserDataModel, ...response.resources };
        this.store.dispatch(new LeadCreationUserDataCreateAction({ leadCreationUserDataModel: this.leadCreationUserDataModel }));
      }
    });
  }

  getForkJoinRequests(): void {
    if (!this.leadId) return;
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LEADS_LANDING, this.leadId, false, undefined)])
      .subscribe((resp: IApplicationResponse[]) => {
        resp.forEach((res: IApplicationResponse, ix: number) => {
          if (res.isSuccess && res.statusCode === 200 && res.resources) {
            switch (ix) {
              case 0:
                this.leadDetails = new LeadHeaderData(res.resources);
                this.leadInfo = res.resources;
                this.prepareDynamicBreadCrumbs();
                break;
            }
          }
        });
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  prepareDynamicBreadCrumbs() {
    if (this.fromUrl == 'Leads List') {
      this.breadCrumb = {
        pageTitle: { key: 'Lead Info', value: this.leadDetails.customerName },
        items: [{ key: this.leadDetails.leadRefNo }, {
          key: 'Leads', routeUrl: '/sales/task-management/leads'
        }, { key: 'view' }]
      };
    }
    else if (this.fromUrl == 'My Leads List') {
      this.breadCrumb = {
        pageTitle: { key: 'My Lead Info', value: this.leadDetails.customerName },
        items: [{ key: this.leadDetails.leadRefNo }, { key: 'My Tasks' }, { key: 'My Sales' }, {
          key: 'My Leads', routeUrl: `/my-tasks/my-sales?leadId=${this.leadDetails.leadId}&tab=1`
        }, { key: 'view' }]
      };
    }
  }

  getLeadNote() {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LEAD_NOTES, this.leadId, false, null).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.leadNotes = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onEmittedEvent(e) {
    this.getLeadNote();
  }

  getCreatePermissionByActionIconType(actionTypeMenuName): boolean {
    let foundObj = this.filteredSubComponentsPermissions?.find(fSC => fSC.menuName === actionTypeMenuName);
    if (foundObj) {
      return foundObj.subMenu.length == 0 ? true : false;
    }
    else {
      return true;
    }
  }

  openScheduleAppointment() {
    let isAccessDenied = this.getCreatePermissionByActionIconType('Schedule Appointment');
    if (isAccessDenied) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    else {
      const dialogReff = this.dialog.open(BookAppoinmentModalComponent, { width: '900px', disableClose: true });
      dialogReff.componentInstance.outputData.subscribe((result) => {
        if (result) {
          this.getLeadNote();
        }
      });
    }
  }

  openBookAppoinmentHistory() {
    let isAccessDenied = this.getCreatePermissionByActionIconType('Appointment History');
    if (isAccessDenied) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    else {
      this.router.navigate([`${this.urlPrefix}appointment-history`], { queryParams: { leadId: this.leadId } });
    }
  }

  teleSaleCall(): void {
    let isAccessDenied = this.getCreatePermissionByActionIconType('Tele Sales Call');
    if (isAccessDenied) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    else {
      this.router.navigate([`${this.urlPrefix}lead-services/add-edit`], { queryParams: { leadId: this.leadId, customerId: this.leadDetails?.['customerId'] } });
    }
  }

  updateLeadInfo(): void {
    let isAccessDenied = this.getCreatePermissionByActionIconType('Update Lead Info');
    if (isAccessDenied) {
      this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    else {
      if (this.fromUrl === 'Leads List' || this.fromUrl == 'Raw Leads List' || this.fromUrl == 'Customer Management') {
        this.router.navigate([`${this.urlPrefix}lead-info/add-edit`], { queryParams: { leadId: this.leadId } });
      } else if (this.fromUrl === 'My Leads List' || this.fromUrl == 'My Raw Leads List') {
        this.router.navigate([`${this.urlPrefix}lead-info/add-edit`], { queryParams: { leadId: this.leadId } })
      }
    }
  }

  onRedirectToCustomerViewPage() {
    this.rxjsService.setViewCustomerData({
      customerId: this.leadDetails.customerId,
      addressId: this.leadDetails.addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  ngOnDestroy() {
    let serviceAgreementStepsParams = new ServiceAgreementStepsParams({
      createdUserId: this.loggedInUserData ? this.loggedInUserData.userId : null,
      fromUrl: this.fromUrl
    });
    this.store.dispatch(new LeadCreationStepperParamsCreateAction({ serviceAgreementStepsParams }));
  }

  openLeadDialog(): void {
    const dialogReff = this.dialog.open(LeadNoteModalComponent, { width: '60vw', data: { leadId: this.leadId }, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    })
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getLeadNote();
      }
    });
  }

  openExemptPopup() {
    this.isCpaExempt = this.leadInfo.isCPAExempt;
    $(this.exempt_modal.nativeElement).modal('show');
  }

  saveCpaExempt() {
    this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMERS_CPA_EXEMPT, {
      customerId: this.leadDetails.customerId,
      isCPAExempt: this.isCpaExempt,
      modifiedUserId: this.loggedInUserData.userId
    }).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.leadInfo.isCPAExempt = this.isCpaExempt;
        $(this.exempt_modal.nativeElement).modal('hide');
      }
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  openScheduleCallbackViewComponent(leadCallbackId) {
    let data = new ScheduleCallBackViewModel({ leadCallbackId, fromModule: 'Lead', type: 'readOnly' });
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(ScheduleCallbackViewModalComponent, { width: '900px', disableClose: true, data });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.getLeadNote();
    });
  }

  ngDestory() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
