import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { selectStaticEagerLoadingDocumentTypesState$ } from '@app/modules/';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, CustomDirectiveConfig, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { LeadHeaderData, OtherDocumentUploadModel, SalesModuleApiSuffixModels, selectLeadHeaderDataState$ } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'other-document-add-edit',
  templateUrl: './other-document-add-edit.component.html'
})
export class OtherDocumentAddEditUploadComponent implements OnInit {
  otherDocumentUploadForm: FormGroup;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  uploadedFile: File;
  fileId: "";
  documentTypes = [];
  breadCrumb: BreadCrumbModel;
  leadHeaderData: LeadHeaderData;
  loggedInUserData: LoggedInUserModel;
  uploadFileName = "";

  constructor(private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createUploadForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingDocumentTypesState$),
      this.store.select(selectLeadHeaderDataState$),
      this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.documentTypes = response[1];
      this.leadHeaderData = new LeadHeaderData(response[2]);
      this.fileId = response[3]['fileId'];
      this.breadCrumb = {
        pageTitle: { key: this.fileId ? 'Update Uploaded File' : 'Upload File', value: this.leadHeaderData.leadRefNo },
        items: [{ key: this.leadHeaderData.leadRefNo, routeUrl: 'landingPage' }, {
          key: 'Other Documents'
        }, { key: this.fileId ? 'Update Uploaded File' : 'Upload File' }]
      };
      if (this.fileId) {
        this.getDocumentDetailsById();
      }
    });
  }

  createUploadForm() {
    let supportDocumentUploadModel = new OtherDocumentUploadModel();
    this.otherDocumentUploadForm = this.formBuilder.group({});
    Object.keys(supportDocumentUploadModel).forEach((key) => {
      this.otherDocumentUploadForm.addControl(key, new FormControl(supportDocumentUploadModel[key]));
    });
    let requiredFormControlValidators = this.fileId ? ["documentTypeId", "fileName"] : ["documentTypeId", "fileUpload", "fileName"];
    this.otherDocumentUploadForm = setRequiredValidator(this.otherDocumentUploadForm, requiredFormControlValidators);
  }

  getDocumentDetailsById() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.LEAD_DOCUMENT, this.fileId, false, null, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.uploadFileName = response.resources.fileName;
          this.otherDocumentUploadForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  emitUploadedFiles(uploadedFile) {
    if (!uploadedFile || uploadedFile?.length == 0) {
      return;
    }
    this.uploadedFile = uploadedFile;
    this.otherDocumentUploadForm.controls.fileUpload.setValue(uploadedFile['name']);
  }

  onSubmit() {
    this.otherDocumentUploadForm.get('fileUpload').markAllAsTouched();
    if (this.otherDocumentUploadForm.invalid) {
      return;
    }
    let formData = new FormData();
    this.otherDocumentUploadForm.value.leadId = this.leadHeaderData.leadId;
    this.otherDocumentUploadForm.value.referenceId = this.leadHeaderData.leadId;
    this.otherDocumentUploadForm.value.createdUserId = this.loggedInUserData.userId;
    if(this.uploadedFile){
      formData.append("file", this.uploadedFile, this.uploadedFile['name']);
    }
    formData.append("json", JSON.stringify(this.otherDocumentUploadForm.value));
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.LEAD_DOCUMENT, formData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.navigateToOtherDocuments();
        }
      });
  }

  navigateToOtherDocuments() {
    if (this.leadHeaderData.fromUrl == 'Leads List') {
      this.router.navigate(['/sales/task-management/leads/other-documents'], { queryParams: { id: this.leadHeaderData.leadId } });
    }
    else {
      this.router.navigate(['/my-tasks/my-sales/my-leads/other-documents'], { queryParams: { id: this.leadHeaderData.leadId } });
    }
  }
}

