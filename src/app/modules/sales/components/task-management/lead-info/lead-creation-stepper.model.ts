class LeadStepperCreationModel {
    constructor(leadStepperCreationModel?: LeadStepperCreationModel) {
        this.isFormValid = leadStepperCreationModel.isFormValid;
        this.nextStepper = leadStepperCreationModel.nextStepper;
        this.isBackBtnClicked = leadStepperCreationModel.isBackBtnClicked;
        this.isSubmitAliasNextBtnClicked = leadStepperCreationModel.isSubmitAliasNextBtnClicked;
        this.stepperParams = leadStepperCreationModel.stepperParams;
    }
    isFormValid?= false;
    nextStepper: number;
    isBackBtnClicked?= false;
    isSubmitAliasNextBtnClicked?= false;
    stepperParams?: StepperParams;
}

class StepperParams {
    constructor(stepperParams: StepperParams) {
        this.leadId = stepperParams.leadId;
        this.rawLeadId = stepperParams.rawLeadId;
        this.leadName = stepperParams.leadName;
        this.rawLeadName = stepperParams.rawLeadName;
        this.cityId = stepperParams.cityId;

    }
    leadId?= undefined;
    rawLeadId?= undefined;
    leadName?= undefined;
    rawLeadName?= undefined;
    cityId?= undefined;
}

class ServiceAgreementStepsParams {
    constructor(stepperParams: ServiceAgreementStepsParams) {
        this.isFirstTabFormSubmitted = stepperParams.isFirstTabFormSubmitted == undefined ? false : stepperParams.isFirstTabFormSubmitted;
        this.isSecondTabFormSubmitted = stepperParams.isSecondTabFormSubmitted == undefined ? false : stepperParams.isSecondTabFormSubmitted;
        this.isThirdTabFormSubmitted = stepperParams.isThirdTabFormSubmitted == undefined ? false : stepperParams.isThirdTabFormSubmitted;
        this.isFourthTabFormSubmitted = stepperParams.isFourthTabFormSubmitted == undefined ? false : stepperParams.isFourthTabFormSubmitted;
        this.isFifthTabFormSubmitted = stepperParams.isFifthTabFormSubmitted == undefined ? false : stepperParams.isFifthTabFormSubmitted;
        this.isSixthTabFormSubmitted = stepperParams.isSixthTabFormSubmitted == undefined ? false : stepperParams.isSixthTabFormSubmitted;
        this.isSeventhTabFormSubmitted = stepperParams.isSeventhTabFormSubmitted == undefined ? false : stepperParams.isSeventhTabFormSubmitted;
        this.isEighthTabFormSubmitted = stepperParams.isEighthTabFormSubmitted == undefined ? false : stepperParams.isEighthTabFormSubmitted;
        this.isNinethTabFormSubmitted = stepperParams.isNinethTabFormSubmitted == undefined ? false : stepperParams.isNinethTabFormSubmitted;
        this.leadId = stepperParams.leadId == undefined ? "" : stepperParams.leadId;
        this.rawLeadId = stepperParams.rawLeadId;
        this.agreementId = stepperParams.agreementId == undefined ? "" : stepperParams.agreementId;
        this.passwordId = stepperParams.passwordId;
        this.siteHazardId = stepperParams.siteHazardId;
        this.propertyAccessId = stepperParams.propertyAccessId;
        this.accountHolderId = stepperParams.accountHolderId;
        this.installationCheckListId = stepperParams.installationCheckListId;
        this.installationInfoId = stepperParams.installationInfoId;
        this.termsConditionId = stepperParams.termsConditionId;
    }
    isFirstTabFormSubmitted?= false;
    isSecondTabFormSubmitted?= false;
    isThirdTabFormSubmitted?= false;
    isFourthTabFormSubmitted?= false;
    isFifthTabFormSubmitted?= false;
    isSixthTabFormSubmitted?= false;
    isSeventhTabFormSubmitted?= false;
    isEighthTabFormSubmitted?= false;
    isNinethTabFormSubmitted?= false;
    leadId: string;
    keyholderId?: string = undefined;
    rawLeadId?: string = undefined;
    agreementId?: string;
    passwordId?: string = undefined;
    siteHazardId?: string = undefined;
    propertyAccessId?: string = undefined;
    accountHolderId?: string = undefined;
    installationCheckListId?: string = undefined;
    installationInfoId?: string = undefined;
    termsConditionId?: string = undefined;
}

class ServiceAgreementModel {
    params: ServiceAgreementStepsParams;
    selectedIndex: number
}

export { LeadStepperCreationModel, StepperParams, ServiceAgreementModel }