import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, debounceTimeForSearchkeyword, formConfigs, HttpCancelService, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { NewAddressModel, SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils';
import { Store } from '@ngrx/store';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-new-address',
  templateUrl: './new-address-popup.html',
  styleUrls: ['./new-address-popup.scss']
})
export class NewAddressPopupComponent implements OnInit {
  newAddressForm: FormGroup;
  @Output() outputData = new EventEmitter<any>();
  selectedCityOption = {};
  selectedSuburbOption = {};
  provinces = [];
  formConfigs = formConfigs;
  getLoopableObjectCityRequestObservable: Observable<IApplicationResponse>;
  getLoopableObjectSuburbRequestObservable: Observable<IApplicationResponse>;
  // re-usable for non-customer
  isShowAddressType = true;
  loggedInUserData: LoggedInUserModel;

  constructor(@Inject(MAT_DIALOG_DATA) public leadInfoFormData,
    private dialog: MatDialog, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private router: Router, private store: Store<AppState>,
    private crudService: CrudService, private httpCancelService: HttpCancelService) {
    this.rxjsService.setDialogOpenProperty(true);
    this.combineLatestNgrxStoreDataOne();
  }

  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(loggedInUserData),
    ]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  ngOnInit(): void {
    this.leadInfoFormData = { ...this.leadInfoFormData.basicInfo, ...this.leadInfoFormData };
    delete this.leadInfoFormData.basicInfo;
    this.leadInfoFormData = { ...this.leadInfoFormData.contactInfo, ...this.leadInfoFormData };
    delete this.leadInfoFormData.contactInfo;
    this.leadInfoFormData = { ...this.leadInfoFormData.addressInfo, ...this.leadInfoFormData };
    delete this.leadInfoFormData.addressInfo;
    this.leadInfoFormData.mobile2 = this.leadInfoFormData.mobile2 ? this.leadInfoFormData.mobile2.toString().replace(/\s/g, "") : null;
    this.leadInfoFormData.officeNo = this.leadInfoFormData.officeNo ? this.leadInfoFormData.officeNo.toString().replace(/\s/g, "") : null;
    this.leadInfoFormData.mobile1 = this.leadInfoFormData.mobile1 ? this.leadInfoFormData.mobile1.toString().replace(/\s/g, "") : null;
    this.leadInfoFormData.premisesNo = this.leadInfoFormData.premisesNo ? this.leadInfoFormData.premisesNo.toString().replace(/\s/g, "") : null;
    if (this.leadInfoFormData?.isShowAddressType === false || this.leadInfoFormData?.fromComponent == 'Office Address') {
      this.isShowAddressType = false;
    }
    this.getProvinces();
    this.createNewAddressForm();
    this.onFormControlChanges();
  }

  filterDataByKeywordSearch(searchtext: string, type: string): Observable<IApplicationResponse> {
    switch (type) {
      case "city":
        return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_CITIES, null, true, prepareGetRequestHttpParams(null, null, {
          searchtext, provinceId: this.newAddressForm.get('provinceId').value
        }));
      case "suburb":
        return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_SUBURBS,
          null, true, prepareGetRequestHttpParams(null, null, {
            searchtext, provinceId: this.newAddressForm.get('provinceId').value
          }));
    }
  }

  getProvinces() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_PROVINCES,
      undefined, false, prepareRequiredHttpParams({ countryId: formConfigs.countryId }))
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
          this.provinces = resp.resources;
          this.newAddressForm.patchValue(this.leadInfoFormData);
          if (this.leadInfoFormData?.cityName) {
            this.newAddressForm.get('city').setValue(this.leadInfoFormData.cityName);
            this.selectedCityOption = { id: this.leadInfoFormData.cityId, displayName: this.leadInfoFormData.cityName };
          }
          if (this.leadInfoFormData?.suburbName) {
            this.newAddressForm.get('suburb').setValue(this.leadInfoFormData.suburbName);
            this.selectedSuburbOption = { id: this.leadInfoFormData.suburbId, displayName: this.leadInfoFormData.suburbName };
          }
        }
      });
  }

  createNewAddressForm(): void {
    let newAddressModel = new NewAddressModel();
    this.newAddressForm = this.formBuilder.group({});
    Object.keys(newAddressModel).forEach((key) => {
      this.newAddressForm.addControl(key, new FormControl(newAddressModel[key]));
    });
    this.newAddressForm = setRequiredValidator(this.newAddressForm, ["formatedAddress", "city", "suburb", "streetName",
      "postalCode", 'provinceId', "streetNo"]);
    this.newAddressForm.controls.formatedAddress.setValue(this.leadInfoFormData.formatedAddress);
    this.newAddressForm.controls.isAddressExtra.setValue(this.leadInfoFormData?.isHideEstateAddres ? this.leadInfoFormData?.isAddressExtra : true);
    if (this.leadInfoFormData?.isShowAddressType === false || this.leadInfoFormData?.fromComponent == 'Office Address') {
      this.newAddressForm.get('isAddressType').setValue(false);
    }
  }

  onFormControlChanges(): void {
    this.newAddressForm.get('isAddressType').valueChanges.subscribe((isAddressType: boolean) => {
      if (isAddressType) {
        this.newAddressForm.get("formatedAddress").setValidators([Validators.required])
        this.newAddressForm.get("formatedAddress").updateValueAndValidity();
      } else {
        this.newAddressForm.get("formatedAddress").clearValidators();
        this.newAddressForm.get("formatedAddress").updateValueAndValidity();
      }
    });
    this.onCityNameFormControlValueChanges();
    this.onSuburbNameFormControlValueChanges();
  }

  onSuburbNameFormControlValueChanges(): void {
    this.newAddressForm
      .get("suburb")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((searchtext) => {
          if (!searchtext) {
            this.getLoopableObjectSuburbRequestObservable = of();
          }
          else {
            this.getLoopableObjectSuburbRequestObservable = this.filterDataByKeywordSearch(searchtext, 'suburb');
          }
        });
  }

  onCityNameFormControlValueChanges(): void {
    this.newAddressForm
      .get("city")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((searchtext) => {
          if (!searchtext) {
            this.getLoopableObjectCityRequestObservable = of();
          }
          else {
            this.getLoopableObjectCityRequestObservable = this.filterDataByKeywordSearch(searchtext, 'city');
          }
        });
  }

  onSelectedItemOption(selectedObject, type?: string): void {
    if (!selectedObject) return;
    if (type == 'city') {
      this.selectedCityOption = selectedObject;
    }
    else if (type == 'suburb') {
      this.selectedSuburbOption = selectedObject;
    }
  }

  onSubmit(): void {
    if (this.newAddressForm.invalid) {
      this.newAddressForm.get('city').markAllAsTouched();
      this.newAddressForm.get('suburb').markAllAsTouched();
      return;
    }
    this.leadInfoFormData = { ...this.leadInfoFormData, ...this.newAddressForm.value };
    let found = this.provinces.filter(bt => bt['id'] === this.newAddressForm.value.provinceId);
    this.leadInfoFormData.province = found[0].displayName;
    this.leadInfoFormData.mobile2 = this.leadInfoFormData.mobile2 ? this.leadInfoFormData.mobile2 : null;
    this.leadInfoFormData.officeNo = this.leadInfoFormData.officeNo ? this.leadInfoFormData.officeNo : null;
    this.leadInfoFormData.mobile1 = this.leadInfoFormData.mobile1 ? this.leadInfoFormData.mobile1 : null;
    this.leadInfoFormData.premisesNo = this.leadInfoFormData.premisesNo ? this.leadInfoFormData.premisesNo : null;
    this.leadInfoFormData.createdUserId = this.loggedInUserData.userId;
    this.leadInfoFormData.addressId = null;
    if (!this.leadInfoFormData.isAddressType) {
      this.leadInfoFormData.suburbName = this.newAddressForm.value.suburb;
      this.leadInfoFormData.provinceName = this.leadInfoFormData.province;
      this.leadInfoFormData.cityName = this.newAddressForm.value.city;
    }
    if (!this.leadInfoFormData.addressConfidentLevelId) {
      this.leadInfoFormData.addressConfidentLevelId = 0;
    }
    delete this.leadInfoFormData.leadCategoryId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (this.leadInfoFormData.isAddressType) {
      let crudService: Observable<IApplicationResponse> =
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_NEW_ADDRESS, this.leadInfoFormData);
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.dialog.closeAll();
          this.router.navigateByUrl("/sales/raw-lead");
        }
      });
    } else {
      let obj = {
        addressInfo: this.leadInfoFormData,
        createdUserId: this.loggedInUserData.userId
      }
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_CUSTOM_ADDRESS, obj, 1)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.outputData.emit(this.leadInfoFormData);
            this.dialog.closeAll();
          }
        });
    }
  }

  onDiscardForm(): void {
    this.newAddressForm.reset(new NewAddressModel());
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
