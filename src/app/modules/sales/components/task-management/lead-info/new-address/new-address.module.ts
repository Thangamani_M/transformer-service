import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import {
    NewAddressPopupComponent
} from '@sales/components/task-management';
@NgModule({
  declarations: [NewAddressPopupComponent],
  imports: [
    CommonModule, 
    MaterialModule, SharedModule, ReactiveFormsModule, FormsModule, 
  ],
  exports: [],
  providers: [],
  entryComponents: [NewAddressPopupComponent]
})
export class NewAddressModule { }
