import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, CommonPaginationConfig, ExtensionModalComponent, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, SignalRTriggers } from '@app/shared';
import { ResponseMessageTypes } from "@app/shared/enums";
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { select, Store } from '@ngrx/store';
import { TelephonyApiSuffixModules } from '@sales/shared';
import { combineLatest } from 'rxjs';
import { callBackCustomerInfoModalComponent } from '../../lead-notes/callback-customers-info-popup.component';
import { callBackCustomerListModalComponent } from '../../lead-notes/callback-customers-list-popup.component';
import { QueueTransferPopupComponent } from './queue-transfer-popup.component';
import { UserTransferPopupComponent } from './user-transfer-popup.component';
@Component({
  selector: 'app-open-scape',
  templateUrl: './open-scape.component.html',
  styleUrls: ['./open-scape.component.scss']
})
export class OpenScapComponent implements OnInit {
  @Input() value: number;
  @Input() details;
  @Output('onComplete') timerOver: EventEmitter<any> = new EventEmitter<any>();
  limit = CommonPaginationConfig.defaultPageSize;
  pageIndex: any = CommonPaginationConfig.defaultPageIndex;
  agentExtensionNo;
  loggedInUserData: LoggedInUserModel;
  customerContactNumber;
  clientName = "";
  rawLeadId = "";
  leadId = "";
  customerId = "";
  finalMobileNumber = '';
  uniqueCallId;
  targetPosition = 0;
  clickCount = 0;
  moreCalls = [];
  recentCalls = [];
  mobile = {};
  isCallDialled = false;
  //new 
  receivedScheduleCallbackDetail;
  receivedInboundCallDetail;
  receivedCustomerContactDetail;
  receivedOpenscapeConfigDetail;
  dialPadHeading = 'Dial Pad';
  isInBoundCallComing = false;
  isOutBoundCallGoing = false;
  isOpenScapeDivMaximized = false;
  showDialpadContainer = true;
  showIncomingOrOutgoingCallContainer = false;
  showOnGoingCallTimerContainer = false;
  isCallOnHold = false;
  showRecentCallLogs = false;
  selectedIndex;
  standardInboundCustomerDetail;
  selectedCustomerDetailFromCustomersListComponent;
  siteAddressId = "";

  constructor(private rxjs: RxjsService,
    private store: Store<AppState>,
    private http: CrudService,
    private dialog: MatDialog,
    private snackbarService: SnackbarService, private router: Router
  ) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (!this.agentExtensionNo) {
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    }
    else {
      // receive schedule callback observable
      this.rxjs.getscheduleCallbackDetails().subscribe((scheduleCallbackDetails) => {
        if (!scheduleCallbackDetails) {
          return;
        }
        this.receivedScheduleCallbackDetail = scheduleCallbackDetails;
        this.leadId = this.receivedScheduleCallbackDetail.leadId ? this.receivedScheduleCallbackDetail.leadId : null;
        this.customerId = this.receivedScheduleCallbackDetail.customerId ? this.receivedScheduleCallbackDetail.customerId : null;
        this.siteAddressId = this.receivedCustomerContactDetail.siteAddressId ? this.receivedCustomerContactDetail.siteAddressId : null;
        this.finalMobileNumber = this.receivedScheduleCallbackDetail.contactNumber;
        this.finalMobileNumber = this.finalMobileNumber && this.finalMobileNumber.replace(/[a-z]/gi, '').replace(/ /g, '');
        this.customerContactNumber = this.finalMobileNumber;
        this.clientName = this.selectedCustomerDetailFromCustomersListComponent?.FullName ?
          this.selectedCustomerDetailFromCustomersListComponent.FullName : this.receivedScheduleCallbackDetail.Customers ? this.receivedScheduleCallbackDetail.Customers[0].FullName : 'Unknown';
        this.createOutboundCallToCustomer();
      });
      // after received customer call  ( from CCC to customer )
      this.rxjs.getUniqueCallId().subscribe((openscapeConfigDetail) => {
        if (!openscapeConfigDetail) {
          return;
        }
        this.dialPadHeading = 'Outgoing...';
        this.isInBoundCallComing = false;
        this.isOutBoundCallGoing = false;
        this.isCallOnHold = false;
        this.receivedOpenscapeConfigDetail = openscapeConfigDetail;
        this.uniqueCallId = this.receivedOpenscapeConfigDetail.UniqueCallid;
        switch (this.receivedOpenscapeConfigDetail?.triggerName) {
          case SignalRTriggers.OutboundCallDeliveredTrigger:
            this.isOutBoundCallGoing = true;
            this.dialPadHeading = 'Outgoing...';
            this.showIncomingOrOutgoingCallContainer = true;
            this.showOnGoingCallTimerContainer = false;
            break;
          case SignalRTriggers.OutboundCallNotificationsTrigger:
            this.dialPadHeading = 'Active Call...';
            this.showOnGoingCallTimerContainer = true;
            this.showIncomingOrOutgoingCallContainer = false;
            if (this.customerId) {
              this.router.navigate(["/customer/manage-customers/customer-verification"], {
                queryParams: {
                  customerId: this.customerId, siteAddressId: this.siteAddressId
                }
              });
            }
            break;
          // under development
          case SignalRTriggers.CallHoldNotificationsTrigger:
            this.dialPadHeading = 'On Hold...';
            this.isCallOnHold = true;
            this.showOnGoingCallTimerContainer = true;
            this.showIncomingOrOutgoingCallContainer = false;
            break;
          case SignalRTriggers.CallTransferredNotificationsTrigger:
            this.dialPadHeading = 'Active Call...';
            this.showOnGoingCallTimerContainer = true;
            this.showIncomingOrOutgoingCallContainer = false;
            break;
          // end of under development
          case SignalRTriggers.OutboundCallDisconnectedNotificationsTrigger:
            this.dialPadHeading = 'Dial Pad';
            this.showDialpadContainer = true;
            this.showOnGoingCallTimerContainer = false;
            this.showIncomingOrOutgoingCallContainer = false;
            this.finalMobileNumber = "";
            this.isCallDialled = false;
            this.rxjs.setUniqueCallId(null);
            this.dialog.closeAll();
            break;
          case SignalRTriggers.OutboundCallFailedNotificationsTrigger:
            this.dialPadHeading = 'Dial Pad';
            this.showDialpadContainer = true;
            this.showOnGoingCallTimerContainer = false;
            this.showIncomingOrOutgoingCallContainer = false;
            this.finalMobileNumber = "";
            this.isCallDialled = false;
            this.rxjs.setUniqueCallId(null);
            this.dialog.closeAll();
            break;
        }
      });
      // receive customer contact detail observable  ( alias outbound calls from CCC to customer )
      this.rxjs.getCustomerContactNumber().subscribe((customerContactDetail) => {
        if (!customerContactDetail) {
          return;
        }
        this.dialPadHeading = 'Outgoing...';
        this.isInBoundCallComing = false;
        this.isOutBoundCallGoing = true;
        this.receivedCustomerContactDetail = customerContactDetail;
        this.customerContactNumber = this.receivedCustomerContactDetail.customerContactNumber;
        this.clientName = this.receivedCustomerContactDetail.clientName;
        this.rawLeadId = this.receivedCustomerContactDetail.rawLeadId;
        this.customerId = this.receivedCustomerContactDetail.customerId ? this.receivedCustomerContactDetail.customerId : null;
        this.siteAddressId = this.receivedCustomerContactDetail.siteAddressId ? this.receivedCustomerContactDetail.siteAddressId : null;
        this.leadId = this.receivedCustomerContactDetail.leadId && this.receivedCustomerContactDetail.leadId;
        this.finalMobileNumber = this.receivedCustomerContactDetail.customerContactNumber.replace(/[a-z]/gi, '').replace(/ /g, '')
        this.createOutboundCallToCustomer();
      });
      // receive inbound call detail observable
      this.rxjs.getInboundCallDetails().subscribe((inboundCallDetail) => {
        if (!inboundCallDetail) {
          return;
        }
        this.showRecentCallLogs = false;
        this.isOpenScapeDivMaximized = true;
        this.receivedInboundCallDetail = inboundCallDetail;
        this.finalMobileNumber = this.standardInboundCustomerDetail?.From ? this.standardInboundCustomerDetail.From :
          this.receivedInboundCallDetail?.From ? this.receivedInboundCallDetail.From : '-';
        this.finalMobileNumber = this.finalMobileNumber && this.finalMobileNumber.replace(/[a-z]/gi, '').replace(/ /g, '');
        this.customerContactNumber = this.finalMobileNumber;
        this.clientName = this.selectedCustomerDetailFromCustomersListComponent?.FullName ?
          this.selectedCustomerDetailFromCustomersListComponent.FullName : (this.standardInboundCustomerDetail?.Customers &&
            this.standardInboundCustomerDetail.Customers.length > 0) ? this.standardInboundCustomerDetail.Customers[0].FullName :
            (this.receivedInboundCallDetail?.Customers &&
              this.receivedInboundCallDetail.Customers.length > 0) ? this.receivedInboundCallDetail.Customers[0].FullName : 'Unknown';
        this.uniqueCallId = this.receivedInboundCallDetail.UniqueCallid;
        switch (this.receivedInboundCallDetail?.triggerName) {
          case SignalRTriggers.InboundCallDeliveredTrigger:
            this.dialPadHeading = 'Incoming Call...';
            this.isInBoundCallComing = true;
            this.showIncomingOrOutgoingCallContainer = true;
            this.showDialpadContainer = false;
            this.showOnGoingCallTimerContainer = false;
            this.standardInboundCustomerDetail = inboundCallDetail;
            break;
          case SignalRTriggers.InboundCallNotificationsTrigger:
            let matDialogConfig: MatDialogConfig = { disableClose: true, data: this.standardInboundCustomerDetail, width: '850px' };
            this.dialPadHeading = 'Active Call...';
            this.showOnGoingCallTimerContainer = true;
            this.showIncomingOrOutgoingCallContainer = false;
            if (this.standardInboundCustomerDetail?.RoleTypeId == 2) {
              let dialogReff = this.dialog.open(callBackCustomerListModalComponent, matDialogConfig);
              dialogReff.componentInstance.outputData.subscribe(ele => {
                if (this.receivedInboundCallDetail?.triggerName !== SignalRTriggers.InboundCallFailedNotificationsTrigger &&
                  this.receivedOpenscapeConfigDetail?.triggerName !== SignalRTriggers.OutboundCallFailedNotificationsTrigger) {
                  this.dialog.open(callBackCustomerListModalComponent, matDialogConfig);
                }
              });
            } else if (this.standardInboundCustomerDetail?.RoleTypeId == 1) {
              let dialogReff = this.dialog.open(callBackCustomerInfoModalComponent, matDialogConfig);
              dialogReff.afterClosed().subscribe((result) => {
              });
            }
            break;
          case SignalRTriggers.InboundCallDisconnectedNotificationsTrigger:
            this.dialPadHeading = 'Dial Pad';
            this.showIncomingOrOutgoingCallContainer = false;
            this.showOnGoingCallTimerContainer = false;
            this.showDialpadContainer = true;
            this.finalMobileNumber = "";
            this.isCallDialled = false;
            this.dialog.closeAll();
            break;
          case SignalRTriggers.InboundCallFailedNotificationsTrigger:
            this.dialPadHeading = 'Dial Pad';
            this.showIncomingOrOutgoingCallContainer = false;
            this.showOnGoingCallTimerContainer = false;
            this.showDialpadContainer = true;
            this.finalMobileNumber = "";
            this.isCallDialled = false;
            this.dialog.closeAll();
            //this.snackbarService.openSnackbar('Something went wrong', ResponseMessageTypes.ERROR);
            break;
        }
      });
      // receive selected customer detail from the customers list when inbound call triggers
      this.rxjs.getAnyPropertyValue().subscribe((selectedCustomerDetail) => {
        if (!selectedCustomerDetail) {
          return;
        }
        if (selectedCustomerDetail.key == 'openscape module') {
          this.selectedCustomerDetailFromCustomersListComponent = selectedCustomerDetail;
          this.clientName = this.selectedCustomerDetailFromCustomersListComponent.FullName;
          this.customerContactNumber = this.standardInboundCustomerDetail?.From ? this.standardInboundCustomerDetail.From :
            this.receivedInboundCallDetail?.From ? this.receivedInboundCallDetail.From : '-';
        }
      });
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.store.pipe(select(agentLoginDataSelector))])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.agentExtensionNo = response[1];
      });
  }

  highlightRow(e) {
    this.selectedIndex = e;
  }

  onScroll(e) {
    this.pageIndex = parseInt(this.pageIndex) + 1;
    this.getRecentCalls();
  }

  getRecentCalls() {
    this.http.get(
      ModulesBasedApiSuffix.TELEPHONY,
      TelephonyApiSuffixModules.RECENT_CALL,
      undefined,
      false, prepareGetRequestHttpParams(this.pageIndex.toString(), this.limit.toString(), {
        userId: this.loggedInUserData.userId,
        extension: this.agentExtensionNo
      })
    ).subscribe(resp => {
      if (this.pageIndex == 0) {
        this.recentCalls = resp.resources;
      } else {
        this.moreCalls = resp.resources;
        this.recentCalls = [...this.recentCalls, ...this.moreCalls];
      }
      this.rxjs.setGlobalLoaderProperty(false);
    });
  }

  numberClick(number) {
    if (this.clickCount === 0) {
      this.targetPosition = this.finalMobileNumber.length;
    }
    this.finalMobileNumber = [this.finalMobileNumber.slice(0, this.targetPosition), number, this.finalMobileNumber.slice(this.targetPosition)].join('');
    if (this.targetPosition === this.finalMobileNumber.length) {
      this.targetPosition = this.finalMobileNumber.length;
    } else {
      this.targetPosition = this.targetPosition + 1;
    }
  }

  changeInput(e) {
    this.clickCount = this.clickCount + 1;
    this.targetPosition = e.target.selectionStart;
  }

  clearNumber() {
    if (this.targetPosition === 0) {
      return;
    }
    this.finalMobileNumber = this.finalMobileNumber.slice(0, this.targetPosition - 1) + this.finalMobileNumber.slice(this.targetPosition);
    this.targetPosition = this.targetPosition - 1;
  }

  createOutboundCallToCustomer() {
    if (!this.agentExtensionNo || !this.leadId)
      this.mobile[this.finalMobileNumber] = this.finalMobileNumber
    const payload = {
      from: this.agentExtensionNo,
      to: this.finalMobileNumber,
      pabxId: this.loggedInUserData.openScapePabxId,
      userId: this.loggedInUserData.userId,
      rawLeadId: this.rawLeadId ? this.rawLeadId : null,
      leadId: this.leadId ? this.leadId : null,
      customerId: this.customerId ? this.customerId : null
    }
    if (this.isCallDialled) {
      return;
    }
    this.http.create(ModulesBasedApiSuffix.TELEPHONY, TelephonyApiSuffixModules.CUSTOMER_DIAL, payload).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.isOpenScapeDivMaximized = true;
        this.showIncomingOrOutgoingCallContainer = true;
        this.showDialpadContainer = false;
        this.showRecentCallLogs = false;
        this.showOnGoingCallTimerContainer = false;
        this.isCallDialled = true;
      } else {
        this.isCallDialled = false;
        this.rxjs.setscheduleCallbackDetails(null);
        this.rxjs.setCustomerContactNumber(null);
        this.rxjs.setInboundCallDetails(null);
        this.rxjs.setAnyPropertyValue(null);
        this.showOnGoingCallTimerContainer = false;
        this.showIncomingOrOutgoingCallContainer = false;
        this.showDialpadContainer = true;
        this.isInBoundCallComing = false;
        this.isOutBoundCallGoing = false;
        this.isCallOnHold = false;
        this.dialPadHeading = 'Dial Pad';
      }
    });
  }

  onHoldCall() {
    this.isCallOnHold = !this.isCallOnHold;
    if (this.isCallOnHold) {
      const payload = {
        from: this.agentExtensionNo,
        uniqueCallId: this.uniqueCallId,
        pabxId: this.loggedInUserData.openScapePabxId,
        userId: this.loggedInUserData.userId,
      };
      this.http.create(ModulesBasedApiSuffix.TELEPHONY, TelephonyApiSuffixModules.HOLD, payload).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dialPadHeading = 'On Hold...';
        }
      });
    }
    else {
      this.dialPadHeading = 'Active Call...';
    }
  }

  toggleWindow(toggleType: string) {
    if (toggleType === 'minimize') {
      this.isOpenScapeDivMaximized = false;
    } else {
      this.isOpenScapeDivMaximized = true;
    }
  }

  dialPadClick() {
    if (!this.showOnGoingCallTimerContainer) {
      this.showRecentCallLogs = !this.showRecentCallLogs;
      if (this.showRecentCallLogs) {
        this.dialPadHeading = this.isInBoundCallComing ? 'Incoming Call...' : this.isOutBoundCallGoing ? 'Outgoing Call..' : 'Dial Pad';
        if (this.recentCalls.length == 0) {
          this.getRecentCalls();
        }
      }
      else {
        this.dialPadHeading = this.isInBoundCallComing ? 'Incoming Call...' : this.isOutBoundCallGoing ? 'Outgoing Call..' : 'Dial Pad';
      }
    }
  }

  createOutGoingOrAcceptIncomingCustomerCall() {
    if (!this.finalMobileNumber) {
      return;
    }
    if (this.isInBoundCallComing) {
      let payload = {
        from: this.agentExtensionNo,
        pabxId: this.loggedInUserData.openScapePabxId,
        userId: this.loggedInUserData.userId
      }
      this.http.create(ModulesBasedApiSuffix.TELEPHONY, TelephonyApiSuffixModules.ANSWER, payload).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.showOnGoingCallTimerContainer = true;
          this.showIncomingOrOutgoingCallContainer = false;
          this.showDialpadContainer = false;
          this.isInBoundCallComing = false;
        }
      });
    }
  }

  disconnectCall() {
    this.mobile = {};
    const payload = {
      from: this.agentExtensionNo,
      pabxId: this.loggedInUserData.openScapePabxId,
      userId: this.loggedInUserData.userId,
    };
    this.http.create(ModulesBasedApiSuffix.TELEPHONY, TelephonyApiSuffixModules.DISCONNECT, payload).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.isCallDialled = false;
        this.rxjs.setscheduleCallbackDetails(null);
        this.rxjs.setCustomerContactNumber(null);
        this.rxjs.setInboundCallDetails(null);
        this.rxjs.setAnyPropertyValue(null);
      }
      this.showOnGoingCallTimerContainer = false;
      this.showIncomingOrOutgoingCallContainer = false;
      this.showDialpadContainer = true;
      this.isInBoundCallComing = false;
      this.isOutBoundCallGoing = false;
      this.isCallOnHold = false;
      this.dialPadHeading = 'Dial Pad';
    });
  }

  openActiveCallFeatures(type: string) {
    this.rxjs.setDialogOpenProperty(true);
    if (type == 'user transfer') {
      this.dialog.open(UserTransferPopupComponent, {
        width: '750px', disableClose: true, data: {
          agentExtensionNo: this.agentExtensionNo,
          uniqueCallId: this.uniqueCallId,
          pabxId: this.loggedInUserData.openScapePabxId,
          userId: this.loggedInUserData.userId
        }
      });
    }
    else if ('queue transfer') {
      this.dialog.open(QueueTransferPopupComponent, { width: '300px', disableClose: true });
    }
  }
}
