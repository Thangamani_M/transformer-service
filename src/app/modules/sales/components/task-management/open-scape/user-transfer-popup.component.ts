import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { TelephonyApiSuffixModules } from '@sales/shared';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-user-transfer-popup',
  templateUrl: './user-transfer-popup.component.html',
  styleUrls: ['./open-scape.component.scss']
})

export class UserTransferPopupComponent implements OnInit {
  transferCallForm: FormGroup;
  selectedUserData;
  userList;
  agents = [];
  isFormSubmitted = false;
  loggedInUserData: LoggedInUserModel;

  constructor(private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data,
    private http: CrudService, private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjs: RxjsService) { }

  ngOnInit() {
    this.rxjs.setDialogOpenProperty(true);
    this.combineLatestNgrxStoreData();
    this.createForm();
    this.getUserDetails();
    this.transferCallForm.get('userNumber').valueChanges.subscribe((userId) => {
      if (!userId) return;
      this.selectedUserData = this.userList?.find(uL => uL.userId == userId);
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData)])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      });
  }

  createForm() {
    this.transferCallForm = this.formBuilder.group({
      userNumber: ['', Validators.required],
    });
  }

  callTransfer() {
    this.isFormSubmitted = true;
    if (this.transferCallForm.invalid) {
      return
    }
    let payload = {
      from: this.data.agentExtensionNo,
      to: this.selectedUserData.extension,
      uniqueCallId: this.data.uniqueCallId,
      pabxId: this.loggedInUserData.openScapePabxId,
      userId: this.data.userId
    }
    this.rxjs.setFormChangeDetectionProperty(true);
    this.http.create(ModulesBasedApiSuffix.TELEPHONY, TelephonyApiSuffixModules.SINGLE_STEP_TRANSFER, payload)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.dialog.closeAll();
        }
      });
  }

  getUserDetails(pageIndex?: string, pageSize?: string) {
    this.http.get(
      ModulesBasedApiSuffix.TELEPHONY,
      TelephonyApiSuffixModules.PABX_AGENTS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        UserId: this.data.userId,
        Extension: this.data.agentExtensionNo
      })
    ).subscribe(resp => {
      if (resp.resources && resp.statusCode == 200 && resp.isSuccess) {
        this.userList = resp.resources;
        let leadCategoryTmpArray = resp.resources;
        for (let i = 0; i < leadCategoryTmpArray.length; i++) {
          let temp = {};
          temp['display'] = `${leadCategoryTmpArray[i].displayName} (${leadCategoryTmpArray[i].extension})`;
          temp['id'] = leadCategoryTmpArray[i].userId;
          this.agents.push(temp);
        }
      }
      this.rxjs.setPopupLoaderProperty(false);
    });
  }

  ngOnDestroy() {
    this.rxjs.setDialogOpenProperty(false);
  }
}