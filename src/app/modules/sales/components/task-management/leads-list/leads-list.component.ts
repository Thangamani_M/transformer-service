import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LeadCreationUserDataCreateAction, LeadCreationUserDataModel, LeadHeaderDataCreateAction, SalesModuleApiSuffixModels, Sales_Leads_Components, selectStaticEagerLoadingLeadGroupsState$, selectStaticEagerLoadingLeadStatusState$, selectStaticEagerLoadingSiteTypesState$ } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  RxjsService,currentComponentPageBasedPermissionsSelector$, SnackbarService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes
} from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-leads-list',
  templateUrl: './leads-list.component.html'
})

export class LeadsListComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties:any = {
    tableCaption: "Leads",
    breadCrumbItems: [{ displayName: 'Leads', relativeRouterUrl: '' }, { displayName: 'Leads Dashboard' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Leads',
          dataKey: 'leadId',
          enableAction: true,
          enableAddActionBtn: true,
          enableBreadCrumb: true,
          enableScrollable: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'leadRefNo', header: 'Lead Number' },
          { field: 'clientName', header: 'Client Name' },
          { field: 'suburbName', header: 'Suburb' },
          { field: 'appointmentDate', header: 'Appt Date & Time', isDateTime: true },
          { field: 'leadGroupName', header: 'Lead Group', type: 'dropdown', options: [] },
          { field: 'siteTypeName', header: 'Site Type', type: 'dropdown', options: [] },
          { field: 'assignTo', header: 'Assigned To' },
          { field: 'leadStatusName', header: 'Status', width: '200px', type: 'dropdown', options: [] },
          ],
          apiSuffixModel: InventoryModuleApiSuffixModels.ITEM_BRAND,
          moduleName: ModulesBasedApiSuffix.INVENTORY,
        },
      ]
    }
  }

  constructor(
    private crudService: CrudService,
    private router: Router,private snackbarService:SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>) {
    super();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getLeads();
    this.store.dispatch(new LeadCreationUserDataCreateAction({ leadCreationUserDataModel: new LeadCreationUserDataModel() }));
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(selectStaticEagerLoadingLeadGroupsState$),
      this.store.select(selectStaticEagerLoadingSiteTypesState$),
      this.store.select(selectStaticEagerLoadingLeadStatusState$),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).pipe(take(1)).subscribe((response) => {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns[4].options = response[0] = response[0]?.map(item => {
        return { label: item.displayName, value: item.id };
      });
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns[5].options = response[1] = response[1]?.map(item => {
        return { label: item.displayName, value: item.id };
      });
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns[7].options = response[2] = response[2]?.map(item => {
        return { label: item.displayName, value: item.id };
      });
      if (response[3][Sales_Leads_Components.LEADS]) {
        let prepareDynamicTableTabsFromPermissionsObj =
          prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, [response[3][Sales_Leads_Components.LEADS][0]]);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object,searchObj?): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        else {
          this.openAddEditPage(CrudType.CREATE, row);
        }
        break;
        case CrudType.GET:
          if (searchObj?.leadStatusName) {
            searchObj['leadStatusId'] = searchObj['leadStatusName'];
          } else if (searchObj?.siteTypeName) {
            searchObj['siteTypeId'] = searchObj['siteTypeName'];
          } else if (searchObj?.leadGroupName) {
            searchObj['leadGroupId'] = searchObj['leadGroupName'];
          }
          this.getLeads(row["pageIndex"], row["pageSize"], searchObj);
          break;
        case CrudType.VIEW:
          this.openAddEditPage(CrudType.VIEW, row);
          break;
    }
  }
  
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    this.rxjsService.setFromUrl("Leads List");
    switch (type) {
      case CrudType.CREATE:
        this.router.navigateByUrl("/sales/task-management/leads/lead-info/add-edit");
        this.store.dispatch(new LeadHeaderDataCreateAction({ leadHeaderData: { fromUrl: 'Leads List' } }));
        break;
      case CrudType.VIEW:
        this.router.navigate(['/sales/task-management/leads/lead-info/view'], { queryParams: { leadId: editableObject['leadId'], selectedIndex: 1 } });
        break;
    }
  }

  getLeads(pageIndex?: string, pageSize?: string,
    otherParams?: object) {
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_LEADS;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      salesModuleApiSuffixModels, undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          response.resources.forEach((leadObj) => {
            leadObj.cssClass = leadObj.leadSubStatusCssClass;
          });
          this.dataList = response.resources;
          this.totalRecords = response.totalCount;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
}