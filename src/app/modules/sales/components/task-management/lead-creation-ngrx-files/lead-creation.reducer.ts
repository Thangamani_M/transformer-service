import { LeadCreationUserDataModel, LeadHeaderData, ServiceAgreementStepsParams } from '@modules/sales/models';
import {
  LeadCreationStepperParamsActions, LeadCreationStepperParamsActionTypes, LeadCreationUserDataActions,
  LeadCreationUserDataActionTypes
} from '@sales/components/task-management';

export function leadCreationUserDataReducer(state = new LeadCreationUserDataModel(),
  action: LeadCreationUserDataActions): LeadCreationUserDataModel {
  switch (action.type) {
    case LeadCreationUserDataActionTypes.LeadCreationUserDataCreateAction:
      return action.payload.leadCreationUserDataModel;

    case LeadCreationUserDataActionTypes.LeadCreationUserDataChangeAction:
      return { ...state, ...action.payload.leadCreationUserDataModel };

    case LeadCreationUserDataActionTypes.LeadCreationUserDataRemoveAction:
      return null;

    default:
      return state;
  }
}

export function leadCreationStepperParamsReducer(state = new ServiceAgreementStepsParams(),
  action: LeadCreationStepperParamsActions): ServiceAgreementStepsParams {
  switch (action.type) {
    case LeadCreationStepperParamsActionTypes.LeadCreationStepperParamsCreateAction:
      return action.payload.serviceAgreementStepsParams;

    case LeadCreationStepperParamsActionTypes.LeadCreationStepperParamsChangeAction:
      return { ...state, ...action.payload.serviceAgreementStepsParams };

    case LeadCreationStepperParamsActionTypes.LeadCreationStepperParamsRemoveAction:
      return null;

    default:
      return state;
  }
}

export function leadHeaderDataReducer(state = new LeadHeaderData(),
  action: LeadCreationStepperParamsActions): LeadHeaderData {
  switch (action.type) {
    case LeadCreationStepperParamsActionTypes.LeadHeaderDataCreateAction:
      return action.payload.leadHeaderData;

    case LeadCreationStepperParamsActionTypes.LeadHeaderDataRemoveAction:
      return null;
      
    default:
      return state;
  }
}