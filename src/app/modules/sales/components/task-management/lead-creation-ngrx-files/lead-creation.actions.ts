import { LeadCreationUserDataModel, LeadHeaderData, ServiceAgreementStepsParams } from '@modules/sales/models';
import { Action } from '@ngrx/store';

enum LeadCreationUserDataActionTypes {
  LeadCreationUserDataCreateAction = '[Lead Creation] User Data Create Action',
  LeadCreationUserDataChangeAction = '[Lead Creation] User Data Change Action',
  LeadCreationUserDataRemoveAction = '[Lead Creation] User Data Remove Action',
}

class LeadCreationUserDataCreateAction implements Action {

  readonly type = LeadCreationUserDataActionTypes.LeadCreationUserDataCreateAction;

  constructor(public payload: { leadCreationUserDataModel: LeadCreationUserDataModel }) {
  }

}

class LeadCreationUserDataChangeAction implements Action {

  readonly type = LeadCreationUserDataActionTypes.LeadCreationUserDataChangeAction;

  constructor(public payload: { leadCreationUserDataModel: LeadCreationUserDataModel }) {
  }

}

class LeadCreationUserDataRemoveAction implements Action {

  readonly type = LeadCreationUserDataActionTypes.LeadCreationUserDataRemoveAction;

}

type LeadCreationUserDataActions = LeadCreationUserDataCreateAction | LeadCreationUserDataChangeAction | LeadCreationUserDataRemoveAction;

enum LeadCreationStepperParamsActionTypes {
  LeadCreationStepperParamsCreateAction = '[Lead Creation] Stepper Params Create Action',
  LeadCreationStepperParamsChangeAction = '[Lead Creation] Stepper Params Action',
  LeadCreationStepperParamsRemoveAction = '[Lead Creation] Stepper Params Remove Action',
  LeadHeaderDataCreateAction = '[Lead Header Data] Lead Header Data Create Action',
  LeadHeaderDataRemoveAction = '[Lead Header Data] Lead Header Data Remove Action',
}
class LeadCreationStepperParamsCreateAction implements Action {

  readonly type = LeadCreationStepperParamsActionTypes.LeadCreationStepperParamsCreateAction;

  constructor(public payload: { serviceAgreementStepsParams: ServiceAgreementStepsParams }) {
  }

}

class LeadCreationStepperParamsChangeAction implements Action {

  readonly type = LeadCreationStepperParamsActionTypes.LeadCreationStepperParamsChangeAction;

  constructor(public payload: { serviceAgreementStepsParams: ServiceAgreementStepsParams }) {
  }

}

class LeadCreationStepperParamsRemoveAction implements Action {

  readonly type = LeadCreationStepperParamsActionTypes.LeadCreationStepperParamsRemoveAction;

}

class LeadHeaderDataCreateAction implements Action {

  readonly type = LeadCreationStepperParamsActionTypes.LeadHeaderDataCreateAction;

  constructor(public payload: { leadHeaderData: LeadHeaderData }) {
  }

}

class LeadHeaderDataRemoveAction implements Action {

  readonly type = LeadCreationStepperParamsActionTypes.LeadHeaderDataRemoveAction;

}

type LeadCreationStepperParamsActions = |
  LeadCreationStepperParamsCreateAction | LeadCreationStepperParamsChangeAction | LeadCreationStepperParamsRemoveAction |
  LeadHeaderDataCreateAction | LeadHeaderDataRemoveAction;

export {
  LeadCreationUserDataCreateAction, LeadCreationUserDataChangeAction, LeadCreationUserDataRemoveAction, LeadCreationStepperParamsActions,
  LeadCreationStepperParamsCreateAction, LeadCreationStepperParamsChangeAction, LeadCreationStepperParamsRemoveAction, LeadCreationUserDataActions,
  LeadCreationStepperParamsActionTypes, LeadCreationUserDataActionTypes, LeadHeaderDataCreateAction, LeadHeaderDataRemoveAction
};

