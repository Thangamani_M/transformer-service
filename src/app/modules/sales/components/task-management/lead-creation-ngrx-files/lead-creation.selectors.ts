import { createSelector, createFeatureSelector } from '@ngrx/store';
import { ServiceAgreementStepsParams, LeadStepperCreationModel, LeadHeaderData } from '@modules/sales/models';


const selectLeadCreationUserDataState = createFeatureSelector<LeadStepperCreationModel>("leadCreationUserData");
const selectLeadCreationStepperParamsState = createFeatureSelector<ServiceAgreementStepsParams>("leadCreationStepperParams");
const selectLeadHeaderDataState = createFeatureSelector<LeadHeaderData>("leadHeaderData");

const selectLeadCreationUserDataState$ = createSelector(
  selectLeadCreationUserDataState,
  selectLeadCreationUserDataState => selectLeadCreationUserDataState
);

const selectLeadCreationStepperParamsState$ = createSelector(
  selectLeadCreationStepperParamsState,
  selectLeadCreationStepperParamsState => selectLeadCreationStepperParamsState
);

const selectLeadHeaderDataState$ = createSelector(
  selectLeadHeaderDataState,
  selectLeadHeaderDataState => selectLeadHeaderDataState
);

export {
  selectLeadCreationUserDataState, selectLeadCreationStepperParamsState, selectLeadCreationUserDataState$,
  selectLeadCreationStepperParamsState$, selectLeadHeaderDataState$
}