import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {
  LeadCreationUserDataCreateAction, LeadCreationUserDataChangeAction, LeadCreationUserDataRemoveAction, LeadCreationStepperParamsActions,
  LeadCreationStepperParamsCreateAction, LeadCreationStepperParamsChangeAction, LeadCreationStepperParamsRemoveAction, LeadCreationUserDataActions,
  LeadCreationStepperParamsActionTypes, LeadCreationUserDataActionTypes, LeadHeaderDataCreateAction, LeadHeaderDataRemoveAction
} from '@sales/components/task-management/lead-creation-ngrx-files';
import { tap } from 'rxjs/operators';
import { defer, of } from 'rxjs';
import { AppDataService } from '@app/shared/services';

@Injectable()
export class LeadCreationEffects {

  @Effect({ dispatch: false })
  createLeadUserData$ = this.actions$.pipe(
    ofType<LeadCreationUserDataCreateAction>(LeadCreationUserDataActionTypes.LeadCreationUserDataCreateAction),
    tap(action =>
      this.appDataService.leadCreationUserData = action.payload.leadCreationUserDataModel)
  );

  @Effect({ dispatch: false })
  updateLeadUserData$ = this.actions$.pipe(
    ofType<LeadCreationUserDataChangeAction>(LeadCreationUserDataActionTypes.LeadCreationUserDataChangeAction),
    tap(action =>
      this.appDataService.leadCreationUserData = action.payload.leadCreationUserDataModel)
  );

  @Effect({ dispatch: false })
  removeLeadUserData$ = this.actions$.pipe(
    ofType<LeadCreationUserDataRemoveAction>(LeadCreationUserDataActionTypes.LeadCreationUserDataRemoveAction),
    tap(() =>
      this.appDataService.leadCreationUserData = null
    )
  );

  @Effect()
  initUserData$ = defer(() => {
    const leadCreationUserDataModel = this.appDataService.leadCreationUserData;
    if (leadCreationUserDataModel) {
      return of(new LeadCreationUserDataCreateAction({ leadCreationUserDataModel }));
    }
    else {
      return <any>of(new LeadCreationUserDataRemoveAction());
    }
  });


  @Effect({ dispatch: false })
  createLeadStepperParams$ = this.actions$.pipe(
    ofType<LeadCreationStepperParamsCreateAction>(LeadCreationStepperParamsActionTypes.LeadCreationStepperParamsCreateAction),
    tap(action =>
      this.appDataService.leadCreationStepperParams = action.payload.serviceAgreementStepsParams)
  );

  @Effect({ dispatch: false })
  updateLeadStepperParams$ = this.actions$.pipe(
    ofType<LeadCreationStepperParamsChangeAction>(LeadCreationStepperParamsActionTypes.LeadCreationStepperParamsChangeAction),
    tap(action =>
      this.appDataService.leadCreationStepperParams = action.payload.serviceAgreementStepsParams)
  );

  @Effect({ dispatch: false })
  removeLeadStepperParams$ = this.actions$.pipe(
    ofType<LeadCreationStepperParamsRemoveAction>(LeadCreationStepperParamsActionTypes.LeadCreationStepperParamsRemoveAction),
    tap(() =>
      this.appDataService.leadCreationStepperParams = null
    )
  );

  @Effect()
  initStepperParams$ = defer(() => {
    const serviceAgreementStepsParams = this.appDataService.leadCreationStepperParams;
    if (serviceAgreementStepsParams) {
      return of(new LeadCreationStepperParamsCreateAction({ serviceAgreementStepsParams }));
    }
    else {
      return <any>of(new LeadCreationStepperParamsRemoveAction());
    }
  });

  @Effect({ dispatch: false })
  createLeadHeaderData$ = this.actions$.pipe(
    ofType<LeadHeaderDataCreateAction>(LeadCreationStepperParamsActionTypes.LeadHeaderDataCreateAction),
    tap(action =>
      this.appDataService.leadHeaderData = action.payload.leadHeaderData)
  );

  @Effect({ dispatch: false })
  removeLeadHeaderData$ = this.actions$.pipe(
    ofType<LeadHeaderDataRemoveAction>(LeadCreationStepperParamsActionTypes.LeadHeaderDataRemoveAction),
    tap(() =>
      this.appDataService.leadHeaderData = null
    )
  );

  @Effect()
  initLeadHeaderData$ = defer(() => {
    const leadHeaderData = this.appDataService.leadHeaderData;
    if (leadHeaderData) {
      return of(new LeadHeaderDataCreateAction({ leadHeaderData }));
    }
    else {
      return <any>of(new LeadHeaderDataRemoveAction());
    }
  });

  constructor(private actions$: Actions, private appDataService: AppDataService) {

  }

}
