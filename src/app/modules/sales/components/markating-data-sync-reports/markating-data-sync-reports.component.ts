import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CommonPaginationConfig, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CAMPAIGN_MANAGEMENT_COMPONENT } from '@modules/event-management/shared/enums/campaign-mngt-component';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-markating-data-sync-reports',
  templateUrl: './markating-data-sync-reports.component.html'
})
export class MarkatingDataSyncReportsComponent extends PrimeNgTableVariablesModel implements OnInit {
  constructor(private router: Router, private rxjsService: RxjsService,private snackbarService: SnackbarService,
    private crudService: CrudService, private store: Store<AppState>) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Marketing Data Sync Dashboard",
      breadCrumbItems: [{ displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Marketing', relativeRouterUrl: '' },
      { displayName: 'Marketing Data Sync Report', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Marketing Data Sync Dashboard',
            dataKey: 'marketingDataSyncMaintainanceId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'updateRef', header: 'Update Ref', width: '200px', nofilter: true },
            { field: 'marketingDataSyncMaintainanceUpdateTypeName', header: 'Update Type', width: '200px', shouldHideCalendar: true },
            { field: 'updateStart', header: 'Update Start', width: '200px', isDateTime: true },
            { field: 'updateEnd', header: 'Update End', width: '200px', isDateTime: true },
            { field: 'updateResult', header: 'Update Result', width: '200px', shouldHideCalendar: true },
            { field: 'errorDescription', header: 'Error Description', width: '200px' }],
            apiSuffixModel: SalesModuleApiSuffixModels.MARKETING_DATA_SYNC_REPORT,
            moduleName: ModulesBasedApiSuffix.SALES,
            enableAddActionBtn: false,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getMarketingDataList();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CAMPAIGN_MANAGEMENT_COMPONENT.MARKETING_DATA_SYNC_REPORT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }

    });

  }

  getMarketingDataList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let collectionModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      collectionModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex ? pageIndex : CommonPaginationConfig.defaultPageIndex, pageSize ? pageSize : CommonPaginationConfig.defaultPageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      if (data.isSuccess && data.statusCode == 200 && data.resources) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.totalRecords = 0;
      }
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getMarketingDataList(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  loadActionTypes(editableObject?: any, type?: CrudType) {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("sales/marketing-data/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["sales/marketing-data/view"], { queryParams: { id: editableObject['marketingDataSyncMaintainanceId'] } });
            break;
        }
    }
  }

  openAddEditPage(type: CrudType, editableObject?: object | string): void {
    this.loadActionTypes(editableObject, type);
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {});
    }
  }
}