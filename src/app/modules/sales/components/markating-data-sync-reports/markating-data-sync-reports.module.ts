import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@app/shared';
import { MarkatingDataSyncReportsComponent } from './markating-data-sync-reports.component';

const reportRoutes: Routes = [
  { path: '', component: MarkatingDataSyncReportsComponent, data: { title: 'Markating Data Sync Report' } },
];
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(reportRoutes),
  ],
  declarations: [MarkatingDataSyncReportsComponent]
})
export class MarkatingDataSyncReportsModule { }
