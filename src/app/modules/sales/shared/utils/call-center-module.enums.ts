enum CallCenterModuleApiSuffixModels {
    AGENTLOGIN = 'agent/login',
    OUTBOUND = 'agent/outbound',
}
export { CallCenterModuleApiSuffixModels };