enum RawLeadStatuses{
     New=1,
     Unallocated=2,
     Allocated=3,
     Follow_Up=4,
     Escalated=5,
     Archive=6,
     Lead_Created=7,
     Address_Verification_Pending=8
}

export {RawLeadStatuses}