enum TelephonyApiSuffixModules {
    AGENTLOGIN = 'agent/logon',
    OUTBOUND = 'agent/outbound',
    CUSTOMER_DIAL = 'agent/dial',
    DISCONNECT = 'agent/disconnect',
    AGENT_LOGOFF = 'agent/logoff',
    HOLD = 'agent/hold',
    SINGLE_STEP_TRANSFER = 'agent/single-step-transfer',
    PABX_AGENTS = 'pabx/agents',
    USERGROUP_AGENTS = 'agent-user-group',
    RECENT_CALL = 'agent/recent-calls',
    ANSWER = 'agent/answer',
    AGENT_UPDATE_CUSTOMER = 'agent/update-customer',
    LOGGEDIN_MEDIA = 'agent/loggedin-media',
    EMAIL_LOGIN = 'email/com/logon',
    EMAIL_LOGOUT = 'email/com/logoff',
    EAMIL_ROUTING_STATE = 'email/com/routing-state',
    EMAIL_QUEUE = 'pabx/queues',
    CREATE_CALLBACK = 'callback/create',
    LOGGED_AGENTS_LIST='agent/loggedin-extensions',
    RETRIEVE_SPECIFIC_AGENT_DETAIL='agent/loggedin-extension-details'
}

export { TelephonyApiSuffixModules };