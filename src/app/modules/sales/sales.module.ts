import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AngularEditorModule } from '@kolkov/angular-editor';
import { AssignedToModalComponent } from '@modules/sales/components/lead-notes/assigned-to-modal.component';
import { BookAppoinmentModalComponent } from '@modules/sales/components/lead-notes/book-appoinment-modal.component';
import { CustomerContactAddEditModalComponent } from '@modules/sales/components/lead-notes/customer-contact-edit-modal.component';
import { EmailModalComponent } from '@modules/sales/components/lead-notes/email-modal.component';
import { ScheduleCallbackAddEditModalComponent } from '@modules/sales/components/lead-notes/schedule-callback-add-edit-modal.component';
import { SMSModalComponent } from '@modules/sales/components/lead-notes/sms-modal.component';
import { SalesRoutingModule } from '@modules/sales/sales-routing.module';
import { SalesDashboardComponent } from '@sales/components/dashboard';
import { ChangeStatusModalComponent } from '@sales/components/raw-lead';
import { LayoutModule } from '../../shared/layouts/layout.module';
import { MaterialModule } from '../../shared/material.module';
import { SharedModule } from '../../shared/shared.module';
import { CustomerCallModalComponent, LeadNoteModalComponent } from './components/lead-notes';
@NgModule({
  declarations: [SalesDashboardComponent, ChangeStatusModalComponent,
    AssignedToModalComponent, BookAppoinmentModalComponent, CustomerContactAddEditModalComponent,
    EmailModalComponent, ScheduleCallbackAddEditModalComponent, SMSModalComponent,
   CustomerCallModalComponent,
    LeadNoteModalComponent
  ],
  imports: [
    CommonModule,
    SalesRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    AngularEditorModule
  ],
  entryComponents: [ChangeStatusModalComponent, AssignedToModalComponent, BookAppoinmentModalComponent, CustomerContactAddEditModalComponent,
    EmailModalComponent, ScheduleCallbackAddEditModalComponent, SMSModalComponent,
    CustomerCallModalComponent, LeadNoteModalComponent
  ]
})
export class SalesModule { }
