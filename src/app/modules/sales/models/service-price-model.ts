class ServicePriceModel {
  constructor(servicePriceManualAddEditModel?: ServicePriceModel) {
    this.servicePriceId = valueFiller(servicePriceManualAddEditModel, 'servicePriceId');
    this.districtId = valueFiller(servicePriceManualAddEditModel, 'districtId');
    this.itemOwnershipTypeId = valueFiller(servicePriceManualAddEditModel, 'itemOwnershipTypeId');
    this.tierId = valueFiller(servicePriceManualAddEditModel, 'tierId');
    this.servicePrice = valueFiller(servicePriceManualAddEditModel, 'servicePrice');
    this.taxId = valueFiller(servicePriceManualAddEditModel, 'taxId');
    this.totalPrice = valueFiller(servicePriceManualAddEditModel, 'totalPrice');
  }
  servicePriceId?: number;
  districtId?: string;
  itemOwnershipTypeId?: string;
  tierId?: string;
  servicePrice?: string;
  taxId?: string;
  totalPrice?: string;
}
class ServicePriceItemModel {
  constructor(servicePriceItemModel?: ServicePriceItemModel) {
    this.serviceId = valueFiller(servicePriceItemModel, 'serviceId');
    this.serviceNumber = valueFiller(servicePriceItemModel, 'serviceNumber');
    this.serviceName = valueFiller(servicePriceItemModel, 'serviceName');
    this.serviceMappingId = valueFiller(servicePriceItemModel, 'serviceMappingId');
    this.taxId = valueFiller(servicePriceItemModel, 'taxId');
    this.taxPercentage = valueFiller(servicePriceItemModel, 'taxPercentage');
    this.tiers = servicePriceItemModel && servicePriceItemModel.tiers.length ? servicePriceItemModel.tiers.map(k => new ServicePriceItemModelTier(k)) : [new ServicePriceItemModelTier(), new ServicePriceItemModelTier(), new ServicePriceItemModelTier()];
  }
  serviceId?: number;
  serviceName?: string;
  serviceNumber?: string;
  serviceMappingId?: number;
  taxId?: number;
  taxPercentage?: any;
  tiers: ServicePriceItemModelTier[];
}
class ServicePriceItemModelTier {
  constructor(servicePriceItemModelTier?: ServicePriceItemModelTier) {
    this.servicePriceId = valueFiller(servicePriceItemModelTier, 'servicePriceId');
    this.tierId = valueFiller(servicePriceItemModelTier, 'tierId');
    this.inclVAT = valueFiller(servicePriceItemModelTier, 'inclVAT');
    this.exclVAT = valueFiller(servicePriceItemModelTier, 'exclVAT');
  }
  servicePriceId?: number;
  tierId?: number;
  inclVAT?: any;
  exclVAT?: any;
}

const valueFiller = (data, key) => {
  return data ? data[key] == undefined ? '' : data[key] : '';
}

export { ServicePriceModel, ServicePriceItemModel, ServicePriceItemModelTier };