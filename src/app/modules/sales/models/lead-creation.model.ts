abstract class CommonModels {
    constructor(commonModels?: CommonModels) {
        this.leadId = commonModels ? commonModels.leadId == undefined ? '' : commonModels.leadId : '';
        this.createdUserId = commonModels ? commonModels.createdUserId == undefined ? '' : commonModels.createdUserId : '';
        this.modifiedUserId = commonModels ? commonModels.modifiedUserId == undefined ? '' : commonModels.modifiedUserId : '';
    }
    leadId?: string;
    createdUserId?: string;
    modifiedUserId?: string;
}
class LeadInfoModel extends CommonModels {
    constructor(leadInfoModel?: LeadInfoModel) {
        super(leadInfoModel);
        this.rawLeadId = leadInfoModel == undefined ? '' : leadInfoModel.rawLeadId == undefined ? '' : leadInfoModel.rawLeadId;
        this.basicInfo = leadInfoModel == undefined ? new BasicInfoModel() : leadInfoModel.basicInfo == undefined ? new BasicInfoModel() : leadInfoModel.basicInfo;
        this.contactInfo = leadInfoModel == undefined ? new ContactInfoModel() : leadInfoModel.contactInfo == undefined ? new ContactInfoModel() : leadInfoModel.contactInfo;
        this.addressInfo = leadInfoModel == undefined ? new AddressModel() : leadInfoModel.addressInfo == undefined ? new AddressModel() : leadInfoModel.addressInfo;
        this.leadCategoryId = leadInfoModel == undefined ? [] : leadInfoModel.leadCategoryId == undefined ? [] : leadInfoModel.leadCategoryId;
        this.sourceId = leadInfoModel == undefined ? null : leadInfoModel.sourceId == undefined ? null : leadInfoModel.sourceId;
        this.siteTypeId = leadInfoModel == undefined ? null : leadInfoModel.siteTypeId == undefined ? null : leadInfoModel.siteTypeId;
        this.customerId = leadInfoModel == undefined ? null : leadInfoModel.customerId == undefined ? null : leadInfoModel.customerId;
        this.leadGroupId = leadInfoModel == undefined ? null : leadInfoModel.leadGroupId == undefined ? null : leadInfoModel.leadGroupId;
        this.isNewCustomer = leadInfoModel == undefined ? true : leadInfoModel.isNewCustomer == undefined ? true : leadInfoModel.isNewCustomer;
        this.directSaleId = leadInfoModel == undefined ? '' : leadInfoModel.directSaleId == undefined ? '' : leadInfoModel.directSaleId;
        this.afrigisAddressInfo = leadInfoModel == undefined ? null : leadInfoModel.afrigisAddressInfo == undefined ? null : leadInfoModel.afrigisAddressInfo;
        this.leadValidationType = leadInfoModel == undefined ? null : leadInfoModel.leadValidationType == undefined ? null : leadInfoModel.leadValidationType;
        this.isReconnectionAddressConfirmation = leadInfoModel == undefined ? null : leadInfoModel.isReconnectionAddressConfirmation == undefined ? null : leadInfoModel.isReconnectionAddressConfirmation;
    }
    rawLeadId?: string;
    afrigisAddressInfo?: string;
    basicInfo?: BasicInfoModel;
    contactInfo?: ContactInfoModel;
    addressInfo?: AddressModel;
    leadCategoryId?: Array<number>;
    leadGroupId?: string;
    sourceId?: number;
    siteTypeId?: number;
    customerId?: string;
    isNewCustomer?: boolean;
    directSaleId?: string;
    leadValidationType?: string;
    isReconnectionAddressConfirmation?: boolean;
}
class ContactInfoModel {
    constructor(contactInfoModel?: ContactInfoModel) {
        this.contactId = contactInfoModel == undefined ? null : contactInfoModel.contactId == undefined ? null : contactInfoModel.contactId;
        this.email = contactInfoModel == undefined ? "" : contactInfoModel.email == undefined ? "" : contactInfoModel.email;
        this.mobile1 = contactInfoModel == undefined ? "" : contactInfoModel.mobile1 == undefined ? "" : contactInfoModel.mobile1;
        this.mobile1CountryCode = contactInfoModel ? contactInfoModel.mobile1CountryCode == undefined ? '+27' : contactInfoModel.mobile1CountryCode : '+27';
        this.mobile2 = contactInfoModel == undefined ? "" : contactInfoModel.mobile2 == undefined ? "" : contactInfoModel.mobile2;
        this.mobile2CountryCode = contactInfoModel ? contactInfoModel.mobile2CountryCode == undefined ? '+27' : contactInfoModel.mobile2CountryCode : '+27';
        this.officeNo = contactInfoModel == undefined ? "" : contactInfoModel.officeNo == undefined ? "" : contactInfoModel.officeNo;
        this.officeNoCountryCode = contactInfoModel ? contactInfoModel.officeNoCountryCode == undefined ? '+27' : contactInfoModel.officeNoCountryCode : '+27';
        this.premisesNo = contactInfoModel == undefined ? "" : contactInfoModel.premisesNo == undefined ? "" : contactInfoModel.premisesNo;
        this.premisesNoCountryCode = contactInfoModel ? contactInfoModel.premisesNoCountryCode == undefined ? '+27' : contactInfoModel.premisesNoCountryCode : '+27';
    }
    contactId?: string;
    email?: string;
    mobile1?: string;
    mobile1CountryCode?: string;
    mobile2?: string;
    mobile2CountryCode?: string;
    officeNo?: string;
    officeNoCountryCode?: string;
    premisesNo?: string;
    premisesNoCountryCode?: string;
}
class BasicInfoModel {
    constructor(basicInfoModel?: BasicInfoModel) {
        this.customerTypeId = basicInfoModel == undefined ? null : basicInfoModel.customerTypeId == undefined ? null : basicInfoModel.customerTypeId;
        this.titleId = basicInfoModel == undefined ? null : basicInfoModel.titleId == undefined ? null : basicInfoModel.titleId == 0 ? null : basicInfoModel.titleId;
        this.firstName = basicInfoModel == undefined ? "" : basicInfoModel.firstName == undefined ? "" : basicInfoModel.firstName;
        this.lastName = basicInfoModel == undefined ? "" : basicInfoModel.lastName == undefined ? "" : basicInfoModel.lastName;
        this.said = basicInfoModel == undefined ? null : basicInfoModel.said == undefined ? null : basicInfoModel.said;
        this.companyName = basicInfoModel == undefined ? "" : basicInfoModel.companyName == undefined ? "" : basicInfoModel.companyName;
        this.companyRegNo = basicInfoModel == undefined ? "" : basicInfoModel.companyRegNo == undefined ? "" : basicInfoModel.companyRegNo;
    }
    customerTypeId?: number;
    titleId?: number;
    firstName?: string;
    lastName?: string;
    said?: number;
    companyName?: string;
    companyRegNo?: string;
}
class AddressModel {
    constructor(addressModel?: AddressModel) {
        this.postalCode = addressModel == undefined ? "" : addressModel.postalCode == undefined ? "" : addressModel.postalCode;
        this.buildingNo = addressModel == undefined ? "" : addressModel.buildingNo == undefined ? "" : addressModel.buildingNo;
        this.buildingName = addressModel == undefined ? "" : addressModel.buildingName == undefined ? "" : addressModel.buildingName;
        this.streetName = addressModel == undefined ? "" : addressModel.streetName == undefined ? "" : addressModel.streetName;
        this.streetNo = addressModel == undefined ? "" : addressModel.streetNo == undefined ? "" : addressModel.streetNo;
        this.longitude = addressModel == undefined ? "" : addressModel.longitude == undefined ? "" : addressModel.longitude;
        this.latitude = addressModel == undefined ? "" : addressModel.latitude == undefined ? "" : addressModel.latitude;
        this.provinceName = addressModel == undefined ? "" : addressModel.provinceName == undefined ? "" : addressModel.provinceName;
        this.provinceId = addressModel == undefined ? "" : addressModel.provinceId == undefined ? "" : addressModel.provinceId;
        this.suburbName = addressModel == undefined ? "" : addressModel.suburbName == undefined ? "" : addressModel.suburbName;
        this.suburbId = addressModel == undefined ? "" : addressModel.suburbId == undefined ? "" : addressModel.suburbId;
        this.cityName = addressModel == undefined ? "" : addressModel.cityName == undefined ? "" : addressModel.cityName;
        this.cityId = addressModel == undefined ? "" : addressModel.cityId == undefined ? "" : addressModel.cityId;
        this.estateStreetNo = addressModel == undefined ? "" : addressModel.estateStreetNo == undefined ? "" : addressModel.estateStreetNo;
        this.estateStreetName = addressModel == undefined ? "" : addressModel.estateStreetName == undefined ? "" : addressModel.estateStreetName;
        this.estateName = addressModel == undefined ? "" : addressModel.estateName == undefined ? "" : addressModel.estateName;
        this.formatedAddress = addressModel == undefined ? "" : addressModel.formatedAddress == undefined ? "" : addressModel.formatedAddress;
        this.seoid = addressModel == undefined ? "" : addressModel.seoid == undefined ? "" : addressModel.seoid;
        this.addressConfidentLevel = addressModel == undefined ? null : addressModel.addressConfidentLevel == undefined ? null : addressModel.addressConfidentLevel;
        this.addressConfidentLevelId = addressModel == undefined ? null : addressModel.addressConfidentLevelId == undefined ? null : addressModel.addressConfidentLevelId;
        this.latLong = addressModel == undefined ? "" : addressModel.latLong == undefined ? "" : addressModel.latLong;
        this.addressId = addressModel == undefined ? "" : addressModel.addressId == undefined ? "" : addressModel.addressId;
        this.IsLSSScheme = addressModel == undefined ? false : addressModel.IsLSSScheme == undefined ? false : addressModel.IsLSSScheme;
        this.jsonObject = addressModel == undefined ? "" : addressModel.jsonObject == undefined ? "" : addressModel.jsonObject;
        this.isAddressComplex = addressModel == undefined ? false : addressModel.isAddressComplex == undefined ? false : addressModel.isAddressComplex;
        this.isAddressExtra = addressModel == undefined ? false : addressModel.isAddressExtra == undefined ? false : addressModel.isAddressExtra;
        this.isAfrigisSearch = addressModel == undefined ? false : addressModel.isAfrigisSearch == undefined ? false : addressModel.isAfrigisSearch;
        this.isNewAddress = addressModel == undefined ? false : addressModel.isNewAddress == undefined ? false : addressModel.isNewAddress;
        this.afriGISAddressId = addressModel == undefined ? "" : addressModel.afriGISAddressId == undefined ? "" : addressModel.afriGISAddressId;
    }
    postalCode?: string;
    buildingNo?: string;
    buildingName?: string;
    streetNo?: string;
    streetName?: string;
    longitude?: string;
    latitude?: string;
    provinceName?: string;
    provinceId?: string;
    suburbName?: string;
    suburbId?: string;
    cityName?: string;
    cityId?: string;
    estateStreetNo?: string;
    estateStreetName?: string;
    estateName?: string;
    formatedAddress?: string;
    seoid?: string;
    addressConfidentLevel?: string;
    addressConfidentLevelId?: string;
    IsLSSScheme?: boolean;
    jsonObject?: string;
    latLong?: string;
    addressId?: string;
    isAddressComplex?: boolean;
    isAfrigisSearch?: boolean;
    isAddressExtra?: boolean;
    isNewAddress?: boolean;
    afriGISAddressId?: string;
}
class NewAddressModel {
    constructor(newAddressModel?: NewAddressModel) {
        this.formatedAddress = newAddressModel == undefined ? null : newAddressModel.formatedAddress == undefined ? null : newAddressModel.formatedAddress;
        this.buildingNo = newAddressModel == undefined ? null : newAddressModel.buildingNo == undefined ? null : newAddressModel.buildingNo;
        this.buildingName = newAddressModel == undefined ? null : newAddressModel.buildingName == undefined ? null : newAddressModel.buildingName;
        this.streetNo = newAddressModel == undefined ? null : newAddressModel.streetNo == undefined ? null : newAddressModel.streetNo;
        this.streetName = newAddressModel == undefined ? null : newAddressModel.streetName == undefined ? null : newAddressModel.streetName;
        this.provinceId = newAddressModel == undefined ? null : newAddressModel.provinceId == undefined ? null : newAddressModel.provinceId;
        this.province = newAddressModel == undefined ? '' : newAddressModel.province == undefined ? '' : newAddressModel.province;
        this.cityId = newAddressModel == undefined ? null : newAddressModel.cityId == undefined ? null : newAddressModel.cityId;
        this.city = newAddressModel == undefined ? null : newAddressModel.city == undefined ? null : newAddressModel.city;
        this.suburbId = newAddressModel == undefined ? null : newAddressModel.suburbId == undefined ? null : newAddressModel.suburbId;
        this.suburb = newAddressModel == undefined ? null : newAddressModel.suburb == undefined ? null : newAddressModel.suburb;
        this.reason = newAddressModel == undefined ? null : newAddressModel.reason == undefined ? null : newAddressModel.reason;
        this.postalCode = newAddressModel == undefined ? null : newAddressModel.postalCode == undefined ? null : newAddressModel.postalCode;
        this.isAddressType = newAddressModel == undefined ? true : newAddressModel.isAddressType == undefined ? true : newAddressModel.isAddressType;
        this.estateName = newAddressModel == undefined ? null : newAddressModel.estateName == undefined ? null : newAddressModel.estateName;
        this.estateStreetNo = newAddressModel == undefined ? null : newAddressModel.estateStreetNo == undefined ? null : newAddressModel.estateStreetNo;
        this.estateStreetName = newAddressModel == undefined ? null : newAddressModel.estateStreetName == undefined ? null : newAddressModel.estateStreetName;
        this.addressId = newAddressModel == undefined ? null : newAddressModel.addressId == undefined ? null : newAddressModel.addressId;
        this.addressConfidentLevelName = newAddressModel == undefined ? null : newAddressModel.addressConfidentLevelName == undefined ? null : newAddressModel.addressConfidentLevelName;
        this.jsonObject = newAddressModel == undefined ? null : newAddressModel.jsonObject == undefined ? null : newAddressModel.jsonObject;
        this.seoId = newAddressModel == undefined ? null : newAddressModel.seoId == undefined ? null : newAddressModel.seoId;
        this.addressConfidentLevelId = newAddressModel == undefined ? 0 : newAddressModel.addressConfidentLevelId == undefined ? 0 : newAddressModel.addressConfidentLevelId;
        this.isAddressComplex = newAddressModel == undefined ? true : newAddressModel.isAddressComplex == undefined ? true : newAddressModel.isAddressComplex;
        this.isAddressExtra = newAddressModel == undefined ? true : newAddressModel.isAddressExtra == undefined ? true : newAddressModel.isAddressExtra;
    }
    formatedAddress?: string;
    buildingNo?: string;
    buildingName?: string;
    streetNo?: string;
    streetName?: string;
    provinceId?: string;
    province?: string;
    cityId?: string;
    city?: string;
    suburbId?: string;
    suburb?: string;
    reason?: string;
    postalCode?: number;
    isAddressType?: boolean;
    estateName?: string;
    estateStreetNo?: string;
    estateStreetName?: string;
    addressId?: string;
    addressConfidentLevelName?: string;
    jsonObject?: string;
    seoId?: string;
    addressConfidentLevelId?: number;
    isAddressExtra?: boolean;
    isAddressComplex?: boolean;
}
class ServiceInfoModel extends CommonModels {
    constructor(serviceInfoModel?: ServiceInfoModel) {
        super(serviceInfoModel);
        this.leadServiceId = serviceInfoModel == undefined ? "" : serviceInfoModel.leadServiceId == undefined ? "" : serviceInfoModel.leadServiceId;
        this.servicePriceId = serviceInfoModel == undefined ? "" : serviceInfoModel.servicePriceId == undefined ? "" : serviceInfoModel.servicePriceId;
        this.districtId = serviceInfoModel == undefined ? "" : serviceInfoModel.districtId == undefined ? "" : serviceInfoModel.districtId;
        this.tierId = serviceInfoModel == undefined ? 0 : serviceInfoModel.tierId == undefined ? 0 : serviceInfoModel.tierId;
        this.serviceCategoryId = serviceInfoModel == undefined ? 0 : serviceInfoModel.serviceCategoryId == undefined ? 0 : serviceInfoModel.serviceCategoryId;
        this.serviceId = serviceInfoModel == undefined ? 0 : serviceInfoModel.serviceId == undefined ? 0 : serviceInfoModel.serviceId;
        this.siteTypeId = serviceInfoModel == undefined ? "" : serviceInfoModel.siteTypeId == undefined ? "" : serviceInfoModel.siteTypeId;
        this.itemOwnershipTypeId = serviceInfoModel == undefined ? 0 : serviceInfoModel.itemOwnershipTypeId == undefined ? 0 : serviceInfoModel.itemOwnershipTypeId;
        this.qty = serviceInfoModel == undefined ? 0 : serviceInfoModel.qty == undefined ? 0 : serviceInfoModel.qty;
        this.taxPercentage = serviceInfoModel == undefined ? 0 : serviceInfoModel.taxPercentage == undefined ? 0 : serviceInfoModel.taxPercentage;
        this.servicePrice = serviceInfoModel == undefined ? 0 : serviceInfoModel.servicePrice == undefined ? 0 : serviceInfoModel.servicePrice;
        this.taxPrice = serviceInfoModel == undefined ? 0 : serviceInfoModel.taxPrice == undefined ? 0 : serviceInfoModel.taxPrice;
        this.totalPrice = serviceInfoModel == undefined ? 0 : serviceInfoModel.totalPrice == undefined ? 0 : serviceInfoModel.totalPrice;
    }
    leadServiceId = "";
    servicePriceId = "";
    districtId = "";
    tierId = 0;
    serviceCategoryId = 0;
    serviceId = 0;
    siteTypeId = "";
    itemOwnershipTypeId = 1;
    qty = 0;
    taxPercentage = 0;
    servicePrice = 0;
    taxPrice = 0;
    totalPrice = 0;
}
class ItemInfoModel extends CommonModels {
    constructor(itemInfoModel?: ItemInfoModel) {
        super(itemInfoModel);
        this.leadItemId = itemInfoModel == undefined ? "" : itemInfoModel.leadItemId == undefined ? "" : itemInfoModel.leadItemId;
        this.itemCode = itemInfoModel == undefined ? "" : itemInfoModel.itemCode == undefined ? "" : itemInfoModel.itemCode;
        this.itemId = itemInfoModel == undefined ? "" : itemInfoModel.itemId == undefined ? "" : itemInfoModel.itemId;
        this.itemName = itemInfoModel == undefined ? "" : itemInfoModel.itemName == undefined ? "" : itemInfoModel.itemName;
        this.unitPrice = itemInfoModel == undefined ? 0 : itemInfoModel.unitPrice == undefined ? 0 : itemInfoModel.unitPrice;
        this.qty = itemInfoModel == undefined ? 0 : itemInfoModel.qty == undefined ? 0 : itemInfoModel.qty;
        this.itemPrice = itemInfoModel == undefined ? 0 : itemInfoModel.itemPrice == undefined ? 0 : itemInfoModel.itemPrice;
        this.unitTaxPrice = itemInfoModel == undefined ? 0 : itemInfoModel.unitTaxPrice == undefined ? 0 : itemInfoModel.unitTaxPrice;
        this.taxPrice = itemInfoModel == undefined ? 0 : itemInfoModel.taxPrice == undefined ? 0 : itemInfoModel.taxPrice;
        this.totalPrice = itemInfoModel == undefined ? 0 : itemInfoModel.totalPrice == undefined ? 0 : itemInfoModel.totalPrice;
        this.vat = itemInfoModel == undefined ? 0 : itemInfoModel.vat == undefined ? 0 : itemInfoModel.vat;
        this.total = itemInfoModel == undefined ? 0 : itemInfoModel.total == undefined ? 0 : itemInfoModel.total;
        this.subTotal = itemInfoModel == undefined ? 0 : itemInfoModel.subTotal == undefined ? 0 : itemInfoModel.subTotal;
    }
    leadItemId: string;
    itemCode: string;
    itemId: string;
    itemName: string;
    unitPrice: number;
    qty: number;
    itemPrice: number;
    taxPrice: number;
    unitTaxPrice: number;
    totalPrice: number;
    vat: number;
    total: number;
    subTotal: number;
}
class ItemInfoFormModel {
    constructor(itemInfoFormModel?: ItemInfoFormModel) {
        this.systemTypeId = itemInfoFormModel == undefined ? "" : itemInfoFormModel.systemTypeId == undefined ? "" : itemInfoFormModel.systemTypeId;
        this.structureTypeId = itemInfoFormModel == undefined ? "" : itemInfoFormModel.structureTypeId == undefined ? "" : itemInfoFormModel.structureTypeId;
        this.levyConfigId = itemInfoFormModel == undefined ? "" : itemInfoFormModel.levyConfigId == undefined ? "" : itemInfoFormModel.levyConfigId;
        this.search = itemInfoFormModel == undefined ? "" : itemInfoFormModel.search == undefined ? "" : itemInfoFormModel.search;
    }
    systemTypeId: string;
    search: string;
    structureTypeId: string;
    levyConfigId: string;
}
class TeleSalesOrderFormModel extends CommonModels {
    constructor(teleSalesOrderFormModel?: TeleSalesOrderFormModel) {
        super(teleSalesOrderFormModel);
        this.agreementTypeId = teleSalesOrderFormModel == undefined ? null : teleSalesOrderFormModel.agreementTypeId == undefined ? null : teleSalesOrderFormModel.agreementTypeId;
        this.agreementId = teleSalesOrderFormModel == undefined ? null : teleSalesOrderFormModel.agreementId == undefined ? null : teleSalesOrderFormModel.agreementId;
        this.agreementTypeName = teleSalesOrderFormModel == undefined ? null : teleSalesOrderFormModel.agreementTypeName == undefined ? null : teleSalesOrderFormModel.agreementTypeName;
        this.agreementOutcomeId = teleSalesOrderFormModel == undefined ? null : teleSalesOrderFormModel.agreementOutcomeId == undefined ? null : teleSalesOrderFormModel.agreementOutcomeId;
        this.description = teleSalesOrderFormModel == undefined ? "" : teleSalesOrderFormModel.description == undefined ? "" : teleSalesOrderFormModel.description;
        this.agreementOutcomeReasonId = teleSalesOrderFormModel == undefined ? null : teleSalesOrderFormModel.agreementOutcomeReasonId == undefined ? "" : teleSalesOrderFormModel.agreementOutcomeReasonId;
        this.isAgreementApproved = teleSalesOrderFormModel == undefined ? false : teleSalesOrderFormModel.isAgreementApproved == undefined ? false : teleSalesOrderFormModel.isAgreementApproved;
        this.salesOrderNumber = teleSalesOrderFormModel == undefined ? "" : teleSalesOrderFormModel.salesOrderNumber == undefined ? "" : teleSalesOrderFormModel.salesOrderNumber;
    }
    agreementTypeId: string;
    agreementId: string;
    agreementTypeName: string;
    agreementOutcomeId: string;
    description: string;
    agreementOutcomeReasonId: string;
    isAgreementApproved: boolean;
    salesOrderNumber: string;
}
class AllPartitionsModel {
    constructor(allPartitionsModel?: AllPartitionsModel) {
        this.isChecked = allPartitionsModel == undefined ? false : allPartitionsModel.isChecked == undefined ? false : allPartitionsModel.isChecked;
        this.partition = allPartitionsModel == undefined ? "" : allPartitionsModel.partition == undefined ? "" : allPartitionsModel.partition;
        this.partitionCode = allPartitionsModel == undefined ? "" : allPartitionsModel.partitionCode == undefined ? "" : allPartitionsModel.partitionCode;
        this.partitionName = allPartitionsModel == undefined ? "" : allPartitionsModel.partitionName == undefined ? "" : allPartitionsModel.partitionName;
        this.partitionId = allPartitionsModel == undefined ? null : allPartitionsModel.partitionId == undefined ? null : allPartitionsModel.partitionId;
        this.primaryLeadId = allPartitionsModel == undefined ? null : allPartitionsModel.primaryLeadId == undefined ? null : allPartitionsModel.primaryLeadId;
        this.customerId = allPartitionsModel == undefined ? null : allPartitionsModel.customerId == undefined ? null : allPartitionsModel.customerId;
        this.partitionLeadId = allPartitionsModel == undefined ? null : allPartitionsModel.partitionLeadId == undefined ? null : allPartitionsModel.partitionLeadId;
        this.isPrimary = allPartitionsModel == undefined ? false : allPartitionsModel.isPrimary == undefined ? false : allPartitionsModel.isPrimary;
        this.index = allPartitionsModel == undefined ? 0 : allPartitionsModel.index == undefined ? 0 : allPartitionsModel.index;
        this.currentCursorIndex = allPartitionsModel == undefined ? 0 : allPartitionsModel.currentCursorIndex == undefined ? 0 : allPartitionsModel.currentCursorIndex;
        this.createdUserId = allPartitionsModel == undefined ? null : allPartitionsModel.createdUserId == undefined ? null : allPartitionsModel.createdUserId;
    }
    isChecked: boolean;
    partitionId: string;
    primaryLeadId: string;
    partitionLeadId: string;
    customerId: string;
    partition: string;
    partitionCode: string
    partitionName: string;
    isPrimary: boolean;
    createdUserId: string;
    index: number;
    currentCursorIndex: number;
}
class QuotationConfirmModel {
    constructor(quotationConfirmModel?: QuotationConfirmModel) {
        this.quotationVersionId = quotationConfirmModel == undefined ? "" : quotationConfirmModel.quotationVersionId == undefined ? "" : quotationConfirmModel.quotationVersionId;
        this.upfrontPaymentConfigId = quotationConfirmModel == undefined ? "" : quotationConfirmModel.upfrontPaymentConfigId == undefined ? "" : quotationConfirmModel.upfrontPaymentConfigId;
        this.isAdminandRLFPayment = quotationConfirmModel == undefined ? true : quotationConfirmModel.isAdminandRLFPayment == undefined ? true : quotationConfirmModel.isAdminandRLFPayment;
        this.modifiedUserId = quotationConfirmModel == undefined ? "" : quotationConfirmModel.modifiedUserId == undefined ? "" : quotationConfirmModel.modifiedUserId;
    }
    quotationVersionId: string;
    upfrontPaymentConfigId: string
    isAdminandRLFPayment: boolean
    modifiedUserId: string;
}
class QuotationCreationFormModel extends CommonModels {
    constructor(quotationCreationFormModel?: QuotationCreationFormModel) {
        super(quotationCreationFormModel);
        this.quotationId = quotationCreationFormModel == undefined ? "" : quotationCreationFormModel.quotationId == undefined ? "" : quotationCreationFormModel.quotationId;
        this.expiryDate = quotationCreationFormModel == undefined ? "" : quotationCreationFormModel.expiryDate == undefined ? "" : quotationCreationFormModel.expiryDate;
        this.isPartitionAvailability = quotationCreationFormModel == undefined ? false : quotationCreationFormModel.isPartitionAvailability == undefined ? false : quotationCreationFormModel.isPartitionAvailability;
        this.contractPeriodId = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.contractPeriodId == undefined ? null : quotationCreationFormModel.contractPeriodId;
        this.billingIntervalId = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.billingIntervalId == undefined ? null : quotationCreationFormModel.billingIntervalId;
        this.buildingStructureTypeId = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.buildingStructureTypeId == undefined ? null : quotationCreationFormModel.buildingStructureTypeId;
        this.roofTypeId = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.roofTypeId == undefined ? null : quotationCreationFormModel.roofTypeId;
        this.isPartnershipRewards = quotationCreationFormModel == undefined ? false : quotationCreationFormModel.isPartnershipRewards == undefined ? false : quotationCreationFormModel.isPartnershipRewards;
        this.levyConfigId = quotationCreationFormModel == undefined ? "" : quotationCreationFormModel.levyConfigId == undefined ? "" : quotationCreationFormModel.levyConfigId;
        this.concessionPercentage = quotationCreationFormModel == undefined ? 0 : quotationCreationFormModel.concessionPercentage == undefined ? 0 : quotationCreationFormModel.concessionPercentage;
        this.concessionStatus = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.concessionStatus == undefined ? null : quotationCreationFormModel.concessionStatus;
        this.quotationConcessionId = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.quotationConcessionId == undefined ? null : quotationCreationFormModel.quotationConcessionId;
        this.quotationConcession = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.quotationConcession == undefined ? null : quotationCreationFormModel.quotationConcession;
        this.quotationItems = quotationCreationFormModel == undefined ? [] : quotationCreationFormModel.quotationItems == undefined ? [] : quotationCreationFormModel.quotationItems;
        this.quotationServices = quotationCreationFormModel == undefined ? [] : quotationCreationFormModel.quotationServices == undefined ? [] : quotationCreationFormModel.quotationServices;
        this.quotationVersions = quotationCreationFormModel == undefined ? [] : quotationCreationFormModel.quotationVersions == undefined ? [] : quotationCreationFormModel.quotationVersions;
        this.itemRewardDiscounts = quotationCreationFormModel == undefined ? [] : quotationCreationFormModel.itemRewardDiscounts == undefined ? [] : quotationCreationFormModel.itemRewardDiscounts;
        this.serviceRewardDiscounts = quotationCreationFormModel == undefined ? [] : quotationCreationFormModel.serviceRewardDiscounts == undefined ? [] : quotationCreationFormModel.serviceRewardDiscounts;
        this.concessionTypeId = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.concessionTypeId == undefined ? null : quotationCreationFormModel.concessionTypeId;
        this.levyAmount = quotationCreationFormModel == undefined ? "0.00" : quotationCreationFormModel.levyAmount == undefined ? "0.00" : quotationCreationFormModel.levyAmount;
        this.productAmount = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.productAmount == undefined ? null : quotationCreationFormModel.productAmount;
        this.concessionAmount = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.concessionAmount == undefined ? null : quotationCreationFormModel.concessionAmount;
        this.grandTotalAmount = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.grandTotalAmount == undefined ? null : quotationCreationFormModel.grandTotalAmount;
        this.recurringServiceFee = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.recurringServiceFee == undefined ? null : quotationCreationFormModel.recurringServiceFee;
        this.productAndInstallationSubTotal = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.productAndInstallationSubTotal == undefined ? null : quotationCreationFormModel.productAndInstallationSubTotal;
        this.recurringServiceFeeGrandTotal = quotationCreationFormModel == undefined ? "0.00" : quotationCreationFormModel.recurringServiceFeeGrandTotal == undefined ? "0.00" : quotationCreationFormModel.recurringServiceFeeGrandTotal;
        this.productAndInstallationGrandTotal = quotationCreationFormModel == undefined ? "0.00" : quotationCreationFormModel.productAndInstallationGrandTotal == undefined ? "0.00" : quotationCreationFormModel.productAndInstallationGrandTotal;
        this.isPrimaryPartitionType = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.isPrimaryPartitionType == undefined ? null : quotationCreationFormModel.isPrimaryPartitionType;
        this.partitionId = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.partitionId == undefined ? null : quotationCreationFormModel.partitionId;
        this.noOfPartition = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.noOfPartition == undefined ? null : quotationCreationFormModel.noOfPartition;
        this.partitionPaymentTypeId = quotationCreationFormModel == undefined ? null : quotationCreationFormModel.partitionPaymentTypeId == undefined ? null : quotationCreationFormModel.partitionPaymentTypeId;
    }
    quotationId: string;
    expiryDate: Date | string;
    isPartitionAvailability: boolean;
    contractPeriodId: string;
    billingIntervalId: string;
    buildingStructureTypeId: string;
    roofTypeId: string;
    productAmount: string;
    concessionAmount: string;
    grandTotalAmount: string;
    levyConfigId: string;
    levyAmount: string;
    quotationConcessionId: string;
    quotationConcession: QuotationConcession;
    quotationItems = [];
    quotationServices = [];
    quotationVersions: [];
    itemRewardDiscounts: [];
    serviceRewardDiscounts = [];
    concessionStatus: string;
    concessionPercentage: number;
    recurringServiceFee: string;
    concessionTypeId: string;
    productAndInstallationSubTotal: string;
    recurringServiceFeeGrandTotal: string;
    productAndInstallationGrandTotal: string;
    isPrimaryPartitionType: boolean;
    partitionPaymentTypeId: number;
    noOfPartition: number;
    partitionId?: string;
    isPartnershipRewards: boolean;
}
class QuotationConcession {
    constructor(quotationConcession?: QuotationConcession) {
        this.concessionTypeId = quotationConcession == undefined ? null : quotationConcession.concessionTypeId == undefined ? null :
            quotationConcession.concessionTypeId;
        this.concessionMotivationId = quotationConcession == undefined ? null : quotationConcession.concessionMotivationId == undefined ? null :
            quotationConcession.concessionMotivationId;
        this.concessionPercentage = quotationConcession == undefined ? 0 : quotationConcession.concessionPercentage == undefined ? 0 : quotationConcession.concessionPercentage;
        this.description = quotationConcession == undefined ? "" : quotationConcession.description == undefined ? "" :
            quotationConcession.description;
        this.roleId = quotationConcession == undefined ? null : quotationConcession.roleId == undefined ? null :
            quotationConcession.roleId;
        this.validityPeriod = quotationConcession == undefined ? null : quotationConcession.validityPeriod == undefined ? null :
            quotationConcession.validityPeriod;
    }
    concessionTypeId?: number;
    concessionMotivationId?: number;
    concessionPercentage?: number;
    description?: string;
    roleId?: string;
    validityPeriod: string;
}
class TechnicalAppointmentFormModel {
    constructor(technicalAppointmentFormModel?: TechnicalAppointmentFormModel) {
        this.customerId = technicalAppointmentFormModel == undefined ? null : technicalAppointmentFormModel.customerId == undefined ? null : technicalAppointmentFormModel.customerId;
        this.saleOrderId = technicalAppointmentFormModel == undefined ? null : technicalAppointmentFormModel.saleOrderId == undefined ? null : technicalAppointmentFormModel.saleOrderId;
        this.saleOrderRefNo = technicalAppointmentFormModel == undefined ? null : technicalAppointmentFormModel.saleOrderRefNo == undefined ? null : technicalAppointmentFormModel.saleOrderRefNo;
        this.appointmentRefNo = technicalAppointmentFormModel == undefined ? '' : technicalAppointmentFormModel.appointmentRefNo == undefined ? '' : technicalAppointmentFormModel.appointmentRefNo;
        this.serviceCallNo = technicalAppointmentFormModel == undefined ? '' : technicalAppointmentFormModel.serviceCallNo == undefined ? '' : technicalAppointmentFormModel.serviceCallNo;
        this.technicianId = technicalAppointmentFormModel == undefined ? '' : technicalAppointmentFormModel.technicianId == undefined ? '' : technicalAppointmentFormModel.technicianId;
        this.appointmentId = technicalAppointmentFormModel == undefined ? null : technicalAppointmentFormModel.appointmentId == undefined ? null : technicalAppointmentFormModel.appointmentId;
        this.quotationversionId = technicalAppointmentFormModel == undefined ? null : technicalAppointmentFormModel.quotationversionId == undefined ? null : technicalAppointmentFormModel.quotationversionId;
        this.quotationNumber = technicalAppointmentFormModel == undefined ? null : technicalAppointmentFormModel.quotationNumber == undefined ? null : technicalAppointmentFormModel.quotationNumber;
        this.techinicianAreaManager = technicalAppointmentFormModel == undefined ? '-' : technicalAppointmentFormModel.techinicianAreaManager == undefined ? '-' : technicalAppointmentFormModel.techinicianAreaManager;
        this.technicalArea = technicalAppointmentFormModel == undefined ? '-' : technicalAppointmentFormModel.technicalArea == undefined ? '-' : technicalAppointmentFormModel.technicalArea;
        this.createdUserId = technicalAppointmentFormModel == undefined ? null : technicalAppointmentFormModel.createdUserId == undefined ? null : technicalAppointmentFormModel.createdUserId;
        this.appointmentDate = technicalAppointmentFormModel == undefined ? null : technicalAppointmentFormModel.appointmentDate == undefined ? null : technicalAppointmentFormModel.appointmentDate;
        this.preferredDate = technicalAppointmentFormModel == undefined ? null : technicalAppointmentFormModel.preferredDate == undefined ? null : technicalAppointmentFormModel.preferredDate;
        this.comments = technicalAppointmentFormModel == undefined ? null : technicalAppointmentFormModel.comments == undefined ? null : technicalAppointmentFormModel.comments;
        this.isSpecialAppointment = technicalAppointmentFormModel == undefined ? false : technicalAppointmentFormModel.isSpecialAppointment == undefined ? false : technicalAppointmentFormModel.isSpecialAppointment;
        this.isUrgent = technicalAppointmentFormModel == undefined ? null : technicalAppointmentFormModel.isUrgent == undefined ? null : technicalAppointmentFormModel.isUrgent;
        this.isCode50 = technicalAppointmentFormModel == undefined ? null : technicalAppointmentFormModel.isCode50 == undefined ? null : technicalAppointmentFormModel.isCode50;
        this.specialAppointmentDate = technicalAppointmentFormModel == undefined ? null : technicalAppointmentFormModel.specialAppointmentDate == undefined ? null : technicalAppointmentFormModel.specialAppointmentDate;
    }
    customerId: string;
    saleOrderId: string;
    saleOrderRefNo: string;
    quotationversionId: string;
    quotationNumber: string;
    techinicianAreaManager: string;
    technicalArea: string;
    isSpecialAppointment: boolean;
    isUrgent: boolean;
    isCode50: boolean;
    createdUserId: string;
    appointmentDate: string;
    preferredDate: string;
    comments: string;
    appointmentRefNo: string;
    serviceCallNo: string;
    appointmentId: string;
    technicianId: string;
    specialAppointmentDate: string;
}

export {
    LeadInfoModel, AddressModel, ContactInfoModel, BasicInfoModel,
    ServiceInfoModel, ItemInfoModel, ItemInfoFormModel, QuotationCreationFormModel,
    QuotationConcession, TeleSalesOrderFormModel,
    QuotationConfirmModel, AllPartitionsModel, NewAddressModel, TechnicalAppointmentFormModel
};
