class ScheduleCallBackModel {
    constructor(scheduleCallBackModel?: ScheduleCallBackModel) {
        this.leadCallBackId = scheduleCallBackModel == undefined ? null : scheduleCallBackModel.leadCallBackId == undefined ? null : scheduleCallBackModel.leadCallBackId;
        this.createdUserId = scheduleCallBackModel == undefined ? null : scheduleCallBackModel.createdUserId == undefined ? null : scheduleCallBackModel.createdUserId;
    }
    leadCallBackId: string;
    createdUserId: string;
}

export { ScheduleCallBackModel };