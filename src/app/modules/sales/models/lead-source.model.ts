class LeadSource {
    constructor(LeadSource?: LeadSource) {
        this.leadSourceId = LeadSource ? LeadSource.leadSourceId == undefined ? '' : LeadSource.leadSourceId : '';
        this.leadSourceName = LeadSource ? LeadSource.leadSourceName == undefined ? '' : LeadSource.leadSourceName : '';
        this.sourceCode = LeadSource ? LeadSource.sourceCode == undefined ? '' : LeadSource.sourceCode : '';
        this.description = LeadSource ? LeadSource.description == undefined ? '' : LeadSource.description : '';
    }
    leadSourceId?: any;
    leadSourceName?: string;
    sourceCode?: string;
    description?: string;
}
export { LeadSource };