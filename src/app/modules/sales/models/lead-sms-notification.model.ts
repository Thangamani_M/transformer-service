import { defaultCountryCode, DEFAULT_EXTENSION_NUMBER } from "@app/shared";
class LeadSMSNotificationtModel {
    constructor(leadNotification?: LeadSMSNotificationtModel) {
        this.leadId = leadNotification ? leadNotification.leadId == undefined ? null : leadNotification.leadId : null;
        this.notificationTo = leadNotification ? leadNotification.notificationTo == undefined ? '' : leadNotification.notificationTo : '';
        this.notificationText = leadNotification ? leadNotification.notificationText == undefined ? '' : leadNotification.notificationText : '';
        this.createdUserId = leadNotification ? leadNotification.createdUserId == undefined ? '' : leadNotification.createdUserId : '';
        this.notificationToCountryCode = leadNotification ? leadNotification.notificationToCountryCode == undefined ? '' : leadNotification.notificationToCountryCode : '';
    }
    leadId?: string;
    notificationTo?: string;
    notificationText?: string;
    createdUserId?: string;
    notificationToCountryCode?: string;
}
class ScheduleCallBackViewModel {
    constructor(scheduleCallback?: ScheduleCallBackViewModel | any) {
        this.leadCallbackId = scheduleCallback == undefined ? '' : scheduleCallback.leadCallbackId == undefined ? '' : scheduleCallback.leadCallbackId;
        this.customerCallbackId = scheduleCallback == undefined ? '' : scheduleCallback.customerCallbackId == undefined ? '' : scheduleCallback.customerCallbackId;
        this.callbackDateTime = scheduleCallback == undefined ? '' : scheduleCallback.callbackDateTime == undefined ? '' : scheduleCallback.callbackDateTime;
        this.assignType = scheduleCallback == undefined ? '' : scheduleCallback.assignType == undefined ? '' : scheduleCallback.assignType;
        this.customerCallbackNumber = scheduleCallback == undefined ? '' : scheduleCallback.customerCallbackNumber == undefined ? '' : scheduleCallback.customerCallbackNumber;
        this.callbackQueueName = scheduleCallback == undefined ? '' : scheduleCallback.callbackQueueName == undefined ? '' : scheduleCallback.callbackQueueName;
        this.customerName = scheduleCallback == undefined ? '' : scheduleCallback.customerName == undefined ? '' : scheduleCallback.customerName;
        this.comments = scheduleCallback == undefined ? '' : scheduleCallback.comments == undefined ? '' : scheduleCallback.comments;
        this.createdUserId = scheduleCallback == undefined ? '' : scheduleCallback.createdUserId == undefined ? '' : scheduleCallback.createdUserId;
        this.leadRefNo = scheduleCallback == undefined ? '' : scheduleCallback.leadRefNo == undefined ? '' : scheduleCallback.leadRefNo;
        this.customerRefNo = scheduleCallback == undefined ? '' : scheduleCallback.customerRefNo == undefined ? '' : scheduleCallback.customerRefNo;
        this.leadName = scheduleCallback == undefined ? '' : scheduleCallback.leadName == undefined ? '' : scheduleCallback.leadName;
        this.contactNumber = scheduleCallback == undefined ? '' : scheduleCallback.contactNumber == undefined ? '' : scheduleCallback.contactNumber;
        this.contactNumberCountryCode = scheduleCallback == undefined ? defaultCountryCode : scheduleCallback.contactNumberCountryCode == undefined ? defaultCountryCode : scheduleCallback.contactNumberCountryCode;
        this.callbackStatusName = scheduleCallback == undefined ? '' : scheduleCallback.callbackStatusName == undefined ? '' : scheduleCallback.callbackStatusName;
        this.fromModule = scheduleCallback == undefined ? 'Lead' : scheduleCallback.fromModule == undefined ? 'Lead' : scheduleCallback.fromModule;
        this.callbackStatusId = scheduleCallback == undefined ? null : scheduleCallback.callbackStatusId == undefined ? null : scheduleCallback.callbackStatusId;
        this.leadCallbackRefNo = scheduleCallback == undefined ? null : scheduleCallback.leadCallbackRefNo == undefined ? null : scheduleCallback.leadCallbackRefNo;
        this.customerCallbackRefNo = scheduleCallback == undefined ? null : scheduleCallback.customerCallbackRefNo == undefined ? null : scheduleCallback.customerCallbackRefNo;
        this.triggerName = scheduleCallback == undefined ? null : scheduleCallback.triggerName == undefined ? null : scheduleCallback.triggerName;
        this.type = scheduleCallback == undefined ? null : scheduleCallback.type == undefined ? null : scheduleCallback.type;
    }
    leadCallbackId?: string;
    customerCallbackId?:string;
    callbackDateTime?: string;
    assignType?: string;
    customerCallbackNumber?: string;
    callbackQueueName?: string;
    customerName?: string;
    comments?: string;
    createdUserId?: string;
    leadRefNo?: string;
    customerRefNo?:string;
    leadName?: string;
    contactNumber?: string;
    contactNumberCountryCode?:string;
    callbackStatusName?: string;
    fromModule?: string;
    callbackStatusId:number;
    leadCallbackRefNo?:string;
    customerCallbackRefNo?:string;
    triggerName?:string;
    type?:string;
}
class ScheduleCallBackAddEditModel {
    constructor(scheduleCallback?: ScheduleCallBackAddEditModel) {
        this.leadId = scheduleCallback == undefined ? '' : scheduleCallback.leadId == undefined ? '' : scheduleCallback.leadId;
        this.customerId = scheduleCallback == undefined ? '' : scheduleCallback.customerId == undefined ? '' : scheduleCallback.customerId;
        this.callbackDateTime = scheduleCallback == undefined ? '' : scheduleCallback.callbackDateTime == undefined ? '' : scheduleCallback.callbackDateTime;
        this.isSelf = scheduleCallback == undefined ? true : scheduleCallback.isSelf == undefined ? true : scheduleCallback.isSelf;
        this.contactNumber = scheduleCallback == undefined ? '' : scheduleCallback.contactNumber == undefined ? '' : scheduleCallback.contactNumber;
        this.contactNumberCountryCode = scheduleCallback == undefined ? defaultCountryCode : scheduleCallback.contactNumberCountryCode == undefined ? defaultCountryCode : scheduleCallback.contactNumberCountryCode;
        this.queueId = scheduleCallback == undefined ? '' : scheduleCallback.queueId == undefined ? '' : scheduleCallback.queueId;
        this.comments = scheduleCallback == undefined ? '' : scheduleCallback.comments == undefined ? '' : scheduleCallback.comments;
        this.createdUserId = scheduleCallback == undefined ? '' : scheduleCallback.createdUserId == undefined ? '' : scheduleCallback.createdUserId;
        this.extension = scheduleCallback == undefined ? DEFAULT_EXTENSION_NUMBER : scheduleCallback.extension == undefined ? DEFAULT_EXTENSION_NUMBER : scheduleCallback.extension;
        this.pabxId = scheduleCallback == undefined ? "" : scheduleCallback.pabxId == undefined ? "" : scheduleCallback.pabxId;
        this.contactPerson = scheduleCallback == undefined ? '' : scheduleCallback.contactPerson == undefined ? '' : scheduleCallback.contactPerson;
        this.leadRefNo = scheduleCallback == undefined ? '' : scheduleCallback.leadRefNo == undefined ? '' : scheduleCallback.leadRefNo;
        this.leadName = scheduleCallback == undefined ? '' : scheduleCallback.leadName == undefined ? '' : scheduleCallback.leadName;
        this.customerRefNo = scheduleCallback == undefined ? '' : scheduleCallback.customerRefNo == undefined ? '' : scheduleCallback.customerRefNo;
        this.isCustomer = scheduleCallback == undefined ? false : scheduleCallback.isCustomer == undefined ? false : scheduleCallback.isCustomer;
        this.ticketId = scheduleCallback == undefined ? null : scheduleCallback.ticketId == undefined ? null : scheduleCallback.ticketId;
    }
    leadId?: string;
    customerId?: string;
    callbackDateTime?: string;
    isSelf?: boolean;
    contactNumber?: string;
    contactNumberCountryCode?: string;
    queueId?: string;
    comments?: string;
    createdUserId?: string;
    extension?: string;
    pabxId?: string;
    contactPerson?: string;
    leadRefNo?: string;
    customerRefNo?: string;
    leadName?: string;
    isCustomer?: boolean;
    ticketId?:string;
}

export { LeadSMSNotificationtModel, ScheduleCallBackViewModel, ScheduleCallBackAddEditModel };