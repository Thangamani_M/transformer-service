export class RewardCriteriaModel {
    constructor(rewardCriteriaModel?: RewardCriteriaModel) {
        this.rewardPartnerName = rewardCriteriaModel == undefined ? "" : rewardCriteriaModel.rewardPartnerName == undefined ? "" : rewardCriteriaModel.rewardPartnerName;
        this.rewardPartnerId = rewardCriteriaModel == undefined ? "" : rewardCriteriaModel.rewardPartnerId == undefined ? "" : rewardCriteriaModel.rewardPartnerId;
        this.rewardName = rewardCriteriaModel == undefined ? "" : rewardCriteriaModel.rewardName == undefined ? "" : rewardCriteriaModel.rewardName;
        this.rewardConfigId = rewardCriteriaModel == undefined ? "" : rewardCriteriaModel.rewardConfigId == undefined ? "" : rewardCriteriaModel.rewardConfigId;
        this.isActive = true;
        this.rewardDiscountSerivces = rewardCriteriaModel == undefined ? [] : rewardCriteriaModel.rewardDiscountSerivces == undefined ? [] : rewardCriteriaModel.rewardDiscountSerivces;
    }
    rewardPartnerName: string;
    rewardPartnerId: string;
    rewardName: string;
    rewardConfigId: string;
    isActive: boolean;
    rewardDiscountSerivces: RewardDiscountServicesModel[];
}
export class RewardDiscountServicesModel {
    constructor(rewardDiscountServices?: RewardDiscountServicesModel) {
        this.serviceCategoryId = rewardDiscountServices == undefined ? "" : rewardDiscountServices.serviceCategoryId == undefined ? "" : rewardDiscountServices.serviceCategoryId;
        this.serviceDiscountPercentage = rewardDiscountServices == undefined ? "" : rewardDiscountServices.serviceDiscountPercentage == undefined ? "" : rewardDiscountServices.serviceDiscountPercentage;
        this.leadGroupId = rewardDiscountServices == undefined ? "" : rewardDiscountServices.leadGroupId == undefined ? "" : rewardDiscountServices.leadGroupId;
        this.validFrom = rewardDiscountServices == undefined ? "" : rewardDiscountServices.validFrom == undefined ? "" : rewardDiscountServices.validFrom;
        this.validTo = rewardDiscountServices == undefined ? "" : rewardDiscountServices.validTo == undefined ? "" : rewardDiscountServices.validTo;
        this.isDeleted = rewardDiscountServices == undefined ? false : rewardDiscountServices.isDeleted == undefined ? false : rewardDiscountServices.isDeleted;
        this.rewardDiscountConfigId = rewardDiscountServices == undefined ? "" : rewardDiscountServices.rewardDiscountConfigId == undefined ? "" : rewardDiscountServices.rewardDiscountConfigId;
        this.isActive = rewardDiscountServices == undefined ? true : rewardDiscountServices.isActive == undefined ? true : rewardDiscountServices.isActive;
    }
    serviceCategoryId: string;
    serviceDiscountPercentage: string;
    leadGroupId: string;
    validFrom: string;
    validTo: string;
    isDeleted: boolean;
    isActive: boolean;
    rewardDiscountConfigId: string
}
export class RewardPartnershipModel {
    constructor(rewardPartnershipModel?: RewardPartnershipModel) {
        this.rewardPartnershipId = rewardPartnershipModel == undefined ? "" : rewardPartnershipModel.rewardPartnershipId == undefined ? "" : rewardPartnershipModel.rewardPartnershipId;
        this.rewardPartnershipCompanyId = rewardPartnershipModel == undefined ? "" : rewardPartnershipModel.rewardPartnershipCompanyId == undefined ? "" : rewardPartnershipModel.rewardPartnershipCompanyId;
        this.rewardPartnershipName = rewardPartnershipModel == undefined ? "" : rewardPartnershipModel.rewardPartnershipName == undefined ? "" : rewardPartnershipModel.rewardPartnershipName;
        this.rewardPartnershipCompanyName = rewardPartnershipModel == undefined ? "" : rewardPartnershipModel.rewardPartnershipCompanyName == undefined ? "" : rewardPartnershipModel.rewardPartnershipCompanyName;
        this.isMembershipNumberRequired = rewardPartnershipModel == undefined ? false : rewardPartnershipModel.isMembershipNumberRequired == undefined ? false : rewardPartnershipModel.isMembershipNumberRequired;
        this.startDate = rewardPartnershipModel == undefined ? "" : rewardPartnershipModel.startDate == undefined ? "" : rewardPartnershipModel.startDate;
        this.endDate = rewardPartnershipModel == undefined ? "" : rewardPartnershipModel.endDate == undefined ? "" : rewardPartnershipModel.endDate;
        this.isFixedContractFee = rewardPartnershipModel == undefined ? false : rewardPartnershipModel.isFixedContractFee == undefined ? false : rewardPartnershipModel.isFixedContractFee;
        this.isComplimentaryMonths = rewardPartnershipModel == undefined ? false : rewardPartnershipModel.isComplimentaryMonths == undefined ? false : rewardPartnershipModel.isComplimentaryMonths;
        this.isWaiverAdminFee = rewardPartnershipModel == undefined ? false : rewardPartnershipModel.isWaiverAdminFee == undefined ? false : rewardPartnershipModel.isWaiverAdminFee;
        this.isWaiverAnnualNetworkFee = rewardPartnershipModel == undefined ? false : rewardPartnershipModel.isWaiverAnnualNetworkFee == undefined ? false : rewardPartnershipModel.isWaiverAnnualNetworkFee;
        this.isWaiverProRateCharges = rewardPartnershipModel == undefined ? false : rewardPartnershipModel.isWaiverProRateCharges == undefined ? false : rewardPartnershipModel.isWaiverProRateCharges;
        this.complimentaryMonthId = rewardPartnershipModel == undefined ? null : rewardPartnershipModel.complimentaryMonthId == undefined ? null : rewardPartnershipModel.complimentaryMonthId;
        this.annualNetworkFeePeriodId = rewardPartnershipModel == undefined ? null : rewardPartnershipModel.annualNetworkFeePeriodId == undefined ? null : rewardPartnershipModel.annualNetworkFeePeriodId;
        this.createdUserId = rewardPartnershipModel == undefined ? "" : rewardPartnershipModel.createdUserId == undefined ? "" : rewardPartnershipModel.createdUserId;
        this.fixedContractFeePeriodId = rewardPartnershipModel == undefined ? null : rewardPartnershipModel.fixedContractFeePeriodId == undefined ? null : rewardPartnershipModel.fixedContractFeePeriodId;
    }
    rewardPartnershipId: string;
    rewardPartnershipCompanyId: string;
    rewardPartnershipName: string;
    rewardPartnershipCompanyName: string;
    isMembershipNumberRequired: boolean;
    isFixedContractFee: boolean;
    isComplimentaryMonths: boolean;
    isWaiverAdminFee: boolean;
    isWaiverAnnualNetworkFee: boolean;
    endDate: string;
    startDate: string;
    complimentaryMonthId: string;
    annualNetworkFeePeriodId: number;
    isWaiverProRateCharges: boolean;
    fixedContractFeePeriodId: number;
    createdUserId: string;
}
export class RewardPartnershipServiceInfoModel {
    constructor(rewardPartnershipServiceInfoModel?: RewardPartnershipServiceInfoModel) {
        this.rewardPartnershipId = rewardPartnershipServiceInfoModel == undefined ? "" : rewardPartnershipServiceInfoModel.rewardPartnershipId == undefined ? "" : rewardPartnershipServiceInfoModel.rewardPartnershipId;
        this.isDiscountOnMonthlyFee = rewardPartnershipServiceInfoModel == undefined ? false : rewardPartnershipServiceInfoModel.isDiscountOnMonthlyFee == undefined ? false : rewardPartnershipServiceInfoModel.isDiscountOnMonthlyFee;
        this.isFixedServiceDiscountPercentage = rewardPartnershipServiceInfoModel == undefined ? false : rewardPartnershipServiceInfoModel.isFixedServiceDiscountPercentage == undefined ? false : rewardPartnershipServiceInfoModel.isFixedServiceDiscountPercentage;
        this.isPreferenceRateApplicable = rewardPartnershipServiceInfoModel == undefined ? false : rewardPartnershipServiceInfoModel.isPreferenceRateApplicable == undefined ? false : rewardPartnershipServiceInfoModel.isPreferenceRateApplicable;
        this.isSpecificServices = rewardPartnershipServiceInfoModel == undefined ? false : rewardPartnershipServiceInfoModel.isSpecificServices == undefined ? false : rewardPartnershipServiceInfoModel.isSpecificServices;
        this.serviceDiscountAmount = rewardPartnershipServiceInfoModel == undefined ? 0 : rewardPartnershipServiceInfoModel.serviceDiscountAmount == undefined ? 0 : rewardPartnershipServiceInfoModel.serviceDiscountAmount;
        this.serviceDiscountPercentage = rewardPartnershipServiceInfoModel == undefined ? 0 : rewardPartnershipServiceInfoModel.serviceDiscountAmount == undefined ? 0 : rewardPartnershipServiceInfoModel.serviceDiscountAmount;
        this.createdUserId = rewardPartnershipServiceInfoModel == undefined ? "" : rewardPartnershipServiceInfoModel.createdUserId == undefined ? "" : rewardPartnershipServiceInfoModel.createdUserId;
        this.rewardPartnershipServices = rewardPartnershipServiceInfoModel == undefined ? [] : rewardPartnershipServiceInfoModel.rewardPartnershipServices == undefined ? [] : rewardPartnershipServiceInfoModel.rewardPartnershipServices;
        this.modifiedUserId = rewardPartnershipServiceInfoModel == undefined ? "" : rewardPartnershipServiceInfoModel.modifiedUserId == undefined ? "" : rewardPartnershipServiceInfoModel.modifiedUserId;
    }
    rewardPartnershipId: string;
    isDiscountOnMonthlyFee: boolean;
    isFixedServiceDiscountPercentage: boolean;
    isPreferenceRateApplicable: boolean;
    isSpecificServices: boolean;
    serviceDiscountAmount: number;
    serviceDiscountPercentage: number;
    createdUserId: string;
    modifiedUserId: string;
    rewardPartnershipServices: RewardPartnershipServicesModel[];
}
export class RewardPartnershipServicesModel {
    constructor(rewardPartnershipServicesModel?: RewardPartnershipServicesModel) {
        this.rewardPartnershipServiceId = rewardPartnershipServicesModel == undefined ? null : rewardPartnershipServicesModel.rewardPartnershipServiceId == undefined ? null : rewardPartnershipServicesModel.rewardPartnershipServiceId;
        this.serviceCategoryId = rewardPartnershipServicesModel == undefined ? "" : rewardPartnershipServicesModel.serviceCategoryId == undefined ? "" : rewardPartnershipServicesModel.serviceCategoryId;
        this.createdUserId = rewardPartnershipServicesModel == undefined ? "" : rewardPartnershipServicesModel.createdUserId == undefined ? "" : rewardPartnershipServicesModel.createdUserId;
        this.discountAmount = rewardPartnershipServicesModel == undefined ? "" : rewardPartnershipServicesModel.discountAmount == undefined ? "" : rewardPartnershipServicesModel.discountAmount;
        this.discountPercentage = rewardPartnershipServicesModel == undefined ? "" : rewardPartnershipServicesModel.discountPercentage == undefined ? "" : rewardPartnershipServicesModel.discountPercentage;
        this.rewardPartnershipId = rewardPartnershipServicesModel == undefined ? "" : rewardPartnershipServicesModel.rewardPartnershipId == undefined ? "" : rewardPartnershipServicesModel.rewardPartnershipId;
        this.serviceId = rewardPartnershipServicesModel == undefined ? "" : rewardPartnershipServicesModel.serviceId == undefined ? "" : rewardPartnershipServicesModel.serviceId;
        this.serviceName = rewardPartnershipServicesModel == undefined ? "" : rewardPartnershipServicesModel.serviceName == undefined ? "" : rewardPartnershipServicesModel.serviceName;
        this.isPercentage = rewardPartnershipServicesModel == undefined ? false : rewardPartnershipServicesModel.isPercentage == undefined ? false : rewardPartnershipServicesModel.isPercentage;
        this.serviceRefNo = rewardPartnershipServicesModel == undefined ? "" : rewardPartnershipServicesModel.serviceRefNo == undefined ? "" : rewardPartnershipServicesModel.serviceRefNo;
    }
    rewardPartnershipServiceId: string;
    createdUserId: string;
    discountAmount: string;
    discountPercentage: string;
    isPercentage: boolean;
    rewardPartnershipId: string;
    serviceCategoryId: string;
    serviceId: string;
    serviceName: string;
    serviceRefNo: string;
}
export class RewardPartnershipItemInfoModel {
    constructor(rewardPartnershipItemInfoModel?: RewardPartnershipItemInfoModel) {
        this.rewardPartnershipId = rewardPartnershipItemInfoModel == undefined ? "" : rewardPartnershipItemInfoModel.rewardPartnershipId == undefined ? "" : rewardPartnershipItemInfoModel.rewardPartnershipId;
        this.isOverAllItemDiscount = rewardPartnershipItemInfoModel == undefined ? false : rewardPartnershipItemInfoModel.isOverAllItemDiscount == undefined ? false : rewardPartnershipItemInfoModel.isOverAllItemDiscount;
        this.isFixedItemDiscountPercentage = rewardPartnershipItemInfoModel == undefined ? false : rewardPartnershipItemInfoModel.isFixedItemDiscountPercentage == undefined ? false : rewardPartnershipItemInfoModel.isFixedItemDiscountPercentage;
        this.isPreferenceRateApplicable = rewardPartnershipItemInfoModel == undefined ? false : rewardPartnershipItemInfoModel.isPreferenceRateApplicable == undefined ? false : rewardPartnershipItemInfoModel.isPreferenceRateApplicable;
        this.isSpecialDiscount = rewardPartnershipItemInfoModel == undefined ? false : rewardPartnershipItemInfoModel.isSpecialDiscount == undefined ? false : rewardPartnershipItemInfoModel.isSpecialDiscount;
        this.itemDiscountAmount = rewardPartnershipItemInfoModel == undefined ? 0 : rewardPartnershipItemInfoModel.itemDiscountAmount == undefined ? 0 : rewardPartnershipItemInfoModel.itemDiscountAmount;
        this.itemDiscountPercentage = rewardPartnershipItemInfoModel == undefined ? 0 : rewardPartnershipItemInfoModel.itemDiscountPercentage == undefined ? 0 : rewardPartnershipItemInfoModel.itemDiscountPercentage;
        this.createdUserId = rewardPartnershipItemInfoModel == undefined ? "" : rewardPartnershipItemInfoModel.createdUserId == undefined ? "" : rewardPartnershipItemInfoModel.createdUserId;
        this.rewardPartnershipServicesListDTO = rewardPartnershipItemInfoModel == undefined ? [] : rewardPartnershipItemInfoModel.rewardPartnershipServicesListDTO == undefined ? [] : rewardPartnershipItemInfoModel.rewardPartnershipServicesListDTO;
        this.modifiedUserId = rewardPartnershipItemInfoModel == undefined ? "" : rewardPartnershipItemInfoModel.modifiedUserId == undefined ? "" : rewardPartnershipItemInfoModel.modifiedUserId;
    }
    rewardPartnershipId: string;
    isOverAllItemDiscount: boolean;
    isFixedItemDiscountPercentage: boolean;
    isPreferenceRateApplicable: boolean;
    isSpecialDiscount: boolean;
    itemDiscountAmount: number;
    itemDiscountPercentage: number;
    createdUserId: string;
    modifiedUserId: string;
    rewardPartnershipServicesListDTO: RewardPartnershipItemServicesModel[];
}
export class RewardPartnershipItemServicesModel {
    constructor(rewardPartnershipItemServicesModel?: RewardPartnershipItemServicesModel) {
        this.rewardPartnershipItemId = rewardPartnershipItemServicesModel == undefined ? "" : rewardPartnershipItemServicesModel.rewardPartnershipItemId == undefined ? "" : rewardPartnershipItemServicesModel.rewardPartnershipItemId;
        this.createdUserId = rewardPartnershipItemServicesModel == undefined ? "" : rewardPartnershipItemServicesModel.createdUserId == undefined ? "" : rewardPartnershipItemServicesModel.createdUserId;
        this.sellingPrice = rewardPartnershipItemServicesModel == undefined ? "" : rewardPartnershipItemServicesModel.sellingPrice == undefined ? "" : rewardPartnershipItemServicesModel.sellingPrice;
        this.stockName = rewardPartnershipItemServicesModel == undefined ? "" : rewardPartnershipItemServicesModel.stockName == undefined ? "" : rewardPartnershipItemServicesModel.stockName;
        this.stockCode = rewardPartnershipItemServicesModel == undefined ? "" : rewardPartnershipItemServicesModel.stockCode == undefined ? "" : rewardPartnershipItemServicesModel.stockCode;
        this.itemId = rewardPartnershipItemServicesModel == undefined ? "" : rewardPartnershipItemServicesModel.itemId == undefined ? "" : rewardPartnershipItemServicesModel.itemId;
    }
    createdUserId: string;
    sellingPrice: string;
    stockCode: string;
    stockName: string;
    rewardPartnershipItemId: string;
    itemId: string;
}
export class ViewRewardCriteria {
    rewardPartnerName: string;
    rewardName: string;
}
