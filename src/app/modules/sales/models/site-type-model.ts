
export class SiteTypeModel {
    constructor(siteTypeModel?:SiteTypeModel){
      this.siteTypeId = siteTypeModel ? siteTypeModel.siteTypeId == undefined ? '' : siteTypeModel.siteTypeId : '';
      this.siteTypeName = siteTypeModel ? siteTypeModel.siteTypeName == undefined ? '' : siteTypeModel.siteTypeName : '';
      this.description = siteTypeModel ? siteTypeModel.description == undefined ? '' : siteTypeModel.description : '';
      this.createdUserId = siteTypeModel ? siteTypeModel.createdUserId == undefined ? '' : siteTypeModel.createdUserId : '';
      this.modifiedUserId = siteTypeModel ? siteTypeModel.modifiedUserId == undefined ? '' : siteTypeModel.modifiedUserId : '';
    }
    public siteTypeId?: string;
    public siteTypeName: string;
    public description?:string;
    public createdUserId?:string;
    public modifiedUserId?:string;
  }
  