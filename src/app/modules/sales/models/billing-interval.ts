export class BillingInterval {
  billingIntervalId: string;
  billingIntervalName: string;
  description: string;
  cssClass: string;
  isDeleted: boolean;
  isSystem: boolean;
  isActive: boolean;
  createdDate: Date;
  createdUserId: string;
  modifiedDate: Date;
  modifiedUserId: string;
  billingIntervalValue: number;
}
