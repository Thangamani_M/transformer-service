class OtherDocumentUploadModel {
    constructor(supportDocumentUploadModel?: OtherDocumentUploadModel) {
        this.fileName = supportDocumentUploadModel ? supportDocumentUploadModel.fileName == undefined ? '' : supportDocumentUploadModel.fileName : '';
        this.createdUserId = supportDocumentUploadModel ? supportDocumentUploadModel.createdUserId == undefined ? '' : supportDocumentUploadModel.createdUserId : '';
        this.fileUpload = supportDocumentUploadModel ? supportDocumentUploadModel.fileUpload == undefined ? '' : supportDocumentUploadModel.fileUpload : '';
        this.referenceId = supportDocumentUploadModel ? supportDocumentUploadModel.referenceId == undefined ? '' : supportDocumentUploadModel.referenceId : '';
        this.documentId = supportDocumentUploadModel ? supportDocumentUploadModel.documentId == undefined ? '' : supportDocumentUploadModel.documentId : '';
        this.documentTypeId = supportDocumentUploadModel ? supportDocumentUploadModel.documentTypeId == undefined ? null : supportDocumentUploadModel.documentTypeId : null;
        this.leadId = supportDocumentUploadModel ? supportDocumentUploadModel.leadId == undefined ? '' : supportDocumentUploadModel.leadId : '';
        this.documentPath = supportDocumentUploadModel ? supportDocumentUploadModel.documentPath == undefined ? '' : supportDocumentUploadModel.documentPath : '';
        this.fileSize = supportDocumentUploadModel ? supportDocumentUploadModel.fileSize == undefined ? '' : supportDocumentUploadModel.fileSize : '';
    }
    fileName?: string;
    createdUserId?: string;
    fileUpload?: string;
    referenceId?: string;
    documentId?: string;
    documentTypeId?: string;
    fileSize?: string;
    documentPath?: string;
    leadId?: string;
}

export { OtherDocumentUploadModel };