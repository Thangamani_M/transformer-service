class LeadNotesModel{
    constructor(leadNotesModel?:LeadNotesModel){
        this.leadId=leadNotesModel?leadNotesModel.leadId==undefined?'':leadNotesModel.leadId:'';
        this.comments=leadNotesModel?leadNotesModel.comments==undefined?'':leadNotesModel.comments:'';
         this.createdUserId=leadNotesModel?leadNotesModel.createdUserId==undefined?'':leadNotesModel.createdUserId:'';
    }
    leadId: any;
    comments: string;
    createdUserId: string;
}
export{LeadNotesModel}