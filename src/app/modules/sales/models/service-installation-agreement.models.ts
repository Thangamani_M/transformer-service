import { defaultCountryCode } from '@app/shared';
abstract class CommonModels {
    constructor(commonModels?: CommonModels) {
        this.createdUserId = commonModels == undefined ? "" : commonModels.createdUserId == undefined ? '' : commonModels.createdUserId;
        this.modifiedUserId = commonModels == undefined ? "" : commonModels.modifiedUserId == undefined ? '' : commonModels.modifiedUserId;
        this.leadId = commonModels == undefined ? "" : commonModels.leadId == undefined ? '' : commonModels.leadId;
        this.partitionId = commonModels == undefined ? null : commonModels.partitionId == undefined ? null : commonModels.partitionId;
        this.customerId = commonModels == undefined ? "" : commonModels.customerId == undefined ? '' : commonModels.customerId;
        this.quotationVersionId = commonModels == undefined ? "" : commonModels.quotationVersionId == undefined ? '' : commonModels.quotationVersionId;
    }
    createdUserId: string;
    modifiedUserId: string;
    leadId: string;
    partitionId: string;
    customerId: string;
    quotationVersionId: string;
}
class KeyHolderInfoAddEditModel {
    constructor(keyHolderInfoModel?: KeyHolderInfoAddEditModel) {
        this.keyholders = keyHolderInfoModel == undefined ? [] : keyHolderInfoModel.keyholders == undefined ? [] : keyHolderInfoModel.keyholders;
        this.emergencyContacts = keyHolderInfoModel == undefined ? [] : keyHolderInfoModel.emergencyContacts == undefined ? [] : keyHolderInfoModel.emergencyContacts;
    }
    keyholders: KeyholderObjectModel[];
    emergencyContacts: EmergencyContactModel[];
}
class KeyholderObjectModel extends CommonModels {
    constructor(keyholderObjectModel?: KeyholderObjectModel) {
        super(keyholderObjectModel);
        this.keyHolderId = keyholderObjectModel == undefined ? "" : keyholderObjectModel.keyHolderId == undefined ? "" : keyholderObjectModel.keyHolderId;
        this.keyHolderName = keyholderObjectModel == undefined ? "" : keyholderObjectModel.keyHolderName == undefined ? "" : keyholderObjectModel.keyHolderName;
        this.contactNo = keyholderObjectModel == undefined ? "" : keyholderObjectModel.contactNo == undefined ? "" : keyholderObjectModel.contactNo;
        this.contactNoCountryCode = keyholderObjectModel == undefined ? defaultCountryCode : keyholderObjectModel.contactNoCountryCode == undefined ? defaultCountryCode : keyholderObjectModel.contactNoCountryCode;
        this.password = keyholderObjectModel == undefined ? "" : keyholderObjectModel.password == undefined ? "" : keyholderObjectModel.password;
        this.index = keyholderObjectModel == undefined ? 0 : keyholderObjectModel.index == undefined ? 0 : keyholderObjectModel.index;
        this.currentCursorIndex = keyholderObjectModel == undefined ? 0 : keyholderObjectModel.currentCursorIndex == undefined ? 0 : keyholderObjectModel.currentCursorIndex;
        this.isPasswordShow = keyholderObjectModel == undefined ? false : keyholderObjectModel.isPasswordShow == undefined ? false : keyholderObjectModel.isPasswordShow;
    }
    keyHolderId: string;
    keyHolderName: string;
    contactNo: string;
    contactNoCountryCode: string;
    password: string;
    index: number;
    currentCursorIndex: number;
    isPasswordShow: boolean;
}
class EmergencyContactModel extends CommonModels {
    constructor(emergencyContactModel?: EmergencyContactModel) {
        super(emergencyContactModel);
        this.emergencyContactId = emergencyContactModel == undefined ? "" : emergencyContactModel.emergencyContactId == undefined ? "" : emergencyContactModel.emergencyContactId;
        this.contactPerson = emergencyContactModel == undefined ? "" : emergencyContactModel.contactPerson == undefined ? "" : emergencyContactModel.contactPerson;
        this.contactNo = emergencyContactModel == undefined ? "" : emergencyContactModel.contactNo == undefined ? "" : emergencyContactModel.contactNo;
        this.contactNoCountryCode = emergencyContactModel == undefined ? defaultCountryCode : emergencyContactModel.contactNoCountryCode == undefined ? defaultCountryCode : emergencyContactModel.contactNoCountryCode;
        this.index = emergencyContactModel == undefined ? 0 : emergencyContactModel.index == undefined ? 0 : emergencyContactModel.index;
        this.currentCursorIndex = emergencyContactModel == undefined ? 0 : emergencyContactModel.currentCursorIndex == undefined ? 0 : emergencyContactModel.currentCursorIndex;
    }
    emergencyContactId: string;
    contactPerson: string;
    contactNo: string;
    contactNoCountryCode: string;
    index: number;
    currentCursorIndex: number;
}
class PasswordsAddEditModel extends CommonModels {
    constructor(passwordsAddEditModel?: PasswordsAddEditModel) {
        super(passwordsAddEditModel);
        this.contractId = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.contractId == undefined ? '' : passwordsAddEditModel.contractId;
        this.specialInstructionId = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.specialInstructionId == undefined ? '' : passwordsAddEditModel.specialInstructionId;
        this.customerSpecialInstructions = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.customerSpecialInstructions == undefined ? '' : passwordsAddEditModel.customerSpecialInstructions;
        this.distressWord = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.distressWord == undefined ? '' : passwordsAddEditModel.distressWord;
        this.medical = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.medical == undefined ? '' : passwordsAddEditModel.medical;
        this.isDogs = passwordsAddEditModel == undefined ? false : passwordsAddEditModel.isDogs == undefined ? false : passwordsAddEditModel.isDogs;
        this.noOfDogs = passwordsAddEditModel == undefined ? null : passwordsAddEditModel.noOfDogs == undefined ? null : passwordsAddEditModel.noOfDogs;
        this.isVAS = passwordsAddEditModel == undefined ? true : passwordsAddEditModel.isVAS == undefined ? true : passwordsAddEditModel.isVAS;
        this.alarmCancellationPassword = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.alarmCancellationPassword == undefined ? '' : passwordsAddEditModel.alarmCancellationPassword;
        this.dogTypes = passwordsAddEditModel == undefined ? [] : passwordsAddEditModel.dogTypes == undefined ? [] : passwordsAddEditModel.dogTypes;
        this.medicalConditions = passwordsAddEditModel == undefined ? [] : passwordsAddEditModel.medicalConditions == undefined ? [] : passwordsAddEditModel.medicalConditions;
        this.dogDescription = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.dogDescription == undefined ? '' : passwordsAddEditModel.dogDescription;
        this.addressId = passwordsAddEditModel == undefined ? '' : passwordsAddEditModel.addressId == undefined ? '' : passwordsAddEditModel.addressId;
    }
    specialInstructionId: string;
    contractId: string;
    noOfDogs: string;
    customerSpecialInstructions: string;
    distressWord: string;
    medical: string;
    isDogs: boolean;
    isVAS: boolean;
    alarmCancellationPassword: string;
    dogTypes: DocTypes[];
    medicalConditions: MedicalConditions[];
    dogDescription: string;
    addressId: string;
}
class InstallationHazardModel extends CommonModels {
    constructor(installationHazardModel?: InstallationHazardModel) {
        super(installationHazardModel);
        this.siteHazardId = installationHazardModel == undefined ? null : installationHazardModel.siteHazardId == undefined ? null : installationHazardModel.siteHazardId;
        this.contractId = installationHazardModel == undefined ? '' : installationHazardModel.contractId == undefined ? '' : installationHazardModel.contractId;
        this.leadId = installationHazardModel == undefined ? '' : installationHazardModel.leadId == undefined ? '' : installationHazardModel.leadId;
        this.customerId = installationHazardModel == undefined ? '' : installationHazardModel.customerId == undefined ? '' : installationHazardModel.customerId;
        this.partitionId = installationHazardModel == undefined ? '' : installationHazardModel.partitionId == undefined ? '' : installationHazardModel.partitionId;
        this.specialInstructionId = installationHazardModel == undefined ? '' : installationHazardModel.specialInstructionId == undefined ? '' : installationHazardModel.specialInstructionId;
        this.noOfDogs = installationHazardModel == undefined ? null : installationHazardModel.noOfDogs == undefined ? null : installationHazardModel.noOfDogs;
        this.viciousDogsDescription = installationHazardModel == undefined ? '' : installationHazardModel.viciousDogsDescription == undefined ? '' : installationHazardModel.viciousDogsDescription;
        this.confinedSpaceDescription = installationHazardModel == undefined ? '' : installationHazardModel.confinedSpaceDescription == undefined ? '' : installationHazardModel.confinedSpaceDescription;
        this.extendedLadderDescription = installationHazardModel == undefined ? '' : installationHazardModel.extendedLadderDescription == undefined ? '' : installationHazardModel.extendedLadderDescription;
        this.constructionSiteDescription = installationHazardModel == undefined ? '' : installationHazardModel.constructionSiteDescription == undefined ? '' : installationHazardModel.constructionSiteDescription;
        this.harmfulChemicalDescription = installationHazardModel == undefined ? '' : installationHazardModel.harmfulChemicalDescription == undefined ? '' : installationHazardModel.harmfulChemicalDescription;
        this.loudNoiseAreaDescription = installationHazardModel == undefined ? '' : installationHazardModel.loudNoiseAreaDescription == undefined ? '' : installationHazardModel.loudNoiseAreaDescription;
        this.asbestosDescription = installationHazardModel == undefined ? '' : installationHazardModel.asbestosDescription == undefined ? '' : installationHazardModel.asbestosDescription;
        this.unsafeDescription = installationHazardModel == undefined ? '' : installationHazardModel.unsafeDescription == undefined ? '' : installationHazardModel.unsafeDescription;
        this.confinedSpaceInstalledDescription = installationHazardModel == undefined ? '' : installationHazardModel.confinedSpaceInstalledDescription == undefined ? '' : installationHazardModel.confinedSpaceInstalledDescription;
        this.isInstallationRequired = installationHazardModel == undefined ? true : installationHazardModel.isInstallationRequired == undefined ? true : installationHazardModel.isInstallationRequired;
        this.isViciousDogs = installationHazardModel == undefined ? false : installationHazardModel.isViciousDogs == undefined ? false : installationHazardModel.isViciousDogs;
        this.isConfinedSpace = installationHazardModel == undefined ? false : (installationHazardModel.isConfinedSpace == undefined || installationHazardModel.isConfinedSpace == null) ?
            null : installationHazardModel.isConfinedSpace;
        this.isExtendedLadders = installationHazardModel == undefined ? false : (installationHazardModel.isExtendedLadders == undefined || installationHazardModel.isExtendedLadders == null) ?
            null : installationHazardModel.isExtendedLadders;
        this.isConstructionSite = installationHazardModel == undefined ? false : (installationHazardModel.isConstructionSite == undefined || installationHazardModel.isConstructionSite == null) ?
            null : installationHazardModel.isConstructionSite;
        this.isHarmfulChemical = installationHazardModel == undefined ? false : (installationHazardModel.isHarmfulChemical == undefined || installationHazardModel.isHarmfulChemical == null) ?
            null : installationHazardModel.isHarmfulChemical;
        this.isLoudNoiseArea = installationHazardModel == undefined ? false : (installationHazardModel.isLoudNoiseArea == undefined || installationHazardModel.isLoudNoiseArea == null) ?
            null : installationHazardModel.isLoudNoiseArea;
        this.isAsbestos = installationHazardModel == undefined ? false : (installationHazardModel.isAsbestos == undefined || installationHazardModel.isAsbestos == null) ?
            null : installationHazardModel.isAsbestos;
        this.isUnsafe = installationHazardModel == undefined ? false : (installationHazardModel.isUnsafe == undefined || installationHazardModel.isUnsafe == null) ?
            null : installationHazardModel.isUnsafe;
        this.isConfinedSpaceInstalled = installationHazardModel == undefined ? false : (installationHazardModel.isConfinedSpaceInstalled == undefined || installationHazardModel.isConfinedSpaceInstalled == null) ?
            null : installationHazardModel.isConfinedSpaceInstalled;
        this.dogTypes = installationHazardModel == undefined ? [] : installationHazardModel.dogTypes == undefined ? [] : installationHazardModel.dogTypes;
    }
    siteHazardId: string;
    specialInstructionId: string;
    noOfDogs: string;
    leadId: string;
    contractId: string;
    customerId: string;
    isDogs: boolean;
    isVAS: boolean;
    partitionId: string;
    isInstallationRequired: boolean;
    isViciousDogs: boolean;
    isConfinedSpace: boolean;
    isExtendedLadders: boolean;
    isConstructionSite: boolean;
    isHarmfulChemical: boolean;
    isLoudNoiseArea: boolean;
    isAsbestos: boolean;
    isUnsafe: boolean;
    isConfinedSpaceInstalled: boolean;
    viciousDogsDescription: string;
    unsafeDescription: string;
    confinedSpaceInstalledDescription: string;
    confinedSpaceDescription: string;
    extendedLadderDescription: string;
    constructionSiteDescription: string;
    harmfulChemicalDescription: string;
    loudNoiseAreaDescription: string;
    asbestosDescription: string;
    dogTypes: DocTypes[];
}
class FinduSubscriberModel extends CommonModels {
    constructor(finduSubscriberModel?: FinduSubscriberModel) {
        super(finduSubscriberModel);
        this.leadId = finduSubscriberModel == undefined ? '' : finduSubscriberModel.leadId == undefined ? '' : finduSubscriberModel.leadId;
        this.customerId = finduSubscriberModel == undefined ? '' : finduSubscriberModel.customerId == undefined ? '' : finduSubscriberModel.customerId;
        this.partitionId = finduSubscriberModel == undefined ? '' : finduSubscriberModel.partitionId == undefined ? '' : finduSubscriberModel.partitionId;
        this.contractId = finduSubscriberModel == undefined ? '' : finduSubscriberModel.contractId == undefined ? '' : finduSubscriberModel.contractId;
        this.mobileNo = finduSubscriberModel == undefined ? '' : finduSubscriberModel.mobileNo == undefined ? '' : finduSubscriberModel.mobileNo;
        this.mobileNoCountryCode = finduSubscriberModel == undefined ? '' : finduSubscriberModel.mobileNoCountryCode == undefined ? '' : finduSubscriberModel.mobileNoCountryCode;
        this.email = finduSubscriberModel == undefined ? '' : finduSubscriberModel.email == undefined ? '' : finduSubscriberModel.email;
        this.title = finduSubscriberModel == undefined ? '' : finduSubscriberModel.title == undefined ? '' : finduSubscriberModel.title;
        this.password = finduSubscriberModel == undefined ? '' : finduSubscriberModel.password == undefined ? '' : finduSubscriberModel.password;
        this.firstName = finduSubscriberModel == undefined ? '' : finduSubscriberModel.firstName == undefined ? '' : finduSubscriberModel.firstName;
        this.lastName = finduSubscriberModel == undefined ? '' : finduSubscriberModel.lastName == undefined ? '' : finduSubscriberModel.lastName;
        this.dob = finduSubscriberModel == undefined ? '' : finduSubscriberModel.dob == undefined ? '' : finduSubscriberModel.dob;
        this.companyName = finduSubscriberModel == undefined ? '' : finduSubscriberModel.companyName == undefined ? '' : finduSubscriberModel.companyName;
        this.address1 = finduSubscriberModel == undefined ? '' : finduSubscriberModel.address1 == undefined ? '' : finduSubscriberModel.address1;
        this.address2 = finduSubscriberModel == undefined ? '' : finduSubscriberModel.address2 == undefined ? '' : finduSubscriberModel.address2;
        this.address3 = finduSubscriberModel == undefined ? '' : finduSubscriberModel.address3 == undefined ? '' : finduSubscriberModel.address3;
        this.city = finduSubscriberModel == undefined ? '' : finduSubscriberModel.city == undefined ? '' : finduSubscriberModel.city;
        this.language = finduSubscriberModel == undefined ? null : finduSubscriberModel.language == undefined ? null : finduSubscriberModel.language;
        this.gender = finduSubscriberModel == undefined ? '' : finduSubscriberModel.gender == undefined ? '' : finduSubscriberModel.gender;
        this.height = finduSubscriberModel == undefined ? null : finduSubscriberModel.height == undefined ? null : finduSubscriberModel.height;
        this.build = finduSubscriberModel == undefined ? null : finduSubscriberModel.build == undefined ? null : finduSubscriberModel.build;
        this.race = finduSubscriberModel == undefined ? null : finduSubscriberModel.race == undefined ? null : finduSubscriberModel.race;
        this.nationality = finduSubscriberModel == undefined ? null : finduSubscriberModel.nationality == undefined ? null : finduSubscriberModel.nationality;
        this.vehicleRegistrationNo = finduSubscriberModel == undefined ? '' : finduSubscriberModel.vehicleRegistrationNo == undefined ? '' : finduSubscriberModel.vehicleRegistrationNo;
        this.vehicleMake = finduSubscriberModel == undefined ? '' : finduSubscriberModel.vehicleMake == undefined ? '' : finduSubscriberModel.vehicleMake;
        this.vehicleBodyType = finduSubscriberModel == undefined ? '' : finduSubscriberModel.vehicleBodyType == undefined ? '' : finduSubscriberModel.vehicleBodyType;
        this.vehicleColour = finduSubscriberModel == undefined ? '' : finduSubscriberModel.vehicleColour == undefined ? '' : finduSubscriberModel.vehicleColour;
        this.medicalInfo = finduSubscriberModel == undefined ? '' : finduSubscriberModel.medicalInfo == undefined ? '' : finduSubscriberModel.medicalInfo;
        this.medicalSymptoms = finduSubscriberModel == undefined ? '' : finduSubscriberModel.medicalSymptoms == undefined ? '' : finduSubscriberModel.medicalSymptoms;
        this.medicalDiseases = finduSubscriberModel == undefined ? '' : finduSubscriberModel.medicalDiseases == undefined ? '' : finduSubscriberModel.medicalDiseases;
        this.noOfUserCount = finduSubscriberModel == undefined ? '' : finduSubscriberModel.noOfUserCount == undefined ? '' : finduSubscriberModel.noOfUserCount;
        this.createdUserId = finduSubscriberModel == undefined ? '' : finduSubscriberModel.createdUserId == undefined ? '' : finduSubscriberModel.createdUserId;
        this.finduAddressId = finduSubscriberModel == undefined ? '' : finduSubscriberModel.finduAddressId == undefined ? '' : finduSubscriberModel.finduAddressId;
        this.finduUserId = finduSubscriberModel == undefined ? '' : finduSubscriberModel.finduUserId == undefined ? '' : finduSubscriberModel.finduUserId;
        this.additionalUsers = finduSubscriberModel == undefined ? [] : finduSubscriberModel.additionalUsers == undefined ? [] : finduSubscriberModel.additionalUsers;
        this.vehicleModel = finduSubscriberModel == undefined ? '' : finduSubscriberModel.vehicleModel == undefined ? '' : finduSubscriberModel.vehicleModel;
        this.heightId = finduSubscriberModel == undefined ? null : finduSubscriberModel.heightId == undefined ? null : finduSubscriberModel.heightId;
    }
    leadId: string;
    customerId: string;
    contractId: string;
    partitionId: string;
    mobileNo: string;
    mobileNoCountryCode: string;
    email: string;
    title: string;
    password: string;
    firstName: string;
    lastName: string;
    dob: string;
    companyName: string;
    address1: string;
    address2: string;
    address3: string;
    city: string;
    language: string;
    gender: string;
    height: string;
    build: string;
    race: string;
    nationality: string;
    vehicleRegistrationNo: string;
    vehicleMake: string;
    vehicleBodyType: string;
    vehicleColour: string;
    medicalInfo: string;
    medicalSymptoms: string;
    medicalDiseases: string;
    noOfUserCount: string;
    createdUserId: string;
    finduAddressId: string;
    finduUserId: string;
    additionalUsers: AdditionalUsers[];
    vehicleModel:string;
    heightId:number;
}
export class AdditionalUsers {
    constructor(additionalUsers?: AdditionalUsers) {
        this.finduUserId = additionalUsers == undefined ? "" : additionalUsers.finduUserId == undefined ? "" : additionalUsers.finduUserId;
        this.firstName = additionalUsers == undefined ? "" : additionalUsers.firstName == undefined ? "" : additionalUsers.firstName;
        this.lastName = additionalUsers == undefined ? "" : additionalUsers.lastName == undefined ? "" : additionalUsers.lastName;
        this.mobileNo = additionalUsers == undefined ? "" : additionalUsers.mobileNo == undefined ? "" : additionalUsers.mobileNo;
        this.mobileNoCountryCode = additionalUsers == undefined ? "+27" : additionalUsers.mobileNoCountryCode == undefined ? "+27" : additionalUsers.mobileNoCountryCode;
    }
    finduUserId: string;
    lastName: string;
    firstName: string;
    mobileNo: string;
    mobileNoCountryCode: string;
}
export class DocTypes {
    constructor(docTypes?: DocTypes) {
        this.dogTypeDetailId = docTypes == undefined ? "" : docTypes.dogTypeDetailId == undefined ? "" : docTypes.dogTypeDetailId;
        this.dogDetailId = docTypes == undefined ? "" : docTypes.dogDetailId == undefined ? "" : docTypes.dogDetailId;
        this.dogTypeId = docTypes == undefined ? "" : docTypes.dogTypeId == undefined ? "" : docTypes.dogTypeId;
    }
    dogDetailId: string;
    dogTypeId: string;
    dogTypeDetailId: string;
}
export class MedicalConditions {
    constructor(medicalConditions?: MedicalConditions) {
        this.medicalDetailId = medicalConditions == undefined ? "" : medicalConditions.medicalDetailId == undefined ? "" : medicalConditions.medicalDetailId;
        this.medicalCondition = medicalConditions == undefined ? "" : medicalConditions.medicalCondition == undefined ? "" : medicalConditions.medicalCondition;
    }
    medicalDetailId: string;
    medicalCondition: string;
}
class SiteHazardAddEditModel extends CommonModels {
    constructor(siteHazardAddEditModel?: SiteHazardAddEditModel) {
        super(siteHazardAddEditModel);
        this.siteHazardId = siteHazardAddEditModel ? siteHazardAddEditModel.siteHazardId == undefined ? '' : siteHazardAddEditModel.siteHazardId : '';
        this.contractId = siteHazardAddEditModel ? siteHazardAddEditModel.contractId == undefined ? '' : siteHazardAddEditModel.contractId : '';
        this.isPremiseVisible = siteHazardAddEditModel ? siteHazardAddEditModel.isPremiseVisible == undefined ? true : siteHazardAddEditModel.isPremiseVisible : true;
        this.isSwimmingPool = siteHazardAddEditModel ? siteHazardAddEditModel.isSwimmingPool == undefined ? true : siteHazardAddEditModel.isSwimmingPool : true;
        this.swimmingPoolLocation = siteHazardAddEditModel ? siteHazardAddEditModel.swimmingPoolLocation == undefined ? '' : siteHazardAddEditModel.swimmingPoolLocation : '';
        this.isSecurityOfficers = siteHazardAddEditModel ? siteHazardAddEditModel.isSecurityOfficers == undefined ? true : siteHazardAddEditModel.isSecurityOfficers : true;
        this.noOfSecurityOfficers = siteHazardAddEditModel ? siteHazardAddEditModel.noOfSecurityOfficers == undefined ? null : siteHazardAddEditModel.noOfSecurityOfficers : null;
        this.securityServiceProviderName = siteHazardAddEditModel ? siteHazardAddEditModel.securityServiceProviderName == undefined ? '' : siteHazardAddEditModel.securityServiceProviderName : '';
        this.controlRoomPhoneNo = siteHazardAddEditModel ? siteHazardAddEditModel.controlRoomPhoneNo == undefined ? '' : siteHazardAddEditModel.controlRoomPhoneNo : '';
        this.controlRoomPhoneNoCountryCode = siteHazardAddEditModel ? siteHazardAddEditModel.controlRoomPhoneNoCountryCode == undefined ? defaultCountryCode : siteHazardAddEditModel.controlRoomPhoneNoCountryCode : defaultCountryCode;
        this.isSecurityProviderControlRoomOnSite = siteHazardAddEditModel ? siteHazardAddEditModel.isSecurityProviderControlRoomOnSite == undefined ? true : siteHazardAddEditModel.isSecurityProviderControlRoomOnSite : true;
        this.riskDescription = siteHazardAddEditModel ? siteHazardAddEditModel.riskDescription == undefined ? '' : siteHazardAddEditModel.riskDescription : '';
        this.description = siteHazardAddEditModel ? siteHazardAddEditModel.description == undefined ? '' : siteHazardAddEditModel.description : '';
        this.oldSiteHazardId = siteHazardAddEditModel ? siteHazardAddEditModel.oldSiteHazardId == undefined ? '' : siteHazardAddEditModel.oldSiteHazardId : '';
        this.addressId = siteHazardAddEditModel ? siteHazardAddEditModel.addressId == undefined ? '' : siteHazardAddEditModel.addressId : '';
    }
    siteHazardId?: string;
    isPremiseVisible: boolean;
    isSwimmingPool: boolean;
    contractId: string;
    swimmingPoolLocation: string;
    isSecurityOfficers: boolean;
    noOfSecurityOfficers: number;
    securityServiceProviderName: string;
    isSecurityProviderControlRoomOnSite: boolean;
    controlRoomPhoneNo: string;
    controlRoomPhoneNoCountryCode: string;
    riskDescription: string;
    description: string;
    oldSiteHazardId?: string;
    addressId?: string;
}
class CellPanicAddEditModel extends CommonModels {
    constructor(cellPanicAddEditModel?: CellPanicAddEditModel) {
        super(cellPanicAddEditModel);
        this.cellPanicId = cellPanicAddEditModel ? cellPanicAddEditModel.cellPanicId == undefined ? '' : cellPanicAddEditModel.cellPanicId : '';
        this.contractId = cellPanicAddEditModel ? cellPanicAddEditModel.contractId == undefined ? '' : cellPanicAddEditModel.contractId : '';
        this.userName = cellPanicAddEditModel ? cellPanicAddEditModel.userName == undefined ? '' : cellPanicAddEditModel.userName : '';
        this.mobileNo = cellPanicAddEditModel ? cellPanicAddEditModel.mobileNo == undefined ? '' : cellPanicAddEditModel.mobileNo : '';
        this.mobileNoCountryCode = cellPanicAddEditModel ? cellPanicAddEditModel.mobileNoCountryCode == undefined ? '+27' : cellPanicAddEditModel.mobileNoCountryCode : '+27';
        this.isActive = cellPanicAddEditModel ? cellPanicAddEditModel.isActive == undefined ? true : cellPanicAddEditModel.isActive : true;
    }
    cellPanicId?: string;
    contractId: string;
    userName: string;
    mobileNo: string;
    mobileNoCountryCode: string;
    isActive: boolean;
}

const initialWorkingDays = [{ isChecked: false, name: "Monday" },
{ isChecked: false, name: "Tuesday" }, { isChecked: false, name: "Wednesday" }, { isChecked: false, name: "Thursday" },
{ isChecked: false, name: "Friday" }, { isChecked: false, name: "Saturday" }, { isChecked: false, name: "Sunday" }];
class DomesticWorkersAddEditModel extends CommonModels {
    constructor(domesticWorkersAddEditModel?: DomesticWorkersAddEditModel) {
        super(domesticWorkersAddEditModel);
        this.domesticWorkerTypeId = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.domesticWorkerTypeId == undefined ? '' :
            domesticWorkersAddEditModel.domesticWorkerTypeId : '';
        this.domesticWorkerTypeName = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.domesticWorkerTypeName == undefined ? '' :
            domesticWorkersAddEditModel.domesticWorkerTypeName : '';
        this.domesticWorkerId = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.domesticWorkerId == undefined ? '' :
            domesticWorkersAddEditModel.domesticWorkerId : '';
        this.employeeName = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.employeeName == undefined ? '' : domesticWorkersAddEditModel.employeeName : '';
        this.contactNo = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.contactNo == undefined ? '' : domesticWorkersAddEditModel.contactNo : '';
        this.domesticWorkerName = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.domesticWorkerName == undefined ? '' : domesticWorkersAddEditModel.domesticWorkerName : '';
        this.identityNo = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.identityNo == undefined ? "" : domesticWorkersAddEditModel.identityNo : "";
        this.isSleepIn = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.isSleepIn == undefined ? false : domesticWorkersAddEditModel.isSleepIn : false;
        this.tempWorkingDays = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.tempWorkingDays == undefined ? initialWorkingDays : domesticWorkersAddEditModel.tempWorkingDays : initialWorkingDays;
        this.days = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.days == undefined ? "" : domesticWorkersAddEditModel.days : "";
        this.contactNoCountryCode = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.contactNoCountryCode == undefined ? defaultCountryCode : domesticWorkersAddEditModel.contactNoCountryCode : defaultCountryCode;
        this.tempGUID = domesticWorkersAddEditModel ? domesticWorkersAddEditModel.tempGUID == undefined ? "" : domesticWorkersAddEditModel.tempGUID : "";
    }
    domesticWorkerId?: string;
    employeeName: string;
    identityNo: string;
    contactNo: string;
    contactNoCountryCode: string;
    domesticWorkerTypeName?: string;
    domesticWorkerTypeId?: string;
    domesticWorkerName?: string;
    isSleepIn?: boolean | string = true;
    tempWorkingDays = [];
    days?: string;
    tempGUID?: string;
}
class OpenCloseMonitoringAddEditModel extends CommonModels {
    constructor(openCloseMonitoringAddEditModel?: OpenCloseMonitoringAddEditModel) {
        super(openCloseMonitoringAddEditModel);
        this.mondayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.mondayArmingTime == undefined ? '' : openCloseMonitoringAddEditModel.mondayArmingTime : '';
        this.mondayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.mondayDisarmingTime == undefined ? '' : openCloseMonitoringAddEditModel.mondayDisarmingTime : '';
        this.tuesdayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.tuesdayArmingTime == undefined ? '' : openCloseMonitoringAddEditModel.tuesdayArmingTime : '';
        this.tuesdayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.tuesdayDisarmingTime == undefined ? '' : openCloseMonitoringAddEditModel.tuesdayDisarmingTime : '';
        this.wednesdayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.wednesdayArmingTime == undefined ? "" : openCloseMonitoringAddEditModel.wednesdayArmingTime : "";
        this.wednesdayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.wednesdayDisarmingTime == undefined ? "" : openCloseMonitoringAddEditModel.wednesdayDisarmingTime : "";
        this.thursdayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.thursdayArmingTime == undefined ? "" : openCloseMonitoringAddEditModel.thursdayArmingTime : "";
        this.thursdayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.thursdayDisarmingTime == undefined ? "" : openCloseMonitoringAddEditModel.thursdayDisarmingTime : "";
        this.fridayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.fridayArmingTime == undefined ? "" : openCloseMonitoringAddEditModel.fridayArmingTime : "";
        this.fridayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.fridayDisarmingTime == undefined ? "" : openCloseMonitoringAddEditModel.fridayDisarmingTime : "";
        this.saturdayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.saturdayArmingTime == undefined ? "" : openCloseMonitoringAddEditModel.saturdayArmingTime : "";
        this.saturdayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.saturdayDisarmingTime == undefined ? "" : openCloseMonitoringAddEditModel.saturdayDisarmingTime : "";
        this.sundayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.sundayArmingTime == undefined ? "" : openCloseMonitoringAddEditModel.sundayArmingTime : "";
        this.sundayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.sundayDisarmingTime == undefined ? "" : openCloseMonitoringAddEditModel.sundayDisarmingTime : "";
        this.publicHolidayArmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.publicHolidayArmingTime == undefined ? "" : openCloseMonitoringAddEditModel.publicHolidayArmingTime : "";
        this.publicHolidayDisarmingTime = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.publicHolidayDisarmingTime == undefined ? "" : openCloseMonitoringAddEditModel.publicHolidayDisarmingTime : "";
        this.openAndCloseMonitoringId = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.openAndCloseMonitoringId == undefined ? "" : openCloseMonitoringAddEditModel.openAndCloseMonitoringId : "";
        this.monthlyReportEmailTo = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.monthlyReportEmailTo == undefined ? "" : openCloseMonitoringAddEditModel.monthlyReportEmailTo : "";
        this.allDays = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.allDays == undefined ? false : openCloseMonitoringAddEditModel.allDays : false;
        this.contacts = openCloseMonitoringAddEditModel == undefined ? [] : openCloseMonitoringAddEditModel.contacts == undefined ? [] : openCloseMonitoringAddEditModel.contacts;
        this.addressId = openCloseMonitoringAddEditModel ? openCloseMonitoringAddEditModel.addressId == undefined ? "" : openCloseMonitoringAddEditModel.addressId : "";
    }
    mondayArmingTime?: string;
    mondayDisarmingTime?: string;
    tuesdayArmingTime?: string;
    tuesdayDisarmingTime?: string;
    wednesdayArmingTime?: string;
    wednesdayDisarmingTime?: string;
    thursdayArmingTime?: string;
    thursdayDisarmingTime?: string;
    fridayArmingTime?: string;
    fridayDisarmingTime?: string;
    saturdayArmingTime?: string;
    saturdayDisarmingTime?: string;
    sundayArmingTime?: string;
    sundayDisarmingTime?: string;
    publicHolidayArmingTime?: string;
    publicHolidayDisarmingTime?: string;
    monthlyReportEmailTo?: string;
    allDays: boolean;
    openAndCloseMonitoringId?: string;
    contacts: OpenCloseContact[];
    addressId?: string;
}
class OpenCloseContact {
    constructor(openCloseContact?: OpenCloseContact) {
        this.openAndCloseMonitoringContactId = openCloseContact == undefined ? "" : openCloseContact.openAndCloseMonitoringContactId == undefined ? "" : openCloseContact.openAndCloseMonitoringContactId;
        this.smsNoCountryCode = openCloseContact == undefined ? "+27" : openCloseContact.smsNoCountryCode == undefined ? "+27" : openCloseContact.smsNoCountryCode;
        this.smsNo = openCloseContact ? openCloseContact.smsNo == undefined ? "" : openCloseContact.smsNo : "";
        this.index = openCloseContact == undefined ? 0 : openCloseContact.index == undefined ? 0 : openCloseContact.index;
        this.currentCursorIndex = openCloseContact == undefined ? 0 : openCloseContact.currentCursorIndex == undefined ? 0 : openCloseContact.currentCursorIndex;
    }
    openAndCloseMonitoringContactId?: string;
    smsNo?: string;
    smsNoCountryCode?: string;
    index?: number;
    currentCursorIndex?: number;
}
class AccessToPremisesAddEditModel extends CommonModels {
    constructor(accessToPremisesAddEditModel?: AccessToPremisesAddEditModel) {
        super(accessToPremisesAddEditModel);
        this.leadId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.leadId == undefined ? "" : accessToPremisesAddEditModel.leadId : "";
        this.createdUserId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.createdUserId == undefined ? "" : accessToPremisesAddEditModel.createdUserId : "";
        this.customerId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.customerId == undefined ? "" : accessToPremisesAddEditModel.customerId : "";
        this.transmitterId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.transmitterId == undefined ? "" : accessToPremisesAddEditModel.transmitterId : "";
        this.partitionId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.partitionId == undefined ? "" : accessToPremisesAddEditModel.partitionId : "";
        this.receiverId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.receiverId == undefined ? "" : accessToPremisesAddEditModel.receiverId : "";
        this.isArmedResponsePadlock = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isArmedResponsePadlock == undefined ? false : accessToPremisesAddEditModel.isArmedResponsePadlock : false;
        this.isAuthroziedNoAccess = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isAuthroziedNoAccess == undefined ? false : accessToPremisesAddEditModel.isAuthroziedNoAccess : false;
        this.accessPremiseId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.accessPremiseId == undefined ? "" : accessToPremisesAddEditModel.accessPremiseId : "";
        this.padlockAccessCode = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.padlockAccessCode == undefined ? "" : accessToPremisesAddEditModel.padlockAccessCode : "";
        this.barrelLockAccessCode = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.barrelLockAccessCode == undefined ? "" : accessToPremisesAddEditModel.barrelLockAccessCode : "";
        this.isPadlockOnGate = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isPadlockOnGate == undefined ? false : accessToPremisesAddEditModel.isPadlockOnGate : false;
        this.padlockDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.padlockDescription == undefined ? "" : accessToPremisesAddEditModel.padlockDescription : "";
        this.isBarrelLockFittedOnGate = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isBarrelLockFittedOnGate == undefined ? false : accessToPremisesAddEditModel.isBarrelLockFittedOnGate : false;
        this.barrelLockFittedDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.barrelLockFittedDescription == undefined ? "" : accessToPremisesAddEditModel.barrelLockFittedDescription : "";
        this.unProtectWallDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.unProtectWallDescription == undefined ? "" : accessToPremisesAddEditModel.unProtectWallDescription : "";
        this.isBarrelLockFitted = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isBarrelLockFitted == undefined ? false : accessToPremisesAddEditModel.isBarrelLockFitted : false;
        this.isDigipad = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isDigipad == undefined ? false : accessToPremisesAddEditModel.isDigipad : false;
        this.isLockBox = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isLockBox == undefined ? false : accessToPremisesAddEditModel.isLockBox : false;
        this.isAuthorisedNoAccess = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isAuthorisedNoAccess == undefined ? false : accessToPremisesAddEditModel.isAuthorisedNoAccess : false;
        this.isArmedResponseReceiverOnGate = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isArmedResponseReceiverOnGate == undefined ? false : accessToPremisesAddEditModel.isArmedResponseReceiverOnGate : false;
        this.isUnProtectWall = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isUnProtectWall == undefined ? false : accessToPremisesAddEditModel.isUnProtectWall : false;
        this.isVirtualAgentService = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isVirtualAgentService == undefined ? false : accessToPremisesAddEditModel.isVirtualAgentService : false;
        this.siteInstructionForAROfficer = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.siteInstructionForAROfficer == undefined ? null : accessToPremisesAddEditModel.siteInstructionForAROfficer : null;
        this.isSecureGate = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isSecureGate == undefined ? false : accessToPremisesAddEditModel.isSecureGate : false;
        this.isOpenAccess = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isOpenAccess == undefined ? false : accessToPremisesAddEditModel.isOpenAccess : false;
        this.authroziedNoAccessDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.authroziedNoAccessDescription == undefined ? "" : accessToPremisesAddEditModel.authroziedNoAccessDescription : "";
        this.barrelLockComments = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.barrelLockComments == undefined ? "" : accessToPremisesAddEditModel.barrelLockComments : "";
        this.digipadComments = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.digipadComments == undefined ? "" : accessToPremisesAddEditModel.digipadComments : "";
        this.digipadAccessCode = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.digipadAccessCode == undefined ? "" : accessToPremisesAddEditModel.digipadAccessCode : "";
        this.lockBoxAccessCode = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.lockBoxAccessCode == undefined ? "" : accessToPremisesAddEditModel.lockBoxAccessCode : "";
        this.isPadlockOnDoor = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isPadlockOnDoor == undefined ? false : accessToPremisesAddEditModel.isPadlockOnDoor : false;
        this.isDigipadOnGate = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isDigipadOnGate == undefined ? false : accessToPremisesAddEditModel.isDigipadOnGate : false;
        this.isLockBox = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isLockBox == undefined ? false : accessToPremisesAddEditModel.isLockBox : false;
        this.isBarrelLockOnDoor = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isBarrelLockOnDoor == undefined ? false : accessToPremisesAddEditModel.isBarrelLockOnDoor : false;
        this.isDigipadOnDoor = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isDigipadOnDoor == undefined ? false : accessToPremisesAddEditModel.isDigipadOnDoor : false;
        this.isLockBoxOnDoor = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.isLockBoxOnDoor == undefined ? false : accessToPremisesAddEditModel.isLockBoxOnDoor : false;
        this.openAccessDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.openAccessDescription == undefined ? "" : accessToPremisesAddEditModel.openAccessDescription : "";
        this.secureGateDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.secureGateDescription == undefined ? "" : accessToPremisesAddEditModel.secureGateDescription : "";
        this.lockBoxComments = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.lockBoxComments == undefined ? "" : accessToPremisesAddEditModel.lockBoxComments : "";
        this.armedResponseReceiverOnGateDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.armedResponseReceiverOnGateDescription == undefined ? "" : accessToPremisesAddEditModel.armedResponseReceiverOnGateDescription : "";
        this.lockBoxDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.lockBoxDescription == undefined ? "" : accessToPremisesAddEditModel.lockBoxDescription : "";
        this.digipadDescription = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.digipadDescription == undefined ? "" : accessToPremisesAddEditModel.digipadDescription : "";
        this.oldAccessPremiseId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.oldAccessPremiseId == undefined ? "" : accessToPremisesAddEditModel.oldAccessPremiseId : "";
        this.addressId = accessToPremisesAddEditModel ? accessToPremisesAddEditModel.addressId == undefined ? "" : accessToPremisesAddEditModel.addressId : "";
    }
    leadId: string;
    createdUserId: string;
    transmitterId: string;
    receiverId: string;
    partitionId: string;
    customerId: string;
    unProtectWallDescription: string;
    accessPremiseId: string;
    isPadlockOnGate?: boolean;
    isArmedResponsePadlock?: boolean;
    padlockAccessCode: string;
    isBarrelLockFitted?: boolean;
    isBarrelLockFittedOnGate?: boolean;
    barrelLockFittedDescription: string;
    barrelLockAccessCode: string;
    isDigipad?: boolean;
    digipadAccessCode: string;
    digipadDescription: string;
    lockBoxDescription: string;
    isLockBox?: boolean;
    lockBoxAccessCode: string;
    isAuthorisedNoAccess?: boolean;
    siteInstructionForAROfficer: string;
    isAuthroziedNoAccess: boolean;
    authroziedNoAccessDescription: string;
    padlockDescription: string;
    barrelLockComments: string;
    digipadComments: string;
    isArmedResponseReceiverOnGate?: boolean;
    isSecureGate: boolean;
    isDigipadOnGate: boolean;
    isUnProtectWall: boolean;
    isOpenAccess: boolean;
    isVirtualAgentService: boolean;
    isPadlockOnDoor: boolean;
    isBarrelLockOnDoor: boolean;
    isDigipadOnDoor: boolean;
    isLockBoxOnDoor: boolean;
    openAccessDescription: string;
    secureGateDescription: string;
    lockBoxComments: string;
    armedResponseReceiverOnGateDescription: string;
    digipads: DigiPadModel[];
    lockBoxes: LockBoxesModel[];
    oldAccessPremiseId?: string;
    addressId?: string;
}
export class DigiPadModel {
    constructor(digiPadModel?: DigiPadModel) {
        this.accessPremiseDigipadId = digiPadModel == undefined ? "" : digiPadModel.accessPremiseDigipadId == undefined ? "" : digiPadModel.accessPremiseDigipadId;
        this.digipadAccessCode = digiPadModel == undefined ? "" : digiPadModel.digipadAccessCode == undefined ? "" : digiPadModel.digipadAccessCode;
        this.gateName = digiPadModel == undefined ? "" : digiPadModel.gateName == undefined ? "" : digiPadModel.gateName;
        this.isPasswordShow = digiPadModel == undefined ? false : digiPadModel.isPasswordShow == undefined ? false : digiPadModel.isPasswordShow;
    }
    accessPremiseDigipadId: string;
    digipadAccessCode: string;
    gateName: string;
    isPasswordShow: boolean;
}
export class LockBoxesModel {
    constructor(lockBoxesModel?: LockBoxesModel) {
        this.accessPremiseLockBoxId = lockBoxesModel == undefined ? "" : lockBoxesModel.accessPremiseLockBoxId == undefined ? "" : lockBoxesModel.accessPremiseLockBoxId;
        this.lockBoxAccessCode = lockBoxesModel == undefined ? "" : lockBoxesModel.lockBoxAccessCode == undefined ? "" : lockBoxesModel.lockBoxAccessCode;
        this.gateName = lockBoxesModel == undefined ? "" : lockBoxesModel.gateName == undefined ? "" : lockBoxesModel.gateName;
        this.isPasswordShow = lockBoxesModel == undefined ? false : lockBoxesModel.isPasswordShow == undefined ? false : lockBoxesModel.isPasswordShow;
    }
    accessPremiseLockBoxId: string;
    lockBoxAccessCode: string;
    gateName: string;
    isPasswordShow: boolean;
}
class ServicesAndChargesAddEditModel extends CommonModels {
    constructor(servicesAndChargesAddEditModel?: ServicesAndChargesAddEditModel) {
        super(servicesAndChargesAddEditModel);
        this.serviceChargesId = servicesAndChargesAddEditModel ? servicesAndChargesAddEditModel.serviceChargesId == undefined ? "" : servicesAndChargesAddEditModel.serviceChargesId : "";
        this.serviceMappingId = servicesAndChargesAddEditModel ? servicesAndChargesAddEditModel.serviceMappingId == undefined ? "" : servicesAndChargesAddEditModel.serviceMappingId : "";
        this.value = servicesAndChargesAddEditModel ? servicesAndChargesAddEditModel.value == undefined ? "" : servicesAndChargesAddEditModel.value : "";
        this.isVirtualAgentService = servicesAndChargesAddEditModel ? servicesAndChargesAddEditModel.isVirtualAgentService == undefined ? true : servicesAndChargesAddEditModel.isVirtualAgentService : true;
        this.padlockOnDoor = servicesAndChargesAddEditModel ? servicesAndChargesAddEditModel.padlockOnDoor == undefined ? true : servicesAndChargesAddEditModel.padlockOnDoor : true;
        this.barrelLockOnDoor = servicesAndChargesAddEditModel ? servicesAndChargesAddEditModel.barrelLockOnDoor == undefined ? false : servicesAndChargesAddEditModel.barrelLockOnDoor : false;
        this.digipadOnDoor = servicesAndChargesAddEditModel ? servicesAndChargesAddEditModel.digipadOnDoor == undefined ? true : servicesAndChargesAddEditModel.digipadOnDoor : true;
        this.lockBoxOnDoor = servicesAndChargesAddEditModel ? servicesAndChargesAddEditModel.lockBoxOnDoor == undefined ? false : servicesAndChargesAddEditModel.lockBoxOnDoor : false;
    }
    serviceChargesId?: string;
    serviceMappingId: string;
    value: string;
    isVirtualAgentService: boolean;
    padlockOnDoor: boolean;
    barrelLockOnDoor: boolean;
    digipadOnDoor: boolean;
    lockBoxOnDoor: boolean;
}
class AccountHolderDetailAddEditModel extends CommonModels {
    constructor(accountHolderDetailAddEditModel?: AccountHolderDetailAddEditModel) {
        super(accountHolderDetailAddEditModel);
        this.accountHolderId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.accountHolderId == undefined ? null : accountHolderDetailAddEditModel.accountHolderId : null;
        this.parentId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.parentId == undefined ? "" : accountHolderDetailAddEditModel.parentId : "";
        this.accountHolderName = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.accountHolderName == undefined ? "" : accountHolderDetailAddEditModel.accountHolderName : "";
        this.surname = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.surname == undefined ? "" : accountHolderDetailAddEditModel.surname : "";
        this.said = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.said == undefined ? "" : accountHolderDetailAddEditModel.said : "";
        this.bankId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.bankId == undefined ? "" : accountHolderDetailAddEditModel.bankId : "";
        this.accountTypeId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.accountTypeId == undefined ? "" : accountHolderDetailAddEditModel.accountTypeId : "";
        this.debitDate = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.debitDate == undefined ? '0' : accountHolderDetailAddEditModel.debitDate : "0";
        this.accountNumber = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.accountNumber == undefined ? "" : accountHolderDetailAddEditModel.accountNumber : "";
        this.bankBranchId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.bankBranchId == undefined ? "" : accountHolderDetailAddEditModel.bankBranchId : "";
        this.contactName = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.contactName == undefined ? "" : accountHolderDetailAddEditModel.contactName : "";
        this.contactNumber = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.contactNumber == undefined ? "" : accountHolderDetailAddEditModel.contactNumber : "";
        this.contactNumberCountryCode = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.contactNumberCountryCode == undefined ? defaultCountryCode : accountHolderDetailAddEditModel.contactNumberCountryCode : defaultCountryCode;
        this.email = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.email == undefined ? "" : accountHolderDetailAddEditModel.email : "";
        this.isSameasPrimaryAddress = accountHolderDetailAddEditModel == undefined ? false : accountHolderDetailAddEditModel.isSameasPrimaryAddress == undefined ? false : accountHolderDetailAddEditModel.isSameasPrimaryAddress;
        this.buildingNumber = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.buildingNumber == undefined ? "" : accountHolderDetailAddEditModel.buildingNumber : "";
        this.buildingName = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.buildingName == undefined ? "" : accountHolderDetailAddEditModel.buildingName : "";
        this.address = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.address == undefined ? "" : accountHolderDetailAddEditModel.address : "";
        this.latitude = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.latitude == undefined ? "" : accountHolderDetailAddEditModel.latitude : "";
        this.longitude = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.longitude == undefined ? "" : accountHolderDetailAddEditModel.longitude : "";
        this.latLong = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.latLong == undefined ? "" : accountHolderDetailAddEditModel.latLong : "";
        this.isNewDebtor = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.isNewDebtor == undefined ? true : accountHolderDetailAddEditModel.isNewDebtor : true;
        this.isGroupInvoice = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.isGroupInvoice == undefined ? false : accountHolderDetailAddEditModel.isGroupInvoice : false;
        this.isSendCustomerCopy = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.isSendCustomerCopy == undefined ? true : accountHolderDetailAddEditModel.isSendCustomerCopy : false;
        this.debtorPaymentTypeId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.debtorPaymentTypeId == undefined ? "" : accountHolderDetailAddEditModel.debtorPaymentTypeId : "";
        this.vatNo = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.vatNo == undefined ? "" : accountHolderDetailAddEditModel.vatNo : "";
        this.postalCode = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.postalCode == undefined ? "" : accountHolderDetailAddEditModel.postalCode : "";
        this.streetAddress = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.streetAddress == undefined ? "" : accountHolderDetailAddEditModel.streetAddress : "";
        this.streetNumber = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.streetNumber == undefined ? "" : accountHolderDetailAddEditModel.streetNumber : "";
        this.cityName = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.cityName == undefined ? "" : accountHolderDetailAddEditModel.cityName : "";
        this.suburbName = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.suburbName == undefined ? "" : accountHolderDetailAddEditModel.suburbName : "";
        this.provinceName = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.provinceName == undefined ? "" : accountHolderDetailAddEditModel.provinceName : "";
        this.districtId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.districtId == undefined ? "" : accountHolderDetailAddEditModel.districtId : "";
        this.leadId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.leadId == undefined ? "" : accountHolderDetailAddEditModel.leadId : "";
        this.createdUserId = accountHolderDetailAddEditModel ? accountHolderDetailAddEditModel.createdUserId == undefined ? "" : accountHolderDetailAddEditModel.createdUserId : "";
    }
    accountHolderId?: string;
    parentId: string;
    accountHolderName: string;
    surname: string;
    said: string;
    bankId: string;
    accountTypeId: string;
    debitDate: string;
    accountNumber: string;
    bankBranchId: string;
    contactName: string;
    contactNumber: string;
    contactNumberCountryCode: string;
    email: string;
    isSameasPrimaryAddress: boolean;
    buildingNumber: string;
    buildingName: string;
    address: string;
    latLong: string;
    latitude: string;
    longitude: string;
    isNewDebtor: boolean;
    isGroupInvoice: boolean;
    debtorPaymentTypeId: string;
    isSendCustomerCopy: boolean;
    vatNo: string;
    postalCode: string;
    streetAddress: string;
    streetNumber: string;
    cityName: string;
    suburbName: string;
    provinceName: string;
    districtId?: string;
    leadId: string;
    createdUserId: string;
}
class InstallationInfoAddEditModel extends CommonModels {
    constructor(installationInfoAddEditModel?: InstallationInfoAddEditModel) {
        super(installationInfoAddEditModel);
        this.installationInfoId = installationInfoAddEditModel ? installationInfoAddEditModel.installationInfoId == undefined ? "" : installationInfoAddEditModel.installationInfoId : "";
        this.nominatedRepId = installationInfoAddEditModel ? installationInfoAddEditModel.nominatedRepId == undefined ? '' : installationInfoAddEditModel.nominatedRepId : '';
        this.nominatedRepName = installationInfoAddEditModel ? installationInfoAddEditModel.nominatedRepName == undefined ? '' : installationInfoAddEditModel.nominatedRepName : '';
        this.tempNominatedRepId = installationInfoAddEditModel ? installationInfoAddEditModel.tempNominatedRepId == undefined ? '' : installationInfoAddEditModel.tempNominatedRepId : '';
        this.panelTypeId = installationInfoAddEditModel ? installationInfoAddEditModel.panelTypeId == undefined ? '' : installationInfoAddEditModel.panelTypeId : '';
        this.itemOwnershipTypeId = installationInfoAddEditModel ? installationInfoAddEditModel.itemOwnershipTypeId == undefined ? '' : installationInfoAddEditModel.itemOwnershipTypeId : ''
        this.noOfRadio = installationInfoAddEditModel ? installationInfoAddEditModel.noOfRadio == undefined ? null : installationInfoAddEditModel.noOfRadio : null;
        this.noOfCommunicator = installationInfoAddEditModel ? installationInfoAddEditModel.noOfCommunicator == undefined ? null : installationInfoAddEditModel.noOfCommunicator : null;
    }
    installationInfoId?: string;
    nominatedRepId?: string;
    nominatedRepName?: string;
    tempNominatedRepId?: string;
    panelTypeId?: string;
    itemOwnershipTypeId?: string;
    noOfRadio: number;
    noOfCommunicator: number;
}
class InstallationCheckListAddEditModel extends CommonModels {
    constructor(installationCheckListAddEditModel?: InstallationCheckListAddEditModel) {
        super(installationCheckListAddEditModel);
        this.isBarrelLock = installationCheckListAddEditModel ? installationCheckListAddEditModel.isBarrelLock == undefined ? true : installationCheckListAddEditModel.isBarrelLock : true;
        this.isDigipad = installationCheckListAddEditModel ? installationCheckListAddEditModel.isDigipad == undefined ? false : installationCheckListAddEditModel.isDigipad : false;
        this.isLockBox = installationCheckListAddEditModel ? installationCheckListAddEditModel.isLockBox == undefined ? true : installationCheckListAddEditModel.isLockBox : true;
        this.isAuthorisedNoAccess = installationCheckListAddEditModel ? installationCheckListAddEditModel.isAuthorisedNoAccess == undefined ? false : installationCheckListAddEditModel.isAuthorisedNoAccess : false;
        this.isArmedResponseReceiverOnGate = installationCheckListAddEditModel ? installationCheckListAddEditModel.isArmedResponseReceiverOnGate == undefined ? true : installationCheckListAddEditModel.isArmedResponseReceiverOnGate : true;
        this.isSecureGate = installationCheckListAddEditModel ? installationCheckListAddEditModel.isSecureGate == undefined ? true : installationCheckListAddEditModel.isSecureGate : true;
        this.isUnProtectedWall = installationCheckListAddEditModel ? installationCheckListAddEditModel.isUnProtectedWall == undefined ? true : installationCheckListAddEditModel.isUnProtectedWall : true;
        this.isOpenAccess = installationCheckListAddEditModel ? installationCheckListAddEditModel.isOpenAccess == undefined ? false : installationCheckListAddEditModel.isOpenAccess : false;
        this.isVirtualAgentService = installationCheckListAddEditModel ? installationCheckListAddEditModel.isVirtualAgentService == undefined ? true : installationCheckListAddEditModel.isVirtualAgentService : true;
        this.padlockOnDoor = installationCheckListAddEditModel ? installationCheckListAddEditModel.padlockOnDoor == undefined ? true : installationCheckListAddEditModel.padlockOnDoor : true;
        this.barrelLockOnDoor = installationCheckListAddEditModel ? installationCheckListAddEditModel.barrelLockOnDoor == undefined ? false : installationCheckListAddEditModel.barrelLockOnDoor : false;
        this.digipadOnDoor = installationCheckListAddEditModel ? installationCheckListAddEditModel.digipadOnDoor == undefined ? true : installationCheckListAddEditModel.digipadOnDoor : true;
        this.lockBoxOnDoor = installationCheckListAddEditModel ? installationCheckListAddEditModel.lockBoxOnDoor == undefined ? false : installationCheckListAddEditModel.lockBoxOnDoor : false;
    }
    isBarrelLock: boolean;
    isDigipad: boolean;
    isLockBox: boolean;
    isAuthorisedNoAccess: boolean;
    isArmedResponseReceiverOnGate: boolean;
    isSecureGate: boolean;
    isUnProtectedWall: boolean;
    isOpenAccess: boolean;
    isVirtualAgentService: boolean;
    padlockOnDoor: boolean;
    barrelLockOnDoor: boolean;
    digipadOnDoor: boolean;
    lockBoxOnDoor: boolean;
}
class ServiceDebterModel extends CommonModels {
    constructor(serviceDebterModel?: ServiceDebterModel) {
        super(serviceDebterModel);
        this.leadId = serviceDebterModel ? serviceDebterModel.leadId == undefined ? null : serviceDebterModel.leadId : '';
        this.customerId = serviceDebterModel ? serviceDebterModel.customerId == undefined ? null : serviceDebterModel.customerId : null;
        this.siteTypeId = serviceDebterModel ? serviceDebterModel.siteTypeId == undefined ? null : serviceDebterModel.siteTypeId : null;
        this.debtorTypeId = serviceDebterModel ? serviceDebterModel.debtorTypeId == undefined ? null : serviceDebterModel.debtorTypeId : null;
        this.bdiNumber = serviceDebterModel ? serviceDebterModel.bdiNumber == undefined ? null : serviceDebterModel.bdiNumber : null;
        this.debtorRefNo = serviceDebterModel ? serviceDebterModel.debtorRefNo == undefined ? null : serviceDebterModel.debtorRefNo : null;
        this.titleId = serviceDebterModel ? serviceDebterModel.titleId == undefined ? '' : serviceDebterModel.titleId : '';
        this.firstName = serviceDebterModel ? serviceDebterModel.firstName == undefined ? null : serviceDebterModel.firstName : null;
        this.lastName = serviceDebterModel ? serviceDebterModel.lastName == undefined ? null : serviceDebterModel.lastName : null;
        this.said = serviceDebterModel ? serviceDebterModel.said == undefined ? null : serviceDebterModel.said : null;
        this.email = serviceDebterModel ? serviceDebterModel.email == undefined ? '' : serviceDebterModel.email : '';
        this.companyName = serviceDebterModel ? serviceDebterModel.companyName == undefined ? '' : serviceDebterModel.companyName : '';
        this.companyRegNo = serviceDebterModel ? serviceDebterModel.companyRegNo == undefined ? '' : serviceDebterModel.companyRegNo : '';
        this.companyVATNo = serviceDebterModel ? serviceDebterModel.companyVATNo == undefined ? '' : serviceDebterModel.companyVATNo : '';
        this.addressLine1 = serviceDebterModel ? serviceDebterModel.addressLine1 == undefined ? '' : serviceDebterModel.addressLine1 : '';
        this.addressLine2 = serviceDebterModel ? serviceDebterModel.addressLine2 == undefined ? '' : serviceDebterModel.addressLine2 : '';
        this.addressLine3 = serviceDebterModel ? serviceDebterModel.addressLine3 == undefined ? '' : serviceDebterModel.addressLine3 : '';
        this.addressLine4 = serviceDebterModel ? serviceDebterModel.addressLine4 == undefined ? '' : serviceDebterModel.addressLine4 : '';
        this.mobile1CountryCode = serviceDebterModel ? serviceDebterModel.mobile1CountryCode == undefined ? '+27' : serviceDebterModel.mobile1CountryCode : '+27';
        this.mobile1 = serviceDebterModel ? serviceDebterModel.mobile1 == undefined ? '' : serviceDebterModel.mobile1 : '';
        this.mobile2CountryCode = serviceDebterModel ? serviceDebterModel.mobile2CountryCode == undefined ? '+27' : serviceDebterModel.mobile2CountryCode : '+27';
        this.mobile2 = serviceDebterModel ? serviceDebterModel.mobile2 == undefined ? '' : serviceDebterModel.mobile2 : '';
        this.premisesNoCountryCode = serviceDebterModel ? serviceDebterModel.premisesNoCountryCode == undefined ? '+27' : serviceDebterModel.premisesNoCountryCode : '+27';
        this.premisesNo = serviceDebterModel ? serviceDebterModel.premisesNo == undefined ? '' : serviceDebterModel.premisesNo : '';
        this.officeNoCountryCode = serviceDebterModel ? serviceDebterModel.officeNoCountryCode == undefined ? '+27' : serviceDebterModel.officeNoCountryCode : '+27';
        this.officeNo = serviceDebterModel ? serviceDebterModel.officeNo == undefined ? '' : serviceDebterModel.officeNo : '';
        this.postalCode = serviceDebterModel ? serviceDebterModel.postalCode == undefined ? '' : serviceDebterModel.postalCode : '';
        this.alternateEmail = serviceDebterModel ? serviceDebterModel.alternateEmail == undefined ? null : serviceDebterModel.alternateEmail : null;
        this.debtorId = serviceDebterModel ? serviceDebterModel.debtorId == undefined ? '' : serviceDebterModel.debtorId : '';
        this.debtorAdditionalInfoId = serviceDebterModel ? serviceDebterModel.debtorAdditionalInfoId == undefined ? '' : serviceDebterModel.debtorAdditionalInfoId : '';
        this.billingAddressId = serviceDebterModel ? serviceDebterModel.billingAddressId == undefined ? '' : serviceDebterModel.billingAddressId : '';
        this.debtorContactId = serviceDebterModel ? serviceDebterModel.debtorContactId == undefined ? '' : serviceDebterModel.debtorContactId : '';
        this.contactId = serviceDebterModel ? serviceDebterModel.contactId == undefined ? '' : serviceDebterModel.contactId : '';
        this.createdUserId = serviceDebterModel ? serviceDebterModel.createdUserId == undefined ? '' : serviceDebterModel.createdUserId : '';
        this.modifiedUserId = serviceDebterModel ? serviceDebterModel.modifiedUserId == undefined ? '' : serviceDebterModel.modifiedUserId : '';
        this.isDebtorSameAsCustomer = serviceDebterModel ? serviceDebterModel.isDebtorSameAsCustomer == undefined ? false : serviceDebterModel.isDebtorSameAsCustomer : false;
        this.isExistingDebtor = serviceDebterModel ? serviceDebterModel.isExistingDebtor == undefined ? false : serviceDebterModel.isExistingDebtor : false;
        this.isEmailCommunication = serviceDebterModel ? serviceDebterModel.isEmailCommunication == undefined ? false : serviceDebterModel.isEmailCommunication : false;
        this.isSMSCommunication = serviceDebterModel ? serviceDebterModel.isSMSCommunication == undefined ? false : serviceDebterModel.isSMSCommunication : false;
        this.isPhoneCommunication = serviceDebterModel ? serviceDebterModel.isPhoneCommunication == undefined ? false : serviceDebterModel.isPhoneCommunication : false;
        this.isPostCommunication = serviceDebterModel ? serviceDebterModel.isPostCommunication == undefined ? false : serviceDebterModel.isPostCommunication : false;
        this.isBillingSameAsSiteAddress = serviceDebterModel ? serviceDebterModel.isBillingSameAsSiteAddress == undefined ? false : serviceDebterModel.isBillingSameAsSiteAddress : false;
        this.isLinkDebtor = serviceDebterModel ? serviceDebterModel.isLinkDebtor == undefined ? false : serviceDebterModel.isLinkDebtor : false;
        this.linkContractDebtor = serviceDebterModel ? serviceDebterModel.linkContractDebtor == undefined ? null : serviceDebterModel.linkContractDebtor : null;
    }
    isDebtorSameAsCustomer: boolean;
    isLinkDebtor: boolean;
    isExistingDebtor: boolean;
    isEmailCommunication: boolean;
    isSMSCommunication: boolean;
    isPhoneCommunication: boolean;
    isPostCommunication: boolean;
    isBillingSameAsSiteAddress: boolean;
    customerId: string;
    leadId: string;
    siteTypeId: string;
    debtorTypeId: string;
    bdiNumber: string;
    debtorRefNo: string;
    titleId: string;
    firstName: string;
    lastName: string;
    said: string;
    email: string;
    modifiedUserId: string;
    companyName: string;
    companyRegNo: string;
    companyVATNo: string;
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
    addressLine4: string;
    mobile1CountryCode: string;
    mobile1: string;
    mobile2CountryCode: string;
    mobile2: string;
    premisesNoCountryCode: string;
    premisesNo: string;
    officeNo: string;
    officeNoCountryCode: string;
    postalCode: string;
    alternateEmail: string;
    debtorId: string;
    debtorAdditionalInfoId: string;
    billingAddressId: string;
    debtorContactId: string;
    contactId: string;
    createdUserId: string;
    linkContractDebtor;
}
class SearchDebterModel {
    constructor(searchDebterModel?: SearchDebterModel) {
        this.firstName = searchDebterModel ? searchDebterModel.firstName == undefined ? '' : searchDebterModel.firstName : '';
        this.lastName = searchDebterModel ? searchDebterModel.lastName == undefined ? '' : searchDebterModel.lastName : '';
        this.said = searchDebterModel ? searchDebterModel.said == undefined ? '' : searchDebterModel.said : '';
        this.phoneNumber = searchDebterModel ? searchDebterModel.phoneNumber == undefined ? '' : searchDebterModel.phoneNumber : '';
        this.customerNumber = searchDebterModel ? searchDebterModel.customerNumber == undefined ? '' : searchDebterModel.customerNumber : '';
        this.companyVATNo = searchDebterModel ? searchDebterModel.companyVATNo == undefined ? '' : searchDebterModel.companyVATNo : '';
        this.companyName = searchDebterModel ? searchDebterModel.companyName == undefined ? '' : searchDebterModel.companyName : '';
        this.BDINumber = searchDebterModel ? searchDebterModel.BDINumber == undefined ? '' : searchDebterModel.BDINumber : '';
        this.companyRegNo = searchDebterModel ? searchDebterModel.companyRegNo == undefined ? '' : searchDebterModel.companyRegNo : '';
        this.bankId = searchDebterModel ? searchDebterModel.bankId == undefined ? '' : searchDebterModel.bankId : '';
        this.debtorRefNo = searchDebterModel ? searchDebterModel.debtorRefNo == undefined ? '' : searchDebterModel.debtorRefNo : '';
        this.accountNo = searchDebterModel ? searchDebterModel.accountNo == undefined ? '' : searchDebterModel.accountNo : '';
        this.contractNumber = searchDebterModel ? searchDebterModel.contractNumber == undefined ? '' : searchDebterModel.contractNumber : '';
        this.customerId = searchDebterModel ? searchDebterModel.customerId == undefined ? '' : searchDebterModel.customerId : '';
        this.email = searchDebterModel ? searchDebterModel.email == undefined ? '' : searchDebterModel.email : '';
    }
    firstName?: string;
    email?: string;
    lastName?: string;
    said?: string;
    phoneNumber?: string;
    customerNumber?: string;
    companyVATNo?: string;
    companyName: string;
    BDINumber: string;
    companyRegNo: string;
    debtorRefNo: string;
    bankId: string;
    customerId: string;
    accountNo: string;
    contractNumber: string;
}
export class PaymentModel {
    constructor(paymentModel?: PaymentModel) {
        this.amount = paymentModel == undefined ? "" : paymentModel.amount == undefined ? "" : paymentModel.amount;
        this.chargeTypeId = paymentModel == undefined ? "" : paymentModel.chargeTypeId == undefined ? "" : paymentModel.chargeTypeId;
        this.chargeTypeName = paymentModel == undefined ? "" : paymentModel.chargeTypeName == undefined ? "" : paymentModel.chargeTypeName;
        this.isFirstBillDate = paymentModel == undefined ? false : paymentModel.isFirstBillDate == undefined ? false : paymentModel.isFirstBillDate;
        this.isInitialDeposit = paymentModel == undefined ? true : paymentModel.isInitialDeposit == undefined ? true : paymentModel.isInitialDeposit;
        this.isSpecialDebitOrderAfterFirstSignal = paymentModel == undefined ? false : paymentModel.isSpecialDebitOrderAfterFirstSignal == undefined ? false : paymentModel.isSpecialDebitOrderAfterFirstSignal;
        this.servicePaymentOptionId = paymentModel == undefined ? "" : paymentModel.servicePaymentOptionId == undefined ? "" : paymentModel.servicePaymentOptionId;
    }
    amount: string;
    chargeTypeId: string;
    chargeTypeName: string;
    isFirstBillDate: boolean;
    isInitialDeposit: boolean;
    isSpecialDebitOrderAfterFirstSignal: boolean;
    servicePaymentOptionId: string;
}
export class InstallationPriceModel {
    constructor(installationPriceModel?: InstallationPriceModel) {
        this.installationPaymentOptionId = installationPriceModel == undefined ? null : installationPriceModel.installationPaymentOptionId == undefined ? null : installationPriceModel.installationPaymentOptionId;
        this.installationFeeId = installationPriceModel == undefined ? null : installationPriceModel.installationFeeId == undefined ? null : installationPriceModel.installationFeeId;
        this.paymentMethodId = installationPriceModel == undefined ? '' : installationPriceModel.paymentMethodId == undefined ? '' : installationPriceModel.paymentMethodId;
        this.paymentPercentageId = installationPriceModel == undefined ? '' : installationPriceModel.paymentPercentageId == undefined ? '' : installationPriceModel.paymentPercentageId;
        this.paymentDate = installationPriceModel == undefined ? '' : installationPriceModel.paymentDate == undefined ? '' : installationPriceModel.paymentDate;
        this.createdUserId = installationPriceModel == undefined ? '' : installationPriceModel.createdUserId == undefined ? '' : installationPriceModel.createdUserId;
        this.isProofOfPaymentRequired = installationPriceModel == undefined ? false : installationPriceModel.isProofOfPaymentRequired == undefined ? false : installationPriceModel.isProofOfPaymentRequired;
        this.paymentOptionId = installationPriceModel == undefined ? "" : installationPriceModel.paymentOptionId == undefined ? "" : installationPriceModel.paymentOptionId;
        this.proofOfPaymentDocumentPath = installationPriceModel == undefined ? "" : installationPriceModel.proofOfPaymentDocumentPath == undefined ? "" : installationPriceModel.proofOfPaymentDocumentPath;
        this.shouldDisable = installationPriceModel == undefined ? true : installationPriceModel.shouldDisable == undefined ? true : installationPriceModel.shouldDisable;
        this.isSelectedDateValid = installationPriceModel == undefined ? false : installationPriceModel.isSelectedDateValid == undefined ? false : installationPriceModel.isSelectedDateValid;
        this.invalidMessage = installationPriceModel == undefined ? "" : installationPriceModel.invalidMessage == undefined ? "" : installationPriceModel.invalidMessage;
    }
    installationPaymentOptionId: string;
    installationFeeId: string;
    paymentMethodId: string;
    paymentOptionId: string;
    createdUserId: string;
    paymentDate: string
    paymentPercentageId: string;
    purchaseOrderDate: string;
    isProofOfPaymentRequired: boolean;
    proofOfPaymentDocumentPath: string;
    shouldDisable: boolean;
    isSelectedDateValid: false;
    invalidMessage: string;
}
class ServiceFeeModel {
    constructor(serviceFeeModel?: ServiceFeeModel) {
        this.serviceFeeId = serviceFeeModel ? serviceFeeModel.serviceFeeId == undefined ? null : serviceFeeModel.serviceFeeId : null;
        this.processSaleId = serviceFeeModel ? serviceFeeModel.processSaleId == undefined ? null : serviceFeeModel.processSaleId : null;
        this.monthlyFee = serviceFeeModel ? serviceFeeModel.monthlyFee == undefined ? null : serviceFeeModel.monthlyFee : null;
        this.billingIntervalId = serviceFeeModel ? serviceFeeModel.billingIntervalId == undefined ? null : serviceFeeModel.billingIntervalId : null;
        this.paymentMethodId = serviceFeeModel ? serviceFeeModel.paymentMethodId == undefined ? '' : serviceFeeModel.paymentMethodId : '';
        this.isProFormaInvoiceRequired = serviceFeeModel ? serviceFeeModel.isProFormaInvoiceRequired == undefined ? false : serviceFeeModel.isProFormaInvoiceRequired : false;
        this.isPurchaseOrderExist = serviceFeeModel ? serviceFeeModel.isPurchaseOrderExist == undefined ? false : serviceFeeModel.isPurchaseOrderExist : false;
        this.purchaseOrderNo = serviceFeeModel ? serviceFeeModel.purchaseOrderNo == undefined ? null : serviceFeeModel.purchaseOrderNo : null;
        this.purchaseOrderDate = serviceFeeModel ? serviceFeeModel.purchaseOrderDate == undefined ? null : serviceFeeModel.purchaseOrderDate : null;
        this.paymentDay = serviceFeeModel ? serviceFeeModel.paymentDay == undefined ? null : serviceFeeModel.paymentDay : null;
        this.billingIntervalName = serviceFeeModel ? serviceFeeModel.billingIntervalName == undefined ? null : serviceFeeModel.billingIntervalName : null;
        this.createdUserId = serviceFeeModel ? serviceFeeModel.createdUserId == undefined ? null : serviceFeeModel.createdUserId : null;
        this.accountHolderName = serviceFeeModel ? serviceFeeModel.accountHolderName == undefined ? null : serviceFeeModel.accountHolderName : null;
        this.accountNo = serviceFeeModel ? serviceFeeModel.accountNo == undefined ? null : serviceFeeModel.accountNo : null;
        this.accountTypeName = serviceFeeModel ? serviceFeeModel.accountTypeName == undefined ? null : serviceFeeModel.accountTypeName : null;
        this.bankBranchCode = serviceFeeModel ? serviceFeeModel.bankBranchCode == undefined ? null : serviceFeeModel.bankBranchCode : null;
        this.bankName = serviceFeeModel ? serviceFeeModel.bankName == undefined ? null : serviceFeeModel.bankName : null;
        this.bdiNumber = serviceFeeModel ? serviceFeeModel.bdiNumber == undefined ? null : serviceFeeModel.bdiNumber : null;
        this.cardNo = serviceFeeModel ? serviceFeeModel.cardNo == undefined ? null : serviceFeeModel.cardNo : null;
        this.cardTypeName = serviceFeeModel ? serviceFeeModel.cardTypeName == undefined ? null : serviceFeeModel.cardTypeName : null;
        this.debtorName = serviceFeeModel ? serviceFeeModel.debtorName == undefined ? null : serviceFeeModel.debtorName : null;
        this.expiryDate = serviceFeeModel ? serviceFeeModel.expiryDate == undefined ? null : serviceFeeModel.expiryDate : null;
        this.paymentMethodName = serviceFeeModel ? serviceFeeModel.paymentMethodName == undefined ? null : serviceFeeModel.paymentMethodName : null;
        this.paymentDetails = serviceFeeModel ? serviceFeeModel.paymentDetails == undefined ? [] : serviceFeeModel.paymentDetails : [];
    }
    serviceFeeId?: string;
    cardTypeName: string;
    billingIntervalName?: string;
    processSaleId?: string;
    monthlyFee?: string;
    billingIntervalId?: string;
    paymentMethodId?: string;
    isProFormaInvoiceRequired?: boolean;
    isPurchaseOrderExist?: boolean;
    purchaseOrderNo?: string;
    purchaseOrderDate?: string;
    paymentDay?: string;
    createdUserId?: string;
    paymentDetails: PaymentModel[];
    accountHolderName?: string;
    accountNo?: string;
    accountTypeName?: string;
    bankBranchCode?: string;
    bankName?: string;
    bdiNumber?: string;
    cardNo?: string;
    debtorName?: string;
    expiryDate?: string;
    paymentMethodName?: string;
}
export class InstallationFeeModel {
    constructor(installationFeeModel?: InstallationFeeModel) {
        this.installationFeeId = installationFeeModel == undefined ? null : installationFeeModel.installationFeeId == undefined ? null : installationFeeModel.installationFeeId;
        this.processSaleId = installationFeeModel == undefined ? null : installationFeeModel.processSaleId == undefined ? null : installationFeeModel.processSaleId;
        this.installationFee = installationFeeModel == undefined ? null : installationFeeModel.installationFee == undefined ? null : installationFeeModel.installationFee;
        this.paymentMethodId = installationFeeModel == undefined ? null : installationFeeModel.paymentMethodId == undefined ? null : installationFeeModel.paymentMethodId;
        this.paymentOptionId = installationFeeModel == undefined ? '' : installationFeeModel.paymentOptionId == undefined ? '' : installationFeeModel.paymentOptionId;
        this.isProFormaInvoiceRequired = installationFeeModel == undefined ? false : installationFeeModel.isProFormaInvoiceRequired == undefined ? false : installationFeeModel.isProFormaInvoiceRequired;
        this.isPurchaseOrderExist = installationFeeModel == undefined ? false : installationFeeModel.isPurchaseOrderExist == undefined ? false : installationFeeModel.isPurchaseOrderExist;
        this.purchaseOrderNo = installationFeeModel == undefined ? "" : installationFeeModel.purchaseOrderNo == undefined ? "" : installationFeeModel.purchaseOrderNo;
        this.installationPaymentOptions = installationFeeModel ? installationFeeModel.installationPaymentOptions == undefined ? [] : installationFeeModel.installationPaymentOptions : [];
        this.purchaseOrderDate = installationFeeModel ? installationFeeModel.purchaseOrderDate == undefined ? null : installationFeeModel.purchaseOrderDate : null;
        this.createdUserId = installationFeeModel ? installationFeeModel.createdUserId == undefined ? null : installationFeeModel.createdUserId : null;
        this.accountHolderName = installationFeeModel ? installationFeeModel.accountHolderName == undefined ? null : installationFeeModel.accountHolderName : null;
        this.accountNo = installationFeeModel ? installationFeeModel.accountNo == undefined ? null : installationFeeModel.accountNo : null;
        this.accountTypeName = installationFeeModel ? installationFeeModel.accountTypeName == undefined ? null : installationFeeModel.accountTypeName : null;
        this.bankBranchCode = installationFeeModel ? installationFeeModel.bankBranchCode == undefined ? null : installationFeeModel.bankBranchCode : null;
        this.bankName = installationFeeModel ? installationFeeModel.bankName == undefined ? null : installationFeeModel.bankName : null;
        this.bdiNumber = installationFeeModel ? installationFeeModel.bdiNumber == undefined ? null : installationFeeModel.bdiNumber : null;
        this.cardNo = installationFeeModel ? installationFeeModel.cardNo == undefined ? null : installationFeeModel.cardNo : null;
        this.cardTypeName = installationFeeModel ? installationFeeModel.cardTypeName == undefined ? null : installationFeeModel.cardTypeName : null;
        this.recurringDebtName = installationFeeModel ? installationFeeModel.recurringDebtName == undefined ? null : installationFeeModel.recurringDebtName : null;
        this.expiryDate = installationFeeModel ? installationFeeModel.expiryDate == undefined ? null : installationFeeModel.expiryDate : null;
        this.paymentMethodName = installationFeeModel ? installationFeeModel.paymentMethodName == undefined ? null : installationFeeModel.paymentMethodName : null;
    }
    installationFeeId: string;
    cardTypeName: string;
    purchaseOrderDate: string;
    createdUserId: string;
    processSaleId: string;
    installationFee: string;
    paymentMethodId: string;
    paymentOptionId: string;
    isProFormaInvoiceRequired: boolean;
    isPurchaseOrderExist: boolean;
    purchaseOrderNo: string;
    installationPaymentOptions: InstallationPriceModel[];
    accountHolderName?: string;
    accountNo?: string;
    accountTypeName?: string;
    bankBranchCode?: string;
    bankName?: string;
    bdiNumber?: string;
    cardNo?: string;
    recurringDebtName?: string;
    expiryDate?: string;
    paymentMethodName?: string;
}
class CustomerIdentificationModel {
    constructor(customerIdentificationModel?: CustomerIdentificationModel) {
        this.SAID = customerIdentificationModel == undefined ? null : customerIdentificationModel.SAID == undefined ? null : customerIdentificationModel.SAID;
        this.companyRegNo = customerIdentificationModel == undefined ? null : customerIdentificationModel.companyRegNo == undefined ? null : customerIdentificationModel.companyRegNo;
        this.leadId = customerIdentificationModel == undefined ? null : customerIdentificationModel.leadId == undefined ? null : customerIdentificationModel.leadId;
        this.customerId = customerIdentificationModel == undefined ? null : customerIdentificationModel.customerId == undefined ? null : customerIdentificationModel.customerId;
        this.customerTypeId = customerIdentificationModel == undefined ? null : customerIdentificationModel.customerTypeId == undefined ? null : customerIdentificationModel.customerTypeId;
        this.passportNo = customerIdentificationModel == undefined ? null : customerIdentificationModel.passportNo == undefined ? null : customerIdentificationModel.passportNo;
        this.passportExpiryDate = customerIdentificationModel == undefined ? new Date() : customerIdentificationModel.passportExpiryDate == undefined ? new Date() : customerIdentificationModel.passportExpiryDate;
        this.modifiedUserId = customerIdentificationModel == undefined ? null : customerIdentificationModel.modifiedUserId == undefined ? null : customerIdentificationModel.modifiedUserId;
        this.isPassport = customerIdentificationModel == undefined ? false : customerIdentificationModel.isPassport == undefined ? false : customerIdentificationModel.isPassport;
        this.passportExpiryMonth = customerIdentificationModel == undefined ? null : customerIdentificationModel.passportExpiryMonth == undefined ? null : customerIdentificationModel.passportExpiryMonth;
        this.passportExpiryYear = customerIdentificationModel == undefined ? null : customerIdentificationModel.passportExpiryYear == undefined ? null : customerIdentificationModel.passportExpiryYear;
    }
    SAID?: string;
    companyRegNo?: string;
    leadId?: string;
    customerId?: string;
    customerTypeId?: string;
    isPassport?: boolean;
    passportNo?: string;
    passportExpiryMonth: string;
    passportExpiryYear: string;
    passportExpiryDate: Date;
    modifiedUserId: string;
}

export {
    KeyHolderInfoAddEditModel, PasswordsAddEditModel, SiteHazardAddEditModel, CellPanicAddEditModel,
    DomesticWorkersAddEditModel, OpenCloseMonitoringAddEditModel, AccessToPremisesAddEditModel, ServicesAndChargesAddEditModel,
    AccountHolderDetailAddEditModel, InstallationInfoAddEditModel, InstallationCheckListAddEditModel,
    KeyholderObjectModel, EmergencyContactModel, OpenCloseContact,
    InstallationHazardModel, FinduSubscriberModel, ServiceDebterModel, SearchDebterModel, ServiceFeeModel, CustomerIdentificationModel
};

