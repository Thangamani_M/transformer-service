class EquipmentPriceAddEditModel {
    constructor(equipmentPriceAddEditmodel? : EquipmentPriceAddEditModel) {
        this.equipmentPriceId = equipmentPriceAddEditmodel? equipmentPriceAddEditmodel.equipmentPriceId == undefined ? null : equipmentPriceAddEditmodel.equipmentPriceId : null;
        this.plantId = equipmentPriceAddEditmodel? equipmentPriceAddEditmodel.plantId == undefined ? null : equipmentPriceAddEditmodel.plantId : null;
        this.priceListNameId = equipmentPriceAddEditmodel? equipmentPriceAddEditmodel.priceListNameId == undefined ? null : equipmentPriceAddEditmodel.priceListNameId : null;
        this.priceListTypeId = equipmentPriceAddEditmodel? equipmentPriceAddEditmodel.priceListTypeId == undefined ? null : equipmentPriceAddEditmodel.priceListTypeId : null;
        this.labourRate = equipmentPriceAddEditmodel? equipmentPriceAddEditmodel.labourRate == undefined ? null : equipmentPriceAddEditmodel.labourRate : null;
        this.materialMarkup = equipmentPriceAddEditmodel? equipmentPriceAddEditmodel.materialMarkup == undefined ? null : equipmentPriceAddEditmodel.materialMarkup : null;
        this.consMarkup = equipmentPriceAddEditmodel? equipmentPriceAddEditmodel.consMarkup == undefined ? null : equipmentPriceAddEditmodel.consMarkup : null;
    }
    equipmentPriceId? : string;
    plantId? : string;
    priceListNameId? : string;
    priceListTypeId? : string;
    labourRate? : number;
    materialMarkup? : number;
    consMarkup? : number;
}

export {EquipmentPriceAddEditModel};