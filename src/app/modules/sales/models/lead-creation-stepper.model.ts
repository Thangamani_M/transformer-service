import { ContractTypes } from '../shared';
class LeadStepperCreationModel {
  constructor(leadStepperCreationModel?: LeadStepperCreationModel) {
    this.isFormSubmitted = leadStepperCreationModel.isFormSubmitted;
    this.nextStepper = leadStepperCreationModel.nextStepper;
    this.isBackBtnClicked = leadStepperCreationModel.isBackBtnClicked;
    this.isSubmitAliasNextBtnClicked =
      leadStepperCreationModel.isSubmitAliasNextBtnClicked;
    this.stepperParams = leadStepperCreationModel.stepperParams;
  }
  isFormSubmitted?= false;
  nextStepper: number;
  isBackBtnClicked?= false;
  isSubmitAliasNextBtnClicked?= false;
  stepperParams?: StepperParams;
}
class StepperParams {
  constructor(stepperParams?: StepperParams) {
    this.leadId = stepperParams == undefined ? "" : stepperParams.leadId == undefined ? "" : stepperParams.leadId;
    this.rawLeadId = stepperParams == undefined ? "" : stepperParams.rawLeadId == undefined ? "" : stepperParams.rawLeadId;
    this.subUrbId = stepperParams == undefined ? "" : stepperParams.subUrbId == undefined ? "" : stepperParams.subUrbId;
    this.provinceId = stepperParams == undefined ? "" : stepperParams.provinceId == undefined ? "" : stepperParams.provinceId;
    this.cityId = stepperParams == undefined ? "" : stepperParams.cityId == undefined ? "" : stepperParams.cityId;
    this.customerId = stepperParams == undefined ? "" : stepperParams.customerId == undefined ? "" : stepperParams.customerId;
    this.selectedTabIndex = stepperParams == undefined ? 0 : stepperParams.selectedTabIndex == undefined ? 0 : stepperParams.selectedTabIndex;
    this.isleadInfoFormSubmitted = stepperParams == undefined ? false : stepperParams.isleadInfoFormSubmitted == undefined ? false : stepperParams.isleadInfoFormSubmitted;
    this.isServiceInfoFormSubmitted = stepperParams == undefined ? false : stepperParams.isServiceInfoFormSubmitted == undefined ? false : stepperParams.isServiceInfoFormSubmitted;
    this.isItemInfoFormSubmitted = stepperParams == undefined ? false : stepperParams.isItemInfoFormSubmitted == undefined ? false : stepperParams.isItemInfoFormSubmitted;
    this.isQuotationCreationFormSubmitted = stepperParams == undefined ? false : stepperParams.isQuotationCreationFormSubmitted == undefined ? false : stepperParams.isQuotationCreationFormSubmitted;
    this.isTeleSalesFormSubmitted = stepperParams == undefined ? false : stepperParams.isTeleSalesFormSubmitted == undefined ? false : stepperParams.isTeleSalesFormSubmitted;
    this.numberOfValidForms = stepperParams == undefined ? 0 : stepperParams.numberOfValidForms == undefined ? 0 : stepperParams.numberOfValidForms;
    this.serviceInfoFormDataArrayLength = stepperParams == undefined ? 0 : stepperParams.serviceInfoFormDataArrayLength == undefined ? 0 : stepperParams.serviceInfoFormDataArrayLength;
    this.itemInfoFormDataArrayLength = stepperParams == undefined ? 0 : stepperParams.itemInfoFormDataArrayLength == undefined ? 0 : stepperParams.itemInfoFormDataArrayLength;
    this.isFormChangeDetected = stepperParams == undefined ? false : stepperParams.isFormChangeDetected == undefined ? false : stepperParams.isFormChangeDetected;
    this.agreementTabs = stepperParams == undefined ? [] : stepperParams.agreementTabs == undefined ? [] : stepperParams.agreementTabs;
  }
  leadId?= undefined;
  rawLeadId?= undefined;
  subUrbId?= undefined;
  provinceId?= undefined;
  cityId?= undefined;
  selectedTabIndex?= 0;
  isleadInfoFormSubmitted?= false;
  isServiceInfoFormSubmitted?= false;
  isItemInfoFormSubmitted?= false;
  serviceInfoFormDataArrayLength?: number;
  itemInfoFormDataArrayLength?: number;
  isQuotationCreationFormSubmitted?= false;
  isTeleSalesFormSubmitted?= false;
  numberOfValidForms?= 0;
  customerId?= "";
  isFormChangeDetected?: boolean;
  agreementTabs?: [];
}
class LeadHeaderData {
  constructor(leadHeaderData?: LeadHeaderData) {
    this.addressId = leadHeaderData == undefined ? "" : leadHeaderData.addressId == undefined ? "" : leadHeaderData.addressId;
    this.leadId = leadHeaderData == undefined ? "" : leadHeaderData.leadId == undefined ? "" : leadHeaderData.leadId;
    this.leadRefNo = leadHeaderData == undefined ? "" : leadHeaderData.leadRefNo == undefined ? "" : leadHeaderData.leadRefNo;
    this.leadGroupName = leadHeaderData == undefined ? "" : leadHeaderData.leadGroupName == undefined ? "" : leadHeaderData.leadGroupName;
    this.leadGroupId = leadHeaderData == undefined ? null : leadHeaderData.leadGroupId == undefined ? null : leadHeaderData.leadGroupId;
    this.siteTypeId = leadHeaderData == undefined ? null : leadHeaderData.siteTypeId == undefined ? null : leadHeaderData.siteTypeId;
    this.siteTypeName = leadHeaderData == undefined ? null : leadHeaderData.siteTypeName == undefined ? null : leadHeaderData.siteTypeName;
    this.customerId = leadHeaderData == undefined ? null : leadHeaderData.customerId == undefined ? null : leadHeaderData.customerId;
    this.customerName = leadHeaderData == undefined ? null : leadHeaderData.customerName == undefined ? null : leadHeaderData.customerName;
    this.leadCategoryName = leadHeaderData == undefined ? null : leadHeaderData.leadCategoryName == undefined ? null : leadHeaderData.leadCategoryName;
    this.sourceName = leadHeaderData == undefined ? null : leadHeaderData.sourceName == undefined ? null : leadHeaderData.sourceName;
    this.addressConfidentLevelName = leadHeaderData == undefined ? null : leadHeaderData.addressConfidentLevelName == undefined ? null : leadHeaderData.addressConfidentLevelName;
    this.buildingNo = leadHeaderData == undefined ? null : leadHeaderData.buildingNo == undefined ? null : leadHeaderData.buildingNo;
    this.buildingName = leadHeaderData == undefined ? null : leadHeaderData.buildingName == undefined ? null : leadHeaderData.buildingName;
    this.cityName = leadHeaderData == undefined ? null : leadHeaderData.cityName == undefined ? null : leadHeaderData.cityName;
    this.suburbId = leadHeaderData == undefined ? null : leadHeaderData.suburbId == undefined ? null : leadHeaderData.suburbId;
    this.suburbName = leadHeaderData == undefined ? null : leadHeaderData.suburbName == undefined ? null : leadHeaderData.suburbName;
    this.provinceName = leadHeaderData == undefined ? null : leadHeaderData.provinceName == undefined ? null : leadHeaderData.provinceName;
    this.postalCode = leadHeaderData == undefined ? null : leadHeaderData.postalCode == undefined ? null : leadHeaderData.postalCode;
    this.districtId = leadHeaderData == undefined ? null : leadHeaderData.districtId == undefined ? null : leadHeaderData.districtId;
    this.districtName = leadHeaderData == undefined ? null : leadHeaderData.districtName == undefined ? null : leadHeaderData.districtName;
    this.fullAddress = leadHeaderData == undefined ? null : leadHeaderData.fullAddress == undefined ? null : leadHeaderData.fullAddress;
    this.leadStatusName = leadHeaderData == undefined ? null : leadHeaderData.leadStatusName == undefined ? null : leadHeaderData.leadStatusName;
    this.leadStatusCssClass = leadHeaderData == undefined ? null : leadHeaderData.leadStatusCssClass == undefined ? null : leadHeaderData.leadStatusCssClass;
    this.leadStatusColorCode = leadHeaderData == undefined ? null : leadHeaderData.leadStatusColorCode == undefined ? null : leadHeaderData.leadStatusColorCode;
    this.leadSubStatusName = leadHeaderData == undefined ? null : leadHeaderData.leadSubStatusName == undefined ? null : leadHeaderData.leadSubStatusName;
    this.leadSubStatusCssClass = leadHeaderData == undefined ? null : leadHeaderData.leadSubStatusCssClass == undefined ? null : leadHeaderData.leadSubStatusCssClass;
    this.leadSubStatusColorCode = leadHeaderData == undefined ? null : leadHeaderData.leadSubStatusColorCode == undefined ? null : leadHeaderData.leadSubStatusColorCode;
    this.lssName = leadHeaderData == undefined ? null : leadHeaderData.lssName == undefined ? null : leadHeaderData.lssName;
    this.isServiceAgreementExist = leadHeaderData == undefined ? false : leadHeaderData.isServiceAgreementExist == undefined ? false : leadHeaderData.isServiceAgreementExist;
    this.fromUrl = leadHeaderData == undefined ? "My Leads List" : leadHeaderData.fromUrl == undefined ? "My Leads List" : leadHeaderData.fromUrl;
    this.email = leadHeaderData == undefined ? null : leadHeaderData.email == undefined ? null : leadHeaderData.email;
    this.isSaleCompleted = leadHeaderData == undefined ? false : leadHeaderData.isSaleCompleted == undefined ? false : leadHeaderData.isSaleCompleted;
  }
  addressId?: string;
  leadId?: string;
  email?: string;
  leadRefNo?: string;
  leadGroupName?: string;
  leadGroupId?: number;
  siteTypeId?: number;
  siteTypeName?: string;
  customerId?: string;
  customerName?: string;
  leadCategoryName?: string;
  sourceName?: string;
  addressConfidentLevelName?: string;
  buildingNo?: number;
  buildingName?: string;
  cityName?: string;
  suburbId?: string;
  suburbName?: string;
  provinceName?: string;
  postalCode?: number;
  districtId?: string;
  districtName?: string;
  fullAddress?: string;
  leadStatusName?: string;
  leadStatusCssClass?: string;
  leadStatusColorCode?: string;
  leadSubStatusName?: string;
  leadSubStatusCssClass?: string;
  leadSubStatusColorCode?: string;
  lssName?: string;
  isServiceAgreementExist?: Boolean;
  fromUrl?: string;
  isSaleCompleted?: boolean;
}
class ServiceAgreementStepsParams {
  constructor(stepperParams?: ServiceAgreementStepsParams) {
    this.isAgreementGenerated = stepperParams == undefined ? false : stepperParams.isAgreementGenerated == undefined ? false : stepperParams.isAgreementGenerated;
    this.leadId = stepperParams == undefined ? "" : stepperParams.leadId == undefined ? "" : stepperParams.leadId;
    this.agreementTypeId = stepperParams == undefined ? "" : stepperParams.agreementTypeId == undefined ? "" : stepperParams.agreementTypeId;
    this.createdUserId = stepperParams == undefined ? "" : stepperParams.createdUserId == undefined ? "" : stepperParams.createdUserId;
    this.modifiedUserId = stepperParams == undefined ? "" : stepperParams.modifiedUserId == undefined ? "" : stepperParams.modifiedUserId;
    this.partitionId = stepperParams == undefined ? "" : stepperParams.partitionId == undefined ? "" : stepperParams.partitionId;
    this.quotationVersionId = stepperParams == undefined ? "" : stepperParams.quotationVersionId == undefined ? "" : stepperParams.quotationVersionId;
    this.customerId = stepperParams == undefined ? "" : stepperParams.customerId == undefined ? "" : stepperParams.customerId;
    this.contractType = stepperParams == undefined ? ContractTypes.SERVICE_AGREEMENT : stepperParams.contractType == undefined ? ContractTypes.SERVICE_AGREEMENT : stepperParams.contractType;
    this.agreementTabs = stepperParams == undefined ? [] : stepperParams.agreementTabs == undefined ? [] : stepperParams.agreementTabs;
    this.contractId = stepperParams == undefined ? null : stepperParams.contractId == undefined ? null : stepperParams.contractId;
    this.debtorId = stepperParams == undefined ? null : stepperParams.debtorId == undefined ? null : stepperParams.debtorId;
    this.debtorTypeId = stepperParams == undefined ? null : stepperParams.debtorTypeId == undefined ? null : stepperParams.debtorTypeId;
    this.selectedQuotationVersionIds = stepperParams == undefined ? "" : stepperParams.selectedQuotationVersionIds == undefined ? "" : stepperParams.selectedQuotationVersionIds;
    this.selectedQuotationNumbers = stepperParams == undefined ? "" : stepperParams.selectedQuotationNumbers == undefined ? "" : stepperParams.selectedQuotationNumbers;
    this.debtorAdditionalInfoId = stepperParams == undefined ? "" : stepperParams.debtorAdditionalInfoId == undefined ? "" : stepperParams.debtorAdditionalInfoId;
    this.fromUrl = stepperParams == undefined ? "Leads List" : stepperParams.fromUrl == undefined ? "Leads List" : stepperParams.fromUrl;
    this.isExist = stepperParams == undefined ? false : stepperParams.isExist == undefined ? false : stepperParams.isExist;
    this.isDebtorVetted = stepperParams == undefined ? false : stepperParams.isDebtorVetted == undefined ? false : stepperParams.isDebtorVetted;
    this.isDebtorAccountVetted = stepperParams == undefined ? false : stepperParams.isDebtorAccountVetted == undefined ? false : stepperParams.isDebtorAccountVetted;
    this.isOnInvoicePayment = stepperParams == undefined ? false : stepperParams.isOnInvoicePayment == undefined ? false : stepperParams.isOnInvoicePayment;
    this.addressId = stepperParams == undefined ? "" : stepperParams.addressId == undefined ? "" : stepperParams.addressId;
    this.isItemAndOnInvoiceOnly = stepperParams == undefined ? false : stepperParams.isItemAndOnInvoiceOnly == undefined ? false : stepperParams.isItemAndOnInvoiceOnly;
  }
  isAgreementGenerated?: boolean;
  leadId?: string;
  createdUserId?: string;
  modifiedUserId?: string;
  agreementTypeId?: string;
  partitionId?: string;
  customerId?: string;
  quotationVersionId?: string;
  agreementTabs?;
  contractType?: string;
  contractId?: string;
  debtorId?: string;
  debtorTypeId?;
  selectedQuotationVersionIds?: string;
  selectedQuotationNumbers?: string;
  fromUrl?: string;
  isExist?: boolean;
  debtorAdditionalInfoId?: string;
  isDebtorVetted?: boolean;
  isDebtorAccountVetted?: boolean;
  isOnInvoicePayment?: boolean;
  addressId?: string;
  isItemAndOnInvoiceOnly?: boolean;
}
class ServiceAgreementModel {
  params: ServiceAgreementStepsParams;
  selectedIndex: number;
}

export {
  LeadStepperCreationModel,
  StepperParams,
  ServiceAgreementStepsParams,
  ServiceAgreementModel, LeadHeaderData
};

