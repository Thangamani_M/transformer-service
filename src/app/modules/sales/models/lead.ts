import { defaultCountryCode } from '@app/shared';
import { Address } from './address';
import { AddressModel } from './lead-creation.model';
import { LSSScheme } from './lssScheme';
class Lead {
    leadId?: string;
    rawLeadId?: string;
    leadNumber?: string;
    callTypeId?: number;
    leadGroupId?: string;
    leadCategoryId?: string;
    siteTypeId?: string;
    commercialProductId?: string;
    title?: string;
    firstName?: string;
    lastName?: string;
    corporateFlag?= false;
    customerId?: string;
    industryId?: string;
    isFootPrint?= false;
    gender?: string;
    isNewOwner?= false;
    movingDate?: string;
    movingReasons?: string;
    isInterestedforReconnection?= false;
    isNewAddress?= false;
    isFidelityPremise?= false;
    isContinueOurServices?= false;
    isInstallationRequired?= false;
    leadSourceId?: string;
    sSNumber?: string;
    comments?: string;
    leadStatusId?: string;
    isDeleted?= false;
    isActive?= false;
    createdDate?: string;
    createdUserId?: string;
    modifiedDate?: string;
    modifiedUserId?: string;
    callCategoryId?: string;
    productTypeId?: string;
    email?: string;
    mobileNumber1?: string;
    mobileNumber2?: string;
    homeNumber?: string;
    officeNumber?: string;
    isLSS?= 'false';
    isPartition?= false;
    lssSchemeId?: string;
    probabilityOfClosure?: string;
    isContribution?= 'false';
    lSSSchemeNumber?: string;
    lssScheme: LSSScheme;
    customerNumber?: string;
    latLang?: string;
    address: Address;
    preferredLeadCategories: any;
    said: any
    isNGO: string
    poNumber: any
    expiryDate?: any
    companyRegisterNo: any
}
class LeadList {
    leadId?: string;
    leadNumber: string;
    clientName: string;
    email: string;
    contactNumber: string;
    suburb: string;
    city: string;
    probabilityOfClosure: string;
    leadCategoryName: string;
    status: string;
    CssClass: string;
    assignedTo: string;
}

class RawLeadList {
    rawLeadId?: string;
    rawLeadNumber: string;
    fullName: string;
    email: string;
    contactNumber: string;
    sourceCode: string;
    suburb: string;
    callAttempts: string;
    siteType: string;
    city: string;
    createdDate: string;
    rawLeadStatusName: string;
    isActive: string;
}
class SkillList {
    leadCategoryName: string;
}
class LeadCreationUserDataModel {
    constructor(leadCreationUserDataModel?: LeadCreationUserDataModel | any) {
        this.lssName = leadCreationUserDataModel == undefined ? "" : leadCreationUserDataModel.lssName == undefined ? "" : leadCreationUserDataModel.lssName;
        this.leadNumber = leadCreationUserDataModel == undefined ? "" : leadCreationUserDataModel.leadNumber == undefined ? "" : leadCreationUserDataModel.leadNumber;
        this.isAgreementApproved = leadCreationUserDataModel == undefined ? false : leadCreationUserDataModel.isAgreementApproved == undefined ? false : leadCreationUserDataModel.isAgreementApproved;
        this.isConcessionApprovalPending = leadCreationUserDataModel == undefined ? false : leadCreationUserDataModel.isConcessionApprovalPending == undefined ? false : leadCreationUserDataModel.isConcessionApprovalPending;
        this.isQuoteApproved = leadCreationUserDataModel == undefined ? false : leadCreationUserDataModel.isQuoteApproved == undefined ? false : leadCreationUserDataModel.isQuoteApproved;
        this.leadGroupId = leadCreationUserDataModel == undefined ? "" : leadCreationUserDataModel.leadGroupId == undefined ? "" : leadCreationUserDataModel.leadGroupId;
        this.leadId = leadCreationUserDataModel == undefined ? "" : leadCreationUserDataModel.leadId == undefined ? "" : leadCreationUserDataModel.leadId;
        this.firstName = leadCreationUserDataModel == undefined ? "" : leadCreationUserDataModel.firstName == undefined ? "" : leadCreationUserDataModel.firstName;
        this.lastName = leadCreationUserDataModel == undefined ? "" : leadCreationUserDataModel.lastName == undefined ? "" : leadCreationUserDataModel.lastName;
        this.suburbId = leadCreationUserDataModel == undefined ? "" : leadCreationUserDataModel.suburbId == undefined ? "" : leadCreationUserDataModel.suburbId;
        this.districtId = leadCreationUserDataModel == undefined ? "" : leadCreationUserDataModel.districtId == undefined ? "" : leadCreationUserDataModel.districtId;
        this.address = leadCreationUserDataModel == undefined ? null : leadCreationUserDataModel.address == undefined ? null : leadCreationUserDataModel.address;
        this.title = leadCreationUserDataModel == undefined ? "" : leadCreationUserDataModel.title == undefined ? "" : leadCreationUserDataModel.title;
        this.email = leadCreationUserDataModel == undefined ? "" : leadCreationUserDataModel.email == undefined ? "" : leadCreationUserDataModel.email;
        this.mobileNumber1 = leadCreationUserDataModel == undefined ? "" : leadCreationUserDataModel.mobileNumber1 == undefined ? "" : leadCreationUserDataModel.mobileNumber1;
        this.mobileNumber1CountryCode = leadCreationUserDataModel == undefined ? defaultCountryCode : leadCreationUserDataModel.mobileNumber1CountryCode == undefined ? defaultCountryCode : leadCreationUserDataModel.mobileNumber1CountryCode;
        this.siteTypeId = leadCreationUserDataModel == undefined ? "" : leadCreationUserDataModel.siteTypeId == undefined ? "" : leadCreationUserDataModel.siteTypeId;
        this.siteTypeName = leadCreationUserDataModel == undefined ? "" : leadCreationUserDataModel.siteTypeName == undefined ? "" : leadCreationUserDataModel.siteTypeName;
        this.leadStatusName = leadCreationUserDataModel == undefined ? "" : leadCreationUserDataModel.leadStatusName == undefined ? "" : leadCreationUserDataModel.leadStatusName;
        this.imageUrl = leadCreationUserDataModel == undefined ? "../../../../../../assets/img/admin-avatar.png" :
            leadCreationUserDataModel.imageUrl == undefined ? "../../../../../../assets/img/admin-avatar.png" : leadCreationUserDataModel.imageUrl;
        this.userId = leadCreationUserDataModel == undefined ? null : leadCreationUserDataModel.userId == undefined ? null : leadCreationUserDataModel.userId;
        this.agreementId = leadCreationUserDataModel == undefined ? null : leadCreationUserDataModel.agreementId == undefined ? null : leadCreationUserDataModel.agreementId;
        this.agreementOutcomeId = leadCreationUserDataModel == undefined ? null : leadCreationUserDataModel.agreementOutcomeId == undefined ? null : leadCreationUserDataModel.agreementOutcomeId;
        this.agreementOutcomeReasonId = leadCreationUserDataModel == undefined ? null : leadCreationUserDataModel.agreementOutcomeReasonId == undefined ? null : leadCreationUserDataModel.agreementOutcomeReasonId;
        this.partitionId = leadCreationUserDataModel == undefined ? null : leadCreationUserDataModel.partitionId == undefined ? null : leadCreationUserDataModel.partitionId;
        this.servicesCount = leadCreationUserDataModel == undefined ? 0 : leadCreationUserDataModel.servicesCount == undefined ? 0 : leadCreationUserDataModel.servicesCount;
        this.itemsCount = leadCreationUserDataModel == undefined ? 0 : leadCreationUserDataModel.itemsCount == undefined ? 0 : leadCreationUserDataModel.itemsCount;
        this.isAnyQuotationPending = leadCreationUserDataModel == undefined ? true : leadCreationUserDataModel.isAnyQuotationPending == undefined ? true : leadCreationUserDataModel.isAnyQuotationPending;
        this.isAgreementGenerated = leadCreationUserDataModel == undefined ? false : leadCreationUserDataModel.isAgreementGenerated == undefined ? false : leadCreationUserDataModel.isAgreementGenerated;
    }
    leadNumber: string;
    isAgreementApproved = false;
    isConcessionApprovalPending = false;
    isQuoteApproved = false;
    leadGroupId: string;
    lssName: string;
    leadId: string;
    address: AddressModel;
    firstName: string;
    lastName: string;
    suburbId: string;
    districtId: string;
    title: string;
    email: string;
    mobileNumber1: string;
    mobileNumber1CountryCode: string;
    siteTypeId: string;
    siteTypeName: string;
    leadStatusName: string;
    imageUrl?= "../../../../../../assets/img/admin-avatar.png";
    userId: string;
    servicesCount?: number;
    itemsCount?: number;
    agreementId?: string;
    agreementOutcomeId?: string;
    agreementOutcomeReasonId?: string;
    partitionId?: string;
    isAnyQuotationPending: boolean;
    isAgreementGenerated: boolean
}
class LeadNotes {
    leadId?: string;
    customerNumber: string;
    clientName: string;
    email: string;
    mobileNumber: string;
    suburbName: string;
    city: string;
    probabilityOfClosure: string;
    assignedTo: string;
    leadNumber: string;
    siteTypeName: string;
    division: string
}

export { Lead, LeadList, RawLeadList, SkillList, LeadCreationUserDataModel, LeadNotes };
