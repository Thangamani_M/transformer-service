
export class BookAppointmentModel {
    constructor(bookAppointmentModel?:BookAppointmentModel){
      this.appointmentId = bookAppointmentModel ? bookAppointmentModel.appointmentId == undefined ? '' : bookAppointmentModel.appointmentId : '';
      this.leadId = bookAppointmentModel ? bookAppointmentModel.leadId == undefined ? '' : bookAppointmentModel.leadId : '';
      this.appointmentDate = bookAppointmentModel ? bookAppointmentModel.appointmentDate == undefined ? '' : bookAppointmentModel.appointmentDate : '';
      this.alternateSellerTime = bookAppointmentModel ? bookAppointmentModel.alternateSellerTime == undefined ? '' : bookAppointmentModel.alternateSellerTime : '';
      this.contactNo = bookAppointmentModel ? bookAppointmentModel.contactNo == undefined ? null : bookAppointmentModel.contactNo : null;
      this.appointmentDuration = bookAppointmentModel ? bookAppointmentModel.appointmentDuration == undefined ? '' : bookAppointmentModel.appointmentDuration : '';
      this.isAlternateSeller = bookAppointmentModel ? bookAppointmentModel.isAlternateSeller == undefined ? false : bookAppointmentModel.isAlternateSeller : false
      this.customerId = bookAppointmentModel ? bookAppointmentModel.customerId == undefined ? '' : bookAppointmentModel.customerId : '';
      this.appointmentTimeFrom = bookAppointmentModel ? bookAppointmentModel.appointmentTimeFrom == undefined ? '' : bookAppointmentModel.appointmentTimeFrom : '';
      this.email = bookAppointmentModel ? bookAppointmentModel.email == undefined ? '' : bookAppointmentModel.email : '';
      this.sellerId = bookAppointmentModel ? bookAppointmentModel.sellerId == undefined ? '' : bookAppointmentModel.sellerId : '';
      this.sellerSlotId = bookAppointmentModel ? bookAppointmentModel.sellerSlotId == undefined ? '' : bookAppointmentModel.sellerSlotId : '';
      this.fullName = bookAppointmentModel ? bookAppointmentModel.fullName == undefined ? '' : bookAppointmentModel.fullName : '';
      this.contactNoDuplicate = bookAppointmentModel ? bookAppointmentModel.contactNoDuplicate == undefined ? '' : bookAppointmentModel.contactNoDuplicate : '';
      this.contactNumberCountryCode = bookAppointmentModel ? bookAppointmentModel.contactNumberCountryCode == undefined ? '+27' : bookAppointmentModel.contactNumberCountryCode : '+27';
    }
     appointmentId?: string;
     fullName?:string;
     contactNoDuplicate?:string;
     contactNumberCountryCode?:string;
     appointmentStatusId?: string;
     appointmentDuration?:string;
     priorityId?:string;
     leadId?:string;
     assignedById?:string;
     assignedToId?:string;
     comments?:string;
     appointmentDate?:string;
     alternateSellerTime?;
     availableTime?:string;
     fromTime?:string;
     toTime?:string;
     contactNo?:string;
     estimatedTime?:string;
     isAlternateSeller?:boolean;
     tentativeTimeSlot?:string;
     customerId?:string;
     appointmentTimeFrom?:string;
     email?:string;
     sellerId?:string;
     sellerSlotId?:string;
  }
  export class AppointmentViewModel {
    appointmentId?: string;
    appointmentNumber: string;
    appointmentTypeName: string;
    referenceId: string;
    sellerName: string;
    clientName: string;
    contactNumber: string;
    timeSlot: string;
    priorityName: string;
    siteAddress:string;
    appointmentStatusName:string;
}