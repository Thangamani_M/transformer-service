class SalesDiscountManualAddEditModel {
    constructor(salesdicount?: SalesDiscountManualAddEditModel) {
        this.discountPercentage = salesdicount ? salesdicount.discountPercentage == undefined ? '' : salesdicount.discountPercentage : '';
        this.roleId = salesdicount ? salesdicount.roleId == undefined ? '' : salesdicount.roleId : '';
        this.salesDiscountId = salesdicount ? salesdicount.salesDiscountId == undefined ? '' : salesdicount.salesDiscountId : '';
    }

    discountPercentage?: string;
    salesDiscountId?: string;
    roleId?: string;
}
export { SalesDiscountManualAddEditModel }
