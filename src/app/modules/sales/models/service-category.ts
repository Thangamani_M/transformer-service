class ServiceCategoryModel {
  constructor(serviceCategoryModel?: ServiceCategoryModel) {
    this.serviceCategoryId = serviceCategoryModel ? serviceCategoryModel.serviceCategoryId == undefined ? '' : serviceCategoryModel.serviceCategoryId : '';
    this.createdUserId = serviceCategoryModel ? serviceCategoryModel.createdUserId == undefined ? '' : serviceCategoryModel.createdUserId : '';
    this.modifiedUserId = serviceCategoryModel ? serviceCategoryModel.modifiedUserId == undefined ? '' : serviceCategoryModel.modifiedUserId : '';
    this.serviceCategoryName = serviceCategoryModel ? serviceCategoryModel.serviceCategoryName == undefined ? '' : serviceCategoryModel.serviceCategoryName : '';
    this.description = serviceCategoryModel ? serviceCategoryModel.description == undefined ? '' : serviceCategoryModel.description : '';
    this.isActive = serviceCategoryModel ? serviceCategoryModel.isActive == undefined ? false : serviceCategoryModel.isActive : false;
    this.isQtyAvailable = serviceCategoryModel ? serviceCategoryModel.isQtyAvailable == undefined ? false : serviceCategoryModel.isQtyAvailable : false;
  }
  serviceCategoryId?: string;
  createdUserId?: string;
  modifiedUserId?: string;
  serviceCategoryName: string;
  description?: string;
  isActive?: boolean;
  isQtyAvailable?: boolean
}
export { ServiceCategoryModel }