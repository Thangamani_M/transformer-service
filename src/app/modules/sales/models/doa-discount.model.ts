class DOADiscountAddEditModel {
    constructor(dOADiscountAddEditModel? : DOADiscountAddEditModel) {
        this.doaDiscountId = dOADiscountAddEditModel? dOADiscountAddEditModel.doaDiscountId == undefined ? '' : dOADiscountAddEditModel.doaDiscountId : '';
        this.roleId = dOADiscountAddEditModel? dOADiscountAddEditModel.roleId == undefined ? '' : dOADiscountAddEditModel.roleId : '';
        this.minPercentage = dOADiscountAddEditModel? dOADiscountAddEditModel.minPercentage == undefined ? null : dOADiscountAddEditModel.minPercentage : null;
        this.maxPercentage = dOADiscountAddEditModel? dOADiscountAddEditModel.maxPercentage == undefined ? null : dOADiscountAddEditModel.maxPercentage : null;
        this.noOfLevels = dOADiscountAddEditModel? dOADiscountAddEditModel.noOfLevels == undefined ? null : dOADiscountAddEditModel.noOfLevels : null;
        this.levelId1 = dOADiscountAddEditModel? dOADiscountAddEditModel.levelId1 == undefined ? '' : dOADiscountAddEditModel.levelId1 : '';
        this.levelId2 = dOADiscountAddEditModel? dOADiscountAddEditModel.levelId2 == undefined ? '' : dOADiscountAddEditModel.levelId2 : '';       
        this.levelId3 = dOADiscountAddEditModel? dOADiscountAddEditModel.levelId3 == undefined ? '' : dOADiscountAddEditModel.levelId3 : ''; 
        this.createdUserId = dOADiscountAddEditModel? dOADiscountAddEditModel.createdUserId == undefined ? '' : dOADiscountAddEditModel.createdUserId : '';       
        this.modifiedUserId = dOADiscountAddEditModel? dOADiscountAddEditModel.modifiedUserId == undefined ? '' : dOADiscountAddEditModel.modifiedUserId : '';       
     }
     doaDiscountId? : string;
    roleId? : string;
    minPercentage? : number;
    maxPercentage? : number;
    noOfLevels? : number;
    levelId1? : string;
    levelId2? : string;
    levelId3? : string;
    createdUserId?:string;
    modifiedUserId?:string;
}

export {DOADiscountAddEditModel};