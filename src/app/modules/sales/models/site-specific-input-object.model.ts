export class SiteSpecificInputObjectModel {
    constructor(model?: SiteSpecificInputObjectModel) {
        this.fromComponentType = model == undefined ? 'lead' : model.fromComponentType == undefined ? 'lead' : model.fromComponentType;
        this.customerId = model == undefined ? '' : model.customerId == undefined ? '' : model.customerId;
        this.addressId = model == undefined ? '' : model.addressId == undefined ? '' : model.addressId;
        this.partitionId = model == undefined ? '' : model.partitionId == undefined ? '' : model.partitionId;
        this.quotationVersionId = model == undefined ? '' : model.quotationVersionId == undefined ? '' : model.quotationVersionId;
        this.leadId = model == undefined ? '' : model.leadId == undefined ? '' : model.leadId;
    }
    fromComponentType: string;
    customerId: string;
    addressId: string;
    partitionId: string;
    quotationVersionId: string;
    leadId: string;
}