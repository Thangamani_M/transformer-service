export class LeadItem {
    leadItemId:any;
    leadId:any;
    itemId:any;
    itemOwnershipType:string;
    existingQty:number;
    newQty:number;
    comments:string;
    unitPrice:any;
}
