import { defaultCountryCode } from "@app/shared";
class CustomerContactEditModel {
    constructor(customerContactEditModel?: CustomerContactEditModel) {
        this.titleId = customerContactEditModel ? customerContactEditModel.titleId == undefined ? '' : customerContactEditModel.titleId : '';
        this.email = customerContactEditModel ? customerContactEditModel.email == undefined ? '' : customerContactEditModel.email : '';
        this.customerId = customerContactEditModel ? customerContactEditModel.customerId == undefined ? '' : customerContactEditModel.customerId : '';
        this.customerName = customerContactEditModel ? customerContactEditModel.customerName == undefined ? '' : customerContactEditModel.customerName : '';
        this.customerTypeName = customerContactEditModel ? customerContactEditModel.customerTypeName == undefined ? '' : customerContactEditModel.customerTypeName : '';
        this.firstName = customerContactEditModel ? customerContactEditModel.firstName == undefined ? '' : customerContactEditModel.firstName : '';
        this.lastName = customerContactEditModel ? customerContactEditModel.lastName == undefined ? '' : customerContactEditModel.lastName : '';
        this.mobile1 = customerContactEditModel ? customerContactEditModel.mobile1 == undefined ? null : customerContactEditModel.mobile1 : null;
        this.officeNo = customerContactEditModel ? customerContactEditModel.officeNo == undefined ? "" : customerContactEditModel.officeNo : "";
        this.mobile2 = customerContactEditModel ? customerContactEditModel.mobile2 == undefined ? '' : customerContactEditModel.mobile2 : '';
        this.premisesNo = customerContactEditModel ? customerContactEditModel.premisesNo == undefined ? '' : customerContactEditModel.premisesNo : '';
        this.contactId = customerContactEditModel ? customerContactEditModel.contactId == undefined ? '' : customerContactEditModel.contactId : '';
        this.saId = customerContactEditModel ? customerContactEditModel.saId == undefined ? '' : customerContactEditModel.saId : '';
        this.mobile1CountryCode = customerContactEditModel ? customerContactEditModel.mobile1CountryCode == undefined ? '+27' : customerContactEditModel.mobile1CountryCode : '+27';
        this.mobile2CountryCode = customerContactEditModel ? customerContactEditModel.mobile2CountryCode == undefined ? '+27' : customerContactEditModel.mobile2CountryCode : '+27';
        this.officeNoCountryCode = customerContactEditModel ? customerContactEditModel.officeNoCountryCode == undefined ? '+27' : customerContactEditModel.officeNoCountryCode : '+27';
        this.premisesNoCountryCode = customerContactEditModel ? customerContactEditModel.premisesNoCountryCode == undefined ? '+27' : customerContactEditModel.premisesNoCountryCode : '+27';
        this.isCPAExempt = customerContactEditModel == undefined ? false : customerContactEditModel.isCPAExempt == undefined ? false : customerContactEditModel.isCPAExempt;
        this.contactInfo = customerContactEditModel == undefined ? new ContactInfoModel() : customerContactEditModel.contactInfo == undefined ? new ContactInfoModel() : customerContactEditModel.contactInfo;
        this.companyRegNo = customerContactEditModel == undefined ? null : customerContactEditModel.companyRegNo == undefined ? null : customerContactEditModel.companyRegNo;
        this.customerTypeId = customerContactEditModel == undefined ? null : customerContactEditModel.customerTypeId == undefined ? null : customerContactEditModel.customerTypeId;
        this.isPassport = customerContactEditModel == undefined ? null : customerContactEditModel.isPassport == undefined ? null : customerContactEditModel.isPassport;
        this.passportNo = customerContactEditModel == undefined ? null : customerContactEditModel.passportNo == undefined ? null : customerContactEditModel.passportNo;
        this.passportExpiryMonth = customerContactEditModel == undefined ? null : customerContactEditModel.passportExpiryMonth == undefined ? null : customerContactEditModel.passportExpiryMonth;
        this.passportExpiryYear = customerContactEditModel == undefined ? null : customerContactEditModel.passportExpiryYear == undefined ? null : customerContactEditModel.passportExpiryYear;
        this.passportExpiryDate = customerContactEditModel == undefined ? null : customerContactEditModel.passportExpiryDate == undefined ? null : customerContactEditModel.passportExpiryDate;
    }
    email?: string;
    customerId?: string;
    customerName?: string;
    customerTypeName?: string;
    firstName?: string;
    lastName?: string;
    mobile1?: Date;
    officeNo?: string;
    mobile2?: string;
    premisesNo?: string;
    contactId?: string;
    saId?: string;
    mobile1CountryCode?: string;
    mobile2CountryCode?: string;
    officeNoCountryCode?: string;
    premisesNoCountryCode?: string;
    isCPAExempt?: boolean;
    contactInfo: ContactInfoModel;
    companyRegNo?: string;
    customerTypeId?: string;
    isPassport?: boolean;
    passportNo?: string;
    passportExpiryMonth: string;
    passportExpiryYear: string;
    passportExpiryDate: Date;
    titleId?: string;
}
class ContactInfoModel {
    constructor(contactInfoModel?: ContactInfoModel) {
        this.contactId = contactInfoModel == undefined ? null : contactInfoModel.contactId == undefined ? null : contactInfoModel.contactId;
        this.email = contactInfoModel == undefined ? "" : contactInfoModel.email == undefined ? "" : contactInfoModel.email;
        this.mobile1 = contactInfoModel == undefined ? "" : contactInfoModel.mobile1 == undefined ? "" : contactInfoModel.mobile1;
        this.mobile1CountryCode = contactInfoModel == undefined ? defaultCountryCode : contactInfoModel.mobile1CountryCode == undefined ? defaultCountryCode : contactInfoModel.mobile1CountryCode;
        this.mobile2 = contactInfoModel == undefined ? "" : contactInfoModel.mobile2 == undefined ? "" : contactInfoModel.mobile2;
        this.mobile2CountryCode = contactInfoModel == undefined ? defaultCountryCode : contactInfoModel.mobile2CountryCode == undefined ? defaultCountryCode : contactInfoModel.mobile2CountryCode;
        this.officeNo = contactInfoModel == undefined ? "" : contactInfoModel.officeNo == undefined ? "" : contactInfoModel.officeNo;
        this.officeNoCountryCode = contactInfoModel == undefined ? defaultCountryCode : contactInfoModel.officeNoCountryCode == undefined ? defaultCountryCode : contactInfoModel.officeNoCountryCode;
        this.premisesNo = contactInfoModel == undefined ? "" : contactInfoModel.premisesNo == undefined ? "" : contactInfoModel.premisesNo;
        this.premisesNoCountryCode = contactInfoModel == undefined ? defaultCountryCode : contactInfoModel.premisesNoCountryCode == undefined ? defaultCountryCode : contactInfoModel.premisesNoCountryCode;
    }
    contactId?: string;
    email?: string;
    mobile1?: string;
    mobile1CountryCode?: string;
    mobile2?: string;
    mobile2CountryCode?: string;
    officeNo?: string;
    officeNoCountryCode?: string;
    premisesNo?: string;
    premisesNoCountryCode?: string;
}
class AccountAddModel {
    constructor(accountAddModel?: AccountAddModel) {
        this.debtorId = accountAddModel ? accountAddModel.debtorId == undefined ? '' : accountAddModel.debtorId : '';
        this.titleId = accountAddModel ? accountAddModel.titleId == undefined ? '' : accountAddModel.titleId : '';
        this.firstName = accountAddModel ? accountAddModel.firstName == undefined ? '' : accountAddModel.firstName : '';
        this.lastName = accountAddModel ? accountAddModel.lastName == undefined ? null : accountAddModel.lastName : null;
        this.companyName = accountAddModel ? accountAddModel.companyName == undefined ? null : accountAddModel.companyName : null;
        this.bankId = accountAddModel ? accountAddModel.bankId == undefined ? '' : accountAddModel.bankId : '';
        this.paymentMethodId = accountAddModel ? accountAddModel.paymentMethodId == undefined ? '' : accountAddModel.paymentMethodId : '';
        this.bankBranchCode = accountAddModel ? accountAddModel.bankBranchCode == undefined ? null : accountAddModel.bankBranchCode : null;
        this.isAllSubscription = accountAddModel ? accountAddModel.isAllSubscription == undefined ? true : accountAddModel.isAllSubscription : true;
        this.debitDay = accountAddModel ? accountAddModel.debitDay == undefined ? "" : accountAddModel.debitDay : "";
        this.accountNo = accountAddModel ? accountAddModel.accountNo == undefined ? '' : accountAddModel.accountNo : '';
        this.accountTypeId = accountAddModel ? accountAddModel.accountTypeId == undefined ? '' : accountAddModel.accountTypeId : '';
        this.bankBranchId = accountAddModel ? accountAddModel.bankBranchId == undefined ? '' : accountAddModel.bankBranchId : '';
        this.oldDebtorAccountDetailId = accountAddModel ? accountAddModel.oldDebtorAccountDetailId == undefined ? null : accountAddModel.oldDebtorAccountDetailId : null;
        this.customerId = accountAddModel ? accountAddModel.customerId == undefined ? '' : accountAddModel.customerId : '';
        this.subscriptionId = accountAddModel ? accountAddModel.subscriptionId == undefined ? '' : accountAddModel.subscriptionId : '';
        this.debtorTypeId = accountAddModel ? accountAddModel.debtorTypeId == undefined ? '' : accountAddModel.debtorTypeId : '';
        this.debtorAdditionalInfoId = accountAddModel ? accountAddModel.debtorAdditionalInfoId == undefined ? '' : accountAddModel.debtorAdditionalInfoId : '';
        this.createdUserId = accountAddModel ? accountAddModel.createdUserId == undefined ? '' : accountAddModel.createdUserId : '';
        this.proofOfDocument = accountAddModel ? accountAddModel.proofOfDocument == undefined ? '' : accountAddModel.proofOfDocument : '';
        this.passportExpiryDate = accountAddModel ? accountAddModel.passportExpiryDate == undefined ? '' : accountAddModel.passportExpiryDate : '';
        this.passportNo = accountAddModel ? accountAddModel.passportNo == undefined ? '' : accountAddModel.passportNo : '';
    }
    debtorId?: string;
    titleId?: string;
    firstName?: string;
    lastName?: string;
    companyName?: string;
    bankId?: string;
    paymentMethodId?: string;
    bankBranchCode?: string;
    isAllSubscription?: boolean = true;
    debitDay?: string;
    accountNo?: string;
    accountTypeId?: string;
    bankBranchId?: string;
    proofOfDocument?: string;
    oldDebtorAccountDetailId?: string;
    customerId?: string;
    subscriptionId?: string;
    debtorTypeId?: string;
    debtorAdditionalInfoId?: string;
    createdUserId?: string;
    passportExpiryDate?: any;
    passportNo?: string;
}

export { AccountAddModel, CustomerContactEditModel, ContactInfoModel };