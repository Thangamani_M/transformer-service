export class CallReasonAddEditModel {
  constructor(callReasonAddEditModel?: CallReasonAddEditModel) {
    this.callReasonId = callReasonAddEditModel ? callReasonAddEditModel.callReasonId == undefined ? '' : callReasonAddEditModel.callReasonId : '';
    this.callReasonName = callReasonAddEditModel ? callReasonAddEditModel.callReasonName == undefined ? '' : callReasonAddEditModel.callReasonName : '';
    this.description = callReasonAddEditModel ? callReasonAddEditModel.description == undefined ? '' : callReasonAddEditModel.description : '';
    this.createdUserId = callReasonAddEditModel ? callReasonAddEditModel.createdUserId == undefined ? '' : callReasonAddEditModel.createdUserId : '';
  }
  callReasonId?: string
  callReasonName?: string
  description?: string
  createdUserId?: string
}
export class CallWrapupReasonModel {
  callLogId?: string;
  wrapupReasonId?: string;
  callReasonId?: string;
  notes?: string;
  createdUserId?: string;
  callWrapupReasonId?: string;
  constructor(callWrapupReasonModel?: CallWrapupReasonModel) {
    this.wrapupReasonId = callWrapupReasonModel ? callWrapupReasonModel.wrapupReasonId == undefined ? '' : callWrapupReasonModel.wrapupReasonId : '';
    this.callWrapupReasonId = callWrapupReasonModel ? callWrapupReasonModel.callWrapupReasonId == undefined ? '' : callWrapupReasonModel.callWrapupReasonId : '';
    this.callReasonId = callWrapupReasonModel ? callWrapupReasonModel.callReasonId == undefined ? '' : callWrapupReasonModel.callReasonId : '';
    this.callLogId = callWrapupReasonModel ? callWrapupReasonModel.callLogId == undefined ? '' : callWrapupReasonModel.callLogId : '';
    this.notes = callWrapupReasonModel ? callWrapupReasonModel.notes == undefined ? '' : callWrapupReasonModel.notes : '';
    this.createdUserId = callWrapupReasonModel ? callWrapupReasonModel.createdUserId == undefined ? '' : callWrapupReasonModel.createdUserId : '';
  }
}
