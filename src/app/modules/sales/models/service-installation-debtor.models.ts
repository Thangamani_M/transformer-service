class DebtorBankingDetailsModel {
    constructor(debtorBankingDetails?: DebtorBankingDetailsModel) {
        this.leadId = debtorBankingDetails ? debtorBankingDetails.leadId == undefined ? '' : debtorBankingDetails.leadId : '';
        this.createdUserId = debtorBankingDetails ? debtorBankingDetails.createdUserId == undefined ? '' : debtorBankingDetails.createdUserId : '';
        this.debtorId = debtorBankingDetails ? debtorBankingDetails.debtorId == undefined ? '' : debtorBankingDetails.debtorId : '';
        this.saleOrderId = debtorBankingDetails ? debtorBankingDetails.saleOrderId == undefined ? '' : debtorBankingDetails.saleOrderId : '';
        this.customerId = debtorBankingDetails ? debtorBankingDetails.customerId == undefined ? '' : debtorBankingDetails.customerId : '';
        this.debtorAdditionalInfoId = debtorBankingDetails ? debtorBankingDetails.debtorAdditionalInfoId == undefined ? '' : debtorBankingDetails.debtorAdditionalInfoId : '';
        this.debtorAccountDetailId = debtorBankingDetails ? debtorBankingDetails.debtorAccountDetailId == undefined ? null : debtorBankingDetails.debtorAccountDetailId : null;
        this.isInvoiceRequired = debtorBankingDetails ? debtorBankingDetails.isInvoiceRequired == false ? true : debtorBankingDetails.isInvoiceRequired : false;
        this.isPrintedStatement = debtorBankingDetails ? debtorBankingDetails.isPrintedStatement == false ? true : debtorBankingDetails.isPrintedStatement : false;
        this.isSendUnsecuredPDFStatement = debtorBankingDetails ? debtorBankingDetails.isSendUnsecuredPDFStatement == false ? true : debtorBankingDetails.isSendUnsecuredPDFStatement : false;
        this.isSMSDebtor = debtorBankingDetails ? debtorBankingDetails.isSMSDebtor == false ? true : debtorBankingDetails.isSMSDebtor : false;
        this.isGroupInvoice = debtorBankingDetails ? debtorBankingDetails.isGroupInvoice == false ? true : debtorBankingDetails.isGroupInvoice : false;
        this.isBankAccount = debtorBankingDetails ? debtorBankingDetails.isBankAccount == true ? false : debtorBankingDetails.isBankAccount : true;
        this.isCardSelected = debtorBankingDetails ? debtorBankingDetails.isCardSelected == false ? true : debtorBankingDetails.isCardSelected : false;
        this.leadId = debtorBankingDetails ? debtorBankingDetails.leadId == undefined ? '' : debtorBankingDetails.leadId : '';
        this.firstName = debtorBankingDetails ? debtorBankingDetails.firstName == undefined ? null : debtorBankingDetails.firstName : null;
        this.lastName = debtorBankingDetails ? debtorBankingDetails.lastName == undefined ? null : debtorBankingDetails.lastName : null;
        this.companyName = debtorBankingDetails ? debtorBankingDetails.companyName == undefined ? null : debtorBankingDetails.companyName : null;
        this.accountNo = debtorBankingDetails ? debtorBankingDetails.accountNo == undefined ? null : debtorBankingDetails.accountNo : null;
        this.bankBranchId = debtorBankingDetails ? debtorBankingDetails.bankBranchId == undefined ? null : debtorBankingDetails.bankBranchId : null;
        this.accountTypeId = debtorBankingDetails ? debtorBankingDetails.accountTypeId == undefined ? null : debtorBankingDetails.accountTypeId : null;
        this.cardTypeId = debtorBankingDetails ? debtorBankingDetails.cardTypeId == undefined ? null : debtorBankingDetails.cardTypeId : null;
        this.cardNo = debtorBankingDetails ? debtorBankingDetails.cardNo == undefined ? null : debtorBankingDetails.cardNo : null;
        this.debtorTypeId = debtorBankingDetails ? debtorBankingDetails.debtorTypeId == undefined ? '' : debtorBankingDetails.debtorTypeId : '';
        this.selectedMonth = debtorBankingDetails ? debtorBankingDetails.selectedMonth == undefined ? null : debtorBankingDetails.selectedMonth : null;
        this.selectedYear = debtorBankingDetails ? debtorBankingDetails.selectedYear == undefined ? null : debtorBankingDetails.selectedYear : null;
        this.expiryDate = debtorBankingDetails ? debtorBankingDetails.expiryDate == undefined ? '' : debtorBankingDetails.expiryDate : '';
        this.titleId = debtorBankingDetails ? debtorBankingDetails.titleId == undefined ? null : debtorBankingDetails.titleId : null;
        this.bankId = debtorBankingDetails ? debtorBankingDetails.bankId == undefined ? null : debtorBankingDetails.bankId : null;
        this.bankBranchCode = debtorBankingDetails ? debtorBankingDetails.bankBranchCode == undefined ? '' : debtorBankingDetails.bankBranchCode : '';
        this.isExistingDebtor = debtorBankingDetails ? debtorBankingDetails.isExistingDebtor == false ? true : debtorBankingDetails.isExistingDebtor : false;
        this.isOnlineDebtorAccepted = debtorBankingDetails == undefined ? false : debtorBankingDetails.isOnlineDebtorAccepted == undefined ? false : debtorBankingDetails.isOnlineDebtorAccepted;
    }
    leadId?: string;
    createdUserId?: string;
    debtorId?: string;
    saleOrderId?: string;
    customerId?: string;
    debtorAdditionalInfoId?: string;
    debtorAccountDetailId?: string;
    isInvoiceRequired?: boolean;
    isPrintedStatement?: boolean;
    isSendUnsecuredPDFStatement?: boolean;
    isSMSDebtor?: boolean;
    isGroupInvoice?: boolean;
    isBankAccount?: boolean;
    titleId?: string;
    firstName?: string;
    lastName?: string;
    companyName?: string;
    accountNo?: string;
    bankId?: string;
    bankBranchId?: string;
    accountTypeId?: string;
    cardTypeId?: string;
    cardNo?: string;
    expiryDate?: string;
    debtorTypeId?: string;
    selectedMonth?: string;
    selectedYear?: string;
    bankBranchCode?: string;
    isCardSelected?: boolean;
    isExistingDebtor?: boolean;
    isOnlineDebtorAccepted?: boolean;
}
class CreditVettingInfo {
    constructor(creditVettingInfo?: CreditVettingInfo) {
        this.saidCustomer = creditVettingInfo ? creditVettingInfo.saidCustomer == undefined ? '' : creditVettingInfo.saidCustomer : '';
        this.saidDebtor = creditVettingInfo ? creditVettingInfo.saidDebtor == undefined ? '' : creditVettingInfo.saidDebtor : '';
        this.customerRefNo = creditVettingInfo ? creditVettingInfo.customerRefNo == undefined ? '' : creditVettingInfo.customerRefNo : '';
        this.debtorRefNo = creditVettingInfo ? creditVettingInfo.debtorRefNo == undefined ? '' : creditVettingInfo.debtorRefNo : '';
        this.documentName = creditVettingInfo ? creditVettingInfo.documentName == undefined ? '' : creditVettingInfo.documentName : '';
        this.systemOwnership = creditVettingInfo ? creditVettingInfo.systemOwnership == undefined ? '' : creditVettingInfo.systemOwnership : '';
        this.companyRegNoCust = creditVettingInfo ? creditVettingInfo.companyRegNoCust == undefined ? '' : creditVettingInfo.companyRegNoCust : '';
        this.companyNameCust = creditVettingInfo ? creditVettingInfo.companyNameCust == undefined ? '' : creditVettingInfo.companyNameCust : '';
        this.systemOwnershipId = creditVettingInfo ? creditVettingInfo.systemOwnershipId == undefined ? '' : creditVettingInfo.systemOwnershipId : '';
        this.companyRegNoDebt = creditVettingInfo ? creditVettingInfo.companyRegNoDebt == undefined ? '' : creditVettingInfo.companyRegNoDebt : '';
        this.companyNameDebt = creditVettingInfo ? creditVettingInfo.companyNameDebt == undefined ? '' : creditVettingInfo.companyNameDebt : '';
        this.dobCust = creditVettingInfo ? creditVettingInfo.dobCust == undefined ? '' : creditVettingInfo.dobCust : '';
        this.dobDebt = creditVettingInfo ? creditVettingInfo.dobDebt == undefined ? '' : creditVettingInfo.dobDebt : '';
        this.passportNoCustomer = creditVettingInfo ? creditVettingInfo.passportNoCustomer == undefined ? '' : creditVettingInfo.passportNoCustomer : '';
        this.passportNoDebtor = creditVettingInfo ? creditVettingInfo.passportNoDebtor == undefined ? '' : creditVettingInfo.passportNoDebtor : '';
        this.passportExpiryDateCust = creditVettingInfo ? creditVettingInfo.passportExpiryDateCust == undefined ? '' : creditVettingInfo.passportExpiryDateCust : '';
        this.passportExpiryDateDebt = creditVettingInfo ? creditVettingInfo.passportExpiryDateDebt == undefined ? '' : creditVettingInfo.passportExpiryDateDebt : '';
        this.expiryDebtDate = creditVettingInfo ? creditVettingInfo.expiryDebtDate == undefined ? '' : creditVettingInfo.expiryDebtDate : '';
        this.expiryDebtMonth = creditVettingInfo ? creditVettingInfo.expiryDebtMonth == undefined ? '' : creditVettingInfo.expiryDebtMonth : '';
        this.isPassportCust = creditVettingInfo ? creditVettingInfo.isPassportCust == undefined ? false : creditVettingInfo.isPassportCust : false;
        this.isPassportDebtor = creditVettingInfo ? creditVettingInfo.isPassportDebtor == undefined ? false : creditVettingInfo.isPassportDebtor : false;
        this.createdUserId = creditVettingInfo ? creditVettingInfo.createdUserId == undefined ? '' : creditVettingInfo.createdUserId : '';
    }
    saidCustomer: string;
    saidDebtor: string;
    dobCust: string;
    systemOwnership: string;
    systemOwnershipId: string;
    companyNameCust: string;
    companyRegNoCust: string;
    companyNameDebt: string;
    companyRegNoDebt: string;
    dobDebt: string;
    passportNoCustomer: string;
    passportNoDebtor: string;
    passportExpiryDateCust: string;
    passportExpiryDateDebt: string;
    expiryDebtDate: string;
    documentName: string;
    expiryDebtMonth: string;
    createdUserId: string;
    isPassportCust: boolean;
    isPassportDebtor: boolean;
    customerRefNo: string;
    debtorRefNo: string;
}

export { DebtorBankingDetailsModel, CreditVettingInfo };
