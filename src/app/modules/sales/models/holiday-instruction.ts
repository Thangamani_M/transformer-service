import { defaultCountryCode } from "@app/shared";
class HolidayInstructionModelAddEditModel {
    constructor(holidayInstructionModelAddEditModel?: HolidayInstructionModelAddEditModel) {
        this.holidayInstructionId = holidayInstructionModelAddEditModel ? holidayInstructionModelAddEditModel.holidayInstructionId == undefined ? '' : holidayInstructionModelAddEditModel.holidayInstructionId : '';
        this.customerId = holidayInstructionModelAddEditModel ? holidayInstructionModelAddEditModel.customerId == undefined ? '' : holidayInstructionModelAddEditModel.customerId : '';
        this.addressId = holidayInstructionModelAddEditModel ? holidayInstructionModelAddEditModel.addressId == undefined ? '' : holidayInstructionModelAddEditModel.addressId : '';
        this.holidayInstructionFromDate = holidayInstructionModelAddEditModel ? holidayInstructionModelAddEditModel.holidayInstructionFromDate == undefined ? null : holidayInstructionModelAddEditModel.holidayInstructionFromDate : null;
        this.holidayInstructionToDate = holidayInstructionModelAddEditModel ? holidayInstructionModelAddEditModel.holidayInstructionToDate == undefined ? null : holidayInstructionModelAddEditModel.holidayInstructionToDate : null;
        this.numberOfDailyPaidPatrols = holidayInstructionModelAddEditModel ? holidayInstructionModelAddEditModel.numberOfDailyPaidPatrols == undefined ? null : holidayInstructionModelAddEditModel.numberOfDailyPaidPatrols : null;
        this.createdUserId = holidayInstructionModelAddEditModel ? holidayInstructionModelAddEditModel.createdUserId == undefined ? '' : holidayInstructionModelAddEditModel.createdUserId : '';
        this.modifiedUserId = holidayInstructionModelAddEditModel ? holidayInstructionModelAddEditModel.modifiedUserId == undefined ? '' : holidayInstructionModelAddEditModel.modifiedUserId : '';
        this.paidPatrolStartDate = holidayInstructionModelAddEditModel ? holidayInstructionModelAddEditModel.paidPatrolStartDate == undefined ? null : holidayInstructionModelAddEditModel.paidPatrolStartDate : null;
        this.paidPatrolEndDate = holidayInstructionModelAddEditModel ? holidayInstructionModelAddEditModel.paidPatrolEndDate == undefined ? null : holidayInstructionModelAddEditModel.paidPatrolEndDate : null;
        this.isAdditionalPaidPatrols = holidayInstructionModelAddEditModel ? holidayInstructionModelAddEditModel.isAdditionalPaidPatrols == undefined ? false : holidayInstructionModelAddEditModel.isAdditionalPaidPatrols : false;
        this.comments = holidayInstructionModelAddEditModel ? holidayInstructionModelAddEditModel.comments == undefined ? '' : holidayInstructionModelAddEditModel.comments : '';
        this.holidayInstructionContacts = holidayInstructionModelAddEditModel ? holidayInstructionModelAddEditModel.holidayInstructionContacts == undefined ? [] : holidayInstructionModelAddEditModel.holidayInstructionContacts : [];
        this.holidayInstructionEmergencyContacts = holidayInstructionModelAddEditModel ? holidayInstructionModelAddEditModel.holidayInstructionEmergencyContacts == undefined ? [] : holidayInstructionModelAddEditModel.holidayInstructionEmergencyContacts : [];
    }
    holidayInstructionId?: string;
    customerId?: string;
    addressId?: string;
    holidayInstructionFromDate?: Date;
    holidayInstructionToDate?: Date;
    comments?: string;
    isAdditionalPaidPatrols: boolean;
    numberOfDailyPaidPatrols?: string;
    createdUserId?: string;
    modifiedUserId?: string;
    paidPatrolStartDate?: Date;
    paidPatrolEndDate?: Date;
    holidayInstructionContacts: HolidayInstructionContactsInfo[];
    holidayInstructionEmergencyContacts: HolidayInstructionEmergencyContacts[];
}
class HolidayInstructionContactsInfo {
    constructor(model?: HolidayInstructionContactsInfo) {
        this.holidayInstructionContactId = model ? model.holidayInstructionContactId == undefined ? '' : model.holidayInstructionContactId : '';
        this.contactPersonName = model ? model.contactPersonName == undefined ? '' : model.contactPersonName : '';
        this.contactNumberCountryCode = model ? model.contactNumberCountryCode == undefined ? defaultCountryCode : model.contactNumberCountryCode : defaultCountryCode;
        this.contactNumber = model ? model.contactNumber == undefined ? '' : model.contactNumber : '';
    }
    contactPersonName?: string;
    contactNumber?: string;
    contactNumberCountryCode?: string;
    holidayInstructionContactId?: string;
}
class HolidayInstructionEmergencyContacts {
    constructor(emergencyContactInfo?: HolidayInstructionEmergencyContacts) {
        this.holidayInstructionEmergencyContactId = emergencyContactInfo ? emergencyContactInfo.holidayInstructionEmergencyContactId == undefined ? '' : emergencyContactInfo.holidayInstructionEmergencyContactId : '';
        this.emergencyContactPersonName = emergencyContactInfo ? emergencyContactInfo.emergencyContactPersonName == undefined ? '' : emergencyContactInfo.emergencyContactPersonName : '';
        this.emergencyContactNumberCountryCode = emergencyContactInfo ? emergencyContactInfo.emergencyContactNumberCountryCode == undefined ? defaultCountryCode : emergencyContactInfo.emergencyContactNumberCountryCode : defaultCountryCode;
        this.emergencyContactNumber = emergencyContactInfo ? emergencyContactInfo.emergencyContactNumber == undefined ? '' : emergencyContactInfo.emergencyContactNumber : '';
    }
    holidayInstructionEmergencyContactId?: string;
    emergencyContactPersonName?: string;
    emergencyContactNumber?: string;
    emergencyContactNumberCountryCode?: string;
}

export { HolidayInstructionModelAddEditModel, HolidayInstructionEmergencyContacts,HolidayInstructionContactsInfo };