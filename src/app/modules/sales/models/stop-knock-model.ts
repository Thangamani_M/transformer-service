import { defaultCountryCode } from "@app/shared";
class StopAndKnockModel {
  constructor(stopAndKnockModel?: StopAndKnockModel) {
    this.stopAndKnockId = stopAndKnockModel == undefined ? null : stopAndKnockModel.stopAndKnockId == undefined ? null : stopAndKnockModel.stopAndKnockId;
    this.addressInfo = stopAndKnockModel == undefined ? new StopAndKnockAddressModel() : stopAndKnockModel.addressInfo == undefined ? new StopAndKnockAddressModel() : stopAndKnockModel.addressInfo;
    this.contactInfo = stopAndKnockModel == undefined ? new StopAndKnockContactInfoModel() : stopAndKnockModel.contactInfo == undefined ? new StopAndKnockContactInfoModel() : stopAndKnockModel.contactInfo;
    this.basicInfo = stopAndKnockModel == undefined ? new StopAndKnockBasicInfoModel() : stopAndKnockModel.basicInfo == undefined ? new StopAndKnockBasicInfoModel() : stopAndKnockModel.basicInfo;
    this.stopAndKnockRefNo = stopAndKnockModel == undefined ? null : stopAndKnockModel.stopAndKnockRefNo == undefined ? null : stopAndKnockModel.stopAndKnockRefNo;
    this.serviceTerminatedDate = stopAndKnockModel == undefined ? null : stopAndKnockModel.serviceTerminatedDate == undefined ? null : stopAndKnockModel.serviceTerminatedDate;
    this.addressId = stopAndKnockModel == undefined ? null : stopAndKnockModel.addressId == undefined ? null : stopAndKnockModel.addressId;
    this.contactPersonTypeId = stopAndKnockModel == undefined ? null : stopAndKnockModel.contactPersonTypeId == undefined ? null : stopAndKnockModel.contactPersonTypeId;
    this.sourceId = stopAndKnockModel == undefined ? null : stopAndKnockModel.sourceId == undefined ? null : stopAndKnockModel.sourceId;
    this.siteTypeId = stopAndKnockModel == undefined ? null : stopAndKnockModel.siteTypeId == undefined ? null : stopAndKnockModel.siteTypeId;
    this.leadGroupId = stopAndKnockModel == undefined ? null : stopAndKnockModel.leadGroupId == undefined ? null : stopAndKnockModel.leadGroupId;
    this.createdUserId = stopAndKnockModel == undefined ? null : stopAndKnockModel.createdUserId == undefined ? null : stopAndKnockModel.createdUserId;
    this.leadCategoryId = stopAndKnockModel == undefined ? [] : stopAndKnockModel.leadCategoryId == undefined ? [] : stopAndKnockModel.leadCategoryId;
    this.leadValidationType = stopAndKnockModel == undefined ? '' : stopAndKnockModel.leadValidationType == undefined ? '' : stopAndKnockModel.leadValidationType;
    this.customerId = stopAndKnockModel == undefined ? '' : stopAndKnockModel.customerId == undefined ? '' : stopAndKnockModel.customerId;
  }
  basicInfo?: StopAndKnockBasicInfoModel;
  contactInfo?: StopAndKnockContactInfoModel;
  addressInfo?: StopAndKnockAddressModel;
  stopAndKnockCreateOrUpdateModel: StopAndKnockCreateOrUpdateModel;
  stopAndKnockId?: string;
  stopAndKnockRefNo?: string;
  serviceTerminatedDate?: Date;
  addressId?: string;
  contactPersonTypeId?: number;
  sourceId?: number;
  siteTypeId?: number;
  leadGroupId?: number;
  createdUserId?: string;
  leadCategoryId?: [];
  leadValidationType?:string;
  customerId?:string;
}
class StopAndKnockContactInfoModel {
  constructor(stopAndKnockContactInfoModel?: StopAndKnockContactInfoModel) {
    this.contactId = stopAndKnockContactInfoModel == undefined ? null : stopAndKnockContactInfoModel.contactId == undefined ? null : stopAndKnockContactInfoModel.contactId;
    this.email = stopAndKnockContactInfoModel == undefined ? "" : stopAndKnockContactInfoModel.email == undefined ? "" : stopAndKnockContactInfoModel.email;
    this.mobile1 = stopAndKnockContactInfoModel == undefined ? "" : stopAndKnockContactInfoModel.mobile1 == undefined ? "" : stopAndKnockContactInfoModel.mobile1;
    this.mobile1CountryCode = stopAndKnockContactInfoModel == undefined ? defaultCountryCode : stopAndKnockContactInfoModel.mobile1CountryCode == undefined ? defaultCountryCode : stopAndKnockContactInfoModel.mobile1CountryCode;
    this.mobile2 = stopAndKnockContactInfoModel == undefined ? "" : stopAndKnockContactInfoModel.mobile2 == undefined ? "" : stopAndKnockContactInfoModel.mobile2;
    this.mobile2CountryCode = stopAndKnockContactInfoModel == undefined ? defaultCountryCode : stopAndKnockContactInfoModel.mobile2CountryCode == undefined ? defaultCountryCode : stopAndKnockContactInfoModel.mobile2CountryCode;
    this.officeNo = stopAndKnockContactInfoModel == undefined ? "" : stopAndKnockContactInfoModel.officeNo == undefined ? "" : stopAndKnockContactInfoModel.officeNo;
    this.officeNoCountryCode = stopAndKnockContactInfoModel == undefined ? defaultCountryCode : stopAndKnockContactInfoModel.officeNoCountryCode == undefined ? defaultCountryCode : stopAndKnockContactInfoModel.officeNoCountryCode;
    this.premisesNo = stopAndKnockContactInfoModel == undefined ? "" : stopAndKnockContactInfoModel.premisesNo == undefined ? "" : stopAndKnockContactInfoModel.premisesNo;
    this.premisesNoCountryCode = stopAndKnockContactInfoModel == undefined ? defaultCountryCode : stopAndKnockContactInfoModel.premisesNoCountryCode == undefined ? defaultCountryCode : stopAndKnockContactInfoModel.premisesNoCountryCode;
  }
  contactId?: string;
  email?: string;
  mobile1?: string;
  mobile1CountryCode?: string;
  mobile2?: string;
  mobile2CountryCode?: string;
  officeNo?: string;
  officeNoCountryCode?: string;
  premisesNo?: string;
  premisesNoCountryCode?: string;
}
class StopAndKnockBasicInfoModel {
  constructor(stopAndKnockBasicInfoModel?: StopAndKnockBasicInfoModel) {
    this.customerTypeId = stopAndKnockBasicInfoModel == undefined ? null : stopAndKnockBasicInfoModel.customerTypeId == undefined ? null : stopAndKnockBasicInfoModel.customerTypeId;
    this.titleId = stopAndKnockBasicInfoModel == undefined ? null : stopAndKnockBasicInfoModel.titleId == undefined ? null : stopAndKnockBasicInfoModel.titleId;
    this.firstName = stopAndKnockBasicInfoModel == undefined ? "" : stopAndKnockBasicInfoModel.firstName == undefined ? "" : stopAndKnockBasicInfoModel.firstName;
    this.lastName = stopAndKnockBasicInfoModel == undefined ? "" : stopAndKnockBasicInfoModel.lastName == undefined ? "" : stopAndKnockBasicInfoModel.lastName;
    this.said = stopAndKnockBasicInfoModel == undefined ? null : stopAndKnockBasicInfoModel.said == undefined ? null : stopAndKnockBasicInfoModel.said;
    this.companyName = stopAndKnockBasicInfoModel == undefined ? "" : stopAndKnockBasicInfoModel.companyName == undefined ? "" : stopAndKnockBasicInfoModel.companyName;
    this.companyRegNo = stopAndKnockBasicInfoModel == undefined ? "" : stopAndKnockBasicInfoModel.companyRegNo == undefined ? "" : stopAndKnockBasicInfoModel.companyRegNo;
  }
  customerTypeId?: number;
  titleId?: number;
  firstName?: string;
  lastName?: string;
  said?: number;
  companyName?: string;
  companyRegNo?: string;
}
class StopAndKnockCreateOrUpdateModel {
  constructor(stopAndKnockUpdateModel?: StopAndKnockCreateOrUpdateModel) {
    this.stopAndKnockId = stopAndKnockUpdateModel == undefined ? "" : stopAndKnockUpdateModel.stopAndKnockId == undefined ? "" :
      stopAndKnockUpdateModel.stopAndKnockId;
    this.addressId = stopAndKnockUpdateModel == undefined ? "" : stopAndKnockUpdateModel.addressId == undefined ? "" :
      stopAndKnockUpdateModel.addressId;
    this.leadCategoryId = stopAndKnockUpdateModel == undefined ? [] : stopAndKnockUpdateModel.leadCategoryId == undefined ? [] :
      stopAndKnockUpdateModel.leadCategoryId;
    this.serviceTerminatedDate = stopAndKnockUpdateModel == undefined ? "" : stopAndKnockUpdateModel.serviceTerminatedDate == undefined ? "" :
      stopAndKnockUpdateModel.serviceTerminatedDate;
    this.assignedToUserId = stopAndKnockUpdateModel == undefined ? "" : stopAndKnockUpdateModel.assignedToUserId == undefined ? "" :
      stopAndKnockUpdateModel.assignedToUserId;
    this.contactPersonTypeId = stopAndKnockUpdateModel == undefined ? null : stopAndKnockUpdateModel.contactPersonTypeId == undefined ? null :
      stopAndKnockUpdateModel.contactPersonTypeId;
    this.modifiedUserId = stopAndKnockUpdateModel == undefined ? "" : stopAndKnockUpdateModel.modifiedUserId == undefined ? "" :
      stopAndKnockUpdateModel.modifiedUserId;
    this.modifiedDate = stopAndKnockUpdateModel == undefined ? "" : stopAndKnockUpdateModel.modifiedDate == undefined ? "" :
      stopAndKnockUpdateModel.modifiedDate;
    this.sourceId = stopAndKnockUpdateModel == undefined ? "" : stopAndKnockUpdateModel.sourceId == undefined ? "" :
      stopAndKnockUpdateModel.sourceId;
    this.siteTypeId = stopAndKnockUpdateModel == undefined ? "" : stopAndKnockUpdateModel.siteTypeId == undefined ? "" :
      stopAndKnockUpdateModel.siteTypeId;
  }
  stopAndKnockId?: string;
  addressId?: string;
  leadCategoryId?: [];
  sourceId?: string;
  siteTypeId?: string;
  serviceTerminatedDate?: string;
  assignedToUserId?: string;
  contactPersonTypeId?: number;
  modifiedUserId?: string;
  modifiedDate?: string;
  basicInfo: StopAndKnockBasicInfoModel;
  contactInfo: StopAndKnockContactInfoModel;
}
class StopAndKnockAddressModel {
  constructor(addressModel?: StopAndKnockAddressModel) {
    this.addressId = addressModel == undefined ? "" : addressModel.addressId == undefined ? "" : addressModel.addressId;
    this.suburbName = addressModel == undefined ? "" : addressModel.suburbName == undefined ? "" : addressModel.suburbName;
    this.provinceName = addressModel == undefined ? "" : addressModel.provinceName == undefined ? "" : addressModel.provinceName;
    this.cityName = addressModel == undefined ? "" : addressModel.cityName == undefined ? "" : addressModel.cityName;
    this.addressConfidentLevelName = addressModel == undefined ? null : addressModel.addressConfidentLevelName == undefined ? null : addressModel.addressConfidentLevelName;
    this.districtName = addressModel == undefined ? "" : addressModel.districtName == undefined ? "" : addressModel.districtName;
    this.buildingName = addressModel == undefined ? "" : addressModel.buildingName == undefined ? "" : addressModel.buildingName;
    this.buildingNo = addressModel == undefined ? "" : addressModel.buildingNo == undefined ? "" : addressModel.buildingNo;
    this.streetNo = addressModel == undefined ? "" : addressModel.streetNo == undefined ? "" : addressModel.streetNo;
    this.streetName = addressModel == undefined ? "" : addressModel.streetName == undefined ? "" : addressModel.streetName;
    this.estateName = addressModel == undefined ? "" : addressModel.estateName == undefined ? "" : addressModel.estateName;
    this.estateStreetNo = addressModel == undefined ? "" : addressModel.estateStreetNo == undefined ? "" : addressModel.estateStreetNo;
    this.estateStreetName = addressModel == undefined ? "" : addressModel.estateStreetName == undefined ? "" : addressModel.estateStreetName;
    this.formatedAddress = addressModel == undefined ? "" : addressModel.formatedAddress == undefined ? "" : addressModel.formatedAddress;
    this.jsonObject = addressModel == undefined ? "" : addressModel.jsonObject == undefined ? "" : addressModel.jsonObject;
    this.seoid = addressModel == undefined ? "" : addressModel.seoid == undefined ? "" : addressModel.seoid;
    this.provinceId = addressModel == undefined ? "" : addressModel.provinceId == undefined ? "" : addressModel.provinceId;
    this.suburbId = addressModel == undefined ? "" : addressModel.suburbId == undefined ? "" : addressModel.suburbId;
    this.cityId = addressModel == undefined ? "" : addressModel.cityId == undefined ? "" : addressModel.cityId;
    this.postalCode = addressModel == undefined ? "" : addressModel.postalCode == undefined ? "" : addressModel.postalCode;
    this.longitude = addressModel == undefined ? "" : addressModel.longitude == undefined ? "" : addressModel.longitude;
    this.latitude = addressModel == undefined ? "" : addressModel.latitude == undefined ? "" : addressModel.latitude;
    this.addressConfidentLevelId = addressModel == undefined ? null : addressModel.addressConfidentLevelId == undefined ? null : addressModel.addressConfidentLevelId;
    this.isAddressComplex = addressModel == undefined ? false : addressModel.isAddressComplex == undefined ? false : addressModel.isAddressComplex;
    this.latLong = addressModel == undefined ? "" : addressModel.latLong == undefined ? "" : addressModel.latLong;
    this.isAddressExtra = addressModel == undefined ? false : addressModel.isAddressExtra == undefined ? false : addressModel.isAddressExtra;
  }
  addressId?: string;
  suburbName?: string;
  cityName?: string;
  provinceName?: string;
  addressConfidentLevelName?: string;
  districtName?: string;
  buildingName?: string;
  buildingNo?: string;
  streetNo?: string;
  streetName?: string;
  estateName?: string;
  estateStreetNo?: string;
  estateStreetName?: string;
  formatedAddress?: string;
  jsonObject?: string;
  seoid?: string;
  provinceId?: string;
  suburbId?: string;
  cityId?: string;
  postalCode?: string;
  longitude?: string;
  latitude?: string;
  addressConfidentLevelId?: string;
  isAddressComplex?: boolean;
  isAddressExtra?: boolean;
  latLong?: string;
}
class StopAndKnockAdditionalOptionsModel {
  constructor(stopAndKnockAdditionalOptionsModel?: StopAndKnockAdditionalOptionsModel) {
    this.notes = stopAndKnockAdditionalOptionsModel == undefined ? "" : stopAndKnockAdditionalOptionsModel.notes == undefined ? "" : stopAndKnockAdditionalOptionsModel.notes;
    this.followupDateAndTime = stopAndKnockAdditionalOptionsModel == undefined ? "" : stopAndKnockAdditionalOptionsModel.followupDateAndTime == undefined ? "" : stopAndKnockAdditionalOptionsModel.followupDateAndTime;
    this.contactNo = stopAndKnockAdditionalOptionsModel == undefined ? "" : stopAndKnockAdditionalOptionsModel.contactNo == undefined ? "" : stopAndKnockAdditionalOptionsModel.contactNo;
    this.contactNoCountryCode = stopAndKnockAdditionalOptionsModel == undefined ? defaultCountryCode : stopAndKnockAdditionalOptionsModel.contactNoCountryCode == undefined ? defaultCountryCode : stopAndKnockAdditionalOptionsModel.contactNoCountryCode;
    this.agentUserGroupId = stopAndKnockAdditionalOptionsModel == undefined ? "" : stopAndKnockAdditionalOptionsModel.agentUserGroupId == undefined ? "" : stopAndKnockAdditionalOptionsModel.agentUserGroupId;
    this.agentUserId = stopAndKnockAdditionalOptionsModel == undefined ? "" : stopAndKnockAdditionalOptionsModel.agentUserId == undefined ? "" : stopAndKnockAdditionalOptionsModel.agentUserId;
    this.sellerUserGroupId = stopAndKnockAdditionalOptionsModel == undefined ? "" : stopAndKnockAdditionalOptionsModel.sellerUserGroupId == undefined ? "" : stopAndKnockAdditionalOptionsModel.sellerUserGroupId;
    this.sellerUserId = stopAndKnockAdditionalOptionsModel == undefined ? "" : stopAndKnockAdditionalOptionsModel.sellerUserId == undefined ? "" : stopAndKnockAdditionalOptionsModel.sellerUserId;
  }
  notes?: string;
  followupDateAndTime?: string;
  contactNo?: string;
  contactNoCountryCode?: string;
  agentUserGroupId?: string;
  agentUserId?: string;
  sellerUserGroupId?: string;
  sellerUserId?: string;
}
class StopAndKnockRemoveEquipmentModel {
  constructor(removeEquipmentModel?: StopAndKnockRemoveEquipmentModel) {
    this.contactPersonTypeId = removeEquipmentModel == undefined ? 0 : removeEquipmentModel.contactPersonTypeId == undefined ? 0 : removeEquipmentModel.contactPersonTypeId;
    this.stopAndKnockId = removeEquipmentModel == undefined ? null : removeEquipmentModel.stopAndKnockId == undefined ? null : removeEquipmentModel.stopAndKnockId;
    this.email = removeEquipmentModel == undefined ? "" : removeEquipmentModel.email == undefined ? "" : removeEquipmentModel.email;
    this.mobile1 = removeEquipmentModel == undefined ? "" : removeEquipmentModel.mobile1 == undefined ? "" : removeEquipmentModel.mobile1;
    this.mobile1CountryCode = removeEquipmentModel == undefined ? defaultCountryCode : removeEquipmentModel.mobile1CountryCode == undefined ? defaultCountryCode : removeEquipmentModel.mobile1CountryCode;
    this.officeNo = removeEquipmentModel == undefined ? "" : removeEquipmentModel.officeNo == undefined ? "" : removeEquipmentModel.officeNo;
    this.officeNoCountryCode = removeEquipmentModel == undefined ? defaultCountryCode : removeEquipmentModel.officeNoCountryCode == undefined ? defaultCountryCode : removeEquipmentModel.officeNoCountryCode;
    this.removalDate = removeEquipmentModel == undefined ? new Date() : removeEquipmentModel.removalDate == undefined ? new Date() : removeEquipmentModel.removalDate;
    this.firstName = removeEquipmentModel == undefined ? null : removeEquipmentModel.firstName == undefined ? null : removeEquipmentModel.firstName;
    this.lastName = removeEquipmentModel == undefined ? null : removeEquipmentModel.lastName == undefined ? null : removeEquipmentModel.lastName;
    this.isMorning = removeEquipmentModel == undefined ? false : removeEquipmentModel.isMorning == undefined ? false : removeEquipmentModel.isMorning;
    this.isAfternoon = removeEquipmentModel == undefined ? false : removeEquipmentModel.isAfternoon == undefined ? false : removeEquipmentModel.isAfternoon;
    this.notes = removeEquipmentModel == undefined ? null : removeEquipmentModel.notes == undefined ? null : removeEquipmentModel.notes;
    this.stopAndKnockRefNo = removeEquipmentModel == undefined ? null : removeEquipmentModel.stopAndKnockRefNo == undefined ? null : removeEquipmentModel.stopAndKnockRefNo;
  }
  contactPersonTypeId?: number;
  email?: string;
  mobile1?: string;
  mobile1CountryCode?: string;
  officeNo?: string;
  officeNoCountryCode?: string;
  removalDate?: Date;
  stopAndKnockId?: string;
  firstName?: string;
  lastName?: string;
  isMorning?: boolean;
  isAfternoon?: boolean;
  notes?: string;
  stopAndKnockRefNo?: string;
}

export {
  StopAndKnockModel, StopAndKnockAddressModel, StopAndKnockBasicInfoModel, StopAndKnockContactInfoModel,
  StopAndKnockCreateOrUpdateModel, StopAndKnockAdditionalOptionsModel, StopAndKnockRemoveEquipmentModel
};