export class CalendarEventModel {
    constructor(calendarEventModel?:CalendarEventModel){
      this.actionTypeId = calendarEventModel ? calendarEventModel.actionTypeId == undefined ? '' : calendarEventModel.actionTypeId : '';
      this.title = calendarEventModel ? calendarEventModel.title == undefined ? '' : calendarEventModel.title : '';
      this.start = calendarEventModel ? calendarEventModel.start == undefined ? '' : calendarEventModel.start : '';
      this.end = calendarEventModel ? calendarEventModel.end == undefined ? '' : calendarEventModel.end : '';
    }
    public actionTypeId?: string;
    public title?:string;
    public start?:string;
    public end?:string;
  }
  
  