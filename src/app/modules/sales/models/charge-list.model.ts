export class ChargeListManualAddEditModel {
    constructor(chargeListManualAddEditModel?: ChargeListManualAddEditModel) {
        this.chargeListNumber = chargeListManualAddEditModel ? chargeListManualAddEditModel.chargeListNumber == undefined ? '' : chargeListManualAddEditModel.chargeListNumber : '';
        this.startDate = chargeListManualAddEditModel ? chargeListManualAddEditModel.startDate == undefined ? '' : chargeListManualAddEditModel.startDate : '';
        this.endDate = chargeListManualAddEditModel ? chargeListManualAddEditModel.endDate == undefined ? '' : chargeListManualAddEditModel.endDate : '';
        this.exclVAT = chargeListManualAddEditModel ? chargeListManualAddEditModel.exclVAT == undefined ? '' : chargeListManualAddEditModel.exclVAT : '';
        this.inclVAT = chargeListManualAddEditModel ? chargeListManualAddEditModel.inclVAT == undefined ? '' : chargeListManualAddEditModel.inclVAT : '';
        this.discountExclVAT = chargeListManualAddEditModel ? chargeListManualAddEditModel.discountExclVAT == undefined ? '' : chargeListManualAddEditModel.discountExclVAT : '';
        this.discountTotal = chargeListManualAddEditModel ? chargeListManualAddEditModel.discountTotal == undefined ? '' : chargeListManualAddEditModel.discountTotal : '';
        this.totalExclVAT = chargeListManualAddEditModel ? chargeListManualAddEditModel.totalExclVAT == undefined ? '' : chargeListManualAddEditModel.totalExclVAT : '';
        this.totalInclVAT = chargeListManualAddEditModel ? chargeListManualAddEditModel.totalInclVAT == undefined ? '' : chargeListManualAddEditModel.totalInclVAT : '';
        this.runDate = chargeListManualAddEditModel ? chargeListManualAddEditModel.runDate == undefined ? '' : chargeListManualAddEditModel.runDate : '';
        this.runByName = chargeListManualAddEditModel ? chargeListManualAddEditModel.runByName == undefined ? '' : chargeListManualAddEditModel.runByName : '';
        this.division = chargeListManualAddEditModel ? chargeListManualAddEditModel.division == undefined ? '' : chargeListManualAddEditModel.division : '';
    }
    chargeListNumber?: string;
    startDate?: string;
    endDate?: string;
    exclVAT?: string;
    inclVAT?: string;
    discountExclVAT?: string;
    discountTotal?: string;
    totalExclVAT?: string;
    totalInclVAT?: string;
    runDate?: string;
    runByName?: string;
    division?: string;
}