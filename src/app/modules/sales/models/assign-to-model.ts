export class AssignToModel {
  constructor(assignToModel?: AssignToModel) {
    this.leadId = assignToModel ? assignToModel.leadId == undefined ? '' : assignToModel.leadId : '';
    this.assignedTo = assignToModel ? assignToModel.assignedTo == undefined ? '' : assignToModel.assignedTo : '';
    this.groupId = assignToModel ? assignToModel.groupId == undefined ? '' : assignToModel.groupId : '';
    this.createdUserId = assignToModel ? assignToModel.createdUserId == undefined ? '' : assignToModel.createdUserId : '';
    this.modifiedUserId = assignToModel ? assignToModel.modifiedUserId == undefined ? '' : assignToModel.modifiedUserId : '';
  }
  public leadId?: string;
  public assignedTo?: string;
  public groupId?: string;
  public createdUserId?: string;
  public modifiedUserId?: string;
}
