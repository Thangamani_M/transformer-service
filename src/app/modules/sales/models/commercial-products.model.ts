
class CommercialProducts {
    constructor(CommercialProducts?: CommercialProducts) {
        this.commercialProductId = CommercialProducts ? CommercialProducts.commercialProductId == undefined ? '' : CommercialProducts.commercialProductId : '';
        this.commercialProductName = CommercialProducts ? CommercialProducts.commercialProductName == undefined ? '' : CommercialProducts.commercialProductName : '';
        this.description = CommercialProducts ? CommercialProducts.description == undefined ? '' : CommercialProducts.description : '';
        this.createdUserId = CommercialProducts ? CommercialProducts.createdUserId == undefined ? 'True' : CommercialProducts.createdUserId : '';
        this.modifiedUserId = CommercialProducts ? CommercialProducts.modifiedUserId == undefined ? 'True' : CommercialProducts.modifiedUserId : '';
    }
    commercialProductId?: string
    commercialProductName?: string
    description?: string;
    createdUserId: string;
    modifiedUserId: string;
}
class SellerSkills {
    constructor(SellerSkills?: SellerSkills) {
        this.skillName = SellerSkills ? SellerSkills.skillName == undefined ? '' : SellerSkills.skillName : '';
        this.description = SellerSkills ? SellerSkills.description == undefined ? '' : SellerSkills.description : '';
        this.createdUserId = SellerSkills ? SellerSkills.createdUserId == undefined ? 'True' : SellerSkills.createdUserId : '';
    }
    skillName?: string
    description?: string;
    createdUserId: string;
}
export { CommercialProducts, SellerSkills }