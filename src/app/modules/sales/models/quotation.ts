export class Quotation {
    quotationId: string;
    quotationTypeId: string;
    customerId: any;
    leadId: any;
    addressId: any;
    assignedById: any;
    assignedToId: any;
    targetDate: any;
    expiryDate: Date;
    itemOwnerShipTypeId: any;
    description: string;
    termsAndConditions: string;
    quotationVersion: QuotationVersion;
    quotationItems: Array<QuotationItem> = [];
    quotationServices: Array<QuotationService> = [];
}
export class QuotationVersion {
    approvalMode: any;
    discountAmount: any;
    discountPercentage: any;
    transportCharge: any;
    grandTotal: any;
    quotationStatus: any;
    billingIntervalId: any;
    isQuotationSentToCustomer: boolean;
    isQuotationAcceptByCustomer: boolean;
    customerComments: string;
    isNegotiation: boolean;
}
export class QuotationItem {
    quotationDate: Date;
    itemId: any;
    uOMId: any;
    existingQty: number;
    newQty: number;
    price: any;
    taxTypeId: any;
    taxAmount: any;
    discountAmount: any;
    discountPercentage: any;
    labourCost: any;
    totalAmount: any;
}
export class QuotationService {
    servicePriceId: any;
}
