class LeadEmailNotificationtModel {
    constructor(leadNotification? : LeadEmailNotificationtModel) {
        this.leadId = leadNotification? leadNotification.leadId == undefined ? null : leadNotification.leadId : null;
        this.notificationTo = leadNotification? leadNotification.notificationTo == undefined ? '' : leadNotification.notificationTo : '';
        this.notificationCC = leadNotification? leadNotification.notificationCC == undefined ? null : leadNotification.notificationCC : null;
        this.notificationSubject = leadNotification? leadNotification.notificationSubject == undefined ? '' : leadNotification.notificationSubject : '';
        this.notificationText = leadNotification? leadNotification.notificationText == undefined ? '' : leadNotification.notificationText : '';
        this.createdUserId = leadNotification? leadNotification.createdUserId == undefined ? '' : leadNotification.createdUserId : '';       
     }
    leadId? : string;
    notificationTo? : string;
    notificationCC? : string;
    notificationSubject? : string;
    notificationText? : string;
    createdUserId? : string;
}

export {LeadEmailNotificationtModel};