export class Address {

    constructor() {
        this.addressId = '';
        this.leadId = '';
        this.CcustomerID = '';
        this.addressTypeID = '';
        this.buildingNumber = '';
        this.buildingName = '';
        this.streetAddress = '';
        this.provinceName = '';
        this.cityName = '';
        this.suburbName = '';
        this.postalCode = '';
        this.latLang = '';
        this.latitude = '';
        this.longitude = '';
        this.isPrimary = false;
        this.fullAddress = '';
        this.jsonAddress = '';
    }
    addressId?: string;
    leadId?: string;
    CcustomerID?: string;
    addressTypeID?: string;
    postalCode?: string;
    buildingNumber?: string;
    buildingName?: string;
    longitude?: string;
    latitude?: string;
    isVerified?: boolean;
    isActive?: boolean;
    streetAddress?: string;
    isPrimary?: boolean;
    latLang?: string;
    provinceName?: string;
    provinceCode?: string;
    cityName?: string;
    cityCode?: string;
    suburbName?: string;
    fullAddress?: string;
    jsonAddress?: string;
}