export class TaskRawlead {
    RawLeadId?: string;
    RawLeadNumber: string;
    ClientName: string;
    Email: string;
    ContactNumber: string;
    Suburb: string;
    City: string;
    LeadCategoryName: string;
    LeadId: string;
    status: string;
    CssClass: string;
}
export class TaskRawleadCustomerDetails {
    customerId: string;
    customerAddressId: string;
    customerNumber: string;
    customername: string;
    email: string;
    suburb: string;
    phone: string;
    siteAddress: string;
}
export class params {
    Email?: string;
    PhoneNumber?: string;
}
export class Tasklead {
    leadId?: string;
    leadNumber: string;
    clientName: string;
    email: string;
    contactNumber: string;
    probabilityOfClosure: string;
    leadCategoryName: string;
    status: string;
    CssClass: string;
}
export class Taskcalls {
    callLogNumber?: string;
    clientName: string;
    startTime: string;
    endTime: string;
    holdTime: string;
    comments: string;
}