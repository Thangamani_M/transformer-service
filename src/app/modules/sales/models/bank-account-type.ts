export class BankAccountType {
  accountTypeId: string;
  accountTypeName: string;
  description: string;
  cssClass: string;
  isDeleted: boolean;
  isSystem: boolean;
  isActive: boolean;
  createdDate: Date;
  createdUserId: string;
  modifiedDate: Date;
  modifiedUserId: string;
}
