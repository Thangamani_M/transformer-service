export class PartitionSearch {
    constructor(partitionSearch?: PartitionSearch) {
        this.customerName = partitionSearch ? partitionSearch.customerName == undefined ? '' : partitionSearch.customerName : '';
        this.customerNumber = partitionSearch ? partitionSearch.customerNumber == undefined ? '' : partitionSearch.customerNumber : '';
        this.partition = partitionSearch ? partitionSearch.partition == undefined ? '' : partitionSearch.partition : '';
        this.partitionCode = partitionSearch ? partitionSearch.partitionCode == undefined ? '' : partitionSearch.partitionCode : '';
        this.partitionId = partitionSearch ? partitionSearch.partitionId == undefined ? '' : partitionSearch.partitionId : '';
        this.partitionLeadId = partitionSearch ? partitionSearch.partitionLeadId == undefined ? '' : partitionSearch.partitionLeadId : '';
        this.partitionName = partitionSearch ? partitionSearch.partitionName == undefined ? '' : partitionSearch.partitionName : '';
        this.primaryLeadId = partitionSearch ? partitionSearch.primaryLeadId == undefined ? '' : partitionSearch.primaryLeadId : '';
        this.status = partitionSearch ? partitionSearch.status == undefined ? '' : partitionSearch.status : '';
        this.address = partitionSearch ? partitionSearch.address == undefined ? '' : partitionSearch.address : '';
        this.partitionPaymentTypeName = partitionSearch ? partitionSearch.partitionPaymentTypeName == undefined ? '' : partitionSearch.partitionPaymentTypeName : '';
        this.paymentTypeName = partitionSearch ? partitionSearch.paymentTypeName == undefined ? '' : partitionSearch.paymentTypeName : '';
        this.isPrimary = partitionSearch == undefined ? false : partitionSearch.isPrimary == undefined ? false : partitionSearch.isPrimary;
    }
    customerName: string;
    customerNumber: string;
    isPrimary: boolean;
    partition: string;
    partitionCode: string;
    partitionId: string;
    partitionLeadId: string;
    partitionName: string;
    primaryLeadId: string;
    status: string;
    address: string;
    partitionPaymentTypeName: string;
    paymentTypeName: string;
}
export class SecondaryPartition {
    constructor(secondaryPartition?: SecondaryPartition) {
        this.partition = secondaryPartition ? secondaryPartition.partition == undefined ? '' : secondaryPartition.partition : '';
        this.partitionName = secondaryPartition ? secondaryPartition.partitionName == undefined ? '' : secondaryPartition.partitionName : '';
    }
    partition?: string
    partitionName?: string
}