class FollowupAddEditModel{
    constructor(followup?:FollowupAddEditModel){ 
        this.targetDate=followup?followup.targetDate==undefined?'':followup.targetDate:'';
        this.followupTypeId=followup?followup.followupTypeId==undefined?'':followup.followupTypeId:'';
        this.description = followup?followup.description==undefined?'':followup.description:'';
        this.leadId = followup?followup.leadId==undefined?'':followup.leadId:'';
    }
    targetDate?: string ;
    followupTypeId?:string;
    description?:string;
    leadId?:string;
}
export{FollowupAddEditModel}