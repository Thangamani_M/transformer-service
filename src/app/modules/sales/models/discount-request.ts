
class DiscountRequestManualAddModel {
    constructor(discountRequestManualAddModel?: DiscountRequestManualAddModel) {
        this.quotationConcessionLogId = discountRequestManualAddModel ? discountRequestManualAddModel.quotationConcessionLogId == undefined ? '' : discountRequestManualAddModel.quotationConcessionLogId : '';
        this.quotationConcessionId = discountRequestManualAddModel ? discountRequestManualAddModel.quotationConcessionId == undefined ? '' : discountRequestManualAddModel.quotationConcessionId : '';
        this.status = discountRequestManualAddModel ? discountRequestManualAddModel.status == undefined ? '' : discountRequestManualAddModel.status : '';
        this.concessionPercentage = discountRequestManualAddModel ? discountRequestManualAddModel.concessionPercentage == undefined ? '' : discountRequestManualAddModel.concessionPercentage : '';
        this.comments = discountRequestManualAddModel ? discountRequestManualAddModel.comments == undefined ? '' : discountRequestManualAddModel.comments : '';
        this.isApproved = discountRequestManualAddModel ? discountRequestManualAddModel.isApproved == true ? false : discountRequestManualAddModel.isApproved : true;
        this.approvedBy = discountRequestManualAddModel ? discountRequestManualAddModel.approvedBy == undefined ? '' : discountRequestManualAddModel.approvedBy : '';
        this.createdUserId = discountRequestManualAddModel ? discountRequestManualAddModel.createdUserId == undefined ? '' : discountRequestManualAddModel.createdUserId : '';
        this.quotationId = discountRequestManualAddModel ? discountRequestManualAddModel.quotationId == undefined ? '' : discountRequestManualAddModel.quotationId : '';
        this.referenceId = discountRequestManualAddModel ? discountRequestManualAddModel.referenceId == undefined ? '' : discountRequestManualAddModel.referenceId : '';
        this.requestType = discountRequestManualAddModel ? discountRequestManualAddModel.requestType == undefined ? '' : discountRequestManualAddModel.requestType : '';
    }
    quotationConcessionLogId?: string;
    quotationConcessionId?: string;
    status?: string;
    concessionPercentage?: string;
    comments?: string;
    isApproved?: boolean;
    isCancelled?: boolean;
    approvedBy?: string;
    createdUserId?: string;
    quotationId?: string;
    referenceId: string;
    requestType: string;
}
class DiscountRequest {
    quotationId?: string;
    employeeName: string;
    customerId: string;
    clientName: string;
    suburb: string;
    district: string;
    town: string;
    motivationReason: string;
    status: string;
}

export { DiscountRequestManualAddModel, DiscountRequest };

