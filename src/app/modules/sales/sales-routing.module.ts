import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalesDashboardComponent } from '@sales/components/dashboard';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';

const routes: Routes = [
    { path: 'dashboard', component: SalesDashboardComponent, data: { title: 'Sales Dashboard' } },
    { path: 'raw-lead', loadChildren: () => import('../sales/components/raw-lead/raw-lead.module').then(m => m.RawLeadModule) },
    { path: 'task-management', loadChildren: () => import('../sales/components/task-management/task-management.module').then(m => m.TaskManagementModule) },
    { path: 'stop-and-knock', loadChildren: () => import('../sales/components/stop-and-knock/stop-and-knock.module').then(m => m.StopAndKnockModule) },
    { path: 'reward-partnerships', loadChildren: () => import('../sales/components/reward-criteria/reward-criteria.module').then(m => m.RewardCriteriaModule) },
    { path: 'new-address-approval', loadChildren: () => import('../sales/components/raw-lead-new-address-request/raw-lead-new-address-request.module').then(m => m.RawLeadNewAddressRequestModule) },
    { path: 'marketing-data', loadChildren: () => import('../sales/components/markating-data-sync-dashboard/markating-data-sync-dashboard.module').then(m => m.MarkatingDataSyncDashboardModule),canActivate: [AuthGuard]  },
    { path: 'marketing-data-reports', loadChildren: () => import('../sales/components/markating-data-sync-reports/markating-data-sync-reports.module').then(m => m.MarkatingDataSyncReportsModule),canActivate: [AuthGuard]  },
    { path: 'sms-campaign', loadChildren: () => import('../sales/components/sms-campaign/sms-campaign.module').then(m => m.SmsCampaignModule) ,canActivate: [AuthGuard] }
];
@NgModule({
    imports: [RouterModule.forChild(routes)]
})

export class SalesRoutingModule { }
