import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-lss-registartion-view',
  templateUrl: './lss-registration-view.component.html'
})

export class LssRegistartionViewComponent {
  lssId: string;
  viewData = [
    {
      name: 'BASIC INFORMATION', columns: [
        { name: 'Scheme ID', value: "" },
        { name: 'Scheme Name', value: "" },
        { name: 'Scheme Type', value: "" },
        { name: 'Region', value: "" },
        { name: 'District', value: "" },
        { name: 'Branch Name', value: "" },
        { name: 'Branch Code', value: "" },
        { name: 'Contract No', value: "" },
        { name: 'Billstock ID', value: "" },
        { name: 'Resources', value: "" },
        { name: 'Start Date', value: "" },
        { name: 'Renewal Date', value: "" }, { name: 'Start Up Cost', value: "", isRandSymbolRequired: true },
        { name: 'Go Live Date', value: "" },
      ]
    },
    {
      name: 'CDM INFORMATION', columns: [
        { name: 'Name', value: "" },
        { name: 'Surname', value: "" },
        { name: 'Employee ID', value: "" }, { name: 'Mobile Number', value: "", isFullNumber: true },
        { name: 'Email', value: "" },
      ]
    },
    {
      name: 'RCDM INFORMATION', columns: [
        { name: 'Name', value: "" },
        { name: 'Surname', value: "" },
        { name: 'Employee ID', value: "" }, { name: 'Mobile Number', value: "", isFullNumber: true },
        { name: 'Email', value: "" },
      ]
    }
  ]
  primengTableConfigProperties: any = {
    tableCaption: "LSS Registration View",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'LSS ', relativeRouterUrl: '/lss' }, {
      displayName: 'View LSS'
    }],
    tableComponentConfigs: {
      tabsList: [
        {
          enableBreadCrumb: true,
          enableAction: true,
          enableEditActionBtn: true
        }]
    }
  }
  componentPermissions = [];

  constructor(private rxjsService: RxjsService, private router: Router, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.lssId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LSS_REGISTRATION, this.lssId, false, null).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.viewData = [
            {
              name: 'BASIC INFORMATION', columns: [
                { name: 'Scheme ID', value: response.resources?.lssRefNo },
                { name: 'Scheme Name', value: response.resources?.lssName },
                { name: 'Scheme Type', value: response.resources?.lssTypeName },
                { name: 'Region', value: response.resources?.regionName },
                { name: 'District', value: response.resources?.districtName },
                { name: 'Branch Name', value: response.resources?.branchName },
                { name: 'Branch Code', value: response.resources?.branchNode },
                { name: 'Contract No', value: response.resources?.contractNo },
                { name: 'Billstock ID', value: response.resources?.billstockRefNo },
                { name: 'Resources', value: response.resources?.lssResourceName },
                { name: 'Start Date', value: response.resources?.startDate },
                { name: 'Renewal Date', value: response.resources?.renewalDate },
                { name: 'Start Up Cost', value: response.resources?.startUpCost, isRandSymbolRequired: true },
                { name: 'Go Live Date', value: response.resources?.goLiveDate },
              ]
            },
            {
              name: 'CDM INFORMATION', columns: [
                { name: 'Name', value: response.resources?.cdmName },
                { name: 'Surname', value: response.resources?.cdmLastName },
                { name: 'Employee ID', value: response.resources?.cdmEmployeeCode },
                { name: 'Mobile Number', value: response.resources?.cdmContactNumber, isFullNumber: true },
                { name: 'Email', value: response.resources?.cdmEmail },
              ]
            },
            {
              name: 'RCDM INFORMATION', columns: [
                { name: 'Name', value: response.resources?.rcdmName },
                { name: 'Surname', value: response.resources?.rcdmLastName },
                { name: 'Employee ID', value: response.resources?.rcdmEmployeeCode },
                { name: 'Mobile Number', value: response.resources?.rcdmContactNumber, isFullNumber: true },
                { name: 'Email', value: response.resources?.rcdmEmail },
              ]
            },
          ]
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]['LSS Schemes']
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['lss/lss-registration/stage-one'], { queryParams: { id: this.lssId } });
        break;
    }
  }

  goBack() {
    this.router.navigate(['/lss']);
  }
}
