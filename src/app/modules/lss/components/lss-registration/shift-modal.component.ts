import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import {
  convertCustomHoursAndMinutesToDateFormat,
  convertTwentyFourHoursToTwelevHours,
  CrudService, IApplicationResponse, ModulesBasedApiSuffix,
  RxjsService, setRequiredValidator
} from '@app/shared';
import { LssShiftModel } from '@modules/lss/models';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
@Component({
  selector: 'app-lead-notes-modal-component',
  templateUrl: './shift-modal.component.html'
})
export class ShiftModalComponent implements OnInit {
  shiftForm: FormGroup;

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public shiftModel,
    private rxjsService: RxjsService,
    private dialog: MatDialog, private crudService: CrudService) {
    shiftModel['startShiftTime'] = convertCustomHoursAndMinutesToDateFormat(shiftModel['startShiftTime']);
    shiftModel['endShiftTime'] = convertCustomHoursAndMinutesToDateFormat(shiftModel['endShiftTime']);
    this.createShiftForm(shiftModel);
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
  }

  createShiftForm(shiftModel: LssShiftModel): void {
    let lssShiftModel = new LssShiftModel(shiftModel);
    this.shiftForm = this.formBuilder.group({});
    Object.keys(lssShiftModel).forEach((key) => {
      this.shiftForm.addControl(key, new FormControl(lssShiftModel[key]));
    });
    this.shiftForm = setRequiredValidator(this.shiftForm, ["noOfGuards", "lssGradeId", "startShiftTime", "endShiftTime"]);
  }

  onSubmit(): void {
    if (this.shiftForm.invalid) {
      return;
    }
    this.shiftForm.value.shiftDayType = this.shiftModel.shiftDayType;
    this.shiftForm.value.index = this.shiftModel.index;
    this.shiftForm.value.lssGradeName = this.shiftModel.grades.find(g => g['id'] == this.shiftForm.value.lssGradeId).displayName;
    this.shiftForm.value.shiftTypeName = this.shiftForm.value.isDayShift ? 'Day' : 'Night';
    if (!this.shiftForm.value.lssShiftId) {
      this.rxjsService.setAnyPropertyValue(this.shiftForm.value);
      this.dialog.closeAll();
      return;
    }
    this.shiftForm.value.startShiftTime = convertTwentyFourHoursToTwelevHours(this.shiftForm.value.startShiftTime);
    this.shiftForm.value.endShiftTime = convertTwentyFourHoursToTwelevHours(this.shiftForm.value.endShiftTime);
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LSS_SHIFTS, this.shiftForm.value, 1).
      subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.rxjsService.setAnyPropertyValue(this.shiftForm.value);
          this.dialog.closeAll();
        }
      })
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
