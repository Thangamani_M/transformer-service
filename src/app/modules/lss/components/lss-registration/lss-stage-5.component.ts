
import { Component, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, ReusablePrimeNGTableFeatureService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, removeFormControlError, standardHeightForPscrollContainer } from '@app/shared/utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { LssInstallation, LssInstallationPricingModel } from '@modules/lss/models/lss-stage-model';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-lss-registartion-stage-five',
  templateUrl: './lss-stage-5.component.html',
})

export class LssRegistartionStageFiveComponent {
  lssId = "";
  lssPricingForm: FormGroup;
  lssInstallationPricing: FormArray;
  @ViewChildren('input') rows: QueryList<any>;
  formConfigs = formConfigs;
  serviceCategoryId = "";
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  servicesBasedOnServiceCategoryId = {};
  stockCodes = [];
  items;
  standardHeightForPscrollContainer = standardHeightForPscrollContainer;
  breadCrumb: BreadCrumbModel;
  loggedInUserData: LoggedInUserModel;

  constructor(
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private router: Router, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private store: Store<AppState>) {
    this.breadCrumb = {
      pageTitle: { key: "Stage 5 - LSS Registration" },
      items: [{ key: "LSS", routeUrl: `/lss` },
      { key: "Stage 5 - LSS Registration" }]
    };
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createLssForm();
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]).pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        if (response[1]?.id) {
          this.lssId = response[1].id;
          this.getLssItemById().subscribe((response: IApplicationResponse) => {
            this.items = response.resources.lssInstallationPricing;
            this.rxjsService.setGlobalLoaderProperty(false);
            this.lssPricingForm.patchValue(response.resources);
            if (!this.lssPricingForm.get('isItemDiscountPercentage').value) {
              this.lssPricingForm.get('isItemDiscountPercentage').setValue(false)
            }
            if (!this.lssPricingForm.get('isSpecialPackageForKitInstallation').value) {
              this.lssPricingForm.get('isSpecialPackageForKitInstallation').setValue(false)
            }
            if (!this.lssPricingForm.get('isSpecificItemDiscount').value) {
              this.lssPricingForm.get('isSpecificItemDiscount').setValue(false)
            }
            this.lssInstallationPricing = this.getServiceListItemsArray;
            if (response.resources.lssInstallationPricing.length > 0) {
              response.resources.lssInstallationPricing.forEach((serviceListModel) => {
                this.lssInstallationPricing.push(this.createItemList(serviceListModel));
              });
            } else {
              this.lssInstallationPricing.push(this.createItemList());
            }
          }
          )
        };
      });
  }

  onDiscountPercentageChanges() {
    this.lssPricingForm.get('itemDiscountPercentage').valueChanges.subscribe((levyPercentage: string) => {
      if (levyPercentage == '0' || levyPercentage == '00' || levyPercentage == '0.0' ||
        levyPercentage == '0.00' || levyPercentage == '00.0' || levyPercentage == '00.00' ||
        levyPercentage == '00.' || levyPercentage == '0.' || levyPercentage == '100') {
        this.lssPricingForm.get('itemDiscountPercentage').setErrors({ 'invalid': true });
      } else {
        this.lssPricingForm = removeFormControlError(this.lssPricingForm as FormGroup, 'invalid');
      }
    });
  }

  getServiceDetailsById(id): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_SERVICE,
      id,
      false,
      null);
  }

  onPriceChanges(i) {
    this.getServiceListItemsArray.controls[i].get('sellingPrice').valueChanges.subscribe((levyPercentage: string) => {
      if (levyPercentage == '0' || levyPercentage == '00' || levyPercentage == '0.0' ||
        levyPercentage == '0.00' || levyPercentage == '00.0' || levyPercentage == '00.00' ||
        levyPercentage == '00.' || levyPercentage == '0.') {
        this.getServiceListItemsArray.controls[i].get('sellingPrice').setErrors({ 'invalid': true });
      }
      else {
        this.getServiceListItemsArray.controls[i] = removeFormControlError(this.getServiceListItemsArray.controls[i] as FormGroup, 'invalid');
      }
    });
  }

  onFormControlChanges(): void {
    this.lssPricingForm.get('isSpecificItemDiscount').valueChanges.subscribe((serv: boolean) => {
      if (!serv) {
        this.items = null;
        if (this.lssInstallationPricing) {
          this.lssInstallationPricing.clear();
        }
      } else {
        if (this.lssInstallationPricing && this.lssInstallationPricing.value.length > 0) {
        } else {
          if (this.items && this.items.length > 0) {
          } else {
            this.lssInstallationPricing = this.getServiceListItemsArray;
            let servicesModel = new LssInstallationPricingModel();
            this.lssInstallationPricing.push(this.createItemList(servicesModel));
          }
        }
      }
    });
    this.lssPricingForm.get('isItemDiscountPercentage').valueChanges.subscribe((serv: boolean) => {
      this.lssPricingForm.get('itemDiscountPercentage').setValue(null);
      this.lssPricingForm.get('itemDiscountAmount').setErrors({ 'invalid': false });
      this.lssPricingForm.get('itemDiscountAmount').markAsUntouched();
      this.lssPricingForm.get('itemDiscountPercentage').setErrors({ 'invalid': false });
      this.lssPricingForm.get('itemDiscountPercentage').markAsUntouched();
      this.lssPricingForm.get("itemDiscountPercentage").clearValidators();
      this.lssPricingForm.get("itemDiscountPercentage").updateValueAndValidity();
      this.lssPricingForm.get("itemDiscountAmount").clearValidators();
      this.lssPricingForm.get("itemDiscountAmount").updateValueAndValidity();
      this.lssPricingForm = removeFormControlError(this.lssPricingForm, 'invalid');
    });
    this.lssPricingForm.get('isSpecialPackageForKitInstallation').valueChanges.subscribe((serv: boolean) => {
      if (!serv) {
        this.lssPricingForm.get("itemDiscountAmount").setValue("");
        this.lssPricingForm.get("itemDiscountAmount").disable();
        this.lssPricingForm = removeFormControlError(this.lssPricingForm, 'invalid');
      } else {
        this.lssPricingForm.get("itemDiscountAmount").enable();
      }
      if (!serv) {
        this.lssPricingForm.get('isItemDiscountPercentage').setValue(false);
        this.lssPricingForm.get("itemDiscountPercentage").disable();
        this.lssPricingForm.get('isItemDiscountPercentage').disable();
        this.lssPricingForm = removeFormControlError(this.lssPricingForm, 'invalid');
      } else {
        this.lssPricingForm.get("itemDiscountPercentage").enable();
        this.lssPricingForm.get('isItemDiscountPercentage').enable();
      }
    });
  }

  selectedOptions: any = [];
  getSelectedDivision(e, i) {
    if (this.stockCodes.length > 0) {
      let found = this.stockCodes.find(index => index.id == e.id);
      if (found != undefined) {
        this.getServiceListItemsArray.controls[i].get("stockName").setValue(found.displayName);
        this.getServiceListItemsArray.controls[i].get("itemId").setValue(found.id);
      }
    }
  }

  onStockCodeInputChange(val: any, i) {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM, undefined, true, prepareGetRequestHttpParams(
      null, null, {
      itemCode: val
    }
    )).subscribe(resp => {
      if (resp && resp.resources) {
        this.stockCodes = resp.resources
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    });
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    });
  }

  submitted: boolean = false;
  addServiceInfoItems(): void {
    if (this.getServiceListItemsArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.lssInstallationPricing = this.getServiceListItemsArray;
    let lssInstallationPricingModel = new LssInstallationPricingModel();
    this.lssInstallationPricing.insert(0, this.createItemList(lssInstallationPricingModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  createItemList(serviceListModel?: LssInstallationPricingModel): FormGroup {
    let lssInstallationPricingModel = new LssInstallationPricingModel(serviceListModel);
    let formControls = {};
    lssInstallationPricingModel.createdUserId = this.loggedInUserData.userId;
    lssInstallationPricingModel.lssId = this.lssId;
    Object.keys(lssInstallationPricingModel).forEach((key) => {
      if (key == 'lssItemPricingId' || key == 'lssId') {
        formControls[key] = [{ value: lssInstallationPricingModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: lssInstallationPricingModel[key], disabled: false }, [Validators.required]]
      }
    });
    return this.formBuilder.group(formControls);
  }

  get getServiceListItemsArray(): FormArray {
    if (!this.lssPricingForm) return;
    return this.lssPricingForm.get("lssInstallationPricing") as FormArray;
  }

  removServiceListItem(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog('Are you sure you want to delete this?', undefined,
      DynamicConfirmByMessageConfirmationType.DANGER).onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getServiceListItemsArray.controls[i].value.lssItemPricingId != '') {
            this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LSS_PRICING, undefined,
              prepareGetRequestHttpParams(null, null, {
                ids: this.getServiceListItemsArray.controls[i].value.lssItemPricingId,
                isDeleted: true,
                modifiedUserId: this.loggedInUserData.userId
              }), 1).subscribe((res) => {
                if (res.isSuccess) {
                  this.getServiceListItemsArray.removeAt(i);
                }
                if (this.getServiceListItemsArray.length === 0) {
                  this.addServiceInfoItems();
                };
              })
          } else {
            this.getServiceListItemsArray.removeAt(i);
          }
        }
      });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  createLssForm() {
    let lssInstallation = new LssInstallation();
    this.lssPricingForm = this.formBuilder.group({
      lssInstallationPricing: this.formBuilder.array([])
    });
    Object.keys(lssInstallation).forEach((key) => {
      this.lssPricingForm.addControl(key, new FormControl(lssInstallation[key]));
    });
    this.lssPricingForm.get('createdUserId').setValue(this.loggedInUserData.userId);
    this.lssPricingForm.get('modifiedUserId').setValue(this.loggedInUserData.userId);
  }

  getLssItemById(): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LSS_PRICING,
      this.lssId,
      false,
      null);
  }

  forkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM, undefined, true, null)]
    ).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.stockCodes = resp.resources;
              break;
          }
        }
      });
    });
  }

  submit() {
    if (this.lssPricingForm.value.isSpecificItemDiscount) {
      if (this.lssPricingForm.value.isItemDiscountPercentage) {
        this.lssPricingForm.get("itemDiscountPercentage").setValidators([Validators.required]);
        this.lssPricingForm.get("itemDiscountPercentage").updateValueAndValidity();
        this.lssPricingForm.get("itemDiscountAmount").clearValidators()
        this.lssPricingForm.get("itemDiscountAmount").updateValueAndValidity();
      } else {
        this.lssPricingForm.get("itemDiscountAmount").setValidators([Validators.required]);
        this.lssPricingForm.get("itemDiscountAmount").updateValueAndValidity();
        this.lssPricingForm.get("itemDiscountPercentage").clearValidators();
        this.lssPricingForm.get("itemDiscountPercentage").updateValueAndValidity();
      }
    } else {
      this.lssPricingForm.get("itemDiscountAmount").clearValidators();
      this.lssPricingForm.get("itemDiscountAmount").updateValueAndValidity();
      this.lssPricingForm.get("itemDiscountPercentage").clearValidators();
      this.lssPricingForm.get("itemDiscountPercentage").updateValueAndValidity();
    }
    if (!this.lssPricingForm.value.isSpecificItemDiscount) {
      if (this.lssInstallationPricing) {
        this.lssInstallationPricing.clear();
      }
    }
    if (this.lssPricingForm.invalid) {
      return;
    }
    let obj = this.lssPricingForm.value
    if (obj.lssInstallationPricing.length == 0) {
      if (obj.lssInstallationPricing) {
        obj.lssInstallationPricing = null;
      }
    }
    if (!obj.isSpecificItemDiscount) {
      obj.lssInstallationPricing = null;
    }
    if (!obj.isSpecialPackageForKitInstallation) {
      obj.isItemDiscountPercentage = false;
      obj.itemDiscountAmount = "";
      obj.itemDiscountPercentage = "";
    }
    if (obj.isItemDiscountPercentage) {
      obj.itemDiscountAmount = "";
    } else {
      obj.itemDiscountPercentage = "";
    }
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LSS_PRICING, obj)
      .subscribe(resp => {
        if (resp.isSuccess) {
          this.submitted = true;
          this.router.navigate(['lss']);
        }
      });
  }

  onSubmit() {
    if (this.submitted) {
      this.router.navigate(['lss']);
    } else {
      this.submit();
    }
  }

  cancel() {
    this.router.navigate(['lss/lss-registration/stage-four'], { queryParams: { id: this.lssId } });
  }

  onArrowClick(type: string) {
    if (type == 'previous') {
      this.router.navigate(['lss/lss-registration/stage-four'], { queryParams: { id: this.lssId } });
    }
  }
}