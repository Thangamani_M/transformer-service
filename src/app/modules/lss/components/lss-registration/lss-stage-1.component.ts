import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { selectStaticEagerLoadingLssTypesState$, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel,
  CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, disableFormControls, formConfigs, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix, prepareAutoCompleteListFormatForMultiSelection, prepareGetRequestHttpParams, removeFormControlError, RxjsService, SERVER_REQUEST_DATE_TIME_TRANSFORM, SERVER_REQUEST_DATE_TRANSFORM, setRequiredValidator, standardHeightForPscrollContainer
} from '@app/shared';
import { StageOneModel } from '@modules/lss';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-lss-registartion-stage-one',
  templateUrl: './lss-stage-1.component.html'
})

export class LssRegistartionStageOneComponent {
  lssStageOneForm: FormGroup;
  lssId = '';
  lssTypes = [];
  regions = [];
  districts = [];
  branches = [];
  resources = [];
  divisions = [];
  selectedCdm = {};
  formConfigs = formConfigs;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  selectedCDMOption;
  selectedRCDMOption;
  loggedInUserData: LoggedInUserModel;
  isFormSubmitted = false;
  getLoopableCDMObjectRequestObservable;
  getLoopableRCDMObjectRequestObservable;
  breadCrumb: BreadCrumbModel;
  standardHeightForPscrollContainer = standardHeightForPscrollContainer;
  todayDate=new Date();

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService, private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private router: Router, private formBuilder: FormBuilder, private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.createStageOneForm();
    this.getForkJoinRequests();
    this.onFormControlChanges();
    this.breadCrumb = {
      pageTitle: { key: "Stage 1 - LSS Registration" },
      items: [{ key: "LSS", routeUrl: `/lss` },
      { key: "Stage 1 - LSS Registration" },
      ],
    };
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingLssTypesState$), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.lssTypes = prepareAutoCompleteListFormatForMultiSelection(response[1]);
      this.lssId = response[2]['id'];
    });
  }

  createStageOneForm() {
    let stageOneModel = new StageOneModel();
    this.lssStageOneForm = this.formBuilder.group({});
    Object.keys(stageOneModel).forEach((key) => {
      if (key == 'startUpCost') {
        this.lssStageOneForm.addControl(key, new FormControl(stageOneModel[key], Validators.min(1)));
      } else {
        this.lssStageOneForm.addControl(key, new FormControl(stageOneModel[key]));
      }
    });
    this.lssStageOneForm = setRequiredValidator(this.lssStageOneForm, ["lssRefNo", "lssName", "lssTypeId", "regionId", "divisionId",
      "districtId", "branchId", "contractNo", "lssResourceId", "billstockRefNo", "startDate", "renewalDate", "goLiveDate",
      "startUpCost", "cdmFullName", "rcdmFullName"]);
  }

  getForkJoinRequests(): void {
    forkJoin([this.getRegions(), this.getResources()]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((respObj: IApplicationResponse, ix: number) => {
        if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
          switch (ix) {
            case 0:
              this.regions = respObj.resources;
              break;
            case 1:
              this.resources = prepareAutoCompleteListFormatForMultiSelection(respObj.resources, false, { displayProperty: 'lssResourceName', valueProperty: 'lssResourceId' });
              break;
          }
          if (ix == response.length - 1 && this.lssId) {
            this.getLssDetailById();
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    });
  }

  getLssDetailById() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LSS_REGISTRATION, this.lssId, false, null, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200) {
          Object.keys(response.resources).forEach((key) => {
            if (key == 'goLiveDate' || key == 'renewalDate' || key == 'startDate') {
              response.resources[key] = new Date(response.resources[key]);
            }
          });
          let type = response.resources.lssTypeId;
          type = type.split(',').map(Number);
          response.resources.lssTypeId = type;
          let resources = response.resources.lssResourceId;
          resources = resources.split(',');
          response.resources.lssResourceId = resources;
          this.selectedCDMOption = {
            cdmEmployeeId: response.resources.cdmEmployeeId,
            cdmName: response.resources.cdmName, cdmLastName: response.resources.cdmLastName,
            cdmEmployeeCode: response.resources.cdmEmployeeCode, cdmEmail: response.resources.cdmEmail,
            cdmFullName: response.resources.cdmFullName
          };
          this.selectedRCDMOption = {
            rcdmEmployeeId: response.resources.rcdmEmployeeId,
            rcdmName: response.resources.rcdmName, rcdmLastName: response.resources.rcdmLastName,
            rcdmEmployeeCode: response.resources.rcdmEmployeeCode, rcdmEmail: response.resources.rcdmEmail,
            rcdmFullName: response.resources.rcdmFullName
          };
          response.resources.rcdmName = response.resources.rcdmName + " " + response.resources.rcdmLastName;
          let lssStageOneModel = new StageOneModel(response.resources);
          delete lssStageOneModel.cdmFullName;
          delete lssStageOneModel.rcdmFullName;
          this.lssStageOneForm.patchValue(lssStageOneModel);
          this.lssStageOneForm.get('cdmFullName').setValue(response.resources.cdmFullName, { emitEvent: false, onlySelf: true });
          this.lssStageOneForm.get('rcdmFullName').setValue(response.resources.rcdmFullName, { emitEvent: false, onlySelf: true });
          if (lssStageOneModel.isSchemeActive) {
            this.lssStageOneForm = disableFormControls(this.lssStageOneForm, ["lssRefNo", "lssName", "regionId", "divisionId", "districtId",
              "billstockRefNo"]);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getRegions(): Observable<IApplicationResponse> {
    return this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null, { CountryId: formConfigs.countryId }));
  }

  getResources(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_RESOURCES, undefined, false);
  }

  onOptionsSelected(e) {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.SALES_API_BRANCH, e.target.value, false, null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.lssStageOneForm.controls['branchCode'].setValue(response.resources?.branchCode);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onFormControlChanges() {
    this.lssStageOneForm.get('startUpCost').valueChanges.subscribe((startUpCost: string) => {
      if (startUpCost == '0' || startUpCost == '00' || startUpCost == '0.0' ||
        startUpCost == '0.00' || startUpCost == '00.0' || startUpCost == '00.00' ||
        startUpCost == '00.' || startUpCost == '0.') {
        this.lssStageOneForm.get('startUpCost').setErrors({ 'invalid': true });
      }
      else {
        this.lssStageOneForm = removeFormControlError(this.lssStageOneForm, 'invalid');
      }
    });
    this.lssStageOneForm.get('regionId').valueChanges.subscribe((regionId: string) => {
      if (!regionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null, { regionId }))
        .subscribe((response: IApplicationResponse) => {
          this.divisions = [];
          if (response && response.resources && response.isSuccess) {
            this.divisions = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    });
    this.lssStageOneForm.get('divisionId').valueChanges.subscribe((divisionId: string) => {
      if (!divisionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null, { divisionId }))
        .subscribe((response: IApplicationResponse) => {
          this.districts = [];
          if (response && response.resources && response.isSuccess) {
            this.districts = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    });
    this.lssStageOneForm.get('districtId').valueChanges.subscribe((districtId: string) => {
      if (!districtId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null, { districtId }))
        .subscribe((response: IApplicationResponse) => {
          this.branches = [];
          if (response && response.resources && response.isSuccess) {
            this.branches = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    });
    this.onCDMAndOrRCDMFullNameFormControlValueChanges();
  }

  onCDMAndOrRCDMFullNameFormControlValueChanges(): void {
    this.lssStageOneForm
      .get("cdmFullName")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((search: IApplicationResponse) => {
          this.getLoopableCDMObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
            UserModuleApiSuffixModels.GET_EMPLOYEE_SEARCH, null, true, prepareGetRequestHttpParams(null, null, {
              search
            }));
        });
    this.lssStageOneForm
      .get("rcdmFullName")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((search: IApplicationResponse) => {
          this.getLoopableRCDMObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
            UserModuleApiSuffixModels.GET_EMPLOYEE_SEARCH, null, true, prepareGetRequestHttpParams(null, null, {
              search
            }));
        });
  }

  onSelectedItemOption(selectedObject, type?: string): void {
    if (!selectedObject) return;
    setTimeout(() => {
      if (type == 'CDM') {
        this.selectedCDMOption = selectedObject;
        this.lssStageOneForm.patchValue({
          cdmName: selectedObject.firstName ? selectedObject.firstName : selectedObject.cdmName, cdmLastName: selectedObject.lastName ? selectedObject.lastName :
            selectedObject.cdmLastName,
          cdmEmployeeCode: selectedObject.employeeNo ? selectedObject.employeeNo : selectedObject.cdmEmployeeCode, cdmEmail: selectedObject.email ?
            selectedObject.email : selectedObject.cdmEmail,
          cdmFullName: selectedObject.fullName ? selectedObject.fullName : selectedObject.cdmFullName,
          cdmEmployeeId: selectedObject.employeeId ? selectedObject.employeeId : selectedObject.cdmEmployeeId
        }, { emitEvent: false, onlySelf: true });
      }
      else {
        this.selectedRCDMOption = selectedObject;
        this.lssStageOneForm.patchValue({
          rcdmName: selectedObject.firstName ? (selectedObject.firstName + selectedObject.lastName) : selectedObject.rcdmName, rcdmLastName: selectedObject.lastName ? selectedObject.lastName :
            selectedObject.rcdmLastName,
          rcdmEmployeeCode: selectedObject.employeeNo ? selectedObject.employeeNo : selectedObject.rcdmEmployeeCode, rcdmEmail: selectedObject.email ?
            selectedObject.email : selectedObject.rcdmEmail,
          rcdmFullName: selectedObject.fullName ? selectedObject.fullName : selectedObject.rcdmFullName,
          rcdmEmployeeId: selectedObject.employeeId ? selectedObject.employeeId : selectedObject.rcdmEmployeeId
        }, { emitEvent: false, onlySelf: true });
      }
    }, 200);
  }

  clearFormControlValues(type: string) {
    if (type == 'CDM') {
      this.lssStageOneForm.patchValue({
        cdmName: '', cdmLastName: '',
        cdmEmployeeCode: '', cdmEmail: '',
        cdmContactNumber: '', cdmEmployeeId: ''
      });
    }
    else {
      this.lssStageOneForm.patchValue({
        rcdmName: '', rcdmLastName: '',
        rcdmEmployeeCode: '', rcdmEmail: '',
        rcdmContactNumber: '', rcdmEmployeeId: ''
      });
    }
  }

  changeRegion() {
    this.lssStageOneForm.get('divisionId').setValue('');
    this.lssStageOneForm.get('districtId').setValue('');
    this.lssStageOneForm.get('branchCode').setValue('');
    this.lssStageOneForm.get('branchId').setValue('');
    this.districts = [];
    this.divisions = [];
    this.branches = [];
  }

  changeDivision() {
    this.lssStageOneForm.get('districtId').setValue('');
    this.lssStageOneForm.get('branchCode').setValue('');
    this.lssStageOneForm.get('branchId').setValue('');
    this.districts = [];
    this.branches = [];
  }

  changeDistrict() {
    this.lssStageOneForm.get('branchCode').setValue('');
    this.lssStageOneForm.get('branchId').setValue('');
    this.branches = [];
  }

  onSubmit() {
    this.isFormSubmitted = true;
    if (this.lssStageOneForm.invalid || !this.lssStageOneForm.get("cdmFullName").value ||
      !this.lssStageOneForm.get("rcdmFullName").value) {
      this.lssStageOneForm.get("cdmFullName").markAllAsTouched();
      this.lssStageOneForm.get("rcdmFullName").markAllAsTouched();
      return;
    }
    this.lssStageOneForm.value.lssTypeId = typeof this.lssStageOneForm.value.lssTypeId === 'string' ? this.lssStageOneForm.value.lssTypeId : this.lssStageOneForm.value.lssTypeId.join(',');
    this.lssStageOneForm.value.lssResourceId = typeof this.lssStageOneForm.value.lssResourceId === 'string' ? this.lssStageOneForm.value.lssResourceId : this.lssStageOneForm.value.lssResourceId.join(',');
    let payload = this.lssStageOneForm.value;
    payload.cdmEmployeeId = this.selectedCDMOption.employeeId ?
      this.selectedCDMOption.employeeId : this.selectedCDMOption.cdmEmployeeId;
    payload.rcdmEmployeeId = this.selectedRCDMOption.employeeId ?
      this.selectedRCDMOption.employeeId : this.selectedRCDMOption.rcdmEmployeeId;
    payload.renewalDate = this.datePipe.transform(payload.renewalDate, SERVER_REQUEST_DATE_TRANSFORM);
    payload.startDate = this.datePipe.transform(payload.startDate, SERVER_REQUEST_DATE_TRANSFORM);
    payload.goLiveDate = this.datePipe.transform(payload.goLiveDate, SERVER_REQUEST_DATE_TIME_TRANSFORM);
    payload.createdUserId = this.loggedInUserData.userId;
    payload.lssId = this.lssId ? this.lssId : null;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LSS_REGISTRATION, payload).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        if (!this.lssStageOneForm.get('lssId').value) {
          this.lssStageOneForm.get('lssId').setValue(response.resources);
        }
        this.lssId = response.resources;
      }
    });
  }

  onArrowClick() {
    if (this.lssId || this.lssStageOneForm.get('lssId').value) {
      this.router.navigate(['lss/lss-registration/stage-two'], { queryParams: { id: this.lssId } });
    }
  }
}