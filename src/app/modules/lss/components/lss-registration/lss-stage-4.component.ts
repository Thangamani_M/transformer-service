import { Component, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, ReusablePrimeNGTableFeatureService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControlError, standardHeightForPscrollContainer } from '@app/shared/utils';
import { LssServiceInfoModel, LssServicePriceInfoModel } from '@modules/lss/models/lss-stage-model';
import { loggedInUserData, selectDynamicEagerLoadingServiceCategoriesState$ } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-lss-registartion-stage-four',
  templateUrl: './lss-stage-4.component.html'
})

export class LssRegistartionStageFourComponent {
  lssId = "";
  formConfigs = formConfigs;
  lssServicePriceForm: FormGroup;
  services: FormArray;
  loggedInUserData: LoggedInUserModel;
  serviceCategories = [];
  @ViewChildren('input') rows: QueryList<any>;
  serviceCategoryId = "";
  disabled: boolean = false;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  servicesBasedOnServiceCategoryId = {};
  breadCrumb: BreadCrumbModel;
  standardHeightForPscrollContainer = standardHeightForPscrollContainer;

  constructor(
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private router: Router, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private store: Store<AppState>) {
    this.breadCrumb = {
      pageTitle: { key: "Stage 4 - LSS Registration" },
      items: [{ key: "LSS", routeUrl: `/lss` },
      { key: "Stage 4 - LSS Registration" }]
    };
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createServicePriceForm();
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectDynamicEagerLoadingServiceCategoriesState$),
      this.activatedRoute.queryParams]).pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.serviceCategories = response[1];
        if (response[2]?.id) {
          this.lssId = response[2].id;
          this.getDetailsByLSSId().subscribe((response: IApplicationResponse) => {
            if (response.resources && response.isSuccess && response.statusCode == 200) {
              this.lssServicePriceForm.patchValue(response.resources);
              if (!this.lssServicePriceForm.get('isServiceOverAllDiscountOnMonthlyFee').value) {
                this.lssServicePriceForm.get('isServiceOverAllDiscountOnMonthlyFee').setValue(false)
              }
              if (!this.lssServicePriceForm.get('isPercentage').value) {
                this.lssServicePriceForm.get('isPercentage').setValue(false)
              }
              if (!this.lssServicePriceForm.get('isSpecialServices').value) {
                this.lssServicePriceForm.get('isSpecialServices').setValue(false)
              }
              if (!this.lssServicePriceForm.get('serviceDiscountAmount').value) {
                this.lssServicePriceForm.get('serviceDiscountAmount').setValue("")
              }
              if (!this.lssServicePriceForm.get('serviceDiscountPercentage').value) {
                this.lssServicePriceForm.get('serviceDiscountPercentage').setValue("")
              }
              this.services = this.getServiceListItemsArray;
              if (response.resources.services.length > 0) {
                response.resources.services.forEach((serviceListModel) => {
                  this.services.push(this.createServiceItemList(serviceListModel));
                });
                this.services.value.forEach(element => {
                  this.getServicesById(element.serviceCategoryId).subscribe((response: IApplicationResponse) => {
                    this.servicesBasedOnServiceCategoryId[element.serviceCategoryId] = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                  }
                  )
                });
              } else {
                this.services.push(this.createServiceItemList());
              }
            }
            this.rxjsService.setGlobalLoaderProperty(false)
            this.onChangeDiscountFee();
          });
        }
      });
  }

  onDiscountPercentageChanges() {
    this.lssServicePriceForm.get('serviceDiscountPercentage').valueChanges.subscribe((levyPercentage: string) => {
      if (levyPercentage == '0' || levyPercentage == '00' || levyPercentage == '0.0' ||
        levyPercentage == '0.00' || levyPercentage == '00.0' || levyPercentage == '00.00' ||
        levyPercentage == '00.' || levyPercentage == '0.' || levyPercentage == '100') {
        this.lssServicePriceForm.get('serviceDiscountPercentage').setErrors({ 'invalid': true });
      } else {
        this.lssServicePriceForm = removeFormControlError(this.lssServicePriceForm as FormGroup, 'invalid');
      }
    });
  }

  getServicesById(id): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_SERVICES,
      undefined,
      false,
      prepareGetRequestHttpParams(null, null, {
        serviceCategoryId: id
      }));
  }

  getServiceDetailsById(id): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_SERVICE,
      id,
      false,
      null);
  }

  onChangeServiceCategory(serviceCategoryId) {
    this.serviceCategoryId = serviceCategoryId;
    this.getServicesById(serviceCategoryId).subscribe((response: IApplicationResponse) => {
      if (response.resources && response.isSuccess && response.statusCode == 200) {
        this.servicesBasedOnServiceCategoryId[this.serviceCategoryId] = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onChangeService(serviceId, i) {
    this.getServiceDetailsById(serviceId).subscribe((response: IApplicationResponse) => {
      if (response.resources && response.isSuccess && response.statusCode == 200) {
        this.getServiceListItemsArray.controls[i].get("serviceNumber").setValue(response.resources?.serviceNumber);
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    });
  }

  onPercentageChanges(i, str) {
    if (str == 'amt') {
      this.getServiceListItemsArray.controls[i].get('discountAmount').valueChanges.subscribe((levyPercentage: string) => {
        if (levyPercentage == '0' || levyPercentage == '00' || levyPercentage == '0.0' ||
          levyPercentage == '0.00' || levyPercentage == '00.0' || levyPercentage == '00.00' ||
          levyPercentage == '00.' || levyPercentage == '0.') {
          this.getServiceListItemsArray.controls[i].get('discountAmount').setErrors({ 'invalid': true });
        }
        else {
          this.getServiceListItemsArray.controls[i] = removeFormControlError(this.getServiceListItemsArray.controls[i] as FormGroup, 'invalid');
        }
      });
    } else {
      this.getServiceListItemsArray.controls[i].get('discountPercentage').valueChanges.subscribe((levyPercentage: string) => {
        if (levyPercentage == '0' || levyPercentage == '00' || levyPercentage == '0.0' ||
          levyPercentage == '0.00' || levyPercentage == '00.0' || levyPercentage == '00.00' ||
          levyPercentage == '00.' || levyPercentage == '0.' || levyPercentage.includes('99.')) {
          this.getServiceListItemsArray.controls[i].get('discountPercentage').setErrors({ 'invalid': true });
        }
        else {
          this.getServiceListItemsArray.controls[i] = removeFormControlError(this.getServiceListItemsArray.controls[i] as FormGroup, 'invalid');
        }
      });
    }
  }

  changePercentageWithoutform() {
    this.lssServicePriceForm.get("serviceDiscountPercentage").clearValidators();
    this.lssServicePriceForm.get("serviceDiscountPercentage").updateValueAndValidity();
    this.lssServicePriceForm.get("serviceDiscountAmount").clearValidators();
    this.lssServicePriceForm.get("serviceDiscountAmount").updateValueAndValidity();
  }

  onChangeDiscountFee() {
    if (this.lssServicePriceForm.get('isServiceOverAllDiscountOnMonthlyFee').value) {
      if (this.services) {
        this.services.clear();
        this.services = null;
      }
    } else {
      if (this.services && this.services.value.length > 0) {
      } else {
        this.services = this.getServiceListItemsArray;
        let servicesModel = new LssServicePriceInfoModel();
        this.services.push(this.createServiceItemList(servicesModel));
        this.getServiceListItemsArray.controls[0].get("discountAmount").setValidators([Validators.required]);
        this.getServiceListItemsArray.controls[0].get("discountAmount").updateValueAndValidity();
      }
    }
  }

  onFormControlChanges(): void {
    this.lssServicePriceForm.get('isServiceOverAllDiscountOnMonthlyFee').valueChanges.subscribe((isServiceOverAllDiscountOnMonthlyFee: boolean) => {
      this.disabled = isServiceOverAllDiscountOnMonthlyFee;
      this.lssServicePriceForm.get("serviceDiscountAmount").clearValidators();
      this.lssServicePriceForm.get("serviceDiscountAmount").updateValueAndValidity();
      this.lssServicePriceForm.get("serviceDiscountPercentage").clearValidators();
      this.lssServicePriceForm.get("serviceDiscountPercentage").updateValueAndValidity();
      if (!this.disabled) {
        this.lssServicePriceForm.get('serviceDiscountAmount').setValue(null);
        this.lssServicePriceForm.get('serviceDiscountPercentage').setValue(null);
        this.lssServicePriceForm.get('isPercentage').setValue(false)
        this.lssServicePriceForm.get('isSpecialServices').setValue(true);
      } else {
        this.lssServicePriceForm.get('isSpecialServices').setValue(false);
      }
    });
    this.lssServicePriceForm.get('isPercentage').valueChanges.subscribe((serv: boolean) => {
      this.lssServicePriceForm.get("serviceDiscountAmount").clearValidators();
      this.lssServicePriceForm.get("serviceDiscountAmount").updateValueAndValidity();
      this.lssServicePriceForm.get("serviceDiscountPercentage").clearValidators();
      this.lssServicePriceForm.get("serviceDiscountPercentage").updateValueAndValidity();
    });
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  addServiceInfoItems(): void {
    if (this.getServiceListItemsArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.services = this.getServiceListItemsArray;
    let servicesModel = new LssServicePriceInfoModel();
    this.services.insert(0, this.createServiceItemList(servicesModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.getServiceListItemsArray.controls[0].get("discountAmount").setValidators([Validators.required]);
    this.getServiceListItemsArray.controls[0].get("discountAmount").updateValueAndValidity();
  }

  createServiceItemList(serviceListModel?: LssServicePriceInfoModel): FormGroup {
    let servicesModel = new LssServicePriceInfoModel(serviceListModel);
    let formControls = {};
    servicesModel.lssId = this.lssId;
    servicesModel.createdUserId = this.loggedInUserData.userId;
    Object.keys(servicesModel).forEach((key) => {
      formControls[key] = [{ value: servicesModel[key], disabled: false }, [Validators.required]]
      if (key == 'discountPercentage') {
        formControls[key] = [{ value: servicesModel[key], disabled: false }]
      }
      if (key == 'discountAmount') {
        formControls[key] = [{ value: servicesModel[key], disabled: false }]
      }
      if (key == 'lssServicePricingId') {
        formControls[key] = [{ value: servicesModel[key], disabled: false }]
      }
      if (key == 'serviceName') {
        formControls[key] = [{ value: servicesModel[key], disabled: false }]
      }
      if (key == 'serviceNumber') {
        formControls[key] = [{ value: servicesModel[key], disabled: false }]
      }
      if (key == 'serviceCategoryName') {
        formControls[key] = [{ value: servicesModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  get getServiceListItemsArray(): FormArray {
    if (!this.lssServicePriceForm) return;
    return this.lssServicePriceForm.get("services") as FormArray;
  }

  removServiceListItem(i: number): void {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog('Are you sure you want to delete this?', undefined,
      DynamicConfirmByMessageConfirmationType.DANGER).onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (this.getServiceListItemsArray.controls[i].value.lssServicePricingId) {
            this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LSS_SERVICE_PRICE, undefined,
              prepareRequiredHttpParams({
                modifiedUserId: this.loggedInUserData.userId,
                ids: this.getServiceListItemsArray.controls[i].value.lssServicePricingId,
                isDeleted: true
              })).subscribe((res) => {
                if (res.isSuccess && res.statusCode == 200) {
                  this.getServiceListItemsArray.removeAt(i);
                }
                if (this.getServiceListItemsArray.length === 0) {
                  this.addServiceInfoItems();
                };
              });
          } else {
            this.getServiceListItemsArray.removeAt(i);
          }
        }
      });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  changePercentage(index) {
    if (this.getServiceListItemsArray.controls[index].get("isPercentage").value == false) {
      this.getServiceListItemsArray.controls[index].get("discountAmount").setValidators([Validators.required]);
      this.getServiceListItemsArray.controls[index].get("discountAmount").updateValueAndValidity();
      this.getServiceListItemsArray.controls[index].get("discountPercentage").clearValidators();
      this.getServiceListItemsArray.controls[index].get("discountPercentage").setValue(null);
      this.getServiceListItemsArray.controls[index].get("discountPercentage").updateValueAndValidity();
    } else {
      this.getServiceListItemsArray.controls[index].get("discountPercentage").setValidators([Validators.required]);
      this.getServiceListItemsArray.controls[index].get("discountPercentage").updateValueAndValidity();
      this.getServiceListItemsArray.controls[index].get("discountAmount").clearValidators();
      this.getServiceListItemsArray.controls[index].get("discountAmount").setValue(null);
      this.getServiceListItemsArray.controls[index].get("discountAmount").updateValueAndValidity();
    }
  }

  createServicePriceForm() {
    let lssServiceInfoModel = new LssServiceInfoModel();
    this.lssServicePriceForm = this.formBuilder.group({
      services: this.formBuilder.array([])
    });
    Object.keys(lssServiceInfoModel).forEach((key) => {
      if (key == 'serviceDiscountAmount') {
        this.lssServicePriceForm.addControl(key, new FormControl(lssServiceInfoModel[key]));
      } else if (key == 'serviceDiscountPercentage') {
        this.lssServicePriceForm.addControl(key, new FormControl(lssServiceInfoModel[key]));
      } else {
        this.lssServicePriceForm.addControl(key, new FormControl(lssServiceInfoModel[key]));
      }
    });
    this.lssServicePriceForm.get('modifiedUserId').setValue(this.loggedInUserData.userId);
    this.lssServicePriceForm.get('createdUserId').setValue(this.loggedInUserData.userId);
  }

  getDetailsByLSSId(pageIndex?: string, pageSize?: string, searchKey?: string): Observable<any> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LSS_SERVICE_PRICE,
      undefined,
      false,
      prepareGetRequestHttpParams(null, null, {
        lssId: this.lssId
      }));
  }

  submitted: boolean = false;
  submit() {
    if (this.lssServicePriceForm.value.isServiceOverAllDiscountOnMonthlyFee) {
      if (this.lssServicePriceForm.value.isPercentage) {
        this.lssServicePriceForm.get("serviceDiscountPercentage").setValidators([Validators.required]);
        this.lssServicePriceForm.get("serviceDiscountPercentage").updateValueAndValidity();
        this.lssServicePriceForm.get("serviceDiscountAmount").clearValidators();
        this.lssServicePriceForm.get("serviceDiscountAmount").updateValueAndValidity();
      } else {
        this.lssServicePriceForm.get("serviceDiscountAmount").setValidators([Validators.required]);
        this.lssServicePriceForm.get("serviceDiscountAmount").updateValueAndValidity();
        this.lssServicePriceForm.get("serviceDiscountPercentage").clearValidators();
        this.lssServicePriceForm.get("serviceDiscountPercentage").updateValueAndValidity();
      }
    } else {
      this.lssServicePriceForm.get("serviceDiscountPercentage").clearValidators();
      this.lssServicePriceForm.get("serviceDiscountPercentage").updateValueAndValidity();
      this.lssServicePriceForm.get("serviceDiscountAmount").clearValidators();
      this.lssServicePriceForm.get("serviceDiscountAmount").updateValueAndValidity();
    }
    if (!this.lssServicePriceForm.value.isSpecialServices) {
      if (this.services) {
        this.services.clear();
      }
    }
    if (this.lssServicePriceForm.invalid) {
      this.lssServicePriceForm.markAllAsTouched();
      return;
    }
    this.lssServicePriceForm.get('createdUserId').setValue(this.loggedInUserData.userId);
    let obj = this.lssServicePriceForm.value;
    if (obj.services.length == 0) {
      obj.services = null;
    }
    if (!obj.isServiceOverAllDiscountOnMonthlyFee) {
      obj.serviceDiscountAmount = null;
      obj.serviceDiscountPercentage = null;
      obj.isPercentage = false;
    } else if (obj.isServiceOverAllDiscountOnMonthlyFee) {
      if (obj.isPercentage) {
        obj.serviceDiscountAmount = null;
      } else {
        obj.serviceDiscountPercentage = null;
      }
    }
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LSS_SERVICE_PRICE, obj)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200) {
          this.submitted = true;
          this.next();
        }
      });
  }

  next() {
    if (this.submitted == false && this.lssServicePriceForm.touched == true) {
      this.submit()
    }
  }

  cancel() {
    this.router.navigate(['lss/lss-registration/stage-three'], { queryParams: { id: this.lssId } });
  }

  redirectForwardBackward(str) {
    if (str == 'backward') {
      this.router.navigate(['lss/lss-registration/stage-three'], { queryParams: { id: this.lssId } });
    } else {
      this.router.navigate(['lss/lss-registration/stage-five'], { queryParams: { id: this.lssId } });
    }
  }

  onArrowClick(type: string) {
    if (type == 'previous') {
      this.router.navigate(['lss/lss-registration/stage-three'], { queryParams: { id: this.lssId } });
    }
    else {
      this.router.navigate(['lss/lss-registration/stage-five'], { queryParams: { id: this.lssId } });
    }
  }
}
