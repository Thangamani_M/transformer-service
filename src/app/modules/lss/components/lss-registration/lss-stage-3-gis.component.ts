import { Component } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import {
  BreadCrumbModel,
  CrudService, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  prepareGetRequestHttpParams, RxjsService,
  setRequiredValidator,
  standardHeightForPscrollContainer
} from "@app/shared";
import { LssBoundaryModel } from "@modules/lss";
import { loggedInUserData } from "@modules/others";
import { SalesModuleApiSuffixModels } from "@modules/sales/shared";
import { Store } from "@ngrx/store";
import "leaflet";
import "leaflet-draw";
import { combineLatest } from "rxjs/internal/observable/combineLatest";
import { take } from "rxjs/operators";

declare let L;
@Component({
  selector: "app-lss-registartion-stage-three-gis",
  templateUrl: "./lss-stage-3-gis.component.html"
})
export class LssRegistartionStageThreeGisComponent {
  lssId = "";
  batchId = "";
  polygonArray = [];
  boundaries = [];
  boundariesCopy = [];
  polyLayers = [];
  gisBoundaryForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  isLSSCreate: boolean = true;
  boundaryRequestDetails;
  breadCrumb:BreadCrumbModel;
  standardHeightForPscrollContainer = standardHeightForPscrollContainer;

  constructor(
    private rxjsService: RxjsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder
  ) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.lssId = response[1].id;
      this.isLSSCreate = (response[1].isLSSCreate === 'true') ? true : false;
      this.batchId = response[1].batchId ? response[1].batchId : "";
    });
  }

  ngOnInit(): void {
    this.createLssBoundaryForm();
    this.getGISDetailById();
    this.breadCrumb = {
      pageTitle: { key: "Stage 3 - LSS Registration(GIS boundary)" },
      items: [{ key: "LSS", routeUrl: `/lss` },{key:'Stage 3 - LSS Registartion'},{ key: "Stage 3 - LSS Registration(GIS boundary)" }],
    };
  }

  createLssBoundaryForm(): void {
    let lssBoundaryModel = new LssBoundaryModel();
    this.gisBoundaryForm = this.formBuilder.group({});
    Object.keys(lssBoundaryModel).forEach((key) => {
      this.gisBoundaryForm.addControl(
        key,
        new FormControl(lssBoundaryModel[key])
      );
    });
    this.gisBoundaryForm = setRequiredValidator(this.gisBoundaryForm, ["goLiveDate"]);
    this.gisBoundaryForm.get("createdUserId").setValue(this.loggedInUserData.userId);
    this.gisBoundaryForm.get("lssId").setValue(this.lssId);
  }

  getGISDetailById() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_GIS_BOUNDARY_DETAILS, undefined, false,
      prepareGetRequestHttpParams(null, null, {
        batchId: this.batchId, lssId: this.lssId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources && response.statusCode == 200) {
          response.resources.goLiveDate = new Date(response.resources.goLiveDate);
          this.gisBoundaryForm.patchValue(response.resources);
          this.boundaries = response.resources.boundaries;
          this.boundariesCopy = response.resources.boundaries;
          this.prepareLeafLetMap(this.boundaries, response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  prepareLeafLetMap(boundaries, resources?) {
    if (boundaries.length > 0) {
      let data = [];
      this.polygonArray = [];
      boundaries.forEach((boundary) => {
        let str = boundary.geometry.coordinates;
        str = str.replace(/^"(.*)"$/, '$1');
        str = str.replace(/[()]/g, '')
        let coordinates = JSON.parse(str);
        coordinates[0].forEach((latLong, index) => {
          let latLongArr = JSON.parse(JSON.stringify(latLong));
          if (Math.sign(latLongArr[0]) == 1) {
            latLong[0] = latLongArr[1];
            latLong[1] = latLongArr[0];
          }
        });
        let obj = { coordinates, properties: { Id: boundary.boundaryId, Name: boundary.boundaryName, BatchId: boundary.batchId } };
        data.push(obj);
      });
      this.polyLayers = [];
      data.forEach((feature) => {
        this.polyLayers.push(L.polygon(feature.coordinates, feature.properties));
      });
      setTimeout(() => {
        this.polygonArray = this.polyLayers;
        this.boundaryRequestDetails = resources;
        this.boundaryRequestDetails['lssId'] = this.lssId;
        this.boundaryRequestDetails['goLiveDate'] = resources.goLiveDate;
      }, 1000);
    } else {
      if (resources) {
        this.boundaryRequestDetails = resources;
        this.boundaryRequestDetails['goLiveDate'] = resources.goLiveDate;
      }
      this.boundaryRequestDetails['lssId'] = this.lssId;
      this.polygonArray = [];
    }
  }

  redirectToStageThree(): void {
    this.router.navigate(["/lss/lss-registration/stage-three"], {
      queryParams: { id: this.lssId },
    });
  }
}
