
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService
} from '@app/shared';
import { loggedInUserData, selectStaticEagerLoadingBoundaryStatusState$ } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-lss-registartion-list',
  templateUrl: './lss-registration-list.component.html'
})

export class LssRegistartionListComponent extends PrimeNgTableVariablesModel {
  constructor(private rxjsService: RxjsService, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private crudService: CrudService, private router: Router,
    private snackbarService: SnackbarService,
    public dialogService: DialogService, private store: Store<AppState>) {
    super();
  }
  primengTableConfigProperties: any = {
    tableCaption: "LSS Registration List",
    breadCrumbItems: [{ displayName: 'LSS', relativeRouterUrl: '' },
    { displayName: 'LSS Registration List', relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'LSS Registration',
          dataKey: 'registrationId',
          enableBreadCrumb: true,
          enableAction: true,
          enableAddActionBtn: true,
          enableStatusActiveAction: true,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'lssRefNo', header: 'Scheme Id', width: '150px' },
          { field: 'lssName', header: 'Scheme Name', width: '150px' },
          { field: 'regionName', header: 'Region', width: '150px' },
          { field: 'districtName', header: 'District', width: '150px' },
          { field: 'startUpCost', header: 'Scheme Cost (Sap Total)', width: '200px', isRandSymbolRequired: true },
          { field: 'noContributors', header: 'No. Of Contributors' , width: '200px'},
          { field: 'contributionAmount', header: 'Contribution Amount', width: '200px', isRandSymbolRequired: true },
          { field: 'startDate', header: 'Start Date', width: '150px', isDate: true },
          { field: 'renewalDate', header: 'Renewal Date', width: '150px', isDate: true },
          { field: 'lssTypeName', header: 'Scheme Type', width: '150px' },
          { field: 'cdm', header: 'CDM', width: '150px' },
          { field: 'dueForRenewal', header: 'Due For Renewal', width: '150px', type: 'dropdown', options: [{ label: 'Yes', value: true }, { label: 'No', value: false }] },
          { field: 'boundaryStatusName', header: 'Boundary Status', width: '150px', type: 'dropdown', options: [] },
          { field: 'isSchemeActive', header: 'Status', type: 'dropdown', shouldCheckDisable: true, options: this.status, width: '150px' }],
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_LSS,
          moduleName: ModulesBasedApiSuffix.SALES,
        }
      ]
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingBoundaryStatusState$),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = response[0];
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns[12].options = response[1] = response[1].map(item => {
        return { label: item.displayName, value: item.id };
      });
      let permission = response[2]["LSS Schemes"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getLSSList();
  }

  getLSSList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_LSS;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      salesModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          response.resources.forEach((lssObj) => {
            lssObj.dueForRenewal = lssObj.dueForRenewal == true ? 'Yes' : 'No';
          });
          this.dataList = response.resources;
          this.totalRecords = response.totalCount;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        if (searchObj?.boundaryStatusName) {
          searchObj.boundaryStatusId = searchObj.boundaryStatusName;
        }
        this.getLSSList(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['lss/lss-registration/stage-one']);
        break;
      case CrudType.VIEW:
        this.router.navigate(['lss/view'], { queryParams: { id: row['lssId'] } });
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
