import { Component, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { BreadCrumbModel, convertTwentyFourHoursToTwelevHours, CrudService, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService, standardHeightForPscrollContainer } from '@app/shared';
import { LssShiftModel } from '@modules/lss';
import { loggedInUserData, selectStaticEagerLoadingLssGradesState$ } from '@modules/others';
import { btnActionTypes, LssShiftCategories, SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { ShiftModalComponent } from './shift-modal.component';
@Component({
  selector: 'app-lss-registartion-stage-two',
  templateUrl: './lss-stage-2.component.html'
})

export class LssRegistartionStageTwoComponent {
  lssId = "";
  lssShiftForm: FormGroup;
  grades = [];
  lssWeekdays = [];
  lssWeekends = [];
  formConfigs = formConfigs;
  lssPublicHolidays = [];
  loggedInUserData: LoggedInUserModel;
  weekdays = 'weekdays';
  weekends = 'weekends';
  publicHolidays = 'publicHolidays';
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  breadCrumb:BreadCrumbModel;
  standardHeightForPscrollContainer=standardHeightForPscrollContainer;
  
  @ViewChildren('weekdays') weekdaysFormControls: QueryList<any>;
  @ViewChildren('weekends') weekendsFormControls: QueryList<any>;
  @ViewChildren('publicHolidays') publicHolidaysFormControls: QueryList<any>;

  constructor(private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private router: Router, private dialog: MatDialog, private formBuilder: FormBuilder, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createLssShiftForm();
    this.onFormControlChanges();
    this.breadCrumb = {
      pageTitle: { key: "Stage 2 - LSS Registration" },
      items: [{ key: "LSS", routeUrl: `/lss` },
      { key: "Stage 2 - LSS Registration" },
      ],
    };
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingLssGradesState$), this.activatedRoute.queryParams]).pipe(take(1))
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.grades = response[1];
        this.lssId = response[2]?.id;
        this.getLssShiftsByLssId();
      });
  }

  createLssShiftForm(): void {
    let lssShiftModel = { weekdays: '', weekends: '', publicHolidays: '' };
    this.lssShiftForm = this.formBuilder.group({});
    Object.keys(lssShiftModel).forEach((key) => {
      this.lssShiftForm.addControl(key, this.createFormGroup());
    });
    this.lssShiftForm.controls[this.weekdays] = setRequiredValidator(this.lssShiftForm.controls[this.weekdays] as FormGroup, ["noOfGuards", "lssGradeId", "startShiftTime", "endShiftTime"]);
    this.lssShiftForm.controls[this.weekends] = setRequiredValidator(this.lssShiftForm.controls[this.weekends] as FormGroup, ["noOfGuards", "lssGradeId", "startShiftTime", "endShiftTime"]);
    this.lssShiftForm.controls[this.publicHolidays] = setRequiredValidator(this.lssShiftForm.controls[this.publicHolidays] as FormGroup, ["noOfGuards", "lssGradeId", "startShiftTime", "endShiftTime"]);
  }

  onFormControlChanges(): void {
    this.weekendsFormGroupControls.get('isSameasWeekDay').valueChanges.subscribe((isSameasWeekDay: boolean) => {
      if (isSameasWeekDay) {
        this.lssWeekends = JSON.parse(JSON.stringify(this.lssWeekdays));
        this.lssWeekends.forEach((lssWeeEnd: LssShiftModel) => {
          lssWeeEnd.lssShiftCategoryId = LssShiftCategories.WEEKENDS;
          lssWeeEnd.lssShiftId = null;
          lssWeeEnd.lssShiftRefNo = null;
        });
      }
      else {
        this.lssWeekends = [];
      }
    });
    this.publicHolidaysFormGroupControls.get('isSameasWeekDay').valueChanges.subscribe((isSameasWeekDay: boolean) => {
      if (isSameasWeekDay) {
        this.lssPublicHolidays = JSON.parse(JSON.stringify(this.lssWeekdays));
        this.lssPublicHolidays.forEach((lssPublicHoliday: LssShiftModel) => {
          lssPublicHoliday.lssShiftCategoryId = LssShiftCategories.PUBLIC_HOLIDAYS;
          lssPublicHoliday.lssShiftId = null;
          lssPublicHoliday.lssShiftRefNo = null;
        });
      }
      else {
        this.lssPublicHolidays = [];
      }
    });
  }

  getLssShiftsByLssId() {
    if (!this.lssId) {
      this.rxjsService.setGlobalLoaderProperty(false);
      return;
    };
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LSS_SHIFTS, undefined, false,
      prepareRequiredHttpParams({
        lssId: this.lssId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          Object.keys(response.resources).forEach((key) => {
            if (!response.resources[key]) return;
            if (key == 'lssWeekdays') {
              this.lssWeekdays = response.resources[key] ? response.resources[key] : [];
            }
            else if (key == 'lssWeekends') {
              this.lssWeekends = response.resources[key] ? response.resources[key] : [];
            }
            else if (key == 'lssPublicHolidays') {
              this.lssPublicHolidays = response.resources[key] ? response.resources[key] : [];
            }
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  focusInAndOutFormArrayFields(type): void {
    let nativeElements = type == this.weekdays ? this.weekdaysFormControls : type == this.weekends ? this.weekendsFormControls :
      this.publicHolidaysFormControls;
    nativeElements.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    });
  }

  createFormGroup(): FormGroup {
    let lssShiftModel = new LssShiftModel();
    let formControls = {};
    Object.keys(lssShiftModel).forEach((key) => {
      formControls[key] = new FormControl(lssShiftModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  get weekdaysFormGroupControls(): FormGroup {
    if (!this.lssShiftForm) return;
    return this.lssShiftForm.get(this.weekdays) as FormGroup;
  }

  get weekendsFormGroupControls(): FormGroup {
    if (!this.lssShiftForm) return;
    return this.lssShiftForm.get(this.weekends) as FormGroup;
  }

  get publicHolidaysFormGroupControls(): FormGroup {
    if (!this.lssShiftForm) return;
    return this.lssShiftForm.get(this.publicHolidays) as FormGroup;
  }

  onArrowClick(type: string) {
    if (type == 'previous') {
      this.router.navigate(['lss/lss-registration/stage-one'], { queryParams: { id: this.lssId } });
    }
    else if (this.lssWeekdays.length > 0 && this.lssWeekdays[0].lssShiftId) {
      this.router.navigate(['lss/lss-registration/stage-three'], { queryParams: { id: this.lssId } });
    }
  }

  openShiftModal(type: string, shiftType: string, shiftObj?, index?: number): void {
    if (type == 'create') {
      let formGroupControls = shiftType == "weekdays" ? this.weekdaysFormGroupControls : shiftType == "weekends" ? this.weekendsFormGroupControls :
        this.publicHolidaysFormGroupControls;
      let lssShiftDays = shiftType == "weekdays" ? this.lssWeekdays : shiftType == "weekends" ? this.lssWeekends :
        this.lssPublicHolidays;
      if (formGroupControls.invalid) {
        this.focusInAndOutFormArrayFields(shiftType);
        return;
      }
      formGroupControls.value.createdUserId = this.loggedInUserData.userId;
      formGroupControls.value.lssId = this.lssId;
      formGroupControls.value.lssGradeName = this.grades.find(g => g['id'] == formGroupControls.value.lssGradeId).displayName;
      formGroupControls.value.shiftTypeName = formGroupControls.value.isDayShift ? 'Day' : 'Night';
      formGroupControls.value.startShiftTime = typeof formGroupControls.value.startShiftTime !== "string" ? convertTwentyFourHoursToTwelevHours(formGroupControls.value.startShiftTime) : formGroupControls.value.startShiftTime;
      formGroupControls.value.endShiftTime = typeof formGroupControls.value.endShiftTime !== "string" ? convertTwentyFourHoursToTwelevHours(formGroupControls.value.endShiftTime) : formGroupControls.value.endShiftTime;
      formGroupControls.value.lssShiftCategoryId = shiftType == "weekdays" ? LssShiftCategories.WEEKDAYS : shiftType == "weekends" ? LssShiftCategories.WEEKENDS : LssShiftCategories.PUBLIC_HOLIDAYS;
      lssShiftDays.unshift(formGroupControls.value);
    }
    else {
      let data = {};
      data = JSON.parse(JSON.stringify(shiftObj));
      data['grades'] = this.grades;
      data['shiftDayType'] = shiftType;
      data['index'] = index;
      data['modifiedUserId'] = this.loggedInUserData.userId;
      const dialogReff = this.dialog.open(ShiftModalComponent, { width: '700px', data, disableClose: true });
      dialogReff.afterClosed().subscribe(result => {
        if (result) {
          return;
        };
        this.rxjsService.getAnyPropertyValue().pipe(take(1)).subscribe((shiftObj) => {
          if (!shiftObj['lssShiftId']) {
            shiftObj['startShiftTime'] = convertTwentyFourHoursToTwelevHours(shiftObj['startShiftTime']);
            shiftObj['endShiftTime'] = convertTwentyFourHoursToTwelevHours(shiftObj['endShiftTime']);
          }
          switch (shiftObj['shiftDayType']) {
            case this.weekdays:
              let foundIndex1 = this.lssWeekdays.findIndex((lw, index) => index == shiftObj['index']);
              delete shiftObj['shiftDayType'];
              delete shiftObj['index'];
              this.lssWeekdays[foundIndex1] = shiftObj;
              break;
            case this.weekends:
              let foundIndex2 = this.lssWeekends.findIndex((lw, index) => index == shiftObj['index']);
              delete shiftObj['shiftDayType'];
              delete shiftObj['index'];
              this.lssWeekends[foundIndex2] = shiftObj;
              break;
            case this.publicHolidays:
              let foundIndex3 = this.lssPublicHolidays.findIndex((lw, index) => index == shiftObj['index']);
              delete shiftObj['shiftDayType'];
              delete shiftObj['index'];
              this.lssPublicHolidays[foundIndex3] = shiftObj;
              break;
          }
        });
      });
    }
  }

  removeItem(shiftTypeName: string, shiftObj: LssShiftModel, index: number) {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog("Are you sure you want to delete this?", undefined, DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (dialogResult) {
          if (shiftObj.lssShiftId) {
            this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LSS_SHIFTS, undefined,
              prepareRequiredHttpParams({
                ids: shiftObj.lssShiftId,
                isDeleted: true,
                modifiedUserId: this.loggedInUserData.userId
              })).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                  this.spliceShiftTypesArray(shiftTypeName, index);
                }
              });
          }
          else {
            this.rxjsService.setFormChangeDetectionProperty(true);
            this.spliceShiftTypesArray(shiftTypeName, index);
          }
        }
      });
  }

  spliceShiftTypesArray(shiftTypeName: string, index: number) {
    switch (shiftTypeName) {
      case this.weekdays:
        this.lssWeekdays.splice(index, 1);
        break;
      case this.weekends:
        this.lssWeekends.splice(index, 1);
        break;
      case this.publicHolidays:
        this.lssPublicHolidays.splice(index, 1);
        break;
    }
  }

  onSubmit(): void {
    if (this.lssWeekdays.length == 0) {
      this.snackbarService.openSnackbar("Atleast One Weekday shift is required..!!", ResponseMessageTypes.WARNING);
      return;
    }
    let finalPayload = [];
    // this logic is added because of unexpected backed data model
    finalPayload = JSON.parse(JSON.stringify([...this.lssWeekdays, ...this.lssWeekends, ...this.lssPublicHolidays]));
    finalPayload.forEach((lssShiftModel: LssShiftModel) => {
      lssShiftModel.createdUserId = this.loggedInUserData.userId;
    });
    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LSS_SHIFTS, finalPayload, 1);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.lssWeekdays = []; this.lssWeekends = []; this.lssPublicHolidays = [];
        this.lssWeekdays = response.resources.lssWeekdays;
        this.lssWeekends = response.resources.lssWeekends;
        this.lssPublicHolidays = response.resources.lssPublicHolidays;
      }
    });
  }

  nextOrPrevStepper(type?: string): void {
    if (type === btnActionTypes.SUBMIT) {
      this.router.navigate(['/lss/lss-registration/stage-three'], { queryParams: { id: this.lssId } });
    }
    else if (type === btnActionTypes.NEXT) {
      this.router.navigate(['/lss/lss-registration/stage-three'], { queryParams: { id: this.lssId } });
    }
    else if (type === btnActionTypes.PREVIOUS) {
      this.router.navigate(['/lss/lss-registration/stage-one'], { queryParams: { id: this.lssId } });
    }
  }
}
