import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ACCEPTABLE_ALL_MAJOR_FILE_EXTENSIONS, BreadCrumbModel, countryCodes, CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, DynamicConfirmByMessageConfirmationType, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControlError, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, setRequiredValidator, SnackbarService, standardHeightForPscrollContainer } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { LssStageThreeModel } from '@modules/lss';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-lss-registartion-stage-three',
  templateUrl: './lss-stage-3.component.html'
})

export class LssRegistartionStageThreeComponent {
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  fileList: File[] = [];
  listOfFiles = [];
  lssBoundaries = [];
  maxFilesUpload = 5;
  lssStageThreeForm: FormGroup;
  lssContactId = "";
  lssId = "";
  formConfigs = formConfigs;
  contributionTypes = [];
  clientTypes = [];
  isRebatePercenatge = true;
  vehicleObject: any = {};
  isMobileDuplicate = false;
  isContactDuplicate = false;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  countryCodes = countryCodes;
  breadCrumb: BreadCrumbModel;
  loggedInUserData: LoggedInUserModel;
  standardHeightForPscrollContainer = standardHeightForPscrollContainer;
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;

  constructor(private rxjsService: RxjsService, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private router: Router, private dialog: MatDialog,
    private crudService: CrudService, private store: Store<AppState>, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private snackbarService: SnackbarService) {
    this.breadCrumb = {
      pageTitle: { key: "Stage 3 - LSS Registration" },
      items: [{ key: "LSS", routeUrl: `/lss` },
      { key: "Stage 3 - LSS Registration" },
      ],
    };
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createStageThreeForm();
    this.onFormControlChanges();
    this.getForkJoinRequests();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.lssId = response[1]?.id;
      });
  }

  onSelectedItemOption(selectedItemOption) {
    if (!selectedItemOption) return;
    this.vehicleObject = selectedItemOption;
  }

  clearFormControlValues() {
    this.vehicleObject = {};
    this.lssStageThreeForm.get('vehicleCallSign').setValue("");
  }

  getForkJoinRequests(): void {
    forkJoin([this.getGISList(), this.getContributionTypes(), this.getClientTypes()]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((respObj: IApplicationResponse, ix: number) => {
        if (respObj.isSuccess) {
          switch (ix) {
            case 0:
              this.lssBoundaries = respObj.resources;
              break;
            case 1:
              this.contributionTypes = respObj.resources;
              break;
            case 2:
              this.clientTypes = respObj.resources;
              break;
          }
          if (ix === response.length - 1 && this.lssId) {
            this.crudService.get(ModulesBasedApiSuffix.SALES,
              SalesModuleApiSuffixModels.SALES_API_LSS_CONTACTS, this.lssId, false, null).subscribe((response: IApplicationResponse) => {
                if (response.resources && response.isSuccess && response.statusCode == 200) {
                  response.resources.vehicleRegistrationId = response.resources.vehicleRegistrationNumber;
                  this.lssStageThreeForm.patchValue(response.resources, { emitEvent: false, onlySelf: true });
                  this.vehicleObject = {};
                  this.vehicleObject['vehicleMake'] = response.resources.vehicleMake;
                  this.vehicleObject['vehicleModel'] = response.resources.vehicleModel;
                  this.vehicleObject['vehicleRegistrationNumber'] = response.resources.vehicleRegistrationNumber;
                  if (response.resources.isRebate && !response.resources.isRebatePercentage) {
                    this.lssStageThreeForm = setRequiredValidator(this.lssStageThreeForm, ["rebateAmount"]);
                  }
                  this.isRebatePercenatge = response.resources.rebatePercentage ? true : false;
                  response.resources.documents && response.resources.documents.forEach(element => {
                    element['name'] = element.documentName;
                    element['documentId'] = element.documentId;
                    element['documentPath'] = element.documentPath;
                    this.fileList.push(element);
                    this.listOfFiles.push(element.name);
                  });
                }
                this.rxjsService.setGlobalLoaderProperty(false);
              });
          }
          else {
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        }
      });
    });
  }

  getGISList(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_GIS_BOUNDARY, undefined, false, prepareGetRequestHttpParams(null, null, {
        lssId: this.lssId
      }));
  }

  getContributionTypes(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_CONTRIBUTION_TYPE, undefined, false, null);
  }

  getClientTypes(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_LSS_CLIENT_TYPE, undefined, false, null);
  }

  onFormControlChanges(): void {
    this.onVehicleRegistrationNumberChanges();
    this.lssStageThreeForm.get('isRebate').valueChanges.subscribe((regionId: string) => {
      let lssStageThreeFormValue = this.lssStageThreeForm.getRawValue();
      if (!lssStageThreeFormValue.isRebate) {
        this.lssStageThreeForm.get("rebatePercentage").clearValidators();
        this.lssStageThreeForm.get("rebatePercentage").setValue('');
        this.lssStageThreeForm.get("rebatePercentage").updateValueAndValidity();
        this.lssStageThreeForm.get("rebateAmount").clearValidators();
        this.lssStageThreeForm.get("rebateAmount").setValue('');
        this.lssStageThreeForm.get("rebateAmount").updateValueAndValidity();
      } else {
        this.isRebatePercenatge = true;
        this.lssStageThreeForm = setRequiredValidator(this.lssStageThreeForm,
          ["rebatePercentage"]);
      }
    });
  }

  createStageThreeForm(): void {
    let stageThreeModel = new LssStageThreeModel();
    this.lssStageThreeForm = this.formBuilder.group({});
    Object.keys(stageThreeModel).forEach((key) => {
      this.lssStageThreeForm.addControl(key, new FormControl(stageThreeModel[key]));
    });
    this.lssStageThreeForm = setRequiredValidator(this.lssStageThreeForm, ["contactPerson", "mobileNo", "contactNo", "email", "lssContributionTypeId", "lssClientTypeId", "contributionAmount"]);
    this.lssStageThreeForm.get('createdUserId').setValue(this.loggedInUserData.userId);
    this.lssStageThreeForm.get('mobileNoCountryCode').setValue('+27');
    this.lssStageThreeForm.get('contactNoCountryCode').setValue('+27');
    this.lssStageThreeForm.get('lssId').setValue(this.lssId);
  }

  onVehicleRegistrationNumberChanges() {
    this.lssStageThreeForm.get("vehicleRegistrationId").valueChanges.pipe(
      debounceTime(debounceTimeForSearchkeyword),
      distinctUntilChanged()).subscribe((vehicleRegistrationNumber: IApplicationResponse) => {
        if (typeof vehicleRegistrationNumber == 'string' && vehicleRegistrationNumber == '') {
          this.clearFormControlValues();
          this.lssStageThreeForm.get('vehicleCallSign').clearValidators();
          this.lssStageThreeForm.get("vehicleCallSign").updateValueAndValidity();
        }
        this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          EventMgntModuleApiSuffixModels.SALES_API_VEHICLE_SEARCH, null, true, prepareGetRequestHttpParams(null, null, {
            vehicleRegistrationNumber
          }));
      });
  }

  onPercentageChanges(i?) {
    this.lssStageThreeForm.get('rebatePercentage').valueChanges.subscribe((rebatePercentage: string) => {
      if (rebatePercentage == '0' || rebatePercentage == '00' || rebatePercentage == '0.0' ||
        rebatePercentage == '0.00' || rebatePercentage == '00.0' || rebatePercentage == '00.00' ||
        rebatePercentage == '00.' || rebatePercentage == '0.' || rebatePercentage.includes('99.')) {
        this.lssStageThreeForm.get('rebatePercentage').setErrors({ 'invalid': true });
      }
      else {
        this.lssStageThreeForm = removeFormControlError(this.lssStageThreeForm, 'invalid');
      }
    });
  }

  radioChange(e) {
    if (e.value) {
      this.isRebatePercenatge = true;
      this.lssStageThreeForm.get('isRebatePercentage').setValue(true);
      this.lssStageThreeForm.get('rebateAmount').setValue('');
      this.lssStageThreeForm.get('rebateAmount').clearValidators();
      this.lssStageThreeForm.get('rebateAmount').updateValueAndValidity();
      this.lssStageThreeForm = setRequiredValidator(this.lssStageThreeForm,
        ["rebatePercentage"]);
    } else {
      this.isRebatePercenatge = false;
      this.lssStageThreeForm.get('isRebatePercentage').setValue(false);
      this.lssStageThreeForm.get('rebatePercentage').setValue('');
      this.lssStageThreeForm.get('rebatePercentage').clearValidators();
      this.lssStageThreeForm.get('rebatePercentage').updateValueAndValidity();
      this.lssStageThreeForm = setRequiredValidator(this.lssStageThreeForm,
        ["rebateAmount"]);
    }
  }

  changeContactNumber(e) {
    let regex = /(\d)\s+(?=\d)/g;
    let subst = `$1`;
    let value = e.target.value.replace(regex, subst);
    const a = this.lssStageThreeForm.value.mobileNo && this.lssStageThreeForm.value.mobileNo.toString()
    let mobileNo = this.lssStageThreeForm.value.mobileNo && a.replace(regex, subst)
    if (value === mobileNo) {
      this.isContactDuplicate = true;
    } else {
      this.isContactDuplicate = false;
      this.isMobileDuplicate = false
    }
  }

  changeMobileNumber(e) {
    let regex = /(\d)\s+(?=\d)/g;
    let subst = `$1`;
    let value = e.target.value.replace(regex, subst);
    const a = this.lssStageThreeForm.value.contactNo && this.lssStageThreeForm.value.contactNo.toString()
    let contactNo = this.lssStageThreeForm.value.contactNo && a.replace(regex, subst)
    if (value === contactNo) {
      this.isMobileDuplicate = true;
    } else {
      this.isMobileDuplicate = false;
      this.isContactDuplicate = false
    }
  }

  onArrowClick(type: string) {
    if (type == 'previous') {
      this.router.navigate(['lss/lss-registration/stage-two'], { queryParams: { id: this.lssId } });
    }
    else if (this.lssStageThreeForm.value.lssContactId) {
      this.router.navigate(['lss/lss-registration/stage-four'], { queryParams: { id: this.lssId } });
    }
  }

  openGisModal(): void {
    this.router.navigate(['lss/lss-registration/stage-three/gis'], { queryParams: { id: this.lssId, isLSSCreate: true } });
  }

  regirectToGis(obj): void {
    this.router.navigate(['lss/lss-registration/stage-three/gis'], { queryParams: { batchId: obj.batchId, id: this.lssId, isLSSCreate: false } });
  }

  removeSelectedFile(selectedFile, index) {
    if (selectedFile.documentId) {
      this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this file?`, undefined,
        DynamicConfirmByMessageConfirmationType.DANGER).
        onClose?.subscribe(dialogResult => {
          if (dialogResult) {
            this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.LSS_CONTACT_DOCUMENTS,
              undefined, prepareRequiredHttpParams({
                Ids: selectedFile.documentId,
                IsDeleted: true,
                ModifiedUserId: this.loggedInUserData.userId,
              })).subscribe((response: IApplicationResponse) => {
                if (response.statusCode == 200 && response.isSuccess) {
                  this.listOfFiles.splice(index, 1);
                  this.fileList.splice(index, 1);
                  this.rxjsService.setFormChangeDetectionProperty(true);
                }
              });
          }
        });
    } else {
      this.listOfFiles.splice(index, 1);
      this.fileList.splice(index, 1);
    }
  }

  uploadFiles(file) {
    this.onFormControlChanges();
    if (file && file.length == 0)
      return;
    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }
    for (let i = 0; i < file.length; i++) {
      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (ACCEPTABLE_ALL_MAJOR_FILE_EXTENSIONS.includes("." + extension)) {
        let filename = this.fileList.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push(selectedFile.name);
          this.myFileInputField.nativeElement.value = null;
        }
      } else {
        this.snackbarService.openSnackbar(`Allow to upload the following file formats only - ${ACCEPTABLE_ALL_MAJOR_FILE_EXTENSIONS}`, ResponseMessageTypes.WARNING);
      }
    }
  }

  onSubmit(): void {
    if (this.lssStageThreeForm.value.rebatePercentage) {
      this.lssStageThreeForm.get("rebateAmount").clearValidators();
      this.lssStageThreeForm.get("rebateAmount").setValidators(null);
      this.lssStageThreeForm.get("rebateAmount").updateValueAndValidity();
    }
    if (this.lssStageThreeForm.value.rebateAmount) {
      this.lssStageThreeForm.get("rebatePercentage").clearValidators();
      this.lssStageThreeForm.get("rebatePercentage").setValidators(null);
      this.lssStageThreeForm.get("rebatePercentage").updateValueAndValidity();
    }
    if (!this.lssStageThreeForm.value.isFixedContribution) {
      this.lssStageThreeForm.get("contributionAmount").clearValidators();
      this.lssStageThreeForm.get("contributionAmount").setValidators(null);
      this.lssStageThreeForm.get("contributionAmount").updateValueAndValidity();
    } else {
      this.lssStageThreeForm = setRequiredValidator(this.lssStageThreeForm, ["contributionAmount"]);
    }
    if (this.vehicleObject?.hasOwnProperty('vehicleModel') && this.vehicleObject.vehicleModel) {
      this.lssStageThreeForm = setRequiredValidator(this.lssStageThreeForm, ["vehicleCallSign"]);
    }
    else {
      this.lssStageThreeForm.get('vehicleCallSign').clearValidators();
      this.lssStageThreeForm.get("vehicleCallSign").updateValueAndValidity();
    }
    if (this.lssStageThreeForm.invalid) {
      return;
    }
    let payload = this.lssStageThreeForm.value;
    payload.vehicleRegistrationId = this.vehicleObject?.['vehicleRegistrationId'] ? this.vehicleObject['vehicleRegistrationId'] : null;
    payload.mobileNo = payload.mobileNo.toString().replace(/[a-z]/gi, '').replace(/ /g, '');
    payload.contactNo = payload.contactNo.toString().replace(/[a-z]/gi, '').replace(/ /g, '');
    let formData = new FormData();
    formData.append('data', JSON.stringify(payload));
    this.fileList.forEach(file => {
      formData.append('file', file);
    });
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_LSS_CONTACTS, formData).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        if (!this.vehicleObject?.['vehicleRegistrationId']) {
          this.lssStageThreeForm.get('vehicleRegistrationId').setValue("", { emitEvent: false, onlySelf: true });
        }
        if (!this.lssStageThreeForm.value.lssContactId) {
          this.lssStageThreeForm.get('lssContactId').setValue(response.resources);
        }
      }
    });
  }
}
