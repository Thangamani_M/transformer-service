import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  LssRegistartionListComponent, LssRegistartionViewComponent, LssRegistartionStageOneComponent, LssRegistartionStageTwoComponent,
  LssRegistartionStageThreeComponent, LssRegistartionStageThreeGisComponent, LssRegistartionStageFourComponent,
  LssRegistartionStageFiveComponent
} from '@app/modules/lss';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: '', component: LssRegistartionListComponent, canActivate: [AuthGuard], data: { title: 'LSS Registration List' }
  },
  {
    path: 'view', component: LssRegistartionViewComponent, canActivate: [AuthGuard], data: { title: 'LSS View' }
  },
  {
    path: 'lss-registration', children: [
      { path: 'stage-one', component: LssRegistartionStageOneComponent, canActivate: [AuthGuard], data: { title: 'LSS Stage 1' } },
      { path: 'stage-two', component: LssRegistartionStageTwoComponent, canActivate: [AuthGuard], data: { title: 'LSS Stage 2' } },
      { path: 'stage-three', component: LssRegistartionStageThreeComponent, canActivate: [AuthGuard], data: { title: 'LSS Stage 3' } },
      { path: 'stage-three/gis', component: LssRegistartionStageThreeGisComponent, canActivate: [AuthGuard], data: { title: 'LSS Stage 3 GIS' } },
      { path: 'stage-four', component: LssRegistartionStageFourComponent, canActivate: [AuthGuard], data: { title: 'LSS Stage 4' } },
      { path: 'stage-five', component: LssRegistartionStageFiveComponent, canActivate: [AuthGuard], data: { title: 'LSS Stage 5' } }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class LssRoutingModule { }
