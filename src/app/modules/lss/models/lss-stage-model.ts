class StageOneModel {
    constructor(stageOneModel?: StageOneModel) {
        this.lssRefNo = stageOneModel ? stageOneModel.lssRefNo == undefined ? '' : stageOneModel.lssRefNo : '';
        this.lssName = stageOneModel ? stageOneModel.lssName == undefined ? '' : stageOneModel.lssName : '';
        this.lssTypeId = stageOneModel ? stageOneModel.lssTypeId == undefined ? '' : stageOneModel.lssTypeId : '';
        this.regionId = stageOneModel ? stageOneModel.regionId == undefined ? '' : stageOneModel.regionId : '';
        this.divisionId = stageOneModel ? stageOneModel.divisionId == undefined ? '' : stageOneModel.divisionId : '';
        this.districtId = stageOneModel ? stageOneModel.districtId == undefined ? '' : stageOneModel.districtId : '';
        this.branchCode = stageOneModel ? stageOneModel.branchCode == undefined ? '' : stageOneModel.branchCode : '';
        this.cdmName = stageOneModel ? stageOneModel.cdmName == undefined ? '' : stageOneModel.cdmName : '';
        this.cdmLastName = stageOneModel ? stageOneModel.cdmLastName == undefined ? '' : stageOneModel.cdmLastName : '';
        this.employeeId = stageOneModel ? stageOneModel.employeeId == undefined ? '' : stageOneModel.employeeId : '';
        this.cdmContactNumber = stageOneModel ? stageOneModel.cdmContactNumber == undefined ? '' : stageOneModel.cdmContactNumber : '';
        this.rcdmContactNumber = stageOneModel ? stageOneModel.rcdmContactNumber == undefined ? '' : stageOneModel.rcdmContactNumber : '';
        this.cdmEmail = stageOneModel ? stageOneModel.cdmEmail == undefined ? '' : stageOneModel.cdmEmail : '';
        this.cdmEmployeeCode = stageOneModel ? stageOneModel.cdmEmployeeCode == undefined ? '' : stageOneModel.cdmEmployeeCode : '';
        this.rcdmName = stageOneModel ? stageOneModel.rcdmName == undefined ? '' : stageOneModel.rcdmName : '';
        this.branchId = stageOneModel ? stageOneModel.branchId == undefined ? '' : stageOneModel.branchId : '';
        this.lssId = stageOneModel ? stageOneModel.lssId == undefined ? '' : stageOneModel.lssId : '';
        this.lssResourceId = stageOneModel ? stageOneModel.lssResourceId == undefined ? '' : stageOneModel.lssResourceId : '';
        this.contractNo = stageOneModel ? stageOneModel.contractNo == undefined ? '' : stageOneModel.contractNo : '';
        this.billstockRefNo = stageOneModel ? stageOneModel.billstockRefNo == undefined ? '' : stageOneModel.billstockRefNo : '';
        this.startDate = stageOneModel ? stageOneModel.startDate == undefined ? null : stageOneModel.startDate : null;
        this.renewalDate = stageOneModel ? stageOneModel.renewalDate == undefined ? null : stageOneModel.renewalDate : null;
        this.goLiveDate = stageOneModel ? stageOneModel.goLiveDate == undefined ? null : stageOneModel.goLiveDate : null;
        this.startUpCost = stageOneModel ? stageOneModel.startUpCost == undefined ? null : stageOneModel.startUpCost : null;
        this.cdmEmployeeId = stageOneModel ? stageOneModel.cdmEmployeeId == undefined ? '' : stageOneModel.cdmEmployeeId : '';
        this.rcdmEmployeeId = stageOneModel ? stageOneModel.rcdmEmployeeId == undefined ? null : stageOneModel.rcdmEmployeeId : null;
        this.createdUserId = stageOneModel ? stageOneModel.createdUserId == undefined ? null : stageOneModel.createdUserId : null;
        this.isSchemeActive = stageOneModel ? stageOneModel.isSchemeActive == undefined ? null : stageOneModel.isSchemeActive : null;
        this.rcdmFullName = stageOneModel ? stageOneModel.rcdmFullName == undefined ? null : stageOneModel.rcdmFullName : null;
        this.cdmFullName = stageOneModel ? stageOneModel.cdmFullName == undefined ? null : stageOneModel.cdmFullName : null;
    }
    lssRefNo?: string;
    lssName?: string;
    lssTypeId?: string;
    branchId?: string;
    lssResourceId?: string;
    contractNo?: string;
    billstockRefNo?: string;
    startDate?: string;
    renewalDate?: string;
    goLiveDate?: string;
    startUpCost?: number;
    cdmEmployeeId?: string;
    rcdmEmployeeId?: string;
    createdUserId?: string;
    divisionId?: string;
    regionId?: string;
    districtId?: string;
    branchCode?: string;
    cdmName?: string;
    cdmLastName?: string;
    employeeId?: string;
    cdmEmail?: string;
    rcdmName?: string;
    cdmEmployeeCode?: string;
    lssId?: string;
    isSchemeActive?: boolean;
    cdmFullName?: string;
    rcdmFullName?: string;
    cdmContactNumber?: string;
    rcdmContactNumber?: string;
}
class LssShiftModel {
    constructor(lssShiftModel?: LssShiftModel) {
        this.createdUserId = lssShiftModel == undefined ? null : lssShiftModel.createdUserId == undefined ? null : lssShiftModel.createdUserId;
        this.modifiedUserId = lssShiftModel == undefined ? null : lssShiftModel.modifiedUserId == undefined ? null : lssShiftModel.modifiedUserId;
        this.lssShiftId = lssShiftModel == undefined ? null : lssShiftModel.lssShiftId == undefined ? null : lssShiftModel.lssShiftId;
        this.lssShiftRefNo = lssShiftModel == undefined ? null : lssShiftModel.lssShiftRefNo == undefined ? null : lssShiftModel.lssShiftRefNo;
        this.lssId = lssShiftModel == undefined ? null : lssShiftModel.lssId == undefined ? null : lssShiftModel.lssId;
        this.lssShiftCategoryId = lssShiftModel == undefined ? null : lssShiftModel.lssShiftCategoryId == undefined ? null : lssShiftModel.lssShiftCategoryId;
        this.isDayShift = lssShiftModel == undefined ? true : lssShiftModel.isDayShift == undefined ? true : lssShiftModel.isDayShift;
        this.shiftTypeName = lssShiftModel == undefined ? "Day" : lssShiftModel.shiftTypeName == undefined ? "Day" : lssShiftModel.shiftTypeName;
        this.noOfGuards = lssShiftModel == undefined ? null : lssShiftModel.noOfGuards == undefined ? null : lssShiftModel.noOfGuards;
        this.lssGradeId = lssShiftModel == undefined ? null : lssShiftModel.lssGradeId == undefined ? null : lssShiftModel.lssGradeId;
        this.lssGradeName = lssShiftModel == undefined ? "" : lssShiftModel.lssGradeName == undefined ? "" : lssShiftModel.lssGradeName;
        this.is24HourShift = lssShiftModel == undefined ? true : lssShiftModel.is24HourShift == undefined ? true : lssShiftModel.is24HourShift;
        this.startShiftTime = lssShiftModel == undefined ? null : lssShiftModel.startShiftTime == undefined ? null : lssShiftModel.startShiftTime;
        this.endShiftTime = lssShiftModel == undefined ? null : lssShiftModel.endShiftTime == undefined ? null : lssShiftModel.endShiftTime;
        this.isSameasWeekDay = lssShiftModel == undefined ? false : lssShiftModel.isSameasWeekDay == undefined ? false : lssShiftModel.isSameasWeekDay;
    }
    createdUserId?: string;
    modifiedUserId?: string;
    lssShiftId?: string;
    lssShiftRefNo?: string;
    lssId?: string;
    lssShiftCategoryId?: number;
    isDayShift?: boolean;
    shiftTypeName?: string;
    noOfGuards?: number;
    lssGradeId?: number;
    lssGradeName?: string;
    is24HourShift?: boolean;
    startShiftTime?: string;
    endShiftTime?: string;
    isSameasWeekDay?: boolean;
}
class LssStageThreeModel {
    constructor(lssStageThreeModel?: LssStageThreeModel) {
        this.createdUserId = lssStageThreeModel == undefined ? null : lssStageThreeModel.createdUserId == undefined ? null : lssStageThreeModel.createdUserId;
        this.lssContactId = lssStageThreeModel == undefined ? '' : lssStageThreeModel.lssContactId == undefined ? '' : lssStageThreeModel.lssContactId;
        this.lssId = lssStageThreeModel ? lssStageThreeModel.lssId == undefined ? '' : lssStageThreeModel.lssId : '';
        this.contactPerson = lssStageThreeModel == undefined ? null : lssStageThreeModel.contactPerson == undefined ? null : lssStageThreeModel.contactPerson;
        this.mobileNo = lssStageThreeModel == undefined ? null : lssStageThreeModel.mobileNo == undefined ? null : lssStageThreeModel.mobileNo;
        this.mobileNoCountryCode = lssStageThreeModel == undefined ? null : lssStageThreeModel.mobileNoCountryCode == undefined ? null : lssStageThreeModel.mobileNoCountryCode;
        this.contactNo = lssStageThreeModel == undefined ? null : lssStageThreeModel.contactNo == undefined ? null : lssStageThreeModel.contactNo;
        this.contactNoCountryCode = lssStageThreeModel == undefined ? null : lssStageThreeModel.contactNoCountryCode == undefined ? null : lssStageThreeModel.contactNoCountryCode;
        this.email = lssStageThreeModel == undefined ? null : lssStageThreeModel.email == undefined ? null : lssStageThreeModel.email;
        this.lssContributionTypeId = lssStageThreeModel == undefined ? null : lssStageThreeModel.lssContributionTypeId == undefined ? null : lssStageThreeModel.lssContributionTypeId;
        this.lssClientTypeId = lssStageThreeModel == undefined ? null : lssStageThreeModel.lssClientTypeId == undefined ? null : lssStageThreeModel.lssClientTypeId;
        this.isFixedContribution = lssStageThreeModel == undefined ? true : lssStageThreeModel.isFixedContribution == undefined ? true : lssStageThreeModel.isFixedContribution;
        this.contributionAmount = lssStageThreeModel == undefined ? null : lssStageThreeModel.contributionAmount == undefined ? null : lssStageThreeModel.contributionAmount;
        this.contactNoCountryCode = lssStageThreeModel == undefined ? null : lssStageThreeModel.contactNoCountryCode == undefined ? null : lssStageThreeModel.contactNoCountryCode;
        this.isSection21 = lssStageThreeModel == undefined ? true : lssStageThreeModel.isSection21 == undefined ? true : lssStageThreeModel.isSection21;
        this.isRebate = lssStageThreeModel == undefined ? true : lssStageThreeModel.isRebate == undefined ? true : lssStageThreeModel.isRebate;
        this.isRebatePercentage = lssStageThreeModel == undefined ? true : lssStageThreeModel.isRebatePercentage == undefined ? true : lssStageThreeModel.isRebatePercentage;
        this.rebateAmount = lssStageThreeModel == undefined ? null : lssStageThreeModel.rebateAmount == undefined ? null : lssStageThreeModel.rebateAmount;
        this.rebatePercentage = lssStageThreeModel == undefined ? '' : lssStageThreeModel.rebatePercentage == undefined ? '' : lssStageThreeModel.rebatePercentage;
        this.vehicleRegistrationId = lssStageThreeModel == undefined ? '' : lssStageThreeModel.vehicleRegistrationId == undefined ? '' : lssStageThreeModel.vehicleRegistrationId;
        this.vehicleCallSign = lssStageThreeModel == undefined ? null : lssStageThreeModel.vehicleCallSign == undefined ? null : lssStageThreeModel.vehicleCallSign;
        this.noOfPole = lssStageThreeModel == undefined ? null : lssStageThreeModel.noOfPole == undefined ? null : lssStageThreeModel.noOfPole;
        this.noOfCamera = lssStageThreeModel == undefined ? null : lssStageThreeModel.noOfCamera == undefined ? null : lssStageThreeModel.noOfCamera;
    }
    createdUserId: string;
    lssContactId: string;
    contactPerson: string;
    mobileNo: string;
    mobileNoCountryCode: string;
    contactNo: string;
    contactNoCountryCode: string;
    email: string;
    lssContributionTypeId: string;
    lssClientTypeId: string;
    isFixedContribution: boolean;
    contributionAmount: number;
    isSection21: boolean;
    isRebate: boolean;
    isRebatePercentage: boolean;
    rebateAmount: number;
    rebatePercentage: string;
    vehicleRegistrationId: string;
    vehicleCallSign: string;
    noOfPole: number;
    noOfCamera: number;
    lssId: string
}
class LssInstallation {
    constructor(lssInstallation?: LssInstallation) {
        this.lssId = lssInstallation ? lssInstallation.lssId == undefined ? '' : lssInstallation.lssId : '';
        this.modifiedUserId = lssInstallation ? lssInstallation.modifiedUserId == undefined ? '' : lssInstallation.modifiedUserId : '';
        this.isSpecialPackageForKitInstallation = lssInstallation ? lssInstallation.isSpecialPackageForKitInstallation == undefined ? false : lssInstallation.isSpecialPackageForKitInstallation : false;
        this.itemDiscountPercentage = lssInstallation ? lssInstallation.itemDiscountPercentage == undefined ? '' : lssInstallation.itemDiscountPercentage : '';
        this.itemDiscountAmount = lssInstallation ? lssInstallation.itemDiscountAmount == undefined ? '' : lssInstallation.itemDiscountAmount : ''; this.lssInstallationPricing = lssInstallation ? lssInstallation.lssInstallationPricing == undefined ? [] : lssInstallation.lssInstallationPricing : [];
        this.createdUserId = lssInstallation ? lssInstallation.createdUserId == undefined ? null : lssInstallation.createdUserId : null;
        this.isItemDiscountPercentage = lssInstallation ? lssInstallation.isItemDiscountPercentage == undefined ? false : lssInstallation.isItemDiscountPercentage : false;
        this.isSpecificItemDiscount = lssInstallation ? lssInstallation.isSpecificItemDiscount == undefined ? false : lssInstallation.isSpecificItemDiscount : false;
    }
    lssId?: string;
    modifiedUserId: string;
    lssInstallationPricing: LssInstallationPricingModel[];
    isSpecialPackageForKitInstallation: boolean;
    itemDiscountPercentage: string;
    itemDiscountAmount: string;
    createdUserId: string;
    isItemDiscountPercentage: boolean;
    isSpecificItemDiscount: boolean;
}
class LssInstallationPricingModel {
    constructor(lssInstallationPricingModel?: LssInstallationPricingModel) {
        this.lssItemPricingId = lssInstallationPricingModel ? lssInstallationPricingModel.lssItemPricingId == undefined ? "" : lssInstallationPricingModel.lssItemPricingId : "";
        this.lssId = lssInstallationPricingModel ? lssInstallationPricingModel.lssId == undefined ? '' : lssInstallationPricingModel.lssId : '';
        this.itemId = lssInstallationPricingModel ? lssInstallationPricingModel.itemId == undefined ? '' : lssInstallationPricingModel.itemId : '';
        this.sellingPrice = lssInstallationPricingModel ? lssInstallationPricingModel.sellingPrice == undefined ? "" : lssInstallationPricingModel.sellingPrice : "";
        this.createdUserId = lssInstallationPricingModel ? lssInstallationPricingModel.createdUserId == undefined ? '' : lssInstallationPricingModel.createdUserId : '';
        this.stockCode = lssInstallationPricingModel ? lssInstallationPricingModel.stockCode == undefined ? '' : lssInstallationPricingModel.stockCode : '';
        this.stockName = lssInstallationPricingModel ? lssInstallationPricingModel.stockName == undefined ? '' : lssInstallationPricingModel.stockName : '';
    }
    lssItemPricingId?: string;
    lssId: string;
    itemId: string;
    sellingPrice: string;
    createdUserId: string;
    stockCode: string;
    stockName: string;
}
class LssServiceInfoModel {
    constructor(lssServiceInfoModel?: LssServiceInfoModel) {
        this.lssId = lssServiceInfoModel == undefined ? "" : lssServiceInfoModel.lssId == undefined ? "" : lssServiceInfoModel.lssId;
        this.isServiceOverAllDiscountOnMonthlyFee = lssServiceInfoModel == undefined ? false : lssServiceInfoModel.isServiceOverAllDiscountOnMonthlyFee == undefined ? false : lssServiceInfoModel.isServiceOverAllDiscountOnMonthlyFee;
        this.isPercentage = lssServiceInfoModel == undefined ? false : lssServiceInfoModel.isPercentage == undefined ? false : lssServiceInfoModel.isPercentage;
        this.isSpecialServices = lssServiceInfoModel == undefined ? false : lssServiceInfoModel.isSpecialServices == undefined ? false : lssServiceInfoModel.isSpecialServices;
        this.serviceDiscountAmount = lssServiceInfoModel == undefined ? '' : lssServiceInfoModel.serviceDiscountAmount == undefined ? '' : lssServiceInfoModel.serviceDiscountAmount;
        this.serviceDiscountPercentage = lssServiceInfoModel == undefined ? '' : lssServiceInfoModel.serviceDiscountAmount == undefined ? '' : lssServiceInfoModel.serviceDiscountAmount;
        this.createdUserId = lssServiceInfoModel == undefined ? "" : lssServiceInfoModel.createdUserId == undefined ? "" : lssServiceInfoModel.createdUserId;
        this.services = lssServiceInfoModel == undefined ? [] : lssServiceInfoModel.services == undefined ? [] : lssServiceInfoModel.services;
        this.modifiedUserId = lssServiceInfoModel == undefined ? "" : lssServiceInfoModel.modifiedUserId == undefined ? "" : lssServiceInfoModel.modifiedUserId;
    }
    lssId: string;
    isServiceOverAllDiscountOnMonthlyFee: boolean;
    isPercentage: boolean;
    isSpecialServices: boolean;
    serviceDiscountAmount: string;
    serviceDiscountPercentage: string;
    createdUserId: string;
    modifiedUserId: string;
    services: LssServicePriceInfoModel[];
}
class LssServicePriceInfoModel {
    constructor(lssServicePriceInfoModel?: LssServicePriceInfoModel) {
        this.lssId = lssServicePriceInfoModel == undefined ? null : lssServicePriceInfoModel.lssId == undefined ? null : lssServicePriceInfoModel.lssId;
        this.serviceCategoryId = lssServicePriceInfoModel == undefined ? "" : lssServicePriceInfoModel.serviceCategoryId == undefined ? "" : lssServicePriceInfoModel.serviceCategoryId;
        this.serviceCategoryName = lssServicePriceInfoModel == undefined ? "" : lssServicePriceInfoModel.serviceCategoryName == undefined ? "" : lssServicePriceInfoModel.serviceCategoryName;
        this.createdUserId = lssServicePriceInfoModel == undefined ? "" : lssServicePriceInfoModel.createdUserId == undefined ? "" : lssServicePriceInfoModel.createdUserId;
        this.discountAmount = lssServicePriceInfoModel == undefined ? "" : lssServicePriceInfoModel.discountAmount == undefined ? "" : lssServicePriceInfoModel.discountAmount;
        this.discountPercentage = lssServicePriceInfoModel == undefined ? "" : lssServicePriceInfoModel.discountPercentage == undefined ? "" : lssServicePriceInfoModel.discountPercentage;
        this.lssServicePricingId = lssServicePriceInfoModel == undefined ? "" : lssServicePriceInfoModel.lssServicePricingId == undefined ? "" : lssServicePriceInfoModel.lssServicePricingId;
        this.serviceId = lssServicePriceInfoModel == undefined ? "" : lssServicePriceInfoModel.serviceId == undefined ? "" : lssServicePriceInfoModel.serviceId;
        this.serviceName = lssServicePriceInfoModel == undefined ? "" : lssServicePriceInfoModel.serviceName == undefined ? "" : lssServicePriceInfoModel.serviceName;
        this.isPercentage = lssServicePriceInfoModel == undefined ? false : lssServicePriceInfoModel.isPercentage == undefined ? false : lssServicePriceInfoModel.isPercentage;
        this.serviceNumber = lssServicePriceInfoModel == undefined ? "" : lssServicePriceInfoModel.serviceNumber == undefined ? "" : lssServicePriceInfoModel.serviceNumber;
    }
    lssServicePricingId: string;
    lssId: string;
    createdUserId: string;
    discountAmount: string;
    discountPercentage: string;
    isPercentage: boolean;
    serviceCategoryName: string;
    serviceCategoryId: string;
    serviceId: string;
    serviceName: string;
    serviceNumber: string;
}
class LssBoundaryModel {
    constructor(lssBoundaryModel?: LssBoundaryModel) {
        this.createdUserId = lssBoundaryModel == undefined ? null : lssBoundaryModel.createdUserId == undefined ? null : lssBoundaryModel.createdUserId;
        this.lssId = lssBoundaryModel == undefined ? "" : lssBoundaryModel.lssId == undefined ? "" : lssBoundaryModel.lssId;
        this.goLiveDate = lssBoundaryModel == undefined ? "" : lssBoundaryModel.goLiveDate == undefined ? "" : lssBoundaryModel.goLiveDate;
    }
    createdUserId: string;
    description: string;
    lssId: string;
    goLiveDate: string;
}

export { StageOneModel, LssStageThreeModel, LssInstallation, LssInstallationPricingModel, LssServiceInfoModel, LssServicePriceInfoModel, LssShiftModel, LssBoundaryModel };