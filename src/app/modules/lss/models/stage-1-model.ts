class StageOneModel {
    constructor(stageOneModel?: StageOneModel) {
        this.lssRefNo = stageOneModel ? stageOneModel.lssRefNo == undefined ? '' : stageOneModel.lssRefNo : '';
        this.lssName = stageOneModel ? stageOneModel.lssName == undefined ? '' : stageOneModel.lssName : '';
        this.lssTypeId = stageOneModel ? stageOneModel.lssTypeId == undefined ? '' : stageOneModel.lssTypeId : '';
        this.branchId = stageOneModel ? stageOneModel.branchId == undefined ? '' : stageOneModel.branchId : '';
        this.lssResourceId = stageOneModel ? stageOneModel.lssResourceId == undefined ? '' : stageOneModel.lssResourceId : '';
        this.contractNo = stageOneModel ? stageOneModel.contractNo == undefined ? '' : stageOneModel.contractNo : '';
        this.billstockRefNo = stageOneModel ? stageOneModel.billstockRefNo == undefined ? '' : stageOneModel.billstockRefNo : '';
        this.startDate = stageOneModel ? stageOneModel.startDate == undefined ? null : stageOneModel.startDate : null;
        this.renewalDate = stageOneModel ? stageOneModel.renewalDate == undefined ? null : stageOneModel.renewalDate : null;
        this.goLiveDate = stageOneModel ? stageOneModel.goLiveDate == undefined ? null : stageOneModel.goLiveDate : null;
        this.startUpCost = stageOneModel ? stageOneModel.startUpCost == undefined ? null : stageOneModel.startUpCost : null;
        this.cdmEmployeeId = stageOneModel ? stageOneModel.cdmEmployeeId == undefined ? null : stageOneModel.cdmEmployeeId : null;
        this.rcdmEmployeeId = stageOneModel ? stageOneModel.rcdmEmployeeId == undefined ? null : stageOneModel.rcdmEmployeeId : null;
        this.createdUserId = stageOneModel ? stageOneModel.createdUserId == undefined ? null : stageOneModel.createdUserId : null;
    }
    lssRefNo?: string;
    lssName?: string;
    lssTypeId?: string;
    branchId?: string;
    lssResourceId?: string;
    contractNo?: string;
    billstockRefNo?: string;
    startDate?: string;
    renewalDate?: string;
    goLiveDate?: string;
    startUpCost?: number;
    cdmEmployeeId?: string;
    rcdmEmployeeId?: string;
    createdUserId?: string;
}
class LssInstallation {
    constructor(lssInstallation?: LssInstallation) {
        this.lssId = lssInstallation ? lssInstallation.lssId == undefined ? '' : lssInstallation.lssId : '';
        this.modifiedUserId = lssInstallation ? lssInstallation.modifiedUserId == undefined ? '' : lssInstallation.modifiedUserId : '';
        this.isSpecialPackageForKitInstallation = lssInstallation ? lssInstallation.isSpecialPackageForKitInstallation == undefined ? true : lssInstallation.isSpecialPackageForKitInstallation : true;
        this.itemDiscountPercentage = lssInstallation ? lssInstallation.itemDiscountPercentage == undefined ? '' : lssInstallation.itemDiscountPercentage : '';
        this.itemDiscountAmount = lssInstallation ? lssInstallation.itemDiscountAmount == undefined ? '' : lssInstallation.itemDiscountAmount : '';
        this.lssInstallationPricing = lssInstallation ? lssInstallation.lssInstallationPricing == undefined ? [] : lssInstallation.lssInstallationPricing : [];
        this.createdUserId = lssInstallation ? lssInstallation.createdUserId == undefined ? null : lssInstallation.createdUserId : null;
        this.isItemDiscountPercentage = lssInstallation ? lssInstallation.isItemDiscountPercentage == undefined ? true : lssInstallation.isItemDiscountPercentage : true;
        this.isSpecificItemDiscount = lssInstallation ? lssInstallation.isSpecificItemDiscount == undefined ? true : lssInstallation.isSpecificItemDiscount : true;
    }
    lssId?: string;
    modifiedUserId: string;
    lssInstallationPricing: LssInstallationPricingModel[];
    isSpecialPackageForKitInstallation: boolean;
    itemDiscountPercentage: string;
    isItemDiscountPercentage: boolean;
    isSpecificItemDiscount: boolean;
    itemDiscountAmount: string;
    createdUserId: string;
}
class LssInstallationPricingModel {
    constructor(lssInstallationPricingModel?: LssInstallationPricingModel) {
        this.lssItemPricingId = lssInstallationPricingModel ? lssInstallationPricingModel.lssItemPricingId == undefined ? "" : lssInstallationPricingModel.lssItemPricingId : "";
        this.lssId = lssInstallationPricingModel ? lssInstallationPricingModel.lssId == undefined ? '' : lssInstallationPricingModel.lssId : '';
        this.itemId = lssInstallationPricingModel ? lssInstallationPricingModel.itemId == undefined ? '' : lssInstallationPricingModel.itemId : '';
        this.sellingPrice = lssInstallationPricingModel ? lssInstallationPricingModel.sellingPrice == undefined ? "" : lssInstallationPricingModel.sellingPrice : "";
        this.createdUserId = lssInstallationPricingModel ? lssInstallationPricingModel.createdUserId == undefined ? '' : lssInstallationPricingModel.createdUserId : '';
        this.stockCode = lssInstallationPricingModel ? lssInstallationPricingModel.stockCode == undefined ? '' : lssInstallationPricingModel.stockCode : '';
        this.stockName = lssInstallationPricingModel ? lssInstallationPricingModel.stockName == undefined ? '' : lssInstallationPricingModel.stockName : '';
    }
    lssItemPricingId?: string;
    lssId: string;
    itemId: string;
    sellingPrice: string;
    createdUserId: string;
    stockCode: string;
    stockName: string;
}

export { LssInstallation, LssInstallationPricingModel, StageOneModel };

