class LssInstallation {
    constructor(lssInstallation?: LssInstallation) {
        this.lssId = lssInstallation ? lssInstallation.lssId == undefined ? '' : lssInstallation.lssId : '';
        this.modifiedUserId = lssInstallation ? lssInstallation.modifiedUserId == undefined ? '' : lssInstallation.modifiedUserId : '';
        this.createdUserId = lssInstallation ? lssInstallation.createdUserId == undefined ? '' : lssInstallation.createdUserId : '';
        this.isPercentage = lssInstallation ? lssInstallation.isPercentage == undefined ? true : lssInstallation.isPercentage : true;
        this.isSpecialPackageForKitInstallation = lssInstallation ? lssInstallation.isSpecialPackageForKitInstallation == undefined ? true : lssInstallation.isSpecialPackageForKitInstallation : true;
        this.itemDiscountPercentage = lssInstallation ? lssInstallation.itemDiscountPercentage == undefined ? true : lssInstallation.itemDiscountPercentage : true;
        this.itemDiscountAmount = lssInstallation ? lssInstallation.itemDiscountAmount == undefined ? 0 : lssInstallation.itemDiscountAmount : 0;
        this.lssInstallationPricing = lssInstallation ? lssInstallation.lssInstallationPricing == undefined ? [] : lssInstallation.lssInstallationPricing : [];
        this.isItemDiscountPercentage = lssInstallation ? lssInstallation.isItemDiscountPercentage == undefined ? true : lssInstallation.isItemDiscountPercentage : true;
        this.isSpecificItemDiscount = lssInstallation ? lssInstallation.isSpecificItemDiscount == undefined ? true : lssInstallation.isSpecificItemDiscount : true;
    }
    lssId?: string;
    modifiedUserId: string;
    createdUserId: string;
    lssInstallationPricing: LssInstallationPricingModel[];
    isSpecialPackageForKitInstallation: boolean;
    itemDiscountPercentage: boolean;
    isItemDiscountPercentage: boolean;
    isSpecificItemDiscount: boolean;
    isPercentage: boolean;
    itemDiscountAmount: Number
}
class LssInstallationPricingModel {
    constructor(lssInstallationPricingModel?: LssInstallationPricingModel) {
        this.lssItemPricingId = lssInstallationPricingModel ? lssInstallationPricingModel.lssItemPricingId == undefined ? "" : lssInstallationPricingModel.lssItemPricingId : "";
        this.lssId = lssInstallationPricingModel ? lssInstallationPricingModel.lssId == undefined ? '' : lssInstallationPricingModel.lssId : '';
        this.itemId = lssInstallationPricingModel ? lssInstallationPricingModel.itemId == undefined ? '' : lssInstallationPricingModel.itemId : '';
        this.sellingPrice = lssInstallationPricingModel ? lssInstallationPricingModel.sellingPrice == undefined ? 0 : lssInstallationPricingModel.sellingPrice : 0;
        this.createdUserId = lssInstallationPricingModel ? lssInstallationPricingModel.createdUserId == undefined ? '' : lssInstallationPricingModel.createdUserId : '';
    }
    lssItemPricingId?: string;
    lssId: string;
    itemId: string;
    sellingPrice: Number;
    createdUserId: string;
}

export { LssInstallation, LssInstallationPricingModel };