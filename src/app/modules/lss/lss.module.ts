import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  LssRegistartionListComponent, LssRegistartionViewComponent, LssRegistartionStageOneComponent, LssRegistartionStageTwoComponent,
  LssRegistartionStageThreeComponent, LssRegistartionStageThreeGisComponent, LssRegistartionStageFourComponent,
  LssRegistartionStageFiveComponent,ShiftModalComponent
} from '@app/modules/lss';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { LeafletModule } from "@asymmetrik/ngx-leaflet";
import { LeafletDrawModule } from "@asymmetrik/ngx-leaflet-draw";
import { LeadLifecycleReusableComponentsModule } from '@modules/sales/components/lead-header';
import { LssRoutingModule } from './lss-routing.module';

@NgModule({
  declarations: [LssRegistartionListComponent, LssRegistartionViewComponent, LssRegistartionStageOneComponent,
    LssRegistartionStageTwoComponent, LssRegistartionStageThreeComponent, LssRegistartionStageThreeGisComponent,
    LssRegistartionStageFourComponent, LssRegistartionStageFiveComponent,ShiftModalComponent],
  imports: [
    CommonModule,
    LeafletModule,
    LeafletDrawModule,
    LssRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LeadLifecycleReusableComponentsModule
  ],
  entryComponents: [ShiftModalComponent],
  exports: []
})
export class LssModule { }
