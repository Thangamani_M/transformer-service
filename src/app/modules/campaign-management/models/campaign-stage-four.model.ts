class CampaigncampaignStageFourModel {
    constructor(campaignStageFourModel?: CampaigncampaignStageFourModel) {
        this.marketingCampaignId = campaignStageFourModel ? campaignStageFourModel.marketingCampaignId == undefined ? '' : campaignStageFourModel.marketingCampaignId : '';
        this.isOverallDiscount = campaignStageFourModel ? campaignStageFourModel.isOverallDiscount == undefined ? false : campaignStageFourModel.isOverallDiscount : false;
        this.isInstallationDiscountPercentage = campaignStageFourModel ? campaignStageFourModel.isInstallationDiscountPercentage == undefined ? false : campaignStageFourModel.isInstallationDiscountPercentage : false;
        this.isSpecificItemDiscount = campaignStageFourModel ? campaignStageFourModel.isSpecificItemDiscount == undefined ? false : campaignStageFourModel.isSpecificItemDiscount : false;
        this.itemDiscountValue = campaignStageFourModel ? campaignStageFourModel.itemDiscountValue == undefined ? '' : campaignStageFourModel.itemDiscountValue : '';
       
        this.items = campaignStageFourModel ? campaignStageFourModel.items == undefined ? [] : campaignStageFourModel.items : [];
 
        this.campaignName = campaignStageFourModel ? campaignStageFourModel.campaignName == undefined ? '' : campaignStageFourModel.campaignName : '';
        this.batchDescription = campaignStageFourModel ? campaignStageFourModel.batchDescription == undefined ? '' : campaignStageFourModel.batchDescription : '';
        this.startDate = campaignStageFourModel ? campaignStageFourModel.startDate == undefined ? '' : campaignStageFourModel.startDate : '';
        this.endDate = campaignStageFourModel ? campaignStageFourModel.endDate == undefined ? '' : campaignStageFourModel.endDate : '';
     
        this.createdUserId = campaignStageFourModel ? campaignStageFourModel.createdUserId == undefined ? null : campaignStageFourModel.createdUserId : null;
    }
    marketingCampaignId?:string;
    isOverallDiscount?: boolean;
    isInstallationDiscountPercentage?: boolean;
    isSpecificItemDiscount?: boolean;
    isSpecificServiceDiscount?: boolean;
    isFixedContractFee?: boolean;
    itemDiscountValue?: any;
    batchDescription?: string;
    campaignName?: string;
    startDate?: string;
    endDate?: string;
    items: ItemListModel[]; 
    createdUserId?: string;
}


  

class ItemListModel {
    constructor(ItemListModel?: ItemListModel) {
        this.marketingCampaignItemId = ItemListModel ? ItemListModel.marketingCampaignItemId == undefined ? '' : ItemListModel.marketingCampaignItemId : '';
        this.marketingCampaignId = ItemListModel ? ItemListModel.marketingCampaignId == undefined ? '' : ItemListModel.marketingCampaignId : '';
        this.itemId = ItemListModel ? ItemListModel.itemId == undefined ? null : ItemListModel.itemId : null;
        this.itemName = ItemListModel ? ItemListModel.itemName == undefined ? null : ItemListModel.itemName : null;
        this.sellingPrice = ItemListModel ? ItemListModel.sellingPrice == undefined ? null : ItemListModel.sellingPrice : null;
        this.createdUserId = ItemListModel ? ItemListModel.createdUserId == undefined ? null : ItemListModel.createdUserId : null;

    }
    marketingCampaignItemId:string;
    marketingCampaignId:string;
    itemId:any;
    itemName:string;
    sellingPrice:string
    createdUserId?: string;
}



export { CampaigncampaignStageFourModel,ItemListModel }
