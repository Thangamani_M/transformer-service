class CampaigncampaignStageTwoModel {
    constructor(campaignStageTwoModel?: CampaigncampaignStageTwoModel) {
        this.marketingCampaignId = campaignStageTwoModel ? campaignStageTwoModel.marketingCampaignId == undefined ? '' : campaignStageTwoModel.marketingCampaignId : '';
        this.isWaiverAdminFee = campaignStageTwoModel ? campaignStageTwoModel.isWaiverAdminFee == undefined ? false : campaignStageTwoModel.isWaiverAdminFee : false;
        this.isWaiverProrataCharge = campaignStageTwoModel ? campaignStageTwoModel.isWaiverProrataCharge == undefined ? false : campaignStageTwoModel.isWaiverProrataCharge : false;
        this.isComplimentaryService = campaignStageTwoModel ? campaignStageTwoModel.isComplimentaryService == undefined ? false : campaignStageTwoModel.isComplimentaryService : false;
        this.isWaiverAnnualNetworkFee = campaignStageTwoModel ? campaignStageTwoModel.isWaiverAnnualNetworkFee == undefined ? false : campaignStageTwoModel.isWaiverAnnualNetworkFee : false;
        this.isFixedContractFee = campaignStageTwoModel ? campaignStageTwoModel.isFixedContractFee == undefined ? false : campaignStageTwoModel.isFixedContractFee : false;
      
        this.complimentaryMonthId = campaignStageTwoModel ? campaignStageTwoModel.complimentaryMonthId == undefined ? '' : campaignStageTwoModel.complimentaryMonthId : '';
        this.annualNetworkFeePeriodId = campaignStageTwoModel ? campaignStageTwoModel.annualNetworkFeePeriodId == undefined ? '' : campaignStageTwoModel.annualNetworkFeePeriodId : '';
        this.fixedContractFeePeriodId = campaignStageTwoModel ? campaignStageTwoModel.complimentaryMonthId == undefined ? '' : campaignStageTwoModel.fixedContractFeePeriodId : '';
       
        this.campaignName = campaignStageTwoModel ? campaignStageTwoModel.campaignName == undefined ? '' : campaignStageTwoModel.campaignName : '';
        this.batchDescription = campaignStageTwoModel ? campaignStageTwoModel.batchDescription == undefined ? '' : campaignStageTwoModel.batchDescription : '';
        this.startDate = campaignStageTwoModel ? campaignStageTwoModel.startDate == undefined ? '' : campaignStageTwoModel.startDate : '';
        this.endDate = campaignStageTwoModel ? campaignStageTwoModel.endDate == undefined ? '' : campaignStageTwoModel.endDate : '';
     
        this.createdUserId = campaignStageTwoModel ? campaignStageTwoModel.createdUserId == undefined ? null : campaignStageTwoModel.createdUserId : null;
    }
    marketingCampaignId?:string;
    isWaiverAdminFee?: boolean;
    isWaiverProrataCharge?: boolean;
    isComplimentaryService?: boolean;
    isWaiverAnnualNetworkFee?: boolean;
    isFixedContractFee?: boolean;
    complimentaryMonthId?: any;
    annualNetworkFeePeriodId?: any;
    fixedContractFeePeriodId?: any;
    batchDescription?: string;
    campaignName?: string;
    startDate?: string;
    endDate?: string;
    createdUserId?: string;
}

export { CampaigncampaignStageTwoModel }
