class CampaigncampaignStageThreeModel {
    constructor(campaignStageThreeModel?: CampaigncampaignStageThreeModel) {
        this.marketingCampaignId = campaignStageThreeModel ? campaignStageThreeModel.marketingCampaignId == undefined ? '' : campaignStageThreeModel.marketingCampaignId : '';
        this.isPreferenceRateApplicable = campaignStageThreeModel ? campaignStageThreeModel.isPreferenceRateApplicable == undefined ? false : campaignStageThreeModel.isPreferenceRateApplicable : false;
        this.isDiscountOnMonthlyFee = campaignStageThreeModel ? campaignStageThreeModel.isDiscountOnMonthlyFee == undefined ? false : campaignStageThreeModel.isDiscountOnMonthlyFee : false;
        this.isServiceDiscountPercentage = campaignStageThreeModel ? campaignStageThreeModel.isServiceDiscountPercentage == undefined ? false : campaignStageThreeModel.isServiceDiscountPercentage : false;
        this.serviceDiscountValue = campaignStageThreeModel ? campaignStageThreeModel.serviceDiscountValue == undefined ? '' : campaignStageThreeModel.serviceDiscountValue : '';
       
        this.isSpecificServiceDiscount = campaignStageThreeModel ? campaignStageThreeModel.isSpecificServiceDiscount == undefined ? false : campaignStageThreeModel.isSpecificServiceDiscount : false;
        this.services = campaignStageThreeModel ? campaignStageThreeModel.services == undefined ? [] : campaignStageThreeModel.services : [];
 
        this.campaignName = campaignStageThreeModel ? campaignStageThreeModel.campaignName == undefined ? '' : campaignStageThreeModel.campaignName : '';
        this.batchDescription = campaignStageThreeModel ? campaignStageThreeModel.batchDescription == undefined ? '' : campaignStageThreeModel.batchDescription : '';
        this.startDate = campaignStageThreeModel ? campaignStageThreeModel.startDate == undefined ? '' : campaignStageThreeModel.startDate : '';
        this.endDate = campaignStageThreeModel ? campaignStageThreeModel.endDate == undefined ? '' : campaignStageThreeModel.endDate : '';
     
        this.createdUserId = campaignStageThreeModel ? campaignStageThreeModel.createdUserId == undefined ? null : campaignStageThreeModel.createdUserId : null;
    }
    marketingCampaignId?:string;
    isPreferenceRateApplicable?: boolean;
    isDiscountOnMonthlyFee?: boolean;
    isServiceDiscountPercentage?: boolean;
    isSpecificServiceDiscount?: boolean;
    isFixedContractFee?: boolean;
    serviceDiscountValue?: any;
    batchDescription?: string;
    campaignName?: string;
    startDate?: string;
    endDate?: string;
    services: ServiceListModel[]; 
    createdUserId?: string;
}


  

class ServiceListModel {
    constructor(serviceListModel?: ServiceListModel) {
        this.marketingCampaignServiceId = serviceListModel ? serviceListModel.marketingCampaignServiceId == undefined ? '' : serviceListModel.marketingCampaignServiceId : '';
        this.marketingCampaignId = serviceListModel ? serviceListModel.marketingCampaignId == undefined ? '' : serviceListModel.marketingCampaignId : '';
        this.serviceCategoryId = serviceListModel ? serviceListModel.serviceCategoryId == undefined ? '' : serviceListModel.serviceCategoryId : '';
        this.serviceId = serviceListModel ? serviceListModel.serviceId == undefined ? '' : serviceListModel.serviceId : '';
        this.serviceNumber = serviceListModel ? serviceListModel.serviceNumber == undefined ? null : serviceListModel.serviceNumber : null;
        this.servicesList = serviceListModel ? serviceListModel.servicesList == undefined ? [] : serviceListModel.servicesList : [];
        this.isPercentage = serviceListModel ? serviceListModel.isPercentage == undefined ? false : serviceListModel.isPercentage : false;
        this.discountPercentage = serviceListModel ? serviceListModel.discountPercentage == undefined ? null : serviceListModel.discountPercentage : null;
        this.discountAmount = serviceListModel ? serviceListModel.discountAmount == undefined ? null : serviceListModel.discountAmount : null;
        this.createdUserId = serviceListModel ? serviceListModel.createdUserId == undefined ? null : serviceListModel.createdUserId : null;

    }
    marketingCampaignServiceId:string;
    marketingCampaignId:string;
    serviceCategoryId:string;
    serviceId:string
    serviceNumber:string;
    servicesList:any;
    isPercentage:boolean
    discountPercentage:string
    discountAmount:string
    createdUserId?: string;
}



export { CampaigncampaignStageThreeModel,ServiceListModel }
