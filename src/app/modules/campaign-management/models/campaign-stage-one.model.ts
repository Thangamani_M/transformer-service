class CampaigncampaignStageOneModel {
    constructor(campaignStageOneModel?: CampaigncampaignStageOneModel) {
        this.marketingCampaignId = campaignStageOneModel ? campaignStageOneModel.marketingCampaignId == undefined ? '' : campaignStageOneModel.marketingCampaignId : '';
        this.campaignName = campaignStageOneModel ? campaignStageOneModel.campaignName == undefined ? '' : campaignStageOneModel.campaignName : '';
        this.batchDescription = campaignStageOneModel ? campaignStageOneModel.batchDescription == undefined ? '' : campaignStageOneModel.batchDescription : '';
        this.startDate = campaignStageOneModel ? campaignStageOneModel.startDate == undefined ? '' : campaignStageOneModel.startDate : '';
        this.endDate = campaignStageOneModel ? campaignStageOneModel.endDate == undefined ? '' : campaignStageOneModel.endDate : '';
        this.sourceId = campaignStageOneModel ? campaignStageOneModel.sourceId == undefined ? '' : campaignStageOneModel.sourceId : '';
        this.sourceGroupId = campaignStageOneModel ? campaignStageOneModel.sourceGroupId == undefined ? '' : campaignStageOneModel.sourceGroupId : '';
        this.budget = campaignStageOneModel ? campaignStageOneModel.budget == undefined ? '' : campaignStageOneModel.budget : '';
        this.totalBudget = campaignStageOneModel ? campaignStageOneModel.totalBudget == undefined ? '' : campaignStageOneModel.totalBudget : '';
        this.sourceName = campaignStageOneModel ? campaignStageOneModel.sourceName == undefined ? '' : campaignStageOneModel.sourceName : '';

        this.regionIds = campaignStageOneModel ? campaignStageOneModel.regionIds == undefined ? [] : campaignStageOneModel.regionIds : [];
        // this.divisionIds = campaignStageOneModel ? campaignStageOneModel.divisionIds == undefined ? [] : campaignStageOneModel.divisionIds : [];
        this.districtIds = campaignStageOneModel ? campaignStageOneModel.districtIds == undefined ? [] : campaignStageOneModel.districtIds : [];
        this.branchIds = campaignStageOneModel ? campaignStageOneModel.branchIds == undefined ? [] : campaignStageOneModel.branchIds : [];
        this.marketingCampaignTypeIds = campaignStageOneModel ? campaignStageOneModel.marketingCampaignTypeIds == undefined ? [] : campaignStageOneModel.marketingCampaignTypeIds : [];
        // this.marketingCampaignMediumIds = campaignStageOneModel ? campaignStageOneModel.marketingCampaignMediumIds == undefined ? [] : campaignStageOneModel.marketingCampaignMediumIds : [];
        this.requestor = campaignStageOneModel ? campaignStageOneModel.requestor == undefined ? '' : campaignStageOneModel.requestor : '';
        this.requestorRegionIds = campaignStageOneModel ? campaignStageOneModel.requestorRegionIds == undefined ? [] : campaignStageOneModel.requestorRegionIds : [];
        this.marketingCampaignDatabaseTypeId = campaignStageOneModel ? campaignStageOneModel.marketingCampaignDatabaseTypeId == undefined ? '' : campaignStageOneModel.marketingCampaignDatabaseTypeId : '';
        this.fromAgeId = campaignStageOneModel ? campaignStageOneModel.fromAgeId == undefined ? '' : campaignStageOneModel.fromAgeId : '';
        this.toAgeId = campaignStageOneModel ? campaignStageOneModel.toAgeId == undefined ? '' : campaignStageOneModel.toAgeId : '';
        this.genderIds = campaignStageOneModel ? campaignStageOneModel.genderIds == undefined ? [] : campaignStageOneModel.genderIds : [];

        this.audienceRegionIds = campaignStageOneModel ? campaignStageOneModel.audienceRegionIds == undefined ? [] : campaignStageOneModel.audienceRegionIds : [];
        this.audienceDivisionIds = campaignStageOneModel ? campaignStageOneModel.audienceDivisionIds == undefined ? [] : campaignStageOneModel.audienceDivisionIds : [];
        this.audienceDistrictIds = campaignStageOneModel ? campaignStageOneModel.audienceDistrictIds == undefined ? [] : campaignStageOneModel.audienceDistrictIds : [];
        this.audienceBranchIds = campaignStageOneModel ? campaignStageOneModel.audienceBranchIds == undefined ? [] : campaignStageOneModel.audienceBranchIds : [];

        this.audienceSuburbIds = campaignStageOneModel ? campaignStageOneModel.audienceSuburbIds == undefined ? [] : campaignStageOneModel.audienceSuburbIds : [];
        this.mainAreaIds = campaignStageOneModel ? campaignStageOneModel.mainAreaIds == undefined ? [] : campaignStageOneModel.mainAreaIds : [];
        this.boundaryIds = campaignStageOneModel ? campaignStageOneModel.boundaryIds == undefined ? [] : campaignStageOneModel.boundaryIds : [];
        this.lssIds = campaignStageOneModel ? campaignStageOneModel.lssIds == undefined ? [] : campaignStageOneModel.lssIds : [];
        this.campaignTypeValues = campaignStageOneModel ? campaignStageOneModel.campaignTypeValues == undefined ? [] : campaignStageOneModel.campaignTypeValues : [];

        this.createdUserId = campaignStageOneModel ? campaignStageOneModel.createdUserId == undefined ? null : campaignStageOneModel.createdUserId : null;
    }
    marketingCampaignId?: string;
    batchDescription?: string;
    campaignName?: string;
    branchIds?: any;
    marketingCampaignTypeIds?: any;
    startDate?: any;
    // cdmmarketingCampaignMediumIds?: any;
    // rcdmmarketingCampaignMediumIds?: any;
    createdUserId?: string;
    divisionIds?: any;
    regionIds?: any;
    requestor: string;
    requestorRegionIds?: any;
    districtIds?: any;
    budget?: string;
    totalBudget: string;
    endDate?: any;
    sourceName?: string;
    // marketingCampaignMediumIds?: any;
    sourceId?: string;
    sourceGroupId?: string;
    marketingCampaignDatabaseTypeId: string
    fromAgeId: string;
    toAgeId: string;
    genderIds: any
    audienceRegionIds: any
    audienceDivisionIds: any
    audienceDistrictIds: any
    audienceBranchIds: any
    audienceSuburbIds: any
    mainAreaIds: any
    boundaryIds: any
    lssIds: any
    campaignTypeValues: CampaignTypeValuesListModel[]
}

class CampaignTypeValuesListModel {
    constructor(campaignStageOneModel?: CampaignTypeValuesListModel) {
        this.marketingCampaignTypeId = campaignStageOneModel ? campaignStageOneModel.marketingCampaignTypeId == undefined ? '' : campaignStageOneModel.marketingCampaignTypeId : '';
        this.rValue = campaignStageOneModel ? campaignStageOneModel.rValue == undefined ? '' : campaignStageOneModel.rValue : '';
    }
    marketingCampaignTypeId?: string;
    rValue?: string;
}

export { CampaigncampaignStageOneModel,CampaignTypeValuesListModel }
