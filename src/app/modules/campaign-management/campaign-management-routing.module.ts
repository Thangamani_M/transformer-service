import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'campaign', loadChildren: () => import('../campaign-management/components/campaign/campaign.module').then(m => m.CampaignModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CampaignManagementRoutingModule { }
