import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { CampaignAddStageFourComponent } from './campaign-add-stage-four/campaign-add-stage-four.component';
import { CampaignAddStageOneComponent } from './campaign-add-stage-one/campaign-add-stage-one.component';
import { CampaignAddStageThreeComponent } from './campaign-add-stage-three/campaign-add-stage-three.component';
import { CampaignAddStageTwoComponent } from './campaign-add-stage-two/campaign-add-stage-two.component';
import { CampaignListComponent } from './campaign-list/campaign-list.component';
import { CampaignRoutingModule } from './campaign-routing.module';



@NgModule({
  declarations: [CampaignListComponent, CampaignAddStageOneComponent, CampaignAddStageTwoComponent, CampaignAddStageThreeComponent, CampaignAddStageFourComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    CampaignRoutingModule
  ]
})
export class CampaignModule { }
