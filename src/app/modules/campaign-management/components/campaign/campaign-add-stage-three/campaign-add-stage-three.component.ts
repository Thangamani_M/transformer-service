import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  clearFormControlValidators,
  ConfirmDialogModel,
  ConfirmDialogPopupComponent,
  CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator
} from '@app/shared';
import { CampaigncampaignStageThreeModel, ServiceListModel } from '@modules/campaign-management/models/campaign-stage-three.model';
import { loggedInUserData } from '@modules/others';
import { CampaignModuleApiSuffixModels } from '@modules/others/configuration/components/campaign-management/shared/enums/campaign.enum';
import { btnActionTypes, SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-campaign-add-stage-three',
  templateUrl: './campaign-add-stage-three.component.html',
  // styleUrls: ['./campaign-add-stage-three.component.scss']
})
export class CampaignAddStageThreeComponent implements OnInit {
  campaignStageThreeForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  marketingCampaignId: string;
  categoryList = [];
  durationList = [];
  serviceList = []
  showError = false;
  errorMessage: string;
  todayDate = new Date();
  formConfigs = formConfigs;
  checkMaxLengthAddress: boolean;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  @ViewChildren('input') rows: QueryList<any>;
  campaignDetails: any
  constructor(private rxjsService: RxjsService, private dialog: MatDialog,
    private crudService: CrudService, private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createStageOneForm();
    this.getCategory()
    if (this.marketingCampaignId) {
      this.getCampaignDetailsById();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.marketingCampaignId = response[1]?.id;
    });
  }

  createStageOneForm(): void {
    let campaigncampaignStageThreeModel = new CampaigncampaignStageThreeModel();
    this.campaignStageThreeForm = this.formBuilder.group({
      services: this.formBuilder.array([])
    });
    Object.keys(campaigncampaignStageThreeModel).forEach((key) => {
      this.campaignStageThreeForm.addControl(key, new FormControl(campaigncampaignStageThreeModel[key]));
    });
    this.campaignStageThreeForm.get('createdUserId').setValue(this.loggedInUserData.userId);
    this.campaignStageThreeForm.get('isDiscountOnMonthlyFee').valueChanges.subscribe((isDiscountOnMonthlyFee: boolean) => {
      if (isDiscountOnMonthlyFee) {
        this.campaignStageThreeForm = setRequiredValidator(this.campaignStageThreeForm, ['serviceDiscountValue']);
        this.campaignStageThreeForm.get('isSpecificServiceDiscount').setValue(false);
        this.getServicesListArray.clear();
      }
      else {
        this.campaignStageThreeForm = clearFormControlValidators(this.campaignStageThreeForm, ['serviceDiscountValue']);
        this.campaignStageThreeForm.get('serviceDiscountValue').setValue(null);
      }
    });
    this.campaignStageThreeForm.get('isSpecificServiceDiscount').valueChanges.subscribe((isSpecificServiceDiscount: boolean) => {
      if (isSpecificServiceDiscount) {
        this.campaignStageThreeForm.get('isDiscountOnMonthlyFee').setValue(false);
        this.getServicesListArray.clear();
        if (this.campaignDetails.services.length > 0) {
          this.campaignDetails.services.forEach((serviceListModel: ServiceListModel | any, i) => {
            serviceListModel.servicesList = [
              { id: serviceListModel.serviceId, displayName: serviceListModel.serviceName }
            ];
            this.getServicesListArray.push(this.createServiceListModel(serviceListModel));
          });
        } else {
          this.getServicesListArray.push(this.createServiceListModel());
        }
      }
    });
    this.campaignStageThreeForm.get('isServiceDiscountPercentage').valueChanges.subscribe((isServiceDiscountPercentage: boolean) => {
      if (isServiceDiscountPercentage) {
        this.campaignStageThreeForm.get('serviceDiscountValue').setValidators([Validators.required, Validators.min(0), Validators.max(100)]);
      } else {
        if (!this.campaignStageThreeForm.get('isDiscountOnMonthlyFee').value) {
          this.campaignStageThreeForm.get('serviceDiscountValue').setValidators(null);
        } else {
          this.campaignStageThreeForm.get('serviceDiscountValue').setValidators([Validators.required]);
        }
      }
      this.campaignStageThreeForm.get('serviceDiscountValue').updateValueAndValidity();
    });
  }

  get getServicesListArray(): FormArray {
    if (!this.campaignStageThreeForm) return;
    return this.campaignStageThreeForm.get("services") as FormArray;
  }

  createServiceListModel(serviceListModel?: ServiceListModel): FormGroup {
    let serviceListFormControlModel = new ServiceListModel(serviceListModel);
    let formControls = {};
    Object.keys(serviceListFormControlModel).forEach((key) => {
      if (key === 'marketingCampaignServiceId' || key == 'discountPercentage' || key == 'serviceNumber') {
        formControls[key] = [{ value: serviceListFormControlModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: serviceListFormControlModel[key], disabled: false }, [Validators.required]]
      }
    });
    let formListcontrols = this.formBuilder.group(formControls);
    formListcontrols.get('marketingCampaignId').setValue(this.marketingCampaignId);
    formListcontrols.get('createdUserId').setValue(this.loggedInUserData.userId);
    formListcontrols.get('isPercentage').valueChanges.subscribe((isPercentage: boolean) => {
      if (isPercentage) {
        formListcontrols.get('discountPercentage').setValidators([Validators.required, Validators.min(0), Validators.max(100)]);
        formListcontrols.get('discountAmount').clearValidators();
      } else {
        formListcontrols.get('discountAmount').setValidators([Validators.required]);
        formListcontrols.get('discountPercentage').clearValidators();
      }
      formListcontrols.get('discountAmount').updateValueAndValidity();
      formListcontrols.get('discountPercentage').updateValueAndValidity();
    });
    formListcontrols.get('serviceCategoryId').setValue(serviceListFormControlModel['serviceCategoryId']);
    formListcontrols.get('serviceCategoryId').valueChanges.subscribe((serviceCategoryId) => {
      if (serviceCategoryId) {
        this.crudService.dropdown(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.UX_SERVICES,
          prepareGetRequestHttpParams(null, null,
            { serviceCategoryId: serviceCategoryId })).subscribe((response) => {
              this.rxjsService.setGlobalLoaderProperty(false);
              if (response.isSuccess && response.statusCode == 200) {
                if (response.resources.length == 0) {
                  formListcontrols.get('serviceId').setValue('')
                } else {
                  let exist = response.resources.find(x => x.id == formListcontrols.get('serviceId').value)
                  if (!exist) {
                    formListcontrols.get('serviceId').setValue('')
                  }
                }
                formListcontrols.get('servicesList').setValue(response.resources)
              }
            });
      }
    });
    formListcontrols.get('serviceId').valueChanges.subscribe((serviceId) => {
      if (serviceId) {
        this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SERVICE, serviceId).subscribe((response) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.isSuccess && response.statusCode == 200) {
            formListcontrols.get('serviceNumber').setValue(response.resources.serviceNumber);
          }
        });
      }
    });
    return formListcontrols
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    });
  }

  addService(): void {
    if (this.getServicesListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    let serviceListModel = new ServiceListModel();
    this.getServicesListArray.insert(0, this.createServiceListModel(serviceListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeService(i: number): void {
    if (this.getServicesListArray.controls[i].value.marketingCampaignServiceId == '') {
      this.getServicesListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getServicesListArray.controls[i].value.marketingCampaignServiceId && this.getServicesListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.CAMPAIGN_SERVICES,
          undefined, prepareGetRequestHttpParams(null, null,
            {
              ids: this.getServicesListArray.controls[i].value.marketingCampaignServiceId,
              modifiedUserId: this.loggedInUserData.userId,
              isDeleted: true,
            })).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess) {
                this.getServicesListArray.removeAt(i);
              }
              if (this.getServicesListArray.length === 0) {
                this.addService();
              };
            });
      }
      else {
        this.getServicesListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  getCategory() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.UX_SERVICE_CATEGORY).subscribe((response) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode == 200) {
        this.categoryList = response.resources;
      }
    });
  }

  getCampaignDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.CAMPAIGN_SERVICES, this.marketingCampaignId, false, null, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200) {
          this.campaignDetails = response.resources;
          response.resources.startDate = new Date(response.resources.startDate);
          response.resources.endDate = new Date(response.resources.endDate);
          this.campaignStageThreeForm.patchValue(response.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onArrowClick(type: string) {
    if (type == 'previous') {
      this.router.navigate(['/campaign-management/campaign/add/stage-two'], { queryParams: { id: this.marketingCampaignId } });
    }
    else if (this.campaignStageThreeForm.valid) {
      this.router.navigate(['/campaign-management/campaign/add/stage-four'], { queryParams: { id: this.marketingCampaignId } });
    }
  }

  onSubmit(type?: string): void {
    if (this.campaignStageThreeForm.invalid) {
      return;
    }
    let formValue = this.campaignStageThreeForm.getRawValue();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.CAMPAIGN_SERVICES, formValue, 1);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.nextOrPrevStepper('next');
      }
    });
  }

  nextOrPrevStepper(type?: string): void {
    if (type === btnActionTypes.SUBMIT) {
      this.router.navigate(['/campaign-management/campaign/add/stage-four'], { queryParams: { id: this.marketingCampaignId } });
    }
    else if (type === btnActionTypes.NEXT) {
      this.router.navigate(['/campaign-management/campaign/add/stage-four'], { queryParams: { id: this.marketingCampaignId } });
    }
    else if (type === btnActionTypes.PREVIOUS) {
      this.router.navigate(['/campaign-management/campaign/add/stage-two'], { queryParams: { id: this.marketingCampaignId } });
    }
  }
}