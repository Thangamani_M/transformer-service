import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignAddStageThreeComponent } from './campaign-add-stage-three.component';

describe('CampaignAddStageThreeComponent', () => {
  let component: CampaignAddStageThreeComponent;
  let fixture: ComponentFixture<CampaignAddStageThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignAddStageThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignAddStageThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
