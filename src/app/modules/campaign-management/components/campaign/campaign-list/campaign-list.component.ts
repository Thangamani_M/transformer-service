import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, PrimengStatusConfirmDialogComponent, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { CampaignModuleApiSuffixModels } from '@modules/others/configuration/components/campaign-management/shared/enums/campaign.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-campaign-list',
  templateUrl: './campaign-list.component.html'
})


export class CampaignListComponent implements OnInit {
  pageLimit: any = [10, 25, 50, 75, 100];
  status: any = [];
  observableResponse;
  selectedTabIndex = 0;
  loggedInUserData: LoggedInUserModel;
  dataList: any;
  totalRecords: any;
  selectedColumns: any[];
  selectedRows: string[] = [];
  addConfirm: boolean = false
  selectedRow: any;
  loading = false;
  primengTableConfigProperties: any = {
    tableCaption: "Campaigns",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Campaigns Management ', relativeRouterUrl: '' }, { displayName: 'Campaigns', relativeRouterUrl: '' }, { displayName: 'Campaigns List', relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Campaigns List',
          dataKey: 'marketingCampaignTypeId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: true,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'campaignRefNo', header: 'Campaign Reference', width: '170px' },
            { field: 'regionName', header: 'Region', width: '200px' },
            { field: 'districtName', header: 'District', width: '200px' },
            { field: 'branchName', header: 'Branch', width: '200px', isparagraph: true },
            { field: 'campaignName', header: 'Campaign Name', width: '200px' },
            { field: 'endDate', header: 'Expiry Date', width: '150px' },
            { field: 'noOfEnquiries', header: 'Legends/Enquires Received', width: '200px' },
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: CampaignModuleApiSuffixModels.MARKETING_CAMPAIGNS,
          moduleName: ModulesBasedApiSuffix.SALES,
        }]
    }
  }
  constructor(private crudService: CrudService,
    private router: Router, private momentService: MomentService,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    public dialogService: DialogService,
  ) {
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ];
  }

  ngOnInit(): void {
    this.getbankstatementcutofftime();
    this.combineLatestNgrxStoreData();
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {

    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getbankstatementcutofftime(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1]["Campaigns"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  openAddEditPage(type: CrudType | string, data?: any | any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("campaign-management/campaign/add/stage-one");
            break;
        }
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["campaign-management/campaign/add/stage-one"], { queryParams: { id: data['marketingCampaignId'] } });
            break;
        }
    }

  }

  getbankstatementcutofftime(pageIndex?: string, pageSize?: string, otherParams?: object | any): void {
    this.loading = true;
    otherParams = { ...otherParams, isAll: true }
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      CampaignModuleApiSuffixModels.MARKETING_CAMPAIGNS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        response.resources.forEach(element => {
          element.endDate = this.momentService.toFormateType(element.endDate, 'DD-MM-YYYY')
        });
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }


}
