import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  clearFormControlValidators,
  CrudService, formConfigs, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator
} from '@app/shared';
import { CampaigncampaignStageTwoModel } from '@modules/campaign-management/models/campaign-stage-two.model';
import { loggedInUserData } from '@modules/others';
import { CampaignModuleApiSuffixModels } from '@modules/others/configuration/components/campaign-management/shared/enums/campaign.enum';
import { btnActionTypes } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-campaign-add-stage-two',
  templateUrl: './campaign-add-stage-two.component.html',
  // styleUrls: ['./campaign-add-stage-two.component.scss']
})
export class CampaignAddStageTwoComponent implements OnInit {
  campaignStageTwoForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  marketingCampaignId: string;
  monthList = [];
  durationList = [];
  periodList = []
  showError = false;
  errorMessage: string;
  todayDate = new Date();
  formConfigs = formConfigs;
  checkMaxLengthAddress: boolean;

  constructor(private rxjsService: RxjsService,
    private crudService: CrudService, private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createStageOneForm();
    this.getMonths()
    this.getPeriods()
    this.getDuration()
    if (this.marketingCampaignId) {
      this.getCampaignDetailsById();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.marketingCampaignId = response[1]?.id;
    });
  }

  createStageOneForm(): void {
    let campaigncampaignStageTwoModel = new CampaigncampaignStageTwoModel();
    this.campaignStageTwoForm = this.formBuilder.group({});
    Object.keys(campaigncampaignStageTwoModel).forEach((key) => {
      this.campaignStageTwoForm.addControl(key, new FormControl(campaigncampaignStageTwoModel[key]));
    });
    this.campaignStageTwoForm.get('createdUserId').setValue(this.loggedInUserData.userId)
    this.campaignStageTwoForm.get('isComplimentaryService').valueChanges.subscribe((isComplimentaryService: boolean) => {
      if (isComplimentaryService) {
        this.campaignStageTwoForm = setRequiredValidator(this.campaignStageTwoForm, ['complimentaryMonthId']);
        this.campaignStageTwoForm.get('complimentaryMonthId').setValue('');
      }
      else {
        this.campaignStageTwoForm = clearFormControlValidators(this.campaignStageTwoForm, ['complimentaryMonthId']);
        this.campaignStageTwoForm.get('complimentaryMonthId').setValue(null);
      }
    })
    this.campaignStageTwoForm.get('isWaiverAnnualNetworkFee').valueChanges.subscribe((isWaiverAnnualNetworkFee: boolean) => {
      if (isWaiverAnnualNetworkFee) {
        this.campaignStageTwoForm = setRequiredValidator(this.campaignStageTwoForm, ['annualNetworkFeePeriodId']);
        this.campaignStageTwoForm.get('annualNetworkFeePeriodId').setValue('');
      }
      else {
        this.campaignStageTwoForm = clearFormControlValidators(this.campaignStageTwoForm, ['annualNetworkFeePeriodId']);
        this.campaignStageTwoForm.get('annualNetworkFeePeriodId').setValue(null);
      }
    })
    this.campaignStageTwoForm.get('isFixedContractFee').valueChanges.subscribe((isFixedContractFee: boolean) => {
      if (isFixedContractFee) {
        this.campaignStageTwoForm = setRequiredValidator(this.campaignStageTwoForm, ['fixedContractFeePeriodId']);
        this.campaignStageTwoForm.get('fixedContractFeePeriodId').setValue('');
      }
      else {
        this.campaignStageTwoForm = clearFormControlValidators(this.campaignStageTwoForm, ['fixedContractFeePeriodId']);
        this.campaignStageTwoForm.get('fixedContractFeePeriodId').setValue(null);
      }
    });
  }

  getMonths() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.UX_COMPLIMENTARY_MONTHS,
      prepareGetRequestHttpParams(null, null,
        { CountryId: formConfigs.countryId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.monthList = response.resources;
          }
        });
  }

  getPeriods() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.UX_CONTRACT_PERIODS,
      prepareGetRequestHttpParams(null, null,
        { CountryId: formConfigs.countryId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.periodList = response.resources;
          }
        });
  }

  getDuration() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.UX_CONTRACT_FEE_PERIODS,
      prepareGetRequestHttpParams(null, null,
        { CountryId: formConfigs.countryId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.durationList = response.resources;
          }
        });
  }

  getCampaignDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.CAMPAIGN_GENERAL_DISCOUNT, this.marketingCampaignId, false, null, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200) {
          response.resources.startDate = new Date(response.resources.startDate);
          response.resources.endDate = new Date(response.resources.endDate);
          this.campaignStageTwoForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onArrowClick(type: string) {
    if (type == 'previous') {
      this.router.navigate(['/campaign-management/campaign/add/stage-one'], { queryParams: { id: this.marketingCampaignId } });
    }
    else if (this.campaignStageTwoForm.valid) {
      this.router.navigate(['/campaign-management/campaign/add/stage-three'], { queryParams: { id: this.marketingCampaignId } });
    }
  }

  onSubmit(type?: string): void {
    if (this.campaignStageTwoForm.invalid) {
      return;
    }
    let formValue = this.campaignStageTwoForm.getRawValue()
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.CAMPAIGN_GENERAL_DISCOUNT, formValue, 1);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.nextOrPrevStepper('next');
      }
    });
  }

  nextOrPrevStepper(type?: string): void {
    if (type === btnActionTypes.SUBMIT) {
      this.router.navigate(['/campaign-management/campaign/add/stage-three'], { queryParams: { id: this.marketingCampaignId } });
    }
    else if (type === btnActionTypes.NEXT) {
      this.router.navigate(['/campaign-management/campaign/add/stage-three'], { queryParams: { id: this.marketingCampaignId } });
    }
    else if (type === btnActionTypes.PREVIOUS) {
      this.router.navigate(['/campaign-management/campaign/add/stage-one'], { queryParams: { id: this.marketingCampaignId } });
    }
  }
}
