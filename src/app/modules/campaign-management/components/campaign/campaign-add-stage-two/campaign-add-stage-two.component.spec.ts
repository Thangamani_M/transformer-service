import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignAddStageTwoComponent } from './campaign-add-stage-two.component';

describe('CampaignAddStageTwoComponent', () => {
  let component: CampaignAddStageTwoComponent;
  let fixture: ComponentFixture<CampaignAddStageTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignAddStageTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignAddStageTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
