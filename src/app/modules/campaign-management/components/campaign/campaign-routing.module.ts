import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CampaignAddStageFourComponent } from './campaign-add-stage-four/campaign-add-stage-four.component';
import { CampaignAddStageOneComponent } from './campaign-add-stage-one/campaign-add-stage-one.component';
import { CampaignAddStageThreeComponent } from './campaign-add-stage-three/campaign-add-stage-three.component';
import { CampaignAddStageTwoComponent } from './campaign-add-stage-two/campaign-add-stage-two.component';
import { CampaignListComponent } from './campaign-list/campaign-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  // { path: '', redirectTo: 'list', canActivate: [AuthGuard] },
  {
    path: '', component: CampaignListComponent, canActivate: [AuthGuard], data: { title: 'Campaign List' }
  },
  {
    path: 'add/stage-one', component: CampaignAddStageOneComponent, canActivate: [AuthGuard], data: { title: 'Campaign Add Stage One' }
  },
  {
    path: 'add/stage-two', component: CampaignAddStageTwoComponent, canActivate: [AuthGuard], data: { title: 'Campaign Add Stage Two' }
  },
  {
    path: 'add/stage-three', component: CampaignAddStageThreeComponent, canActivate: [AuthGuard], data: { title: 'Campaign Add Stage Three' }
  },
  {
    path: 'add/stage-four', component: CampaignAddStageFourComponent, canActivate: [AuthGuard], data: { title: 'Campaign Add Stage Four' }
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class CampaignRoutingModule { }
