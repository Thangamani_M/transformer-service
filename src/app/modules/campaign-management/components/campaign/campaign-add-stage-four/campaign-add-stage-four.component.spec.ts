import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignAddStageFourComponent } from './campaign-add-stage-four.component';

describe('CampaignAddStageFourComponent', () => {
  let component: CampaignAddStageFourComponent;
  let fixture: ComponentFixture<CampaignAddStageFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignAddStageFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignAddStageFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
