import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  clearFormControlValidators,
  ConfirmDialogModel,
  ConfirmDialogPopupComponent,
  CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { CampaigncampaignStageFourModel, ItemListModel } from '@modules/campaign-management/models/campaign-stage-four.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { CampaignModuleApiSuffixModels } from '@modules/others/configuration/components/campaign-management/shared/enums/campaign.enum';
import { btnActionTypes } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-campaign-add-stage-four',
  templateUrl: './campaign-add-stage-four.component.html',
  // styleUrls: ['./campaign-add-stage-four.component.scss']
})
export class CampaignAddStageFourComponent implements OnInit {
  campaignStageFourForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  marketingCampaignId: string;
  itemList = []
  showError = false;
  errorMessage: string;
  todayDate = new Date();
  formConfigs = formConfigs;
  checkMaxLengthAddress: boolean;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  @ViewChildren('input') rows: QueryList<any>;
  campaignDetails: any

  constructor(private rxjsService: RxjsService, private snackbarService: SnackbarService, private dialog: MatDialog,
    private crudService: CrudService, private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createStageOneForm();
    if (this.marketingCampaignId) {
      this.getCampaignDetailsById();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.marketingCampaignId = response[1]?.id;
    });
  }

  getLeadSources(event, i) {
    let otherParams = {}
    otherParams['itemCode'] = event.query
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_ITEM, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.resources) {
          if (response.resources.length > 0) {
            this.itemList = response?.resources;
          } else {
            this.getItemListArray.controls[i].get('itemId').setValue(null)
            this.snackbarService.openSnackbar(response.message ? response.message : 'Data not found', ResponseMessageTypes.WARNING);
            this.itemList = [];
          }
        } else {
          this.getItemListArray.controls[i].get('itemId').setValue(null)
          this.snackbarService.openSnackbar(response.message ? response.message : 'Data not found', ResponseMessageTypes.WARNING);
          this.itemList = [];
        }
      });
  }

  onSelectSource(event, i) {
    this.getItemListArray.controls[i].get('itemName').setValue(event.displayName);
  }

  createStageOneForm(): void {
    let campaigncampaignStageFourModel = new CampaigncampaignStageFourModel();
    this.campaignStageFourForm = this.formBuilder.group({
      items: this.formBuilder.array([])
    });
    Object.keys(campaigncampaignStageFourModel).forEach((key) => {
      this.campaignStageFourForm.addControl(key, new FormControl(campaigncampaignStageFourModel[key]));
    });
    this.campaignStageFourForm.get('createdUserId').setValue(this.loggedInUserData.userId);
    this.campaignStageFourForm.get('isOverallDiscount').valueChanges.subscribe((isOverallDiscount: boolean) => {
      if (isOverallDiscount) {
        this.campaignStageFourForm = setRequiredValidator(this.campaignStageFourForm, ['itemDiscountValue']);
        this.campaignStageFourForm.get('isSpecificItemDiscount').setValue(false);
        this.getItemListArray.clear();
      }
      else {
        this.campaignStageFourForm = clearFormControlValidators(this.campaignStageFourForm, ['itemDiscountValue']);
        this.campaignStageFourForm.get('itemDiscountValue').setValue(null);
      }
    })
    this.campaignStageFourForm.get('isSpecificItemDiscount').valueChanges.subscribe((isSpecificItemDiscount: boolean) => {
      if (isSpecificItemDiscount) {
        this.campaignStageFourForm.get('isOverallDiscount').setValue(false);
        this.getItemListArray.clear();
        if (this.campaignDetails.items.length > 0) {
          this.campaignDetails.items.forEach((itemListModel: ItemListModel, i) => {
            itemListModel.itemId = { id: itemListModel.itemId, displayName: itemListModel.itemName };
            this.getItemListArray.push(this.createItemListModel(itemListModel));
          });
        } else {
          this.getItemListArray.push(this.createItemListModel());
        }
      }
    });
    this.campaignStageFourForm.get('isInstallationDiscountPercentage').valueChanges.subscribe((isInstallationDiscountPercentage: boolean) => {
      if (isInstallationDiscountPercentage) {
        this.campaignStageFourForm.get('itemDiscountValue').setValidators([Validators.required, Validators.min(0), Validators.max(100)])
      } else {
        if (!this.campaignStageFourForm.get('isOverallDiscount').value) {
          this.campaignStageFourForm.get('itemDiscountValue').setValidators(null);
        } else {
          this.campaignStageFourForm.get('itemDiscountValue').setValidators([Validators.required]);
        }
      }
      this.campaignStageFourForm.get('itemDiscountValue').updateValueAndValidity();
    });
  }

  get getItemListArray(): FormArray {
    if (!this.campaignStageFourForm) return;
    return this.campaignStageFourForm.get("items") as FormArray;
  }

  createItemListModel(itemListModel?: ItemListModel): FormGroup {
    let serviceListFormControlModel = new ItemListModel(itemListModel);
    let formControls = {};
    Object.keys(serviceListFormControlModel).forEach((key) => {
      if (key === 'marketingCampaignItemId' || key === 'itemId') {
        formControls[key] = [{ value: serviceListFormControlModel[key], disabled: false }];
      } else {
        formControls[key] = [{ value: serviceListFormControlModel[key], disabled: false }, [Validators.required]];
      }
    });
    let formListcontrols = this.formBuilder.group(formControls);
    formListcontrols.get('marketingCampaignId').setValue(this.marketingCampaignId);
    formListcontrols.get('createdUserId').setValue(this.loggedInUserData.userId);
    return formListcontrols;
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    });
  }

  addService(): void {
    if (this.getItemListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    let itemListModel = new ItemListModel();
    this.getItemListArray.insert(0, this.createItemListModel(itemListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeService(i: number): void {
    if (this.getItemListArray.controls[i].value.marketingCampaignItemId == '') {
      this.getItemListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getItemListArray.controls[i].value.marketingCampaignItemId && this.getItemListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.CAMPAIGN_ITEMS,
          undefined, prepareGetRequestHttpParams(null, null,
            {
              ids: this.getItemListArray.controls[i].value.marketingCampaignItemId,
              modifiedUserId: this.loggedInUserData.userId,
              isDeleted: true,
            })).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess) {
                this.getItemListArray.removeAt(i);
              }
              if (this.getItemListArray.length === 0) {
                this.addService();
              };
            });
      }
      else {
        this.getItemListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  getCampaignDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.CAMPAIGN_ITEMS, this.marketingCampaignId, false, null, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200) {
          this.campaignDetails = response.resources;
          response.resources.startDate = new Date(response.resources.startDate);
          response.resources.endDate = new Date(response.resources.endDate);
          this.campaignStageFourForm.patchValue(response.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onArrowClick(type: string) {
    if (type == 'previous') {
      this.router.navigate(['/campaign-management/campaign/add/stage-three'], { queryParams: { id: this.marketingCampaignId } });
    }
    else if (this.campaignStageFourForm.valid) {
      this.router.navigate(['/campaign-management/campaign/add/stage-five'], { queryParams: { id: this.marketingCampaignId } });
    }
  }

  public checkValidity(): void {
    Object.keys(this.campaignStageFourForm.controls).forEach((key) => {
      this.campaignStageFourForm.controls[key].markAsDirty();
    });
  }

  onSubmit(type?: string): void {
    if (this.campaignStageFourForm.invalid) {
      this.checkValidity();
      return
    }
    let formValue = this.campaignStageFourForm.getRawValue();
    formValue.items.forEach(element => {
      element.itemId = element.itemId.id;
    });
    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.CAMPAIGN_ITEMS, formValue, 1);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.router.navigate(['/campaign-management/campaign']);
      }
    });
  }

  nextOrPrevStepper(type?: string): void {
    if (type === btnActionTypes.SUBMIT) {
      this.router.navigate(['/campaign-management/campaign/add/stage-five'], { queryParams: { id: this.marketingCampaignId } });
    }
    else if (type === btnActionTypes.NEXT) {
      this.router.navigate(['/campaign-management/campaign/add/stage-five'], { queryParams: { id: this.marketingCampaignId } });
    }
    else if (type === btnActionTypes.PREVIOUS) {
      this.router.navigate(['/campaign-management/campaign/add/stage-three'], { queryParams: { id: this.marketingCampaignId } });
    }
  }
}