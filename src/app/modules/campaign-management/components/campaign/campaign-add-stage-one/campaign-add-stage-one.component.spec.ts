import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignAddStageOneComponent } from './campaign-add-stage-one.component';

describe('CampaignAddStageOneComponent', () => {
  let component: CampaignAddStageOneComponent;
  let fixture: ComponentFixture<CampaignAddStageOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignAddStageOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignAddStageOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
