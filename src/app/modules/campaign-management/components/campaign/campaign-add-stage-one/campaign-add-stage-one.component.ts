import { DatePipe } from '@angular/common';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, formConfigs, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CampaigncampaignStageOneModel, CampaignTypeValuesListModel } from '@modules/campaign-management/models/campaign-stage-one.model';
import { loggedInUserData } from '@modules/others';
import { CampaignModuleApiSuffixModels } from '@modules/others/configuration/components/campaign-management/shared/enums/campaign.enum';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
@Component({
  selector: 'app-campaign-add-stage-one',
  templateUrl: './campaign-add-stage-one.component.html',
  // styleUrls: ['./campaign-add-stage-one.component.scss']
})
export class CampaignAddStageOneComponent implements OnInit {
  campaignStageOneForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  marketingCampaignId: string;
  regionList = [];
  divisionList = []
  districtList = [];
  branchList = [];
  sourceList = [];
  campaignList = [];
  sourceTypeList = []
  dataBaseList = []
  ageList = []
  genderList = []
  audienceDivisionList = []
  audienceDistrictList = [];
  audienceBranchList = [];
  suburbsList = []
  areaList = []
  vehicleList = []
  lssSchemeList = []
  showError = false;
  errorMessage: string;
  todayDate = new Date();
  todayStartDate = new Date()
  formConfigs = formConfigs;
  checkMaxLengthAddress: boolean;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  @ViewChildren('input') rows: QueryList<any>;

  constructor(private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private crudService: CrudService, private momentService: MomentService, private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private router: Router, private formBuilder: FormBuilder, private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createStageOneForm();
    this.getRegions();
    this.getCompaignTypes();
    this.getSourceTypesList();
    this.getDataBaseList();
    this.getAgeList();
    this.getGenderList();
    this.getAreaList();
    this.getLssSchemeList();
    if (this.marketingCampaignId) {
      this.getCampaignDetailsById();
    } else {
      this.getCampaignTypeValuesListArray.push(this.createCampaignTypeValuesListModel());
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.marketingCampaignId = response[1]?.id;
    });
  }

  getCampaignDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.MARKETING_CAMPAIGNS, this.marketingCampaignId, false, null, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200) {
          response.resources.startDate = new Date(response.resources.startDate);
          response.resources.endDate = new Date(response.resources.endDate);
          response.resources.districtIds = this.setItems(response.resources.districtIds);
          response.resources.marketingCampaignTypeIds = this.setItems(response.resources.marketingCampaignTypeIds);
          response.resources.regionIds = this.setItems(response.resources.regionIds);
          response.resources.branchIds = this.setItems(response.resources.branchIds);
          response.resources.requestorRegionIds = this.setItems(response.resources.requestorRegionIds);
          response.resources.genderIds = this.setItems(response.resources.genderIds);
          response.resources.audienceRegionIds = this.setItems(response.resources.audienceRegionIds);
          response.resources.audienceDivisionIds = this.setItems(response.resources.audienceDivisionIds);
          response.resources.audienceDistrictIds = this.setItems(response.resources.audienceDistrictIds);
          response.resources.audienceBranchIds = this.setItems(response.resources.audienceBranchIds);
          let suburbIds = [];
          response.resources.audienceSuburb.forEach(element => {
            suburbIds.push({ id: element?.id, displayName: element?.displayName });
          });
          response.resources.audienceSuburbIds = suburbIds;
          let mainAreaIds = [];
          response.resources.mainAreaIds.forEach(element => {
            mainAreaIds.push({ mainAreaId: element });
          });
          response.resources.mainAreaIds = mainAreaIds;
          response.resources.boundaryIds = this.setItems(response.resources.boundaryIds);
          response.resources.lssIds = this.setItems(response.resources.lssIds);
          let sourceData = { id: response.resources.sourceId, displayName: response.resources.sourceName };
          response.resources.sourceId = sourceData;
          this.campaignStageOneForm.patchValue(response.resources);
          if (response.resources.campaignTypeValues.length > 0) {
            response.resources.campaignTypeValues.forEach((element: CampaignTypeValuesListModel) => {
              this.getCampaignTypeValuesListArray.push(this.createCampaignTypeValuesListModel(element));
            })
          } else {
            this.getCampaignTypeValuesListArray.push(this.createCampaignTypeValuesListModel());
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  setItems(itemList) {
    let itesmIds = [];
    itemList.forEach(element => {
      itesmIds.push({ id: element });
    });
    return itesmIds;
  }

  setItemsPost(itemList) {
    let itesmIds = [];
    itemList.forEach(element => {
      itesmIds.push(element.id);
    });
    return itesmIds;
  }

  getRegions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null,
        { CountryId: formConfigs.countryId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.regionList = response.resources;
          }
        });
  }

  getCompaignTypes() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.UX_MARKETING_CAMPAIGN_TYPE, null, false)
      .subscribe((response: IApplicationResponse) => {
        this.branchList = [];
        if (response && response.resources && response.isSuccess) {
          this.campaignList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSourceTypesList() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SOURCE_TYPES, null, false)
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.sourceTypeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDataBaseList() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_MARKETING_MAINTAINCE_DATA, null, false)
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.dataBaseList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getAgeList() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.UX_AGE, null, false)
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.ageList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getGenderList() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CampaignModuleApiSuffixModels.UX_GENDER, null, false)
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.genderList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getAreaList() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_MAIN_AREA, null, false)
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.areaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getLssSchemeList() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.UX_LSS, null, false)
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.lssSchemeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onFromAgeSelect(event) {
    this.campaignStageOneForm.get('toAgeId').setValue(null);
  }

  onToAgeSelect(event) {
    let fromAge = this.ageList.find(x => x.id == this.campaignStageOneForm.get('fromAgeId').value);
    let toAge = this.ageList.find(x => x.id == event.target.value);
    if (fromAge) {
      if (fromAge.displayName > toAge.displayName) {
        this.campaignStageOneForm.get('toAgeId').setValue(null);
        this.snackbarService.openSnackbar('To Age should be grater then from Age', ResponseMessageTypes.WARNING);
      }
    }
  }

  getLeadSources(event): void {
    if (!this.campaignStageOneForm.get('sourceGroupId').value) {
      this.snackbarService.openSnackbar('Please select Source Type', ResponseMessageTypes.WARNING);
      this.campaignStageOneForm.get('sourceId').setValue(null);
      this.sourceList = [];
      return;
    }
    let otherParams = {};
    otherParams['SourceName'] = event.query;
    let SourceGroupName = this.sourceTypeList.find(x => x.sourceGroupId == this.campaignStageOneForm.get('sourceGroupId').value);
    if (SourceGroupName) {
      otherParams['SourceGroupName'] = SourceGroupName.sourceGroupName;
    } else {
      this.snackbarService.openSnackbar('Please select Source Type', ResponseMessageTypes.WARNING);
      return;
    }
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_SOURCE_TYPES, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.resources) {
          if (response.resources.length > 0) {
            this.sourceList = [];
            response.resources.forEach(element => {
              if (element.sources) {
                element.sources.forEach(element1 => {
                  this.sourceList.push(element1);
                });
              }
            });
            this.sourceList = [...this.sourceList];

          } else {
            this.campaignStageOneForm.get('sourceId').setValue(null);
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.WARNING);
            this.sourceList = [];
          }
        } else {
          this.sourceList = [];
        }
      });
  }

  onSelectSource(event) {
    this.campaignStageOneForm.get('sourceName').setValue(null)
  }

  getSuburbs(event): void {
    let otherParams = {};
    otherParams['SearchText'] = event.query;
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      CampaignModuleApiSuffixModels.UX_SUBURBS_AUTOCOMPLETE, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.resources) {
          this.suburbsList = response?.resources;
        } else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.WARNING);
          this.suburbsList = [];
        }
      });
  }

  createStageOneForm(): void {
    let campaigncampaignStageOneModel = new CampaigncampaignStageOneModel();
    this.campaignStageOneForm = this.formBuilder.group({
      campaignTypeValues: this.formBuilder.array([])
    });
    Object.keys(campaigncampaignStageOneModel).forEach((key) => {
      if (key == 'sourceName' || key == 'marketingCampaignId' || key == 'audienceRegionIds' || key == 'audienceDivisionIds' || key == 'audienceDistrictIds' || key == 'audienceBranchIds' || key == 'audienceSuburbIds' || key == 'mainAreaIds' || key == 'boundaryIds' || key == 'lssIds' || key == 'campaignTypeValues' || key == 'fromAgeId' || key == 'toAgeId' || key == 'genderIds') {
        this.campaignStageOneForm.addControl(key, new FormControl(campaigncampaignStageOneModel[key]));
      } else {
        this.campaignStageOneForm.addControl(key, new FormControl(campaigncampaignStageOneModel[key], Validators.required));
      }
    });
    this.campaignStageOneForm.get('createdUserId').setValue(this.loggedInUserData.userId);
    this.campaignStageOneForm.get('startDate').valueChanges.subscribe(val => {
      if (val) {
        this.campaignStageOneForm.get('endDate').setValue(null);
        let date = new Date(val);
        date.setDate(date.getDate() + 1);
        this.todayStartDate = date;
      }
    });
    this.campaignStageOneForm.get('sourceId').valueChanges.subscribe(val => {
      if (val) {
        if (typeof (val) == 'string') {
          this.campaignStageOneForm.get('sourceName').setValue(this.campaignStageOneForm.get('sourceId').value);
        } else {
          this.campaignStageOneForm.get('sourceName').setValue(null);
        }
      }
    });
    this.campaignStageOneForm.get('regionIds').valueChanges.subscribe(regionId => {
      if (!regionId) return;
      if (regionId.length == 0) return;
      let regionIds = []
      regionId.forEach(element => {
        regionIds.push(element.id)
      });
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIN_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { ids: regionIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          this.districtList = [];
          if (response && response.resources && response.isSuccess) {
            this.districtList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.campaignStageOneForm.get('districtIds').valueChanges.subscribe(districtId => {
      if (!districtId) return;
      if (districtId.length == 0) return;
      let districtIds = [];
      districtId.forEach(element => {
        districtIds.push(element.id);
      });
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null,
          { districtId: districtIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          this.branchList = [];
          if (response && response.resources && response.isSuccess) {
            this.branchList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.campaignStageOneForm.get('audienceRegionIds').valueChanges.subscribe(regionId => {
      if (!regionId) return;
      if (regionId.length == 0) return;
      let regionIds = [];
      regionId.forEach(element => {
        regionIds.push(element.id);
      });
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { regionId: regionIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.audienceDivisionList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    });
    this.campaignStageOneForm.get('audienceDivisionIds').valueChanges.subscribe(divisionId => {
      if (!divisionId) return;
      if (divisionId.length == 0) return;
      let divisionIds = [];
      divisionId.forEach(element => {
        divisionIds.push(element.id);
      });
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionId: divisionIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          this.audienceDistrictList = [];
          if (response && response.resources && response.isSuccess) {
            this.audienceDistrictList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    });
    this.campaignStageOneForm.get('audienceDistrictIds').valueChanges.subscribe(districtId => {
      if (!districtId) return;
      if (districtId.length == 0) return;
      let districtIds = [];
      districtId.forEach(element => {
        districtIds.push(element.id);
      });
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null,
          { districtId: districtIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          this.audienceBranchList = [];
          if (response && response.resources && response.isSuccess) {
            this.audienceBranchList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.campaignStageOneForm.get('mainAreaIds').valueChanges.subscribe(mainAreaId => {
      if (!mainAreaId) return;
      if (mainAreaId.length == 0) return;
      let mainAreaIds = []
      mainAreaId.forEach(element => {
        mainAreaIds.push(element.mainAreaId)
      });
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SUB_AREA, null, false,
        prepareGetRequestHttpParams(null, null,
          { mainAreaIds: mainAreaIds.toString() }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.vehicleList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.campaignStageOneForm.get('budget').valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged())
      .subscribe();
    if (this.marketingCampaignId) {
      this.campaignStageOneForm.disable();
    } else {
      this.campaignStageOneForm.enable();
    }
  }

  get getCampaignTypeValuesListArray(): FormArray {
    if (!this.campaignStageOneForm) return;
    return this.campaignStageOneForm.get("campaignTypeValues") as FormArray;
  }

  createCampaignTypeValuesListModel(campaignTypeValuesListModel?: CampaignTypeValuesListModel): FormGroup {
    let campaignTypeValuesListModelFormControlModel = new CampaignTypeValuesListModel(campaignTypeValuesListModel);
    let formControls = {};
    Object.keys(campaignTypeValuesListModelFormControlModel).forEach((key) => {
      formControls[key] = [{ value: campaignTypeValuesListModelFormControlModel[key], disabled: false }, [Validators.required]]
    });
    let formControlGroup = this.formBuilder.group(formControls)
    formControlGroup.get('rValue').valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          if (!obj) return of()
          if (!this.campaignStageOneForm.get('budget').value) {
            formControlGroup.get('rValue').setValue(null, { emitEvent: false });
          }
          let total = this.calculateTotal();
          this.campaignStageOneForm.get('totalBudget').setValue(total);
          return of(this.calculateTotal());
        })
      ).subscribe();
    return formControlGroup;
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    });
  }

  addCmcSmsGroupEmployee(): void {
    if (this.getCampaignTypeValuesListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.getCampaignTypeValuesListArray.insert(0, this.createCampaignTypeValuesListModel());
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.marketingCampaignId) {
      this.getCampaignTypeValuesListArray.disable();
    } else {
      this.getCampaignTypeValuesListArray.enable();
    }
  }

  removeCmcSmsEmployee(i: number): void {
    this.getCampaignTypeValuesListArray.removeAt(i);
    let total = this.calculateTotal();
    this.campaignStageOneForm.get('totalBudget').setValue(total);
    return;
  }

  calculateTotal() {
    if (!this.campaignStageOneForm.get('budget').value) {
      return this.snackbarService.openSnackbar('Budget is required', ResponseMessageTypes.WARNING);
    }
    let total = 0;
    this.getCampaignTypeValuesListArray.value.forEach(element => {
      element.rValue = element.rValue ? element.rValue : 0
      total += parseInt(element.rValue);
    });
    total = total ? total : 0;
    let gTotal = total;
    return gTotal.toFixed(2);
  }

  public checkValidity(): void {
    Object.keys(this.campaignStageOneForm.controls).forEach((key) => {
      this.campaignStageOneForm.controls[key].markAsDirty();
    });
  }

  onSubmit(): void {
    if (this.campaignStageOneForm.invalid) {
      return;
    }
    let formValue = this.campaignStageOneForm.getRawValue();
    if (Number(formValue.budget) < Number(formValue.totalBudget)) {
      this.snackbarService.openSnackbar('Total rvalue and budget should be equal', ResponseMessageTypes.WARNING);
      return;
    }
    formValue.startDate = this.momentService.toFormateType(formValue.startDate, 'YYYY-MM-DD');
    formValue.endDate = this.momentService.toFormateType(formValue.endDate, 'YYYY-MM-DD');
    formValue.sourceId = !formValue.sourceName ? formValue.sourceId?.id : null;
    formValue.budget = Number(formValue.budget);
    formValue.branchIds = this.setItemsPost(formValue.branchIds);
    formValue.districtIds = this.setItemsPost(formValue.districtIds);
    formValue.marketingCampaignTypeIds = this.setItemsPost(formValue.marketingCampaignTypeIds);
    formValue.regionIds = this.setItemsPost(formValue.regionIds);
    formValue.requestorRegionIds = this.setItemsPost(formValue.requestorRegionIds);
    formValue.genderIds = this.setItemsPost(formValue.genderIds);
    formValue.audienceRegionIds = this.setItemsPost(formValue.audienceRegionIds);
    formValue.audienceDivisionIds = this.setItemsPost(formValue.audienceDivisionIds);
    formValue.audienceDistrictIds = this.setItemsPost(formValue.audienceDistrictIds);
    formValue.audienceBranchIds = this.setItemsPost(formValue.audienceBranchIds);
    formValue.audienceSuburbIds = this.setItemsPost(formValue.audienceSuburbIds);
    let mainAreaIds = [];
    formValue.mainAreaIds.forEach(element => {
      mainAreaIds.push(element.mainAreaId);
    });
    formValue.mainAreaIds = mainAreaIds;
    formValue.boundaryIds = this.setItemsPost(formValue.boundaryIds);
    formValue.lssIds = this.setItemsPost(formValue.lssIds);
    this.crudService.create(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.MARKETING_CAMPAIGNS, formValue).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        if (!this.campaignStageOneForm.get('marketingCampaignId').value) {
          this.campaignStageOneForm.get('marketingCampaignId').setValue(response.resources);
        }
        this.marketingCampaignId = response.resources;
      }
    });
  }

  onArrowClick(type: string) {
    if (type == 'previous') {
    }
    else {
      if (this.marketingCampaignId || this.campaignStageOneForm.get('marketingCampaignId').value) {
        this.router.navigate(['/campaign-management/campaign/add/stage-two'], { queryParams: { id: this.marketingCampaignId } });
      }
    }
  }
}
