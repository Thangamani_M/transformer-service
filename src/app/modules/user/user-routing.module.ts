import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'cities', loadChildren: () => import('../user/components/city/city.module').then(m => m.CityModule), data: { preload: true }
    },
    {
        path: 'departments', loadChildren: () => import('../user/components/department/department.module').then(m => m.DepartmentModule), data: { preload: true }
    },
    {
        path: 'features', loadChildren: () => import('../user/components/feature/feature.module').then(m => m.FeatureModule), data: { preload: true }
    },
    {
        path: 'modules', loadChildren: () => import('../user/components/module/module.module').then(m => m.ModuleBasedModule), data: { preload: true }
    },
    {
        path: 'navigations', loadChildren: () => import('../user/components/navigation/navigation.module').then(m => m.NavigationModule), data: { preload: true }
    },
    {
        path: 'provinces', loadChildren: () => import('../user/components/province/province.module').then(m => m.ProvinceModule), data: { preload: true }
    },
    {
        path: 'roles', loadChildren: () => import('../user/components/role/role.module').then(m => m.RoleModule), data: { preload: true }
    },
    {
        path: 'leave-user', loadChildren: () => import('../user/components/special-permission/special-permission.module').then(m => m.SpecialPermissionModule), data: { preload: true }
    },
    {
        path: 'sub-urbans', loadChildren: () => import('../user/components/suburb/suburb.module').then(m => m.SubUrbModule), data: { preload: true }
    },
    {
        path: 'employee', loadChildren: () => import('../user/components/employee/employee.module').then(m => m.EmployeeModule), data: { preload: true }
    },
    {
        path: 'divisions', loadChildren: () => import('../user/components/division/division.module').then(m => m.DivisionModule), data: { preload: true }
    },
    {
        path: 'boundary-management', loadChildren: () => import('../user/components/boundary-management/boundary.module').then(m => m.BoundaryModule), data: { preload: true }
    },
    {
        path: 'districts', loadChildren: () => import('../user/components/district/district.module').then(m => m.DistrictModule), data: { preload: true }
    },
    {
        path: 'branches', loadChildren: () => import('../user/components/branch/branch.module').then(m => m.BranchModule), data: { preload: true }
    },
    {
        path: 'sub-branches', loadChildren: () => import('../user/components/sub-branch/sub-branch.module').then(m => m.SubBranchModule), data: { preload: true }
    },
    {
        path: 'template', loadChildren: () => import('./components/dynamic-template/template.module').then(m => m.TemplateModule), data: { preload: true }
    },
    {
        path: 'user-group', loadChildren: () => import('./components/user-group-mapping/user-group-mapping.module').then(m => m.UserGroupMappingModule), data: { preload: true }
    },
    {
        path: 'employee-registration', loadChildren: () => import('./components/in-employee-registration/in-employee-registration.module').then(m => m.InEmployeeRegistrationModule), data: { preload: true }
    },
    {
        path: 'technicial-staff-registration', loadChildren: () => import('./components/technician-user-type-registration/technician-user-type-registration.module').then(m => m.TechnicianUserTypeRegistrationModule), data: { preload: true }
    },
    {
        path: 'custom-category-config-dashboard', loadChildren: () => import('./components/custom-category-config-dashboard/custom-category-config-dashboard.module').then(m => m.CustomCategoryConfigDashboardModule), data: { preload: true }
    },
    {
        path: 'custom-category-config-dashboard/custom-category-group-config', loadChildren: () => import('./components/custom-category-group-config/custom-category-group-config.module').then(m => m.CustomCategoryGroupConfigModule), data: { preload: true }
    },
    {
        path: 'custom-category-config-dashboard/custom-category-config-list', loadChildren: () => import('./components/custom-category-config-list/custom-category-config-list.module').then(m => m.CustomCategoryConfigListModule), data: { preload: true }
    },
    {
        path: 'custom-category-config-dashboard/custom-category-config', loadChildren: () => import('./components/custom-category-config/custom-category-config.module').then(m => m.CustomCategoryConfigModule), data: { preload: true }
    },
    {
        path: 'menu-configuration', loadChildren: () => import('./components/menu-configuration/menu-configuration.module').then(m => m.MenuConfigurationModule), data: { preload: true }
    },
    {
        path: 'bdi-code-config', loadChildren: () => import('./components/bdi-code-config/bdi-code-config.module').then(m => m.BdiCodeConfigModule), data: { preload: true }
    },
    { path: 'audit-logs', loadChildren: () => import('./components/audit-logs/audit-logs.module').then(m => m.AuditLogsModule) },
    { path: 'audit-log-configuration', loadChildren: () => import('./components/audit-log-roles-based/audit-log-roles-based.module').then(m => m.AuditLogRolesBasedModule) },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class UserRoutingModule { }
