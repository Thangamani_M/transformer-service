 enum IT_MANAGEMENT_COMPONENT  {
  BM_OPERATONS = 'Operations',
  BM_SALES = 'Sales',
  BM_TECHNICAL = 'Technical',
  EMPLOYEE='Employee',
  ROLES_AND_PERMISSIONS ='Roles & Permission',
  PROVINCE='Province',
  CITY='City',
  SUBURB='Suburb',
  DIVISION='Division',
  BRANCH='Branch',
  TEMPLATE='Template',
  SUB_BRANCH ='Sub Branch',
  TECH_STAFF_REGISTRATION='Technical Staff Registration',
  DEPARTMENT='Department',
  GROUP='Group',
  DISTRICTS='Districts',
  INVENTORY_STAFF_REGISTRATION='Inventory Staff Registration',
  CUSTOM_CATEGORY='Custom Category',
  IT_MANAGEMENT ='IT Management',
  MENU_CONFIGURATION='Menu Configuration',
  AUDIT_LOG_CONFIGURATION='Audit Log Configuration'

}
export{IT_MANAGEMENT_COMPONENT}
