class Product{
    public userTypeId: string;
    public userTypeName: string;
    public displayName: string;
    public description: string;
    public isActive: string;
    public createdDate: Date;
    public checked: boolean = false;
}

class Warehouse{
    public cityId:any;
    public cityName:string;
    public cityCode:string;
    public stateId:any;
    public isDeleted:boolean;
    public isSystem:boolean;
    public isActive:boolean;
    public createdDate: Date;
    public checked: boolean=false;
    public stateName:string;
    public countryName:string;
    public countryId:any;
}

 class Joblist {
    public stateId: any;
    public stateName: string;
    public displayName: string;
    public countryId: string;
    public isActive: boolean;
    public createdDate: Date;
    public checked: boolean=false;
}

export {Warehouse,Product,Joblist}