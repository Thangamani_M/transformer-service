import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ModuleAddEditComponent, ModuleListComponent, ModuleRoutingModule } from '@user/components/module';

@NgModule({
  declarations: [ModuleAddEditComponent, ModuleListComponent],
  imports: [
    CommonModule,
    ModuleRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule, LayoutModule,
    SharedModule
  ],
  entryComponents: [ModuleAddEditComponent]
})
export class ModuleBasedModule { }
