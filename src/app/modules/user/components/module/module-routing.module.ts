import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModuleListComponent } from '@user/components/module';


const routes: Routes = [
    { path: 'list', component: ModuleListComponent, data: { title: 'Modules' } },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class ModuleRoutingModule { }
