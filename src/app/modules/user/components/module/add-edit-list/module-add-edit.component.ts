import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix } from '@app/shared';
import {  CrudService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'module-add-edit',
  templateUrl: './module-add-edit.component.html',

})

export class ModuleAddEditComponent implements OnInit {
  @Input() id: string;
  moduleAddEditForm: FormGroup;
  applicationResponse: IApplicationResponse;
  errorMessage: string;
  btnName: string;
  loggedUser: UserLogin;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialog,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }
  ngOnInit(): void {
    if (this.data.moduleId) {
      this.btnName = 'Update';
    } else {
      this.btnName = 'Create';
    }
    this.moduleAddEditForm = this.formBuilder.group({
      moduleName: this.data.moduleName || '',
      moduleUrl: this.data.moduleUrl || '',
      sortOrder: this.data.sortOrder || '',
      description: this.data.description || ''
    });
  }

  save(): void {
    const module = { ...this.data, ...this.moduleAddEditForm.value }
    let API;
    if (module.moduleId != null) {
      module.modifiedUserId = this.loggedUser.userId;
      API = this.crudService.update(ModulesBasedApiSuffix.IT_MANAGEMENT,UserModuleApiSuffixModels.MODULES, module);
    }
    else {
      module.createdUserId = this.loggedUser.userId;
      API = this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,UserModuleApiSuffixModels.MODULES, module);
    }
    API.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.dialog.closeAll();
      }
    })
  }

  numericOnly(event): boolean {
    let patt = /^([0-9])$/;
    let result = patt.test(event.key);
    return result;
  }



}
