import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ReusablePrimeNGTableFeatureService, RxjsService } from '@app/shared';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { ModuleAddEditComponent } from './module-add-edit.component';
import { Module } from '@user/models';

@Component({
  selector: 'app-module-list',
  templateUrl: './module-list.component.html',
})

export class ModuleListComponent extends PrimeNgTableVariablesModel {
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  row: any = {};
  searchColumns: any;
  searchForm: FormGroup;
  observableResponse: any;
  searchText  = ""
  constructor(private rxjsService: RxjsService,
    private _fb: FormBuilder,
    private dialog: MatDialog,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private crudService: CrudService, private router: Router) {
    super()
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
  }

  primengTableConfigProperties: any = {
    tableCaption: "Modules List",
    breadCrumbItems: [{ displayName: 'Internal Settings', relativeRouterUrl: '' },
    { displayName: 'Modules List', relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Modules List',
          dataKey: 'moduleId',
          enableBreadCrumb: true,
          enableAddActionBtn: true,
          enableAction: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: true,
          enableStatusActiveAction: true,
          enableMultiDeleteActionBtn: true,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'moduleName', header: 'Module', width: '150px' },
            // { field: 'moduleUrl', header: 'Module Url', width: '200px' },
            { field: 'description', header: 'Description', width: '150px' },
            { field: 'sortOrder', header: 'Sort Order', width: '150px' },
            { field: 'createdDate', header: 'Created Date', width: '150px', isDate: true },
            { field: 'isActive', header: 'Status', width: '150px' },
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: UserModuleApiSuffixModels.MODULES,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT,
        }
      ]

    }
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRequiredListData();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string,
    otherParams?: object) {

    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.MODULES,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.observableResponse = response;
        this.dataList = this.observableResponse.resources;
        this.totalRecords = response.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage();
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(row);
        break;
        break;
      case CrudType.DELETE:
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getRequiredListData();
            }
          });
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
    }
  }

  applyFilter(filterValue: string) {
    this.getRequiredListData('0','10',{search:filterValue });
  }

  openAddEditPage(data?): void {
    if(!data){
      data = new Module(data);
    }
    const dialogReff = this.dialog.open(ModuleAddEditComponent, { width: '450px', data: data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
      if (!result) { this.getRequiredListData() }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
