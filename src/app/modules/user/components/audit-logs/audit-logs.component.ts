import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, CrudService, RxjsService, prepareGetRequestHttpParams, IApplicationResponse, CrudType, MomentService, getPDropdownData, prepareRequiredHttpParams, LoggedInUserModel } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { AuditLogsFilterModel } from '@modules/user/models';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { type } from 'jquery';
import { combineLatest, forkJoin } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-audit-logs',
  templateUrl: './audit-logs.component.html',
  styleUrls: ['./audit-logs.component.scss']
})
export class AuditLogsComponent extends PrimeNgTableVariablesModel implements OnInit {
  auditLogsFilterForm: FormGroup;
  showFilterForm = false
  dropdownsAndData = []
  allUsers = []
  allScreens = []
  allModules = []
  primengTableConfigProperties: any = {
    tableCaption: "Audit Log",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'IT Management ', relativeRouterUrl: '' }, { displayName: 'Audit Log' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Audit Log',
          dataKey: 'auditLogId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: false,
          shouldShowFilterActionBtn: true,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: true,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: false,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'createdDate', header: 'Date & Time', isDateTime: true, hideSortIcon: true, nosort: true },
            { field: 'actionType', header: 'Type', hideSortIcon: true, nosort: true },
            { field: 'moduleName', header: 'Module', hideSortIcon: true, nosort: true },
            { field: 'pageName', header: 'Screen', hideSortIcon: true, nosort: true },
            { field: 'columnName', header: 'Field Name', hideSortIcon: true, nosort: true },
            { field: 'actionFrom', header: 'Old Value', hideSortIcon: true, nosort: true },
            { field: 'actionTo', header: 'New Value', hideSortIcon: true, nosort: true },
            { field: 'refNo', header: 'Reference ID', hideSortIcon: true, nosort: true },
            { field: 'userName', header: 'User', hideSortIcon: true, nosort: true },
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          hideSortIcon: true,
          apiSuffixModel: UserModuleApiSuffixModels.AUDIT_LOGS,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT
        }]
    }
  }

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private momentService: MomentService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder) {
    super();
  }

  ngOnInit(): void {

    this.createFilterForm()
    this.combineLatestNgrxStoreData()
    this.dropdownsAndData = [
      this.crudService.get(ModulesBasedApiSuffix.SHARED,
        UserModuleApiSuffixModels.AUDIT_LOGS_USERS,undefined,false,prepareRequiredHttpParams({userId: this.loggedInUserData?.userId})),
      this.crudService.get(ModulesBasedApiSuffix.SHARED,
        UserModuleApiSuffixModels.UX_ALL_SCEENS,undefined,false,prepareRequiredHttpParams({userId: this.loggedInUserData?.userId})),
      this.crudService.get(ModulesBasedApiSuffix.SHARED,
        UserModuleApiSuffixModels.UX_ALL_MOUDULES,undefined,false,prepareRequiredHttpParams({userId: this.loggedInUserData?.userId})),
    ];
    this.getRequiredData();

    this.loadDropdownData(this.dropdownsAndData);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }



  loadDropdownData(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.allUsers = getPDropdownData(resp.resources);
              break;
            case 1:
              this.allScreens = getPDropdownData(resp.resources);
              break;
            case 2:
              this.allModules = getPDropdownData(resp.resources);
              break;
          }
          this.rxjsService.setGlobalLoaderProperty(false);

        }
      })
    })
  }
  createFilterForm() {
    let filterModel = new AuditLogsFilterModel();
    this.auditLogsFilterForm = this.formBuilder.group({});
    Object.keys(filterModel).forEach((key) => {
      this.auditLogsFilterForm.addControl(key, new FormControl(filterModel[key]));
    });
    this.onFormControlsChange()
  }

onFormControlsChange(){

  this.auditLogsFilterForm.get('moduleIds').valueChanges.subscribe(moduleIds=>{
    this.allScreens = []
    this.allUsers = []
    if(moduleIds.length){
      this.crudService.get(ModulesBasedApiSuffix.SHARED,
        UserModuleApiSuffixModels.UX_ALL_SCEENS,undefined,false,prepareRequiredHttpParams({userId: this.loggedInUserData?.userId,moduleIds:this.auditLogsFilterForm.get('moduleIds').value.join(",")})).subscribe(response=>{
          if(response.isSuccess){
            this.allScreens = getPDropdownData(response.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);

        })
    }
  })

  this.auditLogsFilterForm.get('pageIds').valueChanges.subscribe(moduleIds=>{
    this.allUsers = []
    if(moduleIds.length){
      this.crudService.get(ModulesBasedApiSuffix.SHARED,
        UserModuleApiSuffixModels.AUDIT_LOGS_USERS,undefined,false,prepareRequiredHttpParams({userId: this.loggedInUserData?.userId,moduleIds:this.auditLogsFilterForm.get('moduleIds').value.join(","),auditPageIds:this.auditLogsFilterForm.get('pageIds').value.join(",")})).subscribe(response=>{
          if(response.isSuccess){
            this.allUsers = getPDropdownData(response.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);

        })
    }
  })
}

  getRequiredData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    otherParams = {...otherParams, userId: this.loggedInUserData?.userId}
    this.crudService.get(ModulesBasedApiSuffix.SHARED, UserModuleApiSuffixModels.AUDIT_LOGS, undefined, false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.dataList = response.resources
          this.totalRecords = response.totalCount;
          this.loading = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm
        break;
      case CrudType.GET:
        this.getRequiredData(row["pageIndex"], row["pageSize"], searchObj);
        break;
    }
  }

  clearForm(boolean) {
    if(boolean){
      this.auditLogsFilterForm.reset();
    }else{
      this.showFilterForm = false
    }
  }
  submitFilter() {
    this.showFilterForm = false
    let filterObj = this.auditLogsFilterForm.value;
    Object.keys(filterObj).forEach(key => {
      if (filterObj[key] === "" || filterObj[key]?.length == 0) {
        delete filterObj[key]
      } else if (key == "fromDate" || key == 'toDate') {
        if (filterObj['fromDate']) {
          filterObj["fromDate"] = this.momentService.localToUTC(filterObj[key]);
        }
        if (filterObj['toDate']) {
          filterObj["toDate"] = this.momentService.localToUTC(filterObj[key]);
        }

      }
    });
    console.log("filterObj", filterObj)
    let searchColumns = Object.entries(filterObj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.onCRUDRequested('get', {}, searchColumns)


  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
