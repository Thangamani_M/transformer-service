import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuditLogsRoutingModule } from './audit-logs-routing.module';
import { AuditLogsComponent } from './audit-logs.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LayoutModule, MaterialModule, SharedModule } from '@app/shared';


@NgModule({
  declarations: [AuditLogsComponent],
  imports: [
    CommonModule,
    AuditLogsRoutingModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
  ]
})
export class AuditLogsModule { }
