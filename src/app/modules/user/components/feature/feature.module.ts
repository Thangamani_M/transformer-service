import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AddFeatureComponent, FeatureAddEditPageComponent, FeatureListComponent, FeatureRoutingModule } from '@user/components/feature';

@NgModule({
  declarations: [ FeatureListComponent, FeatureAddEditPageComponent, AddFeatureComponent],
  imports: [
    CommonModule,
    FeatureRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents: [AddFeatureComponent]
})
export class FeatureModule { }
