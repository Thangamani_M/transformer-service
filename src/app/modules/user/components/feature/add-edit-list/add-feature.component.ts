import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserModuleApiSuffixModels } from '@app/modules';
import { ConfirmDialogPopupComponent, CustomDirectiveConfig, ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';

@Component({
  selector: 'app-feature',
  templateUrl: './add-feature.component.html',
})

export class AddFeatureComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  featureAddForm: FormGroup;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  constructor(@Inject(MAT_DIALOG_DATA) public data, public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>, private crudService: CrudService,
    private httpCancelService: HttpCancelService, private rxjsService: RxjsService,
  ) {
  }


  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.featureAddForm = new FormGroup({
      featureName: new FormControl('', Validators.required),
    });
  }

  dialogClose(): void {
    this.dialogRef.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

  onSubmit(): void {
    if (this.featureAddForm.invalid) {
      return;
    }

    this.featureAddForm.value.moduleId = this.data.data.moduleId;
    this.featureAddForm.value.navigationId = this.data.data.navigationId;
    this.featureAddForm.value.createdUserId = this.data.data.createdUserId

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.FEATURE_LIST, this.featureAddForm.value).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.outputData.emit(true);
        this.dialogRef.close(false);
      }
    })

  }

}
