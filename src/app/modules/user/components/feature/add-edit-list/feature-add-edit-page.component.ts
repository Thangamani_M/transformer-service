import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { AlertService, ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { FeatureAddEditModel, UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { AddFeatureComponent } from './add-feature.component';

@Component({
  selector: 'app-feature-add-edit',
  templateUrl: './feature-add-edit-page.component.html',
})
export class FeatureAddEditPageComponent {
  ModulesList: any;
  isDialogLoaderLoading = true;
  navigationList: any;
  loggedUser: any;
  navigationId: any;
  moduleId: any;
  featureAddEditForm: FormGroup;
  featureList: any;
  addFeatureButtonShow = false;
  constructor(
    private alertService: AlertService,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.navigationId = this.activatedRoute.snapshot.queryParams.navigationId;
    this.moduleId = this.activatedRoute.snapshot.queryParams.moduleId;

  }
  ngOnInit() {
    this.loadModuleDropdown();
    this.createFeatureAddEditForm();
    this.onFormControlChange();

    if (this.navigationId) {
      this.featureAddEditForm.controls['moduleId'].setValue(this.moduleId);
      this.featureAddEditForm.controls['navigationId'].setValue(this.navigationId);
    }
  }

  loadModuleDropdown() {
    this.crudService.dropdown(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.UX_MODULES).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
        this.ModulesList = response.resources;
        this.isDialogLoaderLoading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      } else {
        this.alertService.processAlert(response);
      }
      this.isDialogLoaderLoading = false;
    });
  }

  onFormControlChange(): void {
    this.featureAddEditForm.get('moduleId').valueChanges.subscribe((moduleId: string) => {
      if (!moduleId) return;
      let lssStageOneFormValue = this.featureAddEditForm.getRawValue()
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.FEUTURE_NAVIGATION_LIST, undefined, false, prepareGetRequestHttpParams(null, null,
          { moduleId: lssStageOneFormValue.moduleId }), 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.navigationList = response.resources;
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
    });
    this.featureAddEditForm.get('navigationId').valueChanges.subscribe((navigationId: string) => {
      if (!navigationId) return;
      if (navigationId) {
        this.addFeatureButtonShow = true;
        this.getFeatureList();
      }
    });

  }

  getFeatureList() {
    let FormValue = this.featureAddEditForm.getRawValue()
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.FEUTURE_FEATURES_LIST, FormValue.navigationId, false, null, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.featureList = response.resources.features;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

  }

  updateFeatureStatus(e, i, featureId) {
    const message = `Are you want to change the status?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const index = i;
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) {
        this.getFeatureList();
      } else {
        let data = {
          moduleId: this.featureAddEditForm.value.moduleId,
          navigationId: this.featureAddEditForm.value.navigationId,
          createdUserId: this.loggedUser.userId,
          isActive: e.checked ? true : false,
          featureId: featureId
        }
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.ADD_FEATURE, data).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.getFeatureList();
          }
        })

      }
    })
  }

  createFeatureAddEditForm(): void {
    let featureModel = new FeatureAddEditModel();
    // create form controls dynamically from model class
    this.featureAddEditForm = this.formBuilder.group({});
    Object.keys(featureModel).forEach((key) => {
      this.featureAddEditForm.addControl(key, new FormControl(featureModel[key]));
    });
    this.featureAddEditForm = setRequiredValidator(this.featureAddEditForm, ["createdUserId"]);
    this.featureAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
  }


  openaddFeatureModal(action?, days?): void {
    let data = {
      moduleId: this.featureAddEditForm.value.moduleId,
      navigationId: this.featureAddEditForm.value.navigationId,
      createdUserId: this.loggedUser.userId
    }
    const dialogReff = this.dialog.open(AddFeatureComponent, { width: '700px', disableClose: true, data: { data: data } });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getFeatureList();
      }

    })

  }

  onSubmit() { }
}
