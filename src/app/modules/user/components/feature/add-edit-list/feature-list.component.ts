import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudService, CrudType, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { DialogService } from 'primeng/api';
import { of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-feature-list',
  templateUrl: './feature-list.component.html',
})

export class FeatureListComponent extends PrimeNgTableVariablesModel {
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  row: any = {};
  searchColumns: any;
  searchForm: FormGroup;
  observableResponse: any;
  constructor(private rxjsService: RxjsService, private tableFilterFormService: TableFilterFormService, private dialogService: DialogService, private momentService: MomentService, private _fb: FormBuilder, private crudService: CrudService, private router: Router) {
    super()
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
  }

  primengTableConfigProperties: any = {
    tableCaption: "Feature List",
    breadCrumbItems: [{ displayName: 'Internal Settings', relativeRouterUrl: '' },
    { displayName: 'Feature List', relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Feature List',
          dataKey: 'registrationId',
          enableBreadCrumb: true,
          enableAddActionBtn: true,
          enableAction: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'moduleName', header: 'Module Name ', width: '200px' },
          { field: 'navigationName', header: 'Navigations', width: '200px' },
          { field: 'featureName', header: 'Features', width: '200px' },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: UserModuleApiSuffixModels.FEATURE_LIST,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT,
        }
      ]

    }
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.getRequiredListData();
    this.columnFilterRequest();
  }

1
  getRequiredListData(pageIndex?: string, pageSize?: string,
    otherParams?: object) {
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.FEATURE_LIST,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.observableResponse = response;
        this.dataList = this.observableResponse.resources;
        this.totalRecords = response.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }


  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns;
          // this.otherParams = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
        case CrudType.VIEW:
          this.openAddEditPage(CrudType.EDIT, row);
          break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['user/features/add-edit']);
        break;
      case CrudType.EDIT:
        this.router.navigate(['user/features/add-edit'], { queryParams: { navigationId: editableObject['navigationId'], moduleId: editableObject['moduleId'] } });
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
