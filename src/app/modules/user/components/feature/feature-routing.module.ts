import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FeatureAddEditPageComponent, FeatureListComponent } from '@user/components/feature';



const routes: Routes = [
    { path: 'list', component: FeatureListComponent, data: { title: 'Features' } },
    { path: 'add-edit', component: FeatureAddEditPageComponent, data: { title: 'Feature add-edit' } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class FeatureRoutingModule { }