import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeAddEditComponent, EmployeeListComponent } from '@user/components/employee';
import { EmployeeViewComponent } from './add-edit-list/employee-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: EmployeeListComponent, canActivate: [AuthGuard], data: { title: 'Employee' } },
  { path: 'add-edit', component: EmployeeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Employee Add Edit' } },
  { path: 'add-edit/:id', component: EmployeeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Employee Add Edit' } },
  { path: 'view', component: EmployeeViewComponent, canActivate: [AuthGuard], data: { title: 'Employee View ' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class EmployeeRoutingModule { }
