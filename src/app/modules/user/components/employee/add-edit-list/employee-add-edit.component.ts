import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CustomDirectiveConfig, FindDuplicatePipe } from "@app/shared";
import { ResponseMessageTypes } from "@app/shared/enums";
import { CrudService, RxjsService, SnackbarService } from "@app/shared/services";
import { MomentService } from '@app/shared/services/moment.service';
import { countryCodes, emailPattern, formConfigs, LoggedInUserModel, ModulesBasedApiSuffix, prepareAutoCompleteListFormatForMultiSelection, prepareGetRequestHttpParams, prepareRequiredHttpParams, setRequiredValidator } from "@app/shared/utils";
import { IApplicationResponse } from "@app/shared/utils/interfaces.utils";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { SalesModuleApiSuffixModels } from "@modules/sales/shared/utils/sales-module.enums";
import { UserModuleApiSuffixModels } from "@modules/user/shared";
import { Store } from "@ngrx/store";
import { Employee, EmployeeAddEditModel } from "@user/models";
import { SelectAutocompleteComponent } from "mat-select-autocomplete";
import { combineLatest, forkJoin, Observable } from "rxjs";
import { debounceTime, distinctUntilChanged, switchMap } from "rxjs/operators";
@Component({
  selector: "employee-add-edit",
  templateUrl: "./employee-add-edit.component.html",
  styleUrls: ["./employee.component.scss"],
})
export class EmployeeAddEditComponent implements OnInit {
  selected = true;
  internalUser = true;
  stringConfig = new CustomDirectiveConfig({ isAStringOnlyNoSpace: true });
  isAlphaSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true });

  isAnAlphaNumericOnly = new CustomDirectiveConfig({
    isAnAlphaNumericOnly: true,
  });
  isANumberWithZero = new CustomDirectiveConfig({ isANumberWithZero: true });

  employeeForm: FormGroup;
  employeemaintananceForm: FormGroup;
  id;
  departmentList = [];
  divisionList = "";
  reportinglinesList = "";
  defaultLocationList = "";
  alternativeLocationList = "";
  rolesList = "";
  skillList = "";
  errorMessage: string;
  applicationResponse: IApplicationResponse;
  fileList: File[] = [];
  listOfFiles = [];
  btnName: string;
  dynamicdocumentdetails: Array<any> = [];
  documentdetails: Array<any> = [];
  IsCreatePage: Boolean;
  employeeId;
  Email: string;
  EmployeeDTO = new Employee();
  todayDate = new Date();
  EmployeemaintenanceDTO;
  param = { employeeId: "" };
  isActivechecked: boolean = true;
  isInActive: boolean = false;
  isDisabled: boolean = false;
  isViewImage: boolean = false;
  Isemployee: boolean = true;
  isemployeemaintanance: boolean = false;
  Isemployeemaintab: boolean = true;
  setindex: number = 0;
  employeeProfilename = "";
  IsbuttonDisabled: boolean = false;
  IsEmployeemaintenenceDisabled: Boolean = false;
  usernameEmail = { username: "" };
  employeeNovalidation = { employeeNo: "" };
  isaldreadyexist: boolean = false;
  isaldreadyempnoexist: boolean = false;
  loggedInUserData: LoggedInUserModel;
  selectedOptions: any = [];
  selectedOptions1: any;
  selectedOptions5: any;
  selectedOptions2: any;
  caption: string;
  @ViewChild(SelectAutocompleteComponent, null)
  multiSelect: SelectAutocompleteComponent;
  @ViewChild("userPhoto", null) userPhoto: ElementRef;
  regionList: any = [];
  Divisions: any = [];
  Districts: any = [];
  branchDropdown: any = [];
  userLevels;
  formConfigs = formConfigs;
  designations;
  checkMaxLengthAddress: boolean;
  physicalLocationList = [];
  inValid: boolean;
  physicalLocationBranchList = [];
  roleId;
  managers = [];
  countryCodes = countryCodes;
  details;
  seletDivision = true;
  seletRegion = true;
  seletDistrict = true;
  seletBranch = true;
  divisionId;
  isInternalUser: boolean = true;
  isSubmitted: boolean = false;
  iscontactNumber: boolean = false;
  ismobileNo: boolean = false;
  employeeUserId: string;
  auditdetails;
  isAudit: boolean = false;
  isSave: boolean = false;
  constructor(
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,
    private router: Router,
    private rxjsService: RxjsService,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private momentService: MomentService
  ) {

  }

  ngOnInit() {
    this.combineLatestNgrxStoreData(); 
    this.createEmployeeEditForm();
    this.getRegions();
    this.getForkJoinRequestsOne();
    this.onFormControlChanges();
    if (this.employeeId) {
      this.crudService
        .get(
          ModulesBasedApiSuffix.IT_MANAGEMENT,
          SalesModuleApiSuffixModels.SALES_API_EMPLOYEE_DETAILS,
          undefined,
          false,
          prepareGetRequestHttpParams(null, null, {
            employeeId: this.employeeId,
          }),
          1
        )
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.isSuccess) {
            this.getEmployeePendingJob();
            if (response?.resources?.terminationDate) {
              response.resources.terminationDate = new Date(response.resources?.terminationDate)
            }
            else
              response.resources.terminationDate = null;


            this.details = response.resources;
            this.roleId = this.details.roleId;
            this.internalUser = this.details.isInternalUser;
            this.employeeUserId = this.details.userId;
            this.details.userId = this.details.userRefNo;
            this.getBranchDropdown(this.details.physicalLocationDivisionId);
            this.employeeForm.patchValue(this.details);
            this.getUserLevels();
            if (this.internalUser)
              this.employeeForm.get('userId').disable();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    
  }
  getEmployeePendingJob(){
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,InventoryModuleApiSuffixModels.PENDING_EMPLOYEE,
      undefined,false,
      prepareGetRequestHttpParams(null, null, {
        employeeId: this.employeeId,
      }), 1
    )
    .subscribe((response: IApplicationResponse) => {
         if (response.statusCode == 200 && response.isSuccess && !response.resources.isSuccess) { 
            this.isSave = false;
            if(response.resources.taskDetails){
              this.isSave =true;
               this.disableFields();
              this.snackbarService.openSnackbar("This user has pending job.",ResponseMessageTypes.WARNING);
            }              
         }else if(response.statusCode == 200 && response.isSuccess && response.resources.isSuccess){
            this.isSave =false;
            this.enableFields();
         }else {
          this.isSave =false;
          this.disableFields();
         }
      });
  }
  disableFields(){
    //console.log('disableFields value',this.employeeForm.value);
    this.employeeForm.get("regionId").disable();
    this.employeeForm.get("divisionId").disable();
    this.employeeForm.get("districtId").disable();
    this.employeeForm.get("branchId").disable();
    this.employeeForm.get("departmentId").disable();
    this.employeeForm.get("userLevelId").disable();
    this.employeeForm.get("designationId").disable();
    console.log('disableFields',this.employeeForm.get("regionId").disable());
  }
  enableFields() {
    this.employeeForm.get("regionId").enable();
    this.employeeForm.get("divisionId").enable();
    this.employeeForm.get("districtId").enable();
    this.employeeForm.get("branchId").enable();  
    this.employeeForm.get("departmentId").enable();
    this.employeeForm.get("userLevelId").enable();
    this.employeeForm.get("designationId").enable();
   // console.log('enableFields');
  }
  getBranchDropdown(divisionId) {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_DISTRICT_BRANCH,
        null,
        false,
        prepareGetRequestHttpParams(null, null, { divisionId: divisionId })
      )
      .subscribe((response: IApplicationResponse) => {
        this.branchDropdown = [];
        if (response && response?.resources && response?.isSuccess) {
          this.seletDistrict = true;
          // this.branchDropdown = response.resources;
          for (let i = 0; i < response.resources.length; i++) {
            let temp = {};
            temp['display'] = response.resources[i].displayName;
            temp['value'] = response.resources[i].id;
            this.branchDropdown.push(temp);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.route.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.employeeId = response[1].id;
    });
  }

  getRegions() {
    this.crudService
      .dropdown(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_REGIONS,
        prepareGetRequestHttpParams(null, null, {
          CountryId: formConfigs.countryId,
        })
      )
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {

          if (response.resources?.length > 0) {
            for (let i = 0; i < response.resources.length; i++) {
              let temp = {};
              temp['display'] = response.resources[i].displayName;
              temp['value'] = response.resources[i].id;
              this.regionList.push(temp);
            }
          }

          this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getForkJoinRequestsOne(): void {
    forkJoin([
      this.crudService.get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        SalesModuleApiSuffixModels.SUPPORT_DEPARTMENT_LIST,
        undefined,
        true
      ),
      this.crudService.get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        SalesModuleApiSuffixModels.SUPPORT_DESIGNATIONS_LIST,
        undefined,
        true
      ),
      this.crudService.get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        SalesModuleApiSuffixModels.SUPPORT_DESIGNATIONS_ROLES,
        undefined,
        true
      ),
      this.crudService.get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.PHYSICAL_DIVISION,
        undefined,
        true
      ),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((respObj: IApplicationResponse, ix: number) => {
        if (respObj.isSuccess) {
          switch (ix) {
            case 0:
              this.departmentList = respObj.resources;
              break;
            case 1:
              this.designations = respObj.resources;
              break;
            case 2:
              //this.rolesList = respObj.resources;
              break;
            case 3:
              this.physicalLocationList = respObj.resources;
              break;
          }
        }
        if (ix == response.length - 1) {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  changeRegion() {
    if (!this.employeeForm.value.regionId && (this.employeeForm.value.regionId && this.employeeForm.value.regionId?.length == 0)) {

      this.employeeForm.get("divisionId").setValue(null);
      this.employeeForm.get("districtId").setValue(null);
      this.employeeForm.get("branchId").setValue("");
      this.Districts = [];
      this.Divisions = [];
      this.branchDropdown = [];
    }

  }

  changeDivision() {

    if (!this.employeeForm.value.divisionId && (this.employeeForm.value.divisionId && this.employeeForm.value.divisionId?.length == 0)) {

      this.employeeForm.get("districtId").setValue([]);
      this.employeeForm.get("branchId").setValue([]);
      this.Districts = [];
      this.branchDropdown = [];
    }
  }

  changeDistrict() {
    //  if (!this.employeeForm.value.districtId && (this.employeeForm.value.districtId && this.employeeForm.value.districtId?.length == 0)) {
    if (!this.employeeForm.value.districtId && (this.employeeForm.value.districtId && this.employeeForm.value.districtId.length == 0)) {

      this.employeeForm.get("branchId").setValue([]);
      this.branchDropdown = [];
    }
  }
  onchangeUsername() {
    this.resetValue();
    let userName = this.employeeForm.get('userName').value;
    if (this.isInternalUser) {
      this.crudService.get(
        ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.AD_USER_SEARCH,
        null, true, prepareRequiredHttpParams({ UserName: userName })
      ).subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.setValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }

  }
  setValue(response) {
    this.employeeForm.get('firstName').setValue(response.FirstName);
    this.employeeForm.get('lastName').setValue(response.LastName);
    this.employeeForm.get('emailAddress').setValue(response.email);
    this.employeeForm.get('employeeNo').setValue(response.EmployeeNo);
    this.employeeForm.get('contactNumber').setValue(response.ContactNo);
    this.employeeForm.get('mobileNo').setValue(response.MobileNo);

    // if(response.ContactNo)
    // this.setContactNo(response.ContactNo);
    // if(response.MobileNo)
    // this.setMobileNo(response.MobileNo);

    this.employeeForm.get('contactNumber').markAsTouched();
    this.employeeForm.get('mobileNo').markAsTouched();
    // this.iscontactNumber = true;
    //  this.ismobileNo = true;

    let departmentListArr = this.departmentList.filter(x => x.displayName == response.department)
    if (departmentListArr && departmentListArr.length > 0) {
      this.employeeForm.get('departmentId').setValue(departmentListArr[0].id);
    }
    if (!response.email) {
      let userName = this.employeeForm.get('userName').value;
      this.employeeForm.get('emailAddress').setValue(userName);
    }
  }
  resetValue() {
    this.employeeForm.get('firstName').reset();
    this.employeeForm.get('lastName').reset();
    this.employeeForm.get('emailAddress').reset();
    this.employeeForm.get('employeeNo').reset();
    this.employeeForm.get('departmentId').reset();
    this.employeeForm.get('contactNumber').reset();
    this.employeeForm.get('mobileNo').reset();
  }
  setContactNo(response) {
    let contactNoSplit = response.substring(0, 3);
    if (contactNoSplit.includes('27') || contactNoSplit.includes('+27'))
      this.setContactCode(contactNoSplit, response.substring(2), 'contactNumberCountryCode', 'contactNumber')
    if (contactNoSplit.includes('91') || contactNoSplit.includes('+91'))
      this.setContactCode(contactNoSplit, response.substring(2), 'contactNumberCountryCode', 'contactNumber')
  }
  setMobileNo(response) {
    let contactNoSplit = response.substring(0, 3);
    if (contactNoSplit.includes('27') || contactNoSplit.includes('+27'))
      this.setContactCode(contactNoSplit, response.substring(2), 'mobileNoCountryCode', 'mobileNo')
    if (contactNoSplit.includes('91') || contactNoSplit.includes('+91'))
      this.setContactCode(contactNoSplit, response.substring(2), 'mobileNoCountryCode', 'mobileNo')
  }
  setContactCode(code, num, type1, type2) {
    this.employeeForm.get(type1).setValue(code);
    this.employeeForm.get(type2).setValue(num);
  }
  onFormControlChanges() {

    this.employeeForm
      .get("isInternalUser")
      .valueChanges.subscribe((isInternalUser: any) => {
        this.isInternalUser = isInternalUser ? true : false;
        if (this.isInternalUser)
          this.employeeForm.get('userName').enable();
        else {
          this.employeeForm.get('userName').disable();
          if (this.employeeForm.get('emailAddress').value) {
            this.employeeForm.get('userName').setValue(this.employeeForm.get('emailAddress').value);
          }
        }

      });

    this.employeeForm
      .get("emailAddress")
      .valueChanges.subscribe((emailAddress: any) => {
        if (!this.isInternalUser) {
          this.employeeForm.get('userName').setValue(emailAddress);
          this.employeeForm.get('userName').disable();
        }
        else
          this.employeeForm.get('userName').enable();
      });
    this.employeeForm
      .get("regionId")
      .valueChanges.subscribe((regionId: any[]) => {
        if (!regionId || (regionId && regionId.length == 0)) {
          this.Divisions = [];
          //     this.employeeForm
          // .get("divisionId").setValue("");
          return;
        }
        let reqObject = { regionId: regionId.join(',') }
        this.crudService
          .get(
            ModulesBasedApiSuffix.IT_MANAGEMENT,
            UserModuleApiSuffixModels.UX_DIVISIONS,
            null,
            false,
            prepareRequiredHttpParams(reqObject)
          )
          .subscribe((response: IApplicationResponse) => {
            if (response && response?.resources && response?.isSuccess) {
              this.seletRegion = true;
              this.Divisions = prepareAutoCompleteListFormatForMultiSelection(response.resources);
              //  for (let i = 0; i < response.resources.length; i++) {
              //   let temp = {};
              //   temp['display'] = response.resources[i].displayName;
              //   temp['value'] = response.resources[i].id;
              //   this.Divisions.push(temp);
              //   }
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.employeeForm
      .get("divisionId")
      .valueChanges.subscribe((divisionId) => {
        if (!divisionId) return;
        if (divisionId?.length == 0) {
          return;
        }
        this.divisionId = divisionId;
        this.crudService
          .get(
            ModulesBasedApiSuffix.IT_MANAGEMENT,
            UserModuleApiSuffixModels.UX_DIVISION_DISTRICT,
            null,
            false,
            prepareGetRequestHttpParams(null, null, { divisionId: divisionId })
          )
          .subscribe((response: IApplicationResponse) => {
            this.Districts = [];
            if (response && response?.resources && response?.isSuccess) {
              this.seletDivision = true;
              // this.Districts = response.resources;
              for (let i = 0; i < response.resources.length; i++) {
                let temp = {};
                temp['display'] = response.resources[i].displayName;
                temp['value'] = response.resources[i].id;
                this.Districts.push(temp);
              }
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    this.employeeForm
      .get("contactNumber")
      .valueChanges.subscribe((mobileNumber2: string) => {
        this.iscontactNumber = false;
        this.setPhoneNumberLengthByCountryCode(
          this.employeeForm.get("contactNumberCountryCode").value,
          "contact"
        );
      });
    this.employeeForm
      .get("branchId")
      .valueChanges.subscribe((branchId: string) => {
        if (!branchId) return;
        this.seletBranch = true;
      });

    this.employeeForm
      .get("mobileNo")
      .valueChanges.subscribe((mobileNumber2: string) => {
        this.ismobileNo = false;
        this.employeeForm.get("mobileNo").markAsTouched();
        this.setPhoneNumberLengthByCountryCode(
          this.employeeForm.get("mobileNoCountryCode").value,
          "mobile"
        );
      });
    this.employeeForm
      .get("districtId")
      .valueChanges.subscribe((districtId: string) => {
        if (!districtId) return;
        if (districtId && districtId?.length == 0) {
          return;
        }
        if (this.divisionId) {
          this.crudService
            .get(
              ModulesBasedApiSuffix.IT_MANAGEMENT,
              UserModuleApiSuffixModels.UX_DISTRICT_BRANCH,
              null,
              false,
              prepareGetRequestHttpParams(null, null, { districtId: districtId, divisionId: this.divisionId })
            )
            .subscribe((response: IApplicationResponse) => {
              this.branchDropdown = [];
              if (response && response?.resources && response?.isSuccess) {
                this.seletDistrict = true;
                // this.branchDropdown = response.resources;
                for (let i = 0; i < response.resources.length; i++) {
                  let temp = {};
                  temp['display'] = response.resources[i].displayName;
                  temp['value'] = response.resources[i].id;
                  this.branchDropdown.push(temp);
                }
              }

              this.rxjsService.setGlobalLoaderProperty(false);
            });
        }

      });

    this.employeeForm
      .get("physicalLocationDivisionId")
      .valueChanges.pipe(
        debounceTime(800),
        distinctUntilChanged(),
        switchMap((val) => {
          if (val) {
            this.physicalLocationBranchList = [];
            this.checkMaxLengthAddress = false;
            let temp = this.filterServiceTypeDetailsBySearchOptions(val);
            return temp;
          } else {
            this.physicalLocationBranchList = [];
          }
        })
      )
      .subscribe((response) => {
        // this.employeeForm.get('physicalLocationBranchId').setValue('');
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources && response.resources.length > 0) {
            this.inValid = false;

            for (let i = 0; i < response.resources.length; i++) {
              let temp = {};
              temp['display'] = response.resources[i].displayName;
              temp['value'] = response.resources[i].id;
              this.physicalLocationBranchList.push(temp);
            }
            //this.physicalLocationBranchList = response.resources;
            if (this.physicalLocationBranchList && this.physicalLocationBranchList.length == 0) {
              this.physicalLocationBranchList = [];
              this.inValid = true;
            }
          } else {
            this.physicalLocationBranchList = [];
            this.inValid = true;
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  changeDivisionPhy() {
    this.employeeForm.get('physicalLocationBranchId').setValue('');

  }

  filterServiceTypeDetailsBySearchOptions(
    serachValue?: string
  ): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.PHYSICAL_BRANCH_LIST,
      null,
      true,
      prepareGetRequestHttpParams(null, null, {
        divisionId: serachValue,
      })
    );
  }
  isClicked: boolean = false;
  isClickedReportingEmployee: boolean = false;

  onSelectRole() {
    this.rxjsService.setDialogOpenProperty(true);
    this.isClicked = true;
    this.isClickedReportingEmployee = false;
    this.caption = "Employee Creation";
  }

  changeDepartment() {
    this.isClicked = false;
  }

  isDuplicate: boolean = false;
  onChange(str) {
    this.isDuplicate = false;
    let contact1 = this.employeeForm.value.contactNumber;
    let contact2 = this.employeeForm.value.mobileNo;
    if (str == "contact1") {
      if (contact1 == contact2) {
        this.snackbarService.openSnackbar(
          "Contact number is duplicate",
          ResponseMessageTypes.WARNING
        );
        this.isDuplicate = true;
      } else {
        this.isDuplicate = false;
      }
    } else if (str == "contact2") {
      if (contact2 == contact1) {
        this.isDuplicate = true;
        this.snackbarService.openSnackbar(
          "Mobile number is duplicate",
          ResponseMessageTypes.WARNING
        );
      } else {
        this.isDuplicate = false;
      }
    }
  }

  checkDuplication() {
    this.rxjsService.setNotificationProperty(false);
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        SalesModuleApiSuffixModels.SALES_API_EMPLOYEE_DUPLICATION,
        undefined,
        false,
        prepareGetRequestHttpParams(null, null, {
          employeeNo: this.employeeForm.value.employeeNo,
        }),
        1
      )
      .subscribe((response: IApplicationResponse) => {
        if (response.message == "EmployeeNo already exist") {
          this.employeeForm.controls["employeeNo"].setErrors({
            duplicate: true,
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  setPhoneNumberLengthByCountryCodeOld(countryCode: string, str: string) {
    switch (countryCode) {
      case "+27":
        if (str == "contact") {
          this.employeeForm.get('contactNumber').setValidators([Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
          Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);

        } else {
          this.employeeForm.get('mobileNo').setValidators([Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
          Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        }
        break;
      case "+91":
        if (str == "contact") {
          this.employeeForm.get('contactNumber').setValidators([Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
          Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);

        } else {
          this.employeeForm.get('mobileNo').setValidators([Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
          Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        }
        break;
      default:
        if (str == "contact") {
          this.employeeForm
            .get("contactNumber")
            .setValidators([
              Validators.minLength(formConfigs.indianContactNumberMaxLength),
              Validators.maxLength(formConfigs.indianContactNumberMaxLength),
            ]);
        } else {
          this.employeeForm
            .get("mobileNo")
            .setValidators([
              Validators.minLength(formConfigs.indianContactNumberMaxLength),
              Validators.maxLength(formConfigs.indianContactNumberMaxLength),
            ]);
        }
        break;
    }
  }
  setPhoneNumberLengthByCountryCode(countryCode: string, str: string) {
    let obj;
    if (str == "contact")
      obj = this.employeeForm.get("contactNumber");
    else if (str == "mobileNo")
      obj = this.employeeForm.get("mobileNo");

    switch (countryCode) {
      case "+27":
        obj?.setValidators([
          Validators.minLength(
            formConfigs.southAfricanContactNumberMaxLength
          ),
          Validators.maxLength(
            formConfigs.southAfricanContactNumberMaxLength
          ),
        ]);
        break;
      case "+91":
        obj?.setValidators([
          Validators.minLength(
            formConfigs.indianContactNumberMaxLength
          ),
          Validators.maxLength(
            formConfigs.indianContactNumberMaxLength
          ),
        ]);
        break;
      default:
        obj?.setValidators([
          Validators.minLength(formConfigs.indianContactNumberMaxLength),
          Validators.maxLength(formConfigs.indianContactNumberMaxLength),
        ]);
        break;
    }
  }
  onSelectReportingEmployee() {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.roleId) {
      this.isClickedReportingEmployee = true;
      this.isClicked = false;
      this.caption = "Employee Creation";
    } else {
      this.snackbarService.openSnackbar(
        "Please Select Role first",
        ResponseMessageTypes.WARNING
      );
    }
  }

  getManagers() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_MANAGERS,
        null,
        false,
        prepareGetRequestHttpParams(null, null, {
          roleId: this.employeeForm.value.roleId,
          reportingEmployeeId: this.employeeForm.value.reportingEmployeeId,
          divisionId: this.employeeForm.value.divisionId,
        })
      )
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.managers = response.resources;
          if (this.managers && this.managers.length > 0) {
            if (this.managers.length == 1) {
              this.employeeForm
                .get("managerName")
                .setValue(this.managers[0].displayName);
              this.employeeForm.get("managerId").setValue(this.managers[0].id);
            }
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getUserLevels() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.NEW_USER_LEVEL,
        this.employeeForm.value.roleId,
        false,
        null
      )
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.userLevels = response.resources;
          if (this.userLevels && this.userLevels?.length == 1)
            this.employeeForm.get('userLevelId').setValue(this.userLevels[0].id);

          if (this.userLevels?.length == 0) {
            this.snackbarService.openSnackbar(
              "There are no user levels mapped to the selected role",
              ResponseMessageTypes.WARNING
            );
            this.employeeForm.get('roleName').setValue('');
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  emittedEvent(params) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.roleId = params.roleId;
    if (this.isClicked) {
      this.employeeForm.get("roleName").setValue(params.roleName);
      this.employeeForm.get("roleId").setValue(params.roleId);
      document.getElementById("closeButtonForRole").click();
    }
    this.getUserLevels();
    if (this.isClickedReportingEmployee) {
      this.employeeForm.get("reportingEmployeeName").setValue(params.roleName);
      this.employeeForm.get("reportingEmployeeId").setValue(params.roleId);
      this.getManagers();
      document.getElementById("closeButtonForReportingManager").click();
    }
    else {
      this.employeeForm.get("userLevelId").setValue("");
    }
    this.caption = "";
    this.isClickedReportingEmployee = false;
    this.isClicked = false;
    this.rxjsService.setDialogOpenProperty(false);
    document.querySelectorAll(".modal-backdrop")?.forEach(e => e.remove());
  }

  getDataReportingLine(roleId) {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.GET_DATA_BY_ROLE_ID,
        null,
        true,
        prepareRequiredHttpParams({
          roleId,
        })
      )
      .subscribe((resp) => { });
  }

  filterServiceTypeDetailsBySearchOptionsbranch(
    serachValue?: string
  ): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.PHYSICAL_BRANCH_LIST,
      null,
      true,
      prepareGetRequestHttpParams(null, null, {
        search: serachValue,
        divisionId: this.employeeForm.value.physicalLocationDivisionId,
      })
    );
  }

  onSelected(obj, str) {
    if (str == "division") {
      this.employeeForm.get("physicalLocationBranchName").setValue("");
      this.employeeForm.get("physicalLocationDivisionId").setValue(obj.id);
    } else {
      this.employeeForm.get("physicalLocationBranchId").setValue(obj.id);
    }
  }

  createEmployeeEditForm() {
    let employeeAddEditModel = new EmployeeAddEditModel();
    // create form controls dynamically from model class
    this.employeeForm = this.formBuilder.group({});
    Object.keys(employeeAddEditModel).forEach((key) => {
      if (key == "emailAddress") {
        this.employeeForm.addControl(
          key,
          new FormControl(
            employeeAddEditModel[key],
            Validators.compose([Validators.email, emailPattern])
          )
        );
      } else {
        this.employeeForm.addControl(
          key,
          new FormControl(employeeAddEditModel[key])
        );
      }
    });
    this.employeeForm.get("contactNumberCountryCode").setValue("+27");
    this.employeeForm.get("mobileNoCountryCode").setValue("+27");
    this.employeeForm.get("createdUserId").setValue(this.loggedInUserData.userId);
    this.employeeForm = setRequiredValidator(this.employeeForm, [
      "firstName",
      "lastName",
      "contactNumber",
      "userName",
      "physicalLocationDivisionId",
      "physicalLocationBranchId",
      "regionId",
      "divisionId",
      "districtId",
      "departmentId",
      "userLevelId",
      "designationId",
      "roleName",
      "reportingEmployeeName",
    ]);
  }
  resendVerficationLink() {
    this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.RESEND_VERIFICATION_LINK, {
      modifiedUserId: this.loggedInUserData.userId,
      employeeId: this.employeeId
    })
      .subscribe((response: IApplicationResponse) => {

      });
  }
  onSubmit() {
    this.isSubmitted = true;
    const isMobileNoDuplicate = new FindDuplicatePipe().transform(
      this.employeeForm.get("mobileNo").value,
      this.employeeForm,
      "mobileNo"
    );
    const isContactNumberNoDuplicate = new FindDuplicatePipe().transform(
      this.employeeForm.get("contactNumber").value,
      this.employeeForm,
      "contactNumber"
    );
    // if(isMobileNoDuplicate || isContactNumberNoDuplicate){
    //  return;
    // }
    if (!this.employeeForm.value.regionId || this.employeeForm.value.regionId?.length == 0) {
      this.seletRegion = false;
    }
    if (!this.employeeForm.value.divisionId || this.employeeForm.value.divisionId?.length == 0) {
      this.seletDivision = false;
    }
    if (!this.employeeForm.value.districtId || this.employeeForm.value.districtId?.length == 0) {
      this.seletDistrict = false;
    }
    console.log('this.employeeForm.value.branchId',this.employeeForm.value.branchId)
    if (!this.employeeForm.value.branchId || this.employeeForm.value.branchId?.length == 0) {
      this.seletBranch = false;
      this.snackbarService.openSnackbar(
        "Branch is Mandatory",
        ResponseMessageTypes.WARNING
      );
    }
    let invalid = (this.isInternalUser && !this.employeeForm.get('employeeNo').value) || !this.isInternalUser && !this.employeeForm.get('vendorNo').value || !this.isInternalUser && !this.employeeForm.get('mobileNo').value ? true : false;
    if (this.employeeForm.invalid || invalid) {
      return;
    }
    let obj = this.employeeForm.getRawValue();
    obj.userId = this.employeeUserId;
    if (typeof obj.contactNumber === "string") {
      obj.contactNumber = obj.contactNumber.split(" ").join("");
      obj.contactNumber = obj.contactNumber.replace(/"/g, "");
    }
    if (typeof obj.mobileNo === "string") {
      obj.mobileNo = obj.mobileNo.split(" ").join("");
      obj.mobileNo = obj.mobileNo.replace(/"/g, "");
    }
    obj.status = obj.isActive ? "Active" : "Inactive";
    obj.terminationDate = obj?.terminationDate ? this.momentService.toMoment(obj?.terminationDate).format('YYYY-MM-DD') : null;
    let formData = new FormData();
    formData.append("data", JSON.stringify(obj));
    this.rxjsService.setFormChangeDetectionProperty(true);
    let crudService: Observable<IApplicationResponse> = !this.employeeId
      ? this.crudService.create(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        SalesModuleApiSuffixModels.SALES_API_EMPLOYEE_LISTING,
        formData
      )
      : this.crudService.update(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        SalesModuleApiSuffixModels.SALES_API_EMPLOYEE_LISTING,
        formData
      );
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.isSubmitted = false;
        this.router.navigate(["user/employee"]);
      }
    });
  }

  bindingDepartmentDropdown() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_DEPARTMENTS
      )
      .subscribe({
        next: (response) => {
          this.applicationResponse = response;
          this.departmentList = this.applicationResponse.resources;
        },
        error: (err) => (this.errorMessage = err),
      });
  }

  GetEmployeeById(param) {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.EMPLOYEE_DETAILS,
        undefined,
        false,
        prepareRequiredHttpParams(param)
      )
      .subscribe((resp) => { });
  }

  radioChange(e) {
    if (e.value === true) {
      this.internalUser = true;
    } else {
      this.internalUser = false;
    }
  }

  bindingDivisionDropdown() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_DIVISIONS
      )
      .subscribe({
        next: (response) => {
          this.applicationResponse = response;
          this.divisionList = this.applicationResponse.resources;
        },
        error: (err) => (this.errorMessage = err),
      });
  }

  bindingrolesDropdown() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_ROLES
      )
      .subscribe({
        next: (response) => {
          this.applicationResponse = response;
          this.rolesList = this.applicationResponse.resources;
        },
        error: (err) => (this.errorMessage = err),
      });
  }

  bindingskillsDropdown() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.Ux_dropdown_skills
      )
      .subscribe({
        next: (response) => {
          this.applicationResponse = response;
          this.skillList = this.applicationResponse.resources;
        },
        error: (err) => (this.errorMessage = err),
      });
  }

  bindingreportinglinesDropdown() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_EMPLOYEES
      )
      .subscribe({
        next: (response) => {
          this.applicationResponse = response;
          this.reportinglinesList = this.applicationResponse.resources;
        },
        error: (err) => (this.errorMessage = err),
      });
  }

  bindingdefaultLocationDropdown() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_SUBURBS
      )
      .subscribe({
        next: (response) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.applicationResponse = response;
          this.defaultLocationList = this.applicationResponse.resources;
        },
        error: (err) => (this.errorMessage = err),
      });
  }

  UseremailValidation(email) {
    if (email) {
      this.usernameEmail.username = email;
      this.crudService
        .get(
          ModulesBasedApiSuffix.IT_MANAGEMENT,
          UserModuleApiSuffixModels.UserEmail_Validation,
          undefined,
          false,
          prepareRequiredHttpParams(this.usernameEmail)
        )
        .subscribe((resp) => {
          if (!resp.resources.isSuccess) {
            this.isaldreadyexist = true;
            this.employeeForm.controls["userName"].setErrors({
              incorrect: true,
            });
          } else {
            this.isaldreadyexist = false;
            return false;
          }
        });
    } else {
      this.isaldreadyexist = false;
    }
  }

  EmployeeNoValidation(employeeNo) {
    if (employeeNo) {
      this.employeeNovalidation.employeeNo = employeeNo.toString();
      this.crudService
        .get(
          ModulesBasedApiSuffix.IT_MANAGEMENT,
          UserModuleApiSuffixModels.EMPLOYEENO_VALIDATE,
          undefined,
          false,
          prepareRequiredHttpParams(this.employeeNovalidation)
        )
        .subscribe((resp) => {
          if (!resp.resources.isSuccess) {
            this.isaldreadyempnoexist = true;
            // this.employeeForm.controls["employeeNo"].setErrors({
            //   incorrect: true,
            // });
          } else {
            this.isaldreadyempnoexist = false;
            return false;
          }
        });
    } else {
      this.isaldreadyempnoexist = false;
    }
  }

  openAuditModal() {
    this.isAudit = true;
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      SalesModuleApiSuffixModels.EMPLOYEE_AUDIT, undefined, false, prepareGetRequestHttpParams(null, null, {
        employeeId: this.employeeId
      }), 1).subscribe((response: IApplicationResponse) => {
        this.auditdetails = response.resources;

        this.rxjsService.setDialogOpenProperty(false);
      });
  }


  save() { }

  cancel() {
    this.router.navigate(["user/employee"]);
  }

  redirectToListPage() {
    this.router.navigate(["user/employee"]);
  }
  redirectToViewPage() {
    this.router.navigate(["/user/employee/view"], { queryParams: { id: this.employeeId } });
  }
}
