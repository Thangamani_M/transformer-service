import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IT_MANAGEMENT_COMPONENT, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'employee-view-management',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee.component.scss'],
})
export class EmployeeViewComponent implements OnInit {
  id;
  details;
  auditdetails;
  isAudit: boolean = false;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }
  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    public dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private router: Router) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params.id;
    })

  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.onBootstrapModalEventChanges();
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      SalesModuleApiSuffixModels.SALES_API_EMPLOYEE_DETAILS, undefined, false, prepareGetRequestHttpParams(null, null, {
        employeeId: this.id
      }), 1).subscribe((response: IApplicationResponse) => {
        this.details = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][IT_MANAGEMENT_COMPONENT.EMPLOYEE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onBootstrapModalEventChanges(): void {
    $("#auditModal").on("shown.bs.modal", () => {
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#auditModal").on("hidden.bs.modal", () => {
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  openAuditModal() {
    this.isAudit = true;
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      SalesModuleApiSuffixModels.EMPLOYEE_AUDIT, undefined, false, prepareGetRequestHttpParams(null, null, {
        employeeId: this.id
      }), 1).subscribe((response: IApplicationResponse) => {
        this.auditdetails = response.resources;

        this.rxjsService.setDialogOpenProperty(false);
      });
  }

  editEmployee() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["/user/employee/add-edit"], { queryParams: { id: this.id } });
  }

  redirectToListPage() {
    this.router.navigate(['user/employee'])
  }
}

