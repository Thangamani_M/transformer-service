import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { EmployeeAddEditComponent, EmployeeListComponent, EmployeeRoutingModule } from '@user/components/employee';
import { RoleModule } from '../role';
import { EmployeeViewComponent } from './add-edit-list/employee-view.component';

@NgModule({
  declarations: [EmployeeAddEditComponent, EmployeeViewComponent, EmployeeListComponent],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
   RoleModule
  ],
})
export class EmployeeModule {


}
