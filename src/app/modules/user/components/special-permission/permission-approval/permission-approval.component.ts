import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LeavePermissionApprovalModel, LeavePermissionDetailsModel } from '@app/modules/user/models';
import { AppState } from '@app/reducers';
import { AlertService, CrudService, RxjsService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-permission-approval',
  templateUrl: './permission-approval.component.html',
  styleUrls: ['./permission-approval.component.scss']
})
export class PermissionApprovalComponent implements OnInit {
  specialPermissionId: any;
  loggedUser: UserLogin;
  applicationResponse: IApplicationResponse;
  uxSpecialPermissionStatus: any;
  uxUsers: any;
  leavePermissionModel: LeavePermissionDetailsModel;
  leaveApprovalModel: LeavePermissionApprovalModel;
  permissionApprovalForm: FormGroup;
  userId: any;
  selectStatus: any;
  approvalStatus: boolean = false;
  isButtonDisabled: boolean = false;
  selectStatusDeclined: boolean = false;

  constructor(
    private rxjsService: RxjsService,
    private httpService: CrudService,
    private formBuilder: FormBuilder,
    private router: Router,
    private alertService: AlertService,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.specialPermissionId = this.router.getCurrentNavigation().extras.state.id;
    }
  }

  ngOnInit(): void {
    this.leavePermissionModel = new LeavePermissionDetailsModel();
    this.getUXSpecialPermissionStatus();
    this.permissionApprovalForm = this.formBuilder.group({
      assignedUserId: [''],
      doaComment: [''],
      specialPermissionStatusId: ['', Validators.required]
    });
    if (this.specialPermissionId != null)
      this.getLeaveApprovalDetails(this.specialPermissionId);
    this.getUXUser();
  }

  getUXSpecialPermissionStatus(): void {
    this.httpService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_SPECIAL_PERMISSIONS_STATUS)
      .subscribe(resp => {
        this.applicationResponse = resp;
        this.uxSpecialPermissionStatus = this.applicationResponse.resources.filter(function (rqn: any) {
          //add only required status for select
          return rqn.displayName == "Approved" || rqn.displayName == "Declined";
        });
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  getUXUser(): void {
    this.httpService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_USERS_ALL)
      .subscribe(resp => {
        this.applicationResponse = resp;
        this.uxUsers = this.applicationResponse.resources.filter(user => user.id !== this.leavePermissionModel.userId);
        this.permissionApprovalForm.patchValue(this.applicationResponse.resources);
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  getLeaveApprovalDetails(Id: string) {
    this.httpService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.SPECIAL_PERMISSIONS, Id)
      .subscribe(resp => {
        this.applicationResponse = resp;
        this.leavePermissionModel = this.applicationResponse.resources;
        if (this.leavePermissionModel.status === "Approved")
          this.approvalStatus = true;
        this.permissionApprovalForm.patchValue(this.applicationResponse.resources);
        //to select default value 'please select' on approval mode
        if (!this.uxSpecialPermissionStatus.some(x => x.id == this.permissionApprovalForm.value.specialPermissionStatusId))
          this.permissionApprovalForm.patchValue({
            specialPermissionStatusId: ""
          });
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  Save() {
    this.selectStatus = false;
    if (this.permissionApprovalForm.invalid)
      this.permissionApprovalForm.markAllAsTouched();
    this.leaveApprovalModel = new LeavePermissionApprovalModel();
    const leaveApprovalDetail = this.permissionApprovalForm.value;
    let status = this.uxSpecialPermissionStatus.find(a => a['id'] === leaveApprovalDetail.specialPermissionStatusId);
    if (status === undefined) {
      this.selectStatus = true;
      return;
    }
    this.leaveApprovalModel.autoExpiryDate = this.leavePermissionModel.validToDate;
    this.leaveApprovalModel.approvalByUserId = this.loggedUser.userId;
    this.leaveApprovalModel.specialPermissionId = this.leavePermissionModel.specialPermissionId;
    this.leaveApprovalModel.doaComment = leaveApprovalDetail.doaComment;
    this.leaveApprovalModel.assignedUserId = leaveApprovalDetail.assignedUserId;
    this.leaveApprovalModel.specialPermissionStatusId = leaveApprovalDetail.specialPermissionStatusId;
    this.leaveApprovalModel.specialPermissionApprovalId = this.leavePermissionModel.specialPermissionApprovalId;
    this.leaveApprovalModel.createdUserId = this.loggedUser?.userId
    this.isButtonDisabled = true;
    this.httpService.create(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.SPECIAL_PERMISSIONS_APPROVAL, this.leaveApprovalModel)
      .subscribe(response => {
        this.AfterLeaveApproval(response);
      });
  }
  AfterLeaveApproval(response): void {
    if (response.statusCode == 200) {
      this.router.navigate(['/user/leave-user/approval-list']);

    } else {
      this.isButtonDisabled = false;
      this.alertService.processAlert(response);
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  Statuschange(e) {
    this.selectStatus = false;

    let selectElementText = event.target['options']
    [event.target['options'].selectedIndex].text;

    if (selectElementText === "Declined" || selectElementText === "declined") {
      if (this.permissionApprovalForm.controls['req_approval_comments'] === undefined) {
        this.selectStatusDeclined = true;
        this.permissionApprovalForm.get('doaComment').setValidators([Validators.required]);
        this.permissionApprovalForm.get('doaComment').updateValueAndValidity();
      }
    }
    else {
      this.selectStatusDeclined = false;
      this.permissionApprovalForm.get('doaComment').clearValidators();
      this.permissionApprovalForm.get('doaComment').updateValueAndValidity();
    }

  }
  isCommented(e) {
    let selectElementText = event.target['options']
    [event.target['options'].selectedIndex].text;

    if (selectElementText === undefined || selectElementText === null || selectElementText === "") {
      this.selectStatusDeclined = true;
      this.permissionApprovalForm.get('doaComment').setValidators([Validators.required]);
      this.permissionApprovalForm.get('doaComment').updateValueAndValidity();
    }
    else {
      this.selectStatusDeclined = false;
      this.permissionApprovalForm.get('doaComment').clearValidators();
      this.permissionApprovalForm.get('doaComment').updateValueAndValidity();
    }
  }

  validateApproveDeclineReason() {
    let declineReasonComment = this.permissionApprovalForm.get('doaComment').value;

    if (declineReasonComment != "" || declineReasonComment != null || declineReasonComment != undefined || declineReasonComment != " ") {
      this.selectStatusDeclined = false;
      this.permissionApprovalForm.get('doaComment').clearValidators();
      this.permissionApprovalForm.get('doaComment').updateValueAndValidity();
      this.permissionApprovalForm.controls["doaComment"].setValue(declineReasonComment);
    }
    else {
      this.selectStatusDeclined = true;
      this.permissionApprovalForm.get('doaComment').setValidators([Validators.required]);
      this.permissionApprovalForm.get('doaComment').updateValueAndValidity();
    }


  }


}
