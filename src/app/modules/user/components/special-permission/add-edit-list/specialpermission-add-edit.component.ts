import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Specialpermission } from '@user/models';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-specialpermission-add-edit',
  templateUrl: './specialpermission-add-edit.component.html',
})
export class SpecialpermissionAddEditComponent implements OnInit {
  @Input() id: string;
  specialPermissionAddEditForm: FormGroup;
  applicationResponse: IApplicationResponse;
  errorMessage: string;
  btnName: string;
  userList: any;
  roleList: any;
  statusList: any;
  minDate = new Date();
  maxDate = new Date();
  dropdownsAndData=[]
  constructor(@Inject(MAT_DIALOG_DATA) public data: Specialpermission,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService : CrudService,
   ) {

  }

  ngOnInit(): void {
    if (this.data.specialPermissionId) {
      this.btnName = 'Update';
    } else {
      this.btnName = 'Create';
    }
    this.specialPermissionAddEditForm = this.formBuilder.group({
      userId: this.data.userId || '',
      roleId: this.data.roleId || '',
      specialPermissionStatusId: this.data.specialPermissionStatusId || '',
      validFromDate: this.data.validFromDate || '',
      validToDate: this.data.validToDate
    });
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_USERS),
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_ROLES),
        this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
          UserModuleApiSuffixModels.UX_SPECIAL_PERMISSION_STATUS)
    ];
    this.loadDropdownData(this.dropdownsAndData);
  }

  loadDropdownData(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.userList = resp.resources;
              break;

            case 1:
              this.roleList = resp.resources;
              break;

              case 2:
                this.statusList = resp.resources;
                break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  save(): void {
    if (this.specialPermissionAddEditForm.invalid) {
      this.specialPermissionAddEditForm.markAllAsTouched();
      return;
    }
    let API;
    const specialpermission = { ...this.data, ...this.specialPermissionAddEditForm.value }
    if (specialpermission.validFromDate || specialpermission.validToDate) {
      specialpermission.validFromDate = (new Date(Date.parse(specialpermission.validFromDate)).toDateString());
      specialpermission.validToDate = (new Date(Date.parse(specialpermission.validToDate)).toDateString());
    }
    if (specialpermission.specialPermissionId != null) {
      API = this.crudService.update(ModulesBasedApiSuffix.IT_MANAGEMENT,UserModuleApiSuffixModels.SPECIAL_PERMISSIONS, specialpermission);
    }
    else {
      API = this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,UserModuleApiSuffixModels.SPECIAL_PERMISSIONS, specialpermission);
    }
    API.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.dialog.closeAll();
      }
    })

  }

  FromDateChange(type: string, event: MatDatepickerInputEvent<Date>) {
    this.maxDate = event.value;

  }


}
