import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-specialpermission-list',
  templateUrl: './specialpermission-list.component.html',
})
export class SpecialpermissionListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any = {
    tableCaption: "Leave Management Request List",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Configuration ', relativeRouterUrl: '' }, { displayName: 'Leave Management Request List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Leave Management Request List',
          dataKey: 'specialPermissionId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: true,
          enableAction: true,
          enableAddActionBtn: true,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: false,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'requestTypeName', header: 'Request Type', width: '150px' },
          { field: 'applyType', header: 'Apply Type', width: '150px' },
          { field: 'leaveType', header: 'Leave Type', width: '150px' },
          { field: 'period', header: 'Period', width: '300px' },
          { field: 'nominatedTo', header: 'Nominated To', width: '150px' },
          { field: 'approvedBy', header: 'Approved By', width: '150px' },
          { field: 'status', header: 'Status', width: '150px' },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: UserModuleApiSuffixModels.LEAVE_MANAGEMENT_USER,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT
        }]
    }
  }

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private router: Router,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
    public dialogService: DialogService) {
    super();
  }

  ngOnInit(): void {
    this.getDivisions();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][CONFIGURATION_COMPONENT.LEAVE_MANAGEMENT_REQUEST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getDivisions(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.LEAVE_MANAGEMENT_USER, undefined, false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.dataList = response.resources
          this.dataList.map(item => {
            if (item.status == "Declined") {
              item.cssClass = "status-label-yellow";
            }
            if (item.status == "Approved") {
              item.cssClass = "status-label-green";
            }
            if (item.status == "Pending") {
              item.cssClass = "status-label-blue";
            }
          })
          this.totalRecords = response.totalCount;
          this.loading = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/user/leave-user/list/add-request']);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getDivisions(row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.VIEW:
        this.router.navigate(['/user/leave-user/list/request-view'], { state: { id: row['specialPermissionId'] } });
        break;
    }
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
