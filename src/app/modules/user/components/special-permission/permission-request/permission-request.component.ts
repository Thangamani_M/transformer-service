import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material';
import { Router } from '@angular/router';
import { LeavePermissionUserRequestModel } from '@app/modules/user/models';
import { AppState } from '@app/reducers';
import { AlertService, CrudService, RxjsService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-permission-request',
  templateUrl: './permission-request.component.html',
  styleUrls: ['./permission-request.component.scss']
})
export class PermissionRequestComponent implements OnInit {

  selected;
  uxRequestType: any;
  uxLeaveApplyType: any;
  uxUsers: any;
  applicationResponse: IApplicationResponse;
  permissionRequestForm: FormGroup;
  minDate = new Date();
  maxDate = new Date();
  leavePermission: any;
  specialPermissionId: any;
  loggedUser: UserLogin;
  setLeave: boolean;
  setApply: boolean;
  leaveType: boolean;
  dataRangeFrom: any;
  dataRangeTo: any;
  selectedMoments: any=[];
  dataRange: boolean = false;
  public todayDate: any = new Date();
  //public selectedMoments = [new Date(2019, 1, 12, 10, 30), new Date(2019, 3, 21, 20, 30)];

  //Added newly...
  showReason: boolean;
  dateRangeFrom: boolean = false;
  dateRangeTo: boolean = false;
  leaveTypeName: boolean = false;
  setNomination: boolean;
  ischecked: boolean;
  isnotchecked: boolean;

  constructor(
    private rxjsService: RxjsService,
    private httpService: CrudService,
    private formBuilder: FormBuilder,
    private router: Router,
    private alertService: AlertService,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.specialPermissionId = this.router.getCurrentNavigation().extras.state.id;
    }
  }

  ngOnInit(): void {
    this.getUXRequestType();
    this.getUXUser();
    this.leavePermission = new LeavePermissionUserRequestModel();
    if (this.specialPermissionId != null) {
      this.getLeavePermissionDetails(this.specialPermissionId);
    }
    this.permissionRequestForm = this.formBuilder.group({
      requestTypeId: ['', Validators.required],
      leaveApplyTypeId: [''],
      reason: ['', Validators.required],
      isOffSite: ['1'],
      isOnsite: ['1'],
      assignedUserId: [''],
      periodFrom: ['', Validators.required],
      periodTo: ['', Validators.required]
    });
  }

  ngAfterViewInit(): void {
  }

  getUXRequestType(): void {
    this.httpService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REQUEST_TYPES)
      .subscribe(resp => {
        this.applicationResponse = resp;
        this.uxRequestType = this.applicationResponse.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getUXLeaveApplyType(): void {
    this.httpService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_LEAVE_APPLY_TYPES)
      .subscribe(resp => {
        this.applicationResponse = resp;
        this.uxLeaveApplyType = this.applicationResponse.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getUXUser(): void {
    this.httpService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_USERS_ALL)
      .subscribe(resp => {
        this.applicationResponse = resp;
        this.uxUsers = this.applicationResponse.resources.filter(user => user.id !== this.loggedUser.userId);
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  FromDateChange(type: string, event: MatDatepickerInputEvent<Date>) {
    this.maxDate = event.value;
  }

  getLeavePermissionDetails(id: string) {

    this.httpService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.SPECIAL_PERMISSIONS, id).subscribe({
      next: response => {
        if (response.statusCode == 200 && response.resources != null) {

          if (response.resources['requestTypeName'] === "Leave" || response.resources['requestTypeName'] === "leave") {
            if (response.resources['leaveType'] === "Other") {
              this.setControlsForOtherinLeaveType("Leave", true);
            }
            else {
              this.setControls("Leave");
            }
          }
          else if (response.resources['requestTypeName'] === "Other" || response.resources['requestTypeName'] === "other") {
            this.setControls("Other");
          }
          else {
            this.setControls("Out Of Office");
          }

          if (response.resources["nominatedTo"] !== null) {
            this.isNomination("1");
          }
          else {
            this.isNomination("0");
          }

          response.resources['isSelf'] = response.resources['isSelf'] === true ? '1' : '2';
          response.resources['isOnsite'] = response.resources['isOnsite'] === true ? '1' : '2';
          response.resources['periodFrom'] = new Date(response.resources['validFromDate']);
          response.resources['periodTo'] = new Date(response.resources['validToDate']);
          this.selectedMoments = [response.resources['validFromDate'], response.resources['validToDate']];
          this.permissionRequestForm.patchValue(response.resources);
        }
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);

  }

  Save() {
    if (this.permissionRequestForm.invalid) {
      this.permissionRequestForm.markAllAsTouched();
    }

    if (this.permissionRequestForm.controls["leaveApplyTypeId"].value === Guid.EMPTY)
      this.permissionRequestForm.controls["leaveApplyTypeId"].setValue("");

    //this.dataRange = false;
    this.dateRangeFrom = false;
    this.dateRangeTo = false;
    this.leaveType = false;

    //this.selectedMoments[1] = new Date(2020, 5, 21, 20, 30);
    //this.selectedMoments = [new Date(2019, 1, 12, 10, 30), new Date(2019, 3, 21, 20, 30)];

    const permissionData = this.permissionRequestForm.value;
    this.selectedMoments[0] =new Date(permissionData?.periodFrom)
    this.selectedMoments[1] = new Date(permissionData?.periodTo)

    this.leavePermission.userId = this.loggedUser.userId;
    this.leavePermission.roleId = this.loggedUser.roleId;

    if (this.setLeave === true && permissionData.leaveApplyTypeId === "" || permissionData.leaveApplyTypeId === null) {
      this.leaveType = true;
      return;
    }

    if (this.selectedMoments[0] === null || this.selectedMoments[1] === null) {
      this.dateRangeFrom = true;
      this.dateRangeTo = true;
      return;
    }

    if (this.selectedMoments[0] === undefined || this.selectedMoments[0] === null) {
      this.dateRangeFrom = true;
      return;
    }

    if (this.selectedMoments[1] === undefined || this.selectedMoments[1] === null) {
      this.dateRangeTo = true;
      return;
    }

    this.leavePermission.validFromDate = this.selectedMoments[0].toISOString();
    this.leavePermission.validToDate = this.selectedMoments[1].toISOString();;
    this.leavePermission.requestTypeId = permissionData.requestTypeId;
    if (this.setApply === true) {
      this.leavePermission.isOnsite = permissionData['isOnsite'] === '1' ? true : false;
      this.leavePermission.isOffSite = permissionData['isOnsite'] !== '1' ? true : false;
    }
    else {
      this.leavePermission.isOnsite = false;
      this.leavePermission.isOffSite = false;
    }

    this.leavePermission.isSelf = permissionData['accessType'] === '1' ? true : false;
    this.leavePermission.reason = permissionData.reason;
    this.leavePermission.assignedUserId = permissionData.assignedUserId;
    this.leavePermission.leaveApplyTypeId = permissionData.leaveApplyTypeId;
    if (this.specialPermissionId != null) {
      this.leavePermission.specialPermissionId = this.specialPermissionId;
      this.httpService.update(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.SPECIAL_PERMISSIONS, this.leavePermission)
        .subscribe((response) => {
          this.AfterLeaveRequest(response);
        });
    }
    else {
      this.httpService.create(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.SPECIAL_PERMISSIONS, this.leavePermission)
        .subscribe((response) => {
          this.AfterLeaveRequest(response);
        });
    }

  }

  AfterLeaveRequest(response): void {
    if (response.statusCode == 200) {
      this.router.navigate(['/user/leave-user/list']);

    } else {
      this.alertService.processAlert(response);
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  changeRequestType(e) {

    let selectElementText = event.target['options']
    [event.target['options'].selectedIndex].text;

    if (selectElementText === "Other" || selectElementText === "other") {
      this.showReason = true;
      this.setReasonRequired();
    }
    else {
      this.showReason = false;
      this.setReasonNotRequired();
    }
    this.setControls(selectElementText);
  }

  setControls(status: string) {

    if (status === "Out Of Office") {
      this.setLeave = false;
      this.setApply = true;
      this.showReason = false;
      this.setReasonNotRequired();
    }
    else if (status === "Other") {
      this.showReason = true;
      this.setLeave = false;
      this.setApply = false;
    }
    else {
      this.getUXLeaveApplyType();
      this.setLeave = true;
      this.setApply = false;
      this.showReason = false;
    }
  }

  changeLeaveType(e) {

    this.leaveType = false;

    let selectElementText = event.target['options']
    [event.target['options'].selectedIndex].text;

    if (selectElementText === "Other" || selectElementText === "other") {
      this.showReason = true;
      this.setReasonRequired();

    }
    else {
      this.showReason = false;
      this.setReasonNotRequired();
    }
  }

  openViewPage() {
    this.router.navigate(['/user/leave-user/request-view'], { state: { id: this.specialPermissionId } });
  }

  setControlsForOtherinLeaveType(status: string, isOther: boolean) {

    if (status === "Leave" && isOther === true) {
      this.getUXLeaveApplyType();
      this.showReason = true;
      this.setLeave = true;
      this.setApply = false;
      this.leaveTypeName = true;
    }
  }

  setReasonRequired() {
    this.permissionRequestForm.get('reason').setValidators([Validators.required]);
    this.permissionRequestForm.get('reason').updateValueAndValidity();
    this.permissionRequestForm.controls['reason'].setValue('');

  }
  setReasonNotRequired() {
    this.permissionRequestForm.get('reason').clearValidators();
    this.permissionRequestForm.get('reason').updateValueAndValidity();
    this.permissionRequestForm.controls['reason'].setValue('');

  }

  isNomination(e) {

    if (e === "1") {
      this.setNomination = true;
      this.ischecked = true;
      this.isnotchecked = false;
      this.permissionRequestForm.controls['assignedUserId'].setValue('');
    }
    else {
      this.setNomination = false;
      this.ischecked = false;
      this.isnotchecked = true;
      this.permissionRequestForm.controls['assignedUserId'].setValue('');
    }
  }

}
