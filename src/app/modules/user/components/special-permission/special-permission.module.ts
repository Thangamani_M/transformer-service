import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { PermissionApprovalComponent, PermissionApprovalListComponent, PermissionRequestComponent, PermissionUserViewComponent, SpecialpermissionAddEditComponent, SpecialpermissionListComponent, SpecialPermissionRoutingModule } from '@user/components/special-permission';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';


@NgModule({
  declarations: [SpecialpermissionAddEditComponent, SpecialpermissionListComponent,
    PermissionRequestComponent, PermissionApprovalListComponent, PermissionUserViewComponent,
     PermissionApprovalComponent],
  imports: [
    CommonModule,
    SpecialPermissionRoutingModule,
    ReactiveFormsModule,
    FormsModule, LayoutModule,
    MaterialModule,
    SharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ],
  entryComponents: [SpecialpermissionAddEditComponent]
})
export class SpecialPermissionModule { }
