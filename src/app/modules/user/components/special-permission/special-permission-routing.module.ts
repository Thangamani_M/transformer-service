import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionApprovalComponent, PermissionApprovalListComponent, PermissionRequestComponent, PermissionUserViewComponent, SpecialpermissionListComponent } from '@user/components/special-permission';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: 'list', component: SpecialpermissionListComponent, canActivate:[AuthGuard],data: { title: 'Leave Permission Request List' } },
    { path: 'list/add-request', component: PermissionRequestComponent, canActivate:[AuthGuard],data: { title: 'Create - Edit request' } },
    { path: 'approval-list', component: PermissionApprovalListComponent,canActivate:[AuthGuard], data: { title: 'Leave Permission Approval List' } },
    { path: 'list/request-view', component: PermissionUserViewComponent,canActivate:[AuthGuard], data: { title: 'Leave Requester View Info' } },
    { path: 'approval-list/approval', component: PermissionApprovalComponent,canActivate:[AuthGuard], data: { title: 'Leave Permission Approval' } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class SpecialPermissionRoutingModule { }
