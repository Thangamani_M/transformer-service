import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LeavePermissionDetailsModel } from '@app/modules/user/models/specialpermission';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CONFIGURATION_COMPONENT } from '@modules/others/configuration/utils/configuration-component.enum';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-permission-user-view',
  templateUrl: './permission-user-view.component.html',
  styleUrls: ['./permission-user-view.component.scss']
})
export class PermissionUserViewComponent implements OnInit {
  specialPermissionId: any;
  leavePermissionDetailModel: LeavePermissionDetailsModel;
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{}]
    }
  }
  constructor(private rxjsService: RxjsService,
    private httpService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService,
    private router: Router) {
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.specialPermissionId = this.router.getCurrentNavigation().extras.state.id;
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][CONFIGURATION_COMPONENT.LEAVE_MANAGEMENT_REQUEST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.leavePermissionDetailModel = new LeavePermissionDetailsModel();
    if (this.specialPermissionId != null)
      this.GetLeavePermissionDetails(this.specialPermissionId);
  }
  GetLeavePermissionDetails(id: any) {
    this.httpService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.SPECIAL_PERMISSIONS, id).subscribe({
      next: response => {
        if (response.statusCode == 200 && response.resources != null) {
          this.leavePermissionDetailModel = response.resources;

        }

      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  editLeavePermission(): void {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/user/leave-user/list/add-request'], { state: { id: this.specialPermissionId } });
  }

}
