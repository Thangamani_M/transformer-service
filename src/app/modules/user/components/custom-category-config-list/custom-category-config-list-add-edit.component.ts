
import { I } from '@angular/cdk/keycodes';
import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, MinLengthValidator, Validators } from '@angular/forms';
import { MatDialog, MatOption } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { CustomCategoryListConfigItemListModel, CustomCategoryListConfigModel } from '@modules/user/models/custom-category-list-config.model';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-custom-category-config-list-add-edit',
  templateUrl: './custom-category-config-list-add-edit.component.html'
})
export class CustomCategoryConfigListAddEditComponent implements OnInit {
  proactivePatrolId: any
  divisionDropDown: any = [];
  customCategoryListForm: FormGroup;
  proactivePatrolTypeList: FormArray;
  loggedUser: any;
  hex;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 10 } });
  cmcSmsGroupDetails: any;
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  public colorCode: string = '#278ce2';
  onlyAlphabetics = /^[A-Za-z]+$/;

  // numRegex = /^[1-9]\d*(\.\d+)?$/;
  numRegex = '^\-{0,1}\d+(.\d+){0,1}$';
  isDuplicate: boolean;
  isDuplicateNumber: boolean;
  @ViewChildren('input') rows: QueryList<any>;

  @ViewChild('allSelectedDivision', { static: false }) public allSelectedDivision: MatOption;

  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private router: Router,
    private httpCancelService: HttpCancelService, private store: Store<AppState>, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.proactivePatrolId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }
  ngOnInit() {
    this.createcustomCategoryListForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    if (this.proactivePatrolId) {

      this.getCmcSmsDetailsById().subscribe((response: IApplicationResponse) => {

        this.rxjsService.setGlobalLoaderProperty(false);
        let cmcSmsGroup = new CustomCategoryListConfigModel(response.resources);

        this.cmcSmsGroupDetails = response.resources;
        this.customCategoryListForm.patchValue(cmcSmsGroup);

        this.proactivePatrolTypeList = this.getproactivePatrolTypeListArray;
        response.resources.customCategoryListItemList.forEach((proactivePatrolTypeListModel) => {
          this.proactivePatrolTypeList.push(this.createproactivePatrolTypeListModel(proactivePatrolTypeListModel));

        });
      })
    } else {
      this.proactivePatrolTypeList = this.getproactivePatrolTypeListArray;
      this.proactivePatrolTypeList.push(this.createproactivePatrolTypeListModel());
    }
  }


  createcustomCategoryListForm(): void {
    let proactivePatrolModel = new CustomCategoryListConfigModel();
    this.customCategoryListForm = this.formBuilder.group({
      customCategoryListItemList: this.formBuilder.array([])
    });
    Object.keys(proactivePatrolModel).forEach((key) => {
      this.customCategoryListForm.addControl(key, new FormControl(proactivePatrolModel[key]));
    });
    this.customCategoryListForm = setRequiredValidator(this.customCategoryListForm, ["customCategoryListName", "description", "customCategoryListItemList"]);
    this.customCategoryListForm.get('minLength').setValidators([Validators.required, Validators.min(1), Validators.max(100)]);
    this.customCategoryListForm.get('length').setValidators([Validators.required, Validators.min(1), Validators.max(100)]);
    this.customCategoryListForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.customCategoryListForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    this.customCategoryListForm.get('customCategoryTypeId').valueChanges.subscribe(val => {
      if (val) {
        if (val == '2' || val == '3') {
          this.customCategoryListForm.get('decimals').setValidators([Validators.required, Validators.min(1), Validators.max(100)]);
          this.customCategoryListForm.get('isActive').setValue(false)
          this.customCategoryListForm.get('isActive').disable()
        } else {
          this.customCategoryListForm.get('isActive').setValue(true)
          this.customCategoryListForm.get('isActive').enable()
          this.customCategoryListForm = clearFormControlValidators(this.customCategoryListForm, ["decimals"]);
        }
      }
    })
  }

  //Create FormArray
  get getproactivePatrolTypeListArray(): FormArray {
    if (!this.customCategoryListForm) return;
    return this.customCategoryListForm.get("customCategoryListItemList") as FormArray;
  }
  //Create FormArray controls
  createproactivePatrolTypeListModel(proactivePatrolTypeListModel?: CustomCategoryListConfigItemListModel): FormGroup {
    let proactivePatrolTypeListFormControlModel = new CustomCategoryListConfigItemListModel(proactivePatrolTypeListModel);
    let formControls = {};
    Object.keys(proactivePatrolTypeListFormControlModel).forEach((key) => {
      formControls[key] = [{ value: proactivePatrolTypeListFormControlModel[key], disabled: false }, [Validators.required]];
    });

    let forms = this.formBuilder.group(formControls);
    forms.get('listItemName').valueChanges.subscribe(val => {
      if (val) {
        let dat = val
        let value = dat.split(".")
        if (dat.includes(".")) {
          if (value[1].length >= this.customCategoryListForm.get('decimals').value + 1) {
            this.snackbarService.openSnackbar("Minumun " + this.customCategoryListForm.get('decimals').value + " Decimals Required", ResponseMessageTypes.WARNING);
            forms.get('listItemName').setValue('')
            return
          }

        }
        let minLengthForMoneyAndFloat = Number(this.customCategoryListForm.get('minLength').value) + Number(this.customCategoryListForm.get("decimals").value) + 1
        let maxLengthForMoneyAndFloat = Number(this.customCategoryListForm.get('length').value) + Number(this.customCategoryListForm.get("decimals").value) + 1
        if (this.customCategoryListForm.get('customCategoryTypeId').value == 3 || this.customCategoryListForm.get('customCategoryTypeId').value == 2) {
          forms.get("listItemName").setValidators([Validators.required, Validators.minLength(minLengthForMoneyAndFloat),
          Validators.maxLength(maxLengthForMoneyAndFloat)]);
          forms.updateValueAndValidity()
          // if(value[0].length >= this.customCategoryListForm.get('minLength').value){
          //     forms.get("listItemName").setValidators([Validators.required, Validators.minLength(this.customCategoryListForm.get('minLength').value)])
          //     return
          //   }
        }

      }
    })
    return forms
  }

  //Get Details
  getCmcSmsDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_LIST,
      this.proactivePatrolId
    );
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }
  //Add Employee Details
  addCmcSmsGroupEmployee(index?: number): void {
    if (this.customCategoryListForm.get('customCategoryTypeId').value == '2' || this.customCategoryListForm.get('customCategoryTypeId').value == '3')
      if (this.getproactivePatrolTypeListArray.controls[index].get('listItemName').value.length < this.customCategoryListForm.get('minLength').value) {
        return this.snackbarService.openSnackbar("Minumun length should be " + this.customCategoryListForm.get('minLength').value + "  Required", ResponseMessageTypes.WARNING);
      }

    if (this.getproactivePatrolTypeListArray.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.proactivePatrolTypeList = this.getproactivePatrolTypeListArray;
    let proactivePatrolTypeListModel = new CustomCategoryListConfigItemListModel();
    this.proactivePatrolTypeList.insert(0, this.createproactivePatrolTypeListModel(proactivePatrolTypeListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeCmcSmsEmployee(i: number): void {
    if (this.getproactivePatrolTypeListArray.controls[i].value.proactivePatrolTypeName == '') {
      this.getproactivePatrolTypeListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getproactivePatrolTypeListArray.controls[i].value.proactivePatrolTypeId && this.getproactivePatrolTypeListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PROACTIVE_PATROL_TYPE,
          this.getproactivePatrolTypeListArray.controls[i].value.proactivePatrolTypeId).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
              this.getproactivePatrolTypeListArray.removeAt(i);
            }
            if (this.getproactivePatrolTypeListArray.length === 0) {
              this.addCmcSmsGroupEmployee();
            };
          });
      }
      else {
        this.getproactivePatrolTypeListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  onSubmit(): void {
    if (this.customCategoryListForm.invalid) {
      return;
    }
    let formValue = this.customCategoryListForm.value;
    if (formValue.customCategoryTypeId == 2 || formValue.customCategoryTypeId == 3) {
      formValue.isActive = false;
    }


    if (formValue.customCategoryTypeId == 1) {
      formValue.decimals = 0
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (!this.proactivePatrolId) {

      let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_LIST, formValue)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.router.navigateByUrl('/user/custom-category-config-dashboard?tab=1');
        }
      })
    }
    else if (this.proactivePatrolId) {
      if (this.customCategoryListForm.get('customCategoryTypeId').value === '1') {
        let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_LIST, formValue)
        crudService.subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.router.navigateByUrl('/user/custom-category-config-dashboard?tab=1');
          }
        })
      }
      else {
        this.getproactivePatrolTypeListArray.controls.forEach((k) => {
          let minLengthForMoneyAndFloat = Number(this.customCategoryListForm.get('minLength').value) + Number(this.customCategoryListForm.get("decimals").value) + 1
          let maxLengthForMoneyAndFloat = Number(this.customCategoryListForm.get('length').value) + Number(this.customCategoryListForm.get("decimals").value) + 1
          if (k.value.listItemName.length >= minLengthForMoneyAndFloat && k.value.listItemName.length <= maxLengthForMoneyAndFloat) {
            let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_LIST, formValue)
            crudService.subscribe((response: IApplicationResponse) => {
              if (response.isSuccess) {
                this.router.navigateByUrl('/user/custom-category-config-dashboard?tab=1');
              }
            })
          }
          else {
            this.snackbarService.openSnackbar("List name should be " + minLengthForMoneyAndFloat + "  Required", ResponseMessageTypes.WARNING);
          }
        })
      }
    }
    // let crudService: Observable<IApplicationResponse> = (!this.proactivePatrolId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_LIST, formValue) :
    //   this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_LIST, formValue)
    // crudService.subscribe((response: IApplicationResponse) => {
    //   if (response.isSuccess) {
    //     this.router.navigateByUrl('/user/custom-category-config-dashboard?tab=1');
    //   }
    // })
  }
}
