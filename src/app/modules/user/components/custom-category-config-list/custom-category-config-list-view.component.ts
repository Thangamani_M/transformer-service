
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { IT_MANAGEMENT_COMPONENT } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-custom-category-config-list-view',
  templateUrl: './custom-category-config-list-view.component.html'
})
export class CustomCategoryConfigListViewComponent implements OnInit {
  proactivePatrolId: string;
  proactivePatrolDetails: any;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.proactivePatrolId = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getproactivePatrolDetailsById(this.proactivePatrolId);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][IT_MANAGEMENT_COMPONENT.CUSTOM_CATEGORY]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getproactivePatrolDetailsById(proactivePatrolId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_LIST, proactivePatrolId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.proactivePatrolDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[1].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    if (this.proactivePatrolId) {
      this.router.navigate(['user/custom-category-config-dashboard/custom-category-config-list/add-edit'], { queryParams: { id: this.proactivePatrolId } });
    }
  }
}


