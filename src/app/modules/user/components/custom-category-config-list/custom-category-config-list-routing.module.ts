import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomCategoryConfigListAddEditComponent } from './custom-category-config-list-add-edit.component';
import { CustomCategoryConfigListViewComponent } from './custom-category-config-list-view.component';



const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: CustomCategoryConfigListAddEditComponent, data: { title: 'Group Config List Add/Edit' } },
  { path: 'view', component: CustomCategoryConfigListViewComponent, data: { title: 'Group Config List View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CustomCategoryConfigListRoutingModule { }
