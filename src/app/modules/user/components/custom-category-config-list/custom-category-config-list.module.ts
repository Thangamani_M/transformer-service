import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CustomCategoryConfigListAddEditComponent } from './custom-category-config-list-add-edit.component';
import { CustomCategoryConfigListRoutingModule } from './custom-category-config-list-routing.module';
import { CustomCategoryConfigListViewComponent } from './custom-category-config-list-view.component';
@NgModule({
  declarations: [CustomCategoryConfigListAddEditComponent, CustomCategoryConfigListViewComponent],
  imports: [
    CommonModule,
    CustomCategoryConfigListRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class CustomCategoryConfigListModule { }
