import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
    CrudService, debounceTimeForSearchkeyword, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
    prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { SalesBoundaryModel } from '@modules/user/models/it-management-models';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
    selector: 'sales-boundary-add-edit',
    templateUrl: './sales-boundary-add-edit.component.html',
    styleUrls: ['./sales-boundary-list.component.scss']
})

export class SalesBoundaryAddEditComponent {
    Divisions = [];
    regions = [];
    Districts = [];
    salesBoundaryForm: FormGroup;
    branchDropdown;
    opsList = [];
    subBranches = [];
    provinceList = [];
    loggedInUserData: LoggedInUserModel;
    employees = [];
    boundaryLayerId;
    boundaryLayerName = "";
    boundaryId = "";
    selectedInitiatorOption;
    getLoopableObjectRequestObservable: Observable<IApplicationResponse>;

    constructor(private rxjsService: RxjsService, private router: Router, private activatedRoute: ActivatedRoute,
        private crudService: CrudService
        , private formBuilder: FormBuilder, private store: Store<AppState>) {
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(loggedInUserData), this.activatedRoute.queryParams]
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
            this.boundaryLayerId = response[1]['boundaryLayerId'];
            this.boundaryId = response[1]['id'];
        });
    }

    ngOnInit() {
        this.combineLatestNgrxStoreData();
        if (this.boundaryLayerId == '6') {
            this.boundaryLayerName = 'Reconnection';
        }
        else if (this.boundaryLayerId == '7') {
            this.boundaryLayerName = 'Residential';
        } else {
            this.boundaryLayerName = 'Small Commercial';
        }
        this.forkJoinRequests();
        this.createSalesBoundary();
        this.onFormControlChanges();
        this.getDetailsById();
    }

    createSalesBoundary() {
        let salesBoundaryModel = new SalesBoundaryModel();
        this.salesBoundaryForm = this.formBuilder.group({});
        Object.keys(salesBoundaryModel).forEach((key) => {
            this.salesBoundaryForm.addControl(key, new FormControl(salesBoundaryModel[key]));
        });
        this.salesBoundaryForm.get('boundaryTypeName').setValue('Sales');
        this.salesBoundaryForm.get('boundaryLayerName').setValue(this.boundaryLayerName);
        this.salesBoundaryForm = setRequiredValidator(this.salesBoundaryForm, ["boundaryName", "description", "branchId", "assignToName", "divisionId", "districtId", "regionId"]);
    }

    forkJoinRequests(): void {
        forkJoin([this.getRegions(),
        this.getOPSDropDown()]).subscribe((response) => {
            response.forEach((respObj: IApplicationResponse, ix: number) => {
                if (respObj.isSuccess && respObj.statusCode == 200) {
                    switch (ix) {
                        case 0:
                            this.regions = respObj.resources;
                            break;
                        case 1:
                            this.opsList = respObj.resources;
                            break;
                    }
                }
            });
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    getRegions(): Observable<IApplicationResponse> {
        return this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
            prepareGetRequestHttpParams(null, null, { CountryId: formConfigs.countryId }));
    }

    getOPSDropDown() {
        return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_OPS_UX, null, false, null);
    }

    getProvinceList() {
        this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_PROVINCES,
            undefined, false, prepareRequiredHttpParams({
                countryId: formConfigs.countryId
            }))
            .subscribe(resp => {
                if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
                    this.provinceList = resp.resources;
                }
            });
    }

    changeRegion() {
        this.salesBoundaryForm.get('divisionId').setValue('');
        this.salesBoundaryForm.get('districtId').setValue('');
        this.salesBoundaryForm.get('branchId').setValue('');
        this.Districts = [];
        this.Divisions = [];
        this.branchDropdown = [];
    }

    changeDivision() {
        this.salesBoundaryForm.get('districtId').setValue('');
        this.salesBoundaryForm.get('branchId').setValue('');
        this.Districts = [];
        this.branchDropdown = [];
    }

    changeDistrict() {
        this.salesBoundaryForm.get('branchId').setValue('');
        this.branchDropdown = [];
    }

    onFormControlChanges() {
        this.salesBoundaryForm.get('regionId').valueChanges.subscribe((regionId: string) => {
            if (!regionId) return;
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
                prepareGetRequestHttpParams(null, null,
                    { regionId }))
                .subscribe((response: IApplicationResponse) => {
                    if (response && response.resources && response.isSuccess) {
                        this.Divisions = response.resources;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                });
        });
        this.salesBoundaryForm.get('divisionId').valueChanges.subscribe((divisionId: string) => {
            if (!divisionId) return;
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
                prepareGetRequestHttpParams(null, null,
                    { divisionId }))
                .subscribe((response: IApplicationResponse) => {
                    if (response && response.resources && response.isSuccess) {
                        this.Districts = response.resources;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                });
        });
        this.salesBoundaryForm.get('districtId').valueChanges.subscribe((districtId: string) => {
            if (!districtId) return;
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
                prepareGetRequestHttpParams(null, null,
                    { districtId }))
                .subscribe((response: IApplicationResponse) => {
                    if (response && response.resources && response.isSuccess) {
                        this.branchDropdown = response.resources;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                });
        });
        this.onAssignToNameFormControlValueChanges();
    }

    onAssignToNameFormControlValueChanges(): void {
        this.salesBoundaryForm
            .get("assignToName")
            .valueChanges.pipe(
                debounceTime(debounceTimeForSearchkeyword),
                distinctUntilChanged()).subscribe((search: IApplicationResponse) => {
                    this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
                        UserModuleApiSuffixModels.GET_EMPLOYEE_SEARCH, null, true, prepareGetRequestHttpParams(null, null, {
                            search
                        }));
                });
    }

    onSelectedItemOption(selectedObject): void {
        if (!selectedObject) return;
        this.selectedInitiatorOption = selectedObject;
        this.salesBoundaryForm.patchValue({
            employeeFirstName: selectedObject.firstName, assignToId: selectedObject.employeeId, employeeLastName: selectedObject.lastName,
            employeeNo: selectedObject.employeeNo, initiatorEmailAddress: selectedObject.officialEmail ? selectedObject.officialEmail : selectedObject.email,
            fullName: selectedObject.fullName, assignToName: selectedObject.fullName
        });
    }

    clearInitiatorFormControlValues(): void {
        this.salesBoundaryForm.patchValue({
            employeeFirstName: '', assignToId: '', employeeLastName: '',
            employeeNo: '', initiatorEmailAddress: ''
        });
    }

    cancel() {
        if (this.boundaryLayerId == 6) {
            this.router.navigate(['user/boundary-management', 'sales-boundary'], { queryParams: { tab: 0 } });
        } else if (this.boundaryLayerId == 7) {
            this.router.navigate(['user/boundary-management', 'sales-boundary'], { queryParams: { tab: 1 } });
        } else {
            this.router.navigate(['user/boundary-management', 'sales-boundary'], { queryParams: { tab: 2 } });
        }
    }

    getDetailsById() {
        if (!this.boundaryId) return;
        this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SALES_BOUNDARIES_DETAILS,
            undefined, false, prepareRequiredHttpParams({
                boundaryId: this.boundaryId
            }))
            .subscribe(response => {
                if (response.isSuccess && response.statusCode === 200 && response.resources) {
                    let salesBoundaryModel = new SalesBoundaryModel(response.resources);
                    this.selectedInitiatorOption = {
                        employeeFirstName: salesBoundaryModel.employeeFirstName, assignToId: salesBoundaryModel.assignToId,
                        employeeLastName: salesBoundaryModel.employeeLastName,
                        employeeNo: salesBoundaryModel.employeeNo, initiatorEmailAddress: salesBoundaryModel.initiatorEmailAddress,
                        fullName: salesBoundaryModel.initiatorName
                    }
                    delete salesBoundaryModel.assignToName;
                    this.salesBoundaryForm.patchValue(salesBoundaryModel);
                    this.salesBoundaryForm.get('assignToName').setValue(response.resources.initiatorName, { emitEvent: false, onlySelf: true });
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    changeBranch() {
        this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_UX_SUB_BRANCHES, null, false, prepareGetRequestHttpParams(null, null, {
            branchId: this.salesBoundaryForm.get('branchId').value
        }))
            .subscribe(resp => {
                if (resp.resources && resp.isSuccess && resp.statusCode == 200) {
                    this.subBranches = resp.resources;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    onSubmit() {
        this.salesBoundaryForm.updateValueAndValidity();
        if (this.salesBoundaryForm.invalid || !this.salesBoundaryForm.get("assignToName").value) {
            this.salesBoundaryForm.get("assignToName").markAllAsTouched();
            return;
        }
        this.salesBoundaryForm.value.boundaryLayerId = this.boundaryLayerId;
        this.salesBoundaryForm.value.createdUserId = this.loggedInUserData.userId;
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SALES_BOUNDARIES, this.salesBoundaryForm.value)
            .subscribe(resp => {
                if (resp.isSuccess && resp.statusCode == 200) {
                    this.cancel();
                }
            });
    }
}
