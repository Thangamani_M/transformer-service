import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IT_MANAGEMENT_COMPONENT, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'sales-boundary-list',
  templateUrl: './sales-boundary-list.component.html',
  styleUrls: ['./sales-boundary-list.component.scss'],
})
export class SalesBoundaryListComponent extends PrimeNgTableVariablesModel implements OnInit {
  row = {};
  searchForm: FormGroup;
  searchColumns;
  columnFilterForm: FormGroup;
  groupList = [];
  primengTableConfigProperties: any = {
    tableCaption: "Sales Boundary list",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'IT Management', relativeRouterUrl: '' }, { displayName: 'Boundary Management', relativeRouterUrl: '' }, { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Reconnection' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Reconnection',
          dataKey: 'boundaryId',
          enableBreadCrumb: true,
          enableAction: true,
          enableAddActionBtn: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'boundaryRequestRefNo', header: 'Boundary Number' },
            { field: 'boundaryName', header: 'Boundary Name' },
            { field: 'boundaryLayerName', header: 'Boundary Layer Name' },
            { field: 'divisionName', header: 'Division' },
            { field: 'initiatorName', header: 'Intitiator Name' },
            { field: 'initiatorEmailAddress', header: 'Intitiator Email Address' },
            { field: 'boundaryStatusName', header: 'Status', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px',isDate:true }
          ],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_SALES_BOUNDARIES,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled: true
        },
        {
          caption: 'Residential',
          dataKey: 'boundaryId',
          enableBreadCrumb: true,
          enableAction: true,
          enableAddActionBtn: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'boundaryRequestRefNo', header: 'Boundary Number' },
            { field: 'boundaryName', header: 'Boundary Name' },
            { field: 'boundaryLayerName', header: 'Boundary Layer Name' },
            { field: 'divisionName', header: 'Division' },
            { field: 'initiatorName', header: 'Intitiator Name' },
            { field: 'initiatorEmailAddress', header: 'Intitiator Email Address' },
            { field: 'boundaryStatusName', header: 'Status', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px', isDate:true}
          ],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_SALES_BOUNDARIES,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled: true
        },
        {
          caption: 'Small Commercial',
          dataKey: 'boundaryId',
          enableBreadCrumb: true,
          enableReset: false,
          enableAction: true,
          enableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'boundaryRequestRefNo', header: 'Boundary Number' },
            { field: 'boundaryName', header: 'Boundary Name' },
            { field: 'boundaryLayerName', header: 'Boundary Layer Name' },
            { field: 'divisionName', header: 'Division' },
            { field: 'initiatorName', header: 'Intitiator Name' },
            { field: 'initiatorEmailAddress', header: 'Intitiator Email Address' },
            { field: 'boundaryStatusName', header: 'Status', width: '200px' },
            { field: 'createdDate', header: 'Created On', width: '200px', isDate:true }
          ],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_SALES_BOUNDARIES,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled: true
        }]
    }
  }
  boundaryLayerId: number;

  constructor(private crudService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private momentService: MomentService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private _fb: FormBuilder) {
    super();
    this.columnFilterForm = this._fb.group({});
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'IT Management', relativeRouterUrl: '' }, { displayName: 'Boundary Management', relativeRouterUrl: '' }, { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: this.selectedTabIndex == 0 ? 'Reconnection' : this.selectedTabIndex == 1 ? 'Residential' : 'Small Commercial', relativeRouterUrl: '' }]
    });
  }

  ngOnInit(): void {
    this.boundaryLayerId = 6;
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    if (this.selectedTabIndex == 0) {
      this.boundaryLayerId = 6;
      this.getMainArea();
    }
    else if (this.selectedTabIndex == 1) {
      this.boundaryLayerId = 7;
      this.getMainArea();
    } else {
      this.boundaryLayerId = 8;
      this.getMainArea();
    }
    this.combineLatestNgrxStoreData();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.BM_SALES]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getGroups(): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_ACTION_GROUP, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.groupList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getMainArea(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    let obj = { boundaryLayerId: this.boundaryLayerId };
    if (otherParams) {
      otherParams = { ...otherParams, ...obj };
    } else {
      otherParams = obj;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_SALES_BOUNDARIES,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dataList = response.resources;
          this.totalRecords = response.totalCount;
        }
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSubArea(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_SUB_AREA,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dataList = response.resources;
          this.totalRecords = response.totalCount;
        }
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onTabChange(e) {
    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[e.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = e.index;
    if (this.selectedTabIndex == 0) {
      this.boundaryLayerId = 6;
      this.getMainArea();
    }
    else if (this.selectedTabIndex == 1) {
      this.boundaryLayerId = 7;
      this.getMainArea();
    } else {
      this.boundaryLayerId = 8;
      this.getMainArea();
    }
    this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'IT Management', relativeRouterUrl: '' }, { displayName: 'Boundary Management', relativeRouterUrl: '' }, { displayName: 'Sales', relativeRouterUrl: '' }, { displayName: this.selectedTabIndex == 0 ? 'Reconnection' : this.selectedTabIndex == 1 ? 'Residential' : 'Small Commercial', relativeRouterUrl: '' }]
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/user/boundary-management/sales-boundary/add-edit'], { queryParams: { boundaryLayerId: this.boundaryLayerId } });
        break;
      case CrudType.GET:
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                searchObj[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              }
              else {
                searchObj[key] = searchObj['searchColumns'][key];
              }
            });
          }
          if (searchObj['sortOrderColumn']) {
            searchObj['sortOrder'] = searchObj['sortOrder'];
            searchObj['sortOrderColumn'] = searchObj['sortOrderColumn'];
          }
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.getMainArea(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
          case 1:
            this.getMainArea(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
          case 2:
            this.getMainArea(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['user/boundary-management/sales-boundary/add-edit'], { queryParams: { id: row['boundaryId'], boundaryLayerId: this.boundaryLayerId } });
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getMainArea();
            }
          });
        break;
    }
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
