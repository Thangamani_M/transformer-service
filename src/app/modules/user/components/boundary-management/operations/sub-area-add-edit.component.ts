import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
    CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, getPDropdownData, IApplicationResponse, LoggedInUserModel,
    ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { SubAreaModel } from '@modules/user/models/it-management-models';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
    selector: 'sub-area-add-edit',
    templateUrl: './sub-area-add-edit.component.html',
    styleUrls: ['./boundary-management-list.component.scss']
})

export class SubAreadAddEditComponent {
    subAreaForm: FormGroup;
    opsList;
    loggedInUserData: LoggedInUserModel;
    mainAreaId;
    details;
    isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
    mainAreas = [];
    boundaryLayerId: number;
    id = "";
    boundaryName = "";
    caption = "";
    isFormSubmitted = false;
    selectedInitiatorOption;
    getLoopableObjectRequestObservable: Observable<IApplicationResponse>;
    _tempMainArea :any = []

    constructor(private rxjsService: RxjsService, private router: Router, private activatedRoute: ActivatedRoute,
        private crudService: CrudService, private formBuilder: FormBuilder, private store: Store<AppState>) {
    }

    ngOnInit() {
        this.combineLatestNgrxStoreData();
        this.forkJoinRequests();
        this.createSubArea();
        this.onFormControlChanges();
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(loggedInUserData), this.activatedRoute.queryParams]
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
            this.id = response[1].id;
            this.boundaryLayerId = response[1].boundaryLayerId;
            if (this.id) {
                this.caption = "Update";
            } else {
                this.caption = "Create";
            }
            if (this.boundaryLayerId == 1) {
                this.boundaryName = '';
            } else {
                this.boundaryName = 'Monitoring';
            }
        });
    }

    forkJoinRequests(): void {
        forkJoin([this.getMainAreas(), this.getOPSDropDown()]).subscribe((response) => {
            response.forEach((respObj: IApplicationResponse, ix: number) => {
                if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
                    switch (ix) {
                        case 0:
                          this._tempMainArea = respObj.resources;
                            this.mainAreas = getPDropdownData(respObj.resources,"mainAreaCode","mainAreaId");
                            if (this.id) {
                                this.getDetailsById();
                            }
                            break;
                        case 1:
                            this.opsList = respObj.resources;
                            break;
                    }
                }
            });
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    getMainAreas(): Observable<IApplicationResponse> {
        return this.crudService.dropdown(ModulesBasedApiSuffix.SALES, UserModuleApiSuffixModels.UX_MAIN_AREAS, null);
    }

    getOPSDropDown() {
        return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_OPS_UX, null, false, null);
    }

    onFormControlChanges() {
        this.subAreaForm.get('mainAreaId').valueChanges.subscribe((mainAreaId) => {
            if (!mainAreaId) { return; }
            if (this._tempMainArea && this._tempMainArea.length > 0 && mainAreaId && mainAreaId.length > 0) {
                let filteredSubArea = this._tempMainArea.find(e => e.mainAreaId == mainAreaId);
                this.setSubAreaRelatedFormControlValues(filteredSubArea);
            }
        });
        this.onFullNameFormControlValueChanges();
    }

    setSubAreaRelatedFormControlValues(filteredSubArea) {
        if (filteredSubArea) {
            let filteredOpsObject = this.opsList.find(oP => oP['id'] == filteredSubArea.opsManagementTypeId);
            if (filteredOpsObject) {
                this.subAreaForm.patchValue({
                    mainAreaName: filteredSubArea.mainAreaName,
                    initiatorEmailAddress: filteredSubArea.initiatorEmailAddress, initiatorName: filteredSubArea.initiatorName,
                    assignTo: filteredSubArea.employeeId, opsManagementTypeId: filteredSubArea.opsManagementTypeId
                });
            }
            else {
                this.subAreaForm.patchValue({
                    mainAreaName: '',
                    initiatorEmailAddress: '', initiatorName: '',
                    assignTo: '', opsManagementTypeId: ''
                });
                this.subAreaForm.get('mainAreaId').setValue('');
            }
        }
    }

    onFullNameFormControlValueChanges(): void {
        this.subAreaForm
            .get("fullName")
            .valueChanges.pipe(
                debounceTime(debounceTimeForSearchkeyword),
                distinctUntilChanged()).subscribe((search: IApplicationResponse) => {
                    this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
                        UserModuleApiSuffixModels.GET_EMPLOYEE_SEARCH, null, true, prepareGetRequestHttpParams(null, null, {
                            search
                        }));
                });
    }

    onSelectedItemOption(selectedObject): void {
        if (!selectedObject) return;
        this.selectedInitiatorOption = selectedObject;
        this.subAreaForm.patchValue({
            firstName: selectedObject.firstName, assignTo: selectedObject.employeeId, lastName: selectedObject.lastName,
            employeeNo: selectedObject.employeeNo, officialEmail: selectedObject.officialEmail ? selectedObject.officialEmail : selectedObject.email,
            fullName: selectedObject.fullName
        });
    }

    clearInitiatorFormControlValues(): void {
        this.subAreaForm.patchValue({
            firstName: '', assignTo: '', lastName: '',
            employeeNo: '', officialEmail: ''
        });
    }

    getDetailsById() {
        this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_BOUNDARY_DETAILS,
            undefined, false, prepareGetRequestHttpParams(null, null, {
                boundaryId: this.id
            }), 1)
            .subscribe(response => {
                if (response.resources && response.isSuccess && response.statusCode === 200) {
                    let subAreaModel = new SubAreaModel(response.resources);
                    subAreaModel.fullName = response.resources.fullName;
                    subAreaModel.mainAreaName = this.mainAreas.find(e => e.value == response.resources.mainAreaId)?.label;
                    delete subAreaModel.fullName;
                    this.subAreaForm.patchValue(subAreaModel);
                    this.subAreaForm.get('fullName').setValue(response.resources.fullName, { emitEvent: false, onlySelf: true });
                    this.selectedInitiatorOption = {
                        firstName: subAreaModel.firstName, assignTo: subAreaModel.assignTo,
                        lastName: subAreaModel.lastName,
                        employeeNo: subAreaModel.employeeNo,
                        email: response.resources.officialEmail,
                        fullName: response.resources.fullName
                    }
                }
            });
    }

    createSubArea() {
        let subAreaModel = new SubAreaModel();
        this.subAreaForm = this.formBuilder.group({});
        Object.keys(subAreaModel).forEach((key) => {
            this.subAreaForm.addControl(key, new FormControl(subAreaModel[key]));
        });
        this.subAreaForm = setRequiredValidator(this.subAreaForm, ["mainAreaId", "description", "sapContract", "boundaryName", "fullName"]);
    }

    onSubmit() {
        this.isFormSubmitted = true;
        this.subAreaForm.updateValueAndValidity();
        if (this.subAreaForm.invalid || !this.subAreaForm.get("fullName").value) {
            this.subAreaForm.get("fullName").markAllAsTouched();
            return;
        }
        this.subAreaForm.get('referenceId').setValue(this.subAreaForm.value.mainAreaId);
        this.subAreaForm.get('boundaryLayerId').setValue(this.boundaryLayerId);
        this.subAreaForm.value.createdUserId = this.loggedInUserData.userId;
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_SUB_AREA, this.subAreaForm.value)
            .subscribe(resp => {
                if (resp.isSuccess && resp.statusCode == 200) {
                    this.cancel();
                }
            });
    }

    cancel() {
        if (this.boundaryLayerId == 1) {
            this.router.navigate(['user/boundary-management/operational-boundary'], { queryParams: { tab: 1 } });
        } else if (this.boundaryLayerId == 2) {
            this.router.navigate(['user/boundary-management/operational-boundary'], { queryParams: { tab: 2 } });
        } else {
            this.router.navigate(['user/boundary-management/operational-boundary'], { queryParams: { tab: 0 } });
        }
    }
}
