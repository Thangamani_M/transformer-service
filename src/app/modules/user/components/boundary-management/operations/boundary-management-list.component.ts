import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BoundaryLayers, IT_MANAGEMENT_COMPONENT, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest} from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'boundary-management-list',
  templateUrl: './boundary-management-list.component.html',
  styleUrls: ['./boundary-management-list.component.scss'],
})
export class BoundaryManagementListComponent extends PrimeNgTableVariablesModel implements OnInit {
  row = {};
  searchForm: FormGroup;
  searchColumns;
  columnFilterForm: FormGroup;
  groupList;
  primengTableConfigProperties: any = {
    tableCaption: "Boundary management list",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'IT Management', relativeRouterUrl: '' }, { displayName: 'Boundary Management', relativeRouterUrl: '' }, { displayName: 'Operations', relativeRouterUrl: '' }, { displayName: 'Main Area' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Main Area',
          dataKey: 'mainAreaId',
          enableBreadCrumb: true,
          enableReset: false,
          enableAction: true,
          enableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: true,
          checkBox: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'mainAreaCode', header: 'Main Area ID' },
          { field: 'mainAreaName', header: 'Main Area' },
          { field: 'businessArea', header: 'Business Area' },
          { field: 'divisionName', header: 'Division', width: '90px' },
          { field: 'initiatorName', header: 'Intitiator Name' },
          { field: 'initiatorEmailAddress', header: 'Intitiator Email Address' },
          { field: 'classificationLevel', header: 'Classification level' },
          { field: 'createdDate', header: 'Created On', width: '200px', isDate:true}
          ],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_MAIN_AREA,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled: true
        },
        {
          caption: 'Sub Area',
          dataKey: 'boundaryId',
          enableBreadCrumb: true,
          enableAction: true,
          enableAddActionBtn: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'subArea', header: 'Sub Area' },
          { field: 'mainAreaCode', header: 'Main area ID' },
          { field: 'mainAreaName', header: 'Main Area' },
          { field: 'isBoundaryRequired', header: 'Boundary Required', width: '200px', type:'dropdown', placeholder:"Boundary Required", options:[{label:'Yes',value:true},{label:'No',value:false}] },
          { field: 'initiatorName', header: 'Initiator Name' },
          { field: 'description', header: 'Description ' },
          { field: 'status', header: 'Status' },
          { field: 'createdDate', header: 'Created On', width: '200px' ,isDate:true}
          ],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_SUB_AREA,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled: true
        },
        {
          caption: 'Monitoring',
          dataKey: 'boundaryId',
          enableBreadCrumb: true,
          enableReset: false,
          enableAction: true,
          enableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'subArea', header: 'Monitoring Area' },
          { field: 'mainAreaCode', header: 'Main area ID' },
          { field: 'mainAreaName', header: 'Main Area' },
          { field: 'isBoundaryRequired', header: 'Boundary Required', width: '200px',type:'dropdown', placeholder:"Boundary Required", options:[{label:'Yes',value:true},{label:'No',value:false}]  },
          { field: 'initiatorName', header: 'Initiator Name' },
          { field: 'description', header: 'Description ' },
          { field: 'status', header: 'Status' },
          { field: 'createdDate', header: 'Created On', width: '200px', isDate:true}
          ],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_SUB_AREA,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled: true
        }]
    }
  }
  boundaryLayerId: number;

  constructor(private crudService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private momentService: MomentService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackbarService: SnackbarService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private _fb: FormBuilder) {
    super();
    this.columnFilterForm = this._fb.group({});
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.boundaryLayerId = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'IT Management', relativeRouterUrl: '' },
      { displayName: 'Boundary Management', relativeRouterUrl: '' },
      { displayName: 'Operations', relativeRouterUrl: '' },
      { displayName: this.selectedTabIndex == 0 ? 'Main Area' : this.selectedTabIndex == 1 ? 'Sub Area' : 'Monitoring Sub Area', relativeRouterUrl: '' }]
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ];
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    if (this.selectedTabIndex == 0) {
      this.getMainArea();
    }
    else if (this.selectedTabIndex == 1) {
      this.getSubArea();
    } else {
      this.getSubArea();
    }
    this.combineLatestNgrxStoreData();
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.BM_OPERATONS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getGroups(): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.UX_ACTION_GROUP, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.groupList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getMainArea(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_MAIN_AREA,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getSubArea(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    let obj1;
    switch (this.selectedTabIndex) {
      case 0:
        obj1 = {};
        break;
      case 1:
        obj1 = { boundaryLayerId: BoundaryLayers.SUB_AREA }
        break;
      case 2:
      case 1:
        obj1 = { boundaryLayerId: BoundaryLayers.MONITORING }
        break;
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_SUB_AREA,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onTabChange(e) {
    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[e.index].columns);
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = e.index;
    let breadCrum;
    this.boundaryLayerId = this.selectedTabIndex;
    if (this.selectedTabIndex == 0) {
      breadCrum = "Ticket Groups";
      this.getMainArea();
    }
    else if (this.selectedTabIndex == 1) {
      breadCrum = "Ticket Types";
      this.getSubArea();
    } else {
      this.getSubArea();
    }
    this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'IT Management', relativeRouterUrl: '' }, { displayName: 'Boundary Management', relativeRouterUrl: '' }, { displayName: 'Operations', relativeRouterUrl: '' }, {
      displayName: this.selectedTabIndex == 0 ? 'Main Area' : this.selectedTabIndex == 1 ? 'Sub Area' : 'Monitoring Sub Area',
      relativeRouterUrl: ''
    }]
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.selectedTabIndex == 0) {
          this.router.navigate(['/user/boundary-management/operational-boundary/main-area/add-edit']);
        } else {
          this.router.navigate(['/user/boundary-management/operational-boundary/sub-area/add-edit'], { queryParams: { boundaryLayerId: this.boundaryLayerId } });
        }
        break;
      case CrudType.GET:
        if (Object.keys(this.row).length > 0) {
          if (searchObj['searchColumns']) {
            Object.entries(searchObj['searchColumns']).forEach(([key, value]) => {
              if (key == "mainAreaCode") {

                if (this.selectedTabIndex == 0) {
                  key = 'mainAreaId';
                }
                searchObj['searchColumns'][key] = value
              }
              if (key == "mainAreaName") {
                if (this.selectedTabIndex == 0) {
                  key = 'mainArea';
                }
                searchObj['searchColumns'][key] = value
              }
              if (key == "divisionName") {
                key = 'division';
                searchObj['searchColumns'][key] = value
              }
              if (key == "createdDate") {
                if (this.selectedTabIndex == 0) {
                  key = 'createdOn';
                }
                searchObj['searchColumns'][key] = value
              }
              if (key == "createdOn" || key == "createdDate") {
                searchObj[key] = this.momentService.localToUTC(searchObj['searchColumns'][key]);
              }
              else {
                searchObj[key] = searchObj['searchColumns'][key];
              }
            });
          }
          if (searchObj['sortOrderColumn']) {
            if (searchObj['sortOrderColumn'] == "mainAreaCode") {
              searchObj['sortOrderColumn'] = 'mainAreaId'
            }
            if (searchObj['sortOrderColumn'] == "mainAreaName") {
              searchObj['sortOrderColumn'] = 'mainArea'
            }
            if (searchObj['sortOrderColumn'] == "divisionName") {
              searchObj['sortOrderColumn'] = 'division'
            }
            if (this.row['sortOrderColumn'] == "createdDate") {
              searchObj['sortOrderColumn'] = 'createdOn'
            }
            searchObj['sortOrder'] = searchObj['sortOrder'];
            searchObj['sortOrderColumn'] = searchObj['sortOrderColumn'];
          }
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.getMainArea(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
          case 1:
            this.getSubArea(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
          case 2:
            this.getSubArea(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.selectedTabIndex == 0) {
          this.router.navigate(['/user/boundary-management/operational-boundary/main-area/add-edit'], { queryParams: { id: row['mainAreaId'] } });
        } else {
          this.router.navigate(['/user/boundary-management/operational-boundary/sub-area/add-edit'], { queryParams: { id: row['boundaryId'], boundaryLayerId: this.boundaryLayerId } });
        }
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              if (result) {
                if (this.selectedTabIndex == 0) {
                  this.getMainArea();
                }
                else if (this.selectedTabIndex == 1) {
                  this.getSubArea();
                } else {
                  this.getSubArea();
                }
              }
            }
          });
        break;
    }
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
