import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, formConfigs, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareAutoCompleteListFormatForMultiSelection, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { MainAreaModel } from '@modules/user/models/it-management-models';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
    selector: 'main-area-add-edit',
    templateUrl: './main-area-add-edit.component.html',
    styleUrls: ['./boundary-management-list.component.scss']
})

export class MainAreadAddEditComponent {
    divisions = [];
    regions = [];
    districts = [];
    mainAreaForm: FormGroup;
    branches = [];
    opsList = [];
    subBranches = [];
    provinces = [];
    loggedInUserData: LoggedInUserModel;
    mainAreaId;
    isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
    caption: string;
    isFormSubmitted = false;
    selectedInitiatorOption;
    getLoopableObjectRequestObservable: Observable<IApplicationResponse>;

    constructor(private rxjsService: RxjsService, private router: Router, private activatedRoute: ActivatedRoute, private crudService: CrudService
        , private formBuilder: FormBuilder, private store: Store<AppState>) {
    }

    ngOnInit() {
        this.combineLatestNgrxStoreData();
        this.forkJoinRequests();
        this.createMainArea();
        if (this.mainAreaId) {
            this.caption = "Update"
        } else {
            this.caption = "Create"
        }
        this.onFormControlChanges();
        this.getProvinceList()
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(loggedInUserData), this.activatedRoute.queryParams]
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
            this.mainAreaId = response[1].id;
        });
    }

    changeRegion() {
        this.mainAreaForm.get('divisionId').setValue('');
        this.mainAreaForm.get('districtId').setValue('');
        this.mainAreaForm.get('branchId').setValue('');
        this.mainAreaForm.get('subBranchId').setValue('');
        this.districts = [];
        this.divisions = [];
        this.branches = [];
    }

    changeDivision() {
        this.mainAreaForm.get('districtId').setValue('');
        this.mainAreaForm.get('branchId').setValue('');
        this.mainAreaForm.get('subBranchId').setValue('');
        this.districts = [];
        this.branches = [];
    }

    changeDistrict() {
        this.mainAreaForm.get('branchId').setValue('');
        this.mainAreaForm.get('subBranchId').setValue('');
        this.branches = [];
    }

    onFormControlChanges() {
        this.mainAreaForm.get('regionId').valueChanges.subscribe((regionId: string) => {
            if (!regionId) return;
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
                prepareGetRequestHttpParams(null, null,
                    { regionId }))
                .subscribe((response: IApplicationResponse) => {
                    this.divisions = [];
                    if (response.resources && response.resources && response.isSuccess) {
                        this.divisions = response.resources;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                });
        });
        this.mainAreaForm.get('divisionId').valueChanges.subscribe((divisionId: string) => {
            if (!divisionId) return;
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
                prepareGetRequestHttpParams(null, null,
                    { divisionId }))
                .subscribe((response: IApplicationResponse) => {
                    this.districts = [];
                    if (response && response.resources && response.isSuccess) {
                        this.districts = response.resources;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                });
        });
        this.mainAreaForm.get('districtId').valueChanges.subscribe((districtId: string) => {
            if (!districtId) return;
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
                prepareGetRequestHttpParams(null, null,
                    { districtId }))
                .subscribe((response: IApplicationResponse) => {
                    this.branches = [];
                    if (response && response.resources && response.isSuccess) {
                        this.branches = response.resources;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                })
        });
        this.mainAreaForm.get('branchId').valueChanges.subscribe((branchId: string) => {
            if (!branchId) return;
            this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_UX_SUB_BRANCHES, null, false,
                prepareGetRequestHttpParams(null, null,
                    { branchId }))
                .subscribe((response: IApplicationResponse) => {
                    this.subBranches = [];
                    if (response && response.resources && response.isSuccess) {
                        this.subBranches = response.resources;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                });
        });
        this.mainAreaForm.get('isClassificationDistrictLevel').valueChanges.subscribe((isClassificationDistrictLevel: boolean) => {
            if (isClassificationDistrictLevel) {
                this.mainAreaForm.get('branchId').clearValidators()
                this.mainAreaForm.get('branchId').updateValueAndValidity()
                this.mainAreaForm.get('subBranchId').clearValidators()
                this.mainAreaForm.get('subBranchId').updateValueAndValidity()
            } else {
                this.mainAreaForm.get('branchId').setValidators([Validators.required])
                this.mainAreaForm.get('branchId').updateValueAndValidity();
                this.mainAreaForm.get('subBranchId').setValidators([Validators.required]);
                this.mainAreaForm.get('subBranchId').updateValueAndValidity();
            }
        });
        this.onAssignToNameFormControlValueChanges();
    }

    onAssignToNameFormControlValueChanges(): void {
        this.mainAreaForm
            .get("assignToName")
            .valueChanges.pipe(
                debounceTime(debounceTimeForSearchkeyword),
                distinctUntilChanged()).subscribe((search: IApplicationResponse) => {
                    this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
                        UserModuleApiSuffixModels.GET_EMPLOYEE_SEARCH, null, true, prepareGetRequestHttpParams(null, null, {
                            search
                        }));
                });
    }

    onSelectedItemOption(selectedObject): void {
        if (!selectedObject) return;
        this.selectedInitiatorOption = selectedObject;
        this.mainAreaForm.patchValue({
            firstName: selectedObject.firstName, assignTo: selectedObject.employeeId, lastName: selectedObject.lastName,
            employeeNo: selectedObject.employeeNo, officialEmail: selectedObject.officialEmail ? selectedObject.officialEmail : selectedObject.email,
            fullName: selectedObject.fullName, assignToName: selectedObject.fullName
        });
    }

    clearInitiatorFormControlValues(): void {
        this.mainAreaForm.patchValue({
            firstName: '', assignTo: '', lastName: '',
            employeeNo: '', officialEmail: ''
        });
    }

    getDetailsById() {
        this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_MAIN_AREA,
            this.mainAreaId, false, null)
            .subscribe(response => {
                if (response.isSuccess && response.statusCode === 200) {
                    let salesBoundaryModel = new MainAreaModel(response.resources);
                    salesBoundaryModel.assignToName = response.resources.fullName;
                    salesBoundaryModel.opsManagementTypeId = response.resources.opsManagementTypeId
                    this.selectedInitiatorOption = {
                        firstName: salesBoundaryModel.firstName, assignTo: salesBoundaryModel.assignTo,
                        lastName: salesBoundaryModel.lastName,
                        employeeNo: salesBoundaryModel.employeeNo,
                        officialEmail: salesBoundaryModel.officialEmail,
                        fullName: salesBoundaryModel?.fullName
                    }
                    delete salesBoundaryModel.assignToName;
                    this.mainAreaForm.patchValue(salesBoundaryModel);
                    this.mainAreaForm.get('assignToName').setValue(response.resources.fullName, { emitEvent: false, onlySelf: true });
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    cancel() {
        this.router.navigate(['user/boundary-management/operational-boundary'], { queryParams: { tab: 0 } });
    }

    createMainArea() {
        let mainAreaModel = new MainAreaModel();
        this.mainAreaForm = this.formBuilder.group({});
        Object.keys(mainAreaModel).forEach((key) => {
            this.mainAreaForm.addControl(key, new FormControl(mainAreaModel[key]));
        });
        this.mainAreaForm = setRequiredValidator(this.mainAreaForm, ["mainAreaName", "mainAreaCode", "provinceId",
            "opsManagementTypeId", "businessArea", "subBranchId", "assignTo", "divisionId", "branchId", "districtId",
            "regionId", "assignToName"]);
    }

    changeBranch() {
        this.mainAreaForm.get('subBranchId').setValue('');
        this.subBranches = [];
        this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_UX_SUB_BRANCHES, null, false, prepareGetRequestHttpParams(null, null, {
            branchId: this.mainAreaForm.get('branchId').value
        })).subscribe(resp => {
            this.subBranches = [];
            if (resp.statusCode == 200 && resp.isSuccess && resp.resources) {
                this.subBranches = resp.resources;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    forkJoinRequests(): void {
        forkJoin([this.getregions(), this.getOPSDropDown(), this.getProvinceList()]).subscribe((response) => {
            response.forEach((respObj: IApplicationResponse, ix: number) => {
                if (respObj.isSuccess) {
                    switch (ix) {
                        case 0:
                            this.regions = respObj.resources;
                            break;
                        case 1:
                            this.opsList =  getPDropdownData(respObj.resources);
                            // this.opsList.forEach((opsObj) => {
                            //     opsObj.id = opsObj.id.toString();
                            // })
                            break;
                        case 2:
                            this.provinces = respObj.resources;
                            this.provinces = getPDropdownData(this.provinces);
                            if (this.mainAreaId) {
                                this.getDetailsById();
                            }
                            else {
                                this.rxjsService.setGlobalLoaderProperty(false);
                            }
                            break;
                    }
                }
            });
        });
    }

    getregions(): Observable<IApplicationResponse> {
        return this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
            prepareGetRequestHttpParams(null, null, { CountryId: formConfigs.countryId }));
    }

    getOPSDropDown() {
        return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_OPS_UX, null, false, null, 1)
    }

    getProvinceList() {
        return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_PROVINCES,
            undefined, false, prepareRequiredHttpParams({
                countryId: formConfigs.countryId
            }));
    }

    onSubmit() {
        this.isFormSubmitted = true;
        this.mainAreaForm.updateValueAndValidity();
        if (this.mainAreaForm.invalid || !this.mainAreaForm.get("assignToName").value) {
            this.mainAreaForm.get("assignToName").markAllAsTouched();
            return;
        }
        this.mainAreaForm.value.createdUserId = this.loggedInUserData.userId;
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_MAIN_AREA, this.mainAreaForm.value)
            .subscribe(resp => {
                if (resp.isSuccess && resp.statusCode == 200) {
                    this.cancel();
                }
            });
    }
}
