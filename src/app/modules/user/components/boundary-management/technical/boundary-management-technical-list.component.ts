import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BoundaryLayers, IT_MANAGEMENT_COMPONENT, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'boundary-management-technical',
  templateUrl: './boundary-management-technical-list.component.html',
})
export class BoundaryManagementTechnicalListComponent extends PrimeNgTableVariablesModel implements OnInit {
  row: any = {};
  searchForm: FormGroup;
  searchColumns: any;
  columnFilterForm: FormGroup;
  groupList: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  primengTableConfigProperties: any = {
    tableCaption: "Technical Boundary List",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'IT Management', relativeRouterUrl: '' }, { displayName: 'Boundary Management', relativeRouterUrl: '' }, { displayName: 'Technical', relativeRouterUrl: '' }, { displayName: 'Service Tech Area' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Service Tech Area',
          dataKey: 'boundaryId',
          enableBreadCrumb: true,
          enableReset: false,
          enableAction: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: true,
          enableAddActionBtn: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          shouldShowAddActionBtn: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'boundaryRequestRefNo', header: 'Tech Area ID', width: '150px' },
          { field: 'boundaryName', header: 'Tech Area', width: '100px' },
          { field: 'description', header: 'Description', width: '150px' },
          { field: 'divisionName', header: 'Division', width: '100px' },
          { field: 'districtName', header: 'District', width: '150px' },
          { field: 'branchName', header: 'Branch', width: '150px' },
          { field: 'defaultCapacity', header: 'Capacity', width: '100px' },
          {
            field: 'isInstallation', header: 'Inst', type: 'dropdown', placeholder: 'Select',
            options: [{ label: 'Yes', value: true }, { label: 'No', value: false }], width: '85px'
          },
          {
            field: 'isService', header: 'Serv', type: 'dropdown', placeholder: 'Select',
            options: [{ label: 'Yes', value: true }, { label: 'No', value: false }], width: '85px'
          },
          {
            field: 'isOverheadAreaSetup', header: 'Overhead', type: 'dropdown', placeholder: 'Select',
            options: [{ label: 'Yes', value: true }, { label: 'No', value: false }], width: '120px'
          },
          { field: 'stockCollectionWarehouse', header: 'Warehouse Collections', width: '200px' },
          { field: 'stockHoldingWarehouse', header: 'Warehouse Holding', width: '200px' },
          { field: 'assignTo', header: 'Initiator Name', width: '150px' },
          { field: 'assignEmailAddress', header: 'Initiator Email Address', width: '250px' },
          { field: 'boundaryStatusName', header: 'Status', width: '150px' },
          { field: 'createdDate', header: 'Created On', width: '150px',isDate:true }
          ],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.TECH_AREA_BOUNDARY,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled: true
        },
        {
          caption: 'Installation Tech Area',
          dataKey: 'boundaryId',
          enableBreadCrumb: true,
          enableReset: false,
          enableAction: true,
          enableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableRowDelete: true,
          shouldShowAddActionBtn: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'boundaryRequestRefNo', header: 'Tech Area ID', width: '150px' },
          { field: 'boundaryName', header: 'Tech Area', width: '100px' },
          { field: 'description', header: 'Description', width: '150px' },
          { field: 'divisionName', header: 'Division', width: '100px' },
          { field: 'districtName', header: 'District', width: '150px' },
          { field: 'branchName', header: 'Branch', width: '150px' },
          { field: 'defaultCapacity', header: 'Capacity', width: '100px' },
          {
            field: 'isInstallation', header: 'Inst', type: 'dropdown', placeholder: 'Select',
            options: [{ label: 'Yes', value: true }, { label: 'No', value: false }], width: '85px'
          },
          {
            field: 'isService', header: 'Serv', type: 'dropdown', placeholder: 'Select',
            options: [{ label: 'Yes', value: true }, { label: 'No', value: false }], width: '85px'
          },
          {
            field: 'isOverheadAreaSetup', header: 'Overhead', type: 'dropdown', placeholder: 'Select',
            options: [{ label: 'Yes', value: true }, { label: 'No', value: false }], width: '120px'
          },
          { field: 'stockCollectionWarehouse', header: 'Warehouse Collections', width: '200px' },
          { field: 'stockHoldingWarehouse', header: 'Warehouse Holding', width: '200px' },
          { field: 'assignTo', header: 'Initiator Name', width: '150px' },
          { field: 'assignEmailAddress', header: 'Initiator Email Address', width: '250px' },
          { field: 'boundaryStatusName', header: 'Status', width: '150px' },
          { field: 'createdDate', header: 'Created On', width: '150px' , isDate:true}
          ],
          shouldShowCreateActionBtn: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.TECH_AREA_BOUNDARY,
          moduleName: ModulesBasedApiSuffix.SALES,
          disabled: true
        },
      ]
    }
  }
  boundaryLayerId: number;

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackbarService: SnackbarService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private _fb: FormBuilder) {
    super();
    this.columnFilterForm = this._fb.group({});
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.boundaryLayerId = this.selectedTabIndex
      this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'IT Management', relativeRouterUrl: '' }, { displayName: 'Boundary Management', relativeRouterUrl: '' }, { displayName: 'Technical', relativeRouterUrl: '' }, { displayName: this.selectedTabIndex == 0 ? 'Service Tech Area' : 'Installation Tech Area', relativeRouterUrl: '/configuration/contact-center' }]
    });
  }

  ngOnInit(): void {
    if (this.selectedTabIndex == 0) {
      this.getServiceBoundary();
    }
    else if (this.selectedTabIndex == 1) {
      this.getInstallatioBoundary();
    }
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.BM_TECHNICAL]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getServiceBoundary(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    let obj = { boundaryLayerId: BoundaryLayers.SERVICE };
    if (otherParams) {
      otherParams = { ...otherParams, ...obj };
    } else {
      otherParams = obj;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.TECH_AREA_BOUNDARY,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dataList = response.resources;
          this.dataList.forEach(element => {
            element.isInstallation = element.isInstallation == true ? 'Yes' : 'No';
            element.isService = element.isService == true ? 'Yes' : 'No';
            element.isOverheadAreaSetup = element.isOverheadAreaSetup == true ? 'Yes' : 'No';
          });
          this.totalRecords = response.totalCount;
        }
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getInstallatioBoundary(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    let obj = { boundaryLayerId: BoundaryLayers.INSTALLATION };
    if (otherParams) {
      otherParams = { ...otherParams, ...obj };
    } else {
      otherParams = obj;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.TECH_AREA_BOUNDARY,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dataList = response.resources;
          this.dataList.forEach(element => {
            element.isInstallation = element.isInstallation == true ? 'Yes' : 'No';
            element.isService = element.isService == true ? 'Yes' : 'No';
            element.isOverheadAreaSetup = element.isOverheadAreaSetup == true ? 'Yes' : 'No';
          });
          this.totalRecords = response.totalCount;
        }
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSubArea(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    let obj1 = { boundaryLayerId: this.boundaryLayerId };
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_SUB_AREA,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.dataList = response.resources
          this.totalRecords = response.totalCount;
        }
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });;
  }

  onTabChange(e) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = e.index;
    this.boundaryLayerId = this.selectedTabIndex;
    if (this.selectedTabIndex == 0) {
      this.getServiceBoundary();
    }
    else if (this.selectedTabIndex == 1) {
      this.getInstallatioBoundary();
    }
    this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'IT Management', relativeRouterUrl: '' }, { displayName: 'Boundary Management', relativeRouterUrl: '' }, { displayName: 'Technical', relativeRouterUrl: '' }, { displayName: this.selectedTabIndex == 0 ? 'Service Tech Area' : 'Installation Tech Area', relativeRouterUrl: '/configuration/contact-center' }]
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.selectedTabIndex == 0) {
          this.router.navigate(['/user/boundary-management/technical-boundary/service-tech-area/add-edit']);
        } else {
          this.router.navigate(['user/boundary-management/technical-boundary/installation-tech-area/add-edit']);
        }
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getServiceBoundary(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
          case 1:
            this.getInstallatioBoundary(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.selectedTabIndex == 0) {
          this.router.navigate(['/user/boundary-management/technical-boundary/service-tech-area/add-edit'], { queryParams: { id: row['boundaryId'] } });
        } else {
          this.router.navigate(['user/boundary-management/technical-boundary/installation-tech-area/add-edit'], { queryParams: { id: row['boundaryId'] } });
        }
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              if (this.selectedTabIndex == 0) {
                this.getServiceBoundary();
              }
              else if (this.selectedTabIndex == 1) {
                this.getInstallatioBoundary();
              }
            }
          });
        break;
    }
  }
}
