import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, formConfigs, getPDropdownData, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareAutoCompleteListFormatForMultiSelection, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { BoundaryLayers, SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { TechAreaModel } from '@modules/user/models/it-management-models';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'installation-tech-area-add-edit',
  templateUrl: './installation-tech-area-add-edit.component.html',
})

export class InstallationTechAreaAddEditComponent {
  Divisions = [];
  Regions = [];
  Districts = [];
  techAreaForm: FormGroup;
  branchDropdown = [];
  subBranches = [];
  provinceList = [];
  loggedInUserData: LoggedInUserModel;
  employees = [];
  boundaryId = "";
  details;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  selectedOptions;
  wareHouseList = [];
  locationList = [];
  locationHoldingList = [];
  multiSelectFlag;
  selectBranches = true;
  selectDistrict = true;
  isFormSubmitted = false;
  selectedInitiatorOption;
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;

  constructor(private rxjsService: RxjsService, private router: Router, private activatedRoute: ActivatedRoute, private crudService: CrudService
    , private formBuilder: FormBuilder, private store: Store<AppState>) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.boundaryId = params.id;
    });
  }

  ngOnInit() {
    this.forkJoinRequests();
    this.combineLatestNgrxStoreData();
    this.createServiceTechArea();
    this.onFormControlChanges();
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE_BRANCH, null, false, prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId }), 1)
      .subscribe(resp => {
        this.wareHouseList = resp.resources;
      })
    if (this.boundaryId) {
      this.getDetailsById();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  changeRegion() {
    this.techAreaForm.get('divisionId').setValue('');
    this.techAreaForm.get('districtId').setValue('');
    this.techAreaForm.get('branches').setValue('');
    this.Districts = [];
    this.Divisions = [];
    this.branchDropdown = [];
  }

  changeDivision() {
    this.techAreaForm.get('districtId').setValue('');
    this.techAreaForm.get('branches').setValue('');
    this.Districts = [];
    this.branchDropdown = [];
  }

  changeDistrict() {
    this.techAreaForm.get('branches').setValue('');
    this.branchDropdown = [];
  }

  onFormControlChanges() {
    this.techAreaForm.get('regionId').valueChanges.subscribe((regionId: string) => {
      if (!regionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { regionId: regionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.Divisions = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.techAreaForm.get('isOverheadAreaSetup').valueChanges.subscribe((isOverheadAreaSetup: string) => {
      this.techAreaForm.get('branches').patchValue('');
      this.techAreaForm.get('branches').updateValueAndValidity();
      this.techAreaForm.get('districtId').patchValue('');
      this.techAreaForm.get('districtId').updateValueAndValidity();
      this.multiSelectFlag = isOverheadAreaSetup
    });
    this.techAreaForm.get('divisionId').valueChanges.subscribe((divisionId: string) => {
      if (!divisionId) return;
      this.Districts = [];
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionId: divisionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.Districts = getPDropdownData(response.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.techAreaForm.get('districtId').valueChanges.subscribe((districtId: string) => {
      if (!districtId) return;
      this.selectDistrict = true;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null,
          { districtId: districtId }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.branchDropdown = [];
            this.branchDropdown = getPDropdownData(response.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.techAreaForm.get('branches').valueChanges.subscribe((branches: string) => {
      if (!branches) return;
      this.selectBranches = true;
    });
    this.techAreaForm.get('stockCollectionWarehouse').valueChanges.subscribe((stockCollectionWarehouse: string) => {
      if (!stockCollectionWarehouse) return;
      this.techAreaForm.controls['stockCollectionLocationId'].setValue('');
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION, null, false, prepareRequiredHttpParams({
        warehouseId: stockCollectionWarehouse,
        IsAll: false
      }), 1)
        .subscribe(resp => {
          this.locationList = resp.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.techAreaForm.get('stockHoldingWarehouse').valueChanges.subscribe((stockHoldingWarehouse: string) => {
      if (!stockHoldingWarehouse) return;
      this.techAreaForm.controls['stockHoldingLocationId'].setValue('');
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION, null, false, prepareRequiredHttpParams({
        warehouseId: stockHoldingWarehouse,
        IsAll: false
      }), 1)
        .subscribe(resp => {
          this.locationHoldingList = resp.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.onAssignToFormControlValueChanges();
  }

  onAssignToFormControlValueChanges(): void {
    this.techAreaForm
      .get("assignTo")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((search: IApplicationResponse) => {
          this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
            UserModuleApiSuffixModels.GET_EMPLOYEE_SEARCH, null, true, prepareGetRequestHttpParams(null, null, {
              search
            }));
        });
  }

  onSelectedItemOption(selectedObject): void {
    if (!selectedObject) return;
    this.selectedInitiatorOption = selectedObject;
    this.techAreaForm.patchValue({
      firstName: selectedObject.firstName, assignTo: selectedObject.fullName, lastName: selectedObject.lastName,
      employeeNo: selectedObject.employeeNo, email: selectedObject.email, assignToId: selectedObject.employeeId
    });
  }

  clearInitiatorFormControlValues(): void {
    this.techAreaForm.patchValue({
      firstName: '', lastName: '', assignToId: '',
      employeeNo: '', email: '',
    });
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TECH_AREA_BOUNDARY_DETAILS,
      undefined, false, prepareRequiredHttpParams({
        boundaryId: this.boundaryId
      }), 1)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode === 200) {
          this.details = resp.resources;
          this.multiSelectFlag = this.details.isOverheadAreaSetup;
          delete this.details.assignTo;
          this.techAreaForm.patchValue(this.details);
          this.techAreaForm.get('branches').setValue(this.details.branchIds);
          this.techAreaForm.get('email').setValue(resp.resources.assignEmailAddress);
          this.techAreaForm.get('districtId').setValue(this.details.districtIds && this.details.districtIds);
          this.techAreaForm.get('assignTo').setValue(resp.resources.fullName, { emitEvent: false, onlySelf: true });
          this.techAreaForm.get('firstName').setValue(this.details.employeeFirstName);
          this.techAreaForm.get('lastName').setValue(this.details.employeeLastName);
          if (!this.multiSelectFlag) {
            this.techAreaForm.get('branches').setValue(this.details.branchIds[0]);
            this.techAreaForm.get('districtId').setValue(this.details.districtIds[0]);
          }
          this.selectedInitiatorOption = {
            assignToId: this.details.assignToId,
            firstName: this.details.employeeFirstName, assignTo: this.details.fullName,
            lastName: this.details.employeeLastName,
            employeeNo: this.details.employeeNo,
            email: this.details.assignEmailAddress
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  cancel() {
    this.router.navigate(['user/boundary-management/technical-boundary'], { queryParams: { tab: 1 } });
  }

  createServiceTechArea() {
    let techAreaModel = new TechAreaModel();
    this.techAreaForm = this.formBuilder.group({});
    Object.keys(techAreaModel).forEach((key) => {
      this.techAreaForm.addControl(key, new FormControl(techAreaModel[key]));
    });
    this.techAreaForm.get('boundaryType').setValue('Technical')
    this.techAreaForm.get('layerName').setValue('Installation');
    this.techAreaForm.get('boundaryLayerId').setValue(BoundaryLayers.INSTALLATION);
    if (this.boundaryId) {
      this.techAreaForm.get('boundaryId').setValue(this.boundaryId);
    }
    this.techAreaForm = setRequiredValidator(this.techAreaForm, ["regionId", "assignTo", "divisionId", "districtId", "branches", "boundaryName", "description", "stockCollectionLocationId", "stockHoldingLocationId", "stockCollectionWarehouse", "stockHoldingWarehouse"]);
  }

  onSubmit() {
    this.techAreaForm.updateValueAndValidity();
    this.isFormSubmitted = true;
    if (this.techAreaForm.value.branches.length === 0) {
      this.selectBranches = false;
    } else {
      this.selectBranches = true;
    }
    if (this.techAreaForm.value.districtId.length === 0) {
      this.selectDistrict = false;
    } else {
      this.selectDistrict = true;
    }
    if (this.techAreaForm.invalid || !this.techAreaForm.get("assignTo").value) {
      this.techAreaForm.get("assignTo").markAllAsTouched();
      return;
    }
    if (!this.techAreaForm.get('isInstallation').value &&
      !this.techAreaForm.get('isService').value && !this.techAreaForm.get('isInspection').value) {
      return;
    }
    if (!this.multiSelectFlag) {
      let branchArray = [];
      branchArray.push(this.techAreaForm.value.branches);
      this.techAreaForm.value.branchIds = branchArray;
      let districtArray = [];
      districtArray.push(this.techAreaForm.value.districtId);
      this.techAreaForm.value.districtIds = districtArray;
    } else {
      this.techAreaForm.value.branchIds = this.techAreaForm.value.branches;
      this.techAreaForm.value.districtIds = this.techAreaForm.value.districtId;
    }
    this.techAreaForm.value.assignToId = this.selectedInitiatorOption.assignToId ? this.selectedInitiatorOption.assignToId :
      this.selectedInitiatorOption.employeeId;
    this.techAreaForm.value.createdUserId = this.loggedInUserData.userId;
    this.techAreaForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.techAreaForm.value.fullName = this.techAreaForm.value.assignTo;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TECH_AREA_BOUNDARY, this.techAreaForm.value)
      .subscribe(resp => {
        this.isFormSubmitted = false;
        if (resp.isSuccess && resp.statusCode == 200) {
          this.cancel();
        }
      }
      );
  }

  forkJoinRequests(): void {
    forkJoin([this.getRegions()]).subscribe((response) => {
      response.forEach((respObj: IApplicationResponse, ix: number) => {
        if (respObj.isSuccess) {
          switch (ix) {
            case 0:
              this.Regions = respObj.resources;
              break;
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getRegions(): Observable<IApplicationResponse> {
    return this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null, { countryId: formConfigs.countryId }));
  }

  getBranchDropDown() {
    return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false, null)
  }

  getDivisions(): Observable<IApplicationResponse> {
    return this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS)
  }

  getDistricts(): Observable<IApplicationResponse> {
    return this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT)
  }
}
