import { ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, formConfigs, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareAutoCompleteListFormatForMultiSelection, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { BoundaryLayers, SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { TechAreaModel } from '@modules/user/models/it-management-models';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'service-tech-area-add-edit',
  templateUrl: './service-tech-area-add-edit.component.html',
})

export class ServiceTechAreaAddEditComponent {
  Divisions = [];
  Regions = [];
  Districts = [];
  techAreaForm: FormGroup;
  branchDropdown = [];
  opsList = [];
  subBranches = [];
  provinceList = [];
  loggedInUserData: LoggedInUserModel;
  employees = [];
  boundaryId = "";
  details;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  selectedOptions;
  wareHouseList = [];
  locationList = [];
  locationHoldingList = [];
  multiSelectFlag;
  selectBranches = true;
  selectDistrict = true;
  isFormSubmitted = false;
  selectedInitiatorOption;
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;

  constructor(private rxjsService: RxjsService, private router: Router, private activatedRoute: ActivatedRoute, private crudService: CrudService
    , private formBuilder: FormBuilder, private store: Store<AppState>, private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.forkJoinRequests();
    this.createServiceTechArea();
    this.onFormControlChanges();
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE_BRANCH, null, false, prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId }), 1)
      .subscribe(resp => {
        this.wareHouseList = resp.resources;
      })
    if (this.boundaryId) {
      this.getDetailsById();
    }
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.boundaryId = response[1]?.id;
    });
  }

  onFormControlChanges() {
    this.techAreaForm.get('regionId').valueChanges.subscribe((regionId: string) => {
      if (!regionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { regionId: regionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.Divisions = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });

    this.techAreaForm.get('isOverheadAreaSetup').valueChanges.subscribe((isOverheadAreaSetup: boolean) => {
      if (isOverheadAreaSetup) {
        this.techAreaForm.get('isInspection').enable();
      }
      else {
        this.techAreaForm.get('isInspection').disable();
      }
      this.techAreaForm.get('branches').patchValue('');
      this.techAreaForm.get('branches').updateValueAndValidity();
      this.techAreaForm.get('districtId').patchValue('');
      this.techAreaForm.get('districtId').updateValueAndValidity();
      this.multiSelectFlag = isOverheadAreaSetup;
    });
    this.techAreaForm.get('divisionId').valueChanges.subscribe((divisionId: string) => {
      if (!divisionId) return;
      this.Districts = [];
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionId: divisionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.Districts = getPDropdownData(response.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });

    this.techAreaForm.get('districtId').valueChanges.subscribe((districtId: string) => {
      if (!districtId) return;
      this.selectDistrict = true;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null,
          { districtId: districtId }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.branchDropdown = getPDropdownData(response.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });

    this.techAreaForm.get('branches').valueChanges.subscribe((branches: string) => {
      if (!branches) return;
      this.selectBranches = true;
    });

    this.techAreaForm.get('stockCollectionWarehouse').valueChanges.subscribe((stockCollectionWarehouse: string) => {
      if (!stockCollectionWarehouse) return;
      this.techAreaForm.controls['stockCollectionLocationId'].setValue('');
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION, null, false, prepareRequiredHttpParams({
        warehouseId: stockCollectionWarehouse
      }), 1)
        .subscribe(resp => {
          this.locationList = resp.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });

    this.techAreaForm.get('stockHoldingWarehouse').valueChanges.subscribe((stockHoldingWarehouse: string) => {
      if (!stockHoldingWarehouse) return;
      this.techAreaForm.controls['stockHoldingLocationId'].setValue('');
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION, null, false, prepareRequiredHttpParams({
        warehouseId: stockHoldingWarehouse
      }), 1)
        .subscribe(resp => {
          this.locationHoldingList = resp.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.onAssignToFormControlValueChanges();
  }

  onAssignToFormControlValueChanges(): void {
    this.techAreaForm
      .get("assignTo")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((search: IApplicationResponse) => {
          this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
            UserModuleApiSuffixModels.GET_EMPLOYEE_SEARCH, null, true, prepareGetRequestHttpParams(null, null, {
              search
            }));
        });
  }

  onSelectedItemOption(selectedObject): void {
    if (!selectedObject) return;
    this.selectedInitiatorOption = selectedObject;
    this.techAreaForm.patchValue({
      firstName: selectedObject.firstName, assignTo: selectedObject.fullName, lastName: selectedObject.lastName,
      employeeNo: selectedObject.employeeNo, email: selectedObject.email, assignToId: selectedObject.employeeId
    });
  }

  clearInitiatorFormControlValues(): void {
    this.techAreaForm.patchValue({
      firstName: '', lastName: '', assignToId: '',
      employeeNo: '', email: '',
    });
  }

  getEmployeeListFromSearchKeyword(search?: string): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.GET_EMPLOYEE_SEARCH, null, true, prepareGetRequestHttpParams(null, null, {
      search
    }));
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TECH_AREA_BOUNDARY_DETAILS,
      undefined, false, prepareRequiredHttpParams({
        boundaryId: this.boundaryId
      }), 1)
      .subscribe(response => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.details = response.resources;
          this.multiSelectFlag = this.details.isOverheadAreaSetup;
          delete this.details.assignTo;
          this.techAreaForm.patchValue(this.details);
          this.techAreaForm.get('branches').setValue(this.details.branchIds);
          this.techAreaForm.get('districtId').setValue(this.details.districtIds && this.details.districtIds);
          this.techAreaForm.get('assignTo').setValue(response.resources.fullName, { emitEvent: false, onlySelf: true });
          this.techAreaForm.get('email').setValue(this.details.assignEmailAddress);
          this.techAreaForm.get('firstName').setValue(this.details.employeeFirstName);
          this.techAreaForm.get('lastName').setValue(this.details.employeeLastName);
          if (!this.multiSelectFlag) {
            this.techAreaForm.get('isInspection').disable();
            this.techAreaForm.get('branches').setValue(this.details.branchIds[0]);
            this.techAreaForm.get('districtId').setValue(this.details.districtIds[0]);
          }
          else {
            this.techAreaForm.get('isInspection').enable();
          }
          this.selectedInitiatorOption = {
            assignToId: this.details.assignToId,
            firstName: this.details.employeeFirstName, assignTo: this.details.fullName,
            lastName: this.details.employeeLastName,
            employeeNo: this.details.employeeNo,
            email: this.details.assignEmailAddress,
            fullName: this.details.fullName
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  changeRegion() {
    this.techAreaForm.get('divisionId').setValue('');
    this.techAreaForm.get('districtId').setValue('');
    this.techAreaForm.get('branches').setValue('');
    this.Districts = [];
    this.Divisions = [];
    this.branchDropdown = [];
  }

  changeDivision() {
    this.techAreaForm.get('districtId').setValue('');
    this.techAreaForm.get('branches').setValue('');
    this.Districts = [];
    this.branchDropdown = [];
  }

  changeDistrict() {
    this.techAreaForm.get('branches').setValue('');
    this.branchDropdown = [];
  }

  cancel() {
    this.router.navigate(['user/boundary-management/technical-boundary'], { queryParams: { tab: 0 } });
  }

  createServiceTechArea() {
    let techAreaModel = new TechAreaModel();
    this.techAreaForm = this.formBuilder.group({});
    Object.keys(techAreaModel).forEach((key) => {
      this.techAreaForm.addControl(key, new FormControl(techAreaModel[key]));
    });
    this.techAreaForm.get('boundaryType').setValue('Technical');
    this.techAreaForm.get('isInspection').disable();
    this.techAreaForm.get('layerName').setValue('Service');
    this.techAreaForm.get('boundaryLayerId').setValue(BoundaryLayers.SERVICE);
    if (this.boundaryId) {
      this.techAreaForm.get('boundaryId').setValue(this.boundaryId);
    }
    this.techAreaForm = setRequiredValidator(this.techAreaForm, ["regionId", "assignTo", "divisionId", "districtId",
      "branches", "boundaryName", "description", "stockCollectionLocationId", "stockHoldingLocationId", "stockCollectionWarehouse",
      "stockHoldingWarehouse"]);
  }

  onSubmit() {
    this.techAreaForm.updateValueAndValidity();
    this.isFormSubmitted = true;
    if (this.techAreaForm.value.branches.length === 0) {
      this.selectBranches = false;
    } else {
      this.selectBranches = true;
    }
    if (this.techAreaForm.value.districtId.length === 0) {
      this.selectDistrict = false;
    } else {
      this.selectDistrict = true;
    }
    if (this.techAreaForm.invalid || !this.techAreaForm.get("assignTo").value) {
      this.techAreaForm.get("assignTo").markAllAsTouched();
      return;
    }
    if (!this.techAreaForm.get('isInstallation').value &&
      !this.techAreaForm.get('isService').value && !this.techAreaForm.get('isInspection').value) {
      return;
    }
    if (!this.multiSelectFlag) {
      let branchArray = [];
      branchArray.push(this.techAreaForm.value.branches);
      this.techAreaForm.value.branchIds = branchArray;
      let districtArray = [];
      districtArray.push(this.techAreaForm.value.districtId);
      this.techAreaForm.value.districtIds = districtArray;
    } else {
      this.techAreaForm.value.branchIds = this.techAreaForm.value.branches;
      this.techAreaForm.value.districtIds = this.techAreaForm.value.districtId;
    }
    this.techAreaForm.value.createdUserId = this.loggedInUserData.userId;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TECH_AREA_BOUNDARY, this.techAreaForm.value)
      .subscribe(resp => {
        this.isFormSubmitted = false;
        if (resp.isSuccess && resp.statusCode == 200) {
          this.cancel();
        }
      }
      );
  }

  forkJoinRequests(): void {
    forkJoin([this.getRegions()]).subscribe((response) => {
      response.forEach((respObj: IApplicationResponse, ix: number) => {
        if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
          switch (ix) {
            case 0:
              this.Regions = respObj.resources;
              break;
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getRegions(): Observable<IApplicationResponse> {
    return this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null, { CountryId: formConfigs.countryId }));
  }

  getBranchDropDown() {
    return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false, null)
  }

  getDivisions(): Observable<IApplicationResponse> {
    return this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS)
  }

  getDistricts(): Observable<IApplicationResponse> {
    return this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT)
  }
}
