import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { BoundaryRoutingModule } from './boundary-routing.module';
import { BoundaryManagementListComponent } from './operations/boundary-management-list.component';
import { MainAreadAddEditComponent } from './operations/main-area-add-edit.component';
import { SubAreadAddEditComponent } from './operations/sub-area-add-edit.component';
import { SalesBoundaryAddEditComponent } from './sales/sales-boundary-add-edit.component';
import { SalesBoundaryListComponent } from './sales/sales-boundary-list.component';
import { BoundaryManagementTechnicalListComponent } from './technical/boundary-management-technical-list.component';
import { InstallationTechAreaAddEditComponent } from './technical/installation-tech-area-add-edit.component';
import { ServiceTechAreaAddEditComponent } from './technical/service-tech-area-add-edit.component';
@NgModule({
  declarations: [BoundaryManagementListComponent, SubAreadAddEditComponent, SalesBoundaryAddEditComponent, SalesBoundaryListComponent, BoundaryManagementTechnicalListComponent, InstallationTechAreaAddEditComponent, MainAreadAddEditComponent, ServiceTechAreaAddEditComponent],
  imports: [
    CommonModule,
    BoundaryRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ],
})
export class BoundaryModule { }
