import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BoundaryManagementListComponent } from './operations/boundary-management-list.component';
import { MainAreadAddEditComponent } from './operations/main-area-add-edit.component';
import { SubAreadAddEditComponent } from './operations/sub-area-add-edit.component';
import { SalesBoundaryAddEditComponent } from './sales/sales-boundary-add-edit.component';
import { SalesBoundaryListComponent } from './sales/sales-boundary-list.component';
import { BoundaryManagementTechnicalListComponent } from './technical/boundary-management-technical-list.component';
import { InstallationTechAreaAddEditComponent } from './technical/installation-tech-area-add-edit.component';
import { ServiceTechAreaAddEditComponent } from './technical/service-tech-area-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: 'operational-boundary', component: BoundaryManagementListComponent, canActivate: [AuthGuard], data: { title: 'Boundary Management' } },
  { path: 'operational-boundary/sub-area/add-edit', component: SubAreadAddEditComponent, canActivate: [AuthGuard], data: { title: 'Boundary Management' } },
  { path: 'operational-boundary/main-area/add-edit', component: MainAreadAddEditComponent, canActivate: [AuthGuard], data: { title: 'Sales Boundary' } },
  { path: 'sales-boundary', component: SalesBoundaryListComponent, canActivate: [AuthGuard], data: { title: 'Sales Boundary' } },
  { path: 'sales-boundary/add-edit', component: SalesBoundaryAddEditComponent, canActivate: [AuthGuard], data: { title: 'Sales Boundary' } },
  { path: 'technical-boundary', component: BoundaryManagementTechnicalListComponent, canActivate: [AuthGuard], data: { title: 'Technical Boundary' } },
  { path: 'technical-boundary/service-tech-area/add-edit', component: ServiceTechAreaAddEditComponent, canActivate: [AuthGuard], data: { title: 'Technical Boundary' } },
  { path: 'technical-boundary/installation-tech-area/add-edit', component: InstallationTechAreaAddEditComponent, canActivate: [AuthGuard], data: { title: 'Service Tech Area Add Edit' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class BoundaryRoutingModule { }
