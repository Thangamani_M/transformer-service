import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DepartmentListComponent } from '@user/components/department';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: 'list', component: DepartmentListComponent,canActivate:[AuthGuard], data: { title: 'Department' } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class DepartmentRoutingModule { }
