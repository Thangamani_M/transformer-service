import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DepartmentAddEditComponent, DepartmentModel, UserModuleApiSuffixModels } from '@app/modules/user';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { IT_MANAGEMENT_COMPONENT } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
})
export class DepartmentListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any = {
    tableCaption: "Department",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'IT Management ', relativeRouterUrl: '' }, { displayName: 'Department List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Department',
          dataKey: 'departmentId',
          enableBreadCrumb: true,
          enableAction: true,
          enableAddActionBtn: true,
          enableMultiDeleteActionBtn: true,
          enableStatusActiveAction: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: true,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'departmentName', header: 'Department Name' },
          { field: 'departmentCode', header: 'Department Code' },
          { field: 'description', header: 'Description' },
          { field: 'createdDate', header: 'Created Date', isDate:true },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: UserModuleApiSuffixModels.DEPARTMENTS,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT
        }]
    }
  }

  constructor(private dialog: MatDialog,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    public dialogService: DialogService,
    private snackbarService: SnackbarService,
    private crudService: CrudService) {
    super()
  }

  ngOnInit(): void {
    this.getDepartments();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.DEPARTMENT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getDepartments(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.DEPARTMENTS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getDepartments(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getDepartments();
            }
          });
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
    }
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  openAddEditPage(type: CrudType | string, divisionModel: DepartmentModel | any): void {
    let data = new DepartmentModel(divisionModel);
    const dialogReff1 = this.dialog.open(DepartmentAddEditComponent, { width: '450px', data, disableClose: true });
    dialogReff1.afterClosed().subscribe(result => {
      if (!result) return;
    });
    dialogReff1.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.getDepartments();
      }
    });
  }

  onTabChange(event) { }
}
