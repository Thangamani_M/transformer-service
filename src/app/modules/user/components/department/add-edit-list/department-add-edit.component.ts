import { Component, EventEmitter, Inject, OnInit, Optional, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DepartmentModel, loggedInUserData, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-department-add-edit',
  templateUrl: './department-add-edit.component.html',
})

export class DepartmentAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  departmentAddEditForm: FormGroup;
  loggedUser: UserLogin;
  ref: any;
  @Output() outputData = new EventEmitter<any>();

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public departmentModel: DepartmentModel,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createDepartmentForm();
  }

  createDepartmentForm(): void {
    let departmentFormModel = new DepartmentModel(this.departmentModel);
    this.departmentAddEditForm = this.formBuilder.group({});
    Object.keys(departmentFormModel).forEach((key) => {
      this.departmentAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(departmentFormModel[key]));
    });
    this.departmentAddEditForm = setRequiredValidator(this.departmentAddEditForm, ["departmentName", "departmentCode"]);
  }

  onSubmit(): void {
    if (this.departmentAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.departmentAddEditForm.value.departmentId ? this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.DEPARTMENTS, this.departmentAddEditForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.DEPARTMENTS, this.departmentAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dialog.closeAll();
        this.departmentAddEditForm.reset();
        this.outputData.emit(true)
      }
    });
  }

  dialogClose(): void {
    this.dialogRef.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
