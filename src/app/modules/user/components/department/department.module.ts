import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DepartmentAddEditComponent, DepartmentListComponent, DepartmentRoutingModule } from '@user/components/department';

@NgModule({
  declarations: [DepartmentAddEditComponent, DepartmentListComponent],
  imports: [
    CommonModule,
    DepartmentRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
  ],
  entryComponents: [DepartmentAddEditComponent]
})
export class DepartmentModule { }