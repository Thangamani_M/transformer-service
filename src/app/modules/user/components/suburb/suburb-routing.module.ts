import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SuburblistComponent } from '@user/components/suburb';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
    { path: 'list', component: SuburblistComponent,canActivate:[AuthGuard], data: { title: 'Suburb' } }
];
@NgModule({
    imports: [RouterModule.forChild(routes)]
})

export class SubUrbRoutingModule { }
