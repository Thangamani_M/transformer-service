import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SuburbAddEditComponent, SuburblistComponent, SubUrbRoutingModule } from '@user/components/suburb';

@NgModule({
  declarations: [SuburbAddEditComponent, SuburblistComponent],
  imports: [
    CommonModule,
    SubUrbRoutingModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents: [SuburbAddEditComponent]
})
export class SubUrbModule { }