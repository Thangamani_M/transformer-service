import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CrudType, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { loggedInUserData } from '@modules/others';
import { IT_MANAGEMENT_COMPONENT, UserModuleApiSuffixModels } from '@modules/user/shared/utils';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { SuburbAddEditComponent } from './';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
declare var $;
@Component({
  selector: 'app-suburblist',
  templateUrl: './suburblist.component.html',
})
export class SuburblistComponent extends PrimeNgTableVariablesModel implements OnInit {
  applicationResponse: IApplicationResponse;
  observableResponse;
  totalLength;
  searchCol: any = {}
  primengTableConfigProperties: any = {
    tableCaption: "Suburb",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'IT Management ', relativeRouterUrl: '' }, { displayName: 'Suburb List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Suburb',
          dataKey: 'suburbId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: true,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          enableAction: true,
          enableMultiDeleteActionBtn: true,
          enableAddActionBtn: true,
          columns: [{ field: 'suburbName', header: 'Suburb Name' },
          { field: 'provinceName', header: 'Province Name' },
          { field: 'cityName', header: 'City Name' },
          { field: 'tierName', header: 'Tier Name' },
          { field: 'createdDate', header: 'Created Date' },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: UserModuleApiSuffixModels.SUBURB,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT
        }]
    }
  }

  constructor(private httpService: CrudService,
    private dialog: MatDialog,
    private rxjsService: RxjsService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    public dialogService: DialogService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    super()
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getSuburb();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.SUBURB]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?, searchCol?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.OpenDialogWindow(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.searchCol = searchCol
            this.getSuburb(
              row["pageIndex"], row["pageSize"], searchCol);
            break;
        }
        break;
      case CrudType.EDIT:
        this.OpenDialogWindow(CrudType.EDIT, row);
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.OpenDialogWindow(CrudType.VIEW, row);
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchCol].isActive = this.dataList[searchCol].isActive ? false : true;
            }
          });
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getSuburb();
            }
          });
        break;
    }
  }

  getSuburb(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    otherParams = { ...this.searchCol }
    this.httpService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.SUBURB,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse
        this.totalRecords = response.totalCount;
        this.loading = false;
        this.applicationResponse = response;
        this.totalLength = this.applicationResponse.totalCount;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }


  private OpenDialogWindow(type: CrudType | string, suburb) {
    const dialogReff = this.dialog.open(SuburbAddEditComponent, { width: '450px', data: suburb ? suburb : {}, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
      if (result) {
        this.observableResponse = this.getSuburb();
      }

    });
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
