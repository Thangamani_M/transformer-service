import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogPopupComponent, CustomDirectiveConfig, setRequiredValidator } from '@app/shared';
import { AlertService, CrudService, RxjsService } from '@app/shared/services';
import { LoggedInUserModel, ModulesBasedApiSuffix } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils';
import { Store } from '@ngrx/store';
import { Suburb, SuburbModel } from '@user/models';
import { Guid } from 'guid-typescript';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-suburb-add-edit',
  templateUrl: './suburb-add-edit.component.html',
})
export class SuburbAddEditComponent implements OnInit {
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  suburbAddEditForm: FormGroup;
  errorMessage: string;
  btnName: string;
  countries = [];
  cities = [];
  provinces = [];
  tiers = [];
  ddlFilterCountry: any = '';
  tmpSrchCntryId: string;
  tmpSrchRgnId: string;
  loggedInUserData: LoggedInUserModel;
  cancelButton = true;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Suburb,
    private dialog: MatDialog,
    private httpServices: CrudService,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private rxjsService: RxjsService,
    public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
    private store: Store<AppState>) {
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.combineLatestNgrxStoreData();
    if (this.data.suburbId) {
      this.btnName = 'Update';
      this.getSuburbDetails();
    }
    else {
      this.btnName = 'Create';
    }
    this.bindingCountryDropdown();
    this.createSuburbForm();
    this.onFormControlChanges();
    if (this.data.suburbId != null) {
      this.tmpSrchCntryId = this.data.countryId;
      this.tmpSrchRgnId = this.data.regionId;
      this.bindingProvincesDropdown(this.data.countryId, this.data.regionId);
      this.bindingCitiesDropdown(this.data.provinceId);
      this.bindingTierDropdown();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onFormControlChanges(): void {
    this.suburbAddEditForm.get('provinceId').valueChanges.subscribe((id: string) => {
      this.loadCitiesbyProvinces(id);
    });
  }

  createSuburbForm(): void {
    let suburbModel = new SuburbModel();
    this.suburbAddEditForm = this.formBuilder.group({});
    Object.keys(suburbModel).forEach((key) => {
      this.suburbAddEditForm.addControl(key, new FormControl(suburbModel[key]));
    });
    this.suburbAddEditForm = setRequiredValidator(this.suburbAddEditForm, ["suburbName", "countryId", "provinceId", "cityId", "tierId"]);
    this.suburbAddEditForm.get('createdUserId').setValue(this.loggedInUserData.userId);
  }

  bindingCountryDropdown() {
    this.httpServices.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_COUNTRIES)
      .subscribe((response) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.countries = response.resources;
          if (this.btnName == "Create") {
            this.SetCountryDefaultValue();
            this.bindingProvincesDropdown(this.tmpSrchCntryId, this.tmpSrchRgnId);
          }
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  SetCountryDefaultValue() {
    this.tmpSrchCntryId = this.countries.find(x => x.displayName == 'South Africa').id;
    this.suburbAddEditForm.get('countryId').patchValue(this.tmpSrchCntryId);
  }

  getSuburbDetails() {
    this.httpServices.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.SUBURB, this.data.suburbId)
      .subscribe({
        next: response => {
          if(response.resources.tierId === null){
            response.resources.tierId = "";
          }
          this.suburbAddEditForm.patchValue(response.resources);
          this.rxjsService.setPopupLoaderProperty(false);
        }
      });
  }

  loadProvincesbyCountries(cntryId) {
    this.suburbAddEditForm.controls['provinceId'].setValue('');
    if (cntryId != "") {
      this.provinces = [0];
      this.cities = [0];
      let stringToSplit = cntryId;
      let x = stringToSplit.split(": ");
      this.bindingProvincesDropdown(x, undefined);
    }
    else {
      this.provinces = [];
      this.cities = [];
    }
  }

  bindingProvincesDropdown(cntryId, regionId) {
    if (regionId == undefined || regionId == Guid.EMPTY) {
      this.httpServices.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_PROVINCES, '?CountryId=' + cntryId)
        .subscribe((response) => {
          if (response.resources && response.statusCode == 200 && response.isSuccess) {
            this.provinces = response.resources;
          }
          this.rxjsService.setPopupLoaderProperty(false);
        });
    }
    else {
      this.httpServices.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_PROVINCES, '?CountryId=' + cntryId + '&RegionId=' + regionId)
        .subscribe((response) => {
          if (response.resources && response.statusCode == 200 && response.isSuccess) {
            this.provinces = response.resources;
          }
          this.rxjsService.setPopupLoaderProperty(false);
        });
    }
  }

  loadCitiesbyProvinces(e) {
    this.suburbAddEditForm.controls['cityId'].setValue('');
    this.bindingCitiesDropdown(e);
    this.bindingTierDropdown();
  }

  bindingCitiesDropdown(provinceId) {
    this.httpServices.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_CITIES, '?ProvinceId=' + provinceId)
      .subscribe((response) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.cities = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  bindingTierDropdown() {
    this.httpServices.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_TIERS)
      .subscribe((response) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.tiers = response.resources;
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  save(): void {
    if (this.suburbAddEditForm.invalid) {
      return
    }
    const suburb = { ...this.data, ...this.suburbAddEditForm.value }
    if (suburb.suburbId != null && suburb.suburbId != "") {
      suburb.modifiedUserId = this.loggedInUserData.userId;
      this.httpServices.update(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.SUBURB, suburb)
        .subscribe((response) => {
          if (response.resources && response.statusCode == 200 && response.isSuccess) {
            this.onSaveComplete(response);
          }
          this.rxjsService.setPopupLoaderProperty(false);
        });
    }
    else {
      suburb.createdUserId = this.loggedInUserData.userId;
      this.httpServices.create(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.SUBURB, suburb)
        .subscribe((response) => {
          if (response.resources && response.statusCode == 200 && response.isSuccess) {
            this.onSaveComplete(response);
          }
          this.rxjsService.setPopupLoaderProperty(false);
        });
    }
  }

  onSaveComplete(response): void {
    if (response.statusCode == 200) {
      this.dialog.closeAll();
      this.dialogRef.close(true);
      this.suburbAddEditForm.reset();
    } else {
      this.alertService.processAlert(response);
    }
  }

  closePopUp() {
    this.dialogRef.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
