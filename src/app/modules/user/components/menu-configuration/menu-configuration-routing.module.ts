import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MenuConfigurationComponent }   from '@user/components/menu-configuration';
import { AuthenticationGuard as AuthGuard, CanDeactivateGuard } from '@app/shared/services/authguards';

const routes: Routes = [
       {path:'',component:MenuConfigurationComponent,data:{title:'Menu Configuration'},canActivate:[AuthGuard] }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class MenuConfigurationRoutingModule { }