import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog, MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, currentComponentPageBasedPermissionsSelector$, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { debounceTimeForDeepNestedObjectSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareRequiredHttpParams } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { IT_MANAGEMENT_COMPONENT, UserModuleApiSuffixModels } from '@modules/user/shared/utils';
import { Store } from '@ngrx/store';
import { MenuConfigurationPopupAddEditComponent } from '@user/components/menu-configuration';
import { DialogService } from 'primeng/api';
import { combineLatest, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
interface MenuCategory {
  menuId: any,
  menuName: string,
  url: string,
  menuParentId: string,
  menuCategoryId: number,
  sortOrder: number,
  menuCategoryName: string,
  level: 2,
  subLevel: MenuCategory[]
}
interface FlatNode {
  expandable: boolean;
  menuName: string;
  level: number;
  menuId: any;
}
@Component({
  selector: 'app-menu-configuration',
  templateUrl: './menu-configuration.component.html',
  styleUrls: ['./menu-configuration.component.scss']
})
export class MenuConfigurationComponent implements OnInit {
  @Input() caption: string;
  @Input() roleId: string;
  @Output() emittedEvent = new EventEmitter<any>();
  menuCategories = [];
  loggedInUserData: LoggedInUserModel;
  searchText$ = new Subject();
  searchText = '';
  transformer = (node: MenuCategory, level: number) => {
    return {
      expandable: !!node.subLevel && node.subLevel.length > 0,
      menuName: node.menuName,
      level: level,
      subLevel: node.subLevel,
      menuId: node.menuId
    };
  };
  flatTreeControl = new FlatTreeControl<FlatNode>(
    (node) => node.level,
    (node) => node.expandable
  );
  treeFlattener = new MatTreeFlattener(
    this.transformer,
    (node) => node.level,
    (node) => node.expandable,
    (node) => node.subLevel,
  );
  dataSource = new MatTreeFlatDataSource(this.flatTreeControl, this.treeFlattener);
  expandedNodes = [];
  filteredSubComponentsPermissions = [];

  constructor(
    public dialogService: DialogService,
    private dialog:MatDialog,
    private httpServices: CrudService, private store: Store<AppState>,
    private rxjsService: RxjsService, private snackbarService: SnackbarService) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.getMenuConfigurations();
    this.searchText$.pipe(
      debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
      distinctUntilChanged(),
      switchMap(searchText => {
        return of(searchText);
      })).subscribe((searchText: string) => {
        this.rxjsService.setGlobalLoaderProperty(true);
        this.applyFilter();
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      if (response[1][IT_MANAGEMENT_COMPONENT.MENU_CONFIGURATION]) {
        this.filteredSubComponentsPermissions = response[1][IT_MANAGEMENT_COMPONENT.MENU_CONFIGURATION];
      }
    });
  }

  hasChild(_: number, node: MenuCategory) {
    return node.subLevel != null && node.subLevel.length > 0;
  }

  filterRecursive(array: any[], property: string) {
    let filteredData;
    if (this.searchText) {
      let filterText = this.searchText.toLowerCase();
      filteredData = array.map((copy) => Object.assign({}, copy)).filter(function x(y, index) {
        if (y[property].toLowerCase().includes(filterText)) {
          return true;
        }
        if (y.subLevel) {
          return (y.subLevel = y.subLevel.map((copy) => Object.assign({}, copy)).filter(x)).length;
        }
      });
    } else {
      filteredData = array;
    }
    return filteredData;
  }

  filterTree() {
    this.dataSource.data = this.filterRecursive(
      this.menuCategories,
      'menuName'
    );
    setTimeout(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    }, 2000);
  }

  applyFilter() {
    this.filterTree();
    if (this.searchText && this.dataSource.data.length < 3) {
      this.flatTreeControl.expandAll();
    } else if (this.searchText && this.dataSource.data.length > 3) {
      this.flatTreeControl.expand(this.flatTreeControl.dataNodes[0]);
      this.flatTreeControl.expandDescendants(this.flatTreeControl.dataNodes[0]);
    }
    else {
      this.flatTreeControl.collapseAll();
    }
  }

  getMenuConfigurations(searchText?: string) {
    let crudService = searchText ?
      this.httpServices.get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.MENU_SEARCH, searchText
      ) :
      this.httpServices.get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.MENU_SEARCH,
        undefined,
        false
      );
    crudService.subscribe((resp) => {
      if (resp.statusCode == 200 && resp.isSuccess) {
        this.menuCategories = resp.resources;
        this.dataSource.data = this.menuCategories;
        this.refreshTreeData();
      }
      setTimeout(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }, 1000);
    });
  }

  onOpenMenuConfigPopup(type, selectedNode?) {
    // let permissionType = type.includes('new') || type.includes('add') ? PermissionTypes.ADD :
    //   type.includes('edit') ? PermissionTypes.EDIT : PermissionTypes.DELETE;
    // if (!this.filteredSubComponentsPermissions.find((fS) => fS.menuName == permissionType)) {
    //   this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    // else {
      let data = selectedNode;
      if (data) {
        data.type = type;
      }
      else {
        data = { level: 1 };
        data.type = type;
      }
      data.sortOrderForParent = this.menuCategories.length;
      if (type.toLowerCase().includes('delete')) {
        const message = `Are you sure you want to delete this?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
          maxWidth: "400px",
          data: dialogData,
          disableClose: true
        });
        dialogRef.afterClosed().subscribe(dialogResult => {
          if (!dialogResult) return;
          this.deleteSelectedMenuByMenuId(selectedNode);
        });
      }
      else {
        this.rxjsService.setDialogOpenProperty(true);
        const ref = this.dialogService.open(MenuConfigurationPopupAddEditComponent, {
          showHeader: false,
          baseZIndex: 1000,
          width: "450px",
          data: { ...data},
        });
        ref.onClose.subscribe((resp) => {
          if(resp){
              this.saveExpandedNodes();
              this.getMenuConfigurations();
          }
        })
      }
  }

  addNode(node: MenuCategory) {
    // if (!this.filteredSubComponentsPermissions.find((fS) => fS.menuName == PermissionTypes.ADD)) {
    //   this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    // else {
      node.menuId = this.findNodeMaxId(this.dataSource.data) + 1;
      this.dataSource.data.push(node);
      this.refreshTreeData();
    // }

  }

  addChildNode(childrenNodeData: MenuCategory) {
    // if (!this.filteredSubComponentsPermissions.find((fS) => fS.menuName == PermissionTypes.ADD)) {
    //   this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    // else {
      childrenNodeData.menuId =
        this.findNodeMaxId(this.dataSource.data) + 1;
      childrenNodeData.subLevel.push(childrenNodeData);
      this.refreshTreeData();
    // }
  }

  editNode(nodeToBeEdited: MenuCategory) {
    // if (!this.filteredSubComponentsPermissions.find((fS) => fS.menuName == PermissionTypes.EDIT)) {
    //   this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    // else {
      const fatherElement: MenuCategory = this.findFatherNode(
        nodeToBeEdited.menuId,
        this.dataSource.data
      );
      let elementPosition: number;
      nodeToBeEdited.menuId =
        this.findNodeMaxId(this.dataSource.data) + 1;
      if (fatherElement[0]) {
        fatherElement[0].subLevel[fatherElement[1]] = nodeToBeEdited;
      } else {
        elementPosition = this.findPosition(
          nodeToBeEdited.menuId,
          this.dataSource.data
        );
        this.dataSource.data[elementPosition] = nodeToBeEdited;
      }
      this.refreshTreeData();
    // }
  }

  refreshTreeData() {
    this.applyFilter();
    this.dataSource.data = [...this.dataSource.data];
    this.restoreExpandedNodes();
  }

  saveExpandedNodes() {
    this.expandedNodes = new Array<MenuCategory>();
    this.flatTreeControl.dataNodes.forEach(node => {
      if (node.expandable && this.flatTreeControl.isExpanded(node)) {
        this.expandedNodes.push(node);
      }
    });
  }

  restoreExpandedNodes() {
    this.expandedNodes.forEach(node => {
      this.flatTreeControl.expand(this.flatTreeControl.dataNodes.find(n => n.menuId === node.menuId));
    });
  }

  findNodeMaxId(node: MenuCategory[]) {
    const flatArray = this.flatJsonArray([], node);
    const flatArrayWithoutSubLevel = [];
    flatArray.forEach(element => {
      flatArrayWithoutSubLevel.push(element.menuId);
    });
    return Math.max(...flatArrayWithoutSubLevel);
  }

  findPosition(id: string, data: MenuCategory[]) {
    for (let i = 0; i < data.length; i += 1) {
      if (id === data[i].menuId) {
        return i;
      }
    }
  }

  flatJsonArray(flattenedAray: Array<MenuCategory>, node: MenuCategory[]) {
    const array: Array<MenuCategory> = flattenedAray;
    node.forEach(element => {
      if (element.subLevel) {
        array.push(element);
        this.flatJsonArray(array, element.subLevel);
      }
    });
    return array;
  }

  findFatherNode(id: string, data: MenuCategory[]) {
    for (let i = 0; i < data.length; i += 1) {
      const currentFather = data[i];
      for (let z = 0; z < currentFather.subLevel.length; z += 1) {
        if (id === currentFather.subLevel[z]['menuId']) {
          return [currentFather, z];
        }
      }
      for (let z = 0; z < currentFather.subLevel.length; z += 1) {
        if (id !== currentFather.subLevel[z]['menuId']) {
          const result = this.findFatherNode(id, currentFather.subLevel);
          if (result !== false) {
            return result;
          }
        }
      }
    }
    return false;
  }

  deleteSelectedMenuByMenuId(selectedNode: MenuCategory) {
    let ids = selectedNode['menuId'];
    if (selectedNode['subLevel'].length > 0) {
      selectedNode['subLevel'].forEach((subLevelNode) => {
        ids = ids + "," + subLevelNode.menuId;
      });
    }
    this.httpServices.delete(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.MENU_CONFIG_MENUS, undefined,
      prepareRequiredHttpParams({
        ids,
        isDeleted: true,
        modifiedUserId: this.loggedInUserData.userId
      }), 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.saveExpandedNodes();
          this.getMenuConfigurations();
        }
      });
  }

  deleteNode(nodeToBeDeleted: MenuCategory) {
    const deletedElement: MenuCategory = this.findFatherNode(
      nodeToBeDeleted.menuId,
      this.dataSource.data
    );
    let elementPosition: number;
    if (deletedElement[0]) {
      deletedElement[0].subLevel.splice(deletedElement[1], 1);
    } else {
      elementPosition = this.findPosition(
        nodeToBeDeleted.menuId,
        this.dataSource.data
      );
      this.dataSource.data.splice(elementPosition, 1);
    }
    this.refreshTreeData();
  }

  onRoleSelection(childNode) {
    this.emittedEvent.emit({ roleId: childNode.id, roleName: childNode.displayName });
  }

  contains(text: string, term: string): boolean {
    return text.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  }

}
