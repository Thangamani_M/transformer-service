import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, MaterialModule, SharedModule } from '@app/shared';
import { MenuConfigurationComponent, MenuConfigurationPopupAddEditComponent, MenuConfigurationRoutingModule } from '@user/components/menu-configuration';
import {MatTreeModule} from '@angular/material/tree';

@NgModule({
  declarations: [MenuConfigurationComponent, MenuConfigurationPopupAddEditComponent],
  imports: [
    CommonModule,
    MenuConfigurationRoutingModule,
    MaterialModule,MatTreeModule,
    ReactiveFormsModule,
    FormsModule, LayoutModule,
    SharedModule
  ],
  entryComponents: [MenuConfigurationPopupAddEditComponent]
})
export class MenuConfigurationModule { }