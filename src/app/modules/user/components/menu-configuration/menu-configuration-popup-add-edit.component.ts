import { Component,  OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { LoggedInUserModel, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { loggedInUserData } from '@modules/others';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { MenuConfigModel } from '@user/models';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-menu-configuration-popup-add-edit',
  templateUrl: './menu-configuration-popup-add-edit.component.html',
  styleUrls: ['./menu-configuration.component.scss']
})
export class MenuConfigurationPopupAddEditComponent implements OnInit {
  menuConfigForm: FormGroup;
  btnName: string;
  loggedInUserData: LoggedInUserModel;
  featureTypes = [];
  isSmallIconIsRequired = false
  data:any

  constructor(public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private store: Store<AppState>) {
    this.data =  this.config.data
    this.btnName = this.config.data.type.toLowerCase().includes('new') ? 'Add' : 'Update';
  }

  ngOnInit() {
    this.getMenuCategoryFeatureTypes();
    this.combineLatestNgrxStoreData();
    this.createMenuConfigurationForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).pipe(take(1)).subscribe(async (response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      // comment the below method if harcoded data is not required

      // await this.prepareAllMenusFromParent({
      //   menuName: 'Boundary Management',
      //   smallIcon: '',
      //   subMenus: [
      //     {
      //       menuName: 'GIS Dashboard',
      //       url: '/my-tasks/boundary-management/gis-dashboard',
      //       menuCategoryId: 1,
      //       subMenus: [{
      //         menuName: 'List',
      //         menuCategoryId: 3,
      //       }, {
      //         menuName: 'Edit',
      //         menuCategoryId: 3,
      //       }]
      //     },
      //     {
      //     menuName: 'Sales Boundaries',
      //     url: '/boundary-management/view/sales-boundaries',
      //     menuCategoryId: 1,
      //     subMenus: [{
      //       menuName: 'List',
      //       menuCategoryId: 3,
      //     }, {
      //       menuName: 'Edit',
      //       menuCategoryId: 3,
      //     }, {
      //       menuName: 'Change Owner',
      //       menuCategoryId: 3,
      //     }]
      //   },
      //   {
      //     menuName: 'Technical Boundaries',
      //     url: '/boundary-management/view/technical-boundaries',
      //     menuCategoryId: 1,
      //     subMenus: [{
      //       menuName: 'List',
      //       menuCategoryId: 3,
      //     }, {
      //       menuName: 'Edit',
      //       menuCategoryId: 3,
      //     }, {
      //       menuName: 'Change Owner',
      //       menuCategoryId: 3,
      //     }]
      //   },
      //   {
      //     menuName: 'Operations Boundaries',
      //     url: '/boundary-management/view/operations-boundaries',
      //     menuCategoryId: 1,
      //     subMenus: [{
      //       menuName: 'List',
      //       menuCategoryId: 3,
      //     }, {
      //       menuName: 'Edit',
      //       menuCategoryId: 3,
      //     }, {
      //       menuName: 'Change Owner',
      //       menuCategoryId: 3,
      //     }]
      //   },
      //   {
      //     menuName: 'Other Boundaries',
      //     url: '/boundary-management/view/other-boundaries',
      //     menuCategoryId: 1,
      //     subMenus: [{
      //       menuName: 'List',
      //       menuCategoryId: 3,
      //     }, {
      //       menuName: 'Upload',
      //       menuCategoryId: 3,
      //     }]
      //   },{
      //     menuName: 'Rollback Process',
      //     url: '/boundary-management/view/rollbacks',
      //     menuCategoryId: 1,
      //     subMenus: [{
      //       menuName: 'List',
      //       menuCategoryId: 3,
      //     }, {
      //       menuName: 'Edit',
      //       menuCategoryId: 3,
      //     }]
      //   }
      // ]
      // });
      //  let menus = {
      //   menuName: 'Credit Control',
      //   smallIcon: 'sidebar-item-icon fa fa-money',
      //   sortOrder:10,
      //   subMenus: [
      //     {
      //       menuName: 'My Task',
      //       url: '/collection/my-task-list',
      //       menuCategoryId: 1,
      //       subMenus: [
      //         {
      //           menuName: 'List',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Edit',
      //           menuCategoryId: 3,
      //         }
      //       ],
      //     },
      //     {
      //       menuName: 'Vendor Registration',
      //       url: '/collection/vendor-registration',
      //       menuCategoryId: 1,
      //       subMenus: [
      //         {
      //           menuName: 'Add',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Edit',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'List',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Email',
      //           menuCategoryId: 3,
      //         }
      //       ],
      //     },
      //     {
      //       menuName: 'Bank Statement Uploads',
      //       url: '',
      //       menuCategoryId: 1,
      //       subMenus: [
      //        {
      //         menuName: 'Statement Uploads',
      //         url: '/collection/credit-controller/bank-statement-uploads',
      //         menuCategoryId: 1,
      //         subMenus: [
      //           {
      //             menuName: 'Add',
      //             menuCategoryId: 3,
      //           },
      //           {
      //             menuName: 'List',
      //             menuCategoryId: 3,
      //           }
      //         ],
      //        },
      //        {
      //         menuName: 'Posting batches Logs',
      //         url: '/collection/credit-controller/payment-batch-logs',
      //         menuCategoryId: 1,
      //         subMenus: [
      //           {
      //             menuName: 'List',
      //             menuCategoryId: 3,
      //           }
      //         ],
      //        },
      //        {
      //         menuName: 'Posted file status',
      //         url: '/collection/credit-controller/posted-file-status',
      //         menuCategoryId: 1,
      //         subMenus: [
      //           {
      //             menuName: 'List',
      //             menuCategoryId: 3,
      //           }
      //         ],
      //        }
      //       ],
      //     }
      //   ]
      // }
      // let menus = {
      //   menuName: 'Dealer Management',
      //   smallIcon: 'sidebar-item-icon fa fa-user-o',
      //   subMenus: [
      //     {
      //       menuName: 'Dealer Maintenance',
      //       url: '/dealer/dealer-maintenance',
      //       menuCategoryId: 1,
      //       subMenus: [
      //         {
      //           menuName: 'Add',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'List',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Dealer Info',
      //           menuCategoryId: 2,
      //           subMenus: [
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Reinstate',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Termination',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Suspend',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         }, {
      //           menuName: 'Dealer Principle',
      //           menuCategoryId: 2,
      //           subMenus: [
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         },
      //         {
      //           menuName: 'Branch Details',
      //           menuCategoryId: 2,
      //           subMenus: [
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Delete',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         },
      //         {
      //           menuName: 'Dealer Branch Category',
      //           menuCategoryId: 2,
      //           subMenus: [
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         },
      //         {
      //           menuName: 'Dealer Documents',
      //           menuCategoryId: 2,
      //           subMenus: [
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         },
      //       ]

      //     }, {
      //       menuName: 'Configuration',
      //       url: '',
      //       menuCategoryId: 1,
      //       subMenus: [
      //         {
      //           menuName: 'Dealer Type',
      //           url: '/dealer/dealer-type',
      //           menuCategoryId: 1,
      //           subMenus: [
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Delete',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         },
      //         {
      //           menuName: 'Dealer Category',
      //           url: '/dealer/dealer-category',
      //           menuCategoryId: 1,
      //           subMenus: [
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Delete',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         },
      //         {
      //           menuName: 'Dealer Type Configuration',
      //           url: '/dealer/dealer-type-config',
      //           menuCategoryId: 1,
      //           subMenus: [
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Delete',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         },
      //         {
      //           menuName: 'Dealer Commission Configuration',
      //           url: '/dealer/dealer-commission-config',
      //           menuCategoryId: 1,
      //           subMenus: [
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Delete',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         },
      //         {
      //           menuName: 'Dealer Document Type',
      //           url: '/dealer/dealer-document-type',
      //           menuCategoryId: 1,
      //           subMenus: [
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Delete',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         },
      //         {
      //           menuName: 'Dealer Required Document',
      //           url: '/dealer/dealer-req-doc-config',
      //           menuCategoryId: 1,
      //           subMenus: [

      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         },
      //         {
      //           menuName: 'Average Contract Price',
      //           url: '/dealer/average-contract-config',
      //           menuCategoryId: 1,
      //           subMenus: [
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             },
      //           ]
      //         },
      //         {
      //           menuName: 'DSF Config',
      //           url: '/dealer/dsf-config',
      //           menuCategoryId: 1,
      //           subMenus: [
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Delete',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         }

      //       ]
      //     }, {
      //       menuName: 'Customer Contract',
      //       url: '/dealer/dealer-contract',
      //       menuCategoryId: 1,
      //       subMenus: [
      //         {
      //           menuName: 'List',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Add',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Edit',
      //           menuCategoryId: 3,
      //         }
      //       ]
      //     },
      //     {
      //       menuName: 'Dealer Stock Order',
      //       url: '/dealer/dealer-stock-order',
      //       menuCategoryId: 1,
      //       subMenus: [
      //         {
      //           menuName: 'List',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Add',
      //           menuCategoryId: 3,
      //         }
      //       ]
      //     },
      //     {
      //       menuName: 'Dealer Staff Registration',
      //       url: '/dealer/employee-management',
      //       menuCategoryId: 1,
      //       subMenus: [
      //         {
      //           menuName: 'Add',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'List',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Edit',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Reinstate',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Termination',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Suspend',
      //           menuCategoryId: 3,
      //         }
      //       ]
      //     },
      //     {
      //       menuName: 'Dealer Billing Management',
      //       url: '/dealer/dealer-billing-configuration',
      //       menuCategoryId: 1,
      //       subMenus: [
      //         {
      //           menuName: 'WEEKLY',
      //           menuCategoryId: 2,
      //           subMenus: [
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         },
      //         {
      //           menuName: 'MONTHLY',
      //           menuCategoryId: 2,
      //           subMenus: [
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         },
      //         {
      //           menuName: 'Quarterly',
      //           menuCategoryId: 2,
      //           subMenus: [
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         },
      //         {
      //           menuName: 'Yearly',
      //           menuCategoryId: 2,
      //           subMenus: [
      //             {
      //               menuName: 'List',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Add',
      //               menuCategoryId: 3,
      //             },
      //             {
      //               menuName: 'Edit',
      //               menuCategoryId: 3,
      //             }
      //           ]
      //         }
      //       ]
      //     },
      //     {
      //       menuName: 'Dealer Billing',
      //       url: '/dealer/dealer-billing-commission',
      //       menuCategoryId: 1,
      //       subMenus: [
      //         {
      //           menuName: 'List',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Download',
      //           menuCategoryId: 3,
      //         }
      //       ]
      //     },
      //     {
      //       menuName: 'Cancel Stock Order',
      //       url: '/dealer/dealer-stock-order/cancellation',
      //       menuCategoryId: 1,
      //       subMenus: [
      //         {
      //           menuName: 'List',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Add',
      //           menuCategoryId: 3,
      //         }
      //       ]
      //     },
      //     {
      //       menuName: 'Clawback',
      //       url: '/dealer/clawback',
      //       menuCategoryId: 1,
      //       subMenus: [
      //         {
      //           menuName: 'List',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Download',
      //           menuCategoryId: 3,
      //         }
      //       ]
      //     },
      //     {
      //       menuName: 'Clawback Reversal',
      //       url: '/dealer/clawback-reversal',
      //       menuCategoryId: 1,
      //       subMenus: [
      //         {
      //           menuName: 'List',
      //           menuCategoryId: 3,
      //         },
      //         {
      //           menuName: 'Download',
      //           menuCategoryId: 3,
      //         }
      //       ]
      //     },
      //   ]
      // }

      //  await this.prepareAllMenusFromParent(menus)

      // await this.prepareAllMenusFromParent({
      //   menuName: 'My Task',
      //   smallIcon: 'sidebar-item-icon fa fa-street-view',
      //   subMenus: [
      //     {
      //     menuName: 'My Sales',
      //     url: '/my-tasks/my-sales',
      //     menuCategoryId: 1,
      //     subMenus: [
      //       {
      //       menuName:'My Raw Leads',
      //       menuCategoryId:2,
      //       subMenus:[
      //         {
      //           menuName: 'List',
      //           menuCategoryId: 3,
      //         }, {
      //           menuName: 'Add',
      //           menuCategoryId: 3,
      //         }, {
      //           menuName: 'Edit',
      //           menuCategoryId: 3,
      //         }
      //       ]
      //     },
      //     {
      //       menuName:'My Leads',
      //       menuCategoryId:2,
      //       subMenus:[
      //         {
      //           menuName:'Add',
      //           menuCategoryId:3
      //         },
      //         {
      //           menuName: 'Landing Page',
      //           menuCategoryId: 3,
      //           subMenus:[
      //             {
      //               menuName: 'AssignReassign',
      //               menuCategoryId: 3,
      //               subMenus:[{
      //                 menuName:'Add',
      //                 menuCategoryId:3
      //               }]
      //             },
      //             {
      //               menuName: 'Sales Documents',
      //               menuCategoryId: 3,
      //               subMenus:[{
      //                 menuName:'List',
      //                 menuCategoryId:3
      //               }]
      //             },
      //             {
      //               menuName: 'Other Documents',
      //               menuCategoryId: 3,
      //               subMenus:[
      //                 {
      //                 menuName:'List',
      //                 menuCategoryId:3
      //               },
      //               {
      //                 menuName: 'Add',
      //                 menuCategoryId: 3,
      //               },
      //               {
      //                 menuName: 'Edit',
      //                 menuCategoryId: 3,
      //               },
      //               {
      //                 menuName: 'Download',
      //                 menuCategoryId: 3,
      //               }]
      //             },
      //             {
      //               menuName: 'SMS',
      //               menuCategoryId: 3,
      //               subMenus:[{
      //                 menuName:'Add',
      //                 menuCategoryId:3
      //               }]
      //             },
      //             {
      //               menuName: 'Email',
      //               menuCategoryId: 3,
      //               subMenus:[{
      //                 menuName:'Send',
      //                 menuCategoryId:3
      //               }]
      //             },
      //             {
      //               menuName: 'Schedule Call Back',
      //               menuCategoryId: 3,
      //               subMenus:[{
      //                 menuName:'Submit',
      //                 menuCategoryId:3
      //               }]
      //             },
      //             {
      //               menuName: 'Dial',
      //               menuCategoryId: 3,
      //               subMenus:[{
      //                 menuName:'Dial',
      //                 menuCategoryId:3
      //               }]
      //             },
      //             {
      //               menuName: 'Notes',
      //               menuCategoryId: 3,
      //               subMenus:[{
      //                 menuName:'Add',
      //                 menuCategoryId:3
      //               }]
      //             },
      //             {
      //               menuName: 'Update Lead Info',
      //               menuCategoryId: 3,
      //               subMenus:[{
      //                 menuName:'Edit',
      //                 menuCategoryId:3
      //               }]
      //             },
      //             {
      //               menuName: 'Appointment History',
      //               menuCategoryId: 3,
      //               subMenus:[{
      //                 menuName:'List',
      //                 menuCategoryId:3
      //               }]
      //             },
      //             {
      //               menuName: 'Tele Sales Call',
      //               menuCategoryId: 3,
      //               subMenus:[{
      //                 menuName:'Edit',
      //                 menuCategoryId:3
      //               }]
      //             },
      //             {
      //               menuName: 'Schedule Appointment',
      //               menuCategoryId: 3,
      //               subMenus:[{
      //                 menuName:'Save',
      //                 menuCategoryId:3
      //               }]
      //             },
      //           ]
      //         },
      //       ]
      //     },
      //     {
      //       menuName:'My Quote Follow-Up',
      //       menuCategoryId:2,
      //       subMenus:[
      //         {
      //           menuName: 'List',
      //           menuCategoryId: 3,
      //         }
      //       ]
      //     },
      //     {
      //       menuName:'My Stop And Knock',
      //       menuCategoryId:2,
      //       subMenus:[
      //         {
      //           menuName: 'List',
      //           menuCategoryId: 3,
      //         }, {
      //           menuName: 'Edit',
      //           menuCategoryId: 3,
      //         }
      //       ]
      //     },
      //   ]
      //   },
      //   {
      //     menuName: 'Boundary Management',
      //     url: '',
      //     menuCategoryId: 1,
      //     subMenus: [{
      //       menuName: 'View Request',
      //       url:'/my-tasks/boundary-management/view-request',
      //       menuCategoryId: 1,
      //       subMenus: [{
      //         menuName: 'List',
      //         menuCategoryId: 3
      //       }, {
      //         menuName: 'Edit',
      //         menuCategoryId: 3,
      //       }]
      //     },
      //     {
      //       menuName: 'Approval Dashboard',
      //       url:'/my-tasks/boundary-management/approval-dashboard',
      //       menuCategoryId: 1,
      //       subMenus: [{
      //         menuName: 'List',
      //         menuCategoryId: 3
      //       }, {
      //         menuName: 'Edit',
      //         menuCategoryId: 3,
      //       },
      //       {
      //         menuName: 'Approve',
      //         menuCategoryId: 3,
      //       },
      //       {
      //         menuName: 'Decline',
      //         menuCategoryId: 3,
      //       }]
      //     }
      //   ]
      //   },
      //   {
      //     menuName: 'Sales DOA',
      //     url: '/my-tasks/sales-DOA',
      //     menuCategoryId: 1,
      //     subMenus: [{
      //       menuName: 'List',
      //       menuCategoryId: 3,
      //     }, {
      //       menuName: 'Edit',
      //       menuCategoryId: 3,
      //     }, {
      //       menuName: 'Approve',
      //       menuCategoryId: 3,
      //     },
      //     {
      //       menuName: 'Decline',
      //       menuCategoryId: 3,
      //     }]
      //   },
      //   {
      //     menuName: 'My Tickets',
      //     url: '/my-tasks/my-tickets',
      //     menuCategoryId: 1,
      //     subMenus: [{
      //       menuName: 'List',
      //       menuCategoryId: 3,
      //     }, {
      //       menuName: 'Edit',
      //       menuCategoryId: 3,
      //     }]
      //   },
      //   {
      //     menuName: 'Over Look Escalations',
      //     url: '/my-tasks/overlook-escalations',
      //     menuCategoryId: 1,
      //     subMenus: [{
      //       menuName: 'List',
      //       menuCategoryId: 3,
      //     }, {
      //       menuName: 'Edit',
      //       menuCategoryId: 3,
      //     },
      //     {
      //       menuName: 'Resolve Escalation',
      //       menuCategoryId: 3,
      //     }]
      //   },
      //   {
      //     menuName: 'Task List',
      //     url: '/my-tasks/task-list',
      //     menuCategoryId: 1,
      //     subMenus: [{
      //       menuName: 'List',
      //       menuCategoryId: 3,
      //     }, {
      //       menuName: 'Edit',
      //       menuCategoryId: 3,
      //     }]
      //   },
      //   {
      //     menuName: 'Validate Address',
      //     url: '/my-tasks/validate-address',
      //     menuCategoryId: 1,
      //     subMenus: [{
      //       menuName: 'List',
      //       menuCategoryId: 3,
      //     }, {
      //       menuName: 'Edit',
      //       menuCategoryId: 3,
      //     }]
      //   }]
      // });

      //await this.prepareBulkMenuFeaturesByMenuId(['List','Edit'],"24cf9cfb-9a0b-49a7-b64c-37e56b68d655");
    });
  }

  getMenuCategoryFeatureTypes() {
    this.crudService.dropdown(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.UX_MENU_CATEGORY).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.featureTypes = response.resources;
          if (this.data.menuId && !this.data.type.toLowerCase().includes('add')) {
            this.getMenuDetailsByMenuId();
          }
          else {
            this.rxjsService.setPopupLoaderProperty(false);
          }
        }
      });
  }

  getMenuDetailsByMenuId() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.MENU_DETAIL, this.data.menuId)
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.menuConfigForm.patchValue(response.resources);
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  createMenuConfigurationForm(): void {
    let menuConfigModel = new MenuConfigModel();
    this.menuConfigForm = this.formBuilder.group({});
    Object.keys(menuConfigModel).forEach((key) => {
      this.menuConfigForm.addControl(key, new FormControl(menuConfigModel[key]));
    });
    let validatorList = ["menuName", "menuCategoryId", "sortOrder"];
    if ((this.data.type == 'new parent' || this.data.type == 'edit first level child' || this.data.type == 'edit nested level child') && (this.data.level === 0)) {
      //  alert("SHOW ICON")
      validatorList.push('smallIcon');
      this.isSmallIconIsRequired = true;
    }
    this.menuConfigForm = setRequiredValidator(this.menuConfigForm, validatorList);
  }


  onSubmit(): void {
    if (this.menuConfigForm.invalid) {
      return;
    }
    this.menuConfigForm.value.userId = this.loggedInUserData.userId;
    if (this.data.menuId && !this.data.menuParentId && this.data.type.toLowerCase().includes('add') ||
      this.data.menuId && this.data.menuParentId && this.data.type.toLowerCase().includes('add')) {
      this.menuConfigForm.value.menuParentId = this.data.menuId;
    }
    else if (this.data.menuParentId && this.data.type.toLowerCase().includes('edit')) {
      this.menuConfigForm.value.menuParentId = this.data.menuParentId;

    }

    this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.MENU_CONFIG_MENUS, this.menuConfigForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.ref.close(true)
        }
        this.rxjsService.setPopupLoaderProperty(false);
      });
  }

  // this method is for only development purpose where have hardcoded some menu features as per input params
  async prepareAllMenusFromParent(inputObject) {
    let payload = {
      userId: this.loggedInUserData.userId,
      menuId: null,
      menuName: inputObject.menuName,
      url: "",
      menuParentId: null,
      menuCategoryId: 1,
      sortOrder: this.data.sortOrderForParent,
      isActive: true,
      smallIcon: inputObject.smallIcon
    }
    // create top level parent first
    await this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.MENU_CONFIG_MENUS, payload).subscribe(async (response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.prepareSubMenusFromParent(inputObject.subMenus, response.resources);
        }
      });
  }

  async prepareSubMenusFromParent(subMenusArr, menuParId: string) {
    let subMenus = subMenusArr, menuParentId = menuParId;
    for (let i = 0; i < subMenus?.length; i++) {
      let currentLoopedMenuProps = subMenus[i];
      let payload = {
        userId: this.loggedInUserData.userId,
        menuId: null,
        menuName: currentLoopedMenuProps.menuName,
        url: currentLoopedMenuProps.url ? currentLoopedMenuProps.url : "",
        menuParentId,
        menuCategoryId: currentLoopedMenuProps.menuCategoryId ? currentLoopedMenuProps.menuCategoryId : 3,
        sortOrder: i + 1,
        isActive: true,
      }
      await this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.MENU_CONFIG_MENUS, payload).subscribe(async (response) => {
          if (response.resources && response.isSuccess && response.statusCode == 200) {
            await this.prepareSubMenusFromParent(currentLoopedMenuProps.subMenus, response.resources);
          }
        });
    }
  }

  // async prepareFirstLevelSubMenusFromParent(subMenusArr, menuParId: string) {
  //   let subMenus = subMenusArr, menuParentId = menuParId;
  //   for (let i = 0; i < subMenus.length; i++) {
  //     let currentLoopedMenuProps = subMenus[i];
  //     let payload = {
  //       userId: this.loggedInUserData.userId,
  //       menuId: null,
  //       menuName: currentLoopedMenuProps.menuName,
  //       url: currentLoopedMenuProps.url ? currentLoopedMenuProps.url : "",
  //       menuParentId,
  //       menuCategoryId: currentLoopedMenuProps.menuCategoryId ? currentLoopedMenuProps.menuCategoryId : 3,
  //       sortOrder: i + 1,
  //       isActive: true,
  //     }
  //     await this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,
  //       UserModuleApiSuffixModels.MENU_CONFIG_MENUS, payload).toPromise();
  //   }
  // }

  async prepareBulkMenuFeaturesByMenuId(listOfMenuFeatures: Array<string>, menuParentId: string) {
    let payload;
    for (let i = 0; i < listOfMenuFeatures.length; i++) {
      payload = {
        userId: this.loggedInUserData.userId,
        menuId: null,
        menuName: listOfMenuFeatures[i],
        url: "",
        menuParentId,
        menuCategoryId: 3,
        sortOrder: i + 1,
        isActive: true,
        smallIcon: ""
      }
      await this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.MENU_CONFIG_MENUS, payload).toPromise();
    }
  }

  closeDialog(flag) {
    this.ref.close(flag)
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
