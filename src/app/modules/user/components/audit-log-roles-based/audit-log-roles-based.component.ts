import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, CrudService, SnackbarService, ReusablePrimeNGTableFeatureService, RxjsService, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, prepareDynamicTableTabsFromPermissions, CrudType, prepareGetRequestHttpParams, IApplicationResponse, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { loggedInUserData } from '@modules/others';
import { UserModuleApiSuffixModels, IT_MANAGEMENT_COMPONENT } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-audit-log-roles-based',
  templateUrl: './audit-log-roles-based.component.html',
})
export class AuditLogRolesBasedComponent extends PrimeNgTableVariablesModel implements OnInit {

  observableResponse;
  SearchText = ""
  primengTableConfigProperties: any = {
    tableCaption: "Audit Logs Roles Configuration",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'IT Management ', relativeRouterUrl: '' }, { displayName: 'Audit Logs Roles Configuration' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Audit Logs Roles Configuration',
          dataKey: 'roleId',
          enableBreadCrumb: true,
          enableAction: true,
          enableAddActionBtn: true,
          enableStatusActiveAction: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'roleName', header: 'Role' },
          { field: 'modules', header: 'Modules' },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: UserModuleApiSuffixModels.AUDIT_LOGS_ROLE_BASED_ACCESS,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT
        }]
    }
  }

  constructor(
    private dialog: MatDialog,
    private crudService: CrudService,
    private router: Router,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private rxjsService: RxjsService,
    private dialogService: DialogService) {
    super()
  }

  ngOnInit() {
    this.getAuditLogsPermissionsList()
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.AUDIT_LOG_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getAuditLogsPermissionsList(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.AUDIT_LOGS_ROLE_BASED_ACCESS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    console.log(type)
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }

        this.router.navigate(['/user/audit-log-configuration/add-edit'])

        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getAuditLogsPermissionsList(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;

        case CrudType.DELETE:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }

          const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
            showHeader: false,
            baseZIndex: 10000,
            width: '400px',
            data: {
              dataObject:{ids: row["roleId"], modifiedUserId: this.loggedInUserData.userId,},
              isdeleted: false,
              modifiedUserId: this.loggedInUserData.userId,
              moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
              apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel,
              method:'delete'
            },
          });
          ref.onClose.subscribe((result) => {
            this.selectedRows = [];
            if (result) {
              this.getAuditLogsPermissionsList();
            }
          });


          // this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          //   this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
          //     if (result) {
          //       this.selectedRows = [];
          //       this.getAuditLogsPermissionsList();
          //     }
          //   });
          break;

      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        console.log(row,searchObj)
        this.router.navigate(['/user/audit-log-configuration/add-edit'],{queryParams:{roleId: row["roleId"]}})

        // this.OpenDialogWindow(CrudType.VIEW, row);
        break;
    }
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

}
