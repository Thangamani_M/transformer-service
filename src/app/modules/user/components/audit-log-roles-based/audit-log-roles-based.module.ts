import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditLogRolesBasedComponent } from './audit-log-roles-based.component';
import { AuditLogRolesBasedRouteModule } from './audit-log-roles-based.routing';
import { LayoutModule, MaterialModule, SharedModule } from '@app/shared';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuditLogRolesBasedAddEditComponent } from './audit-log-roles-based-add-edit.component';
import {OrderListModule} from 'primeng/orderlist';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    OrderListModule,
    AuditLogRolesBasedRouteModule
  ],
  declarations: [AuditLogRolesBasedComponent,AuditLogRolesBasedAddEditComponent]
})
export class AuditLogRolesBasedModule { }
