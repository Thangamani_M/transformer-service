import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, getPDropdownData, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { AuditLogsRolesBasedFormModel } from '@modules/user/models/it-management-models';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-audit-log-roles-based-add-edit',
  templateUrl: './audit-log-roles-based-add-edit.component.html',
  styles: [`
  ::ng-deep body .ibox-body .ui-inputtext {
    border:  unset !important;
  }
  ::ng-deep label.ui-dropdown-label.ui-inputtext.ui-corner-all.ui-placeholder{
    height:unset !important;
  }
  .list-border-1{
    padding:5px;
    border:1x solid #ddd;
    max-height:44vh;
    overflow-y:scroll;
    height:43vh;
    width:100%;
  },
  .largerCheckbox {
            width: 40px !important;
            height: 40px !important;
        }
`]
})
export class AuditLogRolesBasedAddEditComponent implements OnInit {
  auditLogsRolesBasedForm: FormGroup;
  rolesDropDown = []
  modulesDropdown = []
  pagesDropdown = []
  loggedInUserData: LoggedInUserModel

  primengTableConfigProperties: any = {
    tableCaption: "Audit Logs Roles Based Access",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'IT Management ', relativeRouterUrl: '' }, { displayName: 'Audit Logs Roles Configuration', relativeRouterUrl: '/user/audit-log-configuration' }, { displayName: 'Save Audit Logs Roles Configuration' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Audit Logs Roles Configuration',
          dataKey: 'roleId',
          enableBreadCrumb: true
        }
      ]
    }
  };
  roleId = ""
  isEdit = false
  constructor(
    private crudService: CrudService,
    private router: Router,
    private store: Store<AppState>,
    private rxjsService: RxjsService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe(param => {
      this.roleId = param['roleId'];
      if (param['roleId']) {
        this.isEdit = true
      }
    })
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.createAuditLogRolesBasedForm()
    this.getRolesDropdown()
    this.getModulesDropdown()
    this.rxjsService.setGlobalLoaderProperty(false)
    if (this.roleId) {
      this.getDetailsById()
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }
  getDetailsById() {
    this.rxjsService.setGlobalLoaderProperty(true)

    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.AUDIT_LOGS_ROLE_BASED_ACCESS, this.roleId).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false)
      if (response.isSuccess) {
        console.log(response)
        let moduleIds = []
        response.resources?.modules.forEach(item => {
          moduleIds.push(item?.moduleId)
        })
        this.auditLogsRolesBasedForm.get('moduleIds').setValue(moduleIds, { emitEvent: false, onlySelf: true })
        this.auditLogsRolesBasedForm.get("roleId").setValue(this.roleId, { emitEvent: false, onlySelf: true });
        this.auditLogsRolesBasedForm.get("isActive").setValue(response.resources?.isActive);
        this.auditLogsRolesBasedForm.get("isExport").setValue(response.resources?.isExport);
        this.pagesDropdown = response.resources?.pages
        this.checkPageAndPatchValue();


      }
    })
  }

  createAuditLogRolesBasedForm() {
    let AuditLogRolesBasedModel = new AuditLogsRolesBasedFormModel();
    this.auditLogsRolesBasedForm = this.formBuilder.group({});
    Object.keys(AuditLogRolesBasedModel).forEach((key) => {
      this.auditLogsRolesBasedForm.addControl(key, new FormControl(AuditLogRolesBasedModel[key]));
    });
    this.auditLogsRolesBasedForm = setRequiredValidator(this.auditLogsRolesBasedForm, ["roleId", "auditPageIds"]);
    this.onFormControlChanges();
  }

  getRolesDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_ROLES).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false)
      if (response.isSuccess) {
        this.rolesDropDown = getPDropdownData(response.resources);
      }
    })
  }

  getModulesDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_MODULES).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false)
      if (response.isSuccess) {
        this.modulesDropdown = getPDropdownData(response.resources);
      }
    })
  }

  onFormControlChanges() {
    this.auditLogsRolesBasedForm.get("roleId").valueChanges.subscribe(roleId => {
      if (roleId) {
        this.roleId = roleId
        this.getDetailsById()
      }
    })

    this.auditLogsRolesBasedForm.get("moduleIds").valueChanges.subscribe(moduleIds => {
      if (moduleIds.length != 0) {
        this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.AUDIT_LOGS_PAGE, undefined, false, prepareRequiredHttpParams({ moduleIds: moduleIds.join(','), roleId: this.auditLogsRolesBasedForm.get("roleId").value })).subscribe(response => {
          this.rxjsService.setGlobalLoaderProperty(false)
          if (response.isSuccess) {
            this.pagesDropdown = response.resources
            this.checkPageAndPatchValue()
          }
        })
      }else{
        this.auditLogsRolesBasedForm.get("auditPageIds").setValue([])
        this.pagesDropdown = []
      }
    })
    this.auditLogsRolesBasedForm.get("isSelectedAll").valueChanges.subscribe(boolean => {
      console.log("boolean", boolean)
      console.log("boolean 1", this.auditLogsRolesBasedForm.get("isSelectedAll").value)
      this.selectAndUnselectAll(boolean)
    })
    this.auditLogsRolesBasedForm.get("auditPageIds").valueChanges.subscribe(ids => {
      if (ids.length != 0) {
        if (ids.length == this.pagesDropdown.length) {
          this.auditLogsRolesBasedForm.get("isSelectedAll").setValue(true, { emitEvent: false, onlySelf: true })
        } else {
          this.auditLogsRolesBasedForm.get("isSelectedAll").setValue(false, { emitEvent: false, onlySelf: true })
        }
      } else {
        this.auditLogsRolesBasedForm.get("isSelectedAll").setValue(false, { emitEvent: false, onlySelf: true })

      }
    })
  }

  selectAndUnselectAll(isSelect) {
    let selectedPageIds = []
    if (isSelect) {
      this.pagesDropdown.forEach(item => {
        selectedPageIds.push(item.auditPageId);
      })
    }
    this.auditLogsRolesBasedForm.get("auditPageIds").setValue(selectedPageIds, { emitEvent: false, onlySelf: true })
  }

  checkPageAndPatchValue() {
    let selectedPageIds = []
    this.pagesDropdown.forEach(item => {
      if (item?.auditLogRoleBasedAccessId) {
        selectedPageIds.push(item.auditPageId);
      }
    })
    this.auditLogsRolesBasedForm.get("auditPageIds").setValue(selectedPageIds)
    if (selectedPageIds.length == this.pagesDropdown.length && this.pagesDropdown.length !=0) {
      this.auditLogsRolesBasedForm.get("isSelectedAll").setValue(true, { emitEvent: false, onlySelf: true })
    } else {
      this.auditLogsRolesBasedForm.get("isSelectedAll").setValue(false, { emitEvent: false, onlySelf: true })
    }
  }
  onSubmit() {
    if (this.auditLogsRolesBasedForm.invalid) {
      return "";
    }
    let formObj = this.auditLogsRolesBasedForm.getRawValue()
    console.log(this.auditLogsRolesBasedForm.value)
    formObj.createdUserId = this.loggedInUserData?.userId

    this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.AUDIT_LOGS_ROLE_BASED_ACCESS, formObj).subscribe(response => {
      if (response.isSuccess) {
        this.cancel()
      }
    })
  }
  cancel() {
    this.router.navigate(['/user/audit-log-configuration']);
  }
}
