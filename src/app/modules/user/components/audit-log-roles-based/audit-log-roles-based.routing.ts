import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditLogRolesBasedAddEditComponent } from './audit-log-roles-based-add-edit.component';
import { AuditLogRolesBasedComponent } from './audit-log-roles-based.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


const routes: Routes = [
  { path: '', component: AuditLogRolesBasedComponent, pathMatch: 'full',canActivate:[AuthGuard],data:{title:"Audit Log Configuration"} },
  { path: 'add-edit', component: AuditLogRolesBasedAddEditComponent,canActivate:[AuthGuard], data:{title:"Audit Log Configuration Add/Edit"} },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditLogRolesBasedRouteModule { }
