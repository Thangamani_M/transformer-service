import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ProvinceRoutingModule, StateAddEditComponent, StateListComponent } from '@user/components/province';

@NgModule({
  declarations: [StateListComponent, StateAddEditComponent],
  imports: [
    CommonModule,
    ProvinceRoutingModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents: [StateAddEditComponent]
})
export class ProvinceModule { }