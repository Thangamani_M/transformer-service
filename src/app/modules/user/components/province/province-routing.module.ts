import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StateListComponent } from '@user/components/province';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: 'list', component: StateListComponent, canActivate:[AuthGuard],data: { title: 'Province' } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class ProvinceRoutingModule { }
