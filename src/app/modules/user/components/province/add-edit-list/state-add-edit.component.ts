import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, prepareRequiredHttpParams } from '@app/shared';
import { AlertService, CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';
import { State } from '@user/models';
@Component({
  selector: 'state-add-edit',
  templateUrl: './state-add-edit.component.html',
})
export class StateAddEditComponent implements OnInit {

  @Input() id: string;
  stateAddEditForm: FormGroup;
  applicationResponse: IApplicationResponse;
  errorMessage: string;
  btnName: string;
  countryList: any;
  regionList: any;
  ddlFilterCountry: any = '';
  isFormSubmitted = false;
  isDialogLoaderLoading: boolean;
  loggedUser: UserLogin;
  public alphaNumericWithSpacePattern = { '0': { pattern: new RegExp('^[A-Za-z0-9 ]*$') } };

  constructor(@Inject(MAT_DIALOG_DATA) public data: State,
    private dialog: MatDialog,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private alertService: AlertService, private httpCancelService: HttpCancelService,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.data.provinceId) {
      this.btnName = 'Update';
      this.ddlFilterCountry = this.data.countryId;
    } else {
      this.btnName = 'Create';
    }
    this.stateAddEditForm = this.formBuilder.group({
      countryId: this.data.countryId || '',
      regionId: this.data.regionId || '',
      provinceName: [this.data.provinceName || '', Validators.required],
      provinceCode: this.data.provinceCode || ''
    });

    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_COUNTRIES).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode == 200) {
        this.countryList = response.resources;
        this.ddlFilterCountry = this.countryList.find(({ displayName }) => displayName === 'South Africa').id;
        this.loadRegions();
       }
    });
  }
  loadRegions() {

    if (this.data.provinceId != null) {
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS, undefined, false, prepareRequiredHttpParams({countryId:this.ddlFilterCountry})).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.regionList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
    else {
      this.regionList = [];
      this.stateAddEditForm.controls['regionId'].setValue('');
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS, undefined, false, prepareRequiredHttpParams({countryId:this.ddlFilterCountry})).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.regionList = response.resources;
         }
         this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }



  save(): void {
    if (this.stateAddEditForm.invalid) {
      this.stateAddEditForm.markAllAsTouched();
      return;
    }
    this.isFormSubmitted = true;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    const state = { ...this.data, ...this.stateAddEditForm.value }
    let API;
    if (state.provinceId != null) {
      state.modifiedUserId = this.loggedUser.userId;
      API = this.crudService.update(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.PROVINCES,state);
    }
    else {
      state.createdUserId = this.loggedUser.userId;
      API = this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.PROVINCES,state);
    }
    API.subscribe(response => {
      if(response.isSuccess && response.statusCode ==200){
        this.dialog.closeAll();
      }
    })
  }



  onSaveComplete(response): void {
    if (response.statusCode == 200) {
      this.dialog.closeAll();
      // Reset the form to clear the flags
      this.stateAddEditForm.reset();
    }
    else {
      //Display Alert message
      this.alertService.processAlert(response);
    }
    this.isFormSubmitted = false;
  }
}
