import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, SnackbarService } from '@app/shared';
import { HttpCancelService, RxjsService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { loggedInUserData } from '@modules/others';
import { IT_MANAGEMENT_COMPONENT, UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { StateAddEditComponent } from './';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-state-list',
  templateUrl: './state-list.component.html',
})

export class StateListComponent extends PrimeNgTableVariablesModel implements OnInit {
  applicationResponse: IApplicationResponse;
  observableResponse;
  primengTableConfigProperties: any = {
    tableCaption: "Province",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'IT Management ', relativeRouterUrl: '' }, { displayName: 'Province List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Province',
          dataKey: 'provinceId',
          enableBreadCrumb: true,
          enableAction: true,
          enableReset: false,
          enableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableStatusActiveAction: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 2,
          columns: [{ field: 'countryName', header: 'Country Name' },
          { field: 'regionName', header: 'Region Name' },
          { field: 'provinceName', header: 'Province Name' },
          { field: 'provinceCode', header: 'Province Code' },
          { field: 'createdDate', header: 'Created Date', isDate: true },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: UserModuleApiSuffixModels.PROVINCES,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT
        }]
    }
  }
  constructor(
    private dialog: MatDialog,
    private crudService: CrudService,
    private store: Store<AppState>,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    public snackbarService: SnackbarService,
    public dialogService: DialogService,
    private rxjsService: RxjsService, private httpCancelService: HttpCancelService) {
    super()
  }

  ngOnInit() {
    this.getStates();
    this.combineLatestNgrxStoreData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.PROVINCE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getStates(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.PROVINCES,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.OpenDialogWindow(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getStates(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.OpenDialogWindow(CrudType.EDIT, row);
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  OpenDialogWindow(type: CrudType | string, state) {
    const dialogReff = this.dialog.open(StateAddEditComponent, { width: '450px', minHeight: '450px', data: state ? state : {}, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
      if (result) {
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        return;
      };
      this.rxjsService.setDialogOpenProperty(false);
      this.getStates();
    });
  }
}
