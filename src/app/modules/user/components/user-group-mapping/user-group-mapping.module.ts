import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { UserGroupMappingAddEditComponent, UserGroupMappingListComponent, UserGroupMappingRoutingModule, UserGroupMappingViewComponent } from '@user-components/user-group-mapping';



@NgModule({
  declarations: [UserGroupMappingAddEditComponent, UserGroupMappingViewComponent, UserGroupMappingListComponent],
  imports: [
    CommonModule,
    MaterialModule, SharedModule, LayoutModule, UserGroupMappingRoutingModule,
    ReactiveFormsModule, FormsModule
  ]
})
export class UserGroupMappingModule { }
