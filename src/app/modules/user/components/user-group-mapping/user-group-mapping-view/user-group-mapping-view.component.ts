import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { IT_MANAGEMENT_COMPONENT, UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-user-group-mapping-view',
  templateUrl: './user-group-mapping-view.component.html',
})

export class UserGroupMappingViewComponent implements OnInit {
  userGroupDetail;
  groupId: string;
  primengTableConfigProperties;
  componentPermissions = [];

  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,private rxjsService: RxjsService, private router: Router, private snackbarService: SnackbarService
  ) {
    this.primengTableConfigProperties = {
      tableCaption: "View Group",
      breadCrumbItems: [{ displayName: 'IT Management', relativeRouterUrl: '' },
      { displayName: 'Group List', relativeRouterUrl: '/user/user-group' },
      { displayName: 'View Group', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.activatedRoute.queryParams,this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.groupId = response[0]['id'];
      this.getMappingDetailsByGroupId(this.groupId);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.GROUP]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getMappingDetailsByGroupId(groupId): void {
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.GROUP,
      groupId
    ).subscribe(response => {
      if (response.resources && response.statusCode == 200 && response.isSuccess) {
        this.userGroupDetail = [
          { name: 'Group Name', value: response.resources.groupName }, { name: 'District', value: response.resources.groupDistricts.map(c => c.districtName) },
          { name: 'Department', value: response.resources.groupDepartments.map(c => c.departmentName) },
          { name: 'Role', value: response.resources.groupRoles.map(c => c.roleName) }, { name: 'Region', value: response.resources.groupRegions.map(c => c.regionName) },
          { name: 'Division', value: response.resources.groupDivisions.map(c => c.divisionName) },
          { name: 'Area', value: response.resources.groupAreas.map(c => c.areaName) },
        ];
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          this.router.navigate([`/user/user-group/add-edit`], {
            queryParams: { id: this.groupId }
          });
        }else{
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
          break;
    }
  }
}
