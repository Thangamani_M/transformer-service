import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData, UserGroupMappingModel, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, removeFormControlError, RxjsService, setRequiredValidator
} from '@app/shared';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
@Component({
  selector: 'app-user-group-mapping-add-edit',
  templateUrl: './user-group-mapping-add-edit.component.html',
})
export class UserGroupMappingAddEditComponent implements OnInit {
  groupMappingForm: FormGroup;
  groupId: string;
  groupList = [];
  regionList = [];
  districtList = [];
  divisionList = [];
  departmentList = [];
  areaList = [];
  roleList = [];
  selectedRegions = [];
  selectedDistricts = [];
  selectedDivisions = [];
  selectedDepartments = [];
  selectedAreas = [];
  selectedRoles = [];
  loggedInUserData: LoggedInUserModel;
  isButtonDisabled: boolean = false;
  btnName: string = "Save";
  applicationResponse: IApplicationResponse;
  existKey = [];
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAStringOnlyNoSpace = new CustomDirectiveConfig({ isAStringOnlyNoSpace: true });

  constructor(private crudService: CrudService, private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute,
    private router: Router, private rxjsService: RxjsService, private httpCancelService: HttpCancelService,
    private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createGroupMappingForm();
    this.getSelectList().subscribe((response: IApplicationResponse[]) => {
      if (!response[0].isSuccess) return;
      this.groupList = response[0].resources;
      this.regionList = response[1].resources;
      this.districtList = response[2].resources;
      this.divisionList = response[3].resources;
      this.departmentList = response[4].resources;
      this.areaList = response[5].resources;
      this.roleList = response[6].resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    if (this.groupId) {
      this.groupMappingForm.patchValue({
        groupId: this.groupId
      });
      this.getMappingDetailsByGroupId(this.groupId);
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.groupId = response[1]['id'];
    });
  }

  onFormControlChanges() {
    this.groupMappingForm.get('groupName').valueChanges.pipe(
      debounceTime(debounceTimeForSearchkeyword),
      distinctUntilChanged(),
      switchMap((groupName) => {
        if (!groupName) {
          return of();
        }
        else {
          return this.findGroupNameDuplication(groupName);
        }
      })
    ).subscribe((response: IApplicationResponse) => {
      if (response.statusCode == 200 && response.isSuccess && response.resources) {
        this.groupMappingForm.get('groupName').setErrors({ duplicate: true });
      }
      else if (response.statusCode == 200 && response.isSuccess && !response.resources) {
        this.groupMappingForm = removeFormControlError(this.groupMappingForm, "duplicate");
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  findGroupNameDuplication(groupName): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.GROUP_DETAIL, null, null,
      prepareGetRequestHttpParams(undefined, undefined, { groupName }))
  }

  createGroupMappingForm(): void {
    let groupMappingAddEditModel = new UserGroupMappingModel();
    this.groupMappingForm = this.formBuilder.group({});
    Object.keys(groupMappingAddEditModel).forEach((key) => {
      if (key === 'createdUserId' || key === 'modifiedUserId') {
        this.groupMappingForm.addControl(key, new FormControl(this.loggedInUserData.userId));
      }
      else
        this.groupMappingForm.addControl(key, new FormControl(groupMappingAddEditModel[key]));
    });
    this.groupMappingForm = setRequiredValidator(this.groupMappingForm, ["groupName","groupRegions","groupDistricts","groupRoles"]);
  }

  getSelectList(): Observable<any> {
    return forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_GROUPS),
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
        prepareGetRequestHttpParams(undefined, undefined, { countryId: formConfigs.countryId })),
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT),
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS),
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DEPARTMENTS),
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_AREAS),
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_RoLES)])
  }

  getMappingDetailsByGroupId(groupId) {
    this.clearSelection();
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.GROUP,
      groupId
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe(response => {
      let result = response.resources;
      if (result.groupRegions && result.groupRegions.length > 0) {
        this.btnName = "Update";
      }
      else
        this.btnName = "Save";

      result.groupRegions.map(item => {
        this.selectedRegions.push({id:item.regionId,displayName:item.regionName})
      })
      response.resources.groupRegions = this.selectedRegions;
      result.groupDistricts.map(item => {
        this.selectedDistricts.push({id:item.districtId,displayName:item.districtName})
      })
      response.resources.groupDistricts = this.selectedDistricts;
      result.groupDivisions.map(item => {
        this.selectedDivisions.push({id:item.divisionId,displayName:item.divisionName})
      })
      response.resources.groupDivisions = this.selectedDivisions;
      result.groupDepartments.map(item => {
        this.selectedDepartments.push({id:item.departmentId,displayName:item.departmentName})
      })
      response.resources.groupDepartments = this.selectedDepartments;
      result.groupAreas.map(item => {
        this.selectedAreas.push({id:item.areaId,displayName:item.areaName})
      })
      response.resources.groupAreas = this.selectedAreas;

      result.groupRoles.map(item => {
        this.selectedRoles.push({id:item.roleId,displayName:item.roleName})
      })
      response.resources.groupRoles = this.selectedRoles;

      this.groupMappingForm.patchValue(response.resources)
      this.groupMappingForm.controls.groupName.setValue(result.groupName, { emitEvent: false, onlySelf: true });
    });
  }

  clearSelection() {
    this.selectedRegions = [];
    this.selectedDistricts = [];
    this.selectedDivisions = [];
    this.selectedDepartments = [];
    this.selectedAreas = [];
    this.selectedRoles = [];
  }

  onSubmit(): void {
    if (this.groupMappingForm.invalid) {
      this.groupMappingForm.markAllAsTouched();
      return;
    }
    this.isButtonDisabled = true;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let groupRegionsTypes = [];
    this.groupMappingForm.get('groupRegions').value?.map((id) => {
      groupRegionsTypes.push(id.id);
    });
    let groupDistrictsTypes = [];
    this.groupMappingForm.get('groupDistricts').value?.map((id) => {
      groupDistrictsTypes.push(id.id);
    });
    let groupDivisionsTypes = [];
    this.groupMappingForm.get('groupDivisions').value?.map((id) => {
      groupDivisionsTypes.push(id.id);
    });
    let groupDepartmentsTypes = [];
    this.groupMappingForm.get('groupDepartments').value?.map((id) => {
      groupDepartmentsTypes.push(id.id);
    });
    let groupAreasTypes = [];
    this.groupMappingForm.get('groupAreas').value?.map((id) => {
      groupAreasTypes.push(id.id);
    });
    let groupRolesTypes = [];
    this.groupMappingForm.get('groupRoles').value?.map((id) => {
      groupRolesTypes.push(id.id);
    });
    this.groupMappingForm.value.groupRegions = groupRegionsTypes;
    this.groupMappingForm.value.groupDistricts = groupDistrictsTypes;
    this.groupMappingForm.value.groupDivisions = groupDivisionsTypes;
    this.groupMappingForm.value.groupDepartments = groupDepartmentsTypes;
    this.groupMappingForm.value.groupAreas = groupAreasTypes;
    this.groupMappingForm.value.groupRoles = groupRolesTypes;
    let crudService: Observable<IApplicationResponse> = this.btnName === "Save" ? this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.GROUP, this.groupMappingForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.GROUP, this.groupMappingForm.value)
    crudService.subscribe({
      next: response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.router.navigate(['/user/user-group/']);
        } else {
          this.isButtonDisabled = false;
        }
      },
      error: err => {
        this.isButtonDisabled = false;
      }
    });
  }
}
