import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserGroupMappingAddEditComponent, UserGroupMappingListComponent, UserGroupMappingViewComponent } from '@user/components/user-group-mapping';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: '', component: UserGroupMappingListComponent, canActivate: [AuthGuard], data: { title: 'Group List' } },
  { path: 'list', component: UserGroupMappingListComponent, canActivate: [AuthGuard], data: { title: 'Group List' } },
  { path: 'add-edit', component: UserGroupMappingAddEditComponent, canActivate: [AuthGuard], data: { title: 'Group Add/Edit' } },
  { path: 'view', component: UserGroupMappingViewComponent, canActivate: [AuthGuard], data: { title: 'View Group' } }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes)

  ],

})

export class UserGroupMappingRoutingModule { }
