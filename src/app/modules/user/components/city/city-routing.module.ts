import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CityListComponent } from '@user/components/city';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: 'list', component: CityListComponent, canActivate:[AuthGuard],data: { title: 'City' } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
})

export class CityRoutingModule { }
