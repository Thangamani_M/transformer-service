import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CityAddEditComponent, CityListComponent } from '@user/components/city';
import { CityRoutingModule } from '@user/components/city/city-routing.module';
@NgModule({
  declarations: [CityAddEditComponent, CityListComponent],
  imports: [
    CommonModule,
    CityRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule
  ],
  entryComponents: [CityAddEditComponent]
})
export class CityModule { }
