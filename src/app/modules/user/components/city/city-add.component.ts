import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, prepareRequiredHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';
import { City } from '@user/models';

@Component({
  selector: 'city-add-edit',
  templateUrl: './city-add.component.html',
})

export class CityAddEditComponent implements OnInit {
  @Input() id: string;
  cityAddEditForm: FormGroup;
  applicationResponse: IApplicationResponse;
  city = new City();
  errorMessage: string;
  isADependentDropdownSelectionChanged: boolean;
  provinceList = [];
  countryList = [];
  provinceid: number;
  btnName: string;
  regionid: number;
  regionList = [];
  public countryId: string;
  isFormSubmitted = false;
  loggedUser: UserLogin;
  public alphaNumericWithSpacePattern = { '0': { pattern: new RegExp('^[A-Za-z0-9 ]*$') } };

  constructor(@Inject(MAT_DIALOG_DATA) public data: City,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.data.cityId) {
      this.btnName = 'Update';
    }
    else {
      this.btnName = 'Create';
    }
    this.cityAddEditForm = this.formBuilder.group({
      cityName: [''],
      countryId: '',
      provinceId: '',
      createdDate: '',
      cityId: ''
    });
    this.bindingCountryDropdown();
    if (this.data.cityId != null) {
      this.countryList = [0];
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.CITIES, this.data.cityId).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.cityAddEditForm.patchValue(response.resources);
          this.cityAddEditForm.updateValueAndValidity();
          this.applicationResponse = response,
            this.city = this.applicationResponse.resources;
          this.countryId = this.city.countryId;
          this.bindingProvincesDropdown(this.city.countryId);
        }
        this.rxjsService.setGlobalLoaderProperty(false);

      });
    }
  }

  private bindingCountryDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_COUNTRIES).subscribe(response => {
      this.applicationResponse = response;
      if (response.isSuccess && response.statusCode == 200) {
        this.countryList = this.applicationResponse.resources;
      }
      if (this.btnName == "Create") {
        this.SetCountryDefaultValue();
        this.bindingProvincesDropdown(this.countryId);
      }
      this.rxjsService.setGlobalLoaderProperty(false);


    });
  }

  private bindingProvincesDropdown(cntryId) {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_PROVINCES, undefined, false, prepareRequiredHttpParams({ countryId: cntryId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.provinceList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);

    });
  }

  SetCountryDefaultValue() {
    this.countryId = this.countryList.find(x => x.displayName == 'South Africa').id;
    this.cityAddEditForm.get('countryId').patchValue(this.countryId);
  }

  public loadProvincesbyCountries(CountryId) {
    this.cityAddEditForm.controls['provinceId'].setValue('');
    if (CountryId != "") {
      this.provinceList = [0];
      let stringToSplit = CountryId;
      let x = stringToSplit.split(": ");
      this.bindingProvincesDropdown(x);
    }
    else {
      this.provinceList = [];
    }
  }

  save(): void {
    if (this.cityAddEditForm.invalid) {
      this.cityAddEditForm.markAllAsTouched();
      return;
    }
    this.isFormSubmitted = true;
    const city = { ...this.city, ...this.cityAddEditForm.value }
    let API;
    if (city.cityId) {
      city.modifiedUserId = this.loggedUser.userId;
      API = this.crudService.update(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.CITIES,city);
    }
    else {
      city.createdUserId = this.loggedUser.userId;
      API = this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.CITIES,city);
    }
    API.subscribe(response => {
      if(response.isSuccess && response.statusCode ==200){
        this.dialog.closeAll();
      }
    })
  }


  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
