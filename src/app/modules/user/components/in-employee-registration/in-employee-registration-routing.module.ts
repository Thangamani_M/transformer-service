import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InEmployeeRegistrationListComponent } from './in-employee-registration-list/in-employee-registration-list.component';
import { InEmployeeRegistrationUpdateComponent } from './in-employee-registration-update/in-employee-registration-update.component';
import { InEmployeeRegistrationViewComponent } from './in-employee-registration-view/in-employee-registration-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
        { path:'', component: InEmployeeRegistrationListComponent,canActivate:[AuthGuard], data: { title: 'Inventory Staff Registration List' }},
        { path:'view',component: InEmployeeRegistrationViewComponent, canActivate:[AuthGuard],data: {title : 'Inventory Staff Registration View'}},
        { path:'add-edit', component: InEmployeeRegistrationUpdateComponent,canActivate:[AuthGuard], data: {title: 'Inventory Staff Registration Update'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class InEmployeeRegistrationRoutingModule { }
