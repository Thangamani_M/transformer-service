import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';


@Component({
    selector: 'app-in-employee-registration-update',
    templateUrl: './in-employee-registration-update.component.html',
    styleUrls: ['./in-employee-registration-update.component.scss']
})

export class InEmployeeRegistrationUpdateComponent {

    employeeId: any;
    employeeDetails: any;
    employeeRegistrationAddEditForm: FormGroup;
    werehouseDropDown: any = [];
    alterwarehouseList: any = [];
    selectedIndex: any = 0;
    selectedOptions: any = [];
    userData: UserLogin;
    maxDate: Date;
    minDate = new Date('1990')
    todayDate=0;
    constructor(
        private activatedRoute: ActivatedRoute, private router: Router,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private rxjsService: RxjsService, private crudService: CrudService,
        private store: Store<AppState>, private snackbarService: SnackbarService,
        private datePipe: DatePipe, private changeDetectorRef: ChangeDetectorRef,) {
        this.employeeId = this.activatedRoute.snapshot.queryParams.id;
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
    }

    ngOnInit() {
        this.createEmployeeeRegistrationForm();
        this.onLoadValue();
    }

    ngAfterViewChecked() {
        this.changeDetectorRef.detectChanges();
    }

    onLoadValue() {
        let api = [this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE_BRANCH, prepareRequiredHttpParams({ userId: this.employeeId})),];
        if (this.employeeId) {
            api.push(this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INVENTORY_STAFF_REGISTRATION, this.employeeId, false, null));
        }
        forkJoin(api).subscribe((response: IApplicationResponse[]) => {
            response?.forEach((res: IApplicationResponse, ix: number) => {
                if (res.resources && res.statusCode === 200) {
                    switch(ix) {
                        case 0:
                            this.werehouseDropDown = res.resources;
                            break;
                        case 1:
                          if(res?.resources?.terminationDate){
                            res.resources.terminationDate = new Date(res.resources?.terminationDate)
                          }
                            this.employeeDetails = res.resources;
                            this.employeeRegistrationAddEditForm.patchValue(res.resources, {emitEvent: false});
                            if (res.resources?.defaultWarehouseId) {
                                this.onDefaultWarehouseChange(res.resources?.defaultWarehouseId);
                            }
                            break;
                    }
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            })
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    createEmployeeeRegistrationForm() {
        this.employeeRegistrationAddEditForm = this.formBuilder.group({
            defaultWarehouseId: ['', Validators.required],
            AlternativeWarehouseIds: [''],
            terminationDate: [''],
        });
        this.onValueChanges();
    }

    onValueChanges() {
        this.employeeRegistrationAddEditForm.get('defaultWarehouseId').valueChanges.subscribe(requestWarehousedata => {
            this.onDefaultWarehouseChange(requestWarehousedata, false);
        });
    }

    // To bind the values in the alternative warehose dropdown except the selected default warehouse
    onDefaultWarehouseChange(requestWarehousedata, flag=true) {
        if(requestWarehousedata) {
            this.alterwarehouseList = [];
            this.selectedOptions = [];
            let respObj = this.werehouseDropDown.filter(issueWarehousedata => requestWarehousedata != issueWarehousedata['id']);
            for (let i = 0; i < respObj.length; i++) {
                let temp = {};
                temp['display'] = respObj[i].displayName;
                temp['value'] = respObj[i].id;
                this.alterwarehouseList.push(temp);
            }
            if (this.alterwarehouseList.length) {
                if (this.employeeId && flag) {
                    //this.employeeRegistrationAddEditForm.get('defaultWarehouseId').setValue()
                   this.selectedOptions = this.employeeDetails['alternativeWarehouseIds'].toLowerCase().split(',');
                }else {
                    this.selectedOptions = []
                }
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        }
    }

    onSubmit() {
        this.rxjsService.setFormChangeDetectionProperty(true);
        if (this.employeeRegistrationAddEditForm.invalid) {
            this.employeeRegistrationAddEditForm.get('AlternativeWarehouseIds').markAllAsTouched();
            return
        }

        let data = this.employeeRegistrationAddEditForm.get('AlternativeWarehouseIds').value;
        let dateValue = this.datePipe.transform(this.employeeRegistrationAddEditForm.get('terminationDate').value, 'yyyy-MM-dd');
        const formData = {
            EmployeeId: this.employeeId, CreatedUserId: this.userData.userId,
            defaultWarehouseId: this.employeeRegistrationAddEditForm.get('defaultWarehouseId').value,
            terminationDate: dateValue,
            AlternativeWarehouseIds: data.toString(),
        }

        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let crudService: Observable<IApplicationResponse> =
            this.crudService.update(ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.INVENTORY_STAFF_REGISTRATION, formData)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigateToList();
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    navigateToList() {
        this.router.navigate(["user/employee-registration"], {
            queryParams: { tab: 0 },
            skipLocationChange: true
        });
    }

    navigateToViewPage() {
        this.router.navigate(["user/employee-registration/view"], {
            queryParams: { id: this.employeeId },
            skipLocationChange: true
        });
    }
}

