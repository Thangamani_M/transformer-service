import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { IT_MANAGEMENT_COMPONENT } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-in-employee-registration-view',
  templateUrl: './in-employee-registration-view.component.html',
})
export class InEmployeeRegistrationViewComponent {

  employeeId: any;
  employeeDetails: any;
  selectedIndex: any = 0;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }
  constructor(
    private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.employeeId = this.activatedRoute.snapshot.queryParams.id;
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][IT_MANAGEMENT_COMPONENT.INVENTORY_STAFF_REGISTRATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    if (this.employeeId) {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INVENTORY_STAFF_REGISTRATION, this.employeeId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.employeeDetails = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  navigateToList() {
    this.router.navigate(["user/employee-registration"], {
      queryParams: { tab: 0 },
      skipLocationChange: true
    });
  }

  navigateToEditPage() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    this.router.navigate(["user/employee-registration/add-edit"], {
      queryParams: { id: this.employeeId },
      skipLocationChange: true
    });
  }
}
