import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxPrintModule } from 'ngx-print';
import { InEmployeeRegistrationListComponent } from './in-employee-registration-list/in-employee-registration-list.component';
import { InEmployeeRegistrationRoutingModule } from './in-employee-registration-routing.module';
import { InEmployeeRegistrationUpdateComponent } from './in-employee-registration-update/in-employee-registration-update.component';
import { InEmployeeRegistrationViewComponent } from './in-employee-registration-view/in-employee-registration-view.component';


@NgModule({
    declarations: [
        InEmployeeRegistrationListComponent,
        InEmployeeRegistrationViewComponent,
        InEmployeeRegistrationUpdateComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule, FormsModule,
        NgxPrintModule,
        InEmployeeRegistrationRoutingModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ]
})

export class InEmployeeRegistrationModule { }