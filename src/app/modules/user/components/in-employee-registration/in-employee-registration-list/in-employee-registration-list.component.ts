import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig,
  formConfigs, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { InventoryEmployeeFilterModel } from '@modules/inventory/models/stock-order-query.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { IT_MANAGEMENT_COMPONENT } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-in-employee-registration-list',
  templateUrl: './in-employee-registration-list.component.html',
})

export class InEmployeeRegistrationListComponent extends PrimeNgTableVariablesModel implements OnInit {

  observableResponse: any;
  primengTableConfigProperties: any;
  columnFilterForm: FormGroup;
  searchForm: FormGroup;
  searchColumns: any;
  row: any = {};
  first: any = 0;
  reset: boolean;
  filterData: any;
  showFilterForm = false;
  employeeNumberList = [];
  inventoryStaffFilterForm: FormGroup;
  applicationResponse: IApplicationResponse;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  formConfigs = formConfigs;
  errorMessage: string;
  rolesList: any = [];

  constructor(private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private commonService: CrudService, private tableFilterFormService: TableFilterFormService,
    private activatedRoute: ActivatedRoute,
    private router: Router, private _fb: FormBuilder,
    private rxjsService: RxjsService,
    private store: Store<AppState>) {

    super()
    this.primengTableConfigProperties = {
      tableCaption: "Inventory Staff Registration",
      breadCrumbItems: [{ displayName: 'IT Management', relativeRouterUrl: '' }, { displayName: 'Payment Configuration' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Inventory Staff Registration',
            dataKey: 'employeeId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableAction: true,
            enableStatusActiveAction: false,
            shouldShowFilterActionBtn: true,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableExportBtn: true,
            columns: [{ field: 'userId', header: 'User ID', width: '200px' }, { field: 'employeeNo', header: 'Employee / Vendor Number', width: '220px' },
            { field: 'userName', header: 'Name', width: '200px' }, { field: 'role', header: 'Role', width: '200px' },
            { field: 'branch', header: 'Branch', width: '200px' }, { field: 'emailAddress', header: 'Email Address', width: '200px' },
            { field: 'contactNumber', header: 'Contact Number', width: '200px' }, { field: 'createdDate', header: 'Employee Creation Date', isDate: true, width: '200px' },
            { field: 'status', header: 'Status', width: '200px' }],
            rowExpantable: false,
            rowExpantableIndex: 0,
            shouldShowDeleteActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            shouldShowAddActionBtn: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.INVENTORY_STAFF_REGISTRATION,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          },
        ]
      }
    }

    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.createInventoryStaffFilterForm();
    this.bindingrolesDropdown();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.INVENTORY_STAFF_REGISTRATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let inventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    inventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      inventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.selectedRows = []
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let pageIndex = this.row && this.row["pageIndex"] ? this.row["pageIndex"] : 0;
        let pageSize = this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
        this.exportList(pageIndex, pageSize);
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any, otherParams?: object) {
    let queryParams;
    queryParams = this.generateQueryParams(otherParams, pageIndex, pageSize);
    let exportapi = InventoryModuleApiSuffixModels.INVENTORY_STAFF_REGISTRATION_EXPORT
    let label = 'Inventory Staff Registration';
    this.commonService.downloadFile(ModulesBasedApiSuffix.INVENTORY,
      exportapi, null, null, queryParams).subscribe((response: any) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response) {
          this.saveAsExcelFile(response, label);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }
  generateQueryParams(otherParams, pageIndex, pageSize) {

    let queryParamsString;
    queryParamsString = new HttpParams({ fromObject: otherParams })
      .set('pageIndex', pageIndex)
      .set('pageSize', pageSize);

    queryParamsString.toString();
    return '?' + queryParamsString;
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onTabChange(event) {
    this.row = {};
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.dataList = [];
    this.totalRecords = null;
    this.filterData = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/user/employee-registration'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(["user/employee-registration/view"], {
          queryParams: {
            id: editableObject['employeeId']
          },
          skipLocationChange: true
        });
        break;
    }
  }

  createInventoryStaffFilterForm(requisitionListModel?: InventoryEmployeeFilterModel) {
    let purchaseListFilterModel = new InventoryEmployeeFilterModel(requisitionListModel);
    this.inventoryStaffFilterForm = this.formBuilder.group({});
    Object.keys(purchaseListFilterModel).forEach((key) => {
      if (typeof purchaseListFilterModel[key] === 'string') {
        this.inventoryStaffFilterForm.addControl(key, new FormControl(purchaseListFilterModel[key]));
      }
    });
  }

  inputChangeEmployeeFilter() {

    this.inventoryStaffFilterForm.get('employeeNo').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        if (searchText != '') {
          return this.commonService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.SEARCH_EMPLOYEE, null, true,
            prepareGetRequestHttpParams(null, null, { 'SearchText': searchText })
          )
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.employeeNumberList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

  }

  bindingrolesDropdown() {
    this.commonService
      .get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.UX_INVENTORY_ROLES
      )
      .subscribe({
        next: (response) => {
          this.applicationResponse = response;
          this.rolesList = this.applicationResponse.resources;
        },
        error: (err) => (this.errorMessage = err),
      });
  }

  submitFilter() {

    this.filterData = Object.assign({},
      this.filterData,
      this.inventoryStaffFilterForm.get('employeeNo').value == '' || this.inventoryStaffFilterForm.get('employeeNo').value == null ? null :
        { employeeNo: this.inventoryStaffFilterForm.get('employeeNo').value },
      this.inventoryStaffFilterForm.get('email').value == '' || this.inventoryStaffFilterForm.get('email').value == null ? null :
        { emailAddress: this.inventoryStaffFilterForm.get('email').value },
      this.inventoryStaffFilterForm.get('contactNumber').value == '' || this.inventoryStaffFilterForm.get('contactNumber').value == null ? null :
        { contactNumber: this.inventoryStaffFilterForm.get('contactNumber').value },
      this.inventoryStaffFilterForm.get('userName').value == '' || this.inventoryStaffFilterForm.get('userName').value == null ? null :
        { userName: this.inventoryStaffFilterForm.get('userName').value },
      this.inventoryStaffFilterForm.get('roleId').value == '' || this.inventoryStaffFilterForm.get('roleId').value == null ? null :
        { RoleId: this.inventoryStaffFilterForm.get('roleId').value },
    );
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.observableResponse = this.onCRUDRequested('get', this.row);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.inventoryStaffFilterForm.reset();
    this.filterData = null;
    this.reset = true;
    this.row = { pageIndex: 0, pageSize: this.row["pageSize"] };
    this.showFilterForm = !this.showFilterForm;
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
