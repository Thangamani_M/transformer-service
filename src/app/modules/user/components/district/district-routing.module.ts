import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DistrictListComponent } from './add-edit-list/district-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: DistrictListComponent, canActivate: [AuthGuard], data: { title: 'District' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})

export class DistrictRoutingModule { }
