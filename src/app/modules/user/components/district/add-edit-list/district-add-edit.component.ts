import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { DistrictModel, loggedInUserData, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {  CrudService, CustomDirectiveConfig, formConfigs, getPDropdownData, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-district-add-edit',
  templateUrl: './district-add-edit.component.html',
})

export class DistrictAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  isAStringOnlyNoSpace = new CustomDirectiveConfig({ isAStringOnlyNoSpace: true });
  districtAddEditForm: FormGroup;
  alphaConfig = new CustomDirectiveConfig({ isAStringWithHyphenDash: true });


  loggedUser: UserLogin;
  regionList: any;
  divisionDropDown: any = []
  districtModel: DistrictModel;

  constructor(public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.districtModel  = this.config.data
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createDivisionForm();
    this.getRegions();
    this.onFormControlChanges();
    this.getDetailsById();
  }


  getDetailsById() {
    let _ids = []
    for (const iterator of this.districtModel?.divisionId.split(',')) {
      _ids.push(iterator.toLowerCase())
    }
    this.districtModel.divisionId = _ids;


    this.districtAddEditForm.patchValue(this.districtModel);

  }

  onFormControlChanges(): void {
    this.districtAddEditForm.get('regionId').valueChanges.subscribe((regionId: string) => {
      if (!regionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { regionId: regionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.divisionDropDown = getPDropdownData(response.resources);
          }
          if (this.districtModel.districtId && (this.districtModel.regionId == this.districtAddEditForm.get("regionId").value )) {
            this.districtAddEditForm.get('divisionId').setValue(this.districtModel.divisionId)
          }else{
            this.districtAddEditForm.get('divisionId').setValue([])
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
  }

  regionChange() {
    this.districtAddEditForm.controls.divisionId.setValue([]);
  }

  getRegions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null,
        { CountryId: formConfigs.countryId })).subscribe((response) => {

          if (response.statusCode == 200) {
            this.regionList = response.resources;
          }
          let found = this.regionList.filter(e => e.id == this.districtModel.regionId)
          if (found.length == 0) {
            this.districtAddEditForm.controls.regionId.setValue('');
          }
        });
  }

  getDivisionDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_DIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionDropDown = getPDropdownData(response.resources);
        }
      });
  }


  createDivisionForm(): void {
    let districtModel = new DistrictModel();
    this.districtAddEditForm = this.formBuilder.group({});
    Object.keys(districtModel).forEach((key) => {
      this.districtAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(districtModel[key]));
    });
    this.districtAddEditForm = setRequiredValidator(this.districtAddEditForm, ["regionId", "divisionId", "districtName"]);
  }

  onSubmit(): void {
    if (this.districtAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.districtAddEditForm.value.districtId ? this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.DIVISION_DISTRICT, this.districtAddEditForm.value) :
      this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.DIVISION_DISTRICT, this.districtAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {

      if (response.isSuccess && response.statusCode === 200) {
        // this.dialog.closeAll();
        this.ref.close(false)
        this.districtAddEditForm.reset();
      }
    })
  }

  dialogClose(): void {
    this.ref.close(true);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
