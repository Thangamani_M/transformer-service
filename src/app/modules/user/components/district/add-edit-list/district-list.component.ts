import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DistrictAddEditComponent, IT_MANAGEMENT_COMPONENT, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, SnackbarService } from '@app/shared';
import { CrudService, RxjsService, } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { DistrictModel } from '@modules/user/models';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-district-list',
  templateUrl: './district-list.component.html',
})
export class DistrictListComponent extends PrimeNgTableVariablesModel implements OnInit {
  observableResponse;
  primengTableConfigProperties: any = {
    tableCaption: "District",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'IT Management ', relativeRouterUrl: '' }, { displayName: 'District List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'District',
          dataKey: 'districtId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: true,
          enableMultiDeleteActionBtn: true,
          enableStatusActiveAction: true,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: true,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'districtName', header: 'District Name' },
          { field: 'divisionName', header: 'Division Name' },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: UserModuleApiSuffixModels.DISTRICT,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT
        }]
    }
  }

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private snackbarService: SnackbarService,
    private dialog: MatDialog) {
    super()
  }

  ngOnInit(): void {
    this.getDistricts();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.DISTRICTS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getDistricts(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.DIVISION_DISTRICT,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.observableResponse = response.resources;
          this.dataList = this.observableResponse
          this.totalRecords = response.totalCount;
          this.loading = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;

      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getDistricts(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getDistricts();
            }
          });
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  openAddEditPage(type: CrudType | string, districtModel?: DistrictModel | any): void {
  //   let data = new DistrictModel(districtModel);
  //   const dialogReff = this.dialog.open(DistrictAddEditComponent, { width: '450px', data, disableClose: false });
  //   dialogReff.afterClosed().subscribe(result => {
  //     if (!result)
  //       this.observableResponse = this.getDistricts();
  //   });
  // }
  let data = new DistrictModel(districtModel);
  const ref = this.dialogService.open(DistrictAddEditComponent, {
    showHeader: false,
    baseZIndex: 1000,
    width: '450px',
    data: data,
  });
  ref.onClose.subscribe((resp) => {
    if (!resp) {
      this.getDistricts();
    }
  })
}
}
