import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DistrictAddEditComponent } from './add-edit-list/district-add-edit.component';
import { DistrictListComponent } from './add-edit-list/district-list.component';
import { DistrictRoutingModule } from './district-routing.module';

@NgModule({
  declarations: [DistrictListComponent, DistrictAddEditComponent],
  imports: [
    CommonModule,
    DistrictRoutingModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
  ],
  entryComponents: [DistrictAddEditComponent]
})
export class DistrictModule { }