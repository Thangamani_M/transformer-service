import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { TechnicalStaffRegistrationFilterModel } from '@modules/user/models/technician-user-type-registration.model';
import { IT_MANAGEMENT_COMPONENT } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-technician-user-type-registration-list',
  templateUrl: './technician-user-type-registration-list.component.html',
})
export class TechnicianUserTypeRegistrationListComponent implements OnInit {

  userData: UserLogin;
  dataList: any;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  searchColumns: any
  row: any = {}
  today: any = new Date();
  loading: boolean;
  columnFilterForm: FormGroup;
  technicalStaffRegFilterForm: FormGroup;
  searchForm: FormGroup;
  showFilterForm = false;
  status: any = [];
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  selectedRows: any
  roleList = [];
  branchList = [];
  techAreaList = [];
  first: any = 0;
  reset: boolean;
  filterData: any;

  constructor(private crudService: CrudService, private tableFilterFormService: TableFilterFormService, private formBuilder: FormBuilder,
    private router: Router,
    private store: Store<AppState>,
    private rxjsService: RxjsService) {
    this.status = [
      { label: 'Active', value: true },
      { label: 'InActive', value: false },
    ];
    this.primengTableConfigProperties = {
      tableCaption: "Technical Staff Registration List",
      breadCrumbItems: [{ displayName: 'IT Management', relativeRouterUrl: '' }, { displayName: 'Technical Staff Registration List' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Technical Staff Registration List',
            dataKey: 'employeeId',
            enableBreadCrumb: true,
            enableReset: false,
            enableAction: true,
            enableStatusActiveAction: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            shouldShowFilterActionBtn: true,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'userId', header: 'User ID', width: '120px' },
              { field: 'employeeNumber', header: 'Employee/ Vendor Number', width: '220px' },
              { field: 'name', header: 'Name', width: '200px' },
              { field: 'roleName', header: 'Role', width: '200px' },
              { field: 'branch', header: 'Branch', width: '250px' },
              { field: 'email', header: 'Email', width: '200px' },
              { field: 'contactNumber', header: 'Contact Number', width: '160px' },
              { field: 'techArea', header: 'Tech Area', width: '200px' },
              { field: 'techStockLocation', header: 'Tech Stock Location', width: '200px' },
              { field: 'stockHoldingWarehouse', header: 'Stock Holding Warehouse', width: '200px' },
              { field: 'stockHoldingLocation', header: 'Stock Holding Location', width: '200px' },
              { field: 'stockCollectionWarehouse', header: 'Stock Collection Warehouse', width: '210px' },
              { field: 'stockCollectionLocation', header: 'Stock Collection Location', width: '200px' },
              { field: 'isActive', header: 'Status', width: '130px' },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          },
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.getTechnicianUserRegistrationListData();
    this.TechnicalStaffRegListFilterCreationForm();
    this.getRoleList();
    this.getBranchList();
    this.getTechAreaList([]);
    this.technicalStaffRegFilterForm.controls['BranchIds'].valueChanges.subscribe((value) => {
      if (value?.length != 0) {
        this.getTechAreaList(value);
      }
      else if (value?.length == 0) {
        this.getTechAreaList([]);
      }
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.TECH_STAFF_REGISTRATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRoleList() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_USER_TYPE_CONFIG_SEARCH, null, true)
      .subscribe(data => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          for (var i = 0; i < data.resources.length; i++) {
            let tmp = {};
            tmp['value'] = data.resources[i].roleId;
            tmp['display'] = data.resources[i].displayName;
            this.roleList.push(tmp)
          }
        } else {
          this.roleList = []
        }
      });
  }

  getTechAreaList(branchItems) {
    this.techAreaList = [];
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_AREAS, null, true,
      prepareRequiredHttpParams({ BranchIds: branchItems.join(',') }))
      .subscribe(data => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.techAreaList = data.resources;
          // for (var i = 0; i < data.resources.length; i++) {
          //   let tmp = {};
          //   tmp['value'] = data.resources[i].id;
          //   tmp['display'] = data.resources[i].displayName;
          //   this.techAreaList.push(tmp)
          // }
        } else {
          this.techAreaList = []
        }
      });
  }

  getBranchList() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_BRANCHES, null, true)
      .subscribe(data => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          for (var i = 0; i < data.resources.length; i++) {
            let tmp = {};
            tmp['value'] = data.resources[i].branchId;
            tmp['display'] = data.resources[i].branchName;
            this.branchList.push(tmp)
          }
        } else {
          this.branchList = []
        }
      });
  }

  TechnicalStaffRegListFilterCreationForm(): void {
    let technicalStaffRegModel = new TechnicalStaffRegistrationFilterModel();
    // create form controls dynamically from model class
    this.technicalStaffRegFilterForm = this.formBuilder.group({
      // technicianStockOrderItems: this.formBuilder.array([])
    });
    Object.keys(technicalStaffRegModel).forEach((key) => {
      this.technicalStaffRegFilterForm.addControl(key, new FormControl(technicalStaffRegModel[key]));
    });

    // this.technicalStaffRegistrationCreationForm = setRequiredValidator(this.technicalStaffRegistrationCreationForm, ['technicianUserTypeConfigId','boundaryId']);

  }

  getTechnicianUserRegistrationListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = []
        this.totalRecords = 0;
      }
      this.reset = false;
    });

  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: any, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        searchObj = { ...this.filterData, ...searchObj };
        this.getTechnicianUserRegistrationListData(this.row["pageIndex"], this.row["pageSize"], searchObj)
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onTabChange(event) {
    this.row = {};
    this.columnFilterForm = this.formBuilder.group({});
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    this.getTechnicianUserRegistrationListData();
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['/user/technicial-staff-registration/add-edit']);
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['/user/technicial-staff-registration/view'], {
              queryParams: {
                id: editableObject['employeeId']
              }, skipLocationChange: true
            });
            break;
        }
        break;
    }
  }

  submitFilter() {

    let ids = this.technicalStaffRegFilterForm.controls['TechAreaIds'].value;
    let techAreaIds = [];
    ids.forEach(element => {
      techAreaIds.push(element.id);
    });
    this.filterData = Object.assign({},
      this.technicalStaffRegFilterForm.controls['FirstName'].value == null ? null : this.technicalStaffRegFilterForm.controls['FirstName'].value.trim() == '' ? null : { FirstName: this.technicalStaffRegFilterForm.controls['FirstName'].value },
      this.technicalStaffRegFilterForm.controls['LastName'].value == null ? null : this.technicalStaffRegFilterForm.controls['LastName'].value.trim() == '' ? null : { LastName: this.technicalStaffRegFilterForm.controls['LastName'].value },
      this.technicalStaffRegFilterForm.controls['Email'].value == null ? null : this.technicalStaffRegFilterForm.controls['Email'].value.trim() == '' ? null : { Email: this.technicalStaffRegFilterForm.controls['Email'].value },
      this.technicalStaffRegFilterForm.controls['UserId'].value == null ? null : this.technicalStaffRegFilterForm.controls['UserId'].value.trim() == '' ? null : { UserId: this.technicalStaffRegFilterForm.controls['UserId'].value },
      this.technicalStaffRegFilterForm.controls['ContactNumber'].value == null ? null : this.technicalStaffRegFilterForm.controls['ContactNumber'].value.trim() == '' ? null : { ContactNumber: this.technicalStaffRegFilterForm.controls['ContactNumber'].value },
      this.technicalStaffRegFilterForm.controls['EmployeeNumber'].value == null ? null : this.technicalStaffRegFilterForm.controls['EmployeeNumber'].value.trim() == '' ? null : { EmployeeNumber: this.technicalStaffRegFilterForm.controls['EmployeeNumber'].value },
      this.technicalStaffRegFilterForm.controls['RoleName'].value == null ? null : this.technicalStaffRegFilterForm.controls['RoleName'].value?.length == 0 ? null : { RoleId: this.technicalStaffRegFilterForm.controls['RoleName'].value.join(',') },
      this.technicalStaffRegFilterForm.controls['BranchIds'].value == null ? null : this.technicalStaffRegFilterForm.controls['BranchIds'].value?.length == 0 ? null : { BranchIds: this.technicalStaffRegFilterForm.controls['BranchIds'].value.join(',') },
      this.technicalStaffRegFilterForm.controls['TechAreaIds'].value == null ? null : this.technicalStaffRegFilterForm.controls['TechAreaIds'].value?.length == 0 ? null : { TechAreaIds: techAreaIds.toString() },
    )
    this.onCRUDRequested('get');
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.technicalStaffRegFilterForm.reset();
    this.filterData = null;
    this.reset = true;
    // this.getTechnicianUserRegistrationListData();
    this.showFilterForm = !this.showFilterForm;
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

}
