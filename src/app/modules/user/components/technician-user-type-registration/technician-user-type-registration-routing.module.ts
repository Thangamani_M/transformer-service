import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TechnicianUserTypeRegistrationAddEditComponent } from './technician-user-type-registration-add-edit/technician-user-type-registration-add-edit.component';
import { TechnicianUserTypeRegistrationListComponent } from './technician-user-type-registration-list/technician-user-type-registration-list.component';
import { TechnicianUserTypeRegistrationViewComponent } from './technician-user-type-registration-view/technician-user-type-registration-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: '', component: TechnicianUserTypeRegistrationListComponent, canActivate: [AuthGuard], data: { title: 'Technical Staff Registration List' } },
  { path: 'add-edit', component: TechnicianUserTypeRegistrationAddEditComponent, canActivate: [AuthGuard], data: { title: 'Technician Staff Registration Add Edit' } },
  { path: 'view', component: TechnicianUserTypeRegistrationViewComponent, canActivate: [AuthGuard], data: { title: 'Technician Staff Registration View' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class TechnicianUserTypeRegistrationRoutingModule { }
