import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatTabGroup } from '@angular/material';
import { DateAdapter } from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { TechnicalStaffRegistrationModel } from '@modules/user/models/technician-user-type-registration.model';
import { IT_MANAGEMENT_COMPONENT } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
@Component({
  selector: 'app-technician-user-type-registration-add-edit',
  templateUrl: './technician-user-type-registration-add-edit.component.html',
  styleUrls: ['./technician-user-type-registration-add-edit.component.scss']
})

export class TechnicianUserTypeRegistrationAddEditComponent implements OnInit {
  @ViewChild('input', { static: false }) row;
  technicalStaffRegistrationCreationForm: FormGroup;
  userTypeForm: FormGroup;
  TechnicianUserTypeId;
  action;
  formConfigs = formConfigs;
  TechnicianUserTypeDetails;
  filterTechAreaList: any = [];
  techAreaDetail;
  signalManagementCoordinatorList = [];
  selectedIndex: any = 0;
  isWarningNotificationDialog: boolean = false;
  technicianList = [];
  technicianUserTypeList = [];
  countryCodes = [{ displayName: '+27' }];
  userData: UserLogin;
  daysSelected = false;
  weekSelected = false;
  mobileLength = '9';
  alertLocationClear: boolean = false;
  todayDate = new Date();
  alphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  StockDescErrorMessage: any
  isLoading: boolean
  showStockDescError: boolean;
  isTechTestingPasswordShow: boolean;
  isPanicDuressPasswordShow: boolean;
  technicianReplaced;
  formData = new FormData();
  isUpdateJobTypeNew: boolean = false;
  newFilterTechAreaList = [];
  newtechAreaStockLocation;
  isFirstCallUpdate: boolean = false;
  technicianUserTypeConfig;
  isFristTimeUpdate: boolean = false;
  isFirstCallTechStockLoc: boolean = false;
  isValidTechTestMobileNo = true;
  isValidTechTestMobileNoValidMessage = '';
  isValidPsiraRegistrationNumber = true;
  isValidPsiraRegistrationNumberValidMessage = '';
  isPasswordDuplicate = false
  isTechAreaManger: boolean = false;
  alertTechnicalStaffRegValidation: boolean = false;
  alertMessage = '';
  isSaveAlertPopup = false;
  isFirstTechReplaceId = false;
  // Incentive tab Variable
  incetiveForm: FormGroup;
  incentiveSchemeTypeList = [];
  debtorCodeList = [];
  debtorName = '';
  employeeId;
  branchId;
  pisraCertificatePath;
  profileImagePath;
  aodDebtorId;
  techStockLocationFlag: boolean = false;
  @ViewChild('tabGroup', { static: false }) tabGroup: MatTabGroup;
  technicianUserTypeConfigId;
  isTechAreaChangeRequest: boolean;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private dateAdapter: DateAdapter<Date>,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private datePipe: DatePipe,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private snackbarService : SnackbarService) {
    this.dateAdapter.setLocale('en-GB'); //dd/MM/yyyy
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.TechnicianUserTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.employeeId = this.activatedRoute.snapshot.queryParams?.employeeId;
    this.action = this.activatedRoute.snapshot.queryParams.action;
    this.selectedIndex = this.activatedRoute.snapshot.queryParams?.tab ?
      this.activatedRoute.snapshot.queryParams?.tab : 0;
  }

  onTabClicked(e) {
    this.selectedIndex = e.index;

  }

  techTypeChange() {
    if (this.technicalStaffRegistrationCreationForm.get('isJobTypeNew').value == false) {
      // this.getTechnicianName();
      if (this.technicalStaffRegistrationCreationForm.get('techStockLocationId').value != '') {
        this.crudService.get(
          ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.TECH_STOCK_LOCATION_TECH_STOCK_LOCATION_VALIDATE, null, null, prepareGetRequestHttpParams(null, null, { TechstockLocationId: this.technicalStaffRegistrationCreationForm.get('techStockLocationId').value })).subscribe((response: IApplicationResponse) => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if (response.resources.stockLocationCount > 1)
              this.alertLocationClear = true;
          })
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.TechnicalStaffRegistrationCreationForm();
    this.createUserTypeForm();
    this.getSignalManagementCoordinator();
    // this.getTechnicianUserTypeData();
    this.formControlChange();
    this.loadTechnicianStaffAllDetail().subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.technicianUserTypeList = resp.resources;
              // this.technicalStaffRegistrationCreationForm.get('technicianUserTypeConfigId').value != '' ? this.technicianUserTypeList.find(x => x.id == this.technicalStaffRegistrationCreationForm.get('technicianUserTypeConfigId').value).isMultiTechArea == false ? this.isTechAreaManger = true : this.isTechAreaManger = false : null;
              // this.isTechAreaManger == true ? this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue([]) : this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue([]);
              break;
            case 1:
              this.onAfterDetailAPI(resp);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })

    // code for INCENTIVE tab
    this.createIncentiveForm();
    this.getIncentiveSchemeType();
    this.technicalStaffRegistrationCreationForm.get('aodDebtorId').valueChanges.subscribe((Val) => {
      if (Val) {
        if (this.debtorCodeList.length > 0) {
        }
      }

    })
    this.technicalStaffRegistrationCreationForm.get('boundaryId').valueChanges.subscribe((Val) => {

      if (this.technicianUserTypeConfig != undefined && this.technicalStaffRegistrationCreationForm.get('boundaryId').value != '' && this.technicalStaffRegistrationCreationForm.get('boundaryId').value != null) {
        if (this.technicianUserTypeConfig.isStockLocation == true) {
          if (typeof (Val) == 'string') {
            if (this.isFirstCallTechStockLoc == false) {
              this.onTechAreaSelected(Val)
            }
            else {

              this.isFirstCallTechStockLoc = false;
            }
          }
        }
      }
      else {
        if (this.isFirstCallTechStockLoc == true) {
          this.isFirstCallTechStockLoc = false;
        }
      }

    })
    this.technicalStaffRegistrationCreationForm.get('technicianUserTypeConfigId').valueChanges.subscribe((value) => {
      if (this.action == 'Update' && this.employeeId) {
        this.onValidateTechnicalUserType(value);
      } else {
        this.onAfterTechUserTypeValueChange();
      }
    })

    this.technicalStaffRegistrationCreationForm.get('techTestingPassword').valueChanges.subscribe((value) => {
      if (value && this.technicalStaffRegistrationCreationForm.get('panicDuresPassword').value != '') {
        this.technicalStaffRegistrationCreationForm.get('panicDuresPassword').value == value ? this.isPasswordDuplicate = true : this.isPasswordDuplicate = false;

      }
    })
    this.technicalStaffRegistrationCreationForm.get('panicDuresPassword').valueChanges.subscribe((value) => {

      if (value && this.technicalStaffRegistrationCreationForm.get('techTestingPassword').value != '') {
        this.technicalStaffRegistrationCreationForm.get('techTestingPassword').value == value ? this.isPasswordDuplicate = true : this.isPasswordDuplicate = false;


      }
    })
    this.technicalStaffRegistrationCreationForm.get('isJobTypeNew').valueChanges.subscribe((value) => {

      let boundaryId = this.technicalStaffRegistrationCreationForm.controls['boundaryId'].value;

      if (this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails != null) {
        if (this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.isJobTypeNew != value && value == false) {
          this.crudService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_TECHNICIANAREA_CHANGE_VALIDATE, null, true,
            prepareRequiredHttpParams({
              EmployeeId: this.TechnicianUserTypeDetails.employeeId,
            })).subscribe((response: IApplicationResponse) => {
              this.rxjsService.setGlobalLoaderProperty(false);

              if (response.isSuccess) {
                if (!(response.resources.isSuccess)) {
                  this.alertTechnicalStaffRegValidation = true;

                  this.technicalStaffRegistrationCreationForm.get('isJobTypeNew').patchValue(true);
                  this.alertMessage = response.resources.validateMessage;
                }
                else {

                  this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue('');
                  this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].patchValue('');
                  this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].disable();
                  this.technicalStaffRegistrationCreationForm.controls['signalMgtCoordinator'].patchValue('');
                  this.technicalStaffRegistrationCreationForm.controls['currentTechAreaId'].patchValue('');
                  this.technicalStaffRegistrationCreationForm.controls['currentTechStockLocationId'].patchValue('');

                  this.technicalStaffRegistrationCreationForm.controls['currentTechAreaId'].patchValue(null);
                  this.technicalStaffRegistrationCreationForm.controls['currentTechAreaId'].disable();
                  this.technicalStaffRegistrationCreationForm.controls['currentTechStockLocationId'].patchValue(null);
                  this.technicalStaffRegistrationCreationForm.controls['currentTechStockLocationId'].disable();

                }
              }

            })
        }

      }
      else {


        this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue('');
        this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].patchValue('');
        this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].disable();

        this.technicalStaffRegistrationCreationForm.controls['signalMgtCoordinator'].patchValue('');
        this.technicalStaffRegistrationCreationForm.controls['currentTechAreaId'].patchValue('');
        this.technicalStaffRegistrationCreationForm.controls['currentTechStockLocationId'].patchValue('');
        if (this.technicalStaffRegistrationCreationForm.controls['isJobTypeNew'].value == false) {
          this.technicalStaffRegistrationCreationForm.controls['currentTechAreaId'].patchValue(null);
          this.technicalStaffRegistrationCreationForm.controls['currentTechAreaId'].disable();
          this.technicalStaffRegistrationCreationForm.controls['currentTechStockLocationId'].patchValue(null);
          this.technicalStaffRegistrationCreationForm.controls['currentTechStockLocationId'].disable();

          this.technicalStaffRegistrationCreationForm = setRequiredValidator(this.technicalStaffRegistrationCreationForm, ['technicianReplaceId']);
          if (this.technicalStaffRegistrationCreationForm.get('technicianReplaceId').value != null && this.technicalStaffRegistrationCreationForm.get('technicianReplaceId').value != '') {
            this.crudService.get(
              ModulesBasedApiSuffix.TECHNICIAN,
              TechnicalMgntModuleApiSuffixModels.TECH_AREA_BOUNDARY, null, true, prepareGetRequestHttpParams(null, null, { UserId: this.technicalStaffRegistrationCreationForm.get('technicianReplaceId').value })).subscribe((response: IApplicationResponse) => {
                this.rxjsService.setGlobalLoaderProperty(false);
                this.technicianReplaced = response.resources;
                this.technicalStaffRegistrationCreationForm.controls['currentTechAreaId'].patchValue(response.resources.boundaryName);
                this.technicalStaffRegistrationCreationForm.controls['currentTechAreaId'].disable();
                this.technicalStaffRegistrationCreationForm.controls['currentTechStockLocationId'].patchValue(response.resources.techStockLocationName);
                this.technicalStaffRegistrationCreationForm.controls['currentTechStockLocationId'].disable();
              })
          }
        } else {
          this.technicalStaffRegistrationCreationForm.controls['technicianReplaceId'].clearValidators();
          this.technicalStaffRegistrationCreationForm.controls['technicianReplaceId'].updateValueAndValidity();
          if (this.action == 'Update') {
            this.isUpdateJobTypeNew = true;

            if (boundaryId == this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.boundaryName) {

              this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue(this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.boundaryName);

              this.isUpdateJobTypeNew = false;
            }
            else {


              this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue(this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.boundaryName);
            }
          }
        }
        // if (value == false) {
        //   this.getTechnicianName()
        // }
      }

    })
    this.technicalStaffRegistrationCreationForm.get('technicianReplaceId').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        if (searchText != null) {

          if (!searchText) {

            return this.technicianList = [];
          } else {

            return this.crudService.get(
              ModulesBasedApiSuffix.TECHNICIAN,
              TechnicalMgntModuleApiSuffixModels.UX_BRANCH_TECHNICIAN,
              undefined,
              false, prepareRequiredHttpParams(
                {
                  BranchIds: this.TechnicianUserTypeDetails.branchId,
                  SearchText: searchText,
                  employeeId: this.employeeId
                }
              ))
          }
        }
        else {
          return this.technicianList = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.technicianList = response.resources;
          if (this.isFirstTechReplaceId) {
            this.onTechnicianReplacedSelected(this.technicalStaffRegistrationCreationForm.get('technicianReplaceId').value);
            this.isFirstTechReplaceId = false;
          }

        } else {
          this.technicianList = [];

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    this.technicalStaffRegistrationCreationForm.get('currentTechAreaId').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        if ((searchText != null)) {
          if (!searchText) {
            return this.newFilterTechAreaList = [];
          } else {
            let filter = this.technicianList.filter(x => x.displayName == this.technicalStaffRegistrationCreationForm.get('technicianReplaceId').value)
            if (filter && filter.length > 0) {
              return this.crudService.get(
                ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.TECH_AREA_SEARCH, null, true,
                prepareGetRequestHttpParams(null, null, { TechnicalArea: searchText, JobType: 'Replacement', EmployeeId: filter[0].id }))
            }
          }
        } else {
          return this.newFilterTechAreaList = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.newFilterTechAreaList = response.resources;
        } else {
          this.newFilterTechAreaList = [];
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });




  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][IT_MANAGEMENT_COMPONENT.TECH_STAFF_REGISTRATION]
      if (permission) {
        let technical = permission.find(item=> item.menuName == "Technical");
        console.log("technical",technical)
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, technical['subMenu']);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onAfterTechUserTypeValueChange() {

    this.filterTechAreaList = [];
    this.isTechAreaManger == true ? this.technicalStaffRegistrationCreationForm.get('isJobTypeNew').patchValue(true) : this.technicalStaffRegistrationCreationForm.get('isJobTypeNew').patchValue(true);
    this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].reset();
    this.isTechAreaManger == true ? this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue([]) : this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue('')

    let uxTechAreaParams = Object.assign({},
      this.TechnicianUserTypeDetails.employeeId == null ? null : { "EmployeeId": this.TechnicianUserTypeDetails.employeeId },
      this.TechnicianUserTypeDetails.branchId == null ? null : { "BranchId": this.TechnicianUserTypeDetails.branchId },
      this.technicalStaffRegistrationCreationForm.get('technicianUserTypeConfigId').value ? { "TechnicianUserTypeConfigId": this.technicalStaffRegistrationCreationForm.get('technicianUserTypeConfigId').value } : this.technicianUserTypeConfigId ? this.technicianUserTypeConfigId : null

    )

    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN_TECH_AREA, null, false,
      prepareGetRequestHttpParams(null, null,
        uxTechAreaParams
      )
    ).subscribe((response: IApplicationResponse) => {
      // this.filterTechAreaList = response.resources;

      let filterTechAreaList = response.resources;
      for (var i = 0; i < filterTechAreaList.length; i++) {
        let tmp = {};
        tmp['value'] = filterTechAreaList[i].id;
        tmp['display'] = filterTechAreaList[i].displayName;
        tmp['stockCollectionWarehouseId'] = filterTechAreaList[i].stockCollectionWarehouseId;
        tmp['stockCollectionLocationId'] = filterTechAreaList[i].stockCollectionLocationId;
        tmp['stockHoldingWarehouseId'] = filterTechAreaList[i].stockHoldingWarehouseId;
        tmp['stockHoldingLocationId'] = filterTechAreaList[i].stockHoldingLocationId;
        tmp['stockCollectionWarehouse'] = filterTechAreaList[i].stockCollectionWarehouse;
        tmp['stockHoldingWarehouse'] = filterTechAreaList[i].stockHoldingWarehouse;
        tmp['stockCollectionLocation'] = filterTechAreaList[i].stockCollectionLocation;
        tmp['stockHoldingLocation'] = filterTechAreaList[i].stockHoldingLocation;
        this.filterTechAreaList.push(tmp)
      }
      this.rxjsService.setGlobalLoaderProperty(false);

    })
    if (this.technicianUserTypeList.length > 0) {
      this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECHNICIAN_USER_TYPE_CONFIG_DETAILS, null, false,
        prepareGetRequestHttpParams(null, null,
          { RoleId: this.technicianUserTypeList.find(x => x.id == this.technicalStaffRegistrationCreationForm.get('technicianUserTypeConfigId').value).roleId }
        )
      ).subscribe((response: IApplicationResponse) => {

        this.rxjsService.setGlobalLoaderProperty(false);
        this.technicianUserTypeConfig = response.resources[0];
        response.resources.length > 0 ? this.technicianUserTypeConfig.isMultiTechArea == true ? this.isTechAreaManger = true : this.isTechAreaManger = false : null;
        // this.isTechAreaManger == true ? this.technicalStaffRegistrationCreationForm.get('isJobTypeNew').patchValue(true) : this.technicalStaffRegistrationCreationForm.get('isJobTypeNew').patchValue(true);

        // this.isTechAreaManger == true ? this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue([]) : this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue('')


        this.isFristTimeUpdate = false;


        if (this.technicianUserTypeConfig.isPSIRA == false) {

          this.technicalStaffRegistrationCreationForm.get('pSIRAExpiryDate').clearValidators();
          this.technicalStaffRegistrationCreationForm.get('pSIRAExpiryDate').updateValueAndValidity();
          this.technicalStaffRegistrationCreationForm.get('pSIRARegNo').clearValidators();
          this.technicalStaffRegistrationCreationForm.get('pSIRARegNo').updateValueAndValidity();
          this.technicalStaffRegistrationCreationForm.get('pisraCertificateName').clearValidators();
          this.technicalStaffRegistrationCreationForm.get('pisraCertificateName').updateValueAndValidity();

        }
        if (this.technicianUserTypeConfig.isTechTesting == false) {
          this.technicalStaffRegistrationCreationForm.get('techTestingMoblileNumber').clearValidators();
          this.technicalStaffRegistrationCreationForm.get('techTestingMoblileNumber').updateValueAndValidity();
          this.technicalStaffRegistrationCreationForm.get('techTestingPassword').clearValidators();
          this.technicalStaffRegistrationCreationForm.get('techTestingPassword').updateValueAndValidity();
        }
        if (this.technicianUserTypeConfig.isTechTesting == true) {
          this.row?.nativeElement?.focus();
          this.row?.nativeElement?.blur();
        }



      })
    }
  }

  loadTechnicianStaffAllDetail(): Observable<any> {
    let api = [
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_USER_TYPE_CONFIG_SEARCH, null, true),
      this.getDetailAPI(),
    ];
    return forkJoin(api);
  }

  getDetailAPI() {
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_DETAILS,
      undefined, false, prepareRequiredHttpParams({ EmployeeId: this.TechnicianUserTypeId }));
  }

  onLoadDetailAPI() {
    this.getDetailAPI().subscribe((res: IApplicationResponse) => {
        this.onAfterDetailAPI(res);
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onAfterDetailAPI(resp: IApplicationResponse) {
    if (resp.isSuccess && resp?.statusCode == 200) {
      // this.technicalStaffRegistrationCreationForm.controls['terminationDate'].disable()
      this.TechnicianUserTypeDetails = resp.resources;
      this.technicianUserTypeConfigId = resp.resources.technicianStaffRegistrationTechnicianDetails?.technicianUserTypeConfigId;
      // this.getTechnicianUserTypeList();
      if(resp.resources?.technicianStaffRegistrationTechnicianDetails?.psiraExpiryDate){
        resp.resources.technicianStaffRegistrationTechnicianDetails.psiraExpiryDate = new Date(resp.resources.technicianStaffRegistrationTechnicianDetails.psiraExpiryDate)
      }

      if(resp.resources?.technicianStaffRegistrationTechnicianDetails?.terminationDate){
        resp.resources.technicianStaffRegistrationTechnicianDetails.terminationDate = new Date(resp.resources.technicianStaffRegistrationTechnicianDetails.terminationDate)
      }
      if(resp.resources?.technicianStaffRegistrationTechnicianDetails?.startDate){
        resp.resources.technicianStaffRegistrationTechnicianDetails.startDate = new Date(resp.resources.technicianStaffRegistrationTechnicianDetails.startDate)
      }

      this.getDebtorCodeList(resp.resources.branchId);
      this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails != null ? this.debtorName = this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails?.aodDebtorName : this.debtorName = '';
      this.userTypeForm.get('isUserType').setValue(this.TechnicianUserTypeDetails.isInternalUser);
      if (this.action == "Update") {
        if (resp.resources && resp.resources.techAreas && resp.resources.techAreas.length > 0) {
          this.technicalStaffRegistrationCreationForm.controls['newTechStockLocation'].patchValue(resp.resources.techAreas[0].newTechStockLocation);
          this.technicalStaffRegistrationCreationForm.controls['newTechStockLocation'].disable();
        }
        this.branchId = resp.resources.branchId;
        this.pisraCertificatePath = resp.resources.technicianStaffRegistrationTechnicianDetails?.pisraCertificatePath
        this.profileImagePath = resp.resources.technicianStaffRegistrationTechnicianDetails?.profileImagePath;
        if(resp.resources.technicianStaffRegistrationTechnicianDetails) {
          resp.resources.technicianStaffRegistrationTechnicianDetails.lunchBreakFrom = resp.resources.technicianStaffRegistrationTechnicianDetails.lunchBreakFrom == null ? null : this.timeToDate(resp.resources.technicianStaffRegistrationTechnicianDetails.lunchBreakFrom);
          resp.resources.technicianStaffRegistrationTechnicianDetails.lunchBreakTo = resp.resources.technicianStaffRegistrationTechnicianDetails.lunchBreakTo == null ? null : this.timeToDate(resp.resources.technicianStaffRegistrationTechnicianDetails.lunchBreakTo);
          resp.resources.technicianStaffRegistrationTechnicianDetails.workingHoursFrom = resp.resources.technicianStaffRegistrationTechnicianDetails.workingHoursFrom == null ? null : this.timeToDate(resp.resources.technicianStaffRegistrationTechnicianDetails.workingHoursFrom);
          resp.resources.technicianStaffRegistrationTechnicianDetails.workingHoursTo = resp.resources.technicianStaffRegistrationTechnicianDetails.workingHoursTo == null ? null : this.timeToDate(resp.resources.technicianStaffRegistrationTechnicianDetails.workingHoursTo);
        }
        console.log("this.TechnicianUserTypeDetails.isInternalUse",this.TechnicianUserTypeDetails.isInternalUse)
        this.userTypeForm.get('isUserType').setValue(this.TechnicianUserTypeDetails.isInternalUser);

        this.technicalStaffRegistrationCreationForm.patchValue(resp.resources.technicianStaffRegistrationTechnicianDetails, { emitEvent: false });
        this.onAfterTechUserTypeValueChange();
        this.technicalStaffRegistrationCreationForm.patchValue({
          aodDebtorId: resp.resources.technicianStaffRegistrationTechnicianDetails?.aodDebtorId,
          // boundaryId: resp.resources.technicianStaffRegistrationTechnicianDetails?.boundaryId,
          // technicianUserTypeConfigId: resp.resources.technicianStaffRegistrationTechnicianDetails?.technicianUserTypeConfigId,
          techTestingPassword: resp.resources.technicianStaffRegistrationTechnicianDetails?.techTestingPassword,
          panicDuresPassword: resp.resources.technicianStaffRegistrationTechnicianDetails?.panicDuresPassword,
          isJobTypeNew: resp.resources.technicianStaffRegistrationTechnicianDetails?.isJobTypeNew,
          technicianReplaceId: resp.resources.technicianStaffRegistrationTechnicianDetails?.technicianReplaceId,
          currentTechAreaId: resp.resources.technicianStaffRegistrationTechnicianDetails?.currentTechAreaId,
          mobileCountryCode: resp.resources.technicianStaffRegistrationTechnicianDetails?.mobileCountryCode,
          techTestingMoblileNumber: resp.resources.technicianStaffRegistrationTechnicianDetails?.techTestingMoblileNumber,
        })

        // this.onStatucCodeSelected(this.aodDebtorId); //set aodDebtorId
        this.technicalStaffRegistrationCreationForm.get('aodDebtorId').setValue(resp.resources.technicianStaffRegistrationTechnicianDetails?.aodDebtorCode);
        this.aodDebtorId = resp.resources.technicianStaffRegistrationTechnicianDetails?.aodDebtorId;

        this.technicalStaffRegistrationCreationForm.controls['technicianReplaceId'].patchValue(resp.resources.technicianStaffRegistrationTechnicianDetails?.technicianReplaceName);
        this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.isJobTypeReplacement == true ? this.isFirstTechReplaceId = true : null;

        if (resp.resources.technicianStaffRegistrationTechnicianDetails.isJobTypeNew == false) {
          this.techTypeChange();
          this.technicalStaffRegistrationCreationForm.get('techStockLocation').disable();
        }
        if (this.technicalStaffRegistrationCreationForm.value.isPeriodTypeDaily) {
          this.daysSelected = true
        }
        if (this.technicalStaffRegistrationCreationForm.value.isPeriodTypeWeekly) {
          this.weekSelected = true
        }
        if (resp.resources.employeeId)
          this.technicalStaffRegistrationCreationForm.controls['employeeId'].patchValue(resp.resources.employeeId)

        let boundaryIdValue: any;
        let techStockLoc: any = '';
        this.isFristTimeUpdate = true;
        if (this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.isJobTypeReplacement == false) {
          this.crudService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.TECHNICIAN_USER_TYPE_CONFIG_DETAILS, null, false,
            prepareGetRequestHttpParams(null, null,
              { RoleId: this.technicianUserTypeList.find(x => x.id == this.technicalStaffRegistrationCreationForm.get('technicianUserTypeConfigId').value).roleId }
            )
          ).subscribe((response: IApplicationResponse) => {

            this.rxjsService.setGlobalLoaderProperty(false);
            this.technicianUserTypeConfig = response.resources[0];
            response.resources.length > 0 ? this.technicianUserTypeConfig.isMultiTechArea == true ? this.isTechAreaManger = true : this.isTechAreaManger = false : null;
            // this.isTechAreaManger == true ? this.technicalStaffRegistrationCreationForm.get('isJobTypeNew').patchValue(true) : this.technicalStaffRegistrationCreationForm.get('isJobTypeNew').patchValue(true);
            this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].reset();
            this.isTechAreaManger == true ? this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue([]) : this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue('')


            // this.isFristTimeUpdate = false;
            if (resp.resources.techAreas != null) {

              if (this.isTechAreaManger) {
                if (resp.resources.techAreas.length > 0) {
                  boundaryIdValue = [];
                  resp.resources.techAreas.forEach((val) => {

                    boundaryIdValue.push(val.techAreaId);
                  })
                }
              }
              else {
                this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.isJobTypeNew == false ? boundaryIdValue = resp.resources.techAreas[0].newTechAreaId : boundaryIdValue = resp.resources.techAreas[0].techAreaId;;
                this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.isJobTypeNew == false ? techStockLoc = resp.resources.techAreas[0].newTechStockLocation : techStockLoc = resp.resources.techAreas[0].techStockLocation;;

                boundaryIdValue = resp.resources.techAreas[0].techAreaId;
                techStockLoc = resp.resources.techAreas[0].techStockLocation;
              }
            }
            else {
              boundaryIdValue = ''
              techStockLoc = ''
            }

            this.techAreaDetail = resp.resources.techAreas == null ? null : resp.resources.techAreas[0];


            this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue(boundaryIdValue)
            this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].patchValue(techStockLoc)


            if (this.technicianUserTypeConfig.isPSIRA == false) {

              this.technicalStaffRegistrationCreationForm.get('pSIRAExpiryDate').clearValidators();
              this.technicalStaffRegistrationCreationForm.get('pSIRAExpiryDate').updateValueAndValidity();
              this.technicalStaffRegistrationCreationForm.get('pSIRARegNo').clearValidators();
              this.technicalStaffRegistrationCreationForm.get('pSIRARegNo').updateValueAndValidity();
              this.technicalStaffRegistrationCreationForm.get('pisraCertificateName').clearValidators();
              this.technicalStaffRegistrationCreationForm.get('pisraCertificateName').updateValueAndValidity();

            }
            if (this.technicianUserTypeConfig.isTechTesting == false) {
              this.technicalStaffRegistrationCreationForm.get('techTestingMoblileNumber').clearValidators();
              this.technicalStaffRegistrationCreationForm.get('techTestingMoblileNumber').updateValueAndValidity();
              this.technicalStaffRegistrationCreationForm.get('techTestingPassword').clearValidators();
              this.technicalStaffRegistrationCreationForm.get('techTestingPassword').updateValueAndValidity();
            }
            // if (this.technicianUserTypeConfig.isTechTesting == true) {
            //   this.row?.nativeElement?.focus();
            //   this.row?.nativeElement?.blur();
            // }



          })
        }
        else {

          if (resp.resources.techAreas != null) {

            // if(this.isTechAreaManger){
            if (resp.resources.techAreas.length > 1) {
              boundaryIdValue = [];
              resp.resources.techAreas.forEach((val) => {

                boundaryIdValue.push(val.techAreaId);
              })
            }
            // }
            else {
              this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.isJobTypeNew == false ? boundaryIdValue = resp.resources.techAreas[0].newTechAreaId : boundaryIdValue = resp.resources.techAreas[0].techAreaId;;
              this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.isJobTypeNew == false ? techStockLoc = resp.resources.techAreas[0].newTechStockLocation : techStockLoc = resp.resources.techAreas[0].techStockLocation;;

              // boundaryIdValue = data.resources.techAreas[0].techAreaId;
              techStockLoc = resp.resources.techAreas[0].techStockLocation;
            }
          }
          else {
            boundaryIdValue = ''
            techStockLoc = ''
          }

          this.techAreaDetail = resp.resources.techAreas == null ? null : resp.resources.techAreas[0];


          this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue(boundaryIdValue)
          this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].patchValue(techStockLoc)

        }


        this.technicalStaffRegistrationCreationForm.controls['terminationDate'].patchValue(resp.resources.technicianStaffRegistrationTechnicianDetails.terminationDate);
        this.technicalStaffRegistrationCreationForm.controls['startDate'].patchValue(resp.resources.technicianStaffRegistrationTechnicianDetails.startDate);
        this.technicalStaffRegistrationCreationForm.controls['pSIRARegNo'].patchValue(resp.resources.technicianStaffRegistrationTechnicianDetails.psiraRegNo);
        this.technicalStaffRegistrationCreationForm.controls['pSIRAExpiryDate'].patchValue(resp.resources.technicianStaffRegistrationTechnicianDetails.psiraExpiryDate);
        this.technicalStaffRegistrationCreationForm.controls['pisraCertificateName'].patchValue(resp.resources.technicianStaffRegistrationTechnicianDetails.pisraCertificateName);
        this.technicalStaffRegistrationCreationForm.controls['profileImageFileName'].patchValue(resp.resources.technicianStaffRegistrationTechnicianDetails.profileImageFileName)
        this.isFirstCallUpdate = true
        this.isFirstCallTechStockLoc = true



        // code for incentive tab
        this.userTypeForm.get('isUserType').setValue(this.TechnicianUserTypeDetails.isInternalUser);
        if (this.TechnicianUserTypeDetails.hasOwnProperty('incentiveSchemeDetails') && this.TechnicianUserTypeDetails?.incentiveSchemeDetails != undefined) {
          this.incetiveForm.get('technicianDetailId').setValue(this.TechnicianUserTypeDetails?.incentiveSchemeDetails?.technicianDetailId);
          this.incetiveForm.get('incentiveTypeConfigId').setValue(this.TechnicianUserTypeDetails?.incentiveSchemeDetails?.incentiveTypeConfigId ? this.TechnicianUserTypeDetails.incentiveSchemeDetails.incentiveTypeConfigId : '');
          this.incetiveForm.get('incentiveEffectiveDate').setValue(this.TechnicianUserTypeDetails?.incentiveSchemeDetails?.incentiveEffectiveDate ? new Date(this.TechnicianUserTypeDetails?.incentiveSchemeDetails?.incentiveEffectiveDate) : new Date());
          this.incetiveForm.get('incentiveStatus').setValue(this.TechnicianUserTypeDetails?.incentiveSchemeDetails?.techIncentiveSchemeApprovalStatusName ? this.TechnicianUserTypeDetails.incentiveSchemeDetails.techIncentiveSchemeApprovalStatusName : '');
        } else {
          this.tabGroup._tabs['_results'][2].disabled = true;
        }
        this.incetiveForm.get('createdUserId').setValue(this.userData.userId);
      }
      else {
        if (this.action == 'Create') {
          this.technicianUserTypeList.find(x => x.displayName == this.TechnicianUserTypeDetails.roleName) != undefined ? this.technicalStaffRegistrationCreationForm.get('technicianUserTypeConfigId').patchValue(this.technicianUserTypeList.find(x => x.displayName == this.TechnicianUserTypeDetails.roleName).id) : null;
        }
        this.userTypeForm.get('isUserType').setValue(this.TechnicianUserTypeDetails.isInternalUser);
        this.technicalStaffRegistrationCreationForm.value.isPeriodTypeDaily == false ? this.weekSelected = true : this.daysSelected = true;
        this.technicalStaffRegistrationCreationForm.get('isEveryWeekDay').patchValue(true);
        this.tabGroup._tabs['_results'][2].disabled = true;
        this.technicalStaffRegistrationCreationForm.controls['employeeId'].patchValue(resp.resources.employeeId)
      }
      setTimeout(() => {
        if (this.technicianUserTypeConfig != undefined) {
          if (this.technicianUserTypeConfig.isTechTesting == true) {
            this.row?.nativeElement?.focus();
            this.row?.nativeElement?.blur();
          }
        }
      }, 100);
    } else {
      this.TechnicianUserTypeDetails = {};
    }
  }
  onChangeStockCode(event) {
    if (event.target.value) {
      let params = { searchText: event.target.value, branchId: this.branchId };
      // this.searchTerm = event.target.value;
      let api = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_AOD, null, false, prepareRequiredHttpParams(params));
      api.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.debtorCodeList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
          this.isLoading = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }
  onStatucCodeSelected(value) {
    // let TYPE = (type == 'stock-description') ? 'stockDescription' : 'stockCode';

    if (value) {
      let aodDebtorIdArr = this.debtorCodeList.filter(x => x.debtorId == value);

      this.aodDebtorId = aodDebtorIdArr[0].debtorId;
      this.technicalStaffRegistrationCreationForm.get('aodDebtorId').setValue(aodDebtorIdArr[0].debtorRefNo);
      this.technicalStaffRegistrationCreationForm.get('debtorName').setValue(aodDebtorIdArr[0].displayName);

    }

  }
  onTechnicianReplacedSelected(techReplaceVal) {

    if (this.technicalStaffRegistrationCreationForm.controls['isJobTypeNew'].value == false && techReplaceVal != '') {
      this.technicalStaffRegistrationCreationForm.controls['currentTechAreaId'].patchValue(null);
      this.technicalStaffRegistrationCreationForm.controls['currentTechAreaId'].disable();
      this.technicalStaffRegistrationCreationForm.controls['currentTechStockLocationId'].patchValue(null);
      this.technicalStaffRegistrationCreationForm.controls['currentTechStockLocationId'].disable();
      this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.TECH_AREA_BOUNDARY, null, true, prepareGetRequestHttpParams(null, null, { UserId: this.technicianList.find(x => x.displayName == techReplaceVal).id })).subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.resources == null) {
            this.alertTechnicalStaffRegValidation = true;
            this.technicalStaffRegistrationCreationForm.get('technicianReplaceId').patchValue('');
            this.alertMessage = 'Replacement technician does not have tech area mapped'
          }
          else if (response.resources != null && response.resources.stockHoldingWarehouse == null) {
            this.alertTechnicalStaffRegValidation = true;
            this.technicalStaffRegistrationCreationForm.get('technicianReplaceId').patchValue('');
            this.alertMessage = 'Tech Area choosen not does have warehouse and SLOC mapped'
          }
          else {
            this.technicianReplaced = response.resources;
            this.technicalStaffRegistrationCreationForm.controls['currentTechAreaId'].patchValue(response.resources == null ? null : response.resources.boundaryName);
            this.technicalStaffRegistrationCreationForm.controls['currentTechAreaId'].disable();
            this.technicalStaffRegistrationCreationForm.controls['currentTechStockLocationId'].patchValue(response.resources == null ? null : response.resources.techStockLocationName);
            this.technicalStaffRegistrationCreationForm.controls['currentTechStockLocationId'].disable();
          }

        })
    }
  }

  getDebtorCodeList(branchId) {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_AOD,
      undefined,
      false, prepareRequiredHttpParams(
        {
          BranchId: branchId
        }
      )).subscribe(data => {
        if (data.isSuccess) {
          if (data.resources == null) {
            ///  this.debtorCodeList =[];
          }
          else {
            //this.debtorCodeList =data.resources
          }
        }
      })
  }

  formControlChange() {
    this.technicalStaffRegistrationCreationForm.get('mobileCountryCode').valueChanges.subscribe((mobileCountryCode: string) => {

      this.setPhoneNumberLengthByCountryCode(mobileCountryCode);
      setTimeout(() => {
        if (this.technicianUserTypeConfig.isTechTesting == true) {
          this.row?.nativeElement?.focus();
          this.row?.nativeElement?.blur();
        }
      }, 4000);


    });
    this.technicalStaffRegistrationCreationForm.get('techTestingMoblileNumber').valueChanges.subscribe((mobileNumber2: string) => {

      this.setPhoneNumberLengthByCountryCode(this.technicalStaffRegistrationCreationForm.get('mobileCountryCode').value);

    });

  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.technicalStaffRegistrationCreationForm.get('techTestingMoblileNumber').setValidators([Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        break;
      default:
        this.technicalStaffRegistrationCreationForm.get('techTestingMoblileNumber').setValidators([Validators.minLength(formConfigs.indianContactNumberMaxLength),
        Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);

        break;
    }

  }

  uploadFiles(event) {
    this.technicalStaffRegistrationCreationForm.controls['pisraCertificateName'].patchValue(event.target.files[0].name);
    this.formData.append('PISRACertificateName', event.target.files[0]);
  }
  profileUploadFiles(event) {
    this.technicalStaffRegistrationCreationForm.controls['profileImageFileName'].patchValue(event.target.files[0].name);
    this.formData.append('ProfileImage', event.target.files[0]);
  }

  // getTechnicianUserTypeList() {
  //   this.crudService.get(
  //     ModulesBasedApiSuffix.TECHNICIAN,
  //     TechnicalMgntModuleApiSuffixModels.TECHNICIAN_USER_TYPE_CONFIG_SEARCH, null, true).subscribe((response: IApplicationResponse) => {
  //       this.technicianUserTypeList = response.resources;
  //       if (this.action == 'Create') {
  //         this.technicianUserTypeList.find(x => x.displayName == this.TechnicianUserTypeDetails.roleName) != undefined ? this.technicalStaffRegistrationCreationForm.get('technicianUserTypeConfigId').patchValue(this.technicianUserTypeList.find(x => x.displayName == this.TechnicianUserTypeDetails.roleName).id) : null;
  //       }
  //       if (this.technicalStaffRegistrationCreationForm.get('technicianUserTypeConfigId').value != '' && this.technicalStaffRegistrationCreationForm.get('technicianUserTypeConfigId').value != null && this.technicianUserTypeList.length > 0) {
  //         this.crudService.get(
  //           ModulesBasedApiSuffix.TECHNICIAN,
  //           TechnicalMgntModuleApiSuffixModels.TECHNICIAN_USER_TYPE_CONFIG_DETAILS, null, false,
  //           prepareGetRequestHttpParams(null, null,
  //             { RoleId: this.technicianUserTypeList.find(x => x.id == this.technicalStaffRegistrationCreationForm.get('technicianUserTypeConfigId').value).roleId }
  //           )
  //         ).subscribe((response: IApplicationResponse) => {

  //           this.technicianUserTypeConfig = response.resources[0];
  //           response.resources.length > 0 ? this.technicianUserTypeConfig.isMultiTechArea == true ? this.isTechAreaManger = true : this.isTechAreaManger = false : null;
  //           if (this.technicianUserTypeConfig.isPSIRA == false) {
  //             this.technicalStaffRegistrationCreationForm.get('pSIRAExpiryDate').clearValidators();
  //             this.technicalStaffRegistrationCreationForm.get('pSIRAExpiryDate').updateValueAndValidity();
  //             this.technicalStaffRegistrationCreationForm.get('pSIRARegNo').clearValidators();
  //             this.technicalStaffRegistrationCreationForm.get('pSIRARegNo').updateValueAndValidity();
  //             this.technicalStaffRegistrationCreationForm.get('pisraCertificateName').clearValidators();
  //             this.technicalStaffRegistrationCreationForm.get('pisraCertificateName').updateValueAndValidity();
  //           }
  //           if (this.technicianUserTypeConfig.isTechTesting == false) {
  //             this.technicalStaffRegistrationCreationForm.get('techTestingMoblileNumber').clearValidators();
  //             this.technicalStaffRegistrationCreationForm.get('techTestingMoblileNumber').updateValueAndValidity();
  //             this.technicalStaffRegistrationCreationForm.get('techTestingPassword').clearValidators();
  //             this.technicalStaffRegistrationCreationForm.get('techTestingPassword').updateValueAndValidity();
  //           }


  //         })
  //       }

  //       this.rxjsService.setGlobalLoaderProperty(false);
  //     })
  // }

  getTechnicianName() {

    let TechnicianName = ''
    if (this.action == 'Create') {
      if (this.technicalStaffRegistrationCreationForm.get('technicianUserTypeConfigId').value != '') {
        TechnicianName = this.technicianUserTypeList.find(x => x.id == this.technicalStaffRegistrationCreationForm.get('technicianUserTypeConfigId').value).displayName
      }
    }
    else {
      TechnicianName = this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.userTypeName
    }
    if (TechnicianName != '') {
      this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN_BY_ROLE, null, true,
        prepareGetRequestHttpParams(null, null, { RoleName: TechnicianName })).subscribe((response: IApplicationResponse) => {
          this.technicianList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }

  }

  getSignalManagementCoordinator() {
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      TechnicalMgntModuleApiSuffixModels.UX_COORDINATOR, null, true
      // prepareGetRequestHttpParams(null, null, { RoleName: 'Coordinator' })
    ).subscribe((response: IApplicationResponse) => {
      this.signalManagementCoordinatorList = response.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onTechAreaSelected(techValue) {
    //  this.techStockLocationFlag = false;
    this.techAreaDetail = null;
    if (this.technicalStaffRegistrationCreationForm.get('isJobTypeNew').value == true) {
      if (techValue != '' && techValue != null && this.action == 'Update') {
        this.techAreaDetail = this.filterTechAreaList.find(x => x.value === techValue)
      }
      if (this.techAreaDetail != null) {
        if (this.techAreaDetail.stockHoldingWarehouseId == null && this.techAreaDetail.stockHoldingLocationId == null) {
          this.alertTechnicalStaffRegValidation = true;
          let boundaryIdValue: any;
          if (this.TechnicianUserTypeDetails.techAreas != null) {
            if (this.TechnicianUserTypeDetails.techAreas.length > 1) {
              boundaryIdValue = [];
              this.TechnicianUserTypeDetails.resources.techAreas.forEach((val) => {

                boundaryIdValue.push(val.techAreaId);
              })
            }
            else {
              this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.isJobTypeNew == false ? boundaryIdValue = this.TechnicianUserTypeDetails.techAreas[0].newTechAreaId : boundaryIdValue = this.TechnicianUserTypeDetails.techAreas[0].techAreaId;;

            }
          }
          else {
            boundaryIdValue = ''
            // techStockLoc =''
          }

          this.techAreaDetail = this.TechnicianUserTypeDetails.techAreas == null ? null : this.TechnicianUserTypeDetails.techAreas[0];


          this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue(boundaryIdValue)

          this.alertMessage = 'Tech Area choosen not does have warehouse and SLOC mapped';
          this.technicalStaffRegistrationCreationForm.get('newTechStockLocation').reset();
          // this.techStockLocationFlag = true;
        }
        else {
          if (this.TechnicianUserTypeDetails.techAreas[0].techAreaId != techValue) {
            this.crudService.get(
              ModulesBasedApiSuffix.TECHNICIAN,
              TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_TECHNICIANAREA_CHANGE_VALIDATE, null, true,
              prepareRequiredHttpParams({
                EmployeeId: this.TechnicianUserTypeDetails.employeeId,
                TechAreaId: techValue
              })).subscribe((response: IApplicationResponse) => {
                this.rxjsService.setGlobalLoaderProperty(false);

                if (response.isSuccess) {
                  if (!(response.resources.isSuccess)) {
                    this.isTechAreaChangeRequest = true;
                    this.alertTechnicalStaffRegValidation = true;
                    // const dialogData = {
                    //   title: 'Alert',
                    //   message: response.resources.validateMessage,
                    //   isClose: true,
                    //   isAlert: true,
                    // msgCenter: true,
                    // }
                    // this.openConfirmDialog(dialogData)
                    let boundaryIdValue: any;
                    if (this.TechnicianUserTypeDetails.techAreas != null) {
                      if (this.TechnicianUserTypeDetails.techAreas.length > 1) {
                        boundaryIdValue = [];
                        this.TechnicianUserTypeDetails.resources.techAreas.forEach((val) => {

                          boundaryIdValue.push(val.techAreaId);
                        })
                      }
                      else {
                        this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.isJobTypeNew == false ? boundaryIdValue = this.TechnicianUserTypeDetails.techAreas[0].newTechAreaId : boundaryIdValue = this.TechnicianUserTypeDetails.techAreas[0].techAreaId;;

                      }
                    }
                    else {
                      boundaryIdValue = ''
                    }

                    this.techAreaDetail = this.TechnicianUserTypeDetails.techAreas == null ? null : this.TechnicianUserTypeDetails.techAreas[0];


                    this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue(boundaryIdValue)

                    this.alertMessage = response.resources.validateMessage;
                    //  this.techStockLocationFlag = true;
                  }
                  else if (response.resources.isSuccess == true && response.resources.validateMessage == 'IsTechStockLocation') {

                    //= true;
                    if (this.alertTechnicalStaffRegValidation == false) {
                      this.crudService.get(
                        ModulesBasedApiSuffix.TECHNICIAN,
                        TechnicalMgntModuleApiSuffixModels.TECH_STOCK_LOCATIONS_GET_TECH_STOCK_TECH_LOCATIONS, null, true,
                        prepareGetRequestHttpParams(null, null, {
                          // TechAreaId: this.techAreaDetail.boundaryId
                          TechAreaId: techValue
                        })).subscribe((response: IApplicationResponse) => {
                          this.rxjsService.setGlobalLoaderProperty(false);
                          let saveObj = Object.assign({}, this.technicalStaffRegistrationCreationForm.getRawValue());
                          if (saveObj.isJobTypeNew)
                            this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].patchValue(response.resources.techStockLocationName);
                          else if (!saveObj.isJobTypeNew) {
                            this.technicalStaffRegistrationCreationForm.controls['newTechStockLocation'].patchValue(response.resources.techStockLocationName);
                            this.technicalStaffRegistrationCreationForm.controls['newTechStockLocation'].disable();

                          }
                        });
                    }
                  }
                }
                // this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].patchValue(response.resources.techStockLocationName);
              })
          }


        }
      }


      if (techValue != '' && techValue != null && this.action != 'Update') {
        let techAreaItem = this.filterTechAreaList.find(x => x.value == techValue)
        if (techAreaItem.stockHoldingWarehouseId == null && techAreaItem.stockHoldingLocationId == null) {
          this.alertTechnicalStaffRegValidation = true;

          this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue('')
          this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].patchValue(null);

          this.alertMessage = 'Tech Area choosen not does have warehouse and SLOC mapped';
          this.technicalStaffRegistrationCreationForm.get('newTechStockLocation').reset();
        }
        else {
          this.techAreaDetail = this.filterTechAreaList.find(x => x.value === techValue)

          this.crudService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.TECH_STOCK_LOCATIONS_GET_TECH_STOCK_TECH_LOCATIONS, null, true,
            prepareGetRequestHttpParams(null, null, {
              // TechAreaId: this.techAreaDetail.boundaryId
              TechAreaId: techValue
            })).subscribe((response: IApplicationResponse) => {
              this.rxjsService.setGlobalLoaderProperty(false);
              this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].patchValue(response.resources.techStockLocationName);
            })
        }

      }
    }
    else {
      if (this.technicalStaffRegistrationCreationForm.get('technicianReplaceId').value == '' || this.technicalStaffRegistrationCreationForm.get('technicianReplaceId').value == null) {
        this.technicalStaffRegistrationCreationForm.get('newTechStockLocation').setValue(null);
        this.snackbarService.openSnackbar("please select the technician being replaced", ResponseMessageTypes.WARNING);
      }
      else {
        this.techAreaDetail = this.filterTechAreaList.find(x => x.value == techValue)
        if (this.technicianReplaced != null) {
          if (this.techAreaDetail.stockHoldingWarehouseId == this.technicianReplaced.stockHoldingWarehouseId && this.techAreaDetail.stockHoldingLocationId == this.technicianReplaced.stockHoldingLocationId) {
            this.crudService.get(
              ModulesBasedApiSuffix.TECHNICIAN,
              TechnicalMgntModuleApiSuffixModels.TECH_STOCK_LOCATIONS_GET_TECH_STOCK_TECH_LOCATIONS, null, true,
              prepareGetRequestHttpParams(null, null, {
                // TechAreaId: this.techAreaDetail.boundaryId
                TechAreaId: techValue
              })).subscribe((response: IApplicationResponse) => {
                this.rxjsService.setGlobalLoaderProperty(false);
                let saveObj = Object.assign({}, this.technicalStaffRegistrationCreationForm.getRawValue());
                if (saveObj.isJobTypeNew)
                  this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].patchValue(response.resources.techStockLocationName);
                else if (!saveObj.isJobTypeNew && !this.alertTechnicalStaffRegValidation) {
                  this.technicalStaffRegistrationCreationForm.controls['newTechStockLocation'].patchValue(response.resources.techStockLocationName);
                  this.technicalStaffRegistrationCreationForm.controls['newTechStockLocation'].disable();

                }


                // this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].patchValue(response.resources.techStockLocationName);
              })
          }
          else {
            this.alertTechnicalStaffRegValidation = true;
            let boundaryIdValue: any;
            let techStockLoc: any = '';
            this.isFristTimeUpdate = true;
            if (this.TechnicianUserTypeDetails.techAreas != null) {
              if (this.TechnicianUserTypeDetails.techAreas.length > 1) {
                boundaryIdValue = [];
                this.TechnicianUserTypeDetails.techAreas.forEach((val) => {

                  boundaryIdValue.push(val.techAreaId);
                })
              }
              else {
                this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.isJobTypeNew == false ? boundaryIdValue = this.TechnicianUserTypeDetails.techAreas[0].newTechAreaId : boundaryIdValue = this.TechnicianUserTypeDetails.techAreas[0].techAreaId;
                this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.isJobTypeNew == false ? techStockLoc = this.TechnicianUserTypeDetails.techAreas[0].newTechStockLocation : techStockLoc = this.TechnicianUserTypeDetails.techAreas[0].techStockLocation;;


                techStockLoc = this.TechnicianUserTypeDetails.techAreas[0].techStockLocation;
              }
            }
            else {
              boundaryIdValue = ''
              techStockLoc = ''
            }

            this.techAreaDetail = this.TechnicianUserTypeDetails.techAreas == null ? null : this.TechnicianUserTypeDetails.techAreas[0];


            this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue(boundaryIdValue, { emitEvent: false })


            this.alertMessage = 'Warehouse data for the Tech Area Selected does not match that of the Replacement Technician Tech Area, please select a valid Tech Area'
            this.technicalStaffRegistrationCreationForm.get('newTechStockLocation').reset();

          }
        }

      }
    }
    if (this.alertTechnicalStaffRegValidation) {
      this.technicalStaffRegistrationCreationForm.get('newTechStockLocation').reset();
    }
  }

  onTechNewAreaSelected(techValue) {
    this.techAreaDetail = this.newFilterTechAreaList.find(x => x.boundaryName === techValue)
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECH_STOCK_LOCATIONS_GET_TECH_STOCK_TECH_LOCATIONS, null, true,
      prepareGetRequestHttpParams(null, null, { TechAreaId: this.techAreaDetail.boundaryId })).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.newtechAreaStockLocation = response.resources;
        this.technicalStaffRegistrationCreationForm.controls['currentTechStockLocationId'].patchValue(response.resources.techStockLocationName);
      })
  }

  createUserTypeForm() {
    this.userTypeForm = this.formBuilder.group({});
    this.userTypeForm.addControl('isUserType', new FormControl());
    this.userTypeForm = setRequiredValidator(this.userTypeForm, ['isUserType']);

  }

  TechnicalStaffRegistrationCreationForm(): void {
    let TechnicalStaffRegModel = new TechnicalStaffRegistrationModel();
    this.technicalStaffRegistrationCreationForm = this.formBuilder.group({});
    Object.keys(TechnicalStaffRegModel).forEach((key) => {
      this.technicalStaffRegistrationCreationForm.addControl(key, new FormControl(TechnicalStaffRegModel[key]));
    });
    this.technicalStaffRegistrationCreationForm = setRequiredValidator(this.technicalStaffRegistrationCreationForm, ['technicianUserTypeConfigId', 'boundaryId', 'startDate', 'pSIRAExpiryDate', 'pSIRARegNo', 'pisraCertificateName', 'techTestingPassword']);
    var startDayHours = new Date();
    startDayHours.setHours(8, 0, 0, 0);
    var endDayHours = new Date();
    endDayHours.setHours(17, 0, 0, 0);
    this.technicalStaffRegistrationCreationForm.controls['workingHoursFrom'].patchValue(new Date(startDayHours));
    this.technicalStaffRegistrationCreationForm.controls['workingHoursTo'].patchValue(endDayHours);
  }

  // getTechnicianUserTypeData(pageIndex?: string, pageSize?: string, otherParams?: object) {
  //   this.crudService.get(
  //     ModulesBasedApiSuffix.TECHNICIAN,
  //     TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_DETAILS,
  //     undefined,
  //     false, prepareGetRequestHttpParams(pageIndex, pageSize,
  //       {
  //         EmployeeId: this.TechnicianUserTypeId
  //       }
  //     )).subscribe(data => {
  //       this.rxjsService.setGlobalLoaderProperty(false);
  //       if (data.isSuccess) {
  //         this.technicalStaffRegistrationCreationForm.controls['terminationDate'].disable()
  //         this.TechnicianUserTypeDetails = data.resources;
  //         this.getTechnicianUserTypeList();
  //         this.getDebtorCodeList(data.resources.branchId);
  //         this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails != null ? this.debtorName = this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.aodDebtorName :  this.debtorName = '';
  //         if (this.action == "Update") {
  //           data.resources.technicianStaffRegistrationTechnicianDetails.lunchBreakFrom = data.resources.technicianStaffRegistrationTechnicianDetails.lunchBreakFrom == null ? null : this.timeToDate(data.resources.technicianStaffRegistrationTechnicianDetails.lunchBreakFrom);
  //           data.resources.technicianStaffRegistrationTechnicianDetails.lunchBreakTo = data.resources.technicianStaffRegistrationTechnicianDetails.lunchBreakTo == null ? null : this.timeToDate(data.resources.technicianStaffRegistrationTechnicianDetails.lunchBreakTo);
  //           data.resources.technicianStaffRegistrationTechnicianDetails.workingHoursFrom = data.resources.technicianStaffRegistrationTechnicianDetails.workingHoursFrom == null ? null : this.timeToDate(data.resources.technicianStaffRegistrationTechnicianDetails.workingHoursFrom);
  //           data.resources.technicianStaffRegistrationTechnicianDetails.workingHoursTo = data.resources.technicianStaffRegistrationTechnicianDetails.workingHoursTo == null ? null : this.timeToDate(data.resources.technicianStaffRegistrationTechnicianDetails.workingHoursTo);

  //           this.technicalStaffRegistrationCreationForm.patchValue(data.resources.technicianStaffRegistrationTechnicianDetails);
  //           if (data.resources.technicianStaffRegistrationTechnicianDetails.isJobTypeNew == false) {
  //             this.techTypeChange();
  //             this.technicalStaffRegistrationCreationForm.get('techStockLocation').disable();
  //           }
  //           if (this.technicalStaffRegistrationCreationForm.value.isPeriodTypeDaily) {
  //             this.daysSelected = true
  //           }
  //           if (this.technicalStaffRegistrationCreationForm.value.isPeriodTypeWeekly) {
  //             this.weekSelected = true
  //           }
  //           this.technicalStaffRegistrationCreationForm.controls['employeeId'].patchValue(data.resources.employeeId)

  //           let boundaryIdValue: any;
  //           let techStockLoc: any = '';
  //           this.isFristTimeUpdate = true;
  //           if (data.resources.techAreas != null) {
  //             if(this.isTechAreaManger){
  //               if (data.resources.techAreas.length > 0) {
  //                 boundaryIdValue = [];
  //                 data.resources.techAreas.forEach((val) => {

  //                   boundaryIdValue.push(val.techAreaId);
  //                 })
  //               }
  //             }
  //             else {
  //               this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.isJobTypeNew == false ? boundaryIdValue = data.resources.techAreas[0].newTechAreaId : boundaryIdValue = data.resources.techAreas[0].techAreaId;;
  //               this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.isJobTypeNew == false ? techStockLoc = data.resources.techAreas[0].newTechStockLocation : techStockLoc = data.resources.techAreas[0].techStockLocation;;

  //               // boundaryIdValue = data.resources.techAreas[0].techAreaId;
  //               techStockLoc = data.resources.techAreas[0].techStockLocation;
  //             }
  //           }
  //           else {
  //             boundaryIdValue = ''
  //             techStockLoc = ''
  //           }

  //           this.techAreaDetail = data.resources.techAreas == null ? null : data.resources.techAreas[0];


  //           this.technicalStaffRegistrationCreationForm.controls['boundaryId'].patchValue(boundaryIdValue)
  //           this.technicalStaffRegistrationCreationForm.controls['techStockLocation'].patchValue(techStockLoc)

  //           this.technicalStaffRegistrationCreationForm.controls['pSIRARegNo'].patchValue(data.resources.technicianStaffRegistrationTechnicianDetails.psiraRegNo);
  //           this.technicalStaffRegistrationCreationForm.controls['pSIRAExpiryDate'].patchValue(data.resources.technicianStaffRegistrationTechnicianDetails.psiraExpiryDate);
  //           this.technicalStaffRegistrationCreationForm.controls['pisraCertificateName'].patchValue(data.resources.technicianStaffRegistrationTechnicianDetails.pisraCertificateName);
  //           this.technicalStaffRegistrationCreationForm.controls['profileImageFileName'].patchValue(data.resources.technicianStaffRegistrationTechnicianDetails.profileImageFileName)
  //           this.isFirstCallUpdate = true
  //           this.isFirstCallTechStockLoc = true



  //           // code for incentive tab
  //           this.userTypeForm.get('isUserType').setValue(this.TechnicianUserTypeDetails.isInternalUser);
  //           if (this.TechnicianUserTypeDetails.hasOwnProperty('incentiveSchemeDetails') && this.TechnicianUserTypeDetails.incentiveSchemeDetails != undefined) {
  //             this.incetiveForm.get('technicianDetailId').setValue(this.TechnicianUserTypeDetails.incentiveSchemeDetails.technicianDetailId);
  //             this.incetiveForm.get('incentiveTypeConfigId').setValue(this.TechnicianUserTypeDetails.incentiveSchemeDetails.incentiveTypeConfigId ? this.TechnicianUserTypeDetails.incentiveSchemeDetails.incentiveTypeConfigId : '');
  //             this.incetiveForm.get('incentiveEffectiveDate').setValue(new Date(this.TechnicianUserTypeDetails.incentiveSchemeDetails.incentiveEffectiveDate));
  //             this.incetiveForm.get('incentiveStatus').setValue(this.TechnicianUserTypeDetails.incentiveSchemeDetails.techIncentiveSchemeApprovalStatusName ? this.TechnicianUserTypeDetails.incentiveSchemeDetails.techIncentiveSchemeApprovalStatusName : '');
  //           } else {
  //             this.tabGroup._tabs['_results'][2].disabled = true;
  //           }
  //           this.incetiveForm.get('createdUserId').setValue(this.userData.userId);
  //         }
  //         else {
  //           this.userTypeForm.get('isUserType').setValue(this.TechnicianUserTypeDetails.isInternalUser);
  //           this.technicalStaffRegistrationCreationForm.value.isPeriodTypeDaily == false ? this.weekSelected = true : this.daysSelected = true;
  //           this.technicalStaffRegistrationCreationForm.get('isEveryWeekDay').patchValue(true);
  //           this.tabGroup._tabs['_results'][2].disabled = true;
  //           this.technicalStaffRegistrationCreationForm.controls['employeeId'].patchValue(data.resources.employeeId)
  //         }
  //         setTimeout(() => {
  //           if (this.technicianUserTypeConfig != undefined) {
  //             if (this.technicianUserTypeConfig.isTechTesting == true) {
  //               this.row?.nativeElement?.focus();
  //               this.row?.nativeElement?.blur();
  //             }
  //           }
  //         }, 100);
  //       } else {
  //         this.TechnicianUserTypeDetails = {};
  //       }
  //     });
  // }

  timeToDate(time) {
    var timeArray = time.split(':');
    var d = new Date();
    d.setHours(timeArray[0]);
    d.setMinutes(timeArray[1]);
    d.setSeconds(0);
    return d;
  }

  radioChange(e) {
    if (e.value) {
      this.daysSelected = true;
      this.weekSelected = false;
      this.technicalStaffRegistrationCreationForm.get('isEveryWeekDay').patchValue(false)

    } else {
      this.daysSelected = false;
      this.weekSelected = true;
      this.technicalStaffRegistrationCreationForm.get('isEveryWeekDay').patchValue(true)
    }
  }

  pSIRARegistrationNumberblurEvent(event?) {

    // this.technicalStaffRegistrationCreationForm.get('techTestingMoblileNumber').setValue(e.target.value);
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_PSIRAREGNO_VALIDATE, undefined, false, prepareRequiredHttpParams({
      EmployeeId: this.employeeId,
      PSIRARegNo: this.technicalStaffRegistrationCreationForm.get('pSIRARegNo').value
    })).subscribe((response: IApplicationResponse) => {
      // this.isPsiraRegNoBlurEvent = true;
      this.isValidPsiraRegistrationNumber = response.resources.isSuccess;
      this.isValidPsiraRegistrationNumberValidMessage = response.resources.validateMessage;
      this.rxjsService.setGlobalLoaderProperty(false);
    });

  }

  blurEvent(e) {
    this.technicalStaffRegistrationCreationForm.get('techTestingMoblileNumber').setValue(e.target.value);
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_TECHTESTINGMOBILE_VALIDATOR, undefined, false, prepareRequiredHttpParams({
      EmployeeId: this.employeeId,
      TechTestingMoblileNumber: this.technicalStaffRegistrationCreationForm.get('techTestingMoblileNumber').value.toString().replace(/\s/g, "")
    })).subscribe((response: IApplicationResponse) => {

      this.isValidTechTestMobileNo = response.resources.isSuccess;
      this.isValidTechTestMobileNoValidMessage = response.resources.validateMessage;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  saveClick() {
    setTimeout(() => {
      if (this.technicianUserTypeConfig != null) {
        if (this.technicianUserTypeConfig.isTechTesting == true) {
          this.row?.nativeElement?.focus();
          this.row?.nativeElement?.blur();

        }
      }

      if (this.userTypeForm.invalid || (!this.isValidTechTestMobileNo) || this.isPasswordDuplicate || (!this.isValidPsiraRegistrationNumber)) {
        this.userTypeForm.markAllAsTouched();
        return;
      }
      if (this.technicalStaffRegistrationCreationForm.invalid) {
        this.technicalStaffRegistrationCreationForm.markAllAsTouched();
        return
      }

      this.rxjsService.setFormChangeDetectionProperty(true);
      let saveObj = Object.assign({}, this.technicalStaffRegistrationCreationForm.getRawValue());

      saveObj.lunchBreakFrom = this.datePipe.transform(saveObj.lunchBreakFrom, "HH:mm");
      saveObj.lunchBreakTo = this.datePipe.transform(saveObj.lunchBreakTo, "HH:mm");
      saveObj.workingHoursFrom = this.datePipe.transform(saveObj.workingHoursFrom, "HH:mm");
      saveObj.workingHoursTo = this.datePipe.transform(saveObj.workingHoursTo, "HH:mm");
      saveObj.startDate = this.datePipe.transform(saveObj.startDate, "yyyy-MM-dd");
      saveObj.terminationDate = this.datePipe.transform(saveObj.terminationDate, "yyyy-MM-dd");
      saveObj.pSIRAExpiryDate = this.datePipe.transform(saveObj.pSIRAExpiryDate, "yyyy-MM-dd");
      saveObj.techAreaIds = saveObj.isJobTypeNew == true ? (typeof (saveObj.boundaryId) == 'string' ? saveObj.boundaryId : saveObj.boundaryId.join(',')) : this.TechnicianUserTypeDetails.techAreas[0].techAreaId;
      saveObj.techStockLocation = saveObj.isJobTypeNew == true ? saveObj.techStockLocation : this.TechnicianUserTypeDetails.techAreas[0].techStockLocation;
      saveObj.createdUserId = this.userData.userId;
      saveObj.modifiedUserId = this.userData.userId;
      saveObj.isJobTypeReplacement = saveObj.isJobTypeNew == true ? false : true;
      saveObj.isJobTypeNew = saveObj.isJobTypeNew == true ? true : false;
      saveObj.technicianReplaceId = saveObj.isJobTypeNew == true ? null : saveObj.technicianReplaceId;
      // saveObj.currentTechAreaId = saveObj.isJobTypeNew == true ? null : (saveObj.currentTechAreaId != null && saveObj.currentTechAreaId != '') ? this.technicianReplaced.boundaryId : ''
      // saveObj.currentTechStockLocationId = saveObj.isJobTypeNew == true ? null : (saveObj.currentTechStockLocationId != null && saveObj.currentTechStockLocationId != '') ? this.technicianReplaced.techStockLocationId : '';
      saveObj.isPeriodTypeDaily = saveObj.isPeriodTypeDaily == true ? true : false;
      saveObj.isPeriodTypeWeekly = saveObj.isPeriodTypeDaily == true ? false : true;
      saveObj.techTestingMoblileNumber = saveObj.techTestingMoblileNumber.toString().replace(/\s/g, "");
      saveObj.isUserType = this.userTypeForm.get('isUserType').value;
      saveObj.newTechAreaId = saveObj.isJobTypeNew == false ? saveObj.boundaryId : null;
      saveObj.newTechStockLocation = saveObj.isJobTypeNew == false ? saveObj.newTechStockLocation ? saveObj.newTechStockLocation : this.technicalStaffRegistrationCreationForm.value.techStockLocation : null;
      saveObj.isEveryDay = saveObj.isEveryDay == true ? true : false;
      saveObj.isEveryWeekDay = saveObj.isEveryDay == false ? true : false;
      saveObj.technicianReplaceId = saveObj.technicianReplaceId == null ? null : this.technicianList?.find(x => x.displayName == saveObj.technicianReplaceId)?.id;
      delete saveObj.boundaryId;

      saveObj.pisraCertificatePath = this.pisraCertificatePath;
      saveObj.profileImagePath = this.profileImagePath;
      saveObj.aodDebtorId = this.aodDebtorId;

      this.formData.delete("Obj")
      this.formData.append('Obj', JSON.stringify(saveObj));
      if (this.action == 'Create') {
        this.crudService
          .create(ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION,
            this.formData).subscribe({
              next: response => {
                this.rxjsService.setGlobalLoaderProperty(false);
                if (response.isSuccess && response.resources != null) {
                  if (typeof (response.resources) == 'object') {
                    if (response.resources.alertPopUp == true) {
                      this.alertTechnicalStaffRegValidation = true;
                      this.alertMessage = 'Stock in Location,please clear the Location before re-assigning'
                      this.router.navigate(['/user/technicial-staff-registration']);

                    }
                    else {
                      this.router.navigate(['/user/technicial-staff-registration']);

                    }
                  }
                  else {
                    this.router.navigate(['/user/technicial-staff-registration']);

                  }
                }
                else {
                  this.router.navigate(['/user/technicial-staff-registration']);

                }
              },

            });
      }
      else {
        this.crudService
          .update(ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION,
            this.formData).subscribe({
              next: response => {
                this.rxjsService.setGlobalLoaderProperty(false);
                if (response.isSuccess && response.resources != null) {

                  if (typeof (response.resources) == 'object') {
                    if (response.resources.alertPopUp == true) {
                      this.alertTechnicalStaffRegValidation = true;
                      this.alertMessage = 'A Notification has been sent to the Technician and Technical Area Manager';
                      this.isSaveAlertPopup = true;

                    }
                    else {
                      this.router.navigate(['/user/technicial-staff-registration']);

                    }
                  }
                  else {
                    this.router.navigate(['/user/technicial-staff-registration']);

                  }
                }
                else {
                  this.router.navigate(['/user/technicial-staff-registration']);

                }
              },
            });
      }
    }, 2000);


  }

  openConfirmDialog(dialogData, isTechArea = true) {
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      width: "500px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) {
        if((!dialogData?.isAlert && dialogData?.flag ==  false) || isTechArea == false) {
          this.onLoadDetailAPI();
        }
        return;
      };
      if (this.isTechAreaChangeRequest && this.action == 'Update' && this.employeeId) {
        this.openConfirmTechArea(isTechArea);
      } else if (this.action == 'Update' && this.employeeId){
        this.onCallTechAreaChangeRequest();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  openConfirmTechArea(flag?: boolean) {
    this.isTechAreaChangeRequest = false;
    const type = flag == false ? 'Technical User Type' : 'Technical Area';
    const confirmData = {
      title: 'Confirmation',
      message: `Are you sure want to change the ${type}?`,
      flag: flag,
      isClose: true,
      msgCenter: true,
    }
    this.openConfirmDialog(confirmData);
  }

  alertPopupClose() {
    this.alertTechnicalStaffRegValidation = false;
    if (this.isSaveAlertPopup == true) {
      this.router.navigate(['/user/technicial-staff-registration']);
    } else if (this.isTechAreaChangeRequest && this.action == 'Update' && this.employeeId) {
      this.openConfirmTechArea();
    }
  }

  onValidateTechnicalUserType(id) {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_USERTYPE_VALIDATE, null, false, prepareRequiredHttpParams({
      employeeId: this.TechnicianUserTypeDetails?.employeeId, TechnicianUserTypeId: id
    }))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          if (res.resources?.isSuccess) {
            this.onAfterTechUserTypeValueChange();
          } else if (!res.resources?.isSuccess) {
            this.isTechAreaChangeRequest = true;
            // this.alertTechnicalStaffRegValidation = true;
            const dialogData = {
              title: 'Alert',
              message: res.resources.validateMessage,
              isClose: true,
              isAlert: true,
              msgCenter: true,
            }
            this.openConfirmDialog(dialogData, false)
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onCallTechAreaChangeRequest() {
    this.rxjsService.setGlobalLoaderProperty(true);
    const reqObj = {
      employeeId: this.TechnicianUserTypeDetails?.employeeId,
      techAreaId: this.TechnicianUserTypeDetails?.techAreas[0]?.techAreaId,
      createdUserId: this.userData?.userId,
    }
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_TECHAREA_CHANGE_REQUEST, reqObj)
      .subscribe((res: IApplicationResponse) => {
        // if(res?.isSuccess && res?.statusCode == 200) {
        this.router.navigate(['/user/technicial-staff-registration/view'], {
          queryParams: {
            id: this.employeeId
          }, skipLocationChange: true
        });
        // }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  setAll(checked, value, action, index?: any) {
    if (action == 'add') {
      if (checked) {
        if (value == 4) {
          this.technicalStaffRegistrationCreationForm.controls['tsL1'].patchValue(true);
          this.technicalStaffRegistrationCreationForm.controls['tsL2'].patchValue(true);
          this.technicalStaffRegistrationCreationForm.controls['tsL3'].patchValue(true);
        }
        if (value == 3) {
          this.technicalStaffRegistrationCreationForm.controls['tsL1'].patchValue(true);
          this.technicalStaffRegistrationCreationForm.controls['tsL2'].patchValue(true);
        }
        if (value == 2) {
          this.technicalStaffRegistrationCreationForm.controls['tsL1'].patchValue(true);
        }
      }
      else {
        value = 0;
        this.technicalStaffRegistrationCreationForm.controls['tsL1'].patchValue(false);
        this.technicalStaffRegistrationCreationForm.controls['tsL2'].patchValue(false);
        this.technicalStaffRegistrationCreationForm.controls['tsL3'].patchValue(false);
        this.technicalStaffRegistrationCreationForm.controls['tsL4'].patchValue(false);
      }
    }
  }



  // code for Incentive Tab
  createIncentiveForm() {
    this.incetiveForm = this.formBuilder.group({});
    this.incetiveForm.addControl('technicianDetailId', new FormControl());
    this.incetiveForm.addControl('incentiveTypeConfigId', new FormControl());
    this.incetiveForm.addControl('createdUserId', new FormControl());
    this.incetiveForm.addControl('incentiveEffectiveDate', new FormControl());
    this.incetiveForm.addControl('incentiveStatus', new FormControl({ value: '', disabled: true }));
    this.incetiveForm = setRequiredValidator(this.incetiveForm, ['incentiveTypeConfigId']);
    this.incetiveForm.get("createdUserId").setValue(this.userData?.userId)
  }

  getIncentiveSchemeType() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_INCENTIVE_SCHEME_TYPE).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.incentiveSchemeTypeList = response.resources;

        }
      })
  }

  saveIncentive() {
    if(this.incetiveForm.invalid){
      this.incetiveForm.markAllAsTouched();
      return
    }
    if(this.incetiveForm?.value?.incentiveEffectiveDate){
      this.incetiveForm.value.incentiveEffectiveDate = this.incetiveForm.value.incentiveEffectiveDate ? this.datePipe.transform(this.incetiveForm.value.incentiveEffectiveDate, 'yyyy-MM-dd') : null
    }
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_INCENTIVE_SUBMIT,
      this.incetiveForm.value).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          //this.router.navigate(['/user/technicial-staff-registration']);
          this.alertTechnicalStaffRegValidation = true;
          this.alertMessage = response?.resources;

        }
      });
  }

  processIncentive() {
    let dataToSend = {
      employeeId: this.TechnicianUserTypeId, //'171d3db6-cd44-4cce-9176-c09c13ced257
      createdUserId: this.userData.userId
    };
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_INCENTIVE_PROCESS,
      dataToSend).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
        }
      });
  }

  showTechPassword(){
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canPasswordViews) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isTechTestingPasswordShow=!this.isTechTestingPasswordShow;
  }

  showDuresPassword(){
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canPasswordViews) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isPanicDuressPasswordShow=!this.isPanicDuressPasswordShow;
  }
}
