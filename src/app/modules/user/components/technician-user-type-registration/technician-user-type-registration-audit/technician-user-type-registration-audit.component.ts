import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-technician-user-type-registration-audit',
  templateUrl: './technician-user-type-registration-audit.component.html',
})
export class TechnicianUserTypeRegistrationAuditComponent implements OnInit {

  @Input() technicianDetailId: string;
  loading: boolean;
  dataList: any;
  status: any = [];
  selectedRows: string[] = [];
  totalRecords: any;
  userSubscription: any;
  listSubscription: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  loggedInUserData: any;
  searchColumns: any;
  observableResponse: any;
  isShowNoRecords: any = true;
  customerId: any;
  customerAddressId: any;
  selectedRowData: any;

  constructor(private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    private crudService: CrudService, private store: Store<AppState>, private rxjsService: RxjsService,) {
    this.primengTableConfigProperties = {
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Audit Log',
            dataKey: 'occurrenceBookSignalId',
            enableAction: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'DateTime', header: 'Date & Time',  isDateTime:true,width: '160px' },
            { field: 'UserName', header: 'User Name', width: '200px' },
            { field: 'Field', header: 'Field', width: '150px' },
            { field: 'OldValue', header: 'Old Value', width: '150px' },
            { field: 'NewValue', header: 'New Value', width: '150px' },],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_DETAIL_LOG,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          },
        ]
      }
    }
    // this.activatedRoute.queryParamMap.subscribe((params) => {
    //   this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    // });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    // this.getRequiredListData();
  }

  ngOnChanges(changes: SimpleChanges) {
    // this.onTableChanges();
    if(changes?.technicianDetailId?.currentValue != changes?.technicianDetailId?.previousValue) {
      this.getRequiredListData();
    }
  }

  onTableChanges() {
    if (this.selectedTabIndex == 1) {
      this.dataList = this.observableResponse;
      this.totalRecords = 0;
      this.isShowNoRecords = this.dataList?.length ? false : true;
    } else if (this.selectedTabIndex == 3) {
      this.dataList = this.observableResponse?.resources;
      this.totalRecords = this.observableResponse?.totalCount ? this.observableResponse?.totalCount : 0;
      this.isShowNoRecords = true;
    } else {
      this.dataList = this.observableResponse;
      this.totalRecords = this.observableResponse?.totalCount ? this.observableResponse?.totalCount : 0;
      this.isShowNoRecords = true;
    }
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }

  combineLatestNgrxStoreData() {
    this.userSubscription = combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    if(!this.technicianDetailId) {
      this.observableResponse = null;
      this.dataList = this.observableResponse;
      this.totalRecords = 0;
      this.isShowNoRecords = true;
      return;
    }
    this.loading = true;
    this.isShowNoRecords = false;
    this.dataList = [];
    let technicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    technicalMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = { TechnicianDetailId: this.technicianDetailId, ...otherParams };
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
    }
    this.listSubscription = this.crudService.get(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        technicalMgntModuleApiSuffixModels, undefined, false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams))
      .subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data?.statusCode == 200) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        // this.totalRecords = 0;
        // this.isShowNoRecords = this.dataList?.length ? false : true;
        this.totalRecords = data?.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse;
        this.totalRecords = 0;
      }
      // this.onTableChanges();
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            break;
        }
    }
  }

  ngOnDestroy() {
    // if (this.listSubscription) {
    //   this.listSubscription.unsubscribe();
    // }
    // if (this.userSubscription) {
    //   this.userSubscription.unsubscribe();
    // }
  }
}
