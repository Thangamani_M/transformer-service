import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicianUserTypeRegistrationAddEditComponent } from './technician-user-type-registration-add-edit/technician-user-type-registration-add-edit.component';
import { TechnicianUserTypeRegistrationAuditComponent } from './technician-user-type-registration-audit/technician-user-type-registration-audit.component';
import { TechnicianUserTypeRegistrationListComponent } from './technician-user-type-registration-list/technician-user-type-registration-list.component';
import { TechnicianUserTypeRegistrationRoutingModule } from './technician-user-type-registration-routing.module';
import { TechnicianUserTypeRegistrationViewComponent } from './technician-user-type-registration-view/technician-user-type-registration-view.component';



@NgModule({
  declarations: [TechnicianUserTypeRegistrationListComponent, TechnicianUserTypeRegistrationViewComponent, TechnicianUserTypeRegistrationAddEditComponent, TechnicianUserTypeRegistrationAuditComponent],
  imports: [
    CommonModule, SharedModule, MaterialModule, ReactiveFormsModule, FormsModule,
    LayoutModule, 
    TechnicianUserTypeRegistrationRoutingModule
  ]
})
export class TechnicianUserTypeRegistrationModule { }
