import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { IT_MANAGEMENT_COMPONENT } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-technician-user-type-registration-view',
  templateUrl: './technician-user-type-registration-view.component.html',
  styleUrls: ['./technician-user-type-registration-view.component.scss']
})
export class TechnicianUserTypeRegistrationViewComponent implements OnInit {

  TechnicianUserTypeId: any = '';
  TechnicianUserTypeDetails;
  selectedIndex: any;
  userData: UserLogin;
  employeeId;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }
  constructor(private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.TechnicianUserTypeId = this.activatedRoute.snapshot.queryParams.id;
    this.selectedIndex = this.activatedRoute.snapshot.queryParams?.tab ?
      this.activatedRoute.snapshot.queryParams?.tab : 0;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getTechnicianUserTypeData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][IT_MANAGEMENT_COMPONENT.TECH_STAFF_REGISTRATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getTechnicianUserTypeData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    // this.loading = true;
    // let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    // TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_DETAILS,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        // otherParams ? otherParams :
        {
          EmployeeId: this.TechnicianUserTypeId
        }
      )

    ).subscribe(data => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.TechnicianUserTypeDetails = data.resources;
        this.employeeId = data.resources.employeeId;
        if (this.TechnicianUserTypeDetails.techAreas != null) {
          let techName = this.TechnicianUserTypeDetails.techAreas.map(x => { return x.newTechAreaName ? x.newTechAreaName : x.techAreaName }).join(',')
          this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.boundaryName = techName

          if (this.TechnicianUserTypeDetails.techAreas.length == 1) {
            this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.stockCollectionWarehouse = this.TechnicianUserTypeDetails.techAreas[0].stockCollectionWarehouse
            this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.stockHoldingWarehouse = this.TechnicianUserTypeDetails.techAreas[0].stockHoldingWarehouse
            this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.stockCollectionLocation = this.TechnicianUserTypeDetails.techAreas[0].stockCollectionLocation
            this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.stockHoldingLocation = this.TechnicianUserTypeDetails.techAreas[0].stockHoldingLocation
            this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.techStockLocation = this.TechnicianUserTypeDetails.techAreas[0].newTechStockLocation ? this.TechnicianUserTypeDetails.techAreas[0].newTechStockLocation : this.TechnicianUserTypeDetails.techAreas[0].techStockLocation

          }
        }
        let skillArray = [];
        this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails == null ? null : (this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.tsL1 == true ? skillArray.push('TSL1') : null);
        this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails == null ? null : (this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.tsL2 == true ? skillArray.push('TSL2') : null);
        this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails == null ? null : (this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.tsL3 == true ? skillArray.push('TSL3') : null);
        this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails == null ? null : (this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.tsL4 == true ? skillArray.push('TSL4') : null);
        this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails == null ? null : (this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.oasl == true ? skillArray.push('OASL') : null);
        this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails == null ? null : (this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.ils == true ? skillArray.push('ISL') : null);
        this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails == null ? null : (this.TechnicianUserTypeDetails.technicianStaffRegistrationTechnicianDetails.skillName = skillArray.join(','));
      } else {
        this.TechnicianUserTypeDetails = {};
      }
    });
  }

  onTabClicked(e) {
    this.selectedIndex = e.index;
    // this.router.navigate(['./'], { relativeTo: this.activatedRoute, queryParams: { id: this.TechnicianUserTypeId, tab: e.index }, skipLocationChange:true})
  }

  navigateToEdit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/user/technicial-staff-registration/add-edit'], {
      queryParams: {
        id: this.TechnicianUserTypeId,
        tab: this.selectedIndex,
        employeeId: this.employeeId,
        action: this.TechnicianUserTypeDetails?.technicianStaffRegistrationTechnicianDetails == null ? 'Create' : 'Update'
      }, skipLocationChange: true
    });
  }

  processIncentive() {
    let dataToSend = {
      employeeId: this.TechnicianUserTypeId, // '171d3db6-cd44-4cce-9176-c09c13ced257'
      createdUserId: this.userData.userId
    };
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICIAN_STAFF_REGISTRATION_INCENTIVE_PROCESS,
      dataToSend).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
        }
      });
  }
}
