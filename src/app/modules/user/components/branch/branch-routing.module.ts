import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BranchListComponent } from './add-edit-list/branch-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: BranchListComponent,canActivate:[AuthGuard], data: { title: 'Branch' } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class BranchRoutingModule { }
