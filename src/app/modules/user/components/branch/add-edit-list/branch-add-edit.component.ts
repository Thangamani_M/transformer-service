import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BranchModel, loggedInUserData, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-branch-add-edit',
  templateUrl: './branch-add-edit.component.html',
})

export class BranchAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  branchAddEditForm: FormGroup;
  isAStringOnlyNoSpace = new CustomDirectiveConfig({ isAStringOnlyNoSpace: true });
  alphaConfig = new CustomDirectiveConfig({ isAStringWithHyphenDash: true });
  loggedUser: UserLogin;
  regionList=[];
  districtList=[];
  divisionDropDown=[];

  constructor(@Inject(MAT_DIALOG_DATA) public branchModel: BranchModel,
    private dialog: MatDialog,
    private crudService: CrudService,
    public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createBranchForm();
    this.getRegions();
    this.onFormControlChanges();
    this.getDetailsById();
  }

  changeRegion() {
    this.branchAddEditForm.controls.divisionId.setValue('');
    this.branchAddEditForm.controls.districtId.setValue('');
    this.divisionDropDown = [];
    this.districtList = [];
  }

  changeDivision() {
    this.branchAddEditForm.controls.districtId.setValue('');
    this.districtList = [];
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.DISTRICT_BRANCH, this.branchModel['branchId'], false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.branchAddEditForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRegions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null,
        { CountryId: formConfigs.countryId })).subscribe((response) => {
          if (response.statusCode == 200 && response.isSuccess && response.resources) {
            this.regionList = response.resources;
          }
          let found = this.regionList.filter(e => e.regionId == this.branchModel.regionId)
          if (found.length == 0) {
            this.branchAddEditForm.controls.regionId.setValue('');
          }
        });
  }

  onFormControlChanges(): void {
    this.branchAddEditForm.get('regionId').valueChanges.subscribe((regionId: string) => {
      if (!regionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { regionId: regionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.divisionDropDown = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.branchAddEditForm.get('divisionId').valueChanges.subscribe((divisionId: string) => {
      if (!divisionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISION_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionId: divisionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources && response.isSuccess) {
            this.districtList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    });
  }

  createBranchForm(): void {
    let branchModelFormModel = new BranchModel();
    this.branchAddEditForm = this.formBuilder.group({});
    Object.keys(branchModelFormModel).forEach((key) => {
      this.branchAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(branchModelFormModel[key]));
    });
    this.branchAddEditForm = setRequiredValidator(this.branchAddEditForm, ["branchName", "branchCode", "divisionId", "districtId", "regionId"]);
  }

  onSubmit(): void {
    if (this.branchAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.branchAddEditForm.value.userId = this.loggedUser.userId;
    this.branchAddEditForm.value.branchId = (this.branchAddEditForm.value && this.branchAddEditForm.value.branchId) ? this.branchAddEditForm.value.branchId : null;
    this.branchAddEditForm.value.isActive = true;
    let crudService: Observable<IApplicationResponse> = !this.branchAddEditForm.value.branchId ? this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.DISTRICT_BRANCH, this.branchAddEditForm.value) :
      this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.DISTRICT_BRANCH, this.branchAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dialog.closeAll();
        this.branchAddEditForm.reset();
      }
    });
  }

  dialogClose() {
    this.dialogRef.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
