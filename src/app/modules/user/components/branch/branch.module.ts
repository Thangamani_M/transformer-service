import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { BranchAddEditComponent } from './add-edit-list/branch-add-edit.component';
import { BranchListComponent } from './add-edit-list/branch-list.component';
import { BranchRoutingModule } from './branch-routing.module';

@NgModule({
  declarations: [BranchListComponent, BranchAddEditComponent],
  imports: [
    CommonModule,
    BranchRoutingModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents: [BranchAddEditComponent]
})
export class BranchModule { }
