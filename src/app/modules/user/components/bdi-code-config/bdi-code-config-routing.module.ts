import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BdiCodeConfigListComponent } from './add-edit-list/bdi-code-config-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: BdiCodeConfigListComponent,canActivate:[AuthGuard], data: { title: 'BDI Code Config' } }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class BdiCodeConfigRoutingModule { }
