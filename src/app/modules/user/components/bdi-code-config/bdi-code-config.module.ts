import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { BdiCodeConfigAddEditComponent, BdiCodeConfigListComponent, BdiCodeConfigRoutingModule } from '@user/components/bdi-code-config';

@NgModule({
  declarations: [BdiCodeConfigAddEditComponent, BdiCodeConfigListComponent],
  imports: [
    CommonModule,
    BdiCodeConfigRoutingModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
  ],
  entryComponents: [BdiCodeConfigAddEditComponent]
})
export class BdiCodeConfigModule { }
