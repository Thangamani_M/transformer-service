import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BdiCodeModel, loggedInUserData, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-bdi-code-config-add-edit',
  templateUrl: './bdi-code-config-add-edit.component.html',
})

export class BdiCodeConfigAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  alphaConfig = new CustomDirectiveConfig({ isAStringWithHyphenDash: true });
  @Output() outputData = new EventEmitter<any>();
  bdiCodeAddEditForm: FormGroup;
  loggedUser: UserLogin;
  divisionList: any;
  divisionListWithName: any;
  constructor(
  public config: DynamicDialogConfig,
  public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createbdiCodeForm();

    this.getDivisons();
    if(this.config.data?.divisionId){
      this.getDetails()
    }
  }

  getDivisons() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS,
      prepareGetRequestHttpParams(null, null,
        {})).subscribe((response) => {
          if (response.statusCode == 200 && response.resources && response.isSuccess) {
            this.divisionList = response.resources;
          }
        });
  }

  getDivisonDetails() {
    let divisionCode = this.bdiCodeAddEditForm.get('divisionCode').value
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.DIVISIONS, divisionCode).subscribe((response) => {
      if (response.statusCode == 200 && response.resources && response.isSuccess) {
        this.divisionListWithName = response.resources;

        this.bdiCodeAddEditForm.get('divisionName').setValue(response.resources?.divisionName)
        this.bdiCodeAddEditForm.get('divisionId').setValue(response.resources?.divisionId)
      }
    });
  }


  getDetails(){
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.PAY_AT_DIVISION, this.config?.data?.divisionId).subscribe((response) => {
      if (response.statusCode == 200 && response.resources && response.isSuccess) {
        response.resources.divisionCode =response.resources.divisionId
        response.resources.isUpdate =true
        this.bdiCodeAddEditForm.patchValue(response.resources);
        this.getDivisonDetails()
      }
    })
  }

  createbdiCodeForm(): void {
    let bdiCodeFormModel = new BdiCodeModel(this.config.data);
    this.bdiCodeAddEditForm = this.formBuilder.group({});
    Object.keys(bdiCodeFormModel).forEach((key) => {
      this.bdiCodeAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(bdiCodeFormModel[key]));
    });
    this.bdiCodeAddEditForm = setRequiredValidator(this.bdiCodeAddEditForm, ["divisionRefNo", "divisionId", "divisionCode", "divisionName"]);
  }

  onSubmit(): void {
    if (this.bdiCodeAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.PAY_AT_DIVISION, this.bdiCodeAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ref.close(true)
        this.outputData.emit(true)
        this.bdiCodeAddEditForm.reset();

      }
    })
  }

  dialogClose(): void {
    this.ref.close(false)
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
