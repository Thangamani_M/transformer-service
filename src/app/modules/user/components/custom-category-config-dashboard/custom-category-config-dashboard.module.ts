import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomCategoryConfigDashboardRoutingModule } from './custom-category-config-dashboard-routing.module';
import { CustomCategoryConfigDashboardComponent } from './custom-category-config-dashboard.component';



@NgModule({
  declarations: [CustomCategoryConfigDashboardComponent],
  imports: [
    CommonModule,
    CustomCategoryConfigDashboardRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule
  ]
})
export class CustomCategoryConfigDashboardModule { }
