import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { IT_MANAGEMENT_COMPONENT } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-custom-category-config-dashboard',
  templateUrl: './custom-category-config-dashboard.component.html'
})
export class CustomCategoryConfigDashboardComponent extends PrimeNgTableVariablesModel implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  primengTableConfigProperties: any;

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
     private snackbarService: SnackbarService,
    public dialogService: DialogService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService,) {
    super()

    this.primengTableConfigProperties = {
      tableCaption: "Custom Category",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'IT Management', relativeRouterUrl: '' }, { displayName: 'Custom Category', relativeRouterUrl: '' }, { displayName: 'Group Configuration' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Group Configuration',
            dataKey: 'customCategoryGroupId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            enableAction: true,
            enableStatusActiveAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            shouldShowAddActionBtn: true,
            enableRowDelete: false,
            enableAddActionBtn: true,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'customCategoryGroupName', header: 'Group Name', width: '200px' }, { field: 'description', header: 'Group Description', width: '200px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' ,isDate:true}, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_GROUP,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            disabled: true
          },
          {
            caption: 'List Configuration',
            dataKey: 'customCategoryConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            shouldShowAddActionBtn: true,
            checkBox: false,
            enableRowDelete: false,
            enableAction: true,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'customCategoryListName', header: 'Categroy List Name', width: '200px' }, { field: 'description', header: 'Description', width: '200px' }, { field: 'customCategoryListItems', header: 'Custom Category List Items', width: '230px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px' ,isDate:true}, { field: 'isActive', header: 'Status', width: '150px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_LIST,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'Custom Category Configuration',
            dataKey: 'customCategoryConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableAction: true,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'customCategoryConfigName', header: 'Categroy Name', width: '200px' }, { field: 'description', header: 'Description', width: '200px' }, { field: 'customModuleName', header: 'Linked To', width: '200px' }, { field: 'defaultValue', header: 'Default Value', width: '150px' }, { field: 'customFieldTypeName', header: 'Field Type', width: '150px' }, { field: 'isMasked', header: 'Masked', width: '150px' }, { field: 'listItemName', header: 'List Value', width: '150px' }, { field: 'customCategoryGroupName', header: 'Group', width: '150px' }, { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px',isDate:true }, { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_CONFIG,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            disabled: true
          },
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.CUSTOM_CATEGORY]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        data.resources.forEach(element => {
          if (this.selectedTabIndex == 2) {
            if (element.customCategoryConfigTypeId == '2') {
              element.defaultValue = element.listItemName
            }
            if (element.customFieldTypeName == 'Date') {
              element.defaultValue = this.momentService.toFormateType(new Date(element.defaultValue), element.customFieldFormat)
            } else if (element.customFieldTypeName == 'Time') {
              element.defaultValue = this.momentService.convertRailayToNormalTime(new Date(element.defaultValue))
            } else if (element.customFieldTypeName == 'DateTime') {
              element.defaultValue = this.momentService.toFormateType(new Date(element.defaultValue), element.customFieldFormat)
            }
          }
        });

        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = []
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], searchObj)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
        break;
      default:
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("user/custom-category-config-dashboard/custom-category-group-config/add-edit");
            break;
          case 1:
            this.router.navigateByUrl("user/custom-category-config-dashboard/custom-category-config-list/add-edit");
            break;
          case 2:
            this.router.navigateByUrl("user/custom-category-config-dashboard/custom-category-config/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["user/custom-category-config-dashboard/custom-category-group-config/view"], { queryParams: { id: editableObject['customCategoryGroupId'] } });
            break;
          case 1:
            this.router.navigate(["user/custom-category-config-dashboard/custom-category-config-list/view"], { queryParams: { id: editableObject['customCategoryListId'] } });
            break;
          case 2:
            this.router.navigate(["user/custom-category-config-dashboard/custom-category-config/view"], { queryParams: { id: editableObject['customCategoryConfigId'] } });
            break;
        }
    }
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/user/custom-category-config-dashboard'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }
}

