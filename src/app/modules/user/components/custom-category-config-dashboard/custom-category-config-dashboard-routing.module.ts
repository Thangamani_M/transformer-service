import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomCategoryConfigDashboardComponent } from './custom-category-config-dashboard.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', redirectTo: 'list', canActivate: [AuthGuard], pathMatch: 'full' },
  {
    path: 'list', component: CustomCategoryConfigDashboardComponent, canActivate: [AuthGuard], data: { title: 'Custom Category Config dashboard' }
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class CustomCategoryConfigDashboardRoutingModule { }
