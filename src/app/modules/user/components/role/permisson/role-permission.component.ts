import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AppState } from '@app/reducers';
import { LoggedInUserModel, ModulesBasedApiSuffix } from '@app/shared';
import { AlertService, CrudService, RxjsService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { loggedInUserData } from '@modules/others';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { Modulenavigationfeature } from '../../../models/modulenavigationfeature/modulenavigationfeature.model';

@Component({
  selector: 'app-role-permission',
  templateUrl: './role-permission.component.html',
  styleUrls: ['./role-permission.component.css'],
  providers: [
  ]
})
export class RolePermissionComponent implements OnInit, AfterViewInit {
  data: any;
  selectedObj: any;
  applicationResponse: IApplicationResponse;
  modulenavigationFeature: Modulenavigationfeature[];
  errorMessage: string;
  RoleList: any;
  ddlFilter: any = '';
  availableModules: any;
  assignedModules: any;
  isSearched: boolean = false;//this flag is to identify whether any search has performed on assigned module
  filterdData = ''; // to store assigned module before performing search on it
  assignedModSearchText: string = '';
  loggedInUserData: LoggedInUserModel

  constructor(
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>
    ) {

    this.data = {};
    this.data.isAllSelected = false;
    this.data.isAllCollapsed = false;

    this.selectedObj = {};
    this.selectedObj.isAllSelected = false;
    this.selectedObj.isAllCollapsed = false;
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.loadRoleDropdown();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      });
  }
  ngAfterViewInit(): void {

  }

  loadRoleDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_ROLES).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
          this.applicationResponse = response,
            this.RoleList = this.applicationResponse.resources;
          this.rxjsService.setGlobalLoaderProperty(false);

        }
      });
  }

  save(ModuleNavigationFeature): void {
    if (this.assignedModSearchText != '') {
      this.assignedModSearchText = '';
      this.filterByValue('partial', this.assignedModSearchText);
    }
    if (ModuleNavigationFeature.modules.length > 0) {

      let roleId = this.ddlFilter;
      ModuleNavigationFeature.roleId = roleId;
      ModuleNavigationFeature.createdUserId = this.loggedInUserData?.userId
      this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.MODULES_NAVIGATION_ROLES, ModuleNavigationFeature).subscribe(response => {
        if(response.isSuccess && response.statusCode == 200){
          this.onSaveComplete(response);
        }
      })
    }


  }

  loadModules() {
    // List object having hierarchy of parents and its children
    var modulenavigationFeature = new Modulenavigationfeature();
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.CLONED_ROLE_ACCESS, this.ddlFilter).subscribe(response => {
        if(response.isSuccess && response.statusCode == 200){
          this.applicationResponse = response,
            modulenavigationFeature = this.applicationResponse.resources;
          this.availableModules = JSON.stringify(this.applicationResponse.resources.modules);
          this.data = this.applicationResponse.resources;
          this.isSearched = false;
          this.movedataright(this.data);
          this.rxjsService.setGlobalLoaderProperty(false);

        }
      })
  }




  checkAssignedChild(parentObj, childObj, curentObj) {
    this.childCheck(parentObj, childObj, curentObj);
  }

  checkAssginedParent(parentObj) {
    this.parentCheck(parentObj);
  }


  //Click event on parent checkbox
  parentCheck(parentObj) {

    //Navigation
    for (var i = 0; i < parentObj.navigations.length; i++) {
      parentObj.navigations[i].isSelected = parentObj.isSelected;
      if (parentObj.navigations[i].features == null) {

        //if sub navigation
        for (var k = 0; k < parentObj.navigations[i].navigations.length; k++) {

          parentObj.navigations[i].navigations[k].isSelected = parentObj.isSelected;

          for (var ii = 0; ii < parentObj.navigations[i].navigations[k].features.length; ii++) {
            parentObj.navigations[i].navigations[k].features[ii].isSelected = parentObj.isSelected;
          }
        }
      } else {
        //if feature exist
        for (var k = 0; k < parentObj.navigations[i].features.length; k++) {
          parentObj.navigations[i].features[k].isSelected = parentObj.isSelected;
        }
      }


    }

  }

  // Click event on child checkbox
  childCheck(parentObj, childObj, currentObj) {

    var isSelected = false;
    if (currentObj.features == null) {
      //if sub navigation
      for (var k = 0; k < currentObj.navigations.length; k++) {
        currentObj.navigations[k].isSelected = currentObj.isSelected;
        for (var ii = 0; ii < currentObj.navigations[k].features.length; ii++) {
          currentObj.navigations[k].features[ii].isSelected = currentObj.isSelected;
        }
      }
    } else {
      //if feature exist
      for (var k = 0; k < currentObj.features.length; k++) {
        currentObj.features[k].isSelected = currentObj.isSelected;
      }
    }

    parentObj.isSelected = this.validateAnyOneSelected(parentObj);

  }
  validateAnyOneSelected(parentObj) {
    for (var i = 0; i < parentObj.navigations.length; i++) {

      if (parentObj.navigations[i].isSelected === true) {
        return true;
      }

    }
  }

  subchildCheck(parentObj, childObj, subchildListObj, currentObj?) {
    if (currentObj) {

      for (let i = 0; i < subchildListObj.length; i++) {
        if (subchildListObj.features[i].isSelected === true) {
          this.subNavCheck(parentObj, subchildListObj, null)
        }
      }
      subchildListObj.isSelected = this.validateSelected(subchildListObj.features, currentObj.isSelected);
      childObj.isSelected = this.validateSelected(childObj.navigations, currentObj.isSelected);
      parentObj.isSelected = this.validateSelected(parentObj.navigations, currentObj.isSelected);


    } else {
      childObj.isSelected = this.validateSelected(childObj.features, subchildListObj.isSelected);
      parentObj.isSelected = this.validateSelected(parentObj.navigations, subchildListObj.isSelected);

    }

  }

  validateSelected(parentObj, isSeleted) {
    for (var i = 0; i < parentObj.length; i++) {

      if (parentObj[i].isSelected === true) {
        return true;
      }

    }
    return isSeleted;
  }

  subNavCheck(module, parentNav, navigation) {
    var isCheckedAnyOne = false;

    for (let i = 0; i < parentNav.navigations.length; i++) {

      parentNav.isSelected = this.validateSelected(parentNav.navigations, navigation.isSelected);
      module.isSelected = this.validateSelected(module.navigations, navigation.isSelected);

    }

    if (navigation.isSelected == true) {
      navigation.isSelected = true;

      this.selectOrUnSelectCheckbox(true, navigation);

    } else {

      navigation.isSelected = false;
      this.selectOrUnSelectCheckbox(false, navigation);
    }


  }
  selectOrUnSelectCheckbox(isChecked, navigation) {
    for (let i = 0; i < navigation.features.length; i++) {
      navigation.features[i].isSelected = isChecked;
    }
  }



  // Click event on master select
  selectUnselectAll(obj) {
    obj.isAllSelected = !obj.isAllSelected;
    for (var i = 0; i < obj.ParentChildchecklist.length; i++) {
      obj.ParentChildchecklist[i].isSelected = obj.isAllSelected;
      for (var j = 0; j < obj.ParentChildchecklist[i].childList.length; j++) {
        obj.ParentChildchecklist[i].childList[j].isSelected = obj.isAllSelected;
      }
    }
  }

  // Expand/Collapse event on each parent
  expandCollapse(obj) {
    obj.isClosed = !obj.isClosed;
  }

  // Master expand/ collapse event
  expandCollapseAll(obj) {
    for (var i = 0; i < obj.ParentChildchecklist.length; i++) {
      obj.ParentChildchecklist[i].isClosed = !obj.isAllCollapsed;
    }
    obj.isAllCollapsed = !obj.isAllCollapsed;
  }

  // Just to show updated JSON object on view
  stringify(obj) {
    return JSON.stringify(obj);
  }

  movedataright(obj) {
    const newArray = JSON.parse(JSON.stringify(this.data));
    let temp;
    const tempArray = [];
    for (let i = 0; i < newArray.modules.length; i++) {
      if (newArray.modules[i].isSelected === true) {
        temp = newArray.modules[i];
        //subchildList

        temp.navigations = temp.navigations.filter(child => {
          if (child.isSelected === true) {
            return true;
          } else {
            return false;
          }
        });
        if (temp.navigations.length > 0) {
          for (let j = 0; j < temp.navigations.length; j++) {
            if (temp.navigations[j].isSelected === true && temp.navigations[j].features != null) {
              temp.navigations[j].features = temp.navigations[j].features.filter(subChild => {
                if (subChild.isSelected === true) {
                  return true;
                } else {
                  return false;
                }
              });
            } else {

              for (let v = 0; v < temp.navigations[j].navigations.length; v++) {
                temp.navigations[j].navigations = temp.navigations[j].navigations.filter(subChild => {
                  if (subChild.isSelected === true) {
                    return true;
                  } else {
                    return false;
                  }
                });

              }
            }
          }
        }

        tempArray.push(temp);
      }
    }

    newArray.modules = [];
    newArray.modules = tempArray;

    this.selectedObj = newArray;
    if (this.isSearched)
      this.filterdData = JSON.stringify(this.selectedObj.modules);
  }

  movedataleft(obj) {
    let temp;
    const tempArray = [];
    for (let i = 0; i < this.selectedObj.modules.length; i++) {
      if (this.selectedObj.modules[i].isSelected === true) {
        temp = this.selectedObj.modules[i];
        // tslint:disable-next-line:only-arrow-functions
        temp.navigations = temp.navigations.filter(child => {
          if (child.isSelected === true) {
            return true;
          } else {
            return false;
          }
        })
        if (temp.navigations.length > 0) {
          for (let j = 0; j < temp.navigations.length; j++) {
            if (temp.navigations[j].isSelected === true && temp.navigations[j].features != null) {
              temp.navigations[j].features = temp.navigations[j].features.filter(subChild => {
                if (subChild.isSelected === true) {
                  return true;
                } else {
                  return false;
                }
              });
            } else {
              for (let v = 0; v < temp.navigations[j].navigations.length; v++) {
                temp.navigations[j].navigations = temp.navigations[j].navigations.filter(subChild => {
                  if (subChild.isSelected === true) {
                    return true;
                  } else {
                    return false;
                  }
                });
              }
            }

          }
        }
        tempArray.push(temp);
      }
    }
    this.selectedObj.modules = [];
    this.selectedObj.modules = tempArray;
    if (this.isSearched)
      this.filterdData = JSON.stringify(this.selectedObj.modules);
  }

  onSaveComplete(response): void {
    if (response.statusCode == 200) {
      this.loadModules();
    } else {
      this.rxjsService.setGlobalLoaderProperty(false);
    }

  }


  filterByValue(searchType, searchText) {
    let data = [];

    if (searchType == 'full') {

      let fullData = JSON.parse(this.availableModules);
      data = fullData;

      if (searchText == '') {
        this.data.modules = fullData;
        return;
      }
    }
    else {
      if (!this.isSearched) {
        this.filterdData = JSON.stringify(this.selectedObj.modules);
        this.isSearched = true;
      }
      if (searchText == '') {
        this.selectedObj.modules = JSON.parse(this.filterdData);
        return;
      }
      data = JSON.parse(this.filterdData);
    }

    let result = [];
    data.forEach(function (module) {
      let mod = module;
      let isModExist = false;
      if (module.moduleName.toLowerCase().includes(searchText.toLowerCase())) {
        isModExist = true;
      }
      else {
        let navigations = module.navigations;
        module.navigations = [];
        navigations.forEach(function (navigation) {
          let isNavExist = false;
          if (navigation.navigationName.toLowerCase().includes(searchText.toLowerCase())) {
            isNavExist = true;
            isModExist = true;
          }
          else if (navigation.features) {
            let features = navigation.features;
            navigation.features = [];
            let isFeatExist = false;
            features.forEach(function (feature) {
              if (feature.featureName.toLowerCase().includes(searchText.toLowerCase())) {
                navigation.features.push(feature);
                isFeatExist = true;
                isNavExist = true;
                isModExist = true;
              }
            });//features
          }
          else if (navigation.navigations) {
            let subNavigations = navigation.navigations;
            navigation.navigations = [];
            let isSNavExist = false;
            subNavigations.forEach(function (subNav) {
              if (subNav.navigationName.toLowerCase().includes(searchText.toLowerCase())) {
                navigation.navigations.push(subNav);
                isSNavExist = true;
                isNavExist = true;
                isModExist = true;
              }
              else if (subNav.features) {
                let snfeatures = subNav.features;
                subNav.features = [];
                let SNFeatExist = false;
                snfeatures.forEach(function (sfeature) {
                  if (sfeature.featureName.toLowerCase().includes(searchText.toLowerCase())) {
                    subNav.features.push(sfeature);
                    SNFeatExist = true;
                    isSNavExist = true;
                    isNavExist = true;
                    isModExist = true;
                  }
                });//features
                if (SNFeatExist)
                  navigation.navigations.push(subNav);
              }
            });//subnavigations
          }
          if (isNavExist)
            module.navigations.push(navigation);
        });//navigations
      }
      if (isModExist)
        result.push(module);
    });//module
    if (searchType == 'full')
      this.data.modules = result;
    else
      this.selectedObj.modules = result;
  }


}
