import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes ,currentComponentPageBasedPermissionsSelector$} from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { debounceTimeForDeepNestedObjectSearchkeyword, LoggedInUserModel, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, prepareRequiredHttpParams } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { IT_MANAGEMENT_COMPONENT, UserModuleApiSuffixModels } from '@modules/user/shared/utils';
import { Store } from '@ngrx/store';
import { combineLatest, Subject } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
interface RoleData {
  id: string;
  displayName: string;
  level: number;
  parentRoleId: string;
  child?: RoleData[];
}
@Component({
  selector: 'app-role-view',
  templateUrl: './role-view.component.html',
  styleUrls: ['../../../../user/components/menu-configuration/menu-configuration.component.scss']
})
export class RoleViewComponent implements OnInit,OnChanges {
  @Input() caption: string;
  @Input() departmentId: string;
  @Input() roleId: string;
  @Input() isEmployee:boolean=false;
  @Output() emittedEvent = new EventEmitter<any>();
  rolesAndPermissions = [];
  searchText$ = new Subject();
  transformer = (node: RoleData, level: number) => {
    return {
      expandable: !!node.child && node.child.length > 0,
      displayName: node.displayName,
      level: level,
      child: node.child,
      parentRoleId: node.parentRoleId,
      id: node.id
    };
  };
  flatTreeControl = new FlatTreeControl<any>(
    (node) => node.level,
    (node) => node.expandable
  );
  treeFlattener = new MatTreeFlattener(
    this.transformer,
    (node) => node.level,
    (node) => node.expandable,
    (node) => node.child,
  );
  dataSource = new MatTreeFlatDataSource(this.flatTreeControl, this.treeFlattener);
  expandedNodes = [];
  searchText = '';
  loggedInUserData: LoggedInUserModel;
  filteredSubComponentsPermissions = [];

  constructor(
    private router: Router,
    private httpServices: CrudService,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.roleId && this.caption) {
      this.getDataReportingLine(this.roleId)
    } else if (this.departmentId && this.caption && !this.isEmployee) {
      this.getRolesByDepartment();
    } else if(!this.isEmployee) {//isEmployee - this means it came from employee add/edit screen
      this.getRoles();
    }
    this.searchText$.pipe(
      debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
      distinctUntilChanged(),
      switchMap(searchText => {
        return of(searchText);
      })).subscribe((searchText: string) => {
        // if (!this.filteredSubComponentsPermissions.find((fS) => fS.menuName == PermissionTypes.FILTER)) {
        //   this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        //  this.searchText='';
        // }
        // else if (this.filteredSubComponentsPermissions.find((fS) => fS.menuName == PermissionTypes.FILTER)) {
          if (this.caption == "Employee Creation") {
            this.rxjsService.setPopupLoaderProperty(true);
          }
          else {
            this.rxjsService.setGlobalLoaderProperty(true);
          }
          this.applyFilter();
        // }
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        if (response[1][IT_MANAGEMENT_COMPONENT.ROLES_AND_PERMISSIONS]) {
          this.filteredSubComponentsPermissions = response[1][IT_MANAGEMENT_COMPONENT.ROLES_AND_PERMISSIONS];
        }
      });
  }
  ngOnChanges(changes: SimpleChanges){
    //isEmployee - this means it came from employee add/edit screen
    if(changes &&  changes.departmentId && changes.departmentId.currentValue && this.isEmployee){
      this.departmentId = changes.departmentId.currentValue;
      this.getRolesByDepartments();
    }
  }
  applyFilter() {
    this.filterTree();
    // if (this.searchText && this.dataSource.data.length < 3) {
    //   this.flatTreeControl.expandAll();
    // } else if (this.searchText && this.dataSource.data.length > 3) {
    //   this.flatTreeControl.expand(this.flatTreeControl.dataNodes[0]);
    //   this.flatTreeControl.expandDescendants(this.flatTreeControl.dataNodes[0]);
    // }
    this.dataSource.data = [...this.dataSource.data];
    if (this.searchText) {
      this.flatTreeControl.expandAll();
    }
    else {
      this.flatTreeControl.collapseAll();
    }
  }

  filterTree() {
    this.dataSource.data = this.filterRecursive(
      this.rolesAndPermissions,
      'displayName'
    );
    setTimeout(() => {
      if (this.caption == "Employee Creation") {
        this.rxjsService.setPopupLoaderProperty(false);
      }
      else {
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    }, 2000);
  }

  filterRecursive(array: any[], property: string) {
    let filteredData;
    if (this.searchText) {
      let filterText = this.searchText.toLowerCase();
      filteredData = array.map((copy) => Object.assign({}, copy)).filter(function x(y, index) {
        if (y[property].toLowerCase().includes(filterText)) {
          return true;
        }
        if (y.child) {
          return (y.child = y.child.map((copy) => Object.assign({}, copy)).filter(x)).length;
        }
      });
    } else {
      filteredData = array;
    }
    return filteredData;
  }

  getRolesByDepartment() {
    this.httpServices.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.ROLE_BY_DEPT,
      this.departmentId,
      false, prepareGetRequestHttpParams()
    ).subscribe(resp => {
      if (resp.statusCode == 200 && resp.isSuccess) {
        this.rolesAndPermissions = resp.resources.roleKeyValue;
        this.dataSource.data = this.rolesAndPermissions;
        this.applyFilter();
      }
      if (!this.rolesAndPermissions) {
        this.snackbarService.openSnackbar(
          "There are no active roles mapped with the selected department",
          ResponseMessageTypes.WARNING
        );
      }
      setTimeout(() => {
        this.rxjsService.setPopupLoaderProperty(false);
      }, 500);
    });
  }

  getDataReportingLine(roleId) {
    this.httpServices.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.GET_DATA_BY_ROLE_ID, null, true, prepareRequiredHttpParams({
      roleId
    })).subscribe(resp => {
      if (resp.statusCode == 200 && resp.isSuccess) {
        this.rolesAndPermissions = resp.resources;
        this.dataSource.data = this.rolesAndPermissions;
        this.applyFilter();
      }
      setTimeout(() => {
        this.rxjsService.setPopupLoaderProperty(false);
      }, 500);
    });
  }

  hasChild(_: number, node: RoleData) {
    return node.child != null && node.child.length > 0;
  }

  getRoles(searchText?: string) {
    this.httpServices.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.ROLES,
      undefined,
      false, prepareGetRequestHttpParams()
    ).subscribe(resp => {
      if (resp.isSuccess && resp.statusCode == 200) {
        this.rolesAndPermissions = resp.resources;
        this.dataSource.data = this.rolesAndPermissions;
        this.applyFilter();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  getRolesByDepartments(searchText?: string) {
    if(this.departmentId){
      this.httpServices.get(
        ModulesBasedApiSuffix.IT_MANAGEMENT,
        SalesModuleApiSuffixModels.UX_ROLES_BY_DEPARMENTS,
        undefined,
        false, prepareRequiredHttpParams({departmentId:this.departmentId})
      ).subscribe(resp => {
        if (resp.isSuccess && resp.statusCode == 200) {
          this.rolesAndPermissions = resp.resources;
          this.dataSource.data = this.rolesAndPermissions;
          this.applyFilter();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }
  editRole({ parentRoleId, id }) {
    this.router.navigate(['user/roles/add-edit'], { queryParams: { parentroleid: parentRoleId, childroleid: id, action: 'edit' } });

    // if (this.filteredSubComponentsPermissions.find((fS) => fS.menuName == PermissionTypes.EDIT)) {
    //   this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    // else if (this.filteredSubComponentsPermissions.find((fS) => fS.menuName == PermissionTypes.EDIT)) {
      this.router.navigate(['user/roles/add-edit'], { queryParams: { parentroleid: parentRoleId, childroleid: id, action: 'edit' } });
    // }
  }

  onRoleSelection(childNode) {
    this.emittedEvent.emit({ roleId: childNode.id, roleName: childNode.displayName });
  }

  addRole(obj) {
    // if (!this.filteredSubComponentsPermissions.find((fS) => fS.menuName == PermissionTypes.ADD)) {
    //   this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    // else if (this.filteredSubComponentsPermissions.find((fS) => fS.menuName == PermissionTypes.ADD)) {
      this.router.navigate(['user/roles/add-edit'], { queryParams: { parentroleid: obj.id ? obj.id : obj.parentRoleId, action: 'add' } });
    // }
  }
}
