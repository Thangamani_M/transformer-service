import { Component, Inject, OnInit, Input } from '@angular/core'
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { RxjsService, AlertService } from '@app/shared/services';
import { Role } from '@user/models';
import { RoleService } from '@user/services';

@Component({
  selector: 'role-add-edit',
  templateUrl: './role-add.component.html',
  styleUrls: ['./role-add.component.scss']
})

export class RoleAddEditComponent implements OnInit {
  @Input() id: string;
  roleAddEditForm: FormGroup;
  applicationResponse: IApplicationResponse;
  role = new Role();
  errorMessage: string;
  btnName: string;
  isDialogLoaderLoading: boolean;
  RoleCategoryList: any;
  public alphaNumericWithSpacePattern = { '0': { pattern: new RegExp('^[A-Za-z0-9 ]*$') } };

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Role,
    private dialog: MatDialog,
    private roleServies: RoleService,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private rxjsService: RxjsService) {
  }
  //Initialize the components...
  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.data.roleId) {
      this.btnName = 'Update';
    } else {
      this.btnName = 'Create';
    }

    this.roleAddEditForm = this.formBuilder.group({
      roleName: this.data.roleName || '',
      roleCategoryId: this.data.roleCategoryId || '',
      shortCode: this.data.shortCode || '',
      htmlBadge: this.data.htmlBadge || '',
      description: this.data.description || '',
    });

    this.roleServies.getRoleCategories().subscribe({
      next: response => {
        this.RoleCategoryList = response.resources;
      }
    });
  }


  //Save role values...
  save(): void {
    if (this.roleAddEditForm.invalid) {
      this.roleAddEditForm.markAllAsTouched();
      return;
    }

    const role = { ...this.data, ...this.roleAddEditForm.value }

    if (role.roleId != null) {
      this.roleServies.updateRole(role).subscribe({
        next: response => this.onSaveComplete(response),
        error: err => this.errorMessage = err
      });
    }
    else {
      this.roleServies.createRole(role).subscribe({
        next: response => this.onSaveComplete(response),
        error: err => this.errorMessage = err
      });
    }
  }
  //After completing save...
  onSaveComplete(response): void {
    if (response.statusCode == 200) {
      this.dialog.closeAll();
      //this.toastr.responseMessage(response);
      // Reset the form to clear the flags
      this.roleAddEditForm.reset();
    }
    else {
      //Display Alert message
      this.alertService.processAlert(response);
    }

  }
}
