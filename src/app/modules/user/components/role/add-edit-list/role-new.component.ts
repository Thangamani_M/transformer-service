import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, LoggedInUserModel, MenuCategoriesForPermissions, ModulesBasedApiSuffix, PermissionTypes, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { RxjsService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { loggedInUserData } from '@modules/others';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { Guid } from 'guid-typescript';
import { combineLatest, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
@Component({
  selector: 'app-role-list',
  templateUrl: './role-new.component.html',
  styleUrls: ['./role-new.component.scss']
})

export class RolesComponent implements OnInit {
  stringConfig = new CustomDirectiveConfig({ isAlphaSomeSpecialCharterOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  ddlFilter: any = '';
  filterdData = ''; // to store assigned module before performing search on it
  roleName: any;
  divParent: boolean = true;
  divlblParent: boolean = true;
  parentRoleId = "00000000-0000-0000-0000-000000000000";
  parentRoleName: string;
  childRoleId: any;
  childRoleName: string;

  // Clone Role
  isCloneRole: Boolean = false
  cloneRoleId = null;
  cloneableRoles = []
  loggedInUserData: LoggedInUserModel;
  concatenatedMenuIds = [];

  //mask validations
  public alphaNumericWithSpacePattern = { '0': { pattern: new RegExp('^[A-Za-z0-9 ]*$') } };
  isAlphaSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true });

  assignedModules = [];
  assignedModulesCopy = [];
  isFormSubmitted = false;
  availableModules = [];
  availableModulesCopy = [];
  componentAction;
  filterResultForAvailableModules = [];
  filterResultForAssignedModules = [];
  availableModulesSearchText$ = new Subject();
  assignedModulesSearchText$ = new Subject();
  availableModulesSearch = '';
  assignedModulesSearch = '';

  constructor(
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute,
    private router: Router, private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService
) {
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.availableModulesSearchText$.pipe(
      debounceTime(1000),
      distinctUntilChanged(),
      switchMap(searchText => {
        return of(searchText);
      })
    ).subscribe((searchText: string) => {
      this.rxjsService.setGlobalLoaderProperty(true);
      let filterableModules = this.deepCloneRecursiveNestedObjects(this.availableModulesCopy);
      if (searchText == '') {
        this.compareAndUpdateObjects(this.filterResultForAvailableModules, filterableModules);
        if (this.availableModules.length > 0) {
          this.compareAndUpdateObjects(this.availableModules, filterableModules);
        }
        this.availableModules = filterableModules;
        this.filterResultForAvailableModules = [];
      }
      else {
        this.compareAndUpdateObjects(this.availableModules, filterableModules);
        this.filterResultForAvailableModules = filterableModules.filter(function f(o) {
          return o.menuName.toLowerCase().includes(searchText.toLowerCase()) ||
            o.subMenu && (o.subMenu = o.subMenu.filter(f)).length
        });
        this.availableModules = this.filterResultForAvailableModules;
      }
    });
    this.assignedModulesSearchText$.pipe(
      debounceTime(1000),
      distinctUntilChanged(),
      switchMap(searchText => {
        return of(searchText);
      })
    ).subscribe((searchText: string) => {
      this.rxjsService.setGlobalLoaderProperty(true);
      let filterableModules = this.deepCloneRecursiveNestedObjects(this.assignedModulesCopy);
      if (searchText == '') {
        this.compareAndUpdateObjects(this.filterResultForAssignedModules, filterableModules);
        if (this.assignedModules.length > 0) {
          this.compareAndUpdateObjects(this.assignedModules, filterableModules);
        }
        this.assignedModules = filterableModules;
        this.assignedModules = this.filter(this.assignedModules, ({ isSelected }) => isSelected);
        this.filterResultForAssignedModules = [];
      }
      else {
        this.filterResultForAssignedModules = filterableModules.filter(function f(o) {
          return o.menuName.toLowerCase().includes(searchText.toLowerCase()) ||
            o.subMenu && (o.subMenu = o.subMenu.filter(f)).length
        });
        this.assignedModules = this.filterResultForAssignedModules;
      }
    });
  }

  deepCloneRecursiveNestedObjects(arr) {
    let finalArray = [];
    arr.forEach((obj, i) => {
      if (obj) {
        finalArray.push(this.copy(obj));
        if (obj.subMenu.length > 0) {
          this.deepCloneRecursiveNestedObjects(obj.subMenu);
        }
      }
      if (i == arr.length - 1) {
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
    return finalArray;
  }

  copy(aObject) {
    if (!aObject) {
      return aObject;
    }
    let v;
    let bObject = Array.isArray(aObject) ? [] : {};
    for (const k in aObject) {
      v = aObject[k];
      bObject[k] = (typeof v === "object") ? this.copy(v) : v;
    }
    return bObject;
  }

  onClickedRecursionItem(moduleType: string, clickedItemObj) {
    let menus = moduleType == 'Available Modules' ? this.availableModules : this.assignedModules;
    let uncheckedImmediateParentModule;
    if (clickedItemObj.isSelected) {
      switch (clickedItemObj?.menuCategoryId) {
        case MenuCategoriesForPermissions.MENU:
          // check selected parent's all children hierarchy objects
          if (clickedItemObj.level == 1) {
            this.changeIsSelectedStatusProperty(clickedItemObj?.subMenu, clickedItemObj?.isSelected, clickedItemObj?.menuId);
          }
          //&& (clickedItemObj?.subMenu.find((sM) => sM['menuCategoryId'] == MenuCategoriesForPermissions.FEATURE) || clickedItemObj?.subMenu.find((sM) => sM['menuCategoryId'] == MenuCategoriesForPermissions.TAB))
          else if (clickedItemObj?.subMenu.length > 0) {
            this.changeIsSelectedStatusProperty(clickedItemObj?.subMenu, clickedItemObj?.isSelected, clickedItemObj?.menuId);
          }
          break;
        case MenuCategoriesForPermissions.TAB:
          // check selected parent's all children hierarchy objects
          this.changeIsSelectedStatusProperty(clickedItemObj?.subMenu, clickedItemObj?.isSelected, clickedItemObj?.menuId);
          break;
        case MenuCategoriesForPermissions.FEATURE:
          if (clickedItemObj.subMenu.length > 0) {
            this.changeIsSelectedStatusProperty(clickedItemObj?.subMenu, clickedItemObj?.isSelected, clickedItemObj?.menuId);
          }
          else if (clickedItemObj.menuName !== PermissionTypes.LIST) {
            let uncheckedImmediateParentModule = clickedItemObj;
            for (let i = 0; i < 1; i++) {
              uncheckedImmediateParentModule = this.findItemNested(menus, uncheckedImmediateParentModule?.menuParentId);
              if (i == 0) {
                let listFeature = uncheckedImmediateParentModule?.subMenu?.find((uPM) => uPM['menuName'] === PermissionTypes.LIST);
                if (listFeature) {
                  listFeature.isSelected = true;
                }
              }
              else {
                uncheckedImmediateParentModule.isSelected = true;
              }
            }
          }
          uncheckedImmediateParentModule = clickedItemObj;
          uncheckedImmediateParentModule = this.findItemNested(menus, uncheckedImmediateParentModule?.menuParentId);
          if (uncheckedImmediateParentModule) {
            uncheckedImmediateParentModule.isSelected = true;
          }

          break;
      }
      uncheckedImmediateParentModule = clickedItemObj;
      for (let i = uncheckedImmediateParentModule.level; i > 0; i--) {
        uncheckedImmediateParentModule = this.findItemNested(menus, uncheckedImmediateParentModule?.menuParentId);
        if (uncheckedImmediateParentModule) {
          uncheckedImmediateParentModule.isSelected = true;
        }
      }
    }
    else {
      switch (clickedItemObj?.menuCategoryId) {
        case MenuCategoriesForPermissions.MENU:
          // uncheck selected parent's all children hierarchy objects
          if (clickedItemObj.level == 1) {
            this.changeIsSelectedStatusProperty(clickedItemObj?.subMenu, clickedItemObj?.isSelected, clickedItemObj?.menuId);
          }
          else if (clickedItemObj?.subMenu.length > 0 ) {
          // else if (clickedItemObj?.subMenu.length > 0 && (clickedItemObj?.subMenu.find((sM) => sM['menuCategoryId'] == MenuCategoriesForPermissions.FEATURE) || clickedItemObj?.subMenu.find((sM) => sM['menuCategoryId'] == MenuCategoriesForPermissions.TAB))) {
            this.changeIsSelectedStatusProperty(clickedItemObj?.subMenu, clickedItemObj?.isSelected, clickedItemObj?.menuId);
          }
          break;
        case MenuCategoriesForPermissions.TAB:
          // uncheck selected parent's all children hierarchy objects
          this.changeIsSelectedStatusProperty(clickedItemObj?.subMenu, clickedItemObj?.isSelected, clickedItemObj?.menuId);
          break;
        case MenuCategoriesForPermissions.FEATURE:
          if (clickedItemObj.menuName == PermissionTypes.LIST) {
            let uncheckedImmediateParentModule = clickedItemObj;
            for (let i = 0; i < 1; i++) {
              uncheckedImmediateParentModule = this.findItemNested(menus, uncheckedImmediateParentModule?.menuParentId);
              if (uncheckedImmediateParentModule && i == 0) {
                this.changeIsSelectedStatusProperty(uncheckedImmediateParentModule.subMenu, false, clickedItemObj?.menuId);
                uncheckedImmediateParentModule.isSelected = false;
              }
              else if (uncheckedImmediateParentModule) {
                uncheckedImmediateParentModule.isSelected = false;
              }
            }
          }
          else if (clickedItemObj.subMenu.length == 0) {
            let uncheckedImmediateParentModule = clickedItemObj;
            for (let i = 1; i < 2; i++) {
              uncheckedImmediateParentModule = this.findItemNested(menus, uncheckedImmediateParentModule?.menuParentId);
              if (uncheckedImmediateParentModule && !uncheckedImmediateParentModule.subMenu.find((sM => sM['isSelected']))) {
                uncheckedImmediateParentModule.isSelected = false;
              }
            }
          }
          else if (clickedItemObj.subMenu.length > 0) {
            this.changeIsSelectedStatusProperty(clickedItemObj.subMenu, false, clickedItemObj?.menuId);
          }
          break;

      }
      let loopsCount = 0;
      let uncheckedImmediateParentModule = this.findItemNested(menus, clickedItemObj?.menuParentId);
      if (uncheckedImmediateParentModule) {
        uncheckedImmediateParentModule.isSelected = false;
        if (!uncheckedImmediateParentModule.subMenu.find(sM => sM['isSelected'] == true)) {
          // retrieving up level of tree level from 4 to 3 , 3 to 2, 2 to 1
          for (let i = uncheckedImmediateParentModule.level; i > 0; i--) {
            uncheckedImmediateParentModule = this.findItemNested(menus, uncheckedImmediateParentModule?.menuParentId, 'subMenu');
            if (uncheckedImmediateParentModule) {
              if (uncheckedImmediateParentModule.subMenu.length == 0 || uncheckedImmediateParentModule.subMenu.length == 1) {
                uncheckedImmediateParentModule.isSelected = false;
              }
              if (uncheckedImmediateParentModule.subMenu.length != 0) {
                let _tempCount1 = 0
                uncheckedImmediateParentModule['subMenu'].forEach(items=>{
                  if(items.isSelected){
                    _tempCount1 +=1
                  }
                  if(_tempCount1 == 0){
                    uncheckedImmediateParentModule.isSelected =false
                  }else{
                    uncheckedImmediateParentModule.isSelected =true

                  }
                })
                // uncheckedImmediateParentModule.isSelected = false;
              }
              else if (uncheckedImmediateParentModule.subMenu.length > 0 && loopsCount == 0) {
                uncheckedImmediateParentModule.isSelected = false;
              }
              else if (uncheckedImmediateParentModule.subMenu.length > 0 && loopsCount > 0) {
                // logics to be added further
                if(uncheckedImmediateParentModule.level)
               uncheckedImmediateParentModule = this.findItemNested(menus, uncheckedImmediateParentModule?.menuParentId ? uncheckedImmediateParentModule?.menuParentId :
                  uncheckedImmediateParentModule?.menuId, 'subMenu');

                  if(uncheckedImmediateParentModule){
                    let _tempCount = 0
                    uncheckedImmediateParentModule['subMenu'].forEach(items=>{
                      if(items.isSelected){
                        _tempCount +=1
                      }
                      if(_tempCount == 0){
                        uncheckedImmediateParentModule.isSelected =false
                      }
                    })
                  }
              }
            }
            loopsCount += 1;
          }
        }
        else {
          if (uncheckedImmediateParentModule) {
            uncheckedImmediateParentModule.isSelected = true;
          }
          for (let i = uncheckedImmediateParentModule.level; i > 0; i--) {
            uncheckedImmediateParentModule = this.findItemNested(menus, uncheckedImmediateParentModule?.menuParentId, 'subMenu');
            if (uncheckedImmediateParentModule) {
              uncheckedImmediateParentModule.isSelected = true;
            }
          }
        }
      }
    }
    // else if (clickedItemObj?.menuCategoryId !== MenuCategoriesForPermissions.FEATURE && clickedItemObj.isSelected) {
    //   this.snackbarService.openSnackbar('No feature is configured for this page..!!', ResponseMessageTypes.WARNING);
    // }
    if (moduleType == 'Available Modules' && this.filterResultForAvailableModules.length > 0) {
      this.compareAndUpdateObjects(this.filterResultForAvailableModules, this.availableModulesCopy);
    }
    else if (moduleType == 'Assigned Modules' && this.filterResultForAssignedModules.length > 0) {
      this.compareAndUpdateObjects(this.filterResultForAssignedModules, this.assignedModulesCopy);
    }
  }

  compareAndUpdateObjects(arr1, arr2) {
    arr1.forEach((arr1Obj, i) => {
      let arr2FilteredObj = arr2.find(b => b.menuId == arr1Obj.menuId);
      if (arr2FilteredObj) {
        arr2FilteredObj.isSelected = arr1Obj.isSelected;
        if (arr1Obj.subMenu && arr1Obj.subMenu.length > 0) {
          return this.compareAndUpdateObjects(arr1Obj.subMenu, arr2FilteredObj.subMenu);
        }
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.activatedRoute.queryParams])
      .pipe(take(1)).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        //getting params from url for parentroleid,parentrolename,childroleid and childrolename...
        this.parentRoleId = response[1]['parentroleid'] ? response[1]['parentroleid'] : this.parentRoleId;
        this.getParentRole();
        this.childRoleId = response[1]['childroleid'] ? response[1]['childroleid'] : null;
        this.componentAction = response[1]['action'] ? response[1]['action'] : 'add';
        this.getCloneableRoles();
        if (this.childRoleId) {
          this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.ROLES, this.childRoleId).subscribe((response: IApplicationResponse) => {
            if (response.statusCode == 200 && response.isSuccess) {
              this.childRoleName = response.resources["roleName"];
            }
          })
        }
        this.loadModules(this.parentRoleId, this.childRoleId);
      });
  }

  getCloneableRoles() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.CLONE_ROLE_DROPDOWN, this.childRoleId).
      subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.cloneableRoles = response.resources;
        }
      })
  }

  onCloneableRoleChanged() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.MENU_ROLE_ACCESS, this.cloneRoleId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.availableModules = response.resources.menu;
          this.availableModulesCopy = JSON.parse(JSON.stringify(this.availableModules));
          this.moveDataBetweenAvailableAndAssignedModules('leftToRight');

        }
        this.rxjsService.setGlobalLoaderProperty(false)
      })
  };

  addIsClosedProp = (arr, nestingKey="subMenu") => (
    arr.reduce((a, item) => {
      if (a) return a;
      if (item) {
        item.isClosed = true;
        // if subMenu array is found again loop through the same array ( called as recursive array untill reaches the last object of the infinite array )
        if (item[nestingKey]) {
          return this.addIsClosedProp(item[nestingKey], nestingKey);
        }
        else {
          return item;
        }
      }
    }, null)
  );

  getParentRole() {
    if (this.parentRoleId === "" || this.parentRoleId === null || this.parentRoleId === undefined || this.parentRoleId == "00000000-0000-0000-0000-000000000000") {
      this.divParent = false;
      this.divlblParent = false;
    }
    else {
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.ROLES, this.parentRoleId).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200) {
          this.parentRoleName = response.resources["roleName"];
        }
        this.rxjsService.setGlobalLoaderProperty(false)
      });
    }
  }

  backToRoleView() {
    this.router.navigate(['/user/roles'])
  }

  onSubmit(): void {
    this.isFormSubmitted = true;
    if ((this.componentAction == 'add' && this.isCloneRole && !this.cloneRoleId) || !this.childRoleName) {
      return;
    }
    this.concatenatedMenuIds = [];
    this.getRecursionObjectsProps(this.availableModules, 'menuId', 'subMenu');
    // this.getRecursionObjectsProps(this.assignedModules, 'menuId', 'subMenu');
    if(this.concatenatedMenuIds.length ==0){
      return this.snackbarService.openSnackbar("Assign at least one permission",ResponseMessageTypes.WARNING)
    }
    let payload = {
      roleName: this.childRoleName,
      menuId: this.concatenatedMenuIds,
      isUpdateRoleAssigned: false,
      cloneRoleId: this.cloneRoleId,
      roleId: this.parentRoleId,
      userId: this.loggedInUserData.userId,
    }
    if (this.childRoleId) {
      payload['childRoleId'] = this.childRoleId;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService =
      this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.MENU_ROLE, payload);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.router.navigateByUrl("/user/roles");
      }
    })
  }

  loadModules(parentrolelId, chldrolelId) {
    //passing parentroleid or childroleid to API function...
    if ((this.childRoleId === "" || this.childRoleId === undefined)) {
      this.ddlFilter = Guid.EMPTY;
    } else {
      this.ddlFilter = chldrolelId;
    }
    if (parentrolelId != "" && parentrolelId !== undefined) {
      this.parentRoleId = parentrolelId;
      this.parentRoleName = this.childRoleName;
      this.childRoleName = "";
    }
    this.getModulesByRoleId();
  }

  getModulesByRoleId() {
    if (this.parentRoleId == "00000000-0000-0000-0000-000000000000") {
      this.divParent = false;
      this.divlblParent = false;
    }
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.MENU_ROLE_ACCESS, this.childRoleId ?
      this.childRoleId : this.parentRoleId).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.availableModules = response.resources.menu;
          this.availableModulesCopy = JSON.parse(JSON.stringify(this.availableModules));
          this.isCloneRole = response.resources.cloneRoleId ? true : false;
          this.cloneRoleId = response.resources.cloneRoleId ? response.resources.cloneRoleId : null;
          if (this.componentAction == 'edit') {
            this.moveDataBetweenAvailableAndAssignedModules('leftToRight');
          }
          this.addIsClosedProp(this.availableModules, "subMenu");

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  findItemNestedAndCheckOrUnCheckValues = (arr, replaceablePropName, replaceablePropValue, menuId, nestingKey = 'subMenu') => (
    arr.reduce((a, item) => {
      if (a) {
        a[replaceablePropName] = replaceablePropValue;
        return a;
      };
      if (item.menuId === menuId) {
        item[replaceablePropName] = replaceablePropValue;
        return item;
      };
      if (item[nestingKey])
        return this.findItemNestedAndCheckOrUnCheckValues(item[nestingKey], replaceablePropName, replaceablePropValue, menuId)
    }, null)
  );

  findItemNested = (arr, itemId, nestingKey = 'subMenu') => (
    arr.reduce((a, item) => {
      if (a) return a;
      if (item.menuId === itemId) return item;
      if (item[nestingKey]) return this.findItemNested(item[nestingKey], itemId)
    }, null)
  );

  findIsAtleastOneChildIsChecked = (arr, itemId, nestingKey) => (
    arr.reduce((a, item) => {
      if (a) return a;
      if (item.menuId === itemId) {
        if (item[nestingKey].length > 0) {
          if (item[nestingKey].find(deepNestedItem => deepNestedItem.isSelected == true)) {
            item.isAtleastOneChildIsChecked = true;
          }
          else {
            item.isAtleastOneChildIsChecked = false;
          }
        }
        return item;
      };
      if (item[nestingKey]) return this.findItemNested(item[nestingKey], itemId, nestingKey)
    }, null)
  );

  getRecursionObjectsProps = (arr, retrievablePropertyName, nestingKey) => (
    arr.reduce((a, item) => {
      if (item) {
        if (item.isSelected == true) {
          this.concatenatedMenuIds.push(item[retrievablePropertyName]);
        }
      }
      if (item[nestingKey]) return this.getRecursionObjectsProps(item[nestingKey], retrievablePropertyName, nestingKey)
    }, null)
  );

  prepareCurrentComponentBasedPermissions(parentBasedMenus, parentNavigationId, nestingKey): [] {
    return parentBasedMenus.reduce((parentBasedMenu, item) => {
      if (parentBasedMenu) {
        return parentBasedMenu;
      };
      if (item.menuId === parentNavigationId) return item;
      if (item[nestingKey]) {
        return this.prepareCurrentComponentBasedPermissions(item[nestingKey], parentNavigationId, nestingKey)
      }
    }, null);
  }

  expandOrCollapseAllDescendantNodes(type: string, moduleType: string) {
    let descendantChildren = moduleType == 'available modules' ? this.availableModules : this.assignedModules;
    this.checkOrUnCheckIsClosedProp(descendantChildren, type == 'expand' ? false : true);
    if (moduleType == 'available modules') {
      this.availableModules = Object.assign([], this.availableModules);
    }
    else {
      this.assignedModules = Object.assign([], this.assignedModules);
      this.assignedModulesCopy = Object.assign([], this.assignedModulesCopy);
    }
  }

  findNestedObjectIntoTopLevel(object, key) {
    let resultObject;
    Object.keys(object).reduce((k) => {
      if (object.hasOwnProperty(key)) {
        return this.findNestedObjectIntoTopLevel(object[k], key);
      }
      else if (object) {
        resultObject = object;
        return resultObject;
      }
    });
    return resultObject;
  }

  expandCollapseDescendants(moduleType: string, clickedItem) {
    clickedItem.isClosed = !clickedItem.isClosed;
    this.checkOrUnCheckIsClosedProp([clickedItem], clickedItem.isClosed);
  }

  findByMenuId(data, hideTemporaryProperyMenu, menuId) {
    function iter(a) {
      if (a.menuId === menuId) {
        a.subMenu = [];
        a.subMenu = hideTemporaryProperyMenu.subMenu;
        return true;
      }
      return Array.isArray(a.subMenu) && a.subMenu.some(iter);
    }
    var result;
    data.some(iter);
    return result
  }

  onUnCheckFeatureItem(clickedItemObj, menus) {
    let loopsCount = 0;
    let uncheckedImmediateParentModule = clickedItemObj;
    // retrieving up level of tree level from 4 to 3 , 3 to 2, 2 to 1
    for (let i = uncheckedImmediateParentModule.level; i > 0; i--) {
      uncheckedImmediateParentModule = this.findItemNested(menus, uncheckedImmediateParentModule?.menuParentId, 'subMenu');
      if (uncheckedImmediateParentModule) {
        if (uncheckedImmediateParentModule.subMenu.length == 0 || uncheckedImmediateParentModule.subMenu.length == 1) {
          uncheckedImmediateParentModule.isSelected = false;
        }
        else if (uncheckedImmediateParentModule.subMenu.length > 0 && loopsCount == 0) {
          uncheckedImmediateParentModule.isSelected = false;
        }
        else if (uncheckedImmediateParentModule.subMenu.length > 0 && loopsCount > 0) {
          // logics to be added further
          uncheckedImmediateParentModule = this.findItemNested(uncheckedImmediateParentModule.subMenu, uncheckedImmediateParentModule?.menuParentId ? uncheckedImmediateParentModule?.menuParentId :
            uncheckedImmediateParentModule?.menuId, 'subMenu');
        }
      }
      loopsCount += 1;
    }
  }

  trackByMenuId(index: number, menuObject) {
    return menuObject.menuId + index;
  }

  moveDataBetweenAvailableAndAssignedModules(dataMovingDirection: string) {
    this.rxjsService.setGlobalLoaderProperty(true);
    if (dataMovingDirection == 'leftToRight') {
      if (this.filterResultForAssignedModules.length == 0 && this.filterResultForAvailableModules.length == 0) {
        this.assignedModules = this.filter(this.availableModules, ({ isSelected }) => isSelected);
        this.assignedModulesCopy = this.assignedModules;
      }
      else if (this.filterResultForAssignedModules.length > 0 && this.filterResultForAvailableModules.length > 0) {
        this.compareAndUpdateObjects(this.availableModules, this.assignedModules);
        this.assignedModules = this.filter(this.assignedModules, ({ isSelected }) => isSelected);
      }
      else if (this.filterResultForAssignedModules.length > 0 && this.filterResultForAvailableModules.length == 0) {
        this.compareAndUpdateObjects(this.availableModules, this.assignedModules);
        this.assignedModules = this.filter(this.assignedModules, ({ isSelected }) => isSelected);
      }
      else if (this.filterResultForAssignedModules.length == 0 && this.filterResultForAvailableModules.length > 0) {
        this.compareAndUpdateObjects(this.availableModules, this.assignedModules);
        this.assignedModules = this.filter(this.assignedModules, ({ isSelected }) => isSelected);
      }
    }
    else {
      if (this.filterResultForAssignedModules.length == 0 && this.filterResultForAvailableModules.length == 0) {
        this.compareAndUpdateObjects(this.assignedModules, this.availableModules);
        this.assignedModules = this.filter(this.assignedModules, ({ isSelected }) => isSelected);
      }
      else if (this.filterResultForAssignedModules.length > 0 && this.filterResultForAvailableModules.length > 0) {
        this.compareAndUpdateObjects(this.assignedModules, this.availableModules);
        this.assignedModules = this.filter(this.assignedModules, ({ isSelected }) => isSelected);
      }
      else if (this.filterResultForAssignedModules.length > 0 && this.filterResultForAvailableModules.length == 0) {
        this.compareAndUpdateObjects(this.assignedModules, this.availableModules);
        this.assignedModules = this.filter(this.assignedModules, ({ isSelected }) => isSelected);
      }
      else if (this.filterResultForAssignedModules.length == 0 && this.filterResultForAvailableModules.length > 0) {
        this.compareAndUpdateObjects(this.assignedModules, this.availableModules);
        this.assignedModules = this.filter(this.assignedModules, ({ isSelected }) => isSelected);
      }
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  filter(array, fn) {
    return array.reduce((r, o) => {
      var subMenu = this.filter(o.subMenu || [], fn);
      if (fn(o) || subMenu.length) {
        r.push(Object.assign({}, o, subMenu.length && { subMenu }));
      }
      return r;
    }, []);
  }

  changeIsSelectedStatusProperty = (arr, propValue, menuId, nestingKey = 'subMenu') => (
    arr.reduce((a, item) => {
      if (a) return a;
      if (item) {
        // apply menuparentid as given input menuid to check/uncheck the related object's property
        menuId = item.menuParentId;
        item.isSelected = propValue;
        // if subMenu array is found again loop through the same array ( called as recursive array untill reaches the last object of the infinite array )
        if (item[nestingKey]) {
          return this.changeIsSelectedStatusProperty(item[nestingKey], propValue, menuId);
        }
        else {
          return item;
        }
      }
    }, null)
  );

  addShouldHideTemporaryByFilterProperty(arr, nestingKey = 'subMenu') {
    arr.reduce((a, item) => {
      if (a) return a;
      if (item) {
        item.shouldHideTemporaryByFilter = false;
      };
      if (item[nestingKey]) return this.addShouldHideTemporaryByFilterProperty(item[nestingKey])
    }, null)
  }

  checkOrUnCheckIsClosedProp(arr, isClosedValue, nestingKey = 'subMenu') {
    arr.reduce((a, item, index) => {
      if (a) return a;
      if (item) {
        item.isClosed = isClosedValue;
      };
      if (item[nestingKey]) {
        if(isClosedValue){
          return this.checkOrUnCheckIsClosedProp(item[nestingKey], isClosedValue, nestingKey)
          }
      }
    }, null)
  }
}
