import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTreeModule } from '@angular/material';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { RoleListComponent,RoleAddEditComponent, RolePermissionComponent, RoleRoutingModule, RoleViewComponent } from '@user/components/role';
import {  RolesComponent } from './add-edit-list/role-new.component';
@NgModule({
  declarations: [RoleAddEditComponent ,RoleListComponent, RolePermissionComponent, RoleViewComponent,RolesComponent],
  imports: [
    CommonModule,
    RoleRoutingModule,
    ReactiveFormsModule,
    FormsModule, LayoutModule, MatTreeModule,
    MaterialModule,
    SharedModule
  ],
  exports: [RoleViewComponent, MatTreeModule,]
})
export class RoleModule { }
