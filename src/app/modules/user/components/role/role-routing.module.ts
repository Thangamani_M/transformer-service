import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard  as AuthGuard} from '@app/shared/services/authguards/can-activate-route.authguard';
import { RoleAddEditComponent, RoleListComponent, RolePermissionComponent, RoleViewComponent } from '@user/components/role';
import { RolesComponent } from './add-edit-list/role-new.component';
const routes: Routes = [
  { path: '', component: RoleViewComponent, data: { title: 'Roles And Permissions List' }, canActivate: [AuthGuard] },
  { path: 'permission', component: RolePermissionComponent, data: { title: 'Add Role Permission' }, canActivate: [AuthGuard] },
  { path: 'add-edit', component:RolesComponent,  data: { title: 'Role View' }, canActivate: [AuthGuard] },
  { path: 'add-edit-new', component: RoleListComponent, data: { title: 'Role View' }, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class RoleRoutingModule { }
