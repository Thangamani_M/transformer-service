
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { CustomeCategoryGroupConfigModel } from '@modules/user/models/custom-category-group-config.model';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-custom-category-group-config-add-edit',
  templateUrl: './custom-category-group-config-add-edit.component.html',
})
export class CustomCategoryGroupConfigAddEditComponent implements OnInit {

  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  customCategoryGroupId: string;
  groupConfigForm: FormGroup;
  userData: UserLogin;
  groupConfigDetails: any;

  constructor(private activateRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private crudService: CrudService,
    private router: Router, private rxjsService: RxjsService,
    private store: Store<AppState>) {
    this.customCategoryGroupId = this.activateRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createGroupConfigForm();
    if (this.customCategoryGroupId) {
      this.getnamedStakeValues();
    } else {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  getnamedStakeValues() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_GROUP, this.customCategoryGroupId, true).subscribe((response) => {
      let namedStackAddEditModel = new CustomeCategoryGroupConfigModel(response.resources);
      this.groupConfigDetails = response.resources;
      this.groupConfigForm.setValue(namedStackAddEditModel);
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  createGroupConfigForm(): void {
    let customeCategoryGroupConfigModel = new CustomeCategoryGroupConfigModel();
    // create form controls dynamically from model class
    this.groupConfigForm = this.formBuilder.group({
    });
    Object.keys(customeCategoryGroupConfigModel).forEach((key) => {
      this.groupConfigForm.addControl(key, new FormControl(customeCategoryGroupConfigModel[key]));
    });
    this.groupConfigForm = setRequiredValidator(this.groupConfigForm, ["customCategoryGroupName", "description"]);
  }

  onSubmit(): void {
    if (this.groupConfigForm.invalid) {
      return;
    }

    if (this.customCategoryGroupId) {
      this.groupConfigForm.value.modifiedUserId = this.userData.userId;
    } else {
      this.groupConfigForm.value.createdUserId = this.userData.userId;
    }

    let crudService: Observable<IApplicationResponse> = !this.customCategoryGroupId
      ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_GROUP, this.groupConfigForm.value)
      : this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_GROUP, this.groupConfigForm.value)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {


        this.router.navigate(['/user/custom-category-config-dashboard'], { queryParams: { tab: 0 }, skipLocationChange: true });

      }
    })
  }

}

