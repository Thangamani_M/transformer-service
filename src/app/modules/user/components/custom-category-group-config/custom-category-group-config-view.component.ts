import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { IT_MANAGEMENT_COMPONENT } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-custom-category-group-config-view',
  templateUrl: './custom-category-group-config-view.component.html',
})
export class CustomCategoryGroupConfigViewComponent implements OnInit {

  customCategoryGroupId: string;
  groupConfigDetails: any;
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{}]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.customCategoryGroupId = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getgroupConfigDetailsById(this.customCategoryGroupId);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][IT_MANAGEMENT_COMPONENT.CUSTOM_CATEGORY]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getgroupConfigDetailsById(customCategoryGroupId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_GROUP, customCategoryGroupId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.groupConfigDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    if (this.customCategoryGroupId) {
      this.router.navigate(['user/custom-category-config-dashboard/custom-category-group-config/add-edit'], { queryParams: { id: this.customCategoryGroupId } });

    }
  }

}

