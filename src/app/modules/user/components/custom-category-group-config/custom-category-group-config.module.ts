import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CustomCategoryGroupConfigAddEditComponent } from './custom-category-group-config-add-edit.component';
import { CustomCategoryGroupConfigRoutingModule } from './custom-category-group-config-routing.module';
import { CustomCategoryGroupConfigViewComponent } from './custom-category-group-config-view.component';



@NgModule({
  declarations: [CustomCategoryGroupConfigAddEditComponent, CustomCategoryGroupConfigViewComponent],
  imports: [
    CommonModule,
    CustomCategoryGroupConfigRoutingModule,
    MaterialModule,
    FormsModule,
    InputSwitchModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule
  ]
})
export class CustomCategoryGroupConfigModule { }
