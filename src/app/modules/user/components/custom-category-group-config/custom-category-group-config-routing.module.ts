import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomCategoryGroupConfigAddEditComponent } from './custom-category-group-config-add-edit.component';
import { CustomCategoryGroupConfigViewComponent } from './custom-category-group-config-view.component';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: CustomCategoryGroupConfigAddEditComponent, data: { title: 'Group Config Add/Edit' } },
  { path: 'view', component: CustomCategoryGroupConfigViewComponent, data: { title: 'Group Config View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CustomCategoryGroupConfigRoutingModule { }
