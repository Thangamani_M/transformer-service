import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavigationListComponent } from '@user/components/navigation';


const routes: Routes = [
    { path: 'list', component: NavigationListComponent, data: { title: 'Navigation List' } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class NavigationRoutingModule { }
