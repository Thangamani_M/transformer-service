import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NavigationAddEditComponent, NavigationListComponent, NavigationRoutingModule } from '@user/components/navigation';

@NgModule({
  declarations: [NavigationAddEditComponent, NavigationListComponent],
  imports: [
    CommonModule,
    NavigationRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents: [NavigationAddEditComponent]
})
export class NavigationModule { }
