import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { CrudService, CrudType, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ReusablePrimeNGTableFeatureService, RxjsService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { Navigation } from '@modules/user/models/navigation/navigation';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { NavigationAddEditComponent } from './navigation-add-edit.component';
@Component({
  selector: 'app-navigations-list',
  templateUrl: './navigation-list.component.html',
})

export class NavigationListComponent extends PrimeNgTableVariablesModel {
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  row: any = {};
  searchColumns: any;
  searchForm: FormGroup;
  observableResponse: any;
  moduleList = []
  moduleId = ""
  searchText  = ""
  constructor(private rxjsService: RxjsService,
    private tableFilterFormService: TableFilterFormService,
    private _fb: FormBuilder,
    private dialog: MatDialog,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private crudService: CrudService, private router: Router) {
    super()
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
  }

  primengTableConfigProperties: any = {
    tableCaption: "Navigations List",
    breadCrumbItems: [{ displayName: 'Internal Settings', relativeRouterUrl: '' },
    { displayName: 'Navigations List', relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Navigations List',
          dataKey: 'navigationId',
          enableBreadCrumb: true,
          enableAddActionBtn: true,
          enableAction: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: true,
          enableStatusActiveAction: true,
          enableMultiDeleteActionBtn: true,
          enableRowDelete: true,
          enableFieldsSearch: false,
          enableHyperLink: true,
          cursorLinkIndex: 1,
          columns: [
            { field: 'moduleName', header: 'Module', width: '150px' },
            { field: 'navigationName', header: 'Name', width: '200px' },
            { field: 'sortOrder', header: 'Sort Order', width: '120px' },
            { field: 'navigationUrl', header: 'Url', width: '200px' },
            { field: 'createdDate', header: 'Created Date', width: '150px', isDate: true },
            { field: 'isActive', header: 'Status', width: '200px' },
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: UserModuleApiSuffixModels.NAVIGATIONS,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT,
        }
      ]

    }
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getRequiredListData();
    this.getDropdowns()
  }

  getRequiredListData(pageIndex?: string, pageSize?: string,
    otherParams?: object) {
    if (this.moduleId) {
      otherParams = { ...otherParams, moduleId: this.moduleId }
    }
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.NAVIGATIONS,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.observableResponse = response;
        this.dataList = this.observableResponse.resources;
        this.totalRecords = response.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  getDropdowns() {
    this.crudService.dropdown(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.UX_MODULES).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.moduleList = response.resources;
        }
      })

  }

  onCRUDRequested(type: CrudType | string, row?: object | any, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage();
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(row);
        break;
        break;
      case CrudType.DELETE:
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getRequiredListData();
            }
          });
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
    }
  }

  applyFilter(filterValue: string) {
    this.getRequiredListData('0','10',{search:filterValue });
  }

  openAddEditPage(data?): void {
    if(!data){
      data = new Navigation()
    }
    const dialogReff = this.dialog.open(NavigationAddEditComponent, { width: '450px', data, disableClose: true });
    dialogReff.componentInstance.outputData.subscribe(result => {
      if (result) {
        this.getRequiredListData();
      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
