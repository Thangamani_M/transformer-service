import { Component, ElementRef, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, prepareRequiredHttpParams } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { AlertService, CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';
import { Navigation } from '@user/models';


@Component({
  selector: 'app-navigation-add-edit',
  templateUrl: './navigation-add-edit.component.html',
})
export class NavigationAddEditComponent implements OnInit {
  @Input() id: string;
  @ViewChild("FocusparentNavigationId", null) NavigationField: ElementRef;
  navigationAddEditForm: FormGroup;
  applicationResponse: IApplicationResponse;
  Navigation = new Navigation();
  ListX: any;
  public modulename: string;
  errorMessage: string;
  btnName: string;
  enableChildNavigation = false;
  enabledisableNavigation: boolean = true
  NavigationList: any;
  isRadioSelected: any = false;
  parentNavigationId: string;
  isDialogLoaderLoading = true;
  loggedUser: UserLogin;

  @Output() outputData  = new EventEmitter()

  constructor(@Inject(MAT_DIALOG_DATA) public data: Navigation,
    private snackbarService: SnackbarService,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private crudService: CrudService,
    private rxjsService: RxjsService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.LoadDropdownNavigation();
    if (this.data.navigationId) {
      this.btnName = 'Update';
      if (this.data.parentNavigationId) {
        this.enableChildNavigation = true;
        this.enabledisableNavigation = false;
        this.LoadNavigationDropdown(this.data.moduleId.toString());
      }
    }
    else {
      this.btnName = 'Create';
    }
    this.navigationAddEditForm = this.formBuilder.group({
      navigationName: this.data.navigationName || '',
      navigationUrl: this.data.navigationUrl || '',
      sortOrder: this.data.sortOrder || '',
      description: this.data.description || '',
      moduleId: this.data.moduleId || '',
      navigationId: this.data.navigationId || '',
      parentNavigationId: this.data.parentNavigationId || null
    });

  }
  enableSubNavigation(e) {
    this.enabledisableNavigation = true;
    this.enableChildNavigation = e.checked;

    if (this.data.navigationId != undefined && this.enableChildNavigation == true || this.navigationAddEditForm.controls['moduleId'].value != "" && this.enableChildNavigation == true) {
      this.enabledisableNavigation = false;
    }
    else {
      this.enabledisableNavigation = true;
    }
    if (this.enableChildNavigation && this.navigationAddEditForm.controls['moduleId'].value != "") {
      this.navigationAddEditForm.controls['parentNavigationId'].setErrors({ 'incorrect': true });
      this.LoadNavigationDropdown(this.navigationAddEditForm.controls['moduleId'].value);
    }
    else {
      this.navigationAddEditForm.controls['parentNavigationId'].clearValidators();
      this.navigationAddEditForm.controls['parentNavigationId'].updateValueAndValidity();
    }

  }
  LoadDropdownNavigation() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.UX_MODULES).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.applicationResponse = response;
          this.ListX = this.applicationResponse.resources;
          this.isDialogLoaderLoading = false;
        }
      });
  }

  OnModuleChange(event: any) {
    this.enabledisableNavigation = false;
    if (this.enableChildNavigation)
      this.LoadNavigationDropdown(event.target.options[event.target.selectedIndex].value);
  }

  LoadNavigationDropdown(moduleId?: string) {
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_NAVIGATIONS,undefined, false, prepareRequiredHttpParams({moduleId: moduleId})).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
        this.NavigationList = response.resources;
          }

    })
  }

  save(): void {
    this.isDialogLoaderLoading = true;
    const navigation = { ...this.data, ...this.navigationAddEditForm.value }

    if (this.enableChildNavigation != true) {
      navigation.parentNavigationId = null;
    }
    let API;
    if (navigation.navigationId) {
      navigation.modifiedUserId = this.loggedUser.userId;

    API = this.crudService.update(ModulesBasedApiSuffix.IT_MANAGEMENT,UserModuleApiSuffixModels.NAVIGATIONS, navigation);
    }else {
      navigation.createdUserId = this.loggedUser.userId;
      API = this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,UserModuleApiSuffixModels.NAVIGATIONS, navigation);
    }
    API.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.onSaveComplete(response)
      }
    })
  }
  onSaveComplete(response): void {
    if (response.statusCode == 200) {
      this.outputData.emit(true)
      this.dialog.closeAll();
      this.navigationAddEditForm.reset();
    }
    else {
      this.snackbarService.openSnackbar(response['message'], ResponseMessageTypes.WARNING)
    }
    this.isDialogLoaderLoading = false;
  }

  numericOnly(event): boolean {
    let patt = /^([0-9])$/;
    let result = patt.test(event.key);
    return result;
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }


}
