import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomCategoryConfigAddEditComponent } from './custom-category-config-add-edit.component';
import { CustomCategoryConfigViewComponent } from './custom-category-config-view.component';


const routes: Routes = [
  { path: '', redirectTo: 'add-edit', pathMatch: 'full' },
  { path: 'add-edit', component: CustomCategoryConfigAddEditComponent, data: { title: 'Custom Category Config Add/Edit' } },
  { path: 'view', component: CustomCategoryConfigViewComponent, data: { title: 'Custom Category Config View' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CustomCategoryConfigRoutingModule { }
