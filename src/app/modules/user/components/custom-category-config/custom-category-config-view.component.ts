import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { IT_MANAGEMENT_COMPONENT } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-custom-category-config-view',
  templateUrl: './custom-category-config-view.component.html',
})
export class CustomCategoryConfigViewComponent implements OnInit {


  customerConfigId: string;
  customerConfigDetails: any;
  isPermissionEdit: any
  isPermissionView: any
  isPermissionRequiresPasswordVerification: any
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.customerConfigId = this.activatedRoute.snapshot.queryParams.id
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][IT_MANAGEMENT_COMPONENT.CUSTOM_CATEGORY]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getcustomerConfigDetailsById(this.customerConfigId);
  }

  getcustomerConfigDetailsById(customerConfigId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_CONFIG, customerConfigId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.customerConfigDetails = response.resources;
          if (this.customerConfigDetails.customCategoryConfigTypeId == '2') {
            this.customerConfigDetails.defaultValue = this.customerConfigDetails.listItemName
          }
          this.isPermissionEdit = response.resources.isPermissionEdit
          this.isPermissionView = response.resources.isPermissionView
          this.isPermissionRequiresPasswordVerification = response.resources.isPermissionRequiresPasswordVerification
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.customerConfigId) {
      this.router.navigate(['/user/custom-category-config-dashboard/custom-category-config/add-edit'], { queryParams: { id: this.customerConfigId } });
    }
  }
}

