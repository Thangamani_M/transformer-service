import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { KeyFilterModule } from 'primeng/keyfilter';
import { CustomCategoryConfigAddEditComponent } from './custom-category-config-add-edit.component';
import { CustomCategoryConfigRoutingModule } from './custom-category-config-routing.module';
import { CustomCategoryConfigViewComponent } from './custom-category-config-view.component';



@NgModule({
  declarations: [CustomCategoryConfigAddEditComponent, CustomCategoryConfigViewComponent],
  imports: [
    CommonModule,
    CustomCategoryConfigRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    SharedModule,
    KeyFilterModule
  ]
})
export class CustomCategoryConfigModule { }
