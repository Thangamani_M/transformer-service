
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { CustomeCategoryConfigModel } from '@modules/user/models/custom-category-config.model';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';

@Component({
  selector: 'app-custom-category-config-add-edit',
  templateUrl: './custom-category-config-add-edit.component.html',
  styleUrls: ['./custom-category-config-add-edit.component.scss']
})
export class CustomCategoryConfigAddEditComponent implements OnInit {
  date7: Date;
  customCategoryConfigId: any
  divisionDropDown: any = [];
  customCategoryConfigForm: FormGroup;
  proactivePatrolTypeList: FormArray;
  loggedUser: any;
  hex;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  customerConfigDetails: any;
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  public colorCode: string = '#278ce2';
  proactivePatrolId: any;
  isDuplicate: boolean;
  isDuplicateNumber: boolean;
  @ViewChildren('input') rows: QueryList<any>;
  dropdownsAndData: any
  categoryList: any = []
  categoryListItemList: any = []
  modulesList: any = []
  categoryGroupList: any = []
  fieldFormateList: any = []
  fieldTypeList: any = []
  letterCasesList: any = []
  customFieldTypeName: any = ''
  customFieldFormatName: any = 'dd/mm/yy'
  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog, private snackbarService: SnackbarService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.customCategoryConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {

    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_CUSTOM_CATEGORY_LIST),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_CUSTOM_MODULES),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_CUSTOM_CATEGORY_GROUP),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_CUSTOM_FIELD_TYPE),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_CUSTOM_LETTER_CASES),
    ];
    this.loadDropdownData(this.dropdownsAndData);
    this.createcustomCategoryConfigForm();
    this.rxjsService.setGlobalLoaderProperty(false);
    setTimeout(() => {
      if (this.customCategoryConfigId) {

        this.getCmcSmsDetailsById().subscribe((response: IApplicationResponse) => {
          let cmcSmsGroup = new CustomeCategoryConfigModel(response.resources);

          this.customerConfigDetails = response.resources;
          if (this.customerConfigDetails.customFieldTypeName == 'Date' || this.customerConfigDetails.customFieldTypeName == 'Time' || this.customerConfigDetails.customFieldTypeName == 'DateTime') {
            cmcSmsGroup.defaultValue = new Date(cmcSmsGroup.defaultValue)
          }
          this.customCategoryConfigForm.patchValue(cmcSmsGroup);

        })
      }
    }, 1000);

  }


  loadDropdownData(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.categoryList = resp.resources;
              break;
            case 1:
              this.modulesList = resp.resources;
              break;
            case 2:
              this.categoryGroupList = resp.resources;
              break;
            case 3:
              this.fieldTypeList = resp.resources;
              break;
            case 4:
              this.letterCasesList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createcustomCategoryConfigForm(): void {
    let proactivePatrolModel = new CustomeCategoryConfigModel();
    this.customCategoryConfigForm = this.formBuilder.group({
    });
    Object.keys(proactivePatrolModel).forEach((key) => {
      this.customCategoryConfigForm.addControl(key, new FormControl(proactivePatrolModel[key]));
    });
    this.customCategoryConfigForm = setRequiredValidator(this.customCategoryConfigForm, ["customCategoryConfigName", "description", "customModuleId", "customCategoryGroupId", "customCategoryConfigTypeId", "customCategoryListId", "defaultValue"]);
    this.customCategoryConfigForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.customCategoryConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)

    this.customCategoryConfigForm.get('customCategoryConfigTypeId').valueChanges.subscribe(val => {
      if (val) {
        this.customCategoryConfigForm.get('defaultValue').setValue(null)
        this.customCategoryConfigForm.get('customFieldTypeId').setValue(null)
        this.customCategoryConfigForm.get('customCategoryListId').setValue(null)
        if (val == '1') {
          this.customCategoryConfigForm = setRequiredValidator(this.customCategoryConfigForm, ['customFieldTypeId']);
          this.customCategoryConfigForm = clearFormControlValidators(this.customCategoryConfigForm, ["customCategoryListId"]);
        } else {
          this.customCategoryConfigForm = clearFormControlValidators(this.customCategoryConfigForm, ["customFieldTypeId"]);
          this.customCategoryConfigForm = setRequiredValidator(this.customCategoryConfigForm, ['customCategoryListId']);
        }
      }
    })

    this.customCategoryConfigForm.get('customFieldTypeId').valueChanges.subscribe(val => {
      if (val) {
        this.customCategoryConfigForm.get('defaultValue').setValue(null)
        let customFieldTypeName = this.fieldTypeList.find(x => x.id == val)
        this.customFieldTypeName = customFieldTypeName ? customFieldTypeName.displayName : null
        this.getCustomFieldFormates(val)
        if (this.customFieldTypeName == 'Integer') {
          this.customCategoryConfigForm = setRequiredValidator(this.customCategoryConfigForm, ['customFieldMinValue', 'customFieldMaxValue']);
          this.customCategoryConfigForm.get('customFieldMinValue').setValidators([Validators.required, Validators.min(1), Validators.max(100)]);
          this.customCategoryConfigForm.get('customFieldMaxValue').setValidators([Validators.required, Validators.min(1), Validators.max(100)]);

        } else {
          this.customCategoryConfigForm = clearFormControlValidators(this.customCategoryConfigForm, ['customFieldMinValue', 'customFieldMaxValue']);
        }
        if (this.customFieldTypeName == 'Char') {
          this.customCategoryConfigForm = setRequiredValidator(this.customCategoryConfigForm, ['customFieldMinLength', 'customFieldMaxLength', 'customLetterCaseId']);
        } else {
          this.customCategoryConfigForm = clearFormControlValidators(this.customCategoryConfigForm, ['customFieldMinLength', 'customFieldMaxLength', 'customLetterCaseId']);
        }

        if (this.customFieldTypeName == 'Date' || this.customFieldTypeName == 'Time') {
          this.customCategoryConfigForm = setRequiredValidator(this.customCategoryConfigForm, ['customFieldFormatId']);
        } else {
          this.customCategoryConfigForm = clearFormControlValidators(this.customCategoryConfigForm, ['customFieldFormatId']);
        }
      } else {
        this.customFieldTypeName = null
      }
    })

    this.customCategoryConfigForm.get('customCategoryListId').valueChanges.subscribe(val => {
      if (val) {
        this.customCategoryConfigForm.get('defaultValue').setValue('')
        this.getCategoryListItems(val)
      }
    })

    this.customCategoryConfigForm.get('customFieldFormatId').valueChanges.subscribe(val => {
      if (val) {
        this.customCategoryConfigForm.get('defaultValue').setValue(null)
        let customFieldFormatName = this.fieldFormateList.find(x => x.id == val)
        this.customFieldFormatName = customFieldFormatName ? customFieldFormatName.displayName : null
      }
    })
  }

  getCategoryListItems(id) {

    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_CUSTOM_CATEGORY_LIST_ITEM, null, false,

      prepareGetRequestHttpParams(null, null, {
        CustomCategoryListId: id
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.categoryListItemList = response.resources;

          let existItem = this.categoryListItemList.find(x => x.id == this.customerConfigDetails?.defaultValue)
          if (existItem) {
            this.customCategoryConfigForm.get('defaultValue').setValue(this.customerConfigDetails?.defaultValue)

          } else {
            this.customCategoryConfigForm.get('defaultValue').setValue("")

          }

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  getCustomFieldFormates(id) {

    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_CUSTOM_FIELD_FORMATS, null, false,

      prepareGetRequestHttpParams(null, null, {
        CustomFieldTypeId: id
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.fieldFormateList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  //Get Details 
  getCmcSmsDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_CONFIG,
      this.customCategoryConfigId
    );
  }
  checkValue(event) {
    if (event.target.value < 0) {
      event.target.value = 1;
    }
  }

  onSubmit(): void {

    if (this.customCategoryConfigForm.invalid) {
      return;
    }
    let formValue = this.customCategoryConfigForm.value;
    if (formValue.customCategoryConfigTypeId == '2') {
      formValue.customCategoryListItemId = formValue.defaultValue
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.customCategoryConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORY_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/user/custom-category-config-dashboard?tab=2');
      }
    })
  }

}

