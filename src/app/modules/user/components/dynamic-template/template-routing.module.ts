import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TemplateAddEditComponent, TemplateListComponent, TemplateViewComponent } from '@modules/user/components/dynamic-template';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: TemplateListComponent, canActivate: [AuthGuard], data: { title: 'Template' } },
  { path: 'add-edit', component: TemplateAddEditComponent, canActivate: [AuthGuard], data: { title: 'Template Add Edit' } },
  { path: 'add-edit/:id', component: TemplateAddEditComponent, canActivate: [AuthGuard], data: { title: 'Template Add Edit' } },
  { path: 'view', component: TemplateViewComponent, canActivate: [AuthGuard], data: { title: 'Template View' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class TemplateRoutingModule { }
