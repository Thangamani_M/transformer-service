import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IT_MANAGEMENT_COMPONENT, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService,
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-template-list',
  templateUrl: './template-list.component.html',
})
export class TemplateListComponent extends PrimeNgTableVariablesModel implements OnInit {
  observableResponse;
  primengTableConfigProperties: any = {
    tableCaption: "Template",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'IT Management ', relativeRouterUrl: '/user/template' }, { displayName: 'Template List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Template',
          dataKey: 'templateId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: true,
          enableMultiDeleteActionBtn: true,
          enableStatusActiveAction: true,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: true,
          enableRowDelete: true,
          enableFieldsSearch: false,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'templateType', header: 'Template Type' },
          { field: 'displayName', header: 'Template Name' },
          { field: 'templateContent', header: 'Content',isparagraph:true },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: UserModuleApiSuffixModels.TEMPLATES,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT,
        }]
    }
  }

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    public dialogService: DialogService,
    public snackbarService: SnackbarService,
    private store: Store<AppState>,
    private router: Router) {
    super()
  }

  ngOnInit(): void {
    this.getTemplates();
    this.combineLatestNgrxStoreData();
  }
  applyFilter(filterValue: string) {
    this.getTemplates('0', '10', { search: filterValue });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.TEMPLATE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getTemplates(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.TEMPLATES,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    let otherParams = {};
    if (CrudType.GET === type && Object.keys(row).length > 0) {
      Object.keys(row['searchColumns']).forEach((key) => {
        otherParams[key] = row['searchColumns'][key]['value'];
      });
      otherParams['sortOrder'] = row['sortOrder'];
      if (row['sortOrderColumn']) {
        otherParams['sortOrderColumn'] = row['sortOrderColumn'];
      }
    }
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getTemplates(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getTemplates();
            }
          });
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigateByUrl("/user/template/add-edit");
        break;
      case CrudType.VIEW:
        this.router.navigate(['user/template/view/'], { queryParams: { id: editableObject['templateId'], templateType: editableObject["templateType"] }, skipLocationChange: true });
        break;
    }
  }
}
