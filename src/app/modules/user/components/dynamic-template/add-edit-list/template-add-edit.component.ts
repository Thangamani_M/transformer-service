import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { CrudService, HttpCancelService } from '@app/shared/services';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { TemplateAddEditModel } from '@modules/user/models/template.model';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Observable } from 'rxjs';
@Component({
  selector: 'template-add-edit',
  templateUrl: './template-add-edit.component.html',
  styleUrls: ['./template-add-edit.component.scss']

})

export class TemplateAddEditComponent implements OnInit {
  htmlContent = '';
  templateId: string;
  templateTypeName: string;
  errorMessage: string;
  templateTypes: any;
  templateAddEditModel: any;
  applicationResponse: IApplicationResponse;
  templateForm: FormGroup;
  createOrUpdateCaption: any;
  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
    ],
  };
  constructor(private formBuilder: FormBuilder, private httpService: CrudService, private rxjsService: RxjsService, private httpCancelService: HttpCancelService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.templateId = this.activatedRoute.snapshot.queryParams.id;
    this.templateTypeName = this.activatedRoute.snapshot.queryParams.templateType;
    this.createOrUpdateCaption = this.templateId ? "Update" : "Create";
  }

  ngOnInit() {

    this.createTemplateAddForm();
    this.TemplateTypeDropdown();
    if (this.templateId) {
      this.httpService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.TEMPLATES, this.templateId, true).subscribe((res) => {
        this.templateForm.patchValue(res.resources);

        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }
  createTemplateAddForm(): void {
    let templateAddEditModel = new TemplateAddEditModel();

    this.templateForm = this.formBuilder.group({
      templateTypeId: [templateAddEditModel.templateTypeId || '', [Validators.required]],
      displayName: [templateAddEditModel.displayName || '', [Validators.required]],
      templateContent: [templateAddEditModel.templateContent || '', [Validators.required]],
      subject: [templateAddEditModel.subject || '', [Validators.maxLength(250)]],
      isActive: [templateAddEditModel.isActive || 'true', [Validators.required]],
      isText: [templateAddEditModel.isText || true, [Validators.required]],

    });
    Object.keys(templateAddEditModel).forEach((key) => {
      this.templateForm.addControl(key, new FormControl(templateAddEditModel[key]));
    });


  }
  TemplateTypeDropdown() {

    this.httpService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_TEMPLATE_TYPES)
      .subscribe({
        next: response => {
          this.applicationResponse = response;
          this.templateTypes = this.applicationResponse.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        },
        error: err => this.errorMessage = err
      });

  }
  onSubmit(): void {

    if (this.templateForm.invalid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    const templateModel: TemplateAddEditModel = { ...this.templateForm.value };

    templateModel.templateId = this.templateId;
    let crudService: Observable<IApplicationResponse> = !this.templateId ? this.httpService.create(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.TEMPLATES, this.templateForm.value) :
      this.httpService.update(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.TEMPLATES, templateModel)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/user/template');
      }
    })
  }
  setContentTypeValue(boolean: Boolean) {
    this.templateForm.get("isText").setValue(boolean);
  }

}
