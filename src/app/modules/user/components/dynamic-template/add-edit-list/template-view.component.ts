import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { TemplateAddEditModel } from '@modules/user/models/template.model';
import { IT_MANAGEMENT_COMPONENT, UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-template-view',
  templateUrl: './template-view.component.html',

})
export class TemplateViewComponent implements OnInit {
  templateId: any;
  templateTypeName: any;
  templateForm: FormGroup;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private crudService: CrudService, private formBuilder: FormBuilder,
    private store: Store<AppState>, private snackbarService: SnackbarService, private router: Router) {
    this.templateId = this.activatedRoute.snapshot.queryParams.id;
    this.templateTypeName = this.activatedRoute.snapshot.queryParams.templateType;
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][IT_MANAGEMENT_COMPONENT.TEMPLATE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createTemplateAddForm();
    if (this.templateId) {
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.TEMPLATES, this.templateId, true).subscribe((res) => {
        this.templateForm.patchValue(res.resources);
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }
  createTemplateAddForm(): void {
    let templateAddEditModel = new TemplateAddEditModel();
    this.templateForm = this.formBuilder.group({
      templateTypeId: [templateAddEditModel.templateTypeId],
      displayName: [templateAddEditModel.displayName],
      templateContent: [templateAddEditModel.templateContent],
      subject: [templateAddEditModel.subject],
    });
    Object.keys(templateAddEditModel).forEach((key) => {
      this.templateForm.addControl(key, new FormControl(templateAddEditModel[key]));
    });
  }
  onEditPage() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/user/template/add-edit'], { queryParams: { id: this.templateId, templateType: this.templateTypeName }, skipLocationChange: true },)
  }
}
