import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { TemplateAddEditComponent, TemplateListComponent, TemplateRoutingModule, TemplateViewComponent } from '@modules/user/components/dynamic-template';


@NgModule({
  declarations: [TemplateAddEditComponent, TemplateListComponent, TemplateViewComponent],
  imports: [FormsModule,
    AngularEditorModule,
    CommonModule,
    TemplateRoutingModule,
    ReactiveFormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
  ],
})
export class TemplateModule {


}
