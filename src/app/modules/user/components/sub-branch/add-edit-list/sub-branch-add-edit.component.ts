import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, SubBranchModel, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs,
  HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix,
  prepareGetRequestHttpParams, RxjsService, setRequiredValidator
} from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { BehaviorSubject, Observable } from 'rxjs';
@Component({
  selector: 'app-branch-add-edit',
  templateUrl: './sub-branch-add-edit.component.html',
})

export class SubBranchAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  branchAddEditForm: FormGroup;
  isAStringOnlyNoSpace = new CustomDirectiveConfig({ isAStringOnlyNoSpace: true });
  alphaConfig = new CustomDirectiveConfig({ isAStringWithHyphenDash: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  branchList = [];
  private divisionId = new BehaviorSubject<string>("");

  loggedUser: UserLogin;
  regionList: any;
  districtList: any;
  divisionDropDown: any;
  constructor(@Inject(MAT_DIALOG_DATA) public branchModel: SubBranchModel,
    private dialog: MatDialog,
    private crudService: CrudService,
    public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createBranchForm();
    this.getRegions();
    setTimeout(() => {
      this.onFormControlChanges();
      this.getDetailsById();
    }, 1000);

  }

  changeRegion() {
    this.branchAddEditForm.controls.divisionId.setValue('');
    this.branchAddEditForm.controls.districtId.setValue('');
    this.branchAddEditForm.controls.branchId.setValue('');
    this.divisionDropDown = [];
    this.districtList = [];
    this.branchList = [];
  }

  changeDivision() {
    this.branchAddEditForm.controls.districtId.setValue('');
    this.branchAddEditForm.controls.branchId.setValue('');
    this.districtList = [];
    this.branchList = [];
  }

  changeDistrict() {
    this.branchAddEditForm.controls.branchId.setValue('');
    this.branchList = [];
  }

  getDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.BRANCHES_SUB_BRANCHES, this.branchModel['subBranchId'], false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.branchAddEditForm.patchValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRegions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null,
        { CountryId: formConfigs.countryId })).subscribe((response) => {

          if (response.statusCode == 200) {
            this.regionList = response.resources;
          }
          let found = this.regionList.filter(e => e.regionId == this.branchModel.regionId)
          if (found.length == 0) {
            this.branchAddEditForm.controls.regionId.setValue('');
          }
        });
  }

  onFormControlChanges(): void {
    this.branchAddEditForm.get('regionId').valueChanges.subscribe(async (regionId: string) => {
      if (!regionId) return;
      await this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { regionId: regionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.divisionDropDown = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.branchAddEditForm.get('divisionId').valueChanges.subscribe(async (divisionId: string) => {

      this.setDivisionId(divisionId);
      if (!divisionId) return;
      await this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISION_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionId: divisionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.districtList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });

    this.branchAddEditForm.get('districtId').valueChanges.subscribe(async (districtId: string) => {
      if (!districtId) return;

      await this.getDivisionId().subscribe(async (divisionId) => {
        await this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT_BRANCH, null, false,
          prepareGetRequestHttpParams(null, null,
            { districtId: districtId, divisionId: divisionId ? divisionId : ''  }))
          .subscribe((response: IApplicationResponse) => {
            if (response && response.resources && response.isSuccess) {
              this.branchList = response.resources;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          })

      });

    });
  }
  setDivisionId(data: string) {
    this.divisionId.next(data);
  }

  getDivisionId(): Observable<string> {
    return this.divisionId.asObservable();
  }

  createBranchForm(): void {
    let branchModelFormModel = new SubBranchModel();
    this.branchAddEditForm = this.formBuilder.group({});
    Object.keys(branchModelFormModel).forEach((key) => {
      this.branchAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(branchModelFormModel[key]));
    });
    this.branchAddEditForm = setRequiredValidator(this.branchAddEditForm, ["branchId", "subBranchName", "subBranchCode", "divisionId", "districtId", "regionId"]);
  }

  onSubmit(): void {
    if (this.branchAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.branchAddEditForm.value.userId = this.loggedUser.userId;
    this.branchAddEditForm.value.branchId = (this.branchAddEditForm.value && this.branchAddEditForm.value.branchId) ? this.branchAddEditForm.value.branchId : null;

    let crudService: Observable<IApplicationResponse> = !this.branchAddEditForm.value.subBranchId ? this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.BRANCHES_SUB_BRANCHES, this.branchAddEditForm.value) :
      this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.BRANCHES_SUB_BRANCHES, this.branchAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dialog.closeAll();
        this.branchAddEditForm.reset();
      }
    })
  }

  dialogClose() {
    this.dialogRef.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
