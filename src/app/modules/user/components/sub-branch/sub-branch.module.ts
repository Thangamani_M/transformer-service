import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DynamicDialogModule } from 'primeng/components/dynamicdialog/dynamicdialog';
import { SubBranchAddEditComponent } from './add-edit-list/sub-branch-add-edit.component';
import { BranchListComponent } from './add-edit-list/sub-branch-list.component';
import { SubBranchRoutingModule } from './sub-branch-routing.module';

@NgModule({
  declarations: [BranchListComponent, SubBranchAddEditComponent],
  imports: [
    SubBranchRoutingModule,
    CommonModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    DynamicDialogModule
  ],
  entryComponents: [SubBranchAddEditComponent]
})
export class SubBranchModule { }