import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DivisionListComponent } from '@user/components/division';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
    { path: '', component: DivisionListComponent, canActivate:[AuthGuard],data: { title: 'Division' } }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class DivisionRoutingModule { }
