import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DivisionAddEditComponent, DivisionListComponent, DivisionRoutingModule } from '@user/components/division';
import { DynamicDialogModule } from 'primeng/components/dynamicdialog/dynamicdialog';

@NgModule({
  declarations: [DivisionAddEditComponent, DivisionListComponent],
  imports: [
    CommonModule,
    DivisionRoutingModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    DynamicDialogModule
  ],
  entryComponents: [DivisionAddEditComponent]
})
export class DivisionModule { }