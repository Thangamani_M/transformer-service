import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { DivisionModel, loggedInUserData, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {  CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-division-add-edit',
  templateUrl: './division-add-edit.component.html',
})

export class DivisionAddEditComponent implements OnInit {
  formConfigs = formConfigs;
  alphaConfig = new CustomDirectiveConfig({ isAStringWithHyphenDash: true });
  @Output() outputData = new EventEmitter<any>();
  divisionAddEditForm: FormGroup;
  loggedUser: UserLogin;
  regionList: any;
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createDivisionForm();

    this.getRegions();
  }

  getRegions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null,
        { CountryId: formConfigs.countryId })).subscribe((response) => {
          if (response.statusCode == 200&&response.resources&&response.isSuccess) {
            this.regionList = response.resources;
          }
        });
  }


  createDivisionForm(): void {
    let divisionFormModel = new DivisionModel(this.config.data);
    this.divisionAddEditForm = this.formBuilder.group({});
    Object.keys(divisionFormModel).forEach((key) => {
      this.divisionAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(divisionFormModel[key]));
    });
    this.divisionAddEditForm = setRequiredValidator(this.divisionAddEditForm, ["divisionName", "regionId", "divisionCode"]);
  }

  onSubmit(): void {
    if (this.divisionAddEditForm.invalid) return;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.divisionAddEditForm.value.divisionId ? this.crudService.create(ModulesBasedApiSuffix.IT_MANAGEMENT,
      UserModuleApiSuffixModels.DIVISIONS, this.divisionAddEditForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.DIVISIONS, this.divisionAddEditForm.value);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ref.close(event);
        this.outputData.emit(true)
        this.divisionAddEditForm.reset();

      }
    })
  }

  dialogClose(event?): void {
    this.ref.close(event);
  }


  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
