import { Component, OnInit } from '@angular/core';
import { IT_MANAGEMENT_COMPONENT, UserModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { DivisionModel } from '@modules/user/models';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { DivisionAddEditComponent } from './division-add-edit.component';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-division-list',
  templateUrl: './division-list.component.html',
})
export class DivisionListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any = {
    tableCaption: "Division",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'IT Management ', relativeRouterUrl: '' }, { displayName: 'Division List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Division',
          dataKey: 'divisionId',
          enableBreadCrumb: true,
          enableAction: true,
          enableAddActionBtn: true,
          enableMultiDeleteActionBtn: true,
          enableStatusActiveAction: true,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: true,
          enableRowDelete: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'divisionCode', header: 'Division Code' },
          { field: 'divisionName', header: 'Division Name' },
          { field: 'regionName', header: 'Region Name' },
          { field: 'isActive', header: 'Status' }
          ],
          apiSuffixModel: UserModuleApiSuffixModels.DIVISIONS,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT
        }]
    }
  }

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    public snackbarService: SnackbarService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService) {
    super();
  }

  ngOnInit(): void {
    this.getDivisions();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][IT_MANAGEMENT_COMPONENT.DIVISION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getDivisions(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.DIVISIONS, undefined, false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.dataList = response.resources
          this.totalRecords = response.totalCount;
          this.loading = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getDivisions(row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;

      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canRowDelete) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialog(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getDivisions();
            }
          });
        break;
      case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              this.dataList[searchObj].isActive = this.dataList[searchObj].isActive ? false : true;
            }
          });
        break;
    }
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  openAddEditPage(type: CrudType | string, divisionModel?: DivisionModel | any): void {
    let data = new DivisionModel(divisionModel);
    const ref = this.dialogService.open(DivisionAddEditComponent, {
      baseZIndex: 10000,
      width: "450px",
      data: data,
      showHeader: false,
    });
    ref.onClose.subscribe((resp) => {
      if (resp) {
        this.getDivisions();
      }
    });
  }
}
