import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';
import {environment} from '@environments/environment';
import {Role} from '@user/models';
import {EnableDisable} from  '@app/shared/models';

@Injectable()
export class RoleService {
  constructor(private http: HttpClient) {
  }
  getRoles(pageindex, maximumrows, RoleCategoryId,searchText): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.apiUrl + 'roles?IsAll=true&pageindex=' 
    + pageindex + '&maximumrows=' + maximumrows + '&RoleCategoryId=' + RoleCategoryId + '&Search='+searchText)
    .pipe(catchError(this.handleError<IApplicationResponse>('GetRoles', null)))
    
  }
  getRole(roleid): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.apiUrl + 'roles/' + roleid)
    .pipe(catchError(this.handleError<IApplicationResponse>('GetRole', null)));

  }
  createRole(role: Role): Observable<IApplicationResponse> {
    const headers = new HttpHeaders({ 'content-type': 'application/json' });
    role.roleId = null;
    role.roleName = role.roleName;
    return this.http.post<IApplicationResponse>(environment.apiUrl + 'roles', role, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Create Role', null)));
  }
  updateRole(role: Role): Observable<IApplicationResponse> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<IApplicationResponse>(environment.apiUrl + 'roles', role, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Update Role', null)));
  }
  enableDisableRole(enableDisable: EnableDisable): Observable<IApplicationResponse> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.put<IApplicationResponse>(environment.apiUrl + 'roles/enable-disable', enableDisable)
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Enable or Disable Role', null)));
  }
  //handle single delete or multiple delete
  deleteRole(id): Observable<IApplicationResponse> {
    return this.http.delete<IApplicationResponse>(environment.apiUrl + 'roles?Ids=' + id + '&IsDeleted=' + false)
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Update Role', null)));
  }
  getRoleCategories(): Observable<IApplicationResponse> {

    return this.http.get<IApplicationResponse>(environment.apiUrl + 'ux/rolecategories').pipe(catchError(this.handleError<IApplicationResponse>('GetRoleCategory', null)));
  }
  
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result)
    }
  }
}
