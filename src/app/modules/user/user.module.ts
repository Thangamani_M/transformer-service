import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerorderstatusAddEditComponent, DealerorderstatusListComponent } from '@inventory/components/dealer';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { UserRoutingModule } from './user-routing.module';
@NgModule({
  declarations: [DealerorderstatusAddEditComponent, DealerorderstatusListComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    AngularEditorModule
  ],
  entryComponents: [DealerorderstatusAddEditComponent],
  providers: [DatePipe]
})
export class UserModule { }
