export class Userlevelmodel {
    public userLevelId: any;
    public userLevelName: string;
    public description: string;
    public isActive: boolean;
    public createdDate: Date;
    public checked: boolean=false;
}
