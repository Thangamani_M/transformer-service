export class Source {

    constructor(
        public sourceId?: number,
        public sourceName = '',
        public description = '',
    ) { }

}
