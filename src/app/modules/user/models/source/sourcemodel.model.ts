export class SourceModel {
  public sourceId?: string;
  public sourceName: string;
  public description: string;
}
