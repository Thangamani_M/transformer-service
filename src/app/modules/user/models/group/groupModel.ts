export class GroupModel {
    
    public groupId: any;
    public groupName: string;
    public locationName: string;
    public noofUser: number;    
    public isActive: boolean;
    public createdDate: Date;
}