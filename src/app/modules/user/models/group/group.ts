export class Group {

    constructor(
      public groupId? :number,
      public groupName = '',
      public locationName ='',
      public noofUser: number=0,
      public createdDate?:Date,
      public isActive?: boolean
      ) { }
  }
  