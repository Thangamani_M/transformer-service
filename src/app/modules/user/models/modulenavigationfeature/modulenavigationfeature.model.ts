export class Modulenavigationfeature {
    public ModuleNavigationRoleId:any;
    public RoleId: any;
    public RoleName: string;
    public ModuleId: any;
    public ModuleName:any;
    public NavigationId: any;
    public NavigationName:string;
    public FeatureId: any;
    public FeatureName:string; 
    public isSelected:boolean;
    public isClosed:boolean;
    public roleName:string;
    public childRoleId:any;
    public IsUpdateRoleAssigned:boolean;
}
