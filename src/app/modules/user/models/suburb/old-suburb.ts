export class Suburb {

    constructor(
      public suburbId? :number,
      public suburbName = '',
      public countryId='',
      public provinceId = '',
      public cityId = '',
      public tierId = '',
      public regionId = '',
      public description = ''
      ) { }
  }
  