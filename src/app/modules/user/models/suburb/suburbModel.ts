export class OldSuburbModel 
{   
    public suburbId: any;
    public suburbName: string;
    public regionName:string;
    public provinceName:string;
    public cityName:string;
    public cityId: string;
    public regionId: string;
    public tierId: string;
    public isActive: boolean;
    public createdDate: Date;
    public checked: boolean=false;
}