export class DenyPermission{

    constructor(
        public denyPermissionId?: number ,
        public userId ='',
        public roleId ='',
        public moduleId ='',
        public navigationId = '',
        public featureId ='',
        public validFromDate?: Date,
        public validToDate?: Date
    ){}
}