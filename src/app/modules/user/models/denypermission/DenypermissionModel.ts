export class DenyPermissionModel{
    public denyPermissionId?: any;
        public userId: any;
        public userName: string;
        public roleId: any;
        public roleName: string;
        public moduleId: any;
        public moduleName: string;
        public navigationId: any;
        public navigationName: string;
        public featureId: any;
        public featureName: string;
        public validFromDate?: Date;
        public validToDate?: Date;
        public isActive: boolean;
        public createdDate: Date;
        public checked: boolean = false;
}