export class ModuleModel {
    
    public moduleId: any;
    public moduleName: string;
    public DisplayName: string;
    public moduleUrl: string;
    public description: string;
    public isActive: boolean;
    public createdDate: Date;
    public checked: boolean=false;
}