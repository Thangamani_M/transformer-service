export class Module {

    constructor(
      public moduleId? :number,
      public moduleName = '',
      public sortOrder ?:number,
      public moduleUrl='',
      public description = ''
      ) { }
  }
  