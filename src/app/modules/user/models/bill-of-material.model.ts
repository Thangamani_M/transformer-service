class BillOfMaterialModel {

    address?: string;
    billOfMaterialId?: string;
    billOfMaterialTypeId?: string;
    customer?: string;
    technicianName?: string;
    debtorRefNo?: string;
    referenceId?: string;
    customerId?: string;
    targetOrderAmount?: string;
    differenceAmount?: string;
    isStock?: string;
    isDiscount?: string;
    isLabour?: string;
    totalAmount?: string;
    billOfMaterialStatusId?: string;
    createdUserId?: string;
    modifiedUserId?: string;
    // ************ below variables are not included from API side yet, so created for FormControlName purpose only
    // ************ once added in API check again.
    jobCategory?: string;
    jobType?: string;
    quoteNumber?: string;
    quoteAmount?: string;
    paymentMethod?: string;
    dealerDebtorCode?: string;
    status?: string;
    //
    billOfMaterialItems?: BillOfMaterialItems[] = [];

    constructor(billOfMaterialModel?: BillOfMaterialModel) {

        this.address = billOfMaterialModel ? billOfMaterialModel.address === undefined ? '' : billOfMaterialModel.address : '';

        this.billOfMaterialId = billOfMaterialModel ? billOfMaterialModel.billOfMaterialId === undefined ? '' : billOfMaterialModel.billOfMaterialId : '';

        this.billOfMaterialTypeId = billOfMaterialModel ? billOfMaterialModel.billOfMaterialTypeId === undefined ? '' : billOfMaterialModel.billOfMaterialTypeId : '';

        this.customer = billOfMaterialModel ? billOfMaterialModel.customer === undefined ? '' : billOfMaterialModel.customer : '';

        this.technicianName = billOfMaterialModel ? billOfMaterialModel.technicianName === undefined ? '' : billOfMaterialModel.technicianName : '';

        this.debtorRefNo = billOfMaterialModel ? billOfMaterialModel.debtorRefNo === undefined ? '' : billOfMaterialModel.debtorRefNo : '';

        this.referenceId = billOfMaterialModel ? billOfMaterialModel.referenceId === undefined ? '' : billOfMaterialModel.referenceId : '';

        this.customerId = billOfMaterialModel ? billOfMaterialModel.customerId === undefined ? '' : billOfMaterialModel.customerId : '';

        this.targetOrderAmount = billOfMaterialModel ? billOfMaterialModel.targetOrderAmount === undefined ? '' : billOfMaterialModel.targetOrderAmount : '';

        this.differenceAmount = billOfMaterialModel ? billOfMaterialModel.differenceAmount === undefined ? '' : billOfMaterialModel.differenceAmount : '';

        this.isStock = billOfMaterialModel ? billOfMaterialModel.isStock === undefined ? '' : billOfMaterialModel.isStock : '';

        this.isDiscount = billOfMaterialModel ? billOfMaterialModel.isDiscount === undefined ? '' : billOfMaterialModel.isDiscount : '';

        this.isLabour = billOfMaterialModel ? billOfMaterialModel.isLabour === undefined ? '' : billOfMaterialModel.isLabour : '';

        this.totalAmount = billOfMaterialModel ? billOfMaterialModel.totalAmount === undefined ? '' : billOfMaterialModel.totalAmount : '';

        this.billOfMaterialStatusId = billOfMaterialModel ? billOfMaterialModel.billOfMaterialStatusId === undefined ? '' : billOfMaterialModel.billOfMaterialStatusId : '';

        this.createdUserId = billOfMaterialModel ? billOfMaterialModel.createdUserId === undefined ? '' : billOfMaterialModel.createdUserId : '';

        this.modifiedUserId = billOfMaterialModel ? billOfMaterialModel.modifiedUserId === undefined ? '' : billOfMaterialModel.modifiedUserId : '';

        this.jobCategory = billOfMaterialModel ? billOfMaterialModel.jobCategory === undefined ? '' : billOfMaterialModel.jobCategory : '';
        this.jobType = billOfMaterialModel ? billOfMaterialModel.jobType === undefined ? '' : billOfMaterialModel.jobType : '';
        this.quoteNumber = billOfMaterialModel ? billOfMaterialModel.quoteNumber === undefined ? '' : billOfMaterialModel.quoteNumber : '';
        this.quoteAmount = billOfMaterialModel ? billOfMaterialModel.quoteAmount === undefined ? '' : billOfMaterialModel.quoteAmount : '';
        this.paymentMethod = billOfMaterialModel ? billOfMaterialModel.paymentMethod === undefined ? '' : billOfMaterialModel.paymentMethod : '';
        this.dealerDebtorCode = billOfMaterialModel ? billOfMaterialModel.dealerDebtorCode === undefined ? '' : billOfMaterialModel.dealerDebtorCode : '';
        this.status = billOfMaterialModel ? billOfMaterialModel.status === undefined ? '' : billOfMaterialModel.status : '';

        if (billOfMaterialModel != undefined) {
            if (billOfMaterialModel['bomQuotationItems'] && billOfMaterialModel['bomQuotationItems'].length > 0) {
                billOfMaterialModel['bomQuotationItems'].forEach((element, index) => {
                    let billOfMaterialItemDetails = [];
                    if(element['billOfMaterialItemDetails'] && element['billOfMaterialItemDetails'].length > 0) {
                        element['billOfMaterialItemDetails'].forEach(item => {
                            billOfMaterialItemDetails.push({
                                billOfMaterialItemDetailId: item['billOfMaterialItemDetailId'],
                                billOfMaterialItemId: item['billOfMaterialItemId'],
                                serialNumber: item['serialNumber'],
                                createdUserId: item['createdUserId'],
                                modifiedUserId: item['modifiedUserId']
                            }); 
                        });
                    }
                    this.billOfMaterialItems.push({
                        checkBillOfMaterial: element['checkBillOfMaterial'] ? true : false,
                        billOfMaterialItemId: element['billOfMaterialItemId'],
                        billOfMaterialId: element['billOfMaterialId'],
                        stockTypeId: element['stockTypeId'],
                        itemId: element['itemId'],
                        itemCode: element['itemCode'],
                        itemName: element['itemName'],
                        invoice: element['invoice'],
                        transfer: element['transfer'],
                        used: element['used'],
                        reserved: element['reserved'],
                        request: element['request'],
                        unitPrice: element['unitPrice'],
                        subTotal: element['subTotal'],
                        vATAmount: element['vATAmount'],
                        totalAmount: element['totalAmount'],
                        createdUserId: element['createdUserId'],
                        modifiedUserId: element['modifiedUserId'],
                        billOfMaterialItemDetails: billOfMaterialItemDetails
                    });                    
                });
            }
        }
    }
}

interface BillOfMaterialItems {
    checkBillOfMaterial?: boolean;
    billOfMaterialItemId?: string;
    billOfMaterialId?: string;
    stockTypeId?: string;
    itemId?: string;
    itemCode?: string;
    itemName?: string;
    invoice?: string;
    transfer?: string;
    used?: string;
    reserved?: string;
    request?: string;
    unitPrice?: string;
    subTotal?: string;
    vATAmount?: string;
    totalAmount?: string;
    createdUserId?: string;
    modifiedUserId?: string;
    billOfMaterialItemDetails?: BillOfMaterialItemDetails[];
}

interface BillOfMaterialItemDetails {
    billOfMaterialItemDetailId?: string;
    billOfMaterialItemId?: string;
    serialNumber?: string;
    createdUserId?: string;
    modifiedUserId?: string;
}

export { BillOfMaterialModel };