
class CustomeCategoryConfigModel {
    constructor(NamedStackModel?: CustomeCategoryConfigModel) {
        this.customCategoryConfigId = NamedStackModel ? NamedStackModel.customCategoryConfigId == undefined ? '' : NamedStackModel.customCategoryConfigId : '';
        this.customCategoryConfigName = NamedStackModel ? NamedStackModel.customCategoryConfigName == undefined ? '' : NamedStackModel.customCategoryConfigName : '';
        this.description = NamedStackModel ? NamedStackModel.description == undefined ? "" : NamedStackModel.description : "";
        this.customModuleId = NamedStackModel ? NamedStackModel.customModuleId == undefined ? "" : NamedStackModel.customModuleId : "";
        this.customCategoryGroupId = NamedStackModel ? NamedStackModel.customCategoryGroupId == undefined ? "" : NamedStackModel.customCategoryGroupId : "";
        this.customCategoryConfigTypeId = NamedStackModel ? NamedStackModel.customCategoryConfigTypeId == undefined ? "2" : NamedStackModel.customCategoryConfigTypeId : "2";
        this.customFieldTypeId = NamedStackModel ? NamedStackModel.customFieldTypeId == undefined ? "" : NamedStackModel.customFieldTypeId : "";
        this.customFieldMinValue = NamedStackModel ? NamedStackModel.customFieldMinValue == undefined ? "0" : NamedStackModel.customFieldMinValue : "0";
        this.customFieldMaxValue = NamedStackModel ? NamedStackModel.customFieldMaxValue == undefined ? "0" : NamedStackModel.customFieldMaxValue : "0";
        this.customFieldMinLength = NamedStackModel ? NamedStackModel.customFieldMinLength == undefined ? "0" : NamedStackModel.customFieldMinLength : "0";
        this.customFieldMaxLength = NamedStackModel ? NamedStackModel.customFieldMaxLength == undefined ? "0" : NamedStackModel.customFieldMaxLength : "0";
        this.isSpecialCharactorPermitted = NamedStackModel ? NamedStackModel.isSpecialCharactorPermitted == undefined ? false : NamedStackModel.isSpecialCharactorPermitted : false;
   
        this.customLetterCaseId = NamedStackModel ? NamedStackModel.customLetterCaseId == undefined ? "" : NamedStackModel.customLetterCaseId : "";
        this.customFieldFormatId = NamedStackModel ? NamedStackModel.customFieldFormatId == undefined ? "" : NamedStackModel.customFieldFormatId : "";
        this.isDefaultToCurrentDate = NamedStackModel ? NamedStackModel.isDefaultToCurrentDate == undefined ? false : NamedStackModel.isDefaultToCurrentDate : false;
        this.isValueType  = NamedStackModel ? NamedStackModel.isValueType == undefined ? false : NamedStackModel.isValueType : false;
        this.isMasked = NamedStackModel ? NamedStackModel.isMasked == undefined ? false : NamedStackModel.isMasked : false;
        this.isPermissionEdit = NamedStackModel ? NamedStackModel.isPermissionEdit == undefined ? false : NamedStackModel.isPermissionEdit : false;
        this.isPermissionView = NamedStackModel ? NamedStackModel.isPermissionView == undefined ? false : NamedStackModel.isPermissionView : false;
        this.isPermissionRequiresPasswordVerification = NamedStackModel ? NamedStackModel.isPermissionRequiresPasswordVerification == undefined ? false : NamedStackModel.isPermissionRequiresPasswordVerification : false;
        this.defaultValue = NamedStackModel ? NamedStackModel.defaultValue == undefined ? "" : NamedStackModel.defaultValue : "";
        this.customCategoryListId = NamedStackModel ? NamedStackModel.customCategoryListId == undefined ? "" : NamedStackModel.customCategoryListId : "";
        this.customCategoryListItemId = NamedStackModel ? NamedStackModel.customCategoryListItemId == undefined ? "" : NamedStackModel.customCategoryListItemId : "";

        this.createdUserId = NamedStackModel ? NamedStackModel.createdUserId == undefined ? '' : NamedStackModel.createdUserId : '';
        this.modifiedUserId = NamedStackModel ? NamedStackModel.modifiedUserId == undefined ? '' : NamedStackModel.modifiedUserId : '';
        this.isActive = NamedStackModel ? NamedStackModel.isActive == undefined ? true : NamedStackModel.isActive : true;
    }
    customCategoryConfigId?: string
    customCategoryConfigName: string;
    description: string;
    customModuleId: string
    customCategoryGroupId: string
    customCategoryConfigTypeId: string
    customFieldTypeId: string
    customFieldMinValue: string
    customFieldMaxValue: string
    customFieldMinLength: string
    customFieldMaxLength: string
    isSpecialCharactorPermitted: boolean
    customLetterCaseId:string
    customFieldFormatId:string
    isDefaultToCurrentDate: boolean
    isValueType: boolean
    isMasked: boolean
    isPermissionEdit: boolean
    isPermissionView: boolean
    isPermissionRequiresPasswordVerification: boolean
    defaultValue: any;
    customCategoryListId: string;
    customCategoryListItemId: string;
    createdUserId: string;
    modifiedUserId: string
    isActive: boolean;
}

export {  CustomeCategoryConfigModel }