export class UserModel {

    constructor(
      public userId? :number,
      public username = '',
      public displayName = '',
      public userStatusId = '',
      public userTypeId = ''
      ) { }
  }
  