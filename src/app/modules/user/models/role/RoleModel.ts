export class RoleModel{
    public roleId:any;
    public roleCategoryId:any;
    public roleName:string;
    public shortCode:string;
    public displayName:string;
    public description:string;
    public sortOrder:number;
    public smallIcon:string;
    public largeIcon:string;
    public htmlBadge:string;
    public isDefault:boolean;
    public isSystem:boolean;
    public isActive:boolean;
    public createdDate: Date;
    public checked: boolean=false;
}