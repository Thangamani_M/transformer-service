export class Role{
    constructor(
        public roleId?:number,
        public roleCategoryId?:number,
        public roleName='',
        public description='',
        public shortCode='',
        public htmlBadge=''
    ){}
}