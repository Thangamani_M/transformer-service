
class CustomeCategoryGroupConfigModel {
    constructor(NamedStackModel?: CustomeCategoryGroupConfigModel) {
        this.customCategoryGroupId = NamedStackModel ? NamedStackModel.customCategoryGroupId == undefined ? '' : NamedStackModel.customCategoryGroupId : '';
        this.customCategoryGroupName = NamedStackModel ? NamedStackModel.customCategoryGroupName == undefined ? '' : NamedStackModel.customCategoryGroupName : '';
        this.description = NamedStackModel ? NamedStackModel.description == undefined ? "" : NamedStackModel.description : "";
        this.createdUserId = NamedStackModel ? NamedStackModel.createdUserId == undefined ? '' : NamedStackModel.createdUserId : '';
        this.isActive = NamedStackModel ? NamedStackModel.isActive == undefined ? true : NamedStackModel.isActive : true;
    }
    customCategoryGroupId?: string
    customCategoryGroupName: string;
    description: string;
    createdUserId: string;
    isActive: boolean;
}

export {  CustomeCategoryGroupConfigModel }