export class City{
    constructor(
        public cityId?:string,
        public cityName='',
        public cityCode='',
        public regionId='',
        public provinceId?:string,
        public countryId?:string
    ){}
}