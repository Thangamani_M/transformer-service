export class CityModel{
    public cityId:any;
    public cityName:string;
    public cityCode:string;
    public provinceId:any;
    public regionId:any;
    public isDeleted:boolean;
    public isSystem:boolean;
    public isActive:boolean;
    public createdDate: Date;
    public checked: boolean=false;
    public provinceName:string;
    public countryName:string;
    public countryId:any;
    public regionName:string;
}