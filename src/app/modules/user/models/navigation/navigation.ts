export class Navigation {

  constructor(
    public navigationId?: number,
    public sortOrder?:number,
    public navigationName = '',
    public moduleId?: number,
    public description = '',
    public navigationUrl = '',
    public parentNavigationId?: number
  ) { }
}
