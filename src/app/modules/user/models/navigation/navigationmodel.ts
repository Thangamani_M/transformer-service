export class Navigationmodel {

    public navigationId: any;
    public navigationName: string;
    public moduleId:any;
    public description: string;
    public navigationUrl:string;  
    public isActive: boolean;
    public createdDate: Date;
    public moduleName:string;
    public checked: boolean=false;

}
