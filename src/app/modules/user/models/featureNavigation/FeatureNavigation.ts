export class FeatureNavigation{
    public featureNavigationId : string;
    public featureId: string;
    public moduleId: string;
    public navigationId: string;
    public isActive: boolean = true;
    public createdUserId: string;
    public modifiedUserId: string;
}