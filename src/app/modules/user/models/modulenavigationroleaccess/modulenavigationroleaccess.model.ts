export class ModuleNavigationRoleAccess {
    public roleId:any;
    public RoleName: string;
    public ModuleId: any;
    public ModuleName:any;
    public NavigationId: any;
    public NavigationName:string;
    public navigationUrl:string;
    public FeatureId: any;
    public FeatureName:string; 
    public PermissionId:any;
    public PermissionName:any;
}
