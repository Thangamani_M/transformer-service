export class State {

    constructor(
      public provinceId? :number,
      public provinceName = '',
      public provinceCode = '',
      public countryId = '',
      public regionId = ''
      ) { }
  }
  