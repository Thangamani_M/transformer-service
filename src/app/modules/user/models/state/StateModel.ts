export class StateModel {
    
    public provinceId: any;
    public provinceName: string;
    public countryId: string;
    public regionId: string;
    public isActive: boolean;
    public createdDate: Date;
    public checked: boolean=false;
}