export class Userstatusmodel {
    public userStatusId: any;
    public userStatusName: string;
    public description: string;
    public isActive: boolean;
    public createdDate: Date;
    public checked: boolean=false;
}
