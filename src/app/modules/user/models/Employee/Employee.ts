 class Employee {
        public employeeId? :number;
        public title='';
        public firstName = '';
        public lastName = '';
        public employeeNo='';
        public gender='';
        public dOB='';
public dOJ='';
public designationId='';
public departmentId='';
public  countryId='';
public provinceId='';
public cityId='';
public phone ='+27';
public contactNumber='';
public officialEmail='';
public employeeStatusId='';
public thumbnailImage='';
public signatureImage='';
public reportingTo='';
public divisionId='';
public toSource='';
public oldUserId='';
public oldUserNumber='';
public oldInitials='';
public status='Active';
public createdUserId=null;       
public modifiedUserId=null;   
roleId=null;  
tempRoleId=null;
roleName=null;  
}


class EmployeeMaintananceInfoAddEditModel{
        public isDoaApproval:false;
        public employeeId? :number;
        psiraNo?:string;
        employeeUniqueId?: string;      
    }
    export {Employee,EmployeeMaintananceInfoAddEditModel};
