abstract class CommonModel {
    constructor(commonModel?: CommonModel) {
        this.createdUserId = commonModel == undefined ? null : commonModel.createdUserId == undefined ? null : commonModel.createdUserId;
        this.modifiedUserId = commonModel == undefined ? null : commonModel.modifiedUserId == undefined ? null : commonModel.modifiedUserId;
    }
    createdUserId: string;
    modifiedUserId: string;
}
class DivisionModel extends CommonModel {
    constructor(divisionModel?: DivisionModel) {
        super(divisionModel);
        this.divisionId = divisionModel == undefined ? "" : divisionModel.divisionId == undefined ? "" : divisionModel.divisionId;
        this.divisionName = divisionModel == undefined ? null : divisionModel.divisionName == undefined ? null : divisionModel.divisionName;
        this.description = divisionModel == undefined ? null : divisionModel.description == undefined ? null : divisionModel.description;
        this.regionId = divisionModel == undefined ? "" : divisionModel.regionId == undefined ? "" : divisionModel.regionId;
        this.divisionCode = divisionModel == undefined ? "" : divisionModel.divisionCode == undefined ? "" : divisionModel.divisionCode;
    }
    divisionId: string;
    divisionName: string;
    description: string;
    regionId: string;
    divisionCode: string;
}
class BranchModel extends CommonModel {
    constructor(branchModel?: BranchModel) {
        super(branchModel);
        this.branchId = branchModel == undefined ? "" : branchModel.branchId == undefined ? "" : branchModel.branchId;
        this.regionId = branchModel == undefined ? "" : branchModel.regionId == undefined ? "" : branchModel.regionId;
        this.branchName = branchModel == undefined ? null : branchModel.branchName == undefined ? null : branchModel.branchName;
        this.branchCode = branchModel == undefined ? null : branchModel.branchCode == undefined ? null : branchModel.branchCode;
        this.districtId = branchModel == undefined ? "" : branchModel.districtId == undefined ? "" : branchModel.districtId;
        this.divisionId = branchModel == undefined ? "" : branchModel.divisionId == undefined ? "" : branchModel.divisionId;
    }
    branchName: string;
    branchCode: string;
    districtId: string;
    regionId: string;
    divisionId: string;
    branchId: string;
}
class SubBranchModel extends CommonModel {
    constructor(subBranchModel?: SubBranchModel) {
        super(subBranchModel);
        this.branchId = subBranchModel == undefined ? "" : subBranchModel.branchId == undefined ? "" : subBranchModel.branchId;
        this.subBranchName = subBranchModel == undefined ? "" : subBranchModel.subBranchName == undefined ? "" : subBranchModel.subBranchName;
        this.regionId = subBranchModel == undefined ? "" : subBranchModel.regionId == undefined ? "" : subBranchModel.regionId;
        this.branchName = subBranchModel == undefined ? null : subBranchModel.branchName == undefined ? null : subBranchModel.branchName;
        this.subBranchCode = subBranchModel == undefined ? null : subBranchModel.subBranchCode == undefined ? null : subBranchModel.subBranchCode;
        this.districtId = subBranchModel == undefined ? "" : subBranchModel.districtId == undefined ? "" : subBranchModel.districtId;
        this.divisionId = subBranchModel == undefined ? "" : subBranchModel.divisionId == undefined ? "" : subBranchModel.divisionId;
        this.subBranchId = subBranchModel == undefined ? "" : subBranchModel.subBranchId == undefined ? "" : subBranchModel.subBranchId;
    }
    branchName: string;
    subBranchName: string;
    subBranchCode: string;
    branchCode: string;
    districtId: string;
    regionId: string;
    divisionId: string;
    branchId: string;
    subBranchId: string;
}
class DistrictModel extends CommonModel {
    constructor(districtModel?: DistrictModel) {
        super(districtModel);
        this.districtId = districtModel == undefined ? "" : districtModel.districtId == undefined ? "" : districtModel.districtId;
        this.regionId = districtModel == undefined ? "" : districtModel.regionId == undefined ? "" : districtModel.regionId;
        this.districtName = districtModel == undefined ? null : districtModel.districtName == undefined ? null : districtModel.districtName;
        this.districtCode = districtModel == undefined ? null : districtModel.districtCode == undefined ? null : districtModel.districtCode;
        this.divisionId = districtModel == undefined ? [] : districtModel.divisionId == undefined ? [] : districtModel.divisionId;
    }
    districtName: string;
    districtCode: string;
    divisionId: any;
    regionId: string;
    districtId: string;
}
class DepartmentModel extends CommonModel {
    constructor(departmentModel?: DepartmentModel) {
        super(departmentModel);
        this.departmentId = departmentModel == undefined ? null : departmentModel.departmentId == undefined ? null : departmentModel.departmentId;
        this.departmentName = departmentModel == undefined ? null : departmentModel.departmentName == undefined ? null : departmentModel.departmentName;
        this.departmentCode = departmentModel == undefined ? null : departmentModel.departmentCode == undefined ? null : departmentModel.departmentCode;
        this.description = departmentModel == undefined ? null : departmentModel.description == undefined ? null : departmentModel.description;
    }
    departmentId: string;
    departmentName: string;
    departmentCode: string;
    description: string;
}
class SuburbModel extends CommonModel {
    constructor(suberbModel?: SuburbModel) {
        super(suberbModel);
        this.suburbName = suberbModel == undefined ? null : suberbModel.suburbName == undefined ? null : suberbModel.suburbName;
        this.countryId = suberbModel ? suberbModel.countryId == undefined ? '' : suberbModel.countryId : '';
        this.provinceId = suberbModel ? suberbModel.provinceId == undefined ? '' : suberbModel.provinceId : '';
        this.cityId = suberbModel ? suberbModel.cityId == undefined ? '' : suberbModel.cityId : '';
        this.tierId = suberbModel ? suberbModel.tierId == undefined ? '' : suberbModel.tierId : '';
        this.description = suberbModel == undefined ? null : suberbModel.description == undefined ? null : suberbModel.description;
        this.createdUserId = suberbModel == undefined ? null : suberbModel.createdUserId == undefined ? null : suberbModel.createdUserId;
        this.modifiedUserId = suberbModel == undefined ? null : suberbModel.modifiedUserId == undefined ? null : suberbModel.modifiedUserId;
    }
    suburbName: string;
    countryId: string;
    provinceId: string
    cityId: string;
    tierId: string;
    description: string
    createdUserId: string;
    modifiedUserId: string;
}
class MainAreaModel extends CommonModel {
    constructor(mainAreaModel?: MainAreaModel) {
        super(mainAreaModel);
        this.firstName = mainAreaModel ? mainAreaModel.firstName == undefined ? '' : mainAreaModel.firstName : '';
        this.lastName = mainAreaModel ? mainAreaModel.lastName == undefined ? '' : mainAreaModel.lastName : '';
        this.fullName = mainAreaModel ? mainAreaModel.fullName == undefined ? '' : mainAreaModel.fullName : '';
        this.employeeNo = mainAreaModel ? mainAreaModel.employeeNo == undefined ? '' : mainAreaModel.employeeNo : '';
        this.officialEmail = mainAreaModel ? mainAreaModel.officialEmail == undefined ? '' : mainAreaModel.officialEmail : '';
        this.assignToName = mainAreaModel ? mainAreaModel.assignToName == undefined ? '' : mainAreaModel.assignToName : '';
        this.regionId = mainAreaModel ? mainAreaModel.regionId == undefined ? '' : mainAreaModel.regionId : '';
        this.divisionId = mainAreaModel ? mainAreaModel.divisionId == undefined ? '' : mainAreaModel.divisionId : '';
        this.branchId = mainAreaModel ? mainAreaModel.branchId == undefined ? '' : mainAreaModel.branchId : '';
        this.businessArea = mainAreaModel == undefined ? null : mainAreaModel.businessArea == undefined ? null : mainAreaModel.businessArea;
        this.subBranchId = mainAreaModel ? mainAreaModel.subBranchId == undefined ? '' : mainAreaModel.subBranchId : '';
        this.provinceId = mainAreaModel ? mainAreaModel.provinceId == undefined ? [] : mainAreaModel.provinceId : [];
        this.cityId = mainAreaModel ? mainAreaModel.cityId == undefined ? '' : mainAreaModel.cityId : '';
        this.opsManagementTypeId = mainAreaModel ? mainAreaModel.opsManagementTypeId == undefined ? null : mainAreaModel.opsManagementTypeId : null;
        this.createdUserId = mainAreaModel ? mainAreaModel.createdUserId == undefined ? '' : mainAreaModel.createdUserId : '';
        this.mainAreaId = mainAreaModel ? mainAreaModel.mainAreaId == undefined ? '' : mainAreaModel.mainAreaId : '';
        this.mainAreaName = mainAreaModel ? mainAreaModel.mainAreaName == undefined ? '' : mainAreaModel.mainAreaName : '';
        this.mainAreaCode = mainAreaModel ? mainAreaModel.mainAreaCode == undefined ? '' : mainAreaModel.mainAreaCode : '';
        this.districtId = mainAreaModel ? mainAreaModel.districtId == undefined ? '' : mainAreaModel.districtId : '';
        this.assignTo = mainAreaModel ? mainAreaModel.assignTo == undefined ? '' : mainAreaModel.assignTo : '';
        this.isClassificationDistrictLevel = mainAreaModel == undefined ? false : mainAreaModel.isClassificationDistrictLevel == undefined ? false : mainAreaModel.isClassificationDistrictLevel;
    }
    officialEmail: string;
    firstName: string;
    lastName: string;
    fullName:string;
    employeeNo: string;
    assignToName: string;
    businessArea: string;
    branchId: string;
    divisionId: string;
    regionId: string;
    subBranchId: string;
    countryId: string;
    mainAreaId: string;
    provinceId: [];
    districtId: string;
    mainAreaName: string;
    mainAreaCode: string;
    cityId: string;
    opsManagementTypeId: number;
    description: string
    createdUserId: string;
    modifiedUserId: string;
    assignTo: string;
    isClassificationDistrictLevel: boolean;
}
class SubAreaModel extends CommonModel {
    constructor(subAreaModel?: SubAreaModel) {
        super(subAreaModel);
        this.firstName = subAreaModel ? subAreaModel.firstName == undefined ? '' : subAreaModel.firstName : '';
        this.lastName = subAreaModel ? subAreaModel.lastName == undefined ? '' : subAreaModel.lastName : '';
        this.fullName = subAreaModel ? subAreaModel.fullName == undefined ? '' : subAreaModel.fullName : '';
        this.employeeNo = subAreaModel ? subAreaModel.employeeNo == undefined ? '' : subAreaModel.employeeNo : '';
        this.officialEmail = subAreaModel ? subAreaModel.officialEmail == undefined ? '' : subAreaModel.officialEmail : '';
        this.sapContract = subAreaModel ? subAreaModel.sapContract == undefined ? '' : subAreaModel.sapContract : '';
        this.boundaryId = subAreaModel ? subAreaModel.boundaryId == undefined ? '' : subAreaModel.boundaryId : '';
        this.boundaryName = subAreaModel ? subAreaModel.boundaryName == undefined ? '' : subAreaModel.boundaryName : '';
        this.boundaryLayerId = subAreaModel ? subAreaModel.boundaryLayerId == undefined ? '' : subAreaModel.boundaryLayerId : '';
        this.opsManagementTypeId = subAreaModel ? subAreaModel.opsManagementTypeId == undefined ? '' : subAreaModel.opsManagementTypeId : '';
        this.createdUserId = subAreaModel ? subAreaModel.createdUserId == undefined ? '' : subAreaModel.createdUserId : '';
        this.mainAreaId = subAreaModel ? subAreaModel.mainAreaId == undefined ? '' : subAreaModel.mainAreaId : '';
        this.mainAreaName = subAreaModel ? subAreaModel.mainAreaName == undefined ? '' : subAreaModel.mainAreaName : '';
        this.description = subAreaModel ? subAreaModel.description == undefined ? '' : subAreaModel.description : '';
        this.initiatorName = subAreaModel ? subAreaModel.initiatorName == undefined ? '' : subAreaModel.initiatorName : '';
        this.initiatorEmailAddress = subAreaModel ? subAreaModel.initiatorEmailAddress == undefined ? '' : subAreaModel.initiatorEmailAddress : '';
        this.referenceId = subAreaModel ? subAreaModel.referenceId == undefined ? '' : subAreaModel.referenceId : '';
        this.assignTo = subAreaModel ? subAreaModel.assignTo == undefined ? '' : subAreaModel.assignTo : '';
        this.isBoundaryRequired = subAreaModel ? subAreaModel.isBoundaryRequired == undefined ? false : subAreaModel.isBoundaryRequired : false;
        this.isActive = subAreaModel ? subAreaModel.isActive == undefined ? false : subAreaModel.isActive : false;
        this.isOutsideResponse = subAreaModel ? subAreaModel.isOutsideResponse == undefined ? false : subAreaModel.isOutsideResponse : false;
        this.isSupervisorArea = subAreaModel ? subAreaModel.isSupervisorArea == undefined ? false : subAreaModel.isSupervisorArea : false;
        this.isFixedArea = subAreaModel ? subAreaModel.isFixedArea == undefined ? false : subAreaModel.isFixedArea : false;
    }
    firstName:string;
    lastName:string;
    fullName:string;
    employeeNo:string;
    officialEmail: string;
    sapContract: string;
    referenceId: string;
    initiatorName: string;
    initiatorEmailAddress: string;
    boundaryLayerId: string;
    boundaryId: string;
    boundaryName: string;
    businessArea: string;
    branchId: string;
    divisionId: string;
    regionId: string;
    subBranchId: string;
    countryId: string;
    mainAreaId: string;
    provinceId: [];
    districtId: string;
    mainAreaName: string;
    mainAreaCode: string;
    cityId: string;
    opsManagementTypeId: string;
    description: string
    createdUserId: string;
    modifiedUserId: string;
    assignTo: string;
    isBoundaryRequired: boolean;
    isActive: boolean;
    isOutsideResponse: boolean;
    isSupervisorArea: boolean;
    isFixedArea: boolean;
}
class TechAreaModel extends CommonModel {
    constructor(techAreaModel?: TechAreaModel) {
        super(techAreaModel);
        this.boundaryType = techAreaModel == undefined ? null : techAreaModel.boundaryType == undefined ? null : techAreaModel.boundaryType;
        this.layerName = techAreaModel == undefined ? null : techAreaModel.layerName == undefined ? null : techAreaModel.layerName;
        this.boundaryName = techAreaModel == undefined ? null : techAreaModel.boundaryName == undefined ? null : techAreaModel.boundaryName;
        this.firstName = techAreaModel ? techAreaModel.firstName == undefined ? '' : techAreaModel.firstName : '';
        this.lastName = techAreaModel ? techAreaModel.lastName == undefined ? '' : techAreaModel.lastName : '';
        this.fullName = techAreaModel ? techAreaModel.fullName == undefined ? '' : techAreaModel.fullName : '';
        this.email = techAreaModel ? techAreaModel.email == undefined ? '' : techAreaModel.email : '';
        this.regionId = techAreaModel ? techAreaModel.regionId == undefined ? '' : techAreaModel.regionId : '';
        this.divisionId = techAreaModel ? techAreaModel.divisionId == undefined ? '' : techAreaModel.divisionId : '';
        this.boundaryId = techAreaModel ? techAreaModel.boundaryId == undefined ? '' : techAreaModel.boundaryId : '';
        this.boundaryName = techAreaModel ? techAreaModel.boundaryName == undefined ? '' : techAreaModel.boundaryName : '';
        this.boundaryLayerId = techAreaModel ? techAreaModel.boundaryLayerId == undefined ? null : techAreaModel.boundaryLayerId : null;
        this.assignToId = techAreaModel ? techAreaModel.assignToId == undefined ? '' : techAreaModel.assignToId : '';
        this.description = techAreaModel ? techAreaModel.description == undefined ? '' : techAreaModel.description : '';
        this.isInstallation = techAreaModel ? techAreaModel.isInstallation == undefined ? false : techAreaModel.isInstallation : false;
        this.isService = techAreaModel ? techAreaModel.isService == undefined ? false : techAreaModel.isService : false;
        this.isInspection = techAreaModel ? techAreaModel.isInspection == undefined ? false : techAreaModel.isInspection : false;
        this.isOverheadAreaSetup = techAreaModel ? techAreaModel.isOverheadAreaSetup == undefined ? false : techAreaModel.isOverheadAreaSetup : false;
        this.stockCollectionLocationId = techAreaModel ? techAreaModel.stockCollectionLocationId == undefined ? '' : techAreaModel.stockCollectionLocationId : '';
        this.stockHoldingLocationId = techAreaModel ? techAreaModel.stockHoldingLocationId == undefined ? '' : techAreaModel.stockHoldingLocationId : '';
        this.branches = techAreaModel ? techAreaModel.branches == undefined ? '' : techAreaModel.branches : '';
        this.districtId = techAreaModel ? techAreaModel.districtId == undefined ? '' : techAreaModel.districtId : '';
        this.defaultCapacity = techAreaModel ? techAreaModel.defaultCapacity == undefined ? '' : techAreaModel.defaultCapacity : '';
        this.stockCollectionWarehouse = techAreaModel ? techAreaModel.stockCollectionWarehouse == undefined ? '' : techAreaModel.stockCollectionWarehouse : '';
        this.stockHoldingWarehouse = techAreaModel ? techAreaModel.stockHoldingWarehouse == undefined ? '' : techAreaModel.stockHoldingWarehouse : '';
        this.assignTo = techAreaModel ? techAreaModel.assignTo == undefined ? '' : techAreaModel.assignTo : '';
        this.employeeNo = techAreaModel ? techAreaModel.employeeNo == undefined ? '' : techAreaModel.employeeNo : '';
    }
    email: string;
    firstName: string;
    lastName: string;
    fullName:string;
    divisionId: string;
    regionId: string;
    boundaryId: string;
    boundaryName: string;
    boundaryLayerId: number;
    assignToId: string;
    description: string;
    isInstallation?: boolean;
    isService?: boolean;
    isInspection?:boolean;
    isOverheadAreaSetup: boolean;
    stockCollectionLocationId: string;
    stockHoldingLocationId: string;
    branches: string;
    districtId: string;
    defaultCapacity: string;
    stockCollectionWarehouse: string;
    stockHoldingWarehouse: string;
    boundaryType: string;
    layerName: string;
    assignTo: string;
    employeeNo: string;
}
class SalesBoundaryModel extends CommonModel {
    constructor(salesBoundaryModel?: SalesBoundaryModel) {
        super(salesBoundaryModel);
        this.boundaryTypeName = salesBoundaryModel == undefined ? null : salesBoundaryModel.boundaryTypeName == undefined ? null : salesBoundaryModel.boundaryTypeName;
        this.boundaryLayerName = salesBoundaryModel == undefined ? null : salesBoundaryModel.boundaryLayerName == undefined ? null : salesBoundaryModel.boundaryLayerName;
        this.boundaryName = salesBoundaryModel == undefined ? null : salesBoundaryModel.boundaryName == undefined ? null : salesBoundaryModel.boundaryName;
        this.boundaryId = salesBoundaryModel ? salesBoundaryModel.boundaryId == undefined ? '' : salesBoundaryModel.boundaryId : '';
        this.boundaryLayerId = salesBoundaryModel ? salesBoundaryModel.boundaryLayerId == undefined ? '' : salesBoundaryModel.boundaryLayerId : '';
        this.regionId = salesBoundaryModel ? salesBoundaryModel.regionId == undefined ? '' : salesBoundaryModel.regionId : '';
        this.divisionId = salesBoundaryModel ? salesBoundaryModel.divisionId == undefined ? '' : salesBoundaryModel.divisionId : '';
        this.districtId = salesBoundaryModel ? salesBoundaryModel.districtId == undefined ? '' : salesBoundaryModel.districtId : '';
        this.employeeFirstName = salesBoundaryModel ? salesBoundaryModel.employeeFirstName == undefined ? '' : salesBoundaryModel.employeeFirstName : '';
        this.employeeLastName = salesBoundaryModel ? salesBoundaryModel.employeeLastName == undefined ? '' : salesBoundaryModel.employeeLastName : '';
        this.fullName = salesBoundaryModel ? salesBoundaryModel.fullName == undefined ? '' : salesBoundaryModel.fullName : '';
        this.assignToName = salesBoundaryModel ? salesBoundaryModel.assignToName == undefined ? '' : salesBoundaryModel.assignToName : '';
        this.employeeNo = salesBoundaryModel ? salesBoundaryModel.employeeNo == undefined ? '' : salesBoundaryModel.employeeNo : '';
        this.initiatorEmailAddress = salesBoundaryModel ? salesBoundaryModel.initiatorEmailAddress == undefined ? '' : salesBoundaryModel.initiatorEmailAddress : '';
        this.branchId = salesBoundaryModel ? salesBoundaryModel.branchId == undefined ? '' : salesBoundaryModel.branchId : '';
        this.assignToId = salesBoundaryModel ? salesBoundaryModel.assignToId == undefined ? '' : salesBoundaryModel.assignToId : '';
        this.description = salesBoundaryModel == undefined ? null : salesBoundaryModel.description == undefined ? null : salesBoundaryModel.description;
        this.createdUserId = salesBoundaryModel == undefined ? null : salesBoundaryModel.createdUserId == undefined ? null : salesBoundaryModel.createdUserId;
        this.referenceId = salesBoundaryModel == undefined ? null : salesBoundaryModel.referenceId == undefined ? null : salesBoundaryModel.referenceId;
        this.initiatorName = salesBoundaryModel == undefined ? null : salesBoundaryModel.initiatorName == undefined ? null : salesBoundaryModel.initiatorName;
    }
    boundaryName: string;
    initiatorEmailAddress: string;
    employeeFirstName: string;
    employeeLastName: string;
    fullName:string;
    assignToName:string;
    employeeNo: string;
    initiatorName: string;
    boundaryLayerName: string;
    divisionId: string;
    regionId: string;
    boundaryTypeName: string;
    boundaryId: string;
    boundaryLayerId: string;
    districtId: string;
    branchId: string;
    assignToId: string;
    description: string
    createdUserId: string;
    referenceId: string;
}
class EmployeeAddEditModel extends CommonModel {
    constructor(employeeAddEditModel?: EmployeeAddEditModel) {
        super(employeeAddEditModel);
        this.employeeId = employeeAddEditModel ? employeeAddEditModel.employeeId == undefined ? null : employeeAddEditModel.employeeId : null;
        this.firstName = employeeAddEditModel ? employeeAddEditModel.firstName == undefined ? null : employeeAddEditModel.firstName : null;
        this.lastName = employeeAddEditModel ? employeeAddEditModel.lastName == undefined ? null : employeeAddEditModel.lastName : null;
        this.emailAddress = employeeAddEditModel ? employeeAddEditModel.emailAddress == undefined ? '' : employeeAddEditModel.emailAddress : '';
        this.contactNumber = employeeAddEditModel ? employeeAddEditModel.contactNumber == undefined ? null : employeeAddEditModel.contactNumber : null;
        this.contactNumberCountryCode = employeeAddEditModel ? employeeAddEditModel.contactNumberCountryCode == undefined ? '+27' : employeeAddEditModel.contactNumberCountryCode : '+27';
        this.mobileNo = employeeAddEditModel ? employeeAddEditModel.mobileNo == undefined ? null : employeeAddEditModel.mobileNo : null;
        this.mobileNoCountryCode = employeeAddEditModel ? employeeAddEditModel.mobileNoCountryCode == undefined ? '+27' : employeeAddEditModel.mobileNoCountryCode : '+27';
        this.userName = employeeAddEditModel ? employeeAddEditModel.userName == undefined ? null : employeeAddEditModel.userName : null;
        this.employeeNo = employeeAddEditModel ? employeeAddEditModel.employeeNo == undefined ? null : employeeAddEditModel.employeeNo : null;
        this.vendorNo = employeeAddEditModel ? employeeAddEditModel.vendorNo == undefined ? null : employeeAddEditModel.vendorNo : null;
        this.physicalLocationDivisionId = employeeAddEditModel ? employeeAddEditModel.physicalLocationDivisionId == undefined ? '' : employeeAddEditModel.physicalLocationDivisionId : '';
        this.physicalLocationBranchId = employeeAddEditModel ? employeeAddEditModel.physicalLocationBranchId == undefined ? '' : employeeAddEditModel.physicalLocationBranchId : '';
        this.regionId = employeeAddEditModel ? employeeAddEditModel.regionId == undefined ? '' : employeeAddEditModel.regionId : '';
        this.divisionId = employeeAddEditModel ? employeeAddEditModel.divisionId == undefined ? '' : employeeAddEditModel.divisionId : '';
        this.districtId = employeeAddEditModel ? employeeAddEditModel.districtId == undefined ? '' : employeeAddEditModel.districtId : '';
        this.branchId = employeeAddEditModel ? employeeAddEditModel.branchId == undefined ? '' : employeeAddEditModel.branchId : '';
        this.departmentId = employeeAddEditModel ? employeeAddEditModel.departmentId == undefined ? '' : employeeAddEditModel.departmentId : '';
        this.userLevelId = employeeAddEditModel ? employeeAddEditModel.userLevelId == undefined ? '' : employeeAddEditModel.userLevelId : '';
        this.roleId = employeeAddEditModel ? employeeAddEditModel.roleId == undefined ? '' : employeeAddEditModel.roleId : null;
        this.designationId = employeeAddEditModel ? employeeAddEditModel.designationId == undefined ? '' : employeeAddEditModel.designationId : '';
        this.reportingEmployeeId = employeeAddEditModel ? employeeAddEditModel.reportingEmployeeId == undefined ? '' : employeeAddEditModel.reportingEmployeeId : '';
        this.managerId = employeeAddEditModel ? employeeAddEditModel.managerId == undefined ? '' : employeeAddEditModel.managerId : '';
        this.toSource = employeeAddEditModel ? employeeAddEditModel.toSource == undefined ? null : employeeAddEditModel.toSource : null;
        this.oldUserId = employeeAddEditModel ? employeeAddEditModel.oldUserId == undefined ? null : employeeAddEditModel.oldUserId : null;
        this.oldUserNumber = employeeAddEditModel ? employeeAddEditModel.oldUserNumber == undefined ? null : employeeAddEditModel.oldUserNumber : null;
        this.oldInitials = employeeAddEditModel ? employeeAddEditModel.oldInitials == undefined ? null : employeeAddEditModel.oldInitials : null;
        this.terminationDate = employeeAddEditModel ? employeeAddEditModel.terminationDate == undefined ? null : employeeAddEditModel.terminationDate : null;
        this.gender = employeeAddEditModel ? employeeAddEditModel.gender == undefined ? null : employeeAddEditModel.gender : null;
        this.thumbnailImage = employeeAddEditModel ? employeeAddEditModel.thumbnailImage == undefined ? null : employeeAddEditModel.thumbnailImage : null;
        this.largeImage = employeeAddEditModel ? employeeAddEditModel.largeImage == undefined ? null : employeeAddEditModel.largeImage : null;
        this.password = employeeAddEditModel ? employeeAddEditModel.password == undefined ? null : employeeAddEditModel.password : null;
        this.secondaryVendorNo = employeeAddEditModel ? employeeAddEditModel.secondaryVendorNo == undefined ? null : employeeAddEditModel.secondaryVendorNo : null;
        this.status = employeeAddEditModel ? employeeAddEditModel.status == undefined ? null : employeeAddEditModel.status : null;
        this.userId = employeeAddEditModel ? employeeAddEditModel.userId == undefined ? null : employeeAddEditModel.userId : null;
        this.userRoleId = employeeAddEditModel ? employeeAddEditModel.userRoleId == undefined ? null : employeeAddEditModel.userRoleId : null;
        this.createdUserId = employeeAddEditModel ? employeeAddEditModel.createdUserId == undefined ? null : employeeAddEditModel.createdUserId : null;
        this.isActive = employeeAddEditModel ? employeeAddEditModel.isActive == undefined ? true : employeeAddEditModel.isActive : true;
        this.isInternalUser = employeeAddEditModel ? employeeAddEditModel.isInternalUser == undefined ? true : employeeAddEditModel.isInternalUser : true;
        this.physicalLocationDivisionName = employeeAddEditModel ? employeeAddEditModel.physicalLocationDivisionName == undefined ? '' : employeeAddEditModel.physicalLocationDivisionName : '';
        this.physicalLocationBranchName = employeeAddEditModel ? employeeAddEditModel.physicalLocationBranchName == undefined ? '' : employeeAddEditModel.physicalLocationBranchName : '';
        this.roleName = employeeAddEditModel ? employeeAddEditModel.roleName == undefined ? '' : employeeAddEditModel.roleName : '';
        this.reportingEmployeeName = employeeAddEditModel ? employeeAddEditModel.reportingEmployeeName == undefined ? '' : employeeAddEditModel.reportingEmployeeName : '';
        this.managerName = employeeAddEditModel ? employeeAddEditModel.managerName == undefined ? '' : employeeAddEditModel.managerName : '';
    }
    employeeId: string;
    roleName: string;
    managerName: string;
    reportingEmployeeName: string;
    physicalLocationDivisionName: string;
    physicalLocationBranchName: string;
    firstName: string;
    lastName: string;
    emailAddress: string;
    contactNumber: string;
    contactNumberCountryCode: string;
    mobileNo: string;
    mobileNoCountryCode: string;
    userName: string;
    employeeNo: string;
    vendorNo: string;
    physicalLocationDivisionId: string;
    physicalLocationBranchId: string;
    regionId: string;
    divisionId: string;
    districtId: string;
    branchId: string;
    departmentId: string;
    userLevelId: string;
    roleId: string;
    designationId: string;
    reportingEmployeeId: string;
    managerId: string;
    toSource: string;
    oldUserId: string;
    oldUserNumber: string;
    oldInitials: string;
    terminationDate: string;
    gender: string;
    thumbnailImage: string;
    largeImage: string;
    password: string;
    secondaryVendorNo: string;
    status: string;
    userId: string;
    userRoleId: string;
    isActive: boolean;
    isInternalUser: boolean;
}

class MenuConfigModel {
    constructor(menuConfigModel?: MenuConfigModel) {
        this.userId = menuConfigModel == undefined ? null : menuConfigModel.userId == undefined ? null : menuConfigModel.userId;
        this.menuId = menuConfigModel == undefined ? null : menuConfigModel.menuId == undefined ? null : menuConfigModel.menuId;
        this.menuName = menuConfigModel == undefined ? null : menuConfigModel.userId == undefined ? null : menuConfigModel.userId;
        this.url = menuConfigModel == undefined ? "" : menuConfigModel.url == undefined ? "" : menuConfigModel.url;
        this.menuParentId = menuConfigModel == undefined ? null : menuConfigModel.menuParentId == undefined ? null : menuConfigModel.menuParentId;
        this.menuCategoryId = menuConfigModel == undefined ? null : menuConfigModel.menuCategoryId == undefined ? null : menuConfigModel.menuCategoryId;
        this.sortOrder = menuConfigModel == undefined ? null : menuConfigModel.sortOrder == undefined ? null : menuConfigModel.sortOrder;
        this.isActive = menuConfigModel == undefined ? true : menuConfigModel.isActive == undefined ? true : menuConfigModel.isActive;
        this.isRoleModuleMapping = menuConfigModel == undefined ? false : menuConfigModel.isRoleModuleMapping == undefined ? false : menuConfigModel.isRoleModuleMapping;
        this.smallIcon = menuConfigModel == undefined ? '' : menuConfigModel.smallIcon == undefined ? '' : menuConfigModel.smallIcon;
    }
    userId?:string;
    menuId?:string;
    menuName?:string;
    url?:string;
    menuParentId?:string;
    menuCategoryId?:number;
    sortOrder?: number;
    isActive: boolean;
    isRoleModuleMapping: boolean;
    smallIcon: string;
}

class BdiCodeModel extends CommonModel {
  constructor(bdiCodeModel?: BdiCodeModel) {
      super(bdiCodeModel);
      this.divisionRefNo = bdiCodeModel == undefined ? "" : bdiCodeModel.divisionRefNo == undefined ? "" : bdiCodeModel.divisionRefNo;
      this.isActive = bdiCodeModel == undefined ?true : bdiCodeModel.isActive == undefined ? true : bdiCodeModel.isActive;
      this.isUpdate = bdiCodeModel == undefined ? false : bdiCodeModel.isUpdate == undefined ? false : bdiCodeModel.isUpdate;
      this.modifiedDate = bdiCodeModel == undefined ? "" : bdiCodeModel.modifiedDate == undefined ? "" : bdiCodeModel.modifiedDate;
      this.divisionId = bdiCodeModel == undefined ? "" : bdiCodeModel.divisionId == undefined ? "" : bdiCodeModel.divisionId;
      this.divisionCode = bdiCodeModel == undefined ? "" : bdiCodeModel.divisionCode == undefined ? "" : bdiCodeModel.divisionCode;
      this.divisionName = bdiCodeModel == undefined ? "" : bdiCodeModel.divisionName == undefined ? "" : bdiCodeModel.divisionName;
  }
  branchName: string;
  isUpdate: boolean;
  modifiedDate: string;
  isActive: boolean;
  divisionId: string;
  divisionRefNo: string;
  divisionCode: string;
  divisionName: string;
}


class AuditLogsFilterModel {
  constructor(auditLogsFilterModel?: AuditLogsFilterModel) {
      this.moduleIds = auditLogsFilterModel == undefined ? [] : auditLogsFilterModel.moduleIds == undefined ? [] : auditLogsFilterModel.moduleIds;
      this.pageIds = auditLogsFilterModel == undefined ? [] : auditLogsFilterModel.pageIds == undefined ? [] : auditLogsFilterModel.pageIds;
      this.referenceId = auditLogsFilterModel == undefined ? "" : auditLogsFilterModel.referenceId == undefined ? "" : auditLogsFilterModel.referenceId;
      this.userIds = auditLogsFilterModel == undefined ? [] : auditLogsFilterModel.userIds == undefined ? [] : auditLogsFilterModel.userIds;
      this.fromDate = auditLogsFilterModel == undefined ? "" : auditLogsFilterModel.fromDate == undefined ? "" : auditLogsFilterModel.fromDate;
      this.toDate = auditLogsFilterModel == undefined ? "" : auditLogsFilterModel.toDate == undefined ? "" : auditLogsFilterModel.toDate;
  }
  branchName: string;
  pageIds: object[];;
  referenceId: string;
  moduleIds: object[];
  userIds: object[];
  toDate: string;
  fromDate: string;
}
class AuditLogsRolesBasedFormModel {
  constructor(auditLogsRolesBasedFormModel?: AuditLogsRolesBasedFormModel) {
      this.roleId = auditLogsRolesBasedFormModel == undefined ? "" : auditLogsRolesBasedFormModel.roleId == undefined ? "" : auditLogsRolesBasedFormModel.roleId;
      this.isActive = auditLogsRolesBasedFormModel == undefined ? true : auditLogsRolesBasedFormModel.isActive == undefined ? true : auditLogsRolesBasedFormModel.isActive;
      this.isExport = auditLogsRolesBasedFormModel == undefined ? false : auditLogsRolesBasedFormModel.isExport == undefined ? false : auditLogsRolesBasedFormModel.isExport;
      this.createdUserId = auditLogsRolesBasedFormModel == undefined ? "" : auditLogsRolesBasedFormModel.createdUserId == undefined ? "" : auditLogsRolesBasedFormModel.createdUserId;
      this.auditPageIds = auditLogsRolesBasedFormModel == undefined ? [] : auditLogsRolesBasedFormModel.auditPageIds == undefined ? [] : auditLogsRolesBasedFormModel.auditPageIds;
      this.moduleIds = auditLogsRolesBasedFormModel == undefined ? [] : auditLogsRolesBasedFormModel.moduleIds == undefined ? [] : auditLogsRolesBasedFormModel.moduleIds;
      this.isSelectedAll = auditLogsRolesBasedFormModel == undefined ? false : auditLogsRolesBasedFormModel.isSelectedAll == undefined ? false: auditLogsRolesBasedFormModel.isSelectedAll;
  }
  branchName: string;
  isActive: boolean;;
  createdUserId: string;
  roleId: string;
  auditPageIds: object[];
  moduleIds: object[];
  toDate: string;
  fromDate: string;
  isSelectedAll:boolean;
  isExport:boolean;
}

export {
    DivisionModel, DepartmentModel, BranchModel, SubBranchModel, DistrictModel, SuburbModel,
    MainAreaModel, SalesBoundaryModel, SubAreaModel, EmployeeAddEditModel, TechAreaModel, MenuConfigModel,BdiCodeModel,AuditLogsFilterModel,AuditLogsRolesBasedFormModel
};

