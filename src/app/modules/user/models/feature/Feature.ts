export class Feature {
    
    public featureId: any;
    public featureName: string;
    public isReleased : boolean;
    public requestedDate: string;
    public verifiedDate: string;
    public approvedDate: string;
    public url: string;
    public method: string;
    public appEnvironment: string;
    public description: string;
    public isActive: boolean = true;
    public isSystem: boolean;
    public createdDate: Date;
    
    // public checked: boolean=false;

    //Feature Navigation
    public featureNavigationId : string;
    public moduleId : string;
    public navigationId: string;
    public moduleName: string;
    public navigationName: string;
}
export class FeatureAddEditModel {
        constructor(featureAddEditModel?:FeatureAddEditModel){
            this.featureName=featureAddEditModel?featureAddEditModel.featureName==undefined?'':featureAddEditModel.featureName:'';
            this.moduleId=featureAddEditModel?featureAddEditModel.moduleId==undefined?'':featureAddEditModel.moduleId:'';
            this.navigationId=featureAddEditModel?featureAddEditModel.navigationId==undefined?'':featureAddEditModel.navigationId:'';
            this.featureId=featureAddEditModel?featureAddEditModel.featureId==undefined?'':featureAddEditModel.featureId:'';
            this.createdUserId=featureAddEditModel?featureAddEditModel.createdUserId==undefined?'':featureAddEditModel.createdUserId:'';

        }

        featureName: string;
        moduleId: string;
        navigationId: string;
        featureId: string;
        createdUserId: string;

}