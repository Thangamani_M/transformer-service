export class TemplateAddEditModel {
    displayName?: string;
    templateTypeId?: number;
    templateContent?: string;
    subject: string;
    templateId?:string;
    isActive?:boolean;
    isText?:Boolean
    
}