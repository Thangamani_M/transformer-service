export class ToralenceModel {
    public outOfOfficeConfigId: any;
    public departmentId: any;
    public department: any;
    public toleranceLevel: any;
    public modifiedDate: Date
}