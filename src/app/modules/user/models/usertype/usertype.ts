export class UserType{
    constructor(
        public userTypeId? :number,
        public userTypeName = '',
        public displayName = '',
        public description = ''
        ) { }   
}