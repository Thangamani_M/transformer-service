export class UserTypeModel{
    public userTypeId: string;
    public userTypeName: string;
    public displayName: string;
    public description: string;
    public isActive: string;
    public createdDate: Date;
    public checked: boolean = false;
}