class UserGroupMappingModel {
    constructor(stockInitiationManualAddEditModel?: UserGroupMappingModel) {
        this.groupId = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.groupId == undefined ? '' : stockInitiationManualAddEditModel.groupId : '';
        this.groupRegions = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.groupRegions == undefined ? '' : stockInitiationManualAddEditModel.groupRegions : '';
        this.groupDistricts = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.groupDistricts == undefined ? '' : stockInitiationManualAddEditModel.groupDistricts : '';
        this.groupDivisions = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.groupDivisions == undefined ? '' : stockInitiationManualAddEditModel.groupDivisions : '';
        this.groupDepartments = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.groupDepartments == undefined ? '' : stockInitiationManualAddEditModel.groupDepartments : '';
        this.groupAreas = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.groupAreas == undefined ? '' : stockInitiationManualAddEditModel.groupAreas : '';
        this.groupRoles = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.groupRoles == undefined ? '' : stockInitiationManualAddEditModel.groupRoles : '';
        this.modifiedUserId = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.modifiedUserId == undefined ? '' : stockInitiationManualAddEditModel.modifiedUserId : '';
        this.createdUserId = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.createdUserId == undefined ? '' : stockInitiationManualAddEditModel.createdUserId : '';
        this.groupName = stockInitiationManualAddEditModel ? stockInitiationManualAddEditModel.groupName == undefined ? '' : stockInitiationManualAddEditModel.groupName : '';
    }

    groupId?: string;
    groupName?: string;
    groupRegions?: any;
    groupDistricts?: any;
    groupDivisions?: any;
    groupDepartments?: any;
    groupAreas?: any;
    groupRoles?: any;
    createdUserId?: string;
    modifiedUserId?: string;
}

export { UserGroupMappingModel }