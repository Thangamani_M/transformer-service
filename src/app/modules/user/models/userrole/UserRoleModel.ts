export class UserRoleModel{
    public userRoleId:any;
    public userId:any;
    public roleId:any;
    public description:string;
    public isDeleted:boolean;
    public isActive:boolean;
    public createdDate: Date;
    public checked: boolean=false;
    public userName:string;
    public roleName:string;
    public displayName:string;
}