export class UserRole{
    constructor(
        public userRoleId?:number,
        public userId?:number,
        public roleId?:number,
        public description=''
    ){}
}