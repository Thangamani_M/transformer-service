export class Specialpermission {
    constructor(
        public specialPermissionId?:number,
        public userId='',
        public roleId='',
        public validFromDate='',
        public validToDate='',
        public specialPermissionStatusId=''
    ){}
}
