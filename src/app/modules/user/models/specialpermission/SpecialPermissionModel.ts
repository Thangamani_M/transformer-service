export class SpecialPermissionModel {
    public specialPermissionId: any;
    public userId: string;
    public roleId: string;
    public validFromDate: Date;
    public validToDate: Date
    public specialPermissionStatusId: string;
    public isActive: boolean;
    public createdDate: Date;
    public checked: boolean = false;
    public requestTypeName:string;
    public applyType:string;
    public nominatedTo:string;
    public approvedBy:string;
    public status:string;
    public period:string;
}
export class LeavePermissionManagerModel {
    public SpecialPermissionId:any;
    public ResquestedBy:string;
    public RequestTypeName:string;
    public LeaveType:string;
    public ApplyType:string;
    public ValidFromDate:Date;
    public ValidToDate:Date;
    public NominatedTo:string;
    public Status:string;
    public Period:String;
    public EmployeeName:string;
}
export class LeavePermissionUserRequestModel {
    public specialPermissionId:any;
    public userId:any;
    public roleId:any;
    public requestTypeId:any;
    public validFromDate:Date;
    public validToDate:Date;
    public isOnsite:boolean;
    public isSelf:boolean;
    public reason:string;
    public leaveApplyTypeId:any;
    public assignedUserId:any;
    public isOffsite:boolean;
    public isNominationRequired:boolean;
}
export class LeavePermissionDetailsModel {
    public specialPermissionId:any;
    public resquestedBy:string;
    public requestTypeName:string;
    public applyType:string;
    public validFromDate:Date;
    public validToDate:Date;
    public nominatedTo:string;
    public approverName:string;
    public reason:string;
    public status:string;
    public approveredBy:string;
    public createdDate: Date;
    public doaComment: string;
    public specialPermissionStatusId:any;
    public assignedUserId:any;
    public requestTypeId:any;
    public specialPermissionApprovalId:any;
    public userId:any;
    public period:string;
    public leaveType:string;
    public employeeNo:string;
}
export class LeavePermissionApprovalModel {
    public specialPermissionId:any;
    public doaComment: string;
    public specialPermissionApprovalId:any;
    public approvalByUserId:any;
    public specialPermissionStatusId:any;
    public autoExpiryDate:Date;
    public assignedUserId:any;
    public createdUserId:string
}
