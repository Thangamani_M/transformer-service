class CustomCategoryListConfigModel {
    constructor(proactivePatrolModel?: CustomCategoryListConfigModel) {
        this.customCategoryListId = proactivePatrolModel ? proactivePatrolModel.customCategoryListId == undefined ? '' : proactivePatrolModel.customCategoryListId : '';
        this.customCategoryListName = proactivePatrolModel ? proactivePatrolModel.customCategoryListName == undefined ? "" : proactivePatrolModel.customCategoryListName : "";
        this.customCategoryTypeId = proactivePatrolModel ? proactivePatrolModel.customCategoryTypeId == undefined ? "1" : proactivePatrolModel.customCategoryTypeId : "1";
        this.minLength = proactivePatrolModel ? proactivePatrolModel.minLength == undefined ? "" : proactivePatrolModel.minLength : "";
        this.length = proactivePatrolModel ? proactivePatrolModel.length == undefined ? "" : proactivePatrolModel.length : "";
        this.decimals = proactivePatrolModel ? proactivePatrolModel.decimals == undefined ? "" : proactivePatrolModel.decimals : "";
        this.description = proactivePatrolModel ? proactivePatrolModel.description == undefined ? "" : proactivePatrolModel.description : "";
        this.customCategoryListItemList = proactivePatrolModel ? proactivePatrolModel.customCategoryListItemList == undefined ? [] : proactivePatrolModel.customCategoryListItemList : [];
        this.modifiedUserId = proactivePatrolModel ? proactivePatrolModel.modifiedUserId == undefined ? "" : proactivePatrolModel.modifiedUserId : "";
        this.createdUserId = proactivePatrolModel ? proactivePatrolModel.createdUserId == undefined ? null : proactivePatrolModel.createdUserId : null;
        this.isActive = proactivePatrolModel ? proactivePatrolModel.isActive == undefined ? true : proactivePatrolModel.isActive : true;

    }
    customCategoryListId?: string;
    customCategoryListName: string
    customCategoryTypeId: string
    minLength: string
    length: string
    decimals: string
    modifiedUserId: string;
    customCategoryListItemList: CustomCategoryListConfigItemListModel[]; 
    createdUserId: string;
    isActive: boolean;
    description:string;
}

  

class CustomCategoryListConfigItemListModel {
    constructor(proactivePatrolTypeListModel?: CustomCategoryListConfigItemListModel) {
        // this.listItemNameId = proactivePatrolTypeListModel ? proactivePatrolTypeListModel.listItemNameId == undefined ? '' : proactivePatrolTypeListModel.listItemNameId : '';
        this.listItemName = proactivePatrolTypeListModel ? proactivePatrolTypeListModel.listItemName == undefined ? '' : proactivePatrolTypeListModel.listItemName : '';
        this.listItemDescription = proactivePatrolTypeListModel ? proactivePatrolTypeListModel.listItemDescription == undefined ? '' : proactivePatrolTypeListModel.listItemDescription : '';
        this.isActive = proactivePatrolTypeListModel ? proactivePatrolTypeListModel.isActive == undefined ? true : proactivePatrolTypeListModel.isActive : true;

    }
    // listItemNameId?:string;
    listItemName:string;
    listItemDescription:string;
    isActive:boolean;
}



export { CustomCategoryListConfigModel,CustomCategoryListConfigItemListModel}
