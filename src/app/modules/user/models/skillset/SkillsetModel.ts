export class SkillsetModel{
    
    public skillSetId:any;
    public skillSetName:string;
    public skillSetCode:string; 
    public description:string; 
    public isActive:boolean; 
    public createdDate:Date;
    public checked: boolean=false;
}