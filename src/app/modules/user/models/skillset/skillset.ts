export class Skillset{
    
    constructor(
        public skillSetId?:any,        
        public skillSetCode ='',
        public description = '',
        public skillSetName=''
        ){ }
    
}