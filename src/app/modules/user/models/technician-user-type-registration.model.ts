 class TechnicalStaffRegistrationFilterModel {

    EmployeeNumber: string;
    FirstName: string;
    LastName: string;
    UserId: string;
    Email: string;
    ContactNumber: string; 
    RoleName: string[];
    BranchIds:string[];
    TechAreaIds:string[];   

    constructor(technicalStaffRegistrationFilterModel?: TechnicalStaffRegistrationFilterModel) {

        this.EmployeeNumber = technicalStaffRegistrationFilterModel ? technicalStaffRegistrationFilterModel.EmployeeNumber === undefined ? '' : technicalStaffRegistrationFilterModel.EmployeeNumber : '';

        this.FirstName = technicalStaffRegistrationFilterModel ? technicalStaffRegistrationFilterModel.FirstName === undefined ? '' : technicalStaffRegistrationFilterModel.FirstName : '';

        this.LastName = technicalStaffRegistrationFilterModel ? technicalStaffRegistrationFilterModel.LastName === undefined ? '' : technicalStaffRegistrationFilterModel.LastName : '';

        this.UserId = technicalStaffRegistrationFilterModel ? technicalStaffRegistrationFilterModel.UserId === undefined ? '' : technicalStaffRegistrationFilterModel.UserId : '';

        this.Email = technicalStaffRegistrationFilterModel ? technicalStaffRegistrationFilterModel.Email === undefined ? '' : technicalStaffRegistrationFilterModel.Email : '';

        this.ContactNumber = technicalStaffRegistrationFilterModel ? technicalStaffRegistrationFilterModel.ContactNumber === undefined ? '' : technicalStaffRegistrationFilterModel.ContactNumber : '';
        this.RoleName = technicalStaffRegistrationFilterModel ? technicalStaffRegistrationFilterModel.RoleName === undefined ? [] : technicalStaffRegistrationFilterModel.RoleName : [];
        this.BranchIds = technicalStaffRegistrationFilterModel ? technicalStaffRegistrationFilterModel.BranchIds === undefined ? [] : technicalStaffRegistrationFilterModel.BranchIds : [];
        this.TechAreaIds = technicalStaffRegistrationFilterModel ? technicalStaffRegistrationFilterModel.TechAreaIds === undefined ? [] : technicalStaffRegistrationFilterModel.TechAreaIds : [];


    }
}

class TechnicalStaffRegistrationModel{
 isUserType: boolean;
 technicianDetailId: string;
 employeeId: string;
 technicianUserTypeConfigId: string;
 aodDebtorId:string;
 isJobTypeNew: boolean;
 isJobTypeReplacement: boolean;
 technicianReplaceId: string;
 currentTechAreaId: string;
 currentTechStockLocationId: string;
 boundaryId: string[];
 techStockLocationId: string;
 techStockLocation:string;
 newTechStockLocation:string;
//  aodDebtorCode: string;
 signalMgtCoordinator: string;
 isPeriodTypeDaily: boolean;
 isPeriodTypeWeekly: boolean;
 lunchBreakFrom: string;
 lunchBreakTo: string;
 isMonday: boolean;
 isTuesday: boolean;
 isWednesday: boolean;
 isThursday: boolean;
 isFriday: boolean;
 isSaturday: boolean;
 isSunday: boolean;
 workingHoursFrom: string;
 workingHoursTo: string;
 startDate: string;
 terminationDate: string;
 techTestingMoblileNumber: string;
 panicDuresPassword : string;
 techTestingPassword: string;
 profileImagePath: string;
 profileImageFileName: string;
 deviceUUID: string;
 vehicleRegNumber: string;
 isDeleted: boolean;
 isSystem: boolean;
 isActive: boolean;
 createdUserId: string;
 modifiedUserId: string;
 tsL1: boolean;
 tsL2: boolean;
 tsL3: boolean;
 tsL4: boolean;
 oasl: boolean;
 ils: boolean;
 pSIRARegNo:string;
 pSIRAExpiryDate: string;
 PISRACertificatePath:string;
 pisraCertificateName:string;
 TechStockLocationName : string;
 mobileCountryCode : string;
 isEveryDay: boolean;
 isEveryWeekDay: boolean;
 constructor(technicalStaffRegistrationModel?: TechnicalStaffRegistrationModel) {

    this.isUserType = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isUserType === undefined ? true : technicalStaffRegistrationModel.isUserType : true;
    this.isJobTypeNew = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isJobTypeNew === undefined ? true : technicalStaffRegistrationModel.isJobTypeNew : true;
    this.isJobTypeReplacement = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isJobTypeReplacement === undefined ? true : technicalStaffRegistrationModel.isJobTypeReplacement : false;
    this.aodDebtorId = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.aodDebtorId === undefined ? '' : technicalStaffRegistrationModel.aodDebtorId : '';
    this.technicianDetailId = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.technicianDetailId === undefined ? '' : technicalStaffRegistrationModel.technicianDetailId : '';
    this.employeeId = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.employeeId === undefined ? '' : technicalStaffRegistrationModel.employeeId : '';
    this.technicianUserTypeConfigId = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.technicianUserTypeConfigId === undefined ? '' : technicalStaffRegistrationModel.technicianUserTypeConfigId : '';
    this.technicianReplaceId = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.technicianReplaceId === undefined ? '' : technicalStaffRegistrationModel.technicianReplaceId : '';
    this.currentTechAreaId = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.currentTechAreaId === undefined ? '' : technicalStaffRegistrationModel.currentTechAreaId : '';
    this.currentTechStockLocationId = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.currentTechStockLocationId === undefined ? '' : technicalStaffRegistrationModel.currentTechStockLocationId : '';
    this.boundaryId = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.boundaryId === undefined ? [] : technicalStaffRegistrationModel.boundaryId : [];
    this.techStockLocationId = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.techStockLocationId === undefined ? '' : technicalStaffRegistrationModel.techStockLocationId : '';
    this.techStockLocation = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.techStockLocation === undefined ? '' : technicalStaffRegistrationModel.techStockLocation : '';
    this.newTechStockLocation = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.newTechStockLocation === undefined ? '' : technicalStaffRegistrationModel.newTechStockLocation : '';
    // this.aodDebtorCode = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.aodDebtorCode === undefined ? '' : technicalStaffRegistrationModel.aodDebtorCode : '';
    this.signalMgtCoordinator = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.signalMgtCoordinator === undefined ? '' : technicalStaffRegistrationModel.signalMgtCoordinator : '';
    this.isPeriodTypeDaily = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isPeriodTypeDaily === undefined ? false : technicalStaffRegistrationModel.isPeriodTypeDaily : false;
    this.isPeriodTypeWeekly = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isPeriodTypeWeekly === undefined ? false : technicalStaffRegistrationModel.isPeriodTypeWeekly : false;
    this.lunchBreakFrom = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.lunchBreakFrom === undefined ? '' : technicalStaffRegistrationModel.lunchBreakFrom : '';
    this.lunchBreakTo = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.lunchBreakTo === undefined ? '' : technicalStaffRegistrationModel.lunchBreakTo : '';
    this.isMonday = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isMonday === undefined ? false : technicalStaffRegistrationModel.isMonday : false;
    this.isTuesday = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isTuesday === undefined ? false : technicalStaffRegistrationModel.isTuesday : false;
    this.isWednesday = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isWednesday === undefined ? false : technicalStaffRegistrationModel.isWednesday : false;
    this.isThursday = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isThursday === undefined ? false : technicalStaffRegistrationModel.isThursday : false;
    this.isFriday = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isFriday === undefined ? false : technicalStaffRegistrationModel.isFriday : false;
    this.isSaturday = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isSaturday === undefined ? false : technicalStaffRegistrationModel.isSaturday : false;
    this.isSunday = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isSunday === undefined ? false : technicalStaffRegistrationModel.isSunday : false;
    this.isEveryDay = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isEveryDay === undefined ? false : technicalStaffRegistrationModel.isEveryDay : false;
    this.isEveryWeekDay = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isEveryWeekDay === undefined ? false : technicalStaffRegistrationModel.isEveryWeekDay : false;
    this.workingHoursFrom = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.workingHoursFrom === undefined ? '' : technicalStaffRegistrationModel.workingHoursFrom : '';
    this.workingHoursTo = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.workingHoursTo === undefined ? '' : technicalStaffRegistrationModel.workingHoursTo : '';
    this.startDate = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.startDate === undefined ? '' : technicalStaffRegistrationModel.startDate : '';    
    this.terminationDate = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.terminationDate === undefined ? '' : technicalStaffRegistrationModel.terminationDate : '';    
    this.techTestingMoblileNumber = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.techTestingMoblileNumber === undefined ? '' : technicalStaffRegistrationModel.techTestingMoblileNumber : '';
    this.techTestingPassword = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.techTestingPassword === undefined ? '' : technicalStaffRegistrationModel.techTestingPassword : '';
    this.panicDuresPassword = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.panicDuresPassword === undefined ? '' : technicalStaffRegistrationModel.panicDuresPassword : '';
    this.profileImagePath = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.profileImagePath === undefined ? '' : technicalStaffRegistrationModel.profileImagePath : '';
    this.profileImageFileName = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.profileImageFileName === undefined ? '' : technicalStaffRegistrationModel.profileImageFileName : '';
    this.deviceUUID = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.deviceUUID === undefined ? '' : technicalStaffRegistrationModel.deviceUUID : '';
    this.vehicleRegNumber = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.vehicleRegNumber === undefined ? '' : technicalStaffRegistrationModel.vehicleRegNumber : '';
    this.isDeleted = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isDeleted === undefined ? false : technicalStaffRegistrationModel.isDeleted : false;
    this.isSystem = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isSystem === undefined ? false : technicalStaffRegistrationModel.isSystem : false;
    this.isActive = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.isActive === undefined ? false : technicalStaffRegistrationModel.isActive : false;
    this.createdUserId = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.createdUserId === undefined ? '' : technicalStaffRegistrationModel.createdUserId : '';
    this.modifiedUserId = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.modifiedUserId === undefined ? '' : technicalStaffRegistrationModel.modifiedUserId : '';
    this.tsL1 = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.tsL1 === undefined ? false : technicalStaffRegistrationModel.tsL1 : false;
    this.tsL2 = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.tsL2 === undefined ? false : technicalStaffRegistrationModel.tsL2 : false;
    this.tsL3 = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.tsL3 === undefined ? false : technicalStaffRegistrationModel.tsL3 : false;
    this.tsL4 = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.tsL4 === undefined ? false : technicalStaffRegistrationModel.tsL4 : false;
    this.oasl = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.oasl === undefined ? false : technicalStaffRegistrationModel.oasl : false;
    this.ils = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.ils === undefined ? false : technicalStaffRegistrationModel.ils : false;
    this.pSIRARegNo = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.pSIRARegNo === undefined ? '' : technicalStaffRegistrationModel.pSIRARegNo : '';
    this.pSIRAExpiryDate = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.pSIRAExpiryDate === undefined ? '' : technicalStaffRegistrationModel.pSIRAExpiryDate : '';
    this.PISRACertificatePath = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.PISRACertificatePath === undefined ? '' : technicalStaffRegistrationModel.PISRACertificatePath : '';
    this.pisraCertificateName = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.pisraCertificateName === undefined ? '' : technicalStaffRegistrationModel.pisraCertificateName : '';
    this.TechStockLocationName = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.TechStockLocationName === undefined ? null : technicalStaffRegistrationModel.TechStockLocationName : null;
    this.mobileCountryCode = technicalStaffRegistrationModel ? technicalStaffRegistrationModel.mobileCountryCode === undefined ? '+27' : technicalStaffRegistrationModel.mobileCountryCode : '+27';





 }
}

export { TechnicalStaffRegistrationFilterModel, TechnicalStaffRegistrationModel };

