import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SignalRTriggers, SnackbarService } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { HubConnection, HubConnectionBuilder, LogLevel } from '@aspnet/signalr';
import { environment } from '@environments/environment';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { loggedInUserData, UpdateShiftModel } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-gaurding-dashboard',
  templateUrl: './gaurding-dashboard.component.html',
  styleUrls: ['./gaurding-dashboard.component.scss'],

})
export class GuardingDashboardComponent extends PrimeNgTableVariablesModel implements OnInit {

  searchForm: FormGroup
  columnFilterForm: FormGroup
  updateShiftForm: FormGroup
  selectedColumns: any[];
  today: any = new Date();
  selectedRows: string[] = [];
  selectedRow: any;
  selectedFilterDate: any;
  row: any = {}
  searchColumns: any
  pageSize: number = 10;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  customerObject: any
  showShiftDialog = false
  selectedRowData: any;
  priorityClass = { 1: 'status-label-pink', 2: 'label-color-orange', 3: 'status-label-green' }
  requestTypeClass = { 'Paid': 'label-color-orange', 'Service': 'status-label-blue' }
  primengTableConfigProperties: any = {
    tableCaption: "Risk Watch Guard Dashboard",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Risk Watch', relativeRouterUrl: '' }, { displayName: 'Guarding Dashboard' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Guarding Dashboard',
          dataKey: 'riskWatchRequestGuardsShiftId',
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: true,
          enableRowDelete: false,
          enableStatusActiveAction: false,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [
            { field: 'sNo', header: 'Record', width: '80px' },
            { field: 'requestStatus', header: 'Request Status', width: '160px' },
            { field: 'shiftStartTime', header: 'Shift Start', width: '170px' },
            { field: 'customerRefNo', header: 'Customer Code', width: '140px' },
            { field: 'guardRequestTypeNames', header: 'Request Type', width: '130px' },
            { field: 'servicesFee', header: 'Services Fees', width: '140px' },
            { field: 'divisionName', header: 'Division', width: '100px' },
            { field: 'eta', header: 'ETA', width: '170px' },
            { field: 'cancelledReason', header: 'Cancelled Reason', width: '150px' },

          ],
          apiSuffixModel: "",
          moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          enableMultiDeleteActionBtn: false,
          ebableAddActionBtn: false
        },
      ]
    }
  }


  constructor(
    private rxjsService: RxjsService,
    private tableFilterFormService: TableFilterFormService,
    private store: Store<AppState>,
    public dialogService: DialogService,
    private formBuilder: FormBuilder,
    private momentService: MomentService,
    private httpCancelService: HttpCancelService,
    private crudService: CrudService,
    private snackbarService: SnackbarService) {
    super()
    this.status = [
      { label: 'Pending Requests', value: 'Pending Requests' },
      { label: 'Cancelled Guard', value: 'Cancelled Guard' },
      { label: 'Guard Confirmed', value: 'Guard Confirmed' },
      { label: 'Guarding Processed', value: 'Guarding Processed' },
    ];
    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});
    this.rxjsService.getIsRefershRiskwatchRequestsProperty().subscribe(isLoad => {
      if (isLoad) {
        this.getRiskWatchServiceList()
      }
    })
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.hubConnectionScheduleCallbackConfigs()
    this.rxjsService.getIsRefershRiskwatchRequestsProperty().subscribe(isLoad => {
      if (isLoad) {
        this.getRiskWatchServiceList()
      }
    })
    this.combineLatestNgrxStoreData();
    this.getRiskWatchServiceList()
    this.searchKeywordRequest();
    this.columnFilterRequest();
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
    }
  }


  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          console.log(this.searchColumns)
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    console.log("ROW",row['searchColumns'])

    if (Object.keys(row['searchColumns']).length > 0) {
      Object.keys(row['searchColumns']).forEach((key) => {
        if (key.toLowerCase().includes('date') || key == 'isActive') {
          this.onCRUDRequested(CrudType.GET, row);
        } else {
          setTimeout(() => {
            this.onCRUDRequested(CrudType.GET, row);
          }, 600);
        }
      });
    } else {
      this.onCRUDRequested(CrudType.GET, row);
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1]["Risk Watch Guard Dashboard"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getRiskWatchServiceList(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    pageSize = pageSize ? pageSize : '20';
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.GUARD_REQUEST_DASHBOARD,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }


  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    let otherParams = {};
    if (CrudType.GET === type && Object.keys(row).length > 0) {
      // logic for split columns and its values to key value pair
      Object.keys(row['searchColumns']).forEach((key) => {
        if (key == "shiftStartTime" || key == 'eta') {
          otherParams[key] = this.momentService.localToUTC(row['searchColumns'][key]);
        } else {
          otherParams[key] = row['searchColumns'][key];
        }
      });
      otherParams['sortOrder'] = row['sortOrder'];
      if (row['sortOrderColumn']) {
        otherParams['sortOrderColumn'] = row['sortOrderColumn'];
      }
    }

    switch (type) {

      case CrudType.GET:
        this.getRiskWatchServiceList(
          row["pageIndex"], row["pageSize"], otherParams);
        break;
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }

        this.showShiftDialog = true;
        this.doAction(row);
        break;

    }

  }

  createUpdateShiftForm(status): void {
    let updateShiftFormModel = new UpdateShiftModel();
    this.updateShiftForm = this.formBuilder.group({});
    Object.keys(updateShiftFormModel).forEach((key) => {
      this.updateShiftForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedInUserData.userId) :
        new FormControl(updateShiftFormModel[key]));
    });
    this.updateShiftForm.get('riskWatchRequestGuardsShiftId').setValue(this.selectedRowData.riskWatchRequestGuardsShiftId);
    this.updateShiftForm.get('riskWatchSupervisorGuardAllocationId').setValue(this.selectedRowData.riskWatchSupervisorGuardAllocationId);
    if (status == "Guarding Processed") {
      this.updateShiftForm = setRequiredValidator(this.updateShiftForm, ["confirmRefNumber"]);
      this.updateShiftForm.get('confirmRefNumber').setValidators([Validators.required, Validators.minLength(3), Validators.maxLength(16)]);
    } else {
      this.updateShiftForm = setRequiredValidator(this.updateShiftForm, ["eta", "guardRefNo"]);
    }
  }


  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  doAction(row) {

    this.selectedRowData = row;

    this.createUpdateShiftForm(this.selectedRowData.requestStatus);
  }

  onSubmit() {
    if (this.updateShiftForm.invalid) return '';

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse>;

    if (this.selectedRowData.requestStatus == "Guarding Processed") {
      crudService = this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.GUARD_REQUEST_CONFIRM, this.updateShiftForm.value)
    } else {
      crudService = this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.GUARD_REQUEST_PROCESS, this.updateShiftForm.value)
    }

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.updateShiftForm.reset();
        this.showShiftDialog = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        this.getRiskWatchServiceList()
      }
    })

  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }



  openCustomDialog(customText, header, boolean) {
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: header,
      showHeader: false,
      closable: true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText, isShowNo: boolean },
    });
    confirm.onClose.subscribe((resp) => {
      if (!resp) {
      }
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  connection: HubConnection;

  hubConnectionScheduleCallbackConfigs(): void {
    this.connection = new HubConnectionBuilder()
      .configureLogging(LogLevel.Critical)
      .withUrl(environment.SALES_API)
      .build();

    this.connection.start()
      .then(() => console.log('Connection started for dashboard'))
      .catch(err => this.hubConnectionScheduleCallbackConfigs());

    this.connection.on(SignalRTriggers.RiskWatchListRefreshTrigger, data => {
      this.getRiskWatchServiceList()
    })
  }
}
