import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { GuardingDashboardComponent } from './gaurding-dashboard/gaurding-dashboard.component';
import { GuardingDashboardReportComponent } from './guarding-dashboard-report/guarding-dashboard-report.component';
import { RiskwatchRoutingModule } from './riskwatch-routing.module';



@NgModule({
  declarations: [GuardingDashboardComponent,GuardingDashboardReportComponent],
  imports: [
    CommonModule,
    MaterialModule,
    LayoutModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    RiskwatchRoutingModule
  ]
})
export class RiskwatchModule { }
