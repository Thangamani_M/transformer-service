import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-guarding-dashboard-report',
  templateUrl: './guarding-dashboard-report.component.html',
  styleUrls: ['./guarding-dashboard-report.component.scss']
})
export class GuardingDashboardReportComponent extends PrimeNgTableVariablesModel  implements OnInit {
  row: any = {}
  searchColumns: any;
  isShowNoRecords: any = true;
  pageLimit: any = [4, 25, 50, 75, 100];
  totalRecords: any;
  scrollEnabled: boolean = false;
  GuardingdashboardReportForm: FormGroup;
  isShowCancelledBy = false;
  constructor(private rxjsService: RxjsService,
    private store: Store<AppState>,public dialogService: DialogService,private momentService: MomentService,private _fb: FormBuilder,private crudService: CrudService) {
      super();
    this.totalRecords = 0;
        this.isShowNoRecords = true;
  }
  primengTableConfigProperties: any = {
    tableCaption: "Risk Watch Completed Request",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Risk Watch' }, { displayName: 'Report' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Risk Watch Completed Request',
          dataKey: 'riskWatchRequestGuardsShiftId',
          enableBreadCrumb: true,
          enableExportCSV: false,
          enableCanceledBy: true,
          enableExportExcel: false,
          enableExportExcelMenu: true,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableAction: true,
          enableExportBtn: true,
          enableRowDelete: false,
          enableStatusActiveAction: false,
          enableFieldsSearch: true,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [
            { field: 'sNo', header: 'Record', width: '100px',hideSortIcon: true },
            { field: 'shiftStartTime', header: 'Shift Start', width: '150px',hideSortIcon: true,isDateTime:true,type:'date' },
            { field: 'customerRefNo', header: 'Customer Id', width: '150px',hideSortIcon: true },
            { field: 'customerName', header: 'Customer Name', width: '150px',hideSortIcon: true },
            { field: 'servicesFee', header: 'Service fees', width: '100px',hideSortIcon: true },
            { field: 'divisionName', header: 'Division', width: '80px',hideSortIcon: true },
            { field: 'formatedAddress', header: 'Address', width: '200px',hideSortIcon: true },
          ],
          apiSuffixModel: CustomerModuleApiSuffixModels.GUARD_REQUEST_REPORT,
          moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
          enableMultiDeleteActionBtn: false,
          ebableAddActionBtn: false,
        },
      ]
    }
  }

  ngOnInit(): void {
    this.createGuardingDashboardForm();
    this.combineLatestNgrxStoreData();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  createGuardingDashboardForm() {
    this.GuardingdashboardReportForm = this._fb.group({
      fromDate: [''],
      toDate: [''],
      Status: [''],
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onSelectChange(){
    let _status = this.GuardingdashboardReportForm.get('Status').value;
    if(_status =="CANCELLED"){
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns.push({ field: 'cancelledBy', header: 'Cancelled By', width: '150px',hideSortIcon: true })
    }else{
     let findIndex = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns.findIndex(item=> item.header == "Cancelled By");
     if(findIndex != -1){
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns.splice(findIndex, 1);
     }
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1]["Risk Watch Guard Dashboard"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.GET:
        if(searchObj?.shiftStartTime){
          searchObj.shiftStartTime = this.momentService.localToUTC(searchObj?.shiftStartTime)
        }
        this.getRequiredguardingreportData(
          row["pageIndex"], row["pageSize"],searchObj);
        break;
      case CrudType.EXPORT:
        this.exportExcel();
        break;
    }
  }

  showRecordByFilter(){
    let params = this.GuardingdashboardReportForm.value,
    _params = {
      fromDate : this.momentService.localToUTC(params.fromDate),
      toDate:this.momentService.localToUTC(params.toDate),
      Status : params.Status
    }
    this.getRequiredguardingreportData(null,null, _params);
  }

  getRequiredguardingreportData(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.GUARD_REQUEST_REPORT,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      if (data.isSuccess && data.statusCode == 200) {
        this.dataList = data.resources;
        this.selectedRows = []
        data.resources.forEach(element => {
          if (element.isActive) {
            this.selectedRows.push(element)
          }
        });
        this.totalRecords = 0;
        this.isShowNoRecords = false;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;
        this.isShowNoRecords = true;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }
  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
}
