import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuardingDashboardComponent } from './gaurding-dashboard/gaurding-dashboard.component';
import { GuardingDashboardReportComponent } from './guarding-dashboard-report/guarding-dashboard-report.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


const routes: Routes = [
    { path: 'gaurding-dashboard', component: GuardingDashboardComponent, canActivate:[AuthGuard],data: { title: 'Gaurding Dashboard' } },
    { path: 'guarding-dashboard-report', component: GuardingDashboardReportComponent, canActivate:[AuthGuard],data: { title: 'Guarding Dashboard Report' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class RiskwatchRoutingModule { }
