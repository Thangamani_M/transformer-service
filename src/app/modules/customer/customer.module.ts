import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerManagementService } from '@modules/customer/services';
import { CustomerRoutingModule } from './customer-routing.module';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
  ],
  entryComponents: [],
  exports: [],
  providers: [CustomerManagementService, DatePipe],
})
export class CustomerModule { }
