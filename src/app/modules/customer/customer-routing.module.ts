import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerAddEditComponent } from '@customer/components';

const routes: Routes = [
    {
        path: 'configuration', loadChildren: () => import('./components/customer/customer-configuration/customer-configuration.module').then(m => m.CustomerConfigurationModule)
    },
    {
        path: 'manage-customers', children: [

            {
                path: '', loadChildren: () => import('./components/customer/customer-list/customerlist.module').then(m => m.CustomerListModule)
            },
            {
                path: 'industry', loadChildren: () => import('./components/customer/industry/industry.module').then(m => m.IndustryModule)
            },
            {
                path: 'view-transactions', loadChildren: () => import('./components/customer/customer-management/customer-add-edit/customer-view-transactions.module').then(m => m.CustomerViewTransactionModule)
            },
            {
                path: 'industry-type', loadChildren: () => import('./components/customer/industry-type/industry-type.module').then(m => m.IndustryTypeModule)
            },
            {
                // path: 'view/:id', component: CustomerAddEditComponent, data: { title: 'Customer View' }
                path: 'view/:id', loadChildren: () => import('./components/customer/customer-management/customer-add-edit/customer-add-edit.module').then(m => m.CustomerAddEditModule)
            },
            {
                path: 'ticket', loadChildren: () => import('./components/customer/customer-management/customer-ticket/customer-ticket.module').then(m => m.CustomerTicketModule)
            },
            {
                path: 'support-document', loadChildren: () => import('./components/customer/customer-management/support-document/support-document.module').then(m => m.SupportDocumentModule)
            },
            { 
                path: 'inspection-call', loadChildren: () => import('./../technical-management/components/inspection-call/inspection-call-add-edit/inspection-call-add-edit.module').then(m => m.InspectionCallAddEditModule), data: { preload: true } 
            },
            {
                path: 'call-initiation', loadChildren: () => import('./components/customer/technician-installation/initiation-call-creation/initiation-call-creation.module').then(m => m.InitiationCallCreationModule)
            },
            {
                path: 'dealer-call', loadChildren: () => import('./components/customer/technician-installation/dealer-call-creation/dealer-call-creation-add-edit.module').then(m => m.DealerCallCreationAddEditModule)
            },
            {
                path: 'radio-removal-call', loadChildren: () => import('./components/customer/technician-installation/radio-removal-call/radio-removal-call.module').then(m => m.RadioRemovalCallModule)
            },
            {
                path: 'bill-of-material', loadChildren: () => import('./components/customer/technician-installation/bill-of-materials/bill-of-materials.module').then(m => m.BillOfMaterialsModule)
            },
            {
                path: 'refer-to-sale', loadChildren: () => import('./components/customer/technician-installation/refer-to-sale/refer-to-sale.module').then(m => m.ReferToSaleModule)
            },
            {
                path: 'tech-transfer', loadChildren: () => import('./components/customer/technician-installation/customer-technician-transfer/customer-technician-transfer.module').then(m => m.CustomerTechnicianTransferModule)
            },
            {
                // path: 'holiday-instructions', loadChildren: () => import('./components/customer/customer-management/holiday-instructions/holiday-instruction.module').then(m => m.HolidayInstructionModule)
                path: 'holiday-instructions', loadChildren: () => import('./components/customer/customer-management/holiday-instructions/holiday-instructions-list.module').then(m => m.HolidayInstructionListModule)
            },
            {
                path: 'safe-entry-request/add-edit', loadChildren: () => import('./components/customer/customer-management/safe-entry-request/safe-entry-request-add-edit.module').then(m => m.SafeEntryRequestAddEditModule),
            },
            {
                path: 'trouble-shoot-system', loadChildren: () => import('./components/customer/customer-management/self-help-portal/trouble-shoot-system.module').then(m => m.TroubleShootSystemModule)
            },
            {
                path: 'trouble-shoot-alarm', loadChildren: () => import('./components/customer/customer-management/self-help-portal/trouble-shoot-alarm.module').then(m => m.TroubleShootAlarmModule)
            },
            {
                path: 'trouble-shoot-alarm-quetion-answer', loadChildren: () => import('./components/customer/customer-management/self-help-portal/trouble-shoot-alarm-quetion-answer.module').then(m => m.TroubleShootAlarmQuetionAnswerModule)
            },
            {
                path: 'customer-verification', loadChildren: () => import('./components/customer/customer-management/customer-verifications/customer-verifications.module').then(m => m.CustomerVerificationModule)
            },
            {
                path: 'manual-invoice', loadChildren: () => import('./components/customer/customer-management/manual-invoice/manual-invoice.module').then(m => m.ManualInvoiceModule)
            },
            // {
            //   path:'manual-invoice-approval',component:ManualInvoiceApprovalComponent,data:{title:'Manual Invoice Approval'}
            // },
            {
                path: 'transaction-view', loadChildren: () => import('./components/customer/customer-management/manual-invoice/transaction-view.module').then(m => m.TransactionViewModule)
            },
            {
                path: 'avs-check', loadChildren: () => import('./components/customer/customer-management/customer-add-edit/avs-check-verification/avs-check-verification.module').then(m => m.AvsCheckVerificationModule)
            },
            {
                path: 'quotation-items', loadChildren: () => import('./components/customer/technician-installation/quotation-items/quotation-items.module').then(m => m.QuotationItemsModule)
            },
            {
                path: 'site-hazard', loadChildren: () => import('./components/customer/technician-installation/site-hazard-technician/site-hazard-technician.module').then(m => m.SiteHazardTechnicianModule)
            },
            {
                path: 'lead-hand-over-certificate', loadChildren: () => import('./components/customer/technician-installation/lead-hand-over-certificate/lead-hand-over-certificate.module').then(m => m.LeadHandOverCertificateModule)
            },
            {
                path: 'credit-notes-control', loadChildren: () => import('./components/customer/customer-management/credit-notes-control/credit-notes-control.module').then(m => m.CreditNotesControlModule)
            },
            {
                path: 'customer-service-upgrade-downgrade', loadChildren: () => import('./components/customer/customer-management/customer-service-upgrade-downgrade/customer-service-module').then(m => m.CustomerServiceListModule)
            },
            {
                path: 'balance-of-contract', loadChildren: () => import('./components/customer/customer-management/balance-of-contract/balance-of-contract.module').then(m => m.BalanceOfContractModule)
            },
            {
                path: 'purchase-of-alarm-system', loadChildren: () => import('./components/customer/customer-management/purchase-of-alarm-system/purchase-of-alarm-system.module').then(m => m.PurchaseOfAlarmSystemModule)
            },
            {
                path: 'customer-view-notes', loadChildren: () => import('./components/customer/customer-view-notes/customer-view-notes.module').then(m => m.CustomerViewNotesModule)
            },
            {
                path: 'credit-controller', loadChildren: () => import('./components/customer/customer-management/customer-add-edit/credit-controller/credit-controller.module').then(m => m.CreditControllerModule)
            },
            {
                path: 'payment-transfer', loadChildren: () => import('./components/customer/customer-management/payment-transfer/payment-transfer.module').then(m => m.PaymentTransferModule)
            },
            {
                path: 'payment-split', loadChildren: () => import('./components/customer/customer-management/payment-split/payment-split.module').then(m => m.PaymentSplitModule)
            },
        ]
    },
    {
        path: 'counter-sales', loadChildren: () => import('./components/customer/counter-sales/counter-sales-new.module').then(m => m.CounterSalesModule)
    },
    { path: 'action-types', loadChildren: () => import('./components/customer/ticket-type/ticket-type.module').then(m => m.TicketTypeModule) },
    {
        path: 'ticket-groups', loadChildren: () => import('./components/customer/ticket-group/ticket-group.module').then(m => m.TicketGroupModule)
    },
    {
        path: 'safe-entry', loadChildren: () => import('./components/customer/safe-entry/safe-entry.module').then(m => m.SafeEntryModule)
    },
    {
        // path: 'relocation', loadChildren: () => import('./components/customer/customer-management/relocation/relocation.module').then(m => m.RelocationModule)
        path: 'relocation', loadChildren: () => import('./components/customer/customer-management/relocation/relocation-list.module').then(m => m.RelocationListModule)
    },
    {
        // path: 'client-testing', loadChildren: () => import('./components/customer/customer-management/client-testing/client-testing.module').then(m => m.ClientTestingModule)
        path: 'client-testing', loadChildren: () => import('./components/customer/customer-management/client-testing/client-testing.module').then(m => m.ClientTestingModule)
    },
    {
        path: 'all-tickets', loadChildren: () => import('./components/customer/all-tickets/all-tickets.module').then(m => m.AllTicketsModule)
    },
    { path: 'tech-allocation-calender-view', loadChildren: () => import('./components/customer/technician-installation/technician-allocation-calender-view/technician-allocation-calender-view.module').then(m => m.TechnicianAllocationCalenderViewModule) },
    { path: 'dealer-tech-allocation-calender-view', loadChildren: () => import('./components/customer/technician-installation/dealer-technician-allocation-calender-view/dealer-technician-allocation-calender-view.module').then(m => m.DealerTechnicianAllocationCalenderViewModule) },
    { path: 'inspect-allocation-calender-view', loadChildren: () => import('./components/customer/technician-installation/inspector-allocation-calender-view/inspector-allocation-calender-view.module').then(m => m.InspectorAllocationCalenderViewModule) },
    { path: 'stock-swap', loadChildren: () => import('./components/customer/technician-installation/stock-swap/stock-swap.module').then(m => m.StockSwapModule) },
    // { path: 'category', loadChildren: () => import('./components/customer/category/category-component.module').then(m => m.CustomerCategoryModule) },
    { path: 'upsell-quote', loadChildren: () => import('./components/customer/technician-installation/upsell-quote/upsell-quote-new.module').then(m => m.UpsellQuoteModule) },
    {
        path: 'dashboard', loadChildren: () => import('./components/dashboard/customer-dashboard.module').then(m => m.CustomerDashboardModule)
    },
    {
        path: 'advanced-search', loadChildren: () => import('./components/advanced-search/advanced-search.module').then(m => m.AdvanceSearchModule)
    },
    {
        path: 'non-customer-profile', loadChildren: () => import('./components/non-customer-profile/non-customer-profile.module').then(m => m.NonCustomerProfileModule)
    },
    {
        path: 'stand-by-roster', loadChildren: () => import('./components/customer/customer-management/standby-roster/standby-roster.module').then(m => m.ViewStandByRosterModule),
    },
    {
        path: 'add-promise-to-pay', loadChildren: () => import('./components/customer/customer-management/customer-add-edit/add-promise-to-pay/add-promise-to-pay.module').then(m => m.AddPromiseToPayModule),
    },
    {
        path: 'add-proof-of-payments', loadChildren: () => import('./components/customer/customer-management/customer-add-edit/add-proof-of-payments/add-proof-of-payments.module').then(m => m.AddProofOfPaymentsModule),
    },
    {
        path: 'add-payment-arrangement', loadChildren: () => import('./components/customer/customer-management/customer-add-edit/add-payment-arrangement/add-payment-arrangement.module').then(m => m.AddPaymentArrangementModule),
    },
    {
        path: 'view-payment-arrangement', loadChildren: () => import('./components/customer/customer-management/customer-add-edit/add-payment-arrangement/view-payment-arrangement.module').then(m => m.PaymentArrangementViewModule),
    },
    {
        path: 'request-extend-payment-arrangement',  loadChildren: () => import('./components/customer/customer-management/customer-add-edit/add-payment-arrangement/request-extend-payment-arrangement.module').then(m => m.RequestExtendPaymentArrangementModule),
    },
    {
        path: 'request-extend-payment-arrangement-view', loadChildren: () => import('./components/customer/customer-management/customer-add-edit/add-payment-arrangement/request-extend-payment-arrangement-view.module').then(m => m.RequestExtendPaymentArrangementViewModule),
    },
    {
        path: 'add-refuse-to-pay',loadChildren: () => import('./components/customer/customer-management/customer-add-edit/add-refuse-to-pay/add-refuse-to-pay.module').then(m => m.AddRefuseToPayModule),
    },
    {
        path: 'annual-network-fee-approval',loadChildren: () => import('./components/customer/customer-management/customer-add-edit/annual-network-fee-approval/annual-network-fee-approval.module').then(m => m.AnnualNetworkFeeApprovalModule),
    },


];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class CustomerRoutingModule { }
