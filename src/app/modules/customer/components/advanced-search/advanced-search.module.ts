import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AdvancedSearchComponent, AdvancedSearchListComponent } from "@modules/customer";
import {AccordionModule} from 'primeng/accordion';
import { SimpleSearchListComponent } from "./simple-search-list.component";
import { SimpleSearchComponent } from "./simple-search.component";

@NgModule({
  declarations: [AdvancedSearchComponent, AdvancedSearchListComponent,SimpleSearchComponent,SimpleSearchListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    AccordionModule,
    RouterModule.forChild([
        {
          path: '', component: AdvancedSearchComponent, data: { title: 'Advanced Search' }
        },
        {
          path: 'simple', component: SimpleSearchComponent, data: { title: 'Simple Search' }
        },
        {
          path: 'list', component: AdvancedSearchListComponent, data: { title: 'Advanced Search List' }
        },
        {
          path: 'debtor', loadChildren: () => import('./debtor-search/debtor-search.module').then(m => m.DebtorSearchModule)
        },
    ])
  ],
  exports: [AdvancedSearchComponent,SimpleSearchListComponent],
  entryComponents: [],
})
export class AdvanceSearchModule { }
