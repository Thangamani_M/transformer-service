import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  countryCodes, CrudType, CustomDirectiveConfig, debounceTimeForSearchkeyword, defaultCountryCode, emailPattern, filterFormControlNamesWhichHasValues, formConfigs, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix,
  prepareGetRequestHttpParams, prepareRequiredHttpParams, removePropsWhichHasNoValuesFromObject, RxjsService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection/shared/enum/collection.enum';
import { AddressSearchModel, AdvancedSearchModel, ClientSearchModel, CustomerModuleApiSuffixModels, DebtorSearchModel, OptionsSearchModel, RadioSearchModel, TechnicianSearchModel } from '@modules/customer';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { selectStaticEagerLoadingCustomerTypesState$, selectStaticEagerLoadingDebtorStatusState$, selectStaticEagerLoadingLeadGroupsState$, selectStaticEagerLoadingPaymentMethodsState$, selectStaticEagerLoadingSiteTypesState$ } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';

enum ACTION_TYPE {
  SALES = "Sales",
  TECHNICAL = 'Technical',
  SUBSCRIPTION = 'Subscription',
  DEBTOR = 'Debtor',
  TICKET='Ticket',
  TRANSACTION='Transaction'
}

@Component({
  selector: 'advanced-search',
  templateUrl: './advanced-search.component.html',
  styleUrls: ['./advanced-search.component.scss']
})

export class AdvancedSearchComponent extends PrimeNgTableVariablesModel {
  customerStatusTypes = [];
  @Input() fromUrl = '';
  customerTypes = [];
  divisions = [];
  selectedIndex = 0;
  regions = [];
  districts = [];
  branches = [];
  formConfigs = formConfigs;
  advancedSearchForm: FormGroup;
  isHypenOnly = new CustomDirectiveConfig({ isHypenOnly: true });
  alphanumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isSearchBtnDisabled = true;
  isClearBtnDisabled = true;
  countryCodes = countryCodes;
  subAreas = [];
  selectedOption;
  debtorStatuses = [];
  siteTypes = [];
  leadGroups = [];
  paymentMethods = [];
  reviewType = "";
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;
  creditControllers: any
  isExpanded = true
  isExpandedAll = true
  totalRecords = 0
  searchType='advanced'
  //Radio

  setNames = []
  decoders = []
  //Categories
  dataList: any[];
  menuItems = [];

  mobileValidations = {
    mobile1:11,
    premisesNo:11,
    contactNo:11,
    cellPanicNo:11,
  }


  primengTableConfigProperties: any = {
    tableCaption: "City",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'IT Management ', relativeRouterUrl: '' }, { displayName: 'City List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'City',
          dataKey: 'cityId',
          enableBreadCrumb: true,
          enableAction: true,
          enableAddActionBtn: true,
          enableStatusActiveAction: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: false,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'customerRefNo', header: 'Customer ID' },
          { field: 'customerName', header: 'Name' },
          { field: 'address', header: 'Address' ,isParagraph:true},
          { field: 'mobile1', header: 'Contact', },
          { field: 'status', header: 'Status' },
          { field: '', header: 'Action', width: '150px',toggleMenu:true,nofilter:true,hideSortIcon:true },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: UserModuleApiSuffixModels.CITIES,
          moduleName: ModulesBasedApiSuffix.IT_MANAGEMENT
        }]
    }
  }

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef, private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private router: Router) {
    super()
  }


  ngOnInit(): void {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.combineLatestNgrxStoreData();
    this.createAdvancedSearchForm();
    this.commonForkJoinRequestsForAllTabs();
    this.onFormControlChanges();
    this.getRegions();
    this.combineLatestNgrxForDebtorTab();
    this.menuItems = [
      // {
      //   label: 'Sales Info', data: {}, command: (event) => {
      //     this.doAction(ACTION_TYPE.SALES, this.menuItems[0].data);
      //   }
      // },
      // {
      //   label: 'Technical info', data: {}, command: (event) => {
      //     this.doAction(ACTION_TYPE.TECHNICAL, this.menuItems[1].data);
      //   }
      // },
      // {
      //   label: 'Subscription', data: {}, command: (event) => {
      //     this.doAction(ACTION_TYPE.SUBSCRIPTION, this.menuItems[2].data,);
      //   }
      // },
      // {
      //   label: 'Debtor info', data: {}, command: (event) => {
      //     this.doAction(ACTION_TYPE.DEBTOR, this.menuItems[3].data);
      //   }
      // },
      // {
      //   label: 'Ticket', data: {}, command: (event) => {
      //     this.doAction(ACTION_TYPE.TICKET, this.menuItems[4].data);
      //   }
      // },
      {
        label: 'Transactions', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.TRANSACTION, this.menuItems[0].data);
        }
      }
    ]
  }

  doAction(type?,data?){
    switch (type) {
      case ACTION_TYPE.TRANSACTION:
        this.router.navigate(['customer/manage-customers/view', data.customerId], { queryParams: { addressId: data.addressId,navigateTab:9 }, skipLocationChange: true })
        break;

      default:
        break;
    }
  }

  oggleMenu(menu, event, rowData) {
    this.menuItems.forEach((menuItem) => {
      menuItem.data = rowData;
    });
    menu.toggle(event);
  }

  commonForkJoinRequestsForAllTabs(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.UX_CUSTOMERSTATUS, undefined, true),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_DECODER_ACCOUNT_CONFIGURATIONS), this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SETNAMES),
      this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_COLLECTION_CREDIT_CONTROLLER)]
    ).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.customerStatusTypes = getPDropdownData(resp.resources);
              this.rxjsService.setGlobalLoaderProperty(false);
              break;
            case 1:
              this.decoders = resp.resources;
              break;
            case 2:
              this.setNames = resp.resources;
              break;
            case 3:
              this.creditControllers = resp.resources;
              break;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    });
  }

  combineLatestNgrxForDebtorTab(): void {
    combineLatest([this.store.select(selectStaticEagerLoadingDebtorStatusState$)])
      .subscribe((response) => {
        this.debtorStatuses = getPDropdownData(response[0]);
      });
  }

  getRegions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null,
        { countryId: formConfigs.countryId })).subscribe((response) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.isSuccess && response.statusCode == 200) {
            this.regions = response.resources;
          }
        });
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(selectStaticEagerLoadingCustomerTypesState$),
    this.store.select(selectStaticEagerLoadingLeadGroupsState$), this.store.select(selectStaticEagerLoadingSiteTypesState$),
    this.store.select(selectStaticEagerLoadingPaymentMethodsState$),
    this.activatedRoute.queryParams])
      .subscribe((response) => {
        this.customerTypes = getPDropdownData(response[0]);
        this.leadGroups = getPDropdownData(response[1]);
        this.siteTypes = getPDropdownData(response[2]);
        this.paymentMethods = response[3];
        this.reviewType = response[4]?.reviewType;
      });
  }

  createAdvancedSearchForm(): void {
    let advancedSearchModel = new AdvancedSearchModel();
    this.advancedSearchForm = this.formBuilder.group({});
    Object.keys(advancedSearchModel).forEach((key) => {
      switch (key) {
        case "clientSearchModel":
          this.advancedSearchForm.addControl(key, this.createClientSearchModelForm());
          break;
        case "addressSearchModel":
          this.advancedSearchForm.addControl(key, this.createAddressSearchModelForm());
          break;
        case "debtorSearchModel":
          this.advancedSearchForm.addControl(key, this.createDebtorSearchModelForm());
          break;
        case "technicianSearchModel":
          this.advancedSearchForm.addControl(key, this.createTechnicianSearchModelForm());
          break;
        case "radioSearchModel":
          this.advancedSearchForm.addControl(key, this.createRadioSearchModelForm());
          break;
        case "optionsSearchModel":
          this.advancedSearchForm.addControl(key, this.createOptionsSearchModelForm());
          break;
        default:
          this.advancedSearchForm.addControl(key, new FormControl(advancedSearchModel[key]));
          break;
      }
    });
    this.onFormControlChange()
  }

  createClientSearchModelForm(clientSearchModel?: ClientSearchModel): FormGroup {
    let ClientSearchFormModel = new ClientSearchModel(clientSearchModel);
    let formControls = {};
    Object.keys(ClientSearchFormModel).forEach((key) => {
      formControls[key] = new FormControl(ClientSearchFormModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createAddressSearchModelForm(addressSearchModel?: AddressSearchModel): FormGroup {
    let addressSearchFormModel = new AddressSearchModel(addressSearchModel);
    let formControls = {};
    Object.keys(addressSearchFormModel).forEach((key) => {
      formControls[key] = new FormControl(addressSearchFormModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createTechnicianSearchModelForm(technicianSearchModel?: TechnicianSearchModel): FormGroup {
    let technicianSearchFormModel = new TechnicianSearchModel(technicianSearchModel);
    let formControls = {};
    Object.keys(technicianSearchFormModel).forEach((key) => {
      formControls[key] = new FormControl(technicianSearchFormModel[key]);
    });
    return this.formBuilder.group(formControls);
  }
  createRadioSearchModelForm(radioSearchModelObj?: RadioSearchModel): FormGroup {
    let radioSearchModel = new RadioSearchModel(radioSearchModelObj);
    let formControls = {};
    Object.keys(radioSearchModel).forEach((key) => {
      formControls[key] = new FormControl(radioSearchModel[key]);
    });
    return this.formBuilder.group(formControls);
  }
  createOptionsSearchModelForm(optionsSearchModelObj?: OptionsSearchModel): FormGroup {
    let optionSearchModel = new OptionsSearchModel(optionsSearchModelObj);
    let formControls = {};
    Object.keys(optionSearchModel).forEach((key) => {
      formControls[key] = new FormControl(optionSearchModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createDebtorSearchModelForm(debtorSearchModel?: DebtorSearchModel): FormGroup {
    let debtorSearchFormModel = new DebtorSearchModel(debtorSearchModel);
    let formControls = {};
    Object.keys(debtorSearchFormModel).forEach((key) => {
      if (key == 'email') {
        formControls[key] = new FormControl(debtorSearchFormModel[key], Validators.compose([Validators.email, emailPattern]));
      }
      else {
        formControls[key] = new FormControl(debtorSearchFormModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  get addressSearchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('addressSearchModel') as FormGroup;
  }

  get clientSearchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('clientSearchModel') as FormGroup;
  }

  get debtorSearchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('debtorSearchModel') as FormGroup;
  }

  get technicianSearchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('technicianSearchModel') as FormGroup;
  }
  get radioSearchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('radioSearchModel') as FormGroup;
  }
  get optionsSarchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('optionsSearchModel') as FormGroup;
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onFormControlChanges(): void {
    this.advancedSearchForm.valueChanges
      .pipe(
        map((object) => filterFormControlNamesWhichHasValues(object)))
      .subscribe((keyValuesObj) => {
        if (this.addressSearchFormGroupControls.get('tempMainAreaName').invalid) {
          this.addressSearchFormGroupControls.get('tempMainAreaName').clearValidators();
          this.addressSearchFormGroupControls.get('tempMainAreaName').setErrors(null);
          this.addressSearchFormGroupControls.updateValueAndValidity();
        }
        this.isSearchBtnDisabled = (Object.keys(keyValuesObj).length == 0 || this.advancedSearchForm.invalid) ? true : false;
        this.isClearBtnDisabled = Object.keys(keyValuesObj).length > 0 ? false : true;
      });
    this.advancedSearchForm.get('regionId').valueChanges.subscribe((regionId: string) => {
      if (!regionId) return;
      this.divisions = []
      this.districts = []
      this.branches = []
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { regionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources && response.isSuccess) {
            this.divisions = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.advancedSearchForm.get('divisionId').valueChanges.subscribe((divisionId: string) => {
      if (!divisionId) return;
      this.districts = []
      this.branches = []
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources && response.isSuccess) {
            this.districts = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.advancedSearchForm.get('districtId').valueChanges.subscribe((districtId: string) => {
      if (!districtId) return;
      this.branches = []
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null,
          { districtId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources && response.isSuccess) {
            this.branches = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.onTempMainAreaNameFormControlValueChanges();
  }

  ngAfterViewInit() {
    this.changeDetectorRef.detectChanges()
  }

  onTempMainAreaNameFormControlValueChanges(): void {
    this.addressSearchFormGroupControls.get('tempMainAreaName')
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((search: IApplicationResponse) => {
          this.getLoopableObjectRequestObservable = this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.SALES_API_UX_MAIN_AREA, null, true,
            prepareGetRequestHttpParams(null, null, { search }))
        });
  }

  onSelectedItemOption(selectedObject): void {
    if (!selectedObject) return;
    this.selectedOption = selectedObject;
    this.subAreas = [];
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_SUB_AREA, undefined, false, prepareRequiredHttpParams({
        mainAreaIds: selectedObject['mainAreaId']
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.subAreas = response.resources;
          this.addressSearchFormGroupControls.patchValue({ mainAreaIds: this.selectedOption ? this.selectedOption['mainAreaId'] : "" });
          this.addressSearchFormGroupControls.patchValue({ tempMainAreaName: this.selectedOption ? this.selectedOption['mainAreaName'] : "" }, { emitEvent: false, onlySelf: true });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onFormControlChange(){

    this.clientSearchFormGroupControls
      .get("mobile1CountryCode")
      .valueChanges.subscribe((mobileNumber2: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.clientSearchFormGroupControls.get("mobile1CountryCode").value,this.clientSearchFormGroupControls,'mobile1');
      });

      this.clientSearchFormGroupControls
      .get("premisesNoCountryCode")
      .valueChanges.subscribe((mobileNumber2: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.clientSearchFormGroupControls.get("premisesNoCountryCode").value,this.clientSearchFormGroupControls,'premisesNo');
      });

      this.clientSearchFormGroupControls
      .get("contactNoCountryCode")
      .valueChanges.subscribe((mobileNumber2: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.clientSearchFormGroupControls.get("contactNoCountryCode").value,this.clientSearchFormGroupControls,'contactNo');
      });


      // this.radioSearchFormGroupControls
      // .get("cellPanicNoCountryCode")
      // .valueChanges.subscribe((mobileNumber2: string) => {
      //   this.setPhoneNumberLengthByCountryCode(
      //     this.radioSearchFormGroupControls.get("cellPanicNoCountryCode").value,this.radioSearchFormGroupControls,'cellPanicNo');
      // });

  }

  setPhoneNumberLengthByCountryCode(countryCode: string, formGroup:FormGroup,formControlName) {
    switch (countryCode) {
      case "+27":

        formGroup
          .get(formControlName)
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
          this.mobileValidations[formControlName] = formConfigs.southAfricanContactNumberMaxLength;
          console.log("this.mobileValidations",this.mobileValidations)
        break;
      default:

        formGroup
          .get(formControlName)
          .setValidators([
            Validators.minLength(
              formConfigs.indianContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.indianContactNumberMaxLength
            ),
          ]);
          this.mobileValidations[formControlName] = formConfigs.indianContactNumberMaxLength;

          console.log("this.mobileValidations",this.mobileValidations)

        break;
    }
  }


  onSubmit(pageIndex?, maximumRows?, searchObj?) {
    this.advancedSearchForm.updateValueAndValidity();
    this.advancedSearchForm.value.customerTypeId = this.advancedSearchForm.value.customerTypeId && this.advancedSearchForm.value.customerTypeId.join(',');
    this.advancedSearchForm.value.customerStatusId = this.advancedSearchForm.value.customerStatusId && this.advancedSearchForm.value.customerStatusId.join(',');
    let data: any = {};
    this.advancedSearchForm.value.userId = this.loggedInUserData?.userId
    Object.keys(this.advancedSearchForm.value).forEach((key) => {
      if (key == 'clientSearchModel' || key == 'addressSearchModel' || key == 'debtorSearchModel' || key == 'technicianSearchModel' || key == 'optionsSearchModel' || key == "radioSearchModel") {
        return;
      }
      data[key] = this.advancedSearchForm.value[key];
      data = removePropsWhichHasNoValuesFromObject(data);
    });
   let debtorSearchFormGroupControls =  this.debtorSearchFormGroupControls.getRawValue();
    debtorSearchFormGroupControls.debtorStatus = debtorSearchFormGroupControls.debtorStatus && debtorSearchFormGroupControls.debtorStatus.join(',');
    debtorSearchFormGroupControls.debtorSiteTypeId = debtorSearchFormGroupControls.debtorSiteTypeId && debtorSearchFormGroupControls.debtorSiteTypeId.join(',');
    debtorSearchFormGroupControls.debtorGroup = debtorSearchFormGroupControls.debtorGroup && debtorSearchFormGroupControls.debtorGroup.join(',');

    if (this.clientSearchFormGroupControls.value.mobile1) {
      this.clientSearchFormGroupControls.value.mobile1 = this.clientSearchFormGroupControls.value.mobile1.toString().replace(/\s/g, "");
    }

    if (this.clientSearchFormGroupControls.value.premisesNo) {
      this.clientSearchFormGroupControls.value.premisesNo = this.clientSearchFormGroupControls.value.premisesNo.toString().replace(/\s/g, "");
    }

    if (this.clientSearchFormGroupControls.value.contactNo) {
      this.clientSearchFormGroupControls.value.contactNo = this.clientSearchFormGroupControls.value.contactNo.toString().replace(/\s/g, "");
    }

    if (this.radioSearchFormGroupControls.value.cellPanicNo) {
      this.radioSearchFormGroupControls.value.cellPanicNo = this.radioSearchFormGroupControls.value.cellPanicNo.toString().replace(/\s/g, "");
    }


    data = {
      ...data, ...removePropsWhichHasNoValuesFromObject(this.clientSearchFormGroupControls.value),
      ...removePropsWhichHasNoValuesFromObject(debtorSearchFormGroupControls),
      ...removePropsWhichHasNoValuesFromObject(this.addressSearchFormGroupControls.value),
      ...removePropsWhichHasNoValuesFromObject(this.radioSearchFormGroupControls.value),
      ...removePropsWhichHasNoValuesFromObject(this.technicianSearchFormGroupControls.value),
      ...removePropsWhichHasNoValuesFromObject(this.optionsSarchFormGroupControls.value)
    };

    for (let key in data) {
      let dataCopy = JSON.parse(JSON.stringify(data));
      if (key == 'mainAreaName') {
        data['mainAreaIds'] = dataCopy['mainAreaName'];
        delete data['mainAreaName'];
      }
      if (key == 'subAreaName') {
        data['subAreaId'] = dataCopy['subAreaName'];
        delete data['subAreaName'];
      }
      if (key == 'tempMainAreaName') {
        delete data['tempMainAreaName'];
      }
    }

    data.pageIndex = pageIndex | 0
    data.maximumRows = maximumRows | 10
    data = { ...data, ...searchObj }

    this.crudService.create(ModulesBasedApiSuffix.SHARED, CustomerModuleApiSuffixModels.CUSTOMER_ADVANCED_SEARCH, data).subscribe(response => {
      if (response.isSuccess) {
        this.isExpandedAll = false;
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
      } else {
        this.isExpandedAll = true;
      }
    })
  }

  onClear() {
    this.clientSearchFormGroupControls.reset({ premisesNoCountryCode: defaultCountryCode, mobile1CountryCode: defaultCountryCode,contactNoCountryCode :defaultCountryCode});
    this.radioSearchFormGroupControls.reset({ cellPanicNoCountryCode: defaultCountryCode });
    this.addressSearchFormGroupControls.reset({ subAreaName: "" });
    this.debtorSearchFormGroupControls.reset(new DebtorSearchModel());
    this.technicianSearchFormGroupControls.reset(new TechnicianSearchModel());
    this.divisions = [];
    this.districts = [];
    this.branches = [];
    this.advancedSearchForm.patchValue({
      customerTypeId: [], customerStatusId: [],
      regionId: '', divisionId: '', districtId: '', branchId: ''
    });
  }
  onTabOpen(event) {
    this.isExpandedAll = true
    this.isExpanded = true
  }
  onTabClose(event) {
    this.isExpandedAll = false
  }


  onCRUDRequested(type: CrudType | string, row?: object | any, searchObj?): void {
    switch (type) {
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.onSubmit(
              row["pageIndex"], row["pageSize"], searchObj);
            break;
        }
        break;

      case CrudType.VIEW:
        this.router.navigate(['customer/manage-customers/view', row.customerId], { queryParams: { addressId: row.addressId }, skipLocationChange: true })
        break;
    }
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  navigateToSimpleSearch(){
    this.router.navigate(['/customer/advanced-search/simple'],{skipLocationChange:true})
  }
}
