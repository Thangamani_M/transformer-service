import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, mobileNumberSpaceMaskingReplaceFromFormValues, ModulesBasedApiSuffix,
  prepareGetRequestHttpParams, RxjsService
} from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { CallType } from '../customer/technician-installation/customer-technical/call-type.enum';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
import { MomentService } from '@app/shared/services/moment.service';

@Component({
  selector: 'advanced-search-list',
  templateUrl: './advanced-search-list.component.html'
})

export class AdvancedSearchListComponent extends PrimeNgTableVariablesModel {
  params;
  selectedIndex;
  searchColumns;
  columnFilterForm: FormGroup;
  row: any = {};
  searchForm: FormGroup;
  customerDetails;
  searchType = "";
  primengTableConfigProperties;
  fromUrl = "";
  getVariancedata;
  getReviewType;
  pageSize: any

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private tableFilterFormService: TableFilterFormService, private datePipe: DatePipe,
    private rxjsService: RxjsService, public dialogService: DialogService,
    private route: Router, private store: Store<AppState>, private momentService: MomentService,
    private crudService: CrudService) {
    super();
    this.activatedRoute.queryParams.subscribe(params => {
      this.selectedIndex = params?.selectedIndex ? +params?.selectedIndex : 0;
      this.searchType = params?.searchType;
      this.getReviewType = params?.reviewType;
      this.fromUrl = params?.fromUrl;
      this.params = params.data ? JSON.parse(params.data) : '';
      this.columnFilterForm = this.formBuilder.group({});
      this.searchForm = this.formBuilder.group({ searchKeyword: "" });
      this.getDataBySearchType(null, null, { ...mobileNumberSpaceMaskingReplaceFromFormValues(this.params, ["premisesNo", "mobile1"]), ...this.params });
    });
    this.getVariancedata = localStorage.getItem('techVarianceReport');
    this.primengTableConfigProperties = {
      tableCaption: "",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Customer Tickets', relativeRouterUrl: '' }, { displayName: 'Ticket Groups' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Advance Search List',
            dataKey: (this.searchType == 'client' || this.searchType == 'address') ? 'id' : 'customerId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableAddActionBtn: true,
            enableFieldsSearch: (this.searchType == 'client' || this.searchType == 'address') ? false : true,
            rowExpantable: (this.searchType == 'client' || this.searchType == 'address') ? true : false,
            rowExpantableIndex: (this.searchType == 'client' || this.searchType == 'address') ? 0 : null,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: this.searchType == 'client' ?
              [{ field: 'customerRefNo', header: 'Customer ID', isVisible: true },
              { field: 'firstName', header: 'First Name', isVisible: true },
              { field: 'lastName', header: 'Last Name', isVisible: true },
              { field: 'address', header: 'Client Address', isVisible: true },
              { field: 'mobile1', header: 'Contact', isVisible: true },
              { field: 'mobile1', header: 'Action', isVisible: this.getReviewType == 'tech_variance' ? true : false }] :
              this.searchType == 'debtor' ?
                [{ field: 'debtorRefNo', header: "Debtor's Code", isVisible: true },
                { field: 'debtorName', header: 'Debtor Name', isVisible: true },
                { field: 'mobile1', header: 'Phone', isVisible: true, isPhoneMask: true },
                { field: 'balance', header: 'OutStanding Balance', isVisible: true, isDecimalFormat: true },
                { field: 'siteAddress', header: 'Billing Address', isVisible: true },
                { field: 'noContracts', header: 'No Of Contracts', isVisible: true },
                { field: 'bdiNo', header: 'BDI Number', isVisible: true },
                { field: 'mobile1', header: 'Action', isVisible: this.getReviewType == 'tech_variance' ? true : false }] :
                this.searchType == 'technical' ?
                  [{ field: 'serviceCallNo', header: "Service Call No", isVisible: true },
                  { field: 'status', header: 'Status', isVisible: true },
                  { field: 'techName', header: 'Tech Name', isVisible: true },
                  { field: 'techArea', header: 'Tech Area', isVisible: true },
                  { field: 'creationDate', header: 'Creation Date', isVisible: true },
                  { field: 'scheduleDate', header: 'Schedule Date', isVisible: true },
                  { field: 'customerRefNo', header: 'Cust Code', isVisible: true },
                  { field: 'referToSaleFlag', header: 'Refer to Sales Flag', isVisible: true },
                  { field: 'faultDescription', header: 'Fault Description', isVisible: true },
                  { field: 'jobType', header: 'Job Type', isVisible: true },
                  { field: 'diagnosis', header: 'Diagnosis', isVisible: true },
                  { field: 'client', header: 'Cust Name', isVisible: true },
                  { field: 'address', header: 'Cust Address', isVisible: true },
                  { field: 'mobile1', header: 'Action', isVisible: this.getReviewType == 'tech_variance' ? true : false }] :
                  this.searchType == 'address' ?
                    [{ field: 'customerRefNo', header: 'Customer ID', isVisible: true },
                    { field: 'client', header: 'Client', isVisible: true },
                    { field: 'address', header: 'Client Address', isVisible: true },
                    { field: 'mobile1', header: 'Contact', isVisible: true },
                    { field: 'mobile1', header: 'Action', isVisible: this.getReviewType == 'tech_variance' ? true : false }] : [],
            shouldShowCreateActionBtn: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: SalesModuleApiSuffixModels.TICKET_GROUP,
            moduleName: ModulesBasedApiSuffix.SALES
          }]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setCounterSaleProperty(this.fromUrl);
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      });
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    if (!row['searchColumns']) {
      row['searchColumns'] = this.params;
    }
    else {
      row['searchColumns'] = event.filters;
    }
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }


  getDataBySearchType(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    let modulesBasedApiSuffix = (this.searchType == 'client' || this.searchType == 'debtor') ? ModulesBasedApiSuffix.SALES : (this.searchType == 'technical') ? ModulesBasedApiSuffix.TECHNICIAN :
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT;
    let moduleApiSuffixModels = this.searchType == 'client' ? SalesModuleApiSuffixModels.ADVANCED_SEARCH_CLIENT :
      this.searchType == 'debtor' ? SalesModuleApiSuffixModels.ADVANCED_SEARCH_DEBTORS : this.searchType == "technical" ? TechnicalMgntModuleApiSuffixModels.ADVANCE_CUSTOMER_SEARCH :
        CustomerModuleApiSuffixModels.SEARCH_CUSTOMER_ADDRESS;
    this.loading = true;
    this.crudService.get(
      modulesBasedApiSuffix, moduleApiSuffixModels,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        ...otherParams
      })).pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
          res?.resources?.forEach(val => {
            switch (this.selectedIndex) {
              case 5:
                val.creationDate = val.creationDate ? this.datePipe.transform(val.creationDate, 'dd-MM-yyyy, h:mm:ss a') : '';
                val.scheduleDate = val.scheduleDate ? this.datePipe.transform(val.scheduleDate, 'dd-MM-yyyy, h:mm:ss a') : '';
                break;
            }
            return val;
          })
        }
        return res;
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          if (this.searchType == 'client' || this.searchType == 'address') {
            response.resources.forEach((data) => {
              data.id = data.addressId + data.customerId;
            });
          }
          this.dataList = response.resources;
          this.totalRecords = response.totalCount;
          this.customerDetails = response.resources[0];
        }
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  details(e) {
    this.customerDetails = e;
  }

  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.VIEW:
        if (this.searchType == 'client'||this.searchType == 'address') {
          this.navigateToCustomerViewPage(row, 0);
        }
        this.openViewPage(row);
        break;
      case CrudType.EDIT:
        if (this.searchType == 'debtor') {
          this.navigateToCustomerViewPage(row);
        } else if (this.searchType == 'technical') {
          this.route.navigate(['customer/manage-customers/call-initiation'], { queryParams: { customerId: row['customerId'], customerAddressId: row['customerAddressId'], initiationId: row['initiationId'], callType: row['callTypeId'], appointmentId: row['appointmentId'] ? row['appointmentId'] : null } });
        }
        else {
          this.navigateToCustomerViewPage(row);
        }
        break
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (CrudType.GET === type && Object.keys(row).length > 0) {
          Object.keys(row['searchColumns'] ? row['searchColumns'] : {}).forEach((key) => {
            otherParams[key] = row['searchColumns'][key]['value'];
            if (key == 'ticketStatusName') {
              let status = row['searchColumns'][key]['value'];
              key = 'ticketStatusId';
              otherParams[key] = status.id;
              delete otherParams['ticketStatusName'];
            } else if (key == 'usergroupName') {
              let status = row['searchColumns']['usergroupName'];
              key = 'userGroup';
              otherParams[key] = status;
              delete otherParams['usergroupName'];
            } else if (key == 'ticketTypeName') {
              let status = row['searchColumns']['ticketTypeName'];
              key = 'ticketType';
              otherParams[key] = status;
              delete otherParams['ticketTypeName'];
            } else if (key == 'assignedToName') {
              let status = row['searchColumns']['assignedToName'];
              key = 'assignedTo';
              otherParams[key] = status;
              delete otherParams['assignedToName'];
            } else if (key.toLowerCase().includes('date') && this.row['searchColumns'][key] instanceof Date) {
              otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
            }
            else {
              otherParams[key] = this.row['searchColumns'][key];
            }
          });
          otherParams['sortOrder'] = row['sortOrder'] ? row['sortOrder'] : "ASC";
          if (row['sortOrderColumn']) {
            otherParams['sortOrderColumn'] = row['sortOrderColumn'];
          }
        }
        this.getDataBySearchType(row["pageIndex"], row["pageSize"], { ...mobileNumberSpaceMaskingReplaceFromFormValues(this.params, ["premisesNo", "mobile1"]), ...this.params, ...otherParams });
        break;
    }
  }

  navigateToCustomerViewPage(row, selectedTabIndex = 4) {
    this.rxjsService.setViewCustomerData({
      customerId: row['customerId'],
      addressId: row['addressId'],
      customerTab: selectedTabIndex,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  openViewPage(row) {
    switch (this.selectedIndex) {
      case 2:
        this.route.navigate(["customer/advanced-search/debtor"], {
          queryParams: {
            searchType: this.searchType,
            fromUrl: this.fromUrl,
            data: JSON.stringify(this.params),
            selectedIndex: this.selectedIndex,
            reviewType: this.getReviewType,
            debtorId: row['debtorId'],
            row: JSON.stringify(row),
          }, skipLocationChange: true
        });
        break;
      case 5:
        if (row?.url) {
          this.route.navigateByUrl(row?.url);
        }
        break;
    }
  }

  navigateToList() {
    this.route.navigate(['customer/advanced-search'], {
      queryParams: {
        reviewType: this.getReviewType
      }, skipLocationChange: true
    });
  }

  /* Tech stock take approval customer invoice */
  isCusInvoiceSuccessDialog: boolean = false;
  customerRefNo: any;
  serviceCallResponse: any;

  createCustomerInvoice(rowData: any) {
    let requestbody = {};
    if (this.getVariancedata?.requestType == 'Variance_Report') {
      requestbody = {
        "CustomerId": rowData?.customerId,
        "AddressId": rowData?.addressId,
        "CreatedUserId": this.loggedInUserData.userId,
        "Items": JSON.parse(this.getVariancedata)
      }
    }
    else {
      let data = JSON.parse(this.getVariancedata);
      requestbody = {
        "CustomerId": rowData?.customerId,
        "AddressId": rowData?.addressId,
        "CreatedUserId": this.loggedInUserData.userId,
        "TechStockTakeTAMInvestigationItemId": data?.techStockTakeTAMInvestigationItemId,
        "ItemId": data?.itemId,
        "Qty": data?.qty,
        "Price": data?.price,
        "SerialNumber": data?.serialNumber
      }
    }
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
      this.getVariancedata?.requestType == 'Variance_Report' ?
        TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_VARIANCE_REPORT_SERVICE_CALL :
        TechnicalMgntModuleApiSuffixModels.TECH_STOCK_TAKE_INVESTIGATION_SERVICE_CALL,
      requestbody).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.isCusInvoiceSuccessDialog = true;
          this.customerRefNo = rowData?.customerRefNo;
          this.serviceCallResponse = response.resources;
        }
        else {
          this.isCusInvoiceSuccessDialog = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  navigateToSericeCallPage() {
    this.route.navigate(['/customer/manage-customers/call-initiation'], {
      queryParams: {
        customerId: this.serviceCallResponse.customerId,
        customerAddressId: this.serviceCallResponse.addressId,
        initiationId: this.serviceCallResponse.callInitiationId,
        appointmentId: null,
        callType: this.serviceCallResponse.technicianCallTypeName == CallType.INSTALLATION_CALL ? 1 : (this.serviceCallResponse.technicianCallTypeName == CallType.SERVICE_CALL ? 2 : 3) // 2- service call, 1- installation call
      }, skipLocationChange: false
    });
  }
}
