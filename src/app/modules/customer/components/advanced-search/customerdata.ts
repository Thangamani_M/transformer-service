export const customerData = [
  {
    "customerId": "7ddd3433-b6b3-4f43-b3ff-898bd3677690",
    "addressId": "efcee61a-3931-4f3e-a706-994d45a893c2",
    "customerRefNo": "CUS2301C4ECFE1",
    "customerName": "Karthik",
    "lastName": "N",
    "mobile1": "+27 869879878",
    "address": "22, Wer, 446, Rigel Avenue South, Erasmusrand, Pretoria, Gauteng, 0181",
    "isActive":true,
"customerAdditionalInfo": {
      "customerTypeName": "Commercial",
      "ownership": "Rented",
      "siteTypeName": "Residential",
      "divisionName": "CT ",
      "districtName": "CPT-North",
      "mobile2": "+27 454564564",
      "subArea": null,
      "origin": null
    }
  },
  {
    "customerId": "bcd74b91-69f3-4ae0-b9d9-eea8328ab137",
    "addressId": "f1afc3b8-c85a-415e-8802-36185787cfe6",
    "customerRefNo": "CUS2211498CE20",
    "customerName": "Nagaraj D",
    "mobile1": "+27 567567676",
    "address": "446, Rigel Avenue South, Erasmusrand, Pretoria, Gauteng, 0181",
    "isActive":true,
"customerAdditionalInfo": {
      "customerTypeName": "Commercial",
      "ownership": "Rented",
      "siteTypeName": "Residential",
      "divisionName": "PT",
      "districtName": "PTA",
      "mobile2": "+27 567567673",
      "subArea": null,
      "origin": null
    }
  },
  {
    "customerId": "de48fb5b-0257-412c-91e4-abe771263289",
    "addressId": "6ea840d0-fb30-467d-8bd2-debbe5f4608b",
    "customerRefNo": "CAF221195A92DF4",
    "customerName": "Karthikkumar N",
    "mobile1": "+27 121212121",
    "address": "56, 678, 446, Rigel Avenue South, Erasmusrand, Pretoria, Gauteng, 0181",
    "isActive":true,
"customerAdditionalInfo": {
      "customerTypeName": "Commercial",
      "ownership": "Rented",
      "siteTypeName": "Commercial",
      "divisionName": "CT ",
      "districtName": "CPT-North",
      "mobile2": "+27 121212122",
      "subArea": null,
      "origin": null
    }
  },
  {
    "customerId": "298f9045-797a-4d4d-913b-44c763c88972",
    "addressId": "fede3fe3-eff1-4263-81e5-025f8a722c01",
    "customerRefNo": "HUB220792EAD5FD",
    "customerName": "Venkat",
    "mobile1": "+27 456156125",
    "address": "21, 123rd Street, Seshego-F, Polokwane, Limpopo, 0751",
    "isActive":true,
"customerAdditionalInfo": {
      "customerTypeName": "Commercial",
      "ownership": "Rented",
      "siteTypeName": "NA",
      "divisionName": "JH ",
      "districtName": "Inland-Outlying",
      "mobile2": "+27 785162312",
      "subArea": null,
      "origin": null
    }
  },
  {
    "customerId": "ce44fcaf-92c0-426c-a6ff-e6418814f27b",
    "addressId": "e2bc40ed-9c61-4e14-8b45-34c07043ef79",
    "customerRefNo": "NC22079F08AA2",
    "customerName": "Ryashan Casual guarding",
    "mobile1": "+27 121212121",
    "address": "446, Rigel Avenue South, Erasmusrand, Pretoria, Gauteng, 0181",
    "isActive":true,
"customerAdditionalInfo": {
      "customerTypeName": "Commercial",
      "ownership": "Rented",
      "siteTypeName": "NA",
      "divisionName": "CT ",
      "districtName": "CPT-North",
      "mobile2": "+27 212122123",
      "subArea": null,
      "origin": null
    }
  },
  {
    "customerId": "74625946-d0b8-45f5-a5f4-09f2501f2ad8",
    "addressId": "e2bc40ed-9c61-4e14-8b45-34c07043ef79",
    "customerRefNo": "NC22079D858548",
    "customerName": "Ryashan Casual guarding",
    "mobile1": "+27 121212121",
    "address": "446, Rigel Avenue South, Erasmusrand, Pretoria, Gauteng, 0181",
    "isActive":true,
"customerAdditionalInfo": {
      "customerTypeName": "Commercial",
      "ownership": "Rented",
      "siteTypeName": "NA",
      "divisionName": "CT ",
      "districtName": "CPT-North",
      "mobile2": "+27 212122123",
      "subArea": null,
      "origin": null
    }
  },
  {
    "customerId": "45aa3838-1c22-4ed5-91e4-a2115c01f31a",
    "addressId": "9d1a0b34-3ae0-4cd9-9766-a3ffe5e53166",
    "customerRefNo": "NC2207382DF393",
    "customerName": "Screen Freeze",
    "mobile1": "+27 234234234",
    "address": "guard panic, The Manhattan, 17, Biccard Street, Braamfontein, Johannesburg, Gauteng, 2001",
    "isActive":true,
"customerAdditionalInfo": {
      "customerTypeName": "Commercial",
      "ownership": "Rented",
      "siteTypeName": "NA",
      "divisionName": "JH ",
      "districtName": "JHB-North",
      "mobile2": "+27 ",
      "subArea": null,
      "origin": null
    }
  },
  {
    "customerId": "a4265a41-1987-4f63-8b16-b4b13f2b2f14",
    "addressId": "880fe58a-c8b9-4df1-bb9e-5cd58de19a31",
    "customerRefNo": "CUS2206B5967D1",
    "customerName": "Cole Heugh",
    "mobile1": "+27 684284966",
    "address": "16, Creation Business park, 2, Computer Road, Marconi Beam, Milnerton, Western Cape",
    "isActive":true,
"customerAdditionalInfo": {
      "customerTypeName": "Commercial",
      "ownership": "Rented",
      "siteTypeName": "Residential",
      "divisionName": "CT ",
      "districtName": "CPT-North",
      "mobile2": "+27 837869204",
      "subArea": null,
      "origin": null
    }
  },
  {
    "customerId": "68abbe7f-a82e-4ea6-b06c-7bc206507624",
    "addressId": "aa9768f7-2114-4859-87af-a3dbf73785a9",
    "customerRefNo": "CUS2206FD2EFFB",
    "customerName": "Amiena Carelse",
    "mobile1": "+27 684284966",
    "address": "16, Creation business park, 2, Computer Road, Marconi Beam, Milnerton, Western Cape",
    "isActive":true,
"customerAdditionalInfo": {
      "customerTypeName": "Commercial",
      "ownership": "Rented",
      "siteTypeName": "Residential",
      "divisionName": "CT ",
      "districtName": "CPT-North",
      "mobile2": "+27 837869204",
      "subArea": null,
      "origin": null
    }
  },
  {
    "customerId": "b0e4b08e-b3f9-4076-88cc-5763b308d368",
    "addressId": "48d532e0-47cd-4c92-863c-c31635eb8fd4",
    "customerRefNo": "CUS22066FCA417",
    "customerName": "Rocky Haley",
    "mobile1": "+27 835577611",
    "address": "5, Woodleigh Avenue, Rivonia, Sandton, Gauteng, 2191",
    "isActive":true,
"customerAdditionalInfo": {
      "customerTypeName": "Commercial",
      "ownership": "Rented",
      "siteTypeName": "Commercial",
      "divisionName": "JH ",
      "districtName": "JHB-North",
      "mobile2": "+27 823783109",
      "subArea": null,
      "origin": null
    }
  }
]
