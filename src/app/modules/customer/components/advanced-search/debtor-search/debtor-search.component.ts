import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, OtherService, prepareGetRequestHttpParams, prepareRequiredHttpParams, PrimengStatusConfirmDialogComponent, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest} from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-debtor-search',
  templateUrl: './debtor-search.component.html'
})
export class DebtorSearchComponent extends PrimeNgTableVariablesModel implements OnInit {
  selectedIndex: number;
  isLoading: boolean;
  isSubmitted: boolean;
  listSubscribtion: any;
  dataList: any;
  dateFormat = 'MMM dd, yyyy';
  observableResponse: any;
  loggedInUserData: LoggedInUserModel;
  row: any = {};
  first: any = 0;
  filterData: any;
  searchType: string;
  reviewType: string;
  fromUrl: any;
  params: any;
  debtorId: string;
  isShowNoRecords: any = true;
  debtorConfigDetail: any = {};
  debtorSelectedDetail: any = {};

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private otherService: OtherService, private store: Store<AppState>, private dialogService: DialogService, private activatedRoute: ActivatedRoute,
    private router : Router) {
      super()
    this.activatedRoute.queryParams.subscribe(params => {
      this.selectedIndex = params?.selectedIndex ? +params?.selectedIndex : 0;
      this.searchType = params?.searchType;
      this.reviewType = params?.reviewType;
      this.fromUrl = params?.fromUrl;
      this.params = params?.data ? JSON.parse(params?.data) : '';
      this.debtorId = params?.debtorId;
      this.debtorSelectedDetail = params?.row ? JSON.parse(params?.row) : '';
    });
    this.primengTableConfigProperties = {
      tableCaption: 'Client Search Results',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Customer Management', relativeRouterUrl: '' }, { displayName: 'View Customer Details', relativeRouterUrl: '' },
      { displayName: 'Advance Search', relativeRouterUrl: '/customer/advanced-search', isSkipLocationChange: true }, {
        displayName: 'Client Search Results', relativeRouterUrl: '/customer/advanced-search/list',
        queryParams: { searchType: this.searchType, fromUrl: this.fromUrl, data: JSON.stringify(this.params), selectedIndex: this.selectedIndex, reviewType: this.reviewType, }, isSkipLocationChange: true
      },
      { displayName: 'Results', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Client Search Results',
            dataKey: 'commissionConfigId',
            enableAction: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'customerRefNo', header: 'Customer ID', width: '120px' },
              { field: 'customerName', header: 'Customer Name', width: '200px' },
              { field: 'ContractRefNo', header: 'Contract No', width: '120px' },
              { field: 'address', header: 'Client Address', width: '300px' },
              { field: 'outstandingBalance', header: 'Outstanding Balance', width: '150px', isDecimalFormat: true },
              { field: 'subscription', header: 'Action', width: '120px', isLink: true, nosort: true },
              { field: 'transaction', header: '', width: '120px', isLink: true, nosort: true }
            ],
            enableMultiDeleteActionBtn: false,
            enableBreadCrumb: true,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: false,
            apiSuffixModel: SalesModuleApiSuffixModels.SEARCH_DEBTOR_CUSTOMER_DETAILS,
            moduleName: ModulesBasedApiSuffix.SALES,
          },
        ]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.getTableListData();
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onShowValue() {
    this.debtorConfigDetail = [
      { name: 'Debtor Code', value: this.debtorSelectedDetail?.debtorRefNo ? this.debtorSelectedDetail?.debtorRefNo : '' },
      { name: 'Contact Number', value: this.debtorSelectedDetail?.mobile1 ? this.debtorSelectedDetail?.mobile1 : '' },
      { name: 'Client', value: this.debtorSelectedDetail?.debtorName ? this.debtorSelectedDetail?.debtorName : '' },
      { name: 'Outstanding Balance', value: this.debtorSelectedDetail?.balance ? this.otherService.transformDecimal(this.debtorSelectedDetail?.balance) : '' },
    ];
  }
  // { name: 'Billing Address', value: this.debtorSelectedDetail?.siteAddress ? this.debtorSelectedDetail?.siteAddress : '' },


  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.getTableListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row, unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row, unknownVar);
        break;
      case CrudType.FILTER:
        // this.displayAndLoadFilterData();
        break;
      case CrudType.STATUS_POPUP:
        this.onChangeStatus(row, unknownVar)
        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete();
          }
        } else {
          this.onOneOrManyRowsDelete(row);
        }
        break;
      default:
    }
  }



  openAddEditPage(type: CrudType | string, editableObject?: object | string, unknownVar?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (unknownVar == 'subscription') {
          this.rxjsService.setViewCustomerData({
            customerId: editableObject['customerId'],
            addressId: editableObject['addressId'],
            customerTab: 6,
            monitoringTab: null,
          })
          this.rxjsService.navigateToViewCustomerPage();
        } else if (unknownVar == 'transaction') {
          this.rxjsService.setViewCustomerData({
            customerId: editableObject['customerId'],
            addressId: editableObject['addressId'],
            customerTab: 9,
            monitoringTab: null,
          })
          this.rxjsService.navigateToViewCustomerPage();
        }
        break;
    }
  }

  onChangeStatus(rowData?, index?) {
    let status;
    if (rowData['isActive']) {
      status = true
    } else {
      status = false
    }
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: status,
        // activeText: 'Enable',
        // inActiveText: 'Disable',
        modifiedUserId: this.loggedInUserData?.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        // this.dataList[index].status = this.dataList[index].status == 'Enable' ? 'Disable' : 'Enable';
        this.getTableListData(this.row["pageIndex"], this.row["pageSize"]);
      } else {
        this.getTableListData(this.row["pageIndex"], this.row["pageSize"]);
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].deleteAPISuffixModel,
        method: 'delete',
        dataObject: {
          ids: this.selectedRows.length > 0 ? deletableIds.join(',') : deletableIds,
          isDeleted: true,
        }
      },
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.selectedRows = [];
        this.getTableListData();
      }
    });
  }

  getTableListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams = {
      ...otherParams,
      debtorId: this.debtorId,
    }
    this.isShowNoRecords = false;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    const moduleName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName;
    this.listSubscribtion = this.crudService.get(
      moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.subscription = 'View Subscription';
          val.transaction = 'View Transaction';
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        // this.totalRecords = data.totalCount;
        this.totalRecords = 0;
        this.isShowNoRecords = this.dataList?.length ? false : true;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
        this.isShowNoRecords = true;
      }
    })
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }
  viewAll(){
    this.router.navigate(['/customer/manage-customers/view-transactions/all-transactions'],{queryParams:{debtorId:this.debtorId},skipLocationChange: true})
  }
}
