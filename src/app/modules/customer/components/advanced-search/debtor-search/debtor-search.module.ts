import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DebtorSearchComponent } from "@modules/customer";



@NgModule({
  declarations: [DebtorSearchComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
          path: '', component: DebtorSearchComponent, data: { title: 'Advanced Search List' }
        },
    ])
  ],
  exports: [],
  entryComponents: [],
})
export class DebtorSearchModule { }