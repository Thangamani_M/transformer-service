import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  countryCodes, CustomDirectiveConfig, debounceTimeForSearchkeyword, defaultCountryCode, emailPattern, filterFormControlNamesWhichHasValues, formConfigs, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix,
  prepareAutoCompleteListFormatForMultiSelection,
  prepareGetRequestHttpParams, prepareRequiredHttpParams, removePropsWhichHasNoValuesFromObject, RxjsService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { RadioSearchModel } from '@modules/customer/shared/utils/advanced-search.models';
import { SimpleSearchModel, AddressSearchModel, ClientSearchModel, DebtorSearchModel, TechnicianSearchModel } from '@modules/customer/shared/utils/simple-search.models';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { selectStaticEagerLoadingCustomerTypesState$, selectStaticEagerLoadingDebtorStatusState$, selectStaticEagerLoadingLeadGroupsState$, selectStaticEagerLoadingPaymentMethodsState$, selectStaticEagerLoadingSiteTypesState$ } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
@Component({
  selector: 'advanced-search',
  templateUrl: './simple-search.component.html',
  styleUrls: ['./advanced-search.component.scss']
})

export class SimpleSearchComponent {
  customerStatusTypes = [];
  @Input() fromUrl = '';
  customerTypes = [];
  divisions = [];
  selectedIndex = 0;
  regions = [];
  districts = [];
  branches = [];
  formConfigs = formConfigs;
  advancedSearchForm: FormGroup;
  alphanumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isSearchBtnDisabled = true;
  isClearBtnDisabled = true;
  countryCodes = countryCodes;
  subAreas = [];
  selectedOption;
  debtorStatuses = [];
  siteTypes = [];
  leadGroups = [];
  paymentMethods = [];
  reviewType = "";
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;
  creditControllers: any
  searchType = "simple"
  params: any = {}
  searchTypeForTab = 'client'
  isExpandedAll = true
  isSearched = false
  //Radio

  mobileValidations = {
    mobile1:11,
    premisesNo:11,
    contactNo:11,
    cellPanicNo:11,
    debtorMobile:11
  }

  setNames = []
  decoders = []
  constructor(private crudService: CrudService, private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef, private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private router: Router) {
  }

  ngOnInit(): void {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.combineLatestNgrxStoreData();
    this.createAdvancedSearchForm();
    this.commonForkJoinRequestsForAllTabs();
    this.onFormControlChanges();
    this.getRegions();
  }

  commonForkJoinRequestsForAllTabs(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.UX_CUSTOMERSTATUS, undefined, true),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_DECODER_ACCOUNT_CONFIGURATIONS), this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SETNAMES),
      this.crudService.dropdown(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_COLLECTION_CREDIT_CONTROLLER)]
    ).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.customerStatusTypes = getPDropdownData(resp.resources);
              this.rxjsService.setGlobalLoaderProperty(false);
              break;
            case 1:
              this.decoders = resp.resources;
              break;
            case 2:
              this.setNames = resp.resources;
              break;
            case 3:
              this.creditControllers = resp.resources;
              break;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    });
  }

  combineLatestNgrxForDebtorTab(): void {
    combineLatest([this.store.select(selectStaticEagerLoadingDebtorStatusState$)])
      .subscribe((response) => {
        this.debtorStatuses = prepareAutoCompleteListFormatForMultiSelection(response[0]);
      });
  }

  getRegions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_REGIONS,
      prepareGetRequestHttpParams(null, null,
        { countryId: formConfigs.countryId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.regions = response.resources;
          }
        });
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(selectStaticEagerLoadingCustomerTypesState$),
    this.store.select(selectStaticEagerLoadingLeadGroupsState$), this.store.select(selectStaticEagerLoadingSiteTypesState$),
    this.store.select(selectStaticEagerLoadingPaymentMethodsState$),
    this.activatedRoute.queryParams])
      .subscribe((response) => {
        this.customerTypes = getPDropdownData(response[0]);
        this.leadGroups = prepareAutoCompleteListFormatForMultiSelection(response[1]);
        this.siteTypes = prepareAutoCompleteListFormatForMultiSelection(response[2]);
        this.paymentMethods = response[3];
        this.reviewType = response[4]?.reviewType;
      });
  }

  createAdvancedSearchForm(): void {
    let advancedSearchModel = new SimpleSearchModel();
    this.advancedSearchForm = this.formBuilder.group({});
    Object.keys(advancedSearchModel).forEach((key) => {
      switch (key) {
        case "clientSearchModel":
          this.advancedSearchForm.addControl(key, this.createClientSearchModelForm());
          break;
        case "addressSearchModel":
          this.advancedSearchForm.addControl(key, this.createAddressSearchModelForm());
          break;
        case "debtorSearchModel":
          this.advancedSearchForm.addControl(key, this.createDebtorSearchModelForm());
          break;
        case "technicianSearchModel":
          this.advancedSearchForm.addControl(key, this.createTechnicianSearchModelForm());
          break;
          case "radioSearchModel":
            this.advancedSearchForm.addControl(key, this.createRadioSearchModelForm());
            break;
        default:
          this.advancedSearchForm.addControl(key, new FormControl(advancedSearchModel[key]));
          break;
      }
    });
      this.setPhoneNumberLengthByCountryCode(
      this.debtorSearchFormGroupControls.get("mobile1CountryCode").value,this.debtorSearchFormGroupControls,'mobile1','debtorMobile');
    this.onFormControlChange()
  }

  createClientSearchModelForm(clientSearchModel?: ClientSearchModel): FormGroup {
    let ClientSearchFormModel = new ClientSearchModel(clientSearchModel);
    let formControls = {};
    Object.keys(ClientSearchFormModel).forEach((key) => {
      formControls[key] = new FormControl(ClientSearchFormModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createAddressSearchModelForm(addressSearchModel?: AddressSearchModel): FormGroup {
    let addressSearchFormModel = new AddressSearchModel(addressSearchModel);
    let formControls = {};
    Object.keys(addressSearchFormModel).forEach((key) => {
      formControls[key] = new FormControl(addressSearchFormModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createTechnicianSearchModelForm(technicianSearchModel?: TechnicianSearchModel): FormGroup {
    let technicianSearchFormModel = new TechnicianSearchModel(technicianSearchModel);
    let formControls = {};
    Object.keys(technicianSearchFormModel).forEach((key) => {
      formControls[key] = new FormControl(technicianSearchFormModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createDebtorSearchModelForm(debtorSearchModel?: DebtorSearchModel): FormGroup {
    let debtorSearchFormModel = new DebtorSearchModel(debtorSearchModel);
    let formControls = {};
    Object.keys(debtorSearchFormModel).forEach((key) => {
      if (key == 'email') {
        formControls[key] = new FormControl(debtorSearchFormModel[key], Validators.compose([Validators.email, emailPattern]));
      }
      else {
        formControls[key] = new FormControl(debtorSearchFormModel[key]);
      }
    });

    return this.formBuilder.group(formControls);
  }
  createRadioSearchModelForm(radioSearchModelObj?: RadioSearchModel): FormGroup {
    let radioSearchModel = new RadioSearchModel(radioSearchModelObj);
    let formControls = {};
    Object.keys(radioSearchModel).forEach((key) => {
      formControls[key] = new FormControl(radioSearchModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  get addressSearchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('addressSearchModel') as FormGroup;
  }

  get clientSearchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('clientSearchModel') as FormGroup;
  }

  get debtorSearchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('debtorSearchModel') as FormGroup;
  }

  get technicianSearchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('technicianSearchModel') as FormGroup;
  }

  get radioSearchFormGroupControls(): FormGroup {
    if (!this.advancedSearchForm) return;
    return this.advancedSearchForm.get('radioSearchModel') as FormGroup;
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onFormControlChanges(): void {
    this.advancedSearchForm.valueChanges
      .pipe(
        map((object) => filterFormControlNamesWhichHasValues(object)))
      .subscribe((keyValuesObj) => {
        if (this.addressSearchFormGroupControls.get('tempMainAreaName').invalid) {
          this.addressSearchFormGroupControls.get('tempMainAreaName').clearValidators();
          this.addressSearchFormGroupControls.get('tempMainAreaName').setErrors(null);
          this.addressSearchFormGroupControls.updateValueAndValidity();
        }
        this.isSearchBtnDisabled = (Object.keys(keyValuesObj).length == 0 || this.advancedSearchForm.invalid) ? true : false;
        this.isClearBtnDisabled = Object.keys(keyValuesObj).length > 0 ? false : true;
      });
    this.advancedSearchForm.get('regionId').valueChanges.subscribe((regionId: string) => {
      if (!regionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, null, false,
        prepareGetRequestHttpParams(null, null,
          { regionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources && response.isSuccess) {
            this.divisions = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.advancedSearchForm.get('divisionId').valueChanges.subscribe((divisionId: string) => {
      if (!divisionId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DISTRICT, null, false,
        prepareGetRequestHttpParams(null, null,
          { divisionId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources && response.isSuccess) {
            this.districts = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.advancedSearchForm.get('districtId').valueChanges.subscribe((districtId: string) => {
      if (!districtId) return;
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_BRANCHES, null, false,
        prepareGetRequestHttpParams(null, null,
          { districtId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources && response.isSuccess) {
            this.branches = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    });
    this.onTempMainAreaNameFormControlValueChanges();
  }

  onFormControlChange(){

    this.clientSearchFormGroupControls
      .get("mobile1CountryCode")
      .valueChanges.subscribe((mobileNumber2: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.clientSearchFormGroupControls.get("mobile1CountryCode").value,this.clientSearchFormGroupControls,'mobile1');
      });

      this.clientSearchFormGroupControls
      .get("premisesNoCountryCode")
      .valueChanges.subscribe((mobileNumber2: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.clientSearchFormGroupControls.get("premisesNoCountryCode").value,this.clientSearchFormGroupControls,'premisesNo');
      });


      // this.radioSearchFormGroupControls
      // .get("cellPanicNoCountryCode")
      // .valueChanges.subscribe((mobileNumber2: string) => {
      //   this.setPhoneNumberLengthByCountryCode(
      //     this.radioSearchFormGroupControls.get("cellPanicNoCountryCode").value,this.radioSearchFormGroupControls,'cellPanicNo');
      // });
      this.debtorSearchFormGroupControls.get('mobile1CountryCode')
      .valueChanges.subscribe((mobileNumber2: string) => {
        console.log(mobileNumber2)
        this.setPhoneNumberLengthByCountryCode(
          this.debtorSearchFormGroupControls.get("mobile1CountryCode").value,this.debtorSearchFormGroupControls,'mobile1','debtorMobile');
      });

  }

  setPhoneNumberLengthByCountryCode(countryCode: string, formGroup:FormGroup,formControlName?,optinalValue="") {
    switch (countryCode) {
      case "+27":

        formGroup
          .get(formControlName)
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
          this.mobileValidations[optinalValue?optinalValue:formControlName] = formConfigs.southAfricanContactNumberMaxLength;
          console.log("this.mobileValidations",this.mobileValidations)
        break;
      default:

        formGroup
          .get(formControlName)
          .setValidators([
            Validators.minLength(
              formConfigs.indianContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.indianContactNumberMaxLength
            ),
          ]);
          this.mobileValidations[optinalValue?optinalValue:formControlName] = formConfigs.indianContactNumberMaxLength;

          console.log("this.mobileValidations",this.mobileValidations)

        break;
    }
  }


  ngAfterViewInit() {
    this.changeDetectorRef.detectChanges()
  }

  onTempMainAreaNameFormControlValueChanges(): void {
    this.addressSearchFormGroupControls.get('tempMainAreaName')
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((search: IApplicationResponse) => {
          this.getLoopableObjectRequestObservable = this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.SALES_API_UX_MAIN_AREA, null, true,
            prepareGetRequestHttpParams(null, null, { search }))
        });
  }

  onSelectedItemOption(selectedObject): void {
    if (!selectedObject) return;
    this.selectedOption = selectedObject;
    this.subAreas = [];
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_SUB_AREA, undefined, false, prepareRequiredHttpParams({
        mainAreaIds: selectedObject['mainAreaId']
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.subAreas = response.resources;
          this.addressSearchFormGroupControls.patchValue({ mainAreaName: this.selectedOption ? this.selectedOption['mainAreaId'] : "" });
          this.addressSearchFormGroupControls.patchValue({ tempMainAreaName: this.selectedOption ? this.selectedOption['mainAreaName'] : "" }, { emitEvent: false, onlySelf: true });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onTabChanged(index: number) {
    this.selectedIndex = index;
    let prevTabIndex = this.selectedIndex == 0 ? 0 : this.selectedIndex - 1;
    switch (prevTabIndex) {
      case 0:
        //    this.clientSearchFormGroupControls.reset({ premisesNoCountryCode: defaultCountryCode, mobile1CountryCode: defaultCountryCode });
        break;
      case 1:
        this.combineLatestNgrxForDebtorTab();
        //  this.addressSearchFormGroupControls.reset();
        break;
      case 2:
        //   this.debtorSearchFormGroupControls.reset();
        break;
      case 3:
        break;
      case 4:
        break;
      case 5:
     //   this.technicianSearchFormGroupControls.reset();
        break;
    }
    // this.isSearchBtnDisabled = true;
    // this.isClearBtnDisabled = true;
  }

  onSubmit() {
    this.advancedSearchForm.updateValueAndValidity();
    this.advancedSearchForm.value.customerTypeId = this.advancedSearchForm.value.customerTypeId ? this.advancedSearchForm.value.customerTypeId.join(',') : null;
    this.advancedSearchForm.value.customerStatusId = this.advancedSearchForm.value.customerStatusId ? this.advancedSearchForm.value.customerStatusId.join(',') : null;
    let searchType;
    let data = {};
    Object.keys(this.advancedSearchForm.value).forEach((key) => {
      if (key == 'clientSearchModel' || key == 'addressSearchModel' || key == 'debtorSearchModel' || key == 'technicianSearchModel' || key=='radioSearchModel') {
        return;
      }
      data[key] = this.advancedSearchForm.value[key];
      data = removePropsWhichHasNoValuesFromObject(data);
    });
    switch (this.selectedIndex) {
      case 0:
        searchType = 'client';
        data = { ...data, ...removePropsWhichHasNoValuesFromObject(this.clientSearchFormGroupControls.value) };
        break;
      case 1:
        searchType = 'address';
        data = { ...data, ...removePropsWhichHasNoValuesFromObject(this.addressSearchFormGroupControls.value) };
        for (let key in data) {
          let dataCopy = JSON.parse(JSON.stringify(data));
          if (key == 'mainAreaName') {
            data['mainAreaIds'] = dataCopy['mainAreaName'];
            delete data['mainAreaName'];
          }
          if (key == 'subAreaName') {
            data['subAreaId'] = dataCopy['subAreaName'];
            delete data['subAreaName'];
          }
          if (key == 'tempMainAreaName') {
            delete data['tempMainAreaName'];
          }
        }
        break;
      case 2:
        searchType = 'debtor';
        this.debtorSearchFormGroupControls.value.debtorStatusId = this.debtorSearchFormGroupControls.value.debtorStatusId ? this.debtorSearchFormGroupControls.value.debtorStatusId.join(',') : null;
        this.debtorSearchFormGroupControls.value.siteTypeId = this.debtorSearchFormGroupControls.value.siteTypeId ? this.debtorSearchFormGroupControls.value.siteTypeId.join(',') : null;
        this.debtorSearchFormGroupControls.value.group = this.debtorSearchFormGroupControls.value.group ? this.debtorSearchFormGroupControls.value.group.join(',') : null;
        data = { ...data, ...removePropsWhichHasNoValuesFromObject(this.debtorSearchFormGroupControls.value) };
        break;
      case 3:
        searchType ='radio';
        data = { ...data, ...removePropsWhichHasNoValuesFromObject(this.radioSearchFormGroupControls.value) };
        break;
      case 4:
        break;
      case 5:
        searchType = 'technical';
        data = { ...data, ...removePropsWhichHasNoValuesFromObject(this.technicianSearchFormGroupControls.value) }
        break;
    }
    this.searchTypeForTab = searchType;
    this.params = removePropsWhichHasNoValuesFromObject(data);
    if (this.params?.customerStatusId == undefined) {
      delete this.params?.customerStatusId
    }
    if (this.params?.customerTypeId == undefined) {
      delete this.params?.customerTypeId
    }
    this.isSearched = true

    this.isExpandedAll = false
    // this.router.navigate(["customer/advanced-search/list"], {
    //   queryParams: {
    //     searchType,
    //     fromUrl: this.fromUrl,
    //     data: JSON.stringify(data),
    //     selectedIndex: this.selectedIndex,
    //     reviewType: this.reviewType
    //   }, skipLocationChange: true
    // })
  }

  onClear() {
    switch (this.selectedIndex) {
      case 0:
        this.clientSearchFormGroupControls.reset({ premisesNoCountryCode: defaultCountryCode, mobile1CountryCode: defaultCountryCode });
        break;
      case 1:
        this.addressSearchFormGroupControls.reset({ subAreaName: "" });
        break;
      case 2:
        this.debtorSearchFormGroupControls.reset(new DebtorSearchModel());
        break;
      case 3:
        this.radioSearchFormGroupControls.reset({cellPanicNoCountryCode: defaultCountryCode, });

        break;
      case 4:
        break;
      case 5:
        this.technicianSearchFormGroupControls.reset(new TechnicianSearchModel());
        break;
    }
    this.divisions = [];
    this.districts = [];
    this.branches = [];
    this.advancedSearchForm.patchValue({
      customerTypeId: [], customerStatusId: [],
      regionId: '', divisionId: '', districtId: '', branchId: ''
    });
  }
  navigateToAdvanedSearch() {
    this.router.navigate(['/customer/advanced-search'], { skipLocationChange: true })
  }
  onTabClose(event) {
    this.isExpandedAll = false
  }
  onTabOpen(event) {
    this.isExpandedAll = true
  }
}
