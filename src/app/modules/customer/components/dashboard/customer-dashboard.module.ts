import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerDashboardRoutingModule } from '@modules/customer/components/dashboard';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CustomerDashboardRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule
  ]
})
export class CustomerDashboardModule { }
