import { Component, EventEmitter, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { UserLogin } from '@modules/others/models';
import { AddressModel } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { SharedAddressInformationComponent } from '../../shared/components/address-information/address-information.component';
import { SharedCustomerInformationComponent } from '../../shared/components/customer-information/customer-information.component';
import { FEATURE_NAME, FORMTYPE } from '../../shared/enum/index.enum';
import { CustomerInformationModel } from '../../shared/model';
@Component({
  selector: 'app-repeater-profile-add-edit',
  templateUrl: './repeater-profile-add-edit.component.html',
  styleUrls: ['./repeater-profile-add-edit.component.scss'],
})

export class RepeaterProfileAddEditComponent implements OnInit {

  @ViewChild(SharedCustomerInformationComponent,{static:false}) customerInformationComponent: SharedCustomerInformationComponent;
  @ViewChild(SharedAddressInformationComponent,{static:false}) SharedAddressInformationComponent: SharedAddressInformationComponent;

  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  repeaterProfileForm: FormGroup;
  cameraProfileObject :any
  loggedUser: UserLogin;
  @Output() outputData = new EventEmitter<any>();
  @ViewChildren('input') rows: QueryList<any>;
  // profile information

  // formArray
  cameraCustomerId ;
  feature : FEATURE_NAME
  constructor(
    private router : Router,
    private activatedRoute : ActivatedRoute,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
   this.cameraCustomerId =  this.activatedRoute.snapshot.queryParams.id
   this.feature =  this.activatedRoute.snapshot.queryParams.feature
  }

  ngOnInit(): void {
    this.createRepeaterProfileForm();
    if(this.cameraCustomerId){
      this.getCameraCustomerData()
    }
  }

  createRepeaterProfileForm(): void {
    this.repeaterProfileForm = this.formBuilder.group({
      createdUserId : [this.loggedUser?.userId]
    });

    this.repeaterProfileForm.addControl('customerInformationDTO', this.createCustomerInformationModel());
    this.repeaterProfileForm.addControl('addressInfo', this.createAddressInformationModel());

    this.repeaterProfileForm.controls["customerInformationDTO"] = setRequiredValidator(
      this.repeaterProfileForm.controls["customerInformationDTO"] as FormGroup,
      ["customerTypeId", "titleId", "firstName", "lastName", "email", "mobile1"]
    );

    this.repeaterProfileForm.controls["addressInfo"] = setRequiredValidator(
      this.repeaterProfileForm.controls["addressInfo"] as FormGroup,
      ["formatedAddress", "streetNo"]
    );


  }

  emitCustomerType(data) {
    if (data == true) {
      this.repeaterProfileForm.controls["customerInformationDTO"] = setRequiredValidator(
        this.repeaterProfileForm.controls["customerInformationDTO"] as FormGroup,
        ["companyName"]
      );

    } else {
      this.repeaterProfileForm.controls["customerInformationDTO"] = clearFormControlValidators(
        this.repeaterProfileForm.controls["customerInformationDTO"] as FormGroup,
        ["companyName"]
      );
    }
  }

  emitCustomerInformation(event) {
    switch (event.type) {
      case FORMTYPE.CUSTOMER_INFORMATION:
        this.repeaterProfileForm.get('customerInformationDTO').setValue(event.data);
        break;
      case FORMTYPE.ADDRESS_INFORMATION:
        this.repeaterProfileForm.get('addressInfo').setValue(event.data);

        break;
      default:
        break;
    }
  }
  afrigisAddressInfo  ={};
  emitAfrigisAddressInfo(event){
    this.afrigisAddressInfo = {
      buildingName: event?.buildingName,
      buildingNo: event?.buildingNo,
      streetNo: event?.streetNo,
    };
  }

  emitChangeFormCheckbox (event){
    if(event.type == "addressExtra"){
      if(event.action =="add"){
        this.repeaterProfileForm.controls["addressInfo"].get('estateStreetNo').setValidators([Validators.required]);
        this.repeaterProfileForm.controls["addressInfo"].get('estateStreetNo').updateValueAndValidity();
        this.repeaterProfileForm.controls["addressInfo"].get('estateStreetName').setValidators([Validators.required]);
        this.repeaterProfileForm.controls["addressInfo"].get('estateStreetName').updateValueAndValidity();
        this.repeaterProfileForm.controls["addressInfo"].get('estateName').setValidators([Validators.required]);
        this.repeaterProfileForm.controls["addressInfo"].get('estateName').updateValueAndValidity();
      }else{
        this.repeaterProfileForm.controls["addressInfo"].get('estateStreetNo').clearValidators();
        this.repeaterProfileForm.controls["addressInfo"].get('estateStreetNo').updateValueAndValidity();
        this.repeaterProfileForm.controls["addressInfo"].get('estateStreetName').clearValidators();
        this.repeaterProfileForm.controls["addressInfo"].get('estateStreetName').updateValueAndValidity();
        this.repeaterProfileForm.controls["addressInfo"].get('estateName').clearValidators();
        this.repeaterProfileForm.controls["addressInfo"].get('estateName').updateValueAndValidity();
      }
    }
    if(event.type =="complex"){

      if(event.action  =='add'){
        this.repeaterProfileForm.controls["addressInfo"].get('buildingNo').setValidators([Validators.required]);
        this.repeaterProfileForm.controls["addressInfo"].get('buildingNo').updateValueAndValidity();
        this.repeaterProfileForm.controls["addressInfo"].get('buildingName').setValidators([Validators.required]);
        this.repeaterProfileForm.controls["addressInfo"].get('buildingName').updateValueAndValidity();
      }else{
        this.repeaterProfileForm.controls["addressInfo"].get('buildingNo').clearValidators();
        this.repeaterProfileForm.controls["addressInfo"].get('buildingNo').updateValueAndValidity();
        this.repeaterProfileForm.controls["addressInfo"].get('buildingName').clearValidators();
        this.repeaterProfileForm.controls["addressInfo"].get('buildingName').updateValueAndValidity();
      }
    }

  }

  createCustomerInformationModel(basicInfo?: CustomerInformationModel): FormGroup {
    let customerInfo = new CustomerInformationModel(basicInfo);
    let formControls = {};
    Object.keys(customerInfo).forEach((key) => {
      formControls[key] = new FormControl(customerInfo[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createAddressInformationModel(addressModel?: AddressModel): FormGroup {
    let _addressInfo = new AddressModel(addressModel);
    let formControls = {};
    Object.keys(_addressInfo).forEach((key) => {
      formControls[key] = new FormControl(_addressInfo[key]);
    });
    return this.formBuilder.group(formControls);
  }


  onSubmit(): void {
    this.repeaterProfileForm.get('createdUserId').setValue(this.loggedUser?.userId)
    if (this.repeaterProfileForm.invalid) return;
    let formValue = this.repeaterProfileForm.getRawValue()
    formValue.customerInformationDTO.mobile1 = formValue.customerInformationDTO.mobile1
      .toString()
      .replace(/\s/g, "");
    if (formValue.customerInformationDTO.mobile2) {
      formValue.customerInformationDTO.mobile2 = formValue.customerInformationDTO.mobile2
        .toString()
        .replace(/\s/g, "");
    }
    if (formValue.customerInformationDTO.officeNo) {
      formValue.customerInformationDTO.officeNo = formValue.customerInformationDTO.officeNo
        .toString()
        .replace(/\s/g, "");
    }
    if (formValue.customerInformationDTO.premisesNo) {
      formValue.customerInformationDTO.premisesNo = formValue.customerInformationDTO.premisesNo
        .toString()
        .replace(/\s/g, "");
    }

    if (formValue.addressInfo.latLong) {
      formValue.addressInfo.latitude = formValue.addressInfo.latLong.split(
        ","
      )[0];
      formValue.addressInfo.longitude = formValue.addressInfo.latLong.split(
        ","
      )[1];
    }
    formValue.afrigisAddressInfo =  this.afrigisAddressInfo
    formValue.customerInformationDTO.customerId = this.cameraCustomerId?this.cameraCustomerId:null

    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.REPEATER_PROFILE, formValue).subscribe(response=>{
      if(response.isSuccess && response.statusCode === 200){
        this.router.navigate(['customer/non-customer-profile/key-holder-information'] ,{queryParams:{customerId : response.resources, customerTypeId :  formValue.customerInformationDTO.customerTypeId, communityId:this.cameraCustomerId, feature:this.feature}})
      }
    })
  }



  getCameraCustomerData (){
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.REPEATER_PROFILE_DETAIL,undefined, false,prepareRequiredHttpParams({customerId :this.cameraCustomerId})).subscribe(response=>{
      if(response.isSuccess && response.statusCode === 200){{
        this.repeaterProfileForm.patchValue(response.resources);
        this.cameraProfileObject = response.resources;
        // customer Information Component
        this.customerInformationComponent.createcustomerInformatioanForm()
        this.customerInformationComponent.emitDataWhenFormChange();
        this.customerInformationComponent.customerInformatioanForm.patchValue(response.resources.customerInformationDTO);

        this.SharedAddressInformationComponent.createAddresForm()
            response.resources.addressInfo.latLong =
            response.resources.addressInfo.latitude +
            "," +
            response.resources.addressInfo.longitude;


        this.SharedAddressInformationComponent.addressForm.patchValue(response.resources.addressInfo,{emitEvent:false, onlySelf:true});
        this.SharedAddressInformationComponent.addressForm.get("formatedAddress").setValue(response.resources.addressInfo.formatedAddress)
        this.SharedAddressInformationComponent.onFormControlChanges()
      }}
    })
  }


  navigateToDebtorInformation(){
    if(!this.cameraProfileObject) return ;
    this.router.navigate(['customer/non-customer-profile/key-holder-information'] ,{queryParams:{customerId : this.cameraCustomerId, customerTypeId :  this.cameraProfileObject.customerInformationDTO.customerTypeId, feature:this.feature}})

  }
  goBack(){
    this.router.navigateByUrl('customer/non-customer-profile?tab=3')
  }

}
