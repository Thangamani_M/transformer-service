import { Component, EventEmitter, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { UserLogin } from '@modules/others/models';
import { AddressModel } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { SharedAddressInformationComponent } from '../../shared/components/address-information/address-information.component';
import { SharedCustomerInformationComponent } from '../../shared/components/customer-information/customer-information.component';
import { FEATURE_NAME, FORMTYPE } from '../../shared/enum/index.enum';
import { CustomerInformationModel } from '../../shared/model';
@Component({
  selector: 'app-adt-site-profile-add-edit',
  templateUrl: './adt-site-profile-add-edit.component.html',
  styleUrls: ['./adt-site-profile-add-edit.component.scss'],
})

export class AdtSiteProfileAddEditComponent implements OnInit {

  @ViewChild(SharedCustomerInformationComponent, { static: false }) customerInformationComponent: SharedCustomerInformationComponent;
  @ViewChild(SharedAddressInformationComponent, { static: false }) SharedAddressInformationComponent: SharedAddressInformationComponent;

  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  adtSiteProfileForm: FormGroup;
  adtProfileObject: any
  loggedUser: UserLogin;
  @Output() outputData = new EventEmitter<any>();
  @ViewChildren('input') rows: QueryList<any>;
  // profile information

  // formArray
  adtCustomerId;
  feature: FEATURE_NAME
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.adtCustomerId = this.activatedRoute.snapshot.queryParams.id
    this.feature = this.activatedRoute.snapshot.queryParams.feature
  }

  ngOnInit(): void {
    this.createNonClientProfileForm();
    if (this.adtCustomerId) {
      this.getadtCustomerData()
    }
  }

  createNonClientProfileForm(): void {
    this.adtSiteProfileForm = this.formBuilder.group({
      createdUserId: [this.loggedUser?.userId]
    });

    this.adtSiteProfileForm.addControl('customerInformationDTO', this.createCustomerInformationModel());
    this.adtSiteProfileForm.addControl('addressInfo', this.createAddressInformationModel());

    this.adtSiteProfileForm.controls["customerInformationDTO"] = setRequiredValidator(
      this.adtSiteProfileForm.controls["customerInformationDTO"] as FormGroup,
      ["customerTypeId", "titleId", "firstName", "lastName", "email", "mobile1"]
    );

    this.adtSiteProfileForm.controls["addressInfo"] = setRequiredValidator(
      this.adtSiteProfileForm.controls["addressInfo"] as FormGroup,
      ["formatedAddress", "streetNo"]
    );


  }

  emitCustomerType(data) {
    if (data == true) {
      this.adtSiteProfileForm.controls["customerInformationDTO"] = setRequiredValidator(
        this.adtSiteProfileForm.controls["customerInformationDTO"] as FormGroup,
        ["companyName"]
      );

    } else {
      this.adtSiteProfileForm.controls["customerInformationDTO"] = clearFormControlValidators(
        this.adtSiteProfileForm.controls["customerInformationDTO"] as FormGroup,
        ["companyName"]
      );
    }
  }

  emitCustomerInformation(event) {
    switch (event.type) {
      case FORMTYPE.CUSTOMER_INFORMATION:
        this.adtSiteProfileForm.get('customerInformationDTO').setValue(event.data);
        break;
      case FORMTYPE.ADDRESS_INFORMATION:
        this.adtSiteProfileForm.get('addressInfo').setValue(event.data);

        break;
      default:
        break;
    }
  }
  afrigisAddressInfo = {};
  emitAfrigisAddressInfo(event) {
    this.afrigisAddressInfo = {
      buildingName: event?.buildingName,
      buildingNo: event?.buildingNo,
      streetNo: event?.streetNo,
    };
  }

  emitChangeFormCheckbox(event) {
    if (event.type == "addressExtra") {
      if (event.action == "add") {
        this.adtSiteProfileForm.controls["addressInfo"].get('estateStreetNo').setValidators([Validators.required]);
        this.adtSiteProfileForm.controls["addressInfo"].get('estateStreetNo').updateValueAndValidity();
        this.adtSiteProfileForm.controls["addressInfo"].get('estateStreetName').setValidators([Validators.required]);
        this.adtSiteProfileForm.controls["addressInfo"].get('estateStreetName').updateValueAndValidity();
        this.adtSiteProfileForm.controls["addressInfo"].get('estateName').setValidators([Validators.required]);
        this.adtSiteProfileForm.controls["addressInfo"].get('estateName').updateValueAndValidity();
      } else {
        this.adtSiteProfileForm.controls["addressInfo"].get('estateStreetNo').clearValidators();
        this.adtSiteProfileForm.controls["addressInfo"].get('estateStreetNo').updateValueAndValidity();
        this.adtSiteProfileForm.controls["addressInfo"].get('estateStreetName').clearValidators();
        this.adtSiteProfileForm.controls["addressInfo"].get('estateStreetName').updateValueAndValidity();
        this.adtSiteProfileForm.controls["addressInfo"].get('estateName').clearValidators();
        this.adtSiteProfileForm.controls["addressInfo"].get('estateName').updateValueAndValidity();
      }
    }
    if (event.type == "complex") {

      if (event.action == 'add') {
        this.adtSiteProfileForm.controls["addressInfo"].get('buildingNo').setValidators([Validators.required]);
        this.adtSiteProfileForm.controls["addressInfo"].get('buildingNo').updateValueAndValidity();
        this.adtSiteProfileForm.controls["addressInfo"].get('buildingName').setValidators([Validators.required]);
        this.adtSiteProfileForm.controls["addressInfo"].get('buildingName').updateValueAndValidity();
      } else {
        this.adtSiteProfileForm.controls["addressInfo"].get('buildingNo').clearValidators();
        this.adtSiteProfileForm.controls["addressInfo"].get('buildingNo').updateValueAndValidity();
        this.adtSiteProfileForm.controls["addressInfo"].get('buildingName').clearValidators();
        this.adtSiteProfileForm.controls["addressInfo"].get('buildingName').updateValueAndValidity();
      }
    }

  }

  createCustomerInformationModel(basicInfo?: CustomerInformationModel): FormGroup {
    let customerInfo = new CustomerInformationModel(basicInfo);
    let formControls = {};
    Object.keys(customerInfo).forEach((key) => {
      formControls[key] = new FormControl(customerInfo[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createAddressInformationModel(addressModel?: AddressModel): FormGroup {
    let _addressInfo = new AddressModel(addressModel);
    let formControls = {};
    Object.keys(_addressInfo).forEach((key) => {
      formControls[key] = new FormControl(_addressInfo[key]);
    });
    return this.formBuilder.group(formControls);
  }


  onSubmit(): void {
    this.adtSiteProfileForm.get('createdUserId').setValue(this.loggedUser?.userId)
    if (this.adtSiteProfileForm.invalid) return;
    let formValue = this.adtSiteProfileForm.getRawValue()
    formValue.customerInformationDTO.mobile1 = formValue.customerInformationDTO.mobile1
      .toString()
      .replace(/\s/g, "");
    if (formValue.customerInformationDTO.mobile2) {
      formValue.customerInformationDTO.mobile2 = formValue.customerInformationDTO.mobile2
        .toString()
        .replace(/\s/g, "");
    }
    if (formValue.customerInformationDTO.officeNo) {
      formValue.customerInformationDTO.officeNo = formValue.customerInformationDTO.officeNo
        .toString()
        .replace(/\s/g, "");
    }
    if (formValue.customerInformationDTO.premisesNo) {
      formValue.customerInformationDTO.premisesNo = formValue.customerInformationDTO.premisesNo
        .toString()
        .replace(/\s/g, "");
    }

    if (formValue.addressInfo.latLong) {
      formValue.addressInfo.latitude = formValue.addressInfo.latLong.split(
        ","
      )[0];
      formValue.addressInfo.longitude = formValue.addressInfo.latLong.split(
        ","
      )[1];
    }
    formValue.afrigisAddressInfo = this.afrigisAddressInfo
    formValue.customerInformationDTO.customerId = this.adtCustomerId ? this.adtCustomerId : null
    formValue.customerInformationDTO.siteTypeId = formValue.customerInformationDTO.siteTypeId ? formValue.customerInformationDTO.siteTypeId : 0;
    formValue.siteTypeId = formValue.siteTypeId ? formValue.siteTypeId : 0;
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.ADT_SITE_PROFILE_STORE, formValue).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200) {
        this.navigateToDebtorInformation();
        // this.router.navigate(['customer/non-customer-profile/adt/business-classification'], { queryParams: { customerId: response.resources, customerTypeId: formValue.customerInformationDTO.customerTypeId, communityId: this.adtCustomerId, feature: this.feature } })
      }
    })
  }



  getadtCustomerData(isNavigate =false) {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.ADT_SITE_PROFILE_STORE_DETAIL, undefined, false, prepareRequiredHttpParams({ customerId: this.adtCustomerId })).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200) {
        {
          this.adtSiteProfileForm.patchValue(response.resources);
          this.adtProfileObject = response.resources;
          // customer Information Component
          this.customerInformationComponent.createcustomerInformatioanForm()
          this.customerInformationComponent.emitDataWhenFormChange();
          this.customerInformationComponent.customerInformatioanForm.patchValue(response.resources.customerInformationDTO);
          this.SharedAddressInformationComponent.createAddresForm()
          response.resources.addressInfo.latLong =
            response.resources.addressInfo.latitude +
            "," +
            response.resources.addressInfo.longitude;


          this.SharedAddressInformationComponent.addressForm.patchValue(response.resources.addressInfo, { emitEvent: false, onlySelf: true });
          this.SharedAddressInformationComponent.addressForm.get("formatedAddress").setValue(response.resources.addressInfo.formatedAddress)
          this.SharedAddressInformationComponent.addressForm.get("addressConfidentLevel").setValue(response.resources.addressInfo.addressConfidentLevelName)
          this.SharedAddressInformationComponent.onFormControlChanges()
          if(isNavigate){
            this.navigateToDebtorInformation()
          }
        }
      }
    })
  }


  navigateToDebtorInformation() {
    if (!this.adtProfileObject) return;
    this.router.navigate(['customer/non-customer-profile/adt/business-classification'], { queryParams: { customerId: this.adtCustomerId, customerTypeId: this.adtProfileObject.customerInformationDTO.customerTypeId, feature: this.feature, addressId:this.adtProfileObject?.addressInfo?.addressId } })

  }
  goBack() {
    this.router.navigate(['customer/non-customer-profile'], { queryParams: { tab: 9 } })
  }
}
