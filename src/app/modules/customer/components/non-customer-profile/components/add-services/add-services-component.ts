import { Component, EventEmitter, Inject, OnInit, Output } from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, SnackbarService } from "@app/shared";
import { CustomerModuleApiSuffixModels } from "@modules/customer";
import { selectDynamicEagerLoadingServiceCategoriesState$ } from "@modules/others";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs/internal/observable/combineLatest";
import { map } from "rxjs/operators";

@Component({
    selector: 'app-add-services',
    templateUrl: './add-services-component.html'
  })

  export class AddServicesComponent implements OnInit { 
   
    primengTableConfigProperties: any;
    serviceCategoryId = null;
    selectedRows = [];
    serviceCategories = [];
    isFormSubmitted: boolean;
    customerId:string;
    serviceSubscriptionObj;
    dataList: any;
    pageLimit: number[] = [10, 25, 50, 75, 100];
    row: any = {}
    loading: boolean;
    selectedTabIndex: any = 0;
    totalRecords: any;
    @Output() outputData = new EventEmitter<any>();
    serviceCategoryName:string;
    arr;

    constructor(@Inject(MAT_DIALOG_DATA) public serviceSubscription, public snackbarService: SnackbarService,
    private dialog: MatDialog, private rxjsService: RxjsService, public dialogRef: MatDialogRef<AddServicesComponent>,
    private crudService: CrudService, private httpCancelService: HttpCancelService, private store: Store<AppState>) {
     this.serviceSubscriptionObj = JSON.parse(JSON.stringify(this.serviceSubscription));
    //this.serviceCategoryId = this.serviceSubscriptionObj.serviceCategoryId;
    this.primengTableConfigProperties = {
      tableCaption: "Services",
      breadCrumbItems: [{ displayName: 'Services', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Services',
            dataKey: 'serviceCode',
            captionFontSize: '21px',
            enableBreadCrumb: false,
            enableAction: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'serviceName', header: 'Service Description', width: '50%' },
              { field: 'serviceCode', header: 'Service Code', width: '40%' },
              { field: 'isQtyAvailable', header: 'QTY', type:'QTY', width: '10%' }
            ],
            enableMultiDeleteActionBtn: true,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            // apiSuffixModel: DealerModuleApiSuffixModels.DEALER_TYPE_LIST,
            // moduleName: ModulesBasedApiSuffix.DEALER,
          }
        ]
      }
    }

  }
    ngOnInit() { 
        this.combineLatestNgrxDynamicData();
        this.customerId = this.serviceSubscription.customerId;
    }
    combineLatestNgrxDynamicData() {
        combineLatest([
          this.store.select(selectDynamicEagerLoadingServiceCategoriesState$)])
          .subscribe((response) => {
            this.serviceCategories = response[0];
          });
      }
    onAddServices(pageIndex?: string, pageSize?: string) {
      this.serviceCategoryName = this.getServiceCategoryName();
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_SERVICES, undefined,
        false, prepareGetRequestHttpParams(pageIndex, pageSize, {
          serviceCategoryId: this.serviceCategoryId,
          customerId: this.customerId
        }), 1)
        .pipe(map((res: IApplicationResponse) => {
          if (res?.resources) {
            res?.resources?.forEach(val => {                   
              //  if(val.isQtyAvailable) {
                  val.qty = 1;
              //  }
              return val;
            })
          }
          return res;
        }))
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode == 200 && response.isSuccess) {
           // this.onResponse(response);
            this.dataList = response.resources;
            this.totalRecords = 0;
          }
          else {
            this.dataList = [];
            this.totalRecords = 0;
          }
        })
    }
    
    getServiceCategoryName(): string {
      return this.serviceCategories.find(s => s['id'] == this.serviceCategoryId).displayName;
    }
    onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
      switch (type) {
        case CrudType.QUANTITY_CLICK:
             this.onQuantityClick(row,unknownVar);
        break;
        case CrudType.CHECK:
          break;
        default:
      }
    }

    prepareKeyValueData(type: string) {
      this.selectedRows.forEach((selectedRow) => {
        selectedRow.isChecked = true;
      })
      if (!this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]) {
        this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName] = {};
      }
      // this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['serviceCategoryId'] = this.serviceCategoryId;
      this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'] = this.retrieveUniqueObjects([...this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'] ?
        this.retrieveUniqueObjects(this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values']) : [],
      ...this.retrieveUniqueObjects([...this.selectedRows])]).filter(v => v['isChecked']);
  
      this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'] = this.retrieveUniqueObjects(this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'] ?
        this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'] : []).filter(v => v['isChecked']);
  
      if (this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName]['values'].length === 0) {
        delete this.serviceSubscriptionObj.serviceObj[this.serviceCategoryName];
      }
      this.outputData.emit(this.serviceSubscriptionObj);
    }
    retrieveUniqueObjects = (list: Array<object>): Array<object> => {
      return [...new Set(list.map(obj => obj['serviceCode']))].map(servicePriceId => {
        return list.find(obj => obj['serviceCode'] === servicePriceId)
      });
    }
    onQuantityClick(row,unknownVar) {
      let index = unknownVar.index;
      let type = unknownVar.type;
      if(type == 'minus'){
       this.dataList[index].qty = Number(this.dataList[index].qty) - 1;     
      }      
      else{
         this.dataList[index].qty = Number(this.dataList[index].qty) + 1;
      }
        
    }
    onSubmit() {
      this.prepareKeyValueData('check');
    }
    onChangeSelecedRows(e) {
      this.selectedRows = e;
      // this.prepareKeyValueData('check');
    }
    onActionSubmited(e: any) {
      if (e.data && !e.search && !e?.col?.toString()) {
          this.onCRUDRequested(e.type, e.data);
      } else if (e.data && e.search && !e?.col) {
          this.onCRUDRequested(e.type, e.data, e.search);
      } else if (e.type && !e.data && !e?.col) {
          this.onCRUDRequested(e.type, {});
      } else if (e.type && e.data && e?.col?.toString()) {
          this.onCRUDRequested(e.type, e.data, e?.col);
      }
    }
    onClose() {

    }
  }