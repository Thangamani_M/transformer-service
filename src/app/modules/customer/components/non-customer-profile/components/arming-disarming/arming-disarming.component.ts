import { Component, ElementRef, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import {
  btnActionTypes, CustomerModuleApiSuffixModels,
  loggedInUserData,
  OpenCloseMonitoringAddEditModel,
  selectLeadHeaderDataState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel,
  ConfirmDialogModel,
  ConfirmDialogPopupComponent, countryCodes, CrudService, CustomDirectiveConfig,
  emailPattern, formConfigs,
  HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  onBlurActionOnDuplicatedFormControl,
  onFormControlChangesToFindDuplicate,
  prepareGetRequestHttpParams,
  prepareRequiredHttpParams, ResponseMessageTypes,
  RxjsService,
  SnackbarService
} from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { LeadHeaderData, OpenCloseContact, ServiceAgreementStepsParams } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take, tap } from 'rxjs/operators';
@Component({
  selector: 'app-arming-disarming',
  templateUrl: './arming-disarming.component.html',
  styleUrls: ['../../non-customer-profile.component.scss']
})

export class ArmingDisarmingComponent implements OnInit {
  isDisabled = false;
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  formConfigs = formConfigs;
  mobileNumberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  openCloseMonitoringForm: FormGroup;
  countryCodes = countryCodes;
  leadHeaderData: LeadHeaderData;
  dateFormate: '24';
  openAndCloseMonitoringId;
  contacts: FormArray;
  currentAgreementTabObj;
  loggedInUserData: LoggedInUserModel;
  @ViewChildren('contactRows') contactRows: QueryList<ElementRef>;
  breadCrumb: BreadCrumbModel;
  customerId:string
  feature:string
  addressId:string

  constructor(private crudService: CrudService,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder, private dialog: MatDialog,
    public rxjsService: RxjsService, private store: Store<AppState>,
    private snackbarService: SnackbarService, private router: Router, private momentService: MomentService,
    private activatedRoute : ActivatedRoute) {

    // this.rxjsService.setQuickActionComponent(ModuleName.LEAD)
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadHeaderDataState$), this.store.select(loggedInUserData),
    this.activatedRoute.queryParams]
    ).pipe(take(1)).subscribe((response) => {
      this.leadHeaderData = response[0];
      this.loggedInUserData = new LoggedInUserModel(response[1]);
      this.customerId = response[2]['customerId']
      this.feature = response[2]['feature']
      this.addressId = response[2]['addressId']
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createOpenCloseMonitoringForm();
    this.getArmingAndDisarmingByLeadAndPartitionId();
  }

  getSelectedValue(event) {
    if (!event) return;
    this.toggleAddDays({ checked: this.openCloseMonitoringForm.get('allDays').value })
  }

  focusInAndOutFormArrayFields(): void {
    setTimeout(() => {
      this.contactRows.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      })
    }, 100);
  }

  getContactControl(form): Array<any> {
    return form.controls.contacts.controls;
  }

  addItem() {
    if (this.contacts.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.contacts = this.getContacts;
    let openCloseContactModel = new OpenCloseContact();
    this.contacts.insert(0, this.createContact(openCloseContactModel));
  }

  onFormControlChanges(formControlName: string, formArray: FormArray, formArrayName: string, currentCursorIndex: number): void {
    onFormControlChangesToFindDuplicate(formControlName, formArray, formArrayName, currentCursorIndex, this.openCloseMonitoringForm);
  }

  onBlur(formControlName: string, formArray: FormArray, e, index: number) {
    onBlurActionOnDuplicatedFormControl(formControlName, formArray, e, index);
  }

  createOpenCloseMonitoringForm(): void {
    let openCloseMonitoringAddEditModel = new OpenCloseMonitoringAddEditModel();
    this.openCloseMonitoringForm = this.formBuilder.group({
      contacts: this.formBuilder.array([]),
    });
    Object.keys(openCloseMonitoringAddEditModel).forEach((key) => {
      if (key == 'monthlyReportEmailTo') {
        this.openCloseMonitoringForm.addControl(key,
          new FormControl(openCloseMonitoringAddEditModel[key], Validators.compose([Validators.email, emailPattern])));
      }
      else {
        this.openCloseMonitoringForm.addControl(key,
          new FormControl(openCloseMonitoringAddEditModel[key]));
      }
    });
  }

  get getContacts(): FormArray {
    if (!this.openCloseMonitoringForm) return;
    return this.openCloseMonitoringForm.get("contacts") as FormArray;
  }

  createContact(openCloseContact?: OpenCloseContact): FormGroup {
    let openCloseContactModel = new OpenCloseContact(openCloseContact);
    let formControls = {};
    Object.keys(openCloseContactModel).forEach((key) => {
      formControls[key] = [openCloseContactModel[key],
      (key === 'smsNo') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  toggleAddDays(event): void {
    let mondayArmingTime = this.openCloseMonitoringForm.get('mondayArmingTime').value;
    let mondayDisarmingTime = this.openCloseMonitoringForm.get('mondayDisarmingTime').value;
    if (event.checked && (mondayArmingTime || mondayDisarmingTime)) {
      this.openCloseMonitoringForm.patchValue({
        "tuesdayArmingTime": mondayArmingTime,
        "wednesdayArmingTime": mondayArmingTime,
        "thursdayArmingTime": mondayArmingTime,
        "fridayArmingTime": mondayArmingTime,
      })
      setTimeout(() => {
        this.openCloseMonitoringForm.patchValue({
          "tuesdayDisarmingTime": mondayDisarmingTime,
          "wednesdayDisarmingTime": mondayDisarmingTime,
          "thursdayDisarmingTime": mondayDisarmingTime,
          "fridayDisarmingTime": mondayDisarmingTime,
        })
      }, 0);
    }
  }

  removeItem(i: number): void {
    if (!this.contacts.controls[i].value.openAndCloseMonitoringContactId) {
      this.contacts.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.contacts.controls[i].value.openAndCloseMonitoringContactId && this.contacts.length > 1) {

          this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.ARMING_AND_DISARMING, undefined,
            prepareRequiredHttpParams({
              ids: this.contacts.controls[i].value.openAndCloseMonitoringContactId,
              isDeleted: true,
              modifiedUserId:this.loggedInUserData?.userId
            }), 1).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.contacts.removeAt(i);
          }
          if (this.contacts.length === 0) {
            this.addItem();
          };
        });
      }
      else {
        this.contacts.removeAt(i);
      }
    });
  }

  getArmingAndDisarmingByLeadAndPartitionId(): void {
    let resultObservable = this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.NON_CUSTOMER_ARMING_DISARMING_DETAIL, undefined, false,
        prepareGetRequestHttpParams(undefined, undefined, {
          customerId: this.customerId,
          // addressId: this.addressId,
          // partitionId: this.inputObject.partitionId
        }), 1)

    resultObservable.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode === 200) {
        if (response.resources && (response.resources.openAndCloseMonitoringId || response.resources.customerId)) {
         // this.openCloseMonitoringForm.patchValue(response.resources);
          response.resources.mondayArmingTime = this.formateTime(response.resources.mondayArmingTime)
          response.resources.mondayDisarmingTime = this.formateTime(response.resources.mondayDisarmingTime)
          response.resources.tuesdayArmingTime = this.formateTime(response.resources.tuesdayArmingTime)
          response.resources.tuesdayDisarmingTime = this.formateTime(response.resources.tuesdayDisarmingTime)
          response.resources.thursdayDisarmingTime = this.formateTime(response.resources.thursdayDisarmingTime)
          response.resources.thursdayArmingTime = this.formateTime(response.resources.thursdayArmingTime)
          response.resources.wednesdayArmingTime = this.formateTime(response.resources.wednesdayArmingTime)
          response.resources.wednesdayDisarmingTime = this.formateTime(response.resources.wednesdayDisarmingTime)
          response.resources.fridayArmingTime = this.formateTime(response.resources.fridayArmingTime)
          response.resources.fridayDisarmingTime = this.formateTime(response.resources.fridayDisarmingTime)
          response.resources.saturdayArmingTime = this.formateTime(response.resources.saturdayArmingTime)
          response.resources.saturdayDisarmingTime = this.formateTime(response.resources.saturdayDisarmingTime)
          response.resources.sundayArmingTime = this.formateTime(response.resources.sundayArmingTime)
          response.resources.sundayDisarmingTime = this.formateTime(response.resources.sundayDisarmingTime)
          response.resources.publicHolidayArmingTime = this.formateTime(response.resources.publicHolidayArmingTime)
          response.resources.publicHolidayDisarmingTime = this.formateTime(response.resources.publicHolidayDisarmingTime)
          this.contacts = this.getContacts;
          while (this.contacts.length) {
            this.contacts.removeAt(this.contacts.length - 1);
          }
          response.resources.contacts.forEach((obj) => {
            if (typeof obj.smsNo === "string") {
              obj.smsNo = obj.smsNo.split(" ").join("")
              obj.smsNo = obj.smsNo.replace(/"/g, "");
            }
            this.contacts.push(this.createContact(obj));
          });
          this.openAndCloseMonitoringId = response.resources.openAndCloseMonitoringId
          this.openCloseMonitoringForm.controls['openAndCloseMonitoringId'].setValue(response.resources.openAndCloseMonitoringId);
          if (Object.keys(response.resources ? response.resources : {}).length === 0) return;
          const openCloseMonitoringAddEditModel = new OpenCloseMonitoringAddEditModel(response.resources);
          Object.keys(openCloseMonitoringAddEditModel).forEach((key) => {
            if (openCloseMonitoringAddEditModel[key]) {
              if (key == "createdUserId" || key == "modifiedUserId" || key == "contacts" || key == "customerId" || key == "partitionId" ||
                key == "quotationVersionId" || key == "openAndCloseMonitoringId" || key == "leadId" || key == "countryCode"
                || key == "monthlyReportEmailTo") {
                openCloseMonitoringAddEditModel[key] = openCloseMonitoringAddEditModel[key]
              } else {
                openCloseMonitoringAddEditModel[key] =  this.formateTime(openCloseMonitoringAddEditModel[key])
              }
            }
          });
          this.openCloseMonitoringForm.patchValue(openCloseMonitoringAddEditModel);
          setTimeout(() => {
            this.openCloseMonitoringForm.patchValue(openCloseMonitoringAddEditModel); // To resolve form min value data binding validation issue
          }, 5);
        }
        else {
          this.contacts = this.getContacts;
          this.contacts.push(this.createContact());
          // this.contacts.push(this.createContact());
        }
        if (response.resources && response.resources.contacts.length > 0) {
          setTimeout(() => {
            this.focusInAndOutFormArrayFields();
          }, 10);
        }

      }
    });
  }

  ifEmpty(val) {
    if (!val && !this.openCloseMonitoringForm.get('allDays').value)
      this.snackbarService.openSnackbar('Select Start Time', ResponseMessageTypes.WARNING)
  }
  todayEndTime(dateTime) {
    if (!dateTime) return
    let date = new Date(dateTime).setHours(23, 59)
    return new Date(date)
  }
  formateTime(time) {
    if (!time) return
    let timeToRailway = this.momentService.convertNormalToRailayTimeOnly(time)
    let TimeArray: any = timeToRailway.split(':');
    let date = (new Date).setHours(TimeArray[0], TimeArray[1]);
    return new Date(date)
  }

  onSubmit(): void {
    if (this.openCloseMonitoringForm.invalid) return;
    const openCloseMonitoringAddEditModel = new OpenCloseMonitoringAddEditModel(this.openCloseMonitoringForm.value);
    Object.keys(openCloseMonitoringAddEditModel).forEach((key) => {
      if (openCloseMonitoringAddEditModel[key]) {
        if (key == "createdUserId" || key == "contacts" || key == "customerId"|| key == "partitionId" || key == "quotationVersionId" || key == "openAndCloseMonitoringId" || key == "leadId" || key == "countryCode" ||
          key == "smsNumber" || key == "smsNumberCountryCode" || key == "monthlyReportEmailTo") {
          openCloseMonitoringAddEditModel[key] = openCloseMonitoringAddEditModel[key]
        } else {
          openCloseMonitoringAddEditModel[key] = this.momentService.convertNormalToRailayTime(openCloseMonitoringAddEditModel[key])
        }
      }
    });
    let contacts = this.openCloseMonitoringForm.value.contacts;
    contacts.forEach(element => {
      if (typeof element.smsNo === "string") {
        element.smsNo = element.smsNo.split(" ").join("")
        element.smsNo = element.smsNo.replace(/"/g, "");
      }
    });
    openCloseMonitoringAddEditModel.contacts = contacts;
    openCloseMonitoringAddEditModel.createdUserId = this.loggedInUserData.userId;
    openCloseMonitoringAddEditModel.modifiedUserId = this.loggedInUserData.userId;
    openCloseMonitoringAddEditModel.addressId = openCloseMonitoringAddEditModel?.addressId? openCloseMonitoringAddEditModel?.addressId : this.addressId
    openCloseMonitoringAddEditModel.customerId = this.customerId;
    openCloseMonitoringAddEditModel.monthlyReportEmailTo = openCloseMonitoringAddEditModel.monthlyReportEmailTo ? openCloseMonitoringAddEditModel.monthlyReportEmailTo : null;
    openCloseMonitoringAddEditModel.openAndCloseMonitoringId = this.openCloseMonitoringForm.get('openAndCloseMonitoringId').value ?
      this.openCloseMonitoringForm.get('openAndCloseMonitoringId').value : this.openAndCloseMonitoringId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

      let crudService = this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.NON_CUSTOMER_ARMING_DISARMING, openCloseMonitoringAddEditModel, 1);
    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        if (response.message.toLowerCase().includes('created')) {
          response.resources.contacts.forEach((openCloseMonitoringAddEditModel, index: number) => {
            this.contacts.controls[index].get('openAndCloseMonitoringContactId').setValue(openCloseMonitoringAddEditModel.openAndCloseMonitoringContactId);
          });
          this.openCloseMonitoringForm.get('openAndCloseMonitoringId').setValue(response.resources.openAndCloseMonitoringId);
        }
        this.onArrowClick('next')
      }
    })
  }

  redirectToTab(type: btnActionTypes | string = btnActionTypes.SUBMIT): void {


  }

  onEditBtnClicked() {
    this.isDisabled = false;
    this.focusInAndOutFormArrayFields();
  }

  onArrowClick(type: string) {
    if(type == "previous"){
        this.router.navigate(['/customer/non-customer-profile/password-special-instructions'],{queryParams:{customerId : this.customerId,feature:this.feature, addressId:this.addressId}})
    }else{
      this.router.navigate(['customer/non-customer-profile/access-to-premises'],{queryParams:{customerId : this.customerId,feature:this.feature,addressId:this.addressId}})
  }
  }
}
