import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AccessToPremisesAddEditModel, btnActionTypes, CustomerModuleApiSuffixModels, SalesModuleApiSuffixModels,
  selectLeadCreationStepperParamsState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel, clearFormControlValidators, ConfirmDialogModel, ConfirmDialogPopupComponent, CustomDirectiveConfig,
  formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  prepareRequiredHttpParams, setRequiredValidator, SnackbarService
} from '@app/shared';
import { ModuleName, ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { DigiPadModel, LeadHeaderData, LockBoxesModel, ServiceAgreementStepsParams } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-non-customer-access-to-premises',
  templateUrl: './access-to-premises.component.html',
  // styleUrls: ['./service-installation-agreement.scss']
})

export class NonCustomerAccessToPremisesComponent implements OnInit {

  isDisabled = false;
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  alphaNumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  numericConfig = new CustomDirectiveConfig({ isANumberWithHash: true });
  accessPremisesForm: FormGroup;
  leadHeaderData: LeadHeaderData;
  @ViewChildren('input') rows: QueryList<any>;
  details;
  lockBoxes: FormArray;
  digipads: FormArray;
  currentAgreementTabObj;
  loggedInUserData: LoggedInUserModel;
  breadCrumb: BreadCrumbModel;
  customerId :string
  feature:string
  addressId;

  constructor(private crudService: CrudService, private dialog: MatDialog, private router: Router,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    public rxjsService: RxjsService, private store: Store<AppState>,
    private activatedRoute :ActivatedRoute
  ) {
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD)
  }

  combineLatestNgrxStoreData() {
    combineLatest([
       this.store.select(loggedInUserData),
      this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.customerId =  response[1]['customerId']
      this.feature = response[1]['feature']
      this.addressId = response[1]['addressId']

    });
  }

  ngOnInit(): void {
    this.getAccessPremisesByLeadAndPartitionId();
    this.createAccessPremisesForm();
    this.onFormControlChanges();
  }

  onFormControlChanges(): void {
    this.accessPremisesForm.get('isArmedResponsePadlock').valueChanges.subscribe((isPadlock: boolean) => {
      if (isPadlock) {
        this.accessPremisesForm.get("isPadlockOnGate").setValue(null);
        this.accessPremisesForm.get("isPadlockOnGate").enable();
        this.accessPremisesForm.get("padlockDescription").enable();
        this.accessPremisesForm = setRequiredValidator(this.accessPremisesForm, ['isPadlockOnGate']);
      }
      else {
        this.accessPremisesForm.get("isPadlockOnGate").disable({ emitEvent: false });
        this.accessPremisesForm.get("padlockDescription").disable();
        this.accessPremisesForm.get("isPadlockOnGate").setValue(null);
        this.accessPremisesForm.get("padlockDescription").setValue(null);
        this.accessPremisesForm = clearFormControlValidators(this.accessPremisesForm, ['isPadlockOnGate']);
      }
    });
    this.accessPremisesForm.get('isBarrelLockFitted').valueChanges.subscribe((isBarrelLock: boolean) => {
      if (isBarrelLock) {
        this.accessPremisesForm.get("isBarrelLockFittedOnGate").setValue(null);
        this.accessPremisesForm.get("barrelLockFittedDescription").enable();
        this.accessPremisesForm.get("isBarrelLockFittedOnGate").enable();
        this.accessPremisesForm = setRequiredValidator(this.accessPremisesForm, ['isBarrelLockFittedOnGate']);
      }
      else {
        this.accessPremisesForm.get("isBarrelLockFittedOnGate").disable({ emitEvent: false });
        this.accessPremisesForm.get("barrelLockFittedDescription").disable();
        this.accessPremisesForm.get("isBarrelLockFittedOnGate").setValue(null);
        this.accessPremisesForm.get("barrelLockFittedDescription").setValue(null);
        this.accessPremisesForm = clearFormControlValidators(this.accessPremisesForm, ['isBarrelLockFittedOnGate']);
      }
    });
    this.accessPremisesForm.get('isDigipadOnGate').valueChanges.subscribe((isDigipad: boolean) => {
      if (isDigipad) {
        if (this.digipads != null) {
          this.digipads.clear();
          this.digipads = this.getDigiPadListArray;
          this.digipads.push(this.createDigipadListModel());
        }
        this.accessPremisesForm.get("digipadDescription").enable();
      }
      else {
        if (this.digipads != null) {
          this.digipads.clear();
        }
        this.accessPremisesForm.get("digipadDescription").disable();
      }
    });

    this.accessPremisesForm.get('isLockBox').valueChanges.subscribe((isLockBox: boolean) => {
      if (isLockBox) {
        if (this.lockBoxes != null) {
          this.lockBoxes.clear();
          this.lockBoxes = this.getLockBoxesListArray;
          this.lockBoxes.push(this.createLockBoxesListModel());
        }
        this.accessPremisesForm.get("lockBoxDescription").enable();
      }
      else {
        if (this.lockBoxes != null) {
          this.lockBoxes.clear();
        }
        this.accessPremisesForm.get("lockBoxDescription").disable();
      }
    });

    this.accessPremisesForm.get('isOpenAccess').valueChanges.subscribe((isOpenAccess: boolean) => {
      if (isOpenAccess) {
        this.accessPremisesForm.get("openAccessDescription").enable();
      } else {
        this.accessPremisesForm.get("openAccessDescription").disable();
      }
    });

    this.accessPremisesForm.get('isSecureGate').valueChanges.subscribe((isSecureGate: boolean) => {
      if (isSecureGate) {
        this.accessPremisesForm.get("secureGateDescription").enable();
      } else {
        this.accessPremisesForm.get("secureGateDescription").disable();
      }
    });

    this.accessPremisesForm.get('isArmedResponseReceiverOnGate').valueChanges.subscribe((isArmedResponseReceiverOnGate: boolean) => {
      if (isArmedResponseReceiverOnGate) {
        this.accessPremisesForm.get("armedResponseReceiverOnGateDescription").enable();
      } else {
        this.accessPremisesForm.get("armedResponseReceiverOnGateDescription").disable();
      }
    });

    this.accessPremisesForm.get('isAuthroziedNoAccess').valueChanges.subscribe((isAuthroziedNoAccess: boolean) => {
      if (isAuthroziedNoAccess) {
        this.accessPremisesForm.get("authroziedNoAccessDescription").enable();
      } else {
        this.accessPremisesForm.get("authroziedNoAccessDescription").disable();
      }
    });

    this.accessPremisesForm.get('isUnProtectWall').valueChanges.subscribe((isUnProtectWall: boolean) => {
      if (isUnProtectWall) {
        this.accessPremisesForm.get("unProtectWallDescription").enable();
      } else {
        this.accessPremisesForm.get("unProtectWallDescription").disable();
      }
    });
  }

  getAccessPremisesByLeadAndPartitionId(): void {

      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.NON_CUSTOMER_ACCESS_TO_PREMISES_DETAIL, undefined, false,
        prepareGetRequestHttpParams(undefined, undefined, {
          customerId: this.customerId,

        }), 1).subscribe((response: IApplicationResponse) => {
      if (response.statusCode == 200 && response.resources && response.isSuccess) {
        this.details = response.resources;

        if (!this.details.isArmedResponsePadlock) {
          this.details.isPadlockOnGate = null;
        }

        if (!this.details.isBarrelLockFitted) {
          this.details.isBarrelLockFittedOnGate = null;
        }
        if (!this.details.digipads) {
          this.details.digipads = [];
        }
        if (!this.details.lockBoxes) {
          this.details.lockBoxes = [];
        }
        if (this.details.accessPremiseId || this.details.customerId) {
          this.accessPremisesForm.patchValue(this.details);
          this.digipads = this.getDigiPadListArray;
          this.lockBoxes = this.getLockBoxesListArray;
          if (response.resources.digipads.length > 0) {
            response.resources.digipads.forEach((dg) => {
              this.digipads.push(this.createDigipadListModel(dg));
            });
          }
          else {
            this.digipads.push(this.createDigipadListModel());
          }
          if (response.resources.lockBoxes.length > 0) {
            response.resources.lockBoxes.forEach((md) => {
              this.lockBoxes.push(this.createLockBoxesListModel(md));
            });
          }
          else {
            this.lockBoxes.push(this.createLockBoxesListModel());
          }
        } else {
          this.digipads = this.getDigiPadListArray;
          this.digipads.push(this.createDigipadListModel());

          this.lockBoxes = this.getLockBoxesListArray;
          this.lockBoxes.push(this.createLockBoxesListModel());
        }

      } else {
        this.digipads = this.getDigiPadListArray;
        this.digipads.push(this.createDigipadListModel());

        this.lockBoxes = this.getLockBoxesListArray;
        this.lockBoxes.push(this.createLockBoxesListModel());
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);

    // });
  }

  createAccessPremisesForm(): void {
    let accessToPremisesAddEditModel = new AccessToPremisesAddEditModel();
    this.accessPremisesForm = this.formBuilder.group({
      digipads: this.formBuilder.array([]),
      lockBoxes: this.formBuilder.array([])
    });
    Object.keys(accessToPremisesAddEditModel).forEach((key) => {
      if (key == 'lockBoxDescription' || key == 'armedResponseReceiverOnGateDescription' || key == 'authroziedNoAccessDescription' || key == 'unProtectWallDescription' || key == 'lockBoxDescription' || key == 'openAccessDescription' || key == 'secureGateDescription' || key == 'digipadDescription' || key == 'barrelLockFittedDescription' || key == 'padlockDescription') {
        this.accessPremisesForm.addControl(key, new FormControl({ value: accessToPremisesAddEditModel[key], disabled: true }));

        // this.accessPremisesForm.addControl(key, new FormControl(accessToPremisesAddEditModel[key],dis));
      } else {
        this.accessPremisesForm.addControl(key, new FormControl(accessToPremisesAddEditModel[key]));

      }
    });
    this.accessPremisesForm.get("isPadlockOnGate").setValue(null);
    this.accessPremisesForm.get("isBarrelLockFittedOnGate").setValue(null);

    this.accessPremisesForm.get("isPadlockOnGate").disable();
    this.accessPremisesForm.get("isBarrelLockFittedOnGate").disable()
  }

  addLockBox() {
    if (this.lockBoxes.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.lockBoxes = this.getLockBoxesListArray;
    let lockBoxesModel = new LockBoxesModel();
    this.lockBoxes.insert(0, this.createLockBoxesListModel(lockBoxesModel));

  }

  addDigipad() {
    if (this.digipads.invalid) {
      this.focusInAndOutFormArrayFields();
      return;
    };
    this.digipads = this.getDigiPadListArray;
    let digiPadModel = new DigiPadModel();
    this.digipads.insert(0, this.createDigipadListModel(digiPadModel));

  }


  get getDigiPadListArray(): FormArray {
    if (!this.accessPremisesForm) return;
    return this.accessPremisesForm.get("digipads") as FormArray;
  }


  //Create FormArray controls
  createDigipadListModel(digipad?: DigiPadModel): FormGroup {
    let digipadFormControlModel = new DigiPadModel(digipad);
    let formControls = {};
    Object.keys(digipadFormControlModel).forEach((key) => {
      if (key != 'accessPremiseDigipadId') {
        formControls[key] = [{ value: digipadFormControlModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: digipadFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  get getLockBoxesListArray(): FormArray {
    if (!this.accessPremisesForm) return;
    return this.accessPremisesForm.get("lockBoxes") as FormArray;
  }

  removeLockBox(i) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getLockBoxesListArray.controls[i].value.accessPremiseLockBoxId && this.getLockBoxesListArray.length > 1) {
        let resultObservable =
          this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_DELETE_LOCKBOX,
            undefined,
            prepareRequiredHttpParams({
              Ids: this.getLockBoxesListArray.controls[i].value.accessPremiseLockBoxId,
              IsDeleted: false,
              modifiedUserId: this.loggedInUserData.userId
            }), 1)

        resultObservable.subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getLockBoxesListArray.removeAt(i);
          }
          if (this.getLockBoxesListArray.length === 0) {
            this.addLockBox();
          };

        });
      }
      else {
        this.getLockBoxesListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  removeDigipad(i) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getDigiPadListArray.controls[i].value.accessPremiseDigipadId && this.getDigiPadListArray.length > 1) {
        let resultObservable =
          this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_DELETE_DIGIPAD,
            undefined,
            prepareRequiredHttpParams({
              Ids: this.getDigiPadListArray.controls[i].value.accessPremiseDigipadId,
              IsDeleted: false,
              modifiedUserId:this.loggedInUserData.userId
            }), 1)
        resultObservable.subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getDigiPadListArray.removeAt(i);
          }
          if (this.getDigiPadListArray.length === 0) {
            this.addDigipad();
          };
        });
      }
      else {
        this.getDigiPadListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  //Create FormArray controls
  createLockBoxesListModel(lockBoxesModel?: LockBoxesModel): FormGroup {
    let lockBoxesModelFormControlModel = new LockBoxesModel(lockBoxesModel);
    let formControls = {};
    Object.keys(lockBoxesModelFormControlModel).forEach((key) => {
      if (key != 'accessPremiseLockBoxId') {
        formControls[key] = [{ value: lockBoxesModelFormControlModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: lockBoxesModelFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  onSubmit(): void {
    if (!this.accessPremisesForm.value.isDigipadOnGate) {
      if (this.digipads) {
        this.digipads.clear();
      }
    }
    if (!this.accessPremisesForm.value.isLockBox) {
      if (this.lockBoxes) {
        this.lockBoxes.clear();
      }
    }
    if (this.accessPremisesForm.invalid) {
      return;
    }
    this.accessPremisesForm.value.createdUserId = this.loggedInUserData.userId
    this.accessPremisesForm.value.modifiedUserId = this.loggedInUserData.userId

    this.accessPremisesForm.value.customerId = this.customerId;

    let obj = this.accessPremisesForm.value;
    if (!obj.isDigipadOnGate) {
      obj.digipads = null;
    }
    if (!obj.accessPremiseId) {
      this.rxjsService.setFormChangeDetectionProperty(true);
    }
    if (!obj.isLockBox) {
      obj.lockBoxes = null;
    }
    if (!obj.isArmedResponsePadlock && !obj.isPadlockOnGate && !obj.isBarrelLockFitted && !obj.isArmedResponsePadlock && !obj.isBarrelLockFittedOnGate && !obj.isSecureGate && !obj.isArmedResponseReceiverOnGate && !obj.isUnProtectWall && !obj.isOpenAccess && !obj.isAuthroziedNoAccess && !obj.isDigipadOnGate && !obj.isLockBox && !obj.siteInstructionForAROfficer) {
      this.snackbarService.openSnackbar("Atleast one should be selected..!!", ResponseMessageTypes.WARNING);
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let resultObservable =
      this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.NON_CUSTOMER_ACCESS_TO_PREMISES, obj, 1)

    resultObservable.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        if (!this.accessPremisesForm.value.accessPremiseId) {
          this.accessPremisesForm.get('accessPremiseId').setValue(response.resources);
        }
        this.onArrowClick("next")
      }
    })
  }

  redirectToTab(type: btnActionTypes | string = btnActionTypes.SUBMIT): void {

  }

  onEditBtnClicked() {
    this.isDisabled = false;
  }

  onArrowClick(type: string) {
    if(type == "previous"){
      this.router.navigate(['customer/non-customer-profile/arming-disarming-time'],{queryParams:{customerId :this.customerId, feature:this.feature,addressId:this.addressId}})
    }else {
      this.router.navigate(['customer/non-customer-profile/debtor-information'],{queryParams:{customerId :this.customerId,feature:this.feature,addressId:this.addressId}})
    }
  }
}
