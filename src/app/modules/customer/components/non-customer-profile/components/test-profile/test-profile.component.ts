import { Component, ViewChild } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { clearFormControlValidators, CrudService, HttpCancelService, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { CustomerModuleApiSuffixModels } from "@modules/customer";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { AddressModel } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { SharedAddressInformationComponent } from "../../shared/components/address-information/address-information.component";
import { SharedCustomerInformationComponent } from "../../shared/components/customer-information/customer-information.component";
import { FEATURE_NAME, FORMTYPE } from "../../shared/enum/index.enum";
import { ContactPersonModel, CustomerInformationModel, ProfileManagerModel } from "../../shared/model";

@Component({
    selector: 'app-test-profile',
    templateUrl: './test-profile.component.html',
    styleUrls: ['./test-profile.component.scss'],
})
export class TestProfileAddEditComponent {
    @ViewChild(SharedCustomerInformationComponent, { static: false }) customerInformationComponent: SharedCustomerInformationComponent;
    @ViewChild(SharedAddressInformationComponent, { static: false }) SharedAddressInformationComponent: SharedAddressInformationComponent;

    testProfileForm: FormGroup;
    loggedUser: UserLogin;
    feature: FEATURE_NAME;
    customerId: string;
    // formArray
    contactPersonArray: FormArray;
    profileManagerArray: FormArray;
    cdmArray: FormArray;
    cameraInformationArray: FormArray;
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private crudService: CrudService,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private store: Store<AppState>, private rxjsService: RxjsService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.feature = this.activatedRoute.snapshot.queryParams.feature;
        this.customerId = this.activatedRoute.snapshot.queryParams.id;
    }

    ngOnInit(): void {
        this.createTestProfileForm();

    }
    createTestProfileForm(): void {
        this.testProfileForm = this.formBuilder.group({
            createdUserId: [this.loggedUser?.userId]
        });

        this.testProfileForm.addControl('customerInformationDTO', this.createCustomerInformationModel());
        this.testProfileForm.addControl('addressInfo', this.createAddressInformationModel());

        this.testProfileForm.controls["customerInformationDTO"] = setRequiredValidator(
            this.testProfileForm.controls["customerInformationDTO"] as FormGroup,
            ["customerTypeId", "titleId", "firstName", "lastName", "email", "mobile1"]
        );

        this.testProfileForm.controls["addressInfo"] = setRequiredValidator(
            this.testProfileForm.controls["addressInfo"] as FormGroup,
            ["formatedAddress", "streetNo"]
        );

        this.contactPersonArray = this.getContactPersontArray
        this.profileManagerArray = this.getProfileManagerArray
        if (!this.customerId) {
            this.contactPersonArray.push(this.createProfileContactPersonFormArray());
            this.profileManagerArray.push(this.createProfileManagerFormArray());
        }

    }
    createCustomerInformationModel(basicInfo?: CustomerInformationModel): FormGroup {
        let customerInfo = new CustomerInformationModel(basicInfo);
        let formControls = {};
        Object.keys(customerInfo).forEach((key) => {
            formControls[key] = new FormControl(customerInfo[key]);
        });
        return this.formBuilder.group(formControls);
    }
    createAddressInformationModel(addressModel?: AddressModel): FormGroup {
        let _addressInfo = new AddressModel(addressModel);
        let formControls = {};
        Object.keys(_addressInfo).forEach((key) => {
            formControls[key] = new FormControl(_addressInfo[key]);
        });
        return this.formBuilder.group(formControls);
    }
    get getContactPersontArray(): FormArray {
        if (!this.testProfileForm) return;
        return this.testProfileForm.get("nonCustomerContactPersonDTOs") as FormArray;
    }
    get getProfileManagerArray(): FormArray {
        if (!this.testProfileForm) return;
        return this.testProfileForm.get("nonCustomerProfileManagerDTOs") as FormArray;
    }
    createProfileContactPersonFormArray(contactPerson?: ContactPersonModel): FormGroup {
        let _contactPerson = new ContactPersonModel(contactPerson);
        let formControls = {};
        Object.keys(_contactPerson).forEach((key) => {
            if (key == "contactPersion" || key == "contactNumber" || key == 'contactNumberCountryCode') {
                formControls[key] = new FormControl(_contactPerson[key], [Validators.required]);
            } else {
                formControls[key] = new FormControl(_contactPerson[key]);
            }
        });
        return this.formBuilder.group(formControls);
    }
    createProfileManagerFormArray(profileManager?: ProfileManagerModel): FormGroup {
        let _profileManager = new ProfileManagerModel(profileManager);
        let formControls = {};
        Object.keys(_profileManager).forEach((key) => {
            if (key == "roleId" || key == "employeeId") {
                formControls[key] = new FormControl(_profileManager[key], [Validators.required]);
            } else {
                formControls[key] = new FormControl(_profileManager[key]);
            }
        });
        return this.formBuilder.group(formControls);
    }
    emitCustomerType(data) {
        if (data == true) {
          this.testProfileForm.controls["customerInformationDTO"] = setRequiredValidator(
            this.testProfileForm.controls["customerInformationDTO"] as FormGroup,
            ["companyName"]
          );
    
        } else {
          this.testProfileForm.controls["customerInformationDTO"] = clearFormControlValidators(
            this.testProfileForm.controls["customerInformationDTO"] as FormGroup,
            ["companyName"]
          );
        }
      }
    
      emitCustomerInformation(event) {
        switch (event.type) {
          case FORMTYPE.CUSTOMER_INFORMATION:
            this.testProfileForm.get('customerInformationDTO').setValue(event.data);
            break;
          case FORMTYPE.ADDRESS_INFORMATION:
            this.testProfileForm.get('addressInfo').setValue(event.data);
    
            break;
          default:
            break;
        }
      }
      afrigisAddressInfo  ={};
      emitAfrigisAddressInfo(event){
        this.afrigisAddressInfo = {
          buildingName: event?.buildingName,
          buildingNo: event?.buildingNo,
          streetNo: event?.streetNo,
        };
      }
    
      emitChangeFormCheckbox (event){
        if(event.type == "addressExtra"){
          if(event.action =="add"){
            this.testProfileForm.controls["addressInfo"].get('estateStreetNo').setValidators([Validators.required]);
            this.testProfileForm.controls["addressInfo"].get('estateStreetNo').updateValueAndValidity();
            this.testProfileForm.controls["addressInfo"].get('estateStreetName').setValidators([Validators.required]);
            this.testProfileForm.controls["addressInfo"].get('estateStreetName').updateValueAndValidity();
            this.testProfileForm.controls["addressInfo"].get('estateName').setValidators([Validators.required]);
            this.testProfileForm.controls["addressInfo"].get('estateName').updateValueAndValidity();
          }else{
            this.testProfileForm.controls["addressInfo"].get('estateStreetNo').clearValidators();
            this.testProfileForm.controls["addressInfo"].get('estateStreetNo').updateValueAndValidity();
            this.testProfileForm.controls["addressInfo"].get('estateStreetName').clearValidators();
            this.testProfileForm.controls["addressInfo"].get('estateStreetName').updateValueAndValidity();
            this.testProfileForm.controls["addressInfo"].get('estateName').clearValidators();
            this.testProfileForm.controls["addressInfo"].get('estateName').updateValueAndValidity();
          }
        }
        if(event.type =="complex"){
    
          if(event.action  =='add'){
            this.testProfileForm.controls["addressInfo"].get('buildingNo').setValidators([Validators.required]);
            this.testProfileForm.controls["addressInfo"].get('buildingNo').updateValueAndValidity();
            this.testProfileForm.controls["addressInfo"].get('buildingName').setValidators([Validators.required]);
            this.testProfileForm.controls["addressInfo"].get('buildingName').updateValueAndValidity();
          }else{
            this.testProfileForm.controls["addressInfo"].get('buildingNo').clearValidators();
            this.testProfileForm.controls["addressInfo"].get('buildingNo').updateValueAndValidity();
            this.testProfileForm.controls["addressInfo"].get('buildingName').clearValidators();
            this.testProfileForm.controls["addressInfo"].get('buildingName').updateValueAndValidity();
          }
        }
    
      }
      onSubmit(): void {
        if(this.testProfileForm.touched){
          this.rxjsService.setFormChangeDetectionProperty(true);
        }
        this.testProfileForm
          .get("createdUserId")
          .setValue(this.loggedUser?.userId);
        if (this.testProfileForm.invalid) return;
    
        this.testProfileForm.value.customerInformationDTO.mobile1 =
          this.testProfileForm.value.customerInformationDTO.mobile1
            .toString()
            .replace(/\s/g, "");
        if (this.testProfileForm.value.customerInformationDTO.mobile2) {
          this.testProfileForm.value.customerInformationDTO.mobile2 =
            this.testProfileForm.value.customerInformationDTO.mobile2
              .toString()
              .replace(/\s/g, "");
        }
        if (this.testProfileForm.value.customerInformationDTO.officeNo) {
          this.testProfileForm.value.customerInformationDTO.officeNo =
            this.testProfileForm.value.customerInformationDTO.officeNo
              .toString()
              .replace(/\s/g, "");
        }
        if (this.testProfileForm.value.customerInformationDTO.premisesNo) {
          this.testProfileForm.value.customerInformationDTO.premisesNo =
            this.testProfileForm.value.customerInformationDTO.premisesNo
              .toString()
              .replace(/\s/g, "");
        }
        this.testProfileForm.value.customerInformationDTO.customerId = this.customerId;
       // if(this.testProfileForm.value.customerInformationDTO.siteTypeId)
          this.testProfileForm.value.customerInformationDTO.siteTypeId = 0;
       
          this.crudService
          .create(
            ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            CustomerModuleApiSuffixModels.TEST_PROFILE_CUSTOMER,
            this.testProfileForm.value
          )
          .subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200 && response.resources) {
              this.customerId = this.customerId ? this.customerId : response.resources;
              this.router.navigate(["customer/non-customer-profile"],{queryParams: {tab:6}});
            }
          });
      }
    
}