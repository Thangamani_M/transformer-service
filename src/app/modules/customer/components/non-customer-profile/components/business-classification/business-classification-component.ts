import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { CustomerModuleApiSuffixModels } from "@modules/customer";
import { AddServices } from "@modules/customer/models/add-services.model";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { SharedBusinessClassificationComponent } from "../../shared/components/business-classification-form/business-classification-form-component";
import { FEATURE_NAME, FORMTYPE } from "../../shared/enum/index.enum";
import { BusinessClassificationModel } from "../../shared/model/business-classification.model";
import { AddServicesComponent } from "../add-services/add-services-component";


@Component({
    selector: 'app-business-classification',
    templateUrl: './business-classification-component.html',
    styleUrls: ['./business-classification-component.scss'],
  })

  export class BusinessClassificationComponent implements OnInit {
    @ViewChild(SharedBusinessClassificationComponent, { static: false })
    businessClassificationComponent: SharedBusinessClassificationComponent;
    isEditAccountNumber = true
    businessClassificationForm : FormGroup;
    feature:string;
    loggedUser;
    customerId ;
    cameraProfileObject :any;
    serviceObj={};
    counterKey=-1;
    debtorAPI : CustomerModuleApiSuffixModels
    previousPageRouteUrl :string
    featureName = "Camera Profile";
    tabIndex  = 0;
    businessClassificationDetails;
    customerTypeId:string;
    isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
    constructor(
        private router: Router,
        private crudService: CrudService,
        private dialog: MatDialog,
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder,
        private store: Store<AppState>, private rxjsService: RxjsService) {
        this.activatedRoute.queryParams.subscribe(param => {
          this.feature = param.feature
          this.customerId =  this.activatedRoute.snapshot.queryParams.customerId
          this.customerTypeId =  this.activatedRoute.snapshot.queryParams.customerTypeId
        });
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
          if (!userData) return;
          this.loggedUser = userData;
        });
      }

    ngOnInit(): void {
       this.createbusinessClassificationForm();
       this.createSiteInformationForm();
       this.createServiceInfoForm();
       this.getApiEndPoint();
       if(this.customerId){
        this.getBusinessClassificationById();
      }
    }
    createbusinessClassificationForm() {
        this.businessClassificationForm = this.formBuilder.group({
           createdUserId: [this.loggedUser?.userId],
          });
          this.businessClassificationForm.addControl(
            "businessClassification",
            this.createBusinessClassificationFormModel()
          );
          this.businessClassificationForm.addControl(
            "businessService",
            this.createServiceInfoForm()
          );

    }
    createBusinessClassificationFormModel(
      profileInfo?: BusinessClassificationModel
    ): FormGroup {
      let _profileInfo = new BusinessClassificationModel(profileInfo);
      let formControls = {};
      Object.keys(_profileInfo).forEach((key) => {
        formControls[key] = new FormControl(_profileInfo[key]);
      });
      return this.formBuilder.group(formControls);
    }
    createSiteInformationForm(){
      this.businessClassificationForm.addControl("execuGuardAccountNumber",new FormControl(null) );
      this.businessClassificationForm = setRequiredValidator(this.businessClassificationForm, ['execuGuardAccountNumber']);
    }
    createServiceInfoForm() {
      let addServicesFormModel = new AddServices();
      let formControls = {};
      Object.keys(addServicesFormModel).forEach((key) => {
        formControls[key] = new FormControl(addServicesFormModel[key]);
      });
      return this.formBuilder.group(formControls);

    }
    emitBusinessClassification(event) {
      switch (event && event.type) {
        case FORMTYPE.BUSINESS_CLASSIFICATION:
             this.businessClassificationForm.get("businessClassification").patchValue(event.data);
          break;
          default:
          break;
      }
    }
    emitCustomerType(data) {
      if (data == true) {
        // this.businessClassificationForm.controls["customerInformationDTO"] =
        //   setRequiredValidator(
        //     this.businessClassificationForm.controls[
        //       "businessClassificationForm"
        //     ] as FormGroup,
        //     ["companyName"]
        //   );
      } else {
        // this.businessClassificationForm.controls["businessClassificationForm"] =
        //   clearFormControlValidators(
        //     this.businessClassificationForm.controls[
        //       "businessClassificationForm"
        //     ] as FormGroup,
        //     ["companyName"]
        //   );
      }
    }
    getApiEndPoint(): string {
      let api;
      switch (this.feature) {

        case FEATURE_NAME.EXECU_GUARD_PROFILE:
          api = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_CLASSIFICATION
          this.debtorAPI = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_CLASSIFICATION_DETAIL
          this.previousPageRouteUrl ='customer/non-customer-profile/execu-guard-profile'
          this.featureName = "Execu Guard Profile"
          this.tabIndex =3
          break;

        default:
          break;
      }
      return api
    }

    getBusinessClassificationById() {
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,this.debtorAPI, undefined, false, prepareGetRequestHttpParams(null, null, {
        customerId: this.customerId
      }), 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.businessClassificationDetails = response.resources;
          if(this.businessClassificationDetails?.execuGaurdAccountNumber?.execuGuardAccountNumber){
            this.isEditAccountNumber = false
          }
        this.businessClassificationForm.get('execuGuardAccountNumber').setValue(this.businessClassificationDetails.execuGaurdAccountNumber.execuGuardAccountNumber);
          let classification = {
          categoryId: this.businessClassificationDetails.classifications?.categoryId.toString(),
          debtorGroupId: this.businessClassificationDetails.classifications?.debtorGroupId.toString(),
          installOriginId: this.businessClassificationDetails.classifications?.installOriginId.toString(),
          originId: this.businessClassificationDetails.classifications?.originId.toString(),
          subCategoryId: this.businessClassificationDetails.classificationsubcategory?.subCategoryId.toString(),
          customerClassificationMappingId:"",
        }
        this.businessClassificationComponent.businessClassificationForm.patchValue(
            classification
        );
        this.businessClassificationComponent.emitDataWhenFormChange();
        this.businessClassificationComponent.onChangeOrigin(true)
         this.businessClassificationForm.get('businessClassification').patchValue(classification);
         if(this.businessClassificationDetails.services &&  this.businessClassificationDetails.services.length >0) {
            this.businessClassificationDetails.services.forEach(element => {
              this.serviceObj[element.serviceCategoryName] = {};
              this.serviceObj[element.serviceCategoryName].serviceCategoryName = element.serviceCategoryName;
              let services=[];
              element.services.forEach(eles => {
                eles.serviceMappingId = eles.execuGuardServiceId;
                eles.qty =  eles?.qty ? eles.qty : 1;
                services.push(eles);
              });
              this.serviceObj[element.serviceCategoryName]['values'] = services;
            });
         }
         this.businessClassificationForm.value.businessService = [];
         Object.keys(this.serviceObj).forEach((key) => {
             this.serviceObj[key]['values'].forEach((service) => {
               this.businessClassificationForm.value.businessService.push(service);
             });
         });

        }
      });

    }
    navigateTo(page) {
      if(page == 1) {
        this.router.navigate(['customer/non-customer-profile/execu-guard-profile-add-edit'] ,{queryParams:{id : this.customerId, feature:this.feature}})
      }
      if(page == 3) {
        this.router.navigate(['customer/non-customer-profile/debtor-information'] ,{queryParams:{customerId : this.customerId, customerTypeId :  this.customerTypeId, feature:this.feature}})
      }
    }
    navigate(){

        this.router.navigate(['customer/non-customer-profile/execu-guard-profile-add-edit'] ,{queryParams:{id : this.customerId, feature:this.feature}})

    }
    goBack(){
      this.router.navigate(['customer/non-customer-profile'] ,{queryParams:{tab :5}})
    }
    onAddServices() {
      // if (this.serviceInfoAddEditForm.invalid) return;
      let data = { ...{customerId : this.customerId},...{serviceObj:null} };
      data['serviceObj'] = this.serviceObj;
      const dialogRef = this.dialog.open(AddServicesComponent, {
        width: "1200px",
        data,
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (typeof dialogResult === 'boolean' && dialogResult) {
          //this.isFormChangeDetected = false;
          this.rxjsService.setGlobalLoaderProperty(false);
          return;
        }
      });
      var self = this;
      dialogRef.componentInstance.outputData.subscribe(ele => {
        this.counterKey++;
        self.serviceObj = ele.serviceObj;
        dialogRef.close();
        this.rxjsService.setGlobalLoaderProperty(false);
       // this.isFormChangeDetected = true;
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    onQuantityChange(type, serviceSubItem: object): void {
      if (type === 'plus') {
        serviceSubItem['qty'] = serviceSubItem['qty'] + 1;
      }
      else {
        serviceSubItem['qty'] = (serviceSubItem['qty'] === 1) ? serviceSubItem['qty'] : serviceSubItem['qty'] - 1;
      }
    }
    removeItem(serviceItemKey: string, serviceItemObj: any) {
      const message = `Are you sure you want to delete this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData,
        disableClose: true
      });

      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        let areAllServicesRemoved = false;
       let execuGuardServiceId = serviceItemObj?.execuGuardServiceId? serviceItemObj.execuGuardServiceId :null;
       if(execuGuardServiceId) {
          this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_SERVICES, undefined,
            prepareRequiredHttpParams({
              execuGuardServiceId : execuGuardServiceId,
            }), 1).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                 if(this.customerId)
                  this.getBusinessClassificationById();
              }
            });
       }else {
         if(this.serviceObj && this.serviceObj[serviceItemKey] && this.serviceObj[serviceItemKey].values) {
          this.serviceObj[serviceItemKey].values.forEach((element,index) => {
            if(element.serviceMappingId == serviceItemObj.serviceMappingId)
                this.serviceObj[serviceItemKey].values.splice(index,1);
          });
         }
       }


       // this.selectedItemsWithKeyValue = JSON.parse(JSON.stringify(this.serviceObj));
      });
    }

    onSubmit() {
     if (this.businessClassificationForm.invalid) {
        return;
      }
      this.businessClassificationForm.value.businessService = [];
      Object.keys(this.serviceObj).forEach((key) => {
          this.serviceObj[key]['values'].forEach((service) => {
            this.businessClassificationForm.value.businessService.push(service);
          });

      });
      let categoryId = this.businessClassificationForm.value.businessClassification?.categoryId;
      let debtorGroupId = this.businessClassificationForm.value.businessClassification.debtorGroupId;
      let originId = this.businessClassificationForm.value.businessClassification?.originId;
      let installOriginId = this.businessClassificationForm.value.businessClassification?.installOriginId;
      let subCategoryId = this.businessClassificationForm.value.businessClassification?.subCategoryId;

      let formData = {
        customerId: this.customerId,
        execuGuardAccountNumber: this.businessClassificationForm.value.execuGuardAccountNumber,
        classification: {
          categoryId: categoryId ? Number(categoryId) : null,
          customerClassificationSubCategoryMappingId: "",
          debtorGroupId:  debtorGroupId ? Number(debtorGroupId) : null,
          execuGuardAccountNumber: "",
          installOriginId:  installOriginId ? Number(installOriginId) : null,
          originId: originId ? Number(originId) : null,
          qty: 1,
          serviceMappingId: "",
          subCategoryId:  subCategoryId ? Number(subCategoryId) : null,
        },
        services:  this.businessClassificationForm.value.businessService,
        createdUserId: this.businessClassificationForm.value.createdUserId
      }
      let _apiUrl: any = this.getApiEndPoint()
      this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, _apiUrl, formData, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.navigateTo(3);
        }
      });
    }


  }
