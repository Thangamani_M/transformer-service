
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData, selectStaticEagerLoadingTitlesState$ } from '@app/modules';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, removeFormControlError, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { DebterListComponent, SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { SearchDebterComponent } from '../../shared/components/search-debter/search-debter.component';
import { FEATURE_NAME } from '../../shared/enum/index.enum';
import { DebterInfoModel } from '../../shared/model/debtor-info.model';
@Component({
  selector: 'app-debtor-information',
  templateUrl: './debtor-information.component.html'
})

export class DebtorInformationComponent implements OnInit {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  debtorForm: FormGroup;
  loggedUser: LoggedInUserModel;
  titles = []
  customerId: any
  siteTypeId: any
  communityId:any
  feature:FEATURE_NAME

  constructor(
    private router: Router,
    private crudService: CrudService,
    private snackbarService: SnackbarService,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {

    this.activatedRoute.queryParams.subscribe(param => {
      this.customerId = param.customerId
      this.siteTypeId = param.customerTypeId == 1 ? 2: 1
      this.feature = param.feature
      this.communityId = param.communityId
    })

  }

  ngOnInit(): void {
    this.createDebterForm();
    this.getApiEndPoint()
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingTitlesState$),
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedUser = new LoggedInUserModel(response[0]);
        this.titles = response[1];
      });
      this.getdebtorInfoById()
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createDebterForm() {
    let serviceDebterModel = new DebterInfoModel();
    this.debtorForm = this.formBuilder.group({});
    Object.keys(serviceDebterModel).forEach((key) => {
      this.debtorForm.addControl(key,
        new FormControl(serviceDebterModel[key]));
    });
    this.debtorForm = setRequiredValidator(this.debtorForm, ["titleId", "firstName",
      "lastName", "email", "addressLine1", "addressLine2", "postalCode", "mobile1",]);
    this.debtorForm.get('premisesNoCountryCode').setValue('+27')
  }
  disableFlag = false;
  details: any = {}
  serviceAgreementStepsParams: any

  getdebtorInfoById() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,CustomerModuleApiSuffixModels.NON_CUSTOMER_DEBTOR_INFORMATION_DETAIL, undefined, false, prepareGetRequestHttpParams(null, null, {
      customerId: this.customerId
    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.details = response.resources;
        if (this.details.address) {
          this.details.leadAddress.addressLine1 = this.details.leadAddress.addressLine1 ? this.details.leadAddress.addressLine1.replace(/,/g, ' ') : '';
          this.details.leadAddress.addressLine2 = this.details.leadAddress.addressLine2 ? this.details.leadAddress.addressLine2.replace(/,/g, ' ') : '';
          this.details.leadAddress.addressLine3 = this.details.leadAddress.addressLine3 ? this.details.leadAddress.addressLine3.replace(/,/g, ' ') : '';
          this.details.leadAddress.addressLine4 = this.details.leadAddress.addressLine4 ? this.details.leadAddress.addressLine4.replace(/,/g, ' ') : '';
        }
        this.debtorForm.get('debtorTypeId').setValue(response?.resources?.debtorInfo?response.resources.debtorInfo.debtorTypeId:+this.siteTypeId);
        this.siteTypeId =response?.resources?.debtorInfo?response.resources.debtorInfo.debtorTypeId:+this.siteTypeId
        // if(this.feature == 'execu-guard-profile')
            // this.details.debtorInfo = this.details;

        if (this.details.debtorInfo) {
          this.debtorForm.patchValue(this.details.debtorInfo);
        }
        let check = this.debtorForm.value.isExistingDebtor;
        if (check) {
          this.debtorForm.get('premisesNoCountryCode').setValue('+27')
          this.debtorForm.get('mobile2CountryCode').setValue('+27')
          this.debtorForm.get('mobile1CountryCode').setValue('+27')
          this.debtorForm.get('officeNoCountryCode').setValue('+27')

          this.debtorForm.get('isExistingDebtor').setValue(true)
        } else {
          this.disableFlag = false;
          this.debtorForm.get('premisesNoCountryCode').setValue('+27')
          this.debtorForm.get('mobile2CountryCode').setValue('+27')
          this.debtorForm.get('mobile1CountryCode').setValue('+27')
          this.debtorForm.get('officeNoCountryCode').setValue('+27')
        }
        if (this.details.basicInfo) {
          this.debtorForm.patchValue(this.details.basicInfo);
        }
        if (this.details.address) {
          this.debtorForm.patchValue(this.details.address);
        }

        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  isDuplicate: boolean = false;
  onChange(str) {
    this.isDuplicate = false;
    let contact1 = this.debtorForm.value.mobile1;
    let contact2 = this.debtorForm.value.mobile2;
    let contact3 = this.debtorForm.value.premisesNo;
    if (str == 'contact1') {
      if ((contact1 == contact3) || (contact1 == contact2)) {
        this.snackbarService.openSnackbar("Contact number is duplicate", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
      } else {
        this.isDuplicate = false;
      }
    } else if (str == 'contact2') {
      if ((contact2 == contact1) || (contact2 == contact1)) {
        this.isDuplicate = true;
        this.snackbarService.openSnackbar("Contact number is duplicate", ResponseMessageTypes.WARNING);
      } else {
        this.isDuplicate = false;
      }
    } else {
      if ((contact3 == contact1) || (contact3 == contact2)) {
        this.isDuplicate = true;
        this.snackbarService.openSnackbar("Contact number is duplicate", ResponseMessageTypes.WARNING);
      } else {
        this.isDuplicate = false;
      }
    }
  }

  onFormControlChanges() {
    this.debtorForm.get('debtorTypeId').valueChanges.subscribe((debtorTypeId: number) => {
      if (!this.disableFlag && !this.debtorForm.value.isExistingDebtor) {
        if (debtorTypeId == 1) {
          this.debtorForm = setRequiredValidator(this.debtorForm, ["companyRegNo", "companyName"]);
          if (this.debtorForm.value.isDebtorSameAsCustomer) {
            this.debtorForm.get('companyName').setValue(this.details.customerInformationDTO.companyName)
            this.debtorForm.get('companyRegNo').setValue(this.details.customerInformationDTO.companyRegNo)
          }
          this.debtorForm = setRequiredValidator(this.debtorForm, ["companyRegNo", "companyName"]);
          this.debtorForm = clearFormControlValidators(this.debtorForm, ["said"]);
        } else if (debtorTypeId == 2) {
          if (this.debtorForm.value.isDebtorSameAsCustomer) {
            this.debtorForm.get('said').setValue(this.details.customerInformationDTO.said)
          }
          this.debtorForm = clearFormControlValidators(this.debtorForm, ["companyRegNo", "companyName"]);
          this.debtorForm = setRequiredValidator(this.debtorForm, ["said"]);
        }
      }
    });
  }

  debtorChange: boolean = false;
  changeIsDebtorSame() {
    this.debtorChange = true;
    let isDebtorSameAsCustomer = this.debtorForm.value.isDebtorSameAsCustomer;
    if (isDebtorSameAsCustomer) {
      this.debtorForm.patchValue({
        titleId: this.details.customerInformationDTO.titleId,
        isExistingDebtor: false,
        firstName: this.details.customerInformationDTO.firstName,
        email: this.details.customerInformationDTO.email,
        lastName: this.details.customerInformationDTO.lastName,
        mobile1: this.details.customerInformationDTO.mobile1,
        mobile1CountryCode: this.details.customerInformationDTO.mobile1CountryCode ? this.details.customerInformationDTO.mobile1CountryCode : '+27',
        mobile2CountryCode: this.details.customerInformationDTO.mobile2CountryCode ? this.details.customerInformationDTO.mobile2CountryCode : '+27',
        mobile2: this.details.customerInformationDTO.mobile2,
        officeNoCountryCode: this.details.customerInformationDTO.officeNoCountryCode ? this.details.customerInformationDTO.officeNoCountryCode : '+27',
        officeNo: this.details.customerInformationDTO.officeNo,
        premisesNoCountryCode: this.details.customerInformationDTO.premisesNoCountryCode ? this.details.customerInformationDTO.premisesNoCountryCode : '+27',
        premisesNo: this.details.customerInformationDTO.premisesNo,
        contactId: this.details.customerInformationDTO.contactId,
        said: this.details.customerInformationDTO.said,
        debtorTypeId: this.details.customerInformationDTO.customerTypeId,
        debtorId: this.details.customerInformationDTO.debtorId,
        debtorContactId: this.details.customerInformationDTO.debtorContactId,
        debtorAdditionalInfoId: this.details.customerInformationDTO.debtorAdditionalInfoId
      });
    } else {
      this.debtorForm.patchValue({
        titleId: "",
        firstName: "",
        email: "",
        lastName: "",
        mobile2: "",
        mobile1: "",
        contactId: null,
        said: "",
        debtorId: "",
        debtorContactId: "",
        debtorAdditionalInfoId: ""
      });
    }
  }

  changeIsBillingAddress() {
    let isBillingSameAsSiteAddress = this.debtorForm.value.isBillingSameAsSiteAddress;
    if (isBillingSameAsSiteAddress) {
      this.debtorForm.patchValue({
        addressLine1: this.details.leadAddress.addressLine1,
        addressLine2: this.details.leadAddress.addressLine2,
        addressLine3: this.details.leadAddress.addressLine3,
        addressLine4: this.details.leadAddress.addressLine4,
        postalCode: this.details.leadAddress.postalCode,
      })
    } else {
      this.debtorForm.patchValue({
        addressLine1: "",
        addressLine2: "",
        addressLine3: "",
        addressLine4: "",
        postalCode: "",
      })
    }
  }

  changeIsExisting() {
    this.debtorChange = true;
    let check = this.debtorForm.value.isExistingDebtor;
    if (check) {
      this.debtorForm.reset();
      this.debtorForm.get('premisesNoCountryCode').setValue('+27')
      this.debtorForm.get('titleId').setValue("")
      this.debtorForm.get('mobile2CountryCode').setValue('+27')
      this.debtorForm.get('mobile1CountryCode').setValue('+27')
      this.debtorForm.get('officeNoCountryCode').setValue('+27')
      this.debtorForm.get('isDebtorSameAsCustomer').setValue(false)
      this.debtorForm.get('isExistingDebtor').setValue(true)
      this.debtorForm = removeFormControlError(this.debtorForm as FormGroup, 'invalid');
    } else {
      this.disableFlag = false;
      this.debtorForm.reset();
      this.debtorForm.get('titleId').setValue("")
      this.debtorForm.get('premisesNoCountryCode').setValue('+27')
      this.debtorForm.get('officeNoCountryCode').setValue('+27')
      this.debtorForm.get('mobile2CountryCode').setValue('+27')
      this.debtorForm.get('mobile1CountryCode').setValue('+27')
      this.debtorForm.get('debtorTypeId').setValue(this.details.debtorTypeId)
    }
  }

  dataFromLinkDEbtor: any
  getDepterIdDetails(id, debtorAccountDetailId) {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_DEBTER_DETAILS, undefined, false, prepareGetRequestHttpParams(null, null, {
      debtorId: id,
      debtorAccountDetailId: debtorAccountDetailId,
      isLinkDebtor: true
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataFromLinkDEbtor = response.resources;
        this.disableFlag = true;
        if (this.details.address) {
          this.dataFromLinkDEbtor.addressLine1 = this.dataFromLinkDEbtor.addressLine1 ? this.dataFromLinkDEbtor.addressLine1.replace(/,/g, ' ') : '';
          this.dataFromLinkDEbtor.addressLine2 = this.dataFromLinkDEbtor.addressLine2 ? this.dataFromLinkDEbtor.addressLine2.replace(/,/g, ' ') : '';
          this.dataFromLinkDEbtor.addressLine3 = this.dataFromLinkDEbtor.addressLine3 ? this.dataFromLinkDEbtor.addressLine3.replace(/,/g, ' ') : '';
          this.dataFromLinkDEbtor.addressLine4 = this.dataFromLinkDEbtor.addressLine4 ? this.dataFromLinkDEbtor.addressLine4.replace(/,/g, ' ') : '';
        }
        this.debtorForm.patchValue(response.resources);
        if (this.dataFromLinkDEbtor.premisesNo == 0) {
          this.debtorForm.get('premisesNo').setValue('')
        }
        this.debtorForm.get('premisesNoCountryCode').setValue('+27')
        this.debtorForm.get('officeNoCountryCode').setValue('+27')
        this.debtorForm.get('isExistingDebtor').setValue(true);
        this.debtorForm.get('isBillingSameAsSiteAddress').setValue(true);
        let check = this.debtorForm.value.isExistingDebtor;
        if (check) {
          this.debtorForm.get('premisesNoCountryCode').setValue('+27')
          this.debtorForm.get('mobile2CountryCode').setValue('+27')
          this.debtorForm.get('mobile1CountryCode').setValue('+27')
          this.debtorForm.get('officeNoCountryCode').setValue('+27')
          this.debtorForm.get('isExistingDebtor').setValue(true)
        } else {
          this.disableFlag = false;
          this.debtorForm.reset();
          this.debtorForm.get('premisesNoCountryCode').setValue('+27')
          this.debtorForm.get('mobile2CountryCode').setValue('+27')
          this.debtorForm.get('mobile1CountryCode').setValue('+27')
          this.debtorForm.get('officeNoCountryCode').setValue('+27')
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  openModal() {
    const dialogReff = this.dialog.open(SearchDebterComponent, { width: '960px', disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.openDebterExistModal(ele);
      }
    })
  }

  openDebterExistModal(data) {
    const dialogReff = this.dialog.open(DebterListComponent, { width: '960px', data: data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
    });
    dialogReff.componentInstance.outputData1.subscribe(ele => {
      if (ele) {
        this.getDepterIdDetails(ele.debtorId, ele.debtorAccountDetailId);
      }
    })
  }


  onArrowClick(action) {

  }
  debtorAPI : CustomerModuleApiSuffixModels
  previousPageRouteUrl :string
  featureName = "Camera Profile";
  tabIndex  = 0
  featureIndex =2
  isShowExistingSearch =true
  getApiEndPoint(): string {
    let api;
    switch (this.feature) {
      case FEATURE_NAME.CAMERA_PROFILE:
        api = CustomerModuleApiSuffixModels.CAMERA_PROFILE_DEBTOR
        this.debtorAPI =  CustomerModuleApiSuffixModels.CAMERA_PROFILE_DEBTOR_DETAIL
        this.previousPageRouteUrl ='customer/non-customer-profile/camera-profile-add-edit'
        this.featureName = "Camera Profile"
        this.tabIndex =0
        break;
      case FEATURE_NAME.NON_CLIENT:
        api = CustomerModuleApiSuffixModels.NON_CLIENT_PROFILE_DEBTOR
        this.debtorAPI = CustomerModuleApiSuffixModels.NON_CLIENT_PROFILE_DEBTOR_DETAIL
        this.previousPageRouteUrl ='customer/non-customer-profile/non-client-profile-add-edit'
        this.featureName = "Non-Client"
        this.tabIndex =1
        break;
      case FEATURE_NAME.HUB_PROFILE:
        api = CustomerModuleApiSuffixModels.HUB_PROFILE_DEBTOR
        this.debtorAPI = CustomerModuleApiSuffixModels.HUB_PROFILE_DEBTOR_DETAILS
        this.previousPageRouteUrl ='customer/non-customer-profile/hub-profile-add-edit'
        this.featureName = "Hub Profile"
        this.tabIndex =2
        break;
      case FEATURE_NAME.EXECU_GUARD_PROFILE:
        api = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_DEBATOR
        this.debtorAPI = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_DEBATOR_DETAILS
        this.previousPageRouteUrl ='customer/non-customer-profile/business-classification'
        this.featureName = "ExecuGuard Profile"
        this.featureIndex =3
        this.tabIndex =5
        break;

        case FEATURE_NAME.CASUAL_GUARDING:
          // api = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_DEBATOR
          // this.debtorAPI = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_DEBATOR_DETAILS
          this.previousPageRouteUrl ='customer/non-customer-profile/casual-guarding-add-edit'
          this.featureName = "Casual Guarding"
          this.tabIndex =8
          break;
        case FEATURE_NAME.ADT_PROFILE:
          // api = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_DEBATOR
          // this.debtorAPI = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_DEBATOR_DETAILS
          this.previousPageRouteUrl ='customer/non-customer-profile/access-to-premises'
          this.featureName = "ADT Site Profile"
          this.featureIndex =7
          break;

          case FEATURE_NAME.REPEATER_PROFILE:
            // api = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_DEBATOR
            // this.debtorAPI = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_DEBATOR_DETAILS
            this.previousPageRouteUrl ='customer/non-customer-profile/password-special-instructions'
            this.featureName = "Repeater Profile"
            this.featureIndex = 4
            break;
          case FEATURE_NAME.NKA_CUSTOMER:
            // api = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_DEBATOR
            // this.debtorAPI = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_DEBATOR_DETAILS
            this.previousPageRouteUrl ='customer/non-customer-profile/access-to-premises'
            this.featureName = "NKA Customer"
            this.featureIndex =7
            break;
          case FEATURE_NAME.SUSPENDED_ACCOUNT:
            // api = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_DEBATOR
            // this.debtorAPI = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_DEBATOR_DETAILS
            this.previousPageRouteUrl ='customer/non-customer-profile/suspense-account-add-edit'
            this.featureName = "Suspense Account"
            this.featureIndex =2
            this.isShowExistingSearch =false
            break;

      default:
        api = CustomerModuleApiSuffixModels.CAMERA_PROFILE_DEBTOR
        this.featureName = "Camera Profile";
        this.isShowExistingSearch = true;
        break;


    }
    return api
  }


  onSubmit() {
    if (this.debtorForm.value.debtorTypeId == 1) {
      this.debtorForm = setRequiredValidator(this.debtorForm, ["companyRegNo", "companyName"]);
      this.debtorForm = clearFormControlValidators(this.debtorForm, ["said"]);
    } else if (this.debtorForm.value.debtorTypeId == 2) {
      this.debtorForm = clearFormControlValidators(this.debtorForm, ["companyRegNo", "companyName"]);
      this.debtorForm = setRequiredValidator(this.debtorForm, ["said"]);
    }
    if (this.debtorForm.invalid) {
      return;
    }
    let obj = this.debtorForm.getRawValue()
    obj.createdUserId = this.loggedUser?.userId;
    obj.contractTypeId = null
    obj.customerId = this.customerId;
    obj.siteTypeId = this.siteTypeId?this.siteTypeId:this.debtorForm.value.debtorTypeId;


    if (typeof obj.mobile1 == 'string') {
      obj.mobile1 = obj.mobile1.split(" ").join("")
    }
    if (typeof obj.mobile2 == 'string') {
      obj.mobile2 = obj.mobile2.split(" ").join("")
    }
    if (typeof obj.premisesNo == 'string') {
      obj.premisesNo = obj.premisesNo.split(" ").join("")
    }

    if (typeof obj.officeNo == 'string') {
      obj.officeNo = obj.officeNo.split(" ").join("")
    }


    if (!obj.isBillingSameAsSiteAddress) {
      obj.isBillingSameAsSiteAddress = false;
    }
    if (!obj.isDebtorSameAsCustomer) {
      obj.isDebtorSameAsCustomer = false;
      obj.contactId = null;
    } else {
      obj.contactId = this.details.customerInformationDTO ? this.details.customerInformationDTO.contactId : null;

    }
    if (!obj.isEmailCommunication) {
      obj.isEmailCommunication = false;
    }
    if (!obj.isExistingDebtor) {
      obj.isExistingDebtor = false;
    }
    if (!obj.isPhoneCommunication) {
      obj.isPhoneCommunication = false;
    }
    if (!obj.isPostCommunication) {
      obj.isPostCommunication = false;
    }
    if (!obj.isSMSCommunication) {
      obj.isSMSCommunication = false;
    }
    if (this.debtorChange) {
      obj.debtorId = null;
      obj.debtorRefNo = null;
      obj.bdiNumber = null;
      obj.billingAddressId = null;
      obj.debtorContactId = this.details.debtorInfo ? this.details.debtorInfo.debtorContactId : null;
      obj.debtorAdditionalInfoId = this.details.debtorInfo ? this.details.debtorInfo.debtorAdditionalInfoId : null;
      obj.debtorAccountDetailId = null
      obj.contactId = this.details.customerInformationDTO ? this.details.customerInformationDTO.contactId : null;;

    } else {
      obj.debtorId = this.details?.debtorInfo ? this.details?.debtorInfo?.debtorId : null;
      obj.debtorContactId = this.details?.debtorInfo ? this.details?.debtorInfo?.debtorContactId : null;
      obj.debtorAdditionalInfoId = this.details?.debtorInfo ? this.details?.debtorInfo?.debtorAdditionalInfoId : null;
      obj.debtorAccountDetailId = this.details?.debtorInfo ? this.details?.debtorInfo?.debtorAccountDetailId : null;
    }
    if (obj.isExistingDebtor && this.dataFromLinkDEbtor) {
      obj.billingAddressId = this.dataFromLinkDEbtor.billingAddressId ? this.dataFromLinkDEbtor.billingAddressId : null;
      obj.isBillingSameAsSiteAddress = false;
      obj.contactId = this.dataFromLinkDEbtor.contactId ? this.dataFromLinkDEbtor.contactId : null;
      obj.debtorId = this.dataFromLinkDEbtor.debtorId ? this.dataFromLinkDEbtor.debtorId : null;
      obj.debtorRefNo = this.dataFromLinkDEbtor.debtorRefNo ? this.dataFromLinkDEbtor.debtorRefNo : null;
      obj.bdiNumber = this.dataFromLinkDEbtor.bdiNumber ? this.dataFromLinkDEbtor.bdiNumber : null;
      // obj.debtorContactId = this.dataFromLinkDEbtor.debtorContactId ? this.dataFromLinkDEbtor.debtorContactId : null;
      obj.debtorAccountDetailId = this.dataFromLinkDEbtor.debtorAccountDetailId ? this.dataFromLinkDEbtor.debtorAccountDetailId : null;
    }
    obj.officeNoCountryCode = this.details.basicInfo ? this.details.basicInfo.officeNoCountryCode : null;
    obj.officeNoCountryCode = obj.officeNoCountryCode ? obj.officeNoCountryCode : '+27'
    obj.mobile1CountryCode = this.details.basicInfo ? this.details.basicInfo.mobile1CountryCode : null;
    obj.mobile1CountryCode = obj.mobile1CountryCode ? obj.mobile1CountryCode : '+27'
    this.rxjsService.setFormChangeDetectionProperty(true);
    // let _apiUrl: any = this.getApiEndPoint();
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.NON_CUSTOMER_DEBTOR_INFORMATION, obj, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.router.navigate(['customer/non-customer-profile/debtor-banking-information'], { queryParams: { customerId: this.customerId, debtorTypeId: this.debtorForm.value.debtorTypeId, debtorAdditionalInfoId: response.resources,feature:this.feature } })

        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigateTo(page :number){
    if(page ==1){
      this.router.navigate([this.previousPageRouteUrl] ,{queryParams:{id : this.customerId,customerId:this.customerId,customerTypeId:this.siteTypeId,feature:this.feature}})
    }
    else{
      if(!this.details?.debtorInfo) return '';
    this.router.navigate(['customer/non-customer-profile/debtor-banking-information'] ,{queryParams:{customerId : this.customerId, debtorTypeId: this.siteTypeId, debtorAdditionalInfoId:this.details.debtorInfo.debtorAdditionalInfoId,feature:this.feature}})
  }
}

goBack(){
  this.router.navigate(['customer/non-customer-profile'], { queryParams: { tab: this.tabIndex } })
}





}
