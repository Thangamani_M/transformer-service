import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel,
  ConfirmDialogModel,
  ConfirmDialogPopupComponent,
  CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, formConfigs, generateCurrentYearToNext99Years, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix,
  monthsByNumber,
  prepareGetRequestHttpParams, removeAllFormValidators, RxjsService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { loggedInUserData, selectStaticEagerLoadingCardTypesState$, selectStaticEagerLoadingPaymentMethodsState$, selectStaticEagerLoadingTitlesState$ } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import {
  LeadHeaderData,
  SalesModuleApiSuffixModels, selectLeadCreationStepperParamsState$,
  selectLeadHeaderDataState$, ServiceAgreementStepsParams
} from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { FEATURE_NAME } from '../../shared/enum/index.enum';
import { DebtorBankingInfoModel } from '../../shared/model/debtor-info.model';
@Component({
  selector: 'debtor-banking-information',
  templateUrl: './debtor-banking-information.component.html',
  // styleUrls: ['./service-installation-agreement.scss']
})

export class DebtorBankingInformationComponent implements OnInit {
  serviceAgreementStepsParams: ServiceAgreementStepsParams;
  leadHeaderData: LeadHeaderData;
  leadStatusCssClass: any;
  currentAgreementTabObj;
  isExistingDebtor = false;
  createNewBankAccount = false;
  details: any;
  bankSelected = true;
  monthArray = monthsByNumber;
  yearArray = generateCurrentYearToNext99Years();
  titleList = [];
  bankList = [];
  branchList = [];
  accountTypeList = [];
  cardTypeList = [];
  paymentTypeList = [];
  selectedBranch: any;
  debterBankingDetailsForm: FormGroup;
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  isAccountNumber = new CustomDirectiveConfig({ isAccountNumber: true });
  isANumberWithZero = new CustomDirectiveConfig({ isANumberWithZero: true });
  accountNumberAfterMask: any;
  IsInstallationDebtor: boolean = false;
  breadCrumb: BreadCrumbModel;
  loggedUser: UserLogin
  customerId: string;
  debtorTypeId: string;
  debtorAdditionalInfoId: string;
  feature: FEATURE_NAME
  isShowAccount = true;
  pageIndex;
  isDisablePaymentTypeId = false;
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;
  selectedBranchOption;
  constructor(private crudService: CrudService, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder,
    public rxjsService: RxjsService, private snackbarService: SnackbarService, private dialog: MatDialog, private router: Router, private activatedRoute: ActivatedRoute) {

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParams.subscribe(param => {
      this.customerId = param.customerId
      this.debtorTypeId = param.debtorTypeId
      this.debtorAdditionalInfoId = param.debtorAdditionalInfoId
      this.feature = param.feature
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadCreationStepperParamsState$),
      this.store.select(selectLeadHeaderDataState$), this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(selectStaticEagerLoadingCardTypesState$),
      this.store.select(selectStaticEagerLoadingPaymentMethodsState$)]
    ).subscribe((response) => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(response[0]);
      this.leadHeaderData = response[1];
      this.titleList = response[2];
      this.cardTypeList = response[3];
      this.paymentTypeList = response[4];


    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.getBankList();
    this.getAccountTypeList();
    this.createDebterForm();
    if (this.debtorAdditionalInfoId) {
      this.getDebtorBankingDetailsById();
    }
    this.onFormControlChanges();
    if (this.debterBankingDetailsForm.value.isBankAccount) {
      this.debterBankingDetailsForm.get('isCardSelected').setValue(false);
      this.debterBankingDetailsForm.get('isBankAccount').setValue(true);
      this.debterBankingDetailsForm.get('cardTypeId').disable();
      this.debterBankingDetailsForm.get('cardNo').disable();
      this.debterBankingDetailsForm.get('selectedMonth').disable();
      this.debterBankingDetailsForm.get('selectedYear').disable();
      this.debterBankingDetailsForm.get('cardTypeId').clearValidators();
      this.debterBankingDetailsForm.get('selectedMonth').clearValidators();
      this.debterBankingDetailsForm.get('selectedYear').clearValidators();
      this.debterBankingDetailsForm.get('cardTypeId').updateValueAndValidity();
      this.debterBankingDetailsForm.get('cardNo').updateValueAndValidity();
      this.debterBankingDetailsForm.get('selectedMonth').updateValueAndValidity();
      this.debterBankingDetailsForm.get('selectedYear').updateValueAndValidity();
      this.debterBankingDetailsForm.get('accountNo').enable();
      this.debterBankingDetailsForm.get('bankId').enable();
      this.debterBankingDetailsForm.get('bankBranchId').enable();
      this.debterBankingDetailsForm.get('bankBranchCode').enable();
      this.debterBankingDetailsForm.get('accountTypeId').enable();
      this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm,
        ["accountNo", "bankId", "bankBranchId", "bankBranchCode", "accountTypeId"]);
    }
    if (this.serviceAgreementStepsParams.debtorTypeId == '1') {
      this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm,
        ["companyName"]);
    }
    else {
      this.debterBankingDetailsForm.get('companyName').patchValue('');
      this.debterBankingDetailsForm.get('companyName').clearValidators();
      this.debterBankingDetailsForm.get('companyName').updateValueAndValidity();
    }
    this.getApiEndPoint();
  }

  accountNumberBeforeMask: string;

  createDebterForm() {
    let debtorBankingDetailsModel = new DebtorBankingInfoModel();
    this.debterBankingDetailsForm = this.formBuilder.group({});
    Object.keys(debtorBankingDetailsModel).forEach((key) => {
      this.debterBankingDetailsForm.addControl(key,
        new FormControl(debtorBankingDetailsModel[key]));
    });
    this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm, ["paymentMethodId", "titleId", "firstName", "lastName", "accountNo", "bankId", "bankBranchId", "accountTypeId", "cardTypeId", "cardNo", "selectedMonth", "selectedYear"]);
    this.debterBankingDetailsForm.get('cardNo').setValidators(Validators.compose([
      Validators.minLength(formConfigs.creditCardNumberWithSpaceMask),
      Validators.maxLength(formConfigs.creditCardNumberWithSpaceMask)
    ]));
    this.debterBankingDetailsForm.get('cardNo').updateValueAndValidity();
  }




  getDebtorBankingDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.NON_CUSTOMER_DEBTOR_BANKING_DETAIL, undefined, false, prepareGetRequestHttpParams(null, null, {
      debtorAdditionalInfoId: this.debtorAdditionalInfoId,
      customerId: this.customerId,

    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.details = response.resources;
        // this.details.systemOwnershipId = true;
        // this.details.isDebtorAccountVetted = false;
        // this.details.isDebtorVetted = true;

        this.serviceAgreementStepsParams.debtorId = this.details.debtorId;
        this.serviceAgreementStepsParams.isDebtorAccountVetted = this.details.isDebtorAccountVetted;
        this.serviceAgreementStepsParams.isDebtorVetted = this.details.isDebtorVetted;




        if (this.details && this.details.debtorAccountDetailId && this.details.debtorAccountDetailId) {
          // let expiryDate = this.details.expiryDate.split("/");
          if (this.details.isExistingDebtor) {
            this.isExistingDebtor = true;
          }
          if (this.details.expiryDate) {
            let expiryDate = this.details.expiryDate.split("/");
            this.details.selectedMonth = expiryDate[0];
            this.details.selectedYear = expiryDate[1];
          }
          this.debterBankingDetailsForm.get('debtorAdditionalInfoId').setValue(this.details.debtorAdditionalInfoId);
        }
        if (this.details.paymentMethodId == 0) {
          this.details.paymentMethodId = "";
        }
        if (this.feature == FEATURE_NAME.ADT_PROFILE || this.feature == FEATURE_NAME.NKA_CUSTOMER) {
          this.details.paymentMethodId = this.details.paymentMethodId == 0 ? 2 : this.details.paymentMethodId;
        }
        if (this.feature == FEATURE_NAME.SUSPENDED_ACCOUNT) {
          this.details.paymentMethodId = 1;
          this.isDisablePaymentTypeId = true;
          this.onChangePaymentType()

        }
        this.debterBankingDetailsForm.patchValue(this.details);
        this.onChangePaymentType()

        if (this.details && this.details.isBankAccount) {
          this.selectedBranchOption = { displayName: response.resources?.bankBranchName, id: response.resources?.bankBranchId };
          this.debterBankingDetailsForm.get('bankBranchId').setValue(response.resources?.bankBranchName, { emitEvent: false, onlySelf: true });

          this.bankSelected = true;
          this.debterBankingDetailsForm.get('isCardSelected').setValue(false);
          this.debterBankingDetailsForm.get('isBankAccount').setValue(true);
          this.debterBankingDetailsForm.get('cardTypeId').disable();
          this.debterBankingDetailsForm.get('cardNo').disable();
          this.debterBankingDetailsForm.get('selectedMonth').disable();
          this.debterBankingDetailsForm.get('selectedYear').disable();
          this.debterBankingDetailsForm.get('cardTypeId').clearValidators();
          this.debterBankingDetailsForm.get('selectedMonth').clearValidators();
          this.debterBankingDetailsForm.get('selectedYear').clearValidators();
          this.debterBankingDetailsForm.get('cardTypeId').updateValueAndValidity();
          this.debterBankingDetailsForm.get('cardNo').updateValueAndValidity();
          this.debterBankingDetailsForm.get('selectedMonth').updateValueAndValidity();
          this.debterBankingDetailsForm.get('selectedYear').updateValueAndValidity();
          this.debterBankingDetailsForm.get('accountNo').enable();
          this.debterBankingDetailsForm.get('bankId').enable();
          this.debterBankingDetailsForm.get('bankBranchId').enable();
          this.debterBankingDetailsForm.get('bankBranchCode').enable();
          this.debterBankingDetailsForm.get('accountTypeId').enable();
          this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm,
            ["accountNo", "bankId", "bankBranchId", "bankBranchCode", "accountTypeId"]);

        } else {
          this.bankSelected = false;
          this.debterBankingDetailsForm.get('isBankAccount').setValue(false);
          this.debterBankingDetailsForm.get('isCardSelected').setValue(true);
          this.debterBankingDetailsForm.get('accountNo').disable();
          this.debterBankingDetailsForm.get('bankId').disable();
          this.debterBankingDetailsForm.get('bankBranchId').disable();
          this.debterBankingDetailsForm.get('bankBranchCode').disable();
          this.debterBankingDetailsForm.get('accountTypeId').disable();
          this.debterBankingDetailsForm.get('accountNo').clearValidators();
          this.debterBankingDetailsForm.get('bankId').clearValidators();
          this.debterBankingDetailsForm.get('bankBranchId').clearValidators();
          this.debterBankingDetailsForm.get('bankBranchCode').clearValidators();
          this.debterBankingDetailsForm.get('accountTypeId').clearValidators();
          this.debterBankingDetailsForm.get('accountNo').updateValueAndValidity();
          this.debterBankingDetailsForm.get('bankId').updateValueAndValidity();
          this.debterBankingDetailsForm.get('bankBranchId').updateValueAndValidity();
          this.debterBankingDetailsForm.get('bankBranchCode').updateValueAndValidity();
          this.debterBankingDetailsForm.get('accountTypeId').updateValueAndValidity();
          this.debterBankingDetailsForm.get('cardTypeId').enable();
          this.debterBankingDetailsForm.get('cardNo').enable();
          this.debterBankingDetailsForm.get('selectedMonth').enable();
          this.debterBankingDetailsForm.get('selectedYear').enable();
          this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm,
            ["cardTypeId", "cardNo", "selectedMonth", "selectedYear"]);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getBankList() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.SALES_API_BANKS, undefined, false, prepareGetRequestHttpParams(null, null, {
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.bankList = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getBranchesByBankIdAndBranchKeyword(): void {
    this.debterBankingDetailsForm.get('bankBranchId')
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((searchtext) => {
          if (!searchtext) {
            this.getLoopableObjectRequestObservable = of();
          }
          else {
            this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_BANK_BRANCHES, undefined, false,
              prepareGetRequestHttpParams(null, null, { bankId: this.debterBankingDetailsForm.get('bankId').value, searchText: searchtext }));
          }
        });
  }
  onSelectedBranchOption(selectedObject): void {
    if (!selectedObject) return;
    this.selectedBranchOption = selectedObject;
    this.debterBankingDetailsForm.get('bankBranchCode').setValue(this.selectedBranchOption?.bankBranchCode)
  }
  getAccountTypeList() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_ACCOUNTTYPE
      , undefined, false, prepareGetRequestHttpParams(null, null, {
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.accountTypeList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onFormControlChanges(): void {
    this.getBranchesByBankIdAndBranchKeyword()
    this.debterBankingDetailsForm.get('bankId').valueChanges.subscribe((bankId: string) => {
      if (bankId) {
        this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_BANK_BRANCHES, undefined, false, prepareGetRequestHttpParams(null, null, {
          bankId: bankId
        })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.branchList = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
      }
    });

    this.debterBankingDetailsForm.get('isBankAccount').valueChanges.subscribe((isBankAccount: boolean) => {
      if (isBankAccount) {
      }
    });

    this.debterBankingDetailsForm.get('isCardSelected').valueChanges.subscribe((isCardSelected: boolean) => {
      if (isCardSelected) {
        this.debterBankingDetailsForm.get('cardNo').setValidators(Validators.compose([
          Validators.minLength(formConfigs.creditCardNumberWithSpaceMask),
          Validators.maxLength(formConfigs.creditCardNumberWithSpaceMask)
        ]));
      }
    });
  }

  onBankSelected() {
    this.debterBankingDetailsForm.get('bankBranchId').patchValue(null);
    this.debterBankingDetailsForm.get('bankBranchCode').patchValue('');
  }

  onOptionsSelected(e) {
    this.selectedBranch = this.branchList.filter(element => {
      return element.id == e.target.value;
    })
    this.debterBankingDetailsForm.get('bankBranchCode').setValue(this.selectedBranch[0].bankBranchCode);
  }

  isSelectedCard(e, selected) {
    if (e.value && selected === 'selectedCard') {
      this.bankSelected = false;
      this.debterBankingDetailsForm.get('isBankAccount').setValue(false);
      this.debterBankingDetailsForm.get('isCardSelected').setValue(true);
      this.debterBankingDetailsForm.get('accountNo').patchValue('');
      this.debterBankingDetailsForm.get('bankId').patchValue(null);
      this.debterBankingDetailsForm.get('bankBranchId').patchValue(null);
      this.debterBankingDetailsForm.get('bankBranchCode').patchValue(null);
      this.debterBankingDetailsForm.get('accountTypeId').patchValue(null);
      this.debterBankingDetailsForm.get('accountNo').disable();
      this.debterBankingDetailsForm.get('bankId').disable();
      this.debterBankingDetailsForm.get('bankBranchId').disable();
      this.debterBankingDetailsForm.get('bankBranchCode').disable();
      this.debterBankingDetailsForm.get('accountTypeId').disable();
      this.debterBankingDetailsForm.get('accountNo').clearValidators();
      this.debterBankingDetailsForm.get('bankId').clearValidators();
      this.debterBankingDetailsForm.get('bankBranchId').clearValidators();
      this.debterBankingDetailsForm.get('bankBranchCode').clearValidators();
      this.debterBankingDetailsForm.get('accountTypeId').clearValidators();
      this.debterBankingDetailsForm.get('accountNo').updateValueAndValidity();
      this.debterBankingDetailsForm.get('bankId').updateValueAndValidity();
      this.debterBankingDetailsForm.get('bankBranchId').updateValueAndValidity();
      this.debterBankingDetailsForm.get('bankBranchCode').updateValueAndValidity();
      this.debterBankingDetailsForm.get('accountTypeId').updateValueAndValidity();
      this.debterBankingDetailsForm.get('cardTypeId').enable();
      this.debterBankingDetailsForm.get('cardNo').enable();
      this.debterBankingDetailsForm.get('selectedMonth').enable();
      this.debterBankingDetailsForm.get('selectedYear').enable();
      this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm,
        ["cardTypeId", "cardNo", "selectedMonth", "selectedYear"]);
    }

    if (e.value && selected === 'selectedBank') {
      this.bankSelected = true;
      this.debterBankingDetailsForm.get('isCardSelected').setValue(false);
      this.debterBankingDetailsForm.get('isBankAccount').setValue(true);
      this.debterBankingDetailsForm.get('cardTypeId').patchValue(null);
      this.debterBankingDetailsForm.get('cardNo').patchValue('');
      this.debterBankingDetailsForm.get('selectedMonth').patchValue('');
      this.debterBankingDetailsForm.get('selectedYear').patchValue('');
      this.debterBankingDetailsForm.get('cardTypeId').disable();
      this.debterBankingDetailsForm.get('cardNo').disable();
      this.debterBankingDetailsForm.get('selectedMonth').disable();
      this.debterBankingDetailsForm.get('selectedYear').disable();
      this.debterBankingDetailsForm.get('cardTypeId').clearValidators();
      this.debterBankingDetailsForm.get('cardNo').clearValidators();
      this.debterBankingDetailsForm.get('selectedMonth').clearValidators();
      this.debterBankingDetailsForm.get('selectedYear').clearValidators();
      this.debterBankingDetailsForm.get('cardTypeId').updateValueAndValidity();
      this.debterBankingDetailsForm.get('cardNo').updateValueAndValidity();
      this.debterBankingDetailsForm.get('selectedMonth').updateValueAndValidity();
      this.debterBankingDetailsForm.get('selectedYear').updateValueAndValidity();
      this.debterBankingDetailsForm.get('accountNo').enable();
      this.debterBankingDetailsForm.get('bankId').enable();
      this.debterBankingDetailsForm.get('bankBranchId').enable();
      this.debterBankingDetailsForm.get('bankBranchCode').enable();
      this.debterBankingDetailsForm.get('accountTypeId').enable();
      this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm,
        ["accountNo", "bankId", "bankBranchId", "bankBranchCode", "accountTypeId"]);
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  onChangePaymentType() {
    if (this.debterBankingDetailsForm.get('paymentMethodId').value == 1) {
      this.isShowAccount = false
      this.debterBankingDetailsForm = removeAllFormValidators(this.debterBankingDetailsForm);
      this.debterBankingDetailsForm.updateValueAndValidity();

    } else {
      this.isShowAccount = true;
      this.debterBankingDetailsForm = setRequiredValidator(this.debterBankingDetailsForm, ["titleId", "firstName", "lastName", "accountNo", "bankId", "bankBranchId", "accountTypeId", "cardTypeId", "cardNo", "selectedMonth", "selectedYear"]);
      this.debterBankingDetailsForm.get('cardNo').setValidators(Validators.compose([
        Validators.minLength(formConfigs.creditCardNumberWithSpaceMask),
        Validators.maxLength(formConfigs.creditCardNumberWithSpaceMask)
      ]));
      this.debterBankingDetailsForm.get('cardNo').updateValueAndValidity();
    }
  }

  addNewBankAccount() {
    const message = `Are you sure you want to add new bank account?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) {
        this.isExistingDebtor = true;
        return;
      }
      this.debterBankingDetailsForm.reset();
      this.isExistingDebtor = false;
      this.createNewBankAccount = true;
      this.debterBankingDetailsForm.get('isBankAccount').setValue(true);
      this.bankSelected = true;
    });
  }
  featureName = "Camera Profile";
  tabIndex = 0
  getApiEndPoint(): string {
    let api;
    switch (this.feature) {

      case FEATURE_NAME.CAMERA_PROFILE:
        api = CustomerModuleApiSuffixModels.CAMERA_PROFILE_DEBTOR_BANKING
        this.featureName = "Camera Profile"
        this.tabIndex = 0
        break;
      case FEATURE_NAME.NON_CLIENT:
        api = CustomerModuleApiSuffixModels.NON_CLIENT_PROFILE_DEBTOR_BANKING
        this.featureName = "Non-Client"
        this.tabIndex = 1;
        this.pageIndex = 3;
        break;

      case FEATURE_NAME.HUB_PROFILE:
        api = CustomerModuleApiSuffixModels.HUB_PROFILE_DEBTOR_BANKING
        this.featureName = "Hub Profile"
        this.tabIndex = 2
        break;

      case FEATURE_NAME.REPEATER_PROFILE:
        this.featureName = "Repeater Profile"
        this.pageIndex = 5
        this.tabIndex = 5
        break;


      case FEATURE_NAME.EXECU_GUARD_PROFILE:
        api = CustomerModuleApiSuffixModels.EXECU_GUARD_DEBTOR_BANKING
        this.featureName = "ExecuGuard Profile"
        this.tabIndex = 5
        this.pageIndex = 4;
        break;
      case FEATURE_NAME.NKA_CUSTOMER:
        api = CustomerModuleApiSuffixModels.NON_CUSTOMER_DEBTOR_BANKING
        this.featureName = "NKA Customer"
        this.tabIndex = 10
        this.pageIndex = 8;

        break;
      case FEATURE_NAME.ADT_PROFILE:
        api = CustomerModuleApiSuffixModels.NON_CUSTOMER_DEBTOR_BANKING
        this.featureName = "ADT Site Profile"
        this.tabIndex = 9
        this.pageIndex = 8;

        break;
      case FEATURE_NAME.CASUAL_GUARDING:
        api = CustomerModuleApiSuffixModels.NON_CUSTOMER_DEBTOR_BANKING
        this.featureName = "Casual Guarding Profile"
        this.tabIndex = 8
        this.pageIndex = 3;
        break;
      case FEATURE_NAME.SUSPENDED_ACCOUNT:
        api = CustomerModuleApiSuffixModels.NON_CUSTOMER_DEBTOR_BANKING
        this.featureName = "Suspense Account"
        this.tabIndex = 11
        this.pageIndex = 3;
        break;

      default:
        api = CustomerModuleApiSuffixModels.CAMERA_PROFILE_DEBTOR_BANKING
        break;
    }
    return api
  }
  onSubmit() {
    if (this.feature == FEATURE_NAME.SUSPENDED_ACCOUNT) {
      this.debterBankingDetailsForm = removeAllFormValidators(this.debterBankingDetailsForm);
      this.debterBankingDetailsForm.updateValueAndValidity();
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.debterBankingDetailsForm.invalid) {
      return;
    }
    if (!this.debtorAdditionalInfoId) {
      this.snackbarService.openSnackbar("Debtor ID is not present", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.debterBankingDetailsForm.value.isExistingDebtor && this.createNewBankAccount) {
      this.debterBankingDetailsForm.get('debtorAccountDetailId').setValue(null);
    }

    this.debterBankingDetailsForm.value.cardNo = this.debterBankingDetailsForm.value.cardNo && this.debterBankingDetailsForm.value.cardNo.replace(/\s/g, '');
    this.debterBankingDetailsForm.value.createdUserId = this.loggedUser?.userId;
    this.debterBankingDetailsForm.value.customerId = this.customerId;
    this.debterBankingDetailsForm.value.debtorTypeId = this.debtorTypeId;
    this.debterBankingDetailsForm.value.debtorAdditionalInfoId = this.debtorAdditionalInfoId;
    if (this.debterBankingDetailsForm.value.isCardSelected) {
      this.debterBankingDetailsForm.value.expiryDate = `${this.debterBankingDetailsForm.value.selectedMonth}/${this.debterBankingDetailsForm.value.selectedYear}`
    }
    if (this.debterBankingDetailsForm.get('isBankAccount').value) {
      this.debterBankingDetailsForm.value.bankBranchId = this.selectedBranchOption.id;
    }

    let obj = this.debterBankingDetailsForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let _apiEndPoint: any = this.getApiEndPoint();
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.NON_CUSTOMER_DEBTOR_BANKING, obj).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goBack()
      }
    })
  }

  goBack() {
    this.router.navigate(['customer/non-customer-profile'], { queryParams: { tab: this.tabIndex } })
  }

  navigateToDebtorInformation() {
    this.router.navigate(['customer/non-customer-profile/debtor-information'], { queryParams: { customerId: this.customerId, customerTypeId: this.debtorTypeId, feature: this.feature } })

  }
}
