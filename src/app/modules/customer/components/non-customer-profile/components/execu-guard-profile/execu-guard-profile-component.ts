import { Component, OnInit, ViewChild } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { clearFormControlValidators, CrudService, HttpCancelService, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { CustomerModuleApiSuffixModels } from "@modules/customer";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { AddressModel } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { SharedAddressInformationComponent } from "../../shared/components/address-information/address-information.component";
import { SharedCustomerInformationComponent } from "../../shared/components/customer-information/customer-information.component";
import { SharedProfileInformationComponent } from "../../shared/components/profile-information/profile-information.component";
import { FEATURE_NAME, FORMTYPE } from "../../shared/enum/index.enum";
import { ContactPersonModel, CustomerInformationModel, ProfileManagerModel } from "../../shared/model";

@Component({
    selector: 'app-execu-guard-profile',
    templateUrl: './execu-guard-profile-component.html',
    styleUrls: ['./execu-guard-profile-component.scss'],
  })
  
  export class ExecuGuardProfileAddEditComponent implements OnInit { 
   
    @ViewChild(SharedCustomerInformationComponent,{static:false}) customerInformationComponent: SharedCustomerInformationComponent;
    @ViewChild(SharedProfileInformationComponent,{static:false}) profileInformationComponent: SharedProfileInformationComponent;
    @ViewChild(SharedAddressInformationComponent,{static:false}) SharedAddressInformationComponent: SharedAddressInformationComponent;

    execuGuardProfileForm: FormGroup;
    loggedUser: UserLogin;
    feature : FEATURE_NAME;
    customerId:string;
    cameraProfileObject :any
    // formArray
    contactPersonArray: FormArray;
    profileManagerArray: FormArray;
    cdmArray: FormArray;
    cameraInformationArray: FormArray;

    constructor(
      private router : Router,
      private activatedRoute : ActivatedRoute,
      private crudService: CrudService,
      private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
      private store: Store<AppState>, private rxjsService: RxjsService) {
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.loggedUser = userData;
      });
     this.feature =  this.activatedRoute.snapshot.queryParams.feature;
     this.customerId =  this.activatedRoute.snapshot.queryParams.id;
    }

    ngOnInit(): void {
         this.createExecuGuardProfileForm();
        if(this.customerId){
          this.getExecuGuardData()
        }
      }
      createExecuGuardProfileForm(): void {
        this.execuGuardProfileForm = this.formBuilder.group({
          createdUserId : [this.loggedUser?.userId]
        });
    
        this.execuGuardProfileForm.addControl('customerInformationDTO', this.createCustomerInformationModel());
        this.execuGuardProfileForm.addControl('addressInfo', this.createAddressInformationModel());
    
        this.execuGuardProfileForm.controls["customerInformationDTO"] = setRequiredValidator(
          this.execuGuardProfileForm.controls["customerInformationDTO"] as FormGroup,
          ["customerTypeId", "titleId", "firstName", "lastName", "email", "mobile1"]
        );
    
        this.execuGuardProfileForm.controls["addressInfo"] = setRequiredValidator(
          this.execuGuardProfileForm.controls["addressInfo"] as FormGroup,
          ["formatedAddress", "streetNo"]
        );
    
        this.contactPersonArray = this.getContactPersontArray
        this.profileManagerArray = this.getProfileManagerArray
        if(!this.customerId){
          this.contactPersonArray.push(this.createProfileContactPersonFormArray());
          this.profileManagerArray.push(this.createProfileManagerFormArray());
        }
    
      }

      createCustomerInformationModel(basicInfo?: CustomerInformationModel): FormGroup {
        let customerInfo = new CustomerInformationModel(basicInfo);
        let formControls = {};
        Object.keys(customerInfo).forEach((key) => {
          formControls[key] = new FormControl(customerInfo[key]);
        });
        return this.formBuilder.group(formControls);
      }
      createAddressInformationModel(addressModel?: AddressModel): FormGroup {
        let _addressInfo = new AddressModel(addressModel);
        let formControls = {};
        Object.keys(_addressInfo).forEach((key) => {
          formControls[key] = new FormControl(_addressInfo[key]);
        });
        return this.formBuilder.group(formControls);
      }
      get getContactPersontArray(): FormArray {
        if (!this.execuGuardProfileForm) return;
        return this.execuGuardProfileForm.get("nonCustomerContactPersonDTOs") as FormArray;
      }
      get getProfileManagerArray(): FormArray {
        if (!this.execuGuardProfileForm) return;
        return this.execuGuardProfileForm.get("nonCustomerProfileManagerDTOs") as FormArray;
      }
    
      createProfileContactPersonFormArray(contactPerson?: ContactPersonModel): FormGroup {
        let _contactPerson = new ContactPersonModel(contactPerson);
        let formControls = {};
        Object.keys(_contactPerson).forEach((key) => {
          if (key == "contactPersion" || key == "contactNumber" || key == 'contactNumberCountryCode') {
            formControls[key] = new FormControl( _contactPerson[key], [Validators.required]);
          } else {
            formControls[key] = new FormControl(_contactPerson[key]);
          }
        });
        return this.formBuilder.group(formControls);
      }
      createProfileManagerFormArray(profileManager?: ProfileManagerModel): FormGroup {
        let _profileManager = new ProfileManagerModel(profileManager);
        let formControls = {};
        Object.keys(_profileManager).forEach((key) => {
          if (key == "roleId" || key == "employeeId") {
            formControls[key] = new FormControl(_profileManager[key], [Validators.required]);
          } else {
            formControls[key] = new FormControl(_profileManager[key]);
          }
        });
        return this.formBuilder.group(formControls);
      }
    
      emitCustomerType(data) {
        if (data == true) {
          this.execuGuardProfileForm.controls["customerInformationDTO"] = setRequiredValidator(
            this.execuGuardProfileForm.controls["customerInformationDTO"] as FormGroup,
            ["companyName"]
          );
    
        } else {
          this.execuGuardProfileForm.controls["customerInformationDTO"] = clearFormControlValidators(
            this.execuGuardProfileForm.controls["customerInformationDTO"] as FormGroup,
            ["companyName"]
          );
        }
      }
    
      emitCustomerInformation(event) {
        switch (event.type) {
          case FORMTYPE.CUSTOMER_INFORMATION:
            this.execuGuardProfileForm.get('customerInformationDTO').setValue(event.data);
            break;
          case FORMTYPE.ADDRESS_INFORMATION:
            this.execuGuardProfileForm.get('addressInfo').setValue(event.data);
    
            break;
          default:
            break;
        }
      }
      afrigisAddressInfo  ={};
      emitAfrigisAddressInfo(event){
        this.afrigisAddressInfo = {
          buildingName: event?.buildingName,
          buildingNo: event?.buildingNo,
          streetNo: event?.streetNo,
        };
      }
    
      emitChangeFormCheckbox (event){
        if(event.type == "addressExtra"){
          if(event.action =="add"){
            this.execuGuardProfileForm.controls["addressInfo"].get('estateStreetNo').setValidators([Validators.required]);
            this.execuGuardProfileForm.controls["addressInfo"].get('estateStreetNo').updateValueAndValidity();
            this.execuGuardProfileForm.controls["addressInfo"].get('estateStreetName').setValidators([Validators.required]);
            this.execuGuardProfileForm.controls["addressInfo"].get('estateStreetName').updateValueAndValidity();
            this.execuGuardProfileForm.controls["addressInfo"].get('estateName').setValidators([Validators.required]);
            this.execuGuardProfileForm.controls["addressInfo"].get('estateName').updateValueAndValidity();
          }else{
            this.execuGuardProfileForm.controls["addressInfo"].get('estateStreetNo').clearValidators();
            this.execuGuardProfileForm.controls["addressInfo"].get('estateStreetNo').updateValueAndValidity();
            this.execuGuardProfileForm.controls["addressInfo"].get('estateStreetName').clearValidators();
            this.execuGuardProfileForm.controls["addressInfo"].get('estateStreetName').updateValueAndValidity();
            this.execuGuardProfileForm.controls["addressInfo"].get('estateName').clearValidators();
            this.execuGuardProfileForm.controls["addressInfo"].get('estateName').updateValueAndValidity();
          }
        }
        if(event.type =="complex"){
    
          if(event.action  =='add'){
            this.execuGuardProfileForm.controls["addressInfo"].get('buildingNo').setValidators([Validators.required]);
            this.execuGuardProfileForm.controls["addressInfo"].get('buildingNo').updateValueAndValidity();
            this.execuGuardProfileForm.controls["addressInfo"].get('buildingName').setValidators([Validators.required]);
            this.execuGuardProfileForm.controls["addressInfo"].get('buildingName').updateValueAndValidity();
          }else{
            this.execuGuardProfileForm.controls["addressInfo"].get('buildingNo').clearValidators();
            this.execuGuardProfileForm.controls["addressInfo"].get('buildingNo').updateValueAndValidity();
            this.execuGuardProfileForm.controls["addressInfo"].get('buildingName').clearValidators();
            this.execuGuardProfileForm.controls["addressInfo"].get('buildingName').updateValueAndValidity();
          }
        }
    
      }
      getExecuGuardData (){
        this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_DETAILS,undefined, false,prepareRequiredHttpParams({CustomerId :this.customerId})).subscribe(response=>{
          if(response.isSuccess && response.statusCode === 200){
            delete response.resources.nonCustomerCDMDTOs;  
            this.execuGuardProfileForm.patchValue(response.resources);
            this.cameraProfileObject = response.resources;
            // customer Information Component    
            this.customerInformationComponent.customerInformatioanForm.patchValue(response.resources.customerInformationDTO);
           
            this.SharedAddressInformationComponent.createAddresForm()
            response.resources.addressInfo.latLong =
            response.resources.addressInfo.latitude +
            "," +
            response.resources.addressInfo.longitude;

          this.SharedAddressInformationComponent.addressForm.patchValue(response.resources.addressInfo,{emitEvent:false, onlySelf:true});
          this.SharedAddressInformationComponent.addressForm.get("formatedAddress").setValue(response.resources.addressInfo.formatedAddress)
          this.SharedAddressInformationComponent.onFormControlChanges()
         
    
          }
        })
      }
    
      navigateToDebtorInformation(){
        if(!this.cameraProfileObject) return ;
          this.router.navigate(['customer/non-customer-profile/business-classification'] ,{queryParams:{customerId : this.customerId, customerTypeId :  this.cameraProfileObject.customerInformationDTO.siteTypeId, feature : this.feature}});
      }
      onSubmit(): void {
        if(this.execuGuardProfileForm.touched){
          this.rxjsService.setFormChangeDetectionProperty(true);
        }
        this.execuGuardProfileForm
          .get("createdUserId")
          .setValue(this.loggedUser?.userId);
        if (this.execuGuardProfileForm.invalid) return;
    
        this.execuGuardProfileForm.value.customerInformationDTO.mobile1 =
          this.execuGuardProfileForm.value.customerInformationDTO.mobile1
            .toString()
            .replace(/\s/g, "");
        if (this.execuGuardProfileForm.value.customerInformationDTO.mobile2) {
          this.execuGuardProfileForm.value.customerInformationDTO.mobile2 =
            this.execuGuardProfileForm.value.customerInformationDTO.mobile2
              .toString()
              .replace(/\s/g, "");
        }
        if (this.execuGuardProfileForm.value.customerInformationDTO.officeNo) {
          this.execuGuardProfileForm.value.customerInformationDTO.officeNo =
            this.execuGuardProfileForm.value.customerInformationDTO.officeNo
              .toString()
              .replace(/\s/g, "");
        }
        if (this.execuGuardProfileForm.value.customerInformationDTO.premisesNo) {
          this.execuGuardProfileForm.value.customerInformationDTO.premisesNo =
            this.execuGuardProfileForm.value.customerInformationDTO.premisesNo
              .toString()
              .replace(/\s/g, "");
        }
        this.execuGuardProfileForm.value.customerInformationDTO.customerId = this.customerId;

          this.execuGuardProfileForm.value.customerInformationDTO.siteTypeId = this.execuGuardProfileForm.value.customerInformationDTO.siteTypeId ? Number(this.execuGuardProfileForm.value.customerInformationDTO.siteTypeId) : 0;
       
          this.crudService
          .create(
            ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_CUSTOMER,
            this.execuGuardProfileForm.value
          )
          .subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200 && response.resources) {
              this.customerId = this.customerId ? this.customerId : response.resources;
              this.router.navigate(["customer/non-customer-profile/business-classification"],{queryParams: {feature:'execu-guard-profile',customerId : this.customerId,customerTypeId:this.execuGuardProfileForm.value.customerInformationDTO.siteTypeId}});
            }
          });
      }
    
  }