import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren
} from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { loggedInUserData } from "@app/modules";
import { AppState } from "@app/reducers";
import {
  clearFormControlValidators,
  CrudService,
  CustomDirectiveConfig,
  formConfigs,
  HttpCancelService,
  ModulesBasedApiSuffix,
  prepareRequiredHttpParams,
  RxjsService,
  setRequiredValidator
} from "@app/shared";
import { CustomerModuleApiSuffixModels } from "@modules/customer/shared";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { SharedCustomerInformationComponent } from "../../shared/components/customer-information/customer-information.component";
import { SharedProfileInformationComponent } from "../../shared/components/profile-information/profile-information.component";
import { FEATURE_NAME, FORMTYPE } from "../../shared/enum/index.enum";
import {
  CameraInformationModel,
  CDMModel,
  ContactPersonModel,
  CustomerInformationModel,
  ProfileInformationModel,
  ProfileManagerModel
} from "../../shared/model";
@Component({
  selector: "app-camera-profile-add-edit",
  templateUrl: "./camera-profile-add-edit.component.html",
  styleUrls: ["./camera-profile-add-edit.component.scss"],
})
export class CameraProfileAddEditComponent implements OnInit {
  @ViewChild(SharedCustomerInformationComponent, { static: false })
  customerInformationComponent: SharedCustomerInformationComponent;
  @ViewChild(SharedProfileInformationComponent, { static: false })
  profileInformationComponent: SharedProfileInformationComponent;

  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({
    isAStringOnly: true,
    shouldPasteKeyboardEventBeRestricted: true,
  });
  cameraProfileForm: FormGroup;
  cameraProfileObject: any;
  loggedUser: UserLogin;
  @Output() outputData = new EventEmitter<any>();
  @ViewChildren("input") rows: QueryList<any>;
  // profile information
  tabs = ["Community Name", "Contact Person", "Profile Managers", "CDM"];

  // formArray
  contactPersonArray: FormArray;
  profileManagerArray: FormArray;
  cdmArray: FormArray;
  cameraInformationArray: FormArray;
  cameraCustomerId;
  feature: FEATURE_NAME;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private rxjsService: RxjsService
  ) {
    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.loggedUser = userData;
      });
    this.cameraCustomerId = this.activatedRoute.snapshot.queryParams.id;
    this.feature = this.activatedRoute.snapshot.queryParams.feature;
  }

  ngOnInit(): void {
    this.createcameraProfileForm();
    if (this.cameraCustomerId) {
      this.getCameraCustomerData();
    }
  }

  createcameraProfileForm(): void {
    // let cameraProfileFormModel = new CameraProfileModel();
    this.cameraProfileForm = this.formBuilder.group({
      createdUserId: [this.loggedUser?.userId],
    });

    this.cameraProfileForm.addControl(
      "customerInformationDTO",
      this.createCustomerInformationModel()
    );
    this.cameraProfileForm.addControl(
      "profileInformationDTO",
      this.createProfileInformationModel()
    );
    this.cameraProfileForm.addControl(
      "nonCustomerContactPersonDTOs",
      this.formBuilder.array([])
    );
    this.cameraProfileForm.addControl(
      "nonCustomerProfileManagerDTOs",
      this.formBuilder.array([])
    );
    this.cameraProfileForm.addControl(
      "nonCustomerCDMDTOs",
      this.formBuilder.array([])
    );
    this.cameraProfileForm.addControl(
      "cameraInformationDTO",
      this.formBuilder.array([])
    );

    this.contactPersonArray = this.getContactPersontArray;
    this.profileManagerArray = this.getProfileManagerArray;
    this.cdmArray = this.getCdmArray;
    this.cameraInformationArray = this.getCameraInformationArray;
    if(!this.cameraCustomerId){
      this.contactPersonArray.push(this.createProfileContactPersonFormArray());
      this.profileManagerArray.push(this.createProfileManagerFormArray());
      this.cdmArray.push(this.createCDMFormArray());
      this.cameraInformationArray.push(this.createCameraInformationFormArray());
    }

    this.cameraProfileForm.controls["customerInformationDTO"] =
      setRequiredValidator(
        this.cameraProfileForm.controls["customerInformationDTO"] as FormGroup,
        [
          "customerTypeId",
          "titleId",
          "firstName",
          "lastName",
          "email",
          "mobile1",
          "addressId"
        ]
      );

    this.cameraProfileForm.controls["profileInformationDTO"] =
      setRequiredValidator(
        this.cameraProfileForm.controls["profileInformationDTO"] as FormGroup,
        [
          "communityName",
          "divisionId",
          "mainAreaId",
          "subAreaId",
          "streetName",
          "suburbId",
        ]
      );
  }

  get getContactPersontArray(): FormArray {
    if (!this.cameraProfileForm) return;
    return this.cameraProfileForm.get(
      "nonCustomerContactPersonDTOs"
    ) as FormArray;
  }

  get getProfileManagerArray(): FormArray {
    if (!this.cameraProfileForm) return;
    return this.cameraProfileForm.get(
      "nonCustomerProfileManagerDTOs"
    ) as FormArray;
  }

  get getCdmArray(): FormArray {
    if (!this.cameraProfileForm) return;
    return this.cameraProfileForm.get("nonCustomerCDMDTOs") as FormArray;
  }

  get getCameraInformationArray(): FormArray {
    if (!this.cameraProfileForm) return;
    return this.cameraProfileForm.get("cameraInformationDTO") as FormArray;
  }

  emitCustomerType(data) {
    if (data == true) {
      this.cameraProfileForm.controls["customerInformationDTO"] =
        setRequiredValidator(
          this.cameraProfileForm.controls[
            "customerInformationDTO"
          ] as FormGroup,
          ["companyName"]
        );
    } else {
      this.cameraProfileForm.controls["customerInformationDTO"] =
        clearFormControlValidators(
          this.cameraProfileForm.controls[
            "customerInformationDTO"
          ] as FormGroup,
          ["companyName"]
        );
    }
  }

  emitCustomerInformation(event) {
    switch (event.type) {
      case FORMTYPE.CUSTOMER_INFORMATION:
        this.cameraProfileForm
          .get("customerInformationDTO")
          .setValue(event.data);
        break;
      case FORMTYPE.PROFILE_COMMUNITY:
        if(event.data?.suburbId && typeof event.data?.suburbId  == "object" ){
          event.data.suburbId = event.data.suburbId?.id
        }

        this.cameraProfileForm
          .get("profileInformationDTO")
          .setValue(event.data);
        break;
      case FORMTYPE.PROFILE_MANAGER:

        let pManager = this.getProfileManagerArray.controls[0].get("roleId").value;
      if (!pManager) {
        this.getProfileManagerArray.controls[0].patchValue(event.data);
      } else {
        this.getProfileManagerArray.push(
          this.createProfileManagerFormArray(event.data)
        );
      }
      this.cameraProfileForm.markAllAsTouched();
        break;

      case FORMTYPE.PROFILE_CONTACT:
        if(this.getContactPersontArray.length!=0){
        let contactPerson =
          this.getContactPersontArray.controls[0].get("contactPersion").value;
        if (!contactPerson) {
          this.getContactPersontArray.controls[0].patchValue(event.data);
        } else {
          this.getContactPersontArray.push(
            this.createProfileContactPersonFormArray(event.data)
          );
        }
      }else{
        this.getContactPersontArray.push(
          this.createProfileContactPersonFormArray(event.data)
        );
        }
        this.cameraProfileForm.markAllAsTouched();
        break;

      case FORMTYPE.PROFILE_CDM:
        let cdmManager = this.getCdmArray.controls[0].get("roleId").value;
        if (!cdmManager) {
          this.getCdmArray.controls[0].patchValue(event.data);
        } else {
          this.getCdmArray.push(
            this.createCDMFormArray(event.data)
          );
        }
        this.cameraProfileForm.markAllAsTouched();
        break;
      default:
        break;
    }
  }

  emitRemoveElementByIndex(event) {
    switch (event.type) {
      case FORMTYPE.PROFILE_MANAGER:
        this.getProfileManagerArray.removeAt(event.index);
        this.cameraProfileForm.markAllAsTouched();
        break;
      case FORMTYPE.PROFILE_CDM:
        this.getCdmArray.removeAt(event.index);
        this.cameraProfileForm.markAllAsTouched();
        break;
      case FORMTYPE.PROFILE_CONTACT:
        this.contactPersonArray.removeAt(event.index);
        this.cameraProfileForm.markAllAsTouched();
        break;
    }
    this.contactPersonArray.markAllAsTouched();
  }
  createCustomerInformationModel(
    basicInfo?: CustomerInformationModel
  ): FormGroup {
    let customerInfo = new CustomerInformationModel(basicInfo);
    let formControls = {};
    Object.keys(customerInfo).forEach((key) => {
      formControls[key] = new FormControl(customerInfo[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createProfileInformationModel(
    profileInfo?: ProfileInformationModel
  ): FormGroup {
    let _profileInfo = new ProfileInformationModel(profileInfo);
    let formControls = {};
    Object.keys(_profileInfo).forEach((key) => {
      formControls[key] = new FormControl(_profileInfo[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createProfileContactPersonFormArray(
    contactPerson?: ContactPersonModel
  ): FormGroup {
    let _contactPerson = new ContactPersonModel(contactPerson);
    let formControls = {};
    Object.keys(_contactPerson).forEach((key) => {
      if (
        key == "contactPersion" ||
        key == "contactNumber" ||
        key == "contactNumberCountryCode"
      ) {
        formControls[key] = new FormControl(_contactPerson[key], [
          Validators.required,
        ]);
      } else {
        formControls[key] = new FormControl(_contactPerson[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  createProfileManagerFormArray(
    profileManager?: ProfileManagerModel
  ): FormGroup {
    let _profileManager = new ProfileManagerModel(profileManager);
    let formControls = {};
    Object.keys(_profileManager).forEach((key) => {
      if (key == "roleId" || key == "employeeId") {
        formControls[key] = new FormControl(_profileManager[key], [
          Validators.required,
        ]);
      } else {
        formControls[key] = new FormControl(_profileManager[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  createCDMFormArray(cdmModel?: CDMModel): FormGroup {
    let _cdmModel = new CDMModel(cdmModel);
    let formControls = {};
    Object.keys(_cdmModel).forEach((key) => {
      if (key == "roleId" || key == "employeeId") {
        formControls[key] = new FormControl(_cdmModel[key], [
          Validators.required,
        ]);
      } else {
        formControls[key] = new FormControl(_cdmModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  createCameraInformationFormArray(
    cameraInformationModel?: CameraInformationModel
  ): FormGroup {
    let _cameraInformationModel = new CameraInformationModel(
      cameraInformationModel
    );
    let formControls = {};
    Object.keys(_cameraInformationModel).forEach((key) => {
      if (key == "cameraProfileName" || key == "location") {
        formControls[key] = new FormControl(_cameraInformationModel[key], [
          Validators.required,
        ]);
      } else if (key == "latLong") {
        formControls[key] = new FormControl(_cameraInformationModel[key], [
          Validators.required,
          Validators.pattern(
            /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/
          ),
        ]);
      } else {
        formControls[key] = new FormControl(_cameraInformationModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  addCamera() {
    if (this.getCameraInformationArray.invalid) {
      this.ocusInAndOutFormArrayFields();
      return;
    }

    let cameraInformationModel = new CameraInformationModel();
    this.cameraInformationArray.insert(
      0,
      this.createCameraInformationFormArray(cameraInformationModel)
    );

  }

  removeCamera(index) {
    this.getCameraInformationArray.removeAt(index);
  }

  onSubmit(): void {

    this.rxjsService.setFormChangeDetectionProperty(true);
    this.cameraProfileForm
      .get("createdUserId")
      .setValue(this.loggedUser?.userId);
    if (this.cameraProfileForm.invalid) return;

    this.cameraProfileForm.value.customerInformationDTO.mobile1 =
      this.cameraProfileForm.value.customerInformationDTO.mobile1
        .toString()
        .replace(/\s/g, "");
    if (this.cameraProfileForm.value.customerInformationDTO.mobile2) {
      this.cameraProfileForm.value.customerInformationDTO.mobile2 =
        this.cameraProfileForm.value.customerInformationDTO.mobile2
          .toString()
          .replace(/\s/g, "");
    }
    if (this.cameraProfileForm.value.customerInformationDTO.officeNo) {
      this.cameraProfileForm.value.customerInformationDTO.officeNo =
        this.cameraProfileForm.value.customerInformationDTO.officeNo
          .toString()
          .replace(/\s/g, "");
    }
    if (this.cameraProfileForm.value.customerInformationDTO.premisesNo) {
      this.cameraProfileForm.value.customerInformationDTO.premisesNo =
        this.cameraProfileForm.value.customerInformationDTO.premisesNo
          .toString()
          .replace(/\s/g, "");
    }
    this.crudService
      .create(
        ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.CAMERA_PROFILE,
        this.cameraProfileForm.value
      )
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.router.navigate(["customer/non-customer-profile"],{ queryParams: {tab:0}});
        }
      });
  }

  ocusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    });
  }

  getCameraCustomerData() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.CAMERA_PROFILE_DETAIL,
        undefined,
        false,
        prepareRequiredHttpParams({ CustomerId: this.cameraCustomerId })
      )
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          {
            this.cameraProfileForm.patchValue(response.resources);
            for (let index = 0; index < (response.resources.nonCustomerContactPersonDTOs.length); index++) {
              this.contactPersonArray.push(this.createProfileContactPersonFormArray(response.resources.nonCustomerContactPersonDTOs[index]));
            }
            for (let index = 0; index < (response.resources.nonCustomerProfileManagerDTOs.length); index++) {
              this.profileManagerArray.push(this.createProfileManagerFormArray(response.resources.nonCustomerProfileManagerDTOs[index]));
            }
            for (let index = 0; index < (response.resources.nonCustomerCDMDTOs.length); index++) {
              this.cdmArray.push(this.createCDMFormArray(response.resources.nonCustomerCDMDTOs[index]));
            }
            for (let index = 0; index < (response.resources.cameraInformationDTO.length); index++) {
              this.cameraInformationArray.push(this.createCameraInformationFormArray(response.resources.cameraInformationDTO[index]));
            }

            this.cameraProfileObject = response.resources;
            this.customerInformationComponent.customerInformatioanForm.patchValue(
              response.resources.customerInformationDTO
            );
            this.customerInformationComponent.emitDataWhenFormChange();
            //
            this.profileInformationComponent.profileInformationForm.patchValue(
              response.resources.profileInformationDTO
            );
            this.profileInformationComponent.onChangeMainArea();
            this.profileInformationComponent.setSuburbs();
            this.profileInformationComponent.contactList =
              response.resources.nonCustomerContactPersonDTOs;
            this.profileInformationComponent.profileManagerList =
              response.resources.nonCustomerProfileManagerDTOs;
            this.profileInformationComponent.cdmList =
              response.resources.nonCustomerCDMDTOs;
          }
        }
      });
  }

  navigateToDebtorInformation() {
    if (!this.cameraProfileObject) return;

    this.router.navigate(["customer/non-customer-profile/debtor-information"], {
      queryParams: {
        customerId: this.cameraCustomerId,
        customerTypeId:
          this.cameraProfileObject.customerInformationDTO.customerTypeId,
        feature: this.feature,
      },
    });
  }
  goBack(){
    this.router.navigateByUrl('customer/non-customer-profile?tab=0')
  }
}
