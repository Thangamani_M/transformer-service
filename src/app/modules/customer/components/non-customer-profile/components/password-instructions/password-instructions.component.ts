import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerModuleApiSuffixModels, CustomerVerificationType, SalesModuleApiSuffixModels, selectStaticEagerLoadingDogTypesState$ } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  ConfirmDialogModel, ConfirmDialogPopupComponent,
  CustomDirectiveConfig, ResponseMessageTypes, SnackbarService
} from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import {
  formConfigs, IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator
} from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { DocTypes, LeadHeaderData, MedicalConditions } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { FEATURE_NAME } from '../../shared/enum/index.enum';
import { PasswordsInstructionsModel } from '../../shared/model/keyholder.model';
@Component({
  selector: 'app-password-instructions',
  templateUrl: './password-instructions.component.html'
})

export class PasswordInstructionsComponent implements OnInit {
  @Input() inputObject = { fromComponentType: 'lead', customerId: "", addressId: "", partitionId: "",quotationVersionId:"" };
  params
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  passwordsForm: FormGroup;
  responseLength = 0;
  leadHeaderData: LeadHeaderData;
  dogTypes: FormArray;
  medicalConditions: FormArray;
  @ViewChildren('input') rows: QueryList<any>;
  dogTypesList = [];
  details;
  show: boolean = false;
  showPlus: boolean = false;
  loggedInUserData: LoggedInUserModel;
  isPasswordShow = false;
  isPasswordShowForDistressWord=false;
  customerId
  feature
  isShowNext = false
  isShowNextButton = false
  currentIndex = 3;
  backwardUrl = ""
  uniqueCallId;
  addressId;
  constructor(private crudService: CrudService, private snackbarService: SnackbarService, private dialog: MatDialog, private router: Router,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    public rxjsService: RxjsService, private store: Store<AppState>
  ) {
    this.activatedRoute.queryParams.subscribe(param => {
      this.customerId = param.customerId;
      this.feature = param.feature;
      this.addressId = param.addressId;

    })
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingDogTypesState$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.dogTypesList = response[1];
    });
  }

  ngOnInit(): void {
    this.createPasswordForm();
    this.getPasswordDetails()
    this.onFormControlChanges();
    this.decideNavigations()
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  showNoOfDogs: boolean = false;
  onFormControlChanges(): void {
    this.passwordsForm.get('isDogs').valueChanges.subscribe((isDogOnSite: boolean) => {
      if (!isDogOnSite) {
        if (this.details.customerSpecialInstructionId) {
          if (this.dogTypes && this.dogTypes.length > 0) {
            this.snackbarService.openSnackbar("Please delete existing dogs", ResponseMessageTypes.WARNING);
            setTimeout(() => {
              this.passwordsForm.get('isDogs').setValue(true)
            }, 10)
          }
        }
      }
    });
    this.passwordsForm.get('noOfDogs').valueChanges.subscribe((noOfDogs: boolean) => {
      if (noOfDogs) {
        this.showPlus = true;
      }
      else {
        this.showPlus = false;
        this.show = false;
      }
    });
  }

  getPasswordDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
       CustomerModuleApiSuffixModels.REPEATER_PROFILE_SPECIAL_INSTRUCTIONS_DETAIL, null, false,
       prepareRequiredHttpParams({ customerId: this.customerId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.details = response.resources
        if (this.details.customerId) {
          if(this.details.customerSpecialInstructionId){
            this.isShowNextButton = true;
            this.isShowNext =true;
          }
          this.passwordsForm.patchValue(response.resources);
          this.passwordsForm.get('isDogs').setValue(this.details.isDogs);
          this.dogTypes = this.getdogTypesListArray;
          this.medicalConditions = this.getMedicalConditionListArray;
          if (response.resources.dogTypes.length > 0) {
            this.show = true;
            response.resources.dogTypes.forEach((dg) => {
              this.dogTypes.push(this.createDogTypesListModel(dg));
            });
          };
          // if (this.details.contractId) {
          //   this.passwordsForm.get('isDogs').disable();
          // }
          this.medicalConditions = this.getMedicalConditionListArray;
          while (this.getMedicalConditionListArray.length) {
            this.getMedicalConditionListArray.removeAt(this.getMedicalConditionListArray.length - 1);
          }
          if (response.resources.medicalConditions.length > 0) {
            response.resources.medicalConditions.forEach((md) => {
              this.medicalConditions.push(this.createMedicalConditionListModel(md));
            });
          }
          else {
            this.medicalConditions.push(this.createMedicalConditionListModel());
          }
        } else {
          this.medicalConditions = this.getMedicalConditionListArray;
          this.medicalConditions.push(this.createMedicalConditionListModel());
        }
      }
    })
  }


  createPasswordForm(): void {
    let passwordsAddEditModel = new PasswordsInstructionsModel();
    this.passwordsForm = this.formBuilder.group({
      dogTypes: this.formBuilder.array([]),
      medicalConditions: this.formBuilder.array([])
    });
    Object.keys(passwordsAddEditModel).forEach((key) => {
      this.passwordsForm.addControl(key, new FormControl(passwordsAddEditModel[key]));
    });
    this.passwordsForm = setRequiredValidator(this.passwordsForm, ["alarmCancellationPassword"]);
    this.addMedicalItem()
  }

  get getdogTypesListArray(): FormArray {
    if (!this.passwordsForm) return;
    return this.passwordsForm.get("dogTypes") as FormArray;
  }

  //Create FormArray controls
  createDogTypesListModel(docTypes?: DocTypes): FormGroup {
    let docTypesFormControlModel = new DocTypes(docTypes);
    let formControls = {};
    Object.keys(docTypesFormControlModel).forEach((key) => {
      if (key == 'dogTypeId') {
        formControls[key] = [{ value: docTypesFormControlModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: docTypesFormControlModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  get getMedicalConditionListArray(): FormArray {
    if (!this.passwordsForm) return;
    return this.passwordsForm.get("medicalConditions") as FormArray;
  }

  //Create FormArray controls
  createMedicalConditionListModel(medicalConditions?: MedicalConditions): FormGroup {
    let medicalConditionsFormControlModel = new MedicalConditions(medicalConditions);
    let formControls = {};
    Object.keys(medicalConditionsFormControlModel).forEach((key) => {
      formControls[key] = [{ value: medicalConditionsFormControlModel[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  addItem(event): void {
    this.dogTypes = this.getdogTypesListArray;
    let docTypesFormControlModel = new DocTypes();

    // if (this.passwordsForm.value.customerSpecialInstructionId) {
    //   this.dogTypes.insert(0, this.createDogTypesListModel(docTypesFormControlModel));
    // } else {
    //   this.dogTypes.clear();
    //   for (var i = 0; i < this.passwordsForm.value.noOfDogs; i++) {
    //     this.dogTypes.insert(i, this.createDogTypesListModel(docTypesFormControlModel));
    //   }
    // }
    // this.passwordsForm.get('noOfDogs').setValue(this.dogTypes.length);


    let dogs = parseInt(event.target.value);
    if (event.target.value) {
      if (this.details && this.details?.isDogs && dogs < this.details?.dogTypes.length) {
        this.snackbarService.openSnackbar("Please delete existing dogs", ResponseMessageTypes.WARNING);
        this.passwordsForm.get('noOfDogs').setValue(this.details?.dogTypes.lengthh);

        return;
      }
      else if (this.details && this.details?.isDogs && dogs >= this.details?.dogTypes.length) {
        this.dogTypes.clear();
        this.details.dogTypes.forEach((dg) => {
          this.dogTypes.push(this.createDogTypesListModel(dg));
        });
        for (var i = this.details?.dogTypes.length; i < dogs; i++) {
          this.dogTypes.insert(i, this.createDogTypesListModel(docTypesFormControlModel));
        }

      } else {
        this.dogTypes.clear();
        for (var j = 0; j < this.passwordsForm.value.noOfDogs; j++) {
          this.dogTypes.insert(j, this.createDogTypesListModel(docTypesFormControlModel));
        }
      }
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  addMedicalItem(): void {
    this.medicalConditions = this.getMedicalConditionListArray;
    this.medicalConditions.insert(0, this.createMedicalConditionListModel());
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  deleteDogType(i: number) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getdogTypesListArray.controls[i].value.dogTypeDetailId && this.getdogTypesListArray.length > 0) {
        let resultObservable = this.inputObject.fromComponentType == 'lead' ?
          this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_DELETE_DOG_TYPES,
            undefined,
            prepareRequiredHttpParams({
              Ids: this.getdogTypesListArray.controls[i].value.dogTypeDetailId,
              IsDeleted: false,
            }), 1) :
          this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.DOG_TYPES,
            undefined,
            prepareRequiredHttpParams({
              Ids: this.getdogTypesListArray.controls[i].value.dogTypeDetailId,
              IsDeleted: false,
            }), 1);

        resultObservable.subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getdogTypesListArray.removeAt(i);
            this.details.dogTypes.splice(i, 1);
          }
          this.passwordsForm.get('noOfDogs').setValue(this.getdogTypesListArray.length);
        });
      }
      else {
        this.getdogTypesListArray.removeAt(i);
        this.passwordsForm.get('noOfDogs').setValue(this.getdogTypesListArray.length);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  deleteMedical(i: number) {
    if (this.getMedicalConditionListArray.controls[i].value.medicalDetailId == '') {
      this.getMedicalConditionListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getMedicalConditionListArray.controls[i].value.medicalDetailId && this.getMedicalConditionListArray.length > 1) {
        let resultObservable = this.inputObject.fromComponentType == 'lead' ?
          this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_DELETE_MEDICAL,
            undefined,
            prepareRequiredHttpParams({
              Ids: this.getMedicalConditionListArray.controls[i].value.medicalDetailId,
              IsDeleted: false,
            }), 1) :
          this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.MEDICAL_CONDITIONS,
            undefined,
            prepareRequiredHttpParams({
              Ids: this.getMedicalConditionListArray.controls[i].value.medicalDetailId,
              IsDeleted: false,
            }), 1);

        resultObservable.subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getMedicalConditionListArray.removeAt(i);
          }
          if (this.getMedicalConditionListArray.length === 0) {
            this.addMedicalItem();
          };
        });
      }
      else {
        this.getdogTypesListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onSubmit(): void {
    let postObj = this.passwordsForm.getRawValue();
    if (!postObj.isDogs) {
      if (this.dogTypes) {
        this.dogTypes.clear();
      }
    }
    if (this.passwordsForm.invalid) {
      return;
    }

    postObj.customerId = this.customerId

    let data = postObj.dogTypes;
    let detailId;
    if (data.length > 0) {
      data.forEach(element => {
        detailId = element.dogDetailId
      });
    }
    if (detailId) {
      if (data.length > 0) {
        data.forEach(element => {
          element.dogDetailId = detailId;
        });
      }
    }
    postObj.dogTypes = data;
    if (!postObj.isDogs) {
      postObj.dogTypes = null;
      postObj.noOfDogs = null;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let resultObservable = this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.REPEATER_PROFILE_SPECIAL_INSTRUCTIONS, postObj, 1)
    resultObservable.subscribe(resp => {
      if (resp.resources && resp.statusCode == 200 && resp.isSuccess) {
        this.onArrowClick('next')

      }
    });
  }

  onEditBtnClicked() {
    if(this.activatedRoute?.snapshot?.queryParams?.feature_name) {
      this.focusInAndOutFormArrayFields();
    } else {
    this.rxjsService.setGlobalLoaderProperty(true);
    let filterObj = { userId: this.loggedInUserData?.userId, customerId: this.inputObject?.customerId, siteAddressId: this.inputObject?.addressId, uniqueCallId: this.uniqueCallId ? this.uniqueCallId : null };
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CHECK, null, false,
      prepareRequiredHttpParams(filterObj))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.focusInAndOutFormArrayFields()
        } else {
          this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: this.inputObject?.customerId, siteAddressId: this.inputObject?.addressId, tab: CustomerVerificationType.SPECIAL_INSTRUCTIONS, customerTab: 7, monitoringTab: 0, } })
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  onArrowClick(type: string) {
    if (type == "previous") {
      this.router.navigate(['customer/non-customer-profile/key-holder-information'], { queryParams: { customerId: this.customerId, feature: this.feature ,addressId: this.addressId} })
    } else {
      if(this.feature == FEATURE_NAME.REPEATER_PROFILE){
        this.router.navigate(['customer/non-customer-profile/debtor-information'], { queryParams: { customerId: this.customerId, feature: this.feature ,addressId: this.addressId} })
      }else{
      this.router.navigate(['customer/non-customer-profile/arming-disarming-time'], { queryParams: { customerId: this.customerId, feature: this.feature ,addressId: this.addressId} })
      }
    }
  }

  decideNavigations() {
    if (this.feature == FEATURE_NAME.REPEATER_PROFILE) {
      this.currentIndex = 3
    } else if (this.feature == FEATURE_NAME.ADT_PROFILE) {
      this.isShowNext = true
      this.currentIndex = 4
    }else if(this.feature  ==FEATURE_NAME.NKA_CUSTOMER){
      this.isShowNext = true
      this.currentIndex = 4
    }
  }

  goBack() {
    this.router.navigate(['customer/non-customer-profile'])
  }
}
