import { Component, EventEmitter, OnInit, Output, QueryList, ViewChild, ViewChildren } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { CustomerModuleApiSuffixModels } from "@modules/customer";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { SharedCustomerInformationComponent } from "../../shared/components/customer-information/customer-information.component";
import { SharedProfileInformationComponent } from "../../shared/components/profile-information/profile-information.component";
import { FEATURE_NAME, FORMTYPE } from "../../shared/enum/index.enum";
import { CDMModel, ContactPersonModel, CustomerInformationModel, ProfileInformationModel, ProfileManagerModel } from "../../shared/model";

@Component({
    selector: "app-community-profile-add-edit",
    templateUrl: "./community-profile-add-edit.component.html"
})
export class CommunityProfileAddEditComponent implements OnInit {
    @ViewChild(SharedCustomerInformationComponent, { static: false })
    customerInformationComponent: SharedCustomerInformationComponent;
    @ViewChild(SharedProfileInformationComponent, { static: false })
    profileInformationComponent: SharedProfileInformationComponent;

    formConfigs = formConfigs;
    stringConfig = new CustomDirectiveConfig({
        isAStringOnly: true,
        shouldPasteKeyboardEventBeRestricted: true,
    });
    communityProfileForm: FormGroup;
    cameraProfileObject: any;
    loggedUser: UserLogin;
    @Output() outputData = new EventEmitter<any>();
    @ViewChildren("input") rows: QueryList<any>;
    // profile information
    tabs = ["Community Name", "Contact Person", "Profile Managers", "CDM"];

    // formArray
    contactPersonArray: FormArray;
    profileManagerArray: FormArray;
    cdmArray: FormArray;
    cameraInformationArray: FormArray;
    cameraCustomerId;
    feature: FEATURE_NAME;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private crudService: CrudService,
        private formBuilder: FormBuilder,
        private httpCancelService: HttpCancelService,
        private store: Store<AppState>,
        private rxjsService: RxjsService
    ) {
        this.store
            .pipe(select(loggedInUserData))
            .subscribe((userData: UserLogin) => {
                if (!userData) return;
                this.loggedUser = userData;
            });
        // this.cameraCustomerId = this.activatedRoute.snapshot.queryParams.id;
        this.feature = this.activatedRoute.snapshot.queryParams.feature;
    }

    ngOnInit(): void {
        this.createCommunityappProfileForm();
    }
    createCommunityappProfileForm(): void {
        this.communityProfileForm = this.formBuilder.group({
            createdUserId: [this.loggedUser?.userId],
        });

        this.communityProfileForm.addControl(
            "customerInformationDTO",
            this.createCustomerInformationModel()
        );
        this.communityProfileForm.addControl(
            "profileInformationDTO",
            this.createProfileInformationModel()
        );
        this.communityProfileForm.addControl(
            "nonCustomerContactPersonDTOs",
            this.formBuilder.array([])
        );
        this.communityProfileForm.addControl(
            "nonCustomerProfileManagerDTOs",
            this.formBuilder.array([])
        );
        this.communityProfileForm.addControl(
            "nonCustomerCDMDTOs",
            this.formBuilder.array([])
        );


        this.contactPersonArray = this.getContactPersontArray;
        this.profileManagerArray = this.getProfileManagerArray;
        this.cdmArray = this.getCdmArray;

        if (!this.cameraCustomerId) {
            this.contactPersonArray.push(this.createProfileContactPersonFormArray());
            this.profileManagerArray.push(this.createProfileManagerFormArray());
            this.cdmArray.push(this.createCDMFormArray());

        }

        this.communityProfileForm.controls["customerInformationDTO"] =
            setRequiredValidator(
                this.communityProfileForm.controls["customerInformationDTO"] as FormGroup,
                [
                    "customerTypeId",
                    "titleId",
                    "firstName",
                    "lastName",
                    "email",
                    "mobile1",
                    "addressId"
                ]
            );

        this.communityProfileForm.controls["profileInformationDTO"] =
            setRequiredValidator(
                this.communityProfileForm.controls["profileInformationDTO"] as FormGroup,
                [
                    "communityName",
                    "divisionId",
                    "mainAreaId",
                    "subAreaId",
                    "streetName",
                    "suburbId",
                ]
            );
    }

    get getContactPersontArray(): FormArray {
        if (!this.communityProfileForm) return;
        return this.communityProfileForm.get(
            "nonCustomerContactPersonDTOs"
        ) as FormArray;
    }
    get getProfileManagerArray(): FormArray {
        if (!this.communityProfileForm) return;
        return this.communityProfileForm.get(
            "nonCustomerProfileManagerDTOs"
        ) as FormArray;
    }
    get getCdmArray(): FormArray {
        if (!this.communityProfileForm) return;
        return this.communityProfileForm.get("nonCustomerCDMDTOs") as FormArray;
    }
    emitCustomerType(data) {
        if (data == true) {
            this.communityProfileForm.controls["customerInformationDTO"] =
                setRequiredValidator(
                    this.communityProfileForm.controls[
                    "customerInformationDTO"
                    ] as FormGroup,
                    ["companyName"]
                );
        } else {
            this.communityProfileForm.controls["customerInformationDTO"] =
                clearFormControlValidators(
                    this.communityProfileForm.controls[
                    "customerInformationDTO"
                    ] as FormGroup,
                    ["companyName"]
                );
        }
    }

    emitCustomerInformation(event) {
        switch (event.type) {
            case FORMTYPE.CUSTOMER_INFORMATION:
                this.communityProfileForm
                    .get("customerInformationDTO")
                    .setValue(event.data);
                break;
            case FORMTYPE.PROFILE_COMMUNITY:
              if(event.data?.suburbId && typeof event.data?.suburbId  == "object" ){
                event.data.suburbId = event.data.suburbId?.id
              }
                this.communityProfileForm
                    .get("profileInformationDTO")
                    .setValue(event.data);
                break;
            case FORMTYPE.PROFILE_MANAGER:

                let pManager = this.getProfileManagerArray.controls[0].get("roleId").value;
                if (!pManager) {
                    this.getProfileManagerArray.controls[0].patchValue(event.data);
                } else {
                    this.getProfileManagerArray.push(
                        this.createProfileManagerFormArray(event.data)
                    );
                }
                this.communityProfileForm.markAllAsTouched();
                break;

            case FORMTYPE.PROFILE_CONTACT:
              if(this.getContactPersontArray.length!=0){
                let contactPerson =
                    this.getContactPersontArray.controls[0].get("contactPersion").value;
                if (!contactPerson) {
                    this.getContactPersontArray.controls[0].patchValue(event.data);
                } else {
                    this.getContactPersontArray.push(
                        this.createProfileContactPersonFormArray(event.data)
                    );
                }
              }else{
                this.getContactPersontArray.push(
                  this.createProfileContactPersonFormArray(event.data)
              );
              }
                this.communityProfileForm.markAllAsTouched();
                break;

            case FORMTYPE.PROFILE_CDM:
                let cdmManager = this.getCdmArray.controls[0].get("roleId").value;
                if (!cdmManager) {
                    this.getCdmArray.controls[0].patchValue(event.data);
                } else {
                    this.getCdmArray.push(
                        this.createCDMFormArray(event.data)
                    );
                }
                this.communityProfileForm.markAllAsTouched();
                break;
            default:
                break;
        }
    }

    emitRemoveElementByIndex(event) {
        switch (event.type) {
            case FORMTYPE.PROFILE_MANAGER:
                this.getProfileManagerArray.removeAt(event.index);
                this.communityProfileForm.markAllAsTouched();
                break;
            case FORMTYPE.PROFILE_CDM:
                this.getCdmArray.removeAt(event.index);
                this.communityProfileForm.markAllAsTouched();
                break;
            case FORMTYPE.PROFILE_CONTACT:
                this.contactPersonArray.removeAt(event.index);
                this.communityProfileForm.markAllAsTouched();
                break;
        }
        this.contactPersonArray.markAllAsTouched();
    }
    createCustomerInformationModel(
        basicInfo?: CustomerInformationModel
    ): FormGroup {
        let customerInfo = new CustomerInformationModel(basicInfo);
        let formControls = {};
        Object.keys(customerInfo).forEach((key) => {
            formControls[key] = new FormControl(customerInfo[key]);
        });
        return this.formBuilder.group(formControls);
    }

    createProfileInformationModel(
        profileInfo?: ProfileInformationModel
    ): FormGroup {
        let _profileInfo = new ProfileInformationModel(profileInfo);
        let formControls = {};
        Object.keys(_profileInfo).forEach((key) => {
            formControls[key] = new FormControl(_profileInfo[key]);
        });
        return this.formBuilder.group(formControls);
    }

    createProfileContactPersonFormArray(
        contactPerson?: ContactPersonModel
    ): FormGroup {
        let _contactPerson = new ContactPersonModel(contactPerson);
        let formControls = {};
        Object.keys(_contactPerson).forEach((key) => {
            if (
                key == "contactPersion" ||
                key == "contactNumber" ||
                key == "contactNumberCountryCode"
            ) {
                formControls[key] = new FormControl(_contactPerson[key], [
                    Validators.required,
                ]);
            } else {
                formControls[key] = new FormControl(_contactPerson[key]);
            }
        });
        return this.formBuilder.group(formControls);
    }

    createProfileManagerFormArray(
        profileManager?: ProfileManagerModel
    ): FormGroup {
        let _profileManager = new ProfileManagerModel(profileManager);
        let formControls = {};
        Object.keys(_profileManager).forEach((key) => {
            if (key == "roleId" || key == "employeeId") {
                formControls[key] = new FormControl(_profileManager[key], [
                    Validators.required,
                ]);
            } else {
                formControls[key] = new FormControl(_profileManager[key]);
            }
        });
        return this.formBuilder.group(formControls);
    }

    createCDMFormArray(cdmModel?: CDMModel): FormGroup {
        let _cdmModel = new CDMModel(cdmModel);
        let formControls = {};
        Object.keys(_cdmModel).forEach((key) => {
            if (key == "roleId" || key == "employeeId") {
                formControls[key] = new FormControl(_cdmModel[key], [
                    Validators.required,
                ]);
            } else {
                formControls[key] = new FormControl(_cdmModel[key]);
            }
        });
        return this.formBuilder.group(formControls);
    }
    onSubmit(): void {
      this.rxjsService.setFormChangeDetectionProperty(true);
        this.communityProfileForm
          .get("createdUserId")
          .setValue(this.loggedUser?.userId);
        if (this.communityProfileForm.invalid) return;

        this.communityProfileForm.value.customerInformationDTO.mobile1 =
          this.communityProfileForm.value.customerInformationDTO.mobile1
            .toString()
            .replace(/\s/g, "");
        if (this.communityProfileForm.value.customerInformationDTO.mobile2) {
          this.communityProfileForm.value.customerInformationDTO.mobile2 =
            this.communityProfileForm.value.customerInformationDTO.mobile2
              .toString()
              .replace(/\s/g, "");
        }
        if (this.communityProfileForm.value.customerInformationDTO.officeNo) {
          this.communityProfileForm.value.customerInformationDTO.officeNo =
            this.communityProfileForm.value.customerInformationDTO.officeNo
              .toString()
              .replace(/\s/g, "");
        }
        if (this.communityProfileForm.value.customerInformationDTO.premisesNo) {
          this.communityProfileForm.value.customerInformationDTO.premisesNo =
            this.communityProfileForm.value.customerInformationDTO.premisesNo
              .toString()
              .replace(/\s/g, "");
        }
        this.communityProfileForm.value.customerInformationDTO.siteTypeId = 0;
        this.crudService
          .create(
            ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            CustomerModuleApiSuffixModels.COMMUNITY_PROFILE,
            this.communityProfileForm.value
          )
          .subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.router.navigate(["customer/non-customer-profile"],{queryParams: {tab:7}});
            }
          });
      }
    goBack(){
        this.router.navigateByUrl('customer/non-customer-profile?tab=7')
      }
}
