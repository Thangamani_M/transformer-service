import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from "@app/shared";
import { CustomerModuleApiSuffixModels } from "@modules/customer";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { SharedBusinessClassificationComponent } from "../../shared/components/business-classification-form/business-classification-form-component";
import { FEATURE_NAME, FORMTYPE } from "../../shared/enum/index.enum";
import { BusinessClassificationModel } from "../../shared/model/business-classification.model";


@Component({
    selector: 'app-non-customer-business-classification',
    templateUrl: './non-customer-business-classification.component.html',
    // styleUrls: ['./business-classification-component.scss'],
  })

  export class NonCustomerBusinessClassificationComponent implements OnInit {
    @ViewChild(SharedBusinessClassificationComponent, { static: false })
    businessClassificationComponent: SharedBusinessClassificationComponent;

    businessClassificationForm : FormGroup;
    feature:string;
    loggedUser:LoggedInUserModel;
    customerId ;
    cameraProfileObject :any;
    serviceObj={};
    counterKey=-1;
    debtorAPI : CustomerModuleApiSuffixModels
    previousPageRouteUrl :string
    featureName = "ADT Profile";
    tabIndex  = 0;
    businessClassificationDetails;
    customerTypeId:string;
    addressId = ""

    constructor(
        private router: Router,
        private crudService: CrudService,
        private dialog: MatDialog,
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder,
        private store: Store<AppState>, private rxjsService: RxjsService) {
        this.activatedRoute.queryParams.subscribe(param => {
          this.feature = param.feature
          this.customerId =  this.activatedRoute.snapshot.queryParams.customerId
          this.addressId =  this.activatedRoute.snapshot.queryParams.addressId
          this.customerTypeId =  this.activatedRoute.snapshot.queryParams.customerTypeId
        });
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
          if (!userData) return;
          this.loggedUser = userData;
        });
      }

    ngOnInit(): void {
      this.getFeatureTypes()
       this.createbusinessClassificationForm();
       if(this.customerId){
        this.getBusinessClassificationById();
      }
    }
    backUrl = "";
    forwardUrl = ""
    getFeatureTypes (){
      switch (this.feature) {
        case FEATURE_NAME.ADT_PROFILE:
          this.featureName = "ADT Site Profile"
          this.backUrl = "customer/non-customer-profile/adt-site-add-edit"
          this.forwardUrl = "customer/non-customer-profile/key-holder-information"
          break;
          case FEATURE_NAME.NKA_CUSTOMER:
            this.featureName = "NKA Customer",
            this.backUrl = "customer/non-customer-profile/nka-customer-add-edit"
            this.forwardUrl = "customer/non-customer-profile/key-holder-information"
            break;

        default:
          break;
      }
    }
    createbusinessClassificationForm() {
        this.businessClassificationForm = this.formBuilder.group({
           createdUserId: [this.loggedUser?.userId],
          });
          this.businessClassificationForm.addControl(
            "businessClassification",
            this.createBusinessClassificationFormModel()
          );
    }
    createBusinessClassificationFormModel(
      profileInfo?: BusinessClassificationModel
    ): FormGroup {
      let _profileInfo = new BusinessClassificationModel(profileInfo);
      let formControls = {};
      Object.keys(_profileInfo).forEach((key) => {
        formControls[key] = new FormControl(_profileInfo[key]);
      });
      return this.formBuilder.group(formControls);
    }

    emitBusinessClassification(event) {
      switch (event && event.type) {
        case FORMTYPE.BUSINESS_CLASSIFICATION:
             this.businessClassificationForm.get("businessClassification").patchValue(event.data);
          break;
          default:
          break;
      }
    }

    getBusinessClassificationById() {
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,CustomerModuleApiSuffixModels.NON_CUSTOMER_BUSINESS_CLASSIFICATON_DETAIL, undefined, false, prepareGetRequestHttpParams(null, null, {
        customerId: this.customerId
      }), 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.businessClassificationDetails = response.resources;
          let classification = {
          categoryId: this.businessClassificationDetails.classifications?.categoryId.toString(),
          debtorGroupId: this.businessClassificationDetails.classifications?.debtorGroupId.toString(),
          installOriginId: this.businessClassificationDetails.classifications?.installOriginId.toString(),
          originId: this.businessClassificationDetails.classifications?.originId.toString(),
          subCategoryId: this.businessClassificationDetails.classificationsubcategory?.subCategoryId.toString(),
          customerClassificationMappingId:"",
        }
        this.businessClassificationComponent.businessClassificationForm.patchValue(
            classification
        );
        this.businessClassificationComponent.onChangeOrigin(true)
        this.businessClassificationComponent.emitDataWhenFormChange();
         this.businessClassificationForm.get('businessClassification').patchValue(classification);

        }
      });

    }
    navigateTo(page) {
      if(page == 1) {
        this.router.navigate([this.backUrl] ,{queryParams:{id : this.customerId, feature:this.feature}})
      }
      if(page == 3) {
        this.router.navigate([this.forwardUrl] ,{queryParams:{customerId : this.customerId, customerTypeId :  this.customerTypeId, feature:this.feature, addressId:this.addressId}})
      }
    }

    onSubmit() {
     if (this.businessClassificationForm.invalid) {
        return;
      }

      let categoryId = this.businessClassificationForm.value.businessClassification?.categoryId;
      let debtorGroupId = this.businessClassificationForm.value.businessClassification.debtorGroupId;
      let originId = this.businessClassificationForm.value.businessClassification?.originId;
      let installOriginId = this.businessClassificationForm.value.businessClassification?.installOriginId;
      let subCategoryId = this.businessClassificationForm.value.businessClassification?.subCategoryId;

      let formData = {
        customerId: this.customerId,
        classification: {
          categoryId: categoryId ? Number(categoryId) : null,
          customerClassificationSubCategoryMappingId: "",
          debtorGroupId:  debtorGroupId ? Number(debtorGroupId) : null,
          execuGuardAccountNumber: "",
          installOriginId:  installOriginId ? Number(installOriginId) : null,
          originId: originId ? Number(originId) : null,
          qty: 1,
          serviceMappingId: "",
          subCategoryId:  subCategoryId ? Number(subCategoryId) : null,
        },
        createdUserId: this.loggedUser.userId
      }
      this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.NON_CUSTOMER_BUSINESS_CLASSIFICATON, formData, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.navigateTo(3);
        }
      });
    }


  }
