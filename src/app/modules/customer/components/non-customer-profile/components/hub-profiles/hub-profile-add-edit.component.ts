import { Component, EventEmitter, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { SharedCustomerInformationComponent } from '../../shared/components/customer-information/customer-information.component';
import { SharedProfileInformationComponent } from '../../shared/components/profile-information/profile-information.component';
import { FEATURE_NAME, FORMTYPE } from '../../shared/enum/index.enum';
import { ContactPersonModel, CustomerInformationModel, ProfileInformationModel, ProfileManagerModel } from '../../shared/model';
@Component({
  selector: 'app-hub-profile-add-edit',
  templateUrl: './hub-profile-add-edit.component.html',
  styleUrls: ['./hub-profile-add-edit.component.scss'],
})

export class HubProfileAddEditComponent implements OnInit {

  @ViewChild(SharedCustomerInformationComponent,{static:false}) customerInformationComponent: SharedCustomerInformationComponent;
  @ViewChild(SharedProfileInformationComponent,{static:false}) profileInformationComponent: SharedProfileInformationComponent;

  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  hubProfileForm: FormGroup;
  cameraProfileObject :any
  loggedUser: UserLogin;
  @Output() outputData = new EventEmitter<any>();
  @ViewChildren('input') rows: QueryList<any>;
  // profile information
  tabs = ["Hub Name", "Contact Person", "Profile Managers"]

  // formArray
  contactPersonArray: FormArray;
  profileManagerArray: FormArray;
  cdmArray: FormArray;
  cameraInformationArray: FormArray;
  cameraCustomerId ;
  feature : FEATURE_NAME
  constructor(
    private router : Router,
    private activatedRoute : ActivatedRoute,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
   this.cameraCustomerId =  this.activatedRoute.snapshot.queryParams.id
   this.feature =  this.activatedRoute.snapshot.queryParams.feature
  }

  ngOnInit(): void {
    this.createhubProfileForm();
    if(this.cameraCustomerId){
      this.getCameraCustomerData()
    }
  }

  createhubProfileForm(): void {
    // let hubProfileFormModel = new CameraProfileModel();
    this.hubProfileForm = this.formBuilder.group({
      createdUserId : [this.loggedUser?.userId]
    });

    this.hubProfileForm.addControl('customerInformationDTO', this.createCustomerInformationModel());
    this.hubProfileForm.addControl('profileInformationDTO', this.createProfileInformationModel());
    this.hubProfileForm.addControl('nonCustomerContactPersonDTOs', this.formBuilder.array([]));
    this.hubProfileForm.addControl('nonCustomerProfileManagerDTOs', this.formBuilder.array([]));
    this.hubProfileForm.addControl('cameraInformationDTO', this.formBuilder.array([]));

    this.contactPersonArray = this.getContactPersontArray
    this.profileManagerArray = this.getProfileManagerArray
    if(!this.cameraCustomerId){
    this.contactPersonArray.push(this.createProfileContactPersonFormArray());
    this.profileManagerArray.push(this.createProfileManagerFormArray());
    }







    // this.cameraInformationArray = this.getCameraInformationArray
    // this.cameraInformationArray.push(this.createCameraInformationFormArray());

    this.hubProfileForm.controls["customerInformationDTO"] = setRequiredValidator(
      this.hubProfileForm.controls["customerInformationDTO"] as FormGroup,
      ["customerTypeId", "titleId", "firstName", "lastName", "email", "mobile1","addressId"]
    );

    this.hubProfileForm.controls["profileInformationDTO"] = setRequiredValidator(
      this.hubProfileForm.controls["profileInformationDTO"] as FormGroup,
      ["communityName", "divisionId", "mainAreaId", "streetName", "suburbId"]
    );

  }


  get getContactPersontArray(): FormArray {
    if (!this.hubProfileForm) return;
    return this.hubProfileForm.get("nonCustomerContactPersonDTOs") as FormArray;
  }


  get getProfileManagerArray(): FormArray {
    if (!this.hubProfileForm) return;
    return this.hubProfileForm.get("nonCustomerProfileManagerDTOs") as FormArray;
  }




  get getCameraInformationArray(): FormArray {
    if (!this.hubProfileForm) return;
    return this.hubProfileForm.get("cameraInformationDTO") as FormArray;
  }


  emitCustomerType(data) {
    if (data == true) {
      this.hubProfileForm.controls["customerInformationDTO"] = setRequiredValidator(
        this.hubProfileForm.controls["customerInformationDTO"] as FormGroup,
        ["companyName"]
      );

    } else {
      this.hubProfileForm.controls["customerInformationDTO"] = clearFormControlValidators(
        this.hubProfileForm.controls["customerInformationDTO"] as FormGroup,
        ["companyName"]
      );
    }
  }

  emitCustomerInformation(event) {

    switch (event.type) {
      case FORMTYPE.CUSTOMER_INFORMATION:
        this.hubProfileForm.get('customerInformationDTO').setValue(event.data);
        break;
      case FORMTYPE.PROFILE_COMMUNITY:
        if(event.data?.suburbId && typeof event.data?.suburbId  == "object" ){
          event.data.suburbId = event.data.suburbId?.id
        }
        if (event?.data?.mainAreaId && typeof event.data?.mainAreaId  == "object") {
          event.data.mainAreaId = event.data.mainAreaId?.toString();
          event.data.subAreaId = null;
        }
        this.hubProfileForm.get('profileInformationDTO').setValue(event.data);
        break;
        case FORMTYPE.PROFILE_MANAGER:

          let pManager = this.getProfileManagerArray.controls[0].get("roleId").value;
        if (!pManager) {
          this.getProfileManagerArray.controls[0].patchValue(event.data);
        } else {
          this.getProfileManagerArray.push(
            this.createProfileManagerFormArray(event.data)
          );
        }
        this.hubProfileForm.markAllAsTouched();
          break;

        case FORMTYPE.PROFILE_CONTACT:
          if(this.getContactPersontArray.length!=0){
          let contactPerson =
            this.getContactPersontArray.controls[0].get("contactPersion").value;
          if (!contactPerson) {
            this.getContactPersontArray.controls[0].patchValue(event.data);
          } else {
            this.getContactPersontArray.push(
              this.createProfileContactPersonFormArray(event.data)
            );
          }
        }else{
          this.getContactPersontArray.push(
            this.createProfileContactPersonFormArray(event.data)
          );
        }
          this.hubProfileForm.markAllAsTouched();
          break;
      default:
        break;
    }
  }


  emitRemoveElementByIndex(event) {
    switch (event.type) {
      case FORMTYPE.PROFILE_MANAGER:
        this.getProfileManagerArray.removeAt(event.index);
        this.hubProfileForm.markAllAsTouched();
        break;
      case FORMTYPE.PROFILE_CONTACT:
        this.contactPersonArray.removeAt(event.index);
        this.hubProfileForm.markAllAsTouched();
        break;
    }
    this.contactPersonArray.markAllAsTouched();
  }

  createCustomerInformationModel(basicInfo?: CustomerInformationModel): FormGroup {
    let customerInfo = new CustomerInformationModel(basicInfo);
    let formControls = {};
    Object.keys(customerInfo).forEach((key) => {
      formControls[key] = new FormControl(customerInfo[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createProfileInformationModel(profileInfo?: ProfileInformationModel): FormGroup {
    let _profileInfo = new ProfileInformationModel(profileInfo);
    let formControls = {};
    Object.keys(_profileInfo).forEach((key) => {
      formControls[key] = new FormControl(_profileInfo[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createProfileContactPersonFormArray(contactPerson?: ContactPersonModel): FormGroup {
    let _contactPerson = new ContactPersonModel(contactPerson);
    let formControls = {};
    Object.keys(_contactPerson).forEach((key) => {
      if (key == "contactPersion" || key == "contactNumber" || key == 'contactNumberCountryCode') {
        formControls[key] = new FormControl( _contactPerson[key], [Validators.required]);
      } else {
        formControls[key] = new FormControl(_contactPerson[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  createProfileManagerFormArray(profileManager?: ProfileManagerModel): FormGroup {
    let _profileManager = new ProfileManagerModel(profileManager);
    let formControls = {};
    Object.keys(_profileManager).forEach((key) => {
      if (key == "roleId" || key == "employeeId") {
        formControls[key] = new FormControl(_profileManager[key], [Validators.required]);
      } else {
        formControls[key] = new FormControl(_profileManager[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }



  onSubmit(): void {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.hubProfileForm.get('createdUserId').setValue(this.loggedUser?.userId)
    if (this.hubProfileForm.invalid) return;

    this.hubProfileForm.value.customerInformationDTO.mobile1 = this.hubProfileForm.value.customerInformationDTO.mobile1
      .toString()
      .replace(/\s/g, "");
    if (this.hubProfileForm.value.customerInformationDTO.mobile2) {
      this.hubProfileForm.value.customerInformationDTO.mobile2 = this.hubProfileForm.value.customerInformationDTO.mobile2
        .toString()
        .replace(/\s/g, "");
    }
    if (this.hubProfileForm.value.customerInformationDTO.officeNo) {
      this.hubProfileForm.value.customerInformationDTO.officeNo = this.hubProfileForm.value.customerInformationDTO.officeNo
        .toString()
        .replace(/\s/g, "");
    }
    if (this.hubProfileForm.value.customerInformationDTO.premisesNo) {
      this.hubProfileForm.value.customerInformationDTO.premisesNo = this.hubProfileForm.value.customerInformationDTO.premisesNo
        .toString()
        .replace(/\s/g, "");
    }
    let formValue= this.hubProfileForm.getRawValue();
    formValue.customerInformationDTO.siteTypeId =  formValue.customerInformationDTO.siteTypeId ?  formValue.customerInformationDTO.siteTypeId : 0;
    formValue.siteTypeId =  formValue.siteTypeId? formValue.siteTypeId:0;
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.HUB_PROFILE, this.hubProfileForm.value).subscribe(response=>{
      if(response.isSuccess && response.statusCode === 200){
        this.router.navigate(["customer/non-customer-profile"],{ queryParams: {tab:2}});
        //this.router.navigate(['customer/non-customer-profile/debtor-information'] ,{queryParams:{customerId : response.resources, customerTypeId :  this.hubProfileForm.value.customerInformationDTO.customerTypeId, feature:this.feature}})
      }
    })
  }


  getCameraCustomerData (){
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.HUB_PROFILE_DETAILS,undefined, false,prepareRequiredHttpParams({CustomerId :this.cameraCustomerId})).subscribe(response=>{
      if(response.isSuccess && response.statusCode === 200){{
        delete response.resources.nonCustomerCDMDTOs;
        this.hubProfileForm.patchValue(response.resources);
        this.cameraProfileObject = response.resources;
        // customer Information Component

        this.customerInformationComponent.customerInformatioanForm.patchValue(response.resources.customerInformationDTO);
        for (let index = 0; index < (response.resources.nonCustomerContactPersonDTOs.length); index++) {

          this.getContactPersontArray.push(this.createProfileContactPersonFormArray(response.resources.nonCustomerContactPersonDTOs[index]));
        }
        for (let index = 0; index < (response.resources.nonCustomerProfileManagerDTOs.length); index++) {
          this.getProfileManagerArray.push(this.createProfileManagerFormArray(response.resources.nonCustomerProfileManagerDTOs[index]));
        }
        //
        this.profileInformationComponent.profileInformationForm.patchValue(response.resources.profileInformationDTO);
        this.profileInformationComponent.onChangeMainArea();
        this.profileInformationComponent.setSuburbs();

        this.profileInformationComponent.contactList = response.resources.nonCustomerContactPersonDTOs;
        this.profileInformationComponent.profileManagerList = response.resources.nonCustomerProfileManagerDTOs;
      //   setTimeout(() => {
      //   this.profileInformationComponent.fillData('profile-manager',response.resources.nonCustomerProfileManagerDTOs);
      // }, 5000);

      }}
    })
  }


  navigateToDebtorInformation(){
    if(!this.cameraProfileObject) return ;


    this.router.navigate(['customer/non-customer-profile/debtor-information'] ,{queryParams:{customerId : this.cameraCustomerId, customerTypeId :  this.cameraProfileObject.customerInformationDTO.customerTypeId, feature : this.feature}})

  }

  goBack(){
    this.router.navigateByUrl('customer/non-customer-profile?tab=2')
  }

}
