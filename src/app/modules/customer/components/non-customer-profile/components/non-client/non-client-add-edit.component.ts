import { Component, EventEmitter, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { UserLogin } from '@modules/others/models';
import { AddressModel } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { SharedAddressInformationComponent } from '../../shared/components/address-information/address-information.component';
import { SharedCustomerInformationComponent } from '../../shared/components/customer-information/customer-information.component';
import { FEATURE_NAME, FORMTYPE } from '../../shared/enum/index.enum';
import { CustomerInformationModel } from '../../shared/model';
@Component({
  selector: 'app-camera-profile-add-edit',
  templateUrl: './non-client-add-edit.component.html',
  styleUrls: ['./non-client-add-edit.component.scss'],
})

export class NonClientProfileAddEditComponent implements OnInit {

  @ViewChild(SharedCustomerInformationComponent,{static:false}) customerInformationComponent: SharedCustomerInformationComponent;
  @ViewChild(SharedAddressInformationComponent,{static:false}) SharedAddressInformationComponent: SharedAddressInformationComponent;

  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  nonClientProfileForm: FormGroup;
  cameraProfileObject :any
  loggedUser: UserLogin;
  @Output() outputData = new EventEmitter<any>();
  @ViewChildren('input') rows: QueryList<any>;
  // profile information

  // formArray
  cameraCustomerId ;
  feature : FEATURE_NAME
  constructor(
    private router : Router,
    private activatedRoute : ActivatedRoute,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
   this.cameraCustomerId =  this.activatedRoute.snapshot.queryParams.id
   this.feature =  this.activatedRoute.snapshot.queryParams.feature
  }

  ngOnInit(): void {
    this.createNonClientProfileForm();
    if(this.cameraCustomerId){
      this.getCameraCustomerData()
    }
  }

  createNonClientProfileForm(): void {
    this.nonClientProfileForm = this.formBuilder.group({
      createdUserId : [this.loggedUser?.userId]
    });

    this.nonClientProfileForm.addControl('customerInformationDTO', this.createCustomerInformationModel());
    this.nonClientProfileForm.addControl('addressInfo', this.createAddressInformationModel());

    this.nonClientProfileForm.controls["customerInformationDTO"] = setRequiredValidator(
      this.nonClientProfileForm.controls["customerInformationDTO"] as FormGroup,
      ["customerTypeId", "titleId", "firstName", "lastName", "email", "mobile1"]
    );

    this.nonClientProfileForm.controls["addressInfo"] = setRequiredValidator(
      this.nonClientProfileForm.controls["addressInfo"] as FormGroup,
      ["formatedAddress", "streetNo"]
    );


  }

  emitCustomerType(data) {
    if (data == true) {
      this.nonClientProfileForm.controls["customerInformationDTO"] = setRequiredValidator(
        this.nonClientProfileForm.controls["customerInformationDTO"] as FormGroup,
        ["companyName"]
      );

    } else {
      this.nonClientProfileForm.controls["customerInformationDTO"] = clearFormControlValidators(
        this.nonClientProfileForm.controls["customerInformationDTO"] as FormGroup,
        ["companyName"]
      );
    }
  }

  emitCustomerInformation(event) {
    switch (event.type) {
      case FORMTYPE.CUSTOMER_INFORMATION:
        this.nonClientProfileForm.get('customerInformationDTO').setValue(event.data);
        break;
      case FORMTYPE.ADDRESS_INFORMATION:
        this.nonClientProfileForm.get('addressInfo').setValue(event.data);

        break;
      default:
        break;
    }
  }
  afrigisAddressInfo  ={};
  emitAfrigisAddressInfo(event){
    this.afrigisAddressInfo = {
      buildingName: event?.buildingName,
      buildingNo: event?.buildingNo,
      streetNo: event?.streetNo,
    };
  }

  emitChangeFormCheckbox (event){
    if(event.type == "addressExtra"){
      if(event.action =="add"){
        this.nonClientProfileForm.controls["addressInfo"].get('estateStreetNo').setValidators([Validators.required]);
        this.nonClientProfileForm.controls["addressInfo"].get('estateStreetNo').updateValueAndValidity();
        this.nonClientProfileForm.controls["addressInfo"].get('estateStreetName').setValidators([Validators.required]);
        this.nonClientProfileForm.controls["addressInfo"].get('estateStreetName').updateValueAndValidity();
        this.nonClientProfileForm.controls["addressInfo"].get('estateName').setValidators([Validators.required]);
        this.nonClientProfileForm.controls["addressInfo"].get('estateName').updateValueAndValidity();
      }else{
        this.nonClientProfileForm.controls["addressInfo"].get('estateStreetNo').clearValidators();
        this.nonClientProfileForm.controls["addressInfo"].get('estateStreetNo').updateValueAndValidity();
        this.nonClientProfileForm.controls["addressInfo"].get('estateStreetName').clearValidators();
        this.nonClientProfileForm.controls["addressInfo"].get('estateStreetName').updateValueAndValidity();
        this.nonClientProfileForm.controls["addressInfo"].get('estateName').clearValidators();
        this.nonClientProfileForm.controls["addressInfo"].get('estateName').updateValueAndValidity();
      }
    }
    if(event.type =="complex"){

      if(event.action  =='add'){
        this.nonClientProfileForm.controls["addressInfo"].get('buildingNo').setValidators([Validators.required]);
        this.nonClientProfileForm.controls["addressInfo"].get('buildingNo').updateValueAndValidity();
        this.nonClientProfileForm.controls["addressInfo"].get('buildingName').setValidators([Validators.required]);
        this.nonClientProfileForm.controls["addressInfo"].get('buildingName').updateValueAndValidity();
      }else{
        this.nonClientProfileForm.controls["addressInfo"].get('buildingNo').clearValidators();
        this.nonClientProfileForm.controls["addressInfo"].get('buildingNo').updateValueAndValidity();
        this.nonClientProfileForm.controls["addressInfo"].get('buildingName').clearValidators();
        this.nonClientProfileForm.controls["addressInfo"].get('buildingName').updateValueAndValidity();
      }
    }

  }

  createCustomerInformationModel(basicInfo?: CustomerInformationModel): FormGroup {
    let customerInfo = new CustomerInformationModel(basicInfo);
    let formControls = {};
    Object.keys(customerInfo).forEach((key) => {
      formControls[key] = new FormControl(customerInfo[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createAddressInformationModel(addressModel?: AddressModel): FormGroup {
    let _addressInfo = new AddressModel(addressModel);
    let formControls = {};
    Object.keys(_addressInfo).forEach((key) => {
      formControls[key] = new FormControl(_addressInfo[key]);
    });
    return this.formBuilder.group(formControls);
  }


  onSubmit(): void {
    this.nonClientProfileForm.get('createdUserId').setValue(this.loggedUser?.userId)
    if (this.nonClientProfileForm.invalid) return;
    let formValue = this.nonClientProfileForm.getRawValue()
    formValue.customerInformationDTO.mobile1 = formValue.customerInformationDTO.mobile1
      .toString()
      .replace(/\s/g, "");
    if (formValue.customerInformationDTO.mobile2) {
      formValue.customerInformationDTO.mobile2 = formValue.customerInformationDTO.mobile2
        .toString()
        .replace(/\s/g, "");
    }
    if (formValue.customerInformationDTO.officeNo) {
      formValue.customerInformationDTO.officeNo = formValue.customerInformationDTO.officeNo
        .toString()
        .replace(/\s/g, "");
    }
    if (formValue.customerInformationDTO.premisesNo) {
      formValue.customerInformationDTO.premisesNo = formValue.customerInformationDTO.premisesNo
        .toString()
        .replace(/\s/g, "");
    }

    if (formValue.addressInfo.latLong) {
      formValue.addressInfo.latitude = formValue.addressInfo.latLong.split(
        ","
      )[0];
      formValue.addressInfo.longitude = formValue.addressInfo.latLong.split(
        ","
      )[1];
    }
    formValue.afrigisAddressInfo =  this.afrigisAddressInfo
    formValue.customerInformationDTO.customerId = this.cameraCustomerId?this.cameraCustomerId:null
    formValue.customerInformationDTO.siteTypeId =  formValue.customerInformationDTO.siteTypeId ?  formValue.customerInformationDTO.siteTypeId : 0;
    formValue.siteTypeId =  formValue.siteTypeId? formValue.siteTypeId:0;
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.NON_CLIENT_PROFILE, formValue).subscribe(response=>{
      if(response.isSuccess && response.statusCode === 200){
        this.router.navigate(['customer/non-customer-profile/debtor-information'] ,{queryParams:{customerId : response.resources, customerTypeId :  formValue.customerInformationDTO.customerTypeId, communityId:this.cameraCustomerId, feature:this.feature}})
      }
    })
  }

  getCameraCustomerData (){
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.NON_CLIENT_PROFILE_DETAIL,undefined, false,prepareRequiredHttpParams({customerId :this.cameraCustomerId})).subscribe(response=>{
      if(response.isSuccess && response.statusCode === 200){{
        this.nonClientProfileForm.patchValue(response.resources);
        this.cameraProfileObject = response.resources;
        // customer Information Component
        this.customerInformationComponent.createcustomerInformatioanForm()
        this.customerInformationComponent.emitDataWhenFormChange();
        this.customerInformationComponent.customerInformatioanForm.patchValue(response.resources.customerInformationDTO);
        this.SharedAddressInformationComponent.createAddresForm()
            response.resources.addressInfo.latLong =
            response.resources.addressInfo.latitude +
            "," +
            response.resources.addressInfo.longitude;


        this.SharedAddressInformationComponent.addressForm.patchValue(response.resources.addressInfo,{emitEvent:false, onlySelf:true});
        this.SharedAddressInformationComponent.addressForm.get("formatedAddress").setValue(response.resources.addressInfo.formatedAddress)
        this.SharedAddressInformationComponent.addressForm.get("addressConfidentLevel").setValue(response.resources.addressInfo.addressConfidentLevelName)
        this.SharedAddressInformationComponent.onFormControlChanges()
      }}
    })
  }

  navigateToDebtorInformation(){
    if(!this.cameraProfileObject) return ;
    this.router.navigate(['customer/non-customer-profile/debtor-information'] ,{queryParams:{customerId : this.cameraCustomerId, customerTypeId :  this.cameraProfileObject.customerInformationDTO.customerTypeId, feature:this.feature}})

  }
  goBack(){
    this.router.navigateByUrl('customer/non-customer-profile?tab=1')
  }
}
