import { Component, ElementRef, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import {
  btnActionTypes, KeyHolderInfoAddEditModel,
  loggedInUserData
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  BreadCrumbModel,
  ConfirmDialogModel, ConfirmDialogPopupComponent,
  countryCodes, CrudService, CustomDirectiveConfig, formConfigs,
  HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  prepareGetRequestHttpParams,
  prepareRequiredHttpParams, RxjsService
} from '@app/shared';
import { ModuleName } from '@app/shared/enums';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { LeadHeaderData } from '@modules/sales/models';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { FEATURE_NAME } from '../../shared/enum/index.enum';
import { KeyHolderModel, ProfileEmergencyContactModel } from '../../shared/model/keyholder.model';
@Component({
  selector: 'app-non-customer-keyholder-info',
  templateUrl: './key-holder-details.component.html',
  styleUrls: ['./keyholder-details.component.scss']
})

export class KeyholderDetailsComponent implements OnInit {
  @Input() inputObject = { fromComponentType: 'lead', customerId: "", addressId: "", partitionId: "" };
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  keyHolderInfoForm: FormGroup;
  keyholders: FormArray;
  emergencyContacts: FormArray;
  isFocus = false;
  countryCodes = countryCodes;
  leadHeaderData: LeadHeaderData;
  currentAgreementTabObj;
  @ViewChildren('keyholderRows',) keyholderRows: QueryList<ElementRef>;
  @ViewChildren('emergencyContactRows') emergencyContactRows: QueryList<ElementRef>;
  areEmergencyContactsFocus = false;
  areKeyholdersFocus = false;
  isDisabled = false;
  loggedInUserData: LoggedInUserModel;
  breadCrumb: BreadCrumbModel;
  customerId;
  addressId;
  feature;
  details: any = { emergencyContacts: [], keyholders: [] }

  constructor(private crudService: CrudService,
    private httpCancelService: HttpCancelService, private formBuilder: FormBuilder,
    private router: Router, public rxjsService: RxjsService, private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog
  ) {
    this.activatedRoute.queryParams.subscribe(param => {
      this.customerId = param.customerId
      this.feature = param.feature
      this.addressId = param.addressId

    })
    this.combineLatestNgrxStoreData();
    this.rxjsService.setQuickActionComponent(ModuleName.LEAD)
  }

  ngOnInit(): void {
    // this.isDisabled = this.inputObject.fromComponentType == 'lead' ? false : true;
    this.createKeyHolderInfoForm();
    this.getKeyholderById();
    this.decideNavigations();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).pipe(take(1)).subscribe((response) => {
      if (!response) return;
      this.loggedInUserData = new LoggedInUserModel(response[0]);

    });
  }

  getKeyholderById(): void {
    let resultObservable = this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.REPEATER_PROFILE_KEY_HOLDER_DETAIL, undefined, false,
      prepareGetRequestHttpParams(undefined, undefined, {
        customerId: this.customerId,
      }), 1)
    resultObservable.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode === 200) {
        this.details = response.resources
        this.keyholders = this.keyHoldersArray;
        while (this.keyHoldersArray.length) {
          this.keyHoldersArray.removeAt(this.keyHoldersArray.length - 1);
        }
        this.emergencyContacts = this.emergencyContactsArray;
        while (this.emergencyContactsArray.length) {
          this.emergencyContactsArray.removeAt(this.emergencyContactsArray.length - 1);
        }
        if (response.resources.keyholders.length == 0) {
          this.keyHoldersArray.push(this.createNewItem());
          this.keyHoldersArray.push(this.createNewItem());
          this.emergencyContactsArray.push(this.createNewItem('emergencyContacts'));
        }
        else {
          response.resources.keyholders.forEach((keyholderPersonObj: KeyHolderModel) => {
            this.keyHoldersArray.push(this.createNewItem('keyholders', keyholderPersonObj));
          });
          response.resources.emergencyContacts.forEach((emergencyContactModel: ProfileEmergencyContactModel) => {
            this.emergencyContactsArray.push(this.createNewItem('emergencyContacts', emergencyContactModel));
          });
        }
        this.keyholders = this.keyHoldersArray;
        this.emergencyContacts = this.emergencyContactsArray;
        const keyHolderInfoAddEditModel = new KeyHolderInfoAddEditModel(response.resources);
        this.keyHolderInfoForm.patchValue(keyHolderInfoAddEditModel);
        if (response.resources.keyholders.length > 0 && response.resources.emergencyContacts.length > 0) {
          this.focusAllFields();
        }

      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  focusAllFields() {
    setTimeout(() => {
      this.focusInAndOutFormArrayFields();
      this.focusInAndOutFormArrayFields('emergencyContacts');
    }, 100);
  }

  createKeyHolderInfoForm(): void {
    let keyHolderInfoAddEditModel = new KeyHolderInfoAddEditModel();
    this.keyHolderInfoForm = this.formBuilder.group({});
    Object.keys(keyHolderInfoAddEditModel).forEach((key) => {
      this.keyHolderInfoForm.addControl(key, new FormArray([]));
    });
    this.keyHoldersArray.push(this.createNewItem('keyholders', {}));
    this.emergencyContactsArray.push(this.createNewItem('emergencyContacts', {}));

  }

  createNewItem(itemType = 'keyholders', object?): FormGroup {
    let objectModel = itemType == 'keyholders' ? new KeyHolderModel(object) : new ProfileEmergencyContactModel(object);
    let formControls = {};
    if (itemType == 'keyholders') {
      Object.keys(objectModel).forEach((key) => {
        formControls[key] = [objectModel[key],
        (key === 'customerKeyHolderId' || key === 'keyholderPersonId' ||
          key === 'password' || key === 'createdUserId') ? [] : [Validators.required]]
      });
    }
    else {
      Object.keys(objectModel).forEach((key) => {
        formControls[key] = [objectModel[key],
        (key === 'customerEmergencyContactId' || key === 'createdUserId') ? [] : [Validators.required]]
      });
    }
    formControls['createdUserId'][0] = this.loggedInUserData?.userId;
    formControls['customerId'][0] = this.customerId

    return this.formBuilder.group(formControls);
  }

  focusInAndOutFormArrayFields(itemType = 'keyholders'): void {
    if (itemType == 'keyholders') {
      this.areKeyholdersFocus = true;
      this.keyholderRows.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      })
    }
    else {
      this.areEmergencyContactsFocus = true;
      this.emergencyContactRows.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      })
    }
  }

  addItem(itemType = 'keyholders'): void {
    if (itemType == 'keyholders') {
      if (this.keyHoldersArray.invalid) {
        this.focusInAndOutFormArrayFields(itemType);
        return;
      };
      this.areKeyholdersFocus = false;
      this.keyholders = this.keyHoldersArray;
      let keyHolderModel = new KeyHolderModel();
      this.keyHoldersArray.insert(0, this.createNewItem(itemType, keyHolderModel));
    }
    else {
      if (this.emergencyContactsArray.invalid) {
        this.focusInAndOutFormArrayFields(itemType);

        return;
      };
      this.areEmergencyContactsFocus = false;
      this.emergencyContacts = this.emergencyContactsArray;
      let emergencyContactModel = new ProfileEmergencyContactModel();

      this.emergencyContactsArray.insert(0, this.createNewItem(itemType, emergencyContactModel));
    }
  }

  removeItem(itemType = 'keyholders', i: number): void {
    if (itemType == 'keyholders') {
      if (!this.keyHoldersArray.controls[i].value.customerKeyHolderId) {
        this.keyHoldersArray.removeAt(i);
        return;
      }
    }
    else {
      if (!this.emergencyContactsArray.controls[i].value.customerEmergencyContactId) {
        this.emergencyContactsArray.removeAt(i);
        return;
      }
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    if (itemType == 'keyholders') {
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        if (this.keyHoldersArray.controls[i].value.customerKeyHolderId && this.keyHoldersArray.length > 1) {
          let resultObservable = this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.REPEATER_PROFILE_KEY_HOLDER, undefined,
            prepareRequiredHttpParams({
              customerKeyHolderId: this.keyHoldersArray.controls[i].value.customerKeyHolderId,
              isDeleted: true,
              modifiedUserId: this.loggedInUserData?.userId
            }), 1)
          resultObservable.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              this.keyHoldersArray.removeAt(i);
            }
            if (this.keyHoldersArray.length === 0) {
              this.addItem();
            };
          });
        }
        else {
          this.keyHoldersArray.removeAt(i);
        }
      });
    }
    else {
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        if (this.emergencyContactsArray.controls[i].value.customerEmergencyContactId && this.emergencyContactsArray.length > 1) {
          let resultObservable = this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.REPEATER_PROFILE_EMERGENCY_CONTACTS, undefined,
            prepareRequiredHttpParams({
              customerEmergencyContactId: this.emergencyContactsArray.controls[i].value.customerEmergencyContactId,
              isDeleted: true,
              modifiedUserId: this.loggedInUserData?.userId
            }), 1)

          resultObservable.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              this.emergencyContactsArray.removeAt(i);
            }
            if (this.emergencyContactsArray.length === 0) {
              this.addItem('emergencyContacts');
            };
          });
        }
        else {
          this.emergencyContactsArray.removeAt(i);
        }
      });
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  get keyHoldersArray(): FormArray {
    if (!this.keyHolderInfoForm) return;
    return this.keyHolderInfoForm.get("keyholders") as FormArray;
  }

  get emergencyContactsArray(): FormArray {
    if (!this.keyHolderInfoForm) return;
    return this.keyHolderInfoForm.get("emergencyContacts") as FormArray;
  }

  getKeyHolders(form): Array<any> {
    return form.controls.keyholders.controls;
  }

  getEmergencyContacts(form): Array<any> {
    return form.controls.emergencyContacts.controls;
  }

  findDuplicate(itemType = 'keyholders', formGroup: FormGroup, formArray: FormArray, duplicateFormControlName: string, currentCursorIndex: number): void {
    const lookup = formArray.value.reduce((a, e) => {
      if (a[e[duplicateFormControlName]]) {
        a[e[duplicateFormControlName]] = a[e[duplicateFormControlName]] + 1;
      }
      else {
        a[e[duplicateFormControlName]] = 1;
      }
      return a;
    }, {});
    const itemArray = itemType == 'keyholders' ? this.getKeyHolders(formGroup) : this.getEmergencyContacts(formGroup);
    const duplicatedDataOne = itemArray.
      filter((duplicatedFormGroup: FormGroup) => {
        let filteredResult = lookup[duplicatedFormGroup.controls[duplicateFormControlName].value] !== 1 &&
          lookup[duplicatedFormGroup.controls[duplicateFormControlName].value] !== "";
        if (filteredResult) {
          return duplicatedFormGroup;
        }
      });

    if (currentCursorIndex <= duplicatedDataOne.length) {
      if (duplicatedDataOne.length > 1) {
        duplicatedDataOne.forEach((formGroup) => {
          Object.keys(formGroup.controls).forEach((formControlName) => {
            if (formControlName === duplicateFormControlName && formGroup.controls[duplicateFormControlName].value && formGroup.controls['currentCursorIndex'].value == currentCursorIndex) {
              formGroup.get(duplicateFormControlName).setErrors({ duplicate: true });
            }
          });
        });
      }
    }
    else {
      duplicatedDataOne.forEach((formGroup, ix) => {
        Object.keys(formGroup.controls).forEach(() => {
          if (ix === duplicatedDataOne.length - 1) {
            formGroup.get(duplicateFormControlName).setErrors({ duplicate: true });
          }
        });
      });
    }
  }

  onContactNumberChanges(itemType: string, currentCursorIndex: number): void {
    const controls = itemType == 'keyholders' ? this.keyHoldersArray.controls : this.emergencyContactsArray.controls;
    const formArray = itemType == 'keyholders' ? this.keyHoldersArray : this.emergencyContactsArray;
    controls[currentCursorIndex].get('currentCursorIndex').setValue(currentCursorIndex);
    controls[currentCursorIndex].get('contactNo').valueChanges.subscribe((contactNo: string) => {
      setTimeout(() => {
        this.findDuplicate(itemType, this.keyHolderInfoForm, formArray, 'contactNo', currentCursorIndex);
      }, 100)
    });
  }

  onBlur(itemType: string, e, index: number) {
    const controls = itemType == 'keyholders' ? this.keyHoldersArray.controls : this.emergencyContactsArray.controls;
    if (controls[index].get('contactNo').errors) {
      if (controls[index].get('contactNo').errors.hasOwnProperty('duplicate')) {
        var event = this.disableAllUserEvents();
        e.target.focus();
        event();
        return;
      }
    }
  }

  disableAllUserEvents = () => {
    const events = [
      "click",
      "contextmenu",
      "dblclick",
      "mousedown",
      "mouseenter",
      "mouseleave",
      "mousemove",
      "mouseover",
      "mouseout",
      "mouseup",
      "blur",
      "change",
      "focus",
      "focusin",
      "focusout",
      "input",
      "invalid",
      "reset",
      "search",
      "select",
      "submit",
      "drag",
      "dragend",
      "dragenter",
      "dragleave",
      "dragover",
      "dragstart",
      "drop",
      "copy",
      "cut",
      "paste",
      "mousewheel",
      "wheel",
      "touchcancel",
      "touchend",
      "touchmove",
      "touchstart"
    ];

    const handler = event => {
      event.stopPropagation();
      event.preventDefault();

      return false;
    };

    for (let i = 0, l = events.length; i < l; i++) {
      document.addEventListener(events[i], handler, true);
    }

    return () => {
      for (let i = 0, l = events.length; i < l; i++) {
        document.removeEventListener(events[i], handler, true);
      }
    };
  };

  onSubmit(): void {
    if (this.keyHolderInfoForm.invalid) {
      return;
    }
    this.keyHolderInfoForm.value.keyholders.forEach((keyholderObj: KeyHolderModel) => {
      keyholderObj.contactNo = keyholderObj.contactNo.toString().replace(/\s/g, "");
    });
    this.keyHolderInfoForm.value.emergencyContacts.forEach((emergencyContactObj: ProfileEmergencyContactModel) => {
      emergencyContactObj.contactNo = emergencyContactObj.contactNo.toString().replace(/\s/g, "");
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.REPEATER_PROFILE_KEY_HOLDER, this.keyHolderInfoForm.value, 1);
    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        if (response.resources.keyholders) {
          this.onArrowClick(3);
        }
      }
    });
  }

  onArrowClick(index: number) {
    if (index == 1) {
      this.router.navigate([this.backwardUrl], { queryParams: { id: this.customerId, feature: this.feature ,addressId:this.addressId} })
    } else {
      if (this.customerId) {
        this.router.navigate(['customer/non-customer-profile/password-special-instructions'], { queryParams: { customerId: this.customerId, feature: this.feature,addressId:this.addressId } })
      }
    }

  }



  onEditBtnClicked() {
    this.isDisabled = false;
    this.focusAllFields()
  }

  isShowNext = false
  currentIndex = 2;
  backwardUrl = ""
  decideNavigations() {
    if (this.feature == FEATURE_NAME.REPEATER_PROFILE) {
      this.currentIndex = 2
      this.backwardUrl = "customer/non-customer-profile/repeater-profile-add-edit"
    } else if (this.feature == FEATURE_NAME.ADT_PROFILE) {
      this.isShowNext = true
      this.currentIndex = 3
      this.backwardUrl = "customer/non-customer-profile/adt/business-classification"
    } else if (this.feature == FEATURE_NAME.NKA_CUSTOMER) {
      this.isShowNext = true
      this.currentIndex = 3
      this.backwardUrl = "customer/non-customer-profile/nka-customer/business-classification"
    }
  }

  goNext() {

  }

  goBack() {
    this.router.navigate(['customer/non-customer-profile'], { queryParams: { tab: 3 } })
  }
}
