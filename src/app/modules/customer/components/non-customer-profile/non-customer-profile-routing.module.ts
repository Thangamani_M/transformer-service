import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NonCustomerBusinessClassificationComponent } from './components/non-customer-business-classification/non-customer-business-classification.component';
import { AdtSiteProfileAddEditComponent } from './components/adt-site-profile/adt-site-profile-add-edit.component';
import { BusinessClassificationComponent } from './components/business-classification/business-classification-component';
import { CameraProfileAddEditComponent } from './components/camera-profiles/camera-profile-add-edit.component';
import { CasualGuardingProfileAddEditComponent } from './components/casual-guarding/casual-guarding-add-edit.component';
import { CommunityProfileAddEditComponent } from './components/community-profile/community-profile-add-edit.component';
import { DebtorBankingInformationComponent } from './components/debtor-banking-information/debtor-banking-information.component';
import { DebtorInformationComponent } from './components/debtor-information/debtor-information.component';
import { ExecuGuardProfileAddEditComponent } from './components/execu-guard-profile/execu-guard-profile-component';
import { HubProfileAddEditComponent } from './components/hub-profiles/hub-profile-add-edit.component';
import { KeyholderDetailsComponent } from './components/key-holder-details/key-holder-details.component';
import { NonClientProfileAddEditComponent } from './components/non-client/non-client-add-edit.component';
import { PasswordInstructionsComponent } from './components/password-instructions/password-instructions.component';
import { RepeaterProfileAddEditComponent } from './components/repeater-profile/repeater-profile-add-edit.component';
import { TestProfileAddEditComponent } from './components/test-profile/test-profile.component';
import { WhatsappProfileAddEditComponent } from './components/whatsapp-profiles/whatsapp-profile-add-edit.component';
import { NonCustomerProfileComponent } from './non-customer-profile.component';
import { ArmingDisarmingComponent } from './components/arming-disarming/arming-disarming.component';
import { NonCustomerAccessToPremisesComponent } from './components/access-to-premises/access-to-premises.component';
import { NkaCustomerProfileAddEditComponent } from './components/nka-customer-profile/nka-customer-profile-add-edit.component';
import { SuspenseAccountAddEditComponent } from './components/suspense-account/suspense-account-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const customerModuleRoutes: Routes = [
  { path: '', component: NonCustomerProfileComponent, canActivate: [AuthGuard], data: { title: 'Non Customer Profile' } },
  { path: 'camera-profile-add-edit', component: CameraProfileAddEditComponent, canActivate: [AuthGuard], data: { title: 'Add Camera Profile' } },
  { path: 'whatsapp-profile-add-edit', component: WhatsappProfileAddEditComponent, canActivate: [AuthGuard], data: { title: 'Add Whatsapp Profile' } },
  { path: 'community-profile-add-edit', component: CommunityProfileAddEditComponent, canActivate: [AuthGuard], data: { title: 'Add Community Profile' } },
  { path: 'execu-guard-profile-add-edit', component: ExecuGuardProfileAddEditComponent, canActivate: [AuthGuard], data: { title: 'Execu Guard Profile' } },
  { path: 'test-profile-add-edit', component: TestProfileAddEditComponent, canActivate: [AuthGuard], data: { title: 'Test Profile' } },
  { path: 'non-client-profile-add-edit', component: NonClientProfileAddEditComponent, canActivate: [AuthGuard], data: { title: 'Non Client Profile' } },
  { path: 'hub-profile-add-edit', component: HubProfileAddEditComponent, canActivate: [AuthGuard], data: { title: 'Hub Profile' } },
  { path: 'repeater-profile-add-edit', component: RepeaterProfileAddEditComponent, canActivate: [AuthGuard], data: { title: 'Repeater Profile' } },
  { path: 'business-classification', component: BusinessClassificationComponent, canActivate: [AuthGuard], data: { title: 'Business Classification' } },
  { path: 'debtor-information', component: DebtorInformationComponent, canActivate: [AuthGuard], data: { title: 'Debtor Information' } },
  { path: 'debtor-banking-information', component: DebtorBankingInformationComponent, canActivate: [AuthGuard], data: { title: 'Debtor Banking Information' } },
  { path: 'key-holder-information', component: KeyholderDetailsComponent, canActivate: [AuthGuard], data: { title: 'Key Holder Information' } },
  { path: 'password-special-instructions', component: PasswordInstructionsComponent, canActivate: [AuthGuard], data: { title: 'Password & Special Instructions' } },
  { path: 'casual-guarding-add-edit', component: CasualGuardingProfileAddEditComponent, canActivate: [AuthGuard], data: { title: 'Casual Guarding Profile' } },
  { path: 'adt-site-add-edit', component: AdtSiteProfileAddEditComponent, canActivate: [AuthGuard], data: { title: 'ADT Site Profile' } },
  { path: 'adt/business-classification', component: NonCustomerBusinessClassificationComponent, canActivate: [AuthGuard], data: { title: 'ADT Business Classification' } },
  { path: 'nka-customer/business-classification', component: NonCustomerBusinessClassificationComponent, canActivate: [AuthGuard], data: { title: 'ADT Business Classification' } },
  { path: 'arming-disarming-time', component: ArmingDisarmingComponent, canActivate: [AuthGuard], data: { title: 'ADT- Arming and Disarming' } },
  { path: 'access-to-premises', component: NonCustomerAccessToPremisesComponent, canActivate: [AuthGuard], data: { title: 'ADT Business Classification' } },
  { path: 'nka-customer-add-edit', component: NkaCustomerProfileAddEditComponent, canActivate: [AuthGuard], data: { title: 'NKA Customer Profile' } },
  { path: 'suspense-account-add-edit', component: SuspenseAccountAddEditComponent, canActivate: [AuthGuard], data: { title: 'Suspense Account' } },
]
  ;

@NgModule({
  imports: [RouterModule.forChild(customerModuleRoutes)],

})

export class NonCustomerProfileRoutingModule { }
