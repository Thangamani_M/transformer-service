import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import {
  CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { SalesModuleApiSuffixModels, SearchDebterModel, selectLeadCreationStepperParamsState$, ServiceAgreementStepsParams } from '@modules/sales';
import { Store } from '@ngrx/store';
@Component({
  selector: 'app-search-service-modal',
  templateUrl: './search-debter.component.html',
})
export class SearchDebterComponent implements OnInit {
  verifications;
  searchDebterModel = new SearchDebterModel();
  @Output() outputData = new EventEmitter<any>();
  searchServiceForm: FormGroup
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  banks: any;
  serviceAgreementStepsParams: ServiceAgreementStepsParams;

  constructor(private crudService: CrudService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder, private snackBarService: SnackbarService, @Inject(MAT_DIALOG_DATA) public leadId: any,
    private dialog: MatDialog, private rxjsService: RxjsService) {
    this.store.select(selectLeadCreationStepperParamsState$).subscribe(res => {
      this.serviceAgreementStepsParams = new ServiceAgreementStepsParams(res);
    })
  }

  ngOnInit(): void {
    this.createSearchForm();
    this.getBanks();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
  }

  createSearchForm() {
    let searchDebterModel = new SearchDebterModel();
    this.searchServiceForm = this.formBuilder.group({
    });
    Object.keys(searchDebterModel).forEach((key) => {
      this.searchServiceForm.addControl(key, new FormControl(searchDebterModel[key]));
    });
  }

  getBanks() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.SALES_API_BANKS, undefined, false, null).subscribe((response: IApplicationResponse) => {

      this.banks = response.resources;
    })
  }

  search() {
    if (this.searchServiceForm.invalid) {
      return;
    }
    let data = this.searchServiceForm.value;
    if (data) {
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.NON_CUSTOMER_DEBTOR_SEARCH, undefined, false, prepareGetRequestHttpParams(null, null, data), 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {

          if (response.resources.length == 0) {
            this.snackBarService.openSnackbar("No Data Found", ResponseMessageTypes.SUCCESS);
          } else {
            this.dialog.closeAll();
            this.outputData.emit(response.resources);
          }
        }
      });
    } else {
      this.snackBarService.openSnackbar("Atleast One search criteria is required", ResponseMessageTypes.WARNING);
    }
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
