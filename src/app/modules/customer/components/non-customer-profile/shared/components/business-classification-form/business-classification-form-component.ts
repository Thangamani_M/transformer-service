import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { BusinessClassificationModel } from "../../model/business-classification.model";

@Component({
    selector: 'app-shared-business-classification-form',
    templateUrl: './business-classification-form-component.html',
    // styleUrls:['.']
  })

  export class SharedBusinessClassificationComponent implements OnInit {

    categoryList =[];
    subCategoryList =[];
    debatorGroupList = [];
    originList=[];
    installOriginList=[];
    dealTypeList :any;
    loggedUser;
    dealType: string='';
    businessClassificationForm: FormGroup;
    @Output() emitData = new EventEmitter<any>();
    @Input() details;


    constructor(
      private crudService: CrudService,private formBuilder: FormBuilder,
        private store: Store<AppState>, private rxjsService: RxjsService) {
          this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
          });
      }

      ngOnInit(): void {
       this.createBusinessClassificationForm();
        // this.combineLatestNgrxStoreDataOne()
        this.getCategoryList();
        this.getOriginList();
        this.getInstallOriginList();
        this.getDebtorGroupList();
        this.emitDataWhenFormChange();
        this.getSubCategoryList();
      }
      createBusinessClassificationForm(data?: BusinessClassificationModel): void {
        let businessClassificationModel = new BusinessClassificationModel(data);
        this.businessClassificationForm = this.formBuilder.group({});
        Object.keys(businessClassificationModel).forEach((key) => {
          this.businessClassificationForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
            new FormControl(businessClassificationModel[key]));
        });
        this.businessClassificationForm = setRequiredValidator(this.businessClassificationForm, ["categoryId"]);
        this.onCustomerTypeChanges();
      }

      onCustomerTypeChanges(): void {
        // this.businessClassificationForm
        //   .get("customerTypeId")
        //   .valueChanges.subscribe((customerTypeId: string) => {
        //     if (!customerTypeId) return;
        //     this.customerTypeName = this.customerTypes.find(
        //       (s) => s["id"] == customerTypeId
        //     ).displayName;
        //     if (this.customerTypeName === this.commercialCustomerType) {
        //       this.isCommercial = true
        //       this.emitCustomerType.emit(true)
        //       this.customerInformatioanForm.get('companyName').setValidators(Validators.required);
        //       this.customerInformatioanForm.updateValueAndValidity()

        //     } else {
        //       this.emitCustomerType.emit(false)
        //       this.customerInformatioanForm.get('companyName').clearAsyncValidators()
        //       this.customerInformatioanForm.updateValueAndValidity()
        //       this.isCommercial = false;
        //     }
        //   });
      }
      getCategoryList() {
        this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_CATEGORY, null, false, null)
          .subscribe((response: IApplicationResponse) => {
            this.categoryList = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          })
      }
     getSubCategoryList(){
      //https://fidelity-billing-dev.azurewebsites.net/api/ux/sub-categories
      this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_SUBCATEGORY, null, false, null)
          .subscribe((response: IApplicationResponse) => {
            this.subCategoryList = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          })
     }
      getOriginList() {
        this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_ORIGINS, null, false, null)
          .subscribe((response: IApplicationResponse) => {
            this.originList = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          })
      }

      getInstallOriginList() {
        this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_INSTALL_ORIGIN, null, false, null)
          .subscribe((response: IApplicationResponse) => {
            this.installOriginList = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          })
      }

      getDebtorGroupList() {
        this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_DEBTOR_GROUP, null, false, null)
          .subscribe((response: IApplicationResponse) => {
            this.debatorGroupList = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          })
      }

      emitDataWhenFormChange(){
        this.businessClassificationForm.valueChanges.subscribe(data=>{
          this.emitData.emit({type : 'business-classification' , data : this.businessClassificationForm.value});
        })
      }
      onChangeOrigin(event) {
        let originId= this.businessClassificationForm.get('originId').value
        if(originId) {
          this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.ORIGIN, originId, false, null)
          .subscribe((response: IApplicationResponse) => {
            this.dealTypeList = response.resources;
            this.dealType= response.resources.dealType;
            this.rxjsService.setGlobalLoaderProperty(false);
          });
        }

      }
  }
