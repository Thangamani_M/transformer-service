import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {
  loggedInUserData,
  selectStaticEagerLoadingCustomerTypesState$,
  selectStaticEagerLoadingSiteTypesState$,
  selectStaticEagerLoadingTitlesState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, LoggedInUserModel, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { FEATURE_NAME } from '../../enum/index.enum';
import { CustomerInformationModel } from '../../model';
@Component({
  selector: 'app-shared-customer-information',
  templateUrl: './customer-information.component.html',
  // styleUrls:['.']
})

export class SharedCustomerInformationComponent implements OnInit {
  formConfigs = formConfigs;
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  customerInformatioanForm: FormGroup;
  loggedUser: LoggedInUserModel;
  countryCodes = countryCodes;
  isFormSubmitted = false;
  titles = []
  siteTypes = []
  customerTypes = []
  customerTypeName: any
  isCommercial: Boolean = false;
  commercialCustomerType = "Commercial"; // edit here
  residentialCustomerType = "Residential"; // edit here
  @Output() emitData = new EventEmitter<any>();
  @Output() emitCustomerType = new EventEmitter<any>();
  isSiteType: boolean = false;
  feature: string;
  addressList = []
  showAddress  =false;

  constructor(

    private snackbarService: SnackbarService,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.feature = this.activatedRoute.snapshot.queryParams.feature;
  }

  ngOnInit(): void {
    this.createcustomerInformatioanForm();
    this.combineLatestNgrxStoreDataOne()
    this.emitDataWhenFormChange();
    this.setSiteType();
    this.decideToShowAddress()
  }

  getCustomerOfficeAddress() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.NON_CUSTOMER_OFFICE_ADDRESS).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.addressList = response.resources;
      }
    })
  }
  setSiteType() {
    if (this.feature == 'execu-guard-profile')
      this.isSiteType = true;
  }
  decideToShowAddress(){
    if(this.feature == FEATURE_NAME.CAMERA_PROFILE  || this.feature == FEATURE_NAME.HUB_PROFILE ||
      this.feature == FEATURE_NAME.COMMUNITY_PROFILE || this.feature == FEATURE_NAME.WHATSAPP_PROFILE){
        this.showAddress = true
        this.getCustomerOfficeAddress()
      }
  }
  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(selectStaticEagerLoadingSiteTypesState$),
      this.store.select(selectStaticEagerLoadingCustomerTypesState$),
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedUser = new LoggedInUserModel(response[0]);
        this.titles = response[1];
        this.siteTypes = response[2];
        this.customerTypes = response[3];
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }


  createcustomerInformatioanForm(data?: CustomerInformationModel): void {
    let customerInformatioanModel = new CustomerInformationModel(data);
    this.customerInformatioanForm = this.formBuilder.group({});
    Object.keys(customerInformatioanModel).forEach((key) => {
      this.customerInformatioanForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
        new FormControl(customerInformatioanModel[key]));
    });
    this.customerInformatioanForm = setRequiredValidator(this.customerInformatioanForm, ["customerTypeId", "titleId", "firstName", "lastName", "email", "mobile1"]);
    if (this.isSiteType) {
      this.customerInformatioanForm = setRequiredValidator(this.customerInformatioanForm, ["customerTypeId", "titleId", "firstName", "lastName", "email", "mobile1", "siteTypeId"]);
    }else if(this.showAddress){
      this.customerInformatioanForm = setRequiredValidator(this.customerInformatioanForm, ["customerTypeId", "titleId", "firstName", "lastName", "email", "mobile1", "addressId"]);

    }
    this.onCustomerTypeChanges();
  }

  onCustomerTypeChanges(): void {
    this.customerInformatioanForm
      .get("customerTypeId")
      .valueChanges.subscribe((customerTypeId: string) => {
        if (!customerTypeId) return;
        this.customerTypeName = this.customerTypes.find(
          (s) => s["id"] == customerTypeId
        ).displayName;
        if (this.customerTypeName === this.commercialCustomerType) {
          this.isCommercial = true
          this.emitCustomerType.emit(true)
          this.customerInformatioanForm.get('companyName').setValidators(Validators.required);
          this.customerInformatioanForm.updateValueAndValidity()

        } else {
          this.emitCustomerType.emit(false)
          this.customerInformatioanForm.get('companyName').clearAsyncValidators()
          this.customerInformatioanForm.updateValueAndValidity()
          this.isCommercial = false;
        }
      });
  }
  isDuplicate = false;
  onChange(str) {
    this.isDuplicate = false;
    let contact1 = this.customerInformatioanForm.value.mobile1;
    let contact2 = this.customerInformatioanForm.value.mobile2;
    let contact3 = this.customerInformatioanForm.value.premisesNo;
    if (str == 'contact1') {
      if ((contact1 == contact3) || (contact1 == contact2)) {
        this.snackbarService.openSnackbar("Contact number is duplicate", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
      } else {
        this.isDuplicate = false;
      }
    } else if (str == 'contact2') {
      if ((contact2 == contact1) || (contact2 == contact1)) {
        this.isDuplicate = true;
        this.snackbarService.openSnackbar("Contact number is duplicate", ResponseMessageTypes.WARNING);
      } else {
        this.isDuplicate = false;
      }
    } else {
      if ((contact3 == contact1) || (contact3 == contact2)) {
        this.isDuplicate = true;
        this.snackbarService.openSnackbar("Contact number is duplicate", ResponseMessageTypes.WARNING);
      } else {
        this.isDuplicate = false;
      }
    }
  }

  emitDataWhenFormChange() {
    this.customerInformatioanForm.valueChanges.subscribe(data => {
      this.emitData.emit({ type: 'customer', data: this.customerInformatioanForm.value });
    })
  }


}
