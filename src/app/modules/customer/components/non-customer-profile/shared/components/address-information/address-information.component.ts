

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import {
  loggedInUserData,
  selectStaticEagerLoadingCustomerTypesState$,
  selectStaticEagerLoadingSiteTypesState$,
  selectStaticEagerLoadingTitlesState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, destructureAfrigisObjectAddressComponents, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator, validateLatLongFormat } from '@app/shared';
import { LeafLetFullMapViewModalComponent } from '@app/shared/components/leaf-let';
import { AddressModel, NewAddressPopupComponent, SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
import { isUndefined } from 'util';
@Component({
  selector: 'app-shared-address-information',
  templateUrl: './address-information.component.html',
})

export class SharedAddressInformationComponent implements OnInit {
  formConfigs = formConfigs;
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  addressForm: FormGroup;
  loggedUser: LoggedInUserModel;
  countryCodes = countryCodes;
  isFormSubmitted = false;
  titles = []
  siteTypes = []
  customerTypes = []
  customerTypeName: any
  isCommercial: Boolean = false;
  addressList = []

  @Output() emitData = new EventEmitter<any>();
  @Output() emitActions = new EventEmitter<any>();
  @Output() emitAfrigis = new EventEmitter<any>();
  selectedAddressOption = {};
  selectedLocationOption = {};
  shouldShowLocationPinBtn = false;
  latLongObj;

  constructor(
    private dialog: MatDialog,
    private crudService: CrudService,
    private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private rxjsService: RxjsService) {

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreDataOne()
    this.createAddresForm();
    this.onFormControlChanges();


  }

  emitDataWhenFormChange() {
    this.addressForm.valueChanges.subscribe(data => {
      this.emitData.emit({ type: 'address', data: this.addressForm.getRawValue() });
    })
  }
  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(selectStaticEagerLoadingSiteTypesState$),
      this.store.select(selectStaticEagerLoadingCustomerTypesState$),
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedUser = new LoggedInUserModel(response[0]);
        this.titles = response[1];
        this.siteTypes = response[2];
        this.customerTypes = response[3];
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createAddresForm(address?: AddressModel) {
    let addressModel = new AddressModel(address);
    let formControls = {}
    Object.keys(addressModel).forEach((key) => {
      if (
        key === "streetName" ||
        key === "suburbName" ||
        key === "cityName" ||
        key === "provinceName" ||
        key === "postalCode" ||
        key === "estateStreetNo" ||
        key === "estateStreetName" ||
        key === "estateName" ||
        key === "addressConfidentLevel"
      ) {
        formControls[key] = new FormControl({
          value: addressModel[key],
          disabled: true,
        });
      } else {
        formControls[key] = new FormControl(addressModel[key]);
      }
    });
    this.addressForm = this.formBuilder.group(formControls);
    this.addressForm = setRequiredValidator(this.addressForm, ["formatedAddress", "streetNo"])
    this.emitDataWhenFormChange();
  }


  onFormControlChanges(): void {
    this.addressForm.get('isAfrigisSearch').valueChanges.subscribe((isAfrigisSearch: boolean) => {
      this.addressForm.get('formatedAddress').setValue("");
      this.addressList = [];
    });
    this.getAddressListFromAfrigis();
    this.getAddressListFromLatLongSearch();
    this.addressForm
      .get("isAddressComplex")
      .valueChanges.subscribe((isAddressComplex: boolean) => {
        if (isAddressComplex) {
          this.addressForm.get("buildingNo").enable();
          this.addressForm.get("buildingName").enable();
          this.addressForm.get('buildingNo').setValidators([Validators.required]);
          this.addressForm.get('buildingName').setValidators([Validators.required]);
          this.addressForm.get('buildingNo').updateValueAndValidity()
          this.addressForm.get('buildingName').updateValueAndValidity()
          this.emitActions.emit({ type: 'complex', action: 'add' });
        } else {
          this.addressForm.get('buildingNo').clearValidators()
          this.addressForm.get('buildingName').clearValidators()
          this.addressForm.get('buildingNo').updateValueAndValidity()
          this.addressForm.get('buildingName').updateValueAndValidity()
          this.addressForm.get("buildingNo").setValue(null);
          this.addressForm.get("buildingName").setValue(null);
          this.emitActions.emit({ type: 'complex', action: 'remove' });
          //this.addressForm.get("streetNo").setValue(null);
        }
      });

    this.addressForm
      .get("isAddressExtra")
      .valueChanges.subscribe((isAddressExtra: boolean) => {
        if (isAddressExtra) {

          this.addressForm.get("estateStreetNo").enable();
          this.addressForm.get("estateStreetName").enable();
          this.addressForm.get("estateName").enable();
          this.addressForm.get('estateStreetNo').setValidators([Validators.required]);
          this.addressForm.get('estateStreetName').setValidators([Validators.required]);
          this.addressForm.get('estateName').setValidators([Validators.required]);
          this.addressForm.get('estateStreetNo').updateValueAndValidity()
          this.addressForm.get('estateStreetName').updateValueAndValidity()
          this.addressForm.get('estateName').updateValueAndValidity()
          this.addressForm.updateValueAndValidity()
          this.emitActions.emit({ type: 'addressExtra', action: 'add' });
        } else {
          this.addressForm.get('estateStreetNo').clearValidators()
          this.addressForm.get('estateStreetName').clearValidators()
          this.addressForm.get('estateName').clearValidators()

          this.addressForm.get('estateStreetNo').updateValueAndValidity()
          this.addressForm.get('estateStreetName').updateValueAndValidity()
          this.addressForm.get('estateName').updateValueAndValidity()
          this.addressForm.get("estateStreetNo").disable();
          this.addressForm.get("estateStreetName").disable();
          this.addressForm.get("estateName").disable();
          this.addressForm
            .get("estateStreetNo")
            .setValue(null);
          this.addressForm
            .get("estateStreetName")
            .setValue(null);
          this.addressForm.get("estateName").setValue(null);
          this.emitActions.emit({ type: 'addressExtra', action: 'remove' });
        }
      });
  }

  getAddressListFromAfrigis(): void {
    var searchText: string;
    this.addressForm
      .get("formatedAddress")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap((searchKeyword) => {
          if (!searchKeyword) {
            return of();
          }
          if (searchKeyword === "") {
            this.addressForm
              .get("formatedAddress")
              .setErrors({ invalid: false });
            this.addressForm
              .get("formatedAddress")
              .setErrors({ required: true });
          } else if (searchKeyword.length < 3) {
            this.addressForm.get("formatedAddress").setErrors({
              minlength: {
                actualLength: searchKeyword.length,
                requiredLength: 3,
              },
            });
          } else if (isUndefined(this.selectedAddressOption)) {
            this.addressForm
              .get("formatedAddress")
              .setErrors({ invalid: true });
          } else if (
            this.selectedAddressOption["description"] != searchKeyword &&
            this.addressForm.get("isAfrigisSearch").value
          ) {
            //this.addressForm.get('formatedAddress').setErrors({ 'invalid': true });
            this.clearAddressFormGroupValues();
          } else if (
            this.selectedAddressOption["fullAddress"] != searchKeyword &&
            !this.addressForm.get("isAfrigisSearch").value
          ) {
            this.addressForm
              .get("formatedAddress")
              .setErrors({ invalid: true });
            this.clearAddressFormGroupValues();
          }
          searchText = searchKeyword;
          if (!searchText) {
            return this.addressList;
          } else if (typeof searchText === "object") {
            return (this.addressList = []);
          } else {
            return this.filterAddressByKeywordSearch(searchText, "address");
          }
        })
      )
      .subscribe((response: IApplicationResponse) => {
        if (
          response.isSuccess &&
          response.statusCode === 200 &&
          response.resources
        ) {
          this.addressList = response.resources;
          if (isUndefined(this.selectedAddressOption) && searchText !== "") {
            this.addressForm
              .get("formatedAddress")
              .setErrors({ invalid: true });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  clearAddressFormGroupValues(): void {

    this.addressForm.patchValue({
      latitude: null,
      longitude: null,
      suburbName: null,
      cityName: null,
      provinceName: null,
      postalCode: null,
      streetName: null,
      streetNo: null,
      buildingNo: null,
      buildingName: null,
      estateName: null,
      estateStreetName: null,
      estateStreetNo: null,
    });
  }

  filterAddressByKeywordSearch(
    searchtext: string,
    type: string
  ): Observable<IApplicationResponse> {
    if (type == "address") {
      if (searchtext.length < 3) {
        return of();
      }
      return this.crudService.get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS,
        null,
        true,
        prepareRequiredHttpParams({
          searchtext,
          isAfrigisSearch: this.addressForm.get(
            "isAfrigisSearch"
          ).value,
        })
      );
    } else {
      return this.crudService.get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS,
        null,
        true,
        prepareRequiredHttpParams({
          searchtext,
          isAfrigisSearch: this.addressForm.get(
            "isAfrigisSearch"
          ).value,
        }),
        1
      );
    }
  }

  latLongList = []
  onSelectedItemOption(
    isSelected: boolean,
    type: string,
    selectedObject: object
  ): void {
    if (isSelected) {
      if (type == "address") {
        if (selectedObject["seoid"]) {
          this.getAddressFullDetails(selectedObject["seoid"]);
          setTimeout(() => {
            const addressList = this.addressList.filter(
              (bt) => bt["seoid"] === selectedObject["seoid"]
            );
            if (addressList.length > 0) {
              this.selectedAddressOption = addressList[0];
            }
          }, 200);
        } else {

          selectedObject["longitude"] = selectedObject["longitude"]
            ? selectedObject["longitude"]
            : "";
          selectedObject["latitude"] = selectedObject["latitude"]
            ? selectedObject["latitude"]
            : "";
          if (selectedObject["latitude"] && selectedObject["longitude"]) {
            selectedObject[
              "latLong"
            ] = `${selectedObject["latitude"]}, ${selectedObject["longitude"]}`;
          } else {
            selectedObject["latLong"] = "";
          }
          this.emitAfrigis.emit({
            buildingName: selectedObject["buildingName"],
            buildingNo: selectedObject["buildingNo"], streetNo: selectedObject["streetNo"],
          })
          this.addressForm.patchValue(
            {
              seoid: "",
              jsonObject: "",
              latitude: selectedObject["latitude"],
              longitude: selectedObject["longitude"],
              latLong: selectedObject["latLong"],
              suburbName: selectedObject["suburbName"],
              cityName: selectedObject["cityName"],
              provinceName: selectedObject["provinceName"],
              postalCode: selectedObject["postalCode"],
              streetName: selectedObject["streetName"],
              streetNo: selectedObject["streetNo"],
              estateName: selectedObject["estateName"],
              estateStreetName: selectedObject["estateStreetName"],
              estateStreetNo: selectedObject["estateStreetNo"],
              buildingName: selectedObject["buildingName"],
              buildingNo: selectedObject["buildingNo"],
              addressConfidentLevel:
              selectedObject["addressConfidentLevelName"],
              addressConfidentLevelId:
              selectedObject["addressConfidentLevelId"],
            },
            { emitEvent: false, onlySelf: true }
          );
          setTimeout(() => {
            const addressList = this.addressList.filter(
              (bt) => bt["addressId"] === selectedObject["addressId"]
            );
            if (addressList.length > 0) {
              this.selectedAddressOption = addressList[0];
            }
          }, 200);
        }
      } else {
        setTimeout(() => {
          const latLongList = this.latLongList.filter(
            (ll) => ll["seoid"] === selectedObject["seoid"]
          );
          if (latLongList.length > 0) {
            this.selectedLocationOption = latLongList[0];
          }
        }, 200);
      }
    }
  }
  setAtleastOneFieldRequiredError(type: string): void {

    if (this.addressForm.get("isAddressComplex").value) {
      this.addressForm
        .get("buildingNo")
        .setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.addressForm
        .get("buildingName")
        .setErrors({ atleastOneOfTheFieldsIsRequired: true });
    }
    if (this.addressForm.get("isAddressExtra").value) {
      this.addressForm
        .get("estateName")
        .setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.addressForm
        .get("estateStreetNo")
        .setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.addressForm
        .get("estateStreetName")
        .setErrors({ atleastOneOfTheFieldsIsRequired: true });
    }
  }


  getAddressFullDetails(seoid: string): void {
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_ADDRESS_DETAILS,
        undefined,
        false,
        prepareRequiredHttpParams({ seoid })
      )
      .subscribe((response: IApplicationResponse) => {
        if (
          response.isSuccess &&
          response.statusCode === 200 &&
          response.resources
        ) {
          this.addressForm
            .get("jsonObject")
            .setValue(JSON.stringify(response.resources));
          this.addressForm
            .get("addressConfidentLevel")
            .setValue(response.resources.addressConfidenceLevel);
          response.resources.addressDetails.forEach((addressObj) => {
            this.addressForm
              .get("addressConfidentLevelId")
              .setValue(addressObj.confidence);
            this.patchAddressFormGroupValues(
              "address",
              addressObj,
              addressObj["address_components"]
            );
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  patchAddressFormGroupValues(
    type: string,
    addressObj,
    addressComponents: Object[]
  ) {
    addressObj.geometry.location.lat = addressObj.geometry.location.lat
      ? addressObj.geometry.location.lat
      : "";
    addressObj.geometry.location.lng = addressObj.geometry.location.lng
      ? addressObj.geometry.location.lng
      : "";

    if (addressObj.geometry.location.lat && addressObj.geometry.location.lng) {
      addressObj.geometry.location.latLong = `${addressObj.geometry.location.lat}, ${addressObj.geometry.location.lng}`;
    } else {
      addressObj.geometry.location.latLong = "";
    }

    let {
      suburbName,
      cityName,
      provinceName,
      postalCode,
      streetName,
      streetNo,
      buildingNo,
      buildingName,
      estateName,
      estateStreetName,
      estateStreetNo,
    } = destructureAfrigisObjectAddressComponents(addressComponents);
    if (type == "address") {
      this.emitData.emit({ type: 'address', data: this.addressForm.getRawValue() });
      this.addressForm.patchValue(
        {
          latitude: addressObj.geometry.location.lat,
          longitude: addressObj.geometry.location.lng,
          latLong: addressObj.geometry.location.latLong,
          buildingName,
          buildingNo,
          estateName,
          estateStreetName,
          estateStreetNo,
          suburbName,
          cityName,
          provinceName,
          postalCode,
          streetName,
          streetNo,
        },
        { emitEvent: false, onlySelf: true },
      );
      this.emitData.emit({ type: 'address', data: this.addressForm.getRawValue() });
    }
  }


  openAddressPopup(): void {
    const dialogReff = this.dialog.open(NewAddressPopupComponent, {
      width: "750px",
      disableClose: true,
      data: {
        createdUserId: this.loggedUser.userId, modifiedUserId: this.loggedUser.userId, isShowAddressType: false, mobile2: '', officeNo: '', mobile1: '', premisesNo: '',
        isAddressExtra: this.addressForm.get('isAddressExtra').value, isAddressComplex: this.addressForm.get('isAddressComplex').value,
        isAddressType: false, isHideEstateAddres: true
      },
    });

    dialogReff.afterClosed().subscribe((result) => { });
    dialogReff.componentInstance.outputData.subscribe((ele) => {
      if (ele) {
        this.shouldShowLocationPinBtn = ele.isAddressType == false ? true : false;
        this.addressForm
          .get("formatedAddress")
          .setValue(ele.streetName);
        // this.addressForm.get('formatedAddress').setValue(ele.streetName);
        // this.getPriceIncreaseList();
      }
    });
  }

  openFindPinMap(type: string) {
    this.openFullMapViewWithConfig(type);
  }

  openFullMapViewWithConfig(fromUrl) {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(LeafLetFullMapViewModalComponent, {
      width: "100vw",
      maxHeight: "100vh",
      disableClose: true,
      data: {
        fromUrl: 'Two Table Architecture Custom Address',
        boundaryRequestId: null,
        boundaryRequestRefNo: null,
        isDisabled: false,
        boundaryRequestDetails: {},
        boundaries: [],
        latLong: this.latLongObj,
        shouldShowLegend: false
      },
    });
    dialogReff.afterClosed().subscribe((result) => {
      if (result?.hasOwnProperty('latLong')) {
        this.latLongObj = result.latLong;
        this.addressForm
          .get("latLong").setValue(`${result.latLong.lat.toFixed(4)}, ${result.latLong.lng.toFixed(4)}`)
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getAddressListFromLatLongSearch(): void {
    var searchText: string;


    this.addressForm
      .get("latLong")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap((searchKeyword) => {
          if (!searchKeyword || searchKeyword == "-") {
            return of();
          }
          if (searchKeyword === "") {
            this.addressForm
              .get("latLong")
              .setErrors({ invalid: false });
            this.addressForm
              .get("latLong")
              .setErrors({ required: true });
          } else if (
            isUndefined(this.selectedLocationOption) ||
            validateLatLongFormat(searchKeyword)
          ) {
            this.addressForm
              .get("latLong")
              .setErrors({ invalid: true });
          }
          // else if (this.selectedLocationOption['description'] !== searchKeyword) {
          //   this.addressForm.get('latLong').setErrors({ 'invalid': true });
          //   this.clearAddressFormGroupValues();
          // }
          searchText = searchKeyword;
          if (!searchText) {
            return this.addressList;
          } else if (typeof searchText === "object") {
            return (this.addressList = []);
          } else {
            return this.filterAddressByKeywordSearch(searchText, "location");
          }
        })
      )
      .subscribe((response: IApplicationResponse) => {
        if (
          response.isSuccess &&
          response.statusCode === 200 &&
          response.resources
        ) {
          this.addressList = response.resources;
          if (isUndefined(this.selectedLocationOption) && searchText !== "") {
            this.addressForm
              .get("latLong")
              .setErrors({ invalid: true });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

}
