import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {
  ItManagementApiSuffixModels,
  loggedInUserData
} from '@app/modules';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, countryCodes, CrudService, CustomDirectiveConfig, formConfigs, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Feature, UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';
import { FEATURE_NAME } from '../../enum/index.enum';
import { CDMModel, ContactPersonModel, ProfileInformationModel, ProfileManagerModel } from '../../model';
@Component({
  selector: 'app-shared-profile-information',
  templateUrl: './profile-information.component.html',
  styleUrls: ['./profile-information.component.scss']
})

export class SharedProfileInformationComponent implements OnInit {
  formConfigs = formConfigs;
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  profileInformationForm: FormGroup;
  contactPersonForm: FormGroup
  cdmForm: FormGroup
  profileManagerForm: FormGroup
  loggedUser: LoggedInUserModel;
  countryCodes = countryCodes;
  isFormSubmitted = false;
  titles = []
  siteTypes = []
  customerTypes = []
  customerTypeName: any
  isCommercial: Boolean = false;
  commercialCustomerType = "Commercial"; // edit here
  residentialCustomerType = "Residential"; // edit here
  selectedTab = "Hub Name";

  @Output() emitData = new EventEmitter<any>();
  @Output() removeList = new EventEmitter<any>();

  @Input() tabs: any

  // Dropdowns
  divisionList = [];
  mainAreaList = [];
  subAreaList = [];
  suburbsList = []
  rolesList = [];
  employeeList = []
  provinceList = []
  cityList = []

  // lists

  contactList = []
  profileManagerList = []
  cdmList = []
  feature: FEATURE_NAME
  profileManagerApiUrl = "";
  cdmApiUrl = "";


  cdmEmployees = []
  profileManagerEmployees = []


  constructor(
    private activatedRoute: ActivatedRoute,
    private dialogService: DialogService,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.activatedRoute.queryParams.subscribe(param => {
      this.feature = param.feature
    })
  }
  dropdownsAndData = []
  ngOnInit(): void {
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_DIVISIONS),
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_ROLES),
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_PROVINCES,
        undefined, false, prepareRequiredHttpParams({
          countryId: formConfigs.countryId
        })),
    ];
    this.loadDropdownData();
    this.createProfileInformationForm();
    this.createContactPersonForm()
    this.createProfileManagerForm();
    this.createCDMFORM()
    this.combineLatestNgrxStoreDataOne()
    this.emitDataWhenFormChange();
    this.decideCommunity()
  }

  onChangeDivision(value) {
    if (value) {
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        ItManagementApiSuffixModels.UX_MAIN_AREAS_BY_DIVISION, prepareRequiredHttpParams({ divisionId: value })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            if (this.getHubProfile()) {
              this.mainAreaList = getPDropdownData(response.resources);
            } else {
              this.mainAreaList = response.resources;
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }
  loadDropdownData() {
    forkJoin(this.dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.divisionList = resp.resources;
              break;
            case 1:
              this.rolesList = resp.resources;
              break;
            case 2:
              this.provinceList = getPDropdownData(resp.resources);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }


  onChangeRole(event: any, type) {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_USERS, undefined, false, prepareRequiredHttpParams({ roleId: event?.value?.id })).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false);

      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        if (type == 'pm') {
          this.profileManagerEmployees = response.resources
        } else {
          this.cdmEmployees = response.resources;
        }
      }
    })
  }

  getCityList(event) {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_CITIES,
      undefined, false, prepareRequiredHttpParams({
        provinceId: event?.value
      }))
      .subscribe(resp => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
          this.cityList = getPDropdownData(resp.resources);
        }
      });
  }
  getSuburbList(event) {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_SUBURBS,
      undefined, false, prepareRequiredHttpParams({
        cityId: event?.value
      }))
      .subscribe(resp => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
          this.suburbsList = resp.resources;
          let _find = this.suburbsList.find(item => item.id == this.profileInformationForm.get("suburbId").value);
          if (_find) {
            this.profileInformationForm.get("suburbId").setValue(_find)
          }
        }
      });
  }

  onChangeMainArea() {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_SUB_AREA, undefined, false, prepareRequiredHttpParams({
        mainAreaIds: this.profileInformationForm.get('mainAreaId').value
      })).subscribe(response => {
        if (response.isSuccess) {
          this.subAreaList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(loggedInUserData),

    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedUser = new LoggedInUserModel(response[0]);
      });
  }

  createProfileInformationForm(): void {
    let customerInformatioanModel = new ProfileInformationModel();
    this.profileInformationForm = this.formBuilder.group({});
    Object.keys(customerInformatioanModel).forEach((key) => {
      this.profileInformationForm.addControl(key, new FormControl(customerInformatioanModel[key]));
    });
    this.profileInformationForm = setRequiredValidator(this.profileInformationForm, ["communityName", "divisionId", "mainAreaId", "subAreaId", "streetName", "suburbId"]);
    if (this.getHubProfile()) {
      this.profileInformationForm = clearFormControlValidators(this.profileInformationForm, ["subAreaId"]);
    }
  }
  // --- STARTED Contact Person ---

  createContactPersonForm(): void {
    let contactPersonModel = new ContactPersonModel();
    this.contactPersonForm = this.formBuilder.group({});
    Object.keys(contactPersonModel).forEach((key) => {
      this.contactPersonForm.addControl(key, new FormControl(contactPersonModel[key]));
    });
    this.contactPersonForm = setRequiredValidator(this.contactPersonForm, ["contactPersion", "contactNumber", "contactNumberCountryCode"]);
    this.contactPersonForm.get('contactNumberCountryCode').setValue('+27')
  }

  addContact() {
    if (this.contactPersonForm.invalid) return;
    let _isDuplicate = false;
    this.contactList.forEach(item => {
      if (item.contactNumber == this.contactPersonForm.get('contactNumber').value) {
        _isDuplicate = true;
      }
    })
    if (_isDuplicate) {
      this.snackbarService.openSnackbar("Contact Number already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.contactPersonForm.value.contactNumber = this.contactPersonForm.value.contactNumber
      .toString()
      .replace(/\s/g, "");


    this.contactList.push(this.contactPersonForm.value);
    this.emitData.emit({ type: 'profile-contact', data: this.contactPersonForm.value });
    this.contactPersonForm.reset();
    this.contactPersonForm.get('contactNumberCountryCode').setValue('+27')
    this.contactPersonForm.get('contactPersion').setErrors(null)
    this.contactPersonForm.get('contactNumber').setErrors(null)
  }
  removeContact(index) {
    if (this.contactList.length == 1) {
      this.snackbarService.openSnackbar("Atleast one Contact Person is Required", ResponseMessageTypes.WARNING);
      return;
    }

    this.contactList.splice(index, 1);
    this.removeList.emit({ type: 'profile-contact', index });
  }

  // Profile Managers ----
  createProfileManagerForm(): void {
    let profileManagerModel = new ProfileManagerModel();
    this.profileManagerForm = this.formBuilder.group({});
    Object.keys(profileManagerModel).forEach((key) => {
      this.profileManagerForm.addControl(key, new FormControl(profileManagerModel[key]));
    });
    this.profileManagerForm = setRequiredValidator(this.profileManagerForm, ["roleId", "employeeId"]);
  }

  addProfileManager() {

    if (this.profileManagerForm.invalid) return;
    let _isDuplicate = false
    let profileManager = this.profileManagerForm.value
    this.profileManagerList.forEach(item => {
      if (item.roleId == this.profileManagerForm.get('roleId').value.id && item.employeeId == this.profileManagerForm.get('employeeId').value.id) {
        _isDuplicate = true;
      }
    })
    if (_isDuplicate) {
      this.snackbarService.openSnackbar("Profile Manager already exist", ResponseMessageTypes.WARNING);
      return;
    }

    this.profileManagerList.push({
      employeeName: profileManager.employeeId.displayName,
      roleName: profileManager.roleId.displayName,
      ...this.profileManagerForm.value
    });
    this.profileManagerForm.value.roleId = profileManager.roleId.id
    this.profileManagerForm.value.employeeId = profileManager.employeeId.id

    this.emitData.emit({ type: 'profile-manager', data: this.profileManagerForm.value });
    this.profileManagerForm.reset()
    this.profileManagerForm.get('roleId').setErrors(null)
    this.profileManagerForm.get('employeeId').setErrors(null)
  }

  removeProfileManager(index, profile) {
    if (this.profileManagerList.length == 1) {
      this.snackbarService.openSnackbar("Atleast one Profile Manager is Required", ResponseMessageTypes.WARNING);
      return;
    }
    if (profile.communityManagerId) {
      this.deleteData({ communityManagerId: profile.communityManagerId }, index, this.profileManagerApiUrl, 'profile');
    } else {
      this.profileManagerList.splice(index, 1);
      this.removeList.emit({ type: 'profile-manager', index: index });
    }
  }


  // cdm
  createCDMFORM(): void {
    let cdmModel = new CDMModel();
    this.cdmForm = this.formBuilder.group({});
    Object.keys(cdmModel).forEach((key) => {
      this.cdmForm.addControl(key, new FormControl(cdmModel[key]));
    });
    this.cdmForm = setRequiredValidator(this.cdmForm, ["roleId", "employeeId"]);
  }

  addCDM() {
    if (this.cdmForm.invalid) return;
    let _isDuplicate = false
    this.cdmList.forEach(item => {
      if (item.roleId == this.cdmForm.get('roleId').value.id && item.employeeId == this.cdmForm.get('employeeId').value.id) {
        _isDuplicate = true;
      }
    })
    if (_isDuplicate) {
      this.snackbarService.openSnackbar("CDM already exist", ResponseMessageTypes.WARNING);
      return;
    }
    let cdmValue = this.cdmForm.value
    this.cdmList.push({
      employeeName: cdmValue.employeeId.displayName,
      roleName: cdmValue.roleId.displayName,
      ...this.cdmForm.value
    });
    this.cdmForm.value.roleId = cdmValue.roleId.id
    this.cdmForm.value.employeeId = cdmValue.employeeId.id
    this.emitData.emit({ type: 'profile-cdm', data: this.cdmForm.value });
    this.cdmForm.reset();
    this.cdmForm.get('roleId').setErrors(null)
    this.cdmForm.get('employeeId').setErrors(null)
  }

  removeCDM(index, cdm) {
    if (this.cdmList.length == 1) {
      this.snackbarService.openSnackbar("Atleast one Profile Manager is Required", ResponseMessageTypes.WARNING);
      return;
    }

    if (cdm.communityCDMId) {
      this.deleteData({ communityCDMId: cdm.communityCDMId }, index, this.cdmApiUrl, 'cdm')
    } else {
      this.cdmList.splice(index, 1);
      this.removeList.emit({ type: 'profile-cdm', index: index });
    }

  }

  selectTab(tab) {
    this.selectedTab = tab
  }


  emitDataWhenFormChange() {
    this.profileInformationForm.valueChanges.subscribe(data => {
      this.emitData.emit({ type: 'profile-community', data: this.profileInformationForm.value });
    })
  }

  fillData(type, data) {
    if (type == "profile-manager") {
      for (const iterator of data) {
        let _role = this.rolesList.find(v => v.id == iterator.roleId)
        let _employee = this.employeeList.find(v => v.id == iterator.employeeId)
        this.profileManagerList.push({
          ...data, employeeName: _employee.displayName,
          roleName: _role.displayName
        })
      }
    }
    if (type == 'cdm') {
      for (const iterator of data) {
        let _role = this.rolesList.find(v => v.id == iterator.roleId)
        let _employee = this.employeeList.find(v => v.id == iterator.employeeId)
        this.cdmList.push({
          ...data, employeeName: _employee.displayName,
          roleName: _role.displayName,
        })
      }
    }

  }

  getHubProfile() {
    return this.feature == FEATURE_NAME.HUB_PROFILE;
  }


  decideCommunity() {
    this.profileManagerApiUrl = CustomerModuleApiSuffixModels.COMMUNITY_PROFILE_MANAGER
    this.cdmApiUrl = CustomerModuleApiSuffixModels.COMMUNITY_CDM

    switch (this.feature) {
      case FEATURE_NAME.CAMERA_PROFILE:
        this.selectedTab = "Community Name"
        break;
      case FEATURE_NAME.HUB_PROFILE:
        this.selectedTab = "Hub Name"
        break;
      case FEATURE_NAME.WHATSAPP_PROFILE:
        this.selectedTab = "WhatsApp Group"
        break;
      case FEATURE_NAME.COMMUNITY_PROFILE:
        this.selectedTab = "Community Name"
        break;
      default:
        break;
    }
  }

  deleteData(id, index, apiUrl, type) {
    // community-cdm
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        dataObject: id,
        modifiedUserId: this.loggedUser.userId,
        selectAll: false,
        moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        apiSuffixModel: apiUrl,
        method: 'delete'
      },
    });
    ref.onClose.subscribe((result) => {

      if (result) {
        if (type == "cdm") {
          this.cdmList.splice(index, 1);
          this.removeList.emit({ type: 'profile-cdm', index: index });
        } else {
          this.profileManagerList.splice(index, 1);
          this.removeList.emit({ type: 'profile-manager', index: index });
        }

      }
    });
  }


  setSuburbs() {

    this.onChangeDivision(this.profileInformationForm.get("divisionId").value);
    this.getCityList({ value: this.profileInformationForm.get("provinceId").value })
    this.getSuburbList({ value: this.profileInformationForm.get("cityId").value })

  }

}
