
export enum FORMTYPE {
  CUSTOMER_INFORMATION = 'customer',
  ADDRESS_INFORMATION = 'address',
  PROFILE_COMMUNITY = 'profile-community',
  PROFILE_MANAGER = 'profile-manager',
  PROFILE_CONTACT = 'profile-contact',
  PROFILE_CDM= 'profile-cdm',
  CAMERA_INFORMATION='camera-information',
  BUSINESS_CLASSIFICATION = 'business-classification'
}


export enum FEATURE_NAME{
  CAMERA_PROFILE = 'camera-profile',
  REPEATER_PROFILE ='repeater-profile',
  EXECUGUARD='execu-guard',
  HUB_PROFILE='hub-profile',
  TEST_PROFILE='test-profile',
  CASUAL_GUARDING="casual-guarding",
  NKA_CUSTOMER='nka-customer',
  NON_CLIENT='non-client',
  WHATSAPP_PROFILE   = 'whatsapp-profile',
  EXECU_GUARD_PROFILE = 'execu-guard-profile',
  COMMUNITY_PROFILE   = 'community-profile',
  ADT_PROFILE="adt-site",
  SUSPENDED_ACCOUNT='suspended-account'



}
