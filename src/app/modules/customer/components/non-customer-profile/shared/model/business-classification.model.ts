class BusinessClassificationModel {
      constructor(businessClassificationModel?: BusinessClassificationModel) {
        this.categoryId = businessClassificationModel?.categoryId != undefined ? businessClassificationModel?.categoryId : '';
        this.debtorGroupId = businessClassificationModel?.debtorGroupId != undefined ? businessClassificationModel?.debtorGroupId : '';
        this.originId = businessClassificationModel?.originId != undefined ? businessClassificationModel?.originId : '';
        this.installOriginId = businessClassificationModel?.installOriginId != undefined ? businessClassificationModel?.installOriginId : '';
        this.customerClassificationMappingId = businessClassificationModel?.customerClassificationMappingId != undefined ? businessClassificationModel?.customerClassificationMappingId : '';
        this.subCategoryId = businessClassificationModel?.subCategoryId != undefined ? businessClassificationModel?.subCategoryId : '';
        // this.serviceMappingId = businessClassificationModel ? businessClassificationModel.serviceMappingId == undefined ? null : businessClassificationModel.serviceMappingId : '';
        //  this.qty = businessClassificationModel ? businessClassificationModel.qty == undefined ? null : businessClassificationModel.qty : '';
        // this.execuGuardAccountNumber = businessClassificationModel ? businessClassificationModel.execuGuardAccountNumber == undefined ? null : businessClassificationModel.execuGuardAccountNumber : '';
        // this.customerTypeId = businessClassificationModel == undefined ? '' : businessClassificationModel.customerTypeId == undefined ? '' : businessClassificationModel.customerTypeId;
        // this.customerClassificationSubCategoryMappingId = businessClassificationModel ? businessClassificationModel.customerClassificationSubCategoryMappingId == undefined ? null : businessClassificationModel.customerClassificationSubCategoryMappingId : '';
      }
    categoryId: string;
    debtorGroupId: string;
    originId: string;
    installOriginId: string;
    customerClassificationSubCategoryMappingId: string;
    customerClassificationMappingId: string;
    subCategoryId: string;
    serviceMappingId: string;
    qty: string;
    execuGuardAccountNumber: string;
    customerTypeId: string;
  
  }
  
  export { BusinessClassificationModel };
  