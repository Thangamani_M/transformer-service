import { DocTypes, MedicalConditions } from "@modules/sales";


class KeyHolderModel {
  constructor(keyHolderModel?: KeyHolderModel) {
    this.customerKeyHolderId = keyHolderModel == undefined ? '' : keyHolderModel.customerKeyHolderId == undefined ? '' : keyHolderModel.customerKeyHolderId;
    this.keyHolderName = keyHolderModel == undefined ? '' : keyHolderModel.keyHolderName == undefined ? '' : keyHolderModel.keyHolderName;
    this.contactNo = keyHolderModel == undefined ? '' : keyHolderModel.contactNo == undefined ? '' : keyHolderModel.contactNo;
    this.customerId = keyHolderModel == undefined ? '' : keyHolderModel.customerId == undefined ? '' : keyHolderModel.customerId;
    this.password = keyHolderModel == undefined ? '' : keyHolderModel.password == undefined ? '' : keyHolderModel.password;
    this.createdUserId = keyHolderModel == undefined ? '' : keyHolderModel.createdUserId == undefined ? '' : keyHolderModel.createdUserId;
    this.contactNoCountryCode = keyHolderModel == undefined ? '+27' : keyHolderModel.contactNoCountryCode == undefined ? '+27' : keyHolderModel.contactNoCountryCode;
    this.index = keyHolderModel == undefined ? 0 : keyHolderModel.index == undefined ? 0 : keyHolderModel.index;
    this.currentCursorIndex = keyHolderModel == undefined ? 0 : keyHolderModel.currentCursorIndex == undefined ? 0 : keyHolderModel.currentCursorIndex;
    this.isPasswordShow = keyHolderModel == undefined ? false : keyHolderModel.isPasswordShow == undefined ? false : keyHolderModel.isPasswordShow;

  }
  customerKeyHolderId?: string;
  keyHolderName?: string;
  customerId?: string;
  password?: string;
  createdUserId?: string;
  contactNo?: string;
  contactNoCountryCode?: string;
  index: number;
  currentCursorIndex: number;
  isPasswordShow: boolean;
}


class ProfileEmergencyContactModel {
  constructor(emergencyContactModel?: ProfileEmergencyContactModel) {
    this.customerEmergencyContactId = emergencyContactModel == undefined ? '' : emergencyContactModel.customerEmergencyContactId == undefined ? '' : emergencyContactModel.customerEmergencyContactId;
    this.contactPerson = emergencyContactModel == undefined ? '' : emergencyContactModel.contactPerson == undefined ? '' : emergencyContactModel.contactPerson;
    this.contactNo = emergencyContactModel == undefined ? '' : emergencyContactModel.contactNo == undefined ? '' : emergencyContactModel.contactNo;
    this.customerId = emergencyContactModel == undefined ? '' : emergencyContactModel.customerId == undefined ? '' : emergencyContactModel.customerId;
    this.createdUserId = emergencyContactModel == undefined ? '' : emergencyContactModel.createdUserId == undefined ? '' : emergencyContactModel.createdUserId;
    this.contactNoCountryCode = emergencyContactModel == undefined ? '+27' : emergencyContactModel.contactNoCountryCode == undefined ? '+27' : emergencyContactModel.contactNoCountryCode;
    this.index = emergencyContactModel == undefined ? 0 : emergencyContactModel.index == undefined ? 0 : emergencyContactModel.index;
    this.currentCursorIndex = emergencyContactModel == undefined ? 0 : emergencyContactModel.currentCursorIndex == undefined ? 0 : emergencyContactModel.currentCursorIndex;

  }
  customerEmergencyContactId?: string;
  contactPerson?: string;
  customerId?: string;
  createdUserId?: string;
  contactNo?: string;
  contactNoCountryCode?: string;
  index: number;
  currentCursorIndex: number;
}


class PasswordsInstructionsModel  {
  constructor(passwordsInstructionsModel?: PasswordsInstructionsModel) {
      this.contractId = passwordsInstructionsModel == undefined ? '' : passwordsInstructionsModel.contractId == undefined ? '' : passwordsInstructionsModel.contractId;
      this.customerSpecialInstructionId = passwordsInstructionsModel == undefined ? '' : passwordsInstructionsModel.customerSpecialInstructionId == undefined ? '' : passwordsInstructionsModel.customerSpecialInstructionId;
      this.customerSpecialInstructions = passwordsInstructionsModel == undefined ? '' : passwordsInstructionsModel.customerSpecialInstructions == undefined ? '' : passwordsInstructionsModel.customerSpecialInstructions;
      this.distressWord = passwordsInstructionsModel == undefined ? '' : passwordsInstructionsModel.distressWord == undefined ? '' : passwordsInstructionsModel.distressWord;
      this.medical = passwordsInstructionsModel == undefined ? '' : passwordsInstructionsModel.medical == undefined ? '' : passwordsInstructionsModel.medical;
      this.isDogs = passwordsInstructionsModel == undefined ? false : passwordsInstructionsModel.isDogs == undefined ? false : passwordsInstructionsModel.isDogs;
      this.noOfDogs = passwordsInstructionsModel == undefined ? null : passwordsInstructionsModel.noOfDogs == undefined ? null : passwordsInstructionsModel.noOfDogs;
      this.isVAS = passwordsInstructionsModel == undefined ? false : passwordsInstructionsModel.isVAS == undefined ? false : passwordsInstructionsModel.isVAS;
      this.alarmCancellationPassword = passwordsInstructionsModel == undefined ? '' : passwordsInstructionsModel.alarmCancellationPassword == undefined ? '' : passwordsInstructionsModel.alarmCancellationPassword;
      this.dogTypes = passwordsInstructionsModel == undefined ? [] : passwordsInstructionsModel.dogTypes == undefined ? [] : passwordsInstructionsModel.dogTypes;
      this.medicalConditions = passwordsInstructionsModel == undefined ? [] : passwordsInstructionsModel.medicalConditions == undefined ? [] : passwordsInstructionsModel.medicalConditions;
      this.dogDescription = passwordsInstructionsModel == undefined ? '' : passwordsInstructionsModel.dogDescription == undefined ? '' : passwordsInstructionsModel.dogDescription;
  }
  customerSpecialInstructionId: string;
  contractId: string;
  noOfDogs: string;
  customerSpecialInstructions: string;
  distressWord: string;
  medical: string;
  isDogs: boolean;
  isVAS: boolean;
  alarmCancellationPassword: string;
  dogTypes: DocTypes[];
  medicalConditions: MedicalConditions[];
  dogDescription: string;
}

export { KeyHolderModel, ProfileEmergencyContactModel, PasswordsInstructionsModel };

