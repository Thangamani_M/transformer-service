

class CustomerInformationModel {
  constructor(customerInformatioanModel?: CustomerInformationModel) {
    this.customerTypeId = customerInformatioanModel == undefined ? '' : customerInformatioanModel.customerTypeId == undefined ? '' : customerInformatioanModel.customerTypeId;
    this.titleId = customerInformatioanModel == undefined ? '' : customerInformatioanModel.titleId == undefined ? '' : customerInformatioanModel.titleId;
    this.firstName = customerInformatioanModel == undefined ? "" : customerInformatioanModel.firstName == undefined ? "" : customerInformatioanModel.firstName;
    this.lastName = customerInformatioanModel == undefined ? "" : customerInformatioanModel.lastName == undefined ? "" : customerInformatioanModel.lastName;
    this.said = customerInformatioanModel == undefined ? null : customerInformatioanModel.said == undefined ? null : customerInformatioanModel.said;
    this.companyName = customerInformatioanModel == undefined ? "" : customerInformatioanModel.companyName == undefined ? "" : customerInformatioanModel.companyName;
    this.registerNumber = customerInformatioanModel == undefined ? "" : customerInformatioanModel.registerNumber == undefined ? "" : customerInformatioanModel.registerNumber;
    this.mobile1 = customerInformatioanModel == undefined ? "" : customerInformatioanModel.mobile1 == undefined ? "" : customerInformatioanModel.mobile1;
    this.mobile2 = customerInformatioanModel == undefined ? "" : customerInformatioanModel.mobile2 == undefined ? "" : customerInformatioanModel.mobile2;
    this.officeNo = customerInformatioanModel == undefined ? "" : customerInformatioanModel.officeNo == undefined ? "" : customerInformatioanModel.officeNo;
    this.premisesNo = customerInformatioanModel == undefined ? "" : customerInformatioanModel.premisesNo == undefined ? "" : customerInformatioanModel.premisesNo;
    this.mobile1CountryCode = customerInformatioanModel == undefined ? "+27" : customerInformatioanModel.mobile1CountryCode == undefined ? "+27" : customerInformatioanModel.mobile1CountryCode;
    this.mobile2CountryCode = customerInformatioanModel == undefined ? "+27" : customerInformatioanModel.mobile2CountryCode == undefined ? "+27" : customerInformatioanModel.mobile2CountryCode;
    this.officeNoCountryCode = customerInformatioanModel == undefined ? "+27" : customerInformatioanModel.officeNoCountryCode == undefined ? "+27" : customerInformatioanModel.officeNoCountryCode;
    this.premisesNoCountryCode = customerInformatioanModel == undefined ? "+27" : customerInformatioanModel.premisesNoCountryCode == undefined ? "+27" : customerInformatioanModel.premisesNoCountryCode;
    this.email = customerInformatioanModel == undefined ? "" : customerInformatioanModel.email == undefined ? "" : customerInformatioanModel.email;
    this.companyName = customerInformatioanModel == undefined ? "" : customerInformatioanModel.companyName == undefined ? "" : customerInformatioanModel.companyName;
    this.customerId = customerInformatioanModel == undefined ? "" : customerInformatioanModel.customerId == undefined ? "" : customerInformatioanModel.customerId;
    this.customerContactId = customerInformatioanModel == undefined ? "" : customerInformatioanModel.customerContactId == undefined ? "" : customerInformatioanModel.customerContactId;
    this.contactId = customerInformatioanModel == undefined ? "" : customerInformatioanModel.contactId == undefined ? "" : customerInformatioanModel.contactId;
    this.siteTypeId = customerInformatioanModel == undefined ? null : customerInformatioanModel.siteTypeId == undefined ? null : customerInformatioanModel.siteTypeId;
    this.addressId = customerInformatioanModel == undefined ? null : customerInformatioanModel.addressId == undefined ? null : customerInformatioanModel.addressId;
  }
  customerTypeId?: string;
  titleId?: string;
  firstName?: string;
  lastName?: string;
  said?: number;
  companyName?: string;
  registerNumber?: string;
  mobile1?: string;
  mobile2?: string;
  officeNo?: string;
  premisesNo?: string;
  mobile1CountryCode?: string;
  mobile2CountryCode?: string;
  officeNoCountryCode?: string;
  premisesNoCountryCode?: string;
  email?: string;
  customerId?: string;
  customerContactId?: string;
  contactId?: string;
  siteTypeId?: string;
  addressId?: string;
}

class ProfileInformationModel {
  constructor(profileInformatioanModel?: ProfileInformationModel) {
    this.communityId = profileInformatioanModel == undefined ? '' : profileInformatioanModel.communityId == undefined ? '' : profileInformatioanModel.communityId;
    this.communityName = profileInformatioanModel == undefined ? '' : profileInformatioanModel.communityName == undefined ? '' : profileInformatioanModel.communityName;
    this.regionId = profileInformatioanModel == undefined ? '' : profileInformatioanModel.regionId == undefined ? '' : profileInformatioanModel.regionId;
    this.mainAreaId = profileInformatioanModel == undefined ? '' : profileInformatioanModel.mainAreaId == undefined ? '' : profileInformatioanModel.mainAreaId;
    this.subAreaId = profileInformatioanModel == undefined ? '' : profileInformatioanModel.subAreaId == undefined ? '' : profileInformatioanModel.subAreaId;
    this.streetName = profileInformatioanModel == undefined ? '' : profileInformatioanModel.streetName == undefined ? '' : profileInformatioanModel.streetName;
    this.provinceId = profileInformatioanModel == undefined ? null : profileInformatioanModel.provinceId == undefined ? null : profileInformatioanModel.provinceId;
    this.cityId = profileInformatioanModel == undefined ? null : profileInformatioanModel.cityId == undefined ? null : profileInformatioanModel.cityId;
    this.suburbId = profileInformatioanModel == undefined ? null : profileInformatioanModel.suburbId == undefined ? null : profileInformatioanModel.suburbId;
    this.divisionId = profileInformatioanModel == undefined ? null : profileInformatioanModel.divisionId == undefined ? null : profileInformatioanModel.divisionId;
  }

  communityId?: string;
  communityName?: string;
  regionId?: string;
  mainAreaId?: string;
  subAreaId?: string;
  streetName?: string;
  suburbId?: string;
  divisionId?: string;
  provinceId?:string;
  cityId?:string;
}
class ContactPersonModel {
  constructor(contactPersonModel?: ContactPersonModel) {
    this.communityContactPersonId = contactPersonModel == undefined ? '' : contactPersonModel.communityContactPersonId == undefined ? '' : contactPersonModel.communityContactPersonId;
    this.contactPersion = contactPersonModel == undefined ? '' : contactPersonModel.contactPersion == undefined ? '' : contactPersonModel.contactPersion;
    this.contactNumber = contactPersonModel == undefined ? '' : contactPersonModel.contactNumber == undefined ? '' : contactPersonModel.contactNumber;
    this.contactNumberCountryCode = contactPersonModel == undefined ? '+27' : contactPersonModel.contactNumberCountryCode == undefined ? '+27' : contactPersonModel.contactNumberCountryCode;

  }
  communityContactPersonId?: string;
  contactPersion?: string;
  contactNumber?: string;
  contactNumberCountryCode?: string
}

class ProfileManagerModel {
  constructor(profileManagerModel?: ProfileManagerModel) {
    this.communityManagerId = profileManagerModel == undefined ? '' : profileManagerModel.communityManagerId == undefined ? '' : profileManagerModel.communityManagerId;
    this.roleId = profileManagerModel == undefined ? '' : profileManagerModel.roleId == undefined ? '' : profileManagerModel.roleId;
    this.employeeId = profileManagerModel == undefined ? '' : profileManagerModel.employeeId == undefined ? '' : profileManagerModel.employeeId;

  }
  communityManagerId?: string;
  roleId?: string;
  employeeId?: string;
}

class CDMModel {
  constructor(cdmModel?: CDMModel) {
    this.communityCDMId = cdmModel == undefined ? '' : cdmModel.communityCDMId == undefined ? '' : cdmModel.communityCDMId;
    this.roleId = cdmModel == undefined ? '' : cdmModel.roleId == undefined ? '' : cdmModel.roleId;
    this.employeeId = cdmModel == undefined ? '' : cdmModel.employeeId == undefined ? '' : cdmModel.employeeId;

  }
  communityCDMId?: string;
  roleId?: string;
  employeeId?: string;
}


class CameraInformationModel {
  constructor(cameraInformationModel?: CameraInformationModel) {
    this.cameraProfileId = cameraInformationModel == undefined ? '' : cameraInformationModel.cameraProfileId == undefined ? '' : cameraInformationModel.cameraProfileId;
    this.location = cameraInformationModel == undefined ? '' : cameraInformationModel.location == undefined ? '' : cameraInformationModel.location;
    this.latLong = cameraInformationModel == undefined ? '' : cameraInformationModel.latLong == undefined ? '' : cameraInformationModel.latLong;
    this.cameraProfileName = cameraInformationModel == undefined ? '' : cameraInformationModel.cameraProfileName == undefined ? '' : cameraInformationModel.cameraProfileName;

  }
  cameraProfileId?: string;
  location?: string;
  cameraProfileName?: string;
  latLong?: string;
}




class CameraProfileModel {
  constructor(CameraProfileModel?: CameraProfileModel) {
    this.customerInformationDTO = CameraProfileModel ? CameraProfileModel.customerInformationDTO == undefined ? undefined : CameraProfileModel.customerInformationDTO : undefined;
  }
  customerInformationDTO: CustomerInformationModel

}

export { CameraProfileModel, CustomerInformationModel, ProfileInformationModel, ContactPersonModel, ProfileManagerModel, CDMModel, CameraInformationModel };

