class DebterInfoModel {
  constructor(debterInfoModel?: DebterInfoModel) {
      this.leadId = debterInfoModel ? debterInfoModel.leadId == undefined ? null : debterInfoModel.leadId : '';
      this.customerId = debterInfoModel ? debterInfoModel.customerId == undefined ? null : debterInfoModel.customerId : null;
      this.siteTypeId = debterInfoModel ? debterInfoModel.siteTypeId == undefined ? null : debterInfoModel.siteTypeId : null;
      this.debtorTypeId = debterInfoModel ? debterInfoModel.debtorTypeId == undefined ? null : debterInfoModel.debtorTypeId : null;
      this.bdiNumber = debterInfoModel ? debterInfoModel.bdiNumber == undefined ? null : debterInfoModel.bdiNumber : null;
      this.debtorRefNo = debterInfoModel ? debterInfoModel.debtorRefNo == undefined ? null : debterInfoModel.debtorRefNo : null;
      this.titleId = debterInfoModel ? debterInfoModel.titleId == undefined ? '' : debterInfoModel.titleId : '';
      this.firstName = debterInfoModel ? debterInfoModel.firstName == undefined ? null : debterInfoModel.firstName : null;
      this.lastName = debterInfoModel ? debterInfoModel.lastName == undefined ? null : debterInfoModel.lastName : null;
      this.said = debterInfoModel ? debterInfoModel.said == undefined ? null : debterInfoModel.said : null;
      this.email = debterInfoModel ? debterInfoModel.email == undefined ? '' : debterInfoModel.email : '';
      this.companyName = debterInfoModel ? debterInfoModel.companyName == undefined ? '' : debterInfoModel.companyName : '';
      this.companyRegNo = debterInfoModel ? debterInfoModel.companyRegNo == undefined ? '' : debterInfoModel.companyRegNo : '';

      this.companyVATNo = debterInfoModel ? debterInfoModel.companyVATNo == undefined ? '' : debterInfoModel.companyVATNo : '';
      this.addressLine1 = debterInfoModel ? debterInfoModel.addressLine1 == undefined ? '' : debterInfoModel.addressLine1 : '';
      this.addressLine2 = debterInfoModel ? debterInfoModel.addressLine2 == undefined ? '' : debterInfoModel.addressLine2 : '';
      this.addressLine3 = debterInfoModel ? debterInfoModel.addressLine3 == undefined ? '' : debterInfoModel.addressLine3 : '';
      this.addressLine4 = debterInfoModel ? debterInfoModel.addressLine4 == undefined ? '' : debterInfoModel.addressLine4 : '';
      this.mobile1CountryCode = debterInfoModel ? debterInfoModel.mobile1CountryCode == undefined ? '+27' : debterInfoModel.mobile1CountryCode : '+27';
      this.mobile1 = debterInfoModel ? debterInfoModel.mobile1 == undefined ? '' : debterInfoModel.mobile1 : '';
      this.mobile2CountryCode = debterInfoModel ? debterInfoModel.mobile2CountryCode == undefined ? '+27' : debterInfoModel.mobile2CountryCode : '+27';
      this.mobile2 = debterInfoModel ? debterInfoModel.mobile2 == undefined ? '' : debterInfoModel.mobile2 : '';
      this.premisesNoCountryCode = debterInfoModel ? debterInfoModel.premisesNoCountryCode == undefined ? '+27' : debterInfoModel.premisesNoCountryCode : '+27';
      this.premisesNo = debterInfoModel ? debterInfoModel.premisesNo == undefined ? '' : debterInfoModel.premisesNo : '';
      this.officeNoCountryCode = debterInfoModel ? debterInfoModel.officeNoCountryCode == undefined ? '+27' : debterInfoModel.officeNoCountryCode : '+27';
      this.officeNo = debterInfoModel ? debterInfoModel.officeNo == undefined ? '' : debterInfoModel.officeNo : '';

      this.postalCode = debterInfoModel ? debterInfoModel.postalCode == undefined ? '' : debterInfoModel.postalCode : '';
      this.alternateEmail = debterInfoModel ? debterInfoModel.alternateEmail == undefined ? null : debterInfoModel.alternateEmail : null;
      this.debtorId = debterInfoModel ? debterInfoModel.debtorId == undefined ? '' : debterInfoModel.debtorId : '';
      this.debtorAdditionalInfoId = debterInfoModel ? debterInfoModel.debtorAdditionalInfoId == undefined ? '' : debterInfoModel.debtorAdditionalInfoId : '';
      this.billingAddressId = debterInfoModel ? debterInfoModel.billingAddressId == undefined ? '' : debterInfoModel.billingAddressId : '';
      this.debtorContactId = debterInfoModel ? debterInfoModel.debtorContactId == undefined ? '' : debterInfoModel.debtorContactId : '';
      this.contactId = debterInfoModel ? debterInfoModel.contactId == undefined ? '' : debterInfoModel.contactId : '';
      this.createdUserId = debterInfoModel ? debterInfoModel.createdUserId == undefined ? '' : debterInfoModel.createdUserId : '';
      this.modifiedUserId = debterInfoModel ? debterInfoModel.modifiedUserId == undefined ? '' : debterInfoModel.modifiedUserId : '';
      this.isDebtorSameAsCustomer = debterInfoModel ? debterInfoModel.isDebtorSameAsCustomer == undefined ? false : debterInfoModel.isDebtorSameAsCustomer : false;
      this.isExistingDebtor = debterInfoModel ? debterInfoModel.isExistingDebtor == undefined ? false : debterInfoModel.isExistingDebtor : false;
      this.isEmailCommunication = debterInfoModel ? debterInfoModel.isEmailCommunication == undefined ? false : debterInfoModel.isEmailCommunication : false;
      this.isSMSCommunication = debterInfoModel ? debterInfoModel.isSMSCommunication == undefined ? false : debterInfoModel.isSMSCommunication : false;
      this.isPhoneCommunication = debterInfoModel ? debterInfoModel.isPhoneCommunication == undefined ? false : debterInfoModel.isPhoneCommunication : false;
      this.isPostCommunication = debterInfoModel ? debterInfoModel.isPostCommunication == undefined ? false : debterInfoModel.isPostCommunication : false;
      this.isBillingSameAsSiteAddress = debterInfoModel ? debterInfoModel.isBillingSameAsSiteAddress == undefined ? false : debterInfoModel.isBillingSameAsSiteAddress : false;
      this.isLinkDebtor = debterInfoModel ? debterInfoModel.isLinkDebtor == undefined ? false : debterInfoModel.isLinkDebtor : false;

  }
  isDebtorSameAsCustomer: boolean;
  isLinkDebtor: boolean;
  isExistingDebtor: boolean;
  isEmailCommunication: boolean;
  isSMSCommunication: boolean;
  isPhoneCommunication: boolean;
  isPostCommunication: boolean;
  isBillingSameAsSiteAddress: boolean;
  customerId: string;
  leadId: string;
  siteTypeId: string;
  debtorTypeId: string;
  bdiNumber: string;
  debtorRefNo: string;
  titleId: string;
  firstName: string;
  lastName: string;
  said: string;
  email: string;
  modifiedUserId: string;
  companyName: string;
  companyRegNo: string;
  companyVATNo: string;
  addressLine1: string;
  addressLine2: string;
  addressLine3: string;
  addressLine4: string;
  mobile1CountryCode: string;
  mobile1: string;
  mobile2CountryCode: string;
  mobile2: string;
  premisesNoCountryCode: string;
  premisesNo: string;
  officeNo: string;
  officeNoCountryCode: string;
  postalCode: string;
  alternateEmail: string;
  debtorId: string;
  debtorAdditionalInfoId: string;
  billingAddressId: string;
  debtorContactId: string;
  contactId: string;
  createdUserId: string;

}


class DebtorBankingInfoModel {
  constructor(debtorBankingInfoModel?: DebtorBankingInfoModel) {
      this.createdUserId = debtorBankingInfoModel ? debtorBankingInfoModel.createdUserId == undefined ? '' : debtorBankingInfoModel.createdUserId : '';
      this.debtorId = debtorBankingInfoModel ? debtorBankingInfoModel.debtorId == undefined ? '' : debtorBankingInfoModel.debtorId : '';
      this.customerId = debtorBankingInfoModel ? debtorBankingInfoModel.customerId == undefined ? '' : debtorBankingInfoModel.customerId : '';
      this.debtorAdditionalInfoId = debtorBankingInfoModel ? debtorBankingInfoModel.debtorAdditionalInfoId == undefined ? '' : debtorBankingInfoModel.debtorAdditionalInfoId : '';
      this.debtorAccountDetailId = debtorBankingInfoModel ? debtorBankingInfoModel.debtorAccountDetailId == undefined ? null : debtorBankingInfoModel.debtorAccountDetailId : null;
      this.isInvoiceRequired = debtorBankingInfoModel ? debtorBankingInfoModel.isInvoiceRequired == false ? true : debtorBankingInfoModel.isInvoiceRequired : false;
      this.isPrintedStatement = debtorBankingInfoModel ? debtorBankingInfoModel.isPrintedStatement == false ? true : debtorBankingInfoModel.isPrintedStatement : false;
      this.isSendUnsecuredPDFStatement = debtorBankingInfoModel ? debtorBankingInfoModel.isSendUnsecuredPDFStatement == false ? true : debtorBankingInfoModel.isSendUnsecuredPDFStatement : false;
      this.isSMSDebtor = debtorBankingInfoModel ? debtorBankingInfoModel.isSMSDebtor == false ? true : debtorBankingInfoModel.isSMSDebtor : false;
      this.isGroupInvoice = debtorBankingInfoModel ? debtorBankingInfoModel.isGroupInvoice == false ? true : debtorBankingInfoModel.isGroupInvoice : false;
      this.isBankAccount = debtorBankingInfoModel ? debtorBankingInfoModel.isBankAccount == true ? false : debtorBankingInfoModel.isBankAccount : true;
      this.isCardSelected = debtorBankingInfoModel ? debtorBankingInfoModel.isCardSelected == false ? true : debtorBankingInfoModel.isCardSelected : false;
      this.firstName = debtorBankingInfoModel ? debtorBankingInfoModel.firstName == undefined ? '' : debtorBankingInfoModel.firstName : '';
      this.lastName = debtorBankingInfoModel ? debtorBankingInfoModel.lastName == undefined ? '' : debtorBankingInfoModel.lastName : '';
      this.companyName = debtorBankingInfoModel ? debtorBankingInfoModel.companyName == undefined ? '' : debtorBankingInfoModel.companyName : '';
      this.accountNo = debtorBankingInfoModel ? debtorBankingInfoModel.accountNo == undefined ? '' : debtorBankingInfoModel.accountNo : '';
      this.bankBranchId = debtorBankingInfoModel ? debtorBankingInfoModel.bankBranchId == undefined ? null : debtorBankingInfoModel.bankBranchId : null;
      this.accountTypeId = debtorBankingInfoModel ? debtorBankingInfoModel.accountTypeId == undefined ? null : debtorBankingInfoModel.accountTypeId : null;
      this.cardTypeId = debtorBankingInfoModel ? debtorBankingInfoModel.cardTypeId == undefined ? null : debtorBankingInfoModel.cardTypeId : null;
      this.cardNo = debtorBankingInfoModel ? debtorBankingInfoModel.cardNo == undefined ? '' : debtorBankingInfoModel.cardNo : '';
      this.debtorTypeId = debtorBankingInfoModel ? debtorBankingInfoModel.debtorTypeId == undefined ? '' : debtorBankingInfoModel.debtorTypeId : '';
      this.selectedMonth = debtorBankingInfoModel ? debtorBankingInfoModel.selectedMonth == undefined ? '' : debtorBankingInfoModel.selectedMonth : '';
      this.selectedYear = debtorBankingInfoModel ? debtorBankingInfoModel.selectedYear == undefined ? '' : debtorBankingInfoModel.selectedYear : '';
      this.expiryDate = debtorBankingInfoModel ? debtorBankingInfoModel.expiryDate == undefined ? '' : debtorBankingInfoModel.expiryDate : '';
      this.titleId = debtorBankingInfoModel ? debtorBankingInfoModel.titleId == undefined ? null : debtorBankingInfoModel.titleId : null;
      this.bankId = debtorBankingInfoModel ? debtorBankingInfoModel.bankId == undefined ? null : debtorBankingInfoModel.bankId : null;
      this.bankBranchCode = debtorBankingInfoModel ? debtorBankingInfoModel.bankBranchCode == undefined ? '' : debtorBankingInfoModel.bankBranchCode : '';
      this.isExistingDebtor = debtorBankingInfoModel ? debtorBankingInfoModel.isExistingDebtor == false ? true : debtorBankingInfoModel.isExistingDebtor : false;
      this.isOnlineDebtorAccepted = debtorBankingInfoModel ==undefined ? false: debtorBankingInfoModel.isOnlineDebtorAccepted == undefined ? false : debtorBankingInfoModel.isOnlineDebtorAccepted;
      this.paymentMethodId = debtorBankingInfoModel ==undefined ? '': debtorBankingInfoModel.paymentMethodId == undefined ? '' : debtorBankingInfoModel.paymentMethodId;
  }

  leadId?: string;
  createdUserId?: string;
  debtorId?: string;
  customerId?: string;
  debtorAdditionalInfoId?: string;
  debtorAccountDetailId?: string;
  isInvoiceRequired?: boolean;
  isPrintedStatement?: boolean;
  isSendUnsecuredPDFStatement?: boolean;
  isSMSDebtor?: boolean;
  isGroupInvoice?: boolean;
  isBankAccount?: boolean;
  titleId?: string;
  firstName?: string;
  lastName?: string;
  companyName?: string;
  accountNo?: string;
  bankId?: string;
  bankBranchId?: string;
  accountTypeId?: string;
  cardTypeId?: string;
  cardNo?: string;
  expiryDate?: string;
  debtorTypeId?: string;
  selectedMonth?: string;
  selectedYear?: string;
  bankBranchCode?: string;
  isCardSelected?: boolean;
  isExistingDebtor?: boolean;
  isOnlineDebtorAccepted?:boolean;
  paymentMethodId:string;
}


export  {DebterInfoModel, DebtorBankingInfoModel}
