
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SearchDebterComponent, SharedAddressInformationComponent, SharedBusinessClassificationComponent, SharedCustomerInformationComponent, SharedProfileInformationComponent } from './components';
@NgModule({
  declarations: [SharedCustomerInformationComponent,
     SharedProfileInformationComponent,
     SharedAddressInformationComponent,
     SharedBusinessClassificationComponent,
     SearchDebterComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule
  ],
  exports: [SharedCustomerInformationComponent,
    SharedProfileInformationComponent,
    SharedAddressInformationComponent,
    SharedBusinessClassificationComponent],
  entryComponents: [SearchDebterComponent]
})
export class SharedNonCustomerProfileModule { }
