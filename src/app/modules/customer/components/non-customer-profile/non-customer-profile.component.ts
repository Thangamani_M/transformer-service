import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { MatMenuItem } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import {
  CrudService,
  CrudType,
  currentComponentPageBasedPermissionsSelector$,
  CustomDirectiveConfig,
  IApplicationResponse,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams,
  ResponseMessageTypes,
  RxjsService,
  SnackbarService
} from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CustomerModuleApiSuffixModels, CUSTOMER_COMPONENT } from "@modules/customer/shared";
import { loggedInUserData } from "@modules/others/auth.selectors";
import { Store } from "@ngrx/store";
import { DialogService } from "primeng/api";
import { combineLatest } from "rxjs";
import { FEATURE_NAME } from "./shared/enum/index.enum";



@Component({
  selector: "app-non-customer-profile.component",
  templateUrl: "./non-customer-profile.component.html",
  styleUrls: ["./non-customer-profile.component.scss"],
})
export class NonCustomerProfileComponent implements OnInit {
  primengTableConfigProperties: any;
  selectedTabIndex = 0;
  loading = false;
  observableResponse;
  dataList: any;
  totalRecords = 0;

  onRowClick: any;
  onSearchInputChange: any;

  deletConfirm = false;
  statusConfirm = false;
  addConfirm = false;
  showDialogSpinner = false;
  public bradCrum: MatMenuItem[];
  selectedColumns: any[];
  selectedRow: any;
  selectedRows: string[] = [];
  pageLimit: number[] = [10, 25, 50, 75, 100];

  searchKeyword: FormControl;
  searchForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({
    isAnAlphaNumericOnly: true,
  });

  columnFilterForm: FormGroup;
  searchColumns: any;
  row: any = {};
  today: Date;
  first: any = 0;
  filterData: any;
  status: any = [{ label: 'Active', value: 1 }, { label: 'In-Active', value: 2 }]
  selectedFilterDate: any;

  constructor(
    private crudService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private _fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private snackbarService: SnackbarService
  ) {
    this.primengTableConfigProperties = {
      tableCaption: "Non Customer Profiles",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Customer Management', relativeRouterUrl: '' }, { displayName: 'Non Customer Profile', relativeRouterUrl: '' }, { displayName: '', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Camera Profile",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "customerRefNo", header: "Customer ID", width: "150px" },
              { field: "customerName", header: "Customer Name", width: "150px" },
              { field: "communityName", header: "Community Name", width: "150px" },
              { field: "suburbName", header: "Suburb", width: "150px" },
              { field: "mainAreaName", header: "Main Area", width: "150px" },
              { field: "subAreaName", header: "Sub Area", width: "150px" },
              { field: "status", header: "Status", width: "150px", type: 'dropdown', options: this.status },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.NON_CUSTOMER_CAMERA_PROFILE,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            featureName: FEATURE_NAME.CAMERA_PROFILE,
            disabled: true
          },
          {
            caption: "Non-Client",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "customerRefNo", header: "Customer ID", width: "150px" },
              { field: "customerName", header: "Customer Name", width: "150px" },
              { field: "suburbName", header: "Suburb", width: "150px" },
              { field: "mainAreaName", header: "Main Area", width: "150px" },
              { field: "subAreaName", header: "Sub Area", width: "150px" },
              { field: "status", header: "Status", width: "150px", type: 'dropdown', options: this.status },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.NON_CLIENT_PROFILE_CUSTOMER,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            featureName: FEATURE_NAME.NON_CLIENT,
            disabled: true
          },
          {
            caption: "Hub Profile",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "customerRefNo", header: "Customer ID", width: "150px" },
              { field: "customerName", header: "Customer Name", width: "150px" },
              { field: "suburbName", header: "Suburb", width: "150px" },
              { field: "mainAreaName", header: "Main Area", width: "150px", isparagraph: true },
              { field: "subAreaName", header: "Sub Area", width: "150px", isparagraph: true },
              { field: "status", header: "Status", width: "150px", type: 'dropdown', options: this.status },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.HUB_PROFILE_DETAIL_LIST,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            featureName: FEATURE_NAME.HUB_PROFILE,
            disabled: true
          },
          {
            caption: "Repeater Profile",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "customerRefNo", header: "Customer ID", width: "150px" },
              { field: "customerName", header: "Customer Name", width: "150px" },
              { field: "suburbName", header: "Suburb", width: "150px" },
              { field: "mainAreaName", header: "Main Area", width: "150px" },
              { field: "subAreaName", header: "Sub Area", width: "150px" },
              { field: "status", header: "Status", width: "150px", type: 'dropdown', options: this.status },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.REPEATER_PROFILE_LIST,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            featureName: FEATURE_NAME.REPEATER_PROFILE,
            disabled: true
          },
          {
            caption: "WhatsApp Profile",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "customerRefNo", header: "Customer ID", width: "150px" },
              { field: "customerName", header: "Customer Name", width: "150px" },
              { field: "communityName", header: "WhatsApp Group", width: "150px" },
              { field: "suburbName", header: "Suburb", width: "150px" },
              { field: "mainAreaName", header: "Main Area", width: "150px" },
              { field: "subAreaName", header: "Sub Area", width: "150px" },
              { field: "status", header: "Status", width: "150px", type: 'dropdown', options: this.status },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.WHATSAPP_PROFILE,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            featureName: FEATURE_NAME.WHATSAPP_PROFILE,
            disabled: true
          },
          {
            caption: "Execu Gaurd",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "customerRefNo", header: "Customer ID", width: "150px" },
              { field: "customerName", header: "Customer Name", width: "150px" },
              { field: "suburbName", header: "Suburb", width: "150px" },
              { field: "mainAreaName", header: "Main Area", width: "150px" },
              { field: "subAreaName", header: "Sub Area", width: "150px" },
              { field: "execuGuardAccountNumber", header: "ExecuGaurd Account Number", width: "200px" },
              { field: "status", header: "Status", width: "150px", type: 'dropdown', options: this.status },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            featureName: FEATURE_NAME.EXECU_GUARD_PROFILE,
            disabled: true
          },
          {
            caption: "Test Profile",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "customerRefNo", header: "Customer ID", width: "150px" },
              { field: "customerName", header: "Customer Name", width: "150px" },
              { field: "suburbName", header: "Suburb", width: "150px" },
              { field: "mainAreaName", header: "Main Area", width: "150px" },
              { field: "subAreaName", header: "Sub Area", width: "150px" },
              { field: "status", header: "Status", width: "150px", type: 'dropdown', options: this.status },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.TEST_PROFILE,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            featureName: FEATURE_NAME.TEST_PROFILE,
            disabled: true
          },
          {
            caption: "Community Profile",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "customerRefNo", header: "Customer ID", width: "150px" },
              { field: "customerName", header: "Customer Name", width: "150px" },
              { field: "communityName", header: "WhatsApp Group", width: "150px" },
              { field: "suburbName", header: "Suburb", width: "150px" },
              { field: "mainAreaName", header: "Main Area", width: "150px" },
              { field: "subAreaName", header: "Sub Area", width: "150px" },
              { field: "status", header: "Status", width: "150px", type: 'dropdown', options: this.status },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.COMMUNITY_PROFILE,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            featureName: FEATURE_NAME.COMMUNITY_PROFILE,
            disabled: true
          },
          {
            caption: "Casual Guarding",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "customerRefNo", header: "Customer ID", width: "150px" },
              { field: "customerName", header: "Customer Name", width: "150px" },
              { field: "debtorName", header: "Debtor Name", width: "150px" },
              { field: "suburbName", header: "Suburb", width: "150px" },
              { field: "mainAreaName", header: "Main Area", width: "150px" },
              { field: "subAreaName", header: "Sub Area", width: "150px" },
              { field: "status", header: "Status", width: "150px", type: 'dropdown', options: this.status },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.CASUAL_GUARDING_PROFILE,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            featureName: FEATURE_NAME.CASUAL_GUARDING,
            disabled: true
          },
          {
            caption: "ADT Site",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "customerRefNo", header: "Customer ID", width: "150px" },
              { field: "customerName", header: "Customer Name", width: "150px" },
              { field: "debtorName", header: "Debtor Name", width: "150px" },
              // { field: "communityName", header: "WhatsApp Group", width: "150px" },
              { field: "suburbName", header: "Suburb", width: "150px" },
              { field: "mainAreaName", header: "Main Area", width: "150px" },
              { field: "subAreaName", header: "Sub Area", width: "150px" },
              { field: "status", header: "Status", width: "150px", type: 'dropdown', options: this.status },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.ADT_SITE_PROFILE,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            featureName: FEATURE_NAME.ADT_PROFILE,
            disabled: true
          },
          {
            caption: "NKA Customer",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "customerRefNo", header: "Customer ID", width: "150px" },
              { field: "customerName", header: "Customer Name", width: "150px" },
              { field: "debtorName", header: "Debtor Name", width: "150px" },
              // { field: "communityName", header: "WhatsApp Group", width: "150px" },
              { field: "suburbName", header: "Suburb", width: "150px" },
              { field: "mainAreaName", header: "Main Area", width: "150px" },
              { field: "subAreaName", header: "Sub Area", width: "150px" },
              { field: "status", header: "Status", width: "150px", type: 'dropdown', options: this.status },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.NKA_CUSTOMER_PROFILE,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            featureName: FEATURE_NAME.NKA_CUSTOMER,
            disabled: true
          },
          {
            caption: "Suspense Account",
            dataKey: "customerId",
            enableBreadCrumb: true,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "customerRefNo", header: "Customer ID", width: "150px" },
              { field: "customerName", header: "Customer Name", width: "150px" },
              { field: "debtorName", header: "Debtor Name", width: "150px" },
              { field: "suburbName", header: "Suburb", width: "150px" },
              { field: "mainAreaName", header: "Main Area", width: "150px" },
              { field: "subAreaName", header: "Sub Area", width: "150px" },
              { field: "status", header: "Status", width: "150px", type: 'dropdown', options: this.status },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: CustomerModuleApiSuffixModels.SUSPENDED_ACCOUNT,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            featureName: FEATURE_NAME.SUSPENDED_ACCOUNT,
            disabled: true
          },
        ],
      },
    };
    this.status = [
      { label: "Active", value: true },
      { label: "In-Active", value: false },
    ];

    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex =
        Object.keys(params["params"]).length > 0 ? +params["params"]["tab"] : 0;
      this.primengTableConfigProperties.selectedTabIndex =
        this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName =
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[
          this.selectedTabIndex
        ].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object
  ) {
    this.loading = true;
    let _customerModuleApiSuffixModels: CustomerModuleApiSuffixModels;
    _customerModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[
        this.selectedTabIndex
      ].apiSuffixModel;

    this.crudService
      .get(
        ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        _customerModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      )
      .subscribe((data: IApplicationResponse) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.observableResponse = data.resources;
          this.dataList = this.observableResponse;
          this.totalRecords = data.totalCount;
        } else {
          this.observableResponse = null;
          this.dataList = this.observableResponse;
          this.totalRecords = 0;
        }
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CUSTOMER_COMPONENT.NON_CUSTOMERS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj: any = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        this.selectedTabIndex = +prepareDynamicTableTabsFromPermissionsObj?.primengTableConfigProperties?.selectedTabIndex || +prepareDynamicTableTabsFromPermissionsObj?.selectedTabIndex || 0;
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.EDIT, row);
        break;

    }
  }

  openAddEditPage(
    type: CrudType | string,
    row: any
  ): void {

    let feature: FEATURE_NAME = this.primengTableConfigProperties.tableComponentConfigs.tabsList[
      this.selectedTabIndex
    ].featureName;
    switch (feature) {

      case FEATURE_NAME.CAMERA_PROFILE:
        if (type == CrudType.CREATE) {
          this.router.navigate(['/customer/non-customer-profile/camera-profile-add-edit'], { queryParams: { feature: feature }, skipLocationChange: true })
        } else {
          if (row.status == "Active") {
            this.redirectToCustomer(row, feature, 12)
          } else {
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.router.navigate(['/customer/non-customer-profile/camera-profile-add-edit'], { queryParams: { id: row.customerId, feature: feature }, skipLocationChange: true })
          }
        }
        break;
      case FEATURE_NAME.NON_CLIENT:
        if (type == CrudType.CREATE) {
          this.router.navigate(['/customer/non-customer-profile/non-client-profile-add-edit'], { queryParams: { feature: feature }, skipLocationChange: true })
        } else {
          if (row.status != "Active") {
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.router.navigate(['/customer/non-customer-profile/non-client-profile-add-edit'], { queryParams: { id: row.customerId, feature: feature }, skipLocationChange: true })
          } else {
            this.redirectToCustomer(row, feature)
          }
        }
        break;
      case FEATURE_NAME.HUB_PROFILE:
        if (type == CrudType.CREATE) {
          this.router.navigate(['/customer/non-customer-profile/hub-profile-add-edit'], { queryParams: { feature: feature }, skipLocationChange: true })
        } else {
          if (row.status == "Active") {
            this.redirectToCustomer(row, feature, 12)
          } else {
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.router.navigate(['/customer/non-customer-profile/hub-profile-add-edit'], { queryParams: { id: row.customerId, feature: feature }, skipLocationChange: true })
          }
        }
        break;
      case FEATURE_NAME.REPEATER_PROFILE:
        if (type == CrudType.CREATE) {
          this.router.navigate(['/customer/non-customer-profile/repeater-profile-add-edit'], { queryParams: { feature: feature }, skipLocationChange: true })
        } else {
          if (row.status == "Active") {
            this.redirectToCustomer(row, feature, 12)
          } else {
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.router.navigate(['/customer/non-customer-profile/repeater-profile-add-edit'], { queryParams: { id: row.customerId, feature: feature }, skipLocationChange: true })
          }
        }
        break;
      case FEATURE_NAME.WHATSAPP_PROFILE:
        if (type == CrudType.CREATE) {
          this.router.navigate(['/customer/non-customer-profile/whatsapp-profile-add-edit'], { queryParams: { feature: feature }, skipLocationChange: true })
        } else {
          this.redirectToCustomer(row, feature)
        }
        break;
      case FEATURE_NAME.COMMUNITY_PROFILE:
        if (type == CrudType.CREATE) {
          this.router.navigate(['/customer/non-customer-profile/community-profile-add-edit'], { queryParams: { feature: feature }, skipLocationChange: true })
        } else {
          this.redirectToCustomer(row, feature, 12)
        }
        break;
      case FEATURE_NAME.EXECU_GUARD_PROFILE:
        if (type == CrudType.CREATE) {
          this.router.navigate(['/customer/non-customer-profile/execu-guard-profile-add-edit'], { queryParams: { feature: feature }, skipLocationChange: true })
        } else {
          if (row.status == "Active") {
            this.redirectToCustomer(row, feature, 12)
          } else {
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.router.navigate(['/customer/non-customer-profile/execu-guard-profile-add-edit'], { queryParams: { id: row.customerId, feature: feature }, skipLocationChange: true })
          }
        }
        break;
      case FEATURE_NAME.TEST_PROFILE:
        if (type == CrudType.CREATE) {
          this.router.navigate(['/customer/non-customer-profile/test-profile-add-edit'], { queryParams: { feature: feature }, skipLocationChange: true })
        } else {
          this.redirectToCustomer(row, feature)
        }
        break;
      case FEATURE_NAME.CASUAL_GUARDING:
        if (type == CrudType.CREATE) {
          this.router.navigate(['/customer/non-customer-profile/casual-guarding-add-edit'], { queryParams: { feature: feature }, skipLocationChange: true })
        } else {
          if (row.status == "Active") {
            this.redirectToCustomer(row, feature)
          } else {
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.router.navigate(['/customer/non-customer-profile/casual-guarding-add-edit'], { queryParams: { id: row.customerId, feature: feature }, skipLocationChange: true })
          }
        }
        break;
      case FEATURE_NAME.ADT_PROFILE:
        if (type == CrudType.CREATE) {
          this.router.navigate(['/customer/non-customer-profile/adt-site-add-edit'], { queryParams: { feature: feature }, skipLocationChange: true })
        } else {
          if (row.status == "Active") {
            this.redirectToCustomer(row, feature)
          } else {
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.router.navigate(['/customer/non-customer-profile/adt-site-add-edit'], { queryParams: { id: row.customerId, feature: feature }, skipLocationChange: true })
          }
        }
        break;
      case FEATURE_NAME.NKA_CUSTOMER:
        if (type == CrudType.CREATE) {
          this.router.navigate(['/customer/non-customer-profile/nka-customer-add-edit'], { queryParams: { feature: feature }, skipLocationChange: true })
        } else {
          if (row.status == "Active") {
            this.redirectToCustomer(row, feature)
          } else {
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.router.navigate(['/customer/non-customer-profile/nka-customer-add-edit'], { queryParams: { id: row.customerId, feature: feature }, skipLocationChange: true })
          }
        }
        break;
      case FEATURE_NAME.SUSPENDED_ACCOUNT:
        if (type == CrudType.CREATE) {
          this.router.navigate(['/customer/non-customer-profile/suspense-account-add-edit'], { queryParams: { feature: feature }, skipLocationChange: true })
        } else {
          if (row.status == "Active") {
            this.redirectToCustomer(row, feature)
          } else {
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            this.router.navigate(['/customer/non-customer-profile/suspense-account-add-edit'], { queryParams: { id: row.customerId, feature: feature }, skipLocationChange: true })
          }
        }
        break;
      default:
        break;
    }
  }

  redirectToCustomer(row, feature, index = 0) {
    this.rxjsService.setViewCustomerData({
      customerId: row.customerId,
      addressId: row.customerAddressId,
      feature_name: feature,
      featureIndex: this.selectedTabIndex,
      customerTab: index,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  tabClick(e) {
    this.selectedTabIndex = e?.index;
    this.router.navigate(['/customer', 'non-customer-profile'], { queryParams: { tab: this.selectedTabIndex } });
    this.dataList = [];
    let queryParams = {};
    queryParams['tab'] = this.selectedTabIndex;
    this.onAfterTabChange(queryParams);
    this.getRequiredListData();
  }

  onAfterTabChange(queryParams) {
    if (this.selectedTabIndex != 0) {
      this.router.navigate([`../`], { relativeTo: this.activatedRoute, queryParams: queryParams });
    }
  }

}
