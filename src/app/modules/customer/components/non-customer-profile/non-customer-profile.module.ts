import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ServiceSalesInstallationAgreementModule } from '@modules/sales';
import { NonCustomerAccessToPremisesComponent } from './components/access-to-premises/access-to-premises.component';
import { AddServicesModule } from './components/add-services/add-services-module';
import { AdtSiteProfileAddEditComponent } from './components/adt-site-profile/adt-site-profile-add-edit.component';
import { ArmingDisarmingComponent } from './components/arming-disarming/arming-disarming.component';
import { BusinessClassificationComponent } from './components/business-classification/business-classification-component';
import { CameraProfileAddEditComponent } from './components/camera-profiles/camera-profile-add-edit.component';
import { CasualGuardingProfileAddEditComponent } from './components/casual-guarding/casual-guarding-add-edit.component';
import { CommunityProfileAddEditComponent } from './components/community-profile/community-profile-add-edit.component';
import { DebtorBankingInformationComponent } from './components/debtor-banking-information/debtor-banking-information.component';
import { DebtorInformationComponent } from './components/debtor-information/debtor-information.component';
import { ExecuGuardProfileAddEditComponent } from './components/execu-guard-profile/execu-guard-profile-component';
import { HubProfileAddEditComponent } from './components/hub-profiles/hub-profile-add-edit.component';
import { KeyholderDetailsComponent } from './components/key-holder-details/key-holder-details.component';
import { NkaCustomerProfileAddEditComponent } from './components/nka-customer-profile/nka-customer-profile-add-edit.component';
import { NonClientProfileAddEditComponent } from './components/non-client/non-client-add-edit.component';
import { NonCustomerBusinessClassificationComponent } from './components/non-customer-business-classification/non-customer-business-classification.component';
import { PasswordInstructionsComponent } from './components/password-instructions/password-instructions.component';
import { RepeaterProfileAddEditComponent } from './components/repeater-profile/repeater-profile-add-edit.component';
import { SuspenseAccountAddEditComponent } from './components/suspense-account/suspense-account-add-edit.component';
import { TestProfileAddEditComponent } from './components/test-profile/test-profile.component';
import { WhatsappProfileAddEditComponent } from './components/whatsapp-profiles/whatsapp-profile-add-edit.component';
import { NonCustomerProfileRoutingModule } from './non-customer-profile-routing.module';
import { NonCustomerProfileComponent } from './non-customer-profile.component';
import { SharedNonCustomerProfileModule } from './shared/shared-non-customer-profile.module';
@NgModule({
  declarations: [NonCustomerProfileComponent, CameraProfileAddEditComponent,
    DebtorInformationComponent,
    DebtorBankingInformationComponent,NonClientProfileAddEditComponent,HubProfileAddEditComponent,KeyholderDetailsComponent,
    RepeaterProfileAddEditComponent,PasswordInstructionsComponent,WhatsappProfileAddEditComponent,ExecuGuardProfileAddEditComponent,BusinessClassificationComponent,
    TestProfileAddEditComponent,CommunityProfileAddEditComponent,CasualGuardingProfileAddEditComponent,AdtSiteProfileAddEditComponent,NonCustomerBusinessClassificationComponent,
    ArmingDisarmingComponent,NonCustomerAccessToPremisesComponent,NkaCustomerProfileAddEditComponent,SuspenseAccountAddEditComponent],
  imports: [
    CommonModule,
    MaterialModule,
    NonCustomerProfileRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule,
    SharedNonCustomerProfileModule,
    AddServicesModule,
  ServiceSalesInstallationAgreementModule
  ],
})
export class NonCustomerProfileModule { }
