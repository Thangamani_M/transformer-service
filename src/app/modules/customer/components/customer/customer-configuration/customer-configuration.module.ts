import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDialogRef, MatSelectModule, MAT_DIALOG_DATA } from "@angular/material";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { NgxPrintModule } from "ngx-print";
import { SignaturePadModule } from "ngx-signaturepad";
import { AutoCompleteModule } from "primeng/autocomplete";
import { CustomerConfigurationRoutingModule } from "./customer-configuration-routing.module";
import { CustomerConfigurationComponent } from "./customer-configuration.component";



@NgModule({
  declarations: [CustomerConfigurationComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatSelectModule,
    AutoCompleteModule,
    NgxPrintModule,
    SignaturePadModule,
    CustomerConfigurationRoutingModule
  ],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }]
})
export class CustomerConfigurationModule { }