import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CustomerConfigurationComponent } from "./customer-configuration.component";


const custConfig: Routes = [
    {
        path: '', component: CustomerConfigurationComponent, data: { title: 'Customers configuration' }
    },
];

@NgModule({
  imports: [RouterModule.forChild(custConfig)],
  
})

export class CustomerConfigurationRoutingModule { }