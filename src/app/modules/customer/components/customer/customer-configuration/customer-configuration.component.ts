import { Component, OnInit } from '@angular/core';
import { RxjsService } from '@app/shared';
import { DialogService } from 'primeng/api';

@Component({
  selector: 'app-customer-configuration',
  templateUrl: './customer-configuration.component.html',
  providers: [DialogService]
})
export class CustomerConfigurationComponent implements OnInit {

  constructor(

    private rxjsService: RxjsService,

  ) {
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
  }

}
