import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TicketGroupListComponent } from './ticket-group-list.component';
import { TicketGroupAddEditComponent } from './ticket-group-add-edit.component';
import { TicketGroupViewComponent } from './ticket-group-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


const ticketGroup: Routes = [
    {
        path: '', component: TicketGroupListComponent, canActivate:[AuthGuard], data: { title: 'Action Group' }
    },
    {
        path: 'view', component: TicketGroupViewComponent, canActivate:[AuthGuard], data: { title: 'View Action Group' }
    },
    {
        path: 'add-edit', component: TicketGroupAddEditComponent, canActivate:[AuthGuard], data: { title: 'Action Group Add Edit' }
    },
];

@NgModule({
  imports: [RouterModule.forChild(ticketGroup)],
  
})

export class TicketGroupRoutingModule { }