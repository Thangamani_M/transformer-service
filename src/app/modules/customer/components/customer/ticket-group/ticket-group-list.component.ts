import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-ticket-group-list',
  templateUrl: './ticket-group-list.component.html',
  styleUrls: ['./ticket-group.component.scss'],
  providers: [DialogService]
})

export class TicketGroupListComponent implements OnInit {
  observableResponse;
  selectedTabIndex = 0;
  loggedInUserData: LoggedInUserModel;
  status: any = [];
  dataList: any;
  totalRecords: any;
  selectedColumns: any[];
  selectedRows: string[] = [];
  selectedRow: any;
  pageSize: number = 10;
  loading = false;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  primengTableConfigProperties: any = {
    tableCaption: "Action Group",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Customer Management', relativeRouterUrl: '/customer/dashboard' }, { displayName: 'Action Group List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Action Group',
          dataKey: 'ticketGroupId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'ticketGroupName', header: 'Group Name' },
          { field: 'departmentCode', header: 'Department Code' },
          { field: 'description', header: 'Description' },
          { field: 'createdDate', header: 'Created On' },
          { field: 'isActive', header: 'Status' }
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.TICKET_GROUP,
          moduleName: ModulesBasedApiSuffix.SALES,
        }]
    }
  }

  constructor(private crudService: CrudService,
    private rxjsService: RxjsService,
    public dialogService: DialogService,
    private store: Store<AppState>,
    private router: Router) {
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ];
  }

  ngOnInit(): void {
    this.getTicketGroups();
    this.combineLatestNgrxStoreData();
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    // [breadCrumbItem['queryParams']['key']]: breadCrumbItem['queryParams']['value']
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.onCRUDRequested(CrudType.GET, row);
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getTicketGroups(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.TICKET_GROUP,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }


  onCRUDRequested(type: CrudType | string, row?: object): void {

    let otherParams = {};
    if (CrudType.GET === type && Object.keys(row).length > 0) {
      // logic for split columns and its values to key value pair

      Object.keys(row['searchColumns']).forEach((key) => {
        otherParams[key] = row['searchColumns'][key]['value'];
      });
      otherParams['sortOrder'] = row['sortOrder'];
      if (row['sortOrderColumn']) {
        otherParams['sortOrderColumn'] = row['sortOrderColumn'];
      }
    }
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getTicketGroups(
              row["pageIndex"], row["pageSize"], otherParams);
            break;
        }
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
    }
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }

    });
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigateByUrl("/customer/ticket-groups/add-edit");
        break;
      case CrudType.EDIT:
        this.router.navigate(["/customer/ticket-groups/view"], { queryParams: { id: editableObject['ticketGroupId'] } });
        break;
    }
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  onTabChange(event) { }
}