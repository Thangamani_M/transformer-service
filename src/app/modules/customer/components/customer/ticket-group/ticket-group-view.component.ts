import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { CrudService, RxjsService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';

@Component({
    selector: 'app-ticket-group-view',
    templateUrl: './ticket-group-view.component.html',
    styleUrls: ['./ticket-group.component.scss']
})
export class TicketGroupViewComponent implements OnInit {
    ticketGroupId: string;
    ticketGroupDetail: any = {};

    constructor(private router: Router, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
        private crudService: CrudService) {
        this.ticketGroupId = this.activatedRoute.snapshot.queryParams.id;
    }
    ngOnInit(): void {
        this.crudService.get(ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.TICKET_GROUP, this.ticketGroupId, false, null).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                    this.ticketGroupDetail = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    onEditButtonClicked(): void {
        this.router.navigate(['customer/ticket-groups/add-edit'], { queryParams: { id: this.ticketGroupId } })
    }
}