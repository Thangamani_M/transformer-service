import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { CrudService, SnackbarService, RxjsService, HttpCancelService } from '@app/shared/services';
import { CustomDirectiveConfig, setRequiredValidator, ModulesBasedApiSuffix, IApplicationResponse } from '@app/shared';
import { SalesModuleApiSuffixModels, TicketGroupAddEditModel } from '@app/modules';
import { Observable, forkJoin } from 'rxjs';

@Component({
  selector: 'app-ticket-group-add-edit',
  templateUrl: './ticket-group-add-edit.component.html',
  styleUrls: ['./ticket-group.component.scss']
})

export class TicketGroupAddEditComponent implements OnInit {
  ticketGroupForm: FormGroup;
  ticketGroupId: string;
  btnName: string;
  createOrUpdateCaption: string;
  isButtonDisabled: boolean = false;
  isAlphaSpecialCharterOnlyRestrictCommetntSymbol = new CustomDirectiveConfig({ isAlphaSpecialCharterOnlyRestrictCommetntSymbol: true });

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private crudService: CrudService,
    private httpCancelService: HttpCancelService, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private router: Router) {
    this.ticketGroupId = this.activatedRoute.snapshot.queryParams.id;
    this.createOrUpdateCaption = this.ticketGroupId ? "Update" : "Create";
    this.btnName = this.ticketGroupId ? "Update" : "Save";
  }

  ngOnInit(): void {
    this.createAddEditForm();

    if (this.ticketGroupId) {
      this.getTicketGroupById().subscribe((response: IApplicationResponse) => {
        let ticketGroupAddEditModel = new TicketGroupAddEditModel(response.resources);
        this.ticketGroupForm.setValue(ticketGroupAddEditModel);
        this.ticketGroupForm.patchValue({
          isActive: ticketGroupAddEditModel.isActive.toString()
        })
      })
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createAddEditForm(): void {
    let ticketGroupAddEditModel = new TicketGroupAddEditModel();
    this.ticketGroupForm = this.formBuilder.group({
      //ticketGroupName: ['',[Validators.required]],
      isActive: ['true', [Validators.required]]
    });
    // create doaDiscount form controls dynamically from model class    
    Object.keys(ticketGroupAddEditModel).forEach((key) => {
      this.ticketGroupForm.addControl(key, new FormControl(ticketGroupAddEditModel[key]));
    });
    this.ticketGroupForm = setRequiredValidator(this.ticketGroupForm,
      ["ticketGroupName"]);
  }


  getTicketGroupById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.TICKET_GROUP,
      this.ticketGroupId
    );
  }

  onSubmit(): void {
    if (this.ticketGroupForm.invalid) {
      return;
    }
    this.isButtonDisabled = true;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.ticketGroupId ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKET_GROUP, this.ticketGroupForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKET_GROUP, this.ticketGroupForm.value)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/customer/ticket-groups');
      }
      else
        this.isButtonDisabled = false;
    })
  }

}