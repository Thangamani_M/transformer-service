
import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ConfirmDialogModel, ConfirmDialogPopupComponent } from '@app/shared';
import { EnableDisable } from '@app/shared/models';
import { CrudService, RxjsService } from '@app/shared/services';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { IndustryType } from '@modules/customer/models';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared/utils';
import { IndustryTypeAddEditComponent } from './industry-type-add-edit.component';



@Component({
  selector: 'app-industrytype-list',
  templateUrl: './industry-type-list.component.html'
})
export class IndustryTypeListComponent implements OnInit {
  displayedColumns: string[] = ['select', 'industryTypeName', 'description', 'createdDate', 'isActive'];
  dataSource = new MatTableDataSource<IndustryType>();
  selection = new SelectionModel<IndustryType>(true, []);
  checkedItemBrandIds: string[] = [];
  enableDisable = new EnableDisable();
  applicationResponse: IApplicationResponse;
  industryType: IndustryType[];
  errorMessage: string;
  limit: number = 25;
  skip: number = 0;
  totalLength: number = 0;
  pageIndex: number = 0;
  pageLimit: number[] = [25, 50, 75, 100]
  ids: string[] = [];

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private httpService: CrudService,
    private dialog: MatDialog,

    private rxjsService: RxjsService) { }

  ngOnInit(): void {
    this.getIndustryTypes();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  changePage(event) {
    this.pageIndex = event.pageIndex;
    this.limit = event.pageSize;
    this.getIndustryTypes();
  }

  private getIndustryTypes() {
    this.httpService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.INDUSTRYTYPE,
      undefined,
      false, prepareGetRequestHttpParams(this.pageIndex.toString(), this.limit.toString())
    ).subscribe(resp => {
      this.applicationResponse = resp;
      this.industryType = this.applicationResponse.resources;
      this.dataSource.data = this.industryType;
      this.totalLength = this.applicationResponse.totalCount;
    });
  }



  /**To check Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  openAddDialog(industryTypeId): void {

    var industryType = new IndustryType();
    if (industryTypeId) {
      this.httpService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.INDUSTRYTYPE, industryTypeId).subscribe(
        {
          next: response => {
            this.applicationResponse = response,
              industryType = this.applicationResponse.resources;
            this.OpenDialogWindow(industryType);
          }
        });
    }
    else {
      this.OpenDialogWindow(industryType);
    }
  }

  private OpenDialogWindow(industryType: IndustryType) {
    const dialogReff = this.dialog.open(IndustryTypeAddEditComponent, { width: '450px', data: industryType });
    dialogReff.afterClosed().subscribe(result => {
      this.selection.clear();
      this.getIndustryTypes();
    });
  }
  enableDisableSelected(id, status: any) {
    this.enableDisable.ids = id;
    this.enableDisable.isActive = status.currentTarget.checked;
    this.httpService
      .enableDisable(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.INDUSTRYTYPE, this.enableDisable).subscribe({
        next: response => { //this.toastr.responseMessage(response);
        },
        error: err => this.errorMessage = err,
      });
  }

  removeSelected() {
    this.selection.selected.forEach(item => {
      this.ids.push(item.industryCategoryId);
    });

    if (this.ids.length > 0) {
      const message = `Are you sure you want to delete this?`;

      const dialogData = new ConfirmDialogModel("Confirm Action", message);

      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData
      });

      dialogRef.afterClosed().subscribe(dialogResult => {
        if (dialogResult) {
          //convert selected id to array
          this.deleteIndustryType();
        }
      });
    }
    else {
      this.applicationResponse.statusCode = 204;
      this.applicationResponse.message = "Please select industry type";
      //this.toastr.responseMessage(this.applicationResponse);
    }

  }

  deleteIndustryType(): void {
    this.httpService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.INDUSTRYTYPE, this.ids.join(','))
      .subscribe({
        next: response => {
          //this.toastr.responseMessage(response);
          this.ids = [];
          //clear check box
          this.selection = new SelectionModel<IndustryType>(true, []);
          this.getIndustryTypes();
        },
        error: err => this.errorMessage = err
      });
  }

}
