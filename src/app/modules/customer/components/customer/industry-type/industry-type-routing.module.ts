import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IndustryTypeListComponent } from './industry-type-list.component';

const industry: Routes = [
    {
        path: '', component: IndustryTypeListComponent, data: { title: 'Industry Type List' }
    },
];

@NgModule({
  imports: [RouterModule.forChild(industry)],
  
})

export class IndustryTypeRoutingModule { }