import { Component, OnInit, Input, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';

import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';
import {RxjsService,CrudService,AlertService} from '@app/shared/services';
import { IndustryType } from '@app/modules/customer/models';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import {CustomerModuleApiSuffixModels} from '@modules/customer/shared';


@Component({
  selector: 'app-industry-type-add-edit',
  templateUrl: './industry-type-add-edit.component.html'
})
export class IndustryTypeAddEditComponent implements OnInit {

  @Input() id: string;
  industryTypeAddEditForm: FormGroup;
  applicationResponse: IApplicationResponse;
  errorMessage: string;
  btnName: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data:IndustryType,

    private dialog: MatDialog,
    private httpServices: CrudService,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private rxjsService: RxjsService) { }

  ngOnInit(): void {
    if (this.data.industryCategoryName) {
      this.btnName = 'Update';
    } else {
      this.btnName = 'Create';
    }
    this.industryTypeAddEditForm = this.formBuilder.group({
      industryCategoryName: this.data.industryCategoryName || '',
      description: this.data.description || ''
    });

  }

  save(): void {
    const industryType = { ...this.data, ...this.industryTypeAddEditForm.value }
    if (industryType.industryCategoryId != null) {
      this.httpServices.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.INDUSTRYTYPE, industryType)
        .subscribe({
          next: response => {
            this.onSaveComplete(response);
          },
          error: err => this.errorMessage = err
        });

    }
    else {

      this.httpServices.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.INDUSTRYTYPE, industryType)
        .subscribe({
          next: response => {
            this.onSaveComplete(response);
          },
          error: err => this.errorMessage = err
        });


    }
  }

  onSaveComplete(response): void {
    if (response.statusCode == 200) {
      this.dialog.closeAll();
      //this.toastr.responseMessage(response);
      // Reset the form to clear the flags
      this.industryTypeAddEditForm.reset();
    } else {
      //Display Alert message
      this.alertService.processAlert(response);
    }
  }

}
