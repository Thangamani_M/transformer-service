import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDialogRef, MatSelectModule, MAT_DIALOG_DATA } from "@angular/material";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { NgxPrintModule } from "ngx-print";
import { SignaturePadModule } from "ngx-signaturepad";
import { AutoCompleteModule } from "primeng/autocomplete";
import { IndustryTypeAddEditComponent } from "./industry-type-add-edit.component";
import { IndustryTypeListComponent } from "./industry-type-list.component";
import { IndustryTypeRoutingModule } from "./industry-type-routing.module";



@NgModule({
    declarations: [IndustryTypeListComponent, IndustryTypeAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatSelectModule,
    AutoCompleteModule,
    NgxPrintModule,
    SignaturePadModule,
    IndustryTypeRoutingModule
  ],
  entryComponents: [IndustryTypeAddEditComponent],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }]
})
export class IndustryTypeModule { }