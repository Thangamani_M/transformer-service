import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TicketTypeAddEditComponent } from './ticket-type-add-edit.component';
import { TicketTypeListComponent } from './ticket-type-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


const actionType: Routes = [
    {
        path: '', component: TicketTypeListComponent, canActivate:[AuthGuard], data: { title: 'Action Type' }
    },
    {
        path: 'add-edit', component: TicketTypeAddEditComponent, canActivate:[AuthGuard], data: { title: 'Action Type Add Edit' }
    },
];

@NgModule({
  imports: [RouterModule.forChild(actionType)],
  
})

export class TicketTypeRoutingModule { }