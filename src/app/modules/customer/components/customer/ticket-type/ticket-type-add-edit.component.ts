import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomDirectiveConfig, ModulesBasedApiSuffix, RxjsService, trimValidator } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, SnackbarService } from '@app/shared/services';
import { ActionTypeModel, ActionTypeModelName } from '@modules/customer/models/action-type-model';
import { SalesModuleApiSuffixModels } from '@modules/sales';

@Component({
  selector: 'app-ticket-type-add-edit',
  templateUrl: './ticket-type-add-edit.component.html',
  styleUrls: ['./ticket-type-add-edit.component.scss']
})
export class TicketTypeAddEditComponent implements OnInit {
  header: any;
  actionGroupList = [];
  actionTypeList = [];
  actionTypeForm: FormGroup;
  actionType: any;
  saveActionList: any;
  actionTypeNameList = [];
  ticketGroupId: any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private crudService: CrudService, private formBuilder: FormBuilder, private router: Router, private snackBarService: SnackbarService) {
    this.ticketGroupId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {

    this.LoadActionGroup();
    if (this.ticketGroupId) {
      this.header = "Update";
      this.GetTicketGroupDetails();
    } else {
      this.header = "Create";
    }
    this.actionTypeForm = this.formBuilder.group({
      ticketGroupId: [this.ticketGroupId || '', [Validators.required]],
      ticketTypeName: ['' || '', [trimValidator]]
    });
  }
  GetTicketGroupDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ACTION_TYPE_DETAILS, this.ticketGroupId).subscribe((res) => {
      this.actionTypeForm.get('ticketGroupId').setValue(res.resources[0].ticketGroupId);
      res.resources.forEach(resourcesObj => {
        this.actionTypeNameList = [{
          ticketTypeName: resourcesObj.ticketTypeName,
          ticketTypeId: resourcesObj.ticketTypeId
        }]
        this.actionTypeList.unshift(this.actionTypeNameList[0]);
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  LoadActionGroup() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_ACTION_GROUP, undefined, true).subscribe((res) => {
      this.actionGroupList = res.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  AddActionType() {

    if (this.actionTypeForm.value.ticketTypeName.trim()) {
      if (this.actionTypeList.length != 0) {
        let existItem = this.actionTypeList.find(item => item.ticketTypeName === this.actionTypeForm.value.ticketTypeName);
        if (existItem) {
          this.snackBarService.openSnackbar("Action type already exist", ResponseMessageTypes.WARNING);
          return;
        }
      }

      this.actionTypeNameList = [{
        ticketTypeName: this.actionTypeForm.value.ticketTypeName,
        ticketTypeId: null
      }]
      this.actionTypeList.unshift(this.actionTypeNameList[0]);
      this.actionTypeForm.get('ticketTypeName').setValue('');
    } else {
      this.snackBarService.openSnackbar("Please enter value", ResponseMessageTypes.WARNING);
      return;
    }



  }
  DeleteActionType(index) {

    if (this.actionTypeList[index].ticketTypeId != null) {
      this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ACTION_TYPE, this.actionTypeList[index].ticketTypeId).subscribe((res) => {
        if (res.isSuccess) {
          this.actionTypeList.splice(index, 1);
        }
      })

    } else {
      this.actionTypeList.splice(index, 1);
    }

  }
  submit() {
    if (this.actionTypeForm.invalid) {
      this.actionTypeForm.markAllAsTouched();
      return;
    }
    if (this.actionTypeList.length == 0) {
      this.snackBarService.openSnackbar("Please Add atleast one action type", ResponseMessageTypes.WARNING);
      return;
    }
    let actionTypeModel = new ActionTypeModel();
    actionTypeModel.ticketGroupId = this.actionTypeForm.value.ticketGroupId;
    for (var i = 0; i < this.actionTypeList.length; i++) {
      this.saveActionList = new ActionTypeModelName();
      this.saveActionList.ticketTypeName = this.actionTypeList[i].ticketTypeName;
      this.saveActionList.ticketTypeId = this.actionTypeList[i].ticketTypeId;
      actionTypeModel.ticketTypeNameList.push(this.saveActionList);
    }

    if (actionTypeModel.ticketTypeNameList.length != 0) {
      if (this.ticketGroupId) {
        this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ACTION_TYPE, actionTypeModel).subscribe((res => {
          if (res.isSuccess) {
            this.router.navigate(['/customer/action-types'], { skipLocationChange: true });
          }
        }))
      } else {
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ACTION_TYPE, actionTypeModel).subscribe((res => {
          if (res.isSuccess) {
            this.router.navigate(['/customer/action-types'], { skipLocationChange: true });
          }
        }))
      }

    }

  }
}
