import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDialogRef, MatSelectModule, MAT_DIALOG_DATA } from "@angular/material";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { NgxPrintModule } from "ngx-print";
import { SignaturePadModule } from "ngx-signaturepad";
import { AutoCompleteModule } from "primeng/autocomplete";
import { TicketTypeAddEditComponent } from "./ticket-type-add-edit.component";
import { TicketTypeListComponent } from "./ticket-type-list.component";
import { TicketTypeRoutingModule } from "./ticket-type-routing.module";


@NgModule({
    declarations: [TicketTypeListComponent, TicketTypeAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatSelectModule,
    AutoCompleteModule,
    NgxPrintModule,
    SignaturePadModule,
    TicketTypeRoutingModule
  ],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }]
})
export class TicketTypeModule { }