import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerViewNotesComponent } from './customer-view-notes.component';


const routes: Routes = [
  {
    path: '', component: CustomerViewNotesComponent, data: { title: 'View Customer Notes' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CustomerViewNotesRoutingModule { }
