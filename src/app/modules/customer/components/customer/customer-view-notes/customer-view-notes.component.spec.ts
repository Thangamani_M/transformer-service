import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerViewNotesComponent } from './customer-view-notes.component';

describe('CustomerViewNotesComponent', () => {
  let component: CustomerViewNotesComponent;
  let fixture: ComponentFixture<CustomerViewNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerViewNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerViewNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
