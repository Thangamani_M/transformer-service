import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { DataViewModule } from 'primeng/dataview';
import { CustomerViewNotesRoutingModule } from './customer-view-notes-routing.module';
import { CustomerViewNotesComponent } from './customer-view-notes.component';



@NgModule({
  declarations: [CustomerViewNotesComponent],
  imports: [
    CommonModule,
    CustomerViewNotesRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    DataViewModule,
    MaterialModule
  ]
})
export class CustomerViewNotesModule { }
