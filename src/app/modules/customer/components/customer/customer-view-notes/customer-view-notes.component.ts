import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { addFormControls, CrudService, removeFormControls, IApplicationResponse, CrudType, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CustomerManagementService, CustomerModuleApiSuffixModels } from '@modules/customer';
import { forkJoin } from 'rxjs';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { MomentService } from '@app/shared/services/moment.service';

@Component({
  selector: 'app-customer-view-notes',
  templateUrl: './customer-view-notes.component.html',
  styleUrls: ['./customer-view-notes.component.scss']
})
export class CustomerViewNotesComponent implements OnInit {
  customerId: any
  customerAddressId: any
  custInfoList: any
  selectedAddressDetails: any
  showFilterForm: boolean = false
  filterForm: FormGroup
  notesList: any = []
  departmentDropdown: any = []
  actionTypeDropdown: any = []
  userGroupDropdown: any = []
  statusDropdown: any = []
  totalRecords: any = 0
  partitionId: any;
  feature: string;
  featureIndex: string;
  ticketdetails:any = {}

  constructor(private activatedRoute: ActivatedRoute, private momentService: MomentService, private _fb: FormBuilder, private crudService: CrudService, private router: Router, private customerManagementService: CustomerManagementService, private rxjsService: RxjsService) {
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.customerAddressId = this.activatedRoute.snapshot.queryParams.customerAddressId;
    this.feature = this.activatedRoute.snapshot.queryParams?.feature_name;
    this.featureIndex = this.activatedRoute.snapshot.queryParams?.featureIndex;
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.createFilterForm();

    this.getCustomerInfo(this.customerId)
    this.getAllDropdowns()

  }



  //===========CUSTOMER INFO============//
  getCustomerInfo(custId) {
    this.customerManagementService.getCustomer(custId).subscribe(resp => {
      if (resp?.isSuccess && resp?.statusCode == 200) {
        this.custInfoList = resp.resources;
        this.partitionId = resp.resources?.partitionId
        this.selectedAddressDetails = resp.resources.addresses.find(cA => cA['addressId'] == this.customerAddressId);
        if (this.selectedAddressDetails?.partitionDetails?.length > 0) {
          this.partitionId = this.selectedAddressDetails?.partitionDetails?.find(pD => pD['isPrimary']) ?
            this.selectedAddressDetails.partitionDetails?.find(pD => pD['isPrimary']).id : this.selectedAddressDetails?.partitionDetails[0]?.id;
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getAllDropdowns() {
    let dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UX_TICKET_DEPARTMENT),
      // this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
      //   SalesModuleApiSuffixModels.UX_TICKET_GROUP),
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
        SalesModuleApiSuffixModels.RAW_LEAD_USER_GROUPS),
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UX_TICKETSTATUS),
    ];
    this.loadActionTypes(dropdownsAndData);
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.departmentDropdown = resp.resources;
              break;
            case 1:
              this.userGroupDropdown = resp.resources;
              break;
            case 2:
              this.statusDropdown = resp.resources;
              break;
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onAddressChange(event) {
    this.filterForm.get('siteAddressId').setValue(event.target.value)
    this.submitFilter()
  }

  loadData(event) {
    //event.first = First row offset
    //event.rows = Number of rows per page
    let filteredData = this.filterForm.value
    filteredData.fromDate = this.momentService.toFormateType(filteredData.fromDate, 'DD-MMM-YYYY')
    filteredData.toDate = this.momentService.toFormateType(filteredData.toDate, 'DD-MMM-YYYY')
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || !filteredData[key]) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.getRequiredListData(event.first, event.rows, filterdNewData);
    this.showFilterForm = false;
  }

  createFilterForm(): void {
    this.filterForm = this._fb.group({
      customerId: [this.customerId ? this.customerId : '', Validators.required],
      siteAddressId: [this.customerAddressId ? this.customerAddressId : '', Validators.required],
      isTickets: [true],
      isVerification: [true],
      ticketGroupIds: [''],
      ticketTypeIds: [''],
      groupIds: [''],
      statusIds: [''],
      fromDate: [new Date(), Validators.required],
      toDate: [new Date(), Validators.required],
    });
    this.filterForm = setRequiredValidator(this.filterForm, ["customerId", "fromDate", "toDate"]);

    this.filterForm.get('isTickets').valueChanges.subscribe(val => {
      if (val) {
        // this.filterForm = setRequiredValidator(this.filterForm, ["customerId", "ticketGroupIds", "ticketTypeIds", "groupIds", "statusIds", "fromDate", "toDate"]);

        this.filterForm.get('ticketGroupIds').enable()
        this.filterForm.get('ticketTypeIds').enable()
        this.filterForm.get('groupIds').enable()
        this.filterForm.get('statusIds').enable()

      } else {

        this.filterForm.get('ticketGroupIds').setValue(null)
        this.filterForm.get('ticketTypeIds').setValue(null)
        this.filterForm.get('groupIds').setValue(null)
        this.filterForm.get('statusIds').setValue(null)

        this.filterForm.get('ticketGroupIds').disable()
        this.filterForm.get('ticketTypeIds').disable()
        this.filterForm.get('groupIds').disable()
        this.filterForm.get('statusIds').disable()
      }
    })

    this.filterForm.get('ticketGroupIds').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      let otherParams = {}
      otherParams['ticketGroupIds'] = val
      this.crudService.get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UX_TICKET_GROUP,
        undefined,
        false, prepareGetRequestHttpParams('0', '0', otherParams)).subscribe(resp => {
          if (resp?.isSuccess && resp?.statusCode == 200) {
            this.actionTypeDropdown = resp.resources
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    })
  }

  onChangePartition() {

  }

  callDail(number: any) {

  }

  submitFilter() {
    if (this.filterForm.invalid) {
      this.filterForm.markAllAsTouched();
      return;
    }
    let event = {
      first: 0,
      rows: 10
    }
    this.loadData(event)


  }

  resetForm() {
    this.filterForm.reset()
    let otherParams = {}
    this.filterForm.get('customerId').setValue(this.customerId)
    this.filterForm.get('fromDate').setValue(new Date())
    this.filterForm.get('toDate').setValue(new Date())
    otherParams['customerId'] = this.customerId
    otherParams['siteAddressId'] = this.customerAddressId
    otherParams['fromDate'] = this.momentService.toFormateType(new Date(), 'DD-MMM-YYYY')
    otherParams['toDate'] = this.momentService.toFormateType(new Date(), 'DD-MMM-YYYY')
    this.getRequiredListData('0', '0', otherParams);
    this.showFilterForm = !this.showFilterForm;
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {

    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.CUSTOMER_TICKET_NOTES_VERIFICATION,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.notesList = data.resources
        let findIndex =  this.notesList.findIndex(item=> item.ticketTypeName == "RefertoSales");
        if(findIndex !=-1){
          this.getTicketById(findIndex)
        }
        this.totalRecords = data.totalCount ? data.totalCount : 0
      }
    }, error => {
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  navigateToViewCustomer() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.customerAddressId,
      feature_name: this.feature,
      featureIndex: this.featureIndex,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams);
        break;
      case CrudType.EXPORT:
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      default:
    }
  }


  getTicketById(index) {
    let ticketId = this.notesList[index]?.ticketid
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKETS, ticketId, false, null, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200) {
          this.rxjsService.setGlobalLoaderProperty(true);
          this.ticketdetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

}
