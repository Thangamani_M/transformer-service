import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService } from '@app/shared/services';
import { ModulesBasedApiSuffix, IApplicationResponse } from '@app/shared';
import { SalesModuleApiSuffixModels } from '@modules/sales';

@Component({
  selector: 'app-safe-entry-view',
  templateUrl: './safe-entry-view.component.html'
})
export class SafeEntryViewComponent implements OnInit {
  customerId: string;
  safeEntryDetail: any = {};
  constructor(private activatedRoute: ActivatedRoute, private crudService: CrudService, private router: Router) {
    this.customerId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SAFE_ENTRY, this.customerId, false, null).subscribe((response: IApplicationResponse) => {
        this.safeEntryDetail = response.resources;
      });

  }
  onEditButtonClicked(): void {
    this.router.navigate(['customer/safe-entry/add-edit'], { queryParams: { id: this.customerId, safeEntryId: this.safeEntryDetail["safeEntryId"] }, skipLocationChange: true })
  }

}
