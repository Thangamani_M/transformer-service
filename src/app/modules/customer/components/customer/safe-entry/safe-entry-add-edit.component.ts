import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, setRequiredValidator } from '@app/shared';
import { CrudService, HttpCancelService } from '@app/shared/services';
import { SafeEntryModel } from '@modules/customer/models/safe-entry-model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, finalize, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-safe-entry-add-edit',
  templateUrl: './safe-entry-add-edit.component.html',
  styleUrls: ['./safe-entry-add-edit.component.scss']
})
export class SafeEntryAddEditComponent implements OnInit {
  safeEntryForm: FormGroup;
  vechicleRegistrationNumberList: any = [];
  isLoading: boolean;
  safeEntryId: string;
  userData: UserLogin;
  minDate: Date = new Date();
  customerId: string;
  fromTime: any;
  contractList: any = [];
  userGroupList: any = [];
  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private store: Store<AppState>, private crudService: CrudService, private httpCancelService: HttpCancelService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.customerId = this.activatedRoute.snapshot.queryParams.id;
    this.safeEntryId = this.activatedRoute.snapshot.queryParams.safeEntryId;

  }

  ngOnInit(): void {
    this.createSafeEntryManualAddForm();
    this.getSafeEntryById().subscribe((response: IApplicationResponse) => {

    });
    if (this.safeEntryId) {
      this.getSafeEntryById().subscribe((response: IApplicationResponse) => {
        let safeEntryModel = new SafeEntryModel(response.resources);
        let ContactNumber = safeEntryModel.contactNumber.split(' ');
        safeEntryModel.contactNumberValue = ContactNumber[1] + " " + ContactNumber[2] + " " + ContactNumber[3];
        safeEntryModel.contactCountryCode = ContactNumber[0];
        safeEntryModel.startTime = response.resources.requestDuration.split("(")[1].split("-")[0].replace(/[^\d.-:]/g, '') + " " + response.resources.requestDuration.split("(")[1].split("-")[0].replace(/[^a-z]/gi, '');
        safeEntryModel.endTime = response.resources.requestDuration.split("(")[1].split("-")[1].replace(/[^\d.-:]/g, '') + " " + response.resources.requestDuration.split("(")[1].split("-")[1].replace(/[^a-z]/gi, '');

        this.safeEntryForm.setValue(safeEntryModel);
      });
    } else {
      this.safeEntryForm.controls['contactCountryCode'].setValue("+27");
      this.safeEntryForm.controls['vehicleColor'].setValue("");
    }
    this.safeEntryForm.get('vehicleRegistrationNumber').valueChanges.pipe(debounceTime(100), distinctUntilChanged(), switchMap(searchText => {
      this.vechicleRegistrationNumberList = [];
      if (!searchText) {

        this.isLoading = false;
        return this.vechicleRegistrationNumberList;
      } else if (typeof searchText === 'object') {
        this.isLoading = false;
        return this.vechicleRegistrationNumberList = [];
      } else {
        return this.crudService.get(
          ModulesBasedApiSuffix.SALES,
          SalesModuleApiSuffixModels.UX_SAFE_ENTRY, null, true,
          prepareGetRequestHttpParams(null, null, { searchText, isAll: false }))
          .pipe(
            finalize(() => this.isLoading = false),
          )
      }
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.vechicleRegistrationNumberList = response.resources;

      }
    })
  }
  createSafeEntryManualAddForm(): void {
    let safeEntryManualAddEditModel = new SafeEntryModel();
    this.safeEntryForm = this.formBuilder.group({});
    Object.keys(safeEntryManualAddEditModel).forEach((key) => {
      this.safeEntryForm.addControl(key, new FormControl(safeEntryManualAddEditModel[key]));
    });
    this.safeEntryForm = setRequiredValidator(this.safeEntryForm, ["contactPersonName", "contactNumberValue", "safeEntryDate", "startTime", "endTime", "reason", "comments"]);
  }
  getSafeEntryById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SAFE_ENTRY,
      this.customerId
    );
  }
  getDropdown(): Observable<any> {
    return forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.AGREEMENT_BY_CUSTOMER, undefined, true, prepareGetRequestHttpParams(null, null, { CustomerId: this.customerId })),
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.GROUP, undefined, true)

    )
  }
  onSubmit() {
    if (this.safeEntryForm.invalid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.safeEntryForm.value.customerId = "dcab2f61-598d-4337-8441-1be87ec0cd1e";
    if (this.safeEntryId) {
      this.safeEntryForm.value.safeEntryId = this.safeEntryId;
      this.safeEntryForm.value.modifiedUserId = this.userData.userId;
    } else {
      this.safeEntryForm.value.createdUserId = this.userData.userId;

    }
    if (this.safeEntryForm.value.contactNumberValue) {
      let contactNumber = this.safeEntryForm.value.contactCountryCode + ' ' + this.safeEntryForm.value.contactNumberValue;
      this.safeEntryForm.value.contactNumber = contactNumber;
    }
    if (this.safeEntryForm.value.safeEntryDate) {
      this.safeEntryForm.value.safeEntryDate = (new Date(Date.parse(this.safeEntryForm.value.safeEntryDate))).toDateString();
    }
    let crudService: Observable<IApplicationResponse> = !this.safeEntryId ? this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SAFE_ENTRY, this.safeEntryForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SAFE_ENTRY, this.safeEntryForm.value)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
      }
    })
  }
}
