import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SafeEntryAddEditComponent } from './safe-entry-add-edit.component';
import { SafeEntryViewComponent } from './safe-entry-view.component';



const safeEntry: Routes = [
    {
        path: 'view', component: SafeEntryViewComponent, data: { title: 'Safe Entry View' }
    },
    {
        path: 'add-edit', component: SafeEntryAddEditComponent, data: { title: 'Safe Entry Add Edit' }
    },
];

@NgModule({
    imports: [RouterModule.forChild(safeEntry)],
    
})

export class SafeEntryRoutingModule { }