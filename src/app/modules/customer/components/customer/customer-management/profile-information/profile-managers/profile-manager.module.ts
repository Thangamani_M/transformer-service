import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ProfileInformationManagerComponent } from "./profile-manager.component";

@NgModule({
  declarations: [ProfileInformationManagerComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [ProfileInformationManagerComponent],
  entryComponents: [ProfileInformationManagerComponent],
})
export class ProfileInformationManagerModule { }