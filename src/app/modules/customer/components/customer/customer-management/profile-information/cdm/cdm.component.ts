
import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import {
  CustomDirectiveConfig, formConfigs, setRequiredValidator,
  CrudService, RxjsService, LoggedInUserModel, ModulesBasedApiSuffix, IApplicationResponse, ResponseMessageTypes, SnackbarService, prepareRequiredHttpParams, PERMISSION_RESTRICTION_ERROR
} from '@app/shared';
import {
  loggedInUserData

} from '@app/modules';
import { Store } from '@ngrx/store';
import { AppState } from '@app/reducers';
import { combineLatest, forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';
import { UserModuleApiSuffixModels } from '@modules/user';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { CDMModel } from '@modules/customer/components/non-customer-profile/shared/model';
@Component({
  selector: 'app-profile-information-cdm',
  templateUrl: './cdm.component.html',
  styleUrls: ['../profile-information.component.scss']
})

export class ProfileInformationCdmComponent implements OnInit {
  formConfigs = formConfigs;
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  cdmForm: FormGroup
  loggedUser: LoggedInUserModel;

  // Dropdowns

  rolesList = [];
  employeeList = []
  // lists
  cdmList = []
  pageIndex: number = 0

  constructor(
    public ref: DynamicDialogRef, public config: DynamicDialogConfig,
    private dialogService: DialogService,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,
    private store: Store<AppState>, private rxjsService: RxjsService) {

  }
  dropdownsAndData = []
  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.dropdownsAndData = [
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_ROLES),
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_EMPLOYEES)
    ];
    this.getCDMByCommunity()
    this.loadDropdownData();
    this.createCDMFORM()
    this.combineLatestNgrxStoreDataOne()
 }


 getCDMByCommunity (){
  this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.COMMUNITY_CDM_DETAIL,null, false, prepareRequiredHttpParams({communityId  : this.config.data.communityId})).subscribe(response=>{
    if(response.isSuccess && response.statusCode==200){
      this.cdmList = response.resources;
    }
  })
}
  loadDropdownData() {
    forkJoin(this.dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.rolesList = resp.resources;
              break;
            case 1:
              this.employeeList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }


  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(loggedInUserData),

    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedUser = new LoggedInUserModel(response[0]);
      });

  }



  // cdm
  createCDMFORM(): void {
    let cdmModel = new CDMModel();
    this.cdmForm = this.formBuilder.group({});
    Object.keys(cdmModel).forEach((key) => {
      this.cdmForm.addControl(key, new FormControl(cdmModel[key]));
    });
    this.cdmForm = setRequiredValidator(this.cdmForm, ["roleId", "employeeId"]);
  }

  addCDM() {
    if(!this.config.data.permission?.create){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.cdmForm.invalid) return;
    let _isDuplicate = false
    this.cdmList.forEach(item => {
      if (item.roleId == this.cdmForm.get('roleId').value && item.employeeId == this.cdmForm.get('employeeId').value) {
        _isDuplicate = true;
      }
    })
    if (_isDuplicate) {
      this.snackbarService.openSnackbar("CDM already exist", ResponseMessageTypes.WARNING);
      return;
    }
    let obj = {
      createdUserId: this.loggedUser?.userId,
      communityId: this.config.data.communityId,
      roleId: [this.cdmForm.get('roleId').value],
      employeeId: [this.cdmForm.get('employeeId').value]
    }
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.COMMUNITY_CDM, obj).subscribe(response=>{
      if(response.isSuccess && response.statusCode ==200){
        this.getCDMByCommunity()
      }
    })


    // let _role = this.rolesList.find(v => v.id == this.cdmForm.get('roleId').value)
    // let _employee = this.employeeList.find(v => v.id == this.cdmForm.get('employeeId').value)
    // this.cdmList.push({
    //   employeeName: _employee.displayName,
    //   roleName: _role.displayName,
    //   ...this.cdmForm.value
    // });
    this.cdmForm.reset();
    this.cdmForm.get('roleId').setErrors(null)
    this.cdmForm.get('employeeId').setErrors(null)
  }

  removeCDM(index, cdm) {
    if(!this.config.data.permission?.delete){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.cdmList.length == 1) {
      this.snackbarService.openSnackbar("Atleast Profile Manager is Required", ResponseMessageTypes.WARNING);
      return;
    }

    if (cdm.communityCDMId) {
      this.deleteData(cdm.communityCDMId, index, CustomerModuleApiSuffixModels.COMMUNITY_CDM, 'cdm')
    } else {
      this.cdmList.splice(index, 1);
    }

  }


  deleteData(id, index, apiUrl, type) {
    // community-cdm
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        dataObject: {communityCDMId:id},
        modifiedUserId: this.loggedUser.userId,
        selectAll: false,
        moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        apiSuffixModel: apiUrl,
        method:'delete'
      },
    });
    ref.onClose.subscribe((result) => {

      if (result) {
        this.cdmList.splice(index, 1);
      }
    });
  }
  dialogClose() :void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
