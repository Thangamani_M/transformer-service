import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ProfileInformationCameraComponent } from "./camera-information.component";

@NgModule({
  declarations: [ProfileInformationCameraComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [ProfileInformationCameraComponent],
  entryComponents: [ProfileInformationCameraComponent],
})
export class ProfileInformationCameraModule { }