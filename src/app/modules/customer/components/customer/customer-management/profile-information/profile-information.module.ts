import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AddServicesModule } from "@modules/customer/components/non-customer-profile/components/add-services/add-services-module";
import { ProfileInformationCameraModule } from "./camera-information/camera-information.module";
import { ProfileInformationCdmModule } from "./cdm/cdm.module";
import { CustomerProfileInformationComponent } from "./profile-information.component";
import { ProfileInformationManagerModule } from "./profile-managers/profile-manager.module";

@NgModule({
  declarations: [CustomerProfileInformationComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    ProfileInformationManagerModule,
    ProfileInformationCdmModule,
    ProfileInformationCameraModule,
    AddServicesModule,
  ],
  exports: [CustomerProfileInformationComponent],
  entryComponents: [],
})
export class CustomerProfileInformationModule { }