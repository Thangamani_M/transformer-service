
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {
  loggedInUserData
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { ProfileManagerModel } from '@modules/customer/components/non-customer-profile/shared/model';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-profile-information-manager',
  templateUrl: './profile-manager.component.html',
  styleUrls: ['../profile-information.component.scss']
})

export class ProfileInformationManagerComponent implements OnInit {
  formConfigs = formConfigs;
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });

  profileManagerForm: FormGroup
  loggedUser: LoggedInUserModel;

  @Output() emitData = new EventEmitter<any>();
  @Output() emitCustomerType = new EventEmitter<any>();

  // Dropdowns
  rolesList = [];
  employeeList = []
  // lists
  profileManagerList = []
  temp_ProfileManagerList = []
  constructor(
    public ref: DynamicDialogRef, public config: DynamicDialogConfig,
    private dialogService: DialogService,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,
    private store: Store<AppState>, private rxjsService: RxjsService) {

  }
  dropdownsAndData = []
  ngOnInit(): void {
    this.dropdownsAndData = [
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_ROLES),
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_EMPLOYEES)
    ];
    this.getProfileManagerByCommunity();
    this.loadDropdownData();
    this.createProfileManagerForm()
    this.combineLatestNgrxStoreDataOne()
    this.rxjsService.setDialogOpenProperty(true);

  }

  loadDropdownData() {
    forkJoin(this.dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.rolesList = resp.resources;
              break;
            case 1:
              this.employeeList = resp.resources;
              break;
          }
          this.mapData(this.temp_ProfileManagerList);
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }


  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(loggedInUserData),
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedUser = new LoggedInUserModel(response[0]);
      });

  }
getProfileManagerByCommunity (){
  this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.COMMUNITY_PROFILE_MANAGER_DETAIL,null, false, prepareRequiredHttpParams({communityId  : this.config.data.communityId})).subscribe(response=>{
    if(response.isSuccess && response.statusCode==200){
      this.profileManagerList = response.resources;
    }
  })
}

  // Profile Managers ----
  createProfileManagerForm(): void {
    let profileManagerModel = new ProfileManagerModel();
    this.profileManagerForm = this.formBuilder.group({});
    Object.keys(profileManagerModel).forEach((key) => {
      this.profileManagerForm.addControl(key, new FormControl(profileManagerModel[key]));
    });
    this.profileManagerForm = setRequiredValidator(this.profileManagerForm, ["roleId", "employeeId"]);
  }

  addProfileManager() {
    if(!this.config.data?.permission.create){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.profileManagerForm.invalid) return;
    let _isDuplicate = false
    this.profileManagerList.forEach(item => {
      if (item.roleId == this.profileManagerForm.get('roleId').value && item.employeeId == this.profileManagerForm.get('employeeId').value) {
        _isDuplicate = true;
      }
    })
    if (_isDuplicate) {
      this.snackbarService.openSnackbar("Profile Manager already exist", ResponseMessageTypes.WARNING);
      return;
    }

    // let _role = this.rolesList.find(v => v.id == this.profileManagerForm.get('roleId').value)
    // let _employee = this.employeeList.find(v => v.id == this.profileManagerForm.get('employeeId').value)

    let obj = {
      createdUserId: this.loggedUser?.userId,
      communityId: this.config.data.communityId,
      roleId: [this.profileManagerForm.get('roleId').value],
      employeeId: [this.profileManagerForm.get('employeeId').value]
    }
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.COMMUNITY_PROFILE_MANAGER, obj).subscribe(response=>{
      if(response.isSuccess && response.statusCode ==200){
        this.getProfileManagerByCommunity()
      }
    })
    // this.profileManagerList.push({
    //   employeeName: _employee.displayName,
    //   roleName: _role.displayName,
    //   ...this.profileManagerForm.value
    // });
    this.profileManagerForm.reset()
    this.profileManagerForm.get('roleId').setErrors(null)
    this.profileManagerForm.get('employeeId').setErrors(null)
  }

  removeProfileManager(index, profile) {
    if(!this.config.data?.permission.delete){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.profileManagerList.length == 1) {
      this.snackbarService.openSnackbar("Atleast Profile Manager is Required", ResponseMessageTypes.WARNING);
      return;
    }
    if (profile.communityManagerId) {
      this.deleteData(profile.communityManagerId, index, CustomerModuleApiSuffixModels.COMMUNITY_PROFILE_MANAGER);
    } else {
      this.profileManagerList.splice(index, 1);
    }
  }

  mapData (data){
    for (const iterator of data) {
      let _role = this.rolesList.find(v => v.id == iterator.roleId)
      let _employee = this.employeeList.find(v => v.id == iterator.employeeId)

      this.profileManagerList.push({
        ...data, employeeName: _employee.displayName,
        roleName: _role.displayName
      })
    }
  }

  deleteData(id, index, apiUrl) {
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        dataObject: {communityManagerId:id},
        modifiedUserId: this.loggedUser.userId,
        moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        apiSuffixModel: apiUrl,
        method:'delete'
      },
    });
    ref.onClose.subscribe((result) => {

      if (result) {
        this.profileManagerList.splice(index, 1);
      }
    });
  }
  dialogClose() :void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
