

import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, ExtensionModalComponent, formConfigs, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { AddServicesComponent } from '@modules/customer/components/non-customer-profile/components/add-services/add-services-component';
import { FEATURE_NAME } from '@modules/customer/components/non-customer-profile/shared/enum/index.enum';
import { CameraInformationModel, ProfileInformationModel } from '@modules/customer/components/non-customer-profile/shared/model';
import { AddServices } from '@modules/customer/models/add-services.model';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin } from 'rxjs';
import { ProfileInformationCameraComponent } from './camera-information/camera-information.component';
import { ProfileInformationCdmComponent } from './cdm/cdm.component';
import { ProfileInformationManagerComponent } from './profile-managers/profile-manager.component';
@Component({
  selector: 'app-camera-profile-information',
  templateUrl: './profile-information.component.html',
  styleUrls: ['./profile-information.component.scss']
})

export class CustomerProfileInformationComponent implements OnInit {
  @Input() permission
  formConfigs = formConfigs;
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  cameraInfoForm: FormGroup
  profileInformationForm: FormGroup
  siteInfoForm: FormGroup
  servicesForm: FormGroup
  loggedUser: LoggedInUserModel;
  serviceDetails;
  serviceObj = {};
  // Dropdowns
  divisionList = [];
  mainAreaList = [];
  subAreaList = [];
  suburbsList = []
  // lists
  cdmList = []
  pageIndex: number = 0
  dropdownsAndData = []
  customerId: any
  customerObject: any
  agentExtensionNo: any
  featureName: string
  postServiceApi: string
  putServiceApi: string
  cameraInfoRole = []
  profileManagerRole = []
  cmdsRole = []

  primengTableConfigPropertiesCamera: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }
  primengTableConfigPropertiesProfile: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }

  primengTableConfigPropertiesCdms: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }

  constructor(
    private snackbarService: SnackbarService,
    private dialogService: DialogService,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.customerId = this.activatedRoute.snapshot.params['id']
    this.featureName = this.activatedRoute.snapshot.queryParams.feature_name
  }


  ngOnInit(): void {
    this.getApiEndPoint();
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT,
        UserModuleApiSuffixModels.UX_REGIONS),
      this.crudService.get(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_MAIN_AREA),
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
        SalesModuleApiSuffixModels.COUNTER_SALES_SUBURUBS),
    ]
    this.loadDropdownData()
    this.createCameraInformationForm();
    this.createProfileInformationForm()
    this.createSiteInfoForm();
    this.createLineInfoForm();
    this.getCustomerDetails()
    if (this.customerId && this.featureName == 'execu-guard-profile') {
      this.getBusniessClassificationById();
    }

    this.cameraInfoRole = this.permission['subMenu']?.find(item => item.menuName.includes('Camera Info'));
    let prepareDynamicTableTabsFromPermissionsObj1 = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesCamera, this.cameraInfoRole['subMenu']);
    this.primengTableConfigPropertiesCamera = prepareDynamicTableTabsFromPermissionsObj1['primengTableConfigProperties'];

    this.profileManagerRole = this.permission['subMenu']?.find(item => item.menuName.includes('Profile Managers'));
    let prepareDynamicTableTabsFromPermissionsObj2 = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesProfile, this.profileManagerRole['subMenu']);
    this.primengTableConfigPropertiesProfile = prepareDynamicTableTabsFromPermissionsObj2['primengTableConfigProperties'];

    this.cmdsRole = this.permission['subMenu']?.find(item => item.menuName.includes('CDMs'));
    let prepareDynamicTableTabsFromPermissionsObj3 = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesCdms, this.cmdsRole['subMenu']);
    this.primengTableConfigPropertiesCdms = prepareDynamicTableTabsFromPermissionsObj3['primengTableConfigProperties'];

  }
  config = {
    isShowCDM: true,
    isShowProfileInformation: true,
    isShowProfileManagers: true,
    isShowCommunity: true,
    isShowContactPersons: true,
    isShowCameraInfo: false,
    isShowSiteInformation: false,
    isShowLineInformation: false,
    communityName: 'Community Name'
  }
  detailsApiEndpoint: CustomerModuleApiSuffixModels;
  getApiEndPoint() {
    switch (this.featureName) {
      case FEATURE_NAME.CAMERA_PROFILE:
        this.detailsApiEndpoint = CustomerModuleApiSuffixModels.CAMERA_PROFILE_DETAIL
        this.config.communityName = "Community Name"
        this.config.isShowCameraInfo = true
        break;

      case FEATURE_NAME.HUB_PROFILE:
        this.detailsApiEndpoint = CustomerModuleApiSuffixModels.HUB_PROFILE_DETAILS
        this.config.communityName = "Hub Name"
        this.config.isShowCDM = false
        break;

      case FEATURE_NAME.REPEATER_PROFILE:
        this.detailsApiEndpoint = CustomerModuleApiSuffixModels.REPEATER_PROFILE_DETAIL
        this.config.communityName = "Hub Name"
        this.config = {
          isShowCDM: false,
          isShowProfileInformation: false,
          isShowProfileManagers: false,
          isShowCommunity: false,
          isShowContactPersons: true,
          isShowCameraInfo: false,
          communityName: 'Community Name',
          isShowSiteInformation: false,
          isShowLineInformation: false,
        }
        break;
      case FEATURE_NAME.WHATSAPP_PROFILE:
        this.detailsApiEndpoint = CustomerModuleApiSuffixModels.WHATSAPP_PROFILE_DETAIL;
        this.config.communityName = "Group Name";
        this.config.isShowCameraInfo = false;
        break;
      case FEATURE_NAME.EXECU_GUARD_PROFILE:
        this.detailsApiEndpoint = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_CLASSIFICATION_DETAIL
        this.postServiceApi = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_SERVICES;
        this.putServiceApi = CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_ACCOUNT_NUMBER;
        //this.config.communityName = "Hub Name"
        this.config = {
          isShowCDM: false,
          isShowProfileInformation: false,
          isShowProfileManagers: false,
          isShowCommunity: false,
          isShowContactPersons: false,
          isShowCameraInfo: false,
          communityName: 'Community Name',
          isShowSiteInformation: true,
          isShowLineInformation: true
        }
        break;
      case FEATURE_NAME.COMMUNITY_PROFILE:
        this.detailsApiEndpoint = CustomerModuleApiSuffixModels.COMMUNITY_PROFILE_DETAIL;
        this.config.communityName = "Community Name";
        this.config.isShowCameraInfo = false;
        break;
      default:
        break;
    }
  }

  loadDropdownData() {
    forkJoin(this.dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.divisionList = resp.resources;
              break;
            case 1:
              if (this.getHubProfile())  {
                this.mainAreaList = getPDropdownData(resp.resources, 'mainAreaName', 'mainAreaId');
              } else {
                this.mainAreaList = resp.resources;
              }
              break;
            case 2:
              this.suburbsList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getHubProfile() {
    return this.featureName == FEATURE_NAME.HUB_PROFILE;
  }

  onChangeMainArea() {
    if (this.profileInformationForm.get('mainAreaId').value && !this.getHubProfile()) {
      this.crudService.get(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.SALES_API_UX_SUB_AREA, undefined, false, prepareRequiredHttpParams({
          mainAreaIds: this.profileInformationForm.get('mainAreaId').value
        })).subscribe(response => {
          if (response.isSuccess) {
            this.subAreaList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
  }

  createProfileInformationForm(): void {
    let customerInformatioanModel = new ProfileInformationModel();
    this.profileInformationForm = this.formBuilder.group({});
    Object.keys(customerInformatioanModel).forEach((key) => {
      this.profileInformationForm.addControl(key, new FormControl({ value: customerInformatioanModel[key], disabled: true },));
    });
    this.profileInformationForm = setRequiredValidator(this.profileInformationForm, ["communityName", "regionId", "mainAreaId", "subAreaId", "streetName", "suburbId"]);
  }

  createCameraInformationForm(): void {
    let camModel = new CameraInformationModel();
    this.cameraInfoForm = this.formBuilder.group({});
    Object.keys(camModel).forEach((key) => {
      this.cameraInfoForm.addControl(key, new FormControl(camModel[key]));
    });
    this.cameraInfoForm = setRequiredValidator(this.cameraInfoForm, ["cameraProfileName", "latLong", "location"]);
  }

  createSiteInfoForm() {
    this.siteInfoForm = new FormGroup({
      'execuGuardAccountNumber': new FormControl(null),
    });
    this.siteInfoForm = setRequiredValidator(this.siteInfoForm, ['execuGuardAccountNumber']);
  }
  createLineInfoForm() {
    this.servicesForm = this.formBuilder.group({
      createdUserId: [this.loggedUser?.userId],
    });
    this.servicesForm.addControl(
      "services",
      this.createLineInfoForms()
    );
  }

  createLineInfoForms() {
    let addServicesFormModel = new AddServices();
    let formControls = {};
    Object.keys(addServicesFormModel).forEach((key) => {
      formControls[key] = new FormControl(addServicesFormModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  getCustomerDetails() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, this.detailsApiEndpoint, undefined, false, prepareRequiredHttpParams({ customerId: this.customerId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.customerObject = response.resources;
        if (this.customerObject.keyholders) {
          this.customerObject['nonCustomerContactPersonDTOs'] = []
          this.customerObject.keyholders.map(item => {
            this.customerObject['nonCustomerContactPersonDTOs'].push(
              {
                contactPersion: item.keyHolderName,
                contactNumber: item.contactNo,
                contactNumberCountryCode: item.contactNoCountryCode,
                communityContactPersonId: item.customerId
              }
            )
          })
        } else {
          if (this.customerObject?.profileInformationDTO?.mainAreaId && typeof this.customerObject?.profileInformationDTO?.mainAreaId  == "string" && this.getHubProfile()) {
            this.customerObject.profileInformationDTO.mainAreaId = this.customerObject?.profileInformationDTO?.mainAreaId?.split(', ');
          }
          this.profileInformationForm.patchValue(this.customerObject.profileInformationDTO);
          this.onChangeMainArea()
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  getBusniessClassificationById() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, this.detailsApiEndpoint, undefined, false, prepareGetRequestHttpParams(null, null, {
      customerId: this.customerId
    }), 1).subscribe((response: IApplicationResponse) => {

      if (response.isSuccess && response.statusCode === 200) {
        this.serviceDetails = response.resources;
        this.siteInfoForm.get('execuGuardAccountNumber').setValue(this.serviceDetails?.execuGaurdAccountNumber?.execuGuardAccountNumber);
        if (this.serviceDetails.services && this.serviceDetails.services.length > 0) {
          this.serviceDetails.services.forEach(element => {
            this.serviceObj[element.serviceCategoryName] = {};
            this.serviceObj[element.serviceCategoryName].serviceCategoryName = element.serviceCategoryName;
            let services = [];
            if (element && element.services.length > 0) {
              element.services.forEach(ele => {
                ele.serviceMappingId = ele.serviceMappingId;
                ele.qty = ele?.qty ? ele.qty : 1;
                ele.serviceName = ele.serviceName;
                ele.serviceCode = ele.serviceCode;
                services.push(ele);
              });
            }
            this.serviceObj[element.serviceCategoryName]['values'] = services;
          });
        }
        this.servicesForm.value.businessService = [];
        if (this.serviceObj) {
          Object.keys(this.serviceObj).forEach((key) => {
            this.serviceObj[key]['values'].forEach((service) => {
              this.servicesForm.value.businessService.push(service);
            });
          });
        }
      }
    });

  }
  onAddServices() {
    let data = { ...{ customerId: this.customerId }, ...{ serviceObj: null } };
    data['serviceObj'] = this.serviceObj;
    const dialogRef = this.dialog.open(AddServicesComponent, {
      width: "1200px",
      data,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (typeof dialogResult === 'boolean' && dialogResult) {
        //this.isFormChangeDetected = false;
        return;
      }
    });
    var self = this;
    dialogRef.componentInstance.outputData.subscribe(ele => {
      self.serviceObj = ele.serviceObj;
      dialogRef.close();
      // this.isFormChangeDetected = true;
    });
  }
  onQuantityChange(type, serviceSubItem: object): void {
    if (type === 'plus') {
      serviceSubItem['qty'] = serviceSubItem['qty'] + 1;
    }
    else {
      serviceSubItem['qty'] = (serviceSubItem['qty'] === 1) ? serviceSubItem['qty'] : serviceSubItem['qty'] - 1;
    }
  }
  removeItem(serviceItemKey: string, serviceItemObj: any) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      let areAllServicesRemoved = false;
      let execuGuardServiceId = serviceItemObj?.execuGuardServiceId ? serviceItemObj.execuGuardServiceId : null;
      if (execuGuardServiceId) {

        this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.EXECU_GUARD_PROFILE_SERVICES, undefined,
          prepareRequiredHttpParams({
            execuGuardServiceId: execuGuardServiceId,
          }), 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              if (this.customerId)
                this.getBusniessClassificationById();
            }
          });
      } else {
        if (this.serviceObj && this.serviceObj[serviceItemKey] && this.serviceObj[serviceItemKey].values) {
          this.serviceObj[serviceItemKey].values.forEach((element, index) => {
            if (element.serviceMappingId == serviceItemObj.serviceMappingId)
              this.serviceObj[serviceItemKey].values.splice(index, 1);
          });
        }
      }


      // this.selectedItemsWithKeyValue = JSON.parse(JSON.stringify(this.serviceObj));
    });
  }

  Dial(custInfo): void {
    if (!this.agentExtensionNo) {
      this.dialog.closeAll();
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      const dialogReff = this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    } else {
      let data = {
        customerContactNumber: `${custInfo.contactNumberCountryCode}${custInfo.contactNumber}`,
        customerId: custInfo.communityContactPersonId,
        clientName: custInfo && custInfo.contactPersion
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
    }
  }

  openProfileManagerModal() {
    let permission = {
      edit: this.primengTableConfigPropertiesProfile.tableComponentConfigs.tabsList[0].canEdit,
      delete: this.primengTableConfigPropertiesProfile.tableComponentConfigs.tabsList[0].canRowDelete,
      create: this.primengTableConfigPropertiesProfile.tableComponentConfigs.tabsList[0].canCreate,
      view: !this.primengTableConfigPropertiesProfile.tableComponentConfigs.tabsList[0].disabled
    }
    if (!permission.view) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const ref = this.dialogService.open(ProfileInformationManagerComponent, {
      showHeader: true,
      header: "Add/Edit Profile Manager",
      baseZIndex: 10000,
      width: '720px',
      data: {
        communityId: this.customerObject.profileInformationDTO.communityId,
        permission: permission

      }
    });
    ref.onClose.subscribe((result) => {

    })
  }


  openCDMModal() {
    let permission = {
      edit: this.primengTableConfigPropertiesCdms.tableComponentConfigs.tabsList[0].canEdit,
      delete: this.primengTableConfigPropertiesCdms.tableComponentConfigs.tabsList[0].canRowDelete,
      create: this.primengTableConfigPropertiesCdms.tableComponentConfigs.tabsList[0].canCreate,
      view: !this.primengTableConfigPropertiesCdms.tableComponentConfigs.tabsList[0].disabled
    }
    if (!permission.view) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const ref = this.dialogService.open(ProfileInformationCdmComponent, {
      showHeader: true,
      header: "Add/Edit CDM Manager",
      baseZIndex: 10000,
      width: '720px',
      data: {
        communityId: this.customerObject.profileInformationDTO.communityId,
        permission: permission

      }
    });
    ref.onClose.subscribe((result) => {

    })
  }

  openCameraodal(row?) {
    let permission = {
      edit: this.primengTableConfigPropertiesCdms.tableComponentConfigs.tabsList[0].canEdit,
      delete: this.primengTableConfigPropertiesCdms.tableComponentConfigs.tabsList[0].canRowDelete,
      create: this.primengTableConfigPropertiesCdms.tableComponentConfigs.tabsList[0].canCreate
    }
    let _text = row ? row?.cameraProfileName : 'Add'
    if (_text == "Add") {
      if (permission?.create) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
    }
    const ref = this.dialogService.open(ProfileInformationCameraComponent, {
      showHeader: true,
      header: `${_text} - Camera Information`,
      baseZIndex: 10000,
      width: '480px',
      data: { ...row, communityId: this.customerObject.profileInformationDTO.communityId, permission: permission }
    });
    ref.onClose.subscribe((result) => {
      if (!result) this.getCustomerDetails()

    })
  }
  editSiteInfo() {
    if (!this.siteInfoForm.value.execuGuardAccountNumber)
      return;

    let formData = {
      customerId: this.customerId,
      execuGuardAccountNumber: this.siteInfoForm.value.execuGuardAccountNumber,
      createdUserId: this.loggedUser?.userId
    }

    let _apiUrl: any = this.putServiceApi;
    this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, _apiUrl, formData, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (this.customerId)
          this.getBusniessClassificationById();
      }
    });
  }
  servicesSubmit() {
    if (this.servicesForm.invalid) {
      return;
    }
    this.servicesForm.value.businessService = [];
    Object.keys(this.serviceObj).forEach((key) => {
      this.serviceObj[key]['values'].forEach((service) => {
        this.servicesForm.value.businessService.push(service);
      });

    });
    let formData = {
      customerId: this.customerId,
      services: this.servicesForm.value.businessService,
      createdUserId: this.servicesForm.value.createdUserId
    }
    let _apiUrl: any = this.postServiceApi;
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, _apiUrl, formData, 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.rxjsService.setGlobalLoaderProperty(false);
        //this.navigateTo(3);
      }
    });
  }
}
