import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ProfileInformationCdmComponent } from "./cdm.component";

@NgModule({
  declarations: [ProfileInformationCdmComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [ProfileInformationCdmComponent],
  entryComponents: [ProfileInformationCdmComponent],
})
export class ProfileInformationCdmModule { }