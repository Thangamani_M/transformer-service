import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { loggedInUserData } from "@app/modules";
import { AppState } from "@app/reducers";
import {
  CrudService,
  CustomDirectiveConfig,
  formConfigs,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  ResponseMessageTypes,
  RxjsService,
  setRequiredValidator,
  SnackbarService
} from "@app/shared";
import { PrimengDeleteConfirmDialogComponent } from "@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component";
import { CameraInformationModel } from "@modules/customer/components/non-customer-profile/shared/model";
import { CustomerModuleApiSuffixModels } from "@modules/customer/shared";
import { Store } from "@ngrx/store";
import {
  DialogService,
  DynamicDialogConfig,
  DynamicDialogRef
} from "primeng/api";
import { combineLatest } from "rxjs";
import { take } from "rxjs/operators";
@Component({
  selector: "app-profile-information-camera",
  templateUrl: "./camera-information.component.html",
  // styleUrls: ['./cdm.component.scss']
})
export class ProfileInformationCameraComponent implements OnInit {
  formConfigs = formConfigs;
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  stringConfig = new CustomDirectiveConfig({
    isAStringOnly: true,
    shouldPasteKeyboardEventBeRestricted: true,
  });
  cameraInfoForm: FormGroup;
  loggedUser: LoggedInUserModel;
  isDisable = true;
  // Dropdowns

  rolesList = [];
  employeeList = [];
  // lists
  cdmList = [];
  pageIndex: number = 0;
  cameraProfileId: any;

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private dialogService: DialogService,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService
  ) {}
  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.createCameraInformationForm();
    this.combineLatestNgrxStoreDataOne();
  }
  combineLatestNgrxStoreDataOne() {
    combineLatest([this.store.select(loggedInUserData)])
      .pipe(take(1))
      .subscribe((response) => {
        this.loggedUser = new LoggedInUserModel(response[0]);
      });
  }
  createCameraInformationForm(): void {
    let camModel = new CameraInformationModel(this.config.data);
    this.cameraProfileId = this.config.data
      ? this.config.data.cameraProfileId
      : "";
    this.cameraInfoForm = this.formBuilder.group({});
    Object.keys(camModel).forEach((key) => {
      this.cameraInfoForm.addControl(key, new FormControl(camModel[key]));
    });
    this.cameraInfoForm = setRequiredValidator(this.cameraInfoForm, [
      "cameraProfileName",
      "latLong",
      "location",
    ]);
    this.cameraInfoForm
      .get("latLong")
      .setValidators([
        Validators.required,
        Validators.pattern(
          /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/
        ),
      ]);
    if (this.cameraProfileId) {
      this.cameraInfoForm.disable();
    } else {
      this.isDisable = false;
    }
  }
  enableForm() {
    if(!this.config.data.permission?.edit){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isDisable = false;
    this.cameraInfoForm.enable();
  }
  onSubmit() {
    if (this.cameraInfoForm.invalid) return;

    let obj = this.cameraInfoForm.getRawValue();
    obj.createdUserId = this.loggedUser?.userId;
    obj.communityId = this.config.data.communityId;
    let api;
    api = this.cameraProfileId
      ? this.crudService.update(
          ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
          CustomerModuleApiSuffixModels.CAMERA_PROFILE,
          obj
        )
      : this.crudService.create(
          ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
          CustomerModuleApiSuffixModels.CAMERA_PROFILE_INFO,
          obj
        );
    api.subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.ref.close(false);
      }
    });
  }

  deleteData() {
    if(!this.config.data.permission?.delete){
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.dialogClose();
    const refData = this.dialogService.open(
      PrimengDeleteConfirmDialogComponent,
      {
        showHeader: false,
        baseZIndex: 10000,
        width: "400px",
        data: {
          dataObject: { cameraProfileId: this.cameraProfileId },
          modifiedUserId: this.loggedUser.userId,
          moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
          apiSuffixModel: CustomerModuleApiSuffixModels.CAMERA_PROFILE,
          method: "delete",
        },
      }
    );
    refData.onClose.subscribe((result) => {
      this.ref.close(false);
    });
  }
  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
