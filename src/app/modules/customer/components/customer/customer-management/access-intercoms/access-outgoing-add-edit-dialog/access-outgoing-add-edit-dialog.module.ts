import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';

import { AccessOutgoingAddEditDialogComponent } from './access-outgoing-add-edit-dialog.component';

@NgModule({
  declarations: [AccessOutgoingAddEditDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
  ],
  exports: [AccessOutgoingAddEditDialogComponent],
  entryComponents: [AccessOutgoingAddEditDialogComponent],
})
export class AccessOutgoingAddEditDialogModule { }
