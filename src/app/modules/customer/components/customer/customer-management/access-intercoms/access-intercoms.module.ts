import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { AccessIncomingAddEditDialogModule } from './access-incoming-add-edit-dialog/access-incoming-add-edit-dialog.module';
import { AccessIncomingGateCallModule } from './access-incoming-gate-call/access-incoming-gate-call.module';
import { AccessIntercomsComponent } from './access-intercoms.component';
import { AccessOutgoingAddEditDialogModule } from './access-outgoing-add-edit-dialog/access-outgoing-add-edit-dialog.module';
import { AccessOutgoingGateCallModule } from './access-outgoing-gate-call/access-outgoing-gate-call.module';


@NgModule({
  declarations: [AccessIntercomsComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    MatMenuModule,
    TieredMenuModule,
    AccessIncomingAddEditDialogModule,
    AccessOutgoingAddEditDialogModule,
    AccessOutgoingGateCallModule,
    AccessIncomingGateCallModule,
  ],
  exports: [AccessIntercomsComponent],
  entryComponents: [],
})
export class AccessIntercomsModule { }
