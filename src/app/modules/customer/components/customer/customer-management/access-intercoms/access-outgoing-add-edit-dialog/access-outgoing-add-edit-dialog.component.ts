import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';



@Component({
  selector: 'app-access-outgoing-add-edit-dialog',
  templateUrl: './access-outgoing-add-edit-dialog.component.html',
  styleUrls: ['./access-outgoing-add-edit-dialog.component.scss']
})
export class AccessOutgoingAddEditDialogComponent implements OnInit {


  
  
    showDialogSpinner: boolean = false;
    data: any = [];
    accessIntercomForm: FormGroup
    callDetailsData: any
    loggedUser: any
    cameraTypeList: any = []
    countryCodes = countryCodes;
    formConfigs = formConfigs;
    mobileNumberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
    customerAddressId: string
    customerId: string;
    isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  
    constructor(public ref: DynamicDialogRef, private store: Store<AppState>, private rxjsService: RxjsService, public _fb: FormBuilder, public config: DynamicDialogConfig, private crudService: CrudService) {
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.loggedUser = userData;
      });
      this.rxjsService.getCustomerDate().subscribe(data => {
        this.customerId = data.customerId
        this.customerAddressId = data.addressId
      })
    }
  
    ngOnInit(): void {
      this.createOutgoingForm()
      if (this.config.data) {
        this.getCallDetailsById()
      }
    }
  
  
    createOutgoingForm(): void {
      this.accessIntercomForm = this._fb.group({
        gateAccessOutgoingGateCallId: [''],
        outgoingPhoneNumberCountryCode: ['+27', Validators.required],
        outgoingPhoneNumber: ['', Validators.required],
        specialCharacters: ['', Validators.required],
        description: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(250)]],
        latitude: ['', Validators.required],
        longitude: ['', Validators.required],
        customerId: [''],
        customerAddressId: [''],
        isSMSReturned: [true],
        createdUserId: [''],
        modifiedUserId: ['']
      });
      this.accessIntercomForm = setRequiredValidator(this.accessIntercomForm, ["outgoingPhoneNumber", 'description', "specialCharacters", "latitude", "longitude"]);
      this.accessIntercomForm.get('createdUserId').setValue(this.loggedUser.userId)
      this.accessIntercomForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    }
  
  
    getCallDetailsById() {
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.GATE_ACCESS_OUTGOING_GATE_CALL,
        this.config.data.gateAccessOutgoingGateCallId)
        .subscribe(response => {
          this.rxjsService.setGlobalLoaderProperty(false);
  
          this.callDetailsData = response.resources;
          this.accessIntercomForm.patchValue(response.resources);
          this.accessIntercomForm.get('createdUserId').setValue(this.loggedUser.userId)
          this.accessIntercomForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  
        })
    }
    // btnCloseClick() {
    //   this.rxjsService.setDialogOpenProperty(false);
    //   this.ref.close(false);
    // } 
  
    onSubmit() {
      if (this.accessIntercomForm.invalid) {
        Object.keys(this.accessIntercomForm.controls).forEach((key) => {
          this.accessIntercomForm.controls[key].markAsDirty();
        });
        return
      }
      let formValue = this.accessIntercomForm.value
      if(!this.accessIntercomForm.get('gateAccessOutgoingGateCallId').value){
        formValue.customerId = this.customerId
        formValue.customerAddressId = this.customerAddressId
      }
      formValue.outgoingPhoneNumber = formValue.outgoingPhoneNumber.replace(/ /g,'')
      let crudService = !(this.accessIntercomForm.get('gateAccessOutgoingGateCallId').value) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.GATE_ACCESS_OUTGOING_GATE_CALL, formValue) :
        this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.GATE_ACCESS_OUTGOING_GATE_CALL, formValue)
      crudService.subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.ref.close(true);
        }
      });
  
    }
  
    close() {
      this.ref.close();
    }
  
  }
  
  