import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';


@Component({
  selector: 'app-access-incoming-add-edit-dialog',
  templateUrl: './access-incoming-add-edit-dialog.component.html',
  styleUrls: ['./access-incoming-add-edit-dialog.component.scss']
})
export class AccessIncomingAddEditDialogComponent implements OnInit {


  showDialogSpinner: boolean = false;
  data: any = [];
  accessIntercomForm: FormGroup
  callDetailsData: any
  loggedUser: any
  cameraTypeList: any = []
  countryCodes = countryCodes;
  formConfigs = formConfigs;
  mobileNumberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  customerAddressId: string
  customerId: string;
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true, shouldPasteKeyboardEventBeRestricted: true });

  constructor(public ref: DynamicDialogRef, private store: Store<AppState>, private rxjsService: RxjsService, public _fb: FormBuilder, public config: DynamicDialogConfig, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.rxjsService.getCustomerDate().subscribe(data => {
      this.customerId = data.customerId
      this.customerAddressId = data.addressId
    })
  }

  ngOnInit(): void {
    this.createIncomingForm()
    if (this.config.data) {
      this.getCallDetailsById()
    }
  }


  createIncomingForm(): void {
    this.accessIntercomForm = this._fb.group({
      gateAccessIncomingGateCallId: [''],
      incomingPhoneNumberCountryCode: ['+27', Validators.required],
      incomingPhoneNumber: ['', Validators.required],
      specialCharacters: ['', Validators.required],
      description: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(250)]],
      receivedOnPhoneNumberCountryCode: ['+27', Validators.required],
      receivedOnPhoneNumber: ['', Validators.required],
      latitude: ['', Validators.required],
      longitude: ['', Validators.required],
      customerId: [''],
      customerAddressId: [''],
      isActive: [true],
      createdUserId: [''],
      modifiedUserId: ['']
    });
    this.accessIntercomForm = setRequiredValidator(this.accessIntercomForm, ["incomingPhoneNumber", "specialCharacters", 'description', "receivedOnPhoneNumber", "latitude", "longitude"]);
    this.accessIntercomForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.accessIntercomForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }


  getCallDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.GATE_ACCESS_IMCOMING_GATE_CALL,
      this.config.data.gateAccessIncomingGateCallId)
      .subscribe(response => {
        this.rxjsService.setGlobalLoaderProperty(false);

        this.callDetailsData = response.resources;
        this.accessIntercomForm.patchValue(response.resources);
        this.accessIntercomForm.get('createdUserId').setValue(this.loggedUser.userId)
        this.accessIntercomForm.get('modifiedUserId').setValue(this.loggedUser.userId)

      })
  }

  onSubmit() {
    if (this.accessIntercomForm.invalid) {
      Object.keys(this.accessIntercomForm.controls).forEach((key) => {
        this.accessIntercomForm.controls[key].markAsDirty();
      });
      return
    }
    let formValue = this.accessIntercomForm.value
    if(!this.accessIntercomForm.get('gateAccessIncomingGateCallId').value){
      formValue.customerId = this.customerId
      formValue.customerAddressId = this.customerAddressId
    }
    formValue.incomingPhoneNumber = formValue.incomingPhoneNumber.replace(/ /g,'')
    formValue.receivedOnPhoneNumber = formValue.receivedOnPhoneNumber.replace(/ /g,'')
    let crudService = !(this.accessIntercomForm.get('gateAccessIncomingGateCallId').value) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.GATE_ACCESS_IMCOMING_GATE_CALL, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.GATE_ACCESS_IMCOMING_GATE_CALL, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.ref.close(true);
      }
    });

  }

  close() {
    this.ref.close();
  }

}

