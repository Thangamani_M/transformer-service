import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AccessOutgoingAddEditDialogModule } from '../access-outgoing-add-edit-dialog/access-outgoing-add-edit-dialog.module';

import { AccessOutgoingGateCallComponent } from './access-outgoing-gate-call.component';

@NgModule({
  declarations: [AccessOutgoingGateCallComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    AccessOutgoingAddEditDialogModule
  ],
  exports: [AccessOutgoingGateCallComponent],
  entryComponents: [],
})
export class AccessOutgoingGateCallModule { }
