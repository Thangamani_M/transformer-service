import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AccessIncomingAddEditDialogComponent } from './access-incoming-add-edit-dialog.component';


@NgModule({
  declarations: [AccessIncomingAddEditDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
  ],
  exports: [AccessIncomingAddEditDialogComponent],
  entryComponents: [AccessIncomingAddEditDialogComponent],
})
export class AccessIncomingAddEditDialogModule { }
