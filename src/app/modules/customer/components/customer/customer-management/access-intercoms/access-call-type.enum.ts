export enum AccessCallType {
    INCOMMING_GATE_CALL = 'incoming-gate-call',
    OUTGOING_GATE_CALL = 'outgoing-gate-call'
}