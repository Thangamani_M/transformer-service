import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AccessIncomingAddEditDialogModule } from '../access-incoming-add-edit-dialog/access-incoming-add-edit-dialog.module';
import { AccessIncomingGateCallComponent } from './access-incoming-gate-call.component';


@NgModule({
  declarations: [AccessIncomingGateCallComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    AccessIncomingAddEditDialogModule
  ],
  exports: [AccessIncomingGateCallComponent],
  entryComponents: [],
})
export class AccessIncomingGateCallModule { }
