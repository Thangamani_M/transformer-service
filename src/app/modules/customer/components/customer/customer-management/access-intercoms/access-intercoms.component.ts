import { Component, Input, OnInit } from '@angular/core';
import { CrudType, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { DialogService } from 'primeng/api';
import { AccessCallType } from './access-call-type.enum';
import { AccessIncomingAddEditDialogComponent } from './access-incoming-add-edit-dialog/access-incoming-add-edit-dialog.component';
import { AccessOutgoingAddEditDialogComponent } from './access-outgoing-add-edit-dialog/access-outgoing-add-edit-dialog.component';

@Component({
  selector: 'app-access-intercoms',
  templateUrl: './access-intercoms.component.html',
  styleUrls: ['./access-intercoms.component.scss']
})
export class AccessIntercomsComponent implements OnInit {
  @Input() permission;
  incomingRefresh: number  = 0
  outgoingRefresh: number  = 0

  constructor(private dialogService: DialogService,private rxjsService: RxjsService,private snackbarService : SnackbarService) { }

  ngOnInit(): void {
  }

  onCRUDRequested(type: CrudType | string, dialogType: string): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.permission?.canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        this.rxjsService.setDialogOpenProperty(true);
        this.openAddEditPage(CrudType.CREATE, dialogType);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, callType: string): void {
    switch (callType) {
      case AccessCallType.INCOMMING_GATE_CALL:
        const ref = this.dialogService.open(AccessIncomingAddEditDialogComponent, {
          header: 'Gate Access - Incoming Gate Call',
          showHeader: true,
          baseZIndex: 1000,
          width: '700px',
          data: null,
        });
        ref.onClose.subscribe((result) => {
          if (result) {
            this.incomingRefresh++
          }
        });
        break
      case AccessCallType.OUTGOING_GATE_CALL:
        const ref1 = this.dialogService.open(AccessOutgoingAddEditDialogComponent, {
          header: 'Gate Access - Outgoing Gate Call',
          showHeader: true,
          baseZIndex: 1000,
          width: '700px',
          data: null,
        });
        ref1.onClose.subscribe((result) => {
          if (result) {
            this.outgoingRefresh++
          }
        });
        break
    }


  }

}
