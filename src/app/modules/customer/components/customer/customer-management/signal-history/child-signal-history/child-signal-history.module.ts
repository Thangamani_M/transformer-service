import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ChildSignalHistoryComponent } from "./child-signal-history.component";

@NgModule({
  declarations: [ChildSignalHistoryComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [ChildSignalHistoryComponent],
  entryComponents: [],
})
export class ChildSignalHistoryModule { }