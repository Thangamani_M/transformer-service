
import { Component, Input, OnInit } from '@angular/core';
import { CrudType, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { DialogService } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../..../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
    selector: 'app-signal-history-controller',
    templateUrl: './signal-history-controller.component.html',
})
export class SignalHistoryControllerComponent extends PrimeNgTableVariablesModel implements OnInit {
    @Input() occurrenceBookId: string;
    initialLoad: boolean = false;
    isShowNoRecords = true
    constructor(private crudService: CrudService,
        public dialogService: DialogService,
        private momentService: MomentService,
        private rxjsService: RxjsService,) {
            super();
        this.primengTableConfigProperties = {
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: '',
                        dataKey: 'type',
                        enableBreadCrumb: false,
                        enableExportCSV: false,
                        enableExportExcel: false,
                        enableExportCSVSelected: false,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableStatusActiveAction: false,
                        enableFieldsSearch: false,
                        rowExpantable: false,
                        rowExpantableIndex: 0,
                        enableHyperLink: false,
                        cursorLinkIndex: 0,
                        enableSecondHyperLink: false,
                        cursorSecondLinkIndex: 1,
                        columns: [
                            { field: 'type', header: 'Type', width: '100px' },
                            { field: 'signalDatetime', header: 'Date & Time ', width: '100px' },
                            { field: 'measure', header: 'Measure', width: '100px' },
                            { field: 'stormSignal', header: 'Storm Signal', width: '100px' },
                            { field: 'phoneInSignal', header: 'Phone In Signal', width: '100px' },
                        ],
                        apiSuffixModel: EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_CONTROLLER,
                        moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
                        enableMultiDeleteActionBtn: false,
                        ebableAddActionBtn: false,
                        ebableFilterActionBtn: false
                    },


                ]

            }
        }
    }

    ngOnInit(): void {
        this.getRequiredListData();
    }

    getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        let obj1 = { occurrenceBookId: this.occurrenceBookId };
        if (otherParams) {
            otherParams = { ...otherParams, ...obj1 };
        } else {
            otherParams = obj1;
        }
        this.initialLoad = true;
        this.crudService.get(
            ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_CONTROLLER,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.dataList = data.resources;
                this.totalRecords = data.totalCount;
                this.isShowNoRecords = this.dataList.length !=0 ? false : true

            } else {
                this.dataList = null;
                this.totalRecords = 0;
              this.isShowNoRecords = true
            }
        })
    }

    onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
        switch (type) {
            case CrudType.GET:
                let otherParams = {};
                if (Object.keys(this.row).length > 0) {
                    if (this.row['searchColumns']) {
                        Object.keys(this.row['searchColumns']).forEach((key) => {
                            if (key.toLowerCase().includes('date')) {
                                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
                            } else {
                                otherParams[key] = this.row['searchColumns'][key];
                            }
                        });
                    }
                    if (this.row['sortOrderColumn']) {
                        otherParams['sortOrder'] = this.row['sortOrder'];
                        otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
                    }
                }
                this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar);
        }
    }

    onActionSubmited(e: any) {
        if (e.data && !e.search) {
            this.onCRUDRequested(e.type, e.data)
        } else if (e.data && e.search) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data) {
            this.onCRUDRequested(e.type, {})
        }
    }
}

