import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ZoneSuspendModelModule } from "../zone-suspend-model/zone-suspend-model.module";
import { ChildSignalHistoryModule } from "./child-signal-history/child-signal-history.module";
import { SignalHistoryControllerModule } from "./controller/signal-history-controller.module";
import { SignalHistoryInfoCustomerModule } from "./signal-history-info-customer/signal-history-info-customer.module";
import { SignalHistoryComponent } from "./signal-history.component";

@NgModule({
  declarations: [SignalHistoryComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    SignalHistoryInfoCustomerModule,
    SignalHistoryControllerModule,
    ChildSignalHistoryModule,
    ZoneSuspendModelModule
  ],
  exports: [SignalHistoryComponent],
  entryComponents: [],
})
export class SignalHistoryModule { }