import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SignalHistoryControllerComponent } from "./signal-history-controller.component";

@NgModule({
  declarations: [SignalHistoryControllerComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [SignalHistoryControllerComponent],
  entryComponents: [],
})
export class SignalHistoryControllerModule { }