import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SignalHistoryInfoCustomerComponent } from "./signal-history-info-customer.component";

@NgModule({
  declarations: [SignalHistoryInfoCustomerComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [SignalHistoryInfoCustomerComponent],
  entryComponents: [],
})
export class SignalHistoryInfoCustomerModule { }