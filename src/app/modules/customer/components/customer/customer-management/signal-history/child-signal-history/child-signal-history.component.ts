import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CrudType, debounceTimeForSearchkeyword, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
    selector: 'app-child-signal-history',
    templateUrl: './child-signal-history.component.html',
    styleUrls: ['./child-signal-history.component.scss']
})
export class ChildSignalHistoryComponent extends PrimeNgTableVariablesModel implements OnInit {
    @ViewChildren(Table) tables: QueryList<Table>;
    @Input() occurrenceBookId: string;
    @Output() selectedRowData = new EventEmitter<any>();
    observableResponse;
    dataListScroll: any = [];
    searchForm: FormGroup
    columnFilterForm: FormGroup;
    today: any = new Date()
    searchColumns: any
    row: any = {}
    scrollEnabled: boolean = false;
    @Input() chidlselectedRows: any

    constructor(private crudService: CrudService,
        private activatedRoute: ActivatedRoute,
        public dialogService: DialogService,
        private snackbarService: SnackbarService,
        private momentService: MomentService,
        private rxjsService: RxjsService,
        private _fb: FormBuilder) {
        super();
        this.primengTableConfigProperties = {
            //   tableCaption: "Signal Management",
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: '',
                        dataKey: 'occurrenceBookSignalId',
                        enableBreadCrumb: false,
                        enableExportCSV: false,
                        enableExportExcel: false,
                        enableExportCSVSelected: false,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: true,
                        enableRowDelete: false,
                        enableStatusActiveAction: false,
                        enableFieldsSearch: false,
                        rowExpantable: false,
                        rowExpantableIndex: 0,
                        enableHyperLink: false,
                        cursorLinkIndex: 0,
                        enableSecondHyperLink: false,
                        cursorSecondLinkIndex: 1,
                        columns: [
                            { field: 'occurrenceBookNumber', header: 'OB Number', width: '150px' },
                            { field: 'decoder', header: 'Decoder', width: '100px' },
                            { field: 'accountCode', header: 'Transmitter', width: '100px' },
                            { field: 'signalDateTime', header: 'Time of Signal', width: '150px' },
                            { field: 'alarmType', header: 'Signal', width: '100px' },
                            { field: 'description', header: 'Signal Description', width: '100px' },
                            { field: 'zoneNo', header: 'Zone', width: '100px' },
                            { field: 'zoneDescription', header: 'Zone Description', width: '100px' },
                            { field: 'partitionNo', header: 'Partition Number', width: '100px' },
                            { field: 'eventCode', header: 'Event', width: '100px' },
                            { field: 'operatorName', header: 'Operator', width: '100px' },
                            { field: 'status', header: 'Status', width: '100px' },
                        ],
                        moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
                        apiSuffixModel: EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_SIGNAL_OCCURRENCE_BOOK,
                        enableMultiDeleteActionBtn: false,
                        ebableAddActionBtn: true,
                        shouldShowCreateActionBtn: false,

                    },
                ]
            }
        }
        this.searchForm = this._fb.group({ searchKeyword: "" });
        this.columnFilterForm = this._fb.group({});
        this.activatedRoute.queryParamMap.subscribe((params) => {
            this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
            this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
        });
        this.status = [
            { label: 'Active', value: true },
            { label: 'In-Active', value: false },
        ]
    }

    ngOnInit(): void {
        this.searchKeywordRequest();
        this.columnFilterRequest();
    }

    ngOnChanges() {
        this.row['pageIndex'] = 0
        this.row['pageSize'] = 10
        let otherParams = {}
        this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], otherParams);
    }

    getSelectedRow() {
        this.selectedRowData.emit(this.selectedRows);
    }

    ngAfterViewInit() {
        const scrollableBody = this.tables.first.containerViewChild.nativeElement.getElementsByClassName('ui-table-scrollable-view')[0];
        scrollableBody.onscroll = (x) => {
            this.scrollEnabled = true;
            var st = Math.floor(scrollableBody.scrollTop + scrollableBody.offsetHeight)
            let max = scrollableBody.scrollHeight - 150;
            // if (st == max || (max <= st && (max + (10 * this.row['pageIndex'])) >= st)) {
            if (st >= max) {
                if (this.dataList.length < this.totalRecords) {
                    this.onCRUDRequested(CrudType.GET, this.row);
                }
            }
        }
    }


    searchKeywordRequest() {
        this.searchForm.valueChanges
            .pipe(
                debounceTime(debounceTimeForSearchkeyword),
                distinctUntilChanged(),
                switchMap(val => {
                    return of(this.onCRUDRequested(CrudType.GET, {}));
                })
            )
            .subscribe();
    }

    columnFilterRequest() {
        this.columnFilterForm.valueChanges
            .pipe(
                debounceTime(debounceTimeForSearchkeyword),
                distinctUntilChanged(),
                switchMap(obj => {
                    Object.keys(obj).forEach(key => {
                        if (obj[key] === "") {
                            delete obj[key]
                        }
                    });
                    this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
                    this.row['searchColumns'] = this.searchColumns;
                    if (this.row['searchColumns']) {
                        this.row['pageIndex'] = 0;
                    }
                    this.scrollEnabled = false
                    return of(this.onCRUDRequested(CrudType.GET, this.row));
                })
            )
            .subscribe();
    }

    getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;

        let obj1 = {
            occurrenceBookId: this.chidlselectedRows.occurrenceBookId,
        };
        if (otherParams) {
            otherParams = { ...otherParams, ...obj1 };
        } else {
            otherParams = obj1;
        }
        this.crudService.get(
            ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            EventMgntModuleApiSuffixModels.OCCURRENCE_BOOK_SIGNAL_OCCURRENCE_BOOK,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.observableResponse = data.resources;
                if (!this.scrollEnabled) {
                    this.dataList = []
                    this.dataList = this.observableResponse;
                } else {
                    this.observableResponse.forEach(element => {
                        this.dataList.push(element);
                    });
                }
                this.totalRecords = 0;
                this.row['pageIndex'] = this.row['pageIndex'] + 1

            } else {
                this.observableResponse = null;
                this.dataList = this.observableResponse
                this.totalRecords = 0;

            }
        })
    }

    loadPaginationLazy(event) {
    }


    onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
        switch (type) {
            case CrudType.GET:
                let otherParams = {};
                if (this.searchForm.value.searchKeyword) {
                    otherParams["search"] = this.searchForm.value.searchKeyword;
                }
                if (Object.keys(this.row).length > 0) {
                    // logic for split columns and its values to key value pair

                    if (this.row['searchColumns']) {
                        Object.keys(this.row['searchColumns']).forEach((key) => {
                            if (key.toLowerCase().includes('date')) {
                                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
                            } else {
                                otherParams[key] = this.row['searchColumns'][key];
                            }
                        });
                    }

                    if (this.row['sortOrderColumn']) {
                        otherParams['sortOrder'] = this.row['sortOrder'];
                        otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
                    }
                }
                this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
                break;
        }
    }

    onTabChange(e) { }

    exportExcel() { }

    onChangeStatus(rowData, ri) { }
}
