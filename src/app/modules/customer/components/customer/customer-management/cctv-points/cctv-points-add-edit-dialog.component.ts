import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
declare var google: any;
@Component({
  selector: 'app-cctv-points-add-edit-dialog',
  templateUrl: './cctv-points-add-edit-dialog.component.html',
  styleUrls: ['./cctv-points-add-edit-dialog.component.scss']
})
export class CctvPointsAddEditDialogComponent implements OnInit {

  showDialogSpinner: boolean = false;
  data: any = [];
  cctvPointForm: FormGroup
  callDetailsData: any
  loggedUser: any
  cameraTypeList: any = [];
  options: any;
  overlays: any;
  openMapDialog: boolean = false;
  map: any;

  constructor(public ref: DynamicDialogRef, private store: Store<AppState>, private rxjsService: RxjsService, public _fb: FormBuilder, public config: DynamicDialogConfig, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.map = google.maps.Map;
  }

  ngOnInit(): void {
    this.createForm()
    this.getCctvPoiintbyId()
    this.options = {
      center: {
        lat: -25.746020,
        lng: 28.187120
      },
      zoom: 11,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
  }

  openMap() {
   
    this.options = {};
    this.overlays = {};
    let location = [ -25.746020,28.187120]
    this.openMapDialog = true
    if (!location) {
     // location = location.split(",");
    } else {
      location = []
      location[0] = -25.746020 // fidelity south afcia coordinates by default
      location[1] = 28.187120 // fidelity south afcia coordinates by default
    }
    this.options = {
      center: { lat: Number(location[0]), lng: Number(location[1]) },
      zoom: 12
    };

    setTimeout(() => {
      this.map.setCenter({
        lat: Number(location[0]),
        lng: Number(location[1])
      });
    }, 500);

    this.overlays = [new google.maps.Marker({ position: { lat: Number(location[0]), lng: Number(location[1]) }, icon: "assets/img/map-icon.png" })];
  }
 

  handleMapClick(event) {
    //event: MouseEvent of Google Maps api
    this.cctvPointForm.get('latitude').setValue(event.latLng.lat())
    this.cctvPointForm.get('longitude').setValue(event.latLng.lng())
    this.openMapDialog = false
    this.rxjsService.setFormChangeDetectionProperty(true)

  }
 

  setMap(event) {
    this.map = event.map;
  }
 

  btnCloseClick() {
    this.rxjsService.setDialogOpenProperty(false);
    this.ref.close(false);
  }

  filterSingle(event){
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CCTV_POINTS_CAMERA_TYPE,
      event.query)
      .subscribe(response => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.cameraTypeList = response.resources;
        // if(response.resources.length == 0){
        //   this.cctvPointForm.get('cameraType').setValue(null)
        // }
      })
  }

  createForm(): void {
    this.cctvPointForm = this._fb.group({
      cctvPointId: [''],
      cameraId: ['', Validators.required],
      cameraName: ['', Validators.required],
      cameraType: ['', Validators.required],
      latitude: ['', Validators.required],
      longitude: ['', Validators.required],
      isActive: [true],
      createdUserId: [''],
      modifiedUserId: ['']
    });
    this.cctvPointForm = setRequiredValidator(this.cctvPointForm, ["cameraId", "cameraName", "cameraType", "latitude", "longitude"]);
    this.cctvPointForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.cctvPointForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }


  getCctvPoiintbyId() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CCTV_POINTS,
      this.config.data.cctvPointId)
      .subscribe(response => {
        this.rxjsService.setGlobalLoaderProperty(false);

        this.callDetailsData = response.resources;
        this.cctvPointForm.patchValue(response.resources);
        if(response.resources.cameraType){
          this.cameraTypeList = [{cameraType:response.resources.cameraType}];
          this.cctvPointForm.get('cameraType').setValue({cameraType:response.resources.cameraType});
        }
        this.cctvPointForm.get('createdUserId').setValue(this.loggedUser.userId)
        this.cctvPointForm.get('modifiedUserId').setValue(this.loggedUser.userId)

      })
  }

  onSubmit() {
    if (this.cctvPointForm.invalid) {
      Object.keys(this.cctvPointForm.controls).forEach((key) => {
        this.cctvPointForm.controls[key].markAsDirty();
      });
      return
    }
    let formValue = this.cctvPointForm.value
    formValue.cameraType = (typeof formValue.cameraType === 'string') ? formValue.cameraType : formValue.cameraType.cameraType
    let crudService = !(this.cctvPointForm.get('cctvPointId').value) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CCTV_POINTS, this.cctvPointForm.value) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CCTV_POINTS, this.cctvPointForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.ref.close(true);
      }
    });

  }

  close() {
    this.ref.close();
  }

}

