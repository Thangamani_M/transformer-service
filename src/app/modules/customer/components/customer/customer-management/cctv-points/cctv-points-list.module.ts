import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CctvPointsAddEditDialogModule } from "./cctv-points-add-edit-dialog.module";
import { CctvPointsListComponent } from "./cctv-points-list.component";


@NgModule({
    declarations: [CctvPointsListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    CctvPointsAddEditDialogModule,
  ],
  entryComponents: [],
  exports: [CctvPointsListComponent],
  providers: []
})
export class CctvPointsListModule { }