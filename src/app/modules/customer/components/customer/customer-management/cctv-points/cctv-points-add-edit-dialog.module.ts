import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { GMapModule } from "primeng/gmap";
import { CctvPointsAddEditDialogComponent } from "./cctv-points-add-edit-dialog.component";


@NgModule({
    declarations: [CctvPointsAddEditDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    GMapModule,
  ],
  entryComponents: [CctvPointsAddEditDialogComponent],
  exports: [CctvPointsAddEditDialogComponent],
  providers: []
})
export class CctvPointsAddEditDialogModule { }