import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { DialogService } from 'primeng/api';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { ERBExclusionRequestAddEditComponent } from './erb-exclusion-request-add-edit.component';
@Component({
  selector: 'app-erb-exclusion-request',
  templateUrl: './erb-exclusion-request.component.html',
})
export class ERBExclusionRequestComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  @Input() partitionId;
  @Input() customerAddressId;
  @Input() customerId;
  @Input() permission;
  dataList: any;

  constructor(private rxjsService: RxjsService, private datePipe: DatePipe,
    private crudService: CrudService, private momentService: MomentService, public dialogService: DialogService,
    private snackbarService : SnackbarService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' },],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'erbExclusionRequestId',
            captionFontSize: '21px',
            enableBreadCrumb: false,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'reasonName', header: 'Reason', width: '150px' },
              { field: 'comments', header: 'Comment', width: '150px' },
              { field: 'erbExclusionTillDate', header: 'Till Date', width: '200px' },
              { field: 'createdDate', header: 'Created Date', width: '200px' },
              { field: 'statusName', header: 'Status   ', width: '150px' },],
            apiSuffixModel: EventMgntModuleApiSuffixModels.ERB_EXCLUSION_REQUEST,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      let otherParams = {}
      if (this.customerId && this.customerAddressId) {
        this.getRequiredListData(null, null, otherParams);
      }
    }

  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.partitionId == '') {
      const params = { customerId: this.customerId, CustomerAddressId: this.customerAddressId }
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { customerId: this.customerId, CustomerAddressId: this.customerAddressId, partitionId: this.partitionId }
      otherParams = { ...otherParams, ...params };
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.erbExclusionTillDate = val.erbExclusionTillDate ? this.datePipe.transform(val.erbExclusionTillDate, 'dd-MM-yyyy h:mm a') : '';
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.selectedRows = []
        data.resources.forEach(element => {
          if (element.isActive) {
            this.selectedRows.push(element)
          }
        });
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    });
  }



  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.CREATE:
        if (!this.permission?.canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        this.rxjsService.setDialogOpenProperty(true);
        this.openAddEditPage(CrudType.CREATE, row)
        break
      case CrudType.VIEW:
        if (!this.permission?.canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        this.rxjsService.setDialogOpenProperty(true);
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

    openAddEditPage(type: CrudType | string, editableObject?: any | string): void {
      const ref = this.dialogService.open(ERBExclusionRequestAddEditComponent, {
         showHeader: false,
        baseZIndex: 1000,
        width: '750px',
        data: {...editableObject,header: 'ERB Exclusion Request',},
      });
      ref.onClose.subscribe((result) => {
        if (result) {
          this.getRequiredListData()
        }
      });
    }


  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}
