import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ERBExclusionRequestAddEditComponent } from "./erb-exclusion-request-add-edit.component";


@NgModule({
    declarations: [ERBExclusionRequestAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  entryComponents: [ERBExclusionRequestAddEditComponent],
  exports: [ERBExclusionRequestAddEditComponent],
  providers: []
})
export class ERBExclusionRequestAddEditModule { }