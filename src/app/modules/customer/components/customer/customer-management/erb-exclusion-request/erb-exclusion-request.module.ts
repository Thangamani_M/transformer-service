import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ERBExclusionRequestAddEditModule } from "./erb-exclusion-request-add-edit.module";
import { ERBExclusionRequestComponent } from "./erb-exclusion-request.component";


@NgModule({
    declarations: [ERBExclusionRequestComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    ERBExclusionRequestAddEditModule,
  ],
  entryComponents: [],
  exports: [ERBExclusionRequestComponent],
  providers: []
})
export class ERBExclusionRequestModule { }