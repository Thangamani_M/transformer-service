import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { ERBExclusionRequestModel } from '@modules/customer/models/erb-exclusion-request-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-erb-exclusion-request-add-edit',
  templateUrl: './erb-exclusion-request-add-edit.component.html',
  styleUrls: ['./erb-exclusion-request-add-edit.component.scss']
})
export class ERBExclusionRequestAddEditComponent implements OnInit {

  erbExclusionRequestForm: FormGroup
  erbExclusionRequestData: any
  loggedUser: any
  erbExclusionRequestId: any;
  customerAddressId: any;
  customerData: any;
  exclusionReasonConfigList: any = [];
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  startTodayDate = new Date();

  constructor(public ref: DynamicDialogRef, private httpCancelService: HttpCancelService,
    private formBuilder: FormBuilder, private store: Store<AppState>, private rxjsService: RxjsService,
    public _fb: FormBuilder, public config: DynamicDialogConfig, private crudService: CrudService,
    private momentService: MomentService,) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.rxjsService.getCustomerAddresId()
      .subscribe(data => {
        this.customerAddressId = data
      })
    this.rxjsService.getCustomerDate()
      .subscribe(data => {
        this.customerData = data
      })
  }

  ngOnInit(): void {
    this.createForm();
    this.getExclusionReasonConfig();
    if (this.config?.data?.erbExclusionRequestId) {
      this.getERBExclusionRequestDetails();
    }

  }


  btnCloseClick() {
    this.rxjsService.setDialogOpenProperty(false);
    this.ref.close(false);
  }

  createForm(): void {
    let eRBExclusionRequestModel = new ERBExclusionRequestModel();
    this.erbExclusionRequestForm = this.formBuilder.group({
    });
    Object.keys(eRBExclusionRequestModel).forEach((key) => {
      this.erbExclusionRequestForm.addControl(key, new FormControl(eRBExclusionRequestModel[key]));
    });
    this.erbExclusionRequestForm = setRequiredValidator(this.erbExclusionRequestForm, ["erbExclusionReasonConfigId", "erbExclusionTillDate", "comments"]);
    this.erbExclusionRequestForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.erbExclusionRequestForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (!this.erbExclusionRequestId) {
      this.erbExclusionRequestForm.removeControl('modifiedUserId');
    } else {
      this.erbExclusionRequestForm.removeControl('createdUserId');
    }
  }

  getExclusionReasonConfig() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ERB_EXCLUSION_REASON, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.exclusionReasonConfigList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getERBExclusionRequestDetails() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.ERB_EXCLUSION_REQUEST,
      this.config?.data?.erbExclusionRequestId,
      null,
      prepareGetRequestHttpParams(null, null, {
        customerId: this.customerData.customerId,
        customerAddressId: this.customerAddressId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.erbExclusionRequestData = res.resources;
          // this.erbExclusionRequestForm.patchValue(res.resources);
          this.erbExclusionRequestForm.get('isParmanent').setValue(res.resources.isPermanent)
          this.erbExclusionRequestForm.get('erbExclusionTillDate').setValue(new Date(res.resources.erbExclusionTillDate))
          this.erbExclusionRequestForm.get('erbExclusionReasonConfigId').setValue(res.resources.erbExclusionReasonConfigId)
          this.erbExclusionRequestForm.get('comments').setValue(res.resources.comments)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }


  onSubmit() {
    if (this.erbExclusionRequestForm.invalid) {
      return
    }
    let formValue = this.erbExclusionRequestForm.value;
    formValue.customerId = this.customerData.customerId;
    formValue.customerAddressId = this.customerAddressId;
    formValue.erbExclusionRequestId = this.config?.data?.erbExclusionRequestId;
    formValue.erbExclusionTillDate = this.momentService.convertToUTC(this.erbExclusionRequestForm.get('erbExclusionTillDate').value);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService = (!this.config?.data?.erbExclusionRequestId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_EXCLUSION_REQUEST, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ERB_EXCLUSION_REQUEST, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.ref.close(true);
      }
    });

  }

  close() {
    this.ref.close();
  }

}

