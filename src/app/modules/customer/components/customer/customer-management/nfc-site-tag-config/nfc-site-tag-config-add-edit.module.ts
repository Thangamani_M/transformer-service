import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GMapModule } from "primeng/gmap";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NfcSiteTagConfigAddEditComponent } from "./nfc-site-tag-config-add-edit.component";

@NgModule({
  declarations: [NfcSiteTagConfigAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    GMapModule,
  ],
  exports: [NfcSiteTagConfigAddEditComponent],
  entryComponents: [NfcSiteTagConfigAddEditComponent],
})
export class NfcSiteTagConfigAddEditModule { }