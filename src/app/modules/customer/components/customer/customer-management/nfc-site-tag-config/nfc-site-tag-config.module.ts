import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NfcSiteTagConfigAddEditModule } from "./nfc-site-tag-config-add-edit.module";
import { NfcSiteTagConfigComponent } from "./nfc-site-tag-config.component";

@NgModule({
  declarations: [NfcSiteTagConfigComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    NfcSiteTagConfigAddEditModule,
  ],
  exports: [NfcSiteTagConfigComponent],
  entryComponents: [],
})
export class NfcSiteTagConfigModule { }