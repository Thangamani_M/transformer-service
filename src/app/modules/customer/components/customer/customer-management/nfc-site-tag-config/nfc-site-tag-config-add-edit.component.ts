import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { NFCSiteTagConfigModel } from '@modules/customer/models/nfc-site-tag-config.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
declare var google: any;
@Component({
  selector: 'app-nfc-site-tag-config-add-edit',
  templateUrl: './nfc-site-tag-config-add-edit.component.html',
  styleUrls: ['./nfc-site-tag-config-add-edit.component.scss'],
})
export class NfcSiteTagConfigAddEditComponent implements OnInit {

  data: any = [];
  nfcSiteTagConfigForm: FormGroup
  nfcSiteTagConfigData: any
  loggedUser: any
  customerNFCSiteTagConfigId: any;
  customerAddressId: any;
  customerData: any;
  options: any;
  overlays: any;
  openMapDialog: boolean = false;
  map: any;

  constructor(public ref: DynamicDialogRef, private httpCancelService: HttpCancelService, private formBuilder: FormBuilder, private store: Store<AppState>, private rxjsService: RxjsService, public _fb: FormBuilder, public config: DynamicDialogConfig, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.map = google.maps.Map;
    this.rxjsService.getCustomerAddresId()
      .subscribe(data => {
        this.customerAddressId = data
      })
    this.rxjsService.getCustomerDate()
      .subscribe(data => {
        this.customerData = data
      })
  }

  ngOnInit(): void {
    this.createForm();
    this.options = {
      center: {
        lat: -25.746020,
        lng: 28.187120
      },
      zoom: 11,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    if (this.config?.data?.customerNFCSiteTagConfigId) {
      this.getNfcsiteTagConfigDetails();
    }
  }

  createForm(): void {
    let nfcSiteTagConfig = new NFCSiteTagConfigModel();
    this.nfcSiteTagConfigForm = this.formBuilder.group({
    });
    Object.keys(nfcSiteTagConfig).forEach((key) => {
      this.nfcSiteTagConfigForm.addControl(key, new FormControl(nfcSiteTagConfig[key]));
    });
    this.nfcSiteTagConfigForm = setRequiredValidator(this.nfcSiteTagConfigForm, ['tagId', 'description', 'location', 'latitude', 'longitude']);
    this.nfcSiteTagConfigForm.get('createdUserId').setValue(this.loggedUser.userId);
    this.nfcSiteTagConfigForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }
  btnCloseClick() {
    this.rxjsService.setDialogOpenProperty(false);
    this.ref.close(false);
  }

  openMap() {
    this.options = {};
    this.overlays = {};
    let location = [-25.746020, 28.187120]
    this.openMapDialog = true
    if (!location) {
    } else {
      location = []
      location[0] = -25.746020 // fidelity south afcia coordinates by default
      location[1] = 28.187120 // fidelity south afcia coordinates by default
    }
    this.options = {
      center: { lat: Number(location[0]), lng: Number(location[1]) },
      zoom: 12
    };

    setTimeout(() => {
      this.map.setCenter({
        lat: Number(location[0]),
        lng: Number(location[1])
      });
    }, 500);

    this.overlays = [new google.maps.Marker({ position: { lat: Number(location[0]), lng: Number(location[1]) }, icon: "assets/img/map-icon.png" })];
  }

  handleMapClick(event) {
    //event: MouseEvent of Google Maps api
    this.nfcSiteTagConfigForm.get('latitude').setValue(event.latLng.lat())
    this.nfcSiteTagConfigForm.get('longitude').setValue(event.latLng.lng())
    this.openMapDialog = false
    this.rxjsService.setFormChangeDetectionProperty(true)

  }

  setMap(event) {
    this.map = event.map;
  }

  getNfcsiteTagConfigDetails() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_NFC_SITE_TAG_CONFIG,
      this.config?.data?.customerNFCSiteTagConfigId)
      .subscribe(response => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.nfcSiteTagConfigData = response.resources;
        this.nfcSiteTagConfigForm.patchValue(response.resources);
      })
  }

  onSubmit() {
    if (this.nfcSiteTagConfigForm.invalid) {
      return
    }
    let formValue = this.nfcSiteTagConfigForm.value;
    formValue.customerId = this.customerData.customerId;
    formValue.customerAddressId = this.customerAddressId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService = !(this.config?.data?.customerNFCSiteTagConfigId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_NFC_SITE_TAG_CONFIG, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_NFC_SITE_TAG_CONFIG, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.ref.close(true);
      }
    });

  }

  close() {
    this.ref.close();
  }

}
