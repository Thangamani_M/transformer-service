import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ACCEPTABLE_ALL_MAJOR_FILE_EXTENSIONS, addFormControls, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, removeFormControls, ResponseMessageTypes, setRequiredValidator, SnackbarService, WRONG_FILE_EXTENSION_SELECTION_ERROR } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { BalanceOfContractDocumentCaptureModel as PurchaseOfAlarmSystemDocumentCaptureModel } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';

declare let $;
@Component({
    selector: 'app-purchase-of-alarm-systemt-document-capture',
    templateUrl: './purchase-of-alarm-system-document-capture.component.html',
    styleUrls: ['./purchase-of-alarm-system.component.scss']
})
export class PurchaseOfAlarmSystemDocumentCaptureComponent implements OnInit {
    documentUploadForm: FormGroup;
    accept = ACCEPTABLE_ALL_MAJOR_FILE_EXTENSIONS;
    loggedInUserData: LoggedInUserModel;
    isFormSubmittedSuccessfully = false;
    message = 'Are you sure you want to submit this request for DOA Approval?';
    @ViewChild('approval_request', { static: false }) approval_request: ElementRef<any>;
    payload;
    documentationTypes = [];
    balanceOfContractDocuments = [];
    formConfigs = formConfigs;
    formData = new FormData();
    @ViewChild('selectField', { static: false }) selectField;
    @ViewChild('inputField', { static: false }) inputFileUploadField;
    @ViewChild('textAreaField', { static: false }) textAreaField;
    uploadedBlobFiles: File[] = [];
    uploadedFile: File;
    feature: string;
    featureIndex: string;

    constructor(private formBuilder: FormBuilder,
        private rxjsService: RxjsService,
        private crudService: CrudService,
        private httpCancelService: HttpCancelService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private snackbarService: SnackbarService,
        private store: Store<AppState>) {
            this.feature = this.activatedRoute.snapshot.params?.feature_name;
            this.featureIndex = this.activatedRoute.snapshot.params?.featureIndex;
    }

    ngOnInit(): void {
        this.getForkJoinRequests();
        this.combineLatestNgrxStoreData();
        this.onBootstrapModalEventChanges();
        this.createUploadForm();
    }

    getForkJoinRequests(): void {
        forkJoin([
            this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_DOCUMENT_TYPE_FOR_FILE_UPLOAD, null, false,
                prepareRequiredHttpParams({
                    enumName: 'PURCHASEALARMSYSTEM'
                }))])
            .subscribe((response: IApplicationResponse[]) => {
                response.forEach((respObj: IApplicationResponse, ix: number) => {
                    if (respObj.isSuccess && respObj.statusCode == 200) {
                        switch (ix) {
                            case 0:
                                this.documentationTypes = respObj.resources;
                                break;
                        }
                    }
                });
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    onBootstrapModalEventChanges(): void {
        $("#approval_request").on("shown.bs.modal", (e) => {
            this.rxjsService.setDialogOpenProperty(true);
        });
        $("#approval_request").on("hidden.bs.modal", (e) => {
            this.rxjsService.setDialogOpenProperty(false);
            this.documentUploadForm = addFormControls(this.documentUploadForm, [{ controlName: 'documentConfigId', defaultValue: '' }, 'documentName']);
            this.documentUploadForm = setRequiredValidator(this.documentUploadForm, ['documentConfigId', 'documentName']);
            this.rxjsService.setFormChangeDetectionProperty(true);
        });
    }

    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(loggedInUserData),
            this.rxjsService.getAnyPropertyValue()]
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
            this.payload = response[1];
        });
    }

    createUploadForm() {
        let purchaseOfAlarmSystemDocumentCaptureModel = new PurchaseOfAlarmSystemDocumentCaptureModel();
        this.documentUploadForm = this.formBuilder.group({});
        Object.keys(purchaseOfAlarmSystemDocumentCaptureModel).forEach((key) => {
            this.documentUploadForm.addControl(key, new FormControl(purchaseOfAlarmSystemDocumentCaptureModel[key]));
        });
        this.documentUploadForm = setRequiredValidator(this.documentUploadForm, ['notes']);
    }

    uploadFile(event) {
        let uploadedFile = event.target.files[0];
        this.uploadedFile = event.target.files[0];
        const path = uploadedFile.name.split('.');
        const extension = "." + path[path.length - 1];
        if (!this.accept.includes(extension)) {
            this.snackbarService.openSnackbar(WRONG_FILE_EXTENSION_SELECTION_ERROR, ResponseMessageTypes.WARNING);
            return;
        }
        this.documentUploadForm.get('documentName').setValue(uploadedFile['name']);
    }

    addDocument() {
        this.documentUploadForm = setRequiredValidator(this.documentUploadForm, ['documentConfigId', 'documentName']);
        if (this.documentUploadForm.invalid) {
            this.selectField.nativeElement.focus();
            this.selectField.nativeElement.blur();
            this.inputFileUploadField.nativeElement.focus();
            this.inputFileUploadField.nativeElement.blur();
            this.textAreaField.nativeElement.focus();
            this.textAreaField.nativeElement.blur();
            return;
        }
        let isDuplicatedRecord = false;
        for (let obj of this.balanceOfContractDocuments) {
            if (this.documentUploadForm.value.documentName == obj.documentName) {
                isDuplicatedRecord = true;
                break;
            }
        }
        if (isDuplicatedRecord) {
            this.snackbarService.openSnackbar('The uploaded file already exists..!!', ResponseMessageTypes.WARNING);
            this.documentUploadForm.get('documentName').setValue('');
        }
        else {
            this.uploadedBlobFiles.push(this.uploadedFile);
            let balanceOfContractDocumentObj = JSON.parse(JSON.stringify(this.documentUploadForm.value));
            delete balanceOfContractDocumentObj.notes;
            this.balanceOfContractDocuments.push(balanceOfContractDocumentObj);
            this.documentUploadForm.reset(new PurchaseOfAlarmSystemDocumentCaptureModel({ notes: this.documentUploadForm.value.notes }));
        }
    }

    removeDocument(index: number) {
        this.balanceOfContractDocuments.splice(index, 1);
        this.uploadedBlobFiles.splice(index, 1);
    }

    onSubmit(): void {
        if (this.documentUploadForm.get('notes').invalid) {
            return;
        }
        this.documentUploadForm = removeFormControls(this.documentUploadForm, ['documentConfigId', 'documentName']);
        $(this.approval_request.nativeElement).modal('show');
    }

    onSubmitAfterConfirmation() {
        this.formData = new FormData();
        this.payload['balanceOfContractDocuments'] = this.balanceOfContractDocuments;
        this.payload['createdUserId'] = this.loggedInUserData.userId;
        this.payload['notes'] = this.documentUploadForm.get('notes').value;
        this.payload.monthlyAmmortizationAmount = this.payload.purchaseOfAlarmSystemAmountDetail.monthlyAmmortizationAmount;
        this.payload.ammortizationReleaseAmount = this.payload.purchaseOfAlarmSystemAmountDetail.ammortizationReleaseAmount;
        this.payload.cancellationAmount = this.payload.purchaseOfAlarmSystemAmountDetail.cancellationAmount;
        this.payload.cancellationPercentage = this.payload.purchaseOfAlarmSystemAmountDetail.cancellationPercentage;
        this.payload.averageRPUAmount = this.payload.purchaseOfAlarmSystemAmountDetail.averageRPUAmount;
        this.payload.averageRPUPercentage = this.payload.purchaseOfAlarmSystemAmountDetail.averageRPUPercentage;
        this.formData.append('payload', JSON.stringify(this.payload));
        this.uploadedBlobFiles.forEach((file) => {
            this.formData.append('file', file);
        });
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS,
            CollectionModuleApiSuffixModels.PURCHASE_OF_ALARM_SYSTEM_CANCELLATION, this.formData)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                    this.isFormSubmittedSuccessfully = true;
                    this.message = 'Request has been submitted for DOA Approval';
                }
                this.rxjsService.setPopupLoaderProperty(false);
            });
    }

    onAcceptedResponse() {
        $(this.approval_request.nativeElement).modal('hide');
        let queryParams = {addressId: this.payload.addressId, selectedTabIndex: 6};
        if(this.feature && this.featureIndex) {
            queryParams['feature_name'] = this.feature;
            queryParams['featureIndex'] = this.featureIndex;
        }
        this.rxjsService.setViewCustomerData({
            customerId: this.payload.customerId,
            addressId: this.payload.addressId,
            customerTab: 0,
            monitoringTab: null,
            feature_name: this.feature,
            featureIndex: this.featureIndex,
        })
        this.rxjsService.navigateToViewCustomerPage();
    }

    backToBalanceOfContractPage() {
        this.router.navigate(['/customer/manage-customers/purchase-of-alarm-system'], {
            queryParams: {
                contractId: this.payload.contractId,
                serviceId: this.payload.serviceId, customerId: this.payload.customerId,
                isCancel:true
            }
        });
    }
}
