import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { PurchaseOfAlarmSystemComponent, PurchaseOfAlarmSystemDocumentCaptureComponent } from "@modules/customer/components/customer/customer-management/purchase-of-alarm-system";

const routes = [
  {
    path: '', component: PurchaseOfAlarmSystemComponent, data: { title: 'Purchase Of Alarm System' }, canActivate: [AuthGuard]
  },
  {
    path: 'capture-documents', component: PurchaseOfAlarmSystemDocumentCaptureComponent, data: { title: 'Purchase Of Alarm System Capture Documents' }, canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [PurchaseOfAlarmSystemComponent,PurchaseOfAlarmSystemDocumentCaptureComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild(routes)
  ],
  exports: [],
  entryComponents: [],
})
export class PurchaseOfAlarmSystemModule { }
