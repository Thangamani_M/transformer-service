import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CustomDirectiveConfig, formConfigs,
  IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, SnackbarService
} from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import {
  loggedInUserData
} from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
@Component({
  selector: 'app-purchase-of-alarm-system',
  templateUrl: './purchase-of-alarm-system.component.html',
  styleUrls: ['./purchase-of-alarm-system.component.scss']
})

export class PurchaseOfAlarmSystemComponent {
  formConfigs = formConfigs;
  loggedInUserData: LoggedInUserModel;
  numberWithDecimalConfig = new CustomDirectiveConfig({ isADecimalOnly: true });
  purchaseOfAlarmSystemDetail: any = {
    contractStartDate: '', contractEndDate: '',
    cancellationDate: null, ownershipType: 'Rented', contractPeriod: '',
    isScheduleAlarmSystemRemovalAppointment: null, isScheduleAlarmSystemRemovalAppointmentActualValue: null
  };
  purchaseOfAlarmSystemAmountDetail: any = {
    monthlyAmmortizationAmount: 0,
    ammortizationReleaseAmount: 0, cancellationAmount: 0, cancellationPercentage: '0',
    averageRPUAmount: 0, averageRPUPercentage: '0'
  };
  queryParams = {
    customerId: '', serviceId: '', contractId: '', addressId: '', feature: '', featureIndex: ''
  }
  alarmSystemModelTypeId = '';
  modelTypes = [];
  isFormSubmitted = false;
  isRPUValueInvalid = false;
  isRPUValueRequired = false;
  purchaseOfAlarmSystemObj;
  isCancel:boolean=false;
  constructor(private crudService: CrudService,
    public rxjsService: RxjsService, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute) {
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.activatedRoute.queryParams]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.queryParams.customerId = response[1]['customerId'];
      this.queryParams.contractId = response[1]['contractId'];
      this.queryParams.serviceId = response[1]['serviceId'];
      this.queryParams.addressId = response[1]['addressId'];
      this.queryParams.feature = response[1]['feature_name'];
      this.queryParams.featureIndex = response[1]['featureIndex'];
      this.getForkJoinRequests();
    });
  }

  ngOnInit(): void {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.isCancel = (Object.keys(params['params']).length > 0) ? params['params']['isCancel'] : false;
    });
    this.combineLatestNgrxStoreData();
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PURCHASE_OF_ALARM_SYSTEM_CANCELLATION_DETAIL, null, false,
        prepareRequiredHttpParams({
          contractId: this.queryParams.contractId,
          serviceId: this.queryParams.serviceId
        })), this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_ALARM_SYSTEM_MODEL_TYPE),
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.BILL_OF_SERVICE_STOCK_DETAILS, null, null, prepareRequiredHttpParams({
          customerId: this.queryParams.customerId, siteAddressId: this.queryParams.addressId, serviceId: this.queryParams.serviceId,
          contractDbId: this.queryParams.contractId
        }))])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200) {
            switch (ix) {
              case 0:
                this.purchaseOfAlarmSystemDetail = respObj.resources;
                this.purchaseOfAlarmSystemDetail.contractStartDate = new Date(respObj.resources.contractStartDate);
                this.purchaseOfAlarmSystemDetail.contractEndDate = new Date(respObj.resources.contractEndDate);
                if(respObj.resources.cancellationDate){
                  this.purchaseOfAlarmSystemDetail.cancellationDate = new Date(respObj.resources.cancellationDate);
                }
                else{
                  this.snackbarService.openSnackbar('Terminate the Service first in order to proceed further..!!', ResponseMessageTypes.WARNING);
                }
                if (this.purchaseOfAlarmSystemDetail.isScheduleAlarmSystemRemovalAppointment) {
                  this.snackbarService.openSnackbar('Radio Removal Appointment is Scheduled', ResponseMessageTypes.WARNING);
                }
                break;
              case 1:
                this.modelTypes = respObj.resources;
                break;
              case 2:
                this.purchaseOfAlarmSystemObj = {
                  isPurchaseOfAlarmApproved: respObj.resources.isPurchaseOfAlarmApproved,
                  isPurchaseOfAlarmCreated: respObj.resources.isPurchaseOfAlarmCreated,
                  isScheduleAlarmSystemRemovalAppointment: respObj.resources.isScheduleAlarmSystemRemovalAppointment,
                  ownershipType: respObj.resources.ownershipType
                }
                break;
            }
          }
          setTimeout(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          });
        });
      });
  }

  getQueryParms(){
    let queryParams = {addressId: this.queryParams?.addressId,selectedTabIndex: 6};
    if(this.queryParams?.feature && this.queryParams?.featureIndex) {
      queryParams['feature_name'] = this.queryParams?.feature;
      queryParams['featureIndex'] = this.queryParams?.featureIndex;
    }
    return queryParams;
  }

  onModelTypeValueChanged() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PURCHASE_OF_ALARM_CANCELLATION_FEE, null, false,
      prepareRequiredHttpParams({
        alarmSystemModelTypeId: this.alarmSystemModelTypeId,
        contractPeriodId: this.purchaseOfAlarmSystemDetail.contractPeriodId,
        regionName: this.purchaseOfAlarmSystemDetail.regionName,
        contractCompletedMonths: this.purchaseOfAlarmSystemDetail.contractCompletedMonths
      })).subscribe((response) => {
        if (response.statusCode == 200 && response.isSuccess && response.resources) {
          this.purchaseOfAlarmSystemAmountDetail = response.resources;
          this.isRPUValueRequired = true;
          this.isRPUValueInvalid = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onAverageRPUValueChanged() {
    setTimeout(() => {
      let rpuAmount = this.purchaseOfAlarmSystemAmountDetail.averageRPUAmount;
      if (rpuAmount) {
        if (rpuAmount == '0' || rpuAmount == '00' || rpuAmount == '0.0' ||
          rpuAmount == '0.00' || rpuAmount == '00.0' || rpuAmount == '00.00' ||
          rpuAmount == '00.' || rpuAmount == '0.' || rpuAmount == '100') {
          this.isRPUValueRequired = false;
          this.isRPUValueInvalid = true;
        }
        else {
          this.isRPUValueInvalid = false;
          this.isRPUValueRequired = false;
          this.purchaseOfAlarmSystemAmountDetail.averageRPUPercentage =
            (this.purchaseOfAlarmSystemAmountDetail.cancellationAmount / this.purchaseOfAlarmSystemAmountDetail.averageRPUAmount).toFixed(2);
        }
        if (rpuAmount.includes('00.00') || rpuAmount.includes('0.00')) {
          this.purchaseOfAlarmSystemAmountDetail.averageRPUAmount = '0.00';
        }
      }
      else {
        this.isRPUValueRequired = true;
        this.isRPUValueInvalid = false;
      }
    });
  }

  onSubmit(type: string): void {
    this.isFormSubmitted = true;
    if (!this.alarmSystemModelTypeId || this.isRPUValueInvalid || this.isRPUValueRequired) {
      return;
    }
    let payload: any = {};
    payload.customerId = this.queryParams?.customerId;
    payload.contractId = this.queryParams?.contractId;
    payload.serviceId = this.queryParams?.serviceId;
    payload.alarmSystemModelTypeId = this.alarmSystemModelTypeId;
    payload.isGenerateInvoice = type == 'generate invoice' ? true : false;
    payload.isScheduleAlarmSystemRemovalAppointment = this.purchaseOfAlarmSystemDetail.isScheduleAlarmSystemRemovalAppointment;
    payload.purchaseOfAlarmSystemAmountDetail = this.purchaseOfAlarmSystemAmountDetail;
    if(this.queryParams?.feature && this.queryParams?.featureIndex) {
      payload.feature = this.queryParams?.feature;
      payload.featureIndex = this.queryParams?.featureIndex;
    }
    this.rxjsService.setAnyPropertyValue(payload);
    let queryParams = {};
    if(this.queryParams?.feature && this.queryParams?.featureIndex) {
      queryParams['feature_name'] = this.queryParams?.feature;
      queryParams['featureIndex'] = this.queryParams?.featureIndex;
    }
    this.router.navigateByUrl('/customer/manage-customers/purchase-of-alarm-system/capture-documents', { queryParams: queryParams, skipLocationChange: true });
  }
}