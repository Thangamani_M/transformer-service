import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RxjsService } from '@app/shared/services';
import { DialogService, DynamicDialogRef } from 'primeng/api';


@Component({
    selector: 'incorrect-password-dialog-component',
    templateUrl: './incorrect-password-dialog.component.html',
})
export class InCorrectPasswordDialogComponent implements OnInit {

    constructor(public ref: DynamicDialogRef,
        public dialogService: DialogService,
        public dialogRef: MatDialogRef<InCorrectPasswordDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private rxjsService: RxjsService) {
    }

    ngOnInit(): void {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(true);
    }

    close() {
        this.ref.close(true);
    }

}

