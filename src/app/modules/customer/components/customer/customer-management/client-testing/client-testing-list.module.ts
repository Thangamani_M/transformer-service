import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDialogRef, MatSelectModule, MAT_DIALOG_DATA } from "@angular/material";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { NgxPrintModule } from "ngx-print";
import { SignaturePadModule } from "ngx-signaturepad";
import { AutoCompleteModule } from "primeng/autocomplete";
import { ClientTestingListComponent } from "./client-testing-list.component";
import { ClientTestingPasswordComponent } from "./client-testing-password/client-testing-password.component";
import { CorrectPasswordDialogComponent } from "./correct-password-dialog/correct-password-dialog.component";
import { InCorrectPasswordDialogComponent } from "./incorrect-password-dialog/incorrect-password-dialog.component";
import { SelectionPasswordDialogComponent } from "./selection-password-dialog/selection-password-dialog.component";



@NgModule({
  declarations: [ClientTestingListComponent,ClientTestingPasswordComponent, SelectionPasswordDialogComponent,
    InCorrectPasswordDialogComponent, CorrectPasswordDialogComponent,ClientTestingPasswordComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatSelectModule,
    AutoCompleteModule,
    NgxPrintModule,
    SignaturePadModule,
  ],
  entryComponents: [SelectionPasswordDialogComponent,ClientTestingPasswordComponent, InCorrectPasswordDialogComponent, CorrectPasswordDialogComponent,ClientTestingPasswordComponent],
  exports: [ClientTestingListComponent, SelectionPasswordDialogComponent, InCorrectPasswordDialogComponent, CorrectPasswordDialogComponent,ClientTestingPasswordComponent],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }]
})
export class ClientTestingListModule { }
