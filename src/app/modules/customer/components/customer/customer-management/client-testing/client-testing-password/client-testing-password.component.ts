import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, SnackbarService, RxjsService, HttpCancelService, ModulesBasedApiSuffix, prepareRequiredHttpParams, IApplicationResponse, prepareGetRequestHttpParams, ResponseMessageTypes } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { Store, select } from '@ngrx/store';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-client-testing-password',
  templateUrl: './client-testing-password.component.html',
  styleUrls: ['./client-testing-password.component.scss']
})
export class ClientTestingPasswordComponent implements OnInit {

  passwordDialog: boolean = false;
  passwordCorrectDialog: boolean = false;
  isSitePassword: boolean = false;
  selectedKeyHolderId: any;
  keyHolderPassword: boolean = false;
  KeyHolderPasswordList: any;
  loggedUser: any;
  keyHolderList: any = [];
  customerData: any;
  customerAddressId: any;
  customerId = '';
  OccurrenceBookId = '';
  Type: any;
  Selectedpassword: any;
  occurrenceBookId: string;
  siteDropDown: any;
  referenceDropId: any;
  siteType: any;
  sitePasswordPasswordList: any;
  validPassword: boolean;
  passwordCorrect1Dialog:boolean = false;
  clientVehicleDialog: boolean;

  constructor(private crudService: CrudService,
    @Inject(MAT_DIALOG_DATA) public data,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    public dialogService: DialogService, private snackbarService: SnackbarService,
    private store: Store<AppState>, public dialog: MatDialog,
    private rxjsService: RxjsService, private httpCancelService: HttpCancelService) {
    this.customerId = config?.data.customerId
    this.OccurrenceBookId =config?.data.occurrenceBookId
    this.customerAddressId =config?.data.addressId
    this.referenceDropId = config?.data.id
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.rxjsService.setCustomerAddresId(this.customerAddressId)
    this.rxjsService.setCustomerId(this.customerId)
  }

  ngOnInit(): void {
    if (this.referenceDropId) {
      this.getKeyHolder()
    }
    this.isSitePassword = true
  }
  // On click Key Holder
  openKeyHolder() {
    this.isSitePassword = false;
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    this.passwordDialog = false;
    this.keyHolderPassword = true;
    this.getKeyHolder();
  }
  getKeyHolder() {
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_KEY_HOLDER,
      undefined, false, prepareRequiredHttpParams({
        customerId: this.customerId,
        customerAddressId: this.customerAddressId,
        //PartitionId : null
      })
    ).subscribe((response: IApplicationResponse) => {
      this.keyHolderList = [];
      if (response.resources) {
        this.keyHolderList = response.resources;
        this.siteType = this.keyHolderList.filter(v => v.type == 'Customer');
        this.siteDropDown = this.siteType
        let otherParams = {
          LoggedInUserId: this.loggedUser.userId,
          ReferenceId: this.referenceDropId,
          Type: 'KeyHolder',
          //  OccurrenceBookId : this.OccurrenceBookId,
          IsClientTesting: true,
        }
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.KEY_HOLDER_PASSWORD, null, false,
          prepareGetRequestHttpParams(null, null, otherParams))
          .subscribe((response: IApplicationResponse) => {
            if (response.resources) {
              this.sitePasswordPasswordList = response.resources;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });


      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  SitePassword() {
    this.openKeyHolder();
    this.isSitePassword = true;
    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
    this.passwordDialog = false;
    this.keyHolderPassword = false;


    //  this.getSitePassWord()

  }
  getSitePassWord() {
    // this.referenceDropId = this.keyHolderList.filter(v => v.type == 'Customer');
    let otherParams = {
      LoggedInUserId: this.loggedUser.userId,
      ReferenceId: this.referenceDropId,
      Type: 'Customer',
      // OccurrenceBookId : this.OccurrenceBookId,
      IsClientTesting: false,
    }
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.KEY_HOLDER_PASSWORD, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.KeyHolderPasswordList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  onChangeKeyHolder(event) {
    this.selectedKeyHolderId = event.target.value;
    this.Type = this.keyHolderList.find(v => v.id == event.target.value).type;
    let otherParams = {
      LoggedInUserId: this.loggedUser.userId,
      ReferenceId: this.selectedKeyHolderId,
      Type: this.Type,
      // OccurrenceBookId : this.OccurrenceBookId,
      IsClientTesting: false,
    }
    if (this.isSitePassword) {
      otherParams['CustomerId'] = this.customerData;
    }
    if (this.OccurrenceBookId) {
      otherParams['OccurrenceBookId'] = this.OccurrenceBookId;
    } else {
      otherParams['IsClientTesting'] = true;
    }
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.KEY_HOLDER_PASSWORD, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.KeyHolderPasswordList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

  }
  getPasswordList(occurrenceBookId) {
    let otherParams = {
      ReferenceId: this.selectedKeyHolderId ? this.selectedKeyHolderId : this.referenceDropId,
      distressWord: this.Selectedpassword,
      Type: 'KeyHolder',
      occurrenceBookId: occurrenceBookId,
      LoggedInUserId: this.loggedUser.userId
    }

    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.KEY_HOLDER_PASSEORD_VALIDATE, otherParams).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        if (response.resources['valid']) {
          this.validPassword = response.resources['valid']
          this.isSitePassword =false;
          this.rxjsService.setValidPassword(this.validPassword)
          this.snackbarService.openSnackbar("Password Confirmation Successful", ResponseMessageTypes.SUCCESS);
          this.keyHolderPassword = false;
          this.Selectedpassword = null;
          this.passwordCorrectDialog = true;
        } else {
          this.snackbarService.openSnackbar("Password Confirmation Failure", ResponseMessageTypes.ERROR);

        }
        this.rxjsService.setDialogOpenProperty(false);
      }
    });

  }

  passWordCorrect(){
   this.passwordCorrect1Dialog = true;
  }
  // Submission on Key Holder modal
  onSubmitKeyHolder(passwordList) {
    this.Selectedpassword = passwordList;
    this.getPasswordList(this.OccurrenceBookId)
  }
  closemodel() {
    this.dialog.closeAll();
  }
  onClose() { }
  btnCloseClick() {
    this.ref.close();
  }

}

