import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ClientTestingComponent } from './client-testing.component';
import { ClientTestingListModule } from "./client-testing-list.module";



@NgModule({
  declarations: [ClientTestingComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    ClientTestingListModule,
    RouterModule.forChild([
      {
        path: '', component: ClientTestingComponent, data: { title: 'Client Testing' }
      },
    ])
  ],
  entryComponents: [],
  exports: [],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }]
})
export class ClientTestingModule { }
