import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RxjsService } from '@app/shared/services';
import { DialogService, DynamicDialogRef } from 'primeng/api';
@Component({
    selector: 'correct-password-dialog-component',
    templateUrl: './correct-password-dialog.component.html',
})
export class CorrectPasswordDialogComponent implements OnInit {

    constructor( public ref: DynamicDialogRef,
        public dialogService: DialogService,     
        public dialogRef: MatDialogRef<CorrectPasswordDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private rxjsService: RxjsService) {
    }

    ngOnInit(): void {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(true);
    }



    onSubmit() {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(true);
        this.dialogRef.close(true);
    }

    close() {
        this.ref.close(false);
    }

}

