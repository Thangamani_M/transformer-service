import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
import { CorrectPasswordDialogComponent } from '../correct-password-dialog/correct-password-dialog.component';
import { InCorrectPasswordDialogComponent } from '../incorrect-password-dialog/incorrect-password-dialog.component';


@Component({
    selector: 'selection-password-dialog-component',
    templateUrl: './selection-password-dialog.component.html',
    // styleUrls: ['./dispatcher-list.component.scss'],
})
export class SelectionPasswordDialogComponent implements OnInit {

    keyHolderId: any;
    selectionPasswordList: any = [];
    isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
    loggedUser: any;
    constructor(private crudService: CrudService,
        public ref: DynamicDialogRef,
        public dialogService: DialogService,
        private store: Store<AppState>, public config: DynamicDialogConfig,
        public dialogRef: MatDialogRef<SelectionPasswordDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private rxjsService: RxjsService, private httpCancelService: HttpCancelService) {

        this.keyHolderId = this.config.data.id;

        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
    }

    ngOnInit(): void {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(true);
        this.getSelectionPasswordList();
    }

    getSelectionPasswordList() {
        let data = {
            keyHolderId: this.keyHolderId,
            createdUserId: this.loggedUser.userId
        }
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.KEY_HOLDER_PASSWORD, undefined, false, prepareRequiredHttpParams(data))
            .subscribe((response: IApplicationResponse) => {
                if (response.resources) {
                    this.selectionPasswordList = response.resources;

                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    onClickPasswordSelection(passwordList) {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(true);
        if (passwordList.fakeWord) {
            if (this.config.data.type == 1) {
                let data = {
                    keyHolderId: this.keyHolderId,
                    password: passwordList.fakeWord
                }
                this.httpCancelService.cancelPendingRequestsOnFormSubmission();
                let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.KEY_HOLDER_PASSEORD_VALIDATE, data)

                crudService.subscribe((response: IApplicationResponse) => {
                    if (response.isSuccess && !response.resources.valid) {

                        this.ref.close({ valid: false });
                        const ref = this.dialogService.open(InCorrectPasswordDialogComponent, {
                            header: 'Password',
                            showHeader: true,
                            baseZIndex: 1000,
                            width: '650px',
                        });
                        ref.onClose.subscribe((result) => {
                            if (result) {
                                this.ref.close({ valid: false });
                            }
                        });
                        this.rxjsService.setGlobalLoaderProperty(false);
                        this.rxjsService.setDialogOpenProperty(false);
                    }
                    else {

                        this.ref.close({ valid: true });
                        const ref = this.dialogService.open(CorrectPasswordDialogComponent, {
                            header: 'Password',
                            showHeader: true,
                            baseZIndex: 1000,
                            width: '650px',
                        });
                        ref.onClose.subscribe((result) => {
                            if (result) {
                                this.ref.close({ valid: true });
                            }
                        });
                        this.rxjsService.setGlobalLoaderProperty(false);
                        this.rxjsService.setDialogOpenProperty(false);
                    }

                })
            } else {

                let data = {
                    TechnicianId: this.keyHolderId,
                    TechTestingPassword: passwordList.fakeWord
                }
               
                this.httpCancelService.cancelPendingRequestsOnFormSubmission();
                let crudService: Observable<IApplicationResponse> = this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.TECHNICIAN_TESTING_VERIFY_PASSWORD,
                    undefined,
                    false, prepareGetRequestHttpParams(null, null, data))

                crudService.subscribe((response: IApplicationResponse) => {
                    if (response.isSuccess && !response.resources.valid) {
                        this.close()
                        const ref = this.dialogService.open(InCorrectPasswordDialogComponent, {
                            header: 'Password',
                            showHeader: true,
                            baseZIndex: 1000,
                            width: '650px',
                        });
                        ref.onClose.subscribe((result) => {
                            if (result) {
                                this.ref.close({ valid: false });
                            }
                        });
                        this.rxjsService.setGlobalLoaderProperty(false);
                        this.rxjsService.setDialogOpenProperty(false);
                    }
                    else {
                        this.close()
                        const ref = this.dialogService.open(CorrectPasswordDialogComponent, {
                            header: 'Password',
                            showHeader: true,
                            baseZIndex: 1000,
                            width: '650px',
                        });
                        ref.onClose.subscribe((result) => {
                            if (result) {
                                this.ref.close({ valid: true });
                            }
                        });
                        this.rxjsService.setGlobalLoaderProperty(false);
                        this.rxjsService.setDialogOpenProperty(false);
                    }

                })

            }
        }

    }



    close() {
        this.ref.close({ valid: false });
    }

}

