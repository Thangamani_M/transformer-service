import { Location } from '@angular/common';
import { Component, ElementRef, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { countryCodes, CrudType, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { ClientTestingPasswordComponent } from './client-testing-password/client-testing-password.component';
@Component({
  selector: 'app-client-testing-list',
  templateUrl: './client-testing-list.component.html'
})

export class ClientTestingListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  @Input() partitionId;
  @Input() customerAddressId;
  @Input() customerId;
  @Input() permission;
  countryCodes = countryCodes;
  clientTestingForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  signalDurationDropDown: any;
  isGoToPrevPage: boolean;
  validPassword: boolean;
  isDisableFields: boolean = false;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });

  constructor(private rxjsService: RxjsService, private snackbarService: SnackbarService, private dialog: MatDialog, private _fb: FormBuilder, private store: Store<AppState>,
    private crudService: CrudService, private momentService: MomentService, public dialogService: DialogService,
    private httpCancelService: HttpCancelService, private location: Location,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' },],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'keyHolderId',
            captionFontSize: '21px',
            enableBreadCrumb: false,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            radioBtn: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'keyHolderName', header: 'KeyHolder Name', width: '200px' },
              { field: 'keyHolderType', header: 'KeyHolder Type', width: '200px' },
              { field: 'contactNo', header: 'Contact Number', width: '200px', isPhoneMask: true },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.KEY_HOLDER_CLIENT_TESTING_CUSTOMER_CUSTOM_CALL_FLOW,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          }
        ]
      }
    }
    this.rxjsService.getValidPassword().subscribe((data: any) => {
      this.validPassword = data;
    });
    this.rxjsService.getGoToTroubleShootProperty().subscribe((data: boolean) => {
      if (data) {
        this.isGoToPrevPage = data;
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createClientTestingForm();
    this.getSignalDurations();
    // this.getRequiredListData();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      let otherParams = {}
      if (this.customerId && this.customerAddressId) {
        this.getRequiredListData(null, null, otherParams);
      }
    }

  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  createClientTestingForm() {
    this.clientTestingForm = this._fb.group({
      createdUserId: [this.loggedInUserData.userId ? this.loggedInUserData.userId : null],
      keyHolderId: [''],
      isAltSMSNumber: [false],
      altContactCompanyName: [''],
      altContactName: ['', Validators.required],
      customerId: [''],
      customerAddressId: [''],
      isPasswordVerified: ['true'],
      signalTestingDurationId: ['', Validators.required],
      altContactNo: [''],
      altContactNoCountryCode: ['+27'],
      contactNoCountryCode: ['+27'],
      contactNo: [''],
      isActive: ['true'],
      techTestingConfigId: ['']
    })
    this.onFormValueChanges();
  }

  onFormValueChanges() {
    this.clientTestingForm.get("altContactNoCountryCode").valueChanges.pipe(debounceTime(500), distinctUntilChanged(),).subscribe((CountryCode: string) => {
      if (CountryCode) {
        this.setPhoneNumberLengthByCountryCode(
          this.clientTestingForm.get("altContactNoCountryCode").value
        );
      }
    });
  }

  getSignalDurations() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_TECHNICAL_TESTING_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.signalDurationDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.partitionId == '') {
      const params = { customerId: this.customerId, IsAll: true, CustomerAddressId: this.customerAddressId }
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { customerId: this.customerId, IsAll: true, CustomerAddressId: this.customerAddressId, partitionId: this.partitionId }
      otherParams = { ...otherParams, ...params };
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }
  showOptions(event) {
    if (event.checked) {
      this.isDisableFields = true;
      this.setPhoneNumberLengthByCountryCode(this.clientTestingForm.get("altContactNoCountryCode").value)
    } else {
      this.isDisableFields = false;
      this.clientTestingForm?.get('altContactNo')?.clearValidators();
      this.clientTestingForm?.get('altContactNo')?.setErrors(null);
      this.clientTestingForm?.get('altContactNo')?.updateValueAndValidity();
      this.clientTestingForm.get("altContactNo").markAsUntouched();
    }
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.clientTestingForm.get("altContactNo").setValidators([Validators.required,
        Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength),]);
        break;
      default:
        this.clientTestingForm.get("altContactNo").setValidators([Validators.required,
        Validators.minLength(formConfigs.indianContactNumberMaxLength),
        Validators.maxLength(formConfigs.indianContactNumberMaxLength),]);
        break;
    }
    this.clientTestingForm.get("altContactNo").updateValueAndValidity();
  }

  onChangeSelecedRows(event) {
    this.selectedRows = event
    if (event) {
      let contact = event.contactNo.split(" ");
      this.clientTestingForm.get('altContactName').setValue(event.keyHolderName);
      this.clientTestingForm.get('keyHolderId').setValue(event.keyHolderId);
      this.clientTestingForm.get('contactNo').setValue(contact ? contact[1] : null);
      this.clientTestingForm.get('altContactCompanyName').setValue(event ? event.companyName : '');
      this.clientTestingForm.get('contactNoCountryCode').setValue(contact[0] ? contact[0] : null);
      this.clientTestingForm.get('isPasswordVerified').setValue(true);
      this.clientTestingForm.get('isActive').setValue(true);
    }
    else if (!event) {
      this.clientTestingForm.get('keyHolderId').setValue('');
      this.clientTestingForm.get('altContactName').setValue('');
      this.clientTestingForm.get('contactNo').setValue('');
      this.clientTestingForm.get('contactNoCountryCode').setValue('');
    }
  }

  onVerify(): void {
    if (!this.permission?.canVerify) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.clientTestingForm.invalid) {
      this.clientTestingForm.markAllAsTouched();
      this.clientTestingForm.errors
      return
    }
    if (!this.selectedRows) {
      return this.snackbarService.openSnackbar("Please select atleast one KeyHolder ", ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setDialogOpenProperty(true)
    const data = {
      id: this.clientTestingForm.value.keyHolderId,
      customerId: this.customerId,
      addressId: this.customerAddressId,
      LoggedInUserId: this.loggedInUserData.userId,
      type: 1
    }
    const dialogReff = this.dialogService.open(ClientTestingPasswordComponent, { showHeader: false, data: { ...data, isPasswordDialog: true }, width: '500px' });
    dialogReff.onClose.subscribe(result => {
      if (result) {

      }
    });
    // const ref = this.dialog.open(ClientTestingPasswordComponent, {
    //   disableClose: true,
    //   width: '650px',
    //   data: {
    //     id: this.clientTestingForm.value.keyHolderId,
    //     customerId: this.customerId,
    //     addressId: this.customerAddressId,
    //     LoggedInUserId: this.loggedInUserData.userId,
    //     type: 1. // 2- client api
    //   },
    // });
    // ref.afterClosed().subscribe((result) => {
    //   if (result.valid) {
    //   }
    // });
  }

  onActive(): void {
    if (this.clientTestingForm.invalid) {
      this.clientTestingForm.markAllAsTouched();
      return;
    }
    let formValue = this.clientTestingForm.value;
    formValue.isAltSMSNumber = formValue.isAltSMSNumber == true ? true : false;
    formValue.isPasswordVerified = formValue.isPasswordVerified == true ? true : false;
    formValue.isActive = formValue.isActive == true ? true : false;
    formValue.customerId = this.customerId
    formValue.customerAddressId = this.customerAddressId
    formValue.techTestingConfigId = formValue.signalTestingDurationId
    delete formValue.signalTestingDurationId
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CLIENT_TESTING_OTHER, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.selectedRows = [];
        // this.clientTestingForm.get('isAltSMSNumber').setValue(false);
        // this.clientTestingForm.get('altContactName').setValue(null);
        // this.clientTestingForm.get('keyHolderId').setValue(null);
        // this.clientTestingForm.get('altContactNo').setValue(null);
        // this.clientTestingForm.get('altContactCompanyName').setValue(null);
        // this.clientTestingForm.get('signalTestingDurationId').setValue(null);
        this.snackbarService.openSnackbar("Password Activated Successfully", ResponseMessageTypes.SUCCESS);
        this.clientTestingForm.reset();
        this.validPassword = false;
        // this.getRequiredListData();
        if (this.isGoToPrevPage) {
          this.rxjsService.setGoToTroubleShootProperty(false);
          this.location.back();
        }
      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}
