import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { BoundryAllocationComponent } from "./boundry-allocation.component";


@NgModule({
  declarations: [BoundryAllocationComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [BoundryAllocationComponent],
  entryComponents: [],
})
export class BoundryAllocationModule { }
