import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SubscriptionCancelComponent } from "@modules/customer";
import { CustomerTicketViewPageModule } from "../customer-ticket-view-page/customer-ticket-view-page.module";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [SubscriptionCancelComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule,
    CustomerTicketViewPageModule,
    RouterModule.forChild([
      {path: '', component: SubscriptionCancelComponent, data: {title: 'Cancellation History'}, canActivate:[AuthGuard]},
    ])
  ],
  exports: [],
  entryComponents: [],
})
export class SubscriptionCancelModule { }