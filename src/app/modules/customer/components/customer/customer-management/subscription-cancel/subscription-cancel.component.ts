import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CancellationType, CustomerModuleApiSuffixModels } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-subscription-cancel',
  templateUrl: './subscription-cancel.component.html'
})
export class SubscriptionCancelComponent implements OnInit {

  @Input() loading: boolean;
  dataList: any;
  status: any = [];
  selectedRows: string[] = [];
  totalRecords: any;
  userSubscription: any;
  listSubscription: any;
  contractId: any;
  contractName: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  primengTableConfigProperties: any;
  @Input() selectedTabIndex: any = 0;
  loggedInUserData: any;
  searchColumns: any;
  isViewMore: boolean;
  @Input() observableResponse: any;
  isShowNoRecords: any = true;
  @Input() customerId: any;
  @Input() customerAddressId: any;
  @Input() selectedRowData: any;
  feature: string;
  featureIndex: string;
  isLoading: boolean;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    private crudService: CrudService, private store: Store<AppState>, private rxjsService: RxjsService,) {
    this.primengTableConfigProperties = {
      tableCaption: 'Cancellation History',
      breadCrumbItems: [{ displayName: 'Customer Management:Dashboard', relativeRouterUrl: '' },
      { displayName: 'View Customer', relativeRouterUrl: `/customer/manage-customers/view/`, isSkipLocationChange: true }, { displayName: 'Cancellation History' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Cancellation History',
            dataKey: 'serviceCancellationRefNo',
            enableAction: false,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'serviceCancellationRefNo', header: 'Request ID', width: '90px' },
            { field: 'cancellationType', header: 'Cancellation Type', width: '120px' },
            { field: 'cancellationDocumentationDate', header: 'Created On', width: '90px', isDateTime: true },
            { field: 'suspendServiceDate', header: 'Termination date', width: '120px', isDate: true },
            { field: 'ticketCancellationReasonName', header: 'Reason', width: '100px' },
            { field: 'ticketCancellationSubReasonName', header: 'Sub Reason', width: '100px' },
            { field: 'opposition', header: 'Opposition', width: '100px' },
            { field: 'displayName', header: 'Agent', width: '100px' },
            { field: 'status', header: 'Status', width: '80px', isValue: true },],
            apiSuffixModel: SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL,
            moduleName: ModulesBasedApiSuffix.SALES,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          },
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.contractId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.customerId = (Object.keys(params['params']).length > 0) ? params['params']['customerId'] : '';
      this.customerAddressId = (Object.keys(params['params']).length > 0) ? params['params']['addressId'] : '';
      this.feature = (Object.keys(params['params']).length > 0) ? params['params']['feature_name'] : '';
      this.featureIndex = (Object.keys(params['params']).length > 0) ? params['params']['featureIndex'] : '';
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl += this.customerId; 
      this.primengTableConfigProperties.breadCrumbItems[1].queryParams = {addressId: this.customerAddressId, navigateTab: 6}; 
      if(this.feature && this.featureIndex) {
        this.primengTableConfigProperties.breadCrumbItems[1].queryParams = {addressId: this.customerAddressId, feature_name: this.feature, featureIndex: this.featureIndex};
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  onShowViewPage(e) {
    this.isViewMore = e;
  }

  onAfterChangeAddress(e) {
    this.customerAddressId = e;
    // this.getContracts();
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }

  combineLatestNgrxStoreData() {
    this.userSubscription = combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.isShowNoRecords = false;
    this.dataList = [];
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = { customerId: this.customerId, siteAddressId: this.customerAddressId, ...otherParams };
    let api = this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      salesModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams));
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
    }
    this.listSubscription = api.pipe(map((res: IApplicationResponse) => {
			if(res?.resources) {
				res?.resources?.forEach(val => {
					val.cancellationDocumentationDate = this.datePipe.transform(val.cancellationDocumentationDate, 'dd-MM-yyyy');
					val.suspendServiceDate = this.datePipe.transform(val.suspendServiceDate, 'dd-MM-yyyy');
					return val;
				});
			}
			return res;
		})).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      // this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data?.statusCode == 200) {
        this.observableResponse = data?.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data?.totalCount ? data?.totalCount : 0;
        this.isShowNoRecords = true;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse;
        this.totalRecords = 0;
        this.isShowNoRecords = true;
      }
      this.isLoading = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string | any): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["../add-edit"], { relativeTo: this.activatedRoute });
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            let queryParams = { id: this.contractId, customerId: this.customerId, addressId: this.customerAddressId }; 
            if(this.feature && this.featureIndex) {
              queryParams['feature_name'] = this.feature;
              queryParams['featureIndex'] = this.featureIndex;
            }
            if (editableObject?.cancellationType == CancellationType.CANCELLATION) {
              queryParams['ticketId'] = editableObject['ticketId'];
              queryParams['serviceCancellationId'] = editableObject['serviceCancellationId'];
              this.router.navigate(['customer/manage-customers/ticket/process-cancellation/view'], { queryParams: {...queryParams, fromComponentUrl: 'customer'} });
            } else if (editableObject?.cancellationType == CancellationType.SUSPENDED) {
              queryParams['serviceSuspendId'] = editableObject['serviceCancellationId'];
              this.router.navigate(['customer/manage-customers/ticket/suspend-service/view'], { queryParams: queryParams });
            }
            break;
        }
    }
  }

  getHeight() {
    return this.isViewMore ? '35vh' : '48vh';
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }
}
