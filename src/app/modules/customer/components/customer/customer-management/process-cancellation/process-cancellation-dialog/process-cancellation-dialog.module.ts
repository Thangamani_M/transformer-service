import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ProcessCancellationDialogComponent } from "./process-cancellation-dialog.component";

@NgModule({
  declarations: [ProcessCancellationDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [ProcessCancellationDialogComponent],
  entryComponents: [ProcessCancellationDialogComponent],
})
export class ProcessCancellationDialogModule { }