import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ProcessCancellationComponent } from "@modules/customer";
import { CustomerTicketViewPageModule } from "../customer-ticket-view-page/customer-ticket-view-page.module";
import { ProcessCancellationDialogModule } from "./process-cancellation-dialog/process-cancellation-dialog.module";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [ProcessCancellationComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    CustomerTicketViewPageModule,
    ProcessCancellationDialogModule,
    RouterModule.forChild([
      {
        path: '', component: ProcessCancellationComponent, canActivate:[AuthGuard], data: { title: 'Process Cancellations' }
      },
      {
        path: 'view', component: ProcessCancellationComponent, canActivate:[AuthGuard], data: { title: 'View Process Cancellations' }
      },
    ])
  ],
  exports: [],
  entryComponents: [],
})
export class ProcessCancellationModule { }