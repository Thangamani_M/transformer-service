import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { adjustDateFormatAsPerPCalendar, clearFormControlValidators, ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CrudType, CURRENT_YEAR_TO_NEXT_FIFTY_YEARS, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { ProcessCancellationDialogComponent, TicketProcessCancelDetailsModel, TicketProcessServicesDetailsModel } from '@modules/customer';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
@Component({
  selector: 'app-process-cancellation',
  templateUrl: './process-cancellation.component.html',
  styleUrls: ['./process-cancellation.component.scss']
})
export class ProcessCancellationComponent implements OnInit {
  selectedTabIndex = 0;
  primengTableConfigProperties;
  customerId;
  addressId;
  ticketId;
  contractId;
  serviceCancellationId;
  processCancellationForm: FormGroup;
  contractList = [];
  ticketCancelReasonList = [];
  ticketCancelSubReasonList = [];
  oppositionList = [];
  movingCompReasonList = [];
  listOfFiles = [];
  listOfTicketFiles = [];
  fileList: File[] = [];
  maxFilesUpload: Number = 5;
  canDocDate = new Date();
  suspendServiceDate = new Date();
  contractMinLastBillDate = new Date();
  contractMaxLastBillDate: any = "";
  dateOfRemovalDate = new Date();
  submitted: boolean;
  loading: boolean;
  dataList;
  status = [];
  selectedRows: string[] = [];
  totalRecords = 0;
  userSubscription;
  isViewMore: boolean;
  isEquipment: boolean;
  listSubscription;
  pageLimit = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  searchColumns;
  isShowNoRecords = true;
  selectedRowData;
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  feature: string;
  featureIndex: string;
  isEquipmentMsg: string;
  holidayList: any = [];
  holidays: any = [];
  prorataAmount: string = '';
  isLoading: boolean;

  constructor(private crudService: CrudService, private dialog: MatDialog, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private router: Router, private store: Store<AppState>, private formBuilder: FormBuilder, private datePipe: DatePipe, private dialogService: DialogService,) {
    this.activatedRoute.queryParamMap.subscribe((params: any) => {
      this.customerId = params?.params?.customerId ? params?.params?.customerId : '';
      this.addressId = params?.params?.addressId ? params?.params?.addressId : '';
      this.ticketId = params?.params?.ticketId ? params?.params?.ticketId : '';
      this.contractId = params?.params?.id || params?.params?.contractId || '';
      this.serviceCancellationId = params?.params?.serviceCancellationId ? params?.params?.serviceCancellationId : '';
      this.feature = params?.params?.feature_name ? params?.params?.feature_name : '';
      this.featureIndex = params?.params?.featureIndex ? params?.params?.featureIndex : '';
    });
    this.primengTableConfigProperties = {
      tableCaption: "Process Cancellation",
      breadCrumbItems: [{ displayName: 'Customer Management:Dashboard', relativeRouterUrl: '' },
      { displayName: 'View Ticket', relativeRouterUrl: `/customer/manage-customers/ticket/view`, queryParams: { id: this.ticketId, customerId: this.customerId, addressId: this.addressId, fromComponent: 'Tickets' } }, { displayName: 'Process Cancellation' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Process Cancellation',
            dataKey: 'serviceId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'contractRefNo', header: ' Contract No', width: '80px' },
            { field: 'billId', header: 'Bill ID', width: '50px' },
            { field: 'serviceName', header: 'Service', width: '60px' },
            { field: 'suspendServiceDate', header: 'Termination Date', width: '100px' },
            { field: 'contractStartDate', header: 'Contract Start Date', width: '100px' },
            { field: 'billEndDate', header: 'Bill End Date', width: '100px' },
            { field: 'contractPeriodName', header: 'ICP', width: '90px' },
            { field: 'paymentFrequency', header: 'Freq', width: '50px' },
            { field: 'balanceOfContract', header: 'Balance of Contract', width: '100px' },
            { field: 'contractStatus', header: 'Contract Status', width: '70px', isStatus: true, statusKey: 'statusClass' },],
            apiSuffixModel: SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_CONTRACT_DETAIL,
            moduleName: ModulesBasedApiSuffix.SALES,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }
    this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.onLoadValue();
    if (this.serviceCancellationId) {
      this.primengTableConfigProperties.tableCaption = 'View Process Cancellation';
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = 'View Process Cancellation';
      if (this.activatedRoute.snapshot.queryParams?.fromComponentUrl == 'customer') {
        this.primengTableConfigProperties.breadCrumbItems[1].displayName = 'Cancellation History';
        this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/customer/manage-customers/ticket/cancellation-history';
        this.primengTableConfigProperties.breadCrumbItems[1].queryParams = { id: this.contractId, customerId: this.customerId, addressId: this.addressId };
      }
      if (this.feature && this.featureIndex) {
        this.primengTableConfigProperties.breadCrumbItems[1].queryParams = { id: this.contractId, customerId: this.customerId, addressId: this.addressId, feature_name: this.feature, featureIndex: this.featureIndex };
      }
    }
  }

  initForm(ticketProcessCancelDetailsModel?: TicketProcessCancelDetailsModel) {
    let ticketProcessCancelModel = new TicketProcessCancelDetailsModel(ticketProcessCancelDetailsModel);
    this.processCancellationForm = this.formBuilder.group({});
    Object.keys(ticketProcessCancelModel).forEach((key) => {
      if (typeof ticketProcessCancelModel[key] === 'object') {
        this.processCancellationForm.addControl(key, new FormArray(ticketProcessCancelModel[key]));
      } else {
        this.processCancellationForm.addControl(key, new FormControl(ticketProcessCancelModel[key]));
      }
    });
    this.processCancellationForm = setRequiredValidator(this.processCancellationForm, ["contractId", "suspendServiceDate",
      "contractLastBillDate", "ticketCancellationReasonId", "ticketCancellationSubReasonId", "notes", "isRadioRemovalRequired",
      "cancellationDocumentationDate", "services",]);
    // this.processCancellationForm.get('cancellationDocumentationDate').disable();
    this.processCancellationForm.patchValue({
      cancellationDocumentationDate: this.canDocDate,
      createdUserId: this.loggedInUserData?.userId,
      ticketId: this.ticketId,
      customerId: this.customerId,
      siteAddressId: this.addressId,
    })
    this.processCancellationForm.get('isRadioRemovalRequired').disable({ emitEvent: false });
    this.onValueChanges();
  }

  onAfterChangeAddress(e) {
    this.addressId = e;
    // this.getContracts();
  }

  get getServicesArray(): FormArray {
    if (!this.processCancellationForm) return;
    return this.processCancellationForm?.get('services') as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onValueChanges() {
    this.processCancellationForm.get("contractId").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.onCRUDRequested('get', { pageIndex: 0, pageSize: 10 }, { contractId: res });
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    this.processCancellationForm.get("ticketCancellationReasonId").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TICKET_CANCELLATION_SUB_REASON, prepareRequiredHttpParams({ ticketCancellationReasonId: res })).subscribe((response: IApplicationResponse) => {
          this.ticketCancelSubReasonList = response.resources;
        });
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    this.processCancellationForm.get("suspendServiceDate").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.onSelectContractDate(res)
      }
    });
    this.processCancellationForm.get("contractLastBillDate").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.onSelectContractDate(res, false)
      }
    });
    this.processCancellationForm.get("isRadioRemovalRequired").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res?.toString()) {
        this.isEquipment = res == true ? true : false;
        if (res == true) {
          this.processCancellationForm.get('dateOfRemoval').setValidators([Validators.required]);
          this.processCancellationForm.get('dateOfRemoval').updateValueAndValidity();
        } else {
          this.processCancellationForm = clearFormControlValidators(this.processCancellationForm, ["dateOfRemoval"]);
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onSelectContractDate(res: any, start = true) {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.SHARED, SalesModuleApiSuffixModels.WORKING_DAYS, null, false, prepareRequiredHttpParams({ startDate: this.datePipe.transform(res, 'yyyy-MM-dd') }))
      .subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          if (!response?.resources?.isInputDateWorkingDay && !start) {
            this.snackbarService.openSnackbar("Selected date is Weekend/Public Holiday, Please select valid date.", ResponseMessageTypes.WARNING);
            this.processCancellationForm.get('contractLastBillDate').setValue(null);
            this.rxjsService.setGlobalLoaderProperty(false);
            return;
          }
          if (start && response?.resources?.endDate) {
            // this.contractMinLastBillDate = new Date(response?.resources?.endDate);
            this.processCancellationForm.get('contractLastBillDate').setValue(new Date(response?.resources?.endDate));
          }
          this.onValidateProAmount();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onValidateProAmount() {
    if (!this.contractId && !this.processCancellationForm.get('contractId').value) {
      this.snackbarService.openSnackbar("Contract is required", ResponseMessageTypes.WARNING);
      return;
    }
    if (!this.processCancellationForm?.get('contractLastBillDate').value) {
      this.snackbarService.openSnackbar("Terminate Service On is required", ResponseMessageTypes.WARNING);
      return;
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_CONTRACT_BILL_DATE_VALIDATE, null, false,
      prepareRequiredHttpParams({
        contractId: this.contractId || this.processCancellationForm.get('contractId').value, customerId: this.customerId, siteAddressId: this.addressId,
        contractLastBillDate: this.datePipe.transform(this.processCancellationForm?.get('contractLastBillDate').value, 'yyyy-MM-dd'), terminateServiceDate: this.datePipe.transform(this.processCancellationForm?.get('suspendServiceDate').value, 'yyyy-MM-dd')
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.prorataAmount = response?.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  uploadFiles(file) {
    if (file && file.length == 0)
      return;

    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }

    const supportedExtensions = [
      'jpeg',
      'jpg',
      'png',
      'gif',
      'pdf',
      'doc',
      'docx',
      'xls',
      'xlsx',
    ];

    for (let i = 0; i < file.length; i++) {
      // this.formData.append("file", file[i], file[i]['name']);

      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];

      if (supportedExtensions.includes(extension)) {
        let filename = this.fileList.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push(selectedFile.name);
          this.myFileInputField.nativeElement.value = null;
        }

      } else {
        this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
      }

    }
  }

  removeSelectedFile(index) {
    if (!this.serviceCancellationId) {
      // Delete the item from fileNames list
      this.listOfFiles.splice(index, 1);
      // delete file from FileList
      this.fileList.splice(index, 1);
    }
  }

  onDownloadDocuments(obj, name = 'documentName', path = 'documentPath') {
    if (obj) {
      var link = document.createElement("a");
      if (link.download !== undefined) {
        link.setAttribute("href", obj[path]);
        link.setAttribute("download", obj[name]);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
      // import("file-saver").then(FileSaver => {
      //   FileSaver.saveAs(this.initiatonCallCreationForm.get('nkaServiceCall.attachmentUrl').value, this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments').value);
      // });
    }
  }

  onLoadValue() {
    let api = [
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_SUBSCRIPTION_CANCEL_CONTRACTS, prepareRequiredHttpParams({ customerId: this.customerId, siteAddressId: this.addressId })),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TICKET_CANCELLATION_REASON),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_OPPOSITION),
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_OPPOSITION_REASON),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_TICKET_DOCUMENTS, null, false, prepareRequiredHttpParams({ ticketId: this.ticketId })),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIOREMOVAL_VALIDATE_ONCUSTOMER, null, false, prepareRequiredHttpParams({ customerId: this.customerId, addressId: this.addressId })),
      // this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.HOLIDAY_LIST),
    ];
    if (this.serviceCancellationId) {
      api.push(this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_DETAIL, null, false, prepareRequiredHttpParams({ serviceCancellationId: this.serviceCancellationId }))
        .pipe(map((res: IApplicationResponse) => {
          if (res?.resources?.contracts) {
            res?.resources?.contracts?.forEach(val => {
              val.suspendServiceDate = val.suspendServiceDate ? this.datePipe.transform(val.suspendServiceDate, 'dd-MM-yyyy') : '';
              val.contractStartDate = val.contractStartDate ? this.datePipe.transform(val.contractStartDate, 'dd-MM-yyyy') : '';
              val.billEndDate = val.billEndDate ? this.datePipe.transform(val.billEndDate, 'dd-MM-yyyy') : '';
              val.statusClass = val.contractStatus?.toLowerCase() == 'active' ? 'status-label-green' : val.contractStatus?.toLowerCase() == 'inactive' || val.contractStatus?.toLowerCase() == 'in-active' ? 'status-label-pink' : '';
              return val;
            });
          }
          return res;
        })));
    } else {
      api.push(this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKETS, this.ticketId))
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    forkJoin(api).subscribe((result: IApplicationResponse[]) => {
      result?.forEach((res: IApplicationResponse, ix: number) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          switch (ix) {
            case 0:
              this.contractList = res?.resources;
              if (!this.serviceCancellationId) {
                this.contractMaxLastBillDate = new Date(this.contractList[0]?.contractExpiryDate);
              }
              break;
            case 1:
              this.ticketCancelReasonList = res?.resources;
              break;
            case 2:
              this.oppositionList = res?.resources;
              break;
            case 3:
              this.movingCompReasonList = res?.resources;
              break;
            case 4:
              this.listOfTicketFiles = res?.resources;
              break;
            case 5:
              if (res?.resources?.isSuccess) {
                this.processCancellationForm.get('isRadioRemovalRequired').enable({ emitEvent: false });
              } else {
                this.processCancellationForm.get('isRadioRemovalRequired').disable({ emitEvent: false });
                this.processCancellationForm.get('isRadioRemovalRequired').setValue(false, { emitEvent: false });
              }
              this.isEquipmentMsg = res?.resources?.responseMessage;
              break;
            case 6:
              if (this.serviceCancellationId) {
                this.onLoadProcessCancel(res);
              } else {
                this.onLoadTicketDetails(res);
              }
              break;
          }
        }
      })
    })
    // this.rxjsService.setGlobalLoaderProperty(false);
    this.isLoading = false;
  }

  getYearRange() {
    return CURRENT_YEAR_TO_NEXT_FIFTY_YEARS;
  }

  onLoadTicketDetails(response: IApplicationResponse) {
    if (response.isSuccess && response?.resources) {
      this.primengTableConfigProperties.breadCrumbItems[1].displayName += ` : ${response?.resources?.ticketRefNo}`;
      this.processCancellationForm?.get('ticketCancellationReasonId').setValue(response?.resources?.ticketCancellationReasonId);
      this.processCancellationForm?.get('ticketCancellationSubReasonId').setValue(response?.resources?.ticketCancellationSubReasonId);
    }
  }

  getContracts() {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_SUBSCRIPTION_CANCEL_CONTRACTS, prepareRequiredHttpParams({ customerId: this.customerId, siteAddressId: this.addressId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response?.resources) {
          this.contractList = response?.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  combineLatestNgrxStoreData() {
    this.userSubscription = combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search && !e?.col) {
      if (e?.search?.sortOrderColumn == 'isSelectedService') {
        return;
      }
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {})
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.isShowNoRecords = false;
    this.dataList = [];
    this.selectedRows = [];
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = { CustomerId: this.customerId, SiteAddressId: this.addressId, ...otherParams };
    let api = this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      salesModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams));
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
    }
    this.listSubscription = api.pipe(map((res: IApplicationResponse) => {
      if (res?.resources && this.selectedTabIndex == 0) {
        res?.resources?.forEach(val => {
          val.suspendServiceDate = val.suspendServiceDate ? this.datePipe.transform(val.suspendServiceDate, 'dd-MM-yyyy') : '';
          val.contractStartDate = val.contractStartDate ? this.datePipe.transform(val.contractStartDate, 'dd-MM-yyyy') : '';
          val.billEndDate = val.billEndDate ? this.datePipe.transform(val.billEndDate, 'dd-MM-yyyy') : '';
          // val.isSelectedService = val.suspendServiceDate ? true : false;
          val.isSelectedService = true;
          val.statusClass = val.contractStatus?.toLowerCase() == 'active' ? 'status-label-green' : val.contractStatus?.toLowerCase() == 'inactive' || val.contractStatus?.toLowerCase() == 'in-active' ? 'status-label-pink' : '';
          val.checkHidden = val.isSelectedService;
          if (this.serviceCancellationId) {
            const filterValue = this.getServicesArray.controls?.find(el => el?.get('serviceId').value == val?.serviceId);
            val.checkedValue = filterValue ? true : false;
          }
          return val;
        });
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data?.statusCode == 200) {
        data?.resources?.forEach(el => {
          if (el?.isSelectedService) {
            this.selectedRows.push(el);
          }
        });
        this.onPathFormArray();
        this.dataList = data?.resources;
        this.totalRecords = data?.totalCount;
        this.isShowNoRecords = this.dataList?.length ? false : true;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
        this.isShowNoRecords = true;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        // this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["../add-edit"], { relativeTo: this.activatedRoute });
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['../view'], { relativeTo: this.activatedRoute, queryParams: { id: editableObject['wdRequestId'] } });
            break;
        }
    }
  }

  initCalcDateFormArray(ticketProcessServicesDetailsModel?: TicketProcessServicesDetailsModel) {
    let ticketProcessServicesModel = new TicketProcessServicesDetailsModel(ticketProcessServicesDetailsModel);
    let ticketProcessServicesFormArray = this.formBuilder.group({});
    Object.keys(ticketProcessServicesModel).forEach((key) => {
      ticketProcessServicesFormArray.addControl(key, new FormControl({ value: ticketProcessServicesModel[key], disabled: true }));
    });
    this.getServicesArray.push(ticketProcessServicesFormArray);
  }

  onShowViewPage(e) {
    this.isViewMore = e;
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
    this.clearFormArray(this.getServicesArray);
    this.onPathFormArray();
  }

  onPathFormArray() {
    if (this.selectedRows?.length) {
      this.selectedRows.forEach((el: any) => {
        if (!el?.suspendServiceDate) {
          this.initCalcDateFormArray({
            serviceCancellationDetailId: '',
            serviceId: el?.serviceId,
          })
        }
      });
    }
  }

  onBack() {
    if (this.serviceCancellationId && this.activatedRoute.snapshot.queryParams?.fromComponentUrl == 'customer') {
      let queryParams = { id: this.processCancellationForm.get('contractId').value, customerId: this.customerId, addressId: this.addressId };
      if (this.feature && this.featureIndex) {
        queryParams['feature_name'] = this.feature;
        queryParams['featureIndex'] = this.featureIndex;
      }
      this.router.navigate(['./customer/manage-customers/ticket/cancellation-history'], { queryParams: queryParams });
    } else {
      this.router.navigate(['/customer/manage-customers/ticket/view'], { queryParams: { id: this.ticketId, customerId: this.customerId, fromComponent: 'Tickets' } });
    }
  }

  getDateFormat() {
    return adjustDateFormatAsPerPCalendar();
  }

  onRaiseBOC() {
    const serviceId = this.getServicesArray?.getRawValue()[0]?.serviceId;
    const contractId = this.contractId || this.processCancellationForm?.get('contractId').value;
    this.router.navigate(['/customer/manage-customers/balance-of-contract'],
      { queryParams: { contractId: contractId, customerId: this.customerId, serviceId: serviceId, addressId: this.addressId, ticketId: this.ticketId, serviceCancellationId: this.serviceCancellationId, isRaiseBoc: true } });
  }

  onCancelRequest() {
    const message = `Are you sure you want to Retract this Cancellation?`;
    const dialogData = new ConfirmDialogModel("Cancel Request", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      const reqObj = {
        serviceCancellationId: this.serviceCancellationId,
        modifiedUserId: this.loggedInUserData?.userId,
      }
      this.submitted = true;
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_CANCEL_REQUEST, reqObj)
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.onBack();
          }
          this.submitted = false;
        })
    })
  }

  openCancelSummaryPopup() {
    if (this.processCancellationForm.invalid) {
      this.processCancellationForm.markAllAsTouched();
      return;
    }
    // if(this.listOfFiles.length == 0) {
    //   this.snackbarService.openSnackbar('Please upload the documents', ResponseMessageTypes.WARNING);
    //   return;
    // }
    const contract = this.contractList.find(el => el?.id == this.processCancellationForm.get('contractId').value);
    const data = {
      header: 'Cancellation Summary',
      msg: 'Are you sure want to cancel?',
      row: {
        contractId: contract?.displayName,
        suspendServiceDate: this.datePipe.transform(this.processCancellationForm.get('suspendServiceDate').value, 'dd-MM-yyyy'),
        contractLastBillDate: this.datePipe.transform(this.processCancellationForm.get('contractLastBillDate').value, 'dd-MM-yyyy'),
        billValue: '',
        bocBillDate: '',
        bocValue: '',
        noticeBillDate: '',
        noticeValue: '',
        prorataAmount: this.prorataAmount,
      }
    }
    const ref = this.dialogService.open(ProcessCancellationDialogComponent, {
      header: data?.header,
      baseZIndex: 1000,
      width: '700px',
      closable: false,
      showHeader: false,
      data: data,
    });
    ref.onClose.subscribe((res) => {
      if (res) {
        this.onProcessCancel();
      }
    });
  }

  onProcessCancel() {
    const reqObj = {
      ...this.processCancellationForm.getRawValue()
    }
    reqObj['cancellationDocumentationDate'] = this.processCancellationForm.get('cancellationDocumentationDate').value?.toISOString();
    reqObj['suspendServiceDate'] = reqObj['suspendServiceDate'].toISOString();
    reqObj['contractLastBillDate'] = reqObj['contractLastBillDate'].toISOString();
    reqObj['customerId'] = this.customerId;
    reqObj['siteAddressId'] = this.addressId;
    reqObj['services'] = [];
    reqObj['prorataAmount'] = this.prorataAmount?.toString() ? this.prorataAmount : 0;
    this.getServicesArray.controls?.forEach((el) => {
      reqObj['services'].push({ serviceCancellationDetailId: '', serviceId: el?.get('serviceId').value, })
    });
    let formData = new FormData();
    formData.append('ticket', JSON.stringify(reqObj));
    this.fileList?.forEach(file => {
      formData.append('file', file);
    });
    this.submitted = true;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL, formData)
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && (res?.statusCode == 200 || res?.statusCode == 409)) {
          // this.onBack();
          this.selectedRowData = [];
          this.serviceCancellationId = res?.resources;
          // this.onAfterProcessCancel();
          this.router.navigate(['/customer/manage-customers/ticket/process-cancellation/view'], { queryParams: { id: this.processCancellationForm?.get('contractId').value, customerId: this.customerId, addressId: this.addressId, ticketId: this.ticketId, serviceCancellationId: this.serviceCancellationId } });
        }
        this.submitted = false;
      })
  }

  onAfterProcessCancel() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_DETAIL, null, false, prepareRequiredHttpParams({ serviceCancellationId: this.serviceCancellationId }))
      .pipe(map((res: IApplicationResponse) => {
        if (res?.resources?.contracts) {
          res?.resources?.contracts?.forEach(val => {
            val.suspendServiceDate = val.suspendServiceDate ? this.datePipe.transform(val.suspendServiceDate, 'dd-MM-yyyy') : '';
            val.contractStartDate = val.contractStartDate ? this.datePipe.transform(val.contractStartDate, 'dd-MM-yyyy') : '';
            val.billEndDate = val.billEndDate ? this.datePipe.transform(val.billEndDate, 'dd-MM-yyyy') : '';
            val.statusClass = val.contractStatus?.toLowerCase() == 'active' ? 'status-label-green' : val.contractStatus?.toLowerCase() == 'inactive' || val.contractStatus?.toLowerCase() == 'in-active' ? 'status-label-pink' : '';
            return val;
          });
        }
        return res;
      })).subscribe((res: IApplicationResponse) => {
        this.onLoadProcessCancel(res);
      })
  }

  onLoadProcessCancel(res: IApplicationResponse) {
    if (res?.isSuccess && res?.statusCode == 200) {
      this.processCancellationForm.reset();
      this.processCancellationForm.patchValue({
        contractId: res?.resources?.contractId,
        cancellationDocumentationDate: res?.resources?.cancellationDocumentationDate ? new Date(res?.resources?.cancellationDocumentationDate) : '',
        suspendServiceDate: res?.resources?.suspendServiceDate ? new Date(res?.resources?.suspendServiceDate) : '',
        contractLastBillDate: res?.resources?.contractLastBillDate ? new Date(res?.resources?.contractLastBillDate) : '',
        ticketCancellationSubReasonId: res?.resources?.ticketCancellationSubReasonId,
        oppositionId: res?.resources?.oppositionId,
        oppositionReasonId: res?.resources?.oppositionReasonId,
        status: res?.resources?.status ? res?.resources?.status : '',
        notes: res?.resources?.notes,
        isRadio: res?.resources?.isRadio,
        isSystem: res?.resources?.isSystem,
        customerId: res?.resources?.customerId,
        siteAddressId: res?.resources?.siteAddressId,
        createdUserId: res?.resources?.createdUserId,
        statusName: res?.resources?.statusName,
        dateOfRemoval: res?.resources?.dateOfRemoval ? new Date(res?.resources?.dateOfRemoval) : '',
      }, { emitEvent: false });
      res?.resources?.services?.forEach(el => {
        this.initCalcDateFormArray({
          serviceCancellationDetailId: el?.serviceCancellationDetailId,
          serviceId: el?.serviceId,
        });
      });
      res?.resources?.documents?.forEach(el => {
        this.fileList.push(el);
        this.listOfFiles.push(el.documentName);
      });
      this.dataList = res?.resources?.contracts;
      this.totalRecords = 0;
      this.isShowNoRecords = this.dataList?.length ? false : true;
      this.processCancellationForm.get('ticketCancellationReasonId').patchValue(res?.resources?.ticketCancellationReasonId);
      this.processCancellationForm.get('isRadioRemovalRequired').patchValue(res?.resources?.isRadioRemovalRequired);
      // this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns.unshift(
      //   { field: 'isSelectedService', header: ' ', width: '30px', isCheckbox: true, isDisabled: true, },
      // )
      // if(this.serviceCancellationId) {
      //   const suspendedData = [];
      //   this.getServicesArray.controls?.forEach(el => {
      //     if(el?.get('serviceId').value){
      //       const dataFilter = this.dataList.filter(el1 => el1.serviceId == el?.get('serviceId').value );
      //       suspendedData.push(...dataFilter);
      //     }
      //     return el;
      //   });
      //   this.dataList = suspendedData;
      // }
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].checkBox = false;
      this.processCancellationForm.disable({ emitEvent: false });
      this.processCancellationForm.get('contractLastBillDate').disable({ emitEvent: false });
      // this.onCRUDRequested('get', { pageIndex: 0, pageSize: 10 }, { contractId: res?.resources?.contractId });
    }
  }

  getRetract() {
    return this.processCancellationForm?.get('status').value?.toLowerCase() != 'future cancellation';
  }

  getHeight() {
    return this.isViewMore ? '35vh' : '48vh';
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }
}
