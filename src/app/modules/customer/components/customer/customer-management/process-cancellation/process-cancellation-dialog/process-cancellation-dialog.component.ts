import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { OtherService } from '@app/shared';
import { TicketProcessCancelDialogDetailsModel } from '@modules/customer';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-process-cancellation-dialog',
  templateUrl: './process-cancellation-dialog.component.html',
  styleUrls: ['./process-cancellation-dialog.component.scss']
})
export class ProcessCancellationDialogComponent implements OnInit {

  processCancallationDialogForm: FormGroup;
  
  constructor(public config: DynamicDialogConfig, private datePipe: DatePipe,
    public ref: DynamicDialogRef, private formBuilder: FormBuilder,
    private otherService: OtherService) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(ticketProcessCancelDialogDetailsModel?: TicketProcessCancelDialogDetailsModel) {
    let ticketProcessCancelDialogModel = new TicketProcessCancelDialogDetailsModel(ticketProcessCancelDialogDetailsModel);
    this.processCancallationDialogForm = this.formBuilder.group({});
    Object.keys(ticketProcessCancelDialogModel).forEach((key) => {
      if (typeof ticketProcessCancelDialogModel[key] === 'object') {
        this.processCancallationDialogForm.addControl(key, new FormArray(this.config?.data?.row ? this.config?.data?.row[key] : ticketProcessCancelDialogModel[key]));
      } else if (key == 'prorataAmount') {
        this.processCancallationDialogForm.addControl(key, new FormControl(this.config?.data?.row[key] ? this.otherService.transformDecimal(this.config?.data?.row[key]) : ticketProcessCancelDialogModel[key]));
      } else {
        this.processCancallationDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : ticketProcessCancelDialogModel[key]));
      }
    });
    this.processCancallationDialogForm.disable();
    this.processCancallationDialogForm.get('flag').enable();
  }

  btnCloseClick() {
    this.processCancallationDialogForm.reset();
    this.ref.close(false);
  }

  onSubmitDialog() {
    this.ref.close(true);
  }
}
