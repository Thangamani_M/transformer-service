import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { RxjsService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { Store, select } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { ZoneSuspendModelComponent } from '../zone-suspend-model/zone-suspend-model.component';

@Component({
  selector: 'app-load-request-model',
  templateUrl: './load-request-model.component.html',
})
export class LoadRequestModelComponent implements OnInit {

  @Output() outputData = new EventEmitter<any>();
  leadId: string;
  emailNotificationForm: FormGroup;
  userData;

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private dialog: MatDialog, public dialogRef: MatDialogRef<LoadRequestModelComponent>, private dialogService: DialogService, private store: Store<AppState>) {
    this.leadId = this.activatedRoute.snapshot.queryParams.leadId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.emailNotificationForm = this.formBuilder.group({
      templateType: ['1', Validators.required]
    });
  }

  openZoneSuspendModal() {
   this.rxjsService.setDialogOpenProperty(true)
    const dialogReff = this.dialogService.open(ZoneSuspendModelComponent, {
      showHeader: false,
      data: {}, width: '950px'
    });
    dialogReff.onClose.subscribe(result => {
      if (result) {
      }
    });
  }

  onSubmit(): void {
    if (this.emailNotificationForm.invalid) {
      this.emailNotificationForm.markAllAsTouched();
      return;
    }
    // this.rxjsService.setTabIndexForCustomerComponent(4)
    if (this.emailNotificationForm.get('templateType').value == '1') {
      // this.rxjsService.setTabIndexForMonitoringAndResponseComponent(0)
      this.openZoneSuspendModal()
    } else if (this.emailNotificationForm.get('templateType').value == '2') {
      // this.rxjsService.setTabIndexForMonitoringAndResponseComponent(0)
    } else {
      // this.rxjsService.setTabIndexForMonitoringAndResponseComponent(8)
    }
    this.dialogRef.close(this.emailNotificationForm.get('templateType').value);
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
