import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ZoneSuspendModelModule } from "../zone-suspend-model/zone-suspend-model.module";
import { LoadRequestModelComponent } from "./load-request-model.component";

@NgModule({
  declarations: [LoadRequestModelComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    ZoneSuspendModelModule,
  ],
  exports: [LoadRequestModelComponent],
  entryComponents: [LoadRequestModelComponent],
})
export class LoadRequestModelModule { }