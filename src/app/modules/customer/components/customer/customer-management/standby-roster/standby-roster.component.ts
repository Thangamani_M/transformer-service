import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, CrudService, ExtensionModalComponent, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'app-standby-roster-view',
  templateUrl: './standby-roster.component.html',
  styleUrls: ['./standby-roster.component.scss'],
})


export class ViewStandByRosterComponent {

  customerId: string
  customerAddressId: string
  rosterData: any = {}
  technician = []
  areaManager = []
  agentExtensionNo: any
  appointmentId: any
  installationId: any
  callType: any
  estimatedTime:any
  appointmentLogId:any
  nextAppointmentDate:any 
  quickActionType: string;
  feature: string;
  featureIndex: string;

  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private store: Store<AppState>, private router: Router,
    private snackbarService: SnackbarService, private dialog: MatDialog) {
    this.customerId = this.activatedRoute.snapshot.queryParams?.customerId
    this.customerAddressId = this.activatedRoute.snapshot.queryParams?.addressId
    this.appointmentId = this.activatedRoute.snapshot.queryParams?.appointmentId
    this.installationId = this.activatedRoute.snapshot.queryParams?.installationId
    this.callType = this.activatedRoute.snapshot.queryParams?.callType
    this.nextAppointmentDate = this.activatedRoute.snapshot.queryParams?.nextAppointmentDate
    this.appointmentLogId = this.activatedRoute.snapshot.queryParams?.appointmentLogId
    this.estimatedTime = this.activatedRoute.snapshot.queryParams?.estimatedTime,
    this.quickActionType = this.activatedRoute.snapshot.queryParams?.quickActionType
    this.feature = this.activatedRoute.snapshot.queryParams?.feature_name
    this.featureIndex = this.activatedRoute.snapshot.queryParams?.featureIndex

    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
  }
  ngOnInit() {
    this.getStandByRosterData()
  }

  getStandByRosterData() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.STANDBY_ROSTER_CUSTOMER_VIEW_DETAILS, null, false, prepareRequiredHttpParams({ customerId: this.customerId, addressId: this.customerAddressId })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.rosterData = response.resources;
        this.technician = this.rosterData.technicians
        this.areaManager = this.rosterData.technicalAreaManagers
      } else {
        this.snackbarService.openSnackbar("There no standby roster active for the branch ", ResponseMessageTypes.WARNING);
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }

  dialCustomer(mobileNumber) {
    if (!this.agentExtensionNo) {
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      const dialogReff = this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    } else {
      let data = {
        customerContactNumber: mobileNumber,
        customerId: this.customerId,
        clientName: ''
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
    }
  }
  
  navigateToCallInitiationPage() {
    let queryParams = {
      customerId: this.customerId,
      customerAddressId: this.customerAddressId,
      appointmentId: this.appointmentId ? this.appointmentId : null,
      initiationId: this.installationId,
      callType: this.callType // 2- service call, 1- installation call
    };
    if(this.feature && this.featureIndex) {
      queryParams['feature_name'] = this.feature;
      queryParams['featureIndex'] = this.featureIndex;
    }
    this.router.navigate(['/customer', 'manage-customers', 'call-initiation'], { queryParams: queryParams });
  }

  navigateToAppointmentPage() {
    this.router.navigate(['/customer/tech-allocation-calender-view'], {
      queryParams: {
        nextAppointmentDate: this.nextAppointmentDate ? this.nextAppointmentDate : new Date(),
        installationId: this.installationId,
        customerId: this.customerId,
        customerAddressId: this.customerAddressId,
        appointmentId: this.appointmentId ? this.appointmentId : null,
        appointmentLogId: this.appointmentLogId,
        estimatedTime: this.estimatedTime,
        callType: this.callType ? this.callType : null  // 1- installation call, 2- service call
      }
    });

  }

  navigateToViewCustomer(){
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.customerAddressId,
      feature_name: this.feature,
      featureIndex: this.featureIndex,
      customerTab: 3,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

}
