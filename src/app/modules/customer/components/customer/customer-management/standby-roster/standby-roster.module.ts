import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { AuthenticationGuard, LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from "@app/shared/material.module";
import { ViewStandByRosterComponent } from "./standby-roster.component";


@NgModule({
    declarations: [ViewStandByRosterComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: ViewStandByRosterComponent, data: { title: 'View Standby Roster' }, canActivate: [AuthenticationGuard]
        },
    ]),
  ],
  entryComponents: [],
})
export class ViewStandByRosterModule { }