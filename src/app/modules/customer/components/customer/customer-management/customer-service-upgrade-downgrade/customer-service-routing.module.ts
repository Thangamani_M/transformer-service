import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerServiceListComponent } from './customer-service-list.component';
import { CustomerServiceUpgradeComponent } from './upgrade-add-service/upgrade-add-service.component';
import { UpgradeAdjustCostingComponent } from './upgrade-adjust-costing/upgrade-adjust-costing.component';
import { DowngradeAdjustCostingComponent } from './downgrade-adjust-costing/downgrade-adjust-costing.component';
import { ServiceUpgradeSummaryComponent } from './upgrade-summary/upgrade-summary.component';
import { ServiceDowngradeSummaryComponent } from './downgrade-summary/downgrade-summary.component';
import { CustomerServiceDowngradeComponent } from './downgrade-add-service/downgrade-add-service.component'
import { UpgradeApproveDeclineComponent } from './upgrade-approve-decline/upgrade-approve-decline.component';
import { DowngradeApproveDeclineComponent } from './downgrade-approve-decline/downgrade-approve-decline.component';



const routes: Routes = [
  // { path: '', pathMatch: "full", redirectTo: 'list' },
  { path: '', component: CustomerServiceListComponent, data: { title: 'Upgrade & Downgrade Service' } },
  { path: 'upgrade', component: CustomerServiceUpgradeComponent, data: { title: 'Upgrade Service' } },
  { path: 'upgrade/adjust-costing', component: UpgradeAdjustCostingComponent, data: { title: 'Adjust Costing - Upgrade Service' } },
  { path: 'upgrade/summary', component: ServiceUpgradeSummaryComponent, data: { title: 'Upgrade Summary - Upgrade Service' } },
  { path: 'downgrade', component: CustomerServiceDowngradeComponent, data: { title: 'Downgrade Service' } },
  { path: 'downgrade/adjust-costing', component: DowngradeAdjustCostingComponent, data: { title: 'Adjust Costing - Downgrade Service' } },
  { path: 'downgrade/summary', component: ServiceDowngradeSummaryComponent, data: { title: 'Downgrade Summary - Downgrade Service' } },
  { path: 'upgrade/doa-action', component: UpgradeApproveDeclineComponent, data: { title: 'Upgrade Approve/Decline' } },
  { path: 'downgrade/doa-action', component: DowngradeApproveDeclineComponent, data: { title: 'Downgrade Approve/Decline' } },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CustomerServiceListRoutingModule { }
