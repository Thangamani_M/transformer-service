import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerHeaderModel, UpgradeCustomerServiceModel, UpgradeServicesList } from '@app/modules/customer/models';
import { AppState } from '@app/reducers';
import { CrudService, DynamicConfirmByMessageConfirmationType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, ReusablePrimeNGTableFeatureService, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels, ServicesModel } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CustomerHeaderInfoDataCreateAction } from '../customer-service-ngrx-files/customer-header-info.actions';
import { CustomerHeaderInfoDataState$ } from '../customer-service-ngrx-files/customer-header-info.selectors';
import { CustomerServicePopupComponent } from './add-service/customer-service-popup.component';

declare var $: any;
interface ServiceResponse {
  newServices: any[],
  upgradeCurrentServiceDTOs: any[],
  upgradeCurrentServiceContractDetailDTO: any,
}
@Component({
  selector: 'app-upgrade-add-service',
  templateUrl: './upgrade-add-service.component.html',
  styleUrls: ['../customer-service-list.component.scss']
})
export class CustomerServiceUpgradeComponent implements OnInit {
  customerHeaderModel: CustomerHeaderModel
  existingServicesList: ServiceResponse;
  loggedInUserData: LoggedInUserModel;
  objectKeys = Object.keys;
  serviceObj: any = {};
  serviceInfoAddEditForm: FormGroup;
  isFormChangeDetected: any;
  lssAmount = 0;
  upgradeServiceRequestId: string;
  canEdit = false;
  constructor(
    private router: Router, private store: Store<AppState>, private crudService: CrudService, private rxjsService: RxjsService,
    private dialog: MatDialog, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService) {
    this.upgradeServiceRequestId = this.activatedRoute.snapshot.queryParams.id;
    if (this.activatedRoute.snapshot.queryParams.canEdit) {
      this.canEdit = this.activatedRoute.snapshot.queryParams.canEdit == "false" ? false : true
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getExistingServiceList();
    this.createServiceInfoForm()
  }
  getExistingServiceList() {
    let serviceObj: any = { contractId: this.customerHeaderModel.contractId };
    if (this.upgradeServiceRequestId) {
      serviceObj.upgradeServiceRequestId = this.upgradeServiceRequestId
    }
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE_DETAIL, null, false,
      prepareRequiredHttpParams(serviceObj)).subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.existingServicesList = response.resources;
          if (response.resources.newServices) {
            response.resources.newServices.forEach((respObj) => {
              this.serviceObj[respObj['serviceCategoryName']] = {};
              this.serviceObj[respObj['serviceCategoryName']]['values'] = respObj['upgradeCurrentServiceDTOs'];
              this.serviceObj[respObj['serviceCategoryName']]['serviceCategoryId'] = respObj['serviceCategoryId'];
            });
            this.calculateTotalAmountSummary();
          }
          this.customerHeaderModel.taxPrice = this.existingServicesList.upgradeCurrentServiceContractDetailDTO.taxPercentage
          this.store.dispatch(new CustomerHeaderInfoDataCreateAction({ CustomerHeaderInfoDataModel: this.customerHeaderModel }));
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createServiceInfoForm(): void {
    let serviceInfoFormModel = new UpgradeCustomerServiceModel();
    this.serviceInfoAddEditForm = this.formBuilder.group({});
    Object.keys(serviceInfoFormModel).forEach((key) => {
      this.serviceInfoAddEditForm.addControl(
        key,
        new FormControl(serviceInfoFormModel[key])
      );
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(CustomerHeaderInfoDataState$),
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.customerHeaderModel = new CustomerHeaderModel(response[0]);
      this.loggedInUserData = new LoggedInUserModel(response[1]);
    });
  }

  onAddServices(): void {
    if (this.serviceInfoAddEditForm.invalid) return;
    let data = { ...this.existingServicesList.upgradeCurrentServiceContractDetailDTO, ...this.customerHeaderModel };
    data['serviceObj'] = this.serviceObj;
    data['customerId'] = this.customerHeaderModel.customerId;
    data['addressId'] = this.customerHeaderModel.addressId;
    data['createdUserId'] = this.loggedInUserData.userId;
    const dialogRef = this.dialog.open(CustomerServicePopupComponent, {
      width: "760px",
      data,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (typeof dialogResult === 'boolean' && dialogResult) {
        this.isFormChangeDetected = false;
        return;
      }
      if (dialogResult.fromComponent && dialogResult.fromComponent == 'services') {
        this.calculateTotalAmountSummary();
      }
    });
    var self = this;
    dialogRef.componentInstance.outputData.subscribe(ele => {
      self.serviceObj = ele.serviceObj;
      if (this.serviceObj.hasOwnProperty('LSS Scheme Name')) {
        this.lssAmount = this.serviceObj['LSS Scheme Name'].totalAmount.servicePrice
      }
      this.isFormChangeDetected = true;
    });
  }

  otherServicesTotalAmount = {
    servicePrice: 0,
    taxPrice: 0,
    totalPrice: 0
  };
  valueAddedServicesTotalAmount = {
    servicePrice: 0,
    taxPrice: 0,
    totalPrice: 0
  };
  serviceItemsLength: any
  calculateTotalAmountSummary(): void {
    this.valueAddedServicesTotalAmount = { taxPrice: 0, totalPrice: 0, servicePrice: 0 };
    this.otherServicesTotalAmount = { taxPrice: 0, totalPrice: 0, servicePrice: 0 };
    Object.keys(this.serviceObj).forEach((key) => {
      this.serviceObj[key]["totalAmount"] = { taxPrice: 0, totalPrice: 0, servicePrice: 0 };
      this.serviceObj[key]['isQtyAvailable'] = this.serviceObj[key]['values'].find(s => s['isQtyAvailable'] == true) ? true : false;
      if (this.serviceObj[key]['isQtyAvailable']) {
        this.serviceObj[key]['values'].forEach((obj: ServicesModel) => {
          this.valueAddedServicesTotalAmount.servicePrice = this.valueAddedServicesTotalAmount.servicePrice + (obj['servicePrice']);
          this.valueAddedServicesTotalAmount.taxPrice = this.valueAddedServicesTotalAmount.taxPrice + (obj['taxPrice']);
          this.valueAddedServicesTotalAmount.totalPrice = this.valueAddedServicesTotalAmount.totalPrice +
            (obj['servicePrice'] + obj['taxPrice']);
          this.serviceObj[key]["totalAmount"]['servicePrice'] = this.serviceObj[key]["totalAmount"]['servicePrice'] + (obj['servicePrice']);
          this.serviceObj[key]["totalAmount"]['taxPrice'] = this.serviceObj[key]["totalAmount"]['taxPrice'] + (obj['taxPrice']);
          this.serviceObj[key]["totalAmount"]['totalPrice'] = this.serviceObj[key]["totalAmount"]['totalPrice'] + (obj['servicePrice'] + obj['taxPrice']);
        });
      }
      else {
        this.serviceObj[key]['values'].forEach((obj: ServicesModel) => {
          this.otherServicesTotalAmount.servicePrice = this.otherServicesTotalAmount.servicePrice + obj['unitPrice'];
          this.otherServicesTotalAmount.taxPrice = this.otherServicesTotalAmount.taxPrice + obj['unitTaxPrice'];
          this.otherServicesTotalAmount.totalPrice = this.otherServicesTotalAmount.totalPrice + (obj['unitPrice'] + obj['unitTaxPrice']);
          this.serviceObj[key]["totalAmount"]['servicePrice'] = this.serviceObj[key]["totalAmount"]['servicePrice'] + (obj['unitPrice']);
          this.serviceObj[key]["totalAmount"]['taxPrice'] = this.serviceObj[key]["totalAmount"]['taxPrice'] + (obj['unitTaxPrice']);
          this.serviceObj[key]["totalAmount"]['totalPrice'] = this.serviceObj[key]["totalAmount"]['totalPrice'] + (obj['unitPrice'] + obj['unitTaxPrice']);
        });
      }
    });
    this.serviceItemsLength = this.objectKeys(this.serviceObj).length;
    $('#services_modal').modal('hide');
  }

  onSubmit(): void {
    if (this.serviceObj.hasOwnProperty('LSS Scheme Name') && this.objectKeys(this.serviceObj).length == 1 ||
      !this.serviceObj.hasOwnProperty('LSS Scheme Name') && this.objectKeys(this.serviceObj).length == 0) {
      this.snackbarService.openSnackbar("Atleast One Service Subscription is required..!!", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.serviceInfoAddEditForm.invalid) {
      return;
    }
    let reqObject = this.serviceInfoAddEditForm.getRawValue()
    this.rxjsService.setFormChangeDetectionProperty(true);
    reqObject.upgradeServiceDTOs = [];
    reqObject.upgradeServiceRequestDTO.contractId = this.customerHeaderModel.contractId.toLocaleLowerCase()
    reqObject.upgradeServiceRequestDTO.customerId = this.customerHeaderModel.customerId
    reqObject.upgradeServiceRequestDTO.addressId = this.customerHeaderModel.addressId
    reqObject.upgradeServiceRequestDTO.lssId = null
    reqObject.upgradeServiceRequestDTO.upgradeServiceRequestId = this.upgradeServiceRequestId
    reqObject.upgradeServiceRequestDTO.voluntaryContributionAmount = 0
    Object.keys(this.serviceObj).forEach((key) => {
      if (this.serviceObj.hasOwnProperty(key) && key !== 'LSS Scheme Name') {    // change if the key is changed
        this.serviceObj[key]['values'].forEach((service: UpgradeServicesList) => {
          let serv: UpgradeServicesList = {
            upgradeServiceId: service?.upgradeServiceId ? service?.upgradeServiceId : null,
            servicePriceId: service.servicePriceId,
            qty: service.qty,
            servicePrice: service.servicePrice,
            taxPrice: service.taxPrice,
            totalPrice: service.totalPrice,
            unitPrice: service.unitPrice ? service.unitPrice : service.servicePrice
          }
          reqObject.upgradeServiceDTOs.push(serv);
        });
      } else if (key == "LSS Scheme Name") {
        reqObject.upgradeServiceRequestDTO.lssId = this.existingServicesList.upgradeCurrentServiceContractDetailDTO.lssId
        reqObject.upgradeServiceRequestDTO.voluntaryContributionAmount = this.lssAmount
      }
    });
    reqObject.createdUserId = this.loggedInUserData.userId;
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE, reqObject);
    crudService.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.isFormChangeDetected = false;
        this.upgradeServiceRequestId = response.resources;
        this.navigateToSummary()
      }
    });
  }

  onQuantityChange(type, serviceSubItem: object): void {
    if (type === 'plus') {
      serviceSubItem['qty'] = serviceSubItem['qty'] + 1;
    }
    else {
      serviceSubItem['qty'] = (serviceSubItem['qty'] === 1) ? serviceSubItem['qty'] : serviceSubItem['qty'] - 1;
    }
    serviceSubItem['servicePrice'] = (serviceSubItem['qty'] * serviceSubItem['unitPrice']);
    serviceSubItem['taxPrice'] = (serviceSubItem['qty'] * serviceSubItem['unitTaxPrice']);
    serviceSubItem['totalPrice'] = (serviceSubItem['servicePrice'] + serviceSubItem['taxPrice']);
    this.calculateTotalAmountSummary();
    this.isFormChangeDetected = (type == 'minus' && serviceSubItem['qty'] === 1) ? false : true;
  }
  selectedItemsWithKeyValue: any
  removeItem(serviceItemKey: string, serviceItemObj: object) {
    this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, undefined,
      DynamicConfirmByMessageConfirmationType.DANGER).
      onClose?.subscribe(dialogResult => {
        if (!dialogResult) return;
        let areAllServicesRemoved = false;
        if (serviceItemObj['upgradeServiceId']) {
          this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE, undefined,
            prepareRequiredHttpParams({
              upgradeServiceId: serviceItemObj['upgradeServiceId'],
              isDeleted: true,
              createdUserId: this.loggedInUserData.userId
            })).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                Object.keys(this.serviceObj).forEach((key: string) => {
                  if (key === serviceItemKey) {
                    for (const [ix, val] of this.serviceObj[key]['values'].entries()) {
                      if (val['upgradeServiceId'] === serviceItemObj['upgradeServiceId']) {
                        this.serviceObj[key]['values'].splice(ix, 1);
                        if (this.serviceObj[key]['values'].length === 0) {
                          delete this.serviceObj[key];
                          areAllServicesRemoved = true;
                        }
                        else {
                          areAllServicesRemoved = false;
                        }
                        this.calculateTotalAmountSummary();
                      }
                    }
                  }
                });
                this.selectedItemsWithKeyValue = { ...this.serviceObj };
              }
            });
        }
        else if (serviceItemKey == "Lss Scheme Name" && this.upgradeServiceRequestId) {
          this.crudService.delete(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE_LSS, undefined,
            prepareRequiredHttpParams({
              upgradeServiceRequestId: this.upgradeServiceRequestId,
              lssId: "",
              voluntaryContributionAmount: "",
              createdUserId: this.loggedInUserData.userId
            })).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                delete this.serviceObj['LSS Scheme Name'];
                this.getExistingServiceList();
                this.calculateTotalAmountSummary()
              }
            });
        }
        else {
          Object.keys(this.serviceObj).forEach((key: string) => {
            if (key === serviceItemKey) {
              for (const [ix, val] of this.serviceObj[key]['values'].entries()) {
                if (val['servicePriceId'] === serviceItemObj['servicePriceId']) {
                  this.serviceObj[key]['values'].splice(ix, 1);
                  if (this.serviceObj[key]['values'].length === 0) {
                    delete this.serviceObj[key];
                    areAllServicesRemoved = true;
                  }
                  else {
                    areAllServicesRemoved = false;
                  }
                  this.calculateTotalAmountSummary();
                }
              }
            }
          });
          this.selectedItemsWithKeyValue = { ...this.serviceObj };
        }
        this.selectedItemsWithKeyValue = JSON.parse(JSON.stringify(this.serviceObj));
      });
  }

  navigateToAdjustCosting() {
    this.router.navigate(['customer/manage-customers/customer-service-upgrade-downgrade/upgrade/adjust-costing'], { queryParams: { id: this.upgradeServiceRequestId, canEdit: this.canEdit } });
  }

  navigateToSummary() {
    this.router.navigate(['customer/manage-customers/customer-service-upgrade-downgrade/upgrade/summary'], { queryParams: { id: this.upgradeServiceRequestId, canEdit: this.canEdit } });
  }

  goBack() {
    this.router.navigate(['customer/manage-customers/customer-service-upgrade-downgrade'], { queryParams: { addressId: this.customerHeaderModel.addressId, contractId: this.customerHeaderModel.contractId, customerId: this.customerHeaderModel.customerId, contractName: this.customerHeaderModel.contractName, canEdit: this.canEdit } })
  }

  isViewMore = false
  emitViewMore(e) {
    this.isViewMore = e
  }
}
