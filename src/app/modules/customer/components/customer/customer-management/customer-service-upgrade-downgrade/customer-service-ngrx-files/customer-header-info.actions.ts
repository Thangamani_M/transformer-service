import { CustomerHeaderModel } from '@modules/customer/models/customer-header.model';
import { Action } from '@ngrx/store';

enum CustomerHeaderInfoDataActionTypes {
  CustomerHeaderInfoDataCreateAction = '[Customer Header Creation ]  Create Action',
  CustomerHeaderInfoDataChangeAction = '[Customer Header Creation ] Change Action',
  CustomerHeaderInfoDataRemoveAction = '[Customer Header Creation ]  Remove Action',
}

class CustomerHeaderInfoDataCreateAction implements Action {

  readonly type = CustomerHeaderInfoDataActionTypes.CustomerHeaderInfoDataCreateAction;

  constructor(public payload: { CustomerHeaderInfoDataModel: CustomerHeaderModel }) {
  }

}

class CustomerHeaderInfoDataChangeAction implements Action {

  readonly type = CustomerHeaderInfoDataActionTypes.CustomerHeaderInfoDataChangeAction;

  constructor(public payload: { CustomerHeaderInfoDataModel: CustomerHeaderModel }) {
  }

}

class CustomerHeaderInfoDataRemoveAction implements Action {

  readonly type = CustomerHeaderInfoDataActionTypes.CustomerHeaderInfoDataRemoveAction;

}

type CustomerHeaderInfoDataActions = CustomerHeaderInfoDataCreateAction | CustomerHeaderInfoDataChangeAction | CustomerHeaderInfoDataRemoveAction;


export {
  CustomerHeaderInfoDataCreateAction, CustomerHeaderInfoDataChangeAction,
  CustomerHeaderInfoDataRemoveAction, CustomerHeaderInfoDataActions, CustomerHeaderInfoDataActionTypes
};

