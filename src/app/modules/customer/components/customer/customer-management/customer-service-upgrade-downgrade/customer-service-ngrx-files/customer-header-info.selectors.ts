import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CustomerHeaderModel } from '@modules/customer/models/customer-header.model';

const CustomerHeaderInfoDataState = createFeatureSelector<CustomerHeaderModel>("customerHeaderInfoData");

const CustomerHeaderInfoDataState$ = createSelector(
  CustomerHeaderInfoDataState,
  CustomerHeaderInfoDataState => CustomerHeaderInfoDataState
);

export {
  CustomerHeaderInfoDataState$
};

