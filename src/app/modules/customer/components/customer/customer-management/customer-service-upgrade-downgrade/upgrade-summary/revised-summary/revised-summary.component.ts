import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { CrudService, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-revised-summery',
  templateUrl: './revised-summary.component.html',
  styleUrls: ['../../customer-service-list.component.scss']
})

export class RevisedSummaryModalComponent implements OnInit {
  servicesSummary: any;

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private rxjsService: RxjsService, private router: Router) {
  }

  ngOnInit(): void {
    this.getRevisedSummary();
  }

  getRevisedSummary() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE_COMPARISON_SUMMARY, null, false, prepareRequiredHttpParams({ contractId: this.config.data.contractId, upgradeServiceRequestId: this.config.data.upgradeServiceRequestId || "" })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.servicesSummary = response.resources;
        if (this.servicesSummary.startBillDate) {
          this.servicesSummary.startBillDate = new Date(this.servicesSummary.startBillDate);
        }
      }
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

  onSubmit() {
    let payload = {
      upgradeServiceRequestId: this.config.data.upgradeServiceRequestId,
      doaConfigId: this.servicesSummary.doaConfigId,
      createdUserId: this.config.data?.userId,
      isFinduService: this.config.data?.isFinduService,
      finduUserId: this.config.data?.finduUserId
    }
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE_ACCEPT_CONTRACT, payload).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.dialogClose();
        this.router.navigate(['customer/manage-customers/customer-service-upgrade-downgrade'], { queryParams: { addressId: this.config.data.addressId, contractId: this.config.data.contractId, customerId: this.config.data.customerId, contractName: this.config.data.contractName } })
      }
    });
  }
}
