import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerHeaderModel, CustomerServicePriceAdjustments, CustomerServicePriceAdjustModel } from '@app/modules/customer/models';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { CustomerHeaderInfoDataState$ } from '../customer-service-ngrx-files/customer-header-info.selectors';

declare var $: any;
@Component({
  selector: 'app-downgrade-approve-decline',
  templateUrl: './downgrade-approve-decline.component.html',
  styleUrls: ['../customer-service-list.component.scss']
})
export class DowngradeApproveDeclineComponent implements OnInit {

  customerHeaderModel: CustomerHeaderModel
  loggedInUserData: LoggedInUserModel;
  adjustCostingDetails: any = {
    downgradeServiceAdjustCostingAmountSummaryDTO: {},
    downgradeServiceAdjustCostingDetailDTOs: [],

  }
  detailsData: any = { taxPercentage: 20 }
  priceAdjustmentForm: FormGroup;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  downgradeServiceAdjustCostingPostDTOs: FormArray;
  customerItemPriceAdjustments: FormArray;
  downgradeServiceRequestId: string
  constructor(
    private router: Router, private store: Store<AppState>, private crudService: CrudService, private rxjsService: RxjsService,
    private dialog: MatDialog, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute) {
    this.downgradeServiceRequestId = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getAdjustCostingDetails();
    this.createPriceAdjustmentForm()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(CustomerHeaderInfoDataState$),
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.customerHeaderModel = new CustomerHeaderModel(response[0]);
      this.loggedInUserData = new LoggedInUserModel(response[1]);
    });
  }

  getAdjustCostingDetails() {
    // this.FA3328A8-1641-40A5-B186-6BEEA590D3D0
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_DOWNGRADE_ADJUST_COSTING_DETAIL, null, false, prepareRequiredHttpParams({ downgradeServiceRequestId: this.downgradeServiceRequestId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.adjustCostingDetails = response.resources;
        this.downgradeServiceAdjustCostingPostDTOs = this.getdowngradeServiceAdjustCostingPostDTOs;
        this.adjustCostingDetails.downgradeServiceAdjustCostingDetailDTOs.forEach((obj) => {
          this.convertTodecimal(obj);
          this.downgradeServiceAdjustCostingPostDTOs.push(this.createdowngradeServiceAdjustCostingPostDTOs(obj));
        });
        if(this.adjustCostingDetails.downgradeServiceAdjustCostingAmountSummaryDTO){
          this.priceAdjustmentForm.get('doaConfigId').setValue(this.adjustCostingDetails.downgradeServiceAdjustCostingAmountSummaryDTO.doaConfigId)
          this.doaDetails.doaLevel = this.adjustCostingDetails.downgradeServiceAdjustCostingAmountSummaryDTO.doaAuthorisation
          }

        this.rxjsService.setGlobalLoaderProperty(false);
        this.addServicesAmt()
        this.addServicesPerccent()
      }
    })
  }
  convertTodecimal(obj) {

    obj.adjustedTotal = obj.adjustedTotal ? parseFloat(obj.adjustedTotal).toFixed(2) : 0;
    obj.discountAmount = obj.discountAmount ? parseFloat(obj.discountAmount).toFixed(2) : 0;
    obj.amount = obj.amount ? parseFloat(obj.amount).toFixed(2) : 0;
    obj.subTotal = obj.subTotal ? parseFloat(obj.subTotal).toFixed(2) : 0;
    obj.totalAmount = obj.totalAmount ? parseFloat(obj.totalAmount).toFixed(2) : 0;
    obj.vat = obj.vat ? parseFloat(obj.vat).toFixed(2) : 0;
    return obj;
  }

  createPriceAdjustmentForm() {
    let priceAdjustModel = new CustomerServicePriceAdjustModel();
    this.priceAdjustmentForm = this.formBuilder.group({
      downgradeServiceAdjustCostingPostDTOs: this.formBuilder.array([]),
      customerItemPriceAdjustments: this.formBuilder.array([])
    });
    Object.keys(priceAdjustModel).forEach((key) => {
      this.priceAdjustmentForm.addControl(key, new FormControl(priceAdjustModel[key]));
    });
    this.priceAdjustmentForm = setRequiredValidator(this.priceAdjustmentForm, ["doaConfigId", "downgradeServiceRequestId"]);
    this.priceAdjustmentForm.get('downgradeServiceRequestId').setValue(this.downgradeServiceRequestId)
    this.priceAdjustmentForm.get('createdUserId').setValue(this.loggedInUserData.userId)
  }

  get getdowngradeServiceAdjustCostingPostDTOs(): FormArray {
    if (!this.priceAdjustmentForm) return;
    return this.priceAdjustmentForm.get("downgradeServiceAdjustCostingPostDTOs") as FormArray;
  }


  //Create FormArray controls
  createdowngradeServiceAdjustCostingPostDTOs(downgradeServiceAdjustCostingPostDTOs?: CustomerServicePriceAdjustments): FormGroup {
    let downgradeServiceAdjustCostingPostDTOsModel = new CustomerServicePriceAdjustments(downgradeServiceAdjustCostingPostDTOs);
    let formControls = {};
    Object.keys(downgradeServiceAdjustCostingPostDTOsModel).forEach((key) => {
      if (key === 'amount') {
        formControls[key] = [{ value: downgradeServiceAdjustCostingPostDTOsModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: downgradeServiceAdjustCostingPostDTOsModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }







  summaryData = {
    percentage: "",
    amount: "",
    currentMonthlyInst:0,
    futureMonthlyInst:0
  }

  addServicesAmt() {
    let dt = this.priceAdjustmentForm.get('downgradeServiceAdjustCostingPostDTOs').value;
    let sum = 0;
    if (dt && dt.length > 0) {
      if (!this.priceAdjustmentForm.value.rewardId && this.priceAdjustmentForm.value.isReward) {
        sum = 0;
      } else {
        dt.forEach(element => {
          sum = sum + parseFloat(element.discountAmount);
        });
      }
      this.priceAdjustmentForm.get('serviceDiscountAmount').setValue(JSON.parse(JSON.stringify(sum.toFixed(2))));
      this.summaryData.amount = 'R ' + sum.toFixed(2)
    } else {
      this.summaryData.amount = 'R ' + '0'
    }
  }

  addServicesPerccent() {
    let dt = this.priceAdjustmentForm.get('downgradeServiceAdjustCostingPostDTOs').value;
    let sum = 0;
    let adjust: number = 0;
    let finalTotal: number = 0;
    if (dt && dt.length > 0) {
      dt.forEach(element => {
        sum = sum + parseFloat(element.discountAmount);
        adjust = adjust + parseFloat(element.subTotal);
        finalTotal = (sum / adjust) * 100;
      });

      this.priceAdjustmentForm.get('serviceDiscountPercentage').setValue((finalTotal).toFixed(2));
      this.summaryData.percentage =  (finalTotal).toFixed(2) + ' %';
    } else {
      this.summaryData.percentage = 0 + ' %';

    }
  }

  doaDetails: any = {}


  approve() {
    let obj = {
      downgradeServiceRequestId:this.downgradeServiceRequestId,
      doaConfigId: this.adjustCostingDetails.downgradeServiceAdjustCostingAmountSummaryDTO.doaConfigId,
      createdUserId: this.loggedInUserData?.userId
    }

    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_DOWNGRADE_DOA_APPROVAL, obj).subscribe(response=>{
      if(response.isSuccess && response.statusCode ==200){
        this.router.navigate(['/my-tasks/my-customer']);
      }
    })
  }

  decline(){
    let obj = {
      downgradeServiceRequestId:this.downgradeServiceRequestId,
      createdUserId: this.loggedInUserData?.userId
    }
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_DOWNGRADE_DOA_DECLINED, obj).subscribe(response=>{
      if(response.isSuccess && response.statusCode ==200){
        this.router.navigate(['/my-tasks/my-customer']);
      }
    })
  }

  goBack(){
    this.router.navigate(['/customer/manage-customers/customer-service-downgrade-downgrade/downgrade'],{queryParams:{id:this.downgradeServiceRequestId}})
  }
  goNext(){
    this.router.navigate(['/customer/manage-customers/customer-service-downgrade-downgrade/downgrade/summary'],{queryParams:{id:this.downgradeServiceRequestId}})
  }

  isViewMore =false
    emitViewMore(e){
      this.isViewMore = e
    }
}
