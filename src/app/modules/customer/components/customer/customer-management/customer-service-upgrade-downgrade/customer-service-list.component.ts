import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatMenuItem } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerHeaderModel } from '@app/modules/customer/models';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { CustomerManagementService } from '@modules/customer/services/customermanagement.services';
import { CUSTOMER_COMPONENT } from '@modules/customer/shared';
import { loggedInUserData, selectStaticEagerLoadingTicketStatusState$ } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
import { CustomerHeaderInfoDataCreateAction } from './customer-service-ngrx-files';
declare var $: any;
@Component({
  selector: 'app-customer-service-list',
  templateUrl: './customer-service-list.component.html',
  styleUrls: ['./customer-service-list.component.scss']
})
export class CustomerServiceListComponent implements OnInit {

  subscriptionList = [];
  ticketTypeList = [];
  ticketStatusList = [];
  ticketGroupList = [];
  assigntoList = [];
  selectedval: string;
  groupId: string;
  title: string;
  applicationResponse: IApplicationResponse;
  ticketForm: FormGroup;
  ticketObservableResponse;
  loggedUser: UserLogin;
  selectedTabIndex = 0;
  submitted = false;
  componentProperties = new ComponentProperties();
  id: string;
  customerId: string;
  customerLeadsAddress = "customer leads address";
  tasksObservable;
  startTodayDate = new Date();
  observableResponse: any;

  onRowClick: any;

  onSearchInputChange: any;


  showQuickAction = false;

  dataList: any
  deletConfirm: boolean = false
  addConfirm: boolean = false
  loading: boolean = false;
  public bradCrum: MatMenuItem[];
  status: any = [];
  selectedRows: string[] = [];
  selectedRow: any;
  selectedColumns: any[];
  totalRecords: any;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  searchColumns: any;
  row: any = {};
  primengTableConfigProperties;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  statusConfirm: boolean = false;
  showDialogSpinner: boolean = false;
  cancelDate = new Date('1984');
  fromDate = new Date('1984');
  todayDate = new Date('1984');
  ticketFlagList: any;

  customerHeaderModel: CustomerHeaderModel
  isViewMore = false;
  contractId: string
  contractName: string
  constructor(private tableFilterFormService: TableFilterFormService,
    private _fb: FormBuilder, private router: Router, private store: Store<AppState>, private snackbarService: SnackbarService, private formBuilder: FormBuilder, private crudService: CrudService, private rxjsService: RxjsService, private dialog: MatDialog,
    private activatedRoute: ActivatedRoute, private customerManagementService: CustomerManagementService) {
    this.contractId = this.activatedRoute.snapshot.queryParams.contractId
    this.contractName = this.activatedRoute.snapshot.queryParams.contractName
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {

      if (!userData) return;
      this.loggedUser = userData;
      this.searchForm = this._fb.group({ searchKeyword: "" });
      this.columnFilterForm = this._fb.group({});
    });


  }

  ngOnInit(): void {
    this.getCustomerData()
    this.primengTableConfigProperties = {
      tableCaption: "UPGRADES & DOWNGRADES",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Customers', relativeRouterUrl: '' }, { displayName: 'Ticket List' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "UPGRADES & DOWNGRADES",
            dataKey: 'upgradeServiceRequestRefNo',
            enableBreadCrumb: true,
            ebableAddActionBtn: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'upgradeServiceRequestRefNo', header: 'Offer ID' },
            { field: 'createdDate', header: 'Date', isDate: true },
            { field: 'offerType', header: 'Offer Type' },
            { field: 'startBillDate', header: 'Bill Start Date', type: 'date' },
            { field: 'addedServiceName', header: 'Services Added' },
            { field: 'droppedServiceName', header: 'Services Dropped' },
            { field: 'servicePrice', header: 'Free Service Fee',isRandSymbolRequired:true },
            { field: 'status', header: 'Status' }
            ],
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE,
            moduleName: ModulesBasedApiSuffix.SALES,
          }]
      }
    }
    this.getServiceList();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData();
    this.combineLatestNgrxStoreDataOne()

  }

  getCustomerData() {
    this.customerManagementService.getCustomer(this.customerId).subscribe(resp => {
      if (resp.isSuccess && resp.statusCode == 200 && resp.resources) {
        this.customerHeaderModel = resp.resources;
        this.customerHeaderModel.addressId = resp.resources.addresses.length > 0 ? resp.resources.addresses[0].addressId : null;
        this.customerHeaderModel.fullAddress = resp.resources.addresses.length > 0 ? resp.resources.addresses[0].fullAddress : null;
        this.customerHeaderModel.contractId = this.contractId
        this.customerHeaderModel.contractName = this.contractName
        this.store.dispatch(new CustomerHeaderInfoDataCreateAction({ CustomerHeaderInfoDataModel: this.customerHeaderModel }));
      }
    })
  }

  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)])
      .subscribe((response) => {
        let permissions = response[0][CUSTOMER_COMPONENT.CUSTOMER];
        let customerManagement = permissions.find(item => item.menuName == "Customer Dashboard")
        if (customerManagement) {
          let subscription = customerManagement['subMenu'].find(item => item.menuName.includes("SUBSCRIPTION"))
          let subscriptionQuickAction = subscription['subMenu'].find(item => item.menuName.includes("Quick Action"))
          let upgradeAndDowngrade = subscriptionQuickAction['subMenu'].find(item => item.menuName.includes("Upgrade/Downgrade"))
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, upgradeAndDowngrade['subMenu']);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
  }
  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(selectStaticEagerLoadingTicketStatusState$),
    this.store.select(loggedInUserData),
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.ticketStatusList = response[0];
      });
  }


  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.onCRUDRequested(CrudType.GET, row);
  }

  getServiceList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        contractId: this.contractId,
        ...otherParams
      }), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.observableResponse = response;
        this.dataList = this.observableResponse.resources;
        this.dataList.forEach(ele => {
          ele.isUpdateFlag = false;
        });
        this.totalRecords = response.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }

    })
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }


  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

          this.row['searchColumns'] = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    let otherParams = {};
    if (this.searchForm.value.searchKeyword) {
      otherParams["search"] = this.searchForm.value.searchKeyword;
    }

    if (CrudType.GET === type && Object.keys(row).length > 0) {
      // logic for split columns and its values to key value pair
    }
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.getServiceList(row["pageIndex"], row["pageSize"], otherParams);
            break;
        }
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: any | string): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.VIEW:
        let checkEdit = (status) => {
          if (status == "DOA Declined" || status == "New") {
            return true
          } else {
            return false
          }
        }
        let allowEdit = checkEdit(editableObject?.status)
        if (editableObject.offerType == "Upgrade") {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0]?.canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.router.navigate(['/customer/manage-customers/customer-service-upgrade-downgrade/upgrade'], { queryParams: { id: editableObject.upgradeServiceRequestId, canEdit: allowEdit } });
        }
        if (editableObject.offerType == "Downgrade") {
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0]?.canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.router.navigate(['/customer/manage-customers/customer-service-upgrade-downgrade/downgrade'], { queryParams: { id: editableObject.upgradeServiceRequestId, canEdit: allowEdit } });
        }
        break;
    }
  }

  navigateTo(type) {
    if (type == "Upgrade") {
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0]?.canUpgrade) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigate(['/customer/manage-customers/customer-service-upgrade-downgrade/upgrade'], { queryParams: { canEdit: true } });
    } else {
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0]?.canDowngrade) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.router.navigate(['/customer/manage-customers/customer-service-upgrade-downgrade/downgrade'], { queryParams: { canEdit: true } });
    }
  }
  emitViewMore(e) {
    this.isViewMore = e
  }
}
