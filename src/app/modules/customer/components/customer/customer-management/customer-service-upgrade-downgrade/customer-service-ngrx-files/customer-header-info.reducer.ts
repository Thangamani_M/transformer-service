import { CustomerHeaderModel } from '@modules/customer/models/customer-header.model';
import { CustomerHeaderInfoDataActions, CustomerHeaderInfoDataActionTypes } from './customer-header-info.actions';

export function CustomerHeaderInfoDataReducer(state = new CustomerHeaderModel(),
  action: CustomerHeaderInfoDataActions): CustomerHeaderModel {
  switch (action.type) {
    case CustomerHeaderInfoDataActionTypes.CustomerHeaderInfoDataCreateAction:
      return action.payload.CustomerHeaderInfoDataModel;

    case CustomerHeaderInfoDataActionTypes.CustomerHeaderInfoDataChangeAction:
      return { ...state, ...action.payload.CustomerHeaderInfoDataModel };

    case CustomerHeaderInfoDataActionTypes.CustomerHeaderInfoDataRemoveAction:
      return null;

    default:
      return state;
  }
}

