import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerHeaderModel } from '@app/modules/customer/models';
import { AppState } from '@app/reducers';
import { RxjsService } from '@app/shared';

import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { CustomerHeaderInfoDataState$ } from '../customer-service-ngrx-files/customer-header-info.selectors';

declare var $: any;
@Component({
  selector: 'app-customer-header-info',
  templateUrl: './customer-header.component.html',
  styleUrls: ['./customer-header.component.scss']
})
export class CustomHeaderInfoComponent implements OnInit {

  customerHeaderModel: CustomerHeaderModel

  contractId: string
  isShowDetails = false;
  @Output() emitViewMore = new EventEmitter<any>();
  constructor(
    private router: Router, private store: Store<AppState>, private rxjsService: RxjsService,) {

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();


  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(CustomerHeaderInfoDataState$)]
    ).subscribe((response) => {
      this.customerHeaderModel = new CustomerHeaderModel(response[0]);
    });
  }

  navigateToCustomer() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerHeaderModel?.customerId,
      addressId: this.customerHeaderModel?.addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }


  viweMore() {
    this.isShowDetails = !this.isShowDetails
    this.emitViewMore.emit(this.isShowDetails)
  }

}

