import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerHeaderModel, CustomerServicePriceAdjustments, CustomerServicePriceAdjustModel } from '@app/modules/customer/models';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { CustomerHeaderInfoDataState$ } from '../customer-service-ngrx-files/customer-header-info.selectors';

declare var $: any;
@Component({
  selector: 'app-upgrade-approve-decline',
  templateUrl: './upgrade-approve-decline.component.html',
  styleUrls: ['../customer-service-list.component.scss']
})
export class UpgradeApproveDeclineComponent implements OnInit {

  customerHeaderModel: CustomerHeaderModel
  loggedInUserData: LoggedInUserModel;
  adjustCostingDetails: any = {
    upgradeServiceAdjustCostingAmountSummaryDTO: {},
    upgradeServiceAdjustCostingDetailDTOs: [],

  }
  detailsData: any = { taxPercentage: 20 }
  priceAdjustmentForm: FormGroup;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  upgradeServiceAdjustCostingPostDTOs: FormArray;
  customerItemPriceAdjustments: FormArray;
  upgradeServiceRequestId: string
  constructor(
    private router: Router, private store: Store<AppState>, private crudService: CrudService, private rxjsService: RxjsService,
    private dialog: MatDialog, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute) {
    this.upgradeServiceRequestId = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getAdjustCostingDetails();
    this.createPriceAdjustmentForm()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(CustomerHeaderInfoDataState$),
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.customerHeaderModel = new CustomerHeaderModel(response[0]);
      this.loggedInUserData = new LoggedInUserModel(response[1]);
    });
  }

  getAdjustCostingDetails() {
    // this.FA3328A8-1641-40A5-B186-6BEEA590D3D0
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE_ADJUST_COSTING_DETAIL, null, false, prepareRequiredHttpParams({ upgradeServiceRequestId: this.upgradeServiceRequestId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.adjustCostingDetails = response.resources;
        this.upgradeServiceAdjustCostingPostDTOs = this.getupgradeServiceAdjustCostingPostDTOs;
        this.adjustCostingDetails.upgradeServiceAdjustCostingDetailDTOs.forEach((obj) => {
          this.convertTodecimal(obj);
          this.upgradeServiceAdjustCostingPostDTOs.push(this.createupgradeServiceAdjustCostingPostDTOs(obj));
        });
        if(this.adjustCostingDetails.upgradeServiceAdjustCostingAmountSummaryDTO){
          this.priceAdjustmentForm.get('doaConfigId').setValue(this.adjustCostingDetails.upgradeServiceAdjustCostingAmountSummaryDTO.doaConfigId)
          this.doaDetails.doaLevel = this.adjustCostingDetails.upgradeServiceAdjustCostingAmountSummaryDTO.doaAuthorisation
          }

        this.rxjsService.setGlobalLoaderProperty(false);
        this.addServicesAmt()
        this.addServicesPerccent()
      }
    })
  }
  convertTodecimal(obj) {
    obj.adjustedTotal = obj.adjustedTotal ? parseFloat(obj.adjustedTotal).toFixed(2) : 0;
    obj.discountAmount = obj.discountAmount ? parseFloat(obj.discountAmount).toFixed(2) : 0;
    obj.amount = obj.amount ? parseFloat(obj.amount).toFixed(2) : 0;
    obj.subTotal = obj.subTotal ? parseFloat(obj.subTotal).toFixed(2) : 0;
    obj.totalAmount = obj.totalAmount ? parseFloat(obj.totalAmount).toFixed(2) : 0;
    obj.vat = obj.vat ? parseFloat(obj.vat).toFixed(2) : 0;
    return obj;
  }

  createPriceAdjustmentForm() {
    let priceAdjustModel = new CustomerServicePriceAdjustModel();
    this.priceAdjustmentForm = this.formBuilder.group({
      upgradeServiceAdjustCostingPostDTOs: this.formBuilder.array([]),
      customerItemPriceAdjustments: this.formBuilder.array([])
    });
    Object.keys(priceAdjustModel).forEach((key) => {
      this.priceAdjustmentForm.addControl(key, new FormControl(priceAdjustModel[key]));
    });
    this.priceAdjustmentForm = setRequiredValidator(this.priceAdjustmentForm, ["doaConfigId", "upgradeServiceRequestId"]);
    this.priceAdjustmentForm.get('upgradeServiceRequestId').setValue(this.upgradeServiceRequestId)
    this.priceAdjustmentForm.get('createdUserId').setValue(this.loggedInUserData.userId)
  }

  get getupgradeServiceAdjustCostingPostDTOs(): FormArray {
    if (!this.priceAdjustmentForm) return;
    return this.priceAdjustmentForm.get("upgradeServiceAdjustCostingPostDTOs") as FormArray;
  }


  //Create FormArray controls
  createupgradeServiceAdjustCostingPostDTOs(upgradeServiceAdjustCostingPostDTOs?: CustomerServicePriceAdjustments): FormGroup {
    let upgradeServiceAdjustCostingPostDTOsModel = new CustomerServicePriceAdjustments(upgradeServiceAdjustCostingPostDTOs);
    let formControls = {};
    Object.keys(upgradeServiceAdjustCostingPostDTOsModel).forEach((key) => {
      if (key === 'amount') {
        formControls[key] = [{ value: upgradeServiceAdjustCostingPostDTOsModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: upgradeServiceAdjustCostingPostDTOsModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }







  summaryData = {
    percentage: "",
    amount: ""
  }

  addServicesAmt() {
    let dt = this.priceAdjustmentForm.get('upgradeServiceAdjustCostingPostDTOs').value;
    let sum = 0;
    if (dt && dt.length > 0) {
      if (!this.priceAdjustmentForm.value.rewardId && this.priceAdjustmentForm.value.isReward) {
        sum = 0;
      } else {
        dt.forEach(element => {
          sum = sum + parseFloat(element.discountAmount);
        });
      }
      this.priceAdjustmentForm.get('serviceDiscountAmount').setValue(JSON.parse(JSON.stringify(sum.toFixed(2))));
      this.summaryData.amount = 'R ' + sum.toFixed(2)
    } else {
      this.summaryData.amount = 'R ' + '0'
    }
  }

  addServicesPerccent() {
    let dt = this.priceAdjustmentForm.get('upgradeServiceAdjustCostingPostDTOs').value;
    let sum = 0;
    let adjust: number = 0;
    let finalTotal: number = 0;
    if (dt && dt.length > 0) {
      dt.forEach(element => {
        sum = sum + parseFloat(element.discountAmount);
        adjust = adjust + parseFloat(element.subTotal);
        finalTotal = (sum / adjust) * 100;
      });

      this.priceAdjustmentForm.get('serviceDiscountPercentage').setValue((finalTotal).toFixed(2));
      this.summaryData.percentage =  (finalTotal).toFixed(2) + ' %';
    } else {
      this.summaryData.percentage = 0 + ' %';

    }
  }

  doaDetails: any = {}


  approve() {
    let obj = {
      upgradeServiceRequestId:this.upgradeServiceRequestId,
      doaConfigId: this.adjustCostingDetails.upgradeServiceAdjustCostingAmountSummaryDTO.doaConfigId,
      createdUserId: this.loggedInUserData?.userId
    }
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE_DOA_APPROVAL, obj).subscribe(response=>{
      if(response.isSuccess && response.statusCode ==200){
        this.router.navigate(['/my-tasks/my-customer']);
      }
    })
  }

  decline(){
    let obj = {
      upgradeServiceRequestId:this.upgradeServiceRequestId,
      createdUserId: this.loggedInUserData?.userId
    }
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE_DOA_DECLINED, obj).subscribe(response=>{
      if(response.isSuccess && response.statusCode ==200){
        this.router.navigate(['/my-tasks/my-customer']);
      }
    })
  }

  goBack(){
    this.router.navigate(['/customer/manage-customers/customer-service-upgrade-downgrade/upgrade'],{queryParams:{id:this.upgradeServiceRequestId}})
  }
  goNext(){
    this.router.navigate(['/customer/manage-customers/customer-service-upgrade-downgrade/upgrade/summary'],{queryParams:{id:this.upgradeServiceRequestId}})
  }

  isViewMore =false
    emitViewMore(e){
      this.isViewMore = e
    }
}
