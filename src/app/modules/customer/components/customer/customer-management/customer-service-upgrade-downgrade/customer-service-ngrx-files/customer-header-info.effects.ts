import { Injectable } from '@angular/core';
import { AppDataService } from '@app/shared/services';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { defer, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CustomerHeaderInfoDataActionTypes, CustomerHeaderInfoDataChangeAction, CustomerHeaderInfoDataCreateAction, CustomerHeaderInfoDataRemoveAction } from './customer-header-info.actions';

@Injectable()
export class CustomerHeaderInfoEffects {
  @Effect({ dispatch: false })
  createData$ = this.actions$.pipe(
    ofType<CustomerHeaderInfoDataCreateAction>(CustomerHeaderInfoDataActionTypes.CustomerHeaderInfoDataCreateAction),
    tap(action =>
      this.appDataService.dealerCustomerCreationServiceAgreementData = action.payload.CustomerHeaderInfoDataModel)
  );

  @Effect({ dispatch: false })
  updateData$ = this.actions$.pipe(
    ofType<CustomerHeaderInfoDataChangeAction>(CustomerHeaderInfoDataActionTypes.CustomerHeaderInfoDataChangeAction),
    tap(action =>
      this.appDataService.dealerCustomerCreationServiceAgreementData = action.payload.CustomerHeaderInfoDataModel)
  );

  @Effect({ dispatch: false })
  removeData$ = this.actions$.pipe(
    ofType<CustomerHeaderInfoDataRemoveAction>(CustomerHeaderInfoDataActionTypes.CustomerHeaderInfoDataRemoveAction),
    tap(() =>
      this.appDataService.dealerCustomerCreationServiceAgreementData = null
    )
  );

  @Effect()
  initUserData$ = defer(() => {
    const CustomerHeaderInfoDataModel = this.appDataService.dealerCustomerCreationServiceAgreementData;
    if (CustomerHeaderInfoDataModel) {
      return of(new CustomerHeaderInfoDataCreateAction({ CustomerHeaderInfoDataModel }));
    }
    else {
      return <any>of(new CustomerHeaderInfoDataRemoveAction());
    }
  });

  constructor(private actions$: Actions, private appDataService: AppDataService) {

  }

}
