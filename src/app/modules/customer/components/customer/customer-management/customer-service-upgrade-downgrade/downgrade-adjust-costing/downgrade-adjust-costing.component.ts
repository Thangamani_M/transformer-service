import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerHeaderModel, DowngradeServicePriceAdjustments, DowngradeServicePriceAdjustModel } from '@app/modules/customer/models';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { CustomerHeaderInfoDataState$ } from '../customer-service-ngrx-files/customer-header-info.selectors';

declare var $: any;
@Component({
  selector: 'app-downgrade-adjust-costing',
  templateUrl: './downgrade-adjust-costing.component.html',
  styleUrls: ['../customer-service-list.component.scss']
})
export class DowngradeAdjustCostingComponent implements OnInit {

  customerHeaderModel: CustomerHeaderModel
  loggedInUserData: LoggedInUserModel;
  contractId: string
  adjustCostingDetails: any = {
    downgradeServiceAdjustCostingAmountSummaryDTO: {},
    downgradeServiceAdjustCostingDetailDTOs: [],

  }
  detailsData: any = { taxPercentage: 20 }
  priceAdjustmentForm: FormGroup;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  downgradeServiceAdjustCostingPostDTOs: FormArray;
  customerItemPriceAdjustments: FormArray;
  downgradeServiceRequestId: string
  canEdit=true;
  constructor(
    private router: Router, private store: Store<AppState>, private crudService: CrudService, private rxjsService: RxjsService,
    private dialog: MatDialog, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute) {
    this.downgradeServiceRequestId = this.activatedRoute.snapshot.queryParams.id
    if(this.activatedRoute.snapshot.queryParams.canEdit){
      this.canEdit = this.activatedRoute.snapshot.queryParams.canEdit == "false"? false:true
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getAdjustCostingDetails();
    this.createPriceAdjustmentForm()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(CustomerHeaderInfoDataState$),
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.customerHeaderModel = new CustomerHeaderModel(response[0]);
      this.loggedInUserData = new LoggedInUserModel(response[1]);
    });
  }

  getAdjustCostingDetails() {
    // this.FA3328A8-1641-40A5-B186-6BEEA590D3D0
    if (this.downgradeServiceAdjustCostingPostDTOs) {
      this.priceAdjustmentForm.reset();
      this.createPriceAdjustmentForm()
    }
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_DOWNGRADE_ADJUST_COSTING_DETAIL, null, false, prepareRequiredHttpParams({ downgradeServiceRequestId: this.downgradeServiceRequestId || "" ,contractId: this.customerHeaderModel.contractId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.adjustCostingDetails = response.resources;
        this.downgradeServiceAdjustCostingPostDTOs = this.getdowngradeServiceAdjustCostingPostDTOs;
        this.adjustCostingDetails.downgradeServiceAdjustCostingDetailDTOs.forEach((obj) => {
          this.convertTodecimal(obj);
          this.downgradeServiceAdjustCostingPostDTOs.push(this.createdowngradeServiceAdjustCostingPostDTOs(obj));
        });
        if (this.adjustCostingDetails.downgradeServiceAdjustCostingAmountSummaryDTO) {
          this.priceAdjustmentForm.get('doaConfigId').setValue(this.adjustCostingDetails.downgradeServiceAdjustCostingAmountSummaryDTO.doaConfigId)
          this.doaDetails.doaLevel = this.adjustCostingDetails.downgradeServiceAdjustCostingAmountSummaryDTO.doaAuthorisation
        }

        this.rxjsService.setGlobalLoaderProperty(false);
        this.addServicesAmt()
        this.addServicesPerccent()
      }
    })
  }
  convertTodecimal(obj) {
    obj.adjustedTotal = obj.adjustedTotal!=null ? parseFloat(obj.adjustedTotal).toFixed(2) : 0.00;
    obj.discountAmount = obj.discountAmount!=null ? parseFloat(obj.discountAmount).toFixed(2) : 0.00;
    obj.amount = obj.amount!=null ? parseFloat(obj.amount).toFixed(2) : 0.00;
    obj.subTotal = obj.subTotal!=null ? parseFloat(obj.subTotal).toFixed(2) : 0.00;
    obj.totalAmount = obj.totalAmount!=null ? parseFloat(obj.totalAmount).toFixed(2) : 0.00;
    obj.vat = obj.vat !=null ? parseFloat(obj.vat).toFixed(2) : 0.00
    return obj;
  }


  createPriceAdjustmentForm() {
    let priceAdjustModel = new DowngradeServicePriceAdjustModel();
    this.priceAdjustmentForm = this.formBuilder.group({
      downgradeServiceAdjustCostingPostDTOs: this.formBuilder.array([]),
      customerItemPriceAdjustments: this.formBuilder.array([])
    });
    Object.keys(priceAdjustModel).forEach((key) => {
      this.priceAdjustmentForm.addControl(key, new FormControl(priceAdjustModel[key]));
    });
    // this.priceAdjustmentForm = setRequiredValidator(this.priceAdjustmentForm, ["downgradeServiceRequestId"]);
    this.priceAdjustmentForm.get('downgradeServiceRequestId').setValue(this.downgradeServiceRequestId || "")
    this.priceAdjustmentForm.get('createdUserId').setValue(this.loggedInUserData.userId)
    this.priceAdjustmentForm.get('customerId').setValue(this.customerHeaderModel.customerId)
    this.priceAdjustmentForm.get('contractId').setValue(this.customerHeaderModel.contractId)
    this.priceAdjustmentForm.get('addressId').setValue(this.customerHeaderModel.addressId)
  }

  get getdowngradeServiceAdjustCostingPostDTOs(): FormArray {
    if (!this.priceAdjustmentForm) return;
    return this.priceAdjustmentForm.get("downgradeServiceAdjustCostingPostDTOs") as FormArray;
  }


  //Create FormArray controls
  createdowngradeServiceAdjustCostingPostDTOs(downgradeServiceAdjustCostingPostDTOs?: DowngradeServicePriceAdjustments): FormGroup {
    let downgradeServiceAdjustCostingPostDTOsModel = new DowngradeServicePriceAdjustments(downgradeServiceAdjustCostingPostDTOs);
    let formControls = {};
    Object.keys(downgradeServiceAdjustCostingPostDTOsModel).forEach((key) => {
      if (key === 'amount') {
        formControls[key] = [{ value: downgradeServiceAdjustCostingPostDTOsModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: downgradeServiceAdjustCostingPostDTOsModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }




  changeServiceAmount(index) {

    if (this.getdowngradeServiceAdjustCostingPostDTOs.controls[index].get("isDiscountAmount").value == true) {
      this.getdowngradeServiceAdjustCostingPostDTOs.controls[index].get("isDiscountPercentage").setValue(false);
      this.getdowngradeServiceAdjustCostingPostDTOs.controls[index].get("amount").setValue("");
      this.getdowngradeServiceAdjustCostingPostDTOs.controls[index].get("amount").clearValidators();
      this.getdowngradeServiceAdjustCostingPostDTOs.controls[index].get("amount").updateValueAndValidity();
    }
    this.onPercentageChanges(index, 'service')
  }

  changeServicePercentage(index) {
    if (this.getdowngradeServiceAdjustCostingPostDTOs.controls[index].get("isDiscountPercentage").value == true) {
      this.getdowngradeServiceAdjustCostingPostDTOs.controls[index].get("isDiscountAmount").setValue(false)
      this.getdowngradeServiceAdjustCostingPostDTOs.controls[index].get("amount").setValue(0);
      this.getdowngradeServiceAdjustCostingPostDTOs.controls[index].get("amount").setValidators([Validators.max(100)]);
      this.getdowngradeServiceAdjustCostingPostDTOs.controls[index].get("amount").updateValueAndValidity();
    }
    this.onPercentageChanges(index, 'service')
  }



  onPercentageChanges(i, str) {
    let isDiscountPercentage = this.getdowngradeServiceAdjustCostingPostDTOs.controls[i].get('isDiscountPercentage').value;
    let isDiscountAmount = this.getdowngradeServiceAdjustCostingPostDTOs.controls[i].get('isDiscountAmount').value;
    let discountValue = this.getdowngradeServiceAdjustCostingPostDTOs.controls[i].get('discountAmount').value;
    let entredAmount = this.getdowngradeServiceAdjustCostingPostDTOs.controls[i].get('amount').value;
    if (!entredAmount) {
      entredAmount = 0;
    }
    discountValue = entredAmount
    let subtotal = this.getdowngradeServiceAdjustCostingPostDTOs.controls[i].get('subTotal').value;
    if (isDiscountAmount) {
      this.getdowngradeServiceAdjustCostingPostDTOs.controls[i].get("amount").setValidators([Validators.max(subtotal)]);
    }
    let totalAmount = this.getdowngradeServiceAdjustCostingPostDTOs.controls[i].get('totalAmount').value;
    let vat = this.getdowngradeServiceAdjustCostingPostDTOs.controls[i].get('vat').value;
    let adjustedTotal = this.getdowngradeServiceAdjustCostingPostDTOs.controls[i].get('adjustedTotal').value;

    if (isDiscountPercentage) {
      discountValue = (discountValue * parseFloat(subtotal)) / 100;
    }
    adjustedTotal = subtotal - discountValue;

    vat = (adjustedTotal * this.customerHeaderModel?.taxPrice) / 100;
    totalAmount = parseFloat(adjustedTotal) + parseFloat(vat);
    let dis = (discountValue / subtotal) * 100;
    this.getdowngradeServiceAdjustCostingPostDTOs.controls[i].get('discountPercentage').setValue(dis);
    this.getdowngradeServiceAdjustCostingPostDTOs.controls[i].get('discountAmount').setValue(parseFloat(discountValue).toFixed(2));
    this.getdowngradeServiceAdjustCostingPostDTOs.controls[i].get('totalAmount').setValue(parseFloat(totalAmount).toFixed(2));
    this.getdowngradeServiceAdjustCostingPostDTOs.controls[i].get('vat').setValue(parseFloat(vat).toFixed(2));
    this.getdowngradeServiceAdjustCostingPostDTOs.controls[i].get('adjustedTotal').setValue(parseFloat(adjustedTotal).toFixed(2));
    this.addServicesAmt()
    this.addServicesPerccent()
  }
  summaryData = {
    percentage: "",
    amount: ""
  }

  addServicesAmt() {
    let dt = this.priceAdjustmentForm.get('downgradeServiceAdjustCostingPostDTOs').value;
    let sum = 0;
    if (dt && dt.length > 0) {
      if (!this.priceAdjustmentForm.value.rewardId && this.priceAdjustmentForm.value.isReward) {
        sum = 0;
      } else {
        dt.forEach(element => {
          sum = sum + parseFloat(element.discountAmount);
        });
      }
      this.priceAdjustmentForm.get('serviceDiscountAmount').setValue(JSON.parse(JSON.stringify(sum.toFixed(2))));
      this.summaryData.amount = 'R ' + sum.toFixed(2)
    } else {
      this.summaryData.amount = 'R ' + '0'
    }
  }

  addServicesPerccent() {
    let dt = this.priceAdjustmentForm.get('downgradeServiceAdjustCostingPostDTOs').value;
    let sum = 0;
    let adjust: number = 0;
    let finalTotal: any = 0;
    if (dt && dt.length > 0) {
      dt.forEach(element => {
        sum = sum + parseFloat(element.discountAmount);
        adjust = adjust + parseFloat(element.subTotal);
        finalTotal = (sum / adjust) * 100;
      });

      this.priceAdjustmentForm.get('serviceDiscountPercentage').setValue((finalTotal).toFixed(2));
      this.summaryData.percentage =  (finalTotal).toFixed(2) + ' %';
    } else {
      this.summaryData.percentage = 0 + ' %';

    }
  }

  doaDetails: any = {}
  getDoaConfig() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE_DOA_CONFIG_DETAIL, undefined, false, prepareGetRequestHttpParams(null, null, {
      discountPercentage: this.priceAdjustmentForm.value.serviceDiscountPercentage,
      discountPrice: this.priceAdjustmentForm.value.serviceDiscountAmount,
    }), 1).subscribe(resp => {
        if (resp.resources && resp.isSuccess && resp.statusCode == 200){
          this.doaDetails = resp.resources;
        }else{
          this.doaDetails =  { doaConfigId :null,doaLevel:"-" }
        }
      this.priceAdjustmentForm.get('doaConfigId').setValue(this.doaDetails.doaConfigId)
      this.rxjsService.setGlobalLoaderProperty(false)
    });
  }

  submit() {

    if (this.priceAdjustmentForm.invalid) {
      return ''
    }

    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_DOWNGRADE_ADJUST_COSTING, this.priceAdjustmentForm.getRawValue()).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.downgradeServiceRequestId = response.resources
        this.goNext();
      }
    })
  }

  goBack() {
    this.router.navigate(['/customer/manage-customers/customer-service-upgrade-downgrade/downgrade'], { queryParams: { id: this.downgradeServiceRequestId,canEdit:this.canEdit } })
  }
  goNext() {
    this.router.navigate(['/customer/manage-customers/customer-service-upgrade-downgrade/downgrade/summary'], { queryParams: { id: this.downgradeServiceRequestId,canEdit:this.canEdit } })
  }
  isViewMore = false
  emitViewMore(e) {
    this.isViewMore = e
  }


}
