import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { CustomHeaderInfoModule } from "./customer-header/customer-header.module";
import { CustomerServiceListComponent } from "./customer-service-list.component";
import { CustomerHeaderInfoDataReducer, CustomerHeaderInfoEffects } from './customer-service-ngrx-files';
import { CustomerServiceListRoutingModule } from "./customer-service-routing.module";
import { CustomerServiceDowngradeComponent } from "./downgrade-add-service/downgrade-add-service.component";
import { DowngradeAdjustCostingComponent } from "./downgrade-adjust-costing/downgrade-adjust-costing.component";
import { DowngradeApproveDeclineComponent } from "./downgrade-approve-decline/downgrade-approve-decline.component";
import { ServiceDowngradeSummaryComponent } from "./downgrade-summary/downgrade-summary.component";
import { DowngradeRevisedSummaryModalComponent } from "./downgrade-summary/revised-summary/revised-summary.component";
import { CustomerServicePopupComponent } from './upgrade-add-service/add-service/customer-service-popup.component';
import { CustomerServiceUpgradeComponent } from './upgrade-add-service/upgrade-add-service.component';
import { UpgradeAdjustCostingComponent } from './upgrade-adjust-costing/upgrade-adjust-costing.component';
import { UpgradeApproveDeclineComponent } from "./upgrade-approve-decline/upgrade-approve-decline.component";
import { RevisedSummaryModalComponent } from "./upgrade-summary/revised-summary/revised-summary.component";
import { ServiceUpgradeSummaryComponent } from "./upgrade-summary/upgrade-summary.component";
@NgModule({
  declarations: [CustomerServiceListComponent, CustomerServiceUpgradeComponent, CustomerServicePopupComponent,
    UpgradeAdjustCostingComponent, ServiceUpgradeSummaryComponent, RevisedSummaryModalComponent, DowngradeRevisedSummaryModalComponent, CustomerServiceDowngradeComponent, DowngradeAdjustCostingComponent, ServiceDowngradeSummaryComponent,
    UpgradeApproveDeclineComponent,DowngradeApproveDeclineComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    StoreModule.forFeature('customerHeaderInfoData', CustomerHeaderInfoDataReducer),
    EffectsModule.forFeature([CustomerHeaderInfoEffects]),
    CustomerServiceListRoutingModule,
    CustomHeaderInfoModule
  ],
  exports: [ CustomerServicePopupComponent, RevisedSummaryModalComponent, DowngradeRevisedSummaryModalComponent],
  entryComponents: [ CustomerServicePopupComponent, RevisedSummaryModalComponent, DowngradeRevisedSummaryModalComponent],
})
export class CustomerServiceListModule { }
