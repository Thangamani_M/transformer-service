import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerHeaderModel, CustomerServicePriceAdjustments, CustomerServicePriceAdjustModel } from '@app/modules/customer/models';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { CustomerHeaderInfoDataState$ } from '../customer-service-ngrx-files/customer-header-info.selectors';

declare var $: any;
@Component({
  selector: 'app-upgrade-adjust-costing',
  templateUrl: './upgrade-adjust-costing.component.html',
  styleUrls: ['../customer-service-list.component.scss']
})
export class UpgradeAdjustCostingComponent implements OnInit {

  customerHeaderModel: CustomerHeaderModel
  loggedInUserData: LoggedInUserModel;
  contractId: string
  adjustCostingDetails: any = {
    upgradeServiceAdjustCostingAmountSummaryDTO: {},
    upgradeServiceAdjustCostingDetailDTOs: [],

  }
  detailsData: any = { taxPercentage: 20 }
  priceAdjustmentForm: FormGroup;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  upgradeServiceAdjustCostingPostDTOs: FormArray;
  customerItemPriceAdjustments: FormArray;
  upgradeServiceRequestId: string
  canEdit =false
  constructor(
    private router: Router, private store: Store<AppState>, private crudService: CrudService, private rxjsService: RxjsService,
    private dialog: MatDialog, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute) {
    this.upgradeServiceRequestId = this.activatedRoute.snapshot.queryParams.id
    if(this.activatedRoute.snapshot.queryParams.canEdit){
      this.canEdit = this.activatedRoute.snapshot.queryParams.canEdit == "false"? false:true
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getAdjustCostingDetails();
    this.createPriceAdjustmentForm()
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(CustomerHeaderInfoDataState$),
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.customerHeaderModel = new CustomerHeaderModel(response[0]);
      this.loggedInUserData = new LoggedInUserModel(response[1]);
    });
  }

  getAdjustCostingDetails() {
    // this. FA3328A8-1641-40A5-B186-6BEEA590D3D0
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE_ADJUST_COSTING_DETAIL, null, false, prepareRequiredHttpParams({ upgradeServiceRequestId: this.upgradeServiceRequestId || "", contractId: this.customerHeaderModel.contractId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.adjustCostingDetails = response.resources;
        this.upgradeServiceAdjustCostingPostDTOs = this.getupgradeServiceAdjustCostingPostDTOs;
        this.adjustCostingDetails.upgradeServiceAdjustCostingDetailDTOs.forEach((obj) => {
          this.convertTodecimal(obj);
          this.upgradeServiceAdjustCostingPostDTOs.push(this.createupgradeServiceAdjustCostingPostDTOs(obj));
        });
        if(this.adjustCostingDetails.upgradeServiceAdjustCostingAmountSummaryDTO){
          this.priceAdjustmentForm.get('doaConfigId').setValue(this.adjustCostingDetails.upgradeServiceAdjustCostingAmountSummaryDTO.doaConfigId)
          this.doaDetails.doaLevel = this.adjustCostingDetails.upgradeServiceAdjustCostingAmountSummaryDTO.doaAuthorisation
          }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.addServicesAmt()
        this.addServicesPerccent()
      }
    })
  }
  convertTodecimal(obj) {
    obj.adjustedTotal = obj.adjustedTotal!=null ? parseFloat(obj.adjustedTotal).toFixed(2) : 0.00;
    obj.discountAmount = obj.discountAmount!=null ? parseFloat(obj.discountAmount).toFixed(2) : 0.00;
    obj.amount = obj.amount!=null ? parseFloat(obj.amount).toFixed(2) : 0.00;
    obj.subTotal = obj.subTotal!=null ? parseFloat(obj.subTotal).toFixed(2) : 0.00;
    obj.totalAmount = obj.totalAmount!=null ? parseFloat(obj.totalAmount).toFixed(2) : 0.00;
    obj.vat = obj.vat !=null ? parseFloat(obj.vat).toFixed(2) : 0.00
    return obj;
  }

  createPriceAdjustmentForm() {
    let priceAdjustModel = new CustomerServicePriceAdjustModel();
    this.priceAdjustmentForm = this.formBuilder.group({
      upgradeServiceAdjustCostingPostDTOs: this.formBuilder.array([]),
      customerItemPriceAdjustments: this.formBuilder.array([])
    });
    Object.keys(priceAdjustModel).forEach((key) => {
      this.priceAdjustmentForm.addControl(key, new FormControl(priceAdjustModel[key]));
    });
    // this.priceAdjustmentForm = setRequiredValidator(this.priceAdjustmentForm, ["]);
    this.priceAdjustmentForm.get('upgradeServiceRequestId').setValue(this.upgradeServiceRequestId|| "")
    this.priceAdjustmentForm.get('createdUserId').setValue(this.loggedInUserData.userId)
    this.priceAdjustmentForm.get('customerId').setValue(this.customerHeaderModel.customerId)
    this.priceAdjustmentForm.get('contractId').setValue(this.customerHeaderModel.contractId)
    this.priceAdjustmentForm.get('addressId').setValue(this.customerHeaderModel.addressId)
  }

  get getupgradeServiceAdjustCostingPostDTOs(): FormArray {
    if (!this.priceAdjustmentForm) return;
    return this.priceAdjustmentForm.get("upgradeServiceAdjustCostingPostDTOs") as FormArray;
  }


  //Create FormArray controls
  createupgradeServiceAdjustCostingPostDTOs(upgradeServiceAdjustCostingPostDTOs?: CustomerServicePriceAdjustments): FormGroup {
    let upgradeServiceAdjustCostingPostDTOsModel = new CustomerServicePriceAdjustments(upgradeServiceAdjustCostingPostDTOs);
    let formControls = {};
    Object.keys(upgradeServiceAdjustCostingPostDTOsModel).forEach((key) => {
      if (key === 'amount') {
        formControls[key] = [{ value: upgradeServiceAdjustCostingPostDTOsModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: upgradeServiceAdjustCostingPostDTOsModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }




  changeServiceAmount(index) {

    if (this.getupgradeServiceAdjustCostingPostDTOs.controls[index].get("isDiscountAmount").value == true) {
      this.getupgradeServiceAdjustCostingPostDTOs.controls[index].get("isDiscountPercentage").setValue(false);
      this.getupgradeServiceAdjustCostingPostDTOs.controls[index].get("amount").setValue("");
      this.getupgradeServiceAdjustCostingPostDTOs.controls[index].get("amount").clearValidators();
      this.getupgradeServiceAdjustCostingPostDTOs.controls[index].get("amount").updateValueAndValidity();
    }
    this.onPercentageChanges(index, 'service')
  }

  changeServicePercentage(index) {
    if (this.getupgradeServiceAdjustCostingPostDTOs.controls[index].get("isDiscountPercentage").value == true) {
      this.getupgradeServiceAdjustCostingPostDTOs.controls[index].get("isDiscountAmount").setValue(false)
      this.getupgradeServiceAdjustCostingPostDTOs.controls[index].get("amount").setValue("");
      this.getupgradeServiceAdjustCostingPostDTOs.controls[index].get("amount").setValidators([Validators.max(100)]);
      this.getupgradeServiceAdjustCostingPostDTOs.controls[index].get("amount").updateValueAndValidity();
    }
    this.onPercentageChanges(index, 'service')
  }



  onPercentageChanges(i, str) {
    let isDiscountPercentage = this.getupgradeServiceAdjustCostingPostDTOs.controls[i].get('isDiscountPercentage').value;
    let isDiscountAmount = this.getupgradeServiceAdjustCostingPostDTOs.controls[i].get('isDiscountAmount').value;
    let discountValue = this.getupgradeServiceAdjustCostingPostDTOs.controls[i].get('discountAmount').value;
    let entredAmount = this.getupgradeServiceAdjustCostingPostDTOs.controls[i].get('amount').value;
    if (!entredAmount) {
      entredAmount = 0;
    }
    discountValue = entredAmount
    let subtotal = this.getupgradeServiceAdjustCostingPostDTOs.controls[i].get('subTotal').value;
    if (isDiscountAmount) {
      this.getupgradeServiceAdjustCostingPostDTOs.controls[i].get("amount").setValidators([Validators.max(subtotal)]);
    }
    let totalAmount = this.getupgradeServiceAdjustCostingPostDTOs.controls[i].get('totalAmount').value;
    let vat = this.getupgradeServiceAdjustCostingPostDTOs.controls[i].get('vat').value;
    let adjustedTotal = this.getupgradeServiceAdjustCostingPostDTOs.controls[i].get('adjustedTotal').value;

    if (isDiscountPercentage) {
      discountValue = (discountValue * parseFloat(subtotal)) / 100;
    }
    adjustedTotal = subtotal - discountValue;
    vat = (adjustedTotal * this.customerHeaderModel?.taxPrice) / 100;
    totalAmount = parseFloat(adjustedTotal) + parseFloat(vat);
    let dis = (discountValue / subtotal) * 100;
    this.getupgradeServiceAdjustCostingPostDTOs.controls[i].get('discountPercentage').setValue(dis);
    this.getupgradeServiceAdjustCostingPostDTOs.controls[i].get('discountAmount').setValue(parseFloat(discountValue).toFixed(2));
    this.getupgradeServiceAdjustCostingPostDTOs.controls[i].get('totalAmount').setValue(parseFloat(totalAmount).toFixed(2));
    this.getupgradeServiceAdjustCostingPostDTOs.controls[i].get('vat').setValue(parseFloat(vat).toFixed(2));
    this.getupgradeServiceAdjustCostingPostDTOs.controls[i].get('adjustedTotal').setValue(parseFloat(adjustedTotal).toFixed(2));
    this.addServicesAmt()
    this.addServicesPerccent()
  }
  summaryData = {
    percentage: "",
    amount: ""
  }
isAppliedDiscount : boolean = false;
  addServicesAmt() {
    let dt = this.priceAdjustmentForm.get('upgradeServiceAdjustCostingPostDTOs').value;
    let sum = 0;
    if (dt && dt.length > 0) {
      if (!this.priceAdjustmentForm.value.rewardId && this.priceAdjustmentForm.value.isReward) {
        sum = 0;
      } else {
        dt.forEach(element => {
          sum = sum + parseFloat(element.discountAmount);
        });
      }
      this.priceAdjustmentForm.get('serviceDiscountAmount').setValue(JSON.parse(JSON.stringify(sum.toFixed(2))));
      this.summaryData.amount = 'R ' + sum.toFixed(2)
      if(sum > 0){
        this.isAppliedDiscount = true;
      }
    } else {
      this.isAppliedDiscount = false;
      this.summaryData.amount = 'R ' + '0'
    }
  }

  addServicesPerccent() {
    let dt = this.priceAdjustmentForm.get('upgradeServiceAdjustCostingPostDTOs').value;
    let sum = 0;
    let adjust: number = 0;
    let finalTotal:any = 0;
    if (dt && dt.length > 0) {
      dt.forEach(element => {
        sum = sum + parseFloat(element.discountAmount);
        adjust = adjust + parseFloat(element.subTotal);
        finalTotal = (sum / adjust) * 100;
      });

      this.priceAdjustmentForm.get('serviceDiscountPercentage').setValue((finalTotal).toFixed(2));
      this.summaryData.percentage =  (finalTotal).toFixed(2) + ' %';
    } else {
      this.summaryData.percentage = 0 + ' %';

    }
  }

  doaDetails: any = {}
  getDoaConfig() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE_DOA_CONFIG_DETAIL, undefined, false, prepareGetRequestHttpParams(null, null, {
      discountPercentage: this.priceAdjustmentForm.value.serviceDiscountPercentage,
      discountPrice: this.priceAdjustmentForm.value.serviceDiscountAmount,
    }), 1).subscribe(resp => {
      if (resp.resources && resp.isSuccess && resp.statusCode == 200){
        this.doaDetails = resp.resources;
      }else{
        this.doaDetails =  { doaConfigId :null,doaLevel:"-" }
      }
      this.priceAdjustmentForm.get('doaConfigId').setValue(this.doaDetails.doaConfigId)
      this.rxjsService.setGlobalLoaderProperty(false)
    });
  }

  submit() {

    if (this.priceAdjustmentForm.invalid) {
      return ''
    }

    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE_ADJUST_COSTING, this.priceAdjustmentForm.getRawValue()).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.upgradeServiceRequestId = response.resources
        this.goNext();
      }
    })
  }

  goBack(){
    this.router.navigate(['/customer/manage-customers/customer-service-upgrade-downgrade/upgrade'],{queryParams:{id:this.upgradeServiceRequestId,canEdit:this.canEdit}})
  }
  goNext(){
    this.router.navigate(['/customer/manage-customers/customer-service-upgrade-downgrade/upgrade/summary'],{queryParams:{id:this.upgradeServiceRequestId,canEdit:this.canEdit}})
  }

  isViewMore =false
    emitViewMore(e){
      this.isViewMore = e
    }
}
