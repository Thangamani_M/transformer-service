import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { CrudService, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-revised-summery',
  templateUrl: './revised-summary.component.html',
  styleUrls: ['../../customer-service-list.component.scss']
})

export class DowngradeRevisedSummaryModalComponent implements OnInit {
  servicesSummary: any;

  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private rxjsService: RxjsService, private router: Router) {
  }

  ngOnInit(): void {
    this.getRevisedSummary();
  }

  getRevisedSummary() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_DOWNGRADE_COMPARISON_SUMMARY, null, false, prepareRequiredHttpParams({ contractId: this.config.data.contractId, downgradeServiceRequestId: this.config.data.downgradeServiceRequestId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.servicesSummary = response.resources;
        if (this.servicesSummary.startBillDate) {
          this.servicesSummary.startBillDate = new Date(this.servicesSummary.startBillDate);
        }
      }
      this.rxjsService.setPopupLoaderProperty(false);
    })
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

  onSubmit() {
    let payload = {
      downgradeServiceRequestId: this.config.data.downgradeServiceRequestId,
      doaConfigId: this.servicesSummary.doaConfigId,
      createdUserId: this.config.data?.userId,
      isFinduService: this.config.data?.isFinduService,
      finduUserId: this.config.data?.finduUserId
    }
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_DOWNGRADE_ACCEPT_CONTRACT, payload).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.router.navigate(['customer/manage-customers/customer-service-upgrade-downgrade'], { queryParams: { addressId: this.config.data.addressId, contractId: this.config.data.contractId, customerId: this.config.data.customerId, contractName: this.config.data.contractName } })
        this.dialogClose();
      }
    });
  }
}
