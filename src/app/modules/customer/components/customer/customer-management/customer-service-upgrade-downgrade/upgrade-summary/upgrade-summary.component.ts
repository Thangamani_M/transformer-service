import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerHeaderModel } from '@app/modules/customer/models';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { loggedInUserData, selectStaticEagerLoadingTitlesState$ } from '@modules/others';
import { AdditionalUsers, FinduSubscriberModel, SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/components/common/api';
import { combineLatest, forkJoin } from 'rxjs';
import { CustomerHeaderInfoDataState$ } from '../customer-service-ngrx-files/customer-header-info.selectors';
import { RevisedSummaryModalComponent } from './revised-summary/revised-summary.component';
@Component({
  selector: 'app-customer-upgrade-summary',
  templateUrl: './upgrade-summary.component.html',
  styleUrls: ['../customer-service-list.component.scss']
})
export class ServiceUpgradeSummaryComponent implements OnInit {
  customerHeaderModel: CustomerHeaderModel
  contractId = '';
  servicesSummary: any;
  upgradeServiceRequestId = '';
  isViewMore = false;
  terms: any = {};
  detailsObj: any = {};
  canEdit = false;

  // start findu
  finduForm: FormGroup;
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  additionalUsers: FormArray;
  todayDate = new Date();
  titles = [];
  findUdetails;
  vehicleMakeList;
  isPasswordShow = false;
  buildList = []
  raceList = []
  languageList = []
  heightList = []
  vehicleColors = [];
  countryList = [];
  vehicleModelList = [];
  vehicleShapeList = [];
  originalMakeList = [];
  originalShapeList = [];
  originalColorList = [];
  originalModelList = [];
  selectedVModelID = "";
  selectedVNameID = "";
  selectedVShapeID = "";
  dateRange = "";
  isEmailExist = true;
  isDuplicate: boolean = false;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  loggedInUserData: LoggedInUserModel;
  // end findu

  constructor(
    private router: Router, private store: Store<AppState>, private crudService: CrudService, private rxjsService: RxjsService,
    private dialogService: DialogService, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute) {
    this.upgradeServiceRequestId = this.activatedRoute.snapshot.queryParams.id;
    if (this.activatedRoute.snapshot.queryParams.canEdit) {
      this.canEdit = this.activatedRoute.snapshot.queryParams.canEdit == "false" ? false : true;
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_UPGRADE_SUMMARY_DETAIL, null, false,
      prepareRequiredHttpParams({ contractId: this.customerHeaderModel.contractId, upgradeServiceRequestId: this.upgradeServiceRequestId || "" }))
      .subscribe((response) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.servicesSummary = response?.resources;
          if (this.servicesSummary?.upgradeServiceAmountSummaryDetailDTO?.isFinduService) {
            this.createFindUForm();
            this.getDropDownList();
            if (this.servicesSummary?.upgradeServiceAmountSummaryDetailDTO.noOfUserCount > 0 &&
              !this.servicesSummary?.upgradeServiceAmountSummaryDetailDTO?.finduUserId) {
              this.additionalUsers = this.getAdditionalUsers;
              for (var i = 0; i < this.servicesSummary?.upgradeServiceAmountSummaryDetailDTO.noOfUserCount; i++) {
                this.additionalUsers.insert(i, this.createUserModel());
              }
            }
            this.finduForm.get('noOfUserCount').setValue(this.servicesSummary?.upgradeServiceAmountSummaryDetailDTO.noOfUserCount);
          }
          this.servicesSummary.upgradeDiscountSummaryDetailDTO.discountPercentage = parseFloat(this.servicesSummary.upgradeDiscountSummaryDetailDTO.discountPercentage);
        }
        if (!this.servicesSummary?.upgradeServiceAmountSummaryDetailDTO?.isFinduService) {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    let date = new Date().getFullYear()
    this.dateRange = `${date - 90}:${date}`;
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(CustomerHeaderInfoDataState$),
      this.store.select(selectStaticEagerLoadingTitlesState$),
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.customerHeaderModel = new CustomerHeaderModel(response[0]);
      this.titles = this.customSelectKeyAndValue(response[1]);
      this.loggedInUserData = new LoggedInUserModel(response[2]);
    });
  }

  openRevisedModel() {
    if (this.servicesSummary?.upgradeServiceAmountSummaryDetailDTO?.isFinduService) {
      document.getElementById('findu-button').click();
    }
    else {
      this.openRevisedSummaryModal();
    }
  }

  openRevisedSummaryModal(finduUserId?: string) {
    this.rxjsService.setDialogOpenProperty(true);
    setTimeout(() => {
      this.dialogService.open(RevisedSummaryModalComponent, {
        header: "Upgrade Summary",
        showHeader: true,
        baseZIndex: 10000,
        width: "650px",
        data: {
          upgradeServiceRequestId: this.upgradeServiceRequestId, ...this.customerHeaderModel,
          finduUserId: finduUserId ? finduUserId : this.servicesSummary?.upgradeServiceAmountSummaryDetailDTO?.finduUserId,
          isFinduService: this.servicesSummary?.upgradeServiceAmountSummaryDetailDTO?.isFinduService,
          userId: this.loggedInUserData.userId
        },
      });
    });
  }

  goBack() {
    this.router.navigate(['/customer/manage-customers/customer-service-upgrade-downgrade/upgrade'], { queryParams: { id: this.upgradeServiceRequestId, canEdit: this.canEdit } })
  }

  clickAggrement(str) {
    if (str == 'service') {
      this.detailsObj = this.terms[0];
    } else {
      this.detailsObj = this.terms[1];
    }
  }

  // start findu
  createFindUForm(): void {
    let finduSubscriberModel = new FinduSubscriberModel();
    this.finduForm = this.formBuilder.group({
      additionalUsers: this.formBuilder.array([]),
    });
    Object.keys(finduSubscriberModel).forEach((key) => {
      this.finduForm.addControl(key,
        new FormControl(finduSubscriberModel[key]));
    });
    this.finduForm = setRequiredValidator(this.finduForm, ["mobileNo", "email", "firstName", "lastName", "dob",
      "title", "password"]);
    this.finduForm.get("mobileNoCountryCode").setValue("+27");
  }

  get getAdditionalUsers(): FormArray {
    if (!this.finduForm) return;
    return this.finduForm.get("additionalUsers") as FormArray;
  }

  //Create FormArray controls
  createUserModel(userModel?: AdditionalUsers): FormGroup {
    let userModelFormControlModel = new AdditionalUsers(userModel);
    let formControls = {};
    Object.keys(userModelFormControlModel).forEach((key) => {
      if (key == 'finduUserId') {
        formControls[key] = [{ value: userModelFormControlModel[key], disabled: false }];
      } else {
        formControls[key] = [{ value: userModelFormControlModel[key], disabled: false }, [Validators.required]];
      }
    });
    return this.formBuilder.group(formControls);
  }

  customSelectKeyAndValue(arr) {
    let selectArr = []
    arr.forEach(item => {
      selectArr.push({ id: item.displayName, displayName: item.displayName })
    })
    return selectArr;
  }

  getDropDownList() {
    forkJoin(
      [this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_VEHICLE_MAKE, null, false, null),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_MODEL, null, false, null),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_SHAPE, null, false, null),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_COLOR, null, false, null),
      this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.UX_FINDU_USERS),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_TERMS, undefined, false, null)]
    ).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.originalMakeList = resp.resources;
              this.vehicleMakeList = getPDropdownData(resp.resources, "displayName", "displayName");
              break;
            case 1:
              this.originalModelList = resp.resources;
              break;
            case 2:
              this.originalShapeList = resp.resources;
              break;
            case 3:
              this.originalColorList = resp.resources;
              break;
            case 4:
              let country = [];
              resp?.resources?.nationality.forEach(item => {
                country.push({
                  id: item.displayName,
                  displayName: item.displayName,
                })
              })
              this.countryList = country;
              this.languageList = this.customSelectKeyAndValue(resp?.resources?.language);
              this.buildList = this.customSelectKeyAndValue(resp?.resources?.build);
              this.raceList = this.customSelectKeyAndValue(resp?.resources?.race);
              this.heightList = resp?.resources?.height;
              break;
            case 5:
              this.terms = resp?.resources;
              break;
          }
          if (ix == response.length - 1) {
            if (this.servicesSummary?.upgradeServiceAmountSummaryDetailDTO?.finduUserId) {
              this.getFindUDetails();
            }
            else {
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          }
        }
      });
    });
  }

  getFindUDetails(): void {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.FIND_U_USERS_SUBSCRIPTION, undefined, false,
      prepareRequiredHttpParams({
        finduUserId: this.servicesSummary?.upgradeServiceAmountSummaryDetailDTO?.finduUserId,
      })).subscribe((response: IApplicationResponse) => {
        this.findUdetails = response.resources;
        if (response.isSuccess && response.resources && response.statusCode === 200) {
          this.findUdetails.mobileNoCountryCode = this.findUdetails.mobileNoCountryCode ? this.findUdetails.mobileNoCountryCode : '+27';
          this.findUdetails.additionalUsers = this.findUdetails.additionalUsers ? this.findUdetails.additionalUsers : [];
          if (response.resources.dob) {
            response.resources.dob = new Date(response.resources.dob);
          }
          this.finduForm.patchValue(response.resources);
          this.additionalUsers = this.getAdditionalUsers;
          if (response.resources.finduUserId) {
            if (response.resources.additionalUsers.length > 0) {
              response.resources.additionalUsers.forEach((dg) => {
                this.additionalUsers.push(this.createUserModel(dg));
              });
            }
          }
          this.finduForm.get('noOfUserCount').setValue(response.resources.noOfUserCount);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  checkDuplication() {
    setTimeout(() => {
      if (!this.finduForm?.get('email').value || this.finduForm?.get('email').invalid) {
        return;
      }
      this.rxjsService.setNotificationProperty(false);
      this.crudService
        .get(
          ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
          SalesModuleApiSuffixModels.FINDU_VALIDATION,
          undefined,
          false,
          prepareRequiredHttpParams({
            email: this.finduForm.value.email,
          }))
        .subscribe((response: IApplicationResponse) => {
          this.isEmailExist = response.isSuccess;
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    });
  }

  OnChange(index): boolean {
    if (this.additionalUsers.length > 1) {
      this.isDuplicate = false;
      var cloneArray = this.additionalUsers.value.slice();
      cloneArray.splice(index, 1);
      cloneArray.forEach((obj) => {
        if (typeof obj.mobileNo === "string") {
          obj.mobileNo = obj.mobileNo.split(" ").join("")
          obj.mobileNo = obj.mobileNo.replace(/"/g, "");
        }
      });
      this.additionalUsers.value.forEach((obj) => {
        if (typeof obj.mobileNo === "string") {
          obj.mobileNo = obj.mobileNo.split(" ").join("")
          obj.mobileNo = obj.mobileNo.replace(/"/g, "");
        }
      });
      var findIndex = cloneArray.some(x => x.mobileNo == this.additionalUsers.value[index].mobileNo);
      if (findIndex) {
        this.snackbarService.openSnackbar("Contact number already exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      } else {
        this.isDuplicate = false;
      }
    }
  }

  onElementChange(eve?, type?) {
    if (eve.originalEvent?.type == "input") return "";
    if (type == "vehicleMakeName") {
      this.finduForm.get('vehicleModel').reset();
      this.finduForm.get('vehicleBodyType').reset();
      this.finduForm.get('vehicleColour').reset();
      this.vehicleModelList = [];
      this.vehicleShapeList = [];
      this.vehicleColors = [];
      if (eve?.value) {
        let selectedVName = this.originalMakeList?.find(x => x.displayName == eve.value);
        this.selectedVNameID = selectedVName != undefined && selectedVName?.id || "-1";
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_MODEL, this.selectedVNameID, false)
          .subscribe((response) => {
            if (response.resources && response.isSuccess && response.statusCode == 200) {
              this.vehicleModelList = getPDropdownData(response.resources, "displayName", "displayName");
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
      }
    }
    else if (type == "vehicleModelName") {
      this.finduForm.get('vehicleBodyType').reset();
      this.finduForm.get('vehicleColour').reset();
      this.vehicleShapeList = [];
      this.vehicleColors = [];
      if (eve?.value) {
        let selectedVModel = this.originalModelList?.find(x => x.displayName == eve?.value);
        this.selectedVModelID = selectedVModel != undefined && selectedVModel?.id || "-1";
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_SHAPE, this.selectedVModelID, false)
          .subscribe((response) => {
            this.vehicleShapeList = getPDropdownData(response.resources, "displayName", "displayName");
          });
      }
    }
    else if (type == "vehicleShapeName") {
      this.finduForm.get('vehicleColour').reset();
      let selectColorList = [];
      if (eve?.value) {
        let selectedVShape = this.originalShapeList?.find(x => x.displayName == eve?.value);
        this.selectedVShapeID = selectedVShape != undefined && selectedVShape?.id || "-1";
        let allIds = this.selectedVNameID + "/" + this.selectedVModelID + "/" + this.selectedVShapeID;
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_COLORS, allIds).subscribe((response) => {
          response?.resources?.forEach(result => {
            selectColorList.push(this.originalColorList?.find(x => x.displayName == result));
          });
          if (this.selectedVNameID == "-1" && this.selectedVModelID == "-1" && this.selectedVShapeID == "-1") {
            this.vehicleColors = getPDropdownData(this.vehicleColors, "displayName", "displayName");
          }
          else {
            this.vehicleColors = getPDropdownData(selectColorList, "displayName", "displayName");
          }
        });
      }
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onFindUSubmit() {
    if (this.finduForm.invalid) {
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (!this.isEmailExist) {
      this.snackbarService.openSnackbar("Email already exist", ResponseMessageTypes.ERROR);
      return;
    }
    if (this.isDuplicate) {
      this.snackbarService.openSnackbar("Contact number already exist", ResponseMessageTypes.WARNING);
      return;
    }
    this.finduForm.value.customerId = this.customerHeaderModel.customerId;
    if (this.finduForm?.value?.dob) {
      this.finduForm.value.dob = new Date(this.finduForm?.value?.dob).toDateString();
    }
    this.finduForm.value.height = this.heightList.find(item => item.id == this.finduForm.value.heightId)?.displayName;
    this.finduForm.value.createdUserId = this.loggedInUserData.userId;
    this.finduForm.value.contractId = this.customerHeaderModel.contractId;
    this.finduForm.value.noOfUserCount = this.servicesSummary?.upgradeServiceAmountSummaryDetailDTO.noOfUserCount;
    const mobile = this.finduForm.get('mobileNo').value.toString().replace(/\s/g, '');
    this.finduForm.value.mobileNo = mobile;
    let additionalUsers = this.finduForm.value.additionalUsers;
    additionalUsers.forEach(element => {
      if (typeof element.mobileNo === "string") {
        element.mobileNo = element.mobileNo.split(" ").join("")
        element.mobileNo = element.mobileNo.replace(/\s/g, '').trim();
      }
    });
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.FIND_U_USERS_SUBSCRIPTION, this.finduForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.openRevisedSummaryModal(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  // end findu
  emitViewMore(e) {
    this.isViewMore = e
  }
}
