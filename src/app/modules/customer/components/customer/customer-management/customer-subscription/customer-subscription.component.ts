import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, QuickActionType, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { CampaignModuleApiSuffixModels } from '@modules/others/configuration/components/campaign-management/shared/enums/campaign.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-customer-subscription',
  templateUrl: './customer-subscription.component.html',
  styleUrls: ['./customer-subscription.component.scss']
})
export class CustomerSubscriptionComponent implements OnInit {
  @Input() contractStatus;
  isLoading;
  @Input() activeServices;
  @Input() addressId;
  @Input() customerId;
  @Input() permission;
  bosStockSubscription;
  isHoverDebtor: boolean;
  bosLoading: boolean;
  bosStockDetails;
  bosBaseServiceDetail;
  bosServiceStatusDetail;
  bosHoverBaseServiceDetail;
  bosHoverServiceStatusDetail;
  partitionServiceInfo: any = [];
  paymentFrequencyList: any = [];
  licenseTypeList: any = [];
  doRunCodeList: any = [];
  contractPeriodList: any = [];
  salesRepList: any = [];
  editSubscriptionFlag: boolean = false;
  editSubscriptionForm: FormGroup;
  startTodayDate = 0;
  detailSubcription;
  customer;
  loggedInUserData: LoggedInUserModel;
  actionTabList = {
    enableHeader: true,
    columns: [
      // { field: 'terminate service', displayName: 'Terminate Service', isSelected: false, isDisabled: false },
      { field: 'suspend service', displayName: 'Suspend Service', isSelected: false, isDisabled: false },
      { field: 'wave anf', displayName: 'Wave ANF', isSelected: false, isDisabled: false },
      { field: 'notify expiry offer', displayName: 'Notify Expiry Offer', isSelected: false, isDisabled: false },
      { field: 'save offer', displayName: 'Save Offer', isSelected: false, isDisabled: false },
      { field: 'cancellation history', displayName: 'Cancellation History', isSelected: false, isDisabled: false },
      { field: 'upgrade downgrade', displayName: 'Upgrade/Downgrade', isSelected: false, isDisabled: false },
      { field: 'Purchase Of Alarm System', displayName: 'Purchase Of Alarm System', isSelected: false, isDisabled: false },
      { field: 'Balance Of Contract', displayName: 'Balance Of Contract', isSelected: false, isDisabled: false },
    ]
  }
  dataList: any;
  totalRecords: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  observableResponse: any;
  isShowNoRecords: any = true;
  feature: string;
  featureIndex: string;
  customerQuickAction :any = {subMenu:[]}

  constructor(private store: Store<AppState>, private crudService: CrudService, private rxjsService: RxjsService, private momentService: MomentService,
    private activatedRoute: ActivatedRoute, private datePipe: DatePipe, private router: Router, private snackbarService: SnackbarService) {
      this.primengTableConfigProperties = {
        selectedTabIndex: 0,
        tableComponentConfigs: {
          tabsList: [
            {
              caption: 'Bill Stock List',
              dataKey: 'occurrenceBookSignalId',
              enableAction: false,
              enableBreadCrumb: true,
              enableExportCSV: false,
              enableExportExcel: false,
              enableExportCSVSelected: false,
              enableReset: false,
              enableGlobalSearch: false,
              reorderableColumns: false,
              resizableColumns: false,
              enableScrollable: true,
              checkBox: false,
              enableRowDelete: false,
              enableStatusActiveAction: false,
              enableFieldsSearch: false,
              rowExpantable: false,
              rowExpantableIndex: 0,
              enableHyperLink: false,
              cursorLinkIndex: 0,
              enableSecondHyperLink: false,
              cursorSecondLinkIndex: 1,
              columns: [{ field: 'stockId', header: 'Stock ID', width: '100px' },
              { field: 'stockDescription', header: 'Stock Description', width: '200px' },
              { field: 'endDate', header: 'End Date', width: '160px' },
              { field: 'qty', header: 'Qty', width: '80px' },
              { field: 'unitPrice', header: 'Unit Price', width: '100px', isAmountAlign: true },
              { field: 'totalExcl', header: 'Total Excl.', width: '100px', isAmountAlign: true },
              { field: 'vat', header: 'VAT', width: '70px', isAmountAlign: true },
              { field: 'totalIncl', header: 'Total Incl.', width: '100px', isAmountAlign: true },],
              apiSuffixModel: CustomerModuleApiSuffixModels.BILL_OF_SERVICE_STOCK_DETAILS,
              moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
              enableMultiDeleteActionBtn: false,
              enableAddActionBtn: false,
              shouldShowFilterActionBtn: false,
            },
          ]
        }
      }
      this.feature = this.activatedRoute.snapshot.queryParams.feature_name;
      this.featureIndex = this.activatedRoute.snapshot.queryParams.featureIndex;
  }

  ngOnInit(): void {
    this.customerQuickAction = this.permission!['subMenu'].find(item => item.menuName == "Quick Action");
    let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, this.permission!['subMenu']);
    this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    this.combineLatestNgrxStoreData()
    this.createForm();
    this.getDropDowns();
  }
  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredListData() {
      if (this.bosStockDetails?.stocks?.length) {
        this.observableResponse = this.bosStockDetails?.stocks;
        this.dataList = this.observableResponse;
        this.totalRecords = 0;
        this.isShowNoRecords = this.dataList?.length ? false : true;
      } else {
        this.observableResponse = [];
        this.dataList = this.observableResponse;
        this.totalRecords = 0;
        this.isShowNoRecords = true;
      }
  }

  createForm(): void {
    this.editSubscriptionForm = new FormGroup({
      'debtorName': new FormControl(null),
      'debtorCode': new FormControl(null),
      'paymentMethodName': new FormControl(null),
      'paymentFrequency': new FormControl(null),
      'siteType': new FormControl(null),
      'division': new FormControl(null),
      'dateActivated': new FormControl(null),
      'firstBilDate': new FormControl(null),
      'increaseMonth': new FormControl(null),
      'doRunCode': new FormControl(null),
      'licenseType': new FormControl(null),
      'channel': new FormControl(null),
      'invoiceTemplate': new FormControl(null),
      'contractId': new FormControl(null),
      'nextBillMonth': new FormControl(null),
      'totalExcl': new FormControl(null),
      'totalIncl': new FormControl(null),
      'lastBillDate': new FormControl(null),
      'lastBillAmount': new FormControl(null),
      'contractPeriod': new FormControl(null),
      'contractStartDate': new FormControl(null),
      'contractEndDate': new FormControl(null),
      'salesRep': new FormControl(null),
      'noOfLicenseToBill': new FormControl(null)
    });
    this.editSubscriptionForm = setRequiredValidator(this.editSubscriptionForm, ['debtorName']);
  }
  getDropDowns(): void {
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_BILLING_INTERVALS, null, 1),
      this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_DEBIT_ORDER_RUN_CODES, null, 1),
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES, CampaignModuleApiSuffixModels.UX_CONTRACT_PERIODS, prepareGetRequestHttpParams(null, null, { CountryId: formConfigs.countryId })),
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_USERS_ALL, null, 1),
      this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_LICENSE_TYPE, null, 1),
    ])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess) {
            switch (ix) {
              case 0:
                this.paymentFrequencyList = respObj.resources;
                break;
              case 1:
                this.doRunCodeList = respObj.resources;
                break;
              case 2:
                this.contractPeriodList = respObj.resources;
                break;
              case 3:
                this.salesRepList = respObj.resources;
                break;
              case 4:
                this.licenseTypeList = respObj.resources;
                break;
            }
          }
        });
      });
  }
  disablequickAction() {
    this.actionTabList.columns.forEach(el => {
      if (el.field == 'suspend service') {
        el.isDisabled = true
      }
    })
  }
  enableQuickAction() {
    this.actionTabList.columns.forEach(el => {
      if (el.field == 'suspend service') {
        el.isDisabled = false
      }
    })
  }
  onQuickActionClick(e) {
    this.onQuickRequested(e?.type, e.index);
  }

  onQuickRequested(type: QuickActionType | string, row?: object): void {
    let isAccessDeined;

    switch (type) {
      case QuickActionType.CANCELLATION_HISTORY:
        isAccessDeined = this.getPermissionByActionType("Cancellation History");
        if (isAccessDeined) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let queryParamsCallHistory = { id: this.activeServices?.contractDbId, serviceId: this.activeServices?.serviceId, customerId: this.customerId, addressId: this.addressId };
        if(this.feature && this.featureIndex) {
          queryParamsCallHistory['feature_name'] = this.feature;
          queryParamsCallHistory['featureIndex'] = this.featureIndex;
        }
        this.router.navigate(['/customer/manage-customers/ticket/cancellation-history'], { queryParams: queryParamsCallHistory });
        break;
      case QuickActionType.SAVE_OFFER:
        isAccessDeined = this.getPermissionByActionType("Save Offer");
        if (isAccessDeined) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let queryParamsSaveOffer = { id: this.activeServices?.contractDbId, serviceId: this.activeServices?.serviceId, customerId: this.customerId, addressId: this.addressId };
        if(this.feature && this.featureIndex) {
          queryParamsSaveOffer['feature_name'] = this.feature;
          queryParamsSaveOffer['featureIndex'] = this.featureIndex;
        }
        this.router.navigate(['/customer/manage-customers/ticket/save-offer'], { queryParams: queryParamsSaveOffer });
        break;
      case QuickActionType.UPGRADE_DOWNGRADE:
        isAccessDeined = this.getPermissionByActionType("Upgrade/Downgrade");
        if (isAccessDeined) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let queryParamsUpgradeDowngrade = { contractId: this.activeServices?.contractDbId, contractName: this.activeServices?.contractId, serviceId: this.activeServices?.serviceId, customerId: this.customerId, addressId: this.addressId };
        if(this.feature && this.featureIndex) {
          queryParamsUpgradeDowngrade['feature_name'] = this.feature;
          queryParamsUpgradeDowngrade['featureIndex'] = this.featureIndex;
        }
        this.router.navigate(['/customer/manage-customers/customer-service-upgrade-downgrade'], { queryParams: queryParamsUpgradeDowngrade });
        break;
      case QuickActionType.SUSPEND_SERVICE:
        isAccessDeined = this.getPermissionByActionType("Suspend Service");
        if (isAccessDeined) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let queryParamsSuspendService = { id: this.activeServices?.contractDbId, serviceId: this.activeServices?.serviceId, customerId: this.customerId, addressId: this.addressId };
        if(this.feature && this.featureIndex) {
          queryParamsSuspendService['feature_name'] = this.feature;
          queryParamsSuspendService['featureIndex'] = this.featureIndex;
        }
        this.router.navigate(['/customer/manage-customers/ticket/suspend-service'], { queryParams: queryParamsSuspendService });
        break;
      case QuickActionType.BALANCE_OF_CONTRACT:
        isAccessDeined = this.getPermissionByActionType("Balance Of Contract");
        if (isAccessDeined) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if ((this.purchaseOfAlarmAndBOCObj['ownershipType'] == 'Rented' && (this.purchaseOfAlarmAndBOCObj.isScheduleAlarmSystemRemovalAppointment ||
          (!this.purchaseOfAlarmAndBOCObj.isScheduleAlarmSystemRemovalAppointment &&
            (
              (this.purchaseOfAlarmAndBOCObj.isPurchaseOfAlarmCreated && this.purchaseOfAlarmAndBOCObj.isPurchaseOfAlarmApproved == true) ||
              (!this.purchaseOfAlarmAndBOCObj.isPurchaseOfAlarmCreated && this.purchaseOfAlarmAndBOCObj.isPurchaseOfAlarmApproved == false)
            )))) ||
          this.purchaseOfAlarmAndBOCObj['ownershipType'] == 'Owned') {
          if (this.purchaseOfAlarmAndBOCObj.isBalanceOfContractCreated && (this.purchaseOfAlarmAndBOCObj.isBalanceOfContractApproved == true ||
            this.purchaseOfAlarmAndBOCObj.isBalanceOfContractApproved == null)) {
            let queryParamsBalanceApproval = { id: this.purchaseOfAlarmAndBOCObj.balanceOfContractId, customerId: this.customerId, addressId: this.addressId };
            if(this.feature && this.featureIndex) {
              queryParamsBalanceApproval['feature_name'] = this.feature;
              queryParamsBalanceApproval['featureIndex'] = this.featureIndex;
            }
            this.router.navigate([`/my-tasks/billing-DOA/balance-of-contract/approval`], { queryParams: queryParamsBalanceApproval });
          }
          else {
            let queryParamsBalanceContract = { contractId: this.activeServices?.contractDbId, serviceId: this.activeServices?.serviceId, customerId: this.customerId, addressId: this.addressId };
            if(this.feature && this.featureIndex) {
              queryParamsBalanceContract['feature_name'] = this.feature;
              queryParamsBalanceContract['featureIndex'] = this.featureIndex;
            }
            this.router.navigate(['/customer/manage-customers/balance-of-contract'], { queryParams: queryParamsBalanceContract });
          }
        }
        else if (!this.purchaseOfAlarmAndBOCObj.isScheduleAlarmSystemRemovalAppointment &&
          this.purchaseOfAlarmAndBOCObj.isPurchaseOfAlarmCreated && this.purchaseOfAlarmAndBOCObj.isPurchaseOfAlarmApproved == null) {
          this.snackbarService.openSnackbar('Request for Purchase Of Alarm system is in progress..!!', ResponseMessageTypes.WARNING);
        }
        else if (this.purchaseOfAlarmAndBOCObj['ownershipType'] == 'Rented' &&
          !this.purchaseOfAlarmAndBOCObj.isScheduleAlarmSystemRemovalAppointment && !this.purchaseOfAlarmAndBOCObj.isPurchaseOfAlarmCreated) {
          this.snackbarService.openSnackbar('Create a Purchase Of Alarm system first..!!', ResponseMessageTypes.WARNING);
        }
        break;
      case QuickActionType.PURCHASE_OF_ALARM_SYSTEM:
        isAccessDeined = this.getPermissionByActionType("Purchase Of Alarm System");
        if (isAccessDeined) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.purchaseOfAlarmAndBOCObj['ownershipType'] == 'Rented' && this.purchaseOfAlarmAndBOCObj.isPurchaseOfAlarmCreated &&
          (this.purchaseOfAlarmAndBOCObj.isPurchaseOfAlarmApproved == true || this.purchaseOfAlarmAndBOCObj.isPurchaseOfAlarmApproved == null)) {
          let queryParamsPurchaseAlarmSystem = { id: this.purchaseOfAlarmAndBOCObj.purchaseOfAlarmId, customerId: this.customerId, addressId: this.addressId };
          if(this.feature && this.featureIndex) {
            queryParamsPurchaseAlarmSystem['feature_name'] = this.feature;
            queryParamsPurchaseAlarmSystem['featureIndex'] = this.featureIndex;
          }
          this.router.navigate([`/my-tasks/billing-DOA/purchase-of-alarm-system/approval`], { queryParams: queryParamsPurchaseAlarmSystem });
        }
        else if (this.purchaseOfAlarmAndBOCObj['ownershipType'] == 'Owned') {
          this.snackbarService.openSnackbar('Purchase Of Alarm is not applicable for ownership owned..!!', ResponseMessageTypes.WARNING);
        }
        else if (this.purchaseOfAlarmAndBOCObj['ownershipType'] == 'Rented') {
          let queryParamsPurchaseRented = { contractId: this.activeServices?.contractDbId, serviceId: this.activeServices?.serviceId, customerId: this.customerId, addressId: this.addressId };
          if(this.feature && this.featureIndex) {
            queryParamsPurchaseRented['feature_name'] = this.feature;
            queryParamsPurchaseRented['featureIndex'] = this.featureIndex;
          }
          this.router.navigate(['/customer/manage-customers/purchase-of-alarm-system'], { queryParams: queryParamsPurchaseRented });
        }
        break;
    }
  }

  getBOSContracts() {
    this.isLoading = true;
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.BILL_OF_SERVICE_CONTRACTS, null, null, prepareRequiredHttpParams({ CustomerId: this.customerId, SiteAddressId: this.addressId }))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.contractStatus = res?.resources;
          // this.getBOSStockDetails(res?.resources[0], 0);
        }
        this.isLoading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubscriptionDetailValue(item?, i?: number, response?, hover = false) {
    if (response && (response.isSuspended || response.isCancelled)) {
      this.disablequickAction();
    } else {
      this.enableQuickAction();
    }
    this.detailSubcription = response;
    this.partitionServiceInfo = response?.servicePartitionsDTOs
    this.activeServices.isShowSubscription = true;
    let bosBaseServiceDetail: any = [
      {
        name: (response?.serviceCategoryName ? '(' + response?.serviceCategoryName + ') ' : '(Service Category Name) ') + (item?.services ? item?.services[i]?.serviceName + ' : ' : 'Contract Name : --') + (item?.contractId ? item?.contractId : ''), columns: [
          { name: 'Debtor Name', className: 'pl-2', value: response?.debtorName ? response?.debtorName : '', order: 1 },
          { name: 'Debtor Code', className: 'pl-2', value: response?.debtorCode ? response?.debtorCode : '', order: 2 },
          { name: 'Payment Method', className: 'pl-2', value: response?.paymentMethodName ? response?.paymentMethodName : '', order: 3 },
          { name: 'Payment Frequency', className: 'pl-2', value: response?.paymentFrequency ? response?.paymentFrequency : '', order: 4 },
        ]
      },];
    let bosServiceStatusDetail = [
      {
        name: 'Service Status : ', value: response?.serviceStatus ? response?.serviceStatus : '--', statusClassName: response?.serviceStatusCssClass ? `status-label ${response?.serviceStatusCssClass}` : '', columns: [
          { name: 'Contract ID', value: response?.contractId ? response?.contractId : '', order: 1 },
          { name: 'Next Bill Month', value: response?.nextBillMonth ? response?.nextBillMonth : '', isDate: true, order: 2 },
          { name: 'Total Excl.', value: response?.totalExcl ? `${response?.currencyCode} ${response?.totalExcl}` : '', order: 3 },
          { name: 'Total Incl.', value: response?.totalIncl ? `${response?.currencyCode} ${response?.totalIncl}` : '', order: 4 },
        ]
      },
    ]
    if (!hover) {
      bosBaseServiceDetail[0]['columns'].push({ name: 'Site Type', className: 'pl-2', value: response?.siteType ? response?.siteType : '', order: 5 },
        { name: 'Division', className: 'pl-2', value: response?.division ? response?.division : '', order: 6 },
        { name: 'Date Activated', className: 'pl-2', value: response?.dateActivated ? response?.dateActivated : '', isDate: true, order: 7 },
        { name: 'First Bill Date', className: 'pl-2', value: response?.firstBilDate ? response?.firstBilDate : '', isDate: true, order: 8 },
        { name: 'DO Run Code', className: 'pl-2', value: response?.doRunCode ? response?.doRunCode : '', order: 9 },
        { name: 'Increase Month', className: 'pl-2', value: response?.increaseMonth ? response?.increaseMonth : '', isDate: true, order: 10 },
        { name: 'License Type', className: 'pl-2', value: response?.licenseType ? response?.licenseType : '-', order: 11 },
        { name: 'Channel', className: 'pl-2', value: response?.channel ? response?.channel : '-', order: 12 },
        { name: 'Invoice Template', value: response?.invoiceTemplate ? response?.invoiceTemplate : '-', order: 13 });
      bosServiceStatusDetail[0]['columns'].push({ name: 'Last Bill Date', value: response?.lastBillDate ? response?.lastBillDate : '', isDate: true, order: 5 },
        { name: 'Last Bill Amount', value: response?.lastBillAmount ? response?.lastBillAmount : '', order: 6 },
        { name: 'Contract Period', value: response?.contractPeriod ? response?.contractPeriod : '', order: 7 },
        { name: 'Contract Start Date', value: response?.contractStartDate ? response?.contractStartDate : '', isDate: true, order: 8 },
        { name: 'Contract End Date', value: response?.contractEndDate ? response?.contractEndDate : '', isDate: true, order: 9 },
        { name: 'Sales Rep', value: response?.salesRep ? response?.salesRep : '', order: 10 },
        { name: 'No of License Bill', value: response?.noOfLicenseToBill ? response?.noOfLicenseToBill : '', order: 11 },
        { name: 'BOC Amount', value: response?.bocAmount ? response?.bocAmount : '', order: 12 })
      this.bosBaseServiceDetail = bosBaseServiceDetail;
      this.bosServiceStatusDetail = bosServiceStatusDetail;
      this.bosStockDetails = response;
      if (response?.total?.toString()) {
        this.bosStockDetails?.stocks.push({
          stockId: '',
          stockDescription: '',
          endDate: '',
          qty: '',
          unitPrice: '',
          totalExcl: '',
          vat: 'Total',
          totalIncl: response?.total?.toString() ? `${response?.currencyCode} ${response?.total}` : 0,
        });
      }
      this.getRequiredListData();
    } else {
      this.bosHoverBaseServiceDetail = bosBaseServiceDetail;
      this.bosHoverServiceStatusDetail = bosServiceStatusDetail;
    }
  }

  getActiveService(item, i) {
    return item?.services[i]?.serviceId == this.activeServices?.serviceId && this.activeServices?.contractDbId == item?.contractDbId;
  }

  onMouseLeaveServices() {
    if (!this.activeServices?.serviceId) {
      this.activeServices.isShowSubscription = false;
    }
    if (this.bosStockSubscription && !this.bosStockSubscription.closed && this.isHoverDebtor) {
      this.bosStockSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.isHoverDebtor = false;
  }

  purchaseOfAlarmAndBOCObj;
  getBOSStockDetails(item, i, hover = false) {
    if (this.bosStockSubscription && !this.bosStockSubscription.closed && !hover) {
      this.bosStockSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
      this.bosLoading = hover;
    }
    let apiName = CustomerModuleApiSuffixModels.BILL_OF_SERVICE_DEBTOR_SERVICE;
    this.isHoverDebtor = hover;
    if (hover) {
      this.onSubscriptionDetailValue(item, i, null, hover);
    } else {
      apiName = CustomerModuleApiSuffixModels.BILL_OF_SERVICE_STOCK_DETAILS;
      this.activeServices.serviceId = item?.services[i]?.serviceId;
      this.activeServices.contractDbId = item?.contractDbId;
      this.activeServices.contractId = item?.contractId;
      this.onSubscriptionDetailValue(item, i);
      this.bosLoading = true;
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.bosStockSubscription = this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      apiName, null, null, prepareRequiredHttpParams({
        CustomerId: this.customerId, SiteAddressId: this.addressId, ServiceId: item?.services[i]?.serviceId, ContractDbId: item?.contractDbId
      }))
      .pipe(map((res: IApplicationResponse) => {
        if (res?.resources?.stocks) {
          res?.resources?.stocks?.forEach(val => {
            val.endDate = this.datePipe.transform(val.endDate, 'dd/MM/yyyy');
            val.unitPrice = `${res?.resources?.currencyCode} ${val.unitPrice}`;
            val.totalExcl = `${res?.resources?.currencyCode} ${val.totalExcl}`;
            val.vat = `${res?.resources?.currencyCode} ${val.vat}`;
            val.totalIncl = `${res?.resources?.currencyCode} ${val.totalIncl}`;
            return val;
          })
        }
        return res;
      }))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.purchaseOfAlarmAndBOCObj = {
            isBalanceOfContractApproved: res.resources.isBalanceOfContractApproved,
            isBalanceOfContractCreated: res.resources.isBalanceOfContractCreated,
            isPurchaseOfAlarmApproved: res.resources.isPurchaseOfAlarmApproved,
            isPurchaseOfAlarmCreated: res.resources.isPurchaseOfAlarmCreated,
            isScheduleAlarmSystemRemovalAppointment: res.resources.isScheduleAlarmSystemRemovalAppointment,
            ownershipType: res.resources.ownershipType,
            purchaseOfAlarmId: res.resources.purchaseOfAlarmId,
            balanceOfContractId: res.resources.balanceOfContractId
          }
        }
        if (res?.isSuccess && res?.statusCode == 200 && hover) {
          this.onSubscriptionDetailValue(item, i, res?.resources, hover);
        } else {
          this.onSubscriptionDetailValue(item, i, res?.resources);
          this.bosLoading = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  edit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }

    this.editSubscriptionFlag = true;
    this.setValue();
  }
  setValue() {
    this.editSubscriptionForm.get('debtorName').setValue(this.detailSubcription.debtorName);
    this.editSubscriptionForm.get('doRunCode').setValue(this.detailSubcription.doRunCode);
    this.editSubscriptionForm.get('salesRep').setValue(this.detailSubcription.salesRepId);
    this.editSubscriptionForm.get('contractPeriod').setValue(this.detailSubcription.contractPeriodId);
    this.editSubscriptionForm.get('debtorCode').setValue(this.detailSubcription.debtorCode);
    this.editSubscriptionForm.get('siteType').setValue(this.detailSubcription.siteType);
    this.editSubscriptionForm.get('division').setValue(this.detailSubcription.division);
    this.editSubscriptionForm.get('channel').setValue(this.detailSubcription.channel);
    this.editSubscriptionForm.get('invoiceTemplate').setValue(this.detailSubcription.invoiceTemplate);
    this.editSubscriptionForm.get('contractId').setValue(this.detailSubcription.contractId);
    this.editSubscriptionForm.get('nextBillMonth').setValue(this.detailSubcription.nextBillMonth);
    this.editSubscriptionForm.get('totalExcl').setValue(this.detailSubcription.totalExcl);
    this.editSubscriptionForm.get('totalIncl').setValue(this.detailSubcription.totalIncl);
    this.editSubscriptionForm.get('lastBillAmount').setValue(this.detailSubcription.lastBillAmount);
    this.editSubscriptionForm.get('noOfLicenseToBill').setValue(this.detailSubcription.noOfLicenseToBill);

    this.editSubscriptionForm.get('dateActivated').setValue(new Date(this.detailSubcription.dateActivated));
    this.editSubscriptionForm.get('contractStartDate').setValue(new Date(this.detailSubcription.contractStartDate));
    this.editSubscriptionForm.get('contractEndDate').setValue(new Date(this.detailSubcription.contractEndDate));
    this.editSubscriptionForm.get('increaseMonth').setValue(new Date(this.detailSubcription.increaseMonth));
    this.editSubscriptionForm.get('firstBilDate').setValue(new Date(this.detailSubcription.firstBilDate));
  }
  submitEditSubscription() {
    if (this.editSubscriptionForm.invalid) {
      this.editSubscriptionForm.markAllAsTouched();
      return;
    }
    let formValue = this.editSubscriptionForm.getRawValue();
    formValue.firstBilDate = (formValue.firstBilDate && formValue.firstBilDate != null) ? this.momentService.toMoment(formValue.firstBilDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    formValue.contractStartDate = (formValue.contractStartDate && formValue.contractStartDate != null) ? this.momentService.toMoment(formValue.contractStartDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    formValue.contractEndDate = (formValue.contractEndDate && formValue.contractEndDate != null) ? this.momentService.toMoment(formValue.contractEndDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    formValue.increaseMonth = (formValue.increaseMonth && formValue.increaseMonth != null) ? this.momentService.toMoment(formValue.increaseMonth).format('YYYY-MM-DDThh:mm:ss[Z]') : null;

    let finalObj = {
      contractId: this.detailSubcription.contractDbId,
      customerId: this.loggedInUserData.userId,
      billingIntervalId: formValue.paymentFrequency,
      firstBillDate: formValue.firstBilDate,
      contractStartDate: formValue.contractStartDate,
      contractEndDate: formValue.contractEndDate,
      contractPeriodId: formValue.contractPeriod,
      increaseMonth: formValue.increaseMonth,
      salesRep: formValue.salesRep,
      noOfLicenseBill: formValue.noOfLicenseBill,
      doRunCodeId: formValue.doRunCodeId,
      invoiceTemplateId: 1,
      CreatedUserId: this.loggedInUserData.userId,
    }

    let api = this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.SUBSCRIPTIONS, finalObj, 1);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.editSubscriptionFlag = false;
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
  cancelEditSubscription() {
    this.editSubscriptionFlag = false;
  }
  getPermissionByActionType(actionTypeMenuName): boolean {
    let foundObj = this.customerQuickAction!['subMenu'].find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }
}
