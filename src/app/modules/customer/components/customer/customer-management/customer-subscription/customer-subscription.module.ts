import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerSharedModule } from "@modules/customer/shared/customer-shared.module";
import { OverlayPanelModule } from "primeng/overlaypanel";
import { BillStockModule } from "../../technician-installation/bill-stock-list/bill-stock-list.module";
import { CustomerSubscriptionComponent } from "./customer-subscription.component";

@NgModule({
  declarations: [CustomerSubscriptionComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule, OverlayPanelModule,
    CustomerSharedModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    BillStockModule,
  ],
  exports: [CustomerSubscriptionComponent, BillStockModule,],
  entryComponents: [],
})
export class CustomerSubscriptionModule { }