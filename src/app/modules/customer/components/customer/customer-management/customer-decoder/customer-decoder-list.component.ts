import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, countryCodes, CrudType, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, Observable } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CancelTestingDialogComponent } from './cancel-testing/cancel-testing.component';
import { CustomerDecoderAddDialogComponent } from './customer-decoder-add-dialog/customer-decoder-add-dialog.component';
import { GprsDecoderDialogComponent } from './gprs-decoder-dialog/gprs-decoder-dialog.component';
@Component({
  selector: 'app-customer-decoder-list',
  templateUrl: './customer-decoder-list.component.html',
  styleUrls: ['./customer-decoder.component.scss'],
})
export class CustomerDecoderListComponent extends PrimeNgTableVariablesModel implements OnInit {

  @Input() partitionId;
  @Input() customerId;
  @Input() customerAddressId;
  @Input() permission;
  loggedInUserData: LoggedInUserModel;
  today: any = new Date();
  row: any = {};
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  cellPanicListData: any = [];
  cellPanicForm: FormGroup;
  countryCodes = countryCodes;
  cellPanicListArray: any = [];
  @Output() redirectToCustomerView = new EventEmitter<any>();
  @ViewChild("input", { static: false }) rows;

  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private momentService: MomentService,
    private dialog: MatDialog,
    private rxjsService: RxjsService,
    private _fb: FormBuilder,
    private datePipe: DatePipe,
    private snackbarService: SnackbarService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: " ",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'decoderConfigId',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'decoderName', header: 'Decoder', width: '200px' },
              { field: 'accountCode', header: 'Transmitter', width: '200px' },
              { field: 'setName', header: 'Set Name', width: '200px' },
              { field: 'testDelay', header: 'Test Delay', width: '200px' },
              { field: 'lastAlarmDate', header: 'Last Alarm Date', width: '200px' },
              { field: 'alarmType', header: 'Alarm', width: '200px' },
              { field: 'zoneNo', header: 'Zone', width: '200px' },
              { field: 'zoneDescription', header: 'Zone Description', width: '200px' },
              { field: 'description', header: 'Description', width: '200px' },
              { field: 'dateLocked', header: 'Date Loaded', width: '200px' },
              { field: 'serialNo', header: 'Serial Number', width: '200px' },
              { field: 'number', header: 'Number', width: '200px' },
              { field: 'radioArea', header: 'Radio Area', width: '200px' },
              { field: 'isAddNewRadio', header: 'Replaced', width: '200px' },
              { field: 'commsTypeName', header: 'Comms Type', width: '200px' },
              { field: 'isActive', header: 'Status', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOMER_ACCOUNT,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            shouldShowCreateActionBtn: true,
          },

        ]

      }
    }

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? params['params']['tab'] ? +params['params']['tab'] : 0 : 0;
      this.customerAddressId = (Object.keys(params['params']).length > 0) ? params['params']['addressId'] ? +params['params']['addressId'] : '' : '';
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
    this.customerId = this.activatedRoute.snapshot.paramMap.get('id');
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.creatCellPanicForm()
    let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, this.permission['subMenu']);
    this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
  }
  onSelectedRows(e) {
    this.selectedRows = e
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      let otherParams = {}
      if (this.customerId && this.customerAddressId) {
        this.getRequiredListData(null, null, otherParams);
        this.getCellPanicList(null, null, otherParams)
      }
    }
  }

  creatCellPanicForm() {
    this.cellPanicForm = this._fb.group({
      customerId: [this.customerId, Validators.required],
      customerAddressId: [this.customerAddressId, Validators.required],
      userName: ['', Validators.required],
      mobileNo: ['', Validators.required],
      mobileNoCountryCode: ['+27', Validators.required],
      createdUserId: [this.loggedInUserData.userId, Validators.required],
    })

    this.onFormControlChange();
  }


  onFormControlChange(){
    this.cellPanicForm
    .get("mobileNoCountryCode")
    .valueChanges.subscribe((mobileNoCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode1(mobileNoCountryCode);
      setTimeout(() => {
        this.rows.nativeElement.focus();
        this.rows.nativeElement.blur();
      });
    });

    this.cellPanicForm
    .get("mobileNo")
    .valueChanges.subscribe((mobileNo: string) => {
      this.setPhoneNumberLengthByCountryCode1(
        this.cellPanicForm.get("mobileNoCountryCode").value
      );
    });
  }

  setPhoneNumberLengthByCountryCode1(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.cellPanicForm
          .get("mobileNo")
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
        break;
      default:
        this.cellPanicForm
          .get("mobileNo")
          .setValidators([
            Validators.minLength(formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),
          ]);
        break;
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }


  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.partitionId == '') {
      const params = { CustomerId: this.customerId, CustomerAddressId: this.customerAddressId }
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { CustomerId: this.customerId, CustomerAddressId: this.customerAddressId, partitionId: this.partitionId }
      otherParams = { ...otherParams, ...params };
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.dataList.map(item => {
          item.dateLocked = this.datePipe.transform(item?.dateLocked, 'dd-MM-yyyy, HH:mm:ss');
          item.lastAlarmDate = this.datePipe.transform(item?.lastAlarmDate, 'dd-MM-yyyy, HH:mm:ss');
        })
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null
        this.totalRecords = 0;

      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }

          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row)
        break
      case CrudType.VIEW:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    // const ref = this.dialogService.open(GprsDecoderDialogComponent, {
      this.rxjsService.setDialogOpenProperty(true)
    const ref = this.dialogService.open(CustomerDecoderAddDialogComponent, {
      showHeader: false,
      baseZIndex: 1000,
      width: '650px',
      data: { editableObject, partitionId: this.partitionId, header: 'Decoders', customerId: this.customerId, addressId: this.customerAddressId }
    });
    ref.onClose.subscribe((result) => {
      let otherParams = {}
      if (result) {
        this.getRequiredListData(null, null, otherParams)
      }
      if (result?.isGprs) {
        const ref1 = this.dialogService.open(GprsDecoderDialogComponent, {
          showHeader: false,
          baseZIndex: 1000,
          width: '650px',
          data: {
            editableObject, ...this.partitionId, header: 'Decoders',
            customerId: this.customerId, addressId: this.customerAddressId, ...result, createdUserId: this.loggedInUserData?.userId
          }
        });
        ref1.onClose.subscribe((result) => {
          if (result) {
            this.getRequiredListData(null, null, otherParams)
          }
        });
      } else {
        //   this.getRequiredListData(null, null, {})

      }
    });

  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  navigateToClientTesting() {
    let isAccessDeined = this.getPermissionByActionType("Client Testing");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.redirectToCustomerView.emit({ navigateTab: 4, monitoringTab: 6 });
  }

  navigateToTechnicianTesting() {
    let isAccessDeined = this.getPermissionByActionType("Technician Testing");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    this.redirectToCustomerView.emit({ navigateTab: 4, monitoringTab: 7 });
  }

  getCellPanicList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    if (this.partitionId == '') {
      const params = { CustomerId: this.customerId, CustomerAddressId: this.customerAddressId }
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { CustomerId: this.customerId, CustomerAddressId: this.customerAddressId, partitionId: this.partitionId }
      otherParams = { ...otherParams, ...params };
    }
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CELL_PANIC,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.cellPanicListData = data.resources
      }
    });
  }

  removeCellPanic(data, i): void {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CELL_PANIC,
        null, prepareGetRequestHttpParams(null, null, { id: data.cellPanicId })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            let otherParams = {}
            this.getCellPanicList(null, null, otherParams)
          }
        });

    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeCellPanicNew(i) {
    this.cellPanicListArray.splice(i, 1);
  }

  focusInAndOutFormArrayFields(): void {
    this.rows.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    })
  }

  addCellPanic() {

    if (this.cellPanicForm.invalid) {
      this.focusInAndOutFormArrayFields()
      return;
    }
    let formValue = this.cellPanicForm.value;
    formValue.mobileNo = formValue.mobileNo.replace(/\s/g, "")
    this.cellPanicListArray.push(formValue)
    this.cellPanicForm.get('mobileNo').setValue('')
    this.cellPanicForm.get('userName').setValue(null)
  }

  onSubmitCellPanic() {
    if (this.cellPanicForm.invalid) {
      return;
    }

    let formValue = this.cellPanicListArray;
    let formValue1 = this.cellPanicForm.value;
    formValue1.mobileNo = formValue1.mobileNo.replace(/\s/g, "")
    formValue = [...this.cellPanicListArray, formValue1]
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CELL_PANIC, formValue)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
         this.cellPanicForm.get('userName').setValue(null)
        let otherParams = {}
        this.getCellPanicList(null, null, otherParams)
      }
    })
  }

  openCancelTesting() {
    let isAccessDeined = this.getPermissionByActionType("Cancel Testing");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const ref = this.dialogService.open(CancelTestingDialogComponent, {
      header: 'Cancel Testing',
      showHeader: true,
      baseZIndex: 1000,
      width: '400px',
    });
    ref.onClose.subscribe((result) => {
      if (result) {
      }
    });
  }
  getPermissionByActionType(actionTypeMenuName): boolean {
    let foundObj = this.permission!['subMenu'].find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }
}
