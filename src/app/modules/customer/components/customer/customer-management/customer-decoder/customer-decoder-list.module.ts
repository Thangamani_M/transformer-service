import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatMenuModule } from "@angular/material";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TieredMenuModule } from "primeng/tieredmenu";
import { CancelTestingDialogModule } from "./cancel-testing/cancel-testing.module";
import { CustomerDecoderAddDialogModule } from "./customer-decoder-add-dialog/customer-decoder-add-dialog.module";
import { CustomerDecoderListComponent } from "./customer-decoder-list.component";
import { GprsDecoderDialogModule } from "./gprs-decoder-dialog/gprs-decoder-dialog.module";

@NgModule({
  declarations: [CustomerDecoderListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    MatMenuModule,
    TieredMenuModule,
    CustomerDecoderAddDialogModule,
    CancelTestingDialogModule,
    GprsDecoderDialogModule,

  ],
  exports: [CustomerDecoderListComponent],
  entryComponents: [],
})
export class CustomerDecoderListModule { }
