
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { partitionListModel } from "@modules/customer/models/customer-decoder.model";
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { forkJoin } from 'rxjs';
@Component({
  selector: 'app-customer-decoder-add-dialog',
  templateUrl: './customer-decoder-add-dialog.component.html'
})
export class CustomerDecoderAddDialogComponent implements OnInit {

  cctvPointForm: FormGroup
  callDetailsData: any
  loggedUser: any
  decodersList: any = []
  setNamesList: any = []
  oldDecodersList: any = []
  oldTransmitterList: any = []
  partitionsDropDown: any = [];
  stateList: any = []
  customerId: any = ''
  customerAddressId: any = ''
  isManualAccountCodeGeneration: boolean = true
  isBOMMandatory: boolean = false
  partitionId: any;
  serviceCallDetails: any;
  validateDecoders: any;
  decoderId: string;
  isServiceCallMandatory: any;
  rtrRequestDetails: any;
  @Output() outputData = new EventEmitter<any>();
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(public ref: DynamicDialogRef, private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, public _fb: FormBuilder, public config: DynamicDialogConfig,
    private crudService: CrudService, private snackbarService: SnackbarService,) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.partitionId = this.config?.data?.partitionId;
    this.customerAddressId = this.config?.data?.addressId;
    this.customerId = this.config?.data?.customerId;
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.createForm();
    this.onLoadValue();
  }

  createForm(): void {
    this.cctvPointForm = this._fb.group({
      customerAccountId: [this.config?.data?.customerAccountId ? this.config?.data?.customerAccountId : null],
      customerId: [this.customerId, [Validators.required]],
      customerAddressId: [this.customerAddressId, [Validators.required]],
      decoderId: ['', [Validators.required]],
      accountCode: ['', [Validators.required, Validators.maxLength(50)]],
      setNameId: ['', [Validators.required]],
      testDelay: ['', [Validators.required, Validators.maxLength(4)]],
      serialNo: ['', [Validators.maxLength(25)]],
      isAddNewRadio: ['true', [Validators.required]],
      radioArea: ['', [Validators.maxLength(100)]],
      description: ['', [Validators.required, Validators.maxLength(250)]],
      oldDecoderId: [''],
      oldCustomerAccountId: [''],
      oldDecoderConfigStatusId: [''],
      isActive: [true],
      serviceCallId: [''],
      createdUserId: [''],
      modifiedUserId: [''],
      partitionId: [this.partitionId ? this.partitionId : ''],
    });
    this.cctvPointForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.cctvPointForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    if (this.config?.data?.isShowPartition) {
      this.cctvPointForm.get('serialNo').setValue(this.config?.data?.serialNo);
      this.cctvPointForm.get('serviceCallId').setValue(this.config?.data?.callInitiationId);
      this.cctvPointForm.get('serialNo').disable();
      this.cctvPointForm.get('serviceCallId').disable();
      this.cctvPointForm = setRequiredValidator(this.cctvPointForm, ["partitionId"]);
    }
    this.cctvPointForm.get('decoderId').valueChanges.subscribe((decoderId: string) => {
      if (decoderId) {
        this.getSetNamesDropDownByDecoder(decoderId)
        this.decoderId = decoderId
      }
      let data = this.decodersList.find(x => x.id == Number(decoderId))
      if (data) {
        this.isManualAccountCodeGeneration = data.isManualAccountCodeGeneration
        this.isBOMMandatory = data.isBOMMandatory
        if (data.isBOMMandatory) {
          this.cctvPointForm.get('serialNo').setValidators([Validators.required, Validators.maxLength(25)]);
        }
        else {
          this.cctvPointForm.get('serialNo').setValidators([Validators.maxLength(25)]);
        }
        this.cctvPointForm.get('serialNo').updateValueAndValidity();
      }
    })

    this.cctvPointForm.get('setNameId').valueChanges.subscribe((setNameId: string) => {
      let data = this.setNamesList.find(x => x.id == Number(setNameId))
      this.getServiceCallMandatroy(data.id)
      if (data) {
        this.cctvPointForm.get('testDelay').setValue(data.defaultTestingDelay)
      }
    })

    this.cctvPointForm.get('isAddNewRadio').valueChanges.subscribe((isAddNewRadio: any) => {
      if (isAddNewRadio == 'false') {
        this.cctvPointForm = setRequiredValidator(this.cctvPointForm, ['oldDecoderId', 'oldCustomerAccountId', 'oldDecoderConfigStatusId']);
      }
      else {
        this.cctvPointForm = clearFormControlValidators(this.cctvPointForm, ['oldDecoderId', 'oldCustomerAccountId', 'oldDecoderConfigStatusId']);
      }
    });
    this.cctvPointForm.get('oldDecoderId').valueChanges.subscribe((oldDecoderId: any) => {
      if (oldDecoderId) {
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_OLD_ACCOUNT_CODE,
          undefined,
          false,
          prepareGetRequestHttpParams(null, null, {
            DecoderId: oldDecoderId,
            CustomerId: this.customerId,
            CustomerAddressId: this.customerAddressId
          }))
          .subscribe(response => {
            this.rxjsService.setGlobalLoaderProperty(false);
            this.oldTransmitterList = response.resources
          })
      }
    })

  }

  getServiceCallMandatroy(val) {
    let reqObject = { DecoderId: this.decoderId, SetNameId: val }
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CUSTOMER_ACCOUNT_BOM, null, false, prepareRequiredHttpParams(reqObject)).subscribe(response => {
        if (response.isSuccess) {
          this.isServiceCallMandatory = response.resources.isServiceCallMandatory;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onLoadValue() {
    let otherParams = {};
    otherParams['customerId'] = this.customerId;
    otherParams['customerAddressId'] = this.customerAddressId;
    this.rxjsService.setGlobalLoaderProperty(true);
    let dropdownsAndData = [
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_PARTITION, null, false,
        prepareGetRequestHttpParams(null, null, otherParams)),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_DECODER_ACCOUNT_CONFIGURATIONS),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_OLD_DECODER_ACCOUNT_CONFIG,
        undefined,
        false,
        prepareGetRequestHttpParams(null, null, otherParams)),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_DECODER_CONFIG_STATUS),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_CALL_INTIATION_DECODER, null, false,
      prepareGetRequestHttpParams(null, null, {
        customerId: this.customerId,
        addressId: this.customerAddressId,
      })),
    ];
    if (this.config?.data?.editableObject?.customerAccountId) {
      dropdownsAndData.push(this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_ACCOUNT,
        this.config?.data?.editableObject?.customerAccountId))
    }
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp?.isSuccess && resp?.statusCode === 200) {
          switch (ix) {
            case 0:
              this.partitionsDropDown = resp?.resources;
              break;
            case 1:
              this.decodersList = resp?.resources;
              break;
            case 2:
              this.oldDecodersList = resp?.resources;
              break;
            case 3:
              this.stateList = resp?.resources;
              break;
            case 4:
              this.serviceCallDetails = resp?.resources;
              break;
            case 5:
              this.onPatchValue(resp);
              break;
          }
        }
        if (ix == response?.length -1) {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    });
  }

  getSetNamesDropDownByDecoder(val) {
    let reqObject = { customerId: this.customerId, customerAddressId: this.customerAddressId, decoderId: val }
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.UX_SETNAME_DECODERS, null, false, prepareRequiredHttpParams(reqObject)).subscribe(response => {
        if (response.isSuccess) {
          this.setNamesList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }



  btnCloseClick() {
    this.rxjsService.setDialogOpenProperty(false);
    this.ref.close();
  }



  getValidateDecoders() {
    if (this.cctvPointForm.invalid) {
      Object.keys(this.cctvPointForm.controls).forEach((key) => {
        this.cctvPointForm.controls[key].markAsDirty();
      });
      return
    }
    if (this.config?.data?.isShowPartition) {
      this.onSubmit();
    } else if (this.cctvPointForm.get("serviceCallId").value && this.cctvPointForm.get("serialNo").value) {
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_VADATE_DECODERS, null, false,
        prepareRequiredHttpParams({
          isDecoder: true,
          callInitiationId: this.cctvPointForm.get("serviceCallId").value,
          userId: this.loggedUser.userId,
          serialNumber: this.cctvPointForm.get("serialNo").value,
          decoderId: this.cctvPointForm.get("decoderId").value,
          setNameId: this.cctvPointForm.get("setNameId").value,
        }))
        .subscribe((response: IApplicationResponse) => {
          if (response.resources) {
            this.validateDecoders = response.resources;
            if (this.validateDecoders.isSuccess == false) {
              this.snackbarService.openSnackbar(this.validateDecoders.validateMessage, ResponseMessageTypes.WARNING);
              this.rxjsService.setGlobalLoaderProperty(false);
              return
            }
            this.onSubmit();
          } else {
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
    else {
      this.onSubmit();
    }
  }

  onPatchValue(response: IApplicationResponse) {
      this.callDetailsData = response.resources;
      this.cctvPointForm.patchValue(response.resources);
      if (response.resources.oldDecoderConfigStatusId == 0) {
        this.cctvPointForm.get('oldDecoderConfigStatusId').setValue(null)
      }
      this.cctvPointForm.get('isAddNewRadio').setValue(response.resources.isAddNewRadio ? 'true' : 'false')
      this.cctvPointForm.get('serviceCallId').setValue(response.resources?.callInitiationId);
      this.cctvPointForm.get('createdUserId').setValue(this.loggedUser.userId)
  }

  createPartitionListModel(partitionModel?: partitionListModel): FormGroup {
    let partitionCategoryListFormControlModel = new partitionListModel(partitionModel);
    let formControls = {};
    Object.keys(partitionCategoryListFormControlModel).forEach((key) => {
      if (key === 'partitionId') {
        formControls[key] = [{ value: partitionCategoryListFormControlModel[key], disabled: false }]
      } else if (key === 'partitionNo') {
        formControls[key] = [{ value: partitionCategoryListFormControlModel[key], disabled: false }]
        // [Validators.compose([Validators.required])]
      }
      else {
        formControls[key] = [{ value: partitionCategoryListFormControlModel[key], disabled: false }]
        // , [Validators.required]
      }
    });
    return this.formBuilder.group(formControls);
  }
  onpartitionsChecked($event, i) {
    if ($event.checked) {

      this.cctvPointForm.get('partitions')['controls'][i].controls['partitionNo'].setValidators([Validators.required]);
      this.cctvPointForm.get('partitions')['controls'][i].controls['partitionNo'].updateValueAndValidity();
    }
    else {

      this.cctvPointForm.get('partitions')['controls'][i].controls['partitionNo'].clearValidators();
      this.cctvPointForm.get('partitions')['controls'][i].controls['partitionNo'].updateValueAndValidity()
    }

  }

  generateCode() {
    let codeData = {
      customerId: this.customerId,
      customerAddressId: this.customerAddressId,
      decoderId: this.cctvPointForm.value.decoderId
    }

    let crudService = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DECODER_CODE_GENERATION, codeData)
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.cctvPointForm.get('accountCode').setValue(response.resources.accountCode)
    });

  }

  onSubmit() {
    if (this.cctvPointForm.invalid) {
      this.cctvPointForm.markAllAsTouched();
      Object.keys(this.cctvPointForm.controls).forEach((key) => {
        this.cctvPointForm.controls[key].markAsDirty();
        if (key == 'serviceCallId' && this.isServiceCallMandatory)
          this.cctvPointForm.markAllAsTouched();
      });
      if (this.isServiceCallMandatory)
        this.cctvPointForm.markAllAsTouched();
      return
    }
    let formValue = this.cctvPointForm.getRawValue();
    let check = this.decodersList.find(item => item.id == formValue.decoderId)
    let isGprs = false;

    if (typeof (formValue.isAddNewRadio) == 'string') {
      formValue.isAddNewRadio = (formValue.isAddNewRadio === 'true') ? true : false
    }
    formValue.callInitiationId = this.cctvPointForm.get("serviceCallId").value;
    formValue.oldDecoderConfigStatusId = formValue.oldDecoderConfigStatusId ? formValue.oldDecoderConfigStatusId : 0;
    this.rxjsService.setGlobalLoaderProperty(true);
    let crudService = formValue.customerAccountId ? this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_ACCOUNT, formValue)
      : this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_ACCOUNT, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        const statusName = this.stateList?.find(el => el?.id == this.cctvPointForm.get("oldDecoderConfigStatusId").value)?.displayName;
        if (typeof response.resources == 'object' && this.cctvPointForm.get("isAddNewRadio").value == 'false' && statusName?.toLowerCase() == 'unusable') {
          this.rtrRequestDetails = { ...response.resources, WarehouseId: this.loggedUser?.warehouseId, createdUserId: this.loggedUser.userId };
          this.rtrRequest(this.rtrRequestDetails)
        }
        let check = this.decodersList.find(item => item.id == formValue.decoderId)
        let isGprs = false;
        if (check?.commsTypeName.toLowerCase() == "gprs") {
          isGprs = true
        }
        this.ref.close({
          isGprs: isGprs,
          callInitiationId: formValue.serviceCallId,
          isHaveData: true,
          isCreate: formValue.customerAccountId ? false : true
        });
        this.rxjsService.setDialogOpenProperty(false);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  rtrRequest(rtrRequestDetails) {
    let crudService = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RTR_REQUEST, rtrRequestDetails)
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);

  }

}
