import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared';
import { CustomerHeaderModel } from '@modules/customer/models';
import { GprsSignalModel } from '@modules/customer/models/gprs-add-model';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { Store } from '@ngrx/store';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { CustomerHeaderInfoDataState$ } from '../../customer-service-upgrade-downgrade/customer-service-ngrx-files';

@Component({
  selector: 'app-gprs-decoder-dialog',
  templateUrl: './gprs-decoder-dialog.component.html',
})
export class GprsDecoderDialogComponent implements OnInit {
  GprsSignalForm: FormGroup
  customerHeaderModel: CustomerHeaderModel
  loggedUser: any
  contractData = []
  serviceTypeDropdown = []
  stockIdDropdown = []
  feeDropdown = []

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig,
    private router: Router,
    private formBuilder: FormBuilder, private store: Store<AppState>,
    private crudService: CrudService) { }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getContractData()
    this.createGprsSignalForm()
    this.getRecurringFeeDropdown()
    this.GprsSignalForm.get("callInitiationId").setValue(this.config.data?.callInitiationId ? this.config.data?.callInitiationId : null)
    this.GprsSignalForm.get("createdUserId").setValue(this.config.data?.createdUserId)
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedUser = response[0];
    });
  }

  createGprsSignalForm(): void {
    let gprsModel = new GprsSignalModel();
    this.GprsSignalForm = this.formBuilder.group({});
    Object.keys(gprsModel).forEach((key) => {
      this.GprsSignalForm.addControl(key, new FormControl(gprsModel[key]));
    });
    this.GprsSignalForm = setRequiredValidator(this.GprsSignalForm, ["contractId", "stockId", "serviceCategoryId", "monthlyChargeInclVAT", "radioRecurringFeeId"]);
  }
  onSubmit() {
    if (this.GprsSignalForm.invalid) {
      return
    }

    this.GprsSignalForm.get("customerId").setValue(this.config.data?.customerId)
    this.GprsSignalForm.get("addressId").setValue(this.config.data?.addressId)
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.RADIO_DECODER, this.GprsSignalForm.value).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.decideRedirectOrClose()
      }
    })
  }

  getContractData() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_RADIO_DECODER_CONTRACTS, null, false, prepareRequiredHttpParams({
      customerId: this.config.data?.customerId,
      addressId: this.config.data?.addressId,
    })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.contractData = response.resources
      }
    })
  }

  onChangeContract() {
    let contractId = this.GprsSignalForm.get("contractId").value;
    if (contractId) {
      let check = this.contractData.find(item => item.id == contractId);
      if (check) {
        this.stockIdDropdown = check?.stockIds || [];
      }

    }
  }
  onChangeStockId() {
    let stockId = this.GprsSignalForm.get("stockId").value;
    if (stockId) {
      let check = this.stockIdDropdown.find(item => item.stockId == stockId);
      if (check) {
        this.serviceTypeDropdown = check?.services || [];
        this.GprsSignalForm.get("debtorId").setValue(this.serviceTypeDropdown[0]?.debtorId)
        this.GprsSignalForm.get("debtorName").setValue(this.serviceTypeDropdown[0]?.debtorName)
        this.GprsSignalForm.get("debtorRefNO").setValue(this.serviceTypeDropdown[0]?.debtorRefNO)
      }

    }
  }
  getRecurringFeeDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_RADIO_RECURRING_FEES).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.feeDropdown = response.resources
      }
    })
  }

  decideRedirectOrClose() {
    let check = this.feeDropdown.find(item => item.radioRecurringFeeId == this.GprsSignalForm.get("radioRecurringFeeId").value);
    if (check?.isAutoApproval) {
      this.btnCloseClick();
    } else {
      this.btnCloseClick();
      this.router.navigate(['/customer/annual-network-fee-approval'], { queryParams: { callInitiationId: this.config.data?.callInitiationId, customerId:this.config.data?.customerId } })
    }
  }

  btnCloseClick() {
    this.ref.close(true)
  }
}
