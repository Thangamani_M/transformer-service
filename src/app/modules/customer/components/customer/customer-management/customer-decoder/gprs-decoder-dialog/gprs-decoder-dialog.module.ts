import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { GprsDecoderDialogComponent } from "./gprs-decoder-dialog.component";

@NgModule({
  declarations: [GprsDecoderDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [GprsDecoderDialogComponent],
  entryComponents: [GprsDecoderDialogComponent],
})
export class GprsDecoderDialogModule { }
