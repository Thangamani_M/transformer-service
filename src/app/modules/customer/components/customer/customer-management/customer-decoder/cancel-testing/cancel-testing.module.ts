import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CancelTestingDialogComponent } from "./cancel-testing.component";

@NgModule({
  declarations: [CancelTestingDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [CancelTestingDialogComponent],
  entryComponents: [CancelTestingDialogComponent],
})
export class CancelTestingDialogModule { }