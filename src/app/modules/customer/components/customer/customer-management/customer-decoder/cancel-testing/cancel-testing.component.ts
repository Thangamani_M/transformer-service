import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
    selector: 'cancel-testing-dialog',
    templateUrl: './cancel-testing.component.html',
})
export class CancelTestingDialogComponent implements OnInit {

    loggedUser: any;
    customerData: any;
    customerAddressId: any;
    constructor(private crudService: CrudService, public ref: DynamicDialogRef,
        public dialogService: DialogService,
        private store: Store<AppState>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private rxjsService: RxjsService, private _fb: FormBuilder, private httpCancelService: HttpCancelService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.rxjsService.getCustomerDate().subscribe((data: boolean) => {
            this.customerData = data;
        });
        this.rxjsService.getCustomerAddresId().subscribe((data: boolean) => {
            this.customerAddressId = data;
        });
    }

    ngOnInit(): void {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(true);
    }

    onSubmit() {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(true);
        let data = {
            modifiedUserId: this.loggedUser.userId,
            customerId: this.customerData.customerId,
            customerAddressId: this.customerAddressId
        }
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CLIENT_TESTING_MANUALLY_STOPPED_ALL, data)

        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
                this.rxjsService.setGlobalLoaderProperty(false);
                this.rxjsService.setDialogOpenProperty(true);
                this.ref.close(true);
            }
        })
    }

    close() {
        this.ref.close(false);
    }

}

