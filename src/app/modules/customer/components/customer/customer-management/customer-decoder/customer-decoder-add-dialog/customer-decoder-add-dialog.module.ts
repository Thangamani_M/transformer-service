import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerDecoderAddDialogComponent } from "./customer-decoder-add-dialog.component";

@NgModule({
  declarations: [CustomerDecoderAddDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [CustomerDecoderAddDialogComponent],
  entryComponents: [CustomerDecoderAddDialogComponent],
})
export class CustomerDecoderAddDialogModule { }