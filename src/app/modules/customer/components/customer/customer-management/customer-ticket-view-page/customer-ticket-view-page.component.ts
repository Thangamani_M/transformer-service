import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, CrudService, ExtensionModalComponent, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-customer-ticket-view-page',
  templateUrl: './customer-ticket-view-page.component.html',
  styleUrls: ['./customer-ticket-view-page.component.scss']
})
export class CustomerTicketViewPageComponent implements OnInit {

  viewTicketDetails: any;
  viewAddresDetails: any;
  customerId: any;
  siteAddressId: any;
  customerDetails: any;
  addressForm: FormGroup;
  isViewMore: boolean;
  @Input() isLoading: boolean;
  @Output() changeAddress = new EventEmitter<any>();
  @Output() viewMoreClick = new EventEmitter<any>();
  agentExtensionNo: string;
  customerSubscription: any;

  constructor(private crudService: CrudService, private router: Router, private route: ActivatedRoute, private rxjsService: RxjsService,
    private store: Store<AppState>, private snackbarService: SnackbarService, private dialog: MatDialog) {
    this.customerId = this.route.snapshot.queryParams['customerId'];
    this.siteAddressId = this.route.snapshot.queryParams['addressId'];
    this.viewValue();
    this.viewCustomerDetails();
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
  }

  ngOnInit(): void {
    this.initForm();
    this.onLoadValue();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes?.isLoading?.currentValue == false && this.customerSubscription?.closed) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  initForm() {
    this.addressForm = new FormGroup({
      addressId: new FormControl({value:'', disabled: true})
    })
    this.addressForm = setRequiredValidator(this.addressForm, ["addressId"]);
    this.addressForm.get('addressId').valueChanges.pipe(distinctUntilChanged(), debounceTime(100))
      .subscribe((res: any) => {
        if (res) {
          this.changeAddress.emit(res);
        }
      })
  }

  onLoadValue() {
    let api = [
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.Customer, this.customerId),
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.CUSTOMER_ADDRESS_INFOLIST, null, false, prepareRequiredHttpParams({ customerId: this.customerId }))
    ]
    this.isLoading = true;
    this.rxjsService.setGlobalLoaderProperty(true);
    this.customerSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((res: IApplicationResponse, ix: number) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          switch(ix) {
            case 0:
              this.customerDetails = res;
              this.viewCustomerDetails(res);
              break;
            case 1:
              this.viewAddresDetails[0].options = res?.resources;
              if (!this.siteAddressId && this.viewAddresDetails[0].options.length) {
                this.siteAddressId = this.viewAddresDetails[0].options[0].addressId;
              }
              this.addressForm.get('addressId').setValue(this.siteAddressId ? this.siteAddressId : null);
              break;
          }
        }
        if (this.isLoading == false) {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    })
  }

  viewValue(response?: any) {
    this.viewAddresDetails = [
      { displayName: 'Site Address', value: response?.resources?.siteAddress, type: 'input_select', field: 'addressId', assignValue: 'addressId', className: 'd-flex mb-2 mr-3', displayValue: 'fullAddress', labelWidth: '12%', valueWidth: '75%' },
    ]
  }

  onShowViewPage() {
    this.isViewMore = !this.isViewMore;
    this.viewCustomerDetails(this.customerDetails);
    this.viewMoreClick.emit(this.isViewMore);
  }

  viewCustomerDetails(response?: any) {
    if (this.isViewMore) {
      this.viewTicketDetails = [
        { name: 'Customer ID', value: response?.resources?.customerRefNo, valueColor: '#166DFF', enableHyperLink: true },
        { name: 'Customer Name', value: response?.resources?.customerName },
        { name: 'Customer Type', value: response?.resources?.customerTypeName },
        { name: 'Email Address', value: response?.resources?.email },
        { name: 'Mobile Number 1', value: response?.resources?.mobileNumber1, isButton: response?.resources?.mobileNumber1 ? true : false, isButtonClass: 'circle-plus-btn', isButtonTooltip: 'Dial', isIconClass: 'icon icon-call' },
        { name: 'Mobile Number 2', value: response?.resources?.mobileNumber2, isButton: response?.resources?.mobileNumber2 ? true : false, isButtonClass: 'circle-plus-btn', isButtonTooltip: 'Dial', isIconClass: 'icon icon-call' },
        { name: 'Office Number', value: response?.resources?.premisesNo },
        { name: 'Premises Number', value: response?.resources?.officeNo },
        { name: 'Status', value: response?.resources?.status, statusClass: response?.resources?.status?.toLowerCase() == 'active' ? 'status-label-green' : response?.resources?.status?.toLowerCase() == 'inactive' ? 'status-label-pink' : '' },
      ];
    } else {
      this.viewTicketDetails = [
        { name: 'Customer ID', value: response?.resources?.customerRefNo, valueColor: '#166DFF', enableHyperLink: true },
        { name: 'Customer Name', value: response?.resources?.customerName },
        { name: 'Status', value: response?.resources?.status, statusClass: response?.resources?.status?.toLowerCase() == 'active' ? 'status-label-green' : response?.resources?.status?.toLowerCase() == 'inactive' ? 'status-label-pink' : '' },
      ];
    }
  }

  callDail(e): void {
    if (!this.agentExtensionNo) {
      this.dialog.closeAll();
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      const dialogReff = this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    } else if (this.agentExtensionNo) {
      let data = {
        customerContactNumber: e,
        customerId: this.customerId,
        clientName: this.customerDetails && this.customerDetails.customerName ? this.customerDetails.customerName : '--'
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
    }
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == "customer id") {
      if (!this.customerId || !this.siteAddressId) {
        return;
      }
      this.rxjsService.setViewCustomerData({
        customerId: this.customerId,
        addressId: this.siteAddressId,
        customerTab: 0,
        monitoringTab: null,
      })
      this.rxjsService.navigateToViewCustomerPage();
    } else if (e?.name?.toLowerCase() == "mobile number 1" || e?.name?.toLowerCase() == "mobile number 2" || 
      e?.name?.toLowerCase() == "office number" || e?.name?.toLowerCase() == "premises number") {
      this.callDail(e?.value);
    }
  }
}

