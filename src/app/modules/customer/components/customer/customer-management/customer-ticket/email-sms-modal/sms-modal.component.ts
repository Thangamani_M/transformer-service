import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { formConfigs, IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator, SharedModuleApiSuffixModels } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { LeadSMSNotificationtModel } from '@modules/sales/models/lead-sms-notification.model';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-sms-modal-component',
  templateUrl: './sms-modal.component.html',
  styleUrls: ['./email-modal.component.scss'],
})
export class SendSMSModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  smsNotificationForm: FormGroup;
  formConfigs = formConfigs;
  userData: any;
  minContact: any = formConfigs.contactnumberMaxLength;
  maxContact: any = formConfigs.contactnumberMinLength;

  constructor(public ref: DynamicDialogRef, public configData: DynamicDialogConfig, private formBuilder: FormBuilder, private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService, private crudService: CrudService, private dialog: MatDialog,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.createSMSNotificationAddForm();
    this.smsNotificationForm.get('notificationToCountryCode').valueChanges.subscribe((notificationToCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(notificationToCountryCode);
    });
    this.smsNotificationForm.get('notificationTo').valueChanges.subscribe((notificationTo: string) => {
      this.setPhoneNumberLengthByCountryCode(this.smsNotificationForm.get('notificationToCountryCode').value);
    });
    this.rxjsService.setDialogOpenProperty(true);
  }

  createSMSNotificationAddForm(): void {
    let smsNotificationModel = new LeadSMSNotificationtModel(this.configData.data);
    this.smsNotificationForm = this.formBuilder.group({});
    Object.keys(smsNotificationModel).forEach((key) => {
      this.smsNotificationForm.addControl(key, new FormControl(smsNotificationModel[key]));
    });
    this.smsNotificationForm = setRequiredValidator(this.smsNotificationForm, ["notificationTo", "notificationText"]);
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.smsNotificationForm.get('notificationTo').setValidators([Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        break;
      default:
        this.smsNotificationForm.get('notificationTo').setValidators([Validators.minLength(formConfigs.indianContactNumberMaxLength),
        Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        break;
    }
  }

  onSubmit(): void {
    if (this.smsNotificationForm.invalid) {
      return;
    }
    let formValue = this.smsNotificationForm.value;
    formValue.notificationTo = this.smsNotificationForm.value.notificationTo.replace(/\D/g, '');
    formValue.createdUserId = this.userData.userId;
    formValue.referenceId = this.configData.data.referenceId;
    formValue.leadId = null;
    formValue.customerId = this.configData.data.customerId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.TICKET_SMS_NOTIFICATIONS, formValue, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.rxjsService.setLeadNotesUpdateStatus(true);
          this.outputData.emit(true);
          this.outputData.emit(true);
          this.ref.close(false);
        }
      });
  }

  onClose() {
    this.ref.close(false);
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
