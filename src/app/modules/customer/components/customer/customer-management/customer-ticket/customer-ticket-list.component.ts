import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { loggedInUserData, selectStaticEagerLoadingTicketStatusState$ } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CustomerTicketAddEditComponent } from './customer-ticket-add-edit.component';
@Component({
  selector: 'app-customer-ticket-list',
  templateUrl: './customer-ticket-list.component.html',
  styleUrls: ['./customer-ticket.scss']
})
export class CustomerTicketListComponent extends PrimeNgTableVariablesModel implements OnInit {
  @Input() typeInNumber;
  @Input() caption;
  @Input() idsObject;
  @Input() hideCreate: Boolean = false;
  @Input() addressId;
  @Input() permission;
  title: string;
  applicationResponse: IApplicationResponse;
  loggedUser: UserLogin;
  id: string;
  customerId: string;
  customerLeadsAddress = "customer leads address";
  ticketStatuses = [];

  constructor(private router: Router, private store: Store<AppState>, public dialogService: DialogService,
    private crudService: CrudService, private rxjsService: RxjsService, private dialog: MatDialog,
    private snackbarService: SnackbarService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.rxjsService.getCustomerDate()
      .subscribe(customerId => {
        this.customerId = (typeof customerId === 'string') ? customerId : customerId?.['customerId'];
      });
  }

  ngOnInit(): void {

    this.primengTableConfigProperties = {
      tableCaption: this.caption == this.customerLeadsAddress ? "Customer Leads Address" : "Customer Ticketing",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Customers', relativeRouterUrl: '' }, { displayName: 'Ticket List' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: this.caption == this.customerLeadsAddress ? "Lead" : "Customer Ticketing",
            dataKey: 'templateId',
            enableBreadCrumb: false,
            enableAction: true,
            enableClearfix: true,
            enableAddActionBtn: !this.hideCreate,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: this.caption == this.customerLeadsAddress ? false : true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: this.caption == this.customerLeadsAddress ?
              [{ field: 'leadRefNo', header: 'Lead ID', width: "100px" },
              { field: 'callDateTime', header: 'Creation Date', width: "160px", isDateTime: true },
              { field: 'suburbName', header: 'Suburb', width: "150px" },
              { field: 'appointmentDate', header: 'Appt Date & Time', width: "110px", isDateTime: true },
              { field: 'leadGroupName', header: 'Lead Group', width: "120px" },
              { field: 'siteTypeName', header: 'Site Type', width: "150px" },
              { field: 'assignTo', header: 'Seller', width: "160px" },
              { field: 'leadStatusName', header: 'Status', isStatus: true, statusKey: 'leadStatusCssClass' }]
              : [{ field: 'ticketRefNo', header: 'Ticket ID' },
              { field: 'ticketTypeName', header: 'Action Type' },
              { field: 'usergroupName', header: 'User Group' },
              { field: 'assignedToName', header: 'Assigned To' },
              { field: 'createdDate', header: 'Created Date', isDateTime: true },
              { field: 'lapsedTime', header: 'Lapsed Time' },
              { field: 'ticketStatusName', header: 'Status', type: 'dropdown', options: [] }],
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: SalesModuleApiSuffixModels.TICKETS,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }]
      }
    }
    this.combineLatestNgrxStoreData();
    this.title = "Tickets";
    if (this.typeInNumber != undefined && this.typeInNumber < 3) {
      if (this.typeInNumber == 1) {
        this.title = "Technical Tickets"
      } else if (this.typeInNumber == 2) {
        this.title = "Sales Tickets"
      }
    }
    if (this.caption == this.customerLeadsAddress) {
      this.getCustomerLeads();
    }
    else {
      this.id = this.loggedUser.userId;
      this.getTicketList();
    }
    this.rxjsService.setGlobalLoaderProperty(false);

  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(selectStaticEagerLoadingTicketStatusState$),
    this.store.select(loggedInUserData),
    ]).pipe(take(1)).subscribe((response) => {
      this.ticketStatuses = response[0];
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[6].options = getPDropdownData(response[0], 'displayName', 'displayName');
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let prop in changes) {
      switch (prop) {
        case "idsObject":
          this.getCustomerLeads();
          break;
      }
    }
  }

  getTicketList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    let value;
    if (this.typeInNumber === 1) {
      value = 'Technical'
    } else if (this.typeInNumber === 2) {
      value = 'Sales'
    }
    let obj1 = { ticketGroup: value };
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    if (this.typeInNumber === 3) {
      delete otherParams['ticketGroup']
    }
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.TICKETS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        customerId: this.customerId,
        addressId: this.addressId,
        ...otherParams
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.dataList = response.resources;
          this.dataList.forEach(ele => {
            ele.isUpdateFlag = false;
          });
          this.totalRecords = response.totalCount;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.reset = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCustomerLeads(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.LEADS_CUSTOMER_ADDRESS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        ...{
          customerId: JSON.parse(this.idsObject).customerId,
          addressId: JSON.parse(this.idsObject).addressId
        }, ...otherParams
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.dataList = response.resources;
          this.totalRecords = response.totalCount;
        }
        this.reset = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  openTicketModal() {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialogService.open(CustomerTicketAddEditComponent, { baseZIndex: 1000, width: '750px', showHeader: false, data: { custId: this.customerId, addressId: this.addressId } });
    dialogReff.onClose.subscribe(result => {
      if (result) {
        this.router.navigate(['/customer/manage-customers/ticket/view'], { queryParams: { id: result?.ticketId, customerId: this.customerId, addressId: this.addressId, fromComponent: "Customer" } });
        return;
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any) {
    if (this.caption == this.customerLeadsAddress) {
      this.rxjsService.setFromUrl("Customer Management")
      this.router.navigate(['/sales/task-management/leads/lead-info/view'], { queryParams: { leadId: row['leadId'], selectedIndex: 1 } });
    }
    else {
      switch (type) {
        case CrudType.CREATE:
          if (!this.permission?.canCreate) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.openAddEditPage(CrudType.CREATE, row);
          break;
        case CrudType.GET:
          switch (this.selectedTabIndex) {
            case 0:
              this.row = row ? row : { pageIndex: 0, pageSize: 10 };
              this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
              if (unknownVar?.ticketStatusName) {
                unknownVar.ticketStatusId = this.ticketStatuses?.find(tS => tS['displayName'] === unknownVar?.ticketStatusName)?.id;
              }
              this.getTicketList(row["pageIndex"], row["pageSize"], unknownVar);
              break;
          }
          break;
        case CrudType.VIEW:
          this.openAddEditPage(CrudType.VIEW, row);
          break;
      }
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string) {
    switch (type) {
      case CrudType.CREATE:
        this.openTicketModal();
        break;
      case CrudType.VIEW:
        this.router.navigate(['/customer/manage-customers/ticket/view'], { queryParams: { id: editableObject['ticketId'], customerId: editableObject['customerId'], addressId: this.addressId, data: this.title, ticketgroupid: this.typeInNumber, fromComponent: "Customer" } });
        break;
    }
  }
}
