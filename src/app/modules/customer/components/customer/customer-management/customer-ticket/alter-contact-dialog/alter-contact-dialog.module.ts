import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AlterContactDialogComponent } from './alter-contact-dialog.component';


@NgModule({
  declarations: [AlterContactDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports:[AlterContactDialogComponent],
  entryComponents:[AlterContactDialogComponent]
})
export class AlterContactDialogModule { }
