import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';

import { SendEmailComponent } from './email-modal.component';
import { SendSMSModalComponent } from './sms-modal.component';


@NgModule({
  declarations: [SendEmailComponent,SendSMSModalComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports:[SendEmailComponent, SendSMSModalComponent],
  entryComponents:[SendEmailComponent, SendSMSModalComponent]
})
export class SendEmailSMSModalModule { }
