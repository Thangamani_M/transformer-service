import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService } from '@app/shared/services';
import { emailPattern, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, setRequiredValidator, SharedModuleApiSuffixModels } from '@app/shared/utils';
import { loggedInUserData } from '@modules/others';
import { LeadEmailNotificationtModel } from '@modules/sales/models/lead-email-notification.model';
import { Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-send-email-component',
  templateUrl: './email-modal.component.html',
  styleUrls: ['./email-modal.component.scss'],
})
export class SendEmailComponent implements OnInit {
  leadId = "";
  emailNotificationForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  fileList: File[] = [];
  submitted = false;
  customerId = "";

  constructor(private formBuilder: FormBuilder,
    public ref: DynamicDialogRef, public configData: DynamicDialogConfig,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private crudService: CrudService,
    private store: Store<AppState>) {
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.combineLatestNgrxStoreData();
    this.createEmailNotificationAddForm(this.configData.data);
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).pipe(take(1)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.leadId = response[1]?.leadId;
    });
  }

  createEmailNotificationAddForm(data: LeadEmailNotificationtModel): void {
    let emailNotificationModel = new LeadEmailNotificationtModel(data);
    this.emailNotificationForm = this.formBuilder.group({});
    Object.keys(emailNotificationModel).forEach((key) => {
      if (key == 'notificationCC') {
        this.emailNotificationForm.addControl(key,
          new FormControl(emailNotificationModel[key], Validators.compose([Validators.email, emailPattern])));
      }
      else {
        this.emailNotificationForm.addControl(key, new FormControl(emailNotificationModel[key]));
      }
    });
    this.emailNotificationForm.get('notificationTo').setValue(this.configData.data.notificationTo);
    this.emailNotificationForm = setRequiredValidator(this.emailNotificationForm, ["notificationTo", "notificationSubject", "notificationText"]);
  }

  emitUploadedFiles(uploadedFiles: any[]) {
    this.fileList = uploadedFiles;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.emailNotificationForm.invalid) {
      this.emailNotificationForm.markAllAsTouched();
      return;
    }
    var formValue = this.emailNotificationForm.value;
    formValue.referenceId = this.configData.data.referenceId;
    formValue.leadId = null;
    formValue.createdUserId = this.loggedInUserData.userId;
    formValue.customerId = this.configData.data.customerId;
    formValue.isSent = true;
    let formData = new FormData();
    formData.append('email', JSON.stringify(formValue));
    this.fileList.forEach(file => {
      formData.append('file', file);
    });
    this.crudService.create(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.TICKET_EMAIL_NOTIFICATIONS, formData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.rxjsService.setDialogOpenProperty(false);
          this.ref.close(true);
        }
      });
  }

  onClose() {
    this.ref.close(false);
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
