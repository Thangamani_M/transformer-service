import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SendEmailSMSModalModule } from "./email-sms-modal/email-sms-modal.component";
@NgModule({
    declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    SendEmailSMSModalModule,
    RouterModule.forChild([
      {
          path: 'view', loadChildren: () => import('./customer-ticket-view.module').then(m => m.CustomerTicketViewModule)
      },
      {
        path: 'process-cancellation', loadChildren: () => import('./../process-cancellation/process-cancellation.module').then(m => m.ProcessCancellationModule)
      },
      {
        path: 'cancellation-history', loadChildren: () => import('./../subscription-cancel/subscription-cancel.module').then(m => m.SubscriptionCancelModule)
      },
      {
        path: 'save-offer', loadChildren: () => import('./../customer-save-offer/customer-save-offer.module').then(m => m.CustomerSaveOfferModule)
      },
      {
        path: 'suspend-service', loadChildren: () => import('./../suspension-service/suspension-service.module').then(m => m.SuspensionServiceModule)
      },
    ])
  ],
  exports: [],
  entryComponents: [],
})
export class CustomerTicketModule { }
