import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerTicketAddEditModule } from "./customer-ticket-add-edit.module";

import { CustomerTicketListComponent } from "./customer-ticket-list.component";

@NgModule({
    declarations: [CustomerTicketListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    CustomerTicketAddEditModule,
  ],
  entryComponents: [],
  exports: [CustomerTicketListComponent]
})
export class CustomerTicketListModule { }