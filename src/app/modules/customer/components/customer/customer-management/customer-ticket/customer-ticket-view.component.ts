import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ticket, TicketManualUpdateModel } from '@app/modules/customer/models';
import { AppState } from '@app/reducers';
import { ACCEPTABLE_ALL_MAJOR_FILE_EXTENSIONS, CrudType, currentComponentPageBasedPermissionsSelector$, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, prepareRequiredHttpParams, QuickActionType, ResponseMessageTypes, setRequiredValidator, WRONG_FILE_EXTENSION_SELECTION_ERROR } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { AlterContactDialogComponent, CustomerTicketAddEditComponent } from '@modules/customer/components';
import { CustomerManagementService } from '@modules/customer/services/customermanagement.services';
import { CUSTOMER_COMPONENT } from '@modules/customer/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { SendEmailComponent } from './email-sms-modal/email-modal.component';
import { SendSMSModalComponent } from './email-sms-modal/sms-modal.component';

@Component({
  selector: 'app-customer-ticket-view',
  templateUrl: './customer-ticket-view.component.html',
  styleUrls: ['./customer-ticket.scss']
})
export class CustomerTicketViewComponent implements OnInit {
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  @Input() typeInNumber;
  formData = new FormData();
  ticketViewForm: FormGroup;
  ticketStatusList = [];
  ticketGroupList = [];
  assigntoList = [];
  ticketId: string;
  ticketdetails;
  pastcomments = [];
  ticketDto: ticket | any;
  groupId: string;
  customerId: string;
  fileName: string;
  ticketGroupId: string;
  listOfFiles: any[] = [];
  documentdetails: Array<any> = [];
  addressId;
  documentDetails;
  isViewMore: boolean;
  alterContactSubscription;
  fileList: File[] = [];
  maxFilesUpload: Number = 5;
  customerDetails;
  pagePermissions = []
  actionTabList = {
    enableHeader: true,
    columns: [
      { id: 0, field: 'edit', displayName: 'Update Ticket', isSelected: false },
      { id: 1, field: 'view alter contact', displayName: 'View Alternate Contact', isSelected: false },
      { id: 2, field: 'email', displayName: 'Email', isSelected: false },
      { id: 3, field: 'sms', displayName: 'SMS', isSelected: false },
    ]
  };
  loggedInUserData: LoggedInUserModel;
  isSuspendTicket = false;
  serviceSuspendId = '';
  isLoading: boolean;

  constructor(private activatedRoute: ActivatedRoute, private httpCancelService: HttpCancelService, private rxjsService: RxjsService,
    private store: Store<AppState>, private formBuilder: FormBuilder, private snackbarService: SnackbarService, private dialogService: DialogService,
    private httpService: CrudService, private router: Router, private dialog: MatDialog, private route: ActivatedRoute, private routerlink: Router,
    private customerManagementService: CustomerManagementService) {

  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.getForkJoinRequests();
    this.ticketDto = new ticket();
    this.createTicketManualAddForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams,
      this.store.select(currentComponentPageBasedPermissionsSelector$)
    ]).subscribe((response: any) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let fromComponent = response[1]?.fromComponent;
      let permission;
      let customerTickets;
      let customersPermission = response[2]?.Customers
      if (customersPermission) {
        let customerManagement = customersPermission?.find(item => item.menuName == "Customer Dashboard")
        if (customerManagement) {
          customerTickets = customerManagement?.subMenu?.find(item => item.menuName == "TICKETING")['subMenu'];
        }
      }
      if (fromComponent == "Customer") {
        permission = customerTickets;
      } else {
        permission = response[2][fromComponent]
      }
      this.pagePermissions = permission?.find(item => item.menuName == "Quick Action")
      if (response[1]) {
        this.ticketId = response[1]?.id;
        this.ticketGroupId = response[1]?.ticketgroupid;
        this.customerId = response[1]?.customerId;
        this.isSuspendTicket = response[1]?.isSuspendTicket ? response[1].isSuspendTicket : false;
        this.addressId = response[1]?.addressId;
        this.serviceSuspendId = response[1]?.serviceSuspendId;
      }
    });
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.httpService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TICKETSTATUS, undefined, true, null),
      this.httpService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_GROUPS, undefined, true),
      this.customerManagementService.getCustomer(this.customerId),
      this.getTicketIDApi(),
    ])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
            switch (ix) {
              case 0:
                this.ticketStatusList = respObj.resources;
                break;
              case 1:
                this.ticketGroupList = respObj.resources;
                break;
              case 2:
                this.customerDetails = respObj.resources;
                let _mobile = this.customerDetails?.mobileNumber1.split(" ");
                this.customerDetails.mobileCode = _mobile[0];
                this.customerDetails.mobileNumber = _mobile[1];
                break;
              case 3:
                this.onPatchValue(respObj);
                break;
            }
          }
          // if (ix == response.length - 1) {
          //   this.rxjsService.setGlobalLoaderProperty(false);
          // }
        });
      });
  }

  createTicketManualAddForm(TicketModelData?: TicketManualUpdateModel): void {
    let ticketModel = new TicketManualUpdateModel(TicketModelData);
    this.ticketViewForm = this.formBuilder.group({});
    Object.keys(ticketModel).forEach((key) => {
      this.ticketViewForm.addControl(key, new FormControl(TicketManualUpdateModel[key]));
    });
    this.ticketViewForm = setRequiredValidator(this.ticketViewForm, ["ticketStatusId", "userGroupId", "assignedTo", "description"]);
  }

  OnUserGroupChange(event) {
    this.groupId = event.target.value;
    this.getassignedto(this.groupId);
  }

  getassignedto(id: string) {
    if (id) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.httpService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_ASSINGNTO, prepareGetRequestHttpParams(null, null, { groupId: id })).subscribe((response) => {
        if (response.statusCode == 200) {
          this.assigntoList = response.resources;
        }
        this.isLoading = false;
      });
    }
  }

  onQuickActionClick(e) {
    this.onQuickRequested(e?.type, e.index);
  }

  onCRUDRequested(type: CrudType | string, row?: object): void {
    switch (type) {
      case CrudType.EDIT:
        this.Edit();
        break;
    }
  }

  onQuickRequested(type: QuickActionType | string, row?: object): void {
    switch (type) {
      case QuickActionType.EDIT:
        if (this.getPermissionByActionType("Update Ticket")) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.Edit();
        break;
      case QuickActionType.VIEW_ALTER_CONTACT:
        if (this.getPermissionByActionType("View Alternate Contact")) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.alterContact();
        break;
      case QuickActionType.PROCESS_CANEL:
        if (this.getPermissionByActionType("Process Cancellation")) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        if (this.ticketDto?.serviceCancellationId) {
          this.router.navigate(['/customer/manage-customers/ticket/process-cancellation'], { queryParams: { id: this.ticketDto?.contractId, customerId: this.customerId, addressId: this.addressId, ticketId: this.ticketId, serviceCancellationId: this.ticketDto?.serviceCancellationId }});
        } else if (!this.ticketDto?.serviceCancellationId && this.ticketDto?.isCancellationLetterReceived && this.ticketDto?.ticketTypeName?.toLowerCase() == 'request for cancel') {
          this.router.navigate(['/customer/manage-customers/ticket/process-cancellation'], { queryParams: { customerId: this.customerId, addressId: this.addressId, ticketId: this.ticketId } });
        } else {
          this.snackbarService.openSnackbar('Process Cancellation shows only for Action type must be Request to Cancel and Cancellation Notice to be received', ResponseMessageTypes.WARNING);
        }
        break;
      case QuickActionType.EMAIL:
        if (this.getPermissionByActionType("Email")) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openModel(SendEmailComponent, 'Email');
        break;
      case QuickActionType.SMS:
        if (this.getPermissionByActionType("SMS")) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openModel(SendSMSModalComponent, 'Sms');
        break;
    }
  }

  openModel(component, type) {
    const titile = type?.toLowerCase() == 'sms' ? type?.toUpperCase() : type;
    const ref = this.dialogService.open(component, {
      showHeader: true,
      header: `Send ${titile}`,
      baseZIndex: 10000,
      width: '700px',
      data: {
        notificationTo: type == 'Email' ? this.customerDetails.email : Number(this.customerDetails.mobileNumber),
        notificationToCountryCode: this.customerDetails.mobileCode,
        referenceId: this.ticketId,
        customerId: this.customerId,
      },
    });
    ref.onClose.subscribe((result) => {
      if (result == true) {
        this.getTicketById();
      }
    });
  }

  Edit() {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialogService.open(CustomerTicketAddEditComponent, { baseZIndex: 1000,
      width: '750px', showHeader: false, data: {
        custId: this.customerId, ticketId: this.ticketId, isSuspendTicket: this.isSuspendTicket,
        addressId: this.addressId, serviceSuspendId: this.serviceSuspendId
      }
    });
    dialogReff.onClose.subscribe(result => {
      if (result == null) {
        this.getTicketById();
      }
    });
  }

  alterContact() {
    if (this.alterContactSubscription && !this.alterContactSubscription.closed) {
      this.alterContactSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.alterContactSubscription = this.httpService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKET_ALTERNATE_CONTACTS_DETAIL, null, false, prepareRequiredHttpParams({ customerId: this.customerId, siteAddressId: this.addressId }), 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200) {
          const ref = this.dialogService.open(AlterContactDialogComponent, {
            header: 'View Alternate Contact - ' + this.ticketDto?.ticketRefNo,
            baseZIndex: 500,
            width: '800px',
            closable: false,
            showHeader: false,
            data: {
              result: response?.resources,
              customerId: this.customerId,
              userId: this.loggedInUserData.userId,
              addressId: this.addressId,
              ticketId: this.ticketId,
            },
          });
          ref.onClose.subscribe((res: IApplicationResponse) => {
            if (res?.isSuccess && res?.statusCode == 200) {

            }
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTicketById() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.getTicketIDApi().subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.onPatchValue(response);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getTicketIDApi() {
    return this.httpService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKETS, this.ticketId, false, null, 1);
  }

  onPatchValue(response) {
    this.ticketDto = response.resources;
    this.ticketdetails = response.resources;
    this.ticketViewForm.patchValue(response.resources);
    this.pastcomments = this.ticketdetails.pastcoments;
    this.addressId = response.resources?.siteAddressId;
    this.getassignedto(this.ticketdetails.userGroupId);
    this.showHideProcessCancellation();
    this.actionTabList['columns'].map(el => el.isSelected = false);
    if (!this.ticketdetails.userGroupId) {
      this.isLoading = false;
    }
  }

  uploadFiles(file) {
    if (file && file.length == 0)
      return;
    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }
    for (let i = 0; i < file.length; i++) {
      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (ACCEPTABLE_ALL_MAJOR_FILE_EXTENSIONS.includes(extension)) {
        let filename = this.fileList?.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push(selectedFile.name);
          this.ticketViewForm.markAllAsTouched();
          this.myFileInputField.nativeElement.value = null;
        }
      } else {
        this.snackbarService.openSnackbar(WRONG_FILE_EXTENSION_SELECTION_ERROR, ResponseMessageTypes.WARNING);
      }
    }
  }

  showHideProcessCancellation() {
    if (this.ticketDto?.ticketTypeName?.toLowerCase() == 'request for cancel') {
      this.actionTabList['columns'] = [
        { id: 0, field: 'edit', displayName: 'Update Ticket', isSelected: false },
        { id: 1, field: 'process cancel', displayName: this.ticketDto?.serviceCancellationId ? 'View Process Cancellation' : 'Process Cancellation', isSelected: false },
        { id: 2, field: 'view alter contact', displayName: 'View Alternate Contact', isSelected: false },
        { id: 3, field: 'email', displayName: 'Email', isSelected: false },
        { id: 4, field: 'sms', displayName: 'SMS', isSelected: false },
      ]
    } else {
      this.actionTabList['columns'] = [
        { id: 0, field: 'edit', displayName: 'Update Ticket', isSelected: false },
        { id: 1, field: 'view alter contact', displayName: 'View Alternate Contact', isSelected: false },
        { id: 2, field: 'email', displayName: 'Email', isSelected: false },
        { id: 3, field: 'sms', displayName: 'SMS', isSelected: false },
      ]
    }
  }

  removeSelectedFile(index) {
    this.listOfFiles.splice(index, 1);
    this.fileList.splice(index, 1);
  }

  onSubmit(): void {
    this.ticketViewForm.value.ticketId = this.ticketId;
    this.ticketViewForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.ticketViewForm.value.assignedBy = this.loggedInUserData.userId;
    this.formData.append('ticket', JSON.stringify(this.ticketViewForm.value));
    this.fileList.forEach(file => {
      this.formData.append('file', file);
    });
    if (this.ticketViewForm.invalid) {
      this.ticketViewForm.markAllAsTouched();
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.httpService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKETS, this.formData, 1).subscribe((response) => {
      if (response.statusCode == 200) {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.redirect();
      }
    });
  }

  showOutcome() {
    return this.ticketDto?.ticketStatusName?.toLowerCase() == 'completed';
  }

  onCancel(): void {
    let selectedTabIndex = 0;
    if (this.ticketGroupId === "1") {
      selectedTabIndex = 2;
    }
    else if (this.ticketGroupId === "2") {
      selectedTabIndex = 1;
    }
    else {
      selectedTabIndex = 7;
    }
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId,
      customerTab: selectedTabIndex,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  onShowViewPage(e) {
    this.isViewMore = e;
  }

  getHeight() {
    return this.isViewMore ? '35vh' : '48vh';
  }

  showOutCome() {
    return this.ticketDto?.status?.toLowerCase() == 'completed';
  }

  showReqCancel() {
    return this.ticketDto?.ticketTypeName?.toLowerCase() == 'request for cancel';
  }

  redirect() {
    let selectedTabIndex = 0;
    if (this.ticketGroupId === "1") {
      selectedTabIndex = 6;
    }
    else if (this.ticketGroupId === "2") {
      selectedTabIndex = 5;
    }
    else {
      selectedTabIndex = 10;
    }
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId,
      customerTab: selectedTabIndex,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }
  getPermissionByActionType(actionTypeMenuName): boolean {
    let foundObj = this.pagePermissions['subMenu']?.find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

}
