import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, countryCodes, CrudService, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { TicketAlterContactDetailModel, TicketContactArrayDetailsModel, TicketEmailArrayDetailsModel } from '@modules/customer';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-alter-contact-dialog',
  templateUrl: './alter-contact-dialog.component.html',
  styleUrls: ['./alter-contact-dialog.component.scss']
})
export class AlterContactDialogComponent implements OnInit {

  alterContactDialogForm: FormGroup;
  isSubmitted: boolean;
  isLoading: boolean;
  isFormChangeDetected: boolean;
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  primengTableConfigProperties: any;
  dropdownSubscription: any;

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private datePipe: DatePipe,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService, private dialog: MatDialog,) {
    this.rxjsService.setDialogOpenProperty(true);
    this.primengTableConfigProperties = {
      tableCaption: "View Alternate Contact",
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'View Alternate Contact',
            dataKey: 'customerAlternateContactId',
            formArrayName: 'emails',
            columns: [
              // { field: 'email', displayName: 'Email', type: 'input_text', className: 'col-10', required: true },
              // { field: 'email', displayName: 'Email', type: 'label', className: 'col-10', iconClassName: 'icon icon-email border border-primary rounded-circle p-2' },
              { field: 'email', displayName: 'Email', type: 'label', className: 'col-10', iconClassName: 'icon icon-email pr-1' },
            ],
            isEditFormArray: false,
            isRemoveFormArray: true,
            detailsAPI: SalesModuleApiSuffixModels.TICKET_ALTERNATE_CONTACTS_DETAIL,
            postAPI: SalesModuleApiSuffixModels.TICKET_ALTERNATE_CONTACTS,
            deleteAPI: SalesModuleApiSuffixModels.TICKET_ALTERNATE_CONTACTS,
            moduleName: ModulesBasedApiSuffix.SALES,
          },
          {
            caption: 'View Alternate Contact',
            dataKey: 'customerAlternateContactId',
            formArrayName: 'contacts',
            columns: [
              // { field: 'contactNumberCountryCode', displayName: 'Contact', type: 'input_select', className: 'col-3 pr-0 mt-1', required: true, displayValue: 'displayName', assignValue: 'displayName', options: countryCodes },
              // { field: 'contactNumber', displayName: '', type: 'input_phone', className: 'col-9 pl-0', required: true },
              { field: 'contactNumberCountryCode', displayName: 'Contact', type: 'label', className: 'col-2 d-flex pl-3 mt-1', iconClassName: 'icon icon-calls pr-1' },
              { field: 'contactNumber', displayName: '', type: 'label', className: 'col-9 pt-1 px-0', },
            ],
            isEditFormArray: false,
            isRemoveFormArray: true,
            detailsAPI: SalesModuleApiSuffixModels.TICKET_ALTERNATE_CONTACTS_DETAIL,
            postAPI: SalesModuleApiSuffixModels.TICKET_ALTERNATE_CONTACTS,
            deleteAPI: SalesModuleApiSuffixModels.TICKET_ALTERNATE_CONTACTS,
            moduleName: ModulesBasedApiSuffix.SALES,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.initForm();
    this.config?.data?.result?.emails?.forEach(el => {
      this.initAlterEmailFormArray(el);
    });
    this.config?.data?.result?.contacts?.forEach(el => {
      this.initAlterContactFormArray(el);
    });
  }

  ngAfterViewInit() {
  }

  initForm(ticketAlterContactDetailModel?: TicketAlterContactDetailModel) {
    let alterContactModel = new TicketAlterContactDetailModel(ticketAlterContactDetailModel);
    this.alterContactDialogForm = this.formBuilder.group({});
    Object.keys(alterContactModel).forEach((key) => {
      if (typeof alterContactModel[key] === 'object') {
        this.alterContactDialogForm.addControl(key, new FormArray(alterContactModel[key]));
      } else {
        this.alterContactDialogForm.addControl(key, new FormControl(alterContactModel[key]));
      }
    });
    this.alterContactDialogForm = setRequiredValidator(this.alterContactDialogForm, ["email", "contactNumber"]);
    this.alterContactDialogForm.get('contactNumberCountryCode').disable();
    this.onValueChanges();
  }

  loadValue() {
    let api: any;
    this.isLoading = true;
    api = this.crudService.get(ModulesBasedApiSuffix.SALES, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0].detailsAPI, null, false,
      prepareRequiredHttpParams({ customerId: this.config?.data?.customerId, siteAddressId: this.config?.data?.addressId }));
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.dropdownSubscription = api.subscribe((resp: IApplicationResponse) => {
      if (resp.isSuccess && resp.statusCode === 200) {
        this.patchValue(resp?.resources);
        this.alterContactDialogForm = setRequiredValidator(this.alterContactDialogForm, ["email", "contactNumber"]);
      }
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  patchValue(resp) {
    resp?.emails?.forEach(el => {
      this.initAlterEmailFormArray(el);
    });
    resp?.contacts?.forEach(el => {
      this.initAlterContactFormArray(el);
    });
  }

  btnCloseClick() {
    this.alterContactDialogForm.reset();
    this.ref.close(false);
  }

  onSubmitDialog() {
    if (this.isSubmitted || !this.alterContactDialogForm?.valid) {
      this.alterContactDialogForm?.markAllAsTouched();
      return;
    } else if (!this.alterContactDialogForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    const alterContactObject = {
      ...this.alterContactDialogForm.value,
    }
    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    this.isSubmitted = true;
    let api = this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKET_OUTCOME_CONFIGURATION, alterContactObject)
    if (this.alterContactDialogForm.value?.alterContactId) {
      api = this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKET_OUTCOME_CONFIGURATION, alterContactObject);
    }
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.alterContactDialogForm.reset();
        this.ref.close(res);
      }
      this.rxjsService.setDialogOpenProperty(false);
      this.isSubmitted = false;
    })
  }

  addConfigItem() {
    if (this.alterContactDialogForm.get('email').invalid && !this.alterContactDialogForm.get('contactNumber').value) {
      this.alterContactDialogForm.markAllAsTouched();
      return;
    } else if (this.alterContactDialogForm.get('contactNumber').invalid && !this.alterContactDialogForm.get('email').value) {
      this.alterContactDialogForm.markAllAsTouched();
      return;
    } else if (this.validateEmailsExist()) {
      this.snackbarService.openSnackbar(`Alternate email item already exists`, ResponseMessageTypes.WARNING);
      return;
    } else if (this.validateContactsExist()) {
      this.snackbarService.openSnackbar(`Alternate contact item already exists`, ResponseMessageTypes.WARNING);
      return;
    }
    this.createConfigItem();
  }

  onValueChanges() {
    this.alterContactDialogForm
      .get("contactNumberCountryCode")
      .valueChanges.subscribe((contactNumberCountryCode: string) => {
        this.setPhoneNumberLengthByCountryCode(contactNumberCountryCode, "contact");
      });
    this.alterContactDialogForm
      .get("contactNumber")
      .valueChanges.subscribe((contactNumber: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.alterContactDialogForm.get("contactNumberCountryCode").value,
          "contact"
        );
      });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string, str: string) {
    switch (countryCode) {
      case "+27":
        if (str == "contact") {
          this.alterContactDialogForm
            .get("contactNumber")
            .setValidators([
              Validators.minLength(
                formConfigs.southAfricanContactNumberMaxLength
              ),
              Validators.maxLength(
                formConfigs.southAfricanContactNumberMaxLength
              ),
            ]);
        }

        break;
      default:
        if (str == "contact") {
          this.alterContactDialogForm
            .get("contactNumber")
            .setValidators([
              Validators.minLength(formConfigs.indianContactNumberMaxLength),
              Validators.maxLength(formConfigs.indianContactNumberMaxLength),
            ]);
        }
        break;
    }
  }

  validateEmailsExist() {
    if(this.alterContactDialogForm.get('email').value) {
      const findItem = this.getEmailFormArray.controls.find(el => el.get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0].columns[0].field).value?.toLowerCase() == this.alterContactDialogForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0].columns[0].field].toLowerCase());
      if (findItem) {
        return true;
      }
    }
    return false;
  }

  validateContactsExist() {
    if(this.alterContactDialogForm.get('contactNumber').value) {
      const findItem = this.getContactsFormArray.controls.find(el => el.get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[1].columns[1].field).value == +this.alterContactDialogForm.value[this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[1].columns[1].field].split(" ").join(""));
      if (findItem) {
        return true;
      }
    }
    return false;
  }

  createConfigItem() {
    this.isFormChangeDetected = true;
    const reqObj = {
      customerId: this.config?.data?.customerId,
      siteAddressId: this.config?.data?.addressId,
      createdUserId: this.config?.data?.userId,
      customerAlternateContactId: null,
      email: this.alterContactDialogForm.get('email').value ? this.alterContactDialogForm.get('email').value : '',
      contactNumber: this.alterContactDialogForm.get('contactNumber').value ? this.alterContactDialogForm.get('contactNumber').value.split(" ").join("") : '',
      contactNumberCountryCode: this.alterContactDialogForm.get('contactNumberCountryCode').value,
    }
    this.crudService.create(ModulesBasedApiSuffix.SALES, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0].postAPI, reqObj)
      .subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.alterContactDialogForm.get('email').reset();
          this.alterContactDialogForm.get('contactNumberCountryCode').setValue('+27');
          this.alterContactDialogForm.get('contactNumber').reset();
          this.clearFormArray(this.getEmailFormArray);
          this.clearFormArray(this.getContactsFormArray);
          //this.initForm();
          this.loadValue();
        }
      })
  }

  addFormArray(emailObj, contactobj) {
    this.initAlterContactFormArray(emailObj);
    this.initAlterEmailFormArray(contactobj);
  }

  initAlterContactFormArray(ticketContactArrayDetailsModel?: TicketContactArrayDetailsModel) {
    let ticketContactDetailsModel = new TicketContactArrayDetailsModel(ticketContactArrayDetailsModel);
    let ticketContactDetailsFormArray = this.formBuilder.group({});
    Object.keys(ticketContactDetailsModel).forEach((key) => {
      ticketContactDetailsFormArray.addControl(key, new FormControl({ value: ticketContactDetailsModel[key], disabled: true }));
    });
    ticketContactDetailsFormArray = setRequiredValidator(ticketContactDetailsFormArray, ["contactNumberCountryCode", "contactNumber"]);
    this.getContactsFormArray.push(ticketContactDetailsFormArray);
  }

  initAlterEmailFormArray(ticketEmailArrayDetailsModel?: TicketEmailArrayDetailsModel) {
    let ticketEmailDetailsModel = new TicketEmailArrayDetailsModel(ticketEmailArrayDetailsModel);
    let ticketEmailDetailsFormArray = this.formBuilder.group({});
    Object.keys(ticketEmailDetailsModel).forEach((key) => {
      ticketEmailDetailsFormArray.addControl(key, new FormControl({ value: ticketEmailDetailsModel[key], disabled: true }));
    });
    ticketEmailDetailsFormArray = setRequiredValidator(ticketEmailDetailsFormArray, ["email"]);
    this.getEmailFormArray.push(ticketEmailDetailsFormArray);
  }

  validateEmailExistItem(e) {
    const findItem = this.getEmailFormArray.controls.filter(el => el.value[e.field].toLowerCase() === this.getEmailFormArray.controls[e.index].get(e.field).value.toLowerCase());
    if (findItem.length > 1) {
      this.isSubmitted = true;
      this.snackbarService.openSnackbar(`${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[1].caption} item already exists`, ResponseMessageTypes.WARNING);
      return;
    } else {
      this.isSubmitted = false;
      return;
    }
  }

  validateContactsExistItem(e) {
    const findItem = this.getContactsFormArray.controls.filter(el => el.value[e.field].toLowerCase() === this.getContactsFormArray.controls[e.index].get(e.field).value.toLowerCase());
    if (findItem.length > 1) {
      this.isSubmitted = true;
      this.snackbarService.openSnackbar(`${this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[1].caption} item already exists`, ResponseMessageTypes.WARNING);
      return;
    } else {
      this.isSubmitted = false;
      return;
    }
  }

  removeConfigItem(i, type) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (type == 'email') {
        this.removeEmailItem(i);
      } else if (type == 'contact') {
        this.removeContactItem(i);
      }
    });
  }

  removeEmailItem(i) {
    let remItem;
    if (this.getEmailFormArray.controls[i].get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0].dataKey).value) {
      this.isSubmitted = true;
      remItem = { customerAlternateContactId: this.getEmailFormArray.controls[i].get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0].dataKey).value, createdUserId: this.config?.data?.userId, isEmail: true }
      let api = this.crudService.delete(ModulesBasedApiSuffix.SALES, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0].deleteAPI, '', prepareRequiredHttpParams({ ...remItem, modifiedUserId: this.config?.data?.userId }));
      api.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response?.statusCode == 200) {
          this.getEmailFormArray.removeAt(i);
          this.clearFormArray(this.getEmailFormArray);
          this.clearFormArray(this.getContactsFormArray);
          this.loadValue();
        }
        this.isSubmitted = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    } else {
      this.getEmailFormArray.removeAt(i);
    }
  }

  removeContactItem(i) {
    let remItem;
    if (this.getContactsFormArray.controls[i].get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[1].dataKey).value) {
      this.isSubmitted = true;
      remItem = { customerAlternateContactId: this.getContactsFormArray.controls[i].get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[1].dataKey).value, createdUserId: this.config?.data?.userId, isEmail: false }
      let api = this.crudService.delete(ModulesBasedApiSuffix.SALES, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[1].deleteAPI, '', prepareRequiredHttpParams({ ...remItem, modifiedUserId: this?.config?.data?.userId }));
      api.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response?.statusCode == 200) {
          this.getContactsFormArray.removeAt(i);
          this.clearFormArray(this.getEmailFormArray);
          this.clearFormArray(this.getContactsFormArray);
          this.loadValue();
        }
        this.isSubmitted = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    } else {
      this.getContactsFormArray.removeAt(i);
    }
  }

  get getEmailFormArray(): FormArray {
    if (!this.alterContactDialogForm) return;
    return this.alterContactDialogForm.get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0].formArrayName) as FormArray;
  }

  get getContactsFormArray(): FormArray {
    if (!this.alterContactDialogForm) return;
    return this.alterContactDialogForm.get(this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[1].formArrayName) as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };
}
