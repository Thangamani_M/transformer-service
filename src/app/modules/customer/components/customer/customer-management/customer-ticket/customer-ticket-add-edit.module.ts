import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerTicketAddEditComponent } from '.';




@NgModule({
  declarations: [CustomerTicketAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports:[CustomerTicketAddEditComponent],
  entryComponents:[CustomerTicketAddEditComponent]
})
export class CustomerTicketAddEditModule { }
