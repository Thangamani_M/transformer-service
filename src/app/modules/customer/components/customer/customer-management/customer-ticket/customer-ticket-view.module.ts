import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerSharedModule } from "@modules/customer/shared/customer-shared.module";
import { CustomerTicketViewPageModule } from "../customer-ticket-view-page/customer-ticket-view-page.module";
import { AlterContactDialogModule } from "./alter-contact-dialog/alter-contact-dialog.module";
import { CustomerTicketAddEditModule } from "./customer-ticket-add-edit.module";

import { CustomerTicketViewComponent } from "./customer-ticket-view.component";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [CustomerTicketViewComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    CustomerTicketAddEditModule,
    CustomerTicketViewPageModule,
    AlterContactDialogModule,
    CustomerSharedModule,
    RouterModule.forChild([
      {
        path: '', component: CustomerTicketViewComponent, canActivate: [AuthGuard], data: { title: 'Ticket View' }
      },
    ]),
  ],
  entryComponents: [],
  exports: [CustomerTicketViewComponent]
})
export class CustomerTicketViewModule { }
