import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TicketManualAddModel } from '@app/modules/customer/models';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, findInvalidControls, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { forkJoin } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Component({
  selector: 'app-customer-ticket-add-edit',
  templateUrl: './customer-ticket-add-edit.component.html',
  styleUrls: ['./customer-ticket.scss']
})
export class CustomerTicketAddEditComponent implements OnInit {
  @Input() id: string;
  ticketForm: FormGroup;
  TicketManualAddModel = new TicketManualAddModel();
  isFormSubmitted = false;
  subscriptionList = [];
  ticketOutcomeList = [];
  isOutcome: boolean;
  ticketTypeList = [];
  ticketStatusList = [];
  ticketGroupList = [];
  assigntoList = [];
  ticketCancelReasonList = [];
  ticketCancelSubReasonList = [];
  isCancelForReq: boolean;
  selectedActionType: string;
  groupId: string;
  title: string;
  loggedUser: UserLogin;
  customerId: string;
  submitted: boolean = false;
  fileList: File[] = [];
  public formData = new FormData();
  buttonDisable = false;
  @Output() outputData = new EventEmitter<any>();
  today = new Date();
  ticketDetails: any = [];
  data: any

  constructor(
    private datePipe: DatePipe, public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private store: Store<AppState>,
    private snackbarService : SnackbarService) {
    this.rxjsService.setDialogOpenProperty(true);
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.data = this.config.data
  }

  ngOnInit(): void {
    this.getForkJoinRequests();
    this.createTicketManualAddForm();
    this.ticketForm.get('ticketTypeId')?.valueChanges.subscribe((ticketTypeId: string) => {
      if (!ticketTypeId) return;
      setTimeout(() => {
        this.OnActionTypeChange(ticketTypeId);
      });
    })
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_ACTIVE_SUBSCRIPTIONS, prepareRequiredHttpParams({ CustomerId: this.data.custId, addressId: this.data?.addressId })),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_TICKETTYPES, undefined, true, null),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TICKET_CANCELLATION_REASON, undefined, true, null),
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TICKETSTATUS, undefined, true, null),
      this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_GROUPS, undefined, true)])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
            switch (ix) {
              case 0:
                this.subscriptionList = getPDropdownData(respObj.resources);
                break;
              case 1:
                this.ticketTypeList = respObj.resources;
                break;
              case 2:
                this.ticketCancelReasonList = respObj.resources;
                break;
              case 3:
                this.ticketStatusList = respObj.resources;
                if (!this.data?.ticketId) {
                  const ticketStatusData = this.ticketStatusList?.find(el => el?.displayName?.toLowerCase() == 'open');
                  this.ticketForm.patchValue({ ticketStatusId: ticketStatusData?.id });
                }
                break;
              case 4:
                this.ticketGroupList = getPDropdownData(respObj.resources);
                break;
            }
          }
          if (ix == response.length - 1 && !this.data.ticketId) {
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else if (ix == response.length - 1 && this.data.ticketId) {
            this.getTicketById();
          }
        });
      });
  }

  getTicketOutcomeList(res) {
    this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TICKET_OUTCOME_CONFIGURATION, prepareRequiredHttpParams({ customerId: this.data.custId, ticketTypeId: res }), 1).subscribe((response) => {
      if (response.statusCode == 200 && response.resources && response.isSuccess) {
        this.ticketOutcomeList = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getAssignedTo() {
    if (this.ticketForm.controls.userGroupId.value?.length) {
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_ASSINGNTO, prepareRequiredHttpParams({ groupId: this.ticketForm.controls.userGroupId.value })).subscribe((response) => {
        if (response.statusCode == 200 && response.resources && response.isSuccess) {
          this.assigntoList = getPDropdownData(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  emitUploadedFiles(uploadedFiles: any[]) {
    this.fileList = uploadedFiles;
    console.log(this.fileList)
    if (this.fileList?.length) {
      this.ticketForm.get('fileName').setValue(this.fileList[0]['name'])
    }else{
      this.ticketForm.get('fileName').setValue(null)
    }

  }

  OnActionTypeChange(event: any) {
    if (this.data?.ticketId) {
      let filteredTicketGroup = this.ticketTypeList?.find(tG => tG['id'] == this.ticketForm.get('ticketGroupId')?.value)?.ticketTypes;
      this.selectedActionType = filteredTicketGroup?.find(tG => tG['id'] == event)?.displayName;
    }
    else {
      try {
        this.selectedActionType = event.target.options[event.target.options.selectedIndex].text;
      }
      catch (e) {
        catchError(e);
      }
    }
    if (this.selectedActionType == 'Cancellations') {
      this.ticketForm.controls['cancellationDate'].setValidators([Validators.required]);
      this.ticketForm.controls['cancellationDate'].updateValueAndValidity();
      this.ticketForm.get('suspensionFromDate').setValidators(null);
      this.ticketForm.get('suspensionFromDate').setErrors(null);
      this.ticketForm.get('suspensionToDate').setValidators(null);
      this.ticketForm.get('suspensionToDate').setErrors(null);
    }
    else if (this.selectedActionType == 'Suspension request') {
      this.ticketForm.controls['suspensionFromDate'].setValidators([Validators.required]);
      this.ticketForm.controls['suspensionFromDate'].updateValueAndValidity();
      this.ticketForm.controls['suspensionToDate'].setValidators([Validators.required]);
      this.ticketForm.controls['suspensionToDate'].updateValueAndValidity();
      this.ticketForm.get('cancellationDate').setValidators(null);
      this.ticketForm.get('cancellationDate').setErrors(null);
    }
    else {
      this.ticketForm.get('cancellationDate').setValidators(null);
      this.ticketForm.get('cancellationDate').setErrors(null);
      this.ticketForm.get('suspensionFromDate').setValidators(null);
      this.ticketForm.get('suspensionFromDate').setErrors(null);
      this.ticketForm.get('suspensionToDate').setValidators(null);
      this.ticketForm.get('suspensionToDate').setErrors(null);
    }
  }

  OnUserGroupChange(event: any) {
    if (event?.value) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.groupId = event.value;
      this.getAssignedTo();
    }
  }

  createTicketManualAddForm(TicketModelData?: TicketManualAddModel): void {
    let ticketModel = new TicketManualAddModel(TicketModelData);
    this.ticketForm = this.formBuilder.group({});
    Object.keys(ticketModel).forEach((key) => {
      this.ticketForm.addControl(key, new FormControl(TicketManualAddModel[key]));
    });
    this.ticketForm = setRequiredValidator(this.ticketForm, ["ticketTypeId", "userGroupId", "ticketStatusId", "description"]);
    this.onSetAutoActionType();
    this.onValueChanges();
  }

  onSetAutoActionType() {
    if (this.data?.ticketTypeId) {
      this.ticketForm.get('ticketTypeId').setValue(this.data?.ticketTypeId);
      if (this.data?.isDisableTickeTypeId) {
        this.ticketForm.get('ticketTypeId').disable({ emitEvent: false });
      }
    }
  }

  onValueChanges() {
    this.ticketForm.get("ticketStatusId").valueChanges.subscribe(res => {
      if (res) {
        this.isOutcome = this.ticketStatusList.find(el => res == el?.id && el?.displayName?.toLowerCase() == 'completed');
        if (this.isOutcome) {
          this.ticketForm = setRequiredValidator(this.ticketForm, ["ticketTypeId", "userGroupId", "ticketStatusId", "description", "ticketOutcomeId"]);
        } else {
          this.ticketForm = setRequiredValidator(this.ticketForm, ["ticketTypeId", "userGroupId", "ticketStatusId", "description"]);
          this.ticketForm = clearFormControlValidators(this.ticketForm, ["ticketOutcomeId"]);
          this.ticketForm.get('ticketOutcomeId').reset();
        }
      }
    })
    this.ticketForm.get("ticketTypeId").valueChanges.subscribe(res => {
      if (res) {
        this.isCancelForReq = this.findRequestForCancel(res);
        this.getTicketOutcomeList(res);
        if(this.isCancelForReq){
          this.ticketForm = setRequiredValidator(this.ticketForm, ["subscriptionIds", "ticketOutcomeId"]);
        } else{
          this.ticketForm = clearFormControlValidators(this.ticketForm, ["subscriptionIds"]);
        }
        if (this.isOutcome && this.isCancelForReq) {
          this.ticketForm.controls['ticketOutcomeId'].setValidators([Validators.required]);
          this.ticketForm.controls['ticketOutcomeId'].updateValueAndValidity();
          this.setRequestForCancelValidators();
        } else if (!this.isOutcome && this.isCancelForReq) {
          this.setRequestForCancelValidators();
          this.ticketForm = clearFormControlValidators(this.ticketForm, ["ticketOutcomeId"]);
          this.ticketForm.get('ticketOutcomeId').reset();
        } else if (this.isOutcome && !this.isCancelForReq) {
          this.ticketForm.controls['ticketOutcomeId'].setValidators([Validators.required]);
          this.ticketForm.controls['ticketOutcomeId'].updateValueAndValidity();
          this.clearRequestForCancelValidators();
        } else {
          this.clearRequestForCancelValidators();
        }
      }
    })
    this.ticketForm.get("ticketCancellationReasonId").valueChanges.subscribe(res => {
      if (res) {
        this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TICKET_CANCELLATION_SUB_REASON, prepareRequiredHttpParams({ ticketCancellationReasonId: res })).subscribe((response: IApplicationResponse) => {
          this.ticketCancelSubReasonList = response.resources;
        });
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    this.ticketForm.get("isCancellationLetterReceived").valueChanges.subscribe(res => {
      if (res?.toString()) {
        if (res == true) {
          this.ticketForm.get('fileName').setValidators([Validators.required]);
          this.ticketForm.get('isRequestTypeWritten').setValidators([Validators.required]);
          this.ticketForm.controls['isRequestTypeWritten'].updateValueAndValidity();
          this.ticketForm.controls['fileName'].updateValueAndValidity();
        }
        if (res == false) {
          this.ticketForm = clearFormControlValidators(this.ticketForm, ["isRequestTypeWritten","fileName"]);
        }
      }
    });
  }

  setRequestForCancelValidators() {
    this.ticketForm.controls['ticketCancellationReasonId'].setValidators([Validators.required]);
    this.ticketForm.controls['ticketCancellationReasonId'].updateValueAndValidity();
    this.ticketForm.controls['ticketCancellationSubReasonId'].setValidators([Validators.required]);
    this.ticketForm.controls['ticketCancellationSubReasonId'].updateValueAndValidity();
    this.ticketForm.controls['isCancellationLetterReceived'].setValidators([Validators.required]);
    this.ticketForm.controls['isCancellationLetterReceived'].updateValueAndValidity();
  }

  clearRequestForCancelValidators() {
    this.ticketForm = clearFormControlValidators(this.ticketForm, ["ticketCancellationReasonId", "ticketCancellationSubReasonId", "isCancellationLetterReceived", "isRequestTypeWritten"]);
    this.ticketForm.get('ticketCancellationReasonId').reset();
    this.ticketForm.get('ticketCancellationSubReasonId').reset();
    this.ticketForm.get('isCancellationLetterReceived').reset();
    this.ticketForm.get('isRequestTypeWritten').reset();
  }

  findRequestForCancel(searchValue) {
    const searchList = this.ticketTypeList.find((el) => {
      return el.ticketTypes.some((item) => {
        return item?.displayName?.toLowerCase() == 'request for cancel' && searchValue == item?.id;
      });
    });
    return searchList?.ticketTypes?.length;
  }

  clickFlag(flag) {
    this.ticketForm.get("flagColorId").setValue(flag.id);
  }

  getTicketById() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKETS, this.data.ticketId, false, null, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200) {
          Object.keys(response.resources).forEach((key) => {
            if (key == 'suspensionFromDate' || key == 'suspensionToDate' || key == 'cancellationDate') {
              response.resources[key] = response.resources[key] ? new Date(response.resources[key]) : null;
            }
          });
          this.ticketDetails = response.resources;
          this.ticketForm?.patchValue({
            ticketGroupId: this.ticketDetails?.ticketGroupId ? this.ticketDetails?.ticketGroupId : null,
            ticketId: this.ticketDetails?.ticketId ? this.ticketDetails?.ticketId : null,
            ticketTypeId: this.ticketDetails?.ticketTypeId ? this.ticketDetails?.ticketTypeId : null,
            customerId: this.ticketDetails?.customerId ? this.ticketDetails?.customerId : null,
            description: this.ticketDetails?.description ? this.ticketDetails?.description : null,
            ticketOutcomeId: this.ticketDetails?.ticketOutcomeId ? this.ticketDetails?.ticketOutcomeId : null,
            ticketStatusId: this.ticketDetails?.ticketStatusId ? this.ticketDetails?.ticketStatusId : null,
            userGroupId: this.ticketDetails?.userGroupId ? this.ticketDetails?.userGroupId : [],
            cancellationDate: this.ticketDetails?.cancellationDate ? this.ticketDetails?.cancellationDate : null,
            suspensionFromDate: this.ticketDetails?.suspensionFromDate ? this.ticketDetails?.suspensionFromDate : null,
            suspensionToDate: this.ticketDetails?.suspensionToDate ? this.ticketDetails?.suspensionToDate : null,
            saveOfferPercentage: this.ticketDetails?.saveOfferPercentage ? this.ticketDetails?.saveOfferPercentage : null,
            subscriptionIds: this.ticketDetails?.subscriptionIds ? this.ticketDetails?.subscriptionIds : null,
            ticketCancellationReasonId: this.ticketDetails?.ticketCancellationReasonId ? this.ticketDetails?.ticketCancellationReasonId : null,
            ticketCancellationSubReasonId: this.ticketDetails?.ticketCancellationSubReasonId ? this.ticketDetails?.ticketCancellationSubReasonId : null,
            isRequestTypeWritten: this.ticketDetails?.isRequestTypeWritten?.toString() ? this.ticketDetails?.isRequestTypeWritten : null,
            isCancellationLetterReceived: this.ticketDetails?.isCancellationLetterReceived?.toString() ? this.ticketDetails?.isCancellationLetterReceived : null,
            assignedBy: this.ticketDetails?.assignedBy ? this.ticketDetails?.assignedBy : null,
            assignedTo: this.ticketDetails?.assignedTo ? this.ticketDetails?.assignedTo : [],
            priorityId: this.ticketDetails?.priorityId ? this.ticketDetails?.priorityId : null,
            flagColorId: this.ticketDetails?.flagColorId ? this.ticketDetails?.flagColorId : null,
            lapsedStartTime: this.ticketDetails?.lapsedStartTime ? this.ticketDetails?.lapsedStartTime : null,
            lapsedEndTime: this.ticketDetails?.lapsedEndTime ? this.ticketDetails?.lapsedEndTime : null,
            createdStartDate: this.ticketDetails?.createdStartDate ? this.ticketDetails?.createdStartDate : null,
            createdEndDate: this.ticketDetails?.createdEndDate ? this.ticketDetails?.createdEndDate : null,
            closedStartDate: this.ticketDetails?.closedStartDate ? this.ticketDetails?.closedStartDate : null,
            closedEndDate: this.ticketDetails?.closedEndDate ? this.ticketDetails?.closedEndDate : null,
            ticketStatusNameMultiple: this.ticketDetails?.ticketStatusNameMultiple ? this.ticketDetails?.ticketStatusNameMultiple : null,
            ticketTypeMultiple: this.ticketDetails?.ticketTypeMultiple ? this.ticketDetails?.ticketTypeMultiple : null,
            userGroupMultiple: this.ticketDetails?.userGroupMultiple ? this.ticketDetails?.userGroupMultiple : null,
            assignedToMultiple: this.ticketDetails?.assignedToMultiple ? this.ticketDetails?.assignedToMultiple : null,
            createdUserId: this.ticketDetails?.createdUserId ? this.ticketDetails?.createdUserId : null,
            siteAddressId: this.ticketDetails?.siteAddressId ? this.ticketDetails?.siteAddressId : null,
            isRadio: this.ticketDetails?.isRadio?.toString() ? this.ticketDetails?.isRadio : null,
            isSystem: this.ticketDetails?.isSystem?.toString() ? this.ticketDetails?.isSystem : null,
          });
          this.fileList = response.resources?.pastcoments[0]?.ticketDocuments ? response.resources?.pastcoments[0]?.ticketDocuments : [];
          this.getAssignedTo();
        }
      });
  }

  onSubmit(): void {
    this.isFormSubmitted = true;
    if (this.fileList?.length) {
      this.ticketForm = clearFormControlValidators(this.ticketForm, ["fileName"]);
    }
    if (findInvalidControls(this.ticketForm.controls)) {
      this.ticketForm.markAllAsTouched();
      return;
    }
    if(this.fileList?.length ==0 && this.isCancelForReq &&  this.ticketForm.value?.isCancellationLetterReceived){
      return this.snackbarService.openSnackbar("File attachment is required",ResponseMessageTypes.WARNING)
    }
    this.ticketForm.patchValue({
      assignedBy: this.loggedUser.userId,
      createdUserId: this.loggedUser.userId,
      customerId: this.data.custId,
      referenceTypeId: 1,
      ticketOutcomeId: this.ticketForm?.get('ticketOutcomeId')?.value ? this.ticketForm?.get('ticketOutcomeId')?.value : null,
    })
    let obj = this.ticketForm.getRawValue();
    if (this.data && this.data.ticketId) {
      obj.ticketId = this.data.ticketId;
      obj.actionTypeId = obj.ticketTypeId;
    }
    this.ticketForm.value.siteAddressId = this.data?.addressId;
    const payload = {
      ...this.ticketForm.getRawValue(), ...this.ticketForm.value
    }
    if (this.selectedActionType == 'Suspension request') {
      payload['cancellationDate'] = null;
      payload['suspensionFromDate'] = this.datePipe.transform(payload['suspensionFromDate'], 'yyyy-MM-dd');
      payload['suspensionToDate'] = this.datePipe.transform(payload['suspensionToDate'], 'yyyy-MM-dd');
    }
    else if (this.selectedActionType == 'Cancellations') {
      payload['cancellationDate'] = this.datePipe.transform(payload['cancellationDate'], 'yyyy-MM-dd');
      payload['suspensionFromDate'] = null;
      payload['suspensionToDate'] = null;
    }
    else {
      payload['cancellationDate'] = null;
      payload['suspensionFromDate'] = null;
      payload['suspensionToDate'] = null;
    }
    if (payload['assignedTo'] == null || payload['assignedTo'].length == 0) {
      payload['assignedTo'] = null;
    }
    let formData = new FormData();
    formData.append('ticket', JSON.stringify(payload));
    this.fileList?.forEach(file => {
      formData.append('file', file);
    });
    this.submitted = true;
    this.rxjsService.setDialogOpenProperty(true);
    this.buttonDisable = true;
    let crudService;
    if ((this.data?.isSuspendTicket == 'true' || this.data?.isSuspendTicket == true) && !this.data.ticketId) {
      crudService = this.getSuspendTicket();
    }
    else {
      if (this.data && this.data.ticketId) {
        crudService = this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKETS, formData)
      }
      else {
        crudService = this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKETS, formData);
      }
    }
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setDialogOpenProperty(false);
        if (this.data?.ticketTypeId) {
          this.ref.close({ ...response?.resources, ticketTypeId: this.data?.ticketTypeId });
        } else {
          this.ref.close(response?.resources);
        }
        this.outputData.emit(true);
      }
      this.buttonDisable = false;
    });
  }

  getSuspendTicket() {
    let payload = {
      serviceSuspendId: this.data?.serviceSuspendId,
      ticketPostDTO: {
        assignedBy: this.ticketForm.get("assignedBy")?.value,
        assignedTo: this.ticketForm.get("assignedTo")?.value,
        ticketTypeId: this.ticketForm.get("ticketTypeId")?.value,
        subscriptionIds: this.ticketForm.get("subscriptionIds")?.value,
        subscriptionServiceId: this.ticketForm.get("subscriptionServiceId")?.value,
        ticketStatusId: this.ticketForm.get("ticketStatusId")?.value,
        userGroupId: this.ticketForm.get("userGroupId")?.value,
        customerId: this.ticketForm.get("customerId")?.value,
        targetDate: this.ticketForm.get("targetDate")?.value,
        description: this.ticketForm.get("description")?.value,
        createdUserId: this.ticketForm.get("createdUserId")?.value,
        priorityId: this.ticketForm.get("priorityId")?.value,
        ticketOutcomeId: this.ticketForm.get("ticketOutcomeId")?.value,
        isRequestTypeWritten: this.ticketForm.get("isRequestTypeWritten")?.value,
        isCancellationLetterReceived: null,
        ticketCancellationSubReasonId: null,
        isRadio: this.ticketForm.get("isRadio")?.value,
        isSystem: this.ticketForm.get("isSystem")?.value,
        siteAddressId: this.data?.addressId,
        ticketId: this.ticketForm.get("ticketId")?.value
      }
    };
    if (this.selectedActionType == 'Suspension request') {
      payload.ticketPostDTO['cancellationDate'] = null;
      payload.ticketPostDTO['suspensionFromDate'] = this.datePipe.transform(this.ticketForm.get("suspensionFromDate").value, 'yyyy-MM-dd');
      payload.ticketPostDTO['suspensionToDate'] = this.datePipe.transform(this.ticketForm.get("suspensionToDate").value, 'yyyy-MM-dd');
    }
    else if (this.selectedActionType == 'Cancellations') {
      payload.ticketPostDTO['suspensionFromDate'] = null;
      payload.ticketPostDTO['suspensionToDate'] = null;
      payload.ticketPostDTO['cancellationDate'] = this.datePipe.transform(this.ticketForm.get("cancellationDate").value, 'yyyy-MM-dd');
    }
    if (payload.ticketPostDTO['assignedTo'] == null || payload.ticketPostDTO['assignedTo'].length == 0) {
      payload.ticketPostDTO['assignedTo'] = null;
    }
    let formData1 = new FormData();
    formData1.append('ticket', JSON.stringify(payload));
    if (this.fileList.length > 0) {
      for (const file of this.fileList) {
        formData1.append('file[]', file);
      }
    }
    return this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_SUSPEND_TICKETS, formData1);
  }

  onClose() {
    this.ref.close(false)
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
