import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerZoneAddEditModule } from "./customer-zone-add-edit.module";
import { CustomerZoneListComponent } from "./customer-zone-list.component";

@NgModule({
  declarations: [CustomerZoneListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    CustomerZoneAddEditModule,
  ],
  exports: [CustomerZoneListComponent],
  entryComponents: [],
})
export class CustomerZoneListModule { }