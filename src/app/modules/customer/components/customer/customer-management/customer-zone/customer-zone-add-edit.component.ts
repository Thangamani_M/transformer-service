import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { adjustDateFormatAsPerPCalendar, clearFormControlValidators, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared/utils';
import { CustomerZoneModel } from '@modules/customer/models/customer-zone-model';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { forkJoin, Observable } from 'rxjs';

@Component({
  selector: 'app-customer-zone-add-edit-component',
  templateUrl: './customer-zone-add-edit.component.html',
  styleUrls: ['./customer-zone.component.scss'],
})
export class CustomerZoneAddEditComponent implements OnInit {
  customerZoneAddEditForm: FormGroup;
  public formData = new FormData();
  userData: any;
  equipmentDropDown: any = [];
  ownershipTypesDropDown: any = [];
  partitionsDropDown: any = []
  zoneDropDown: any = [];
  serviceCallList: any = [];
  showValidationMsg: boolean;

  constructor(private formBuilder: FormBuilder, public config: DynamicDialogConfig, private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService, private crudService: CrudService, public ref: DynamicDialogRef,
    private momentService: MomentService, private snackbarService: SnackbarService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.createCustomerZoneAddEditForm();
    this.onLoadValue();
  }

  createCustomerZoneAddEditForm(): void {
    let customerZoneModel = new CustomerZoneModel();
    this.customerZoneAddEditForm = this.formBuilder.group({
    });

    Object.keys(customerZoneModel).forEach((key) => {
      this.customerZoneAddEditForm.addControl(key, new FormControl(customerZoneModel[key]));
    });
    this.customerZoneAddEditForm = setRequiredValidator(this.customerZoneAddEditForm, ["zoneId", "zoneName", "itemId", "installDate", "itemOwnershipTypeId"]);
    this.customerZoneAddEditForm.get('createdUserId').setValue(this.userData.userId)
    if (this.config?.data?.isShowPartition) {
      this.customerZoneAddEditForm.get('serialNo').setValue(this.config?.data?.serialNo);
      this.customerZoneAddEditForm.get('callInitiationId').setValue(this.config?.data?.callInitiationId);
      this.customerZoneAddEditForm.get('itemOwnershipTypeId').setValue(this.config?.data?.itemOwnershipTypeId);
      this.customerZoneAddEditForm.get('installDate').setValue(new Date());
      this.customerZoneAddEditForm.get('callInitiationId').disable();
      this.customerZoneAddEditForm.get('serialNo').disable();
    }
  }

  onLoadValue() {
    let api = [this.crudService.get(ModulesBasedApiSuffix.INVENTORY, CustomerModuleApiSuffixModels.EQUIPMENT),
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, CustomerModuleApiSuffixModels.ZONE_LIST),
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_OWNERSHIP_TYPES),
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_CALL_INTIATION_DECODER, null, false,
      prepareRequiredHttpParams({ customerId: this.config?.data?.customerId, addressId: this.config?.data?.customerAddressId, })),
    ]
    if (this.config?.data?.isShowPartition) {
      api.push(this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_PARTITION, null, false,
        prepareRequiredHttpParams({
          customerId: this.config?.data?.customerId,
          customerAddressId: this.config?.data?.customerAddressId,
        })),)
    }
    if (this.config?.data?.editableObject?.zoneEquipmentMappingId) {
      api.push(this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ZONE_EQUIPMENT_MAPPING, this.config?.data?.editableObject?.zoneEquipmentMappingId, false, null));
    }
    forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp?.isSuccess && resp?.statusCode == 200) {
          switch (ix) {
            case 0:
              this.equipmentDropDown = getPDropdownData(resp?.resources);
              if (this.config?.data?.isShowPartition) {
                this.customerZoneAddEditForm.get('itemId').setValue(this.config?.data?.itemId);
                this.customerZoneAddEditForm.get('itemId').disable();
              }
              break;
            case 1:
              this.zoneDropDown = resp?.resources;
              break;
            case 2:
              this.ownershipTypesDropDown = resp?.resources;
              break;
            case 3:
              this.serviceCallList = resp?.resources;
              break;
            case 4:
              if (this.config?.data?.isShowPartition) {
                this.partitionsDropDown = resp?.resources;
              } else {
                this.onPatchValue(resp);
              }
              break;
            case 5:
              this.onPatchValue(resp);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onChangeZone(e) {
    const val = this.zoneDropDown.filter(k => k.zoneId === e.target.value);
    this.customerZoneAddEditForm.get('zoneName').setValue(val[0].zoneCode);
  }

  onPatchValue(resp) {
    this.customerZoneAddEditForm.get('installDate').setValue(resp?.resources?.installDate ? new Date(resp?.resources?.installDate) : null);
    this.customerZoneAddEditForm.get('zoneName').setValue(resp?.resources?.zoneName);
    this.customerZoneAddEditForm.get('itemId').setValue(resp?.resources?.itemId);
    this.customerZoneAddEditForm.get('serialNo').setValue(resp?.resources?.serialNo);
    this.customerZoneAddEditForm.get('itemOwnershipTypeId').setValue(resp?.resources?.itemOwnershipTypeId);
    this.customerZoneAddEditForm.get('zoneId').setValue(resp?.resources?.zoneId);
    this.customerZoneAddEditForm.get('callInitiationId').setValue(resp?.resources?.callInitiationId);
  }

  displayCodeFn(val) {
    if (val) {
      return val?.displayName ? val?.displayName : val;
    } else { return val }
  }

  getValidateDecoders() {
    if (this.customerZoneAddEditForm.invalid) {
      Object.keys(this.customerZoneAddEditForm.controls).forEach((key) => {
        this.customerZoneAddEditForm.controls[key].markAsDirty();
      });
      return
    }
    if (this.config?.data?.isShowPartition) {
      this.onSubmit();
    } else  {
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_VADATE_DECODERS, null, false,
        prepareRequiredHttpParams({
          isDecoder: false,
          callInitiationId: this.customerZoneAddEditForm.get("callInitiationId").value,
          userId: this.userData.userId,
          serialNumber: this.customerZoneAddEditForm.get("serialNo").value,
          itemId: this.customerZoneAddEditForm.get("itemId").value,
        }))
        .subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.resources) {
            if (response.resources?.isSuccess == false) {
              this.snackbarService.openSnackbar(response.resources?.validateMessage, ResponseMessageTypes.WARNING);
              return
            }
            this.onSubmit();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  dialogClose() {
    this.ref.close(false)
  }

  getDateFormat() {
    return adjustDateFormatAsPerPCalendar();
  }

  onSubmit(): void {
    this.showValidationMsg = false;
    if (this.customerZoneAddEditForm.invalid) {
      this.customerZoneAddEditForm.markAllAsTouched();
      return;
    }
    delete this.customerZoneAddEditForm.value['createdDate'];
    let formValue = { ...this.customerZoneAddEditForm.getRawValue() };
    formValue.customerId = this.config?.data?.customerId;
    formValue.addressId = this.config?.data?.customerAddressId
    formValue['partitionId'] = this.config?.data?.partitionId ? this.config?.data?.partitionId : formValue['partitionId'];
    formValue['addressId'] = this.config?.data?.customerAddressId;
    formValue['zoneEquipmentMappingId'] = this.config?.data?.editableObject?.zoneEquipmentMappingId;
    formValue.installDate = this.momentService.localToUTCDateTime(this.customerZoneAddEditForm.get('installDate').value);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.config?.data?.editableObject?.zoneEquipmentMappingId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ZONE_EQUIPMENT_MAPPING, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ZONE_EQUIPMENT_MAPPING, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.ref.close(true)
      }
      this.rxjsService.setDialogOpenProperty(false);
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
