import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerZoneAddEditComponent } from "./customer-zone-add-edit.component";

@NgModule({
  declarations: [CustomerZoneAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [CustomerZoneAddEditComponent],
  entryComponents: [CustomerZoneAddEditComponent],
})
export class CustomerZoneAddEditModule { }