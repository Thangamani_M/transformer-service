import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';

import { TechnicianTestingListComponent } from "./technician-testing-list.component";

@NgModule({
    declarations: [TechnicianTestingListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  entryComponents: [],
  exports: [TechnicianTestingListComponent],
  providers: []
})
export class TechnicianTestingListModule { }