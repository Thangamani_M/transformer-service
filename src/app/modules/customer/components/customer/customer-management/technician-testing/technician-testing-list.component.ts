import { Component, Input, OnInit, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudType, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, Observable } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CorrectPasswordDialogComponent } from '../client-testing/correct-password-dialog/correct-password-dialog.component';
import { InCorrectPasswordDialogComponent } from '../client-testing/incorrect-password-dialog/incorrect-password-dialog.component';
@Component({
  selector: 'app-technician-testing-list',
  templateUrl: './technician-testing-list.component.html'
})

export class TechnicianTestingListComponent extends PrimeNgTableVariablesModel implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  @Input() partitionId;
  @Input() customerId;
  @Input() customerAddressId;
  @Input() permission;
  formConfigs = formConfigs;
  loggedInUserData: LoggedInUserModel;
  today: any = new Date()
  isDisableFields: boolean = false;
  isDisableButton: boolean = true;
  signalDurationDropDown: any = [];
  decoderList: any = [];
  partitionList: any = [];
  technicianTestingForm: FormGroup;
  loggedUser: any;
  myDate = new Date();

  constructor(private crudService: CrudService,
    public dialogService: DialogService,
    private store: Store<AppState>,
    private momentService: MomentService,
    private rxjsService: RxjsService,
    private _fb: FormBuilder,
    private dialog: MatDialog) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: " ",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'employeeId',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            radioBtn: true,
            columns: [
              { field: 'displayName', header: 'Name', width: '200px' },
              { field: 'contactNo', header: 'Number', width: '200px' },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.EMPLOYEE,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            shouldShowCreateActionBtn: false,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.createTechnicianTestingForm();
    this.combineLatestNgrxStoreData()
    this.getSignalDurations();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      let otherParams = {}
      if (this.customerId && this.customerAddressId) {
        this.getRequiredListData(null, null, otherParams);
      }
    }
  }

  createTechnicianTestingForm() {
    this.technicianTestingForm = this._fb.group({
      createdUserId: [this.loggedUser.userId ? this.loggedUser.userId : null],
      technicianId: [''],
      technicianPassword: ['', Validators.required],
      signalTestingDurationId: ['', Validators.required],
      contactNumber: [''],
      contactNumberCountryCode: ['+27'],
      techTestingConfigId: [''],
      decoderId: [''],
      partitionId: [this.partitionId ? this.partitionId : null],
      customerId: [''],
      customerAddressId: [''],
      isPasswordShow:[false],
      appointmentId: ['']
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.partitionId == '') {
      const params = { customerId: this.customerId, CustomerAddressId: this.customerAddressId }
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { customerId: this.customerId, CustomerAddressId: this.customerAddressId, partitionId: this.partitionId }
      otherParams = { ...otherParams, ...params };
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        data.resources.forEach(element => {
          let ccode = element.contactNumberCountryCode ? element.contactNumberCountryCode : ''
          element.contactNo = ccode + ' ' + element.contactNumber
        });;
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;

      }
    })
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      }
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
    this.dialog.closeAll();
  }

  onChangeSelecedRows(event){
    if(event == null){
      this.isDisableFields = false;
      this.technicianTestingForm.get('technicianId').setValue(null);
      this.technicianTestingForm.get('contactNumber').setValue(null);
      this.technicianTestingForm.get('technicianPassword').setValue(null);
      this.technicianTestingForm.get('signalTestingDurationId').setValue(null);
    }
    this.selectedRows = event
    this.isDisableFields = true;
    if (event) {
      this.isDisableFields = true;
      this.technicianTestingForm.get('technicianId').setValue(event.employeeId);
      this.technicianTestingForm.get('contactNumber').setValue(event.contactNumber);
    }
  }

  getSignalDurations() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_TECHNICAL_TESTING_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.signalDurationDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }

  onVerify(): void {
    this.rxjsService.setGlobalLoaderProperty(false)
    let data = {
      UserId: this.technicianTestingForm.value.technicianId,
      Password: this.technicianTestingForm.value.technicianPassword,
      IsTechTesting: true,
      IsPanic: false
    }
    let crudService: Observable<IApplicationResponse> = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICEAN_TESTING_PASSWORD_VERIFICATION,
      undefined,
      false, prepareGetRequestHttpParams(null, null, data))
    crudService.subscribe((response: IApplicationResponse) => {
      this.isDisableButton = response.isSuccess ? false : true
      // if (response.isSuccess == false) {
      //   const ref = this.dialogService.open(InCorrectPasswordDialogComponent, {
      //     header: 'Password',
      //     showHeader: true,
      //     baseZIndex: 1000,
      //     width: '650px',
      //   });
      //   ref.onClose.subscribe((result) => {
      //     if (result) {
      //       // this.ref.close({ valid: false });
      //     }
      //   });
      //   this.rxjsService.setGlobalLoaderProperty(false);
      // }
      // else {
      //   const ref = this.dialogService.open(CorrectPasswordDialogComponent, {
      //     header: 'Password',
      //     showHeader: true,
      //     baseZIndex: 1000,
      //     width: '650px',
      //   });
      //   ref.onClose.subscribe((result) => {
      //   });
      //   this.rxjsService.setGlobalLoaderProperty(false);
      // }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  };

  onActive(): void {
    if (this.technicianTestingForm.invalid) {
      this.technicianTestingForm.markAllAsTouched();
      return;
    }
    this.rxjsService.setGlobalLoaderProperty(false);
    let formValue = this.technicianTestingForm.value;
    formValue.techTestingConfigId = formValue.signalTestingDurationId;
    formValue.customerId = this.customerId;
    formValue.customerAddressId = this.customerAddressId;
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.TECHNICIAN_TESTING_OTHER, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.technicianTestingForm.get('technicianId').setValue('');
        this.technicianTestingForm.get('technicianPassword').setValue('');
        this.technicianTestingForm.get('signalTestingDurationId').setValue(null);
        this.isDisableFields = false;
        this.selectedRows = [];
        this.getRequiredListData();
      }
      
    })
  };

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  };

}
