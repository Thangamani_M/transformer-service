import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerKeyholderAddComponent } from "@modules/customer";

@NgModule({
  declarations: [CustomerKeyholderAddComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [CustomerKeyholderAddComponent],
  entryComponents: [CustomerKeyholderAddComponent],
})
export class CustomerKeyholderModule { }