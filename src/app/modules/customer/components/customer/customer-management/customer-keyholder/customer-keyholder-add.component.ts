import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatTabChangeEvent, MAT_DIALOG_DATA } from '@angular/material';
import { CustomerContact } from '@app/modules/customer/models';
import { CustomDirectiveConfig } from '@app/shared';
import { AlertService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { CustomerManagementService } from '../../../../services/customermanagement.services';

@Component({
  selector: 'customer-keyholder-add',
  templateUrl: './customer-keyholder-add.component.html',
  styleUrls: ['./customer-keyholder-add.component.scss']
})
export class CustomerKeyholderAddComponent implements OnInit {
  @Input() id: string;
  customerContactAddEditForm: FormGroup;
  applicationResponse: IApplicationResponse;
  customerContact = new CustomerContact();
  errorMessage: string;
  btnName: string;
  @Output() selectedTabChange: EventEmitter<MatTabChangeEvent>;
  provinceList: any;
  countryList: any;
  cityList: any;
  provinceid: number;
  cntryId: number;
  tmpStateid: number;
  tmpCntryId: number;
  regionid: number;
  regionList: any;
  suburbList: any;
  contactTypeList: any;
  languageList: any;
  tmpcustomerId: any;
  ddlCnfilter: any;
  enabledisableprovince: boolean = true;
  enabledisablecity: boolean = true;
  enabledisablesuburb: boolean = true;
  tmpPhoneNo: number;
  emailPattern: boolean;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });

  constructor(@Inject(MAT_DIALOG_DATA) public data: CustomerContact,

    private dialog: MatDialog,
    private customerManagementService: CustomerManagementService,
    private formBuilder: FormBuilder,
    private alertService: AlertService) {

  }
  ngOnInit() {

    if (this.data["Name"] == "Create") {
      this.tmpcustomerId = this.data["Id"];
    }
    else {
      this.data.customerContactId = this.data["Id"];
      this.data.customerId = this.data["CustId"]
    }

    if (this.data.customerContactId) {
      this.btnName = 'Update';
      this.enabledisableprovince = false;
      this.enabledisablecity = false;
      this.enabledisablesuburb = false;

    }
    else {
      this.btnName = 'Create';
      this.enabledisableprovince = true;
      this.enabledisablecity = true;
      this.enabledisablesuburb = true;
    }

    this.customerContactAddEditForm = this.formBuilder.group({
      customerContactId: '',
      firstName: '',
      lastName: '',
      phone: '',
      email: ['', [Validators.required, Validators.maxLength(50), Validators.email, Validators.pattern('^[A-za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      contactTypeId: '',
      customerId: '',
      languageId: '',
      countryId: '',
      regionId: '',
      provinceId: '',
      cityId: '',
      suburbId: '',
      displayName: '',
      languageName: '',
    });

    this.bindingCountryDropdown();
    this.bindingContactTypesDropdown();
    this.bindingLanguageDropdown();

    if (this.data.customerContactId != null) {
      this.countryList = [0];
      this.customerManagementService.getCustomerContactByCustomer(this.data.customerContactId).subscribe(
        {
          next: response => {
            this.applicationResponse = response,
              this.customerContact = this.applicationResponse.resources;
            this.customerContactAddEditForm = this.formBuilder.group({
              customerContactId: this.customerContact.customerContactId,
              firstName: this.customerContact.firstName,
              lastName: this.customerContact.lastName,
              phone: this.customerContact.phone,
              email: [this.customerContact.email, [Validators.required, Validators.maxLength(50), Validators.pattern("^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
              contactTypeId: this.customerContact.contactTypeId,
              customerId: this.customerContact.customerId,
              languageId: this.customerContact.languageId,
              countryId: this.customerContact.countryId,
              regionId: this.customerContact.regionId,
              provinceId: this.customerContact.provinceId,
              cityId: this.customerContact.cityId,
              suburbId: this.customerContact.suburbId,
              suburbName: this.customerContact.suburbName,
              countryName: this.customerContact.countryName,
              cityName: this.customerContact.cityName,
              regionName: this.customerContact.regionName,
              provinceName: this.customerContact.provinceName,
              displayName: this.customerContact.displayName
            });
            this.bindingProvincesDropdown(this.customerContact.countryId);
            this.loadCitiesbyProvinces(this.customerContact.provinceId);
            this.loadSuburbsbyCities(this.customerContact.cityId);
          }
        });
    }
  }

  //==============Binding Country Dropdown================//
  //Bind country dropdown values...
  private bindingCountryDropdown() {
    this.customerManagementService.getCountryDropdown().subscribe(
      {
        next: response => {
          this.applicationResponse = response;
          this.countryList = this.applicationResponse.resources;
          this.ddlCnfilter = this.countryList.find(({ displayName }) => displayName === 'South Africa').id;
          this.loadRegionsbyCountries(this.ddlCnfilter);
        }
      })
  }
  public loadRegionsbyCountries(cntryId) {
    if (cntryId != "") {
      this.enabledisableprovince = false;
      this.provinceList = [0];
      let stringToSplit = cntryId;
      let x = stringToSplit.split(": ");
      this.bindingProvincesDropdown(x);
    }
  }
  //Bind state dropdown values...
  private bindingProvincesDropdown(countryId) {
    this.customerManagementService.getProvincesDropdown(countryId).subscribe(
      {
        next: response => {
          this.applicationResponse = response;
          this.provinceList = this.applicationResponse.resources;
        }
      });
  }
  public loadCitiesbyProvinces(provinceId) {
    this.enabledisablecity = false;
    this.cityList = [0];
    let stringToSplit = provinceId;
    let x = stringToSplit.split(": ");
    this.bindingCityDrodown(x);
  }
  //Bind state dropdown values...
  private bindingCityDrodown(provinceId) {
    this.customerManagementService.getCityDropdown(provinceId).subscribe(
      {
        next: response => {
          this.applicationResponse = response;
          this.cityList = this.applicationResponse.resources;
        }
      });
  }

  public loadSuburbsbyCities(cityId) {
    this.enabledisablesuburb = false;
    this.suburbList = [0];
    let stringToSplit = cityId;
    let x = stringToSplit.split(": ");
    this.bindingSuburbsDrodown(x);
  }
  //Bind state dropdown values...
  private bindingSuburbsDrodown(cityId) {
    this.customerManagementService.getSuburbDropdown(cityId).subscribe(
      {
        next: response => {
          this.applicationResponse = response;
          this.suburbList = this.applicationResponse.resources;
        }
      });
  }
  //Bind state dropdown values...
  private bindingContactTypesDropdown() {
    this.customerManagementService.getContactTypesDropdown().subscribe(
      {
        next: response => {
          this.applicationResponse = response;
          this.contactTypeList = this.applicationResponse.resources;
        }
      });
  }
  private bindingLanguageDropdown() {
    this.customerManagementService.getLanguageDropdown().subscribe(
      {
        next: response => {
          this.applicationResponse = response;
          this.languageList = this.applicationResponse.resources;
        }
      });
  }

  //Insert/Update new city record...
  SaveCustomerContacts(): void {
    if (this.customerContactAddEditForm.invalid) {
      this.customerContactAddEditForm.get('contactTypeId').markAsTouched();
      this.customerContactAddEditForm.get('languageId').markAsTouched();
      this.customerContactAddEditForm.get('firstName').markAsTouched();
      this.customerContactAddEditForm.get('lastName').markAsTouched();
      this.customerContactAddEditForm.get('phone').markAsTouched();
      this.customerContactAddEditForm.get('email').markAsTouched();
      this.customerContactAddEditForm.get('countryId').markAsTouched();
      this.customerContactAddEditForm.get('provinceId').markAsTouched();
      this.customerContactAddEditForm.get('cityId').markAsTouched();
      this.customerContactAddEditForm.get('suburbId').markAsTouched();
      return;
    }

    const customerContact = { ...this.customerContact, ...this.customerContactAddEditForm.value }

    if (customerContact.customerContactId != "") {
      //Update city values...
      this.customerManagementService.UpdateCustomerContact(customerContact).subscribe({
        next: response => this.onSaveComplete(response),
        error: err => this.errorMessage = err
      });
    }
    else {
      customerContact.customerId = this.tmpcustomerId;
      //Insert city values...
      this.customerManagementService.SaveCustomerContact(customerContact).subscribe({
        next: response => this.onSaveComplete(response),
        error: err => this.errorMessage = err
      });
    }
  }

  //After save completed...
  onSaveComplete(response): void {
    if (response.statusCode == 200) {
      this.dialog.closeAll();
      //this.toastr.responseMessage(response);
      this.customerContactAddEditForm.reset();
    }
    else {
      this.alertService.processAlert(
        response,
      );
    }
  }

  //Numeric Only Function
  numericsOnly(event): boolean {
    let patt = /^([0-9])$/;
    let result = patt.test(event.key);
    return result;
  }

  //Character only textbox validation..
  public characterOnly(event) {
    if ((event.keyCode > 64 && event.keyCode < 91)
      || (event.keyCode > 96 && event.keyCode < 123)
      || event.keyCode == 8 || event.keyCode == 32) {
      return true;
    }
    else {

      return false;
    }
  }

  public onPaste(event) {
    event.preventDefault();
  }

}
