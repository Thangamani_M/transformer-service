import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { selectStaticEagerLoadingDocumentTypesState$, SupportDocumentUploadModel } from '@app/modules/';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, setRequiredValidator } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-support-document-upload',
  templateUrl: './support-document-upload.component.html',
})
export class SupportDocumentUploadComponent implements OnInit {
  supportDocumentUploadForm: FormGroup;
  userData;
  uploadedFile;
  uploadFileName;
  documentId: "";
  departments = [];
  customerId;
  addressId = "";
  documentTypes = [];
  feature: string;
  featureIndex: string;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });

  constructor(private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.customerId = params.customerId;
      this.documentId = params.documentId;
      this.addressId = params.addressId; 
      this.feature = params?.feature_name ?  params?.feature_name : '';
      this.featureIndex = params?.featureIndex ? params?.featureIndex : '';
    });
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.combineLatestNgrxStoreData();
  }

  ngOnInit(): void {
    this.getDepartments();
    this.createUploadForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectStaticEagerLoadingDocumentTypesState$)]
    ).subscribe((response) => {
      this.documentTypes = response[0];
    });
  }

  createUploadForm() {
    let supportDocumentUploadModel = new SupportDocumentUploadModel();
    this.supportDocumentUploadForm = this.formBuilder.group({});
    Object.keys(supportDocumentUploadModel).forEach((key) => {
      this.supportDocumentUploadForm.addControl(key, key == 'documentId' && this.documentId ?
        new FormControl(this.documentId) : new FormControl(supportDocumentUploadModel[key]));
    });
    this.supportDocumentUploadForm = setRequiredValidator(this.supportDocumentUploadForm, this.documentId ? ["documentTypeId", "fileName"] :
      ["documentTypeId", "fileUpload", "fileName"]);
    if (this.documentId) {
      this.getDocumentDetailsById();
    }
  }

  getDocumentDetailsById() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SUPPORT_DOCUMENT_DETAIL,
      undefined,
      false,
      prepareGetRequestHttpParams(null, null,
        { documentId: this.documentId })
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.uploadFileName = response.resources.fileName;
        response.resources.departmentId = response.resources.departmentId ? response.resources.departmentId : '';
        this.supportDocumentUploadForm.patchValue(response.resources)
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  getDepartments() {
    this.crudService.get(
      ModulesBasedApiSuffix.IT_MANAGEMENT,
      SalesModuleApiSuffixModels.SUPPORT_DEPARTMENT_LIST, null, true).subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.departments = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  emitUploadedFiles(uploadedFile) {
    if (!uploadedFile || uploadedFile?.length == 0) {
      return;
    }
    this.uploadedFile = uploadedFile;
    this.supportDocumentUploadForm.get('fileUpload').setValue(uploadedFile['name']);
  }

  onSubmit() {
    this.supportDocumentUploadForm.get('fileUpload').markAllAsTouched();
    if (this.supportDocumentUploadForm.invalid) {
      return;
    }
    let formData = new FormData();
    this.supportDocumentUploadForm.value.customerId = this.customerId;
    this.supportDocumentUploadForm.value.referenceId = this.customerId;
    this.supportDocumentUploadForm.value.createdUserId = this.userData.userId;
    if (this.uploadedFile) {
      formData.append("file", this.uploadedFile, this.uploadedFile['name']);
    }
    formData.append("json", JSON.stringify(this.supportDocumentUploadForm.value));
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.CUSTOMER_DOCS, formData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.cancel();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  cancel() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId,
      feature_name: this.feature,
      featureIndex: this.featureIndex,
      customerTab: 5,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();

  }

}
