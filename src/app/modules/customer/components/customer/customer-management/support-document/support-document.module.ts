import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { SupportDocumentUploadComponent } from "./support-document-upload.component";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations: [SupportDocumentUploadComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
        SharedModule,
        LayoutModule,
        RouterModule.forChild([
            { path: '', pathMatch: 'full', redirectTo: 'list' },
            {
                path: 'upload', component: SupportDocumentUploadComponent, data: { title: 'Support Document Upload' }, canActivate:[AuthGuard]
            },
        ])
    ],
    exports: [],
    entryComponents: [],
})
export class SupportDocumentModule { }