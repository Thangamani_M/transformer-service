import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { clearFormControlValidators, CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";

@Component({
  selector: 'app-offsetting',
  templateUrl: './offsetting.component.html',
  styleUrls: ['./offsetting.component.scss']
})

export class OffsettingComponent {
  primengTableConfigProperties: any;
  loggedUser: UserLogin;
  selectedTabIndex: any = 0;
  dataList: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  totalRecords: any;
  row: any = {}
  loading: boolean;
  selectedRows: any = [];
  id; addressId; customerId; debtorId;
  type;
  firstHeading: string;
  secondHeading: string;
  thirdHeading: string;
  fourthHeading: string;
  offsetForm: FormGroup;
  allocateForm: FormGroup;
  transactionProcessApproveForm: FormGroup;
  transactionProcessDeclineForm: FormGroup;
  firstSectionDetails;
  secondSectionDetails;
  transactionSection;
  thirdSectionDetails: any = [];
  formSectionDetails = [];
  optionalForms = [];
  requiredForms = [];
  viewable: boolean = false;
  transactionType;
  totalExclVat = 0; totalVAT = 0; totalInclVat = 0;
  customerDetails;
  reasonList: any = [];
  enableSubmit: boolean = true;
  descriptionTypeList = [];
  descriptionSubTypeList = [];
  optionalFormList = [];
  optionalFormListName = [];
  descriptionTypeDocList = [];
  descriptionSubTypeDocList = [];
  fileName;
  docLength = 1;
  fileObject;
  fileObjArr = [];
  optionalFileObjArr = [];
  descriptionSubTypeId;
  descriptionTypeId; optionName;
  optionalNameTemp = ''; descriptionType;
  isEdit: boolean = false;
  limitAmount;
  allocateAmount;
  dataListIndex;
  flag: boolean = false;
  submitFlag: boolean = false;
  approvePopupFlag: boolean = false;
  declinePopupFlag: boolean = false;
  formData = new FormData();
  transactionId;
  invoiceTotal=0;paymentTotal=0;
  startTodayDate=new Date();
  status;
  constructor(private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService, private router: Router, private formBuilder: FormBuilder,
    private rxjsService: RxjsService,  private crudService: CrudService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Allocate Transaction",
      breadCrumbItems: [{displayName:'Customer'},{displayName:'Allocate Transaction'}],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'FULL ALLOCATION',
            dataKey: '',
            captionFontSize: '21px',
            disabled: false,
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'transactionRefNo', header: 'Transaction', width: '200px' },
              { field: 'transactionDate', header: 'Date', width: '200px' },
              { field: 'transactionType', header: 'Type', width: '100px' },
              { field: 'transactionDescription', header: 'Description', width: '150px' },
              { field: 'reference', header: 'Reference', width: '100px' },
              { field: 'transactionTotal', header: 'TransactionTotal', width: '150px' },
              { field: 'paymentAllocationAmount', header: 'PaymentAllocationAmount', width: '200px' },
              { field: 'outStanding', header: 'OutStanding', width: '150px' },
              { field: 'allocation', header: 'Allocation', width: '100px', type: 'check-box' },
              { field: 'amount', header: 'Amount', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableRowDeleteActive: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false
          },
          {
            caption: 'PARTIAL ALLOCATION',
            dataKey: '',
            captionFontSize: '21px',
            disabled: false,
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'transactionRefNo', header: 'Transaction', width: '200px' },
              { field: 'transactionDate', header: 'Date', width: '200px' },
              { field: 'transactionType', header: 'Type', width: '100px' },
              { field: 'transactionDescription', header: 'Description', width: '150px' },
              { field: 'reference', header: 'Reference', width: '100px' },
              { field: 'transactionTotal', header: 'TransactionTotal', width: '150px' },
              { field: 'paymentAllocationAmount', header: 'PaymentAllocationAmount', width: '200px' },
              { field: 'outStanding', header: 'OutStanding', width: '150px' },
              { field: 'allocation', header: 'Allocation', width: '100px', type: 'check-box' },
              { field: 'amount', header: 'Amount', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableRowDeleteActive: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false
          }
        ]
      }
    }
    this.firstHeading = 'CUSTOMER INFO';
    this.secondHeading = 'DEBTOR INFO';
    this.thirdHeading = 'OUTSTANDING BALANCE';
    this.fourthHeading = 'OFFSET';
    this.id = this.activatedRoute.snapshot.queryParams.referenceId;
    this.status = this.activatedRoute.snapshot.queryParams.status;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.debtorId = this.activatedRoute.snapshot.queryParams.debtorId;
    this.type = this.activatedRoute.snapshot.queryParams.type;
    this.transactionType = this.activatedRoute.snapshot.queryParams.transactionType;
    this.transactionId = this.activatedRoute.snapshot.queryParams.transactionId;
    // let route1 = this.type == 'doa' ? { displayName: 'DOA Dashboard', relativeRouterUrl: '/collection/my-task-list' } : { displayName: 'Customer', relativeRouterUrl: '/customer/manage-customers/view-transactions', queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } };
    // this.primengTableConfigProperties.breadCrumbItems.push(route1);
    // let route2 = this.type == 'doa' ? { displayName: 'Credit and Re-Invoice- DOA Approve', relativeRouterUrl: '' } : { displayName: 'Credit and Re-Invoice', relativeRouterUrl: '' };
    // this.primengTableConfigProperties.breadCrumbItems.push(route2);
    // this.viewable = this.type == 'doa' ? true : false;
    // this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].disabled = (this.selectedTabIndex==0 || this.viewable ) ? false : true;
    // this.primengTableConfigProperties.tableComponentConfigs.tabsList[1].disabled = (this.selectedTabIndex==1 || this.viewable ) ? false : true;
    // this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableRowDelete=false;
    if (this.transactionType)
      this.transactionType == 'CN' ? this.primengTableConfigProperties.tableComponentConfigs.tabsList.splice(1, 1) : this.primengTableConfigProperties.tableComponentConfigs.tabsList.splice(0, 1);
  }
  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    // this.getReason();
    this.getRequiredDetail();
    this.createAllocateForm();
  }
  createForm() {
    this.offsetForm = this.formBuilder.group({});
    if (this.formSectionDetails && this.formSectionDetails.length > 0) {
      this.formSectionDetails.forEach((key) => {
        this.offsetForm.addControl(key.formName, new FormControl());
      });
    }
    if (this.transactionSection && this.transactionSection.length > 0) {
      this.transactionSection.forEach((key) => {
        this.offsetForm.addControl(key.formName, new FormControl());
      });
    }
    this.offsetForm.addControl('transactionNote', new FormControl());
    this.offsetForm.addControl('isSendCustomerCopy', new FormControl());
    this.offsetForm = setRequiredValidator(this.offsetForm, ["transactionNote"]);
    let formArray = this.formBuilder.array([]);
    this.offsetForm.addControl('documentsDTOs', formArray);
    this.setOptionalDoc();
    if (this.type == 'doa') {
      this.offsetForm.get('transactionNote').setValue(this.customerDetails.transactionDetailsDTO.transactionNotes);
      this.offsetForm.get('descriptionType').setValue(this.customerDetails.transactionDetailsDTO.invoiceTransactionDescriptionId);
      this.offsetForm.get('isSendCustomerCopy').setValue(this.customerDetails.transactionDetailsDTO.isSendCustomerCopy);
      this.offsetForm.get('transactionNote').disable();
      //  if(this.customerDetails.customer.documentDate)
      //this.offsetForm.get('documentDate').setValue(new Date(this.customerDetails.customer.documentDate));
    }
    this.valueWhenFormChange();
  }
  get transactionProcessFormArray(): FormArray {
    if (this.offsetForm !== undefined) {
      return (<FormArray>this.offsetForm.get('documentsDTOs'));
    }
  }
  createAllocateForm(): void {
    this.allocateForm = new FormGroup({
      'allocatVal': new FormControl(null),
    });
    //this.allocateForm = setRequiredValidator(this.allocateForm, ['allocatVal']);
  }
  createTransactionProcessApproveForm(): void {
    this.transactionProcessApproveForm = new FormGroup({
      'comments': new FormControl(null),
    });
    this.transactionProcessApproveForm = setRequiredValidator(this.transactionProcessApproveForm, ['comments']);
  }
  createTransactionProcessDeclineForm(): void {
    this.transactionProcessDeclineForm = new FormGroup({
      'declineReasonId': new FormControl(null),
      'comments': new FormControl(null),
    });
    this.transactionProcessDeclineForm = setRequiredValidator(this.transactionProcessDeclineForm, ['comments', 'declineReasonId']);
  }
  setOptionalDoc() {
    let obj = this.formBuilder.group({
      invoiceTransactidocumentCountonDescriptionSubTypeDocumentId: null,
      invoiceTransactionDescriptionDocumentId: null,
      documentCount: this.formBuilder.array([]),
      documentName: this.formBuilder.array([]),
      docName: null,
      fileName: null,
      CustomDocumentName: this.formBuilder.array([]),
      isRequired: false
    });
    this.transactionProcessFormArray.push(obj);
  }
  getReason() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_DECLINE_REASON)
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.reasonList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  valueWhenFormChange() {
    this.offsetForm.valueChanges.subscribe(data => {
      let flag = false;
      let documents = this.transactionProcessFormArray.value;
      flag = this.checkRequiredDoc(documents);
      let reqDoc = documents;
      if (reqDoc && reqDoc.length > 0) {
        this.enableSubmit = flag;
      }
      this.enableSubmit = (this.offsetForm.invalid || this.enableSubmit) ? true : false;
    })
  }
  checkRequiredDoc(documents) {
    let flag = false;
    if (documents && documents.length > 0) {
      let requiredDocuments = documents.filter(x => x.isRequired == true);
      let optionalDocuments = documents.filter(x => x.isRequired == false);
      if ((this.descriptionTypeList.length > 0 || this.descriptionSubTypeList.length > 0) && requiredDocuments.length == 0) {
        flag = true; return;
      }
      if (requiredDocuments && requiredDocuments.length > 0) {
        requiredDocuments.forEach(element => {
          if (element.documentName.length == 0)
            flag = true; return;
        });
      }
    }
    return flag;
  }
  getRequiredDetail() {
    this.rxjsService.setGlobalLoaderProperty(true);

    let doaApi = this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.OFFSET_DOA_DETAIL, undefined, false, prepareRequiredHttpParams({TransactionId:this.transactionId,type :this.transactionType}));
    let offsetApi = this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.OFFSET_DETAIL, undefined, false, prepareRequiredHttpParams({ referenceId: this.id, ReferenceTypeId: 1, CustomerId: this.customerId, DebtorId: this.debtorId }));
    let api = this.type == 'doa' ? doaApi : offsetApi;
    if (api) {
      api.subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
          this.customerDetails =  resp.resources;
          if (this.type == 'doa') {
            //  resp.resources.customer = resp.resources.creditReinvoiceDOADetails;
            // this.customerDetails =  resp.resources;
          }
          this.setOffsettingDetailDTO(resp.resources.transactionDetailsDTO);
          this.setBadDebitWriteOffOutstandingBalance(resp.resources.outstandingBalance);
          if (this.selectedTabIndex == 0 && this.type != 'doa') {
            this.dataList = resp.resources.transactionInvoiceItem.transactionInvoiceItemDTOs;
            this.setTotal();
          } else {
            this.dataList = resp.resources.transactionInvoiceItem.transactionInvoiceItemDTOs;
            this.setTotal();
          }
          this.descriptionTypeList = resp.resources.invoiceTransactiondescriptionDTO;
          this.descriptionType = resp.resources.transactionDetailsDTO?.transactionDescription;

            if (resp.resources.transactionDOASupportingDocuments) {
              resp.resources.transactionDOASupportingDocuments.forEach(element => {
                if (element.isRequired)
                  this.requiredForms.push({ documentName: element.documentName, documentPath: element.documentPath });
                else
                  this.optionalForms.push({ documentName: element.documentName, documentPath: element.documentPath });
              });
            }
          // this.invoiceTransactionTypeId = resp.resources.transactionDetailsDTO.invoiceTransactionTypeId;
          // this.subTypeDescription =resp.resources.transactionDetailsDTO?.subTypeDescription;
        }
        else
         this.setOffsettingDetailDTO(null);

        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  setOffsettingDetailDTO(val) {
    let customer = val;
    //let debtor = this.viewable? val.customer : val.debtor ;
    this.firstSectionDetails = [
      { name: 'Customer ID', value:  val?.customerRegNo, valueColor: '#166DFF', enableHyperLink: true },
      { name: 'Customer Name', value: customer?.customerName },
      { name: 'Customer Type', value: customer?.customerType },
      { name: 'Site Address', value: customer?.siteAddress },
      { name: 'Contact Number', value: customer?.contactNumber },
      { name: 'Email Address', value: customer?.emailAddress },
      { name: 'Co Reg No', value: customer?.customerCompanyRegNo },
      { name: 'Vat No', value: customer?.debtorVatno }
    ];
    this.secondSectionDetails = [
      { name: 'Debtor Name', value: customer?.debtorName }, { name: 'Debtor Code', value: customer?.debtorCode },
      { name: 'BDI Number', value: customer?.debtorBDINumber }, { name: 'Contact Number', value: customer?.debtorContact },
      { name: 'Email Address', value: customer?.debtorEmail }, { name: 'Co Reg No', value: customer?.debtorCompanyRegNo },
      { name: 'Vat No', value: customer?.debtorVatno }
    ];
    this.formSectionDetails = [
      { label: 'Region', formName: 'region', type: "text", value: customer?.regionName },
      { label: 'Division', formName: 'division', type: "text", value: customer?.divisionName },
      { label: 'Branch', formName: 'branch', type: "text", value: customer?.branchName },
      { label: 'Transaction Type', formName: 'transactionType', type: "text", value: customer?.transactionType },
      { label: 'Transaction Description', formName: 'transactionDescription', type: "text", value: customer?.transactionTypeDescription },
      { label: 'Description Type', formName: 'descriptionType', type: "select", value: customer?.invoiceTransactionDescriptionId },
      { label: 'Description Sub Type', formName: 'descriptionSubType', type: "select", value: customer?.invoiceTransactionDescriptionSubTypeId },

    ];
    this.createForm();
    //  if(customer?.documentDate){
    //       this.offsetForm.get('documentDate').setValue(new Date(customer.documentDate));
    //      this.offsetForm.get('documentDate').disable();
    //  }
  }
  setBadDebitWriteOffOutstandingBalance(outstandingBalance) {
    this.thirdSectionDetails = [
      // { label: 'Days Outstanding', value: 'Amount', colsize: "col-2" },
      { label: 'D180', value: outstandingBalance?.days180, colsize: "outstanding-balance-col-width" },
      { label: 'D150', value: outstandingBalance?.days150, colsize: "outstanding-balance-col-width" },
      { label: 'D120', value: outstandingBalance?.days120, colsize: "outstanding-balance-col-width" },
      { label: 'D90', value: outstandingBalance?.days90, colsize: "outstanding-balance-col-width" },
      { label: 'D60', value: outstandingBalance?.days60, colsize: "outstanding-balance-col-width" },
      { label: 'D30', value: outstandingBalance?.days30, colsize: "outstanding-balance-col-width" },
      { label: 'Current', value: outstandingBalance?.current, colsize: "outstanding-balance-col-width" },
      { label: 'Balance', value: outstandingBalance?.totalInvoiceOutStandingAmount, colsize: "outstanding-balance-col-width" }
    ]
  }
  setTotal() {
    if (this.dataList && this.dataList.length > 0) {
      this.dataList.forEach((element,index) => {
        element.checked = ( index == 0) ? true :  false;
        element.allocation = (element.transactionType != 'IN') ? true : false;
        element.diffAmount = 0;

        this.paymentTotal = this.paymentTotal + element.outStanding;
      });
      this.invoiceTotal = this.paymentTotal - (this.dataList[0].outStanding);
    }
    let datalis = {
      transactionRefNo: '', transactionDate: '', transactionType: '',
      transactionDescription: '', reference: '', transactionTotal: '', paymentAllocationAmount: '',
      outStanding: '', allocation: '', amount: ''
    };
    this.dataList.push(datalis);

  }
  obj;
  onFileSelected(files: FileList, type, index): void {
    const fileObj = files.item(0);
    this.fileObject = fileObj;
    this.fileName = fileObj.name;
    if(type=='approve'){
      this.obj = fileObj;return;
    }
    if (this.fileObjArr) {
      let filename = this.fileObjArr.filter(x => x.name == fileObj.name);
      if (filename && filename.length > 0) {
        this.snackbarService.openSnackbar("File name exists already.Please upload file with other name.", ResponseMessageTypes.WARNING);
        return;
      }
    }
    this.obj = fileObj;
    if (type == 'optional-doc')
      this.optionalFileObjArr.push(fileObj);

    this.fileObjArr.push(fileObj);
    this.transactionProcessFormArray.value[index].fileName = fileObj.name;
    //this.emitDataWhenFileChange();
  }
  clearRecord() {
    this.fileObjArr = [];
    this.optionalFileObjArr.forEach(file => {
      this.fileObjArr.push(file);
    });
    if (this.transactionProcessFormArray.value && this.transactionProcessFormArray.value.length > 0) {
      let all = this.transactionProcessFormArray.value;
      let required = this.transactionProcessFormArray.value.filter(x => x.isRequired == true);
      let optional = this.transactionProcessFormArray.value.filter(x => x.isRequired == false);
      if (required && required.length > 0) {
        Object.keys(required).forEach(key => {
          delete required[key];
        });
        let finalarr = []; finalarr = required.concat(optional);
        all.forEach((element, i) => {
          this.transactionProcessFormArray.removeAt(0);
        });
        finalarr.forEach((element, i) => {
          if (element) {
            this.transactionProcessFormArray.value.push(element);
          }
        });
      } else {
        all.forEach((element, i) => {
          this.transactionProcessFormArray.removeAt(0);
        });
        optional.forEach((element, i) => {
          if (element) {
            this.transactionProcessFormArray.value.push(element);
          }
        });
      }
    }
  }
  onChangeDescriptionType(id) {
    this.descriptionTypeId = id;
    this.descriptionSubTypeId = null;
    this.descriptionSubTypeList = [];
    this.offsetForm.get('descriptionSubType').setValue(null);
    this.clearRecord();
    if (id) {
      if (this.descriptionTypeList && this.descriptionTypeList.length > 0) {
        let desArr = this.descriptionTypeList.filter(x => x.invoiceTransactionDescriptionId == id);
        if (desArr && desArr.length > 0) {
          // if (desArr[0].subTypes && desArr[0].subTypes.length > 0)
          //   this.descriptionSubTypeList = desArr[0].subTypes;
          // else
          //   this.descriptionSubTypeList = [];

          this.descriptionTypeDocList = desArr[0].documents;
          this.setDescriptionTypeDoc();
          // if ((this.descriptionSubTypeList && this.descriptionSubTypeList.length == 0) && this.descriptionTypeDocList.length > 0) {
          //   this.setDescriptionTypeDoc();
          // }
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }
  onChangeDescriptionSubType(id) {
    this.descriptionSubTypeId = id;
    this.clearRecord();
    if (this.descriptionSubTypeList && this.descriptionSubTypeList.length > 0) {
      let desSubTypeArr = this.descriptionSubTypeList.filter(x => x.invoiceTransactionDescriptionSubTypeId == id);
      if (desSubTypeArr && desSubTypeArr.length > 0) {
        this.descriptionSubTypeDocList = desSubTypeArr[0].documents;
        if (this.descriptionSubTypeDocList && this.descriptionSubTypeDocList.length > 0)
          this.setDescriptionSubTypeDoc();
        else if (this.descriptionTypeDocList.length > 0)
          this.setDescriptionTypeDoc();
      }
    }
  }
  onChange(val, index) {
    this.optionalNameTemp = val;
    if (this.transactionProcessFormArray.value[index].isRequired == false)
      this.transactionProcessFormArray.value[index].CustomDocumentName.push(val);

    //this.emitDataWhenFileChange();
  }
  setDescriptionTypeDoc() {
    let temp = this.transactionProcessFormArray.value;
    this.descriptionTypeDocList.forEach(element => {
      let obj = this.formBuilder.group({
        invoiceTransactidocumentCountonDescriptionSubTypeDocumentId: null,
        invoiceTransactionDescriptionDocumentId: element.invoiceTransactiondescriptiondocumentId,
        documentCount: this.formBuilder.array([]),
        documentName: this.formBuilder.array([]),
        docName: element.documentName,
        fileName: null,
        CustomDocumentName: [],
        isRequired: true
      })
      obj = setRequiredValidator(obj, ["isRequired"]);
      this.transactionProcessFormArray.push(obj);
    });
    if (temp && temp.length > 0) {
      temp.forEach(element => {
        this.transactionProcessFormArray.value.push(element);
      });
    }
    // this.docLength = this.docLength + this.descriptionTypeDocList.length;
  }
  setDescriptionSubTypeDoc() {
    let temp = this.transactionProcessFormArray.value;
    this.descriptionSubTypeDocList.forEach(element => {
      let obj = this.formBuilder.group({
        invoiceTransactionDescriptionSubTypeDocumentId: element.invoiceTransactiondescriptionSubTypeDocumentId,
        invoiceTransactionDescriptionDocumentId: null,
        documentCount: this.formBuilder.array([]),
        documentName: this.formBuilder.array([]),
        docName: element.documentName,
        fileName: null,
        CustomDocumentName: [],
        isRequired: true
      })
      obj = setRequiredValidator(obj, ["isRequired"]);
      this.transactionProcessFormArray.push(obj);
    });
    if (temp && temp.length > 0) {
      temp.forEach(element => {
        this.transactionProcessFormArray.value.push(element);
      });
    }
    //this.docLength = this.docLength + this.descriptionTypeDocList.length;
  }
  addRequiredFormList(control, type, index) {
    if (this.fileObject) {
      if (type == 'required-doc') {
      }
      else if (type == 'optional-doc') {
        if (!this.optionalNameTemp) {
          this.snackbarService.openSnackbar("Please enter the name for optional documment.", ResponseMessageTypes.WARNING);
          return;
        }
      }
      this.transactionProcessFormArray.value[index].fileName = '';
      this.transactionProcessFormArray.value[index].documentCount.push(this.transactionProcessFormArray.value[index].documentCount.length + 1);
      this.transactionProcessFormArray.value[index].documentName.push(this.fileObject.name);
      // this.transactionProcessFormArray.value[index].CustomDocumentName.push(val);
      this.fileObject = null;
      this.optionalNameTemp = '';
      this.optionName = '';
    } else {
      this.snackbarService.openSnackbar("Please upload file.", ResponseMessageTypes.WARNING);
    }
    this.valueChange();
  }
  addOptionalFormList() {
    this.optionalFormList.push({
      invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
      documentPath: '',
      documentName: '',
      isRequired: false
    });
    this.valueChange();
  }
  removeRequiredFormList(index, sindex) {
    let documentName = this.transactionProcessFormArray.value[index].documentName[sindex];
    let findIndex = this.fileObjArr.findIndex(x => x.name == documentName);
    let findIndexoptional = this.optionalFileObjArr.findIndex(x => x.name == documentName);
    this.transactionProcessFormArray.value[index].documentCount.pop();
    this.transactionProcessFormArray.value[index].documentName.splice(sindex, 1);
    if (this.transactionProcessFormArray.value[index].isRequired == false)
      this.transactionProcessFormArray.value[index].CustomDocumentName.splice(sindex, 1);

    this.fileObjArr.splice(findIndex, 1);
    this.optionalFileObjArr.splice(findIndexoptional, 1);
    this.valueChange();
  }
  removeOptionalFormList(index) {
    this.optionalFormList.splice(index, 1);
    this.optionalFormListName.splice(index, 1);
    this.valueChange();
  }
  valueChange() {
    let flag = false;
    let documents = this.transactionProcessFormArray.value;
    flag = this.checkRequiredDoc(documents);
    let reqDoc = documents;
    if (reqDoc && reqDoc.length > 0) {
      this.enableSubmit = flag;
    }
    this.enableSubmit = (this.offsetForm.invalid || this.enableSubmit) ? true : false;
  }
  tabClick(e) {
    this.selectedTabIndex = e?.index;
    this.getRequiredDetail();
  }
  onChangeAllocateAmount(value) {
    if (value > this.limitAmount)
      this.snackbarService.openSnackbar("Please enter amount less than " + this.limitAmount, ResponseMessageTypes.WARNING);
    else
      this.allocateAmount = value;
  }
  saveAllocateAmount() {
    this.dataList[this.dataListIndex].paymentAllocationAmount = parseFloat(this.allocateAmount);
    this.dataList[this.dataListIndex].amount = parseFloat(this.allocateAmount);
    this.dataList[this.dataListIndex].diffAmount = Math.round((parseFloat(this.dataList[this.dataListIndex].outStanding) - parseFloat(this.dataList[this.dataListIndex].amount)) * 100) / 100;
    this.dataList[0].paymentAllocationAmount = parseFloat(this.dataList[0].paymentAllocationAmount) + parseFloat(this.allocateAmount);
     let tol=0;
     this.dataList.forEach((element,index) => {
       if(index!==0){

         tol = tol + this.dataList[index].amount;
       }

     });

    if (this.selectedTabIndex == 1) {
      this.dataList[0].amount = parseFloat(this.dataList[0].outStanding) + parseFloat(tol.toString());
      this.isEdit = false; this.allocateForm.reset();
    // this.dataList[this.dataList.length-1].amount = this.dataList[0].amount;
    }
    if (this.selectedTabIndex == 0){
      this.dataList[0].amount = parseFloat(this.dataList[0].amount) + parseFloat(this.allocateAmount);
      this.dataList[this.dataList.length - 1].amount = Math.round((this.dataList[0].outStanding - this.dataList[0].amount) * 100) / 100;
    }


  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.ACTION:
        this.dataListIndex = this.dataList.findIndex(x => x.transactionRefNo === row['transactionRefNo']);
        this.limitAmount = row['outStanding'];
        if (!unknownVar)
          this.uncheck();
        else {
          this.dataList[this.dataListIndex].checked = true;
          if (this.selectedTabIndex == 0) { //dont show edit popup for full allocation
            this.isEdit = false;
            this.allocateAmount = row['outStanding'];
            this.saveAllocateAmount();
          }
          else
            this.isEdit = unknownVar ? true : false; //show edit popup only for partial allocation
        }

        break;
    }
  }
  uncheck() {
    // Math.round(num * 100) /100
    let first = parseFloat(this.dataList[0].paymentAllocationAmount) - parseFloat(this.dataList[this.dataListIndex].paymentAllocationAmount);
    let sec = parseFloat(this.dataList[0].amount) - parseFloat(this.dataList[this.dataListIndex].amount);
    this.dataList[0].paymentAllocationAmount = Math.round(first * 100) / 100;
    this.dataList[0].amount = Math.round(sec * 100) / 100;
    this.dataList[this.dataListIndex].diffAmount = 0;
    this.dataList[this.dataListIndex].paymentAllocationAmount = 0;
    this.dataList[this.dataListIndex].amount = 0;
    this.dataList[this.dataListIndex].checked = false;
    if (this.selectedTabIndex == 0) // final amount total
      this.dataList[this.dataList.length - 1].amount = Math.round((this.dataList[0].outStanding - this.dataList[0].amount) * 100) / 100;
  }

  onSubmit() {
    if (this.type == 'doa') {
      this.rxjsService.setFormChangeDetectionProperty(false);
      this.approvePopupFlag = !this.approvePopupFlag;
      this.fileName = '';
      this.createTransactionProcessApproveForm();
    }
    else{
      if(this.selectedTabIndex==0 && Number(this.invoiceTotal) != Number(this.dataList[0].outStanding)){ //allow full allocation only if PA amt Equals to Sum of Invoice amt
          this.snackbarService.openSnackbar("You cannot do Full Allocation for this invoice.Try Partial Allocation.",ResponseMessageTypes.WARNING); return;
      }
      if(this.dataList &&  this.dataList.length){

      }
      this.submitFlag = !this.submitFlag;
    }

  }
  cancelSubmitPopup() {
    this.submitFlag = !this.submitFlag;
  }
  cancel() {
    if (this.type == 'doa') {
      this.declinePopupFlag = !this.declinePopupFlag;
      this.fileName = '';
      this.createTransactionProcessDeclineForm();
    }
    else
      this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } });
  }
  finalSubmit() {
    this.formData = new FormData(); let finalDoc=[];
    let offsettingDetailsInsertDTOs = [];
    let bankStatementAmount;
      if(this.dataList &&  this.dataList.length){
        this.dataList.forEach(element => {
          if (element.checked) {
            let temp = {
              invoicePaymentAndOverrideId: element.referenceId,
              invoiceAmount: element.outStanding, diffAmount: element.diffAmount
            }
            offsettingDetailsInsertDTOs.push(temp);
          }
        });
        bankStatementAmount = this.dataList[0].outStanding;
      }
      let documents = this.transactionProcessFormArray.value;
      // Object.keys(documents).forEach(key => {
      //   if(documents[key].documentName.length>0)
      //        finalDoc.push(documents[key]);
      // });
        //  let finalDoc=[];
        Object.keys(documents).forEach(key => {
            if(documents[key].documentName.length>0){
                 if(documents[key].CustomDocumentName &&  documents[key].CustomDocumentName.length >0)
                 documents[key].CustomDocumentName =  documents[key].CustomDocumentName.toString();

                 finalDoc.push(documents[key]);
            }
        });
      let formValue =  this.offsetForm.value;
      let finalObj =  {
        offsettingDTO: {
          offsettingId: null,
          notes: formValue.transactionNote,
          invoiceTransactionDescriptionId: formValue.descriptionType,
          referenceId: this.id,
          referenceTypeId: 1,
          bankStatementAmount: bankStatementAmount,
          bankStatementExcessAmount:  Math.round(( parseFloat(this.dataList[0].outStanding)-(this.dataList[0].amount))* 100) / 100,
          isFullAllocation: this.selectedTabIndex==0?true:false,
          customerId:this.customerId
        },
        offsettingDetailsInsertDTOs:offsettingDetailsInsertDTOs,
        offsettingDocumentsDTOs:finalDoc,
        createdUserId:this.loggedUser.userId
        }
        if(this.fileObjArr&&  this.fileObjArr.length >0){
          this.fileObjArr.forEach((element,index) => {
            this.formData.append("file"+index, element);
          });
        }
        this.formData.append('Obj', JSON.stringify(finalObj));


       let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.OFFSETTING, this.formData);
          api.subscribe((res: any) => {
            if (res?.isSuccess == true && res?.statusCode == 200) {
              this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } });
            }
            this.rxjsService.setDialogOpenProperty(false);
          });
  }
  approve() {
   this.rxjsService.setFormChangeProperty(false);
    // if (this.transactionProcessApproveForm.invalid) return;

    let finalOjb = {
      roleId: this.loggedUser.roleId,
      approvedUserId: this.loggedUser.userId,
      comments: this.transactionProcessApproveForm.value.comments,
      referenceNo: this.transactionId,
      declineReasonId: 0
    }
    let formData = new FormData();
    if(this.obj)
      formData.append("file", this.obj);

    formData.append('obj', JSON.stringify(finalOjb));
    let api = this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_NOTES_DOA_APPROVAL, formData);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.router.navigate(['/collection/my-task-list']);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  cancelApprove() {
    this.approvePopupFlag = !this.approvePopupFlag;
    this.transactionProcessApproveForm = clearFormControlValidators(this.transactionProcessApproveForm, ['comments']);
  }
  decline() {
    if (this.transactionProcessDeclineForm.invalid) return;

    let finalOjb = {
      roleId: this.loggedUser.roleId,
      approvedUserId: this.loggedUser.userId,
      comments: this.transactionProcessDeclineForm.value.comments,
      referenceNo: this.transactionId,
      declineReasonId: this.transactionProcessDeclineForm.value.declineReasonId,
    }
    let formData = new FormData();
    if(this.obj)
    formData.append("file", this.obj);

    formData.append('obj', JSON.stringify(finalOjb));
    let api = this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_NOTES_DOA_APPROVAL, formData);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.router.navigate(['/collection/my-task-list']);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  cancelDecline() {
    this.declinePopupFlag = !this.declinePopupFlag;
    this.transactionProcessDeclineForm = clearFormControlValidators(this.transactionProcessDeclineForm, ['comments', 'declineReasonId']);
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
