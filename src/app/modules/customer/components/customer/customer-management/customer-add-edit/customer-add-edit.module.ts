import { CommonModule, DatePipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { QuickActionModule } from "@app/shared/layouts/app-quick-action/quick-action.module";
import { MaterialModule } from '@app/shared/material.module';
import { CustomerComponentRoutingModule, CustomerManagementService } from "@modules/customer";
import { EmailDashboardListModule } from "@modules/my-tasks/components/email-dashboard/email-dashboard-list.module";
import { ServiceSalesInstallationAgreementModule } from "@modules/sales/components/task-management/service-installation-agreement/service-sales-installation-agreement.module";
import { CustomerTechnicaListModule } from "../../technician-installation/customer-technical/customer-technical-list.module";
import { CallEscalationModule } from "../../technician-installation/call-escalation-dialog/call-escalation-dialog.module";
import { AccessIntercomsModule } from "../access-intercoms/access-intercoms.module";
import { AuditModule } from "../audit/audit.module";
import { BoundryAllocationModule } from "../boundry-allocation/boundry-allocation.module";
import { CctvPointsListModule } from "../cctv-points/cctv-points-list.module";
import { CustomerAddressAddModule } from "../customer-address/customer-address-add.module";
import { CustomerCallHistoryModule } from "../customer-call-history/customer-call-history.module";
import { CustomerCategoriesModule } from "../customer-categories/customer-categories.module";
import { CustomerDecoderSpecialListModule } from "../customer-decoder-special/customer-decoder-special.module";
import { CustomerDecoderListModule } from "../customer-decoder/customer-decoder-list.module";
import { CustomerKeyholderModule } from "../customer-keyholder/customer-keyholder.module";
import { CustomerMessagingGroupHistoryModule } from "../customer-messaging-group/customer-messaging-group-history/customer-messaging-group-history.module";
import { CustomerMessagingGroupListModule } from "../customer-messaging-group/customer-messaging-group-list/customer-messaging-group-list.module";
import { CustomCallWorkflowListModule } from "../customer-preference/custom-call-workflow/custom-call-workflow-list.module";
import { CustomerDispatchFlowListModule } from "../customer-preference/customer-dispatch-flow/customer-dispatch-flow-list.module";
import { SiteCustomCallFlowListModule } from "../customer-preference/site-custom-call-flow/site-custom-call-flow-list.module";
import { SiteDispatchFlowListModule } from "../customer-preference/site-dispatch-flow/site-dispatch-flow-list.module";
import { CustomerSubscriptionModule } from "../customer-subscription/customer-subscription.module";
import { CustomerTicketListModule } from "../customer-ticket/customer-ticket-list.module";
import { SendEmailSMSModalModule } from "../customer-ticket/email-sms-modal/email-sms-modal.component";
import { CustomerVideofiedInfoModule } from "../customer-videofied-info/customer-videofied-info.module";
import { CustomerZoneListModule } from "../customer-zone/customer-zone-list.module";
import { ERBExclusionRequestModule } from "../erb-exclusion-request/erb-exclusion-request.module";
import { ErbSummaryModule } from "../erb-summary/erb-summary.module";
import { HolidayInstructionModule } from "../holiday-instructions/holiday-instruction.module";
import { NfcSiteTagConfigModule } from "../nfc-site-tag-config/nfc-site-tag-config.module";
import { PatrolHistoryListModule } from "../patrol-history/patrol-history.module";
import { CustomerProfileInformationModule } from "../profile-information/profile-information.module";
import { RelocationModule } from "../relocation/relocation.module";
import { SafeEntryRequestListModule } from "../safe-entry-request/safe-entry-request-list.module";
import { SignalHistoryModule } from "../signal-history/signal-history.module";
import { TechnicianTestingListModule } from "../technician-testing/technician-testing-list.module";
import { TransactionTabListModule } from "../transaction-tab-list/transaction-tab-list.module";
import { AccountAddEditModalModule } from "./account-add-edit-modal/account-add-edit-modal.module";
import { CorrespondenceLogModule } from "./correspondence-log/correspondence-log.module";
import { CustomerAddEditComponent } from "./customer-add-edit.component";
import { ClientTestingListModule } from "../client-testing/client-testing-list.module";
import { OtherDocumentsModule } from "@app/shared/components/other-documents/other-documents.module";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
  declarations: [CustomerAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '', component: CustomerAddEditComponent, canActivate: [AuthGuard], data: { title: 'Customer View' }
      },
    ]),
    ServiceSalesInstallationAgreementModule, //routing here
    CustomerComponentRoutingModule,
    CallEscalationModule,
    CustomerSubscriptionModule,
    SendEmailSMSModalModule,
    EmailDashboardListModule,
    CustomerTechnicaListModule,
    CustomerAddressAddModule,
    CustomerTicketListModule,
    CustomerKeyholderModule,
    CustomerDecoderListModule,
    CustomerDecoderSpecialListModule,
    CustomerZoneListModule,
    CustomCallWorkflowListModule,
    SiteCustomCallFlowListModule,
    CustomerDispatchFlowListModule,
    SiteDispatchFlowListModule,
    SignalHistoryModule,
    TechnicianTestingListModule,
    TransactionTabListModule,
    CctvPointsListModule,
    ERBExclusionRequestModule,
    CustomerVideofiedInfoModule,
    CorrespondenceLogModule,
    AccessIntercomsModule,
    CustomerCallHistoryModule,
    PatrolHistoryListModule,
    NfcSiteTagConfigModule,
    CustomerCategoriesModule,
    ErbSummaryModule,
    AuditModule,
    SafeEntryRequestListModule,
    BoundryAllocationModule,
    CustomerMessagingGroupHistoryModule,
    CustomerMessagingGroupListModule,
    CustomerProfileInformationModule,
    AccountAddEditModalModule,
    QuickActionModule,
    RelocationModule,
    ClientTestingListModule,
    HolidayInstructionModule,
    OtherDocumentsModule,
  ],
  entryComponents: [],
  exports: [],
  providers: [CustomerManagementService, DatePipe]
})
export class CustomerAddEditModule { }
