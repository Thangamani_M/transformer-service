import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomHeaderInfoModule } from "../../customer-service-upgrade-downgrade/customer-header/customer-header.module";
import { AnnualNetworkFeeApprovalComponent } from "./annual-network-fee-approval.component";

@NgModule({
  declarations: [AnnualNetworkFeeApprovalComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    CustomHeaderInfoModule,
    RouterModule.forChild([
      {
        path: '', component: AnnualNetworkFeeApprovalComponent, data: { title: 'Annual Network Fee Approval' }
      },
    ])
  ],
  exports: [],
  entryComponents: [],
})
export class AnnualNetworkFeeApprovalModule { }
