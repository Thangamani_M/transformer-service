import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute} from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CustomerTicketAddEditComponent } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
@Component({
  selector: 'app-add-refuse-to-pay',
  templateUrl: './add-refuse-to-pay.component.html',
  styleUrls: ['./add-refuse-to-pay.component.scss']
})
export class AddRefuseToPayComponent implements OnInit {
  customerId;
  debtorId;
  addRefuseToPayForm: FormGroup;
  campaignId;
  callOutcomeList: any = [];
  userData: UserLogin;
  addressId;
  startTodayDate1 = new Date();
  Outcomenotes: any;
  saveofferDialog: boolean = false;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  debtorBalance: any;
  refuseToReasonDrop: any;
  refuseToSubReasonDrop: any;
  refuseToPayId: any;
  result_ticketid: any;
  constructor(private momentService: MomentService, public dialogService: DialogService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService,private crudService: CrudService, private dialog: MatDialog) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.debtorId = this.activatedRoute.snapshot.queryParams.debtorId;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.campaignId = this.activatedRoute.snapshot.queryParams.campaignId;

  }

  ngOnInit(): void {
    this.getCallOutcomeDropdown();
    this.getRefuseToReasonDropDown()
    this.getCallEscalation()
    this.createForm();
  }


  getCallEscalation() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PROMISE_TO_PAY_DEBATOR_OUTSTANDING,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        debtorId: this.debtorId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.debtorBalance = res.resources
          this.addRefuseToPayForm.get('outstandingBalance').setValue(res.resources.outstandingBalance);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  createForm(): void {
    this.addRefuseToPayForm = this.formBuilder.group({
      'callOutcomeId': new FormControl(null),
      'callWrapupId': new FormControl(null),
      'outstandingBalance': new FormControl(null),
      'callDate': new FormControl(null),
      'refuseToPayReasonId': new FormControl(null),
      'refuseToPaySubReasonId': new FormControl(null)
    });
    this.addRefuseToPayForm = setRequiredValidator(this.addRefuseToPayForm, ['callOutcomeId', "callWrapupId", 'outstandingBalance', 'refuseToPayReasonId', 'refuseToPaySubReasonId']);
    this.valueChanges();
    this.addRefuseToPayForm.get('callDate').patchValue(new Date())
    this.addRefuseToPayForm.get('refuseToPayReasonId').valueChanges.subscribe(val => {
      if (!val) {
        return;
      }
      this.getRefuseToSubReasonDropDown(val);
    });
  }

  // Get Method
  changeOutcomeId(val) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_OUTCOME_CALLWRAPUP, null, false,
      prepareGetRequestHttpParams(null, null,
        { CallOutcomeId: val }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.Outcomenotes = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getRefuseToSubReasonDropDown(val) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_REFUSE_TO_SUBREASONS, null, false,
      prepareGetRequestHttpParams(null, null,
        { RefuseToPayReasonId: val }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.refuseToSubReasonDrop = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  valueChanges() {
    this.addRefuseToPayForm.get('callOutcomeId').valueChanges.subscribe(val => {
      if (!val) {
        return;
      }
      this.changeOutcomeId(val);
    });
  }

  getCallOutcomeDropdown() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_CALL_OUTCOME)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.callOutcomeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
 // Create Ticketes
  onaddnotesInfo() {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialogService.open(CustomerTicketAddEditComponent, 
      { width: '700px', showHeader: false, data: { custId: this.customerId, addressId: this.addressId } });
    dialogReff.onClose.subscribe(result => {
      if (result) {
        this.result_ticketid = result.ticketId
        return;
      }
    });
  }

  // refusetoreason Drop Down
  getRefuseToReasonDropDown() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_REFUSE_TO_REASONS)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.refuseToReasonDrop = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  offerDiscount() {
    this.saveofferDialog = true;
  }

  onSubmit() {
    if (this.addRefuseToPayForm.invalid) return;
    let formValue = this.addRefuseToPayForm.value;
    let finalOjb = {
      campaignId: this.campaignId,
      debtorId: this.debtorId,
      customerId: this.customerId,
      addressId: this.addressId,
      refuseToPayReasonId: this.addRefuseToPayForm.get('refuseToPayReasonId').value,
      refuseToPaySubReasonId: this.addRefuseToPayForm.get('refuseToPaySubReasonId').value,
      callDate: this.momentService.convertToUTC(this.addRefuseToPayForm.get('callDate').value),
      callOutcomeId: formValue.callOutcomeId,
      callWrapupId: formValue.callWrapupId,
      outstandingBalance: formValue.outstandingBalance,
      // uniqueCallId:this.uniqueCallId
    }
    Object.keys(finalOjb).forEach(key => {
      if (finalOjb[key] == "" || finalOjb[key] == null) {
        delete finalOjb[key]
      }
    });
    // this.disconnectCall()
    let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUSE_TO_PAY, finalOjb);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        //  this.router.navigate(['/collection/my-task-list']);
        this.refuseToPayId = res.resources

      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }


  getSaveOfferDiscount(val) {
    let data = {
      RefuseToPayId: this.refuseToPayId,
      IsOfferDiscount: true,
      IsAcceptDiscount: val,
    }
    this.crudService.update(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.ACCEPT_SAVEOFFERDISCOUNT, data)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          //  this.refuseToSubReasonDrop = response.resources;
          this.saveofferDialog = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSaveOfferDiscount1(val) {
    let data = {
      RefuseToPayId: this.refuseToPayId,
      IsOfferDiscount: true,
      IsAcceptDiscount: val
    }
    this.crudService.update(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.ACCEPT_SAVEOFFERDISCOUNT, data)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          //  this.refuseToSubReasonDrop = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


}