import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AvsCheckVerificationViewComponent } from "./avs-check-verification-view/avs-check-verification-view.component";

@NgModule({
  declarations: [AvsCheckVerificationViewComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
      {
        path: '', component: AvsCheckVerificationViewComponent, data: { title: 'Avs Check' }
      },
    ])
  ],
  exports: [],
  entryComponents: [],
})
export class AvsCheckVerificationModule { }