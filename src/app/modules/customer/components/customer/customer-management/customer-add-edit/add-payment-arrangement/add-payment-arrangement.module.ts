import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { AddPaymentArrangementComponent } from "./add-payment-arrangement.component";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations: [AddPaymentArrangementComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: AddPaymentArrangementComponent, canActivate: [AuthGuard], data: { title: 'Add Payment Arrangement' }
        },
    ])
  ],
  entryComponents: [],
  providers: []
})
export class AddPaymentArrangementModule { }
