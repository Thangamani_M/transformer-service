import { DatePipe } from "@angular/common";
import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { agentLoginDataSelector, CrudService,  IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, sidebarDataSelector$, SnackbarService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-add-payment-arrangement',
  templateUrl: './add-payment-arrangement.component.html'
})

export class AddPaymentArrangementComponent {
  addPaymentArrangementForm: FormGroup;
  type;
  customerId;
  debtorId;
  addressId;
  userData: UserLogin;
  agentExtensionNo;
  bankList: any = [];
  callOutcomeList: any = [];
  callWrapupList: any = [];
  paymentMethodList: any = [];
  isSelectBank: boolean = false;
  paymentOveridePopup: boolean = false;
  startTodayDate = new Date();
  popUpForm: FormGroup;
  campaignId;
  debtorDetails;
  paymentArrangementInstallment;
  paymentArrangementId;
  paDetails;
  uniqueCallId;
  permissions :any = []
  noOfPayments = [{ id: '1', displayName: '1' }, { id: '2', displayName: '2' }, { id: '3', displayName: '3' }, { id: '4', displayName: '4' }];
  promiseToPayId: string = '';
  primengTableConfigProperties = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }
  constructor( private datePipe: DatePipe,
    private router: Router, private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService, private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.type = this.activatedRoute.snapshot.queryParams.type;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.debtorId = this.activatedRoute.snapshot.queryParams.debtorId;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.campaignId = this.activatedRoute.snapshot.queryParams.campaignId;
    this.paymentArrangementId = this.activatedRoute.snapshot.queryParams.paymentArrangementId;
  }
  ngOnInit(): void {
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
    this.combineLatestNgrxStoreData();
    this.createForm();
    this.createPopupForm();
    this.getBankList();
    this.getDebatorInfoByIds();
    this.getCallOutcomeDropdown();
    this.getPaymentMethodList();
    if (this.paymentArrangementId)
      this.getRequiredDetail();

    this.rxjsService.setGlobalLoaderProperty(false);
  }


  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.pipe(select(agentLoginDataSelector)),
      this.store.select(sidebarDataSelector$)]
    ).subscribe((response) => {
      this.agentExtensionNo = response[0];
      let findCreditControl = response[1]?.['menuAccess'].find(item=> item.menuName =="Credit Control");
      let findCallWrapUpCodes = findCreditControl['subMenu'].find(item => item.menuName == "Call wrap up codes");
      let findPaymentArrangeMent = findCallWrapUpCodes['subMenu'].find(item=> item.menuName =="Payment Arrangement")
      if (findPaymentArrangeMent) {
        this.permissions = findPaymentArrangeMent['subMenu']
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, findPaymentArrangeMent['subMenu']);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  createForm(): void {
    this.addPaymentArrangementForm = this.formBuilder.group({
      'callOutcomeId': new FormControl(null),
      'callWrapupId': new FormControl(null),
      'outstandingBalance': new FormControl(null),
      'noOfPayments': new FormControl(null),
      'debitDescription': new FormControl(null),
      'bankId': new FormControl(null),
      paymentArrangementInstallment: this.formBuilder.array([])
    });
    this.addPaymentArrangementForm = setRequiredValidator(this.addPaymentArrangementForm, ['callOutcomeId', "noOfPayments", 'callWrapupId']);
  }
  valueChanges() {

  }
  createPopupForm() {
    this.popUpForm = this.formBuilder.group({
      'debitDescription': new FormControl(null),
      'paymentDate': new FormControl(null),
    });
  }
  get getItemsArray(): FormArray {
    if (this.addPaymentArrangementForm !== undefined) {
      return (<FormArray>this.addPaymentArrangementForm.get('paymentArrangementInstallment'));
    }
  }
  getRequiredDetail() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PAYMENT_ARRANGEMENTS_DETAILS, null, false,
      prepareRequiredHttpParams({ paymentArrangementId: this.paymentArrangementId })
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.paDetails = data.resources;
        this.setValue(data.resources);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  setValue(data) {
    this.onChangeCallOutcome(data.callOutcomeId);
    this.addPaymentArrangementForm.get('callOutcomeId').setValue(data.callOutcomeId);
    this.addPaymentArrangementForm.get('callWrapupId').setValue(data.callWrapupId);
    this.addPaymentArrangementForm.get('outstandingBalance').setValue(data.outstandingBalance);
    this.addPaymentArrangementForm.get('noOfPayments').setValue(data.noOfPayments);
    this.addPaymentArrangementForm.get('debitDescription').setValue(data.debitDescription);
    this.addPaymentArrangementForm.get('bankId').setValue(data.bankId);

    if (data.paymentInstallments) {
      data.paymentInstallments.forEach(element => {
        let item = this.formBuilder.group({
          paymentArrangementInstallmentId: element.paymentArrangementInstallmentId,
          paymentArrangementId: element.paymentArrangementId,
          paymentDate: new Date(element.paymentDate),
          paymentAmount: element.paymentAmount,
          paymentMethodId: element.paymentMethodId,
          createdUserId: this.userData.userId
        })
        item = setRequiredValidator(item, ["paymentDate","paymentAmount","paymentMethodId"]);
        this.getItemsArray.push(item);
      });
    }
  }
  getPaymentMethodList() {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UX_PAYMENT_METHODS, null, false,
      null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.paymentMethodList = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  getBankList() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_COMPANY_BANKS, null, false,
      null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.bankList = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  onChangeCallOutcome(value) {
    //ux/Outcome-CallWrapup?CallOutcomeId=1
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_CALL_OUTCOME_CALL_WRAPUP, null, null, prepareRequiredHttpParams({ CallOutcomeId: value }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.callWrapupList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getCallOutcomeDropdown() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_CALL_OUTCOME)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.callOutcomeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onChangeNoOfPayments(value) {
    let len = this.getItemsArray.length;
    for (let i = 0; i < len; i++) {
      this.getItemsArray.removeAt(0);
    }
    for (let i = 0; i < parseInt(value); i++) {
      let item = this.formBuilder.group({
        paymentArrangementInstallmentId: null,
        paymentArrangementId: null,
        paymentDate: null,
        paymentAmount: null,
        paymentMethodId: null,
        createdUserId: this.userData.userId
      })
      item = setRequiredValidator(item, ["paymentDate","paymentAmount","paymentMethodId"]);
      this.getItemsArray.push(item);
    }

  }
  getDebatorInfoByIds() {
    if (this.debtorId) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PAYMENT_ARRANGEMENTS_DEBTOR_DETAILS,
        null, false, prepareRequiredHttpParams({ DebtorId: this.debtorId, customerId: this.customerId, addressId: this.addressId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources) {
            this.debtorDetails = response.resources;
            this.addPaymentArrangementForm.get('outstandingBalance').setValue(response.resources.outStandingBalance);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }
  bankSms() {
    let find = this.permissions.find(item => item.menuName == "SMS");
    if (!find) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isSelectBank = true;
  }
  paymentOveride() {
    let find = this.permissions.find(item => item.menuName == "Load Payment Override");
    if (!find) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.paymentOveridePopup = true;
    this.isSelectBank = false;
    this.addPaymentArrangementForm?.get('bankId').setValue(null);
    let formValue = this.addPaymentArrangementForm.value;
    this.paymentArrangementInstallment = formValue.paymentArrangementInstallment;

    if (this.paymentArrangementInstallment && this.paymentArrangementInstallment.length > 0) {
      this.paymentOveridePopup = true;
    }else{
      this.paymentOveridePopup = false;
      this.snackbarService.openSnackbar("Select No Of Payments", ResponseMessageTypes.WARNING);
      return;
    }
    this.isSelectBank = false;
    this.addPaymentArrangementForm?.get('bankId').setValue(null);
    if (this.paymentArrangementInstallment && this.paymentArrangementInstallment.length > 0) {
      this.paymentArrangementInstallment.forEach(element => {
        element.paymentDates = this.datePipe.transform(element.paymentDate, 'dd-MM-yyyy HH:mm:ss');
        //element.paymentDate =  this.datePipe.transform(element.paymentDate, 'dd-mm-yyyy HH:mm:ss');
      });
    }

  }
  apply() {
    if(!this.popUpForm?.get('debitDescription').value)
       this.snackbarService.openSnackbar("Please enter the Debit description", ResponseMessageTypes.WARNING);

    this.addPaymentArrangementForm?.get('debitDescription').setValue(this.popUpForm?.get('debitDescription').value);
    this.paymentOveridePopup = !this.paymentOveridePopup;
  }
  navigate() {
    this.router.navigate(["/collection/call-wrap-up-codes/payment-arrangement"]);
  }
  onSubmit() {
    if (this.addPaymentArrangementForm.invalid && !this.uniqueCallId) return;
    let formValue = this.addPaymentArrangementForm.value;
    let finalOjb = {
      callWrapupDurationTime: "string",
      uniqueCallId: this.uniqueCallId,
      paymentArrangementInstallment: formValue.paymentArrangementInstallment,
      paymentArrangementId: this.paymentArrangementId ? this.paymentArrangementId : null,
      campaignId: this.campaignId,
      customerId: this.customerId,
      addressId: this.addressId,
      debtorId: this.debtorId,
      callOutcomeId: formValue.callOutcomeId,
      callWrapupId: formValue.callWrapupId,
      outstandingBalance: formValue.outstandingBalance,
      noOfPayments: formValue.noOfPayments,
      debitDescription: formValue.debitDescription,
      bankId: formValue.bankId,
      isTicket: true,
      createdUserId: this.userData.userId
    }

    //formValue.paymentDate = (formValue.paymentDate && formValue.paymentDate!=null)  ? this.momentService.toMoment(formValue.paymentDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;

    Object.keys(finalOjb).forEach(key => {
      if (finalOjb[key] == "" || finalOjb[key] == null) {
        delete finalOjb[key]
      }
    });
    let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PAYMENT_ARRANGEMENTS, finalOjb);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.router.navigate(['/collection/call-wrap-up-codes/payment-arrangement']);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  requestExtendedPaymentArrangement() {
    this.router.navigate(["/customer/request-extend-payment-arrangement"], { queryParams: { paymentArrangementId: this.paymentArrangementId } });
  }
  cancel() {
    this.paymentOveridePopup = false;
  }
}
