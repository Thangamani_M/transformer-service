import { Component, EventEmitter, Input, Output } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { BillingModuleApiSuffixModels } from "@modules/sales";

@Component({
  selector: 'app-transaction-process-common',
  templateUrl: './transaction-process-common.component.html',
  styleUrls: ['./transaction-process-common.component.scss']
})

export class TransactionProcessCommonComponent {
  primengTableConfigProperties;
  totalRecords: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  selectedTabIndex: any = 0;
  loading = false;
  transactionProcessForm: FormGroup;
  @Input() dataList: any;
  @Input() firstHeading: string;
  @Input() secondHeading: string;
  @Input() thirdHeading: string;
  @Input() fourthHeading: string;
  @Input() columns;
  @Input() type: string;
  @Input() requiredForms;
  @Input() optionalForms;
  @Input() firstSectionDetails;
  @Input() secondSectionDetails;
  @Input() transactionSection;
  @Input() invoiceTransactionTypeId;
  @Input() transactionDescription;
  @Input() descriptionId;
  @Input() descriptionSubId;
  @Input() subTypeDescription;
  @Input() descriptionType;
  @Input() thirdSectionDetails: any = [];
  @Input() formSectionDetails: any = [];
  @Output() emitData = new EventEmitter<any>();

  uploadApi;
  optionName;
  formData = new FormData();
  requiredFormList = [];
  requiredFormListName = [];
  optionalFormList = [];
  optionalFormListName = [];
  descriptionTypeList: any = [];
  descriptionSubTypeList = [];
  descriptionTypeDocList= [];
  descriptionSubTypeDocList= [];
  descriptionSubTypeName='';
  descriptionSubTypeId;
  descriptionTypeId
  fileName;
  docLength=1;
  fileObject;
  fileObjArr=[];
  optionalFileObjArr=[];
  viewable:boolean=false;
  startTodayDate = new Date();
  optionalNameTemp='';
  constructor(private crudService: CrudService,private snackbarService: SnackbarService, private formBuilder: FormBuilder, private rxjsService: RxjsService,) {
    this.primengTableConfigProperties = {
      tableCaption: "Transaction Process",
      breadCrumbItems: [],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Transaction Process',
            dataKey: '',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false
          }
        ]
      }
    }

  }
  ngOnInit() {
    this.viewable = (this.type == 'doa') ? true: false;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns = this.columns;
    this.createForm();
    this.getAPI();
    //if(this.invoiceTransactionTypeId)
     //  this.getDescriptionType();

    this.emitDataWhenFormChange();
    this.setDateDescriptionValue();
    this.setDescription();
  }
  getAPI(){
   switch(this.type){
     case 'bad-debit-writeOff':
      this.uploadApi =  CollectionModuleApiSuffixModels.BAD_DEBIT_WRITEOFF_UPLOAD;
     break;
     case 'bad-debit-recovery':
      this.uploadApi =  CollectionModuleApiSuffixModels.BAD_DEBIT_WRITEOFF_UPLOAD;
     break;
   }
  }
  createForm() {
    this.transactionProcessForm = this.formBuilder.group({});
    this.formSectionDetails.forEach((key) => {
      this.transactionProcessForm.addControl(key.formName, new FormControl());
    });
    this.transactionProcessForm.addControl('transactionNote', new FormControl());
    let formArray = this.formBuilder.array([]);
    this.transactionProcessForm.addControl('documentsDTOs', formArray);
    if(this.transactionSection && this.transactionSection.length>0)
      this.transactionProcessForm.get('transactionNote').setValue(this.transactionSection[0].value);

    this.setOptionalDoc();
    if(this.viewable){
      this.transactionProcessForm.get('descriptionType').setValue(this.descriptionId);
      this.transactionProcessForm.get('descriptionSubType').setValue(this.descriptionSubId);
    }
  }
  get transactionProcessFormArray(): FormArray {
    if (this.transactionProcessForm !== undefined) {
      return (<FormArray>this.transactionProcessForm.get('documentsDTOs'));
    }
  }
  setDescription(){
    this.descriptionTypeList =this.transactionDescription;
  }
  getDescriptionType() {
    let type = BillingModuleApiSuffixModels.UX_TRANSACTION_DESCRIPTION;
    this.crudService.get(ModulesBasedApiSuffix.BILLING, type, null, false, prepareRequiredHttpParams({invoiceTransactionTypeId:this.invoiceTransactionTypeId})).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        //this.descriptionTypeList = response.resources
      }
    });
  }
  clearRecord(){
    this.fileObjArr=[];
    this.optionalFileObjArr.forEach(file => {
      this.fileObjArr.push(file);
    });
    if(this.transactionProcessFormArray.value && this.transactionProcessFormArray.value.length >0 ){
      let all =this.transactionProcessFormArray.value;
      let required =this.transactionProcessFormArray.value.filter(x=>x.isRequired==true);
      let optional = this.transactionProcessFormArray.value.filter(x=>x.isRequired==false);
      if(required && required.length>0){
        Object.keys(required).forEach(key => {
            delete required[key];
        });
        let finalarr=[];finalarr=required.concat(optional);
          all.forEach((element,i) => {
            this.transactionProcessFormArray.removeAt(0);
          });
          finalarr.forEach((element,i) => {
           if(element){
            this.transactionProcessFormArray.value.push(element);
           }
          });
      } else{
        all.forEach((element,i) => {
          this.transactionProcessFormArray.removeAt(0);
        });
        optional.forEach((element,i) => {
            if(element){
            this.transactionProcessFormArray.value.push(element);
            }
        });
      }
    }
  }
  onChangeDescriptionType(id) {
    this.descriptionTypeId = id;
    this.descriptionSubTypeId=null;
    this.descriptionSubTypeList = [];
    this.transactionProcessForm.get('descriptionSubType').setValue(null);
    this.clearRecord();
    if (id) {
      if(this.transactionDescription && this.transactionDescription.length>0){
        let desArr = this.transactionDescription.filter(x=>x.invoiceTransactionDescriptionId==id);
        if(desArr && desArr.length >0) {
          if(desArr[0].subTypes && desArr[0].subTypes.length >0)
             this.descriptionSubTypeList = desArr[0].subTypes;
          else
             this.descriptionSubTypeList = [];

           this.descriptionTypeDocList= desArr[0].documents;
           if((this.descriptionSubTypeList && this.descriptionSubTypeList.length ==0) && this.descriptionTypeDocList.length>0){
              this.setDescriptionTypeDoc();
           }
        }
      }

      // let params = { InvoiceTransactionDescriptionId: id }
      // this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_INVOICE_TRANSACTION_DESCRIPTION_SUBTYPES, null, false, prepareRequiredHttpParams(params)).subscribe((response: IApplicationResponse) => {
      //   if (response.isSuccess == true && response.statusCode == 200) {
      //     this.descriptionSubTypeList = response.resources;
      //   }
      // });
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }
  onChangeDescriptionSubType(id) {
     this.descriptionSubTypeId = id;
     this.clearRecord();
    if(this.descriptionSubTypeList && this.descriptionSubTypeList.length >0){
      let desSubTypeArr = this.descriptionSubTypeList.filter(x=>x.invoiceTransactionDescriptionSubTypeId==id);
      if(desSubTypeArr && desSubTypeArr.length >0) {
        this.descriptionSubTypeDocList = desSubTypeArr[0].documents;
        if(this.descriptionSubTypeDocList && this.descriptionSubTypeDocList.length >0)
           this.setDescriptionSubTypeDoc();
        else if(this.descriptionTypeDocList.length>0)
           this.setDescriptionTypeDoc();
      }
    }
  }
  setDescriptionTypeDoc(){
    let temp = this.transactionProcessFormArray.value;
    this.descriptionTypeDocList.forEach(element => {
      let obj = this.formBuilder.group({
        invoiceTransactidocumentCountonDescriptionSubTypeDocumentId:null,
        invoiceTransactionDescriptionDocumentId:element.invoiceTransactiondescriptiondocumentId,
        documentCount: this.formBuilder.array([]),
        documentName: this.formBuilder.array([]),
        docName:element.documentName,
        fileName:null,
        CustomDocumentName:null,
        isRequired:true
       })
       obj = setRequiredValidator(obj, ["isRequired"]);
      this.transactionProcessFormArray.push(obj);
    });
    if(temp && temp.length>0){
      temp.forEach(element => {
        this.transactionProcessFormArray.value.push(element);
      });
    }
    this.emitDataWhenFileChange();
    // this.docLength = this.docLength + this.descriptionTypeDocList.length;
  }
  setDescriptionSubTypeDoc(){
    let temp = this.transactionProcessFormArray.value;
    this.descriptionSubTypeDocList.forEach(element => {
      let obj = this.formBuilder.group({
        invoiceTransactionDescriptionSubTypeDocumentId:element.invoiceTransactiondescriptionSubTypeDocumentId ,
        invoiceTransactionDescriptionDocumentId: null,
        documentCount: this.formBuilder.array([]),
        documentName: this.formBuilder.array([]),
        docName:element.documentName,
        fileName:null,
        CustomDocumentName:null,
        isRequired:true
       })
       obj = setRequiredValidator(obj, ["isRequired"]);
       this.transactionProcessFormArray.push(obj);
    });
    if(temp && temp.length>0){
      temp.forEach(element => {
        this.transactionProcessFormArray.value.push(element);
      });
    }
    this.emitDataWhenFileChange();
    //this.docLength = this.docLength + this.descriptionTypeDocList.length;
  }
  setOptionalDoc(){
    let obj = this.formBuilder.group({
      invoiceTransactidocumentCountonDescriptionSubTypeDocumentId:null,
      invoiceTransactionDescriptionDocumentId:null,
      documentCount: this.formBuilder.array([]),
      documentName: this.formBuilder.array([]),
      docName:null,
      fileName:null,
      CustomDocumentName:this.formBuilder.array([]),
      isRequired:false
     });
     this.transactionProcessFormArray.push(obj);
  }
  onChange(val,index){
    this.optionalNameTemp=val;
    if(this.transactionProcessFormArray.value[index].isRequired==false)
      this.transactionProcessFormArray.value[index].CustomDocumentName.push(val);

    this.emitDataWhenFileChange();
  }
  obj: any;
  // onFileSelected(files: FileList, type, index): void {
  //   const fileObj = files.item(0);
  //   this.fileName = fileObj.name;
  //   this.obj = fileObj;
  //   this.getFilePath(fileObj, type, index);
  // }
  setDateDescriptionValue(){
    let  documentDate = this.formSectionDetails.filter(x=>x.formName=='documentDate');
    this.transactionProcessForm.get('documentDate').setValue(documentDate[0].value);
    this.transactionProcessForm.get('documentDate').disable();

    let  descriptionType = this.formSectionDetails.filter(x=>x.formName=='descriptionType');
    let  descriptionSubType= this.formSectionDetails.filter(x=>x.formName=='descriptionSubType');
    let  descriptionTypeId = descriptionType[0].value;
    let  descriptionSubTypeId = descriptionSubType[0].value;
    if(descriptionTypeId){
      this.transactionProcessForm.get('descriptionType').setValue(descriptionTypeId);
      this.transactionProcessForm.get('descriptionSubType').setValue(descriptionSubTypeId);
      this.transactionProcessForm.get('descriptionType').disable();
      this.transactionProcessForm.get('descriptionSubType').disable();
      this.onChangeDescriptionType(descriptionTypeId);
      this.onChangeDescriptionSubType(descriptionSubTypeId);
    }
  }
  getFilePath(fileObj, type, index) {
    if(index==0 && type == 'required-doc')
      this.addRequiredFormList(null,null,null);
    else if(index==0 && type == 'additional-doc')
      this.addOptionalFormList();

    if (type == 'required-doc') {
      this.uploadDoc(fileObj, type).then((data) => {
        this.requiredFormList[index] = {
          invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
          documentPath: data,
          documentName: fileObj.name,
          isRequired: true
        };
        this.emitDataWhenFileChange();
      });
     this.requiredFormListName[index] = fileObj.name;
    }
    if (type == 'additional-doc') {
      this.uploadDoc(fileObj, type).then((data) => {
        this.optionalFormListName[index] = fileObj.name;
        this.optionalFormList[index] = {
          invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
          documentPath: data,
          documentName: fileObj.name,
          isRequired: false
        };
        this.emitDataWhenFileChange();
      });
    }

  }
  uploadDoc(fileObj, type) {
    this.formData = new FormData();
    return new Promise((resolve, reject) => {
      this.formData.append("file", fileObj);
      let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, this.uploadApi, this.formData);
      api.subscribe((res: any) => {
        if (res?.isSuccess == true && res?.statusCode == 200) {
          resolve(res.resources);
        }
        else
          reject(null);

       this.rxjsService.setGlobalLoaderProperty(false);
      })
    });

  }
  onFileSelected(files: FileList, type, index): void {

    const fileObj = files.item(0);
    this.fileObject = fileObj;
    this.fileName=fileObj.name;
    if(this.fileObjArr){

     let filename = this.fileObjArr.filter(x=>x.name==fileObj.name);
     if(filename && filename.length>0){
      this.snackbarService.openSnackbar("File name exists already.Please upload file with other name.",ResponseMessageTypes.WARNING);
      return;
     }
    }
    this.obj = fileObj;
    if(type == 'optional-doc')
        this.optionalFileObjArr.push(fileObj);

     this.fileObjArr.push(fileObj);
    this.transactionProcessFormArray.value[index].fileName=fileObj.name;
    this.emitDataWhenFileChange();
  }
  addRequiredFormList(control,type,index) {
    // this.requiredFormList.push({
    //   invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
    //   documentPath: '',
    //   documentName: '',
    //   isRequired: true
    // });
    // this.requiredFormListName.push('');

    if(this.fileObject){
      if(type=='required-doc'){
      }
      else if(type=='optional-doc'){
        // if(!this.optionalNameTemp){
        //   this.snackbarService.openSnackbar("Please enter the name for optional documment.",ResponseMessageTypes.WARNING);
        //   return;
        // }
      }
      this.transactionProcessFormArray.value[index].fileName='';
      this.transactionProcessFormArray.value[index].documentCount.push(this.transactionProcessFormArray.value[index].documentCount.length+1);
      this.transactionProcessFormArray.value[index].documentName.push(this.fileObject.name);
     // this.transactionProcessFormArray.value[index].CustomDocumentName.push(val);
      this.fileObject=null;
      this.optionalNameTemp='';
    }else {
      this.snackbarService.openSnackbar("Please upload file.", ResponseMessageTypes.WARNING);
    }
    this.emitDataWhenFileChange();
  }
  addOptionalFormList() {
    this.optionalFormList.push({
      invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
      documentPath: '',
      documentName: '',
      isRequired: false
    });
  }
  removeRequiredFormList(index,sindex) {
    // this.requiredFormList.splice(index, 1);
    // this.requiredFormListName.splice(index, 1);
    // this.emitDataWhenFileChange();
    let documentName =  this.transactionProcessFormArray.value[index].documentName[sindex];
    let findIndex= this.fileObjArr.findIndex(x=>x.name==documentName);
    let findIndexoptional =  this.optionalFileObjArr.findIndex(x=>x.name==documentName);
    this.transactionProcessFormArray.value[index].documentCount.pop();
    this.transactionProcessFormArray.value[index].documentName.splice(sindex, 1);
    if(this.transactionProcessFormArray.value[index].isRequired == false)
      this.transactionProcessFormArray.value[index].CustomDocumentName.splice(sindex, 1);

    this.fileObjArr.splice(findIndex, 1);
    this.optionalFileObjArr.splice(findIndexoptional, 1);
    this.emitDataWhenFileChange();
  }
  removeOptionalFormList(index) {
    this.optionalFormList.splice(index, 1);
    this.optionalFormListName.splice(index, 1);
    this.emitDataWhenFileChange();
  }
  emitDataWhenFormChange (){
    this.transactionProcessForm.valueChanges.subscribe(data=>{
      let flag=false;
      let documents = this.transactionProcessFormArray.value;
      flag = this.checkRequiredDoc(documents);
      this.emitData.emit({documents : documents ,documentsArr:this.fileObjArr, data : this.transactionProcessForm.value,flag:flag});
    })
  }
  emitDataWhenFileChange() {
    let flag=false;
    let documents = this.transactionProcessFormArray.value;
    flag = this.checkRequiredDoc(documents);
    this.emitData.emit({documents : documents ,documentsArr:this.fileObjArr, data : this.transactionProcessForm.value,flag:flag});
  }
  checkRequiredDoc(documents){
    let flag=false;
    if(documents && documents.length > 0){
      let requiredDocuments = documents.filter(x=>x.isRequired==true);
      let optionalDocuments = documents.filter(x=>x.isRequired==false);
      if((this.descriptionTypeList.length >0 || this.descriptionSubTypeList.length>0) && requiredDocuments.length==0){
        flag =  true;return;
      }
      if(requiredDocuments && requiredDocuments.length > 0 ){
        requiredDocuments.forEach(element => {
              if(element.documentName.length==0)
              flag =  true;return;
          });
      }
    }
    return flag;
  }

}
