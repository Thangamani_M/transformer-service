import { Component, Input } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";

@Component({
  selector: 'app-correspondence-log',
  templateUrl: './correspondence-log.component.html'
})
export class CorrespondenceLogComponent {
  @Input() permissions;
  primengTableConfigProperties: any;
  dataList = [];
  loading: boolean;
  status: any = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  totalRecords: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  userData;
  first: any = 0;
  selectedTabIndex = 0;
  customerId;
  constructor(private activatedRoute: ActivatedRoute,
    private router: Router, private momentService: MomentService, private store: Store<AppState>, private rxjsService: RxjsService, private crudService: CrudService) {
    this.primengTableConfigProperties = {
      tableCaption: "CORRESPONDENCE LOG",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Billing Management', relativeRouterUrl: '' }, { displayName: 'Create Bulk Upload' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'INVOICES',
            dataKey: 'bdi',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,

            columns: [
              { field: 'invoiceDate', header: 'Invoice Date', width: '200px',isDateTime:true  },
              { field: 'isProcessed', header: 'Processed', width: '200px' },
              { field: 'processedDate', header: 'Processed Date', width: '200px' ,isDateTime:true },
              { field: 'emailStatus', header: 'Email Status ', width: '200px' },
              { field: 'isEmailDelivered', header: 'Email Delivered', width: '200px' },
              { field: 'isEmailRead', header: 'Email Read', width: '200px' },
              { field: 'emailReadDate', header: 'Email Read Date', width: '200px',isDateTime:true  },
              { field: 'isSmsSent', header: 'SMS Sent', width: '200px' },
              { field: 'isSmsDelivered', header: 'SMS Delivered', width: '200px' },
              { field: 'smsStatus', header: 'SMS Status', width: '200px' },
              { field: 'isPushNotificationSent', header: 'Push Notification Sent', width: '200px' },
              { field: 'isPushNotificationDelivered', header: 'Push Notification Delivered', width: '200px' }
            ],
            apiSuffixModel: CollectionModuleApiSuffixModels.CORRESPONDENCE_LOG_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            disabled:true
          },
          {
            caption: 'STATEMENTS',
            dataKey: 'bdi',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'invoiceDate', header: 'Creation Date', width: '200px',isDateTime:true  },
              { field: 'isProcessed', header: 'Processed', width: '200px' },
              { field: 'isEmailDelivered', header: 'Type', width: '200px',},
              { field: 'emailStatus', header: 'Processed', width: '200px' },
              { field: 'processedDate', header: 'Processed Date', width: '200px',isDateTime:true  },
              { field: 'isEmailRead', header: 'Email Status', width: '200px' },
              { field: 'emailStatus', header: 'Email Delivered', width: '200px' },
              { field: 'isSmsSent', header: 'Email Read', width: '200px' },
              { field: 'emailReadDate', header: 'Email Read Date', width: '200px',isDateTime:true  },
              { field: 'smsStatus', header: 'SMS Sent', width: '200px' },
              { field: 'isSmsDelivered', header: 'SMS Delivered', width: '200px' },
              // { field: 'smsStatus', header: 'SMS Status', width: '200px' },
              // { field: 'smsStatus', header: 'SMS Delivered Date', width: '200px' },
              // { field: 'smsStatus', header: 'Posted Date', width: '200px' },
              // { field: 'smsStatus', header: 'File Batch ID', width: '200px' },
              // { field: 'smsStatus', header: 'Unique Report ID', width: '200px' },
              // { field: 'smsStatus', header: 'Debtor ID', width: '200px' }
            ],
            apiSuffixModel: CollectionModuleApiSuffixModels.CORRESPONDENCE_LOG_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            disabled:true
          },
          {
            caption: 'INTERIM STATEMENTS',
            dataKey: 'bdi',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'invoiceDate', header: 'Statement Date', width: '200px',isDateTime:true  },
              { field: 'emailStatus', header: 'Processed', width: '200px' },
              { field: 'processedDate', header: 'Processed Date', width: '200px',isDateTime:true  },
              { field: 'isEmailRead', header: 'Email Status', width: '200px' },
              { field: 'isSmsDelivered', header: 'Email Delivered', width: '200px' },
              { field: 'isSmsSent', header: 'Email Read', width: '200px' },
              { field: 'emailReadDate', header: 'Email Read Date', width: '200px',isDateTime:true  },
              // { field: 'smsStatus', header: 'SMS Sent', width: '200px' },
              // { field: 'smsStatus', header: 'SMS Delivered', width: '200px' },
              // { field: 'smsStatus', header: 'SMS Status', width: '200px' },
              // { field: 'smsStatus', header: 'SMS Delivered Date', width: '200px' },
              // { field: 'smsStatus', header: 'Posted', width: '200px' },
              // { field: 'smsStatus', header: 'User Name', width: '200px' },
              // { field: 'smsStatus', header: 'File Batch ID', width: '200px' },
              // { field: 'smsStatus', header: 'Unique Report ID', width: '200px' },
              // { field: 'smsStatus', header: 'Debtor ID', width: '200px' }
            ],
            apiSuffixModel: CollectionModuleApiSuffixModels.CORRESPONDENCE_LOG_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            disabled:true
          },
          {
            caption: 'ERB NOTIFICATIONS',
            dataKey: 'bdi',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'invoiceDate', header: 'Invoice Date', width: '200px',isDateTime:true  },
              { field: 'isProcessed', header: 'Processed', width: '200px' },
              { field: 'processedDate', header: 'Processed Date', width: '200px',isDateTime:true  },
              { field: 'emailStatus', header: 'Email Status ', width: '200px' },
              { field: 'isEmailDelivered', header: 'Email Delivered', width: '200px' },
              { field: 'isEmailRead', header: 'Email Read', width: '200px' },
              { field: 'emailReadDate', header: 'Email Read Date', width: '200px',isDateTime:true  },
              { field: 'isSmsSent', header: 'SMS Sent', width: '200px' },
              { field: 'isSmsDelivered', header: 'SMS Delivered', width: '200px' },
              { field: 'smsStatus', header: 'SMS Status', width: '200px' },
              { field: 'isPushNotificationSent', header: 'Push Notification Sent', width: '200px' },
              { field: 'isPushNotificationDelivered', header: 'Push Notification Delivered', width: '200px' }
            ],
            apiSuffixModel: CollectionModuleApiSuffixModels.CORRESPONDENCE_LOG_LIST,
            moduleName: ModulesBasedApiSuffix.COLLECTIONS,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            disabled:true
          }
        ]

      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    });
    this.activatedRoute.paramMap.subscribe(res => {
      if (res) {
        this.customerId = res ? res.get('id') : '';
      }
    })
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredList();
  }
  combineLatestNgrxStoreData() {
    let permission = this.permissions?.subMenu?.find(item => item.menuName == "Tabs");
    if (permission) {
      let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission['subMenu']);
      this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
    }
  }



  getRequiredList(pageIndex?, pageSize?, params?) {
    let index = this.selectedTabIndex + 1;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.CORRESPONDENCE_LOG_LIST,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        customerId: this.customerId, CorrespondenceLogTypeId: index
      }), 1).subscribe(resp => {
        if (resp.isSuccess && resp.resources) {
          this.dataList = resp.resources;
          this.totalRecords = resp.totalCount;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredList(row["pageIndex"], row["pageSize"], unknownVar);
        break;
      default:
    }
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  tabClick(event) {
    this.selectedTabIndex = event?.index;
    if (this.selectedTabIndex == 0 || this.selectedTabIndex == 1 || this.selectedTabIndex == 3)
      this.getRequiredList();
  }
}
