import { HttpParams } from "@angular/common/http";
import { Component } from "@angular/core";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";

@Component({
    selector: 'app-transaction-number',
    templateUrl: './transaction-number.component.html',
    styleUrls: ['./transaction-number.component.scss']
})

export class TransactionProcessNumberComponent { 
    primengTableConfigProperties: any;
    loggedUser;
    selectedTabIndex: any = 0;
    totalRecords: any =0;
    dataList=[];
    pageLimit: number[] = [10, 25, 50, 75, 100];
    row: any = {}
    loading: boolean;
    selectedRows: any = [];
    descriptionTypeList=[];
    constructor(private crudService: CrudService,
        public config: DynamicDialogConfig, private store: Store<AppState>,
        public ref: DynamicDialogRef, private rxjsService: RxjsService,) {
            this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
                if (!userData) return;
                this.loggedUser = userData;
            });
            this.primengTableConfigProperties = {
                tableCaption: "Transaction Process",
                breadCrumbItems: [{ displayName: 'Customer Management', relativeRouterUrl: '' }, { displayName: 'Customer List', relativeRouterUrl: '' }, { displayName: 'Transaction Process', relativeRouterUrl: '', }],
                selectedTabIndex: 0,
                tableComponentConfigs: {
                    tabsList: [
                        {
                            caption: 'Transaction Process',
                            dataKey: '',
                            captionFontSize: '21px',
                            enableBreadCrumb: true,
                            enableAction: true,
                            enableReset: false,
                            enableGlobalSearch: false,
                            reorderableColumns: false,
                            resizableColumns: false,
                            enableScrollable: true,
                            checkBox: false,
                            enableRowDelete: false,
                            enableFieldsSearch: false,
                            enableHyperLink: false,
                            cursorLinkIndex: 0,
                            columns: [
                                { field: 'processName', header: 'Process Name', width: '150px' },
                                { field: 'pathDescription', header: 'Path Description', width: '150px' },
                                { field: 'creatorName', header: 'Creator Name', width: '150px' },
                                { field: 'currentLevelUserName', header: 'Current Level User Name', width: '150px' },
                                { field: 'currentLevel', header: 'currentLevel', width: '150px' },
                                { field: 'creditNoteDOAApprovalStatusName', header: 'Credit Note DOA Approval Status Name', width: '150px' },
                                { field: 'actionDateAndTime', header: 'Action Date/Time', width: '150px' }
                            
                            ],
                            enableMultiDeleteActionBtn: false,
                            enableAddActionBtn: false,
                            enableExportBtn: false,
                            enableEmailBtn: false,
                            shouldShowFilterActionBtn: false,
                            areCheckboxesRequired: false,
                            isDateWithTimeRequired: true,
                            enableExportCSV: false
                        }
                    ]
                }
            }
    }
    ngOnInit(): void {
         this.getRequiredListData();
    }
    getRequiredListData() {
        let params = new HttpParams().set('InvoiceRefNo', this.config.data.row.transactionRefNo)
        .set('TransactionType', this.config.data.row.transactionType);
        let api = this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_NOTES_VIEW, null, false, params) ;
        api.subscribe((response: IApplicationResponse) => {
             if(response.isSuccess==true && response.statusCode==200 && response.resources){
                this.dataList.push(response.resources);
                this.totalRecords = this.dataList.length;
              //  this.totalRecords = response.resources.totalRecords ? response.resources.totalRecords:0;
             }
         }); 
    }
    btnCloseClick() {
        this.ref.close(false);
    }
    onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
        switch (type) { }
    }
}