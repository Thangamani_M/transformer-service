import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { RequestExtendPaymentArrangementViewComponent } from "./request-extend-payment-arrangement-view.component";

@NgModule({
    declarations: [RequestExtendPaymentArrangementViewComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '',  component: RequestExtendPaymentArrangementViewComponent, data: { title: 'View Request Extend Payment Arrangement' } 
        },
    ])
  ],
  entryComponents: [],
  providers: []
})
export class RequestExtendPaymentArrangementViewModule { }