import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { PaymentArrangementViewComponent } from "./view-payment-arrangement.component";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
@NgModule({
    declarations: [PaymentArrangementViewComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: PaymentArrangementViewComponent, canActivate: [AuthGuard], data: { title: 'View Payment Arrangement' }
        },
    ])
  ],
  entryComponents: [],
  providers: []
})
export class PaymentArrangementViewModule { }
