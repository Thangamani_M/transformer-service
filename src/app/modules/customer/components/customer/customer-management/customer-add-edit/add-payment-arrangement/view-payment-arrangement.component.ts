import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels, COLLECTION_COMPONENT } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
    selector: 'app-view-payment-arrangement',
    templateUrl: './view-payment-arrangement.component.html'
})

export class PaymentArrangementViewComponent {
    primengTableConfigProperties: any;
    loggedUser: any;
    id;
    paDetails;
    selectedTabIndex: any = 0;
    customerId;
    debtorId;
    addressId;
    constructor(private activatedRoute: ActivatedRoute, private router: Router,
        private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>, private snackbarService : SnackbarService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: "View Payment Arrangement",
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Collection Management', relativeRouterUrl: '' }, { displayName: 'Payment Arrangement', relativeRouterUrl: '' }, { displayName: 'View Payment Arrangement', relativeRouterUrl: '' }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: "View Call Wrapup",
                        dataKey: "d",
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                        ],
                        enableAddActionBtn: true,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                    }
                ]
            }
        }
        this.id = this.activatedRoute.snapshot.queryParams.id;
        this.customerId = this.activatedRoute.snapshot.queryParams.id;
        this.debtorId = this.activatedRoute.snapshot.queryParams.debtorId;
        this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    }
    ngOnInit(): void {
      this.combineLatestNgrxStoreData()
        this.getRequiredDetail();
    }
    combineLatestNgrxStoreData() {
      combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)])
        .subscribe((response) => {
          let permission = response[0][COLLECTION_COMPONENT.PAYMENT_ARRANGEMENT]
          if (permission) {
            let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
            this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
          }
        });
    }

    getRequiredDetail() {
        this.crudService.get(
            ModulesBasedApiSuffix.COLLECTIONS,
            CollectionModuleApiSuffixModels.PAYMENT_ARRANGEMENTS_DETAILS, null, false,
            prepareRequiredHttpParams({ paymentArrangementId: this.id })
        ).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200) {
                this.paDetails = data.resources;
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }
    onBreadCrumbClick(breadCrumbItem: object): void {
        if (breadCrumbItem.hasOwnProperty('queryParams')) {
            this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
                { queryParams: { tab: 1 } })
        }
        else {
            this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
                { queryParams: { tab: 1 } });
        }
    }
    navigate() {
        this.router.navigate(["/collection/call-wrap-up-codes/payment-arrangement"]);
    }
    edit() {
      if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        this.router.navigate(["/customer/add-payment-arrangement"], { queryParams: { paymentArrangementId: this.id, campaignId: this.paDetails.campaignId,customerId:this.customerId,debtorId:this.debtorId,addressId:this.addressId } });
    }
}
