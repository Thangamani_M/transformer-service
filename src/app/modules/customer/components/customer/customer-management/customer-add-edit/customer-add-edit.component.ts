import { SelectionModel } from '@angular/cdk/collections';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatAccordion, MatDialog, MatPaginator, MatSort, MatTabChangeEvent } from '@angular/material';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Customer, CustomerAddress, CustomerClassificationConfigModel, CustomerContact, CustomerSubClassificationConfigModel, DebtoreditUpdateFormModel } from '@app/modules/customer/models';
import { AppState } from '@app/reducers';
import {
  agentLoginDataSelector, CommonPaginationConfig, countryCodes, CrudType, CustomDirectiveConfig, DATE_FORMAT_TYPES, debounceTimeForSearchkeyword, ExtensionModalComponent,
  formConfigs, getPDropdownData, HttpCancelService, LoggedInUserModel, ModulesBasedApiSuffix, OtherService, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams,
  setRequiredValidator, sidebarDataSelector$, SnackbarService
} from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog/primeng-custom-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { ModuleName, ResponseMessageTypes } from '@app/shared/enums';
import { AlertService, CrudService, RxjsService, SessionService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { CustomerAddressAddComponent, CustomerKeyholderAddComponent } from '@modules/customer/components';
import { FEATURE_NAME } from '@modules/customer/components/non-customer-profile/shared/enum/index.enum';
import { CustomerPreferenceModel } from '@modules/customer/models/custom-preference-model';
import { KeyHolders } from '@modules/customer/models/keyholder';
import { CustomerVerificationType } from '@modules/customer/shared/enum/customer-verification.enum';
import { DealerModuleApiSuffixModels } from '@modules/dealer/shared/enum/dealer.enum';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { EmailCompType } from '@modules/my-tasks/components/email-dashboard/email-comp-type.enum';
import { loggedInUserData, selectStaticEagerLoadingAppointmentStatusState$, selectStaticEagerLoadingCallbackStatusState$, selectStaticEagerLoadingTicketStatusState$, selectStaticEagerLoadingTitlesState$ } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels, NatureOfDebtor, SalesModuleApiSuffixModels, ScheduleCallbackViewModalComponent, SiteSpecificInputObjectModel } from '@modules/sales';
import { ScheduleCallBackViewModel } from '@modules/sales/models/lead-sms-notification.model';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CustomerManagementService } from '../../../../services/customermanagement.services';
import { AccountAddEditModalComponent } from './account-add-edit-modal/account-add-edit-modal.component';

declare var $;
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
@Component({
  selector: 'app-customer-add-edit',
  templateUrl: './customer-add-edit.component.html',
  styleUrls: ['./customer-add-edit.component.scss']
})

export class CustomerAddEditComponent extends PrimeNgTableVariablesModel implements OnInit {
  customerAddEditForm: FormGroup;
  visibilityPatrolForm: FormGroup;
  customerAddressAddEditForm: FormGroup;
  customerContactAddEditForm: FormGroup;
  customerDebtorForm: FormGroup;
  customerPreferenceForm: FormGroup;
  customerCategoryForm: FormGroup;
  customerSubCategoryForm: FormGroup;
  customerSubCategoryArray: FormArray;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  formConfigs = formConfigs;
  applicationResponse: IApplicationResponse;
  natureOfDebtor = NatureOfDebtor;
  customer = {};
  customerAddress: CustomerAddress[];
  customerContact: CustomerContact[];
  customerKeyholder: KeyHolders[];
  errorMessage: string;
  loggedInUserData: LoggedInUserModel;
  btnName: string;
  agentExtensionNo: string;
  @Output() selectedTabChange: EventEmitter<MatTabChangeEvent>;
  selection = new SelectionModel<Customer>(true, []);
  ddlCustIdFilter = '';
  dataSource = new MatTableDataSource<Customer>();
  dataSourceSchedule = new MatTableDataSource<any>();
  custList = [];
  custAddressList = '';
  newCustAddressList = '';
  addrTypeList = '';
  keyHolderList = '';
  pageSize: number = 10;
  newProvinceNameList;
  dateFromm;
  tmpcustomerName;
  isLoading = true;
  isRelocated = false;
  contactUpdate = false;
  custInfoList;
  pageIndex: number = 0
  limit: number = 5;
  totalLength;
  SearchText: any = { Search: '' };
  relocationIndex;
  observableResponse;
  dataList;
  totalRecords;
  selectedColumns = [];
  selectedRows: string[] = [];
  selectedRow;
  recentActivitiesExpansionPanelIndex = 0;
  columnFilterForm: FormGroup;
  row = {}
  searchColumns;
  searchForm: FormGroup;
  otherParams;
  paymentUnderride: boolean = false;
  paymentOverride: boolean = false;
  invoiceInfo: boolean = false;
  isDealer: boolean = false;
  isProfileActivated: boolean = false;
  debtorInfoObservableResponse;
  debtorInfoDataList;
  customerAddresses = [];
  addressId = "";
  idsObject;
  today = new Date();
  selectedAddressDetails;
  selectedAddressIndex = 0;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAlphaSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaSpecialCharterOnly: true });
  activeStatus = [];
  selectedIndex = 0;
  todayDate = new Date('1984');
  customerPreferenceDetails;
  customerKeyHolderList = [];
  customerStackConfigList = [];
  thirdPartyResponseList = [];
  loggedUser;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isSelected: boolean = true;
  classificationDetails;
  isSubLoading: boolean;
  contractStatus;
  subClassificationDetails;
  activeServices;
  ownershipList = [];
  categoryList = [];
  originList = [];
  installOriginList = [];
  debtorGroupList = [];
  categoryInputEnable = true;
  originInputEnable = true;
  installInputEnable = true;
  debtorGroupEnable = true;
  customerId;
  createdUserId;
  isnavExpand = false;
  startTodayDate = new Date();
  activeStatusList = [
    {
      id: true,
      value: 'Yes'

    },
    {
      id: false,
      value: 'No'

    }
  ];
  isDisableCustomerPreference: boolean = true;
  ticketStatus = [];
  cancelVisibilityPatrol = false;
  visiBilityChecked = false;
  uniqueCallId;
  debtorInfoPrimengTableConfigProperties = {
    tableCaption: "Banking Details",
    selectedTabIndex: 0,
    breadCrumbItems: [],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Banking Details',
          dataKey: 'subscriptionDebtorAccountId',
          enableBreadCrumb: false,
          enableExportCSV: false,
          enableExportExcel: false,
          enableExportCSVSelected: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableStatusActiveAction: false,
          enableFieldsSearch: false,
          rowExpantable: false,
          rowExpantableIndex: 0,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: false,
          cursorSecondLinkIndex: 1,
          columns: [{ field: 'bankName', header: 'Bank Name' },
          { field: 'debitDay', header: 'Debit Day' },
          { field: 'accountNo', header: 'Account Number', width: '200px' },
          { field: 'accountTypeName', header: 'Account Type' },
          { field: 'branch', header: 'Branch' },
          { field: 'bankBranchCode', header: 'Branch Code' },
          { field: '', header: 'View Results' },
          { field: 'isActive', header: 'Status' },
          { field: '', header: 'Action' },
          ],
          moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
          apiSuffixModel: SalesModuleApiSuffixModels.BANKING_ENABLE_DISABLE,
          enableMultiDeleteActionBtn: false,
          enableAddActionBtn: true
        }
      ]
    }
  };
  recentActivitiesTabPrimengTableConfigProperties: any = {
    tableCaption: "Recent Activities",
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Recent Activities', relativeRouterUrl: '' }, { displayName: 'Recent Activities' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'RECENT TICKETS',
          dataKey: 'customerCallbackId',
          dataList: [],
          totalRecords: 0,
          loading: false,
          enableBreadCrumb: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'ticketRefNo', header: 'Ticket Id' },
          { field: 'actionGroup', header: 'Action Group' },
          { field: 'description', header: 'Description' },
          { field: 'createdBy', header: 'Created By' },
          { field: 'createdOn', header: 'Created On', isDateTime: true },
          { field: 'modifiedDate', header: 'Last Update', isDateTime: true },
          { field: 'status', header: 'Status', type: 'dropdown', options: [], placeholder: 'Select Ticket Status' }],
          moduleName: ModulesBasedApiSuffix.SALES,
          apiSuffixModel: CustomerModuleApiSuffixModels.RECENT_CUSTOMER_TICKETS,
          disabled: true
        },
        {
          caption: 'RECENT & OUTSTANDING INVOICES',
          dataKey: 'recentId',
          dataList: [],
          totalRecords: 0,
          loading: false,
          enableBreadCrumb: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableStatusActiveAction: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'transactionNumber', header: 'Transaction Number', width: '200px' },
          { field: 'transactionType', header: 'Transaction Type', width: '200px' },
          { field: 'transactionDescription', header: 'Description', width: '200px' },
          { field: 'referenceNo', header: 'Reference No', width: '200px' },
          { field: 'paymentType', header: 'Payment Type', width: '200px' },
          { field: 'runCode', header: 'Run Code', width: '200px' },
          { field: 'startTotal', header: 'Start Total', width: '200px' },
          { field: 'totals', header: 'Totals', width: '200px' },
          { field: 'transactionDate', header: 'Transaction Date', width: '200px', isDateTime: true }],
          moduleName: ModulesBasedApiSuffix.SALES,
          apiSuffixModel: CustomerModuleApiSuffixModels.RECENT_CUSTOMER_TICKETS,
          disabled: true
        },
        {
          caption: 'POSITIVE EVENTS',
          dataKey: 'eventId',
          dataList: [],
          totalRecords: 0,
          loading: false,
          enableBreadCrumb: false,
          enableStatusActiveAction: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'alarmTypeConfigName', header: 'Alarm Type Name', width: '200px' }, { field: 'alarmTypeConfigDivisions', header: 'Division', width: '200px' },
          { field: 'alarmTypeConfigList', header: 'Alarm Type List', width: '200px' }, { field: 'description', header: 'Description', width: '200px' }, { field: 'createdUserName', header: 'Created By', width: '200px' },
          { field: 'createdDate', header: 'Created On', width: '200px', isDateTime: true }, { field: 'isActive', header: 'Status', width: '200px' }],
          moduleName: ModulesBasedApiSuffix.SALES,
          apiSuffixModel: CustomerModuleApiSuffixModels.RECENT_CUSTOMER_TICKETS,
          disabled: true
        },
        {
          caption: 'OPEN APPOINTMENTS',
          dataKey: 'appointmentId',
          dataList: [],
          totalRecords: 0,
          loading: false,
          enableBreadCrumb: false,
          reorderableColumns: false,
          enableStatusActiveAction: false,
          resizableColumns: false,
          enableScrollable: true,
          enableFieldsSearch: true,
          enableHyperLink: false,
          columns: [{ field: 'appointmentRefNo', header: 'Appointment ID' },
          { field: 'appointmentDate', header: 'Date', isDateTime: true },
          { field: 'appointmentTime', header: 'Time Slot' },
          { field: 'sellerName', header: 'Seller' },
          { field: 'appointmentStatusName', header: 'Status', type: 'dropdown', options: [], placeholder: 'Select Appointment Status' }],
          shouldShowCreateActionBtn: false,
          moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
          apiSuffixModel: CustomerModuleApiSuffixModels.CUSTOMER_APPPOINTMENTS,
          disabled: true
        },
        {
          caption: 'DEBTOR AGING',
          dataKey: 'agingId',
          dataList: [],
          totalRecords: 0,
          loading: false,
          enableBreadCrumb: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'propertyAccessName', header: 'Property Access Name', width: '200px' },
          { field: 'propertyAccessDivisions', header: 'Division', width: '200px' },
          { field: 'propertyAccessReasonLists', header: 'Property Access Reason', width: '200px' },
          { field: 'description', header: 'Description', width: '200px' },
          { field: 'createdUserName', header: 'Created By', width: '200px' }, { field: 'createdDate', header: 'Created On', width: '200px', isDateTime: true }, { field: 'isActive', header: 'Status', width: '200px' }],
          moduleName: ModulesBasedApiSuffix.SALES,
          apiSuffixModel: CustomerModuleApiSuffixModels.RECENT_CUSTOMER_TICKETS,
          disabled: true
        },
        {
          caption: 'VIEW SCHEDULED CALLBACK',
          dataKey: 'customerCallbackId',
          dataList: [],
          loading: false,
          enableBreadCrumb: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          shouldShowCreateActionBtn: false,
          enableStatusActiveAction: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'callbackRefNo', header: 'Callback ID' },
          { field: 'callbackDateTime', header: 'Callback Timing', isDateTime: true },
          { field: 'callbackQueueName', header: 'Queue Name' },
          { field: 'customerRefNo', header: 'Customer Number' },
          { field: 'callbackStatusName', header: 'Status', type: 'dropdown', options: [], placeholder: 'Select Callback Status' }],
          moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
          apiSuffixModel: SalesModuleApiSuffixModels.SCHEDULE_CALLBACK_CUSTOMERS,
          disabled: true
        }
      ]
    }
  }
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  customerAddressList;
  subscriptionsList;
  detailsObj;
  titleList;
  subscriptionValue;
  editDebtor: boolean = true;
  editClassifications: boolean = false;
  debtorAddress: string = '';
  subscription: string = '';
  natureDebtor: string = '';
  debtorType;
  selectedMonitoringIndex = 0;
  contactTypeList;
  paymentMethodList;
  visibilityPatrolData;
  cancelledReason;
  occurrenceBookId;
  signalData;
  title;
  emailComptype: string = EmailCompType.CUSTOMER;
  feature = "";
  featureIndex: number;
  isHideDebtorContractAndEditButton = false;
  isEnableProfileInformation = false
  callbackStatuses = [];
  contactsForSiteAddressInfoTab = [];
  selectedContractId = '';
  isFormSubmitted = false;
  selectedDebtorInfoIndex = 0;
  @ViewChild('recentActivitiesAccordion', { static: false }) recentActivitiesAccordion: MatAccordion;
  @ViewChild('salesAccordion', { static: false }) salesAccordion: MatAccordion;
  salesExpansionPanelIndex;
  customerProfile: any;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{ disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true },
      { quickAction: [], disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }]
    }
  }
  primengTableConfigPropertiesObjMonitering: any = {
    tableComponentConfigs: {
      tabsList: [{ disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true },
      { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true },
      { disabled: true }, { disabled: true }, { disabled: true }, { disabled: true }]
    }
  }
  primengTableConfigPropertiesDebtorObj: any = {
    tableComponentConfigs: {
      tabsList: [{ disabled: true }, { disabled: true }, { disabled: true }]
    }
  }
  primengTableConfigPropertiesLeadsObj: any = {
    tableComponentConfigs: {
      tabsList: [{}]
    }
  }
  siteAddressInfoPermission = {}
  DATE_FORMAT_TYPES = DATE_FORMAT_TYPES;
  recentActivitiesDetails: any = {};
  recentActivitiesInfo: any = {};
  recentActivitiesTicketNotes: any = {};

  constructor(private crudService: CrudService, private snackbarService: SnackbarService, private momentService: MomentService,
    private customerManagementService: CustomerManagementService, private tableFilterFormService: TableFilterFormService,
    private dialog: MatDialog, private _fb: FormBuilder, public dialogService: DialogService, private otherService: OtherService,
    private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>,
    private alertService: AlertService, private rxjsService: RxjsService, private httpCancelService: HttpCancelService, private sessionService: SessionService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.combineLatestNgrxStoreDataOne();
    this.activatedRoute.paramMap.subscribe(res => {
      if (res) {
        this.customerId = res ? res.get('id') : '';
        this.customer['customerId'] = this.customerId;
        this.rxjsService.setCustomerDate(this.customerId);
        this.combineLatestNgrxStoreData();
        this.getCustomerInfo(this.customerId);
      }
    })
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
    this.onRecentActivitiesPatchValue();
  }

  changeMethodInfo = false;
  paymentMethodId;
  confirmationAlert = false;
  requestDiscount = false;
  statementFee = '';
  changePaymentMethod(e) {
    if (this.paymentMethodId == 1) {
      this.confirmationAlert = true;
    } else {
      this.selectedIndex = 0;
      this.createBankingDetails(null, 'do');
    }
  }

  confirmationAlertClick() {
    this.selectedIndex = 2;
    this.confirmationAlert = false;
  }

  cancelPreStatement() {
    this.confirmationAlert = false;
  }

  cancelRequestDiscount() {
    this.requestDiscount = false;
  }

  requestDiscountClick() {
    if (!this.primengTableConfigPropertiesDebtorObj.tableComponentConfigs.tabsList[2].canRequestDiscount) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.requestDiscount = true;
  }

  raiseRequest() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    let obj = {
      "debtorId": this.detailsObj.debtorId,
      "customerId": this.customerId,
      "statementFeeConfigId": this.paymentMethodList?.statementFeeConfigId,
      "createdUserId": this.loggedUser.userId,
      "statementFeeStatusId": 0,
      "contractId": this.subscription,
      "districtId": null
    }

    this.crudService.create(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.CHANGING_PAYMENT_METHOD, obj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.requestDiscount = false;
          this.changeDebtorType();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  goToStatementFee() {
    let obj = {
      "debtorAdditionalInfoId": this.detailsObj.debtorAdditionalInfoId,
      "customerId": this.customerId,
      "debtorStatementFeeId": this.paymentMethodList.statementFeeConfigId,
      "modifiedUserId": this.loggedUser.userId,
      "modifiedDate": new Date()
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.STATEMENT_FEE_SAVE, obj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getContractList() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.UX_CONTRACT_TYPES, undefined, false, prepareRequiredHttpParams({
      customerId: this.customerId,
      addressId: this.addressId
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.contactTypeList = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getContractsForSiteAddressInfoTab() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.UX_CONTRACTS_CUSTOMERS, undefined, false, prepareRequiredHttpParams({
      customerId: this.customerId,
      addressId: this.addressId
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.contactsForSiteAddressInfoTab = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getChangigMethodList() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_PAYMENT_CHANGE_METHOD
      , undefined, false, null).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess && response.statusCode === 200) {
          this.paymentMethodList = response.resources;
          this.statementFee = this.paymentMethodList?.statementFee;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getAddressData() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      SalesModuleApiSuffixModels.CUSTOMER_ADDRESS_INFOLIST, undefined, false, prepareRequiredHttpParams({
        customerId: this.customerId
      })).subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.customerAddressList = response.resources;
          if (this.activatedRoute.snapshot.queryParams.addressId) {
            this.addressId = this.activatedRoute.snapshot.queryParams.addressId
          } else {
            this.addressId = response.resources?.addresses?.length > 0 ? response.resources.addresses[0].addressId : null;
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getAfterAddressData() {
    this.rxjsService.getTabIndexForCustomerComponent().subscribe((selectedTabIndex: number) => {
      if (selectedTabIndex) {
        this.onSequenceChangeEvent({ index: selectedTabIndex }, false);
      }
    });
    this.rxjsService.getContactUpdateDetails().subscribe((contact: boolean) => {
      this.contactUpdate = contact;
      if (this.contactUpdate) {
        this.getCustomerInfo(this.customerId);
      }
    });
    this.rxjsService.getRelocation().subscribe((isRelocated: boolean) => {
      this.isRelocated = isRelocated;
    });
  }

  onLoadMonitoringResponseTab() {
    this.rxjsService.getTabIndexForMonitoringAndResponseComponent().subscribe((selectedMonitoringIndex: number) => {
      if (selectedMonitoringIndex != 0 && this.selectedTabIndex == 4) {
        this.onMonitoringTabChangeEvent({ index: selectedMonitoringIndex });
      }
    });
  }

  viewTransactions() {
    let queryParams = { id: this.customerId };
    if (this.feature && this.featureIndex) {
      queryParams['feature_name'] = this.feature;
      queryParams['featureIndex'] = this.featureIndex;
    }
    this.router.navigate(["customer/manage-customers/view-transactions"], { queryParams: queryParams, skipLocationChange: true });
  }

  changeDebtorType() {
    if (this.feature == FEATURE_NAME.SUSPENDED_ACCOUNT) {
      return this.getCustomerDebtorForSuspendAccountProfile()
    }
    if (this.debtorType == '1') {
      this.customerDebtorForm.get('companyName').setValidators([Validators.required])
      this.customerDebtorForm.get('companyName').updateValueAndValidity();
      this.customerDebtorForm.get('companyRegNo').setValidators([Validators.required])
      this.customerDebtorForm.get('companyRegNo').updateValueAndValidity();
      this.customerDebtorForm.get('said').clearValidators();
      this.customerDebtorForm.get('said').updateValueAndValidity();
    } else {
      this.customerDebtorForm.get('said').setValidators([Validators.required])
      this.customerDebtorForm.get('said').updateValueAndValidity();
      this.customerDebtorForm.get('companyName').clearValidators();
      this.customerDebtorForm.get('companyName').updateValueAndValidity();
      this.customerDebtorForm.get('companyRegNo').clearValidators();
      this.customerDebtorForm.get('companyRegNo').updateValueAndValidity();
    }

    if (this.subscription) {
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        SalesModuleApiSuffixModels.CUSTOMER_DETAILS_BY_DEBTOR, undefined, false, prepareRequiredHttpParams({
          contractId: this.subscription,
        })).subscribe((response: IApplicationResponse) => {
          if (response.resources) {
            this.detailsObj = response.resources;
            this.natureDebtor = this.detailsObj.natureOfDebtor
            this.debtorType = this.detailsObj.debtorTypeId
            this.paymentMethodId = this.detailsObj.paymentMethodId
            this.detailsObj.fullName = this.detailsObj.firstName + ' ' + this.detailsObj.lastName;
            if (!this.editDebtor) {
              this.customerDebtorForm.patchValue(this.detailsObj);
              this.customerDebtorForm.patchValue(this.detailsObj.debtorAgingInfo);
            }
            this.getCreditControllerByDebtorAndCustomerContract();
            this.debtorInfoDataList = this.detailsObj.debtorBankingDetailList;
            this.debtorInfoDataList.map(element => {
              element.originalAccountNumber = element.accountNo;
              if (element.accountNo) {
                let len = element.accountNo.length - 4;
                let first = element.accountNo.substring(0, len);
                let last = element.accountNo.substring(len, element.accountNo.length);
                let str = ''
                for (let i = 0; i < first.length; i++) {
                  str = str + '*';
                }
                element.accountNo = str + '' + last;
                element.accountNoHashed = str + '' + last;
                element.isShowAccountNumber = false
              }
            });
            this.totalRecords = this.debtorInfoDataList.length;
          } else {
            this.detailsObj = null;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else {
      setTimeout(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }, 500)
    }


  }
  getCreditControllerByDebtorAndCustomerContract() {
    let obj = { customerId: this.customerId, debtorId: this.detailsObj.debtorId, contractId: this.subscription }
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_CREDIT_CONTROL_ASSIGN_DEBTOR_DETAIL, undefined, false, prepareRequiredHttpParams(obj)).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode == 200 && response.resources != "Record not found") {
        this.detailsObj.creditControllerCode = response.resources?.creditControllerCode
        this.detailsObj.creditControllerName = response.resources?.displayName
        this.detailsObj.creditcontrollerTypeName = response.resources?.creditcontrollerTypeName;
        this.customerDebtorForm.get("creditControllerCode").setValue(this.detailsObj?.creditControllerCode)
        this.customerDebtorForm.get("creditControllerName").setValue(this.detailsObj?.creditControllerName)
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  getCustomerDebtorForSuspendAccountProfile() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.SUSPENDED_ACCOUNT_CUSTOMER_DEBTOR, undefined, false, prepareRequiredHttpParams({
        customerId: this.customerId,
        addressId: this.addressId
      })).subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.detailsObj = response.resources;
          this.natureDebtor = this.detailsObj.natureOfDebtor
          this.debtorType = this.detailsObj.debtorTypeId
          this.paymentMethodId = this.detailsObj.paymentMethodId
          this.detailsObj.fullName = this.detailsObj.firstName + ' ' + this.detailsObj.lastName;
        }
        this.rxjsService.setGlobalLoaderProperty(false);

      })

  }

  ngOnInit(): void {
    console.log("REFRESHIG ONINIT")

    this.combineLatestNgrxStoreDataOne();
    history.pushState(null, '');
    if (this.selectedTabIndex == 0) {
      this.combineLatestNgrxStoreDataForAppointmentStatus();
    }
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
    if (this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex]?.columns) {
      this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    }
    this.columnFilterRequest();
    this.relocationIndex = this.activatedRoute.snapshot.queryParams.index;
    this.rxjsService.getGlobalLoaderProperty().subscribe((isLoading: boolean) => {
      this.isLoading = isLoading;
    });
    this.rxjsService.getLeadNotesUpdateStatus()
      .subscribe(status => {
        if (status !== false && this.recentActivitiesExpansionPanelIndex == 5) {
          this.scheduleCallbackopen();
        }
      });
    this.createCustomerDebtorForm();
    this.createSubCategoryForm();
    this.getAfterAddressData();
    this.title = "Tickets";
    if (this.typeInNumber != undefined && this.typeInNumber < 3) {
      if (this.typeInNumber == 1) {
        this.title = "Technical Tickets"
      } else if (this.typeInNumber == 2) {
        this.title = "Sales Tickets"
      }
    }
    this.getChangigMethodList();
    setTimeout(() => {
      let selectedContract = JSON.parse(this.sessionService.getItem('selectedContract'));
      if (Object.keys(selectedContract ? selectedContract : {})?.length > 0) {
        this.onSequenceChangeEvent({ index: 7 }, false);
        this.selectedContractId = selectedContract?.contractId;
        this.onAgreementTabClicked(JSON.parse(this.sessionService.getItem('agreementTab')), this.sessionService.getItem('index'), true)
        this.sessionService.removeItem('agreementTab')
        this.sessionService.removeItem('index')
        if (!this.otherService.previousUrl.includes('customer-verification')) {
          this.sessionService.removeItem('selectedContract');
        }
      }
    }, 5000);

  }

  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(sidebarDataSelector$),
    ])
      .subscribe((response: any) => {

        let customerManagemnt = response[0]?.['menuAccess'].find(item => item.menuName == "Customer Management");
        if (!customerManagemnt) return "";

        let customers = customerManagemnt['subMenu']?.find(item => item.menuName == "Customers");
        let permissions = customers?.subMenu
        if (permissions) {
          let customerManagement = permissions?.find(item => item.menuName == "Customer Dashboard")
          if (customerManagement) {
            let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, customerManagement['subMenu']);
            this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
            customerManagement['subMenu']?.forEach((item, index) => {
              this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[+item.sortOrder - 1]['actions'] = item
            })
            // Debtor
            let debtor = this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[10]['actions']['subMenu'];
            let tabsForDebtor = debtor?.find(item => item.menuName == "Tabs")
            let primengTableConfigPropertiesDebtorObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesDebtorObj, tabsForDebtor['subMenu']);
            this.primengTableConfigPropertiesDebtorObj = primengTableConfigPropertiesDebtorObj['primengTableConfigProperties'];
            //Site Address
            let siteAddress = this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[7]['actions']['subMenu'];
            let siteAddressQuickAction = siteAddress?.find(item => item.menuName == "Quick Action")
            //RECENT ACTIVITIES
            let recentActivitiesTabPrimengTableConfigProperties = prepareDynamicTableTabsFromPermissions(this.recentActivitiesTabPrimengTableConfigProperties, this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[0]['actions']['subMenu']);
            this.recentActivitiesTabPrimengTableConfigProperties = recentActivitiesTabPrimengTableConfigProperties['primengTableConfigProperties'];
            // Leads
            let leads = this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[2]['actions']['subMenu']?.find(item => item.menuName == 'Leads')
            let primengTableConfigPropertiesLeadsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesLeadsObj, leads['subMenu']);
            this.primengTableConfigPropertiesLeadsObj = primengTableConfigPropertiesLeadsObj['primengTableConfigProperties'];

            // MONITORING & RESPONSE
            let primengTableConfigPropertiesObjMonitering = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObjMonitering, this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[4]['actions']['subMenu']);
            this.primengTableConfigPropertiesObjMonitering = primengTableConfigPropertiesObjMonitering['primengTableConfigProperties'];
            // MONITORING & RESPONSE - Actions
            this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[4]?.actions?.subMenu?.map(item => {
              this.primengTableConfigPropertiesObjMonitering.tableComponentConfigs.tabsList[+item.sortOrder - 1]['actions'] = item
            })
            console.log("this.primengTableConfigPropertiesObjMonitering", this.primengTableConfigPropertiesObjMonitering)
            siteAddressQuickAction?.subMenu.forEach(menu => {
              let permissions = {
                canCreate: false,
                canEdit: false,
                canRowDelete: false,
              }
              menu?.subMenu.forEach(subM => {
                if (subM.menuName == "Add") {
                  permissions.canCreate = true
                }
                if (subM.menuName == "Edit") {
                  permissions.canEdit = true
                }
                if (subM.menuName == "Delete") {
                  permissions.canRowDelete = true
                }
              })
              this.siteAddressInfoPermission[menu.menuName] = permissions;
            })
            this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[7]['quickAction'] = siteAddressQuickAction?.subMenu || []
          }
        }
      });
  }

  combineLatestNgrxStoreDataForAppointmentStatus(): void {
    combineLatest([this.store.select(selectStaticEagerLoadingAppointmentStatusState$)]
    ).subscribe((response) => {
      if (response[0]) {
        this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[3].columns[4].options = response[0] = response[0].map(item => {
          return { label: item.displayName, value: item.displayName };
        });
      }
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.getCustomerDetailsById(this.customerId);
    }, 5000);
  }

  countryCodes = countryCodes;
  createCustomerDebtorForm() {
    let debtoreditUpdateFormModel = new DebtoreditUpdateFormModel();
    this.customerDebtorForm = this._fb.group({
    });
    Object.keys(debtoreditUpdateFormModel).forEach((key) => {
      this.customerDebtorForm.addControl(key, new FormControl(debtoreditUpdateFormModel[key]));
    });
    this.customerDebtorForm = setRequiredValidator(this.customerDebtorForm, ["email", "firstName", "titleId", "lastName", "mobile1", "companyName", "companyRegNo", "said"]);
  }

  createCustomerPreferenceForm() {
    let customerPreferenceModel = new CustomerPreferenceModel();
    this.customerPreferenceForm = this._fb.group({
      virtualAgentServicePin: [null, [Validators.required, Validators.min(10), Validators.max(99999999)]],
    });
    Object.keys(customerPreferenceModel).forEach((key) => {
      this.customerPreferenceForm.addControl(key, new FormControl({ value: customerPreferenceModel[key], disabled: true }));
    });
    this.customerPreferenceForm.controls['thirdPartyId'].disable();
    this.customerPreferenceForm.controls['thirdPartyCustomerCode'].disable();
    this.customerPreferenceForm.get('createdUserId').setValue(this.loggedUser.userId);
  }

  changeAddress(type = 'default') {
    if (type == 'changeAddress') {
      this.selectedAddressDetails = this.customerAddresses?.find(cA => cA['addressId'] == this.addressId);
      if (this.selectedAddressDetails?.partitionDetails?.length > 0) {
        this.partitionId = this.selectedAddressDetails?.partitionDetails?.find(pD => pD['isPrimary']) ?
          this.selectedAddressDetails.partitionDetails?.find(pD => pD['isPrimary']).id : this.selectedAddressDetails?.partitionDetails[0]?.id;
      }
      this.selectedAddressIndex = this.customerAddresses?.findIndex(cA => cA['addressId'] == this.addressId);
      this.rxjsService.setCustomerPartitionId(this.partitionId ? this.partitionId : null);
      this.idsObject = JSON.stringify({ addressId: this.addressId, customerId: this.custInfoList?.customerId });
      this.rxjsService.setCustomerAddresId(this.addressId);
      this.getContractList();
      this.getBOSContracts();
      this.getAgreementTabsForCustomer();
      this.subscription = '';
      this.navigateViewCustomerWithoutReload();
      this.custInfoList.siteId = this.selectedAddressDetails?.siteId;
      this.custInfoList.createdDate = this.selectedAddressDetails?.createdDate;
      this.getCustomerDetailsById(this.customerId)
      if (this.selectedTabIndex == 11) {
        this.getCustomerclassificationapi();
        this.getCustomerSubClassification();
      }
      if (this.selectedTabIndex == 4 && this.selectedMonitoringIndex == 4) {
        this.getCustomerPreferenceDetailsById();
      }
    }
  }

  getCustomerclassificationapi() {
    forkJoin([this.getCustomerClassification()])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
            switch (ix) {
              case 0:
                if (respObj.resources.customerClassificationMappingId) {
                  this.isCategaryAvailable = false;
                } else {
                  this.isCategaryAvailable = true;
                }
                if (respObj.message === 'Record not found') {
                  this.isCategaryAvailable = true;
                  this.editClassifications = true;
                } else {
                  this.classificationDetails = respObj.resources;
                  if (respObj?.resources && this.customerCategoryForm) {
                    this.customerCategoryForm.patchValue(respObj.resources);
                  }
                }
                if (!this.isDealer && this.isProfileActivated) {
                  this.getCustomerSubClassification();
                }
                break;
            }
            if (ix == response.length - 1) {
              this.getForkJoinRequestsTwoForClassificationsTab();
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      });
  }

  onChangePartition(event) {
    this.rxjsService.setCustomerPartitionId(event);
    this.getAgreementTabsForCustomer();
    if (this.selectedTabIndex == 4 && this.selectedMonitoringIndex == 4) {
      this.getCustomerPreferenceDetailsById();
    }
  }

  loadPaginationLazy(event): void {
    this.otherParams = {};
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.onCRUDRequested(CrudType.GET, row);
  }

  combineLatestNgrxStoreData(): void {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(selectStaticEagerLoadingTitlesState$),
      this.activatedRoute.queryParams,
      this.store.select(selectStaticEagerLoadingTicketStatusState$),
      this.store.select(selectStaticEagerLoadingCallbackStatusState$)]
    ).pipe(take(1)).subscribe((response) => {
      console.log("REFRESHIG COMB")

      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.titleList = response[1];
      if (Object.keys(response[2]).length) {
        this.addressId = response[2]?.addressId ? response[2]?.addressId : '';
        this.selectedTabIndex = response[2]?.navigateTab?.toString() ? +response[2]?.navigateTab : 0;
        this.selectedMonitoringIndex = response[2]?.monitoringTab?.toString() ? +response[2]?.monitoringTab : 0;
        this.feature = response[2]?.feature_name ? response[2]?.feature_name : '';
        this.featureIndex = response[2]?.featureIndex?.toString() ? +response[2]?.featureIndex : 0;
        if (this.feature == FEATURE_NAME.SUSPENDED_ACCOUNT) {
          this.isHideDebtorContractAndEditButton = true
        }
        if (this.feature == FEATURE_NAME.CAMERA_PROFILE || this.feature == FEATURE_NAME.COMMUNITY_PROFILE || this.feature == FEATURE_NAME.HUB_PROFILE || this.feature == FEATURE_NAME.EXECU_GUARD_PROFILE || this.feature == FEATURE_NAME.REPEATER_PROFILE) {
          this.isEnableProfileInformation = true;
        }
        this.onSequenceChangeEvent({ index: 0 }, false);
        setTimeout(() => {
          this.onSequenceChangeEvent({ index: +response[2]?.navigateTab }, false);
        }, 0);
      }
      this.ticketStatus = response[3];
      if (response[3]) {
        this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[0].columns[6].options = response[3] = response[3].map(item => {
          return { label: item.displayName, value: item.id };
        });
      }
      this.callbackStatuses = response[4];
      if (response[4]) {
        this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[5].columns[4].options = response[4] = response[4].map(item => {
          return { label: item.displayName, value: item.displayName };
        });
      }
    });
  }

  getModelbasedKeyToFindId(obj: object): string {
    for (let key of Object.keys(obj)) {
      if (key.toLowerCase().includes("id")) {
        return key;
      }
    }
  }

  submitDebtor() {
    if (this.customerDebtorForm.invalid) {
      return;
    }
    let obj = this.customerDebtorForm.value;
    obj.modifiedUserId = this.loggedInUserData.userId;
    obj.debtorTypeId = this.debtorType;
    obj.contractId = this.subscription;
    obj.paymentMethodId = this.paymentMethodId;
    if (typeof obj.mobile1 === "string") {
      obj.mobile1 = obj.mobile1.split(" ").join("")
      obj.mobile1 = obj.mobile1.replace(/"/g, "");
    }
    if (typeof obj.mobile2 === "string") {
      obj.mobile2 = obj.mobile2.split(" ").join("")
      obj.mobile2 = obj.mobile2.replace(/"/g, "");
    }
    if (!obj.mobile2) {
      obj.mobile2CountryCode = '';
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.CUSTOMER_DEBTOR_UPDATE, this.customerDebtorForm.value).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.onCustomerVerficationCheck(CustomerVerificationType.DEBTOR_INFO);
      }
    });
  }

  editDebtorInfo() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.onCustomerVerficationCheck(CustomerVerificationType.DEBTOR_INFO);
  }

  editCustomerPreference() {
    if (!this.primengTableConfigPropertiesObjMonitering.tableComponentConfigs.tabsList[4]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.onCustomerVerficationCheck(CustomerVerificationType.CUSTOMER_PREFERENCE);
  }

  resetCustomerPerference() {
    if (!this.primengTableConfigPropertiesObjMonitering.tableComponentConfigs.tabsList[4]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.customerPreferenceForm.reset();
  }

  onCustomerVerficationCheck(tab, selectArg?: any, index?: any) {
    if (this.activatedRoute?.snapshot?.queryParams?.feature_name) {
      this.onAfterCustomerVerificationCheck(tab, selectArg, index);
    } else {
      this.rxjsService.setGlobalLoaderProperty(true);
      let filterObj = { userId: this.loggedInUserData?.userId, customerId: this.customerId, siteAddressId: this.addressId, uniqueCallId: this.uniqueCallId ? this.uniqueCallId : null };
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CHECK, null, false,
        prepareRequiredHttpParams(filterObj))
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.setAgreementTabActive(selectArg, index);
            this.onAfterCustomerVerificationCheck(tab, selectArg, index);
          } else {
            this.setAgreementTabActive(selectArg, index);
            let queryParams = { customerId: this.customerId, siteAddressId: this.addressId, tab: tab, customerTab: this.selectedTabIndex, monitoringTab: this.selectedMonitoringIndex, };
            if (this.feature && this.featureIndex) {
              queryParams['feature_name'] = this.feature;
              queryParams['featureIndex'] = this.featureIndex;
            }
            this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: queryParams, skipLocationChange: true });
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  onAfterCustomerVerificationCheck(tab: number, selectArg?: any, index?: any) {
    switch (tab) {
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 8:
      case 9:
        this.onSiteAddressQuickAction(selectArg, index);
        break;
      case 10:
        this.changeDebtorType();
        this.editDebtor = !this.editDebtor;
        break;
      case 18:
        this.onAfterCustomerPreference();
        break;
      case 7:
        this.snackbarService.openSnackbar("Customer Verified Already Successfully", ResponseMessageTypes.SUCCESS)
        break;
    }
  }

  onAfterCustomerPreference() {
    this.isDisableCustomerPreference = false;
    this.customerPreferenceForm.enable();
  }

  onSiteAddressQuickAction(selectArg: any, index: any) {
    this.onAfterAgreementTabClicked(selectArg, index);
  }

  editClassificationInfo() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.editClassifications = !this.editClassifications;
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.otherParams = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  closeNav() {
    document.getElementById("quickaction_sidebar").style.width = "0";
    $('.main-container').css({ width: '100%' });
    $('#quickaction_sidebar .quick-action-menu-con').css({ right: '0', visibility: 'hidden' });
  }

  openNav() {
    if (this.isnavExpand) {
      document.getElementById("quickaction_sidebar").style.width = "250px";
      $('.main-container').css({ width: 'calc(100% - 250px)' });
      $('#quickaction_sidebar .quick-action-menu-con').css({ right: '12px', visibility: 'visible' });
    } else {
      document.getElementById("quickaction_sidebar").style.width = "100px";
      $('.main-container').css({ width: 'calc(100% - 250px)' });
      $('#quickaction_sidebar .quick-action-menu-con').css({ right: '12px', visibility: 'visible' });
    }
    this.isnavExpand = !this.isnavExpand;
  }

  onCRUDRequested(type: CrudType | string, row?: object): void {
    let otherParams = {};
    if (this.otherParams) {
      Object.keys(this.otherParams).forEach((key) => {
        if (key.toLowerCase().includes('date') || key.toLowerCase().includes('createdon')) {
          otherParams[key] = this.momentService.localToUTC(this.otherParams[key]);
        }
        else if (key == 'status') {
          otherParams[key] = row['searchColumns'][key]['value']['id'];
        }
        else {
          otherParams[key] = this.otherParams[key];
        }
      });
    }
    if (CrudType.GET === type && Object.keys(row).length > 0) {
      Object.keys(row['searchColumns']).forEach((key) => {
        if ((key.toLowerCase().includes('date') && key !== 'callbackDateTime') || key.toLowerCase().includes('createdon')) {
          otherParams[key] = this.momentService.localToUTC(row['searchColumns'][key] ?
            row['searchColumns'][key] : this.row?.['searchColumns']?.[key]);
        }
        else {
          otherParams[key] = row['searchColumns'][key]['value'];
        }
      });
      if (row['sortOrder']) {
        otherParams['sortOrder'] = row['sortOrder'];
      }
      if (row['sortOrderColumn']) {
        otherParams['sortOrderColumn'] = row['sortOrderColumn'];
      }
    }
    let pageIndex, maximumRows;
    if (!row['maximumRows']) {
      maximumRows = CommonPaginationConfig.defaultPageSize;
      pageIndex = CommonPaginationConfig.defaultPageIndex;
    }
    else {
      maximumRows = row["maximumRows"];
      pageIndex = row["pageIndex"];
    }
    delete row['maximumRows'] && row['maximumRows'];
    delete row['pageIndex'] && row['pageIndex'];
    delete row['searchColumns'];
    if (this.callbackDateTime) {
      if (otherParams['callbackDateTime']) {
        otherParams['callbackDateTime'] = this.momentService.localToUTC(this.callbackDateTime);
      }
      else {
        row['callbackDateTime'] = this.momentService.localToUTC(this.callbackDateTime);
      }
    }
    switch (type) {
      case CrudType.EDIT:
        switch (this.selectedTabIndex) {
          //DEBTOR INFO - Bank Details
          case 10:
            row['isEdit'] = true;
            if (row['isActive'] == true) {
              this.createBankingDetails(row);
            }
            break;
        }
        break;
    }
  }

  //==============LISTING OUT CUSTOMER DETAILS INFO====================//
  //===========CUSTOMER INFO============//
  getCustomerInfo(custId) {
    this.customerManagementService.getCustomer(custId).subscribe(resp => {
      if (resp.isSuccess && resp?.statusCode == 200) {
        this.customerAddresses = resp.resources.addresses;
        if (this.activatedRoute.snapshot.queryParams.addressId) {
          this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
        } else {
          this.addressId = resp.resources.addresses.length > 0 ? resp.resources.addresses[0].addressId : null;
        }
        this.selectedAddressDetails = this.customerAddresses.find(cA => cA['addressId'] == this.addressId);
        this.applicationResponse = resp;
        this.customer = this.applicationResponse.resources;
        this.custList = this.applicationResponse.resources;
        let customerList;
        customerList = this.custList;
        if (customerList.isProfileActivated)
          this.isProfileActivated = customerList.isProfileActivated ? true : false;

        if (customerList.customerProfileTypeName == 'Dealers Profile') {
          this.isDealer = true;
        }
        let customerData = {
          customerId: this.customer['customerId'],
          customerRefNo: this.customer['customerRefNo'],
          contactPerson: this.customer['customerName'],
          contactNumber: this.customer['mobileNumber1'],
          status: this.customer['status'],
          firstName: this.customer['firstName'],
          lastName: this.customer['lastName'],
          titleId: this.customer['titleId'],
        }
        this.rxjsService.setCustomerDate(customerData);
        this.custInfoList = this.applicationResponse.resources;
        this.customerProfile = this.custInfoList?.customerProfileTypeName
        this.rxjsService.setCustomerProfile(this.customerProfile)
        this.idsObject = JSON.stringify({ addressId: this.addressId, customerId: resp.resources.customerId });
        this.changeAddress('changeAddress');
        this.tmpcustomerName = this.custList["title"] + ". " + this.custList["firstName"] + " " + this.custList["lastName"];
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getCustomerDetailsById(custId) {
    this.customerManagementService.getCustomer(custId).subscribe(resp => {
      if (resp?.isSuccess && resp?.statusCode == 200) {
        let customerList;
        customerList = resp.resources;
        if (customerList.isProfileActivated)
          this.isProfileActivated = customerList.isProfileActivated ? true : false;
        if (customerList.customerProfileTypeName == 'Dealers Profile') {
          this.isDealer = true;
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  //=======================================================================//
  //Areas of opening and closing Add Address and Add Info Dialog....
  openAddCustomerAddressDialog() {
    this.OpenDialogWindow(this.customerId);
  }

  private OpenDialogWindow(id) {
    const dialogReff = this.dialog.open(CustomerAddressAddComponent, { width: '600px', data: { Id: id, Name: "Create" }, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
      this.selection.clear();
    });
  }

  showDebitDay: boolean = false;
  createBankingDetails(rowData?, str?) {
    if (this.detailsObj) {
      this.detailsObj.contractId = this.subscription;
      if (str == 'do') {
        this.detailsObj.isDoConfig = true;
      } else {
        this.detailsObj.isDoConfig = false;
      }
      this.showDebitDay = true;
      this.detailsObj.customerId = this.customerId;
      this.detailsObj.showDebitDay = this.showDebitDay;
      this.detailsObj.debtorTypeId = this.debtorType;
      this.detailsObj.isEdit = rowData?.isEdit
      this.detailsObj.debtorAccountDetailId = rowData ? rowData.debtorAccountDetailId : '';
      const dialogReff = this.dialog.open(AccountAddEditModalComponent, { width: '750px', data: this.detailsObj, disableClose: true });
      dialogReff.afterClosed().subscribe(result => {
        this.changeDebtorType();
      });
    }
  }

  openCustomerAddressforUpdate(custAddId) {
    const dialogReff = this.dialog.open(CustomerAddressAddComponent, { width: '600px', data: { Id: custAddId, CustId: this.customerId, Name: "Update" }, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
      this.selection.clear();
    });
  }

  openCustomerAddressforDelete(custAddId) {
    this.customerManagementService.DeleteCustomerAddress(custAddId).subscribe({
      next: response => this.onDeleteComplete(response),
      error: err => this.errorMessage = err
    });
    this.ngOnInit();
  }

  OpenAddCustomerKeyholderDialog(): void {
    this.OpenAddCustomerContactDialog(this.customerId);
  }

  private OpenAddCustomerContactDialog(id) {
    const dialogReff = this.dialog.open(CustomerKeyholderAddComponent, { width: '450px', data: { Id: id, Name: "Create" }, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
      this.selection.clear();
    });
  }

  openCustomerContactforUpdate(custCntctId) {
    const dialogReff = this.dialog.open(CustomerKeyholderAddComponent, { width: '450px', data: { Id: custCntctId, CustId: this.customerId, Name: "Update" }, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
      this.selection.clear();
    });
  }

  openCustomerContactforDelete(custCntctId) {
    this.customerManagementService.DeleteCustomerContact(custCntctId).subscribe({
      next: response => this.onDeleteComplete(response),
      error: err => this.errorMessage = err
    });
    this.ngOnInit();
  }

  onDeleteComplete(response): void {
    if (response.statusCode == 200) {
      this.dialog.closeAll();
      this.ngOnInit();
      this.customerAddressAddEditForm.reset();
      this.customerContactAddEditForm.reset();
    }
    else {
      this.alertService.processAlert(
        response,
      );
      this.ngOnInit();
    }
  }

  checkedVisibilty() {
    if (this.visiBilityChecked) {
      this.visibilityPatrolForm.get('fromDate').enable();
      this.visibilityPatrolForm.get('toDate').enable();
    } else {
      this.visibilityPatrolForm.get('fromDate').disable();
      this.visibilityPatrolForm.get('toDate').disable();
    }
  }

  createVisibilityPatrolForm() {
    let formModel = {
      "visibilityPatrolId": '',
      "customerId": this.customerId,
      "customerAddressId": this.addressId,
      "fromDate": "",
      "toDate": "",
      "referenceId": null,
      "referenceTypeId": null,
      "createdUserId": this.loggedUser.userId
    }
    this.visibilityPatrolForm = this._fb.group({
    });
    Object.keys(formModel).forEach((key) => {
      this.visibilityPatrolForm.addControl(key, new FormControl(formModel[key]));
    });
    this.visibilityPatrolForm = setRequiredValidator(this.visibilityPatrolForm, ["fromDate", "toDate"]);
    this.visibilityPatrolForm.get('fromDate').disable();
    this.visibilityPatrolForm.get('toDate').disable();
  }

  cancelVisibilityPatrolClick() {
    this.cancelledReason = '';
    this.cancelVisibilityPatrol = true;
  }

  onSubmitVisibilityPatrolForm() {
    if (this.visibilityPatrolForm.invalid) {
      return;
    }
    this.visibilityPatrolForm.get("customerAddressId").setValue(this.addressId);
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VISIBILITY_PATROL, this.visibilityPatrolForm.value)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.getVisibilityPatrolData();
        }
      });
  }

  onDateChange(event) {
    this.minDateMoving = new Date(this.visibilityPatrolForm.controls.fromDate.value);
  }

  saveCancelVisibility() {
    if (!this.cancelledReason) {
      this.snackbarService.openSnackbar("Reason for cancel is required", ResponseMessageTypes.ERROR);
      return;
    };
    let postObj = {
      "customerId": this.customerId,
      "customerAddressId": this.addressId,
      "iscancelled": true,
      "cancelledReason": this.cancelledReason,
      "createdUserId": this.loggedUser.userId
    };
    this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VISIBILITY_PATROL, postObj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.cancelVisibilityPatrol = false;
          this.createVisibilityPatrolForm();
          this.getVisibilityPatrolData();
        }
      });
  }

  minDateMoving = new Date();
  minDateVisibilityPatrol = new Date();
  getVisibilityPatrolData() {
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.VISIBILITY_PATROL,
      undefined,
      false,
      prepareRequiredHttpParams({
        customerId: this.customerId,
        customerAddressId: this.addressId
      })
    ).subscribe((response: IApplicationResponse) => {
      this.visibilityPatrolData = response.resources;
      if (response.isSuccess && response.resources && response.statusCode === 200) {
        this.visibilityPatrolForm.patchValue(this.visibilityPatrolData);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  removeSelectedSiteAddressTab(index) {
    if (index != 7) {
      if (index != 0) {
        this.sessionService.removeItem('selectedContract')
        this.sessionService.removeItem('index')
        this.sessionService.removeItem('agreementTab')
      }
    }
  }

  navigateViewCustomerWithoutReload() {
    let queryParams = { addressId: this.addressId, navigateTab: this.selectedTabIndex, monitoringTab: this.selectedMonitoringIndex };
    if (this.feature && this.featureIndex) {
      queryParams['feature_name'] = this.feature;
      queryParams['featureIndex'] = this.featureIndex;
    }
    const url = this.router.url?.indexOf('manage-customers/view') != -1;
    if (url) {
      this.router.navigate([], {
        relativeTo: this.activatedRoute,
        queryParams: queryParams,
        queryParamsHandling: 'merge',
        skipLocationChange: true
      });
    }
  }

  redirectToCustomerViewPage(e) {
    this.selectedTabIndex = e.navigateTab;
    this.selectedMonitoringIndex = e.monitoringTab;
    this.onSequenceChangeEvent({ index: this.selectedTabIndex }, e.navigateTab == 4 ? true : false);
  }

  typeInNumber;
  onSequenceChangeEvent(event, changed = true) {
    this.editDebtor = true;
    this.selectedTabIndex = event.index;
    if (this.selectedTabIndex != 4 && changed) {
      this.selectedMonitoringIndex = 0;
    }
    if (this.activatedRoute.snapshot.queryParams?.monitoringTab && !changed) {
      this.selectedMonitoringIndex = +this.activatedRoute.snapshot.queryParams?.monitoringTab;
    }
    if (this.selectedTabIndex != 4 && changed) {
      this.navigateViewCustomerWithoutReload();
      if (this.selectedTabIndex == 0 || this.selectedTabIndex == 1 || this.selectedTabIndex == 2 || this.selectedTabIndex == 10)
        setTimeout(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }, 4000);
    }
    this.removeSelectedSiteAddressTab(event.index)
    switch (event.index) {
      case 0:
        this.onLoadRecentActivities();
        break;
      case 2:
        this.typeInNumber = 2;
        setTimeout(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }, 500);
        break;
      case 3:
        this.typeInNumber = 1;
        setTimeout(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }, 500);
        break;
      case 4:
        this.onMonitoringTabChangeEvent({ index: this.selectedMonitoringIndex });
        break;
      case 5:
        this.inputParams = {
          fromComponentUrl: 'customer', customerId: this.customer ? this.customer['customerId'] : this.customerId,
          addressId: this.addressId
        };
        break;
      case 6:
        this.activeServices = {
          isShowSubscription: false,
        };
        this.getBOSContracts();
        break;
      case 7:
        this.getAgreementTabsForCustomer();
        break;
      case 8:
        this.typeInNumber = 3;
        break;
      case 10:
        this.createTableForDebtorInfo();
        this.changeDebtorType();
        break;
      case 11:
        this.createCustomerClassificationForm();
        this.onFormControlChange();
        this.editClassifications = false;
        this.customerCategoryForm.get('isManualItemOwnerShipType').setValue(false);
        this.customerCategoryForm.get('isManualCategory').setValue(false);
        this.customerCategoryForm.get('isManualOrigin').setValue(false);
        this.customerCategoryForm.get('isManualInstallOrigin').setValue(false);
        this.customerCategoryForm.get('isManualDebtorGroup').setValue(false);
        this.getForkJoinRequestsOneForClassificationsTab();
        break;
      default:
        setTimeout(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }, 500);
        break;
    }
    this.title = "Tickets";
    if (this.typeInNumber != undefined && this.typeInNumber < 3) {
      if (this.typeInNumber == 1) {
        this.title = "Technical Tickets"
      } else if (this.typeInNumber == 2) {
        this.title = "Sales Tickets"
      }
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getForkJoinRequestsOneForClassificationsTab(): void {
    forkJoin([
      this.getContractPurchaseDetails(), this.getCustomerClassification(), this.getCategoryList(), this.getOriginList()
    ])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
            switch (ix) {
              case 0:
                this.contractPurchaseObject = respObj.resources;
                break;
              case 1:
                if (respObj.resources.customerClassificationMappingId) {
                  this.isCategaryAvailable = false;
                } else {
                  this.isCategaryAvailable = true;
                }
                if (respObj.message === 'Record not found') {
                } else {
                  this.classificationDetails = respObj.resources;

                  if (respObj?.resources && this.customerCategoryForm) {
                    this.customerCategoryForm.patchValue(respObj.resources);
                  }
                }
                if (!this.isDealer && this.isProfileActivated) {
                  this.getCustomerSubClassification();
                }
                break;
              case 2:
                this.categoryList = respObj.resources;
                break;
              case 3:
                this.originList = respObj.resources;
                break;
            }
            if (ix == response.length - 1) {
              this.getForkJoinRequestsTwoForClassificationsTab();
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
        this.isLoading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getForkJoinRequestsTwoForClassificationsTab(): void {
    forkJoin([
      this.getInstallOriginList(), this.getDebtorGroupList(), this.getOwnershipList()
    ])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
            switch (ix) {
              case 0:
                this.installOriginList = respObj.resources;
                break;
              case 1:
                this.debtorGroupList = respObj.resources;
                break;
              case 2:
                this.ownershipList = respObj.resources;
                break;
            }
            if (ix == response.length - 1) {
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          }
        });
        this.isLoading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onMatTabGroupSelected(matTabLevel: string, matTabName: string, event) {
    switch (matTabLevel) {
      case "first level":
        break;
      case "second level":
        switch (matTabName) {
          case "debtor info":
            this.selectedDebtorInfoIndex = event.index;
            this.getChangigMethodList();
            break;
          case "first level":
            break;
        }
        break;
      case "third level":
        break;
    }
  }

  onCancelClassification() {
    this.onSequenceChangeEvent({ index: 11 }, false);
  }

  contractPurchaseObject: any = {};
  objectKeys = Object.keys;
  getContractPurchaseDetails(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_CUSTOMER_APPROVAL_DETAILS, null,
      false, prepareRequiredHttpParams({
        customerId: this.customerId,
        addressId: this.addressId
      }))
  }

  onMonitoringTabChangeEvent(event) {
    this.selectedMonitoringIndex = event.index;
    this.navigateViewCustomerWithoutReload();
    switch (event.index) {
      case 0:
        break;
      case 1:
        break;
      case 2:
        break;
      case 3:
        break;
      case 4:
        this.onCustomerPreference();
        break;
      case 5:
        break;
      case 6:
        break;
      case 7:
        break;
      case 8:
        this.getVisibilityPatrolData();
        break;
      case 9:
        break;
    }
  }

  onCustomerPreference() {
    this.isDisableCustomerPreference = true;
    this.createCustomerPreferenceForm();
    this.getForkJoinRequestsForMonitoringAndResponseTab();
    let obj = this.customerPreferenceForm.value;
    // 1 - Password , 2 - Pin
    if (obj.virtualAgentServiceType === 2) {
      this.isSelected = false;
    } else {
      this.isSelected = true;
    }
  }

  getForkJoinRequestsForMonitoringAndResponseTab(): void {
    forkJoin([
      this.getCustomerKeyHolder(), this.getCustomerStack(), this.getThrirdPartyResponse()
    ])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
            switch (ix) {
              case 0:
                this.customerKeyHolderList = respObj.resources;
                break;
              case 1:
                this.customerStackConfigList = getPDropdownData(respObj.resources);
                break;
              case 2:
                this.thirdPartyResponseList = respObj.resources;
                break;
            }
          }
          if (ix == response.length - 1) {
            this.getCustomerPreferenceDetailsById();
          }
        });
        this.isLoading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createTableForDebtorInfo() {
    this.debtorInfoObservableResponse = [{
      bankName: 'Axis Bank',
      debitDay: '21/02/2021',
      accountNumber: 'Axis1001',
      accountType: 'Savings',
      branchName: 'SouthBKC',
      branchCode: 'Branch001',
      status: 'Enabled',
      cssClass: 'status-label-pink'
    },
    {
      bankName: 'Mercantile Bank',
      debitDay: '21/03/2021',
      accountNumber: 'Merc2001',
      accountType: 'Savings',
      branchName: 'WestBKC',
      branchCode: 'Branch002',
      status: 'Enabled',
      cssClass: 'status-label-pink'
    }];
  }

  onRecentActivitiesExpansionPanelOpened(e) {
    if (this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[e].disabled) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.recentActivitiesExpansionPanelIndex = e;
    this.onRecentActivitiesTabCRUDRequested(CrudType.GET,);
  }

  onViewMoreRecentActivities(data: any) {
    if (data[0]?.reference == 'invoice') {
      this.router.navigate(['/customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: data[0]?.debtorId, addressId: this.addressId, tab: 0 }, skipLocationChange: true });
    } else if (data[0]?.reference == 'payment') {
      this.router.navigate(['/customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: data[0]?.debtorId, addressId: this.addressId, tab: 1 }, skipLocationChange: true });
    }
  }

  onSalesExpansionPanelOpened() {
    console.log("this.primengTableConfigPropertiesLeadsObj.tableComponentConfigs.tabsList", this.primengTableConfigPropertiesLeadsObj.tableComponentConfigs.tabsList)
    if (!this.primengTableConfigPropertiesLeadsObj.tableComponentConfigs.tabsList[0]?.canView) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.salesExpansionPanelIndex = 0;
    this.onCRUDRequested(CrudType.GET, {});
  }

  onChangeStatusBaank(rowData, index) {
    this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.debtorInfoPrimengTableConfigProperties.tableComponentConfigs.tabsList[0].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.debtorInfoPrimengTableConfigProperties.tableComponentConfigs.tabsList[0].moduleName,
        apiSuffixModel: this.debtorInfoPrimengTableConfigProperties.tableComponentConfigs.tabsList[0].apiSuffixModel
      },
    });
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[this.recentActivitiesExpansionPanelIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[this.recentActivitiesExpansionPanelIndex].moduleName,
        apiSuffixModel: this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[this.recentActivitiesExpansionPanelIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  scheduleCallbackopen() {
    this.recentActivitiesExpansionPanelIndex = 5;
    this.getScheduledCallbackList(CommonPaginationConfig.defaultPageIndex, CommonPaginationConfig.defaultPageSize);
  }

  onRecentActivitiesTabCRUDRequested(type: CrudType | string, row?: object, searchObj?): void {
    if (row && Object.keys(row).length == 0 || !row) {
      row = {};
      row["pageIndex"] = CommonPaginationConfig.defaultPageIndex;
      row["pageSize"] = CommonPaginationConfig.defaultPageSize;
    }
    switch (type) {
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          // for RECENT ACTIVITIES Tab Selection
          case 0:
            // for RECENT ACTIVITIES's Expansion Panel Selection
            switch (this.recentActivitiesExpansionPanelIndex) {
              // for RECENT ACTIVITIES's RECENT TICKETS Expansion Panel
              case 0:
                this.getRecentTicketList(row["pageIndex"] ? row["pageIndex"] : CommonPaginationConfig.defaultPageIndex, row["pageSize"] ? row["pageSize"] : CommonPaginationConfig.defaultPageSize, searchObj);
                break;
              // for RECENT ACTIVITIES's RECENT & OUTSTANDING INVOICES Expansion Panel
              case 1:
                this.getRecentOutstandingInvoice(row["pageIndex"] ? row["pageIndex"] : CommonPaginationConfig.defaultPageIndex, row["pageSize"] ? row["pageSize"] : CommonPaginationConfig.defaultPageSize, searchObj, this.recentActivitiesExpansionPanelIndex);
                break;
              // for RECENT ACTIVITIES's POSITIVE EVENTS Expansion Panel
              case 2:
                break;
              // for RECENT ACTIVITIES's OPEN APPOINTMENTS Expansion Panel
              case 3:
                this.getOpenAppointments(row["pageIndex"] ? row["pageIndex"] : CommonPaginationConfig.defaultPageIndex, row["pageSize"] ? row["pageSize"] : CommonPaginationConfig.defaultPageSize, searchObj);
                break;
              // for RECENT ACTIVITIES's DEBTOR AGING Expansion Panel
              case 4:
                break;
              // for RECENT ACTIVITIES's VIEW SCHEDULED CALLBACK Expansion Panel
              case 5:
                this.getScheduledCallbackList(row["pageIndex"] ? row["pageIndex"] : CommonPaginationConfig.defaultPageIndex, row["pageSize"] ? row["pageSize"] : CommonPaginationConfig.defaultPageSize, searchObj);
                break;
            }
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          // for RECENT ACTIVITIES Tab Selection
          case 0:
            // for RECENT ACTIVITIES Sub Expansion Panel Selection
            if (this.recentActivitiesExpansionPanelIndex == 0) {
              let queryParams = { id: row['ticketId'], customerId: row['customerId'], data: this.title, ticketgroupid: this.typeInNumber, };
              if (this.feature && this.featureIndex) {
                queryParams['feature_name'] = this.feature;
                queryParams['featureIndex'] = this.featureIndex;
              }
              this.router.navigate(['/customer/manage-customers/ticket/view'], { queryParams: queryParams, skipLocationChange: true });
            }
            else if (this.selectedTabIndex == 0 && this.recentActivitiesExpansionPanelIndex == 5) {
              let data: ScheduleCallBackViewModel;
              if (row?.['fromModule'] == 'Customer') {
                data = new ScheduleCallBackViewModel({ customerCallbackId: row['callbackId'], fromModule: row?.['fromModule'] });
              }
              else if (row?.['fromModule'] == 'Lead') {
                data = new ScheduleCallBackViewModel({ leadCallbackId: row['callbackId'], fromModule: row?.['fromModule'] });
              }
              data.type = "Schedule Callback";
              this.rxjsService.setDialogOpenProperty(true);
              const dialogReff = this.dialog.open(ScheduleCallbackViewModalComponent, { width: '900px', disableClose: true, data });
              dialogReff.afterClosed().subscribe(result => {
                if (result) return;
              });
            }
            break;
        }
        break;
    }
  }

  onActionSubmitted(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onRecentActivitiesTabCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onRecentActivitiesTabCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onRecentActivitiesTabCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onRecentActivitiesTabCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRecentTicketList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[0].loading = true;
    if (!otherParams) {
      otherParams = {};
    }
    otherParams['customerId'] = this.customerId;
    otherParams['addressId'] = this.addressId;
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.RECENT_CUSTOMER_TICKETS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode === 200) {
        this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[0].dataList = response.resources;
        this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[0].totalRecords = response.totalCount;
        this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[0].loading = false;
      }
    });
  }

  getRecentOutstandingInvoice(pageIndex?: string, pageSize?: string, otherParams?: object, index?: number) {
    this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[index].loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.CUSTOMER_RECENT_OUTSTANDING_INVOICES,
      undefined,
      false,
      prepareRequiredHttpParams({ customerId: this.customerId, addressId: this.addressId })
    ).subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode === 200 && response?.resources) {
        this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[index].dataList = [response?.resources];
      }
      this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[index].totalRecords = 0;
      this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[index].isShowNoRecord = !response?.resources;
      this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[index].loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getOpenAppointments(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[3].loading = true;
    if (!otherParams) {
      otherParams = {};
    }
    otherParams['customerId'] = this.customerId;
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.CUSTOMER_APPPOINTMENTS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[3].dataList = response.resources;
        this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[3].totalRecords = response.totalCount;
      }
      this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[3].loading = false;
    });
  }

  getScheduledCallbackList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[5].loading = true;
    if (!otherParams) {
      otherParams = {};
    }
    otherParams['customerId'] = this.customerId;
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      SalesModuleApiSuffixModels.SCHEDULE_CALLBACK_CUSTOMERS,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode === 200) {
        this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[5].dataList = response.resources;
        this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[5].totalRecords = response.totalCount;
        this.recentActivitiesTabPrimengTableConfigProperties.tableComponentConfigs.tabsList[5].loading = false;
      }
      this.rxjsService.setLeadNotesUpdateStatus(false);
    });
  }

  openQuickAction() {
    this.rxjsService.setQuickActionComponent(ModuleName.CUSTOMER)
    $('.quick-action-sidebar').removeClass('hide-quick-action-menu');
  }

  callDail(e): void {
    if (!this.agentExtensionNo) {
      this.dialog.closeAll();
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    } else {
      let data = {
        customerContactNumber: e,
        customerId: this.customerId,
        clientName: this.custInfoList && this.custInfoList.customerName,
        siteAddressId: this.addressId
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
    }
  }

  openDialog(flag) {
    if (flag == 'payment-underride') {
      this.paymentUnderride = true;
    }
    if (flag == 'payment-override') {
      this.paymentOverride = true;
    }
    if (flag == 'invoice-info') {
      this.invoiceInfo = true;
    }
  }

  /* --- Recent Activities Starts --- */

  onLoadRecentActivities() {
    let otherParams = { customerId: this.customerId, addressId: this.addressId };
    if (this.partitionId) {
      otherParams['partitionId'] = this.partitionId;
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.SHARED, CustomerModuleApiSuffixModels.RECENT_ACTIVITIES, null, false, prepareRequiredHttpParams(otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.recentActivitiesDetails = response?.resources;
          this.recentActivitiesTicketNotes = response?.resources?.ticketInfo ? response?.resources?.ticketInfo[0].ticketNotes : '';
          this.onRecentActivitiesPatchValue(response?.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onRecentActivitiesPatchValue(response?: any) {
    const enableHyperLink = { enableHyperLink: true, valueColor: '#166DFF', };
    this.recentActivitiesInfo = {
      customerProfileView: [
        {
          name: 'Customer Profile View', className: 'text-uppercase', columns: [
            { name: 'Division', className: 'pl-2', value: response?.customerInfo ? response?.customerInfo[0]?.divisionName : '', order: 1 },
            { name: 'Response Area', className: 'pl-2', value: response?.customerInfo ? response?.customerInfo[0]?.responseArea : '', order: 2 },
            { name: 'Origin', className: 'pl-2', value: response?.customerInfo ? response?.customerInfo[0]?.originName : '', order: 3 },
            { name: 'Branch', className: 'pl-2', value: response?.customerInfo ? response?.customerInfo[0]?.branchName : '', order: 4 },
            { name: 'Technical Area', className: 'pl-2', value: response?.customerInfo ? response?.customerInfo[0]?.technicalServiceArea : '', order: 5 },
            { name: 'Category', className: 'pl-2', value: response?.customerInfo ? response?.customerInfo[0]?.categoryName : '', order: 6 },
            { name: 'Region', className: 'pl-2', value: response?.customerInfo ? response?.customerInfo[0]?.region : '', order: 7 },
            { name: 'Sales Area', className: 'pl-2', value: response?.customerInfo ? response?.customerInfo[0]?.salesArea : '', order: 8 },
            { name: 'Office Number', className: 'pl-2', value: response?.customerInfo ? response?.customerInfo[0]?.officeNo : '', isFullNumber: true, order: 9 },
          ],
        },
      ],
      technicalView: [
        {
          name: 'Technical Info', className: 'text-uppercase', columns: [
            { name: 'Service Call No', className: 'pl-2', value: response?.technicalInfo ? response?.technicalInfo[0]?.serviceCallNumber : '', ...enableHyperLink, order: 1 },
            { name: 'Call Type', className: 'pl-2', value: response?.technicalInfo ? response?.technicalInfo[0]?.callType : '', order: 2 },
            { name: 'Status', className: 'pl-2', value: response?.technicalInfo ? response?.technicalInfo[0]?.status : '', order: 3 },
            { name: 'Scheduled Date', className: 'pl-2', value: response?.technicalInfo ? response?.technicalInfo[0]?.scheduledDate : '', order: 4 },
            { name: 'Job Type', className: 'pl-2', value: response?.technicalInfo ? response?.technicalInfo[0]?.jobTypeId : '', order: 5 },
            { name: 'Description', className: 'pl-2', value: response?.technicalInfo ? response?.technicalInfo[0]?.jobDescription : '', order: 6 },
            { name: 'Call Escalation Created', className: 'pl-2', value: response?.technicalInfo ? response?.technicalInfo[0]?.callEscalationCreated : '', order: 7 },
            { value: 'view more', index: '3', name: '', isName: true, className: 'pb-3', valueColor: '#459CE8', valueWidth: 'auto', enableHyperLink: true, order: 8 },
          ],
        },
        {
          name: 'Account Info', className: 'text-uppercase', columns: [
            { name: 'Debtor Code', className: 'pl-2', value: response?.accountInfo ? response?.accountInfo[0]?.debtorCode : '', order: 1 },
            { name: 'Outstanding Balance', className: 'pl-2', value: response?.accountInfo ? response?.accountInfo[0]?.outstandingInvoice : '', order: 2 },
            { name: 'Overdue', className: 'pl-2', value: response?.accountInfo ? response?.accountInfo[0]?.overdue : '', order: 3 },
            { name: 'Amount', className: 'pl-2', value: response?.accountInfo ? response?.accountInfo[0]?.amount : '', isRandSymbolRequired: true, order: 4 },
            { value: 'view more', index: '9', name: '', isName: true, className: 'pb-3', valueColor: '#459CE8', valueWidth: 'auto', enableHyperLink: true, order: 5 },
          ],
        },
        {
          name: 'Ticket Info', className: 'text-uppercase', columns: [
            { name: 'Ticket ID', className: 'pl-2', value: response?.ticketInfo ? response?.ticketInfo[0]?.ticketRefNo : '', ...enableHyperLink, order: 1 },
            { name: 'Action Type', className: 'pl-2', value: response?.ticketInfo ? response?.ticketInfo[0]?.actionType : '', order: 2 },
            { name: 'Lapsed Time', className: 'pl-2', value: response?.ticketInfo ? response?.ticketInfo[0]?.lapsedTime : '', order: 3 },
            { name: 'Assigned To', className: 'pl-2', value: response?.ticketInfo ? response?.ticketInfo[0]?.assignTo : '', order: 4 },
            { name: 'Status', className: 'pl-2', value: response?.ticketInfo ? response?.ticketInfo[0]?.status : '', order: 5 },
            { name: 'Notes', className: 'pl-2', value: response?.ticketInfo ? response?.ticketInfo[0]?.ticketNotes?.comments : '', order: 6 },
            { value: 'view more', index: '8', name: '', isName: true, className: 'pb-3', valueColor: '#459CE8', valueWidth: 'auto', enableHyperLink: true, order: 7 },
          ],
        }
      ],
      salesView: [
        {
          name: 'Sales Info', className: 'text-uppercase', columns: [
            { name: 'Appointment ID', className: 'pl-2', value: response?.salesInfo ? response?.salesInfo[0]?.appointmentRefNo : '', ...enableHyperLink, order: 1 },
            { name: 'Date', className: 'pl-2', value: response?.salesInfo ? response?.salesInfo[0]?.date : '', isDate: true, order: 2 },
            { name: 'Timeslot', className: 'pl-2', value: response?.salesInfo ? response?.salesInfo[0]?.timesSlot : '', order: 3 },
            { name: 'Seller', className: 'pl-2', value: response?.salesInfo ? response?.salesInfo[0]?.seller : '', order: 4 },
            { name: 'Status', className: 'pl-2', value: response?.salesInfo ? response?.salesInfo[0]?.status : '', order: 5 },
            { value: 'view more', index: '2', name: '', isName: true, className: 'pb-3', valueColor: '#459CE8', valueWidth: 'auto', enableHyperLink: true, order: 6 },
          ],
        },
        {
          name: 'Contract Info', className: 'text-uppercase', columns: [
            { name: 'Currently Monthly Fee', className: 'pl-2', value: response?.contractInfo ? response?.contractInfo[0]?.currentMonthlyFee : '', isRandSymbolRequired: true, order: 1 },
            { name: 'Contract Start Date', className: 'pl-2', value: response?.contractInfo ? response?.contractInfo[0]?.contractStartDate : '', isDate: true, order: 2 },
            { name: 'Contract End Date', className: 'pl-2', value: response?.contractInfo ? response?.contractInfo[0]?.contractEndDate : '', isDate: true, order: 3 },
            { name: 'Save offer', className: 'pl-2', value: response?.contractInfo ? response?.contractInfo[0]?.recentActivitiesContractOfferInformation?.saveOfferRefNo : '', ...enableHyperLink, order: 4 },
            { name: 'System Ownership', className: 'pl-2', value: response?.contractInfo ? response?.contractInfo[0]?.recentActivitiesItemOwnershipInformation?.itemOwnershipTypeName : '', ...enableHyperLink, order: 5 },
            { name: 'Dealer Warrenty', className: 'pl-2 pb-3', value: response?.contractInfo ? response?.contractInfo[0]?.dealerWarranty : '', order: 6 },
          ],
        },
        {
          name: 'Signal Info', className: 'text-uppercase', columns: [
            { name: 'OB Number', className: 'pl-2', value: response?.signalInfo ? response?.signalInfo[0]?.obNumber : '', order: 1 },
            { name: 'Decoder', className: 'pl-2', value: response?.signalInfo ? response?.signalInfo[0]?.decoder : '', order: 2 },
            { name: 'Time of Signal', className: 'pl-2', value: response?.signalInfo ? response?.signalInfo[0]?.timeOfSignal : '', isDateTime: true, order: 3 },
            { name: 'Signal Description', className: 'pl-2', value: response?.signalInfo ? response?.signalInfo[0]?.signalDescription : '', order: 4 },
            { name: 'Zone', className: 'pl-2', value: response?.signalInfo ? response?.signalInfo[0]?.zone : '', order: 5 },
            { name: 'Zone Description', className: 'pl-2', value: response?.signalInfo ? response?.signalInfo[0]?.zoneDescription : '', order: 6 },
            { name: 'Incident', className: 'pl-2', value: response?.signalInfo ? response?.signalInfo[0]?.incidentName : '', order: 7 },
            { name: 'Incident Classification', className: 'pl-2', value: response?.signalInfo ? response?.signalInfo[0]?.incidentClassification : '', order: 8 },
            { value: 'view more', index: '4', name: '', isName: true, className: 'pb-3', valueColor: '#459CE8', valueWidth: 'auto', enableHyperLink: true, order: 9 },
          ],
        }
      ]
    }
  }

  onLinkClick(e) {
    if (e?.name?.toLowerCase() == "service call no" && this.recentActivitiesDetails?.technicalInfo[0]?.serviceCallNumber) {
      window.open(this.recentActivitiesDetails?.technicalInfo[0]?.hyperlink);
    } else if (e?.name?.toLowerCase() == "ticket id" && this.recentActivitiesDetails?.ticketInfo[0]?.ticketRefNo) {
      window.open(this.recentActivitiesDetails?.ticketInfo[0]?.url);
    } else if (e?.name?.toLowerCase() == "system ownership" && this.recentActivitiesDetails?.contractInfo[0]?.recentActivitiesItemOwnershipInformation?.itemOwnershipTypeName) {
      this.onSequenceChangeEvent({ index: 13 });
    } else if (e?.name?.toLowerCase() == "save offer" && this.recentActivitiesDetails?.contractInfo[0]?.recentActivitiesContractOfferInformation?.saveOfferRefNo) {
      window.open(`${window.location.origin}/customer/manage-customers/ticket/save-offer/view?id=${this.recentActivitiesDetails?.contractInfo[0]?.recentActivitiesContractOfferInformation?.contractId}&customerId=${this.recentActivitiesDetails?.contractInfo[0]?.recentActivitiesContractOfferInformation?.customerId}&addressId=${this.addressId}&serviceId=${this.recentActivitiesDetails?.contractInfo[0]?.serviceId}&saveOfferId=${this.recentActivitiesDetails?.contractInfo[0]?.recentActivitiesContractOfferInformation?.saveOfferId}&viewable=true`)
    } else if (e?.name?.toLowerCase() == "appointment id" && this.recentActivitiesDetails?.salesInfo[0]?.appointmentRefNo) {
      window.open(`${window.location.origin}/sales/task-management/leads/appointment-history?leadId=${this.recentActivitiesDetails?.salesInfo[0]?.leadId}`)
    } else if (e?.value == 'view more') {
      this.onViewMoreClick(e?.index);
    }
  }

  onViewMoreClick(index: number) {
    if (index == 2) {
      if (!this.recentActivitiesDetails?.salesInfo?.length) {
        return;
      } else if (this.recentActivitiesDetails?.salesInfo[0]?.appointmentRefNo) {
        window.open(`${window.location.origin}/sales/task-management/leads/appointment-history?leadId=${this.recentActivitiesDetails?.salesInfo[0]?.leadId}`);
      }
    } else {
      this.selectedTabIndex = +index;
      this.onSequenceChangeEvent({ index: this.selectedTabIndex });
    }
  }

  /* --- Recent Activities Ends --- */

  /* --- Subscription Starts --- */
  getBOSContracts() {
    if (!this.addressId) {
      return;
    }
    this.isSubLoading = true;
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.BILL_OF_SERVICE_CONTRACTS, null, null,
      prepareRequiredHttpParams({
        customerId: this.customerId ? this.customerId :
          this.customer?.['customerId'],
        siteAddressId: this.addressId
      }))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.contractStatus = res?.resources;
        }
        this.isSubLoading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }
  /* --- Subscription Ends --- */

  // dineshkumar changes
  partitionId = "";
  inputObject = new SiteSpecificInputObjectModel();
  inputParams = { fromComponentUrl: 'customer', customerId: "", addressId: "" };
  agreementTabs = [];
  selectedAgreementTabName;

  getAgreementTabsForCustomer() {
    if (this.selectedTabIndex == 7 && ((this.selectedAddressDetails?.partitionDetails && this.selectedAddressDetails?.partitionDetails?.length > 0 && this.partitionId) ||
      this.selectedAddressDetails?.partitionDetails?.length == 0 || this.selectedAddressDetails?.partitionDetails == null)) {
      this.crudService.get(
        ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.AGREEMENT_TABS,
        undefined,
        false,
        prepareRequiredHttpParams({
          customerId: this.customer['customerId'],
          addressId: this.addressId,
          partitionId: this.partitionId
        }),
      ).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources && response.statusCode === 200) {
          let selectedContract = JSON.parse(this.sessionService.getItem('selectedContract'));
          if (Object.keys(selectedContract ? selectedContract : {})?.length > 0) {
            this.selectedContractId = selectedContract?.contractId;
            this.inputObject = {
              fromComponentType: 'customer', customerId: this.customer['customerId'],
              addressId: this.addressId, partitionId: this.partitionId,
              leadId: selectedContract.leadId,
              quotationVersionId: selectedContract.quotationversionId
            };
          }
          else {
            this.selectedContractId = "";
          }
          this.isFormSubmitted = false;
          this.agreementTabs = response.resources;
          this.agreementTabs.push({
            agreementTabId: 1002,
            agreementTabName: "Safe Entry Request",
            indexNo: 8,
            sortOrder: 13
          })
          if (this.sessionService.getItem('index')) {
            this.agreementTabs.forEach((agreementTab, ix: number) => {
              agreementTab.isSelected = Number(this.sessionService.getItem('index')) == ix ? true : false;
            });
          } else {
            this.selectedAgreementTabName = '';
          }
        }
        this.getContractsForSiteAddressInfoTab();
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  onAgreementTabClicked(agreementTab, index, isDDontCheckPermission = false) {
    if (index != 6) {
      this.isFormSubmitted = true;
      if (!this.selectedContractId) {
        return;
      }
      let isAccessDeined = this.getPermissionByActionType(agreementTab?.agreementTabName, "List");
      if (isAccessDeined && isDDontCheckPermission == false) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
    }
    switch (agreementTab?.agreementTabName) {
      case 'Key Holder Information':
        this.onAfterCustomerVerificationCheck(CustomerVerificationType.KEY_HOLDER_INFORMATION, agreementTab, index);
        break;
      case 'Domestic Workers':
        this.onAfterCustomerVerificationCheck(CustomerVerificationType.DOMESTIC_WORKER, agreementTab, index);
        break;
      case 'Special Instructions':
        this.onAfterCustomerVerificationCheck(CustomerVerificationType.SPECIAL_INSTRUCTIONS, agreementTab, index);
        break;
      case 'Arming and Disarming Time':
        this.onAfterCustomerVerificationCheck(CustomerVerificationType.ARMING_AND_DISARMING_TIME, agreementTab, index);
        break;
      case 'Access to Premises':
        this.onAfterCustomerVerificationCheck(CustomerVerificationType.ACCESS_TO_PREMISES, agreementTab, index);
        break;
      case 'Site Hazard':
        this.onAfterCustomerVerificationCheck(CustomerVerificationType.SITE_HAZARD, agreementTab, index);
        break;
      case 'Customer Verification':
        this.onCustomerVerficationCheck(CustomerVerificationType.CUSTOMER_VERIFICATION, agreementTab, index);
        break;
      case 'Holiday Instruction':
        this.onCustomerVerficationCheck(CustomerVerificationType.HOLIDAY_INSTRUCTION, agreementTab, index);
        break;
      default:
        this.onAfterAgreementTabClicked(agreementTab, index);
        break;
    }
  }
  getPermissionByActionType(actionTypeMenuName, action?): boolean {
    let foundObj = this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[this.selectedTabIndex]!['quickAction']?.find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      if (action) {
        let foundObj1 = foundObj['subMenu']?.find(fSC => fSC.menuName == action);
        if (foundObj1) {
          return false;
        }
        else {
          return true;
        }
      }
      return false;
    }
    else {
      return true;
    }
  }

  setAgreementTabActive(tab, index) {
    this.sessionService.setItem('agreementTab', JSON.stringify(tab)) // to restore the value when click on back
    this.sessionService.setItem('index', index)
  }

  onAfterAgreementTabClicked(agreementTab, index) {
    this.selectedContractDetail = JSON.parse(this.sessionService.getItem('selectedContract'));
    setTimeout(() => {
      this.selectedAgreementTabName = agreementTab?.agreementTabName;
      if (this.selectedContractDetail) {
        this.inputObject = {
          fromComponentType: 'customer', customerId: this.customer['customerId'],
          addressId: this.addressId, partitionId: this.partitionId,
          leadId: this.selectedContractDetail.leadId,
          quotationVersionId: this.selectedContractDetail.quotationversionId
        };
      }
      this.agreementTabs.forEach((agreementTab, ix: number) => {
        agreementTab.isSelected = index == ix ? true : false;
      });
      this.setAgreementTabActive(agreementTab, index)
    }, 500);
  }

  selectedContractDetail;
  onContractSelectionChanged(event) {
    if (event?.target?.value) {
      this.selectedContractDetail = this.contactsForSiteAddressInfoTab.find(cS => cS.contractId == event?.target?.value);
      this.sessionService.setItem('selectedContract', JSON.stringify(this.selectedContractDetail));
    }
  }

  getCustomerPreferenceDetailsById(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let obj1 = {
      customerAddressId: this.addressId, customerId: this.customerId,
      partitionId: this.partitionId
    };
    if (this.partitionId == '') {
      delete obj1['partitionId'];
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CUSTOMER_PREFERENCE_CUSTOMER_ADDRESS,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response) => {
      if (response.resources && response.isSuccess && response.statusCode == 200) {
        let customerPreferenceModel = new CustomerPreferenceModel(response.resources);
        this.customerPreferenceDetails = response.resources;
        if (customerPreferenceModel.virtualAgentServiceType === 2) {
          this.isSelected = false;
        } else {
          this.isSelected = true;
        }
        if (customerPreferenceModel.isThirdPartyResponse === true) {
          this.customerPreferenceForm.get('thirdPartyId').enable();
          this.customerPreferenceForm.get('thirdPartyCustomerCode').enable();
        } else {
          this.customerPreferenceForm.get('thirdPartyId').disable();
          this.customerPreferenceForm.get('thirdPartyCustomerCode').disable();
        }
        this.customerPreferenceForm.patchValue(customerPreferenceModel);
      }
      else if(response.message="Record not found") {
        this.customerPreferenceForm.reset(new CustomerPreferenceModel());
        this.customerPreferenceForm.disable();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
      this.isLoading = false;
    });
  }

  getCustomerKeyHolder(pageIndex?: string, pageSize?: string, otherParams?: object): Observable<IApplicationResponse> {
    this.loading = true;
    let obj1 = {
      customerAddressId: this.addressId, customerId: this.customerId,
      partitionId: this.partitionId
    };
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.CUSTOMER_KEY_HOLDER,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    );
  }

  getCustomerStack(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_NAMED_STACK_CONFIG_MAPPED, undefined, false, null)
  }

  getThrirdPartyResponse(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.THIRD_PARTY_RESPONSE_DROPDOWN, undefined, false, prepareRequiredHttpParams({
      customerId: this.customer['customerId'],
      customerAddressId: this.addressId
    }));
  }

  createCustomerClassificationForm(): void {
    let stageOneModel = new CustomerClassificationConfigModel();
    this.customerCategoryForm = this._fb.group({});
    Object.keys(stageOneModel).forEach((key) => {
      this.customerCategoryForm.addControl(key, new FormControl(stageOneModel[key]));
    });
    this.customerCategoryForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  isCategaryAvailable = true;
  getCustomerClassification(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CUSTOMER_CLASSIFICATION, null, false,
      prepareGetRequestHttpParams(null, null,
        {
          customerId: this.customerId,
          addressId: this.addressId,
          isManualCategory: true,
          isManualOrigin: true,
          isManualInstallOrigin: true,
          isManualDebtorGroup: true,
          isManualItemOwnerShipType: true
        }));
  }

  getCustomerSubClassification() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CUSTOMER_SUB_CLASSIFICATION, null, false,
      prepareGetRequestHttpParams(null, null,
        {
          customerId: this.customerId,
          addressId: this.addressId,
          isManual: true,
        }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.isSuccess && response.statusCode == 200) {
          this.subClassificationDetails = response.resources;
          this.customerSubCategoryArray = this.getSubCategory;
          response.resources?.forEach((queryBuilderTablesListModel: CustomerSubClassificationConfigModel) => {
            this.customerSubCategoryArray.push(this.createSubCategoryArray(queryBuilderTablesListModel));
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getOwnershipList(): Observable<IApplicationResponse> {
    return this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_OWNER_SHIP_TYPE)
  }

  getCategoryList(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_CATEGORY, null, false, null);
  }

  getOriginList(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_ORIGINS, null, false, null)
  }

  getInstallOriginList(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_INSTALL_ORIGIN, null, false, null)
  }

  getDebtorGroupList(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_DEBTOR_GROUP, null, false, null)
  }

  onFormControlChange() {
    this.customerCategoryForm.get('isManualCategory').valueChanges.subscribe((isManualCategory: string) => {
      this.categoryInputEnable = isManualCategory ? false : true;
    });
    this.customerCategoryForm.get('isManualOrigin').valueChanges.subscribe((isManualOrigin: string) => {
      this.originInputEnable = isManualOrigin ? false : true;
    });
    this.customerCategoryForm.get('isManualInstallOrigin').valueChanges.subscribe((isManualInstallOrigin: string) => {
      this.installInputEnable = isManualInstallOrigin ? false : true;
    });
    this.customerCategoryForm.get('isManualDebtorGroup').valueChanges.subscribe((isManualDebtorGroup: string) => {
      this.debtorGroupEnable = isManualDebtorGroup ? false : true;
    });
  }

  createSubCategoryForm(): void {
    this.customerSubCategoryForm = this._fb.group({
      customerSubCategoryArray: this._fb.array([]),
    });
  }

  createSubCategoryArray(commercialProducts?: CustomerSubClassificationConfigModel): FormGroup {
    let commercialPorudctData = new CustomerSubClassificationConfigModel(commercialProducts);
    let formControls = {};
    Object.keys(commercialPorudctData).forEach((key) => {
      if (key === 'isAvailable' || key === 'customerClassificationSubCategoryMappingId' || key === 'isManual' || key == 'subCategoryName') {
        formControls[key] = [{ value: commercialPorudctData[key], disabled: false }, [Validators.required]]
      }
    });
    return this._fb.group(formControls);
  }

  get getSubCategory(): FormArray {
    if (this.customerSubCategoryForm) {
      return this.customerSubCategoryForm.get("customerSubCategoryArray") as FormArray;
    }
  }

  submitSubCategory() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.customerCategoryForm.invalid) {
      return;
    }
    let formVal = this.customerCategoryForm.getRawValue();
    formVal.itemOwnershipTypeId = formVal.itemOwnershipTypeId ? Number(formVal.itemOwnershipTypeId) : Number(1);
    formVal.originId = Number(formVal.originId)
    formVal.categoryId = Number(formVal.categoryId)
    formVal.installOriginId = Number(formVal.installOriginId)
    formVal.debtorGroupId = Number(formVal.debtorGroupId)
    formVal.customerClassificationMappingId = this.classificationDetails && this.classificationDetails.customerClassificationMappingId;
    let finalSubmission = [];
    finalSubmission = this.customerSubCategoryForm.value.customerSubCategoryArray;
    finalSubmission.forEach((key) => {
      if (key.isAvailable === 'true') {
        key.isAvailable = true;
        key.isManual = true;
      }
      if (key.isAvailable === 'false') {
        key.isAvailable = false;
        key.isManual = false;
      }
      key["modifiedUserId"] = this.loggedUser.userId;
    });
    formVal.subCategoriesPutDTO = finalSubmission;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CUSTOMER_CLASSIFICATION, formVal, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.onSequenceChangeEvent({ index: 11 }, false);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  subCategaryChnageEvent(i) {
    if (this.getSubCategory.controls[i].get('isManual').value) {
      this.getSubCategory.controls[i].get('isAvailable').enable()
    } else {
      this.getSubCategory.controls[i].get('isAvailable').disable()
    }
  }

  isSelectedRadio(e) {
    if (e.value === 1) {
      this.isSelected = true;
      this.customerPreferenceForm.controls['virtualAgentServicePin'].clearValidators()
      this.customerPreferenceForm.controls['virtualAgentServicePin'].updateValueAndValidity();
    } else {
      this.isSelected = false;
      this.customerPreferenceForm.get('virtualAgentServicePin').setValidators([Validators.required, Validators.min(10), Validators.max(99999999)])
      this.customerPreferenceForm.controls['virtualAgentServicePin'].updateValueAndValidity();
    }
  }

  isSelected3rdPartyRadio(e) {
    if (e.value == true) {
      this.customerPreferenceForm.controls['thirdPartyId'].enable();
      this.customerPreferenceForm.controls['thirdPartyCustomerCode'].enable();
      this.customerPreferenceForm.get('thirdPartyId').setValidators([Validators.required]);
      this.customerPreferenceForm.get('thirdPartyCustomerCode').setValidators([Validators.required]);
      this.customerPreferenceForm.controls['thirdPartyId'].updateValueAndValidity();
      this.customerPreferenceForm.controls['thirdPartyCustomerCode'].updateValueAndValidity();
    } else {
      this.customerPreferenceForm.controls['thirdPartyId'].disable();
      this.customerPreferenceForm.controls['thirdPartyCustomerCode'].disable();
      this.customerPreferenceForm.controls['thirdPartyId'].clearValidators();
      this.customerPreferenceForm.controls['thirdPartyId'].updateValueAndValidity();
      this.customerPreferenceForm.controls['thirdPartyId'].setValue('');
      this.customerPreferenceForm.controls['thirdPartyCustomerCode'].clearValidators()
      this.customerPreferenceForm.controls['thirdPartyCustomerCode'].updateValueAndValidity();
      this.customerPreferenceForm.controls['thirdPartyCustomerCode'].setValue('');
    }
  }

  onSubmitCustomerPreference() {
    if (this.customerPreferenceForm.value.virtualAgentServiceType === 1) {
      this.customerPreferenceForm.controls['virtualAgentServicePin'].clearValidators();
      this.customerPreferenceForm.controls['virtualAgentServicePin'].updateValueAndValidity();
      this.customerPreferenceForm.controls['virtualAgentServicePin'].setValue('');
    }
    else {
      this.customerPreferenceForm.get('virtualAgentServicePin').setValidators([Validators.required, Validators.min(10), Validators.max(99999999)])
      this.customerPreferenceForm.controls['virtualAgentServicePin'].updateValueAndValidity();
    }
    if (this.customerPreferenceForm.invalid) {
      return;
    }
    if (this.customerPreferenceForm.get('virtualAgentServiceType').value === 1) {
      delete this.customerPreferenceForm.value['virtualAgentServicePin'];
    } else {
      delete this.customerPreferenceForm.value['virtualAgentServicePassword'];
    }
    let payload = this.customerPreferenceForm.value;
    payload.customerAddressId = this.addressId;
    payload.partitionId = this.partitionId;
    payload.customerId = this.customerId;
    payload.createdUserId = this.loggedUser.userId;
    payload.isAutoFeedback = (this.customerPreferenceForm.value.isAutoFeedback == 'True' ||
      this.customerPreferenceForm.value.isAutoFeedback == 'true') ? true : false;
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_PREFERENCE, payload)
      .subscribe(() => this.rxjsService.setGlobalLoaderProperty(false));
  }

  customerCategoryFormSubmit() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.customerCategoryForm.invalid) {
      return;
    }
    let formVal = this.customerCategoryForm.getRawValue();
    formVal.originId = formVal.originId ? Number(formVal.originId) : formVal.originId;
    formVal.categoryId = formVal.categoryId ? Number(formVal.categoryId) : formVal.categoryId;
    formVal.installOriginId = formVal.installOriginId ? Number(formVal.installOriginId) : formVal.installOriginId;
    formVal.debtorGroupId = formVal.debtorGroupId ? Number(formVal.debtorGroupId) : formVal.debtorGroupId;
    formVal.customerClassificationMappingId = this.classificationDetails && this.classificationDetails.customerClassificationMappingId;
    formVal.createdUserId = this.loggedUser.userId;
    formVal.customerId = this.customerId;
    formVal.addressid = this.addressId;
    formVal.customerClassificationSubCategories = null;
    formVal.customerClassificationMappingId = null;
    formVal.itemOwnershipTypeId = formVal.itemOwnershipTypeId ? Number(formVal.itemOwnershipTypeId) : Number(1);
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CUSTOMER_CLASSIFICATION, formVal, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.onSequenceChangeEvent({ index: 11 }, false);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  ngOnDestroy() {
    this.rxjsService.setTabIndexForCustomerComponent(0);
    this.rxjsService.setTabIndexForMonitoringAndResponseComponent(0);
  }

  receivedSelectedRow(emitedData) {
    this.selectedRows = emitedData;
  }

  submitAVS(rowData) {
    let finalData = {
      "customerId": this.customerId,
      "debtorId": this.detailsObj?.debtorId,
      "debtorAccountDetailId": rowData ? rowData?.debtorAccountDetailId : '',
      "contractId": this.detailsObj?.contractId,
      "lastName": rowData ? rowData?.lastName : '',
      "firstName": rowData ? rowData?.firstName : '',
      "said": this.detailsObj?.debtorTypeId == 2 ? this.customerDebtorForm?.get('said')?.value : null,
      "accountHolderName": rowData ? rowData?.accountHolderName : '',
      "accountNumber": rowData ? rowData?.originalAccountNumber : '',
      "accountType": rowData ? rowData?.accountTypeName : '',
      "branchCode": rowData ? rowData?.bankBranchCode : '',
      "createdUserId": this.loggedUser?.userId,
      "debtorTypeId": this.detailsObj?.debtorTypeId,
      "registrationNumber": this.detailsObj?.debtorTypeId == 1 ? this.detailsObj?.companyRegNo : null,
      "debtorAdditionalInfoId": this.detailsObj?.debtorAdditionalInfoId,
      "passportExpiryDate": this.customerDebtorForm?.get('said')?.value ? null : rowData?.passportExpiryDate,
      "passportNo": this.customerDebtorForm?.get('said')?.value ? null : rowData?.passportNo
    }
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.DEBTOR_AVS_VERIFICATION, finalData).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.changeDebtorType();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  navigateToAvsView(rowData) {
    if (rowData?.debtorAccountDetailId) {
      let queryParams = { DebtorAccountDetailId: rowData?.debtorAccountDetailId, customerId: this.customerId, };
      if (this.feature && this.featureIndex) {
        queryParams['feature_name'] = this.feature;
        queryParams['featureIndex'] = this.featureIndex;
      }
      this.router.navigate(["customer/manage-customers/avs-check"], { queryParams: queryParams, skipLocationChange: true });
    } else {
      this.snackbarService.openSnackbar("Avs Check is pending", ResponseMessageTypes.ERROR);
      return;
    }
  }

  callbackDateTime;
  onModelChanged() {
    setTimeout(() => {
      let row = this.callbackDateTime ? { searchColumns: { callbackDateTime: this.callbackDateTime } } : {};
      this.onCRUDRequested(CrudType.GET, row);
    }, 500);
  }

  validateAndSearchCreditController() {
    let object = { referenceId: this.customerId, userId: this.loggedUser?.userId };
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_CREDIT_CONTROL_ASSIGN_DEBTOR_VALIDATE, undefined, false, prepareRequiredHttpParams(object)).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        let queryParams = { customerId: this.customerId, contractId: this.subscription, currentDivisionId: response.resources, debtorId: this.detailsObj.debtorId, ...this.activatedRoute.snapshot.queryParams };
        if (this.feature && this.featureIndex) {
          queryParams['feature_name'] = this.feature;
          queryParams['featureIndex'] = this.featureIndex;
        }
        this.router.navigate(['customer/manage-customers/credit-controller/search-credit-controller'], { queryParams: queryParams, skipLocationChange: true })
      } else {
        this.openCustomDialog(response.message, "Assign Credit Controller", false)
      }
    })
  }

  openCustomDialog(customText, header, boolean) {
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: header,
      showHeader: true,
      closable: true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText, isShowNo: boolean },
    });
    confirm.onClose.subscribe((resp) => {
    });
  }

  showAccountNumber(rowData, index) {
    if (!this.primengTableConfigPropertiesDebtorObj.tableComponentConfigs.tabsList[0].canAccountNumber) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.debtorInfoDataList[index].isShowAccountNumber = !this.debtorInfoDataList[index].isShowAccountNumber;
    if (this.debtorInfoDataList[index].isShowAccountNumber) {
      this.debtorInfoDataList[index].accountNo = this.debtorInfoDataList[index].originalAccountNumber
    } else {
      this.debtorInfoDataList[index].accountNo = this.debtorInfoDataList[index].accountNoHashed
    }
  }
}
