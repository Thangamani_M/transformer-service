import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { AddPromiseToPayComponent } from "./add-promise-to-pay.component";


@NgModule({
    declarations: [AddPromiseToPayComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: AddPromiseToPayComponent, data: { title: 'Add Promise To Pay' }
        },
    ])
  ],
  entryComponents: [],
  providers: []
})
export class AddPromiseToPayModule { }