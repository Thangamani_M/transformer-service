import { Component } from "@angular/core";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from "@app/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";
import { map } from "rxjs/operators";

@Component({
  selector: 'app-promise-to-pay-quote-detail',
  templateUrl: './promise-to-pay-quote-detail.component.html'
})

export class PromiseToPayDetailComponent {
  startTodayDate = new Date();
  loggedUser;
  quoationDetail;
  paymentDetails = [];

  constructor(private store: Store<AppState>,
    private crudService: CrudService, public config: DynamicDialogConfig, public ref: DynamicDialogRef, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }
  ngOnInit(): void {
    this.quoationDetail = this.config.data.row;
    if (this.quoationDetail.quotationVersionId)
      this.getPaymentDetail(this.quoationDetail.quotationVersionId);
  }
  getPaymentDetail(id) {
    let params = { ...{ QuotationVersionId: id } }
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PROMISE_TO_PAY_DETAILs, null, false, prepareRequiredHttpParams(params))
      .pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
          res?.resources?.forEach(val => {
            val.ptpDate = val.ptpDate ? new Date(val.ptpDate) : '';
            return val;
          })
        }
        return res;
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.paymentDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false)
      });
  }
  btnCloseClick() {
    this.ref.close(false);
  }
}