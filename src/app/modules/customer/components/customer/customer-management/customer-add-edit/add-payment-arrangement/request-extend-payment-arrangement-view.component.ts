import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { clearFormControlValidators, CrudService, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";

@Component({
  selector: 'app-quest-extend-payment-arrangement-view',
  templateUrl: './request-extend-payment-arrangement-view.component.html',
  styleUrls: ['./request-extend-payment-arrangement-view.component.scss'],
})

export class RequestExtendPaymentArrangementViewComponent {
  transactionProcessApproveForm: FormGroup;
  transactionProcessDeclineForm: FormGroup;
  primengTableConfigProperties: any;
  loggedUser: UserLogin;
  reasonList = [];
  fileName;
  approvePopupFlag: boolean = false;
  declinePopupFlag: boolean = false;
  referenceId;
  paDetails;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, 
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "View Customer",
      breadCrumbItems: [],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'View Customer',
            dataKey: '',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
            ],
            enableMultiDeleteActionBtn: false,
            enableRowDeleteActive: true,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false
          }
        ]
      }
    }
    // this.id = this.activatedRoute.snapshot.queryParams.creditNoteTransactionId;
    this.referenceId = this.activatedRoute.snapshot.queryParams.requestId;
    // let route1 = this.type == 'doa' ? { displayName: 'DOA Dashboard', relativeRouterUrl: '/collection/my-task-list' } : { displayName: 'Customer', relativeRouterUrl: '/customer/manage-customers/view-transactions', queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } };
    // this.primengTableConfigProperties.breadCrumbItems.push(route1);
    // let route2 = this.type == 'doa' ? { displayName: 'Bad Debit Reversal - DOA Approve', relativeRouterUrl: '' } : { displayName: 'Bad Debit Reversal', relativeRouterUrl: '' };
    // this.primengTableConfigProperties.breadCrumbItems.push(route2);
  }
  ngOnInit(){
    this.createTransactionProcessApproveForm();
    this.createTransactionProcessDeclineForm();
    this.getRequiredDetail();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  getRequiredDetail() {
    this.crudService.get(
        ModulesBasedApiSuffix.COLLECTIONS,
        CollectionModuleApiSuffixModels.PAYMENT_ARRANGEMENTS_EXTENDED_DETAILS,null, false,
        prepareRequiredHttpParams({PaymentArrangementId:'ced041b4-b6f5-4292-ac62-8a6bd95e2406'})
    ).subscribe(data => {
        if (data.isSuccess && data.statusCode == 200) {
            this.paDetails = data.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
    });
}
  createTransactionProcessApproveForm(): void {
    this.transactionProcessApproveForm = new FormGroup({
      'comments': new FormControl(null),
    });
    this.transactionProcessApproveForm = setRequiredValidator(this.transactionProcessApproveForm, ['comments']);
  }
  createTransactionProcessDeclineForm(): void {
    this.transactionProcessDeclineForm = new FormGroup({
      'declineReasonId': new FormControl(null),
      'comments': new FormControl(null),
    });
    this.transactionProcessDeclineForm = setRequiredValidator(this.transactionProcessDeclineForm, ['comments', 'declineReasonId']);
  }
  getReason() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_DECLINE_REASON)
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.reasonList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  obj: any;
  onFileSelected(files: FileList): void {
    const fileObj = files.item(0);
    this.fileName = fileObj.name;
    this.obj = fileObj;
    // this.getFilePath(fileObj);
  }
  approvePopup() {
    this.approvePopupFlag = !this.approvePopupFlag;
  }
  declinePopup() {
    this.declinePopupFlag = !this.declinePopupFlag;
  }
  approve() {

    if (this.transactionProcessApproveForm.invalid) return;

    let finalOjb = {
        roleId: this.loggedUser.roleId,
        approvedUserId: this.loggedUser.userId,
        comments: this.transactionProcessApproveForm.value.comments,
        referenceNo: this.referenceId,
        declineReasonId: 0
    }
    let formData = new FormData();
    formData.append("file", this.obj);
    formData.append('obj', JSON.stringify(finalOjb));
    let api = this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_DOA, formData);
    api.subscribe((res: any) => {
        if (res?.isSuccess == true && res?.statusCode == 200) {
            this.router.navigate(['/my-tasks/my-task-list']);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
    });
}
cancelApprove() {
    this.approvePopupFlag = !this.approvePopupFlag;
    this.transactionProcessApproveForm = clearFormControlValidators(this.transactionProcessApproveForm, ['comments']);
}
decline() {
    if (this.transactionProcessDeclineForm.invalid) return;

    let finalOjb = {
        roleId: this.loggedUser.roleId,
        approvedUserId: this.loggedUser.userId,
        comments: this.transactionProcessDeclineForm.value.comments,
        referenceNo: this.referenceId,
        declineReasonId: this.transactionProcessDeclineForm.value.declineReasonId,
    }
    let formData = new FormData();
    formData.append("file", this.obj);
    formData.append('obj', JSON.stringify(finalOjb));
    let api = this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BAD_DEBT_SUSPENSION_DOA, formData);
    api.subscribe((res: any) => {
        if (res?.isSuccess == true && res?.statusCode == 200) {
            this.router.navigate(['/my-tasks/my-task-list']);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  cancelDecline() {
      this.declinePopupFlag = !this.declinePopupFlag;
      this.transactionProcessDeclineForm = clearFormControlValidators(this.transactionProcessDeclineForm, ['comments', 'declineReasonId']);
  }
  navigate(){
    this.router.navigate(['/my-tasks/my-task-list']);
  }
}