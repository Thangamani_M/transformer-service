import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";

@Component({
    selector: 'app-request-reinstatement',
    templateUrl: './request-reinstatement.component.html',
    styleUrls: ['./request-reinstatement.component.scss']
})

export class RequestReinstatementComponent {
    primengTableConfigProperties: any;
    selectedTabIndex: any = 0;
    dataList: any;
    pageLimit: number[] = [10, 25, 50, 75, 100];
    row: any = {}
    loading: boolean;
    selectedRows=[];
    totalRecords: any;
    observableResponse: any;
    loggedUser: any;
    status: any = [];
    customerId;
    requestReinstatementForm: FormGroup;
    bankDetailFileObj;
    customerReinstatementRequestObj;
    bankDetailFileName;
    customerReinstatementRequestFileName;
    addressId;
    debtorId;
    formData = new FormData();
    constructor(private snackbarService: SnackbarService, private httpCancelService: HttpCancelService,private router:Router,private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: "Re-Instatement Request",
            breadCrumbItems: [{ displayName: 'Customer Management', relativeRouterUrl: '' }, { displayName: 'Customer', relativeRouterUrl: '' }, { displayName: 'Re-Instatement Request', relativeRouterUrl: '' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Re-Instatement Request',
                        dataKey: 'serviceSuspendId',
                        captionFontSize: '21px',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: true,
                        enableRowDelete: false,
                        enableFieldsSearch: false,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                            { field: 'suspensionId', header: 'Suspension Id', width: '200px' },
                            { field: 'contractNo', header: 'Contract No', width: '200px' },
                            { field: 'suspendedService', header: 'Suspended Service', width: '200px' },
                            { field: 'suspendedDate', header: 'Suspended On Date', width: '200px' }
                        ],
                        enableMultiDeleteActionBtn: false,
                        enableAddActionBtn: false,
                        shouldShowFilterActionBtn: false,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        apiSuffixModel: CollectionModuleApiSuffixModels.BAD_DEBT_REINSTATEMENT_SUSPENDED_DETAIL,
                        moduleName: ModulesBasedApiSuffix.COLLECTIONS,
                    }
                ]
            }
        }
        this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
        this.debtorId =  this.activatedRoute.snapshot.queryParams.debtorId;
        this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    }
    ngOnInit(): void {
        this.createForm()
        this.getDealerTypeListData();
        this.rxjsService.setGlobalLoaderProperty(false);
    }
    createForm(): void {
        this.requestReinstatementForm = new FormGroup({
            'isExistingBankDetails': new FormControl(null),
            'notes': new FormControl(null),
        });
        this.requestReinstatementForm = setRequiredValidator(this.requestReinstatementForm, ['isExistingBankDetails']);
    }
    getDealerTypeListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
        otherParams = { ...otherParams, ...{ customerId: this.customerId} }
        this.loading = true;
        let dealerModuleApiSuffixModels: CollectionModuleApiSuffixModels;
        dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

        this.crudService.get(
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
            dealerModuleApiSuffixModels,
            undefined,
            false, prepareRequiredHttpParams(otherParams)
        ).subscribe((data: IApplicationResponse) => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.observableResponse = data.resources;
                this.dataList = this.observableResponse;
                this.totalRecords = data.totalCount;
            } else {
                this.observableResponse = null;
                this.dataList = this.observableResponse
                this.totalRecords = 0;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    obj: any;
    onFileSelected(files: FileList, type): void {
        const fileObj = files.item(0);
        const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
        if (type == 'bank-detail') {
            this.bankDetailFileObj = fileObj;
            this.bankDetailFileName= fileObj.name;
        } else {
            this.customerReinstatementRequestObj = fileObj;
            this.customerReinstatementRequestFileName= fileObj.name;
        }
        //   this.obj = fileObj;
        //   if (this.obj.name) {
        //   }
    }
    onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
        switch (type) {
            case CrudType.GET:
                this.getDealerTypeListData(row["pageIndex"], row["pageSize"], unknownVar)
                break;
            default:
        }
    }
    onChangeSelecedRows(e) {
        this.selectedRows = e;
    }
    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }
    onSubmit() {
        if( this.selectedRows && this.selectedRows.length == 0){
            this.snackbarService.openSnackbar("Please select atleast one Service Suspension",ResponseMessageTypes.WARNING);
            return;
        }
        if (this.requestReinstatementForm?.invalid) { return; }
        this.formData = new FormData();
        let formValue = this.requestReinstatementForm.value;
        let serviceSuspendIds= [];
        this.selectedRows.forEach((element)=>{
            serviceSuspendIds.push(element.serviceSuspendId);
        });
        let finalObj = {
            serviceSuspendId: serviceSuspendIds.toString(),
            isExistingBankDetails: formValue.isExistingBankDetails,
            bankDetailFileName: this.bankDetailFileObj?.name,
            bankDetailFilePath: null,
            customerReinstatementRequestFileName: this.customerReinstatementRequestObj?.name,
            customerReinstatementRequestFilePath: null,
            notes: formValue.notes,
            creditControllerId: this.loggedUser.userId,
            ticketId: null,
            customerId :  this.customerId,
            createdUserId: this.loggedUser.userId
        }

        if(this.bankDetailFileObj)
          this.formData.append("file", this.bankDetailFileObj);
        if(this.customerReinstatementRequestObj)
           this.formData.append("file", this.customerReinstatementRequestObj);

        this.formData.append('Obj', JSON.stringify(finalObj));
        let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BAD_DEBT_REINSTATEMENT, this.formData);
        api.subscribe((res: any) => {
            if (res?.isSuccess == true && res?.statusCode == 200) {
            }
            this.rxjsService.setDialogOpenProperty(false);
        });
    }
    btnCloseClick(){
        this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } });
    }
}
