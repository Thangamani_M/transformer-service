import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, CrudService, CustomDirectiveConfig, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { loggedInUserData } from "@modules/others";
import { TelephonyApiSuffixModules } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'add-proof-of-payments',
  templateUrl: './add-proof-of-payments.component.html'
})
export class AddProofOfPaymentsComponent implements OnInit {
  promiseToPayId;
  type;
  customerId;
  debtorId;
  addProofofpaymentsForm: FormGroup;
  campaignId;
  callOutcomeList = [];
  loggedInUserData: LoggedInUserModel;
  addressId;
  uniqueCallId;
  startTodayDate = new Date();
  startTodayDate1 = new Date();
  agentExtensionNo;
  Outcomenotes;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  debtorBalance;

  constructor(private momentService: MomentService,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    public router: Router) {
    this.startTodayDate.setDate(this.startTodayDate.getDate() + 3);
    this.promiseToPayId = this.activatedRoute.snapshot.queryParams.promiseToPayId;
    this.type = this.activatedRoute.snapshot.queryParams.type;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.debtorId = this.activatedRoute.snapshot.queryParams.debtorId;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.campaignId = this.activatedRoute.snapshot.queryParams.campaignId;
  }

  ngOnInit(): void {
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
    this.getCallEscalation();
    this.combineLatestNgrxStoreData();
    this.createForm();
    this.getCallOutcomeDropdown();
    this.addProofofpaymentsForm.get('callOutcomeId').valueChanges.subscribe(val => {
      if (!val) {
        return;
      }
      this.changeOutcomeId(val);
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.store.pipe(select(agentLoginDataSelector))])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.agentExtensionNo = response[1];
      });
  }

  getCallEscalation() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PROMISE_TO_PAY_DEBATOR_OUTSTANDING,
      null,
      undefined,
      prepareGetRequestHttpParams(null, null, {
        debtorId: this.debtorId
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.debtorBalance = res.resources
          this.addProofofpaymentsForm.get('outstandingBalance').setValue(res.resources.outstandingBalance);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  createForm(): void {
    this.addProofofpaymentsForm = this.formBuilder.group({
      'callOutcomeId': new FormControl(null),
      'callWrapupId': new FormControl(null),
      'outstandingBalance': new FormControl(null),
      'requestDate': new FormControl(null),
      'dueDate': new FormControl(null)
    });
    this.addProofofpaymentsForm = setRequiredValidator(this.addProofofpaymentsForm, ['callOutcomeId', "callWrapupId", 'outstandingBalance', 'requestDate', 'dueDate']);
  }

  // Get Method
  changeOutcomeId(val) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_OUTCOME_CALLWRAPUP, null, false,
      prepareGetRequestHttpParams(null, null,
        { CallOutcomeId: val }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.Outcomenotes = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCallOutcomeDropdown() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_CALL_OUTCOME)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.callOutcomeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit() {
    if (this.addProofofpaymentsForm.invalid && !this.uniqueCallId) return;
    let formValue = this.addProofofpaymentsForm.value;
    formValue.requestDate = (formValue.requestDate && formValue.requestDate != null) ? this.momentService.toMoment(formValue.requestDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    formValue.dueDate = (formValue.dueDate && formValue.dueDate != null) ? this.momentService.toMoment(formValue.dueDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    let finalOjb = {
      campaignId: this.campaignId,
      debtorId: this.debtorId,
      customerId: this.customerId,
      addressId: this.addressId,
      callOutcomeId: formValue.callOutcomeId,
      callWrapupId: formValue.callWrapupId,
      outstandingBalance: formValue.outstandingBalance,
      requestDate: formValue.requestDate,
      paymentMethodId: formValue.paymentMethodId,
      description: formValue.description,
      dueDate: formValue.dueDate,
      createdUserId: this.loggedInUserData.userId,
      callWrapupDuration: 10,
      uniqueCallId: this.uniqueCallId
    }
    Object.keys(finalOjb).forEach(key => {
      if (finalOjb[key] == "" || finalOjb[key] == null) {
        delete finalOjb[key]
      }
    });
    this.disconnectCall();
    let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROOF_OF_PAYMENT, finalOjb);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.router.navigate(['/collection/call-wrap-up-codes/proof-of-payment'])
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  disconnectCall() {
    const payload = {
      from: this.agentExtensionNo,
      pabxId: this.loggedInUserData.openScapePabxId,
      userId: this.loggedInUserData.userId,
    };
    this.crudService.create(ModulesBasedApiSuffix.TELEPHONY, TelephonyApiSuffixModules.DISCONNECT, payload).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.rxjsService.setscheduleCallbackDetails(null);
        this.rxjsService.setCustomerContactNumber(null);
        this.rxjsService.setInboundCallDetails(null);
        this.rxjsService.setAnyPropertyValue(null);
      }
    });
  }
}
