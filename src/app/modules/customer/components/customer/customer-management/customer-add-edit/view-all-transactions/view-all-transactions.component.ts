import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PaymentInvoiceFormModel } from '@modules/customer/models';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-view-all-transactions',
  templateUrl: './view-all-transactions.component.html',
  styleUrls: ['./../customer-view-transactions.component.scss']
})
export class ViewAllTransactionsComponent implements OnInit {
  @ViewChildren('input') inputs: QueryList<any>;

  transactionDetails: any = []
  debtorTransactions: any = { debtorDetails: {}, debtorCustomerDetailsDTO: [] }
  pageLimit: number[] = [5, 25, 50, 75, 100];
  pageIndex: number = 0
  limit: number = 5;
  selectedRowInvoiceId
  selectedRowsTemp = []
  selectedRows = []
  debitAmount = 0;
  outstandingAmount = 0;
  creditAmount = 0;
  selectedTabIndex = 0
  paymentInvoiceForm: FormGroup
  loggedInUserData: LoggedInUserModel;
  templateList = []
  isCheckedEmail = false;
  invoiceInfo = false
  debtorId = "";
  isNotInvoiceType = false;
  totalRecords
  pageSize
  today= new Date()
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  status = []
  tables :any = {
    caption: 'TRANSACTIONS',
    dataKey: 'transactionRefNo',
    enableBreadCrumb: true,
    enableScrollable: true,
    checkBox: true,
    rowExpantableIndex: 0,
    enableHyperLink: true,
    cursorLinkIndex: 0,
    enableSecondHyperLink: false,
    cursorSecondLinkIndex: 1,
    columns: [{ field: 'postDate', header: 'Post Date', width: '200px' },
    { field: 'transactionType', header: 'Type', width: '100px' },
    { field: 'transactionRefNo', header: 'Transaction Number', width: '200px' },
    { field: 'transactionDescription', header: 'Description', width: '150px' },
    { field: 'referenceNumber', header: 'Reference No', width: '150px' },
    { field: 'debitAmount', header: 'Debit', width: '100px' },
    { field: 'creditAmount', header: 'Credit', width: '100px' },
    { field: 'outstandingAmount', header: 'Outstanding', width: '100px' },
    { field: 'paymentStatus', header: 'Payment Status', width: '100px' },
    ],
    enableRowDelete:false,
    enableFieldsSearch:false,
    rowExpantable:false,
    resizableColumns:false,
    reorderableColumns:false,

  }

  columns = [
    { field: 'batchName', header: 'Batch Number', width: '150px' },
    { field: 'division', header: 'Division', width: '150px', },
    { field: 'date', header: 'Date', width: '150px', isDate: true },
    { field: 'noOfRecords', header: 'Number of Records', width: '150px' },
    { field: 'total', header: 'Total', width: '150px' },
  ];
  addressId = ""
  customerId = ""
  fromUrl = ""
  constructor(private crudService: CrudService, private rxjsService: RxjsService, private _fb: FormBuilder, private store: Store<AppState>,
    private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute, private router : Router) {
    this.activatedRoute.queryParams?.subscribe(params => {
      this.debtorId = params['debtorId'],
      this.customerId =  params['id']
      this.addressId = params['addressId']
      this.fromUrl = params['fromUrl']

    })
  }

  ngOnInit() {
    this.getDetails()
    this.getTemplate()
    this.combineLatestNgrxStoreData()
    this.transactionDetails = [
      { name: "Debtor Code", value: "" },
      { name: "Contact Number", value: "" },
      { name: "Debtor Name", value: "" },
      { name: "Outstanding Balance", value: "" },
      { name: "Billing Address", value: "" },
    ]
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData)])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      })
  }

  onCRUDRequested(type?,row?,searchObj?){}



  getTemplate() {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UX_EMAIL_TEMPLATE,
      undefined,
      false, null, 1).subscribe(data => {
        this.templateList = data.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    // this.httpService.dropdown(ModulesBasedApiSuffix.BILLING, ),
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEBTOR_CUSTOMER_TRANSACTIONS, undefined, false, prepareRequiredHttpParams({ debtorId: this.debtorId })).subscribe(response => {
      if (response.isSuccess) {
        this.rxjsService.setGlobalLoaderProperty(false)
        this.debtorTransactions = response.resources
        this.transactionDetails = [
          { name: "Debtor Code", value: response?.resources?.debtorDetails?.debtorRefNo },
          { name: "Contact Number", value: response?.resources?.debtorDetails?.contactNumber },
          { name: "Debtor Name", value: response?.resources?.debtorDetails?.debtorName },
          { name: "Outstanding Balance", value: response?.resources?.debtorDetails?.outstandingBalance, isRandSymbolRequired: true },
          { name: "Billing Address", value: response?.resources?.debtorDetails?.address },
        ]
        this.onTabOpen({ index: 0 })
      }
    })
  }

  onRowSelect(event) {
    this.selectedRowInvoiceId = event?.data.referenceId
    this.selectedRowsTemp = this.selectedRows;
  }
  checkboxSelected(rowData, event) {}
  navigateToPage() {
    if(this.fromUrl == 'customer'){
      this.router.navigate(['customer/manage-customers/view', this.customerId], { queryParams: { addressId: this.addressId ,navigateTab:9 }, skipLocationChange: true })
    }else{
      this.router.navigate(["customer/advanced-search"])

    }
  }
  onRowUnselect(event) {}

  onTabOpen(e) {
    this.selectedTabIndex = e.index;
    this.getTransactionsDetails();

  }

  getTransactionsDetails() {
    let customerDetails = this.debtorTransactions.debtorCustomerDetailsDTO[this.selectedTabIndex];
    if (this.debtorTransactions.debtorCustomerDetailsDTO[this.selectedTabIndex].PAYMENT_TRANSACTION_LIST) return;

    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEBTOR_CUSTOMER_TRANSACTIONS_LIST, undefined, false, prepareRequiredHttpParams({ customerId: customerDetails?.customerId, debtorId: this.debtorId })).subscribe(response => {
      if (response.isSuccess) {
        response?.resources?.debtorCustomerTransactions.forEach(item => {
          item.customerId = this.debtorTransactions.debtorCustomerDetailsDTO[this.selectedTabIndex].customerId
        })
        this.debtorTransactions.debtorCustomerDetailsDTO[this.selectedTabIndex].paymentTransactions = response?.resources?.debtorCustomerTransactions;
        this.debtorTransactions.debtorCustomerDetailsDTO[this.selectedTabIndex]['total'] = response.resources?.total
      }
      this.rxjsService.setGlobalLoaderProperty(false)

    })
  }
  createInvoiceForm() {
    let paymentInvoiceFormModel = new PaymentInvoiceFormModel();
    this.paymentInvoiceForm = this._fb.group({
    });
    Object.keys(paymentInvoiceFormModel).forEach((key) => {
      this.paymentInvoiceForm.addControl(key, new FormControl(paymentInvoiceFormModel[key]));
    });
    this.paymentInvoiceForm = setRequiredValidator(this.paymentInvoiceForm, ["isCustomer", "isDebtor", "customerEmail", "debtorEmail", "templateId"]);
  }

  emailCheck() {
    if (this.paymentInvoiceForm.value.isCustomer) {
      this.paymentInvoiceForm.get('customerEmail').setValidators(Validators.required)
      this.paymentInvoiceForm.get('customerEmail').updateValueAndValidity();
    } else {
      this.paymentInvoiceForm.get('customerEmail').clearValidators();
      this.paymentInvoiceForm.get('customerEmail').updateValueAndValidity();
    }
    if (this.paymentInvoiceForm.value.isDebtor) {
      this.paymentInvoiceForm.get('debtorEmail').setValidators(Validators.required)
      this.paymentInvoiceForm.get('debtorEmail').updateValueAndValidity();
    } else {
      this.paymentInvoiceForm.get('debtorEmail').clearValidators();
      this.paymentInvoiceForm.get('debtorEmail').updateValueAndValidity();
    }
    if (this.paymentInvoiceForm.value.isOther) {
      this.paymentInvoiceForm.get('otherEmail').setValidators(Validators.required)
      this.paymentInvoiceForm.get('otherEmail').updateValueAndValidity();
      this.paymentInvoiceForm.get('otherEmail').setValue('');
    } else {
      this.paymentInvoiceForm.get('otherEmail').clearValidators();
      this.paymentInvoiceForm.get('otherEmail').updateValueAndValidity();
      this.paymentInvoiceForm.setErrors(null)
    }

    this.inputs.forEach((item) => {
      item.nativeElement.focus();
      item.nativeElement.blur();
    });


    if (this.paymentInvoiceForm.value.isCustomer || this.paymentInvoiceForm.value.isDebtor || this.paymentInvoiceForm.value.isOther) {
      this.isCheckedEmail = true
    } else {
      this.isCheckedEmail = false

    }
  }

  submitInvoice() {
    if (this.paymentInvoiceForm.invalid) {
      return;
    }
    let invoiceIds = [];
    let customerIds = []
    let obj = this.paymentInvoiceForm.getRawValue();
    obj['createdUserId'] = this.loggedInUserData.userId;
    if (this.selectedRows && this.selectedRows.length > 0) {
      this.selectedRows.forEach(ele => {
        if (ele.invoiceId) {
          invoiceIds.push(ele.invoiceId)
        }
        if (ele.customerId) {
          customerIds.push(ele.customerId)
        }
      })
      obj['invoiceId'] = invoiceIds.toString();
      obj['customerIds'] = [...new Set(customerIds)].toString();
    }
    obj['isPDFFileFormat'] = false;
    obj['invoiceNotificationId'] = null;
    obj['isEmailRead'] = false;
    obj['emailReadDate'] = new Date();
    obj['isEmailDelivered'] = false;
    obj['isSMSSent'] = false;
    obj['isSMSDelivered'] = false;
    obj['debtorId'] = this.debtorId;
    obj['transactionType'] = "transaction";
    obj['transactionRefNo'] = null;
    obj['transactionDescription'] = null;
    obj['refNo'] = null;
    obj['isOpenTransactions'] = false
    obj['isAllocatedTransactions'] = null;
    obj['createdDate'] = new Date();


    Object.keys(obj).forEach(key => {
      if (obj[key] === "") {
        delete obj[key]
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.DEBTOR_TRANSACTIONS_EMAIL, obj).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setDialogOpenProperty(false);
      if (response.isSuccess) {
        this.invoiceInfo = false;
        this.selectedRows = []
      }
    });
  }
  getInvoiceInfo() {
    let invoiceId = [];
    this.selectedRows.forEach(ele => {
      if (ele.invoiceId) {
        invoiceId.push(ele.invoiceId)
      }
    })
    let find = this.templateList.find(item => item.displayName == "Statement");
    if (invoiceId.length == 0) {
      this.isNotInvoiceType = true
      this.paymentInvoiceForm.get('templateId').setValue(find?.id)
    } else {
      this.paymentInvoiceForm.get('templateId').setValue("")
      this.isNotInvoiceType = false;
    }
    this.paymentInvoiceForm.get('debtorEmail').setValue(this.debtorTransactions?.debtorDetails?.debtorEmail)
  }

  sendInvoice() {
    if (this.selectedRows.length == 0) {
      this.snackbarService.openSnackbar("Please select atleast one record", ResponseMessageTypes.WARNING);
    } else {
      this.invoiceInfo = true;
      this.createInvoiceForm();
      this.emailCheck()
      this.getInvoiceInfo();
    }
  }
}
