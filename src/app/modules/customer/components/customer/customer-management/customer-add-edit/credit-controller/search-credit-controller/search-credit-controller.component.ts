import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CrudType, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeAllFormValidators, ResponseMessageTypes, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog';
import { RxjsService } from '@app/shared/services/rxjs.services';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CreditControllerAssignModel, CreditControllerSearchModel } from '@modules/customer';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { UserModuleApiSuffixModels } from '@modules/user/shared/utils/user-module.enums';
import { Store, select } from '@ngrx/store';
import { DialogService } from 'primeng/api';

@Component({
  selector: 'app-search-credit-controller',
  templateUrl: './search-credit-controller.component.html',
  styleUrls: ['./search-credit-controller.component.scss']
})
export class SearchCreditControllerComponent implements OnInit {


  contractId = ""
  addressId = ""
  customerId = ""
  debtorId = ""
  currentDivisionId = ""
  loggedUser: UserLogin
  searchForm: FormGroup
  assignForm: FormGroup
  selectedRow: any
  notes = ""
  totalRecords: any;
  divisionList = []
  currentDivisionList = []
  pageLimit: any = [10, 25, 50, 75, 100];
  selectedTabIndex: any = 0;
  loading = false;
  dropdownLists: any
  dataList = []
  primengTableConfigProperties: any = {
    tableCaption: "Search Credit Controller",
    breadCrumbItems: [],
    selectedTabIndex: 0,
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Search Credit Controller',
          dataKey: 'creditControllerEmployeeId',
          captionFontSize: '21px',
          enableBreadCrumb: true,
          enableAction: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          radioBtn: true,
          enableFieldsSearch: false,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          columns: [

            { field: 'creditControllerCode', header: 'Code' },
            { field: 'employeeName', header: 'Name' },
            { field: 'email', header: 'Email' },
            { field: 'mobileNumber', header: 'Phone No' },
            { field: 'creditControllerTypeName', header: 'Type Of Credit Controller ' },
          ],
          enableMultiDeleteActionBtn: false,
          enableAddActionBtn: false,
          enableExportBtn: false,
          enableEmailBtn: false,
          shouldShowFilterActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          enableExportCSV: false
        }
      ]
    }
  }

  constructor(private rxjsService: RxjsService, private _fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private store: Store<AppState>,
    private snackbarService: SnackbarService,
    private router: Router, private dialogService: DialogService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.activatedRoute.queryParams.subscribe(param => {
      this.customerId = param['customerId']
      this.addressId = param['addressId']
      this.contractId = param['contractId']
      this.debtorId = param['debtorId']
      this.currentDivisionId = param['currentDivisionId']
    })
  }


  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.createSearchForm();
    this.getDropdownDetails()
    this.getDivisions()
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.searchCreditController(
              row["pageIndex"], row["pageSize"]);
            break;
        }
        break;

    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onAssignChange() {
    let boolean = this.searchForm.get("assignAnotherDivision").value;
    if (boolean) {
      this.searchForm = setRequiredValidator(this.searchForm, ['requestDivisionId']);
    } else {
      this.searchForm = clearFormControlValidators(this.searchForm, ['requestDivisionId']);
      this.searchForm.get("requestDivisionId").setValue("")
      this.searchForm.updateValueAndValidity()
    }
  }
  createSearchForm() {
    let searchModel = new CreditControllerSearchModel(this.activatedRoute.snapshot.queryParams);
    this.searchForm = this._fb.group({
    });
    Object.keys(searchModel).forEach((key) => {
      this.searchForm.addControl(key, new FormControl(searchModel[key]));
    });
    this.searchForm.get("currentDivisionId").setValue(this.currentDivisionId);
  }

  onChangeSelecedRows(e) {
    this.selectedRow = e
  }
  typeOfCreditControllerList = []
  getDropdownDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_CREDIT_CONTROL_ASSIGN_DEBTOR, undefined, false, prepareRequiredHttpParams({ customerId: this.customerId, addressId: this.addressId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.dropdownLists = response.resources?.creditControllerEmployees
          this.typeOfCreditControllerList =  response.resources?.creditControllerTypes
      }
      this.rxjsService.setGlobalLoaderProperty(false);

    })
  }
  getDivisions() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DIVISIONS, undefined, true).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.divisionList = []
        this.currentDivisionList = response.resources
        for (const iterator of response.resources) {
          if (iterator.id != this.currentDivisionId) {
            this.divisionList.push(iterator)
          }
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);

    })

  }

  onChangeCreditController(event){

    this.searchForm.get("creditControllerCode").setValue(event.value)
    this.searchForm.get("employeeName").setValue(event.value)
    this.searchForm.get("creditControllerTypeName").setValue({
      creditControllerTypeName: event.value.creditControllerTypeName,
      creditControllerTypeId: event.value.creditControllerTypeId
    })
  }

  searchCreditController(pageIndex = '0', pageSize = '10') {
    this.rxjsService.setFormChangeDetectionProperty(true)
    if (this.searchForm.invalid) return "";
    let obj = this.searchForm.value;
    obj.creditControllerCode = obj.creditControllerCode?obj.creditControllerCode.creditControllerCode:""
    obj.creditControllerTypeName = obj.creditControllerTypeName?obj.creditControllerTypeName.creditControllerTypeName:""
    obj.employeeName = obj.employeeName?obj.employeeName.employeeName:""
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_CONTROL_ASSIGN_DEBTOR, undefined, false, prepareGetRequestHttpParams(pageIndex, pageSize, obj)).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.dataList = response.resources;
        if (this.dataList.length == 0) {
          this.snackbarService.openSnackbar("No data found!", ResponseMessageTypes.WARNING)
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })


  }

  assignCreditController() {
    let obj = {
      creditControllerAssignToDebtorId: "",
      creditControllerEmployeeId: this.selectedRow.creditControllerEmployeeId,
      isActive: true,
      userId: this.loggedUser?.userId,
      requestDivisionId: this.searchForm.get("requestDivisionId").value,
      customerId: this.customerId,
      contractId: this.contractId,
      debtorId: this.debtorId,
      notes: this.notes
    }
    if (!obj.requestDivisionId) {
      delete obj.requestDivisionId
    }

    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_CONTROL_ASSIGN_DEBTOR, obj).subscribe(
      (response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.openCustomDialog(response.message, "Confirmation", false, true)
        } else {
          this.openCustomDialog(response.message, "Assign - Decline", false)
        }
      })

  }

  openCustomDialog(customText, header, boolean, isRedirect = false) {
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: header,
      showHeader: false,
      closable: true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText, isShowNo: boolean },
    });
    confirm.onClose.subscribe((resp) => {
      if (isRedirect) {
        this.goBack()
      }
    });
  }

  goBack() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

}
