import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchCreditControllerComponent } from './search-credit-controller/search-credit-controller.component';


const routes: Routes = [
  { path: 'search-credit-controller', component: SearchCreditControllerComponent, data:{title:"Search Credit Controller"} }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class CreditControllerRoutingModule { }
