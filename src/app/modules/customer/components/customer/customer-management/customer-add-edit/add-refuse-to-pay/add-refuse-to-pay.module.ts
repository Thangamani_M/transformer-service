import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CustomerTicketAddEditModule } from "../../customer-ticket/customer-ticket-add-edit.module";
import { AddRefuseToPayComponent } from "./add-refuse-to-pay.component";

@NgModule({
    declarations: [AddRefuseToPayComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    CustomerTicketAddEditModule,
    RouterModule.forChild([
        {
            path: '',  component: AddRefuseToPayComponent, data: { title: 'Add Refuse To Pay' } 
        },
    ])
  ],
  entryComponents: [],
  providers: []
})
export class AddRefuseToPayModule { }