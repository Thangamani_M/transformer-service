import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CreditControllerRoutingModule } from './credit-controller-routing.module';
import { SearchCreditControllerComponent } from './search-credit-controller/search-credit-controller.component';



@NgModule({
  declarations: [SearchCreditControllerComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    CreditControllerRoutingModule
  ]
})
export class CreditControllerModule { }
