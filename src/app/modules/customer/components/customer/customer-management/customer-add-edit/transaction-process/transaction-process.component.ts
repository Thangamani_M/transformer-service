import { DatePipe } from "@angular/common";
import { HttpParams } from "@angular/common/http";
import { Component } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator } from "@app/shared";
import { CrudService, RxjsService } from "@app/shared/services";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection/shared";
import { TransactionProcessModel } from "@modules/customer/models/transaction-process-model";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { DialogService } from "primeng/api";
import { TransactionProcessApprovalComponent } from "./transaction-process-approval-request.component";


@Component({
  selector: 'app-transaction-process',
  templateUrl: './transaction-process.component.html',
  styleUrls: ['./transaction-process.component.scss']
})

export class TransactionProcessComponent {

  transactionProcessForm: FormGroup;
  primengTableConfigProperties: any;
  loggedUser;
  selectedTabIndex: any = 0;
  dataList: any=[];
  pageLimit: number[] = [10, 25, 50, 75, 100];
  row: any = {}
  loading: boolean;
  selectedRows: any = [];
  descriptionTypeList = [];
  descriptionSubTypeList = [];
  descriptionSubTypeName: string;
  descriptionSubTypeId: string;
  totalRecords: any;
  fileName;
  transactionProcessData;
  requiredForms: any = [];
  optionalForms: any = [];
  totalExclVat = 0;
  totalVAT = 0;
  totalInclVat = 0;
  formData = new FormData();
  // id = '8BFF5AFC-FC47-4347-BD0E-FE4402D1AEC9';
  id: string;
  transactionType;
  requiredDocumentPath;
  additionalDocumentPath;
  telephoneDocumentPath;
  requiredDocumentName;
  additionalDocumentName;
  telephoneDocumentName;
  viewable: boolean = false;
  type;
  creditType;
  reasonList = [];
  requiredFormList = [];
  requiredFormListName = [];
  optionalFormList = [];
  optionalFormListName = [];
  startTodayDate = new Date();
  invoicePaymentAndOverrideIds;
  invoiceItemId = [];
  isSubmit: boolean = false;
  debtorId;
  addressId;
  ids;
  status;
  constructor(private router: Router,private activatedRoute: ActivatedRoute, private datePipe: DatePipe, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private momentService: MomentService, private dialogService: DialogService, private crudService: CrudService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Transaction Process",
      breadCrumbItems: [{ displayName: 'Customer Management', relativeRouterUrl: '' }, { displayName: 'Customer List', relativeRouterUrl: '' }, { displayName: 'Transaction Process', relativeRouterUrl: '', }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Transaction Process',
            dataKey: '',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'type', header: 'Type', width: '200px' },
              { field: 'stockId', header: 'Stock Id', width: '200px' },
              { field: 'stockName', header: 'Stock Name', width: '200px' },
              { field: 'glCode', header: 'GL Code', width: '200px' },
              { field: 'quantity', header: 'Quantity', width: '200px' },
              { field: 'unitPrice', header: 'Unit Price', width: '200px' },
              { field: 'exclVAT', header: 'Excl VAT', width: '200px' },
              { field: 'vat', header: 'Vat', width: '200px' },
              { field: 'vatCode', header: 'vatCode', width: '200px' },
              { field: 'inclVat', header: 'Amount', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableRowDeleteActive: true,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false
          }
        ]
      }
    }
    this.type = this.activatedRoute.snapshot.queryParams.type;
    this.id = this.activatedRoute.snapshot.queryParams.id;
    this.status = this.activatedRoute.snapshot.queryParams.status;
    this.debtorId = this.activatedRoute.snapshot.queryParams.debtorId;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.ids = this.activatedRoute.snapshot.queryParams.ids;
    this.invoicePaymentAndOverrideIds = this.activatedRoute.snapshot.queryParams.invoicePaymentAndOverrideIds;
    this.transactionType = this.activatedRoute.snapshot.queryParams.transactionType;
    this.creditType = this.activatedRoute.snapshot.queryParams.creditType;
    this.viewable = this.type == 'doa' ? true : false;
  }
  ngOnInit(): void {
    this.setDefaultForm();
    this.createForm();
    this.getDescriptionType();
    this.getRequiredListData();
    this.getReason();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  setDefaultForm() {
    this.requiredFormList.push({
      invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
      documentPath: '',
      documentName: '',
      isRequired: true
    });
    this.optionalFormList.push({
      invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
      documentPath: '',
      documentName: '',
      isRequired: false
    });
    this.requiredFormListName[0] = '';
    this.optionalFormListName[0] = '';
  }
  getReason() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_DECLINE_REASON)
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.reasonList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  createForm(transactionProcessModel?: TransactionProcessModel) {
    let dealerTypeModel = new TransactionProcessModel(transactionProcessModel);
    this.transactionProcessForm = this.formBuilder.group({});
    Object.keys(dealerTypeModel).forEach((key) => {
      this.transactionProcessForm.addControl(key, new FormControl());
    });
    this.transactionProcessForm = setRequiredValidator(this.transactionProcessForm, ["invoiceTransactionDescriptionId", "invoiceTransactionDescriptionSubTypeId", "transactionNotes"]);
    if(this.type != 'doa') {
      this.transactionProcessForm = setRequiredValidator(this.transactionProcessForm, ["sendCustomerCopy"]);
    }
  }
  getDescriptionType() {
    if (this.viewable) {
      let type = (this.transactionType == 'CN') ? CollectionModuleApiSuffixModels.UX_CREDIT_NOTES : CollectionModuleApiSuffixModels.UX_DEBIT_NOTES;
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, type, null, false, null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.descriptionTypeList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    } else {
      let type = (this.creditType == 'credit-notes') ? CollectionModuleApiSuffixModels.UX_CREDIT_NOTES : CollectionModuleApiSuffixModels.UX_DEBIT_NOTES;
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, type, null, false, null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.descriptionTypeList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }

  }
  onChangeDescriptionType(id) {
    if (id) {
      let params = { InvoiceTransactionDescriptionId: id }
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_CREDIT_NOTES_DESCRIPTION_SUB_TYPE, null, false, prepareRequiredHttpParams(params)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.descriptionSubTypeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }
  onChangeDescriptionSubType(id) {
    let filter = this.descriptionSubTypeList.filter(x => x.id == id);
    this.descriptionSubTypeName = filter[0].displayName;
    this.descriptionSubTypeId = id;
  }
  getRequiredListData() {
    let id = this.id;
    let params = new HttpParams().set('TransactionId', id).set('Type', this.transactionType);
    let type = null;
    if (this.creditType == 'credit-notes' || this.creditType == 'debit-notes')
      type = (this.creditType == 'credit-notes') ? CollectionModuleApiSuffixModels.CREDIT_NOTES : CollectionModuleApiSuffixModels.DEBIT_NOTES;
    else if (this.transactionType == 'CN' || this.transactionType == 'DN')
      type = CollectionModuleApiSuffixModels.DOA_DETAIL;

    let api = (this.viewable) ? this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, type, null, false, params) : this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, type, id, false, null);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200 && response.resources) { 
        this.transactionProcessData = response.resources;
        this.transactionProcessForm.get('invoiceTransactionDescriptionId').setValue(this.transactionProcessData.transactionDetailsDTO.invoiceTransactionDescriptionId);
        this.onChangeDescriptionType(this.transactionProcessData.transactionDetailsDTO.invoiceTransactionDescriptionId);
        this.transactionProcessForm.get('invoiceTransactionDescriptionSubTypeId').setValue(this.transactionProcessData.transactionDetailsDTO.invoiceTransactionDescriptionSubTypeId);
        this.transactionProcessForm.get('transactionNotes').setValue(this.transactionProcessData.transactionDetailsDTO.transactionNotes);
        this.transactionProcessForm.get('sendCustomerCopy').setValue(this.transactionProcessData.transactionDetailsDTO.isSendCustomerCopy);
        this.transactionProcessForm.get('creditNoteDocumentDate').setValue(new Date(this.transactionProcessData.transactionDetailsDTO.documentDate));
        if(this.transactionProcessData.transactionDOASupportingDocuments){
          this.transactionProcessData.transactionDOASupportingDocuments.forEach(element => {
            if (element.isRequired)
              this.requiredForms.push({ documentName: element.documentName, documentPath: element.documentPath });
            else
              this.optionalForms.push({ documentName: element.documentName, documentPath: element.documentPath });
          });
        }
        let stocks = this.transactionProcessData.transactionInvoiceItem.stocks;
       // stocks.pop();
        if (stocks && stocks.length>0) {
          stocks.forEach(element => {
            if (element)
              this.invoiceItemId.push(element.invoiceItemId)
          });
        }
        if (this.transactionProcessData.transactionInvoiceItem)
          this.dataList = this.transactionProcessData.transactionInvoiceItem.stocks;

        if (this.dataList && this.dataList.length > 0) {
          this.dataList.forEach(element => {
            this.totalExclVat += element.exclVAT;
            this.totalVAT += element.vat;
            this.totalInclVat += element.inclVat;
          });
          let dat = {
            exclVAT: this.totalExclVat, vat: this.totalVAT, glCode: " ", inclVat: this.totalInclVat, invoiceId: " ", invoiceItemId: " ", quantity: " ",
            stockId: " ", stockName: " ", type: " ", unitPrice: "Total", vatCode: " "
          }
          this.dataList.push(dat);
        }

      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  obj: any;
  onFileSelected(files: FileList, type, index): void {
    const fileObj = files.item(0);
    this.fileName = fileObj.name;
    this.obj = fileObj;

    this.getFilePath(fileObj, type, index);
  }
  getFilePath(fileObj, type, index) {
    if (type == 'required-doc') {
      this.uploadDoc(fileObj, type).then((data) => {
        this.requiredFormList[index] = {
          invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
          documentPath: data,
          documentName: fileObj.name,
          isRequired: true
        };
      });
      this.requiredFormListName[index] = fileObj.name;
    }
    if (type == 'additional-doc') {
      this.uploadDoc(fileObj, type).then((data) => {
        this.optionalFormListName[index] = fileObj.name;
        this.optionalFormList[index] = {
          invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
          documentPath: data,
          documentName: fileObj.name,
          isRequired: false
        };
      });
    }

  }
  uploadDoc(fileObj, type) {
    this.formData = new FormData();
    return new Promise((resolve, reject) => {
      this.formData.append("file", fileObj);
      let type = this.creditType == 'credit-notes' ? CollectionModuleApiSuffixModels.CREDIT_NOTES_UPLOAD : CollectionModuleApiSuffixModels.DEBIT_NOTES_UPLOAD;
      let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, type, this.formData);
      api.subscribe((res: any) => {
        if (res?.isSuccess == true && res?.statusCode == 200) {
          resolve(res.resources);
        }
        else
          reject(null);

        this.rxjsService.setGlobalLoaderProperty(false);
      })
    });

  }
  addRequiredFormList() {
    this.requiredFormList.push({
      invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
      documentPath: '',
      documentName: '',
      isRequired: true
    });
    this.requiredFormListName.push('');
  }
  addOptionalFormList() {
    this.optionalFormList.push({
      invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
      documentPath: '',
      documentName: '',
      isRequired: false
    });
  }
  removeRequiredFormList(index) {
    this.requiredFormList.splice(index, 1);
    this.requiredFormListName.splice(index, 1);
  }
  removeOptionalFormList(index) {
    this.optionalFormList.splice(index, 1);
    this.optionalFormListName.splice(index, 1);
  }
  openTransactionProcessApproval(header, finalObj, actionFlag) {
    const ref = this.dialogService.open(TransactionProcessApprovalComponent, {
      header: header,
      showHeader: false,
      baseZIndex: 1000,
      width: '35vw',
      height: '280px',
      data: {
        finalObj: finalObj,
        screen: this.type,
        actionFlag: actionFlag,
        id: this.id,
        debtorId: this.debtorId,
        addressId: this.addressId,
        ids:this.ids,
        creditType: this.creditType,
        reasonList: this.reasonList
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        //this.getDealerMaintenanceListData()
      }
    });

  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.DELETE:
        this.deleteInvoice(row['invoiceItemId']);
        break;
    }
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  deleteInvoice(invoiceItemId) {
    if (this.invoiceItemId && this.invoiceItemId.length > 0) {
      let index = this.invoiceItemId.findIndex(x => x === invoiceItemId); //find index in your array
      this.invoiceItemId.splice(index, 1); 
       let secindex = this.dataList.findIndex(x => x.invoiceItemId == invoiceItemId);
       this.dataList.splice(secindex, 1);
      this.updateStockData();
    }
  }
  updateStockData() {
    this.dataList.pop();
    if (this.dataList.length > 0) {
      this.totalExclVat=0;this.totalVAT=0;this.totalInclVat=0;
      this.dataList.forEach((element,index) => {
        this.totalExclVat += element.exclVAT;
        this.totalVAT += element.vat;
        this.totalInclVat += element.inclVat;
      });
      let dat = {
        exclVAT: this.totalExclVat, vat: this.totalVAT, glCode: " ", inclVat: this.totalInclVat, invoiceId: " ", invoiceItemId: " ", quantity: " ",
        stockId: " ", stockName: " ", type: " ", unitPrice: "Total", vatCode: " "
      }
      this.dataList.push(dat);
    }
  }
  onSubmit(type) {
    this.isSubmit = true;
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    let creditNoteSupportingDocuments = [];
    let documents = this.requiredFormList.concat(this.optionalFormList);
    if(documents && documents.length >0){
      documents.forEach((ele) => {
        if (ele.documentPath != null && ele.documentPath != '')
          creditNoteSupportingDocuments.push(ele);
      });
    }

    if (!this.viewable &&  (this.transactionProcessForm.invalid || (this.requiredFormList && this.requiredFormList[0].documentPath == ''))) {
      return;
    }
    let formValue = this.transactionProcessForm.getRawValue();
    let finalObj = null;
    let creditNoteDocumentDate = this.transactionProcessData.transactionDetailsDTO.documentDate;
    creditNoteDocumentDate = (creditNoteDocumentDate && creditNoteDocumentDate != null) ? this.momentService.toMoment(creditNoteDocumentDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;

    // let stocks = this.transactionProcessData.transactionInvoiceItem.stocks;
    // stocks.pop();
    // if (stocks && stocks.length) {
    //   stocks.forEach(element => {
    //     if (element)
    //       invoiceItemId.push(element.invoiceItemId)
    //   });
    // }

    finalObj = {
      creditNoteDocumentDate: creditNoteDocumentDate,
      invoiceTransactionTypeId: this.transactionProcessData?.transactionDetailsDTO.invoiceTransactionTypeId,
      invoiceTransactionDescriptionId: formValue.invoiceTransactionDescriptionId,
      invoiceTransactionDescriptionSubTypeId: formValue.invoiceTransactionDescriptionSubTypeId,
      transactionNotes: formValue.transactionNotes,
      isSendCustomerCopy: formValue.sendCustomerCopy,
      isSendToDOAApproval: true,
      userId: this.loggedUser.userId,
      creditNoteSupportingDocuments: creditNoteSupportingDocuments,
      invoiceItemId: this.invoiceItemId,
      totalExclVat: this.totalExclVat,
      totalVAT: this.totalVAT,
      inclVat: this.totalInclVat,
    }
    // if(finalObj.invoicePaymentAndOverrideId){
    //   finalObj.invoicePaymentAndOverrideId: this.id;
    // }
    if (this.creditType == 'debit-notes') {
      finalObj.creditNoteTransactionId = this.transactionProcessData?.transactionDetailsDTO.creditNoteTransactionId;
      finalObj.invoicePaymentAndOverrideId = this.invoicePaymentAndOverrideIds;
    }
    else {
      finalObj.invoicePaymentAndOverrideId = this.id;
    }


    if (type == 'submit')
      this.openTransactionProcessApproval('Approval Request', finalObj, null);
    if (type == 'approve')
      this.openTransactionProcessApproval('Approve Transaction', finalObj, 'approve');

  }
  decline(type) {
    this.openTransactionProcessApproval('Decline Transaction', null, 'decline');
  }
  cancel(){
    this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.id, debtorId: this.debtorId, addressId: this.addressId } });
  }
}