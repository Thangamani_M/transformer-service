import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { clearFormControlValidators, CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, removeAllFormValidators, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";

@Component({
  selector: 'app-credit-and-reinvoice',
  templateUrl: './credit-and-reinvoice.component.html',
  styleUrls: ['./credit-and-reinvoice.component.scss']
})

export class CreditAndReinvoiceComponent {
  primengTableConfigProperties: any;
  loggedUser: UserLogin;
  selectedTabIndex: any = 0;
  dataList: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  totalRecords: any;
  row: any = {}
  loading: boolean;
  selectedRows: any = [];
  id; addressId; customerId; debtorId;
  type;
  firstHeading: string;
  secondHeading: string;
  thirdHeading: string;
  fourthHeading: string;
  creditReinvoiceForm: FormGroup;
  transactionProcessApproveForm: FormGroup;
  transactionProcessDeclineForm: FormGroup;
  firstSectionDetails;
  secondSectionDetails;
  transactionSection;
  thirdSectionDetails: any = [];
  formSectionDetails = [];
  optionalForms = [];
  requiredForms = [];
  viewable: boolean = false;
  totalExclVat = 0; totalVAT = 0; totalInclVat = 0;
  descriptionTypeList = [];
  descriptionSubTypeList = [];
  transactionDescription; descriptionId; descriptionSubId;
  transactionType; invoiceTransactionTypeId;
  uploadApi;
  optionName;
  formData = new FormData();
  requiredFormList = [];
  requiredFormListName = [];
  optionalFormList = [];
  optionalFormListName = [];
  descriptionTypeDocList = [];
  descriptionSubTypeDocList = [];
  descriptionSubTypeName = '';
  descriptionSubTypeId;
  descriptionTypeId
  fileName;
  docLength = 1;
  fileObject;
  fileObjArr = [];
  optionalFileObjArr = [];
  reasonList:any=[];
  startTodayDate = new Date();
  optionalNameTemp = ''; descriptionType;
  invoiceItemId = [];
  flag: boolean = false;
  submitFlag: boolean = false;
  approvePopupFlag: boolean = false;
  declinePopupFlag: boolean = false;
  transactionId;
  enableSubmit: boolean = true;
  customerDetails;
  status;
  creditNoteTransactionId;
  debtorAccountDetailId = ""
  isEditUnitPrice: boolean = false;
  editActionForm: FormGroup;
  isEditIndex;serviceType;
  taxPercentage;
  dataListTemp: any;
  nextTab : boolean = false;
  constructor(private activatedRoute: ActivatedRoute,private momentService: MomentService, private snackbarService: SnackbarService, private router: Router, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Credit and Re-Invoice",
      breadCrumbItems: [],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'CREDIT NOTE',
            dataKey: '',
            captionFontSize: '21px',
            disabled: false,
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'type', header: 'Type', width: '150px' },
              { field: 'stockId', header: 'Stock ID', width: '150px' },
              { field: 'stockName', header: 'Stock Name', width: '150px' },
              { field: 'qty', header: 'QTY', width: '120px',type :'QTY' ,visiblity:false},
              { field: 'unitPrice', header: 'Unit Price', width: '150px' },
              { field: 'exclVAT', header: 'Excl VAT', width: '150px' },
              { field: 'vat', header: 'VAT', width: '120px' },
              { field: 'inclVAT', header: 'Amount', width: '150px' },
              { field: 'vatCode', header: 'VAT Code', width: '150px' },
              { field: 'actions', header: 'Actions', width: '100px',type:'enableEditAction' }
            ],
            enableMultiDeleteActionBtn: false,
            enableRowDeleteActive: true,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false
          },
          {
            caption: 'INVOICE',
            dataKey: '',
            captionFontSize: '21px',
            disabled: true,
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'type', header: 'Type', width: '200px' },
              { field: 'stockId', header: 'Stock ID', width: '200px' },
              { field: 'stockName', header: 'Stock Name', width: '200px' },
              { field: 'qty', header: 'Quantity', width: '150px' },
              { field: 'unitPrice', header: 'Unit Price', width: '150px' },
              { field: 'exclVAT', header: 'Excl VAT', width: '150px' },
              { field: 'vat', header: 'VAT', width: '150px' },
              { field: 'inclVAT', header: 'Amount', width: '150px' },
              { field: 'vatCode', header: 'VAT Code', width: '200px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableRowDeleteActive: true,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false
          }
        ]
      }
    }
    this.firstHeading = 'CUSTOMER INFO';
    this.secondHeading = 'DEBTOR INFO';
    this.thirdHeading = 'OUTSTANDING BALANCE';
    this.fourthHeading = 'CREDIT AND RE-INVOICE';
    this.status = this.activatedRoute.snapshot.queryParams.status;
    this.id = this.activatedRoute.snapshot.queryParams.invoicePaymentAndOverrideIds;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.debtorId = this.activatedRoute.snapshot.queryParams.debtorId;
    this.type = this.activatedRoute.snapshot.queryParams.type;
    this.serviceType = this.activatedRoute.snapshot.queryParams.serviceType;
    this.transactionType = this.activatedRoute.snapshot.queryParams.transactionType;
    this.transactionId = this.activatedRoute.snapshot.queryParams.transactionId;
    this.debtorAccountDetailId = this.activatedRoute.snapshot.queryParams.debtorAccountDetailId;
    let route1 = this.type == 'doa' ? { displayName: 'DOA Dashboard', relativeRouterUrl: '/collection/my-task-list' } : { displayName: 'Customer Management: Transactions', relativeRouterUrl: '/customer/manage-customers/view-transactions', queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } };
    this.primengTableConfigProperties.breadCrumbItems.push(route1);
    let route2 = this.type == 'doa' ? { displayName: 'Credit and Re-Invoice- DOA Approve', relativeRouterUrl: '' } : { displayName: 'Credit and Re-Invoice', relativeRouterUrl: '' };
    this.primengTableConfigProperties.breadCrumbItems.push(route2);
    this.viewable = this.type == 'doa' ? true : false;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].disabled = (this.selectedTabIndex==0 || this.viewable ) ? false : true;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[1].disabled = (this.selectedTabIndex==1 || this.viewable ) ? false : true;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].enableRowDelete=false;

    if(this.serviceType?.toLowerCase() =='installation')
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns[3].visiblity=true;

    if(this.transactionType)
       this.transactionType == 'CN' ? this.primengTableConfigProperties.tableComponentConfigs.tabsList.splice(1, 1):  this.primengTableConfigProperties.tableComponentConfigs.tabsList.splice(0, 1);
  }
  ngOnInit() {
    this.createEditActionForm();
    this.getReason();
    this.getRequiredDetail();
  }
  createForm() {
    this.creditReinvoiceForm = this.formBuilder.group({});
    if (this.formSectionDetails && this.formSectionDetails.length > 0) {
      this.formSectionDetails.forEach((key) => {
        this.creditReinvoiceForm.addControl(key.formName, new FormControl());
      });
    }
    if (this.transactionSection && this.transactionSection.length > 0) {
      this.transactionSection.forEach((key) => {
        this.creditReinvoiceForm.addControl(key.formName, new FormControl());
      });
    }
    this.creditReinvoiceForm.addControl('transactionNote', new FormControl());
    this.creditReinvoiceForm.addControl('isSendCustomerCopy', new FormControl());
    this.creditReinvoiceForm = setRequiredValidator(this.creditReinvoiceForm, ["transactionNote"]);
    let formArray = this.formBuilder.array([]);
    this.creditReinvoiceForm.addControl('documentsDTOs', formArray);
    this.setOptionalDoc();
    if (this.viewable) {
        this.creditReinvoiceForm.get('descriptionType').setValue(this.customerDetails.customer.transactionDescription);
        this.creditReinvoiceForm.get('descriptionSubType').setValue(this.customerDetails.customer.subTypeDescription);
        this.creditReinvoiceForm.get('transactionNote').setValue(this.customerDetails.customer.transactionNotes);
        this.creditReinvoiceForm.get('isSendCustomerCopy').setValue(this.customerDetails.customer.isSendCustomerCopy);
        this.creditReinvoiceForm.get('transactionNote').disable();
        this.creditReinvoiceForm.get('isSendCustomerCopy').disable();
        this.creditReinvoiceForm.get('documentDate').disable();
    }
    if(this.customerDetails.customer.documentDate)
        this.creditReinvoiceForm.get('documentDate').setValue(new Date(this.customerDetails.customer.documentDate));

        this.valueWhenFormChange();
  }
  get transactionProcessFormArray(): FormArray {
    if (this.creditReinvoiceForm !== undefined) {
      return (<FormArray>this.creditReinvoiceForm.get('documentsDTOs'));
    }
  }
  createEditActionForm(): void {
    this.editActionForm = new FormGroup({
      'unitprice': new FormControl(null),
    });
    this.editActionForm = setRequiredValidator(this.editActionForm, ['unitprice']);
  }
  createTransactionProcessApproveForm(): void {
    this.transactionProcessApproveForm = new FormGroup({
      'comments': new FormControl(null),
    });
    this.transactionProcessApproveForm = setRequiredValidator(this.transactionProcessApproveForm, ['comments']);
  }
  createTransactionProcessDeclineForm(): void {
    this.transactionProcessDeclineForm = new FormGroup({
      'declineReasonId': new FormControl(null),
      'comments': new FormControl(null),
    });
    this.transactionProcessDeclineForm = setRequiredValidator(this.transactionProcessDeclineForm, ['comments', 'declineReasonId']);
  }
  getReason() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_DECLINE_REASON)
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.reasonList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getRequiredDetail() {
    this.rxjsService.setGlobalLoaderProperty(true);
    let params:any = {};
    if(this.selectedTabIndex==1){
      params = {InvoicePaymentandOverrideId: this.id,IsInvoice:true };
    }else{
      params = { InvoicePaymentandOverrideId: this.id ,};
    }
    params.debtorId =  this.debtorId;
    let creditNoteAPi = this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_AND_REINVOICE_CREDIT_NOTE_DOA_DETAIL, undefined, false, prepareRequiredHttpParams({ CreditNoteTransactionId: this.transactionId ,Type:'CN'}));
    let invoiceDoaApi = this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_AND_REINVOICE_INVOICE_DOA_DETAIL, undefined, false, prepareRequiredHttpParams({ InvoiceId: this.transactionId ,Type:'IN'}));
    let creditInvoiceApi= this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_AND_REINVOICE, undefined, false, prepareRequiredHttpParams(params));
    let api = this.type == 'doa' ? ((this.transactionType == 'CN') ? creditNoteAPi : (this.transactionType == 'IN') ? invoiceDoaApi : null) : creditInvoiceApi;
    if(api){
      api.subscribe((resp: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(true)
        if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
          this.customerDetails =  resp.resources;
          if(this.type == 'doa') {
            resp.resources.customer = resp.resources.creditReinvoiceDOADetails;
            this.customerDetails =  resp.resources;
          }
          this.setBadDebitWriteOffInvoiceDetailDTO(resp.resources);
          this.setBadDebitWriteOffOutstandingBalance(resp.resources);

          if(this.selectedTabIndex==0 && this.type!='doa'){
            this.dataList = resp.resources.creditReinvoiceCreditNoteItemsDTO?.creditReinvoiceItemDetails ? resp.resources.creditReinvoiceCreditNoteItemsDTO?.creditReinvoiceItemDetails :  resp.resources.creditReinvoiceCreditNoteItems?.creditReinvoiceItemDetails ? resp.resources.creditReinvoiceCreditNoteItems?.creditReinvoiceItemDetails : null;
            this.taxPercentage = resp.resources.creditReinvoiceCreditNoteItemsDTO?.taxPercentage;
            this.setTotal();
          } else if(this.type=='doa'){
               this.dataList = resp.resources.creditReinvoiceCreditNoteItemsDTO?.creditReinvoiceItemDetails ? resp.resources.creditReinvoiceCreditNoteItemsDTO?.creditReinvoiceItemDetails  : resp.resources.creditReinvoiceCreditNoteItems?.creditReinvoiceItemDetails ? resp.resources.creditReinvoiceCreditNoteItems?.creditReinvoiceItemDetails : null;
               this.taxPercentage = resp.resources.creditReinvoiceCreditNoteItemsDTO?.taxPercentage;
               this.setTotal();
          }else{
           // this.dataList = resp.resources.creditReinvoiceCreditNoteItemsDTO?.creditReinvoiceItemDetails ? resp.resources.creditReinvoiceCreditNoteItemsDTO?.creditReinvoiceItemDetails  : resp.resources.creditReinvoiceCreditNoteItems?.creditReinvoiceItemDetails ? resp.resources.creditReinvoiceCreditNoteItems?.creditReinvoiceItemDetails : null;
            if(this.nextTab)
              this.dataList = this.dataListTemp;
            else
             this.setTotal();

            this.taxPercentage = resp.resources.creditReinvoiceCreditNoteItemsDTO?.taxPercentage;
          }

          this.descriptionTypeList = resp.resources.invoiceTransactiondescriptionDTO;
          this.descriptionType = resp.resources.transactionDetailsDTO?.transactionDescription;
          if (resp.resources.creditReinvoiceDOASupportingDocuments) {
            resp.resources.creditReinvoiceDOASupportingDocuments.forEach(element => {
              if (element.isRequired)
                this.requiredForms.push({ documentName: element.documentName, documentPath: element.documentPath });
              else
                this.optionalForms.push({ documentName: element.documentName, documentPath: element.documentPath });
            });
          }
          // this.invoiceTransactionTypeId = resp.resources.transactionDetailsDTO.invoiceTransactionTypeId;
          // this.subTypeDescription =resp.resources.transactionDetailsDTO?.subTypeDescription;
        }
        else
          this.setBadDebitWriteOffInvoiceDetailDTO(null);
      },err=>{
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }
  setBadDebitWriteOffInvoiceDetailDTO(val) {

    let customer = val.customer;
    let debtor = this.viewable? val.customer : val.debtor ;
    this.firstSectionDetails = [
      { name: 'Customer ID', value: val.customer?.customerRefNo ?  val.customer?.customerRefNo : val.customer?.customerRegNo ? val.customer?.customerRegNo : '', valueColor: '#166DFF', enableHyperLink: true },
      { name: 'Customer Name', value: val.customer?.customerName ? val.customer?.customerName : val.customer?.customerType ?val.customer?.customerType:''},
      { name: 'Customer Type', value: customer?.customerTypeName ? customer?.customerTypeName : customer?.customerType ? customer?.customerType:'',  },
      { name: 'Site Address', value: customer?.siteAddress },
      { name: 'Contact Number', value: customer?.contactNumber },
      { name: 'Email Address', value: customer?.email ?  customer?.email :  customer?.emailAddress  ? customer?.emailAddress :'' },
      { name: 'Co Reg No', value: customer?.customerCompanyRegNo },
      { name: 'Vat No', value: customer?.debtorVatno }
    ];
    this.secondSectionDetails = [
      { name: 'Debtor Name', value: debtor?.debtorName }, { name: 'Debtor Code', value: debtor?.debtorRefNo? debtor?.debtorRefNo : val.customer?.debtorCode ? val.customer?.debtorCode : '' },
      { name: 'BDI Number', value: debtor?.bdiNumber ? debtor?.bdiNumber : val.customer?.debtorBDINumber ?  val.customer?.debtorBDINumber : '' }, { name: 'Contact Number', value: debtor?.contactNumber },
      { name: 'Email Address', value: debtor?.email ? debtor?.debtorEmail  : val.customer?.debtorCode ? val.customer?.debtorCode : '' }, { name: 'Co Reg No', value: debtor?.debtorCompanyRegNo ? debtor?.debtorCompanyRegNo : val.customer?.debtorCompanyRegNo  ?  val.customer?.debtorCompanyRegNo  :'' },
      { name: 'Vat No', value: debtor?.debtorVatno  ? debtor?.debtorVatno  :  val.customer?.debtorVatno   ?  val.customer?.debtorVatno :''}
    ];
    this.formSectionDetails = [
      { label: 'Region', formName: 'region', type: "text", value: customer?.regionName },
      { label: 'Division', formName: 'division', type: "text", value: customer?.divisionName },
      { label: 'Branch', formName: 'branch', type: "text", value: customer?.branchName },
      { label: 'Document Date', formName: 'documentDate', type: "date", value: customer?.documentDate ? new Date(customer?.documentDate) : new Date() },
      { label: 'Transaction Type', formName: 'transactionType', type: "text", value: customer?.transactionType },
      { label: 'Transaction Description', formName: 'transactionDescription', type: "text", value: customer?.transactionTypeDescription },
      { label: 'Description Type', formName: 'descriptionType', type: "select", value: customer?.invoiceTransactionDescriptionId },
      { label: 'Description Sub Type', formName: 'descriptionSubType', type: "select", value: customer?.invoiceTransactionDescriptionSubTypeId },
      { label: 'Reference ID', formName: 'reference', type: "text", value: customer?.invoiceRefNo },
    ];
    this.createForm();
    if(customer.documentDate){
        //this.creditReinvoiceForm.get('documentDate').setValue(new Date(customer.documentDate));
       // this.creditReinvoiceForm.get('documentDate').disable();
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  setBadDebitWriteOffOutstandingBalance(outstandingBalance) {
    outstandingBalance = outstandingBalance.outstandingBalance;
    this.thirdSectionDetails = [
      { label: 'D180', value: outstandingBalance?.days180, colsize: "outstanding-balance-col-width" },
      { label: 'D150', value: outstandingBalance?.days150, colsize: "outstanding-balance-col-width" },
      { label: 'D120', value: outstandingBalance?.days120, colsize: "outstanding-balance-col-width" },
      { label: 'D90', value: outstandingBalance?.days90, colsize: "outstanding-balance-col-width" },
      { label: 'D60', value: outstandingBalance?.days60, colsize: "outstanding-balance-col-width" },
      { label: 'D30', value: outstandingBalance?.days30, colsize: "outstanding-balance-col-width" },
      { label: 'Current', value: outstandingBalance?.current, colsize: "outstanding-balance-col-width" },
      { label: 'Balance', value: outstandingBalance?.totalInvoiceOutStandingAmount, colsize: "outstanding-balance-col-width" },
      // { label: 'Days Outstanding', value: 'Amount', colsize: "col-2" }
    ]
  }
  setTotal() {
    this.totalExclVat=0;this.totalVAT=0;this.totalInclVat=0;
    if (this.dataList && this.dataList.length > 0) {
      this.dataList.forEach(element => {
        element.inclVATOld =  element.inclVAT;
        this.totalExclVat += element.exclVAT;
        this.totalVAT += element.vat;
        this.totalInclVat += element.inclVAT;
      });
      let dat = {
        exclVAT: this.totalExclVat?.toFixed(2), vat: this.totalVAT?.toFixed(2), glCode: " ", inclVAT: this.totalInclVat?.toFixed(2), invoiceId: " ", invoiceItemId: " ", qty: " ",
        stockId: " ", stockName: " ", type: " ", unitPrice: "Total", vatCode: " "
      }
      this.dataList.push(dat);
    }
  }

  obj;
  onFileSelected(files: FileList, type, index): void {

    const fileObj = files.item(0);
    this.fileObject = fileObj;
    this.fileName = fileObj.name;
    if(type=='approve'){
      this.obj = fileObj;return;
    }
    if (this.fileObjArr) {
      let filename = this.fileObjArr.filter(x => x.name == fileObj.name);
      if (filename && filename.length > 0) {
        this.snackbarService.openSnackbar("File name exists already.Please upload file with other name.", ResponseMessageTypes.WARNING);
        return;
      }
    }
    this.obj = fileObj;
    if (type == 'optional-doc')
      this.optionalFileObjArr.push(fileObj);

    this.fileObjArr.push(fileObj);
    this.transactionProcessFormArray.value[index].fileName = fileObj.name;
    //this.emitDataWhenFileChange();
  }
  tabClick(e) {
    this.selectedTabIndex = e?.index;
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[ e?.index].disabled = false;
    this.dataListTemp = this.dataList;
    this.nextTab = true;
    this.getRequiredDetail();
  }
  clearRecord() {
    this.fileObjArr = [];
    this.optionalFileObjArr.forEach(file => {
      this.fileObjArr.push(file);
    });
    if (this.transactionProcessFormArray.value && this.transactionProcessFormArray.value.length > 0) {
      let all = this.transactionProcessFormArray.value;
      let required = this.transactionProcessFormArray.value.filter(x => x.isRequired == true);
      let optional = this.transactionProcessFormArray.value.filter(x => x.isRequired == false);
      if (required && required.length > 0) {
        Object.keys(required).forEach(key => {
          delete required[key];
        });
        let finalarr = []; finalarr = required.concat(optional);
        all.forEach((element, i) => {
          this.transactionProcessFormArray.removeAt(0);
        });
        finalarr.forEach((element, i) => {
          if (element) {
            this.transactionProcessFormArray.value.push(element);
          }
        });
      } else {
        all.forEach((element, i) => {
          this.transactionProcessFormArray.removeAt(0);
        });
        optional.forEach((element, i) => {
          if (element) {
            this.transactionProcessFormArray.value.push(element);
          }
        });
      }
    }
  }
  onChangeDescriptionType(id) {
    this.descriptionTypeId = id;
    this.descriptionSubTypeId = null;
    this.descriptionSubTypeList = [];
    this.creditReinvoiceForm.get('descriptionSubType').setValue(null);
    this.clearRecord();
    if (id) {
      if (this.descriptionTypeList && this.descriptionTypeList.length > 0) {
        let desArr = this.descriptionTypeList.filter(x => x.invoiceTransactionDescriptionId == id);
        if (desArr && desArr.length > 0) {
          if (desArr[0].subTypes && desArr[0].subTypes.length > 0)
            this.descriptionSubTypeList = desArr[0].subTypes;
          else
            this.descriptionSubTypeList = [];

          this.descriptionTypeDocList = desArr[0].documents;
          if ((this.descriptionSubTypeList && this.descriptionSubTypeList.length == 0) && this.descriptionTypeDocList.length > 0) {
            this.setDescriptionTypeDoc();
          }
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }
  onChangeDescriptionSubType(id) {
    this.descriptionSubTypeId = id;
    this.clearRecord();
    if (this.descriptionSubTypeList && this.descriptionSubTypeList.length > 0) {
      let desSubTypeArr = this.descriptionSubTypeList.filter(x => x.invoiceTransactionDescriptionSubTypeId == id);
      if (desSubTypeArr && desSubTypeArr.length > 0) {
        this.descriptionSubTypeDocList = desSubTypeArr[0].documents;
        if (this.descriptionSubTypeDocList && this.descriptionSubTypeDocList.length > 0)
          this.setDescriptionSubTypeDoc();
        else if (this.descriptionTypeDocList.length > 0)
          this.setDescriptionTypeDoc();
      }
    }
  }
  setOptionalDoc() {
    let obj = this.formBuilder.group({
      invoiceTransactidocumentCountonDescriptionSubTypeDocumentId: null,
      invoiceTransactionDescriptionDocumentId: null,
      documentCount: this.formBuilder.array([]),
      documentName: this.formBuilder.array([]),
      docName: null,
      fileName: null,
      CustomDocumentName: this.formBuilder.array([]),
      isRequired: false
    });
    this.transactionProcessFormArray.push(obj);
  }
  onChange(val, index) {
    this.optionalNameTemp = val;
    if (this.transactionProcessFormArray.value[index].isRequired == false)
      this.transactionProcessFormArray.value[index].CustomDocumentName.push(val);

    //this.emitDataWhenFileChange();
  }
  setDescriptionTypeDoc() {
    let temp = this.transactionProcessFormArray.value;
    this.descriptionTypeDocList.forEach(element => {
      let obj = this.formBuilder.group({
        invoiceTransactidocumentCountonDescriptionSubTypeDocumentId: null,
        invoiceTransactionDescriptionDocumentId: element.invoiceTransactiondescriptiondocumentId,
        documentCount: this.formBuilder.array([]),
        documentName: this.formBuilder.array([]),
        docName: element.documentName,
        fileName: null,
        CustomDocumentName: [],
        isRequired: true
      })
      obj = setRequiredValidator(obj, ["isRequired"]);
      this.transactionProcessFormArray.push(obj);
    });
    if (temp && temp.length > 0) {
      temp.forEach(element => {
        this.transactionProcessFormArray.value.push(element);
      });
    }
    // this.docLength = this.docLength + this.descriptionTypeDocList.length;
  }
  setDescriptionSubTypeDoc() {
    let temp = this.transactionProcessFormArray.value;
    this.descriptionSubTypeDocList.forEach(element => {
      let obj = this.formBuilder.group({
        invoiceTransactionDescriptionSubTypeDocumentId: element.invoiceTransactiondescriptionSubTypeDocumentId,
        invoiceTransactionDescriptionDocumentId: null,
        documentCount: this.formBuilder.array([]),
        documentName: this.formBuilder.array([]),
        docName: element.documentName,
        fileName: null,
        CustomDocumentName: [],
        isRequired: true
      })
      obj = setRequiredValidator(obj, ["isRequired"]);
      this.transactionProcessFormArray.push(obj);
    });
    if (temp && temp.length > 0) {
      temp.forEach(element => {
        this.transactionProcessFormArray.value.push(element);
      });
    }
    //this.docLength = this.docLength + this.descriptionTypeDocList.length;
  }
  addRequiredFormList(control, type, index) {
    if (this.fileObject) {
      if (type == 'required-doc') {
      }
      else if (type == 'optional-doc') {
        if (!this.optionalNameTemp) {
          this.snackbarService.openSnackbar("Please enter the name for optional documment.", ResponseMessageTypes.WARNING);
          return;
        }
      }
      this.transactionProcessFormArray.value[index].fileName = '';
      this.transactionProcessFormArray.value[index].documentCount.push(this.transactionProcessFormArray.value[index].documentCount.length + 1);
      this.transactionProcessFormArray.value[index].documentName.push(this.fileObject.name);
      // this.transactionProcessFormArray.value[index].CustomDocumentName.push(val);
      this.fileObject = null;
      this.optionalNameTemp = '';
      this.optionName='';
    } else {
      this.snackbarService.openSnackbar("Please upload file.", ResponseMessageTypes.WARNING);
    }
    this.valueChange();
  }
  addOptionalFormList() {
    this.optionalFormList.push({
      invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
      documentPath: '',
      documentName: '',
      isRequired: false
    });
    this.valueChange();
  }
  removeRequiredFormList(index, sindex) {
    let documentName = this.transactionProcessFormArray.value[index].documentName[sindex];
    let findIndex = this.fileObjArr.findIndex(x => x.name == documentName);
    let findIndexoptional = this.optionalFileObjArr.findIndex(x => x.name == documentName);
    this.transactionProcessFormArray.value[index].documentCount.pop();
    this.transactionProcessFormArray.value[index].documentName.splice(sindex, 1);
    if(this.transactionProcessFormArray.value[index].isRequired==false)
      this.transactionProcessFormArray.value[index].CustomDocumentName.splice(sindex, 1);

    this.fileObjArr.splice(findIndex, 1);
    this.optionalFileObjArr.splice(findIndexoptional, 1);
    this.valueChange();
  }
  removeOptionalFormList(index) {
    this.optionalFormList.splice(index, 1);
    this.optionalFormListName.splice(index, 1);
    this.valueChange();
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.DELETE:
        this.deleteInvoice(row['itemId']);
        break;
        case CrudType.QUANTITY_CLICK:
           this.onQuantityClick(unknownVar?.type, row, unknownVar?.index);
        break;
        case CrudType.EDIT_ACTION:
          this.editAction(row,unknownVar?.index);
        break;
    }
  }
  editAction(row,index){
    this.isEditUnitPrice = true;
    this.isEditIndex = index;
  }
  cancelEditAction() {
    this.isEditUnitPrice = false;
  }
  saveUnitPriceChange(){
    this.editActionForm = setRequiredValidator(this.editActionForm, ['unitprice']);
    if(this.editActionForm.invalid)
      return;

     this.dataList[this.isEditIndex].unitPrice = this.editActionForm.get('unitprice').value;
     if(this.dataList[this.isEditIndex].qty){
      this.dataList[this.isEditIndex].vat = ((this.dataList[this.isEditIndex].unitPrice * this.taxPercentage)/100) * this.dataList[this.isEditIndex].qty;
      this.dataList[this.isEditIndex].exclVAT = this.dataList[this.isEditIndex].unitPrice * this.dataList[this.isEditIndex].qty;
      this.dataList[this.isEditIndex].inclVAT =  this.dataList[this.isEditIndex].vat + this.dataList[this.isEditIndex].exclVAT;
     }

     this.dataList[this.isEditIndex].vat = Number(this.dataList[this.isEditIndex].vat)?.toFixed(2);
     this.dataList[this.isEditIndex].exclVAT = Number(this.dataList[this.isEditIndex].exclVAT)?.toFixed(2);
     this.dataList[this.isEditIndex].inclVAT =Number(this.dataList[this.isEditIndex].inclVAT)?.toFixed(2);

     this.editActionForm = clearFormControlValidators(this.editActionForm, ['unitprice']);
     this.editActionForm.get('unitprice').setValue(null);
     this.isEditUnitPrice = false;
     this.updateStockData();
  }
  onQuantityClick(type, rowData, index) {

    if(type=='plus'){
       this.dataList[index].qty = parseInt(rowData.qty) + 1;
       this.dataList[index].vat = ((this.dataList[index].unitPrice * this.taxPercentage)/100) * this.dataList[index].qty;
       this.dataList[index].exclVAT = this.dataList[index].unitPrice * this.dataList[index].qty;
       this.dataList[index].inclVAT =  this.dataList[index].vat + this.dataList[index].exclVAT;

       this.dataList[index].vat = Number(this.dataList[index].vat)?.toFixed(2);
       this.dataList[index].exclVAT = Number(this.dataList[index].exclVAT)?.toFixed(2);
       this.dataList[index].inclVAT =Number(this.dataList[index].inclVAT)?.toFixed(2);

       this.updateStockData();
    }
    else if(type=='minus'){
      if(rowData.qty){
        this.dataList[index].qty = parseInt(rowData.qty) - 1;
        this.dataList[index].vat = ((this.dataList[index].unitPrice * this.taxPercentage)/100) * this.dataList[index].qty;
        this.dataList[index].exclVAT = this.dataList[index].unitPrice * this.dataList[index].qty;
        this.dataList[index].inclVAT =  this.dataList[index].vat + this.dataList[index].exclVAT;

        this.dataList[index].vat = Number(this.dataList[index].vat)?.toFixed(2);
        this.dataList[index].exclVAT = Number(this.dataList[index].exclVAT)?.toFixed(2);
        this.dataList[index].inclVAT =Number(this.dataList[index].inclVAT)?.toFixed(2);

        this.updateStockData();
      }
    }
    // this.onCRUDRequested('quantity-click', rowData, { index: index, type: type });
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  deleteInvoice(invoiceItemId) {
    // if (this.invoiceItemId && this.invoiceItemId.length > 0) {
    //  let index = this.dataList.findIndex(x => x.itemId === invoiceItemId); //find index in your array
     // this.invoiceItemId.splice(index, 1);
       let secindex = this.dataList.findIndex(x => x.itemId == invoiceItemId);
       this.dataList.splice(secindex, 1);
       this.updateStockData();
    // }
  }
  updateStockData() {
    this.dataList.pop();
    if (this.dataList.length > 0) {
      this.totalExclVat=0;this.totalVAT=0;this.totalInclVat=0;
      this.dataList.forEach((element,index) => {
        this.totalExclVat += parseFloat(element.exclVAT);
        this.totalVAT += parseFloat(element.vat);
        this.totalInclVat += parseFloat(element.inclVAT);
      });
      let dat = {
        exclVAT:Number(this.totalExclVat)?.toFixed(2), vat: Number(this.totalVAT)?.toFixed(2), glCode: " ", inclVAT: Number(this.totalInclVat)?.toFixed(2), invoiceId: " ", invoiceItemId: " ", qty: " ",
        stockId: " ", stockName: " ", type: " ", unitPrice: "Total", vatCode: " "
      }
      this.dataList.push(dat);
    }
  }
  onSubmit() {
    this.formData = new FormData();
    if (this.type == 'doa') {
      this.approvePopupFlag = !this.approvePopupFlag;
      this.fileName = '';
      this.createTransactionProcessApproveForm();
      this.rxjsService.setFormChangeDetectionProperty(true)

    }
    else {
      if(this.selectedTabIndex==0){
        this.invoiceItemId=[];
        if(this.dataList && this.dataList.length>0){

          this.dataList.forEach((element,index) => {
            let tem ={};
            tem = {
              invoiceItemId: element.itemId,
              Quantity: element.qty,
              unitprice: element.unitPrice,
              ExclVAT: element.exclVAT,
              Vat:element.vat,
              invcldetailVat: element.inclVAT
            }

            if(index != this.dataList.length-1)
               this.invoiceItemId.push(tem);
                // this.invoiceItemId.push(element.itemId);
          });
        }
        let finalDoc=[];
        let documents = this.transactionProcessFormArray.value;
        if(documents && documents.length>0){
          Object.keys(documents).forEach(key => {
            if(documents[key].documentName.length>0){
              if( documents[key].CustomDocumentName && documents[key].CustomDocumentName.length >0)
                  documents[key].CustomDocumentName =  documents[key].CustomDocumentName.toString();

               finalDoc.push(documents[key]);
            }
          });
        }
        let formValue =  this.creditReinvoiceForm.value;
        formValue.documentDate = (formValue.documentDate && formValue.documentDate != null) ? this.momentService.toMoment(formValue.documentDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;

        let finalObj =  {
            customerId: this.customerDetails.customer.customerId,
            addressId:  this.customerDetails.customer.addressId,
            contractId: this.customerDetails.contracts[0]?.id,
            invoiceId:  this.customerDetails.customer.invoiceId,
            invoicePaymentandOverrideId: this.id,
            transactionRefNo:  this.customerDetails.customer.invoiceRefNo,
            documentDate: formValue.documentDate,
            transactionTypeId:  this.customerDetails.customer.transactionTypeId,
            descriptionTypeId: formValue.descriptionType,
            descriptionSubTypeId: formValue.descriptionSubType,
            transactionNotes: formValue.transactionNote,
            isSendCustomerCopy: formValue.isSendCustomerCopy ? formValue.isSendCustomerCopy  :false,
            isSendToDOAApproval: true,
            createdUserId: this.loggedUser.userId,
            invoiceItemId:this.invoiceItemId,
            totalExclVAT: this.totalExclVat? this.totalExclVat:0,
            totalVAT: this.totalVAT ? this.totalVAT : 0,
            inclVAT: this.totalInclVat ? this.totalInclVat:0,
            creditReinvoiceDocumentsUploadDTO: finalDoc
          }
          if(this.fileObjArr &&  this.fileObjArr.length >0){
            this.fileObjArr.forEach((element,index) => {
              this.formData.append("file"+index, element);
            });
          }
          this.formData.append('Obj', JSON.stringify(finalObj));
          let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_AND_REINVOICE, this.formData);
          api.subscribe((res: any) => {
            if (res?.isSuccess == true && res?.statusCode == 200) {
              this.creditNoteTransactionId = res?.resources;
              this.creditReinvoiceForm.reset();
              let e = { index: 1 };
              this.tabClick(e);
              this.resetAll();
            }
            this.rxjsService.setDialogOpenProperty(false);
          });

       }
       else if(this.selectedTabIndex==1){
        this.submitFlag = !this.submitFlag;
       }
    }

  }
  resetAll(){
    this.creditReinvoiceForm.reset();
    this.transactionProcessFormArray.clear();
    this.creditReinvoiceForm = removeAllFormValidators(this.creditReinvoiceForm);
   // this.creditReinvoiceForm = clearFormControlValidators(this.creditReinvoiceForm, ["transactionNote"]);
   // this.invoiceItemId=[];
    //this.getRequiredDetail();
  }
  valueChange(){
    let flag=false;
    let documents = this.transactionProcessFormArray.value;
    flag = this.checkRequiredDoc(documents);
    let reqDoc = documents;
    if (reqDoc && reqDoc.length > 0){
      this.enableSubmit = flag;
    }
    this.enableSubmit = (this.creditReinvoiceForm.invalid || this.enableSubmit) ? true : false;
  }
  valueWhenFormChange (){
    this.creditReinvoiceForm.valueChanges.subscribe(data=>{
      let flag=false;
      let documents = this.transactionProcessFormArray.value;
      flag = this.checkRequiredDoc(documents);
      let reqDoc = documents;
      if (reqDoc && reqDoc.length > 0){
        this.enableSubmit = flag;
      }
      this.enableSubmit = (this.creditReinvoiceForm.invalid || this.enableSubmit) ? true : false;
    })
  }
  checkRequiredDoc(documents){
    let flag=false;
    if(documents && documents.length > 0){
      let requiredDocuments = documents.filter(x=>x.isRequired==true);
      let optionalDocuments = documents.filter(x=>x.isRequired==false);
      if((this.descriptionTypeList.length >0 || this.descriptionSubTypeList.length>0) && requiredDocuments.length==0){
        flag =  true;return;
      }
      if(requiredDocuments && requiredDocuments.length > 0 ){
        requiredDocuments.forEach(element => {
              if(element.documentName.length==0)
              flag =  true;return;
          });
      }
    }
    return flag;
  }
  finalSubmit() {
    this.formData = new FormData();
    // if(this.selectedTabIndex==0){
      this.invoiceItemId=[];
      this.dataList.forEach((element,index) => {
        if(element.itemId){
          let tem ={};
          tem = {
           invoiceItemId: element.itemId,
           Quantity: element.qty,
           unitprice: element.unitPrice,
           ExclVAT: element.exclVAT,
           Vat:element.vat,
           invcldetailVat: element.inclVAT,
          }
          if(index != this.dataList.length-1)
            this.invoiceItemId.push(tem);
        }

      });

      let finalDoc=[];
      let documents = this.transactionProcessFormArray.value;
      Object.keys(documents).forEach(key => {
        if(documents[key].documentName.length>0){
          if(documents[key].CustomDocumentName && documents[key].CustomDocumentName.length >0)
            documents[key].CustomDocumentName = documents[key].CustomDocumentName.toString();

          finalDoc.push(documents[key]);
        }
      });
      let formValue =  this.creditReinvoiceForm.value;
      formValue.documentDate = (formValue.documentDate && formValue.documentDate != null) ? this.momentService.toMoment(formValue.documentDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
      let finalObj =  {
          customerId: this.customerDetails.customer.customerId,
          addressId:  this.customerDetails.customer.addressId,
          contractId: this.customerDetails.contracts[0]?.id,
          invoiceId:  this.customerDetails.customer.invoiceId,
          invoicePaymentandOverrideId: this.id,
          transactionRefNo:  this.customerDetails.customer.invoiceRefNo,
          documentDate: formValue.documentDate,
          transactionTypeId:  this.customerDetails.customer.transactionTypeId,
          descriptionTypeId: formValue.descriptionType,
          descriptionSubTypeId: formValue.descriptionSubType,
          transactionNotes: formValue.transactionNote,
          isSendCustomerCopy: formValue.isSendCustomerCopy ? formValue.isSendCustomerCopy  :false,
          isSendToDOAApproval: true,
          createdUserId: this.loggedUser.userId,
          invoiceItemId:this.invoiceItemId,
          totalExclVAT: this.totalExclVat? this.totalExclVat:0,
          totalVAT: this.totalVAT ? this.totalVAT : 0,
          inclVAT: this.totalInclVat ? this.totalInclVat:0,
          creditReinvoiceDocumentsUploadDTO: finalDoc,
          creditNoteTransactionId: this.creditNoteTransactionId ? this.creditNoteTransactionId : null,
          debtorAccountDetailId: this.debtorAccountDetailId ? this.debtorAccountDetailId : null
        }
        if(this.fileObjArr&&  this.fileObjArr.length >0){
          this.fileObjArr.forEach((element,index) => {
            this.formData.append("file"+index, element);
          });
        }
        this.formData.append('Obj', JSON.stringify(finalObj));

        let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_AND_REINVOICE_INVOICE, this.formData);
          api.subscribe((res: any) => {
            if (res?.isSuccess == true && res?.statusCode == 200) {
              this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } });
            }
            this.rxjsService.setDialogOpenProperty(false);
          });

    //  }
  }
  cancelSubmitPopup() {
    this.submitFlag = !this.submitFlag;
  }
  cancel() {
    if (this.type == 'doa') {
      this.declinePopupFlag = !this.declinePopupFlag;
      this.fileName = '';
      this.createTransactionProcessDeclineForm();
    }
    else
      this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } });
  }
  approve() {
    this.rxjsService.setFormChangeDetectionProperty(true)
    if (this.transactionProcessApproveForm.invalid) return;

    let finalOjb = {
      roleId: this.loggedUser.roleId,
      approvedUserId: this.loggedUser.userId,
      comments: this.transactionProcessApproveForm.value.comments,
      referenceNo: this.transactionId,
      documentName:this.obj?.name,
      declineReasonId: 0
    }
    let formData = new FormData();
    if(this.obj){
      formData.append("file", this.obj);
    }
    formData.append('obj', JSON.stringify(finalOjb));
    let api = this.transactionType == 'CN' ? this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_AND_REINVOICE_DOA_APPROVAL_CREDI_NOTE, formData) : this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_AND_REINVOICE_DOA_APPROVAL_INVOICE, formData);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.router.navigate(['/collection/my-task-list']);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  cancelApprove() {
    this.approvePopupFlag = !this.approvePopupFlag;
    this.transactionProcessApproveForm = clearFormControlValidators(this.transactionProcessApproveForm, ['comments']);
  }
  decline() {
    if (this.transactionProcessDeclineForm.invalid) return;

    let finalOjb = {
      roleId: this.loggedUser.roleId,
      approvedUserId: this.loggedUser.userId,
      comments: this.transactionProcessDeclineForm.value.comments,
      referenceNo: this.transactionId,
      declineReasonId: this.transactionProcessDeclineForm.value.declineReasonId,
    }
    let formData = new FormData();
    formData.append("file", this.obj);
    formData.append('obj', JSON.stringify(finalOjb));
    let api = this.transactionType == 'CN' ? this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_AND_REINVOICE_DOA_APPROVAL_CREDI_NOTE, formData) : this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_AND_REINVOICE_DOA_APPROVAL_INVOICE, formData);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.router.navigate(['/collection/my-task-list']);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  cancelDecline() {
    this.declinePopupFlag = !this.declinePopupFlag;
    this.transactionProcessDeclineForm = clearFormControlValidators(this.transactionProcessDeclineForm, ['comments', 'declineReasonId']);
  }
}
