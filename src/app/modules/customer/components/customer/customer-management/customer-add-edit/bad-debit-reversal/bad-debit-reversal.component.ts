import { Component, ViewChild } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { clearFormControlValidators, CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { TransactionProcessCommonComponent } from "../shared/transaction-process-common/transaction-process-common.component";

@Component({
    selector: 'app-bad-debit-reversal',
    templateUrl: './bad-debit-reversal.component.html',
    styleUrls: ['./bad-debit-reversal.component.scss']
})

export class BadDebitReversalComponent {
    @ViewChild(TransactionProcessCommonComponent, { static: false })
    customerInformationComponent: TransactionProcessCommonComponent;
    primengTableConfigProperties: any;
    loggedUser: UserLogin;
    selectedTabIndex: any = 0;
    dataList: any;
    pageLimit: number[] = [10, 25, 50, 75, 100];
    row: any = {}
    loading: boolean;
    selectedRows: any = [];
    firstHeading: string;
    secondHeading: string;
    thirdHeading: string;
    fourthHeading: string;
    firstSectionDetails;
    secondSectionDetails;
    transactionSection;
    thirdSectionDetails: any = [];
    formSectionDetails = [];
    optionalForms = [];
    requiredForms = [];
    addressId; id;
    customerId;
    debtorId;
    badDebitRecoveryForm: FormGroup;
    flag: boolean = false;
    submitFlag: boolean = false;
    approvePopupFlag: boolean = false;
    declinePopupFlag: boolean = false;
    transactionProcessApproveForm: FormGroup;
    transactionProcessDeclineForm: FormGroup;
    totalExclVat = 0; totalVAT = 0; totalInclVat = 0;
    fileName;
    finalDetails; type; reasonList = [];
    transactionId;
    transactionDescription;descriptionId;descriptionSubId;
    transactionType;invoiceTransactionTypeId;
    enableSubmit: boolean = false;
    formData = new FormData();
    descriptionType;
    subTypeDescription;
    status;
    constructor(private activatedRoute: ActivatedRoute, private router: Router, private formBuilder: FormBuilder,
        private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: "View Customer",
            breadCrumbItems: [],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'View Customer',
                        dataKey: '',
                        captionFontSize: '21px',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: true,
                        enableFieldsSearch: false,
                        enableHyperLink: false,
                        cursorLinkIndex: 0,
                        columns: [
                            { field: 'type', header: 'Type', width: '200px' },
                            { field: 'stockId', header: 'Stock ID', width: '200px' },
                            { field: 'stockName', header: 'Stock Name', width: '200px' },
                            { field: 'glCode', header: 'GL Code', width: '200px' },
                            { field: 'quantity', header: 'Quantity', width: '200px' },
                            { field: 'unitPrice', header: 'Unit Price', width: '200px' },
                            { field: 'exclVAT', header: 'Excl VAT', width: '200px' },
                            { field: 'vat', header: 'Vat', width: '200px' },
                            { field: 'vatCode', header: 'VAT Code', width: '200px' },
                            { field: 'inclVat', header: 'Amount', width: '200px' }
                        ],
                        enableMultiDeleteActionBtn: false,
                        enableRowDeleteActive: true,
                        enableAddActionBtn: false,
                        enableExportBtn: false,
                        enableEmailBtn: false,
                        shouldShowFilterActionBtn: false,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false
                    }
                ]
            }
        }
        this.id = this.activatedRoute.snapshot.queryParams.creditNoteTransactionId;
        this.status = this.activatedRoute.snapshot.queryParams.status;
        this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
        this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
        this.debtorId = this.activatedRoute.snapshot.queryParams.debtorId;
        this.type = this.activatedRoute.snapshot.queryParams.type;
        this.transactionType = this.activatedRoute.snapshot.queryParams.transactionType;
        this.transactionId = this.activatedRoute.snapshot.queryParams.transactionId;
        let route1 = this.type == 'doa' ? { displayName: 'DOA Dashboard', relativeRouterUrl: '/collection/my-task-list' } : { displayName: 'Customer', relativeRouterUrl: '/customer/manage-customers/view-transactions', queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } };
        this.primengTableConfigProperties.breadCrumbItems.push(route1);
        let route2 = this.type == 'doa' ? { displayName: 'Bad Debt Reversal - DOA Approve', relativeRouterUrl: '' } : { displayName: 'Bad Debt Reversal', relativeRouterUrl: '' };
        this.primengTableConfigProperties.breadCrumbItems.push(route2);
    }
    ngOnInit() {
        this.rxjsService.setGlobalLoaderProperty(true);
        this.getRequiredDetail();
        this.getReason();
        this.firstHeading = 'CUSTOMER INFO';
        this.secondHeading = 'DEBTOR INFO';
        this.thirdHeading = 'OUTSTANDING BALANCE';
        this.fourthHeading = 'WRITE OFF RECOVERY-REVERSAL';
    }
    createForm() {
        this.badDebitRecoveryForm = this.formBuilder.group({});
        this.formSectionDetails.forEach((key) => {
            this.badDebitRecoveryForm.addControl(key.formName, new FormControl());
        });
        this.transactionSection.forEach((key) => {
            this.badDebitRecoveryForm.addControl(key.formName, new FormControl());
        });
        this.badDebitRecoveryForm = setRequiredValidator(this.badDebitRecoveryForm, ["transactionNote"]);
    }
    createTransactionProcessApproveForm(): void {
        this.transactionProcessApproveForm = new FormGroup({
            'comments': new FormControl(null),
        });
        this.transactionProcessApproveForm = setRequiredValidator(this.transactionProcessApproveForm, ['comments']);
    }
    createTransactionProcessDeclineForm(): void {
        this.transactionProcessDeclineForm = new FormGroup({
            'declineReasonId': new FormControl(null),
            'comments': new FormControl(null),
        });
        this.transactionProcessDeclineForm = setRequiredValidator(this.transactionProcessDeclineForm, ['comments', 'declineReasonId']);
    }
    getReason() {
        this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_DECLINE_REASON)
            .subscribe((response) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.reasonList = response.resources
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    getRequiredDetail() {
        let api = this.type == 'doa' ? this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BAD_DEBIT_REVERSAL_DOA_DETAIL, undefined, false, prepareRequiredHttpParams({ type: this.transactionType, transactionId: this.transactionId })) : this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BAD_DEBIT_RECOVERY_REVERSAL, undefined, false, prepareRequiredHttpParams({ BadDebitRecoveryId: this.id }));
        api.subscribe((resp: IApplicationResponse) => {
            if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {
                this.setBadDebitRecoveryInvoiceDetailDTO(resp.resources.transactionDetailsDTO, resp.resources.transactionInvoiceItem.stocks);
                this.setBadDebitRecoveryOutstandingBalance(resp.resources.outstandingBalance);
                this.invoiceTransactionTypeId = resp.resources.transactionDetailsDTO.invoiceTransactionTypeId;
                this.transactionDescription = resp.resources.invoiceTransactiondescriptionDTO;
                this.descriptionId =  resp.resources.transactionDetailsDTO?.invoiceTransactionDescriptionId;
                this.descriptionSubId =   resp.resources.transactionDetailsDTO?.invoiceTransactionDescriptionSubTypeId;
                this.descriptionType =  resp.resources.transactionDetailsDTO?.transactionDescription;
                this.subTypeDescription =resp.resources.transactionDetailsDTO?.subTypeDescription;
                if (resp.resources.transactionDOASupportingDocuments) {
                    resp.resources.transactionDOASupportingDocuments.forEach(element => {
                        if (element.isRequired)
                            this.requiredForms.push({ documentName: element.documentName, documentPath: element.documentPath });
                        else
                            this.optionalForms.push({ documentName: element.documentName, documentPath: element.documentPath });
                    });
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            }
            else
                this.setBadDebitRecoveryInvoiceDetailDTO(null, []);
        })
    }
    setBadDebitRecoveryInvoiceDetailDTO(val, badDebitRecoveryStockDetailDTOs) {
        this.firstSectionDetails = [
            { name: 'Customer ID', value: val?.customerRegNo, valueColor: '#166DFF', enableHyperLink: true },
            { name: 'Customer Name', value: val?.customerName },
            { name: 'Customer Type', value: val?.customerType },
            { name: 'Site Address', value: val?.siteAddress },
            { name: 'Contact Number', value: val?.contactNumber },
            { name: 'Email Address', value: val?.emailAddress },
            { name: 'Co Reg No', value: val?.customerCompanyRegNo },
            { name: 'Vat No', value: val?.debtorVatno }
        ];
        this.secondSectionDetails = [
            { name: 'Debtor Name', value: val?.debtorName }, { name: 'Debtor Code', value: val?.debtorCode },
            { name: 'BDI Number', value: val?.debtorBDINumber }, { name: 'Contact Number', value: val?.debtorContact },
            { name: 'Email Address', value: val?.debtorEmail }, { name: 'Co Reg No', value: val?.debtorCompanyRegNo },
            { name: 'Vat No', value: val?.debtorVatno }
        ];
        this.formSectionDetails = [
            { label: 'Region', formName: 'region', type: "text", value: val?.regionName },
            { label: 'Division', formName: 'division', type: "text", value: val?.divisionName },
            { label: 'Branch', formName: 'branch', type: "text", value: val?.branchName },
            { label: 'Document Date', formName: 'documentDate', type: "date", value: val?.documentDate ? new Date(val?.documentDate) : new Date() },
            { label: 'Transaction Type', formName: 'transactionType', type: "text", value: val?.transactionType },
            { label: 'Transaction Description', formName: 'transactionDescription', type: "text", value: val?.transactionDescription },
            { label: 'Description Type', formName: 'descriptionType', type: "select", value: val?.invoiceTransactionDescriptionId },
            { label: 'Description Sub Type', formName: 'descriptionSubType', type: "select", value: val?.invoiceTransactionDescriptionSubTypeId },
            { label: 'Reference ID', formName: 'reference', type: "text", value: val?.invoiceRefNo },
        ];
        this.flag = true;
        this.dataList = badDebitRecoveryStockDetailDTOs;
        this.transactionSection = [
            { label: '', formName: 'transactionNote', type: "text", value: val?.transactionNotes },
        ];
        this.setTotal();
        this.createForm();
    }
    setBadDebitRecoveryOutstandingBalance(outstandingBalance) {
        this.thirdSectionDetails = [
            { label: 'D180', value: outstandingBalance?.days180, colsize: "outstanding-balance-col-width" },
            { label: 'D150', value: outstandingBalance?.days150, colsize: "outstanding-balance-col-width" },
            { label: 'D120', value: outstandingBalance?.days120, colsize: "outstanding-balance-col-width" },
            { label: 'D90', value: outstandingBalance?.days90, colsize: "outstanding-balance-col-width" },
            { label: 'D60', value: outstandingBalance?.days60, colsize: "outstanding-balance-col-width" },
            { label: 'D30', value: outstandingBalance?.days30, colsize: "outstanding-balance-col-width" },
            { label: 'Current', value: outstandingBalance?.current, colsize: "outstanding-balance-col-width" },
            { label: 'Balance', value: outstandingBalance?.totalInvoiceOutStandingAmount, colsize: "outstanding-balance-col-width" }
        ]
    }
    obj: any;
    onFileSelected(files: FileList): void {
        const fileObj = files.item(0);
        this.fileName = fileObj.name;
        this.obj = fileObj;
        // this.getFilePath(fileObj);
    }
    setTotal() {
        if (this.dataList && this.dataList.length > 0) {
            this.dataList.forEach(element => {
                this.totalExclVat += element.exclVAT;
                this.totalVAT += element.vat;
                this.totalInclVat += element.inclVat;
            });
            let dat = {
                exclVAT: this.totalExclVat, vat: this.totalVAT, glCode: " ", inclVat: this.totalInclVat, invoiceId: " ", invoiceItemId: " ", quantity: " ",
                stockId: " ", stockName: " ", type: " ", unitPrice: "Total", vatCode: " "
            }
            this.dataList.push(dat);
        }
    }
    emitInformation(event) {
        this.finalDetails = event;
        this.badDebitRecoveryForm.patchValue(this.finalDetails.data);
        let reqDoc = this.finalDetails.documents;
        if (reqDoc && reqDoc.length > 0){
          this.enableSubmit = this.finalDetails.flag;
        }
        this.enableSubmit = (this.badDebitRecoveryForm.invalid || this.enableSubmit) ? true : false;
    }
    onSubmit() {
        if (this.type == 'doa') {
            this.approvePopupFlag = !this.approvePopupFlag;
            this.fileName = '';
            this.createTransactionProcessApproveForm();
        }
        else
            this.submitFlag = !this.submitFlag;
    }
    cancelSubmitPopup() {
        this.submitFlag = !this.submitFlag;
    }
    cancel() {
        if (this.type == 'doa') {
            this.declinePopupFlag = !this.declinePopupFlag;
            this.fileName = '';
            this.createTransactionProcessDeclineForm();
        }
        else
            this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } });
    }
    finalSubmit() {
        this.formData = new FormData();      let finalDoc=[];
        Object.keys(this.finalDetails.documents).forEach(key => {
            if(this.finalDetails.documents[key].documentName.length>0){
                 if( this.finalDetails.documents[key].CustomDocumentName &&  this.finalDetails.documents[key].CustomDocumentName.length >0)
                   this.finalDetails.documents[key].CustomDocumentName =    this.finalDetails.documents[key].CustomDocumentName.toString();
                 else
                 this.finalDetails.documents[key].CustomDocumentName ='';

                 finalDoc.push(this.finalDetails.documents[key]);
            }
        });
        let finalObject = {
            badDebitReversalDTO: {
                badDebitReversalId: null,
                badDebitRecoveryId: this.id,
                customerId: this.customerId,
                notes: this.finalDetails.data.transactionNote,
                invoiceTransactionDescriptionId: this.finalDetails.data.descriptionType
            },
            badDebitReversalDocumentsDTOs: finalDoc,
            createdUserId: this.loggedUser.userId
        }
        if(this.finalDetails &&  this.finalDetails.documentsArr &&  this.finalDetails.documentsArr.length >0){
            this.finalDetails.documentsArr.forEach((element,index) => {
              this.formData.append("file"+index, element);
            });
          }
          this.formData.append('Obj', JSON.stringify(finalObject));
        let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BAD_DEBIT_REVERSAL,  this.formData);
        api.subscribe((res: any) => {
            if (res?.isSuccess == true && res?.statusCode == 200) {
                this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } });
            }
            this.rxjsService.setDialogOpenProperty(false);
        });
    }
    approve() {

        if (this.transactionProcessApproveForm.invalid) return;

        let finalOjb = {
            roleId: this.loggedUser.roleId,
            approvedUserId: this.loggedUser.userId,
            comments: this.transactionProcessApproveForm.value.comments,
            referenceNo: this.transactionId,
            declineReasonId: 0
        }
        let formData = new FormData();
        if(this.obj){
            formData.append("file", this.obj);
        }

        formData.append('obj', JSON.stringify(finalOjb));
        let api = this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_NOTES_DOA_APPROVAL, formData);
        api.subscribe((res: any) => {
            if (res?.isSuccess == true && res?.statusCode == 200) {
                this.router.navigate(['/collection/my-task-list']);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    cancelApprove() {
        this.approvePopupFlag = !this.approvePopupFlag;
        this.transactionProcessApproveForm = clearFormControlValidators(this.transactionProcessApproveForm, ['comments']);
    }
    decline() {
        if (this.transactionProcessDeclineForm.invalid) return;

        let finalOjb = {
            roleId: this.loggedUser.roleId,
            approvedUserId: this.loggedUser.userId,
            comments: this.transactionProcessDeclineForm.value.comments,
            referenceNo: this.transactionId,
            declineReasonId: this.transactionProcessDeclineForm.value.declineReasonId,
        }
        let formData = new FormData();
        formData.append("file", this.obj);
        formData.append('obj', JSON.stringify(finalOjb));
        let api = this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_NOTES_DOA_APPROVAL, formData);
        api.subscribe((res: any) => {
            if (res?.isSuccess == true && res?.statusCode == 200) {
                this.router.navigate(['/collection/my-task-list']);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    cancelDecline() {
        this.declinePopupFlag = !this.declinePopupFlag;
        this.transactionProcessDeclineForm = clearFormControlValidators(this.transactionProcessDeclineForm, ['comments', 'declineReasonId']);
    }
    onCRUDRequested(event){

    }
}
