import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { RequestExtendPaymentArrangementComponent } from "./request-extend-payment-arrangement.component";

@NgModule({
    declarations: [RequestExtendPaymentArrangementComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: RequestExtendPaymentArrangementComponent, data: { title: 'Request Extend Payment Arrangement' }
        },
    ])
  ],
  entryComponents: [],
  providers: []
})
export class RequestExtendPaymentArrangementModule { }