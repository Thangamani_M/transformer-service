import { Component } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { agentLoginDataSelector, CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { BillingModuleApiSuffixModels, TelephonyApiSuffixModules } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
  selector: 'add-promise-to-pay',
  templateUrl: './add-promise-to-pay.component.html'
})

export class AddPromiseToPayComponent {
  addPromiseToPayForm: FormGroup;
  popUpForm: FormGroup;
  callOutcomeList = [];
  callWrapupList = [];
  paymentMethodList = [];
  statuList = [];
  bankList = [];
  selectedIds = [];
  startTodayDate = new Date();
  debtorId;
  addressId;
  customerId;
  debtorDetails;
  paymentOveridePopup: boolean = false;
  paymentMethodName;
  isPaymentOverride: boolean = false;
  campaignId;
  promiseToPayId;
  type;
  isSelectBank: boolean = false;
  debatorInfoByIdFromList;
  uniqueCallId;
  agentExtensionNo;
  loggedInUserData: LoggedInUserModel;

  constructor(private momentService: MomentService, private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.promiseToPayId = this.activatedRoute.snapshot.queryParams.promiseToPayId;
    this.type = this.activatedRoute.snapshot.queryParams.type;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.debtorId = this.activatedRoute.snapshot.queryParams.debtorId;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.campaignId = this.activatedRoute.snapshot.queryParams.campaignId;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
    this.createForm();
    this.createPopupForm();
    this.getCallOutcomeDropdown();
    this.getPaymentMethodList();
    this.getBankList();
    if (this.promiseToPayId && this.type == 'list') {
      this.getDebatorInfoByIdFromList();
      this.addPromiseToPayForm.get('callOutcomeId').disable();
      this.addPromiseToPayForm.get('callWrapupId').disable();
      this.addPromiseToPayForm.get('outstandingBalance').disable();
    }
    else {
      this.getDebatorInfo();
      this.getDebatorInfoByIds();
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.store.pipe(select(agentLoginDataSelector))])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.agentExtensionNo = response[1];
      });
  }

  createForm(): void {
    this.addPromiseToPayForm = this.formBuilder.group({
      'callOutcomeId': new FormControl(null),
      'callWrapupId': new FormControl(null),
      'outstandingBalance': new FormControl(null),
      'paymentDate': new FormControl(null),
      'paymentMethodId': new FormControl(null),
      'description': new FormControl(null),
      'bankId': new FormControl(null),
      'statusId': new FormControl(null),
    });
    this.addPromiseToPayForm = setRequiredValidator(this.addPromiseToPayForm, ['callOutcomeId', "paymentDate", 'callWrapupId']);
    if (this.type == 'list')
      this.addPromiseToPayForm = setRequiredValidator(this.addPromiseToPayForm, ['statusId']);
    this.valueChanges();
  }

  createPopupForm() {
    this.popUpForm = this.formBuilder.group({
      'description': new FormControl(null),
      'paymentDate': new FormControl(null),
    });
  }

  valueChanges() {
    this.addPromiseToPayForm
      .get("paymentMethodId")
      .valueChanges.subscribe((paymentMethodId: string) => {
        let filter = this.paymentMethodList.filter(x => x.id == paymentMethodId);
        if (filter && filter.length > 0) {
          this.isPaymentOverride = filter[0].displayName == 'Debit Order' ? true : false;
          this.isSelectBank = !this.isPaymentOverride ? true : false;
        }
      });
    this.addPromiseToPayForm
      .get("paymentDate")
      .valueChanges.subscribe((paymentDate: string) => {
        this.popUpForm.get('paymentDate').setValue(paymentDate);
      });
  }

  onChangeCallOutcome(value) {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_CALL_OUTCOME_CALL_WRAPUP, null, null, prepareRequiredHttpParams({ CallOutcomeId: value }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.callWrapupList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCallOutcomeDropdown() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_CALL_OUTCOME)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.callOutcomeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCallWrapupList() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_CALL_WRAPUP, null, false,
      null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.callWrapupList = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getPaymentMethodList() {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UX_PAYMENT_METHODS, null, false,
      null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.paymentMethodList = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getBankList() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_COMPANY_BANKS, null, false,
      null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.bankList = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getDebatorInfo() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROMISE_TO_PAY_DEBATOR_OUTSTANDING,
      null, false, prepareRequiredHttpParams({ DebtorId: this.debtorId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.resources) {
          this.addPromiseToPayForm.get('outstandingBalance').setValue(response.resources.outstandingBalance);
          this.addPromiseToPayForm.get('paymentMethodId').setValue(response.resources.paymentMethodId);
          let filter = this.paymentMethodList.filter(x => x.id == response.resources.paymentMethodId);
          this.paymentMethodName = (filter && filter.length > 0) ? filter[0].displayName : '';
          this.isPaymentOverride = this.paymentMethodName == 'Debit Order' ? true : false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDebatorInfoByIdFromList() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROMISE_TO_PAY_DETAIL,
      null, false, prepareRequiredHttpParams({ PromiseToPayId: this.promiseToPayId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.resources) {
          this.debatorInfoByIdFromList = response.resources;
          this.statuList = response.resources.promiseToPayStatusList;
          this.addPromiseToPayForm.get('callOutcomeId').setValue(response.resources.callOutcomeId);
          if (response.resources.callOutcomeId)
            this.onChangeCallOutcome(response.resources.callOutcomeId)
          this.addPromiseToPayForm.get('callWrapupId').setValue(response.resources.callWrapupId);
          this.addPromiseToPayForm.get('paymentDate').setValue(new Date(response.resources.paymentDate));
          this.addPromiseToPayForm.get('outstandingBalance').setValue(response.resources.outstandingBalance);
          this.addPromiseToPayForm.get('paymentMethodId').setValue(response.resources.paymentMethodId);
          let filter = this.paymentMethodList.filter(x => x.id == response.resources.paymentMethodId);
          this.paymentMethodName = (filter && filter.length > 0) ? filter[0].displayName : '';
          this.isPaymentOverride = this.paymentMethodName == 'Debit Order' ? true : false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDebatorInfoByIds() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PAYMENT_ARRANGEMENTS_DEBTOR_DETAILS,
      null, false, prepareRequiredHttpParams({ DebtorId: this.debtorId, customerId: this.customerId, addressId: this.addressId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.resources) {
          this.debtorDetails = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  bankSms() {
    this.isSelectBank = true;
  }

  paymentOveride() {
    this.paymentOveridePopup = !this.paymentOveridePopup;
  }

  apply() {
    this.addPromiseToPayForm.get('description').setValue(this.popUpForm.get('description').value);
    this.paymentOveridePopup = !this.paymentOveridePopup;
  }

  cancel() {
    this.paymentOveridePopup = !this.paymentOveridePopup;
  }

  navigate() {
    if (this.type == 'list')
      this.router.navigate(["/collection/call-wrap-up-codes/promise-to-pay"]);
    else {
      this.router.navigate(["/customer/manage-customers/view-transactions"], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId, type: 'doa' } });
    }
  }

  onSubmit() {
    if (this.addPromiseToPayForm.invalid && !this.uniqueCallId) return;
    let formValue = this.addPromiseToPayForm.value;
    formValue.paymentDate = (formValue.paymentDate && formValue.paymentDate != null) ? this.momentService.toMoment(formValue.paymentDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    let finalOjb = {
      campaignId: this.campaignId,
      debtorId: this.debtorId,
      customerId: this.customerId,
      addressId: this.addressId,
      callOutcomeId: formValue.callOutcomeId,
      callWrapupId: formValue.callWrapupId,
      outstandingBalance: formValue.outstandingBalance,
      paymentDate: formValue.paymentDate,
      paymentMethodId: formValue.paymentMethodId,
      description: formValue.description,
      bankId: formValue.bankId,
      createdUserId: this.loggedInUserData.userId,
      callWrapupDuration: 10,
      uniqueCallId: this.uniqueCallId
    }
    Object.keys(finalOjb).forEach(key => {
      if (finalOjb[key] == "" || finalOjb[key] == null) {
        delete finalOjb[key]
      }
    });
    this.disconnectCall();
    let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROMISE_TO_PAY, finalOjb);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.router.navigate(['/collection/call-wrap-up-codes/promise-to-pay']);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  updatePromiseToPay() {
    if (!this.uniqueCallId) return;
    let formValue = this.addPromiseToPayForm.value;
    let finalOjb = {
      promiseToPayId: this.promiseToPayId,
      paymentDate: formValue.paymentDate,
      paymentMethodId: formValue.paymentMethodId,
      statusId: formValue?.statusId,
      modifiedUserId: this.loggedInUserData.userId,
      uniqueCallId: this.uniqueCallId
    }
    this.disconnectCall();
    let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PROMISE_TO_PAY_UPDATE, finalOjb);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.router.navigate(['/collection/call-wrap-up-codes/promise-to-pay']);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  disconnectCall() {
    const payload = {
      from: this.agentExtensionNo,
      pabxId: this.loggedInUserData.openScapePabxId,
      userId: this.loggedInUserData.userId,
    };
    this.crudService.create(ModulesBasedApiSuffix.TELEPHONY, TelephonyApiSuffixModules.DISCONNECT, payload).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.rxjsService.setUniqueCallId(null);
        this.rxjsService.setscheduleCallbackDetails(null);
        this.rxjsService.setCustomerContactNumber(null);
        this.rxjsService.setInboundCallDetails(null);
        this.rxjsService.setAnyPropertyValue(null);
      }
    });
  }
}