import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { AddProofOfPaymentsComponent } from "./add-proof-of-payments.component";


@NgModule({
    declarations: [AddProofOfPaymentsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: AddProofOfPaymentsComponent, data: { title: 'Add Proof of Payments' } 
        },
    ])
  ],
  entryComponents: [],
  providers: []
})
export class AddProofOfPaymentsModule { }