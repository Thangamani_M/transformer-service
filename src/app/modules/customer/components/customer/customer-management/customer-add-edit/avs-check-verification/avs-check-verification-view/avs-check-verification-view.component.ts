import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-avs-check-verification-view',
  templateUrl: './avs-check-verification-view.component.html'
})
export class AvsCheckVerificationViewComponent implements OnInit {

  debtorAccountDetailId: string;
  customerId: string;
  addressId: string;
  dropdownData = [];
  avsCheckForm: FormGroup;
  avscheckDetails: any = {};

  constructor(private activatedRoute: ActivatedRoute,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private formBuilder: FormBuilder,
    private router: Router) {
    this.debtorAccountDetailId = this.activatedRoute.snapshot.queryParams.DebtorAccountDetailId;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
  }

  ngOnInit(): void {
    // this.createAvsForm();
    this.dropdownData = [
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.DEBTOR_AVS_SCORE, null, false, prepareRequiredHttpParams({ DebtorAccountDetailId: this.debtorAccountDetailId }))
    ];
    this.loadActionTypes(this.dropdownData);
  }

  // createAvsForm() {
  //     this.avsCheckForm = this.formBuilder.group({});
  //     this.avsCheckForm.addControl('CustomerRefNo', new FormControl({value: '', disabled: true}));
  //     this.avsCheckForm.addControl('CustomerName', new FormControl({value: '', disabled: true}));
  //     this.avsCheckForm.addControl('CustomerInspectionCallBackId', new FormControl({value: '', disabled: true}));
  //     this.avsCheckForm.addControl('ContactNumber', new FormControl({value: '', disabled: true}));
  //     this.avsCheckForm.addControl('ModifiedUserId', new FormControl());
  // }

  loadActionTypes(dropdownData) {
    forkJoin(dropdownData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.avscheckDetails = resp?.resources?.debtorAVSResultDetail;
              let messageSpan = document.createElement('div');
              messageSpan.innerHTML = this.avscheckDetails?.bankAccountFailureReason;
              document.getElementById('parentContainer').appendChild(messageSpan);
              if (this.avscheckDetails?.accountNo) {
                let len = this.avscheckDetails?.accountNo?.length - 4;
                let first = this.avscheckDetails?.accountNo?.substring(0, len);
                let last = this.avscheckDetails?.accountNo?.substring(len, this.avscheckDetails?.accountNo?.length);
                let str = ''
                for (let i = 0; i < first.length; i++) {
                  str = str + '*';
                }
                this.avscheckDetails.accountNo = str + '' + last;
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  navigateToPage() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }
}
