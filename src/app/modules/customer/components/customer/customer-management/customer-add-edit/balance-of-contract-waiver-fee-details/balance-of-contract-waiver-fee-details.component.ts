import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, CustomDirectiveConfig, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { ItManagementApiSuffixModels, loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Table } from 'primeng/table';
import { of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-balance-of-contract-waiver-fee-details',
  templateUrl: './balance-of-contract-waiver-fee-details.component.html'
})


export class BalanceOfContractWaiverFeeDetailsComponent implements OnInit {

  @Input() inputData: any
  limit: number = 10;
  totalLength: number = 0;
  pageIndex: any = 0;
  pageLimit: number[] = [10, 50, 75, 100];
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  primengTableConfigProperties: any;
  primengTableConfigProperties1: any;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  selectedTabIndex: any = 0;
  loading: Boolean = false;
  initialLoad: boolean = false;
  scrollEnabled: boolean = false;
  observableResponse: any;
  dataList: any = [];
  totalRecords: any = 0;
  dataList1: any = [];
  totalRecords1: any = 0;
  today = new Date();
  row: any = {};
  @ViewChildren(Table) tables: QueryList<Table>;
  selectedRows: any = [];
  searchColumns: any = {};
  radioRemovalForm: FormGroup;
  loggedUser: any;
  keyholderDetails: any;
  status = [
    { label: 'Active', value: true },
    { label: 'In-Active', value: false },
  ];

  filterData: any;
  mainAreaDropDown: any;
  subAreaDropDown: any;
  regionDropDown: any;
  subDivisionDrop: any;
  branchDropDown: any;
  statusDropDown: any;
  subRubDropDown: any;
  radioRemovalFilter: any = false;
  pageSize: any = 10;
  first = 0;
  reset: boolean;
  openQuickAction: boolean = false

  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private datePipe: DatePipe,
    private tableFilterFormService: TableFilterFormService, private store: Store<AppState>, private momentService: MomentService, private httpCancelService: HttpCancelService, private _fb: FormBuilder, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {

    this.primengTableConfigProperties = {
      tableCaption: "Radios and Systems",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal' }, { displayName: 'Radio Removal Admin WorkList', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Radios and Systems',
            dataKey: 'radioRemovalWorkListId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 1,
            columns: [
              { field: 'processName', header: 'Process Name', width: '130px' },
              { field: 'pathDescription', header: 'Path Description', width: '140px' },
              { field: 'creatorUserName', header: 'Creator User Name', width: '170px' },
              { field: 'currentLevelUserName', header: 'Current Level User Name', width: '200px' },
              { field: 'onLevel', header: 'On Level', width: '100px' },
              { field: 'levelName', header: 'Level Name', width: '120px' },
              { field: 'status', header: 'Status', width: '100px' },
              { field: 'actionDate', header: 'Action Date/Time', width: '200px' },
            ],
            moduleName: ModulesBasedApiSuffix.BILLING,
            apiSuffixModel: SalesModuleApiSuffixModels.BALANCE_OF_CONTRACTS_WAIVE_FEE_DETAILS,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableQuickActionBtn: true,
          },
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {

      let filterData = (Object.keys(params['params']).length > 0) ? params['params'] : null;
      let customerData = {}
      // customerData['customerId'] = filterData['id']
      // this.filterData = { ...filterData, ...customerData }
      this.filterData = customerData

    });
  }

  ngOnInit() {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.columnFilterRequest();
    // this.row['pageIndex'] = 0;
    // this.row['pageSize'] = 20;
    // let otherParams = {};
    // this.radioRemovalWorkList(this.row['pageIndex'], this.row['pageSize'], otherParams);
  }


  ngAfterViewInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.

    if(this.inputData){
      this.onCRUDRequested('get');

    }
  }


  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] });
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }



  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  loadPaginationLazyPassword(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        this.filterData['InvoiceId'] = this.inputData.invoiceId
        this.row = row ? row : { pageIndex: 0, pageSize: this.pageSize };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.radioRemovalWorkList(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        break;
      case CrudType.DELETE:
        break;
      case CrudType.EXPORT:
        break;
      case CrudType.FILTER:
        this.radioRemovalFilter = !this.radioRemovalFilter;
        break;
      case CrudType.QUICK_ACTION:
        this.openQuickAction = !this.openQuickAction;
        break;
      case CrudType.VIEW:
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.FILTER:
        switch (this.selectedTabIndex) {
          case 2:
            this.radioRemovalFilter = !this.radioRemovalFilter;
            break;
          case 3:
            this.radioRemovalFilter = !this.radioRemovalFilter;
            break;
        }
        break;
    }
  }
  radioRemovalWorkList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true
    // otherParams['userId'] = this.loggedUser.userId
    let otherParamsAll = Object.assign(otherParams, this.filterData);
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.BALANCE_OF_CONTRACTS_WAIVE_FEE_DETAILS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParamsAll)
    ).pipe(map((res: IApplicationResponse) => {
      // if (res?.resources) {
      //   res?.resources?.forEach(val => {
      //     // val.suspendedOn = this.datePipe.transform(val.suspendedOn, 'dd-MM-yyyy, h:mm:ss a');
      //     return val;
      //   })
      // }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.resources) {
        // this.observableResponse = [data.resources];
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
      this.reset = false;
    });
  }

  resetForm() {
    this.radioRemovalFilter = !this.radioRemovalFilter;
    this.radioRemovalForm.reset();
    this.reset = true;
    // this.getRequiredListData();
  }
  btnCloseClick() {
    this.radioRemovalFilter = !this.radioRemovalFilter;
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  refresh() {
    let otherParams = {};
    this.radioRemovalWorkList(this.row['pageIndex'], this.row['pageSize'], otherParams);
  }
  onChangeStatus(rowData, ri) { }
  onChangeSelecedRows(e) {
    //this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}
