import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { AccountAddEditModalComponent } from "./account-add-edit-modal.component";


@NgModule({
    declarations: [AccountAddEditModalComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  entryComponents: [AccountAddEditModalComponent],
  exports: [AccountAddEditModalComponent],
  providers: []
})
export class AccountAddEditModalModule { }