import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, ModulesBasedApiSuffix, RxjsService, SharedModuleApiSuffixModels } from '@app/shared';
import { AnfModel, SharedOTPModel } from '@modules/customer/models/gprs-add-model';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-annual-network-fee-approval',
  templateUrl: './annual-network-fee-approval.component.html',
  styleUrls: ['./annual-network-fee-approval.component.scss']
})
export class AnnualNetworkFeeApprovalComponent implements OnInit {

  isViewMore = false
  countryCodes = countryCodes;
  anfDetail: any = {}
  anfForm: FormGroup
  otpForm: FormGroup
  statusDropdown = []
  callInitiationId = ""
  customerId = ""
  loggedUser: any
  customerAvailablityDropdown = []
  isHideForInternal = true
  isShowOtp = false;
  viewData = []
  isOtpVerified = false;
  otpDetails = {
    otpId: "",
    otp:""
  }
  addressId: string;

  constructor(private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private rxjsService: RxjsService, private crudService: CrudService,
    private formBuilder: FormBuilder, private router: Router) {
    this.activatedRoute.queryParams.subscribe(param => {
      this.callInitiationId = param['callInitiationId'];
      this.customerId = param['customerId'];
      this.addressId = param['addressId'] ? param['addressId'] : '';
    })
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.createAnfForm()
    this.getDetails()
    this.getStatusDropdown()
    this.getCustomerAvailablityDropdown()
    this.viewData = [
      { name: 'Customer ID', className: "col-4", value: "", order: 1 },
      { name: 'Customer Name', className: "col-4", value: "", order: 2 },
      { name: 'Customer Type', className: "col-4", value: "", order: 3 },
      { name: 'Select Site Address', className: "col-8", value: "", order: 4 },
      { name: 'Contract ID', className: "col-4", value: "", order: 5 }
    ]
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedUser = response[0];
    });
  }

  createAnfForm() {
    this.anfForm = this.formBuilder.group({});
    let gprsModel = new AnfModel();
    Object.keys(gprsModel).forEach((key) => {
      this.anfForm.addControl(key, new FormControl(gprsModel[key]));
    });

    this.otpForm = this.formBuilder.group({});
    let otpModel = new SharedOTPModel();
    Object.keys(otpModel).forEach((key) => {
      this.otpForm.addControl(key, new FormControl(otpModel[key]));
    });
    // this.anfDetail = setRequiredValidator(this.anfDetail, ["contractId"]);
    this.onFormChange()
  }

  onFormChange() {
    this.anfForm.get("isCustomerAgreedCharges").valueChanges.subscribe(value => {
      this.isHideForInternal = value
    })
  }
  getStatusDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_RADIO_DECODER_ANF_STATUS).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false)
      if (response.isSuccess && response.statusCode == 200) {
        this.statusDropdown = response?.resources
      }
    })
  }
  getCustomerAvailablityDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_CUSTOMER_AVAILABLITY).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false)
      if (response.isSuccess && response.statusCode == 200) {
        this.customerAvailablityDropdown = response?.resources
        this.onChangeCustomerAvailablity()
      }
    })
  }

  onChangeCustomerAvailablity() {
    let availablity = this.anfForm.get("customerAvailabilityId").value;
    let check = this.customerAvailablityDropdown.find(item => item.id == availablity)
    if (check?.displayName == "Customer Off-Site") {
      this.isShowOtp = true
    }
    if (check?.displayName == "Customer On-Site") {
      this.isShowOtp = false
    }
  }

  getDetails() {
    //'444F4360-7EE8-4722-B6A5-DCD85CEC64CF'
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.RADIO_DECODER_ANF, this.callInitiationId).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false)
      if (response.isSuccess && response.statusCode == 200) {
        this.anfDetail = response?.resources
        this.viewData = [
          { name: 'Customer ID', className: "col-4", value: this.anfDetail?.customerRefNo, order: 1, valueColor: '#166DFF', enableHyperLink: true },
          { name: 'Customer Name', className: "col-4", value: this.anfDetail?.customerName, order: 2 },
          { name: 'Customer Type', className: "col-4 ", value: this.anfDetail?.customerTypeName, order: 3 },
          { name: 'Select Site Address', className: "col-8", value: this.anfDetail?.fullAddress, order: 4 },
          { name: 'Contract ID', className: "col-4", value: this.anfDetail?.contractRefNo, order: 5 },
        ]
        this.otpForm.get("mobileNo").setValue(this.anfDetail?.mobileNo);
        this.otpForm.get("mobileNoCountryCode").setValue(this.anfDetail?.mobileNoCountryCode);
        this.otpForm.get("userId").setValue(this.customerId);
        this.otpForm.get("createdUserId").setValue(this.loggedUser?.userId);
        this.otpForm.get("createdDate").setValue(new Date().toDateString());
        this.otpForm.get("otpReferenceTypeId").setValue(3);
        this.anfForm.patchValue(response?.resources)
      }
    })
  }
  // f0b67167-978f-48a9-b2d8-8faf4610aff7
  onLinkClick() {
   this.goBack()
  }

  onSubmit() {
    if (this.anfForm.invalid) {
      return "";
    }
    this.anfForm.value.createdUserId = this.loggedUser.userId
    if(this.anfForm.value?.smsNotificationNumber){
      this.anfForm.value.smsNotificationNumber = this.anfForm.value?.smsNotificationNumber.toString().replace(/\s/g, "");
    }
    let formData = new FormData()
    formData.append('data', JSON.stringify(this.anfForm.value));
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.RADIO_DECODER_ANF, formData).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goBack()
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }

  sendOtp(){
   this.crudService.create(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.SEND_OTP, this.otpForm.value).subscribe(response=>{
     if(response.isSuccess && response.statusCode ==200){
       this.isOtpVerified = false;
       this.otpDetails.otpId = response?.resources;
     }
   })
  }

  verifyOtp(){
    this.crudService.update(ModulesBasedApiSuffix.SHARED, SharedModuleApiSuffixModels.VERIFY_OTP, this.otpDetails).subscribe(response=>{
      if(response.isSuccess && response.statusCode ==200){
        this.isOtpVerified = true;
      }
    })
  }
  goBack(){
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }
}
