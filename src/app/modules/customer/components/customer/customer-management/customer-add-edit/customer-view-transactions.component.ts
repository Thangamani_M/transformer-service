import { SelectionModel } from '@angular/cdk/collections';
import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatPaginator, MatSort, MatTabChangeEvent } from '@angular/material';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Customer, CustomerAddress, CustomerContact, PaymentInvoiceFormModel, PaymentOverrideFormModel, PaymentUnderrideFormModel, Transactions } from '@app/modules/customer/models';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, countryCodes, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, ExtensionModalComponent, formConfigs, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControlError, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { KeyHolders } from '@modules/customer/models/keyholder';
import { CUSTOMER_COMPONENT } from '@modules/customer/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { BillingModuleApiSuffixModels, NatureOfDebtor, SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/components/table/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { PromiseToPayDetailComponent } from './promise-to-pay-quote-detail/promise-to-pay-quote-detail.component';
import { TransactionProcessNumberComponent } from './transaction-number/transaction-number.component';

declare var $;
enum QuickActionTransactionsTab {
  CREATE_INVOICE = "Create Invoice",
  SEND_INVOICE = "Send Invoice",
  CREDIT_NOTE = "Credit Note",
  DEBIT_NOTE = "Debit Note",
  CREDIT_AND_RE_INVOICE = "Credit and Re-Invoice",
  BAD_DEBIT_WRITE_OFF = "Bad Debit Write Off",
  BAD_DEBIT_RECOVERY = "Bad Debit Recovery",
  BAD_DEBIT_RECOVERY_REVERSAL = "Bad Debit Recovery-Reversal",
  REFUNDS = "Refunds",
  ALLOCATE_TRANSACTION = 'Allocate Transaction',
  PAYMENT_OVERRIDE = 'Payment Override',
  PAYMENT_UNDERRIDE = 'Payment Underride',
  STATEMENT = "Statement",
  VIEW_TRANSACTIONS = 'View Transactions',
  TRANSFER = 'Transfer',
  PAYMENT_SPLIT = 'Payment Split',
  REQUEST_RE_INSTATEMENT = 'Request Re-instatement',
  REMOVE_CREDIT_NOTES = 'Remove Credit Notes',
  REMOVE_PAYMENT_OPTION = 'Remove Payment Option',
}

@Component({
  selector: 'app-customer-view-transaction',
  templateUrl: './customer-view-transactions.component.html',
  styleUrls: ['./customer-view-transactions.component.scss']
})

export class CustomerViewTransactionComponent implements OnInit {
  displayedColumns: string[] = ['ticketNumber', 'ticketGroupName', 'ticketTypeName', 'createdBy', 'assignedTo', 'createdOn', 'lastUpdate', 'ticketStatusName'];
  displayedColumnsSchedule: string[] = ['customerCallbackId', 'callbackDateTime', 'callbackQueueName', 'customerName', 'customerNumber', 'callbackStatusName'];
  @Input() id: string;
  @ViewChildren('input') inputs: QueryList<any>;
  paymentOverrideForm: FormGroup;
  paymentUnderrideForm: FormGroup;
  paymentInvoiceForm: FormGroup;
  transactions: FormArray;
  @ViewChildren(Table) tables: QueryList<Table>;
  countryCodes = countryCodes;
  customerAddressAddEditForm: FormGroup;
  customerContactAddEditForm: FormGroup;
  customerDebtorForm: FormGroup;
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  manualInvoiceApprovalId
  formConfigs = formConfigs;
  applicationResponse: IApplicationResponse;
  natureOfDebtor = NatureOfDebtor;
  customer: Customer[];
  customerAddress: CustomerAddress[];
  customerContact: CustomerContact[];
  customerKeyholder: KeyHolders[];
  errorMessage: string;
  loggedInUserData: LoggedInUserModel;
  btnName: string;
  agentExtensionNo;
  @Output() selectedTabChange: EventEmitter<MatTabChangeEvent>;
  selection = new SelectionModel<Customer>(true, []);
  ddlCustIdFilter = '';
  dataSource = new MatTableDataSource<Customer>();
  dataSourceSchedule = new MatTableDataSource<any>();
  custList = [];
  pageSize: number = 10;
  custAddressList = '';
  newCustAddressList = '';
  addrTypeList = '';
  keyHolderList = '';
  newProvinceNameList;
  tmpCustomerId;
  tmpcustomerName;
  isLoading = true;
  isRelocated = false;
  custInfoList;
  pageLimit: number[] = [5, 25, 50, 75, 100];
  pageIndex: number = 0
  limit: number = 5;
  totalLength;
  SearchText = { Search: '' };
  relocationIndex;
  observableResponse;
  selectedTabIndex = 0;
  loading = false;
  status = [];
  dataList;
  totalRecords;
  selectedColumns = [];
  selectedRows = [];
  selectedRowsTemp = [];
  selectedRowInvoiceId: string = "";
  selectedRow;
  expandIndex = 0;
  minDate = new Date();
  columnFilterForm: FormGroup;
  row = {}
  searchColumns;
  searchForm: FormGroup;
  otherParams;
  paymentUnderride: boolean = false;
  paymentOverride: boolean = false;
  invoiceInfo: boolean = false;
  removeAlert: boolean = false;
  isPaymentRemove: boolean = false
  callWrapupPopup: boolean = false;
  debtorInfoObservableResponse;
  debtorInfoDataList;
  customerAddresses = [];
  addressId;
  idsObject;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  customerAddressList;
  subscriptionsList;
  detailsObj;
  titleList;
  primengTableConfigProperties;
  primengTableConfigPropertiesBalanceOfContract;
  detailsData: IApplicationResponse | any;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  today = new Date()
  todayDate = new Date('1984');
  debtorId;
  show;
  showFilter: boolean;
  details;
  selectedIdPo = [];
  selectedInvoices = [];
  selectedInvoiceIds = [];
  doRunData;
  totalAmt;
  invoiceDetails;
  invoiceDetailsArr = [];
  alternateEmail;
  isAlternateEmail: boolean = false;
  startDate = new Date();
  totalAmtShown: number;
  noOfPayments = [];
  contractId;
  transactionType = 'transaction';
  debitAmount = 0;
  outstandingAmount = 0;
  creditAmount = 0;
  startTotal = 0;
  total = 0;
  type;
  pendingTransactionPrimengTableConfigProperties;
  isOpenBalanceOfContract: boolean = false
  isEnableWieverViewBtn: boolean = false;
  transactionId;
  pendingTransactionPopup = false;
  subTransaction = [];
  paymentDetails = [];
  templateList = [];
  invoiceOverrideSplitTypeIdSplit = "";
  invoiceOverrideSplitTypeIdRemove = "";
  isNotInvoiceType = false;

  constructor(private dialog: MatDialog, private crudService: CrudService, private tableFilterFormService: TableFilterFormService, private snackbarService: SnackbarService, private momentService: MomentService,
    private _fb: FormBuilder, public dialogService: DialogService, private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>,
    private rxjsService: RxjsService) {
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ];

    for (let i = 2; i < 6; i++) {
      this.noOfPayments.push(i);
    }

    this.primengTableConfigProperties = {
      tableCaption: "Recent Activities",
      selectedTransactionTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Recent Activities', relativeRouterUrl: '' }, { displayName: 'Recent Activities' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'TRANSACTIONS',
            dataKey: 'transactionRefNo',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: true,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'postDate', header: 'Post Date', width: '200px' },
            { field: 'transactionType', header: 'Type', width: '100px' },
            { field: 'transactionRefNo', header: 'Transaction Number', width: '200px' },
            { field: 'transactionDescription', header: 'Description', width: '150px' },
            { field: 'referenceNumber', header: 'Reference No', width: '150px' },
            { field: 'debitAmount', header: 'Debit', width: '100px' },
            { field: 'creditAmount', header: 'Credit', width: '100px' },
            { field: 'outstandingAmount', header: 'Outstanding', width: '100px' },
            { field: 'paymentStatus', header: 'Payment Status', width: '100px' },

            ],
            moduleName: ModulesBasedApiSuffix.BILLING,
            apiSuffixModel: SalesModuleApiSuffixModels.PAYMENT_TRANSACTION_LIST,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'PENDING TRANSACTIONS',
            dataKey: 'indexKey',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'transactionRefNo', header: 'Transaction Number', width: '170px' },
              { field: 'transactionType', header: 'Transaction Type', width: '160px' },
              { field: 'transactionDescription', header: 'Description', width: '150px' },
              { field: 'referenceNumber', header: 'Reference No', width: '120px' },
              { field: 'paymentType', header: 'Payment Type', width: '130px' },
              { field: 'runCode', header: 'Run Code', width: '120px' },
              { field: 'startTotal', header: 'Start Total', width: '100px' },
              { field: 'total', header: 'Totals', width: '100px' },
              { field: 'debitDate', header: 'Transaction Date', width: '100px' },
            ],
            moduleName: ModulesBasedApiSuffix.BILLING,
            apiSuffixModel: SalesModuleApiSuffixModels.PENDING_TRANSACTION_LIST,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'CLIENT QUOTES',
            dataKey: 'customerCallbackId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: true,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'date', header: 'Quotation Date' },
            { field: 'quotationNumber', header: 'Quotation Number' },
            { field: 'description', header: 'Description' },
            { field: 'referenceNumber', header: 'Reference No' },
            { field: 'total', header: 'Total Amount' },
            { field: 'status', header: 'Status' },
            ],
            moduleName: ModulesBasedApiSuffix.BILLING,
            apiSuffixModel: SalesModuleApiSuffixModels.CLIENT_QUOTES,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            disabled: true
          },
          {
            caption: 'PROMISE TO PAY ON ACCEPTED QUOTE',
            dataKey: 'quotationNumber',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'quotationNumber', header: 'Quotation Number' },
              { field: 'paymentOption', header: 'Payment Option' },
              { field: 'quotedAmount', header: 'Quotated Amount' },
              { field: 'paymentMethod', header: 'Payment Method' },
            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            moduleName: ModulesBasedApiSuffix.BILLING,
            apiSuffixModel: BillingModuleApiSuffixModels.PROMISE_TO_PAY,
            disabled: true
          },
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.id = this.activatedRoute.snapshot.queryParams?.id;
    this.debtorId = this.activatedRoute.snapshot.queryParams?.debtorId;
    this.addressId = this.activatedRoute.snapshot.queryParams?.addressId;
    this.transactionId = this.activatedRoute.snapshot.queryParams?.transactionId;
    this.type = this.activatedRoute.snapshot.queryParams?.type;
    this.selectedTabIndex = this.activatedRoute.snapshot?.queryParams?.tab ? +this.activatedRoute.snapshot?.queryParams?.tab : 0;
    this.activatedRoute.paramMap.subscribe((params) => {
      this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
      this.searchKeywordRequest();
      this.columnFilterRequest();
      this.combineLatestNgrxStoreData()
      this.getRequiredListData();
      this.createPaymentOverideForm();
      this.createPaymentUnderideForm();
      this.createInvoiceForm();
      this.getCustomerInfo();
      this.getDoRunata();
      this.getPaymentSplitTypes()
      this.getTemplate();
      this.closeNav();
    });
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {

      }
    });
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;
    return [day, month, year].join('-');
  }

  getDoRunata() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_DORUN, null, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.doRunData = response.resources;
          let filter = this.doRunData.filter(x => x.displayName == '1');
          if (filter && filter.length > 0) {

            this.paymentOverrideForm.get('debitOrderRunCodeId').setValue(filter[0].id);

          }

        }
      });
  }

  openTransaction: boolean = false;
  allocatedTransaction: boolean = false;
  createPaymentUnderideForm() {
    let paymentUnderrideFormModel = new PaymentUnderrideFormModel();
    this.paymentUnderrideForm = this._fb.group({
    });
    Object.keys(paymentUnderrideFormModel).forEach((key) => {
      this.paymentUnderrideForm.addControl(key, new FormControl(paymentUnderrideFormModel[key]));
    });
    this.paymentUnderrideForm = setRequiredValidator(this.paymentUnderrideForm, ["amount", "motivation"]);
  }

  createPaymentOverideForm() {
    let paymentOverrideFormModel = new PaymentOverrideFormModel();
    this.paymentOverrideForm = this._fb.group({
      transactions: this._fb.array([
      ])
    });
    Object.keys(paymentOverrideFormModel).forEach((key) => {
      this.paymentOverrideForm.addControl(key, new FormControl(paymentOverrideFormModel[key]));
    });
    this.paymentOverrideForm = setRequiredValidator(this.paymentOverrideForm, ["debitOrderRunCodeId", "transactionDescription", "notes"]);
    this.isOtpVerified = false;
    this.otpId = "";
    this.otp = "";

  }

  createInvoiceForm() {
    let paymentInvoiceFormModel = new PaymentInvoiceFormModel();
    this.paymentInvoiceForm = this._fb.group({
    });
    Object.keys(paymentInvoiceFormModel).forEach((key) => {
      this.paymentInvoiceForm.addControl(key, new FormControl(paymentInvoiceFormModel[key]));
    });
    this.paymentInvoiceForm = setRequiredValidator(this.paymentInvoiceForm, ["isCustomer", "isDebtor", "customerEmail", "debtorEmail","templateId"]);
  }

  createTransactionListModel(transactions?: Transactions): FormGroup {
    let transactionsModel = new Transactions(transactions);
    let formControls = {};
    Object.keys(transactionsModel).forEach((key) => {
      if (key == 'description' || key == 'invoicePaymentAndOverrideId' || key == 'invoiceRefNo') {
        formControls[key] = [{ value: transactionsModel[key], disabled: false }]
      } else {
        formControls[key] = [{ value: transactionsModel[key], disabled: false }, [Validators.required]]
      }
    });
    return this._fb.group(formControls);
  }

  get getTransactionsListArray(): FormArray {
    if (!this.paymentOverrideForm) return;
    return this.paymentOverrideForm.get("transactions") as FormArray;
  }

  getTemplate() {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UX_EMAIL_TEMPLATE,
      undefined,
      false, null, 1).subscribe(data => {
        this.templateList = data.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    // this.httpService.dropdown(ModulesBasedApiSuffix.BILLING, ),
  }
  getCustomerInfo() {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.CUSTOMER_DEBTOR_ADDRESS,
      undefined,
      false, prepareRequiredHttpParams({
        // customerId:'FDEE15E2-AF32-4545-B221-AC06F2C028D6',
        // debtorId :'130A3803-443D-44D6-B10E-9B9327991BB1'
        customerId: this.id,
        debtorId: this.debtorId,
        addressId: this.addressId

      }, 1)
    ).subscribe(data => {
      this.detailsData = data.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  // Page Level access
  allPermissions: any

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.store.pipe(select(agentLoginDataSelector)),
    this.store.select(currentComponentPageBasedPermissionsSelector$)])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.agentExtensionNo = response[1];
        this.allPermissions = response[2];
        let permissions = response[2][CUSTOMER_COMPONENT.CUSTOMER];
        let customerManagement = permissions.find(item => item.menuName == "Customer Dashboard")
        if (customerManagement) {
          let techModulePermission = customerManagement['subMenu'].find(item => item.menuName == "TRANSACTIONS");
          if (techModulePermission) {
            let menus = techModulePermission['subMenu']?.filter(item => item.menuName != "List");
            let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, menus);
            this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
            this.primengTableConfigProperties.tableComponentConfigs.tabsList.forEach(item => {
              let find = techModulePermission['subMenu'].find(data => data.menuName == item.caption);
              if (find) {
                item.quickAction = find['subMenu'].find(data => data.menuName == 'Quick Action');
              }
            })
          }
        }
      });
  }
  getQuickActionTypePermissions(actionTypeMenuName): boolean {
    let quickAction = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].quickAction
    if (quickAction) {
      let foundObj = quickAction['subMenu'].find(fSC => fSC.menuName == actionTypeMenuName);
      if (foundObj) {
        return false;
      }
      else {
        return true;
      }
    } else {
      return true;

    }
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.dataList = [];
    let obj1 = {
      addressId: this.addressId,
      customerId: this.id,
      debtorId: this.debtorId,
      isOpenTransactions: this.openTransaction,
      isAllocatedTransactions: this.allocatedTransaction
    };
    if (this.contractId) {
      obj1['contractId'] = this.contractId;
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    if (this.selectedTabIndex == 3) {
      let finalObj = {
        addressId: this.addressId,
        customerId: this.id,
        debtorId: this.debtorId
      };
      otherParams = finalObj;
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        this.debitAmount = 0; this.outstandingAmount = 0; this.creditAmount = 0; this.startTotal = 0; this.total = 0;
        res?.resources?.forEach((val, index) => {
          if (this.selectedTabIndex == 0) {
            if (val.transactionType != 'BW' && val.transactionType != 'BR' && val.transactionType != '') {
              this.debitAmount = val.debitAmount + this.debitAmount;
              this.outstandingAmount = val.outstandingAmount + this.outstandingAmount;
              this.creditAmount = val.creditAmount + this.creditAmount;
            }
            val.index = index;
            val.debitAmount = val.debitAmount ? val.debitAmount.toFixed(2) : '0.00';
            val.creditAmount = val.creditAmount ? val.creditAmount.toFixed(2) : '0.00';
            val.outstandingAmount = val.outstandingAmount ? val.outstandingAmount.toFixed(2) : '0.00';
            // { field: 'debitAmount', header: 'Debit',width:'100px' },
            // { field: 'creditAmount', header: 'Credit' ,width:'100px'},
            // { field: 'outstandingAmount', header: 'Outstanding',width:'100px' },
            // val.postDate =  val?.postDate ? this.datePipe.transform(val.postDate, 'dd-MM-yyyy') : '-';
          } else if (this.selectedTabIndex == 1) {
            val.indexKey = index
            this.startTotal = val.startTotal + this.startTotal;
            this.total = val.total + this.total;
            if (val.debitDate) {
              let split = val.debitDate.split(" ");
              val.debitDate = split.length > 0 ? split[0] : null;
              val.debitDate = val.debitDate?.replaceAll("/", "-")
            }
          }
          return val;
        });

      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);

      if (data.isSuccess && data.resources) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
    });
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  isPaymentOverrideAllow: Boolean = true;
  openDialog(flag) {
    this.selectedIdPo = [];
    this.selectedInvoices = [];
    this.selectedRows.forEach(element => {
      this.selectedIdPo.push(element.invoicePaymentAndOverrideIds);
      this.selectedInvoices.push(element?.invoiceRefNo);
      this.selectedInvoiceIds.push(element.invoiceId)
    });
    if (flag == 'create-invoice') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.CREATE_INVOICE);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (!this.contractId) {
        this.snackbarService.openSnackbar("Please choose contract", ResponseMessageTypes.WARNING);
      } else {
        this.router.navigate(['customer/manage-customers/manual-invoice'], { queryParams: { contractId: this.contractId, customerId: this.id, addressId: this.addressId, debtorId: this.debtorId } });
      }
    }
    if (flag == 'Credit-Note') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.CREDIT_NOTE);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows && this.selectedRows.length > 0) {
        if (this.selectedRows[0].invoicePaymentAndOverrideIds) {
          if (this.selectedRows[0].outstandingAmount == 0 && this.selectedRows[0].creditAmount > 0)
            this.snackbarService.openSnackbar("You cannot set credit notes.", ResponseMessageTypes.WARNING);
          else
            this.getCreditNoteData(this.selectedRows[0].invoicePaymentAndOverrideIds, 'credit-note', null);
        }
        else
          this.snackbarService.openSnackbar("You cannot set credit notes.", ResponseMessageTypes.WARNING);
      }
    }
    if (flag == 'Debit-Note') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.DEBIT_NOTE);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows && this.selectedRows.length > 0) {
        if (this.selectedRows[0].creditNoteTransactionId) {
          if (this.selectedRows[0].transactionType == 'CN')
            this.getCreditNoteData(this.selectedRows[0].creditNoteTransactionId, 'debit-note', this.selectedRows[0].invoicePaymentAndOverrideIds);
          else
            this.snackbarService.openSnackbar("You cannot set debit notes.", ResponseMessageTypes.WARNING);
        }
        else
          this.snackbarService.openSnackbar("You cannot set debit notes.", ResponseMessageTypes.WARNING);
      }
    }
    if (flag == 'credit-reinvoice') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.CREDIT_AND_RE_INVOICE);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows && this.selectedRows.length > 0) {
        if (this.selectedRows[0].invoicePaymentAndOverrideIds) {
          this.router.navigate(['customer/manage-customers/view-transactions/credit-and-reinvoice'], { queryParams: { invoicePaymentAndOverrideIds: this.selectedRows[0].invoicePaymentAndOverrideIds, addressId: this.addressId, serviceType: this.selectedRows[0].transactionDescription, customerId: this.id, debtorId: this.debtorId, debtorAccountDetailId: this.detailsData?.debtor?.debtorAccountDetailId,type: 'credit-and reinvoice' } });
        }
      }
      else
        this.snackbarService.openSnackbar("Please select one transaction for Credit and Re-Invoice", ResponseMessageTypes.WARNING);
    }
    if (flag == 'bad-debit-writeoff') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.BAD_DEBIT_WRITE_OFF);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows && this.selectedRows.length > 0) {
        if (this.selectedRows[0].invoicePaymentAndOverrideIds) {

          if (this.selectedRows[0].transactionType == 'IN') {
            let invoicePaymentAndOverrideID = [];
            this.selectedRows.forEach(element => {
              if (element?.invoicePaymentAndOverrideIds)
                invoicePaymentAndOverrideID.push(element?.invoicePaymentAndOverrideIds);
            });
            this.router.navigate(['customer/manage-customers/view-transactions/bad-debit-writeOff'], { queryParams: { invoicePaymentAndOverrideIds: invoicePaymentAndOverrideID?.toString(), addressId: this.addressId, customerId: this.id, debtorId: this.debtorId, type: 'bad-debit-writeOff' } });
          }
          else
            this.snackbarService.openSnackbar("You cannot set bad debit writeoff.", ResponseMessageTypes.WARNING);
        }
        else
          this.snackbarService.openSnackbar("You cannot set bad debit writeoff.", ResponseMessageTypes.WARNING);
      }
      else
        this.snackbarService.openSnackbar("Please select one transaction for bad write off", ResponseMessageTypes.WARNING);
    }
    if (flag == 'bad-debit-recovery') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.BAD_DEBIT_RECOVERY);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows && this.selectedRows.length > 0) {
        if (this.selectedRows[0].creditNoteTransactionId) {
          if (this.selectedRows[0].transactionType == 'BW')
            this.router.navigate(['customer/manage-customers/view-transactions/bad-debit-recovery'], { queryParams: { creditNoteTransactionId: this.selectedRows[0].creditNoteTransactionId, addressId: this.addressId, customerId: this.id, debtorId: this.debtorId, type: 'bad-debit-recovery' } });
          else
            this.snackbarService.openSnackbar("You cannot set bad debit recovery.", ResponseMessageTypes.WARNING);
        }
        else
          this.snackbarService.openSnackbar("You cannot set bad debit recovery.", ResponseMessageTypes.WARNING);
      }
      else
        this.snackbarService.openSnackbar("Please select one transaction for bad debit recovery", ResponseMessageTypes.WARNING);
    }
    if (flag == 'bad-debit-reversal') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.BAD_DEBIT_RECOVERY_REVERSAL);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows && this.selectedRows.length > 0) {
        if (this.selectedRows[0].creditNoteTransactionId) {
          if (this.selectedRows[0].transactionType == 'BR')
            this.router.navigate(['customer/manage-customers/view-transactions/bad-debit-reversal'], { queryParams: { creditNoteTransactionId: this.selectedRows[0].creditNoteTransactionId, addressId: this.addressId, customerId: this.id, debtorId: this.debtorId, type: 'bad-debit-reversal' } });
          else
            this.snackbarService.openSnackbar("You cannot set bad debit reversal.", ResponseMessageTypes.WARNING);
        }
        else
          this.snackbarService.openSnackbar("You cannot set bad debit reversal.", ResponseMessageTypes.WARNING);
      }
      else
        this.snackbarService.openSnackbar("Please select one transaction for bad debit reversal", ResponseMessageTypes.WARNING);
    }
    if (flag == 'refunds') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.REFUNDS);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows && this.selectedRows.length > 0) {
        if (this.selectedRows[0].referenceId) {
          if (this.selectedRows[0].transactionType == 'PA')
            this.router.navigate(['customer/manage-customers/view-transactions/refunds'], { queryParams: { referenceId: this.selectedRows[0].referenceId, addressId: this.addressId, customerId: this.id, debtorId: this.debtorId, type: 'refunds' } });
          else
            this.snackbarService.openSnackbar("You cannot set refunds.", ResponseMessageTypes.WARNING);
        }
        else
          this.snackbarService.openSnackbar("You cannot set refunds.", ResponseMessageTypes.WARNING);
      }
      else
        this.snackbarService.openSnackbar("Please select one transaction for refunds", ResponseMessageTypes.WARNING);
    }
    if (flag == 'remove-credit-Note') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.REMOVE_CREDIT_NOTES);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows && this.selectedRows.length > 0) {
        if (this.selectedRows[0].creditNoteTransactionId)
          this.removecreditNote(this.selectedRows[0].creditNoteTransactionId);
        else
          this.snackbarService.openSnackbar("No Credit Note Transaction exists", ResponseMessageTypes.WARNING);
      } else {
        this.snackbarService.openSnackbar("Please choose transaction", ResponseMessageTypes.WARNING);
      }
    }
    if (flag == 'allocate-transaction') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.ALLOCATE_TRANSACTION);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows && this.selectedRows.length > 0) {
        if (this.selectedRows[0].transactionType == 'PA' || this.selectedRows[0].transactionType == 'CN') {
          this.router.navigate(['customer/manage-customers/view-transactions/offset'], { queryParams: { referenceId: this.selectedRows[0].referenceId, ReferenceTypeId: 1, addressId: this.addressId, customerId: this.id, debtorId: this.debtorId, type: 'offset' } });
        }
      }
      else
        this.snackbarService.openSnackbar("Please select one transaction to Allocate Transaction", ResponseMessageTypes.WARNING);
    }
    if (flag == 'view-transaction') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.VIEW_TRANSACTIONS);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.manualInvoiceApprovalId) {
        this.router.navigate(['customer/manage-customers/transaction-view'], { queryParams: { contractId: this.contractId, customerId: this.id, addressId: this.addressId, debtorId: this.debtorId, manualInvoiceApprovalId: this.manualInvoiceApprovalId, transactionType: this.transactionType } });
      } else {
        this.openModelForPendingTransactions();
        // if(this.selectedRowsTemp[0].transactionDescription == "Payment Split Debit"){
        //   this.openModelForPendingTransactions()
        // }else{
        // this.snackbarService.openSnackbar("Record is not manual invoice , So can not able to see view", ResponseMessageTypes.WARNING);
        // }
      }
    }
    if (flag == 'payment-underride') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.PAYMENT_UNDERRIDE);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows.length == 0) {
        this.snackbarService.openSnackbar("Please select atleast one payment", ResponseMessageTypes.WARNING);
      } else if (this.selectedRows.length == 1 && this.selectedRows[0]?.invoicePaymentAndOverrideIds) {
        this.createPaymentUnderideForm();
        this.paymentUnderride = true;
        this.getOverrideInfo(2);
      } else {
        this.snackbarService.openSnackbar("Please select only one payment", ResponseMessageTypes.WARNING);
      }
    }
    if (flag == 'debit-order') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.PAYMENT_OVERRIDE);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows.length == 0) {
        this.snackbarService.openSnackbar("Please select atleast one invoice", ResponseMessageTypes.WARNING);
      } else {
        if (this.selectedRows[0].transactionDescription == 'Recurring Billing' || this.selectedRows[0].transactionType == null || this.selectedRows[0].transactionDescription == 'Pro-Rata' || this.selectedRows[0].transactionType == 'QN' || this.selectedRows[0].transactionType == 'PA') {
          this.snackbarService.openSnackbar("Debit Order  is not possible", ResponseMessageTypes.WARNING);
          return;
        }
        if (this.selectedRows[0].outstandingAmount > 0) {
          this.createPaymentOverideForm();
          this.paymentOverride = true;
          this.isPaymentRemove = false;

          this.getDoRunata();
          this.getOverrideInfo(1);
        }
        else
          this.snackbarService.openSnackbar("Debit Order cannot be done due to insufficient amount", ResponseMessageTypes.WARNING);
      }
    }
    if (flag == 'request-reinstatement') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.REQUEST_RE_INSTATEMENT);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      this.router.navigate(['customer/manage-customers/view-transactions/request-reinstatement'], { queryParams: { customerId: this.id, addressId: this.addressId, debtorId: this.debtorId } });
    }
    if (flag == 'invoice-info') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.SEND_INVOICE);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows.length == 0) {
        this.snackbarService.openSnackbar("Please select atleast one record", ResponseMessageTypes.WARNING);
      } else {
        this.invoiceInfo = true;
        this.createInvoiceForm();
        this.emailCheck()
        this.getInvoiceInfo();
      }
    }
    if (flag == 'remove-payment') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.REMOVE_PAYMENT_OPTION);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows.length == 0) {
        this.snackbarService.openSnackbar("Please select atleast one invoice", ResponseMessageTypes.WARNING);
      } else if (this.selectedRows.length == 1) {
        if (this.selectedRows[0].transactionType == "PO") {
          this.isPaymentRemove = true;
          this.createPaymentOverideForm();
          this.paymentOverride = true;
          this.getDoRunata();
          this.getOverrideInfo(1);
        } else {
          this.removeAlert = true;
        }
      } else {
        this.snackbarService.openSnackbar("Only one invoice can be deleted at a time", ResponseMessageTypes.WARNING);
      }
    }
    if (flag == 'payment-transfer') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.TRANSFER);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows.length == 0) {
        this.snackbarService.openSnackbar("Please select any one item", ResponseMessageTypes.WARNING);
        return
      }
      if (this.selectedRows[0].invoicePaymentAndOverrideIds) {
        this.router.navigate(['customer/manage-customers/payment-transfer'], { queryParams: { debitAmount: this.selectedRows[0].debitAmount, paymentTransferId: this.selectedRows[0].invoiceId, paymentTransferRefNo: this.selectedRowsTemp[0].transactionRefNo, paymentTransferDate: this.selectedRowsTemp[0].postDate, type: 'admin', creditType: 'payment-transfer', contractId: this.contractId, invoicePaymentAndOverrideIds: this.selectedRows[0].invoicePaymentAndOverrideIds, customerId: this.id, addressId: this.addressId, parentDebtorId: this.debtorId } });
      }
      else {
        this.snackbarService.openSnackbar("You cannot set Payment Transfer", ResponseMessageTypes.WARNING);
      }
    }
    if (flag == 'payment-split') {
      let isAccessDeined = this.getQuickActionTypePermissions(QuickActionTransactionsTab.PAYMENT_SPLIT);
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }

      if (this.selectedRows.length == 0) {
        this.snackbarService.openSnackbar("Please select atleast one invoice", ResponseMessageTypes.WARNING);
      } else {
        if (this.selectedRows[0]?.transactionType != "PA") {
          this.snackbarService.openSnackbar("You cannot set split payment.", ResponseMessageTypes.WARNING);

          // this.snackbarService.openSnackbar("Transaction not eligible for split payment", ResponseMessageTypes.WARNING);
        } else {
          this.router.navigate(['customer/manage-customers/payment-split'], { queryParams: { referenceId: this.selectedRowInvoiceId, customerId: this.id, addressId: this.addressId, debtorId: this.debtorId } });
        }

      }
    }


    this.rxjsService.setGlobalLoaderProperty(false);
    this.rxjsService.setDialogOpenProperty(true);
  }
  getRowItems(rowData, type) {

    if (type == 'pi pi-chevron-right' && rowData.invoiceId) {
      this.crudService.get(ModulesBasedApiSuffix.BILLING,
        BillingModuleApiSuffixModels.PAYMENT_TRANSACTION_STOCK_LIST, null, null,
        prepareRequiredHttpParams({ ReferenceId: rowData.invoiceId }))
        .subscribe((response: IApplicationResponse) => {

          if (response.isSuccess && response.statusCode === 200 && response.resources) {
            if (response.resources.invoiceStcokDetails) {
              this.subTransaction[rowData.index] = response.resources.invoiceStcokDetails;
            }
            else {
              this.subTransaction[rowData.index] = null;
            }
            if (response.resources.invoicePaymentDetails) {
              this.paymentDetails[rowData.index] = response.resources.invoicePaymentDetails;
            }
            else {
              this.paymentDetails[rowData.index] = null;
            }

          } else {
            this.subTransaction[rowData.index] = null;
            this.paymentDetails[rowData.index] = null;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    else {
      this.subTransaction[rowData.index] = null;
      this.paymentDetails[rowData.index] = null;
    }
  }
  getCreditNoteData(id, type, invoicePaymentAndOverrideIds?: any) {
    this.rxjsService.setGlobalLoaderProperty(true);
    type = (type == 'debit-note') ? CollectionModuleApiSuffixModels.DEBIT_NOTES : CollectionModuleApiSuffixModels.CREDIT_NOTES;
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, type, id, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200 && response.resources) {
          if (response.resources.transactionInvoiceItem && response.resources.outstandingBalance) {
            this.router.navigate(['customer/manage-customers/view-transactions/transaction-process'], { queryParams: { type: 'admin', id: id, creditType: type, invoicePaymentAndOverrideIds: invoicePaymentAndOverrideIds, debtorId: this.debtorId, addressId: this.addressId, ids: this.id } });
          }
          else {
            this.snackbarService.openSnackbar("No transactionInvoiceItem", ResponseMessageTypes.WARNING)
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  removecreditNote(creditNoteTransactionId) {
    let params = {
      creditNoteTransactionId: creditNoteTransactionId,
      isActive: false,
      modifiedUserId: this.loggedInUserData.userId
    }
    this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_NOTES_DEACTIVATE, params).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setDialogOpenProperty(false);
      }
    });
  }

  onChangeAmount(i) {
    this.getTransactionsListArray.controls[i].get('amount').valueChanges.subscribe((amt: string) => {
      if (amt == '') {
        amt = '0'
      }
      if (parseFloat(amt) > this.totalAmtShown) {
        this.getTransactionsListArray.controls[i].get('amount').setErrors({ 'dgdgdgdg': true });
      } else {
        this.getTransactionsListArray.controls[i].get('amount').setErrors(null);

      }
      let remainAmt = this.totalAmtShown - parseFloat(amt);
      let total = 0;
      let noOfPayments;
      for (let j = 0; j < this.transactions.value.length; j++) {
        if (!this.transactions.value[j].amount) {
          this.transactions.value[j].amount = 0;
        }
        if (i == j) {

        } else {
          if (this.paymentOverrideForm.get('isPaymentTypeSingle').value) {
            noOfPayments = this.details.invoices.length;
          } else {
            noOfPayments = this.paymentOverrideForm.get('noOfPayments').value
          }
          let cal = remainAmt / (noOfPayments - 1);
          this.transactions.value[j].amount = cal;
          this.getTransactionsListArray.controls[j].get('amount').setValue(cal);
        }
        total = parseFloat(this.transactions.value[j].amount) + total;
      }
      if (this.paymentOverrideForm.get('isPaymentTypeSingle').value && this.details.invoices.length == 1) {
        this.totalAmt = amt;
        let desc = 'Debit Order Payment-Thank you Submitted on ' + this.formatDate(this.minDate) + ' for total R ' + this.totalAmt;
        this.paymentOverrideForm.get('transactionDescription').setValue(desc);
      } else {
        this.totalAmt = total;
        let desc = 'Debit Order Payment-Thank you Submitted on ' + this.formatDate(this.minDate) + ' for total R ' + this.totalAmtShown;
        this.paymentOverrideForm.get('transactionDescription').setValue(desc);
      }
    });
  }

  closeOpenAction = false;
  closeNav() {
    if(document.getElementById("quickaction_sidebar")){
      document.getElementById("quickaction_sidebar").style.width = "0";
      $('.main-container').css({ width: '100%' });
      $('#quickaction_sidebar .quick-action-menu-con').css({ right: '0', visibility: 'hidden' });
    }
  }

  openNav() {
    this.closeOpenAction = !this.closeOpenAction;
    if (!this.closeOpenAction) {
      this.closeNav();
    } else {
      document.getElementById("quickaction_sidebar").style.width = "250px";
      $('.main-container').css({ width: 'calc(100% - 250px)' });
      $('#quickaction_sidebar .quick-action-menu-con').css({ right: '12px', visibility: 'visible' });
    }
  }

  getOverrideInfo(transactionType) {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      EventMgntModuleApiSuffixModels.PAYMENT_OVERRIDE,
      undefined,
      false, prepareGetRequestHttpParams(null, null, {
        invoicePaymentAndOverrideIds: this.selectedIdPo?.toString(),
        customerId: this.id,
        debtorId: this.debtorId,
        TransactionType: transactionType
      })
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      this.transactions = this.getTransactionsListArray;
      this.transactions.clear();
      if (data.isSuccess && data.resources) {
        this.details = data.resources;

        let len = this.details.accountNo.length - 4;
        let first = this.details.accountNo.substring(0, len);
        let last = this.details.accountNo.substring(len, this.details.accountNo.length);
        let str = ''
        for (let i = 0; i < first.length; i++) {
          str = str + '*';
        }
        this.details.accountNo = str + '' + last;
        this.details.invoiceRefNo = this.selectedInvoices?.toString();
        console.log(this.details)
        console.log(this.paymentOverrideForm)
        this.paymentOverrideForm.patchValue(this.details);
        if (this.details.invoices.length > 0) {
          let total = 0;
          this.details.invoices.forEach((serviceListModel) => {
            serviceListModel.debitDate = new Date(serviceListModel.debitDate)
            this.startDate = serviceListModel.debitDate;
            total = serviceListModel.amount + total;
            this.transactions.push(this.createTransactionListModel(serviceListModel));
          });
          this.totalAmt = total;
          this.totalAmtShown = total;
          let desc = 'Debit Order Payment-Thank you Submitted on ' + this.formatDate(this.minDate) + ' for total R' + this.totalAmt;
          this.paymentOverrideForm.get('transactionDescription').setValue(desc);
        } else {
          this.transactions.push(this.createTransactionListModel());
        }
      } else {
        this.transactions.push(this.createTransactionListModel());
      }
    });
  }

  getInvoiceInfo() {
    let invoiceId = [];
    this.selectedRows.forEach(ele => {
      if(ele.invoiceId){
      invoiceId.push(ele.invoiceId)
      }
    })
    let reqQueryObj :any = {
      customerId: this.id,
      debtorId: this.debtorId,
      addressId: this.addressId
    }
    if(invoiceId.length !=0){
      reqQueryObj.invoiceIds =  invoiceId
    }
    // this.selectedInvoiceIds //this.selectedRows[0].invoiceId
    this.rxjsService.setDialogOpenProperty(true)
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      EventMgntModuleApiSuffixModels.INVOICE_INFO,
      undefined,
      false, prepareGetRequestHttpParams(null, null,reqQueryObj )
    ).subscribe(data => {
      this.rxjsService.setDialogOpenProperty(false)
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data.resources) {
        this.invoiceDetails = data.resources;
        let find = this.templateList.find(item=> item.displayName == "Statement");
        if(!this.invoiceDetails?.invoiceRefNos){
          this.isNotInvoiceType = true
          this.paymentInvoiceForm.get('templateId').setValue(find?.id)
          this.paymentInvoiceForm.get('templateId').disable()
        }else{
          this.paymentInvoiceForm.get('templateId').setValue("")
          this.paymentInvoiceForm.get('templateId').enable()

          this.isNotInvoiceType = false;
        }
        this.paymentInvoiceForm.get('customerEmail').setValue(this.invoiceDetails?.customerEmail)
        this.paymentInvoiceForm.get('debtorEmail').setValue(this.invoiceDetails?.debtorEmail)
        this.invoiceDetails.invoiceRefNo = this.selectedInvoices?.toString();
        this.invoiceDetailsArr = this.selectedInvoices;
      }
    });
  }

  selectedCount = 0;
  checkboxSelected(selectedRow, e?) {
    if (selectedRow.referenceType == 4) {
      this.manualInvoiceApprovalId = selectedRow.invoiceId
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete();
          }
        } else {
          this.onOneOrManyRowsDelete(row);
        }
        break;
      case CrudType.FILTER:
        this.filterData();
        break;
      case CrudType.EXPORT:
        break;
    }
  }

  openPromiseToPayDialog(type: CrudType | string, editableObject?: object | string) {
    const ref = this.dialogService.open(PromiseToPayDetailComponent, {
      header: 'Promise To Pay Quote',
      showHeader: false,
      baseZIndex: 1000,
      width: '54vw',
      height: '290px',
      data: {
        row: editableObject
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.getRequiredListData()
      }
    });
  }

  openTransactionNumberDialog(type: CrudType | string, editableObject?: object | string) {
    const ref = this.dialogService.open(TransactionProcessNumberComponent, {
      header: 'Transaction Number',
      showHeader: false,
      baseZIndex: 1000,
      width: '58vw',
      height: '350px',
      data: {
        row: editableObject
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.getRequiredListData()
      }
    });
  }

  filterData() {
    this.showFilter = !this.showFilter;
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("event-management/event-configuration/stack-config/add-edit");
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["event-management/event-configuration/stack-config/view"], { queryParams: { id: editableObject['stackConfigId'] } });
            break;
          case 1:
            this.openTransactionNumberDialog(type, editableObject);
            break;
          case 3:
            this.openPromiseToPayDialog(type, editableObject);
            break;
        }
    }
  }

  onTabChange(event) {
    this.tables.forEach(table => {
      table.rows = 10
    });
    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.router.navigate(['/customer/manage-customers/view-transactions'], { queryParams: { id: this.id, debtorId: this.debtorId, addressId: this.addressId, tab: this.selectedTabIndex } });
    this.selectedTabIndex = event?.index;
    if (this.selectedTabIndex == 0) {
      this.transactionType = 'transaction'
    } else if (this.selectedTabIndex == 1) {
      this.transactionType = 'pending-transaction'
    } else if (this.selectedTabIndex == 2) {
      this.transactionType = 'client-quotes'
    }
    this.selectedRows = [];
    this.getRequiredListData();
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  otp: string = '';
  otpId: string = '';
  getOtp() {
    let postObj = {
      "mobileNo": this.details.smsNotificationNo,
      "email": this.details.email,
      "employeeId": "",
      "customerId": this.id,
      "createdUserId": this.loggedInUserData.userId,
      "mobileNoCountryCode": this.details.smsNotificationNoCountryCode
    }
    this.crudService.create(ModulesBasedApiSuffix.SHARED, EventMgntModuleApiSuffixModels.GET_OTP, postObj).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.otpId = response.resources;
      }
    });
  }

  isOtpVerified = false;
  submitOtp() {
    if (!this.otp) {
      return this.snackbarService.openSnackbar("OTP is required", ResponseMessageTypes.WARNING);
    }
    let otpObj = {
      "otpId": this.otpId,
      "otp": this.otp
    }
    this.crudService.update(ModulesBasedApiSuffix.SHARED, EventMgntModuleApiSuffixModels.OTP_VERIFY, otpObj).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.isOtpVerified = true;
        this.otpVerification = false;
      }
    });
  }
  closeOtpModal() {
    this.otpVerification = false
    this.otpId = "";
    this.otp = "";
    this.isOtpVerified = false;
  }
  otpVerification = false;
  submit() {
    if (this.paymentOverrideForm.invalid) {
      return;
    }
    if (!this.isOtpVerified) {
      this.getOtp();
      this.otpVerification = true;
      if (this.otpVerification) {
        return;
      }
    }
    let obj = this.paymentOverrideForm.value;
    obj.createdUserId = this.loggedInUserData.userId;
    obj.customerId = this.id;
    obj.doRunId = parseInt(obj.doRunId);
    if (!obj.isPaymentTypeSingle) {
      obj.invoicePaymentAndOverrideIds = this.selectedIdPo?.toString();
    } else {
      obj.invoicePaymentAndOverrideIds = null;
    }
    let total = 0;
    obj.transactions.forEach((serviceListModel) => {
      total = parseFloat(serviceListModel.amount) + total;
    });
    this.totalAmt = total;
    if (this.selectedRows.length == 1) {
      if (obj.isPaymentTypeSingle) {
        if (this.totalAmtShown < this.totalAmt) {
          this.snackbarService.openSnackbar("Payment should not be exceeds than total invoice amount", ResponseMessageTypes.WARNING);
          return;
        }
      } else {
        if (this.totalAmtShown == this.totalAmt) {

        }
        else {
          this.snackbarService.openSnackbar("Payment should be equal to total invoice amount", ResponseMessageTypes.WARNING);
          return;
        }
      }
    } else {
      if (this.totalAmtShown == this.totalAmt) {

      }
      else {
        this.snackbarService.openSnackbar("Payment should be equal to total invoice amount", ResponseMessageTypes.WARNING);
        return;
      }
    }
    obj.noOfPayments = obj.noOfPayments ? obj.noOfPayments : 0;
    obj.otpId = this.otpId;
    if (obj && obj.transactions) {
      obj.transactions.forEach(element => {
        element.invoiceOverrideSplitTypeId = this.isPaymentRemove ? this.invoiceOverrideSplitTypeIdRemove : this.invoiceOverrideSplitTypeIdSplit;
        element.debitDate = (element.debitDate && element.debitDate != null) ? this.momentService.toMoment(element.debitDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
      });
    }
    this.crudService.create(ModulesBasedApiSuffix.BILLING, EventMgntModuleApiSuffixModels.PAYMENT_OVERRIDE, obj).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.paymentOverride = false;
        this.selectedRows = [];
        this.isPaymentRemove = false;
      }
    })
  }

  submitUnderRide() {
    if (this.paymentUnderrideForm.invalid) {
      return;
    }
    let obj = this.paymentUnderrideForm.value;
    obj.createdUserId = this.loggedInUserData.userId;
    obj.customerId = this.id;
    obj.debtorId = this.debtorId;
    obj.invoiceId = this.details.invoices[0].invoiceId;
    if (obj.amount > this.detailsData.outstandingBalance.totalOutstanding) {
      this.snackbarService.openSnackbar("No credit balance available for this Payment Underride", ResponseMessageTypes.WARNING);
      return;
    }
    this.crudService.create(ModulesBasedApiSuffix.BILLING, EventMgntModuleApiSuffixModels.PAYMENT_UNDERRIDE, obj).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.paymentUnderride = false;
        this.selectedRows = [];
        this.getRequiredListData();
        this.getCustomerInfo();
      }
    });
  }

  isCheckedEmail =false;
  emailCheck() {
    let isAlternateEmail = this.paymentInvoiceForm.value.isAlternateEmail;
    // if (isAlternateEmail) {
    //   this.paymentInvoiceForm.get('alternateEmail').setValidators(Validators.required)
    //   this.paymentInvoiceForm.get('alternateEmail').updateValueAndValidity();
    // } else {
    //   this.paymentInvoiceForm.get('alternateEmail').clearValidators();
    //   this.paymentInvoiceForm.get('alternateEmail').updateValueAndValidity();
    // }

    if (this.paymentInvoiceForm.value.isCustomer) {
      this.paymentInvoiceForm.get('customerEmail').setValidators(Validators.required)
      this.paymentInvoiceForm.get('customerEmail').updateValueAndValidity();
    } else {
      this.paymentInvoiceForm.get('customerEmail').clearValidators();
      this.paymentInvoiceForm.get('customerEmail').updateValueAndValidity();
    }
    if (this.paymentInvoiceForm.value.isDebtor) {
      this.paymentInvoiceForm.get('debtorEmail').setValidators(Validators.required)
      this.paymentInvoiceForm.get('debtorEmail').updateValueAndValidity();
    } else {
      this.paymentInvoiceForm.get('debtorEmail').clearValidators();
      this.paymentInvoiceForm.get('debtorEmail').updateValueAndValidity();
    }
    if (this.paymentInvoiceForm.value.isOther) {
      this.paymentInvoiceForm.get('otherEmail').setValidators(Validators.required)
      this.paymentInvoiceForm.get('otherEmail').updateValueAndValidity();
      this.paymentInvoiceForm.get('otherEmail').setValue('');
    } else {
      this.paymentInvoiceForm.get('otherEmail').clearValidators();
      this.paymentInvoiceForm.get('otherEmail').updateValueAndValidity();
      this.paymentInvoiceForm.setErrors(null)
     }

      this.inputs.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      });


    if(this.paymentInvoiceForm.value.isCustomer || this.paymentInvoiceForm.value.isDebtor  ||  this.paymentInvoiceForm.value.isOther){
      this.isCheckedEmail = true
    }else{
      this.isCheckedEmail = false

    }
  }

  submitInvoice() {
    if (this.paymentInvoiceForm.invalid) {
      return;
    }
    let invoiceIds = [];
    let obj = this.paymentInvoiceForm.getRawValue();
    obj['createdUserId'] = this.loggedInUserData.userId;
     if (this.selectedRows && this.selectedRows.length > 0) {
      this.selectedRows.forEach(ele => {
        if(ele.invoiceId){
          invoiceIds.push(ele.invoiceId)
        }
      })
      obj['invoiceId'] = invoiceIds.toString();
    }
    obj['isPDFFileFormat'] = false;
    obj['invoiceNotificationId'] = null;
    obj['isEmailRead'] = false;
    obj['emailReadDate'] = new Date();
    obj['isEmailDelivered'] = false;
    obj['isSMSSent'] = false;
    obj['isSMSDelivered'] = false;
    obj['customerId'] = this.id;
    obj['debtorId'] = this.debtorId;
    obj['addressId'] = this.addressId;
    obj['contractId'] = this.contractId;
    obj['transactionType'] = this.transactionType;
    obj['transactionRefNo'] = null;
    obj['transactionDescription'] = null;
    obj['refNo'] = null;
    obj['isOpenTransactions'] = this.openTransaction;
    obj['isAllocatedTransactions'] = null;
    obj['createdDate'] = new Date();


    Object.keys(obj).forEach(key => {
      if (obj[key] === "") {
        delete obj[key]
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.BILLING, EventMgntModuleApiSuffixModels.INVOICE_INFO, obj).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setDialogOpenProperty(false);
      if (response.isSuccess) {
        this.invoiceInfo = false;
        this.selectedRows = []
        this.getRequiredListData();
        this.getCustomerInfo();
      }
    });
  }

  reason: string = '';
  contractChange(e) {
    this.contractId = e?.target?.value;
  }

  submitRemove() {
    this.crudService
      .delete(ModulesBasedApiSuffix.BILLING,
        SalesModuleApiSuffixModels.PENDING_TRANSACTION_LIST,
        undefined,
        prepareRequiredHttpParams({
          referenceId: this.selectedRows[0].referenceId,
          referenceType: this.selectedRows[0].referenceType,
          modifiedUserId: this.loggedInUserData.userId
        }),
      )
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.removeAlert = false;
          this.selectedRows = []
          this.getRequiredListData();
          this.getCustomerInfo();
        }
      });
  }

  changePaymentType() {
    let isPaymentTypeSingle = this.paymentOverrideForm.value.isPaymentTypeSingle;
    if (isPaymentTypeSingle) {
      this.transactions.clear();
      let total = 0;
      this.details.invoices.forEach((serviceListModel) => {
        this.startDate = serviceListModel.debitDate;
        total = serviceListModel.amount + total;
        this.transactions.push(this.createTransactionListModel(serviceListModel));
      });
      this.totalAmt = total;
      this.totalAmtShown = total;
      let desc = 'Debit Order Payment-Thank you Submitted on ' + this.formatDate(this.minDate) + ' for total R' + this.totalAmt;
      this.paymentOverrideForm.get('transactionDescription').setValue(desc);
    } else {
      this.paymentOverrideForm.get('noOfPayments').setValue('2');
      this.assign();
      if (this.details.invoices.length > 0) {
        let total = 0;
        this.details.invoices.forEach((serviceListModel) => {
          this.startDate = serviceListModel.debitDate;
          total = serviceListModel.amount + total;
        });
        this.totalAmt = total;
        this.totalAmtShown = total;
        let desc = 'Debit Order Payment-Thank you Submitted on ' + this.formatDate(this.minDate) + ' for total R' + this.totalAmtShown;
        this.paymentOverrideForm.get('transactionDescription').setValue(desc);
      }
    }
  }

  assign() {
    let noOfPayments;
    noOfPayments = this.paymentOverrideForm.value.noOfPayments;
    if (noOfPayments > 3) {
      this.snackbarService.openSnackbar("DOA Approval Needed", ResponseMessageTypes.WARNING);
      return;
    }
    this.transactions.clear();
    let d = new Date(this.startDate);
    let amount = 0; let finalAmt;
    if (this.totalAmtShown) {
      amount = this.totalAmtShown / noOfPayments;
      amount = this.toTrunc(amount, 2);
      //  finalAmt = parseFloat(this.totalAmtShown.toString()) - parseFloat((amount * noOfPayments).toString());
      // finalAmt = Number((this.totalAmtShown - (amount * noOfPayments)).toFixed(2)) + (amount)
      finalAmt = this.totalAmtShown - (amount * (noOfPayments - 1));

    }
    let to = 0;
    for (var i = 0; i < noOfPayments; i++) {
      let transactionsModel = new Transactions();
      if (this.totalAmtShown)
        transactionsModel['amount'] = (i == (noOfPayments - 1)) ? Math.round(finalAmt * 100) / 100 : this.toTrunc(this.totalAmtShown / noOfPayments, 2);
      else
        transactionsModel['amount'] = 0;

      if (this.details.invoices)
        transactionsModel['invoicePaymentAndOverrideId'] = this.details.invoices[this.details.invoices.length - 1].invoicePaymentAndOverrideId;

      transactionsModel['debitDate'] = new Date(d.getFullYear(), d.getMonth() + (i), d.getDate());
      this.transactions.insert(i, this.createTransactionListModel(transactionsModel));
      to = Number(to) + Number(transactionsModel['amount']);
    }

  }
  toTrunc(value, n) {
    let x = (value.toString() + ".0").split(".");
    return parseFloat(x[0] + "." + x[1].substr(0, n));
  }
  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getRequiredListData();
        this.getCustomerInfo();
      }
    });
  }

  // start of pending transactions
  first = 0;
  reset: boolean;
  onActionSubmitted(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequest(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequest(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequest(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequest(e.type, e.data, e?.col);
    }
  }

  getPendingTransactionWOCStatus(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.isEnableWieverViewBtn = false
    this.loading = true;
    let BillingModuleApiSuffixModels: BillingModuleApiSuffixModels;
    BillingModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      BillingModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.dataList = response.resources;
        this.totalRecords = response.resources.length;
      } else {
        this.dataList = [];
        this.totalRecords = 0;
      }
      this.reset = false;
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onCRUDRequest(type: CrudType | string, row?, unknownVar?): void {
    switch (type) {
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getPendingTransactionWOCStatus(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
    }
  }

  // end of pending transactions
  onCallWrapupClick() {
    this.callWrapupPopup = !this.callWrapupPopup;
  }

  addPaymentArrangement() {

    // this.router.navigate(["customer/add-payment-arrangement"], { queryParams: { campaignId:this.transactionId, customerId: this.id,addressId:this.addressId, debtorId:this.debtorId} });
    if (!this.agentExtensionNo) {
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    }
    else {
      this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
        if (openscapeConfigDetail) {
          this.router.navigate(["customer/add-payment-arrangement"], { queryParams: { campaignId: this.transactionId, customerId: this.id, addressId: this.addressId, debtorId: this.debtorId } });
        } else {
          this.snackbarService.openSnackbar("Please intiate the call or Try again.", ResponseMessageTypes.WARNING)
        }
      });
    }
  }

  addPromiseToPay() {
    if (!this.agentExtensionNo) {
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    }
    else {
      this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
        if (openscapeConfigDetail) {
          this.router.navigate(["customer/add-promise-to-pay"], { queryParams: { campaignId: this.transactionId, customerId: this.id, addressId: this.addressId, debtorId: this.debtorId } });
        } else {
          this.snackbarService.openSnackbar("Please intiate the call or Try again.", ResponseMessageTypes.WARNING)
        }
      });
    }
  }

  addProofofpayments() {

    if (!this.agentExtensionNo) {
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    }
    else {
      this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
        if (openscapeConfigDetail) {
          this.router.navigate(["customer/add-proof-of-payments"], { queryParams: { campaignId: this.transactionId, customerId: this.id, addressId: this.addressId, debtorId: this.debtorId } });
        } else {
          this.snackbarService.openSnackbar("Please intiate the call or Try again.", ResponseMessageTypes.WARNING)
        }
      });
    }
    // this.router.navigate(["customer/add-proof-of-payments"], { queryParams: {campaignId:this.transactionId, customerId: this.id,addressId:this.addressId, debtorId:this.debtorId} });
  }

  addNoContacts() {
    this.router.navigate(["collection/call-wrap-up-codes/no-contact/add-edit"], {
      queryParams: {
        customerId: this.id, addressId: this.addressId,
        campaignId: this.transactionId, debtorId: this.debtorId,
        requestType: 'Add No Contact'
      }
    });
  }

  addRefuseToPay() {
    this.router.navigate(["/customer/add-refuse-to-pay"], {
      queryParams:
        { customerId: this.id, addressId: this.addressId, campaignId: this.transactionId, debtorId: this.debtorId }
    });
  }

  onRowSelect(event) {
    // this.selectedRows = []

    this.selectedRowInvoiceId = event?.data.referenceId

    if (this.selectedTabIndex == 1) {
      // this.openModelForPendingTransactions()
    }
    // this.selectedRows = [event?.data]
    this.selectedRowsTemp = this.selectedRows;
    if (event?.data?.transactionDescription == "Balance of Contract" ||
      event?.data?.transactionDescription == "Purchase of Alarm System") {
      // if (event?.data?.referenceTypeId == 5) {
      this.isEnableWieverViewBtn = true
    } else {
      this.isEnableWieverViewBtn = false
    }
  }
  pendingTransactionsObj: any = {}
  isLoadingTransaction = true;
  openModelForPendingTransactions() {
    this.isLoadingTransaction = true
    this.pendingTransactionPopup = true;
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_NOTES_VIEW, undefined, false, prepareRequiredHttpParams({ invoiceRefNo: this.selectedRowsTemp[0].transactionRefNo, transactionType: this.selectedRowsTemp[0].transactionType })).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.isLoadingTransaction = false;
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.pendingTransactionsObj = response.resources;
      }
    })
  }
  onRowUnselect(event) {
    if (this.selectedRows?.length == 1) {
      if (this.selectedRows[0]?.transactionDescription == "Balance of Contract" ||
        this.selectedRows[0]?.transactionDescription == "Purchase of Alarm System") {
        this.isEnableWieverViewBtn = true
      } else {
        this.isEnableWieverViewBtn = false
      }
    } else {
      this.isEnableWieverViewBtn = false
    }
  }

  navigateToPage() {
    this.rxjsService.setViewCustomerData({
      customerId: this.id,
      addressId: this.addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }
  customerNavigate() {
    this.navigateToPage();
  }
  // openBalanceOfContractPopup(){
  //   this.isOpenBalanceOfContract = true
  // }


  getPaymentSplitTypes() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PAYMENT_OVERRIDE_SPLIT_TYPES).subscribe((response) => {
      if (response.resources && response.statusCode == 200) {
        response.resources.forEach(item => {
          if (item.invoiceOverrideSplitTypeName == "Remove Payment") {
            this.invoiceOverrideSplitTypeIdRemove = item.invoiceOverrideSplitTypeId
          }
          if (item.invoiceOverrideSplitTypeName == "Payment Override") {
            this.invoiceOverrideSplitTypeIdSplit = item.invoiceOverrideSplitTypeId
          }
        })
      }
    })
  }
}
