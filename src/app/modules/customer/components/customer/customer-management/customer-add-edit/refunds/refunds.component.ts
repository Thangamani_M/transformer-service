import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { clearFormControlValidators, CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";

@Component({
  selector: 'app-refunds',
  templateUrl: './refunds.component.html',
  styleUrls: ['./refunds.component.scss']
})

export class RefundsComponent {
  primengTableConfigProperties: any;
  loggedUser: UserLogin;
  selectedTabIndex: any = 0;
  dataList: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  row: any = {}
  loading: boolean;
  selectedRows: any = [];
  firstHeading: string;
  secondHeading: string;
  thirdHeading: string;
  fourthHeading: string;
  firstSectionDetails;
  secondSectionDetails;
  transactionAndBankSection;
  thirdSectionDetails: any = [];
  formSectionDetails = [];
  optionalForms = [];
  requiredForms = [];
  requiredFormList = [];
  requiredFormListName = [];
  optionalFormList = [];
  optionalFormListName = [];
  paymentMethodList = [];
  addressId;
  ids;
  customerId;
  debtorId;
  type;
  transactionId;
  transactionType;
  transactionDescription;
  invoiceTransactionTypeId;
  enableSubmit: boolean = false;
  formData = new FormData();
  descriptionType;
  subTypeDescription;
  refundsForm: FormGroup;
  flag: boolean = false;
  transactionProcessApproveForm: FormGroup;
  transactionProcessDeclineForm: FormGroup;
  fileName;
  finalDetails;
  submitFlag: boolean = false;
  approvePopupFlag: boolean = false;
  declinePopupFlag: boolean = false;
  descriptionSubTypeId;
  descriptionId;
  bankBranchesList = [];
  bankList = [];
  titlesList = [];
  accountTypeList = [];
  paymentDetails = [];
  descriptionTypeList = [];
  refundDetails;
  docLength = 1;
  fileObject;
  fileObjArr = [];
  reasonList=[];
  optionalFileObjArr = [];
  descriptionTypeDocList = [];
  viewable: boolean = false;
  startTodayDate = new Date();
  optionalNameTemp = '';
  refundAlternateBankInfo;
  refundId;
  isAvsCheck:boolean=false;
  isDebtorBank:boolean=false;
  referenceNo;
  optionName;
  status;
  transactionDetailsDTO;
  constructor(private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute, private router: Router, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private momentService: MomentService, private crudService: CrudService, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "View Customer",
      breadCrumbItems: [],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'View Customer',
            dataKey: '',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [],
            enableMultiDeleteActionBtn: false,
            enableRowDeleteActive: true,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false
          }
        ]
      }
    }
    this.ids = this.activatedRoute.snapshot.queryParams.referenceId;
    this.status = this.activatedRoute.snapshot.queryParams.status;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.debtorId = this.activatedRoute.snapshot.queryParams.debtorId;
    this.type = this.activatedRoute.snapshot.queryParams.type;
    this.transactionType = this.activatedRoute.snapshot.queryParams.transactionType;
    this.transactionId = this.activatedRoute.snapshot.queryParams.transactionId;
    let route1 = this.type == 'doa' ? { displayName: 'DOA Dashboard', relativeRouterUrl: '/collection/my-task-list' } : { displayName: 'Customer Management', relativeRouterUrl: '/customer/manage-customers/view-transactions', queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } };
    this.primengTableConfigProperties.breadCrumbItems.push(route1);
    if(this.type != 'doa'){
      let route3  = { displayName: 'Customer List', relativeRouterUrl: '/customer/manage-customers' } ;
      this.primengTableConfigProperties.breadCrumbItems.push(route3);
      let route4  = { displayName: 'View Customer', relativeRouterUrl: '/customer/manage-customers/view-transactions' , queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } } ;
      this.primengTableConfigProperties.breadCrumbItems.push(route4);
    }
    let route2 = this.type == 'doa' ? { displayName: 'Refunds - DOA Approve', relativeRouterUrl: '' } : { displayName: 'Payment Refund', relativeRouterUrl: '' };
    this.primengTableConfigProperties.breadCrumbItems.push(route2);
  }
  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.viewable = (this.type == 'doa') ? true : false;
    this.getBankDropdown();
    this.getTitle();
    this.getPaymentMethodList();
    this.getBankAccountTypeDropdown();
    this.getReason();
    this.getRequiredDetail();
    this.firstHeading = 'CUSTOMER INFO';
    this.secondHeading = 'DEBTOR INFO';
    this.thirdHeading = 'OUTSTANDING BALANCE';
    this.fourthHeading = 'BAD DEBT WRITE OFF';
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  createForm() {
    this.refundsForm = this.formBuilder.group({});
    this.formSectionDetails.forEach((key) => {
      this.refundsForm.addControl(key.formName, new FormControl());
    });
    this.transactionAndBankSection.forEach((key) => {
      this.refundsForm.addControl(key.formName, new FormControl());
    });
    let formArray = this.formBuilder.array([]);
    this.refundsForm.addControl('documentsDTOs', formArray);
    this.setOptionalDoc();
    if (this.viewable) {
      this.refundsForm.get('descriptionType').setValue(this.descriptionId);
    }else {
      this.refundsForm.get('documentDate').setValue(new Date());
    }
    this.refundsForm = setRequiredValidator(this.refundsForm, ["transactionNote","firstName","lastName","titleId","bankId","accountTypeId","accountNo","bankBranchId","said","transactionNote","descriptionType"]);
    this.onValueChanges();
  }
  get transactionProcessFormArray(): FormArray {
    if (this.refundsForm !== undefined) {
      return (<FormArray>this.refundsForm.get('documentsDTOs'));
    }
  }
  onValueChanges() {
    this.refundsForm
      .get("isDebtorBank")
      .valueChanges.subscribe((isDebtorBank: any) => {
          this.isDebtorBank = isDebtorBank;
      });

  }
  createTransactionProcessApproveForm(): void {
    this.transactionProcessApproveForm = new FormGroup({
      'comments': new FormControl(null),
    });
  //  this.transactionProcessApproveForm = setRequiredValidator(this.transactionProcessApproveForm, ['comments']);
  }
  createTransactionProcessDeclineForm(): void {
    this.transactionProcessDeclineForm = new FormGroup({
      'declineReasonId': new FormControl(null),
      'comments': new FormControl(null),
    });
    this.transactionProcessDeclineForm = setRequiredValidator(this.transactionProcessDeclineForm, [ 'declineReasonId']);
  }
  getTitle() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_TITLES,
      undefined,
      false, null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.titlesList = data.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  bankParam: any
  onChangeBank(event) {
    let bankId = event ? event : null;
    if (bankId) {
      this.getBankBranches(bankId);
      this.refundsForm.get("bankBranchCode").setValue(null);
    }
  }
  bankBranchId: any; filterData: any;
  onChangeBankBranch(events) {
    this.bankBranchId = events;
    if (this.bankBranchesList && this.bankBranchesList.length > 0) {
      this.filterData = this.bankBranchesList.filter(x => x.id == this.bankBranchId);
      if (this.filterData && this.filterData.length > 0)
        this.refundsForm.get("bankBranchCode").setValue(this.filterData[0].bankBranchCode);
    }
  }
  getBankDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_BANK, undefined, true).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.bankList = data.resources;
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  getBankAccountTypeDropdown() {
    this.crudService.dropdown(
      ModulesBasedApiSuffix.BILLING,
      SalesModuleApiSuffixModels.UX_ACCOUNT_TYPES).subscribe(data => {
        if (data.isSuccess && data.statusCode == 200) {
          this.accountTypeList = data.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getPaymentMethodList() {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UX_PAYMENT_METHODS, null, false,
      null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.paymentMethodList = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  getBankBranches(bankId: string) {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_BANK_BRANCHES, prepareGetRequestHttpParams(null, null, {
      bankId: bankId ? bankId : ''
    })).subscribe(resp => {
      if (resp.isSuccess && resp.statusCode == 200) {
        this.bankBranchesList = resp.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });

  }
  getRequiredDetail() {
    this.rxjsService.setGlobalLoaderProperty(true);
    let api = this.type == 'doa' ? this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUNDS_DOA_DETAIL, undefined, false, prepareRequiredHttpParams({ type: this.transactionType?this.transactionType:'PR', transactionId: this.transactionId })) : this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUNDS_DETAIL, undefined, false, prepareRequiredHttpParams({ CustomerId: this.customerId, DebtorId: this.debtorId, ReferenceId: this.ids }));
    api.subscribe((resp: IApplicationResponse) => {
      if (resp.isSuccess && resp.statusCode === 200 && resp.resources) {

        this.setBadDebitWriteOffInvoiceDetailDTO(resp.resources.transactionDetailsDTO, resp.resources.refundAlternateBankInfoDTO);
        this.setBadDebitWriteOffOutstandingBalance(resp.resources.outstandingBalance);
        this.transactionDescription = resp.resources.invoiceTransactiondescriptionDTO;
        this.descriptionTypeList = resp.resources.invoiceTransactiondescriptionDTO;
        this.paymentDetails = resp.resources.refundDebtorDetailsDTOs;
        this.refundDetails = resp.resources.refundDebtorDetailSummary;
        this.referenceNo=resp.resources.transactionDetailsDTO?.referenceNo;
        this.transactionDetailsDTO = resp.resources.transactionDetailsDTO;

        if(resp.resources?.refundDebtorDetailSummary?.bankBranchCode)
           this.refundsForm.get('bankBranchCode').setValue(resp.resources.refundDebtorDetailSummary.bankBranchCode);

        if(resp.resources?.refundAlternateBankInfoDTO?.bankId)
            this.refundsForm.get('bankId').setValue(resp.resources.refundAlternateBankInfoDTO.bankId);

        if(this.transactionDetailsDTO)
          this.refundsForm.get('refundRequestDate').setValue(new Date(this.transactionDetailsDTO.refundDate));

        // this.invoiceTransactionTypeId = resp.resources.transactionDetailsDTO.invoiceTransactionTypeId;
        // this.descriptionType = resp.resources.transactionDetailsDTO?.transactionDescription;
        // this.subTypeDescription = resp.resources.transactionDetailsDTO?.subTypeDescription;
        if (resp.resources.transactionDOASupportingDocuments) {
          resp.resources.transactionDOASupportingDocuments.forEach(element => {
            if (element.isRequired)
              this.requiredForms.push({ documentName: element.documentName, documentPath: element.documentPath });
            else
              this.optionalForms.push({ documentName: element.documentName, documentPath: element.documentPath });
          });
        }
      }
      else
        this.setBadDebitWriteOffInvoiceDetailDTO(null, null);

      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }
  setBankDetail() {
    let refundAlternateBankInfo = this.refundAlternateBankInfo;

    if (refundAlternateBankInfo) {
      this.refundsForm.get('titleId').setValue(refundAlternateBankInfo.titleId);
      this.refundsForm.get('firstName').setValue(refundAlternateBankInfo.firstName);
      this.refundsForm.get('lastName').setValue(refundAlternateBankInfo.lastName);
      this.refundsForm.get('paymentMethodId').setValue(refundAlternateBankInfo.paymentMethodId);
      this.refundsForm.get('bankId').setValue(refundAlternateBankInfo.bankId);
      this.onChangeBank(refundAlternateBankInfo.bankId);
      this.getBankBranches(refundAlternateBankInfo.bankId);
      this.refundsForm.get('bankBranchId').setValue(refundAlternateBankInfo.bankBranchId);
      this.onChangeBankBranch(refundAlternateBankInfo.bankBranchId);
      this.refundsForm.get('said').setValue(refundAlternateBankInfo.said);
      this.refundsForm.get('accountNo').setValue(refundAlternateBankInfo.accountNo);
      this.refundsForm.get('accountTypeId').setValue(refundAlternateBankInfo.accountTypeId);
    }
  }
  setBadDebitWriteOffInvoiceDetailDTO(val, refundAlternateBankInfo) {
    this.refundAlternateBankInfo = refundAlternateBankInfo;
    this.firstSectionDetails = [
      { name: 'Customer ID', value: val?.customerRegNo, valueColor: '#166DFF', enableHyperLink: true },
      { name: 'Customer Name', value: val?.customerName },
      { name: 'Customer Type', value: val?.customerType },
      { name: 'Site Address', value: val?.siteAddress },
      { name: 'Contact Number', value: val?.contactNumber },
      { name: 'Email Address', value: val?.emailAddress },
      { name: 'Co Reg No', value: val?.customerCompanyRegNo },
      { name: 'Vat No', value: val?.debtorVatno }
    ];
    this.secondSectionDetails = [
      { name: 'Debtor Name', value: val?.debtorName }, { name: 'Debtor Code', value: val?.debtorCode },
      { name: 'BDI Number', value: val?.debtorBDINumber }, { name: 'Contact Number', value: val?.debtorContact },
      { name: 'Email Address', value: val?.debtorEmail }, { name: 'Co Reg No', value: val?.debtorCompanyRegNo },
      { name: 'Vat No', value: val?.debtorVatno }
    ];
    this.formSectionDetails = [
      { label: 'Document Date', formName: 'documentDate', type: "date", value: val?.documentDate ? new Date(val?.documentDate) : new Date() },
      { label: 'Transaction Type', formName: 'transactionType', type: "text", value: val?.transactionType },
      { label: 'Transaction Description', formName: 'transactionDescription', type: "text", value: val?.transactionTypeDescription },
      { label: 'Description Type', formName: 'descriptionType', type: "select", value: val?.invoiceTransactionDescriptionId }
    ];
    this.flag = true;

    this.transactionAndBankSection = [
      { label: '', formName: 'refundAlternateBankInfoId', type: "text", value: null },
      { label: '', formName: 'paymentMethodId', type: "text", value: '' },
      { label: '', formName: 'titleId', type: "text", value: '' },
      { label: '', formName: 'firstName', type: "text", value: '' },
      { label: '', formName: 'lastName', type: "text", value: '' },
      { label: '', formName: 'accountNo', type: "text", value: '' },
      { label: '', formName: 'bankId', type: "text", value: '' },
      { label: '', formName: 'bankBranchId', type: "text", value: '' },
      { label: '', formName: 'bankBranchCode', type: "text", value: '' },
      { label: '', formName: 'accountTypeId', type: "text", value: '' },
      { label: '', formName: 'said', type: "text", value: '' },
      { label: '', formName: 'said', type: "text", value: '' },
      { label: '', formName: 'filterData', type: "text", value: '' },
      { label: '', formName: 'transactionNote', type: "text", value: '' },
      { label: '', formName: 'isDebtorBank', type: "text", value: '' },
      { label: '', formName: 'paymentMethodId', type: "text", value: '' },
      { label: '', formName: 'isSingle', type: "text", value: '' },
      { label: '', formName: 'refundRequestDate', type: "text", value: '' },
      { label: '', formName: 'refundAmount', type: "text", value: '' }
    ];
    this.createForm();

    if(this.type != 'doa'){
      this.refundsForm.get('isDebtorBank').setValue(true);
    }else if(this.type == 'doa') {
      this.refundsForm.get('descriptionType').setValue(val.invoiceTransactionDescriptionId);
      this.refundsForm.get('documentDate').setValue(new Date(val.documentDate));
      this.refundsForm.get('transactionNote')?.setValue(val.transactionNotes);
    }
    this.refundsForm.get('isDebtorBank').setValue(val.isDebtorBank);
    this.isDebtorBank = val.isDebtorBank;

    if (this.refundAlternateBankInfo)
      this.setBankDetail();
  }
  setBadDebitWriteOffOutstandingBalance(outstandingBalance) {
    this.thirdSectionDetails = [
      { label: 'D180', value: outstandingBalance?.days180, colsize: "outstanding-balance-col-width" },
      { label: 'D150', value: outstandingBalance?.days150, colsize: "outstanding-balance-col-width" },
      { label: 'D120', value: outstandingBalance?.days120, colsize: "outstanding-balance-col-width" },
      { label: 'D90', value: outstandingBalance?.days90, colsize: "outstanding-balance-col-width" },
      { label: 'D60', value: outstandingBalance?.days60, colsize: "outstanding-balance-col-width" },
      { label: 'D30', value: outstandingBalance?.days30, colsize: "outstanding-balance-col-width" },
      { label: 'Current', value: outstandingBalance?.current, colsize: "outstanding-balance-col-width" },
      // { label: 'Days Outstanding', value: 'Amount', colsize: "outstanding-balance-col-width" },
      { label: 'Balance', value: outstandingBalance?.totalInvoiceOutStandingAmount, colsize: "outstanding-balance-col-width" }
    ]
  }
  obj: any;
  setDateDescriptionValue() {
    // let  documentDate = this.formSectionDetails.filter(x=>x.formName=='documentDate');
    // this.refundsForm.get('documentDate').setValue(documentDate[0].value);
    //  this.refundsForm.get('documentDate').disable();

    let descriptionType = this.formSectionDetails.filter(x => x.formName == 'descriptionType');
    let descriptionSubType = this.formSectionDetails.filter(x => x.formName == 'descriptionSubType');
    let descriptionTypeId = descriptionType[0].value;
    let descriptionSubTypeId = descriptionSubType[0].value;
    if (descriptionTypeId) {
      this.refundsForm.get('descriptionType').setValue(descriptionTypeId);
      // this.transactionProcessForm.get('descriptionSubType').setValue(descriptionSubTypeId);
      // this.transactionProcessForm.get('descriptionType').disable();
      // this.transactionProcessForm.get('descriptionSubType').disable();
      this.onChangeDescriptionType(descriptionTypeId);
      // this.onChangeDescriptionSubType(descriptionSubTypeId);
    }
  }
  getReason() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_DECLINE_REASON)
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.reasonList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onFileSelected(files: FileList, type, index): void {

    const fileObj = files.item(0);
    this.fileObject = fileObj;
    this.fileName = fileObj.name;
    if(type=='approve'){
      this.obj = fileObj;return;
    }
    if (this.fileObjArr) {
      let filename = this.fileObjArr.filter(x => x.name == fileObj.name);
      if (filename && filename.length > 0) {
        this.snackbarService.openSnackbar("File name exists already.Please upload file with other name.", ResponseMessageTypes.WARNING);
        return;
      }
    }
    this.obj = fileObj;
    if (type == 'optional-doc')
      this.optionalFileObjArr.push(fileObj);

    this.fileObjArr.push(fileObj);
    this.transactionProcessFormArray.value[index].fileName = fileObj.name;
  }
  addRequiredFormList(control, type, index) {
    if (this.fileObject) {
      if (type == 'required-doc') {
      }
      else if (type == 'optional-doc') {
        if (!this.optionalNameTemp) {
          this.snackbarService.openSnackbar("Please enter the name for optional documment.", ResponseMessageTypes.WARNING);
          return;
        }
      }
      this.transactionProcessFormArray.value[index].fileName = '';
      this.transactionProcessFormArray.value[index].documentCount.push(this.transactionProcessFormArray.value[index].documentCount.length + 1);
      this.transactionProcessFormArray.value[index].documentName.push(this.fileObject.name);
      // this.transactionProcessFormArray.value[index].CustomDocumentName.push(val);
      this.fileObject = null;
      this.optionalNameTemp = '';
    } else {
      this.snackbarService.openSnackbar("Please upload file.", ResponseMessageTypes.WARNING);
    }
    //this.emitDataWhenFileChange();
  }
  addOptionalFormList() {
    this.optionalFormList.push({
      invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
      documentPath: '',
      documentName: '',
      isRequired: false
    });
  }
  onChangeDescriptionType(id) {
    //this.descriptionTypeId = id;
    this.descriptionSubTypeId = null;
    // this.descriptionSubTypeList = [];
    /// this.transactionProcessForm.get('descriptionSubType').setValue(null);
    this.clearRecord();
    if (id) {
      if (this.transactionDescription && this.transactionDescription.length > 0) {
        let desArr = this.transactionDescription.filter(x => x.invoiceTransactionDescriptionId == id);
        this.descriptionTypeDocList = desArr[0].documents;
        this.setDescriptionTypeDoc();
        // if(desArr && desArr.length >0) {
        //   if(desArr[0].subTypes && desArr[0].subTypes.length >0)
        //      this.descriptionSubTypeList = desArr[0].subTypes;
        //   else
        //      this.descriptionSubTypeList = [];

        // this.descriptionTypeDocList= desArr[0].documents;
        //    if((this.descriptionSubTypeList && this.descriptionSubTypeList.length ==0) && this.descriptionTypeDocList.length>0){
        //       this.setDescriptionTypeDoc();
        //    }
        // }
      }

      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }
  setDescriptionTypeDoc() {
    let temp = this.transactionProcessFormArray.value;
    this.descriptionTypeDocList.forEach(element => {
      let obj = this.formBuilder.group({
        invoiceTransactidocumentCountonDescriptionSubTypeDocumentId: null,
        invoiceTransactionDescriptionDocumentId: element.invoiceTransactiondescriptiondocumentId,
        documentCount: this.formBuilder.array([]),
        documentName: this.formBuilder.array([]),
        docName: element.documentName,
        fileName: null,
        CustomDocumentName: null,
        isRequired: true
      })
      obj = setRequiredValidator(obj, ["isRequired"]);
      this.transactionProcessFormArray.push(obj);
    });
    if (temp && temp.length > 0) {
      temp.forEach(element => {
        this.transactionProcessFormArray.value.push(element);
      });
    }
    //this.emitDataWhenFileChange();
    // this.docLength = this.docLength + this.descriptionTypeDocList.length;
  }
  clearRecord() {
    this.fileObjArr = [];
    this.optionalFileObjArr.forEach(file => {
      this.fileObjArr.push(file);
    });
    if (this.transactionProcessFormArray.value && this.transactionProcessFormArray.value.length > 0) {
      let all = this.transactionProcessFormArray.value;
      let required = this.transactionProcessFormArray.value.filter(x => x.isRequired == true);
      let optional = this.transactionProcessFormArray.value.filter(x => x.isRequired == false);
      if (required && required.length > 0) {
        Object.keys(required).forEach(key => {
          delete required[key];
        });
        let finalarr = []; finalarr = required.concat(optional);
        all.forEach((element, i) => {
          this.transactionProcessFormArray.removeAt(0);
        });
        finalarr.forEach((element, i) => {
          if (element) {
            this.transactionProcessFormArray.value.push(element);
          }
        });
      } else {
        all.forEach((element, i) => {
          this.transactionProcessFormArray.removeAt(0);
        });
        optional.forEach((element, i) => {
          if (element) {
            this.transactionProcessFormArray.value.push(element);
          }
        });
      }
    }
  }
  setOptionalDoc() {
    let obj = this.formBuilder.group({
      invoiceTransactidocumentCountonDescriptionSubTypeDocumentId: null,
      invoiceTransactionDescriptionDocumentId: null,
      documentCount: this.formBuilder.array([]),
      documentName: this.formBuilder.array([]),
      docName: null,
      fileName: null,
      CustomDocumentName: this.formBuilder.array([]),
      isRequired: false
    });
    this.transactionProcessFormArray.push(obj);
  }
  removeRequiredFormList(index, sindex) {
    // this.requiredFormList.splice(index, 1);
    // this.requiredFormListName.splice(index, 1);
    // this.emitDataWhenFileChange();
    let documentName = this.transactionProcessFormArray.value[index].documentName[sindex];
    let findIndex = this.fileObjArr.findIndex(x => x.name == documentName);
    let findIndexoptional = this.optionalFileObjArr.findIndex(x => x.name == documentName);
    this.transactionProcessFormArray.value[index].documentCount.pop();
    this.transactionProcessFormArray.value[index].documentName.splice(sindex, 1);
    if (this.transactionProcessFormArray.value[index].isRequired == false)
      this.transactionProcessFormArray.value[index].CustomDocumentName.splice(sindex, 1);

    this.fileObjArr.splice(findIndex, 1);
    this.optionalFileObjArr.splice(findIndexoptional, 1);
    // this.emitDataWhenFileChange();
  }
  onChange(val, index) {
    this.optionalNameTemp = val;
    if (this.transactionProcessFormArray.value[index].isRequired == false)
      this.transactionProcessFormArray.value[index].CustomDocumentName.push(val);

    //this.emitDataWhenFileChange();
  }
  removeOptionalFormList(index) {
    this.optionalFormList.splice(index, 1);
    this.optionalFormListName.splice(index, 1);
    // this.emitDataWhenFileChange();
  }
  avsCheck() {
    if (this.refundsForm.invalid) return;

    let flag = false;let tempFlag=false;
    let documents = this.transactionProcessFormArray.value;
    flag = this.checkRequiredDoc(documents);
    let reqDoc = documents;
    if (reqDoc && reqDoc.length > 0) {
      tempFlag = flag;
    }

     if(tempFlag){
        this.snackbarService.openSnackbar('Please upload the required documents',ResponseMessageTypes.WARNING);
        return;
     }

    let formValue = this.refundsForm.value;
    console.log('refundAmount',formValue?.refundAmount);
    console.log('this.refundDetails?.amount',this.refundDetails?.amount);
    formValue.refundRequestDate = (formValue.refundRequestDate && formValue.refundRequestDate != null) ? this.momentService.toMoment(formValue.refundRequestDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    let finalObj = {
      refundsDTO: {
        refundId: "",
        refundRequestDate: formValue.documentDate,
        customerId: this.customerId,
        refundAmount: formValue?.refundAmount ?  formValue?.refundAmount : this.refundDetails?.amount,
        isSingle: true,
        isDebtorBank: formValue.isDebtorBank,
        invoiceTransactionDescriptionId: formValue.descriptionType,
        notes: formValue.transactionNote
      },
      refundTransactionsDTOs: [
        {
          refundTransactionId: "",
          refundId: "",
          referenceId: this.ids,
          referenceTypeId: 1
        }
      ],
      refundAlternateBankInfoDTO: {
        refundAlternateBankInfoId: "",
        refundId: this.refundId?this.refundId:"",
        debtorId:this.debtorId,
        titleId: formValue.titleId,
        firstName: formValue.firstName,
        lastName: formValue.lastName,
        accountNo: formValue.accountNo,
        bankId: formValue.bankId,
        bankBranchId: formValue.bankBranchId,
        accountTypeId: formValue.accountTypeId,
        paymentMethodId: formValue.paymentMethodId,
        said: formValue.said,
      },
      createdUserId: this.loggedUser.userId
    }
    let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUNDS_AVS, finalObj);
    api.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
         this.refundId = res.resources.refundId;
         if(res.resources.isAvsCheck){
          this.isDebtorBank=true;
         }
         else{
          this.isDebtorBank=false;
          this.snackbarService.openSnackbar("AVS Check Failed. Please try again.",ResponseMessageTypes.ERROR);
         }
         this.isAvsCheck=true;

        // this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } });
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
  onSubmit() {
    if (this.type == 'doa') {
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.approvePopupFlag = !this.approvePopupFlag;
      this.fileName = '';
      this.createTransactionProcessApproveForm();
    }
    else{
      if (this.refundsForm.invalid) return;
         this.submitFlag = !this.submitFlag;
    }

  }
  finalSubmit() {
    if (this.refundsForm.invalid) return;
    let flag = false;
    let documents = this.transactionProcessFormArray.value;

    flag = this.checkRequiredDoc(documents);
    if(flag){
      this.snackbarService.openSnackbar('Please upload the required documents',ResponseMessageTypes.WARNING);
      return;
    }


    //  this.emitData.emit({documents : documents ,documentsArr:this.fileObjArr, data : this.refundsForm.value,flag:flag});

    this.formData = new FormData();
     let finalDoc=[];

     if(documents && documents?.length >0){
        Object.keys(documents).forEach(key => {
          if(documents[key].documentName.length>0){
              if( documents[key].CustomDocumentName &&  documents[key].CustomDocumentName.length >0)
              documents[key].CustomDocumentName =   documents[key].CustomDocumentName.toString();

              finalDoc.push(documents[key]);
          }
      });
     }
     let formValue = this.refundsForm.value;
     console.log('formValue',formValue)
     console.log('formValue refundAmount',formValue.refundAmount)
     console.log('this.refundDetails?.amount',this.refundDetails?.amount)
     formValue.refundRequestDate = (formValue.refundRequestDate && formValue.refundRequestDate != null) ? this.momentService.toMoment(formValue.refundRequestDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
     let finalObject = {
      refundId: this.refundId?this.refundId:null,
      amount: this.refundDetails?.amount,
      createdUserId: this.loggedUser.userId,
      supportingDocumentsUploadInsertDTOs: finalDoc,
      refundsDTO: {
        refundId: "",
        refundRequestDate: formValue.documentDate,
        customerId: this.customerId,
        refundAmount: formValue?.refundAmount?formValue?.refundAmount:this.refundDetails?.amount,
        isSingle: true,
        isDebtorBank: formValue.isDebtorBank,
        invoiceTransactionDescriptionId: formValue.descriptionType,
        notes: formValue.transactionNote
      },
      refundTransactionsDTOs: [
        {
          refundTransactionId: "",
          refundId: "",
          referenceId: this.ids,
          referenceTypeId: 1
        }
      ],
      refundAlternateBankInfoDTO: {
        refundAlternateBankInfoId: "",
        refundId: this.refundId?this.refundId:"",
        debtorId:this.debtorId,
        titleId: formValue.titleId,
        firstName: formValue.firstName,
        lastName: formValue.lastName,
        accountNo: formValue.accountNo,
        bankId: formValue.bankId,
        bankBranchId: formValue.bankBranchId,
        accountTypeId: formValue.accountTypeId,
        paymentMethodId: formValue.paymentMethodId,
        said: formValue.said,
      },
    }

    if (this.fileObjArr && this.fileObjArr && this.fileObjArr.length > 0) {
      this.fileObjArr.forEach((element, index) => {
        this.formData.append("file" + index, element);
      });
    }
    this.formData.append('Obj', JSON.stringify(finalObject));

    let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.REFUNDS, this.formData);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } });
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
  approve() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.transactionProcessApproveForm.invalid) return;

    let finalOjb = {
      roleId: this.loggedUser.roleId,
      approvedUserId: this.loggedUser.userId,
      comments: this.transactionProcessApproveForm.value.comments,
      referenceNo: this.transactionId,
      declineReasonId: 0
    }
    let formData = new FormData();
    if(this.obj)
      formData.append("file", this.obj);

    formData.append('obj', JSON.stringify(finalOjb));
    let api = this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_NOTES_DOA_APPROVAL, formData);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.router.navigate(['/collection/my-task-list']);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  cancelApprove() {
    this.approvePopupFlag = !this.approvePopupFlag;
    this.transactionProcessApproveForm = clearFormControlValidators(this.transactionProcessApproveForm, ['comments']);
  }
  decline() {
    if (this.transactionProcessDeclineForm.invalid) return;

    let finalOjb = {
      roleId: this.loggedUser.roleId,
      approvedUserId: this.loggedUser.userId,
      comments: this.transactionProcessDeclineForm.value.comments,
      referenceNo: this.transactionId,
      declineReasonId: this.transactionProcessDeclineForm.value.declineReasonId,
    }
    let formData = new FormData();
    formData.append("file", this.obj);
    formData.append('obj', JSON.stringify(finalOjb));
    let api = this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_NOTES_DOA_APPROVAL, formData);
    api.subscribe((res: any) => {
      if (res?.isSuccess == true && res?.statusCode == 200) {
        this.router.navigate(['/collection/my-task-list']);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  cancelDecline() {
    this.declinePopupFlag = !this.declinePopupFlag;
    this.transactionProcessDeclineForm = clearFormControlValidators(this.transactionProcessDeclineForm, ['comments', 'declineReasonId']);
  }
  checkRequiredDoc(documents) {
    let flag = false;
    if (documents && documents.length > 0) {
      let requiredDocuments = documents.filter(x => x.isRequired == true);
    //  let optionalDocuments = documents.filter(x => x.isRequired == false);
      if ((this.descriptionTypeList.length > 0 ) && requiredDocuments.length == 0) {
        flag = true; return;
      }
      if (requiredDocuments && requiredDocuments.length > 0) {
        requiredDocuments.forEach(element => {
          if (element.documentName.length == 0)
            flag = true; return;
        });
      }
    }

    return flag;
  }
  checkRequiredDocs(documents) {
    let flag = false;
    if (documents && documents.length > 0) {
      let requiredDocuments = documents.filter(x => x.isRequired == true);
      let optionalDocuments = documents.filter(x => x.isRequired == false);
      if ((this.descriptionTypeList.length > 0) && requiredDocuments.length == 0) {
        flag = true; return;
      }
      if (requiredDocuments && requiredDocuments.length > 0) {
        requiredDocuments.forEach(element => {
          if (element.documentName.length == 0)
            flag = true; return;
        });
      }
    }
    return flag;
  }
  cancel() {
    if (this.type == 'doa') {
        this.declinePopupFlag = !this.declinePopupFlag;
        this.fileName = '';
        this.createTransactionProcessDeclineForm();
    }
    else
        this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } });
  }
  onCRUDRequested(event){

  }
  cancelSubmitPopup(){
    this.submitFlag = !this.submitFlag;
  }
}
