import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerSharedModule } from "@modules/customer/shared/customer-shared.module";
import { CorrespondenceLogComponent } from "./correspondence-log.component";

@NgModule({
  declarations: [CorrespondenceLogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CustomerSharedModule,
    FormsModule,
    MaterialModule,
    SharedModule,
  ],
  exports: [CorrespondenceLogComponent],
  entryComponents: [],
})
export class CorrespondenceLogModule { }