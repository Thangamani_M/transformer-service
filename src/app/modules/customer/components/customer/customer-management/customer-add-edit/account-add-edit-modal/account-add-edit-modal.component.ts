import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { formConfigs, generateDays, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, setRequiredValidator } from '@app/shared/utils';
import { loggedInUserData, selectStaticEagerLoadingTitlesState$ } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { selectLeadHeaderDataState$ } from '@modules/sales';
import { AccountAddModel } from '@modules/sales/models/schedule-callback.model';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-account-add-edit',
  templateUrl: './account-add-edit-modal.component.html',
  styleUrls: ['./account-add-edit-modal.component.scss'],
})
export class AccountAddEditModalComponent implements OnInit {
  accountAddForm: FormGroup;
  formConfigs = formConfigs;
  userData: UserLogin;
  banks = [];
  billingAccountTypes = [];
  selectedBranch;
  branches = [];
  leadHeaderData;
  titles = [];
  paymentMethods = [];
  bankingAccountTypes = [];
  days = [];
  uploadedFile;
  isEdit = false;

  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data,
    private httpCancelService: HttpCancelService, private crudService: CrudService, private dialog: MatDialog,
    private store: Store<AppState>, private rxjsService: RxjsService) {
    this.isEdit = this.data.isEdit;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.days = generateDays();
    this.createScheduleAddForm();
    this.combineLatestNgrxStoreData();
    this.getBankList();
    this.getAccountTypeList();
    this.getPaymentMethods();
    this.getAccountToBeReplaceList();
    this.onFormControlChanges();
    if (this.data.debtorAccountDetailId) {
      let obj = this.data.debtorBankingDetailList.find(e => e.debtorAccountDetailId == this.data.debtorAccountDetailId)
      if(obj.passportExpiryDate){
        obj.passportExpiryDate =  new Date(obj.passportExpiryDate)
      }
      this.accountAddForm.patchValue(obj);
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectLeadHeaderDataState$),
      this.store.select(selectStaticEagerLoadingTitlesState$),
    ]
    ).subscribe((response) => {
      this.leadHeaderData = response[0];
      this.titles = response[1];
    });
  }

  onBankSelected() {
    this.accountAddForm.get('bankBranchId').patchValue('');
    this.accountAddForm.get('bankBranchCode').patchValue('');
  }

  onOptionsSelected(e) {
    this.selectedBranch = this.branches.filter(element => {
      return element.id == e.target.value;
    })
    this.accountAddForm.get('bankBranchCode').setValue(this.selectedBranch[0].bankBranchCode);
  }

  getBankList() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.SALES_API_BANKS, undefined, false, prepareGetRequestHttpParams(null, null, {
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.banks = response.resources;
      }
    });
  }

  getAccountTypeList() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_ACCOUNTTYPE
      , undefined, false, prepareGetRequestHttpParams(null, null, {
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.billingAccountTypes = response.resources;
        }
      });
  }

  getAccountToBeReplaceList() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.BANKING_ACCOUNT_TYPES
      , undefined, false, prepareGetRequestHttpParams(null, null, {
        debtorId: this.data.debtorId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.bankingAccountTypes = response.resources;
        }
      });
  }

  onFormControlChanges(): void {
    this.accountAddForm.get('bankId').valueChanges.subscribe((bankId: string) => {
      this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_BANK_BRANCHES, undefined, false, prepareGetRequestHttpParams(null, null, {
        bankId
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.branches = response.resources;
        }
      });
    });
  }

  getPaymentMethods() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_DOG_TYPES, undefined, false, undefined, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.paymentMethods = response.resources.paymentMethods;
      }
    });
  }

  createScheduleAddForm(): void {
    let accountAddModel = new AccountAddModel();
    this.accountAddForm = this.formBuilder.group({});
    Object.keys(accountAddModel).forEach((key) => {
      this.accountAddForm.addControl(key, new FormControl(accountAddModel[key]));
    });
    this.accountAddForm = setRequiredValidator(this.accountAddForm, ["titleId", "firstName", "lastName", "paymentMethodId", "companyName", "bankId", "bankBranchId", "debitDay", "accountNo", "accountTypeId", "isAllSubscription"]);
    if (this.data.debtorTypeId == 1) {
      this.accountAddForm.get('companyName').setValidators([Validators.required])
      this.accountAddForm.get('companyName').updateValueAndValidity();
    } else {
      this.accountAddForm.get('companyName').clearValidators();
      this.accountAddForm.get('companyName').updateValueAndValidity();
    }
    this.accountAddForm.get('paymentMethodId').setValue(2);
  }

  emitUploadedFiles(uploadedFile) {
    if (!uploadedFile || uploadedFile?.length == 0) {
      return;
    }
    this.uploadedFile = uploadedFile;
    this.accountAddForm.get('proofOfDocument').setValue(uploadedFile['name']);
  }

  onSubmit(): void {
    if (this.accountAddForm.invalid) {
      return;
    }
    let formData = new FormData();
    let obj = this.accountAddForm.value;
    obj.debtorAdditionalInfoId = this.data.debtorAdditionalInfoId;
    obj.debtorId = this.data.debtorId;
    obj.debtorTypeId = this.data.debtorTypeId;
    obj.createdUserId = this.userData.userId;
    obj.contractId = this.data.contractId;
    obj.customerId = this.data.customerId;
    formData.append('data', JSON.stringify(obj));
    formData.append('file', this.uploadedFile);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_CUSTOMER_BANKING,
      formData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.dialog.closeAll();
      }
    });
  }

  updateBankDetails() {
    let formObj = this.accountAddForm.value;
    let obj = {
      modifiedUserId: this.userData?.userId,
      contractId: this.data?.contractId,
      debtorAdditionalInfoId: this.data?.debtorAdditionalInfoId,
      debitDay: formObj?.debitDay
    }
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.CUSTOMER_BANKING_UPDATE,
      obj)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.dialog.closeAll();
      }
    });
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
