import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { AppState } from "@app/reducers";
import { agentLoginDataSelector, CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from "@app/shared";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { BillingModuleApiSuffixModels } from "@modules/sales";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-request-extend-payment-arrangement',
  templateUrl: './request-extend-payment-arrangement.component.html'
})

export class RequestExtendPaymentArrangementComponent {
  addPaymentArrangementForm: FormGroup;
  type;
  customerId;
  debtorId;
  addressId;
  userData: UserLogin;
  agentExtensionNo;
  bankList: any = [];
  callOutcomeList: any = [];
  callWrapupList: any = [];
  paymentMethodList: any = [];
  isSelectBank: boolean = false;
  paymentOveridePopup: boolean = false;
  startTodayDate = new Date();
  popUpForm: FormGroup;
  campaignId;
  debtorDetails;
  paymentArrangementInstallment;
  paymentArrangementId;
  paDetails;
  noOfPayments = [{ id: '1', displayName: '1' }, { id: '2', displayName: '2' }, { id: '3', displayName: '3' }];
  constructor(private activatedRoute: ActivatedRoute, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.type = this.activatedRoute.snapshot.queryParams.type;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.debtorId = this.activatedRoute.snapshot.queryParams.debtorId;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.campaignId = this.activatedRoute.snapshot.queryParams.campaignId;
    this.paymentArrangementId = this.activatedRoute.snapshot.queryParams.paymentArrangementId;
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createForm();
    this.getDebatorInfoByIds();
    this.getCallOutcomeDropdown();
    this.getPaymentMethodList();
    if (this.paymentArrangementId)
      this.getRequiredDetail();

    this.rxjsService.setGlobalLoaderProperty(false);
  }
  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.store.pipe(select(agentLoginDataSelector))])
      .subscribe((response) => {
        this.agentExtensionNo = response[1];
      });
  }
  createForm(): void {
    // this.addPaymentArrangementForm = this.formBuilder.group({
    //   paymentArrangementInstallment: this.formBuilder.array([])
    // }); 

    this.addPaymentArrangementForm = this.formBuilder.group({
      'outstandingBalance': new FormControl(null),
      'noOfPayments': new FormControl(null),
      'debitDescription': new FormControl(null),
      'debatorName': new FormControl(null),
      'debatorCode': new FormControl(null),
      'bdiNumber': new FormControl(null),
      'transactionNotes': new FormControl(null),
      paymentArrangementInstallment: this.formBuilder.array([])
    });
    // this.addPaymentArrangementForm = setRequiredValidator(this.addPaymentArrangementForm, ['callOutcomeId',"paymentDate",'callWrapupId']);
    //  if(this.type=='list')
    // this.addPaymentArrangementForm = setRequiredValidator(this.addPaymentArrangementForm, ['statusId']);

    // this.valueChanges();
  }
  valueChanges() {

  }
  createPopupForm() {
    this.popUpForm = this.formBuilder.group({
      'debitDescription': new FormControl(null),
      'paymentDate': new FormControl(null),
    });
  }
  get getItemsArray(): FormArray {
    if (this.addPaymentArrangementForm !== undefined) {
      return (<FormArray>this.addPaymentArrangementForm.get('paymentArrangementInstallment'));
    }
  }
  getRequiredDetail() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.PAYMENT_ARRANGEMENTS_DETAILS, null, false,
      prepareRequiredHttpParams({ paymentArrangementId: this.paymentArrangementId })
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.paDetails = data.resources;
        this.setValue(data.resources);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  setValue(data) {
    this.onChangeCallOutcome(data.callOutcomeId);
    this.addPaymentArrangementForm.get('callOutcomeId').setValue(data.callOutcomeId);
    this.addPaymentArrangementForm.get('callWrapupId').setValue(data.callWrapupId);
    this.addPaymentArrangementForm.get('outstandingBalance').setValue(data.outstandingBalance);
    this.addPaymentArrangementForm.get('noOfPayments').setValue(data.noOfPayments);
    this.addPaymentArrangementForm.get('debitDescription').setValue(data.debitDescription);


    if (data.paymentInstallments) {
      data.paymentInstallments.forEach(element => {
        let item = this.formBuilder.group({
          paymentArrangementInstallmentId: element.paymentArrangementInstallmentId,
          paymentArrangementId: element.paymentArrangementId,
          paymentDate: new Date(element.paymentDate),
          paymentAmount: element.paymentAmount,
          paymentMethodId: element.paymentMethodId,
          createdUserId: this.userData.userId
        })
        // item = setRequiredValidator(item, ["quantity","stockCode"]);
        this.getItemsArray.push(item);
      });
    }
  }
  getPaymentMethodList() {
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.UX_PAYMENT_METHODS, null, false,
      null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.paymentMethodList = data.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  onChangeCallOutcome(value) {
    //ux/Outcome-CallWrapup?CallOutcomeId=1
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_CALL_OUTCOME_CALL_WRAPUP, null, null, prepareRequiredHttpParams({ CallOutcomeId: value }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.callWrapupList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getCallOutcomeDropdown() {
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.UX_CALL_OUTCOME)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.callOutcomeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onChangeNoOfPayments(value) {
    for (let i = 0; i < this.getItemsArray.value.length; i++) {
      this.getItemsArray.removeAt(i);
    }
    for (let i = 0; i < parseInt(value); i++) {
      let item = this.formBuilder.group({
        paymentArrangementInstallmentId: null,
        paymentArrangementId: null,
        paymentDate: null,
        paymentAmount: null,
        paymentMethodId: null,
        createdUserId: this.userData.userId
      })
      // item = setRequiredValidator(item, ["quantity","stockCode"]);
      this.getItemsArray.push(item);
    }

    // });
  }
  getDebatorInfoByIds() {
    if (this.debtorId) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.PAYMENT_ARRANGEMENTS_DEBTOR_DETAILS,
        null, false, prepareRequiredHttpParams({ DebtorId: this.debtorId, customerId: this.customerId, addressId: this.addressId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources) {
            this.debtorDetails = response.resources;
            this.addPaymentArrangementForm.get('outstandingBalance').setValue(response.resources.outStandingBalance);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }
  bankSms() {
    this.isSelectBank = true;
  }
  paymentOveride() {
    this.paymentOveridePopup = true;
    this.isSelectBank = false;
    this.addPaymentArrangementForm?.get('bankId').setValue(null);
    let formValue = this.addPaymentArrangementForm.value;
    this.paymentArrangementInstallment = formValue.paymentArrangementInstallment;
  }
  apply() {
    this.addPaymentArrangementForm.get('debitDescription').setValue(this.popUpForm.get('debitDescription').value);
    this.paymentOveridePopup = !this.paymentOveridePopup;
  }
  navigate() {

  }
  onSubmit() {
    if (this.addPaymentArrangementForm.invalid) return;
    let formValue = this.addPaymentArrangementForm.value;
    let finalOjb = {
      callWrapupDurationTime: "string",
      uniqueCallId: "string",
      paymentArrangementInstallment: formValue.paymentArrangementInstallment,
      paymentArrangementId: null,
      campaignId: this.campaignId,
      customerId: this.customerId,
      addressId: this.addressId,
      debtorId: this.debtorId,
      outstandingBalance: formValue.outstandingBalance,
      noOfPayments: formValue.noOfPayments,
      createdUserId: this.userData.userId
    }
  }
  requestExtendedPaymentArrangement() {

  }
  cancel() {

  }
}