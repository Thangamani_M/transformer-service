import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { BadDebitRecoveryComponent } from "./bad-debit-recovery/bad-debit-recovery.component";
import { BadDebitReversalComponent } from "./bad-debit-reversal/bad-debit-reversal.component";
import { BadDebitWriteOffComponent } from "./bad-debit-writeoff/bad-debit-writeoff.component";
import { BalanceOfContractWaiverFeeDetailsComponent } from './balance-of-contract-waiver-fee-details/balance-of-contract-waiver-fee-details.component';
import { CreditAndReinvoiceComponent } from "./credit-and-reinvoice/credit-and-reinvoice.component";
import { CustomerViewTransactionComponent } from "./customer-view-transactions.component";
import { OffsettingComponent } from "./offsetting/offsetting.component";
import { PromiseToPayDetailComponent } from "./promise-to-pay-quote-detail/promise-to-pay-quote-detail.component";
import { RefundsComponent } from "./refunds/refunds.component";
import { RequestReinstatementComponent } from "./request-reinstatement/request-reinstatement.component";
import { TransactionProcessCommonComponent } from "./shared/transaction-process-common/transaction-process-common.component";
import { TransactionProcessNumberComponent } from "./transaction-number/transaction-number.component";
import { TransactionProcessApprovalComponent } from "./transaction-process/transaction-process-approval-request.component";
import { TransactionProcessComponent } from "./transaction-process/transaction-process.component";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { ViewAllTransactionsComponent } from "./view-all-transactions/view-all-transactions.component";
import {AccordionModule} from 'primeng/accordion';



@NgModule({
  declarations: [CustomerViewTransactionComponent,TransactionProcessComponent,RefundsComponent ,
    PromiseToPayDetailComponent,TransactionProcessApprovalComponent ,TransactionProcessNumberComponent,
    BadDebitWriteOffComponent,TransactionProcessCommonComponent,BadDebitRecoveryComponent,BadDebitReversalComponent,
     BalanceOfContractWaiverFeeDetailsComponent,CreditAndReinvoiceComponent,OffsettingComponent,RequestReinstatementComponent,ViewAllTransactionsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    AccordionModule,
    RouterModule.forChild([
      {
        path: '', component: CustomerViewTransactionComponent,canActivate:[AuthGuard], data: { title: 'View Transactions' },
      },
      {
        path: 'transaction-process', component: TransactionProcessComponent, canActivate:[AuthGuard],data: { title: 'Transaction Process' }
      },
      {
        path: 'bad-debit-writeOff', component: BadDebitWriteOffComponent,canActivate:[AuthGuard], data: { title: 'Bad Debt Write Off' }
      },
      {
        path: 'bad-debit-recovery', component: BadDebitRecoveryComponent, canActivate:[AuthGuard],data: { title: 'Bad Debt Recovery' }
      },
      {
        path: 'bad-debit-reversal', component: BadDebitReversalComponent,canActivate:[AuthGuard], data: { title: 'Bad Debt Reversal' }
      },
      {
        path: 'credit-and-reinvoice', component: CreditAndReinvoiceComponent, canActivate:[AuthGuard],data: { title: 'Credit And Re-Invoice' }
      },
      {
        path: 'offset', component: OffsettingComponent, canActivate:[AuthGuard],data: { title: 'Offset' }
      },
      {
        path: 'refunds', component: RefundsComponent, canActivate:[AuthGuard],data: { title: 'Refunds' }
      },
      {
        path: 'request-reinstatement', component: RequestReinstatementComponent,canActivate:[AuthGuard], data: { title: 'Request Re-instatement' }
      },
      {
        path: 'all-transactions', component: ViewAllTransactionsComponent,canActivate:[AuthGuard], data: { title: 'View All Transactions' }
      }
    ])
  ],
  exports: [TransactionProcessCommonComponent],
  entryComponents: [PromiseToPayDetailComponent,TransactionProcessApprovalComponent,TransactionProcessNumberComponent,TransactionProcessCommonComponent],
})
export class CustomerViewTransactionModule { }
