import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { CollectionModuleApiSuffixModels } from "@modules/collection";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";

@Component({
    selector: 'app-transaction-process-approval-request',
    templateUrl: './transaction-process-approval-request.component.html',
    styleUrls: ['./transaction-process.component.scss']
})

export class TransactionProcessApprovalComponent {
    finalObj;
    screen;
    isSuccess :boolean=false;
    actionFlag;
    fileName;
    reasonList=[];
    transactionProcessApproveForm:FormGroup;
    transactionProcessDeclineForm:FormGroup;
    loggedUser;
    documentPath;
    documentName;
    formData = new FormData();

    constructor(private crudService: CrudService,private momentService: MomentService,
        public config: DynamicDialogConfig, private store: Store<AppState>,private router:Router,
        public ref: DynamicDialogRef, private rxjsService: RxjsService,) {
            this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
                if (!userData) return;
                this.loggedUser = userData;
            });
    }
    ngOnInit(): void {
        this.screen  = this.config.data.screen;
        this.actionFlag = this.config.data.actionFlag;
        this.reasonList = this.config.data.reasonList;
        this.createTransactionProcessApproveForm();
        this.createTransactionProcessDeclineForm();
    }
    createTransactionProcessApproveForm(): void {
        this.transactionProcessApproveForm = new FormGroup({      
          'comments': new FormControl(null),
        });   

       //   this.transactionProcessApproveForm = setRequiredValidator(this.transactionProcessApproveForm, ['comments']);
    }
    createTransactionProcessDeclineForm(): void {
        this.transactionProcessDeclineForm = new FormGroup({      
          'declineReasonId': new FormControl(null),
          'comments': new FormControl(null),
        });   
        if(this.actionFlag=='decline')
        this.transactionProcessDeclineForm = setRequiredValidator(this.transactionProcessDeclineForm, ['declineReasonId']);
    }
    obj: any;
    onFileSelected(files: FileList,type): void {      
      const fileObj = files.item(0);
      this.fileName = fileObj.name;  
      this.obj = fileObj;
      this.getFilePath(fileObj);
    }
    getFilePath(fileObj){   
          this.uploadDoc(fileObj).then((data)=>{
            this.documentPath = data;
          });
          this.documentName = this.fileName;    
    }
    uploadDoc(fileObj){
        return new Promise((resolve, reject) => {
          this.formData.append("file", fileObj); 
          let type =  this.config.data.creditType == 'credit-notes' ? CollectionModuleApiSuffixModels.CREDIT_NOTES_UPLOAD : CollectionModuleApiSuffixModels.DEBIT_NOTES_UPLOAD;
          let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, type, this.formData);
           api.subscribe((res: any) => {
            if (res?.isSuccess == true && res?.statusCode == 200) {
              resolve(res.resources);
            }
            else 
              reject(null);
            
            this.rxjsService.setDialogOpenProperty(false);
          })
        });
  
      }
    onSubmit() {
        this.finalObj = this.config.data.finalObj ?this.config.data.finalObj : null ;
        let api=null;
        if(this.screen=='doa'){
         this.finalObj =  {
                approvedDate:this.momentService.toMoment(new Date()).format('YYYY-MM-DDThh:mm:ss[Z]'),
                approvedUserId:this.loggedUser.userId,
                documentPath:this.documentPath,
                documentName: this.documentName,
                referenceNo: this.config.data.id,
                roleId: this.loggedUser.roleId
         }
            if(this.actionFlag=='approve'){
              let formValue  = this.transactionProcessApproveForm.getRawValue();  
              this.finalObj.comments = formValue.comments ? formValue.comments : null;
                if (this.transactionProcessApproveForm.invalid) {
                    this.transactionProcessApproveForm.markAllAsTouched();
                    return;
                }   
                api = this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.DOA_UPDATE, this.finalObj);                 
            }
            else if(this.actionFlag=='decline'){
              let formValue  = this.transactionProcessDeclineForm.getRawValue();  
              this.finalObj.comments = formValue.comments ? formValue.comments : null;
                if (this.transactionProcessDeclineForm.invalid) {
                    this.transactionProcessDeclineForm.markAllAsTouched();
                    return;
                }   
                this.finalObj['declineReasonId'] = formValue.declineReasonId;  
                api = this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.DOA_UPDATE, this.finalObj);
             }      

        }              
        else if(this.screen=='admin'){
            let type = (this.config.data.creditType == 'credit-notes') ?CollectionModuleApiSuffixModels.CREDIT_NOTES:CollectionModuleApiSuffixModels.DEBIT_NOTES;
            if(this.config.data.creditType == 'credit-notes'){
              this.finalObj.creditNoteDocumentDate =  this.finalObj.creditNoteDocumentDate;
             }
             else{
              this.finalObj.debitNoteDocumentDate =  this.finalObj.creditNoteDocumentDate;
                 this.finalObj.debitNoteSupportingDocuments =  this.finalObj.creditNoteSupportingDocuments;
             }
               
        
            api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, type, this.finalObj);
        }
        api.subscribe((res: any) => {
         if (res?.isSuccess == true && res?.statusCode == 200) {
            this.isSuccess = true;
            if(this.screen=='doa'){
              this.rxjsService.setDialogOpenProperty(false);
              this.router.navigate(['/collection/my-task-list']);
            }          
            else{
              this.rxjsService.setDialogOpenProperty(false);
              this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id:this.config.data.ids,debtorId:this.config.data.debtorId,addressId:this.config.data.addressId } });
            }
              
         }
         this.ref.close(false);
         this.rxjsService.setDialogOpenProperty(false);
       })
    }
    btnCloseClick() {
        this.ref.close(false);
    }
}