import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { BalanceOfContractComponent, BalanceOfContractDocumentCaptureComponent } from "@modules/customer/components/customer/customer-management/balance-of-contract";

const routes = [
  {
    path: '', component: BalanceOfContractComponent, data: { title: 'Balance Of Contract' }, canActivate: [AuthGuard]
  },
  {
    path: 'capture-documents', component: BalanceOfContractDocumentCaptureComponent, data: { title: 'Balance Of Contract Upload documents' }, canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [BalanceOfContractComponent,BalanceOfContractDocumentCaptureComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild(routes)
  ],
  exports: [],
  entryComponents: [],
})
export class BalanceOfContractModule { }
