import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CustomDirectiveConfig, formConfigs,
  IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix,
  prepareRequiredHttpParams,
  ResponseMessageTypes,
  SnackbarService
} from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import {
  loggedInUserData
} from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
@Component({
  selector: 'app-balance-of-contract',
  templateUrl: './balance-of-contract.component.html',
  styleUrls: ['./balance-of-contract.component.scss']
})

export class BalanceOfContractComponent {
  formConfigs = formConfigs;
  loggedInUserData: LoggedInUserModel;
  numberWithDecimalConfig = new CustomDirectiveConfig({ isADecimalOnly: true });
  balanceOfContractAmountDetails = [];
  balanceOfContractDetail: any = {
    contractStartDate: '', contractEndDate: '',
    cancellationDate: null, lostRadioFee: '0.00', ownershipType: 'Rented', contractPeriod: '', remainingMonths: ''
  };
  balanceOfContractDetailCopy = this.balanceOfContractDetail;
  queryParams = {
    customerId: '', serviceId: '', contractId: '', addressId: ''
  }
  balanceOfContractObj;

  constructor(private crudService: CrudService, private route: ActivatedRoute,
    public rxjsService: RxjsService, private store: Store<AppState>,
    private router: Router, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute) {
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData), this.activatedRoute.queryParams]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.queryParams.customerId = response[1]['customerId'];
      this.queryParams.contractId = response[1]['contractId'];
      this.queryParams.serviceId = response[1]['serviceId'];
      this.queryParams.addressId = response[1]['addressId'];
      this.getForkJoinRequests();
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BALANCE_OF_CONTRACT_CONFIG),
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.BALANCE_OF_CONTRACT_DETAIL, null, false,
        prepareRequiredHttpParams({
          contractId: this.queryParams.contractId,
          serviceId: this.queryParams.serviceId
        })),
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.BILL_OF_SERVICE_STOCK_DETAILS, null, null, prepareRequiredHttpParams({
          customerId: this.queryParams.customerId, siteAddressId: this.queryParams.addressId, serviceId: this.queryParams.serviceId,
          contractDbId: this.queryParams.contractId
        }))
    ])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200) {
            switch (ix) {
              case 0:
                this.balanceOfContractAmountDetails = respObj.resources;
                break;
              case 1:
                if(respObj.resources.lostRadioFee==0){
                  respObj.resources.lostRadioFee='0.00';
                }
                this.balanceOfContractDetail = respObj.resources;
                this.balanceOfContractDetail.totalChargesCopy = respObj.resources?.totalCharges;
                this.balanceOfContractDetail.contractStartDate = new Date(respObj.resources.contractStartDate);
                this.balanceOfContractDetail.contractEndDate = new Date(respObj.resources.contractEndDate);
                if (respObj.resources.cancellationDate) {
                  this.balanceOfContractDetail.cancellationDate = new Date(respObj.resources.cancellationDate);
                }
                else {
                  this.snackbarService.openSnackbar('Terminate the Service first in order to proceed further..!!', ResponseMessageTypes.WARNING);
                }
                this.balanceOfContractDetailCopy = JSON.parse(JSON.stringify(this.balanceOfContractDetail));
                break;
              case 2:
                this.balanceOfContractObj = {
                  isBalanceOfContractApproved: respObj.resources.isBalanceOfContractApproved,
                  isBalanceOfContractCreated: respObj.resources.isBalanceOfContractCreated,
                  ownershipType: respObj.resources.ownershipType
                }
                break;
            }
          }
          setTimeout(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          });
        });
      });
  }

  onChangeLostToRadioValue() {
    setTimeout(() => {
      let total = this.balanceOfContractDetail.remainingContractFee + this.balanceOfContractDetail.outstandingBalance +
        this.balanceOfContractDetail.calendarMonthNotice + this.balanceOfContractDetail.poaCancellationFee;
      if (this.balanceOfContractDetail.lostRadioFee !== '')
        this.balanceOfContractDetail.totalCharges = parseFloat(this.balanceOfContractDetail.lostRadioFee) + total;
      else {
        this.balanceOfContractDetail.totalCharges = parseFloat(this.balanceOfContractDetail.totalChargesCopy);
      }
    });
  }

  onCoolingPeriodChanges() {
    // Put a settimeout to retrieve the proper method trigger after the data is bound using ngModel
    setTimeout(() => {
      if (this.balanceOfContractDetail.isCoolingOffPeriod == true) {
        this.balanceOfContractDetail.currentMonthlyFee=0;
        this.balanceOfContractDetail.lostRadioFee='0.00';
        this.balanceOfContractDetail.poaCancellationFee=0;
        this.balanceOfContractDetail.remainingContractFee=0;
        this.balanceOfContractDetail.outstandingBalance=0;
        this.balanceOfContractDetail.calendarMonthNotice=0;
        this.balanceOfContractDetail.totalCharges=0;
      }
      else {
        this.balanceOfContractDetail.currentMonthlyFee=this.balanceOfContractDetailCopy.currentMonthlyFee;
        this.balanceOfContractDetail.lostRadioFee=this.balanceOfContractDetailCopy.lostRadioFee;
        this.balanceOfContractDetail.poaCancellationFee=this.balanceOfContractDetailCopy.poaCancellationFee;
        this.balanceOfContractDetail.remainingContractFee=this.balanceOfContractDetailCopy.remainingContractFee;
        this.balanceOfContractDetail.outstandingBalance=this.balanceOfContractDetailCopy.outstandingBalance;
        this.balanceOfContractDetail.calendarMonthNotice=this.balanceOfContractDetailCopy.calendarMonthNotice;
        this.balanceOfContractDetail.totalCharges=this.balanceOfContractDetailCopy.totalCharges;
      }
    }, 100);
  }

  onSubmit(type: string): void {
    let payload: any = {};
    payload.customerId = this.queryParams.customerId;
    payload.contractId = this.queryParams.contractId;
    payload.serviceId = this.queryParams.serviceId;
    payload.isGenerateInvoice = type == 'generate invoice' ? true : false;
    payload.isPurchaseOfAlarm = false;
    payload.isCoolingOffPeriod = this.balanceOfContractDetail.isCoolingOffPeriod;
    let balanceOfContractCharges = [];
    this.balanceOfContractAmountDetails.forEach((balanceOfContractAmountDetail) => {
      let amount = balanceOfContractAmountDetail.balanceOfContractChargesConfigName.includes('Current contract amount') ? this.balanceOfContractDetail.currentMonthlyFee :
        balanceOfContractAmountDetail.balanceOfContractChargesConfigName.includes('Purchase of Alarm System cancellation fee due') ? this.balanceOfContractDetail.poaCancellationFee :
          balanceOfContractAmountDetail.balanceOfContractChargesConfigName.includes('Remaining contract balance') ? this.balanceOfContractDetail.remainingContractFee :
            balanceOfContractAmountDetail.balanceOfContractChargesConfigName.includes('Outstanding balances') ? this.balanceOfContractDetail.outstandingBalance :
              balanceOfContractAmountDetail.balanceOfContractChargesConfigName.includes('1 x Calendar month notice') ? this.balanceOfContractDetail.calendarMonthNotice :
                0;
      balanceOfContractCharges.push({
        balanceOfContractChargesConfigId: balanceOfContractAmountDetail.balanceOfContractChargesConfigId,
        amount
      });
    });
    payload.balanceOfContractCharges = balanceOfContractCharges;
    let filteredLostRadioObj = payload.balanceOfContractCharges.find(boC => boC['balanceOfContractChargesConfigId'] == 3);
    if (filteredLostRadioObj) {
      filteredLostRadioObj.amount = this.balanceOfContractDetail.lostRadioFee;
    }
    this.rxjsService.setAnyPropertyValue(payload);
    this.router.navigate(['/customer/manage-customers/balance-of-contract/capture-documents'], { queryParams: {...this.activatedRoute?.snapshot?.queryParams}, skipLocationChange: true });
  }

  navigateToViewCustomer() {
      this.rxjsService.setViewCustomerData({
        customerId: this.queryParams?.customerId,
        addressId: this.queryParams?.addressId || this.route?.snapshot?.queryParams?.addressId,
        customerTab: 6,
        monitoringTab: null,
      })
      this.rxjsService.navigateToViewCustomerPage();
  }
}