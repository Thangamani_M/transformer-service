import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerSharedModule } from "@modules/customer/shared/customer-shared.module";
import { TieredMenuModule } from "primeng/tieredmenu";
import { CustomerCallHistoryComponent } from "./customer-call-history.component";

@NgModule({
  declarations: [CustomerCallHistoryComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    TieredMenuModule,
    CustomerSharedModule
  ],
  exports: [CustomerCallHistoryComponent],
  entryComponents: [],
})
export class CustomerCallHistoryModule { }