
import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatMenuItem } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, CustomDirectiveConfig, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { SharedCustomerAddNotesComponent, SharedCustomerLinkCustomerComponent, SharedCustomerViewCallVerificationComponent, SharedCustomerViewNotesComponent } from '@modules/customer/shared/components';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, forkJoin, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

enum ACTION_TYPE {
  LISTEN = "listen",
  ADD_NOTES = 'add',
  VIEW_NOTES = 'view',
  LINK_CUSTOMER = 'link',
  CALL_VERIFICATION = 'verification'
}

@Component({
  selector: 'app-customer-call-history-component',
  templateUrl: './customer-call-history.component.html',
  // styleUrls: ['./customer-call-history.component.scss']
})
export class CustomerCallHistoryComponent implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  @Input() permission
  agentList = [];
  observableResponse;
  selectedTabIndex: any = 0;
  componentProperties = new ComponentProperties();
  primengTableConfigProperties: any;
  dataList: any
  loading: boolean;
  public bradCrum: MatMenuItem[];
  status: any = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  totalRecords: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  searchKeyword: FormControl;
  searchForm: FormGroup
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  today: any = new Date()
  searchColumns: any
  callHistoryForm: FormGroup;
  addCallNoteForm: FormGroup;
  row: any = {}
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  params: any;
  scrollEnabled: boolean = false;
  isSearched = false;
  openNotesDailog = false;
  callReasonList = [];
  wrpupList = [];
  callLogId: any;
  // callLogId = '21AE91C2-348C-4402-BA91-40378493910A'
  notesList = [];
  menuItems = [];
  dropdownsAndData = []
  callId = ""
  isRequestInProgress = false
  customerId = "";
  pageSize: number = 10;

  constructor(private crudService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService, private httpCancelService: HttpCancelService,
    private rxjsService: RxjsService, private _fb: FormBuilder) {

    this.primengTableConfigProperties = {
      tableCaption: "",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: '',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: true,
            cursorSecondLinkIndex: 5,
            columns: [{ field: 'callId', header: 'Call ID', width: '150px' },
            { field: 'callType', header: 'Type', width: '150px' },
            { field: 'callDateTime', header: 'Call Date & Time', width: '150px' },
            { field: 'agent', header: 'Agent', width: '150px' },
            // { field: 'customerName', header: 'Customer Name', width: '150px' },
            // { field: 'customerRefNo', header: 'Customer ID', width: '150px' },
            { field: 'phoneNumber', header: 'Phone Number', width: '150px' },
            { field: 'outcome', header: 'Outcome', width: '150px' },
            { field: 'callReason', header: 'Call reason', width: '150px' },
            { field: 'action', header: 'Action', width: '200px' },
            ],
            apiSuffixModel: SalesModuleApiSuffixModels.MY_CALLS,
            moduleName: ModulesBasedApiSuffix.SALES,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false
          },
        ]

      }
    }
    this.customerId = this.activatedRoute.snapshot.paramMap.get('id')
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});

    // this.activatedRoute.queryParamMap.subscribe((params) => {
    //   this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
    //   this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    //   this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    // });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest()
    this.combineLatestNgrxStoreData();
    this.getMyCalls();
    // this.createNoteForm();

    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UX_WRAPUP_REASONS),
      this.crudService.get(ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UX_CALL_REASONS)

    ]
    this.loadDropdownData()
    this.menuItems = [
      {
        label: 'Listen to Recording', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.LISTEN, this.menuItems[0].data, this.menuItems[0].label);
        }
      },
      {
        label: 'Add Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.ADD_NOTES, this.menuItems[1].data, this.menuItems[1].label);
        }
      },
      {
        label: 'View Notes', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.VIEW_NOTES, this.menuItems[2].data, this.menuItems[2].label);
        }
      },
      {
        label: 'Link Customer', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.LINK_CUSTOMER, this.menuItems[3].data, this.menuItems[3].label);
        }
      },
      {
        label: 'View Call Verification', data: {}, command: (event) => {
          this.doAction(ACTION_TYPE.CALL_VERIFICATION, this.menuItems[4].data, this.menuItems[4].label);
        }
      }
    ]
  }

  loadDropdownData() {
    forkJoin(this.dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.wrpupList = resp.resources;
              break;
            case 1:
              this.callReasonList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  toggleMenu(menu, event, rowData) {
    this.menuItems.forEach((menuItem) => {
      menuItem.data = rowData;
    });
    menu.toggle(event);
  }

  doAction(type, data, actionType?) {
    let isAccessDeined = this.getPermissionByActionType(actionType);
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    this.callId = data.callId;
    this.callLogId = data.callLogId;
    switch (type) {
      case ACTION_TYPE.LISTEN:
        if (data.recordingURL && data.recordingURL != "Recording not found") {
          window.open(data.recordingURL);
        } else {
          this.snackbarService.openSnackbar("Recording not found", ResponseMessageTypes.WARNING)
        }
        break;
      case ACTION_TYPE.ADD_NOTES:
        this.openAddNotes(data);
        break;
      case ACTION_TYPE.VIEW_NOTES:
        this.viewNotes(data.callLogId);
        break;
      case ACTION_TYPE.LINK_CUSTOMER:
        this.openLinkCustomer(data)
        break;
      case ACTION_TYPE.CALL_VERIFICATION:
        this.viewVerifications(data.callLogId)
        break;
      default:
        break;
    }
  }

  getPermissionByActionType(actionTypeMenuName): boolean {
    let foundObj = this.permission['subMenu'].find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }

  openAddNotes(data) {
    const ref = this.dialogService.open(SharedCustomerAddNotesComponent, {
      header: `Add Notes - ${this.callId}`,
      showHeader: true,
      baseZIndex: 10000,
      width: "600px",
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.getMyCalls();
      }
    });
  }
  openViewNotes(data) {
    const ref = this.dialogService.open(SharedCustomerViewNotesComponent, {
      header: `View Notes - ${this.callId}`,
      showHeader: true,
      baseZIndex: 10000,
      width: "600px",
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {

      }
    });
  }
  openLinkCustomer(data) {
    const ref = this.dialogService.open(SharedCustomerLinkCustomerComponent, {
      header: `Link Call to Customer - ${this.callId}`,
      showHeader: true,
      baseZIndex: 200,
      width: "470px",
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.getMyCalls();
      }
    });
  }
  viewNotes(callLogId) {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.CALL_WRAPUP_REASON, callLogId).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        let viewNotesObject = response.resources;
        if (viewNotesObject.callReasonId) {
          let findCallReason = this.callReasonList.find(item => item.id == viewNotesObject.callReasonId)
          let findWrapupReason = this.wrpupList.find(item => item.id == viewNotesObject.wrapupReasonId)
          viewNotesObject.callReasonName = findCallReason.displayName;
          viewNotesObject.wrapupReasonName = findWrapupReason.displayName;
          this.openViewNotes(viewNotesObject)
        } else {
          this.snackbarService.openSnackbar("No notes added", ResponseMessageTypes.WARNING)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }
  openCallVerification(data) {
    const ref = this.dialogService.open(SharedCustomerViewCallVerificationComponent, {
      header: `View Call Verification - ${this.callId}`,
      showHeader: true,
      baseZIndex: 10000,
      width: "400px",
      data: data,
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {

      }
    });
  }

  viewVerifications(callLogId) {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, SalesModuleApiSuffixModels.CUSTOMER_CALL_VERIFICATION, null, false, prepareRequiredHttpParams({ callLogId: callLogId })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        let viewNotesObject = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (viewNotesObject.length != 0) {
          this.openCallVerification(viewNotesObject)
        } else {
          this.snackbarService.openSnackbar("Customer Call not Verified!.", ResponseMessageTypes.WARNING)
        }

      }
    })
  }

  ngAfterViewInit() {

  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    // [breadCrumbItem['queryParams']['key']]: breadCrumbItem['queryParams']['value']
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  // Create Search Final Signal Form



  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }


  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getMyCalls(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    const params = {
      customerId: this.customerId,
    }
    otherParams = { ...otherParams, ...params };
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.CUSTOMER_CALL_HISTORY,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.isSearched = false;
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
    })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        // this.addConfirm = true;
        // this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getMyCalls(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        if (unknownVar == "customer") {
          this.router.navigate([`/customer/manage-customers/view`, row['customerId']], { queryParams: { addressId: row['addressId'] } });
        }
        break;
      case CrudType.EXPORT:
        break;
      default:
    }
  }

  onTabChange(e) { }

  openNotes(rowData) { }

  onChangeStatus(rowData, ri) { }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }


}
