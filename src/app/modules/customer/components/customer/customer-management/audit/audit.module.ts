import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AuditComponent } from "./audit.component";
import { DecoderHistoryModule } from "./decoder-history/decoder-history.module";
import { ViewOfAuditModule } from "./view-of-audit/view-of-audit.module";


@NgModule({
  declarations: [AuditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    ViewOfAuditModule,
    DecoderHistoryModule,
  ],
  exports: [AuditComponent],
  entryComponents: [],
})
export class AuditModule { }
