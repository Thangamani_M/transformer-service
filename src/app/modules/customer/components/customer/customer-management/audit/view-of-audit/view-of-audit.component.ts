import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { PrimeNgTableVariablesModel } from '../../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-view-of-audit',
  templateUrl: './view-of-audit.component.html'
})
export class ViewOfAuditComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  @Input() partitionId;
  @Input() customerId;
  @Input() customerAddressId;
  row: any = {}
  filterForm: FormGroup;
  dataList: any;

  constructor(private rxjsService: RxjsService, private crudService: CrudService,
    private _fb: FormBuilder, private datePipe:DatePipe,
    private momentService: MomentService,) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Document Type",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Document Type', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Document Type',
            dataKey: 'auditId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'auditDateTime', header: 'Audit Data Time' }, 
            { field: 'auditType', header: 'Audit Type' }, 
            { field: 'radio', header: 'Radio' }, 
            { field: 'action', header: 'Action ' }, 
            { field: 'field', header: 'Field' }, 
            { field: 'newValue', header: 'New Value' }, 
            { field: 'oldValue', header: 'Old Value' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.VIEW_OF_AUDIT,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          }
        ]
      }
    }

  }

  ngOnInit(): void {
    this.createFilterForm()
  }

  createFilterForm(){
   this.filterForm = this._fb.group({
      formDate: ['', Validators.required],
      toDate: ['', Validators.required],
      customerId: [''],
      customerAddressId: []
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      let otherParams = {}
      if (this.customerId && this.customerAddressId) {
        this.getViewOfAudit(null, null, otherParams);
      }
    }

  }

  getViewOfAudit(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.partitionId == '') {
      const params = { customerId: this.customerId, CustomerAddressId: this.customerAddressId }
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { customerId: this.customerId, CustomerAddressId: this.customerAddressId, partitionId: this.partitionId }
      otherParams = { ...otherParams, ...params };
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.dataList.map(item=>{
          item.auditDateTime = this.datePipe.transform(item?.auditDateTime, 'dd-MM-yyyy, HH:mm:ss');
        })
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getViewOfAudit(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  submitFilter() {
  let formValue = this.filterForm.value
  let fromDate = this.momentService.localToUTC(formValue.fromDate)
  let toDate = this.momentService.localToUTC(formValue.toDate)
    let filteredData = Object.assign({},
      { customerId: this.filterForm.get('customerId').value ? this.filterForm.get('customerId').value : '' },
      { customerAddressId: this.filterForm.get('customerAddressId').value ? this.filterForm.get('customerAddressId').value : '' },
      { fromDate: fromDate ?fromDate: 'dd-MM-yyyy' },
      { toDate: toDate ? toDate : 'dd-MM-yyyy' }
    )
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.row['pageIndex'] = 0
    this.dataList = this.getViewOfAudit(this.row['pageIndex'], this.row['pageSize'], filterdNewData);
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}