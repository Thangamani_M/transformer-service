import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DecoderHistoryComponent } from "./decoder-history.component";


@NgModule({
  declarations: [DecoderHistoryComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [DecoderHistoryComponent],
  entryComponents: [],
})
export class DecoderHistoryModule { }
