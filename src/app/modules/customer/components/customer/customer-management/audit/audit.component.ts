import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-audit',
  templateUrl: './audit.component.html',
  styleUrls: ['./audit.component.scss']
})
export class AuditComponent implements OnInit {
  @Input() partitionId;
  @Input() customerAddressId;
  @Input() customerId;
  constructor() { }

  ngOnInit(): void {
  }

}
