import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { PrimeNgTableVariablesModel } from '../../../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-decoder-history',
  templateUrl: './decoder-history.component.html'
})
export class DecoderHistoryComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  @Input() partitionId;
  @Input() customerAddressId;
  @Input() customerId;
  row: any = {}

  constructor(private rxjsService: RxjsService, private crudService: CrudService,
    private datePipe:DatePipe) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Document Type",
      breadCrumbItems: [{ displayName: 'Configuration', relativeRouterUrl: '' }, { displayName: 'Document Type', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Document Type',
            dataKey: 'decoderId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'decoderName', header: 'Decoder' },
            { field: 'accountCode', header: 'Transmitter' },
            { field: 'auditDate', header: 'Change Date' },
            { field: 'userName', header: 'User' },
            { field: 'oldCustomerName', header: 'From Client' },],
            apiSuffixModel: EventMgntModuleApiSuffixModels.DECODER_HISTORY,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    // this.getDecoderHistory();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      let otherParams = {}
      if (this.customerId && this.customerAddressId) {
        this.getDecoderHistory(null, null, otherParams);
      }
    }

  }

  getDecoderHistory(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.partitionId == '') {
      const params = { customerId: this.customerId, IsAll: true, CustomerAddressId: this.customerAddressId }
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { customerId: this.customerId, IsAll: true, CustomerAddressId: this.customerAddressId, partitionId: this.partitionId }
      otherParams = { ...otherParams, ...params };
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.dataList.map(item=>{
          item.auditDate = this.datePipe.transform(item?.auditDate, 'dd-MM-yyyy, HH:mm:ss');
        })
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getDecoderHistory(row["pageIndex"], row["pageSize"], unknownVar)
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}