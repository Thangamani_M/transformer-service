import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ViewOfAuditComponent } from "./view-of-audit.component";


@NgModule({
  declarations: [ViewOfAuditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [ViewOfAuditComponent],
  entryComponents: [],
})
export class ViewOfAuditModule { }
