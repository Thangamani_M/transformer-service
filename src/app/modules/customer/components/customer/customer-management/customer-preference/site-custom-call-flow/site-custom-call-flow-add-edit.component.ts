import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, ReusablePrimeNGTableFeatureService } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService, SnackbarService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { CrudType, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared/utils';
import { SiteCustomCallFlowModel } from '@modules/customer/models/site-custom-call-flow-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-site-custom-call-flow-add-edit-component',
  templateUrl: './site-custom-call-flow-add-edit.component.html',
  styleUrls: ['./site-custom-call-flow.component.scss'],
})
export class SiteCustomCallFlowAddEditComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  siteCustomCallFlowAddEditForm: FormGroup;
  public formData = new FormData();
  userData: any;
  customerId: any;
  signalDropDown: any = [];
  daysDropDown: any = [];
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  row: any = {}
  loggedInUserData: LoggedInUserModel;
  customerPreferenceSiteCustomCallFlowId: any;
  customerAddressId: any;
  loggedUser: any;
  status;
  isSignals: boolean = false;
  isDays: boolean = false;
  isTimeRange: boolean = false;
  customerData: any;
  partitionId: any;
  constructor(private formBuilder: FormBuilder,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private rxjsService: RxjsService,
    public momentService: MomentService,
    private httpCancelService: HttpCancelService,
    private crudService: CrudService,
    private dialog: MatDialog,
    private snackbarService: SnackbarService,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private store: Store<AppState>) {
    super();
    this.customerPreferenceSiteCustomCallFlowId = this.config?.data?.editableObject?.customerPreferenceSiteCustomCallFlowId;
    this.partitionId = this.config?.data?.partitionId
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.rxjsService.getCustomerAddresId()
      .subscribe(data => {
        this.customerAddressId = data
      })
    this.rxjsService.getCustomerDate()
      .subscribe(data => {
        this.customerData = data
      })
    this.primengTableConfigProperties = {
      tableCaption: " ",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'customerPreferenceCustomerCustomCallFlowKeyHolderOptionId',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: true,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            canEdit:true,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'keyHolderName', header: 'Name', width: '100px' },
            { field: 'contactNo', header: 'Number', width: '100px' },
            { field: 'orderNumber', header: 'Order', width: '50px' },
            { field: 'isActive', header: 'Call', width: '100px', isActive: true, options: [] },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.KEYHOLDER_SITE_CUSTOM_CALL_FLOW,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            shouldShowCreateActionBtn: true,
          },
        ]
      }
    }
    if(this.customerPreferenceSiteCustomCallFlowId) {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[3].isActive = true;
    }
    this.status = [
      { label: 'Yes', value: true },
      { label: 'No', value: false },
    ];
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[3].options = this.status;
  }

  ngOnInit() {
    this.createSiteCustomCallFlowAddEditForm();
    this.enableDisableSignals();
    this.enableDisableDays();
    this.enableDisableTimeRange();
    this.onLoadValue();
  }

  onLoadValue() {
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    let otherParams: any = {};
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.loading = true;
    if (!this.customerPreferenceSiteCustomCallFlowId || this.partitionId == '') {
      let obj1 = { customerAddressId: this.customerAddressId, customerId: this.customerData.customerId, 
      partitionId:this.partitionId};
      if(this.partitionId == ''){
        delete obj1['partitionId'];
      }
      if (otherParams) {
        otherParams = { ...otherParams, ...obj1 };
      } else {
        otherParams = obj1;
      }
    } else {
      let obj1 = {
        customerAddressId: this.customerAddressId,
        customerId: this.customerData.customerId,
        customerPreferenceSiteCustomCallFlowId: this.customerPreferenceSiteCustomCallFlowId,
        partitionId:this.partitionId
      };
      if(this.partitionId == ''){
        delete obj1['partitionId'];
      }
      if (otherParams) {
        otherParams = { ...otherParams, ...obj1 };
      } else {
        otherParams = obj1;
      }
    }
    let api = [
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_TYPE_WITH_DESCRIPTION, null, false, null).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DAYS, null, false, null).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, eventMgntModuleApiSuffixModels, undefined, false, prepareRequiredHttpParams({ ...otherParams })).pipe(map(result => result), catchError(error => of(error))),
    ]
    if (this.customerPreferenceSiteCustomCallFlowId) {
      api.push(this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_PREFERENCE_SITE_CUSTOM_CALL_FLOW, this.customerPreferenceSiteCustomCallFlowId, false, null)
      .pipe(map(result => result), catchError(error => of(error))))
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((res: IApplicationResponse, ix: number) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          switch (ix) {
            case 0:
              this.signalDropDown = getPDropdownData(res?.resources);
              break;
            case 1:
              this.daysDropDown = getPDropdownData(res?.resources);
              break;
            case 2:
              this.dataList = res?.resources;
              this.totalRecords = res?.totalCount;
              this.isShowNoRecord = res?.resources ? false : true;
              break;
            case 3:
              this.getSiteCustomCallFlowDetailsById(res);
              break;
          }
        } else {
          switch (ix) {
            case 0:
              this.signalDropDown = res?.resources;
              break;
            case 1:
              this.daysDropDown = res?.resources;
              break;
            case 2:
              this.dataList = null
              this.totalRecords = 0;
              this.isShowNoRecord = true;
              break;
          }
        }
      })
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  } 

  createSiteCustomCallFlowAddEditForm(): void {
    let siteCustomCallFlowModel = new SiteCustomCallFlowModel();
    this.siteCustomCallFlowAddEditForm = this.formBuilder.group({
    });
    Object.keys(siteCustomCallFlowModel).forEach((key) => {
      this.siteCustomCallFlowAddEditForm.addControl(key, new FormControl(siteCustomCallFlowModel[key]));
    });
    this.siteCustomCallFlowAddEditForm = setRequiredValidator(this.siteCustomCallFlowAddEditForm, ["description"]);
    this.siteCustomCallFlowAddEditForm.get('createdUserId').setValue(this.loggedUser?.userId)
    this.enableDisableDays();
    this.enableDisableSignals();
    this.enableDisableTimeRange();
  }
  
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (!this.customerPreferenceSiteCustomCallFlowId || this.partitionId == '') {
      let obj1 = { customerAddressId: this.customerAddressId,
         customerId: this.customerData.customerId,
         partitionId:this.partitionId };
         if(this.partitionId == ''){
          delete obj1['partitionId'];
        }
      if (otherParams) {
        otherParams = { ...otherParams, ...obj1 };
      } else {
        otherParams = obj1;
      }
    } else {
      let obj1 = {
        customerAddressId: this.customerAddressId,
        customerId: this.customerData.customerId,
        customerPreferenceSiteCustomCallFlowId: this.customerPreferenceSiteCustomCallFlowId,
        partitionId:this.partitionId,
      };
      if (otherParams) {
        otherParams = { ...otherParams, ...obj1 };
      } else {
        otherParams = obj1;
      }
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null
        this.totalRecords = 0;

      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }

          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
        case CrudType.STATUS_POPUP:
        this.reusablePrimeNGTableFeatureService.openDynamicChangeStatusDialog(this.selectedTabIndex,
          this.primengTableConfigProperties, row)?.onClose?.subscribe((result) => {
            if (!result) {
              //this.getRequiredListData();
              this.dataList[this.selectedTabIndex].isActive = this.dataList[this.selectedTabIndex].isActive ? false : true;
            }
          });
          break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

  onChangeSelecedRows(e) {
    // this.emitSelectedRow.emit(e)
  }

  onSelectedRows(e) {
    this.selectedRows = e
  }

  //Get Details 
  getSiteCustomCallFlowDetailsById(response: IApplicationResponse) {
    if (response.isSuccess && response.statusCode == 200 && response.resources) {
      this.siteCustomCallFlowAddEditForm.patchValue({
        isSignals: response?.resources?.isSignals,
        isDays: response?.resources?.isDays,
        isTimeRange: response?.resources?.isTimeRange,
        description: response?.resources?.description,
      });
      if (response?.resources?.timeFrom) {
      let workingHoursFromdatetime = response?.resources?.timeFrom == null ? ("17:00:00").split(":") : response?.resources?.timeFrom == '00:00:00' ? ('17:00:00').split(":") : (response?.resources?.timeFrom).split(":");
      let workingHoursFromdate = new Date();
      workingHoursFromdate.setHours(workingHoursFromdatetime[0]);
      workingHoursFromdate.setMinutes(workingHoursFromdatetime[1]);
      this.siteCustomCallFlowAddEditForm.controls['timeFrom'].patchValue(workingHoursFromdate);
      }
      if (response?.resources?.timeTo) {
      let workingHoursTodatetime = response?.resources?.timeTo == null ? ("17:00:00").split(":") : response?.resources?.timeTo == '00:00:00' ? ('17:00:00').split(":") : (response?.resources?.timeTo).split(":");
      let workingHoursTodate = new Date();
      workingHoursTodate.setHours(workingHoursTodatetime[0]);
      workingHoursTodate.setMinutes(workingHoursTodatetime[1]);
      this.siteCustomCallFlowAddEditForm.controls['timeTo'].patchValue(workingHoursTodate);
      }
      if (response?.resources?.alarmTypeList) {
      var alarmTypeList = [];
      response?.resources?.alarmTypeList?.forEach(element => {
        alarmTypeList.push(element.alarmTypeId);
      });
      this.siteCustomCallFlowAddEditForm.get('alarmTypeList').setValue(alarmTypeList);
    }
    if (response?.resources?.dayList) {
      var dayList = [];
      response?.resources?.dayList?.forEach(element => {
        dayList.push(element.dayId);
      });
      this.siteCustomCallFlowAddEditForm.get('dayList').setValue(dayList);
    }
    this.isSignals = this.siteCustomCallFlowAddEditForm.get('isSignals').value;
    this.enableDisableSignals();
    this.isDays = this.siteCustomCallFlowAddEditForm.get('isDays').value;
    this.enableDisableDays();
    this.isTimeRange = this.siteCustomCallFlowAddEditForm.get('isTimeRange').value;
    this.enableDisableTimeRange();
    }
  }

  dialogClose() {
    this.ref.close(true)
  }

  onSubmit(): void {
    if (this.siteCustomCallFlowAddEditForm.invalid) {
      this.siteCustomCallFlowAddEditForm.markAllAsTouched();
      return;
    }
    this.siteCustomCallFlowAddEditForm.controls['keyHolderList'].patchValue(this.dataList);
    let formValue = this.siteCustomCallFlowAddEditForm.value;
    if (formValue.keyHolderList != null) {
      formValue.keyHolderList.forEach((element, index) => {
        formValue.keyHolderList[index].preference = index + 1;
      });
    }
    formValue.customerAddressId = this.customerAddressId;
    formValue.customerId = this.customerData.customerId;
    formValue.partitionId = this.partitionId?this.partitionId:'';
    formValue.customerPreferenceSiteCustomCallFlowId = this.customerPreferenceSiteCustomCallFlowId?this.customerPreferenceSiteCustomCallFlowId:'';
    if (this.siteCustomCallFlowAddEditForm.value.timeFrom) {
      formValue['timeFrom'] = this.momentService.convertTwelveToTwentyFourTime(this.siteCustomCallFlowAddEditForm.value.timeFrom);
    }
    if (this.siteCustomCallFlowAddEditForm.value.timeTo) {
      formValue['timeTo'] = this.momentService.convertTwelveToTwentyFourTime(this.siteCustomCallFlowAddEditForm.value.timeTo);
    }
    if (formValue.dayList && formValue.dayList.length) {
      if (formValue.dayList[0].hasOwnProperty('dayId')) {
        formValue = formValue;
      } else {
        formValue.dayList = formValue.dayList.filter(item => item != '')
        formValue.dayList = formValue.dayList.map(dayId => ({ dayId: dayId }))
      }
    }
    if (formValue.alarmTypeList && formValue.alarmTypeList.length) {
      if (formValue.alarmTypeList[0].hasOwnProperty('alarmTypeId')) {
        formValue = formValue;
      } else {
        formValue.alarmTypeList = formValue.alarmTypeList.filter(item => item != '')
        formValue.alarmTypeList = formValue.alarmTypeList.map(alarmTypeId => ({ alarmTypeId: alarmTypeId }))
      }
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.customerPreferenceSiteCustomCallFlowId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_PREFERENCE_SITE_CUSTOM_CALL_FLOW, formValue) :
      this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_PREFERENCE_SITE_CUSTOM_CALL_FLOW, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.rxjsService.setDialogOpenProperty(false);
        this.rxjsService.setGlobalLoaderProperty(false);
        // this.dialogRef.close(true);
        this.ref.close(false)
      }
    })
  }

  isSignalsChanged(event) {
    this.isSignals = event.target.checked;
    this.enableDisableSignals();
  }

  isDaysChanged(event) {
    this.isDays = event.target.checked;
    this.enableDisableDays();
  }

  isTimeRangeChanged(event) {
    this.isTimeRange = event.target.checked;
    this.enableDisableTimeRange();
  }

  enableDisableSignals() {
    if (this.isSignals) {
      this.siteCustomCallFlowAddEditForm.get('alarmTypeList').setValidators([Validators.required]);
      this.siteCustomCallFlowAddEditForm.get('alarmTypeList').updateValueAndValidity();
      this.siteCustomCallFlowAddEditForm.get('alarmTypeList').enable();
    }
    else {
      this.siteCustomCallFlowAddEditForm.get('alarmTypeList').disable();
    }
  }

  enableDisableDays() {
    if (this.isDays) {
      this.siteCustomCallFlowAddEditForm.get('dayList').setValidators([Validators.required]);
      this.siteCustomCallFlowAddEditForm.get('dayList').updateValueAndValidity();
      this.siteCustomCallFlowAddEditForm.get('dayList').enable();
    }
    else {
      this.siteCustomCallFlowAddEditForm.get('dayList').disable();
    }
  }

  enableDisableTimeRange() {
    if (this.isTimeRange) {
      this.siteCustomCallFlowAddEditForm.get('timeFrom').setValidators([Validators.required]);
      this.siteCustomCallFlowAddEditForm.get('timeTo').setValidators([Validators.required]);
      this.siteCustomCallFlowAddEditForm.get('timeFrom').updateValueAndValidity();
      this.siteCustomCallFlowAddEditForm.get('timeTo').updateValueAndValidity();
      this.siteCustomCallFlowAddEditForm.get('timeFrom').enable();
      this.siteCustomCallFlowAddEditForm.get('timeTo').enable();
    }
    else {
      this.siteCustomCallFlowAddEditForm.get('timeFrom').disable();
      this.siteCustomCallFlowAddEditForm.get('timeTo').disable();
    }
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
    this.dialog.closeAll();
  }

}
