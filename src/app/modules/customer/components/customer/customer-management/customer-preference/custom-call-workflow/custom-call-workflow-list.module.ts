import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomCallWorkflowAddEditModule } from "./custom-call-workflow-add-edit.module";
import { CustomCallWorkflowListComponent } from "./custom-call-workflow-list.component";

@NgModule({
  declarations: [CustomCallWorkflowListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    CustomCallWorkflowAddEditModule,
  ],
  exports: [CustomCallWorkflowListComponent],
  entryComponents: [],
})
export class CustomCallWorkflowListModule { }