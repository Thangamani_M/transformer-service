import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SiteDispatchFlowAddEditModule } from "./site-dispatch-flow-add-edit.module";
import { SiteDispatchFlowListComponent } from "./site-dispatch-flow-list.component";

@NgModule({
  declarations: [SiteDispatchFlowListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    SiteDispatchFlowAddEditModule
  ],
  exports: [SiteDispatchFlowListComponent],
  entryComponents: [],
})
export class SiteDispatchFlowListModule { }