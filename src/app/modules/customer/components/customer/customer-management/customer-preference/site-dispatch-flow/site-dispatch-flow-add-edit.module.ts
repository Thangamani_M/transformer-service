import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SiteDispatchFlowAddEditComponent } from "./site-dispatch-flow-add-edit.component";

@NgModule({
  declarations: [SiteDispatchFlowAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [SiteDispatchFlowAddEditComponent],
  entryComponents: [SiteDispatchFlowAddEditComponent],
})
export class SiteDispatchFlowAddEditModule { }