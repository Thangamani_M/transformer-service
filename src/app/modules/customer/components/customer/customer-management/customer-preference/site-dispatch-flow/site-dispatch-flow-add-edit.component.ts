import { Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MatOption, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { SiteDispatchFlowModel } from '@modules/customer/models/site-dispatch-flow-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-site-dispatch-flow-add-edit-component',
  templateUrl: './site-dispatch-flow-add-edit.component.html',
  styleUrls: ['./site-dispatch-flow.component.scss'],
})
export class SiteDispatchFlowAddEditComponent implements OnInit {
  customerPreferenceSiteDispatchFlowId: any;
  @Output() outputData = new EventEmitter<any>();
  observableResponse;
  selectedTabIndex: any = 0;
  leadId: string;
  siteDispatchFlowAddEditForm: FormGroup;
  public formData = new FormData();
  userData: any;
  submitted = false;
  customerId: any;
  signalDropDown: any = [];
  daysDropDown: any = [];
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  pageLimit: any = [10, 25, 50, 75, 100];
  searchKeyword: FormControl;
  searchForm: FormGroup
  columnFilterForm: FormGroup;
  searchColumns: any
  row: any = {}
  loggedInUserData: LoggedInUserModel;
  totalRecords: any;
  customerAddressId: any;
  loggedUser: any;
  partitionId: any;

  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  @ViewChild('allSelectedSignals', { static: false }) public allSelectedSignals: MatOption;
  @ViewChild('allSelectedDays', { static: false }) public allSelectedDays: MatOption;
  isSignals: boolean = false;
  isDays: boolean = false;
  isTimeRange: boolean = false;
  customerData: any;
  constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data,  private rxjsService: RxjsService,
    public dialogRef: MatDialogRef<SiteDispatchFlowAddEditComponent>, public momentService: MomentService, private httpCancelService: HttpCancelService, private crudService: CrudService,
    private dialog: MatDialog,  private store: Store<AppState>) {
      this.rxjsService.getCustomerDate()
      .subscribe(data => {
        this.customerData = data
      })
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.partitionId = this.data?.partitionId
    this.customerPreferenceSiteDispatchFlowId = this.data?.editableObject?.customerPreferenceSiteDispatchFlowId ? this.data?.editableObject?.customerPreferenceSiteDispatchFlowId : '';
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.createSiteDispatchFlowAddEditForm();
    this.getSignals();
    this.getDays();
    this.enableDisableSignals();
    this.enableDisableDays();
    this.enableDisableTimeRange();
    this.rxjsService.getCustomerAddresId()
      .subscribe(data => {
        this.customerAddressId = data
      })
    if (this.customerPreferenceSiteDispatchFlowId) {
      this.getSiteDispatchFlowDetailsById(this.customerPreferenceSiteDispatchFlowId);
      this.getSignals();
      this.getDays();
    }
  }

  createSiteDispatchFlowAddEditForm(): void {
    let siteDispatchFlowModel = new SiteDispatchFlowModel();
    this.siteDispatchFlowAddEditForm = this.formBuilder.group({
    });
    Object.keys(siteDispatchFlowModel).forEach((key) => {
      this.siteDispatchFlowAddEditForm.addControl(key, new FormControl(siteDispatchFlowModel[key]));
    });
    this.siteDispatchFlowAddEditForm = setRequiredValidator(this.siteDispatchFlowAddEditForm, ["description"]);
    this.siteDispatchFlowAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
  }

  getSiteDispatchFlowDetailsById(customerPreferenceSiteDispatchFlowId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_PREFERENCE_SITE_DISPATCH_FLOW, customerPreferenceSiteDispatchFlowId, false, null)

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          let workingHoursFromdatetime = response.resources.timeFrom == null ? ("17:00:00").split(":") : response.resources.timeFrom == '00:00:00' ? ('17:00:00').split(":") : (response.resources.timeFrom).split(":");
          let workingHoursFromdate = new Date();
          workingHoursFromdate.setHours(workingHoursFromdatetime[0]);
          workingHoursFromdate.setMinutes(workingHoursFromdatetime[1]);
          this.siteDispatchFlowAddEditForm.controls['timeFrom'].patchValue(workingHoursFromdate);

          let workingHoursTodatetime = response.resources.timeTo == null ? ("17:00:00").split(":") : response.resources.timeTo == '00:00:00' ? ('17:00:00').split(":") : (response.resources.timeTo).split(":");
          let workingHoursTodate = new Date();
          workingHoursTodate.setHours(workingHoursTodatetime[0]);
          workingHoursTodate.setMinutes(workingHoursTodatetime[1]);
          this.siteDispatchFlowAddEditForm.controls['timeTo'].patchValue(workingHoursTodate);
          response.resources.timeFrom = workingHoursFromdate
          response.resources.timeTo = workingHoursTodate
          this.siteDispatchFlowAddEditForm.patchValue(response.resources);

          var alarmTypeList = [];
          var dayList = [];
          response.resources.alarmTypeList.forEach(element => {
            alarmTypeList.push(element.alarmTypeId);
          });
          response.resources.dayList.forEach(element => {
            dayList.push(element.dayId);
          });
          this.siteDispatchFlowAddEditForm.get('alarmTypeList').setValue(alarmTypeList);
          this.siteDispatchFlowAddEditForm.get('dayList').setValue(dayList);

          this.isSignals = this.siteDispatchFlowAddEditForm.get('isSignals').value;
          this.enableDisableSignals();
          this.isDays = this.siteDispatchFlowAddEditForm.get('isDays').value;
          this.enableDisableDays();
          this.isTimeRange = this.siteDispatchFlowAddEditForm.get('isTimeRange').value;
          this.enableDisableTimeRange();
        }

        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  getSignals() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_TYPE_WITH_DESCRIPTION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.signalDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDays() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DAYS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.daysDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }



  toggleSignalOne(all) {
    if (this.allSelectedSignals.selected) {
      this.allSelectedSignals.deselect();
      return false;
    }
    if (this.siteDispatchFlowAddEditForm.controls.alarmTypeList.value.length == this.signalDropDown.length)
      this.allSelectedSignals.select();
  }

  toggleAllSignals() {
    if (this.allSelectedSignals.selected) {
      this.siteDispatchFlowAddEditForm.controls.alarmTypeList
        .patchValue([...this.signalDropDown.map(item => item.id), '']);
    } else {
      this.siteDispatchFlowAddEditForm.controls.alarmTypeList.patchValue([]);
    }
  }

  toggleDayOne(all) {
    if (this.allSelectedDays.selected) {
      this.allSelectedDays.deselect();
      return false;
    }
    if (this.siteDispatchFlowAddEditForm.controls.dayList.value.length == this.daysDropDown.length)
      this.allSelectedDays.select();
  }

  toggleAllDays() {
    if (this.allSelectedDays.selected) {
      this.siteDispatchFlowAddEditForm.controls.dayList
        .patchValue([...this.daysDropDown.map(item => item.id), '']);
    } else {
      this.siteDispatchFlowAddEditForm.controls.dayList.patchValue([]);
    }
  }

  onSubmit(): void {

    if (this.siteDispatchFlowAddEditForm.invalid) {
      this.siteDispatchFlowAddEditForm.markAllAsTouched();
      return;
    }

    let formValue = this.siteDispatchFlowAddEditForm.value;
    formValue.customerAddressId = this.customerAddressId;
    formValue.customerId = this.customerData.customerId;

    if (this.siteDispatchFlowAddEditForm.value.timeFrom)
      formValue['timeFrom'] = this.momentService.convertTwelveToTwentyFourTime(this.siteDispatchFlowAddEditForm.value.timeFrom);

    if (this.siteDispatchFlowAddEditForm.value.timeTo)
      formValue['timeTo'] = this.momentService.convertTwelveToTwentyFourTime(this.siteDispatchFlowAddEditForm.value.timeTo);

    if (formValue.dayList && formValue.dayList.length) {
      if (formValue.dayList[0].hasOwnProperty('dayId')) {
        formValue = formValue;
      } else {
        formValue.dayList = formValue.dayList.filter(item => item != '')
        formValue.dayList = formValue.dayList.map(dayId => ({ dayId }))
      }
    }

    if (formValue.alarmTypeList && formValue.alarmTypeList.length) {
      if (formValue.alarmTypeList[0].hasOwnProperty('alarmTypeId')) {
        formValue = formValue;
      } else {
        formValue.alarmTypeList = formValue.alarmTypeList.filter(item => item != '')
        formValue.alarmTypeList = formValue.alarmTypeList.map(alarmTypeId => ({ alarmTypeId }))
      }
    }

    formValue.partitionId = this.partitionId?this.partitionId : ''

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.customerPreferenceSiteDispatchFlowId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_PREFERENCE_SITE_DISPATCH_FLOW, formValue) :
      this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_PREFERENCE_SITE_DISPATCH_FLOW, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.rxjsService.setDialogOpenProperty(false);
        this.rxjsService.setGlobalLoaderProperty(false);
        this.dialogRef.close();
        this.outputData.emit(true);
        this.dialog.closeAll();
      }
    })

  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
    this.dialog.closeAll();
  }


  isSignalsChanged(event) {
    this.isSignals = event.target.checked;
    this.enableDisableSignals();
  }

  isDaysChanged(event) {
    this.isDays = event.target.checked;
    this.enableDisableDays();
  }

  isTimeRangeChanged(event) {
    this.isTimeRange = event.target.checked;
    this.enableDisableTimeRange();
  }

  enableDisableSignals() {
    if (this.isSignals) {
      this.siteDispatchFlowAddEditForm.get('alarmTypeList').setValidators([Validators.required]);
      this.siteDispatchFlowAddEditForm.get('alarmTypeList').updateValueAndValidity();
      this.siteDispatchFlowAddEditForm.get('alarmTypeList').enable();
    }
    else {
      this.siteDispatchFlowAddEditForm.get('alarmTypeList').disable();
    }
  }

  enableDisableDays() {
    if (this.isDays) {
      this.siteDispatchFlowAddEditForm.get('dayList').setValidators([Validators.required]);
      this.siteDispatchFlowAddEditForm.get('dayList').updateValueAndValidity();
      this.siteDispatchFlowAddEditForm.get('dayList').enable();
    }
    else {
      this.siteDispatchFlowAddEditForm.get('dayList').disable();
    }
  }

  cancelButton(){
    this.dialogRef.close()
  }

  enableDisableTimeRange() {
    if (this.isTimeRange) {
      this.siteDispatchFlowAddEditForm.get('timeFrom').setValidators([Validators.required]);
      this.siteDispatchFlowAddEditForm.get('timeTo').setValidators([Validators.required]);
      this.siteDispatchFlowAddEditForm.get('timeFrom').updateValueAndValidity();
      this.siteDispatchFlowAddEditForm.get('timeTo').updateValueAndValidity();
      this.siteDispatchFlowAddEditForm.get('timeFrom').enable();
      this.siteDispatchFlowAddEditForm.get('timeTo').enable();
    }
    else {
      this.siteDispatchFlowAddEditForm.get('timeFrom').disable();
      this.siteDispatchFlowAddEditForm.get('timeTo').disable();
    }
  }

}
