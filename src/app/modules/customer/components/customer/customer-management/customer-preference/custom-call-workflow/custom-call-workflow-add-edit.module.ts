import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomCallWorkflowAddEditComponent } from "./custom-call-workflow-add-edit.component";

@NgModule({
  declarations: [CustomCallWorkflowAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [CustomCallWorkflowAddEditComponent],
  entryComponents: [CustomCallWorkflowAddEditComponent],
})
export class CustomCallWorkflowAddEditModule { }