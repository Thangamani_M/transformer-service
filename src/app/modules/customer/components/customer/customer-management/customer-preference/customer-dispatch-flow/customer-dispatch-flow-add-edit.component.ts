import { Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MatOption, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { CustomerDispatchFlowModel } from '@modules/customer/models/customer-dispatch-flow-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-customer-dispatch-flow-add-edit-component',
    templateUrl: './customer-dispatch-flow-add-edit.component.html',
    styleUrls: ['./customer-dispatch-flow.component.scss'],
})
export class CustomerDispatchFlowAddEditComponent implements OnInit {
    customerPreferenceCustomerDispatchFlowId: any;
    observableResponse;
    selectedTabIndex: any = 0;
    @Output() outputData = new EventEmitter<any>();
    leadId: string;
    customerDispatchFlowAddEditForm: FormGroup;
    public formData = new FormData();
    userData: any;
    submitted = false;
    customerId: any;
    signalDropDown: any = [];
    daysDropDown: any = [];
    isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
    pageLimit: any = [10, 25, 50, 75, 100];
    searchKeyword: FormControl;
    searchForm: FormGroup
    columnFilterForm: FormGroup;
    searchColumns: any
    row: any = {}
    loggedInUserData: LoggedInUserModel;
    totalRecords: any;
    customerAddressId: any;
    loggedUser: any;
    customerData: any;
    @ViewChild('fileInput', null) myFileInputField: ElementRef;
    @ViewChild('allSelectedSignals', { static: false }) public allSelectedSignals: MatOption;
    @ViewChild('allSelectedDays', { static: false }) public allSelectedDays: MatOption;
    isSignals: boolean = false;
    isDays: boolean = false;
    isTimeRange: boolean = false;
    partitionId:any;

    constructor(private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private rxjsService: RxjsService,
        public dialogRef: MatDialogRef<CustomerDispatchFlowAddEditComponent>, public momentService: MomentService, private httpCancelService: HttpCancelService, private crudService: CrudService,
        private dialog: MatDialog, private route: Router, private store: Store<AppState>) {
        this.rxjsService.getCustomerDate()
            .subscribe(data => {
                this.customerData = data
            })
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.partitionId = this.data?.partitionId
        this.customerPreferenceCustomerDispatchFlowId = this.data?.editableObject?.customerPreferenceCustomerDispatchFlowId ? this.data?.editableObject?.customerPreferenceCustomerDispatchFlowId : '';
    }

    ngOnInit() {
        this.rxjsService.setDialogOpenProperty(true);
        this.createcustomerDispatchFlowAddEditForm();
        this.getSignals();
        this.getDays();
        this.enableDisableSignals();
        this.enableDisableDays();
        this.enableDisableTimeRange();
        this.rxjsService.getCustomerAddresId()
            .subscribe(data => {
                this.customerAddressId = data
            })
        if (this.customerPreferenceCustomerDispatchFlowId) {
            this.getCustomerDispatchFlowDetailsById(this.customerPreferenceCustomerDispatchFlowId);
            this.getSignals();
            this.getDays();
        }
    }

    createcustomerDispatchFlowAddEditForm(): void {
        let customerDispatchFlowModel = new CustomerDispatchFlowModel();
        this.customerDispatchFlowAddEditForm = this.formBuilder.group({
        });

        Object.keys(customerDispatchFlowModel).forEach((key) => {
            this.customerDispatchFlowAddEditForm.addControl(key, new FormControl(customerDispatchFlowModel[key]));
        });
        this.customerDispatchFlowAddEditForm = setRequiredValidator(this.customerDispatchFlowAddEditForm, ["description"]);
        this.customerDispatchFlowAddEditForm.get('createdUserId').setValue(this.loggedUser.userId)
    }

    //Get Details

    getCustomerDispatchFlowDetailsById(customerPreferenceCustomerDispatchFlowId: string) {
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_PREFERENCE_CUSTOMER_DISPATCH_FLOW, customerPreferenceCustomerDispatchFlowId, false, null)

            .subscribe((response: IApplicationResponse) => {
                if (response.resources) {
                    let workingHoursFromdatetime = response.resources.timeFrom == null ? ("17:00:00").split(":") : response.resources.timeFrom == '00:00:00' ? ('17:00:00').split(":") : (response.resources.timeFrom).split(":");
                    let workingHoursFromdate = new Date();
                    workingHoursFromdate.setHours(workingHoursFromdatetime[0]);
                    workingHoursFromdate.setMinutes(workingHoursFromdatetime[1]);
                    this.customerDispatchFlowAddEditForm.controls['timeFrom'].patchValue(workingHoursFromdate);

                    let workingHoursTodatetime = response.resources.timeTo == null ? ("17:00:00").split(":") : response.resources.timeTo == '00:00:00' ? ('17:00:00').split(":") : (response.resources.timeTo).split(":");
                    let workingHoursTodate = new Date();
                    workingHoursTodate.setHours(workingHoursTodatetime[0]);
                    workingHoursTodate.setMinutes(workingHoursTodatetime[1]);
                    this.customerDispatchFlowAddEditForm.controls['timeTo'].patchValue(workingHoursTodate);
                    response.resources.timeTo = workingHoursTodate
                    response.resources.timeFrom = workingHoursFromdate
                    this.customerDispatchFlowAddEditForm.patchValue(response.resources);

                    var alarmTypeList = [];
                    var dayList = [];
                    response.resources.alarmTypeList.forEach(element => {
                        alarmTypeList.push(element.alarmTypeId);
                    });
                    response.resources.dayList.forEach(element => {
                        dayList.push(element.dayId);
                    });
                    this.customerDispatchFlowAddEditForm.get('alarmTypeList').setValue(alarmTypeList);
                    this.customerDispatchFlowAddEditForm.get('dayList').setValue(dayList);

                    this.isSignals = this.customerDispatchFlowAddEditForm.get('isSignals').value;
                    this.enableDisableSignals();
                    this.isDays = this.customerDispatchFlowAddEditForm.get('isDays').value;
                    this.enableDisableDays();
                    this.isTimeRange = this.customerDispatchFlowAddEditForm.get('isTimeRange').value;
                    this.enableDisableTimeRange();
                }

                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    getSignals() {
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ALARM_TYPE_WITH_DESCRIPTION, null, false, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources) {
                    this.signalDropDown = response.resources;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    getDays() {
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.DAYS, null, false, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources) {
                    this.daysDropDown = response.resources;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }



    toggleSignalOne(all) {
        if (this.allSelectedSignals.selected) {
            this.allSelectedSignals.deselect();
            return false;
        }
        if (this.customerDispatchFlowAddEditForm.controls.alarmTypeList.value.length == this.signalDropDown.length)
            this.allSelectedSignals.select();
    }

    toggleAllSignals() {
        if (this.allSelectedSignals.selected) {
            this.customerDispatchFlowAddEditForm.controls.alarmTypeList
                .patchValue([...this.signalDropDown.map(item => item.id), '']);
        } else {
            this.customerDispatchFlowAddEditForm.controls.alarmTypeList.patchValue([]);
        }
    }

    toggleDayOne(all) {
        if (this.allSelectedDays.selected) {
            this.allSelectedDays.deselect();
            return false;
        }
        if (this.customerDispatchFlowAddEditForm.controls.dayList.value.length == this.daysDropDown.length)
            this.allSelectedDays.select();
    }

    toggleAllDays() {
        if (this.allSelectedDays.selected) {
            this.customerDispatchFlowAddEditForm.controls.dayList
                .patchValue([...this.daysDropDown.map(item => item.id), '']);
        } else {
            this.customerDispatchFlowAddEditForm.controls.dayList.patchValue([]);
        }
    }

    onSubmit(): void {

        if (this.customerDispatchFlowAddEditForm.invalid) {
            this.customerDispatchFlowAddEditForm.markAllAsTouched();
            return;
        }

        let formValue = this.customerDispatchFlowAddEditForm.value;
        formValue.customerAddressId = this.customerAddressId;
        formValue.customerId = this.customerData.customerId;
        if (this.customerDispatchFlowAddEditForm.value.timeFrom)
            formValue['timeFrom'] = this.momentService.convertTwelveToTwentyFourTime(this.customerDispatchFlowAddEditForm.value.timeFrom);

        if (this.customerDispatchFlowAddEditForm.value.timeTo)
            formValue['timeTo'] = this.momentService.convertTwelveToTwentyFourTime(this.customerDispatchFlowAddEditForm.value.timeTo);

        if (formValue.dayList && formValue.dayList.length) {
            if (formValue.dayList[0].hasOwnProperty('dayId')) {
                formValue = formValue;
            } else {
                formValue.dayList = formValue.dayList.filter(item => item != '')
                formValue.dayList = formValue.dayList.map(dayId => ({ dayId }))
            }
        }

        if (formValue.alarmTypeList && formValue.alarmTypeList.length) {
            if (formValue.alarmTypeList[0].hasOwnProperty('alarmTypeId')) {
                formValue = formValue;
            } else {
                formValue.alarmTypeList = formValue.alarmTypeList.filter(item => item != '')
                formValue.alarmTypeList = formValue.alarmTypeList.map(alarmTypeId => ({ alarmTypeId }))
            }
        }
        formValue.partitionId = this.partitionId?this.partitionId : ''

        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let crudService: Observable<IApplicationResponse> = (!this.customerPreferenceCustomerDispatchFlowId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_PREFERENCE_CUSTOMER_DISPATCH_FLOW, formValue) :
            this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_PREFERENCE_CUSTOMER_DISPATCH_FLOW, formValue)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess) {
                this.rxjsService.setDialogOpenProperty(false);
                this.rxjsService.setGlobalLoaderProperty(false);
                this.dialogRef.close(true);
                this.outputData.emit(true)
            }
        })

    };

    cancelButton(){
        this.dialogRef.close();
    }

    ngOnDestroy() {
        this.rxjsService.setDialogOpenProperty(false);
        this.dialog.closeAll();
    }


    isSignalsChanged(event) {
        this.isSignals = event.target.checked;
        this.enableDisableSignals();
    }

    isDaysChanged(event) {
        this.isDays = event.target.checked;
        this.enableDisableDays();
    }

    isTimeRangeChanged(event) {
        this.isTimeRange = event.target.checked;
        this.enableDisableTimeRange();
    }

    enableDisableSignals() {
        if (this.isSignals) {
            this.customerDispatchFlowAddEditForm.get('alarmTypeList').setValidators([Validators.required]);
            this.customerDispatchFlowAddEditForm.get('alarmTypeList').updateValueAndValidity();
            this.customerDispatchFlowAddEditForm.get('alarmTypeList').enable();
        }
        else {
            this.customerDispatchFlowAddEditForm.get('alarmTypeList').disable();
        }
    }

    enableDisableDays() {
        if (this.isDays) {
            this.customerDispatchFlowAddEditForm.get('dayList').setValidators([Validators.required]);
            this.customerDispatchFlowAddEditForm.get('dayList').updateValueAndValidity();
            this.customerDispatchFlowAddEditForm.get('dayList').enable();
        }
        else {
            this.customerDispatchFlowAddEditForm.get('dayList').disable();
        }
    }

    enableDisableTimeRange() {
        if (this.isTimeRange) {
            this.customerDispatchFlowAddEditForm.get('timeFrom').setValidators([Validators.required]);
            this.customerDispatchFlowAddEditForm.get('timeTo').setValidators([Validators.required]);
            this.customerDispatchFlowAddEditForm.get('timeFrom').updateValueAndValidity();
            this.customerDispatchFlowAddEditForm.get('timeTo').updateValueAndValidity();
            this.customerDispatchFlowAddEditForm.get('timeFrom').enable();
            this.customerDispatchFlowAddEditForm.get('timeTo').enable();
        }
        else {
            this.customerDispatchFlowAddEditForm.get('timeFrom').disable();
            this.customerDispatchFlowAddEditForm.get('timeTo').disable();
        }
    }

}
