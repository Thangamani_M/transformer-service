import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SiteCustomCallFlowAddEditComponent } from "./site-custom-call-flow-add-edit.component";

@NgModule({
  declarations: [SiteCustomCallFlowAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [SiteCustomCallFlowAddEditComponent],
  entryComponents: [SiteCustomCallFlowAddEditComponent],
})
export class SiteCustomCallFlowAddEditModule { }