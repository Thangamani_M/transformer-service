import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerDispatchFlowAddEditComponent } from "./customer-dispatch-flow-add-edit.component";

@NgModule({
  declarations: [CustomerDispatchFlowAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [CustomerDispatchFlowAddEditComponent],
  entryComponents: [CustomerDispatchFlowAddEditComponent],
})
export class CustomCallWorkflowAddEditModule { }