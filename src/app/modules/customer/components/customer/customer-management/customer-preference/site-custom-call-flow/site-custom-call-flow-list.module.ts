import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SiteCustomCallFlowAddEditModule } from "./site-custom-call-flow-add-edit.module";
import { SiteCustomCallFlowListComponent } from "./site-custom-call-flow-list.component";

@NgModule({
  declarations: [SiteCustomCallFlowListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    SiteCustomCallFlowAddEditModule,
  ],
  exports: [SiteCustomCallFlowListComponent],
  entryComponents: [],
})
export class SiteCustomCallFlowListModule { }