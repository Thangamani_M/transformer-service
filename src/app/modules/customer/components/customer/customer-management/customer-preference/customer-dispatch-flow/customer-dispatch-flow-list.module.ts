import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomCallWorkflowAddEditModule } from "./customer-dispatch-flow-add-edit.module";
import { CustomerDispatchFlowListComponent } from "./customer-dispatch-flow-list.component";

@NgModule({
  declarations: [CustomerDispatchFlowListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    CustomCallWorkflowAddEditModule
  ],
  exports: [CustomerDispatchFlowListComponent],
  entryComponents: [],
})
export class CustomerDispatchFlowListModule { }