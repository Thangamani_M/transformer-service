import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { PaymentTransferModel } from '@modules/customer/models/payment-transfer-model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { DebtorSearchComponent } from '@modules/collection/components/credit-controller/bank-statement-uploads/debtor-search/debtor-search.component';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-payment-transfer',
  templateUrl: './payment-transfer.component.html',
  styleUrls: ['./payment-transfer.component.scss']
})
export class PaymentTransferComponent extends PrimeNgTableVariablesModel implements OnInit {
  detailsData;
  debtorId:any;
  pageSize = 10;
  ChildDeptorId:any;
  addressId:any;
  transfersDocuments:any = [];
  loggedUser;
  debtorsList: any = []
  isSubmit: boolean = false;
  requiredForms: any = [];
  requiredFormList = [];
  transationTypes: any;
  TransactionType: any = [];
  optionalForms: any = [];
  fileName;
  optionalFormList = [];
  descriptionSubTypeName: string;
  formData = new FormData();
  optionalFormListName = [];
  descriptionTypeList = [];
  descriptionSubTypeList = [];
  paymenttransferForm:FormGroup;
  viewable: boolean = false;
  descriptionSubTypeId: string;
  requiredFormListName = [];
  id:any;
  transactionType;
  creditType;
  type;
  Transactiontypedescription='';
  InvoicePaymentAndOverrideId;
  paymentTransferId;
  paymentTransferDate;
  paymentTransferRefNo;
  debitAmount;
  startTodayDate: any = new Date();
  PaymentTransferDetail:any;
  DeptorInfoDetail:any;
  TransferFromInfoDetail:any;

  constructor(private crudService: CrudService,private snackbarService: SnackbarService,private formBuilder: FormBuilder,private momentService: MomentService,public dialogService: DialogService,
    private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>,
    private rxjsService: RxjsService) {
      super();
      this.primengTableConfigProperties = {
        tableCaption: "Payment Transfer",
        breadCrumbItems: [{ displayName: 'Customer Management', relativeRouterUrl: '' }, { displayName: 'Customer List', relativeRouterUrl: '' }, { displayName: 'Payment Transfer', relativeRouterUrl: '', }],
        selectedTabIndex: 0,
        tableComponentConfigs: {
          tabsList: [
            {
              caption: '',
              dataKey: '',
              captionFontSize: '21px',
              enableBreadCrumb: true,
              enableAction: true,
              enableReset: false,
              enableGlobalSearch: false,
              reorderableColumns: false,
              resizableColumns: false,
              enableScrollable: false,
              checkBox: true,
              enableRowDelete: false,
              enableFieldsSearch: false,
              enableHyperLink: false,
              cursorLinkIndex: 0,
              columns : [
                { field: 'divisionName', header: 'Division', width: '150px' },
                { field: 'debtorRefNo', header: 'Debtor Code', width: '150px' },
                { field: 'debtorName', header: 'Debtor Name', width: '150px' },
                { field: 'transferAmount', header: 'Transfer Amount',width: '150px' },
                // , value: this.debitAmount
              ],
              enableMultiDeleteActionBtn: false,
              enableRowDeleteActive: false,
              enableAddActionBtn: false,
              enableExportBtn: false,
              enableEmailBtn: false,
              shouldShowFilterActionBtn: false,
              areCheckboxesRequired: false,
              isDateWithTimeRequired: false,
              enableExportCSV: false
            }
          ]
        }
      }
      this.id = this.activatedRoute.snapshot.queryParams.customerId;
      this.debtorId = this.activatedRoute.snapshot.queryParams.parentDebtorId;
      this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
      this.InvoicePaymentAndOverrideId = this.activatedRoute.snapshot.queryParams.invoicePaymentAndOverrideIds;
      this.viewable = this.type == 'doa' ? true : false;
      this.transactionType = this.activatedRoute.snapshot.queryParams.transactionType;
      this.creditType = this.activatedRoute.snapshot.queryParams.creditType;
      this.type = this.activatedRoute.snapshot.queryParams.type;
      this.creditType = this.activatedRoute.snapshot.queryParams.creditType;
      this.paymentTransferId = this.activatedRoute.snapshot.queryParams.paymentTransferId,
      this.paymentTransferRefNo = this.activatedRoute.snapshot.queryParams.paymentTransferRefNo,
      this.paymentTransferDate= this.activatedRoute.snapshot.queryParams.paymentTransferDate,
      this.debitAmount = this.activatedRoute.snapshot.queryParams.debitAmount
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.loggedUser = userData;
      });
     }

  ngOnInit(): void {
    this.setDefaultForm();
    this.createForm();
    this.getCustomerInfo();
    this.getUxTransactionType();
    this.getDescriptionType();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createForm(transactionProcessModel?: PaymentTransferModel) {
    let dealerTypeModel = new PaymentTransferModel(transactionProcessModel);
    this.paymenttransferForm = this.formBuilder.group({});
    Object.keys(dealerTypeModel).forEach((key) => {
      this.paymenttransferForm.addControl(key, new FormControl());
    });
    this.paymenttransferForm = setRequiredValidator(this.paymenttransferForm, ["invoiceTransactionDescriptionId"]);
  }

  getCustomerInfo() {
    let params = { InvoicePaymentAndOverrideId: this.InvoicePaymentAndOverrideId }
    this.crudService.get(
      ModulesBasedApiSuffix.COLLECTIONS,
      CollectionModuleApiSuffixModels.TRANSFERS_DETAIL,
      null,
      false, prepareRequiredHttpParams(params)
    ).subscribe((response : IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
      this.detailsData = response.resources;
      this.onShowValue(this.detailsData.transactionDetailsDTO);
      this.onShowValue1(this.detailsData.transactionDetailsDTO);
      this.onShowValue2(this.detailsData.transactionDetailsDTO);
      this.paymenttransferForm.get('invoiceTransactionDescriptionId').setValue(this.detailsData.transactionDetailsDTO.invoiceTransactionDescriptionId);
      this.onChangeDescriptionType(this.detailsData.transactionDetailsDTO.invoiceTransactionDescriptionId);
       this.paymenttransferForm.get('transactionNotes').setValue(this.detailsData.transactionDetailsDTO.transactionNotes);
      this.paymenttransferForm.get('creditNoteDocumentDate').setValue(new Date(this.detailsData.transactionDetailsDTO.documentDate));
      if(this.detailsData.transactionDOASupportingDocuments){
        this.detailsData.transactionDOASupportingDocuments.forEach(element => {
          if (element.isRequired)
            this.requiredForms.push({ documentName: element.documentName, documentPath: element.documentPath });
          else
            this.optionalForms.push({ documentName: element.documentName, documentPath: element.documentPath });
        });
      }
    }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  onShowValue(response?: any) {
    this.PaymentTransferDetail = [{
      name: 'Customer INFO', columns: [
        { name: 'Customer ID', value: response ? response?.customerRegNo : '' },
        { name: 'Customer Name', value: response ? response?.customerName : '' },
        { name: 'Customer Type', value: response ? response?.customerType : '' },
        { name: 'Contact Address', value: response ? response?.siteAddress : '' },
        { name: 'Contact Number', value: response ? response?.contactNumber : ''},
        { name: 'Email Address', value: response ? response?.emailAddress : '' },

      ]
    }];
  }

  onShowValue1(response?: any) {
    this.DeptorInfoDetail = [{
      name: 'Debtor INFO', columns: [
        { name: 'Debtor Name', value: response ? response?.debtorName : '' },
        { name: 'Debtor Code', value: response ? response?.debtorCode : '' },
        { name: 'BDI Number', value: response ? response?.debtorBDINumber : '' },
        { name: 'Contact Number', value: response ? response?.debtorContact : '' },
        { name: 'Email Address', value: response ? response?.debtorEmail : ''},

      ]
    }];
  }
  onShowValue2(response?: any) {
    this.TransferFromInfoDetail = [{
      name: 'Debtor INFO', columns: [
        { name: 'Debtor Code', value: response ? response?.debtorName : '' },
        { name: 'Amount', value: this.debitAmount? this.debitAmount : '' },
        { name: 'Transaction Reference Number', value: this.paymentTransferRefNo? this.paymentTransferRefNo : '' },
      ]
    }];
  }
  onChangeDescriptionType(id) {
    if (id) {
      let params = { InvoiceTransactionDescriptionId: id }
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.UX_CREDIT_NOTES_DESCRIPTION_SUB_TYPE, null, false, prepareRequiredHttpParams(params)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.descriptionSubTypeList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }
  onChangeDescriptionSubType(id) {
    // let filter = this.descriptionSubTypeList.filter(x => x.id == id);
    // this.descriptionSubTypeName = filter[0].displayName;
    this.descriptionSubTypeId = id;
  }
  setDefaultForm() {
    this.requiredFormList.push({
      invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
      documentPath: '',
      documentName: '',
      isRequired: true
    });
    this.optionalFormList.push({
      invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
      documentPath: '',
      documentName: '',
      isRequired: false
    });
    this.requiredFormListName[0] = '';
    this.optionalFormListName[0] = '';
  }

  getDescriptionType() {
    if (this.viewable) {
      let type = (this.transactionType == 'CN') ? CollectionModuleApiSuffixModels.UX_CREDIT_NOTES : CollectionModuleApiSuffixModels.UX_DEBIT_NOTES;
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, type, null, false, null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.descriptionTypeList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    } else {
      let type = (this.creditType == 'credit-notes') ? CollectionModuleApiSuffixModels.UX_CREDIT_NOTES : CollectionModuleApiSuffixModels.UX_DEBIT_NOTES;
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, type, null, false, null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.descriptionTypeList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }

  }
    //TransactionType Dropdown
    getUxTransactionType() {
      this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_TRANSACTION_TYPES, null, false, null)
          .subscribe((response: IApplicationResponse) => {
              if (response.resources) {
                  this.transationTypes = response.resources;
              }
              this.transationTypes.forEach(element => {
                  let Transactiontype = element.displayName.split('-')[0];
                  this.TransactionType.push({ id: element.id, displayName: Transactiontype })
              });
              this.rxjsService.setGlobalLoaderProperty(false);
          });
  }

  obj: any;
  onFileSelected(files: FileList, type, index): void {
    const fileObj = files.item(0);
    this.fileName = fileObj.name;
    this.obj = fileObj;

    this.getFilePath(fileObj, type, index);
  }
  getFilePath(fileObj, type, index) {
    if (type == 'required-doc') {
      this.uploadDoc(fileObj, type).then((data) => {
        this.requiredFormList[index] = {
          invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
          documentPath: data,
          documentName: fileObj.name,
          isRequired: true
        };
      });
      this.requiredFormListName[index] = fileObj.name;
    }
    if (type == 'additional-doc') {
      this.uploadDoc(fileObj, type).then((data) => {
        this.optionalFormListName[index] = fileObj.name;
        this.optionalFormList[index] = {
          invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
          documentPath: data,
          documentName: fileObj.name,
          isRequired: false
        };
      });
    }

  }

  addRequiredFormList() {
    this.requiredFormList.push({
      invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
      documentPath: '',
      documentName: '',
      isRequired: true
    });
    this.requiredFormListName.push('');
  }

  addOptionalFormList() {
    this.optionalFormList.push({
      invoiceTransactionDescriptionSubTypeId: this.descriptionSubTypeId,
      documentPath: '',
      documentName: '',
      isRequired: false
    });
  }

  removeRequiredFormList(index) {
    this.requiredFormList.splice(index, 1);
    this.requiredFormListName.splice(index, 1);
  }
  removeOptionalFormList(index) {
    this.optionalFormList.splice(index, 1);
    this.optionalFormListName.splice(index, 1);
  }

  uploadDoc(fileObj, type) {
    this.formData = new FormData();
    return new Promise((resolve, reject) => {
      this.formData.append("file", fileObj);
      let type = this.creditType == 'payment-transfer' ? CollectionModuleApiSuffixModels.CREDIT_NOTES_UPLOAD : CollectionModuleApiSuffixModels.DEBIT_NOTES_UPLOAD;
      let api = this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, type, this.formData);
      api.subscribe((res: any) => {
        if (res?.isSuccess == true && res?.statusCode == 200) {
          resolve(res.resources);
        }
        else
          reject(null);

        this.rxjsService.setGlobalLoaderProperty(false);
      })
    });

  }

  cancel(){
    this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.id, debtorId: this.debtorId, addressId: this.addressId } });
  }

  onSubmit(type) {
     this.isSubmit = true;
     if (this.paymenttransferForm.invalid) {
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    let transfersDocuments = [];
    let documentName = [];
    let documents = this.requiredFormList.concat(this.optionalFormList);
    if(documents && documents.length >0){
      documents.forEach((ele) => {
        if (ele.documentPath != null && ele.documentPath != ''){
          documentName.push(ele.documentName);
          transfersDocuments.push({  invoiceTransactionDescriptionSubTypeDocumentId: ele.invoiceTransactionDescriptionSubTypeId,invoiceTransactionDescriptionDocumentId:ele.invoiceTransactionDescriptionSubTypeId ,documentName, isRequired:ele.isRequired,customDocumentName:ele.documentName});
        }
      });
    }
    if (!this.viewable &&  (this.paymenttransferForm.invalid || (this.requiredFormList && this.requiredFormList[0].documentPath == ''))) {
      return;
    }
   // this.formData =
    let formValue = this.paymenttransferForm.value;
    let finalObj = null;
    let creditNoteDocumentDate = this.detailsData.transactionDetailsDTO.documentDate;
    creditNoteDocumentDate = (creditNoteDocumentDate && creditNoteDocumentDate != null) ? this.momentService.toMoment(creditNoteDocumentDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    finalObj = {
      paymentTransferId: '',
      paymentTransferRefNo:this.paymentTransferRefNo,
      paymentTransferDate:this.paymentTransferDate,
      transferAmount:this.debitAmount,
      customerId: this.id,
      addressId:this.addressId,
      parentDebtorId:this.debtorId,
       creditNoteDocumentDate: creditNoteDocumentDate,
      InvoicePaymentAndOverrideId: this.InvoicePaymentAndOverrideId,
      invoiceTransactionDescriptionSubTypeId: formValue.invoiceTransactionDescriptionSubTypeId,
      createdUserId: this.loggedUser.userId,
      comments:formValue.transactionNotes,
      alternateDebtorId:this.ChildDeptorId
    }
    this.transfersDocuments.forEach(transfersDocuments => {
      this.formData.append('file', transfersDocuments);
    });
    this.formData.delete('transfersDocuments');
    this.formData.append('transfersDocuments', JSON.stringify(finalObj));
    let api =  this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.TRANSFERS, this.formData, 1);
    api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.id, debtorId: this.debtorId, addressId: this.addressId } });
            }
          }

    )}

    openDoaSearchModel(event?) {
      if(this.debtorsList.length !=0) {
        return this.snackbarService.openSnackbar("Cannot able to add more then one debtor", ResponseMessageTypes.WARNING)
      }
      const ref = this.dialogService.open(DebtorSearchComponent, {
        showHeader: true,
        header: `Search Debtor`,
        baseZIndex: 10000,
        width: '1200px',
        data: {  },
        dismissableMask: true
      });
      ref.onClose.subscribe((resp) => {
        if (resp?.debtorId) {
         this.ChildDeptorId = resp?.debtorId
        // resp.push(debitAmount=this.debitAmount)
          this.getDebtores(resp)
        }
      })
    }

    getDebtores(debtorData) {
      this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SPLIT_PAYMENTS, debtorData.debtorId).subscribe(response => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.debtorsList.push({ balanceInfo: [response.resources], ...debtorData });

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
}
