import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { PaymentTransferComponent } from "./payment-transfer.component";
import { BankStatementUploadsModule } from '@modules/collection/components/credit-controller/bank-statement-uploads/bank-statement-uploads.module';
import { PaymentTransferRoutingModule } from "./payment-transfer-routing.module";
@NgModule({
    declarations: [PaymentTransferComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    PaymentTransferRoutingModule,
    BankStatementUploadsModule,
  ],
})
export class PaymentTransferModule { }