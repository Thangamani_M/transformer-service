import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentTransferComponent } from './payment-transfer.component';

const routes: Routes = [
    {
        path: '', component: PaymentTransferComponent, data: { title: 'Payment Transfer' }
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class PaymentTransferRoutingModule { }
