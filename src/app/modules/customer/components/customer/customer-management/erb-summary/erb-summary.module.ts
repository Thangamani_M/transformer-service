import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GMapModule } from "primeng/gmap";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ErbSummaryComponent } from "./erb-summary.component";

@NgModule({
  declarations: [ErbSummaryComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    GMapModule,
  ],
  exports: [ErbSummaryComponent],
  entryComponents: [],
})
export class ErbSummaryModule { }