import { Component, Input, OnInit, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MatMenuItem } from '@angular/material';
import { ActivatedRoute} from '@angular/router';
import { AppState } from '@app/reducers';
import { LoggedInUserModel, CrudService, SnackbarService, RxjsService, ModulesBasedApiSuffix, debounceTimeForSearchkeyword, CrudType,ResponseMessageTypes, prepareRequiredHttpParams, CustomDirectiveConfig} from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { TableFilterFormService } from '@app/shared/services/create-form.services';

@Component({
  selector: 'app-erb-summary',
  templateUrl: './erb-summary.component.html'
})
export class ErbSummaryComponent implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  @Input() partitionId;
  @Input() customerAddressId;
  @Input() customerId;
  @Input() permission;
  loading1 = false;
  observableResponse;
  ErbResponse: any;
  selectedTabIndex: any = 0;
  primengTableConfigProperties: any;
  dataList: any;
  public bradCrum: MatMenuItem[];
  selectedColumns: any[];
  selectedRows: string[] = [];
  totalRecords: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  searchKeyword: FormControl;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  searchColumns: any;
  loading: boolean;
  status: any = [];
  today: any = new Date();
  row: any = {};
  erbBillingId: any;
  ErbSummary: any = '';
  pageSize: number = 10;
  ShowBillingInfo: boolean = false;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  constructor(private crudService: CrudService,private tableFilterFormService: TableFilterFormService,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService, private snackbarService: SnackbarService,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder) {
    // this.customerId = this.activatedRoute.snapshot.paramMap.get('id');

    // this.rxjsService.getCustomerAddresId()
    //   .subscribe(data => {
    //     this.customerAddressId = data
    //   })
    this.primengTableConfigProperties = {
      tableCaption: "",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '', relativeRouterUrl: '' }, { displayName: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Create Messgae',
            dataKey: 'erbBillingId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            areCheckboxesRequired: true,
            columns: [{ field: 'year', header: 'Year' },
            { field: 'occurrenceMonth', header: 'Month' },
            { field: 'arrivalCount', header: 'Arrivals Count' },
            { field: 'patrolCount', header: 'Patrols' },
            { field: 'erbAmount', header: 'ERB Count' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.ERB_SUMMARY,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            shouldShowDeleteActionBtn: true,
          },
        ]
      }
    }

    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? params['params']['tab'] ? +params['params']['tab'] : 0 : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
     this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest()
    this.combineLatestNgrxStoreData()
    // this.getRequiredListData();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      let otherParams = {}
      if (this.customerId && this.customerAddressId) {
        this.getRequiredListData(null, null, otherParams);
      }
    }

  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }
  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {

    if (this.partitionId == '') {
      const params = { customerId: this.customerId, IsAll: true, CustomerAddressId: this.customerAddressId }
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { customerId: this.customerId, IsAll: true, CustomerAddressId: this.customerAddressId, partitionId: this.partitionId }
      otherParams = { ...otherParams, ...params };
    }
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.ERB_SUMMARY,
      undefined,
      false, prepareRequiredHttpParams(otherParams)
    ).subscribe(data => {
      this.rxjsService.setGlobalLoaderProperty(false);

      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.resources.length;
        this.row['pageIndex'] = this.row['pageIndex'] + 1
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
    })
  }

  onChecked(data, i?) {
    if (data) {
      data.isChecked = !data.isChecked;
      if (data.isChecked) {
        this.ShowBillingInfo = true;
        let obj1 = {
          CustomerAddressId: data.customerAddressId,
          CustomerId: data.customerId,
          Month:data.month,
         // Month :"5",
          Year:data.year,
        }
        this.crudService.get(
          ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          EventMgntModuleApiSuffixModels.ERB_SUMMARY_DETAILS,
          undefined,
          false, prepareRequiredHttpParams(obj1)
        ).subscribe(details => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (details.isSuccess) {
            this.ErbResponse = details.resources;
          }
        })
      }
      else {
        this.ShowBillingInfo = false;
      }
    }
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        //  this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        // this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        //   this.openAddEditPage(CrudType.VIEW, row);


        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            // this.onOneOrManyRowsDelete()
          }
        }
         else {
          //this.onOneOrManyRowsDelete(row)
        }
        break;
      case CrudType.EXPORT:
        break;
      default:
    }
  }

  onChangeStatus(rowData,ri) {}

  exportExcel() {}

  onTabChange(e) {}
}
