import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatIconModule } from '@angular/material';
import { SharedModule } from "@app/shared";
import { AddContactDialogComponent } from './add-contact-dialog.component';



@NgModule({
  declarations: [AddContactDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
  ],
  exports:[AddContactDialogComponent],
  entryComponents:[AddContactDialogComponent]
})
export class AddContactDialogModule { }
