import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { countryCodes, formConfigs } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'app-add-contact-dialog',
  templateUrl: './add-contact-dialog.component.html'
})
export class AddContactDialogComponent implements OnInit {

  addNewContactForm: FormGroup
  loggedUser: any
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  @ViewChild("input", { static: false }) row;

  constructor(
    public dialogRef: MatDialogRef<AddContactDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private store: Store<AppState>,  public _fb: FormBuilder,) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createAddNewContactForm()
  }

  createAddNewContactForm() {
    this.addNewContactForm = this._fb.group({
      contactName: ['', Validators.required],
      contactNoCountryCode: ['+27', Validators.required],
      contactNo: ['', Validators.required],
      isPrimary: [this.data, Validators.required],
      keyholderId: null,
      callInitiationContactId: null,
    });
    this.onFormControlChange();
  }

  addContactSubmit() {
    if (this.addNewContactForm.invalid) {
      return
    }
    let formData = this.addNewContactForm.value
    // formData.callInitiationId = this.initiationId ? this.initiationId : null
    formData.contactNo = formData.contactNo.replace(/\s/g, "")
    let newcontactData = {
      keyHolderId: Math.random(),
      keyHolderName: formData.contactName,
      contactNumber: formData.contactNoCountryCode + ' ' + formData.contactNo,
      isNewContact: true,
      isPrimary: formData.isPrimary
    };
    this.dialogRef.close(newcontactData);
  }

  
  onFormControlChange(){
    this.addNewContactForm
    .get("contactNoCountryCode")
    .valueChanges.subscribe((contactNoCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(contactNoCountryCode);
      setTimeout(() => {
        this.row.nativeElement.focus();
        this.row.nativeElement.blur();
      });
    });

    this.addNewContactForm
      .get("contactNo")
      .valueChanges.subscribe((contactNo: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.addNewContactForm.get("contactNoCountryCode").value
        );
      });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.addNewContactForm
          .get("contactNo")
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
        break;
      default:
        this.addNewContactForm
          .get("contactNo")
          .setValidators([
            Validators.minLength(formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),
          ]);
        break;
    }
  };

  close() {
    this.dialogRef.close();
  }

}
