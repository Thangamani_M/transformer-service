import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, PrimengStatusConfirmDialogComponent, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { encodedTokenSelector, loggedInUserData, selectStaticEagerLoadingCustomerTypesState$ } from "@modules/others";
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
@Component({
  selector: 'app-safe-entry-request-list',
  templateUrl: './safe-entry-request-list.component.html',
  styleUrls: ['./safe-entry-request-list.component.scss']
})
export class SafeEntryRequestListComponent implements OnInit {
  @Input() inputObject = { fromComponentType: 'customer', customerId: "", addressId: "", partitionId: "" };
  @Input() permission;
  observableResponse;
  selectedTabIndex = 0;
  dataList;
  totalRecords;
  selectedColumns = [];
  selectedRows: string[] = [];
  selectedRow;
  pageLimit = [10, 25, 50, 75, 100];
  loading = false;
  searchColumns;
  columnFilterForm: FormGroup;
  row = {};
  customerType;
  searchForm: FormGroup;
  status = [];
  otherParams = {};
  customerId;
  addressId;
  encodedToken;
  loggedUser: any;
  loggedInUserData: LoggedInUserModel;
  isEligible: Boolean = false;
  pageSize: number = 10;

  primengTableConfigProperties = {
    tableCaption: "Safe Entry Request",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Customers Management' }, { displayName: 'Safe Entry Request', relativeRouterUrl: '/customer/dashboard' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Safe Entry Request',
          dataKey: 'customerSafeEntryRequestId',
          enableBreadCrumb: false,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          enableSecondHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'contactPerson', header: 'Contact Person' },
            { field: 'requestDate', header: 'Requested Date' },
            { field: 'requestTime', header: 'Request Time' },
            { field: 'safeEntryRequestPremisesTypeNames', header: 'Safe Entry Request' },
            { field: 'contactNumber', header: 'Contact Number' },
            { field: 'createdBy', header: 'Created By' },
            { field: 'status', header: 'Status' }],
          shouldShowDeleteActionBtn: false,
          ebableAddActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          enableMultiDeleteActionBtn: false,
          apiSuffixModel: CustomerModuleApiSuffixModels.SAFE_ENTRY_POST,
          moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        }]
    }
  }
  safeEntryEligible: any;


  constructor(
    private httpService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    private router: Router,
    private rxjsService: RxjsService,
    private dialogService: DialogService,
    private store: Store<AppState>,
    private momentService: MomentService,
    private _fb: FormBuilder,
    private snackbarService : SnackbarService
  ) {
    this.columnFilterForm = this._fb.group({});
    // this.activatedRoute.queryParams.subscribe(params => {
    //     this.customerId = params.customerId;
    //     this.addressId = params.addressId
    //   });

    this.store.pipe(select(encodedTokenSelector)).subscribe((encodedToken: string) => {
      this.encodedToken = encodedToken;
    });
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.getCustomers();
    this.getcustomersList()
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData();
  }

  loadPaginationLazy(event) {
    this.otherParams = {};
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(selectStaticEagerLoadingCustomerTypesState$),
    this.store.select(loggedInUserData),
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.customerType = response[0];
        this.loggedInUserData = new LoggedInUserModel(response[1]);
      });
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.otherParams = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    // [breadCrumbItem['queryParams']['key']]: breadCrumbItem['queryParams']['value']
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object): void {
    if (CrudType.GET === type && Object.keys(row).length > 0) {
      Object.keys(row['searchColumns']).forEach((key) => {
        if (key == 'status') {
          let status = row['searchColumns'][key]['value'];
          key = 'status';
          this.otherParams[key] = status.id;
        } else if (key == 'customerType') {
          let type = row['searchColumns'][key]['value'];
          key = 'customerType';
          this.otherParams[key] = type.id;
        }
        else {
          this.otherParams[key] = row['searchColumns'][key]['value'];
        }
      });
      this.otherParams['sortOrder'] = row['sortOrder'];
      if (row['sortOrderColumn']) {
        this.otherParams['sortOrderColumn'] = row['sortOrderColumn'];
      }
    }
    switch (type) {
      case CrudType.CREATE:
        if(this.inputObject.fromComponentType =="customer" && !this.permission.canCreate){
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditCustomerPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        Object.keys(this.otherParams).forEach((key) => {
          if (key.toLowerCase().includes('date')) {
            this.otherParams[key] = this.momentService.localToUTC(this.otherParams[key]);
          } else {
            this.otherParams[key] = this.otherParams[key];
          }
        });
        switch (this.selectedTabIndex) {
          case 0:
            this.getcustomersList(
              row["pageIndex"], row["pageSize"], this.otherParams);
            break;
        }
        break;
      case CrudType.EDIT:
        if(this.inputObject.fromComponentType =="customer" && !this.permission.canEdit){
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditCustomerPage(CrudType.EDIT, row);
        break;
    }
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  openAddEditCustomerPage(type: CrudType | string, editableObject: any): void {
    if (type === 'create') {
      this.router.navigate(['/customer/manage-customers/safe-entry-request/add-edit'], { queryParams: { customerId: this.inputObject.customerId, addressId: this.inputObject.addressId, partitionId: this.inputObject.partitionId } })

    } else {
      this.router.navigate(['/customer/manage-customers/safe-entry-request/add-edit'], { queryParams: { id: editableObject['customerSafeEntryRequestId'], customerId: this.inputObject.customerId, addressId: this.inputObject.addressId, partitionId: this.inputObject.partitionId ,status: editableObject?.status} })
    }
  }

  getcustomersList(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    const params = { customerId: this.inputObject.customerId, CustomerAddressId: this.inputObject.addressId }
    otherParams = { ...otherParams, ...params };
    this.loading = true;
    this.httpService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.SAFE_ENTRY_POST,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getCustomers(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    const params = { customerId: this.inputObject.customerId, CustomerAddressId: this.inputObject.addressId, Authorization: this.encodedToken }
    otherParams = { ...otherParams, ...params };
    this.loading = true;
    this.httpService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.SAFE_ENTRY_REQUEST,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.statusCode === 200 && response.resources && response.isSuccess) {
        this.safeEntryEligible = response.resources
        this.isEligible = response?.resources?.message == "Eligible" ? true : false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
}
