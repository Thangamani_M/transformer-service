import { Component, ElementRef, Inject, OnInit, Optional, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { ContactListModel, SafeEntryRequestAddEditModal } from '@modules/customer/models/customer-safe-entry-request-add-edit.model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { AddContactDialogComponent } from '../add-contact-dialog/add-contact-dialog.component';
@Component({
  selector: 'app-safe-entry-request-add-edit',
  templateUrl: './safe-entry-request-add-edit.component.html',
  styleUrls: ['./safe-entry-request-add-edit.component.scss']
})
export class SafeEntryRequestAddEditComponent implements OnInit {
  @ViewChildren('contactRows',) contactRows: QueryList<ElementRef>;
  @ViewChildren('emergencyContactRows') emergencyContactRows: QueryList<ElementRef>;
  formConfigs = formConfigs;
  requestForm: FormGroup;
  safeEntryRequestForm: FormGroup;
  contactList: FormArray;
  cancellationForm: FormGroup;
  SafeEntryRequestId: string;
  header: string;
  loggedInUserData: LoggedInUserModel;
  btnName: string;
  errorMessage: string;
  isActive: boolean;
  loggedUser: UserLogin;
  details;
  cancelModel = false;
  safeEntryData
  numbers = [];
  customerId;
  addressId;
  exampleHeader;
  todayDate = new Date();
  minToDate = new Date();
  minPaidPatrolStartDate = new Date();
  minPaidPatrolEndDate = new Date();
  isNumberOfDailyPaidpatrols = false;
  countryCodes = countryCodes;
  isContactFocus = false;
  @ViewChild('prefix', { static: false }) prefix: ElementRef<HTMLSelectElement>;
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  permisesTypes;
  vehicalModel;
  vehicalMake;
  vehicalcolor;
  customerKeyHolder = [];
  encodedToken;
  startTodayDate = new Date();
  timeMessage: any = []
  minDate1 = new Date()
  maxDate = new Date()
  partitionId: any;
  status: any
  constructor(private activatedRoute: ActivatedRoute,
    private momentService: MomentService,
    @Optional() public dialogRef: MatDialogRef<AddContactDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private dilog: MatDialog,
    private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService,
    private store: Store<AppState>,
    private router: Router,
    private crudService: CrudService,
    private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,) {
    let date = new Date()
    this.timeMessage = [({ severity: 'warn', summary: 'Please enter a time greater than ' + date.getFullYear() + '-' +date.getMonth()+1 + -+date.getDate() + "," + date.getHours() + ':' + date.getMinutes() + "," + 18 + " " + "Min" + '', detail: '' })];
    const year = date.getFullYear()
    const month = date.getMonth()
    const date1 = date.getDate()
    const hour = date.getHours()
    const hour1 = date.getMinutes()
    this.minDate1 = new Date(year, month, date1, hour, hour1 + 15);
    this.maxDate = new Date(year, month, date1, hour + 23)
    this.SafeEntryRequestId = this.activatedRoute.snapshot.queryParams.id;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.addressId = this.activatedRoute.snapshot.queryParams.addressId;
    this.partitionId = this.activatedRoute.snapshot.queryParams.partitionId;
    this.status = this.activatedRoute.snapshot.queryParams?.status;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createSafeEntryRequestForm();
    this.createCancellationForm()
    this.createCancelForm();
    this.getPermises();
    this.getKeyHolderDrop()
    this.getVehicalColorDrop();
    this.getVehicalMakeDrop();
    this.getVehicalModelDrop();
    if (this.SafeEntryRequestId) {
      this.getSafeEntryRequestById().subscribe((response: IApplicationResponse) => {
        this.safeEntryData = response.resources;
        this.safeEntryData.requestDate = new Date(this.safeEntryData.requestDate)
        this.safeEntryData.requestTime = this.formateTime(this.safeEntryData.requestTime)
        this.safeEntryData.vehicleColorId = this.safeEntryData.vehicleColorName
        this.safeEntryData.vehicleModelId = this.safeEntryData.vehicleModelName
        this.safeEntryData.vehicleMakeId = this.safeEntryData.vehicleMakeName
        this.safeEntryRequestForm.patchValue(this.safeEntryData);
        let contactListArray = this.contactListFormArray;
        for (const obj of this.safeEntryData.contactDetailsList) {
          if (obj.keyHolderId == undefined) { this.customerKeyHolder.push({ customerName: obj.contactName, mobile1: `+27 ${obj.contactNumber}`, ...obj }); }
          contactListArray.push(this.safeEntryRequestModel({ customerNumber: `+27 ${obj.contactNumber}`, ...obj }));
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  formateTime(time) {
    if (!time) return
    let timeToRailway = this.momentService.convertNormalToRailayTimeOnly(time)
    let TimeArray: any = timeToRailway.split(':');
    let date = (new Date).setHours(TimeArray[0], TimeArray[1]);
    return new Date(date)
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getVehicalModelDrop() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_MODEL, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicalModel = response.resources;
        }
      });
  }

  getVehicalMakeDrop() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_MAKE, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicalMake = response.resources;
        }
      });
  }

  getVehicalColorDrop() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VEHICAL_COLOR, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.vehicalcolor = response.resources;
        }
      });
  }

  getSafeEntryRequestById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.SAFE_ENTRY_POST,
      this.SafeEntryRequestId,
      false,
      undefined
    );
  }

  get contactListFormArray(): FormArray {
    if (!this.safeEntryRequestForm) return;
    return this.safeEntryRequestForm.get("contactList") as FormArray;
  }

  getPermises() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SAFE_ENTRY_PREMISES_TYPES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.permisesTypes = response.resources;
        }
      });
  }

  getKeyHolderDrop(otherParams?: object) {
    if (this.partitionId == '') {
      const params = { customerId: this.customerId, CustomerAddressId: this.addressId }
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { customerId: this.customerId, CustomerAddressId: this.addressId, partitionId: this.partitionId }
      otherParams = { ...otherParams, ...params };
    }
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.KEYHOLDER_DETAILS_LIST,
      undefined,
      false,
      prepareRequiredHttpParams(otherParams)
    ).subscribe((response: IApplicationResponse) => {
      this.customerKeyHolder = response.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createSafeEntryRequestForm(): void {
    let safeEntryRequestAddEditModel = new SafeEntryRequestAddEditModal();
    this.safeEntryRequestForm = this.formBuilder.group({
      contactList: this.formBuilder.array([], Validators.minLength(1))
    });
    Object.keys(safeEntryRequestAddEditModel).forEach((key) => {
      if (key == "requestDate" || key == "requestTime") {
        this.safeEntryRequestForm.addControl(key, new FormControl({ value: safeEntryRequestAddEditModel[key], disabled: false }));
      } else {
        this.safeEntryRequestForm.addControl(key, new FormControl({ value: safeEntryRequestAddEditModel[key], disabled: false }));
      }
    });
    this.safeEntryRequestForm = setRequiredValidator(this.safeEntryRequestForm, ["requestDate", "safeEntryRequestPremisesTypeId", "requestTime", 'vehicleRegistrationNumber', 'notes',
      'vehicleModelId', 'vehicleColorId', 'vehicleMakeId']);
    this.safeEntryRequestForm.addControl('contactList', this.formBuilder.array([]));
    let contactListArray = this.contactListFormArray;
    let contactListFormGroup = this.formBuilder.group({
      keyholderId: [null],
      customerNumber: [''],
      contactName: ['', [Validators.required]],
      contactNoCountryCode: ['+27', [Validators.required]],
      contactNumber: ['', [Validators.required]],
    });
    if (!this.SafeEntryRequestId) {
      contactListArray.push(contactListFormGroup);
    }
    this.disabledForm()
  }

  disabledForm() {
    if (this.isDisabled()) {
      this.safeEntryRequestForm.disable()
    } else {
      this.safeEntryRequestForm.enable()
    }
  }

  isDisabled() {
    return (!this.status || this.status == 'Pending' || this.status == 'Rescheduled') ? false : true
  }

  safeEntryRequestModel(contactInfo?: ContactListModel): FormGroup {
    let contactListModel = new ContactListModel(contactInfo ? contactInfo : undefined);
    let formControls = {};
    Object.keys(contactListModel).forEach((key) => {
      if (key === 'contactName' || key === 'contactNoCountryCode' || key === 'contactNumber') {
        formControls[key] = [{ value: contactListModel[key], disabled: this.SafeEntryRequestId ? true : false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: contactListModel[key], disabled: this.SafeEntryRequestId ? true : false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  openAddContact(val) {
    const contactData = {
      customerId: this.customerId,
      customerAddressId: this.addressId
    };
    this.isDuplicate = false;
    const dialogRef = this.dilog.open(AddContactDialogComponent, {
      width: '750px',
      data: contactData
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let contactListArray = this.contactListFormArray;
        const addObj: ContactListModel = {
          contactName: result.keyHolderName,
          customerNumber: result.contactNumber,
          keyholderId: null,
          contactNumber: result.contactNumber?.slice(4, 13),
          contactNoCountryCode: "+27",
        }
        this.contactListFormArray.value.forEach(el => {
          let _contact = `+27 ${el.contactNumber}`
          if (_contact === result.contactNumber) {
            this.isDuplicate = true;
          }
        });
        if (this.isDuplicate) {
          this.snackbarService.openSnackbar("Key Holder Contact number already exist", ResponseMessageTypes.WARNING);
          return;
        }
        this.customerKeyHolder.push({ customerName: addObj.contactName, keyholderId: null, contactNumber: addObj.contactNumber, contactNoCountryCode: '+27', mobile1: result.contactNumber });
        contactListArray.insert(0, this.safeEntryRequestModel(addObj));
        this.contactListFormArray.removeAt(1)
      }
    });
  }

  removecontact(i: number): void {
    // if (this.contactListFormArray.controls[i].value.safeEntryRequestContactId && this.contactListFormArray.length > 0) {
    //   let obj = {
    //     ids: this.contactListFormArray.controls[i].value.safeEntryRequestContactId,
    //     isDeleted: true,
    //     modifiedUserId: this.loggedUser.userId
    //   }
    //   this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.HOLIDAY_INSTRUCTION_CONTACTS,
    //     undefined,
    //     prepareRequiredHttpParams(obj)).subscribe((response: IApplicationResponse) => {
    //       if (response.isSuccess) {
    //         this.contactListFormArray.removeAt(i);
    //       }
    //       if (this.contactListFormArray.length === 0) {
    //         this.removecontact(i);
    //       };
    //     });
    // }
    // else if (this.contactListFormArray.length === 1) {
    //   this.snackbarService.openSnackbar("Atleast one item is required", ResponseMessageTypes.WARNING);
    //   return;
    // }
    // else {
    //   this.contactListFormArray.removeAt(i);
    // }
    this.contactListFormArray.reset();

  }

  public checkValidity(): void {
    Object.keys(this.safeEntryRequestForm.controls).forEach((key) => {
      this.safeEntryRequestForm.controls[key].markAsDirty();
    });
  }

  onSubmit(): void {

    if (this.safeEntryRequestForm.invalid) {
      this.checkValidity()
      return;
    }
    this.safeEntryRequestForm.getRawValue().contactList.forEach(element => {
      element.contactNumber = element.contactNumber.toString().replace(/\s/g, "");
    });
    let formValue = this.safeEntryRequestForm.value;
    if (!this.SafeEntryRequestId) {
      if (typeof formValue.vehicleMakeId === 'string') {

        formValue.vehicleMake = formValue.vehicleMakeId
        formValue.vehicleMakeId = null;
      }
      else {
        formValue.vehicleMakeId = formValue.vehicleMakeId.id ? formValue.vehicleMakeId.id : null;
      }
      if (typeof formValue.vehicleModelId === 'string') {

        formValue.vehicleModel = formValue.vehicleModelId
        formValue.vehicleModelId = null;
      }
      else {
        formValue.vehicleModelId = formValue.vehicleModelId.id ? formValue.vehicleModelId.id : null;
      }
      if (typeof formValue.vehicleColorId === 'string') {

        formValue.vehicleColor = formValue.vehicleColorId
        formValue.vehicleColorId = null;
      }
      else {
        formValue.vehicleColorId = formValue.vehicleColorId.id ? formValue.vehicleColorId.id : null;
      }
    }
    formValue.customerId = this.customerId;
    formValue.requestDate = this.momentService.localToUTC(this.safeEntryRequestForm.get('requestDate').value);
    if (typeof this.safeEntryRequestForm.get('requestTime').value !== 'string') {
      let requestTime = this.momentService.convertNormalToRailayTimeOnly(this.safeEntryRequestForm.get('requestTime').value)
      formValue.requestTime = requestTime;
    }
    let reschedulerequestObj = {};
    if (this.SafeEntryRequestId) {
      reschedulerequestObj = {
        customerSafeEntryRequestId: this.SafeEntryRequestId,
        reschedulerequestDate: formValue.requestDate,
        reschedulerequestTime: formValue.requestTime,
        modifiedUserId: this.loggedUser?.userId
      }
    }
    formValue.createdUserId = this.loggedUser.userId;
    formValue.customerAddressId = this.addressId
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = !this.SafeEntryRequestId
      ? this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SAFE_ENTRY_POST, formValue, 1)
      : this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SAFE_ENTRY_RESHEDULE, reschedulerequestObj, 1)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setViewCustomerData({
          customerId: this.customerId,
          addressId: this.addressId,
          customerTab: 0,
          monitoringTab: null,
        })
        this.rxjsService.navigateToViewCustomerPage();
      }
    });
  }

  getKeyHolders(form): Array<any> {
    return form.controls.contactList.controls;
  }

  isDuplicate = false;
  OnChange(index): boolean {
    if (this.contactListFormArray.length > 1) {
      this.isDuplicate = false;
      var cloneArray = this.contactListFormArray.value.slice();
      cloneArray.splice(index, 1);
      var findIndex = cloneArray.some(x => x.contactNumber === this.contactListFormArray.value[index].contactNumber);
      if (findIndex) {
        this.snackbarService.openSnackbar("Already contact number exist", ResponseMessageTypes.WARNING);
        this.isDuplicate = true;
        return false;
      } else {
        this.isDuplicate = false;
      }
    }
  }

  onChangeCustomer(event, index) {
    const ControlformArray = this.contactListFormArray
    let _keyHolder = this.customerKeyHolder.find(item => item.mobile1 == event.target.value);
    ControlformArray.controls[index].get('contactNumber').setValue(Number(_keyHolder.mobile1.substr(4, 11)));
    ControlformArray.controls[index].get('contactName').setValue(_keyHolder.customerName);
    ControlformArray.controls[index].get('keyholderId').setValue(_keyHolder.keyHolderId);
    ControlformArray.controls[index].get('contactNumber').setErrors(null);
  }

  createCancellationForm() {
    this.cancellationForm = this.formBuilder.group({
      reasonForCancel: ['', Validators.required],
      customerSafeEntryRequestId: ['', Validators.required],
      modifiedUserId: ['', Validators.required],
    });
  }

  cancellationSubmit(): void {
    this.cancellationForm.get("customerSafeEntryRequestId").setValue(this.SafeEntryRequestId)
    this.cancellationForm.get("modifiedUserId").setValue(this.loggedInUserData.userId)
    if (this.cancellationForm.invalid) {
      return;
    }
    let formValue = this.cancellationForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SAFE_ENTRY_CANCEL, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.cancelModel = false
        this.backPage();
      }
    });
  }

  backPage() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  cancellation() {
    this.cancelModel = true
  }

  createCancelForm(): void {
    this.requestForm = this.formBuilder.group({
      reschedulerequestDate: ['', Validators.required],
      reschedulerequestTime: ['', Validators.required]
    });
  }

}
