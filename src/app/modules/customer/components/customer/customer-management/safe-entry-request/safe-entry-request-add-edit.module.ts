import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { AddContactDialogModule } from "../add-contact-dialog/add-contact-dialog.module";
import { SafeEntryRequestAddEditComponent } from "./safe-entry-request-add-edit.component";
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';

@NgModule({
    declarations: [SafeEntryRequestAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    MessagesModule,
    MessageModule,
    LayoutModule,
    AddContactDialogModule,
    RouterModule.forChild([
        {
            path: '', component: SafeEntryRequestAddEditComponent, data: { title: 'Safe Entry Request add-edit' }
        },
    ])
  ],
  entryComponents: [],
  providers: []
})
export class SafeEntryRequestAddEditModule { }