import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { MessageModule } from "primeng/message";
import { MessagesModule } from "primeng/messages";
import { SafeEntryRequestListComponent } from "./safe-entry-request-list.component";


@NgModule({
    declarations: [SafeEntryRequestListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    MessageModule,
    MessagesModule,
  ],
  exports: [SafeEntryRequestListComponent],
  providers: []
})
export class SafeEntryRequestListModule { }