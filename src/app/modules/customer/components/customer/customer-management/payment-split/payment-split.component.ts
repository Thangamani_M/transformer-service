import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { DebtorSearchComponent } from '@modules/collection/components/credit-controller/bank-statement-uploads/debtor-search/debtor-search.component';
import { SplitPaymentAddEditModel, SplitPaymentDocumentsPostDTO, SplitPaymentsDebtors } from '@modules/customer';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/components/dynamicdialog/dialogservice';

@Component({
  selector: 'app-payment-split',
  templateUrl: './payment-split.component.html',
  styleUrls: ['./payment-split.component.scss']
})

export class PaymentSplitComponent implements OnInit {
  pageSize = 10;
  totalRecords = 0;
  selectedTabIndex = 0
  transactionDescriptionDropdown = []
  splitPaymentFrom: any
  splitPaymentDebtorDetail: any = {}
  outstandingBalance: any = {}
  primengTableConfigProperties: any = {
    tableCaption: "Payment Split",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Customer Management', relativeRouterUrl: '' }, { displayName: 'Customer List' }, { displayName: 'View Customer' }, { displayName: 'Payment Split' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Payment Split',
          dataKey: 'employeeId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: false,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'userId', header: 'User ID', width: '150px' },
            { field: 'userName', header: 'User Name', width: '150px' },
          ],
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_STAFF_REGISTRATION,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }
  customerId: string;
  debtorId: string;
  referenceId: string;
  addressId: string
  dataList = []
  columns = [
    { field: 'debtorRefNo', header: 'Debtor Code', width: '150px' },
    { field: 'debtorName', header: 'Debtor Name', width: '150px' },
    { field: 'splitAmount', header: 'Split Amount', width: '150px' },
  ];

  paymentSplitForm: FormGroup;
  splitPaymentsDebtors: FormArray;
  splitPaymentDocumentsPostDTO_1: FormArray;
  splitPaymentDocumentsPostDTO_2: FormArray;
  loggedUser: UserLogin
  debtorsList: any = []
  documentName = ""
  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private dialogService: DialogService, private formBuilder: FormBuilder, private store: Store<AppState>,
    private snackbarService: SnackbarService,private router : Router) {
    activatedRoute.queryParams.subscribe(param => {
      this.customerId = param['customerId']
      this.debtorId = param['debtorId']
      this.referenceId = param['referenceId']
      this.addressId = param['addressId']
    })
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }


  ngOnInit(): void {
    this.createPaymentSplitForm()
    this.getDetails()
  }

  getDetails() {
    let object = {
      customerId: this.customerId,
      debtorId: this.debtorId,
      referenceId: this.referenceId,
    }
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SPLIT_PAYMENTS, undefined, false, prepareRequiredHttpParams(object)).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.transactionDescriptionDropdown = response.resources.transactionDescription
        this.splitPaymentFrom = response.resources.splitPaymentFrom
        this.splitPaymentDebtorDetail = response.resources.splitPaymentDebtorDetail
        this.outstandingBalance = response.resources.outstandingBalance
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }

  calculatRemainingAmount (){
    let totalAmount = 0
    this.debtorsList.forEach(item=>{
      totalAmount += Number(item.splitAmount)
    })

    this.splitPaymentFrom.remainingAmount =  Number(this.splitPaymentFrom?.originalAmount)-Number(totalAmount)


  }


  openDoaSearchModel(event?) {
    if(this.debtorsList.length !=0 && this.paymentSplitForm.get("isSingle").value) {
      return this.snackbarService.openSnackbar("Cannot able to add more then one debtor", ResponseMessageTypes.WARNING)
    }

    const ref = this.dialogService.open(DebtorSearchComponent, {
      showHeader: true,
      header: `Search Debtor`,
      baseZIndex: 10000,
      width: '1200px',
      data: {  },
      dismissableMask: true
    });
    ref.onClose.subscribe((resp) => {
      if (resp?.debtorId) {
        this.getDebtores(resp)
      }
    })
  }

  getDebtores(debtorData) {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SPLIT_PAYMENTS, debtorData.debtorId).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.debtorsList.push({ balanceInfo: [response.resources], ...debtorData, splitAmount: 0 });

      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  createPaymentSplitForm() {
    let formModel = new SplitPaymentAddEditModel()
    this.paymentSplitForm = this.formBuilder.group({});
    Object.keys(formModel).forEach((key) => {
      if (key != "splitPaymentsDebtors") {
        this.paymentSplitForm.addControl(key, new FormControl(formModel[key]));
      }
    });

    this.paymentSplitForm.addControl("splitPaymentsDebtors", this.formBuilder.array([]));
    this.paymentSplitForm.addControl("splitPaymentDocumentsPostDTO_1", this.formBuilder.array([]));
    this.paymentSplitForm.addControl("splitPaymentDocumentsPostDTO_2", this.formBuilder.array([]));

    this.splitPaymentsDebtors = this.getsplitDebtorsFormArray
    this.splitPaymentsDebtors.push(this.createSplitDebtorsFormArray());

    this.splitPaymentDocumentsPostDTO_1 = this.getsplitPaymentDocumentsFormArray_1
    this.splitPaymentDocumentsPostDTO_1.push(this.createSplitDocumentsFormArray({}, 'isRequired'));

    this.splitPaymentDocumentsPostDTO_2 = this.getsplitPaymentDocumentsFormArray_2
    this.splitPaymentDocumentsPostDTO_2.push(this.createSplitDocumentsFormArray());
    this.paymentSplitForm = setRequiredValidator(this.paymentSplitForm, ["invoiceTransactionDescriptionId", "debtorFromId", "referenceId"]);

    this.paymentSplitForm.get("customerId").setValue(this.customerId)
    this.paymentSplitForm.get("referenceId").setValue(this.referenceId)
    this.paymentSplitForm.get("debtorFromId").setValue(this.debtorId)
    this.paymentSplitForm.get("createdUserId").setValue(this.loggedUser?.userId)
  }


  get getsplitDebtorsFormArray(): FormArray {
    if (!this.paymentSplitForm) return;
    return this.paymentSplitForm.get("splitPaymentsDebtors") as FormArray;
  }
  get getsplitPaymentDocumentsFormArray_1(): FormArray {
    if (!this.paymentSplitForm) return;
    return this.paymentSplitForm.get("splitPaymentDocumentsPostDTO_1") as FormArray;
  }
  get getsplitPaymentDocumentsFormArray_2(): FormArray {
    if (!this.paymentSplitForm) return;
    return this.paymentSplitForm.get("splitPaymentDocumentsPostDTO_2") as FormArray;
  }

  createSplitDebtorsFormArray(debtors?: SplitPaymentsDebtors): FormGroup {
    let configFeeModel = new SplitPaymentsDebtors(debtors);
    let formControls = {};
    Object.keys(configFeeModel).forEach((key) => {
      formControls[key] = new FormControl(configFeeModel[key], [Validators.required]);
    });
    return this.formBuilder.group(formControls);
  }
  createSplitDocumentsFormArray(documents?: SplitPaymentDocumentsPostDTO, isRequired?: string): FormGroup {
    let configFeeModel = new SplitPaymentDocumentsPostDTO(documents);
    let formControls = {};
    Object.keys(configFeeModel).forEach((key) => {
      if (isRequired == "isRequired" && key!="invoiceTransactionDescriptionId") {
        formControls[key] = new FormControl(configFeeModel[key], [Validators.required]);
      } else {
        formControls[key] = new FormControl(configFeeModel[key], []);

      }
    });
    formControls['documentName']=this.documentName
    return this.formBuilder.group(formControls);
  }

  addFiles(type) {
    if (type == "required") {
      this.splitPaymentDocumentsPostDTO_1 = this.getsplitPaymentDocumentsFormArray_1
      this.splitPaymentDocumentsPostDTO_1.push(this.createSplitDocumentsFormArray({}, 'isRequired'));
    } else {
      this.splitPaymentDocumentsPostDTO_2 = this.getsplitPaymentDocumentsFormArray_2
      this.splitPaymentDocumentsPostDTO_2.push(this.createSplitDocumentsFormArray());
    }
  }

  uploadFiles(event, type, index) {
    let formdata = new FormData()
    formdata.append('file', event.target.files[0]);
    this.crudService.fileUpload(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_NOTES_UPLOAD, formdata).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {

        if (type == "required") {
          this.getsplitPaymentDocumentsFormArray_1.controls[index].get('documentPath').setValue(response.resources);
          this.getsplitPaymentDocumentsFormArray_1.controls[index].get('isRequired').setValue(true);
          this.getsplitPaymentDocumentsFormArray_1.controls[index].get('fileName').setValue(event.target.files[0].name);
        } else {
          this.getsplitPaymentDocumentsFormArray_2.controls[index].get('documentPath').setValue(response.resources);
          this.getsplitPaymentDocumentsFormArray_2.controls[index].get('isRequired').setValue(true);
          this.getsplitPaymentDocumentsFormArray_2.controls[index].get('fileName').setValue(event.target.files[0].name);

        }

      }
    })
  }
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0)
    }
  }

  onSubmit() {
    let totalSplitAmount = 0;

    let obj = this.paymentSplitForm.getRawValue();

    let splitPaymentDocumentsPostDTO = []

    for (const document of obj.splitPaymentDocumentsPostDTO_1) {
      splitPaymentDocumentsPostDTO.push({
        ...document,
        invoiceTransactionDescriptionId: obj.invoiceTransactionDescriptionId,

      })
    }

    for (const document of obj.splitPaymentDocumentsPostDTO_2) {
      if (document.documentPath) {
        splitPaymentDocumentsPostDTO.push({
          ...document,
          invoiceTransactionDescriptionId: obj.invoiceTransactionDescriptionId,

        })
      }
    }
    this.clearFormArray(this.splitPaymentsDebtors)
    for (const debtor of this.debtorsList) {
      let obj = {
        debtorToId: debtor.debtorId,
        splitPaymentAmount: debtor.splitAmount,
        debtorFromId: this.debtorId
      }
      totalSplitAmount += Number(debtor.splitAmount);
      this.splitPaymentsDebtors = this.getsplitDebtorsFormArray
      this.splitPaymentsDebtors.push(this.createSplitDebtorsFormArray(obj));
    }
    if(this.paymentSplitForm.invalid) return "";

    let latestObj = this.paymentSplitForm.getRawValue();

    if(totalSplitAmount > this.splitPaymentFrom.originalAmount){
      return this.snackbarService.openSnackbar("Split amout should be lessthen original amount", ResponseMessageTypes.WARNING)
    }

    let reqObject = {
      customerId: this.customerId,
      isSingle: latestObj.isSingle,
      notes: latestObj.notes,
      debtorFromId: this.debtorId,
      createdUserId: latestObj.createdUserId,
      referenceId: this.referenceId,
      splitPaymentsDebtors: latestObj.splitPaymentsDebtors,
      splitPaymentDocumentsPostDTO: splitPaymentDocumentsPostDTO
    }

    this.crudService.create(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SPLIT_PAYMENTS, reqObject).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goBack()
      }
    })
  }
  onChangeInvoiceDescription(){

    let descriptionID = this.paymentSplitForm.get("invoiceTransactionDescriptionId").value;

    let find = this.transactionDescriptionDropdown.find(item=> item.id == descriptionID);

    if(find){
      this.documentName =  find.displayName
    this.getsplitPaymentDocumentsFormArray_1.controls.forEach(control=>{
      control.get('documentName').setValue(find?.displayName)
    })
    this.getsplitPaymentDocumentsFormArray_2.controls.forEach(control=>{
      control.get('documentName').setValue(find?.displayName)
    })
  }


  }
  goBack() {
    this.router.navigateByUrl(`/customer/manage-customers/view-transactions?id=${this.customerId}&debtorId=${this.debtorId}&addressId=${this.addressId}`)
  }
}
