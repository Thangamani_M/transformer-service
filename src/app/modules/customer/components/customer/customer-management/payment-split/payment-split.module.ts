import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { BankStatementUploadsModule } from '@modules/collection/components/credit-controller/bank-statement-uploads/bank-statement-uploads.module';
import { ApproveSplitPaymentsComponent } from './approve-split-payments/approve-split-payments.component';
import { PaymentSplitRoutingModule } from './payment-split-routing.module';
import { PaymentSplitComponent } from './payment-split.component';



@NgModule({
  declarations: [PaymentSplitComponent,ApproveSplitPaymentsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    PaymentSplitRoutingModule,
    BankStatementUploadsModule
  ]
})
export class PaymentSplitModule { }
