import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApproveSplitPaymentsComponent } from './approve-split-payments/approve-split-payments.component';

import { PaymentSplitComponent } from './payment-split.component';

const routes: Routes = [
  { path: '', component: PaymentSplitComponent , data:{title:"Payment Split"}},
  { path: 'approve', component: ApproveSplitPaymentsComponent , data:{title:"Approve Split Payments"}},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class PaymentSplitRoutingModule { }
