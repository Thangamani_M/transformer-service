
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { CollectionModuleApiSuffixModels } from '@modules/collection';
import { SplitPaymentAddEditModel } from '@modules/customer';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';

@Component({
  templateUrl: './approve-split-payments.component.html',
  styleUrls: ['../payment-split.component.scss']
})
export class ApproveSplitPaymentsComponent implements OnInit {

  selectedTabIndex = 0;
  totalRecords:any=0;
  transactionDescriptionDropdown = []
  splitPaymentFrom: any
  splitPaymentDebtorDetail: any = {}
  outstandingBalance: any = {}
  primengTableConfigProperties: any = {
    tableCaption: "Transaction Process",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'My Task List', relativeRouterUrl: '' }, { displayName: 'DOA dashboard' , relativeRouterUrl:'/collection/my-task-list' }, { displayName: 'Payment Split Approve/Decline' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Transaction Process',
          dataKey: 'debtorId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          enableAction: true,
          enableAddActionBtn: false,
          enableMultiDeleteActionBtn: false,
          enableStatusActiveAction: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          shouldShowDeleteActionBtn: true,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: true,
          isDateWithTimeRequired: true,
          apiSuffixModel: CollectionModuleApiSuffixModels.CREDIT_CONTROLLER_STAFF_REGISTRATION,
          moduleName: ModulesBasedApiSuffix.COLLECTIONS,
        }]
    }
  }
  paymentSplitForm: FormGroup;
  isChecked = "true"
  approveDialog = false;
  declineDialog = false;
  notes = ""
  reasonList = []
  approveForm: FormGroup
  declineForm: FormGroup
  formData = new FormData();
  loggedUser: UserLogin
  debtorsList: any = []
  transacionId: string
  splitPaymentDocuments = []
  splitPaymentDocuments1 = []
  columns = [
    { field: 'debtorCode', header: 'Debtor Code', width: '150px' },
    { field: 'debtorName', header: 'Debtor Name', width: '150px' },
    { field: 'splitPaymentAmount', header: 'Split Amount', width: '150px' },
  ];
  constructor(private crudService: CrudService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private store: Store<AppState>,
  private router: Router) {
    activatedRoute.queryParams.subscribe(param => {
      this.transacionId = param['transactionId']
    })
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }


  ngOnInit(): void {
    this.createPaymentSplitForm()
    this.getTransactionDetails()
    // this.getDetails()
    this.getReason()
    this.createTransactionProcessDeclineForm()
    this.createTransactionProcessApproveForm()
  }

  getReason() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_DECLINE_REASON)
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.reasonList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTransactionDetails() {
    this.crudService.get(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.SPLIT_PAYMENTS_DOA, this.transacionId).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {

        this.transactionDescriptionDropdown = response.resources.transactionDescription
        this.splitPaymentFrom = response.resources.splitPaymentFrom
        this.splitPaymentDebtorDetail = response.resources.splitPaymentDebtorDetail
        this.outstandingBalance = response.resources.outstandingBalance
        this.debtorsList = response.resources.splitPaymentTos

        for (const document of response.resources.splitPaymentDocuments) {
          if (document.isRequired) {
            this.splitPaymentDocuments.push(document)
          } else {
            this.splitPaymentDocuments1.push(document)

          }
        }
        this.paymentSplitForm.patchValue({
          isSingle: this.debtorsList.length == 0 ? true : false,
          notes: this.splitPaymentDebtorDetail.notes,
          invoiceTransactionDescriptionId: this.splitPaymentDebtorDetail.invoiceTransactionDescriptionId,
        })
      }
      this.rxjsService.setGlobalLoaderProperty(false)
    })
  }

  createPaymentSplitForm() {
    let formModel = new SplitPaymentAddEditModel()
    this.paymentSplitForm = this.formBuilder.group({});
    Object.keys(formModel).forEach((key) => {
      if (key != "splitPaymentsDebtors") {
        this.paymentSplitForm.addControl(key, new FormControl(formModel[key]));
      }
    });
  }




  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0)
    }
  }


  goBack() {
    this.router.navigate(['/collection/my-task-list']);
  }


  uploadFiles(event, type) {
    if (type == "approve") {
      this.approveForm.get("fileName").setValue(event.target.files[0].name)
    } else {
      this.declineForm.get("fileName").setValue(event.target.files[0].name)
    }
    // this.fileName = event.target.files[0].name
    this.formData.append('file', event.target.files[0]);
  }

  openApproveDialog() {
    this.formData = new FormData()
    this.approveDialog = true
    this.declineDialog = false

  }

  openDeclineDialog() {
    this.formData = new FormData()
    this.declineDialog = true
    this.approveDialog = false

  }

  createTransactionProcessApproveForm(): void {
    this.approveForm = new FormGroup({
      'comments': new FormControl(null),
      'fileName': new FormControl(null),
    });
    this.approveForm = setRequiredValidator(this.approveForm, ['comments']);
  }
  createTransactionProcessDeclineForm(): void {
    this.declineForm = new FormGroup({
      'declineReasonId': new FormControl(null),
      'comments': new FormControl(null),
      'fileName': new FormControl(null),
    });
    this.declineForm = setRequiredValidator(this.declineForm, ['comments', 'declineReasonId']);
  }

  submitAction(type) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    let finalOjb = {
      roleId: this.loggedUser.roleId,
      approvedUserId: this.loggedUser.userId,
      comments: this.approveForm.value.comments,
      referenceNo: this.transacionId,
      declineReasonId: this.declineForm.get("declineReasonId").value
  }
    if (type == "approve") {
      if (this.approveForm.invalid) return "";
      finalOjb.comments= this.approveForm.value.comments;
      delete finalOjb.declineReasonId;
    } else {
      if (this.declineForm.invalid) return "";
      finalOjb.comments= this.declineForm.value.comments;
    }

    this.formData.append('data', JSON.stringify(finalOjb));

    this.crudService.update(ModulesBasedApiSuffix.COLLECTIONS, CollectionModuleApiSuffixModels.CREDIT_NOTES_DOA_APPROVAL, this.formData).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {
        this.goBack()
      }
    })
  }

  closeApproveDialog() {
    this.approveDialog = false
  }
  closeDeclineDialog() {
    this.declineDialog = false

  }
}
