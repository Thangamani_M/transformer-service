import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatTabChangeEvent, MAT_DIALOG_DATA } from '@angular/material';
import { CustomerAddress, CustomerAddressFormModel } from '@app/modules/customer/models';
import { CustomDirectiveConfig, formConfigs, setRequiredValidator } from '@app/shared';
import { AlertService, RxjsService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { CustomerManagementService } from '../../../../services/customermanagement.services';
@Component({
  selector: 'customer-address-add',
  templateUrl: './customer-address-add.component.html',
  styleUrls: ['./customer-address-add.component.scss']
})
export class CustomerAddressAddComponent implements OnInit {
  @Input() id: string;
  customerAddressAddEditForm: FormGroup;
  formConfigs = formConfigs;
  applicationResponse: IApplicationResponse;
  customerAddress = new CustomerAddress();
  errorMessage: string;
  btnName: string;
  @Output() selectedTabChange: EventEmitter<MatTabChangeEvent>;
  provinceList: any;
  countryList: any;
  cityList: any;
  provinceid: number;
  cntryId: number;
  tmpStateid: number;
  tmpCntryId: number;
  regionid: number;
  regionList: any;
  suburbList: any;
  addressTypeList: any;
  tmpcustomerId: any;
  streetTypeList: any;
  ddlCnfilter: any;
  // enabledisableprovince:boolean=true;
  enabledisablecity: boolean = true;
  enabledisablesuburb: boolean = true;
  firstName: string;
  tmpMessage: string;

  isDialogLoaderLoading = true;
  isADependentDropdownSelectionChanged = false;
  isNmberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  alphanumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  constructor(@Inject(MAT_DIALOG_DATA) public data: CustomerAddress,

    private dialog: MatDialog,
    private customerManagementService: CustomerManagementService,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private rxjsService: RxjsService
  ) {

  }
  ngOnInit(): void {
    this.createAddressForm();
    this.rxjsService.setDialogOpenProperty(true);


    if (this.data["Name"] == "Create") {
      this.tmpcustomerId = this.data["Id"];
    }
    else {
      this.data.customerAddressId = this.data["Id"];
      this.data.customerId = this.data["CustId"]
    }

    if (this.data.customerAddressId) {
      this.btnName = 'Update';
      this.enabledisablecity = false;
      this.enabledisablesuburb = false;
    }
    else {
      this.btnName = 'Create';
      this.enabledisablecity = true;
      this.enabledisablesuburb = true;
    }

    this.bindingAddressTypesDropdown();
    this.bindingCountryDropdown();
    this.bindingStreetTypes();

    if (this.data.customerAddressId != null) {
      this.countryList = [0];

      this.customerManagementService.getCustomerAddress(this.data.customerAddressId).subscribe(
        {
          next: response => {
            this.applicationResponse = response;
            this.tmpMessage = this.applicationResponse.message;
            this.customerAddress = this.applicationResponse.resources;
            this.customerAddressAddEditForm = this.formBuilder.group({
              customerAddressId: this.customerAddress.customerAddressId,
              addressTypeId: this.customerAddress.addressTypeId,
              countryId: this.customerAddress.countryId,
              customerId: this.customerAddress.customerId,
              streetNumber: this.customerAddress.streetNumber,
              streetName: this.customerAddress.streetName,
              suburbId: this.customerAddress.suburbId,
              cityId: this.customerAddress.cityId,
              regionId: this.customerAddress.regionId,
              provinceId: this.customerAddress.provinceId,
              tierId: this.customerAddress.tierId,
              postalCode: this.customerAddress.postalCode,
              buildingNumber: this.customerAddress.buildingNumber,
              buildingName: this.customerAddress.buildingName,
              roadName: this.customerAddress.roadName,
              suburbName: this.customerAddress.suburbName,
              countryName: this.customerAddress.countryName,
              cityName: this.customerAddress.cityName,
              regionName: this.customerAddress.regionName,
              provinceName: this.customerAddress.provinceName,
              addressStatusId: this.customerAddress.addressStatusId,
              streetTypeId: this.customerAddress.streetTypeId,
              isDefault: this.customerAddress.isDefault,
            });
            this.bindingProvincesDropdown(this.customerAddress.countryId);
            this.bindingCityDrodown(this.customerAddress.provinceId);
            this.bindingSuburbsDrodown(this.customerAddress.cityId);
            this.isDialogLoaderLoading = false;
            this.isADependentDropdownSelectionChanged = false;

            if (this.tmpMessage == "Record Not Found") {
              this.dialog.closeAll();
              this.isDialogLoaderLoading = false;
              this.isADependentDropdownSelectionChanged = false;
              //this.toastr.responseMessage(this.applicationResponse.resources.message);
            }
          }
        });
    }
  }


  createAddressForm(): void {
    let customerAddressFormModel = new CustomerAddressFormModel();
    this.customerAddressAddEditForm = this.formBuilder.group({});
    Object.keys(customerAddressFormModel).forEach((key) => {
      this.customerAddressAddEditForm.addControl(key, new FormControl(customerAddressFormModel[key]));
    });
    this.customerAddressAddEditForm = setRequiredValidator(this.customerAddressAddEditForm, ["addressTypeId", "streetNumber", "streetTypeId", "countryId", "provinceId", "cityId", "suburbId"]);
  }
  //==============Binding Country Dropdown================//
  //Bind country dropdown values...
  private bindingCountryDropdown() {
    this.customerManagementService.getCountryDropdown().subscribe(
      {
        next: response => {
          this.applicationResponse = response;
          this.countryList = this.applicationResponse.resources;
          this.ddlCnfilter = this.countryList.find(({ displayName }) => displayName === 'South Africa').id;
          this.loadRegionsbyCountries(this.ddlCnfilter);
          this.isDialogLoaderLoading = false;
          this.isADependentDropdownSelectionChanged = false;
        }
      })
  }
  public loadRegionsbyCountries(cntryId) {
    if (cntryId != "") {
      this.provinceList = [0];
      let stringToSplit = cntryId;
      let x = stringToSplit.split(": ");
      //this.bindingRegionDrodown(x);
      this.bindingProvincesDropdown(x);
    }
  }
  //Bind state dropdown values...
  private bindingProvincesDropdown(cntryId) {
    this.customerManagementService.getProvincesDropdown(cntryId).subscribe(
      {
        next: response => {
          this.applicationResponse = response;
          this.provinceList = this.applicationResponse.resources;
        }
      });
  }
  public loadCitiesbyProvinces(provinceId) {
    this.enabledisablecity = false;
    this.cityList = [0];
    let stringToSplit = provinceId;
    let x = stringToSplit.split(": ");
    this.bindingCityDrodown(x);
  }
  //Bind state dropdown values...
  private bindingCityDrodown(provinceId) {
    this.customerManagementService.getCityDropdown(provinceId).subscribe(
      {
        next: response => {
          this.applicationResponse = response;
          this.cityList = this.applicationResponse.resources;
        }
      });
  }
  public loadSuburbsbyCities(cityId) {
    this.enabledisablesuburb = false;
    this.suburbList = [0];
    let stringToSplit = cityId;
    let x = stringToSplit.split(": ");
    this.bindingSuburbsDrodown(x);
  }
  //Bind state dropdown values...
  private bindingSuburbsDrodown(cityId) {
    this.customerManagementService.getSuburbDropdown(cityId).subscribe(
      {
        next: response => {
          this.applicationResponse = response;
          this.suburbList = this.applicationResponse.resources;
        }
      });
  }
  //Bind state dropdown values...
  private bindingAddressTypesDropdown() {
    this.customerManagementService.getAddressTypesDropdown().subscribe(
      {
        next: response => {
          this.applicationResponse = response;
          this.addressTypeList = this.applicationResponse.resources;
        }
      });
  }
  private bindingStreetTypes() {
    this.customerManagementService.getStreetTypeDropdown().subscribe(
      {
        next: response => {
          this.applicationResponse = response;
          this.streetTypeList = this.applicationResponse.resources;
        }
      });
  }

  //Insert/Update new city record...
  SaveCustomerAddress(): void {
    if (this.customerAddressAddEditForm.invalid) {
      this.customerAddressAddEditForm.markAllAsTouched();
      return;
    }
    const customerAddress = { ...this.customerAddress, ...this.customerAddressAddEditForm.value }

    customerAddress.regionId = null;
    customerAddress.addressStatusId = "FCE6C5F2-8994-4B87-B361-49E333295705";

    if (customerAddress.customerAddressId != "") {

      //Update city values...
      this.customerManagementService.UpdateCustomerAddress(customerAddress).subscribe({
        next: response => this.onSaveComplete(response),
        error: err => this.errorMessage = err
      });
    }
    else {

      customerAddress.customerId = this.tmpcustomerId;

      if (customerAddress.isDefault == "") {
        customerAddress.isDefault = false;
      }

      //Insert city values...
      this.customerManagementService.SaveCustomerAddress(customerAddress).subscribe({
        next: response => this.onSaveComplete(response),
        error: err => this.errorMessage = err
      });
    }

  }
  //After save completed...
  onSaveComplete(response): void {
    if (response.statusCode == 200) {
      this.dialog.closeAll();
      // //this.toastr.responseMessage(response);
      // Reset the form to clear the flags
      this.customerAddressAddEditForm.reset();

    }
    else {
      //Display Alert message
      this.alertService.processAlert(
        response,
      );

    }

    this.isDialogLoaderLoading = false;
    this.isADependentDropdownSelectionChanged = false;
  }

  //Numeric Only Function
  numericOnly(event): boolean {
    let patt = /^([0-9])$/;
    let result = patt.test(event.key);
    return result;
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }

  private checkIfSpaceonEmpty(e) {
    if (e.which === 32 && !this.customerAddressAddEditForm.controls['streetName'].value)
      e.preventDefault();
  }
}
