import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CustomerAddressAddComponent } from "./customer-address-add.component";


@NgModule({
    declarations: [CustomerAddressAddComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  entryComponents: [CustomerAddressAddComponent],
  exports: [CustomerAddressAddComponent],
  providers: []
})
export class CustomerAddressAddModule { }