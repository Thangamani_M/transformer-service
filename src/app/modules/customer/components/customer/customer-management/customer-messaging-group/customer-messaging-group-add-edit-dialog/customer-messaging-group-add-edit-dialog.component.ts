
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-customer-messaging-group-add-edit-dialog',
  templateUrl: './customer-messaging-group-add-edit-dialog.component.html'
})
export class CustomerMessagingGroupAddEditDialogComponent implements OnInit {

  data: any = [];
  messagingGroupForm: FormGroup;
  callDetailsData: any;
  loggedUser: any;
  contactList: any = [];
  contactNumberList: any = [];
  alarmTypeList: any = [];
  countrycode: any;
  contactNo: any;
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  @ViewChild("input", { static: false }) row;
  isAValidPhoneNumberOnly = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(public ref: DynamicDialogRef, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, public _fb: FormBuilder, public config: DynamicDialogConfig, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }


  ngOnInit(): void {
    this.createForm();
    this.onLoadValue();
    if (this.config?.data?.editableObject?.customerMessagingGroupId) {
      this.getMessagingGroupById();
    }
  }

  onLoadValue() {
    let queryParams = { customerId: this.config?.data?.customerId, customerAddressId: this.config?.data?.addressId };
    if (this.config?.data?.partitionId) {
      queryParams['partitionId'] = this.config?.data?.partitionId;
    }
    let dropdownsAndData = [
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.CUSTOMER_KEY_HOLDER, undefined, null, prepareGetRequestHttpParams(null, null, queryParams)),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ALARM_GROUP)

    ];
    this.loadDropdownData(dropdownsAndData);
  }



  createForm(): void {
    this.messagingGroupForm = this._fb.group({
      customerMessagingGroupId: [this.config?.data?.editableObject?.customerMessagingGroupId ? this.config?.data?.editableObject?.customerMessagingGroupId : null],
      customerId: [this.config?.data?.customerId, Validators.required],
      customerAddressId: [this.config?.data?.addressId, Validators.required],
      keyHolderId: ['', Validators.required],
      contactName: [''],
      contactNo: ['', Validators.required],
      contactNumber: [''],
      customerMessagingAlarmGroupsList: ['', Validators.required],
      isActive: [true],
      contactNumberCountryCode: ['+27',],
      createdUserId: [''],
      modifiedUserId: [''],
      partitionId: [],
    });
    this.messagingGroupForm = setRequiredValidator(this.messagingGroupForm, ["keyHolderId", "contactNo", "customerMessagingAlarmGroupsList"]);
    this.patchValue();
    this.onValueChanges();
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.messagingGroupForm
          .get("contactNo")
          .setValidators([
            Validators.minLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
            Validators.maxLength(
              formConfigs.southAfricanContactNumberMaxLength
            ),
          ]);
        break;
      default:
        this.messagingGroupForm
          .get("contactNo")
          .setValidators([
            Validators.minLength(formConfigs.indianContactNumberMaxLength),
            Validators.maxLength(formConfigs.indianContactNumberMaxLength),
          ]);
        break;
    }
  };

  onValueChanges() {

    this.messagingGroupForm
    .get("contactNumberCountryCode")
    .valueChanges.subscribe((contactNumberCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(contactNumberCountryCode);
      setTimeout(() => {
        this.row.nativeElement.focus();
        this.row.nativeElement.blur();
      });
    });

    this.messagingGroupForm
      .get("contactNo")
      .valueChanges.subscribe((contactNo: string) => {
        this.setPhoneNumberLengthByCountryCode(
          this.messagingGroupForm.get("contactNumberCountryCode").value
        );
      });

    this.messagingGroupForm.get('keyHolderId').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      let contact = this.contactList.find(x => x.id == val)
      if (contact) {
        this.messagingGroupForm.get('contactName').setValue(contact?.displayName ? contact?.displayName : null)
        this.contactNumberList = []
        if (contact.mobile1) {
          this.contactNumberList.push({ id: contact.mobile1, value: contact.mobile1 })
        }
        if (contact.mobile2) {
          this.contactNumberList.push({ id: contact.mobile2, value: contact.mobile2 })
        }
        if (contact.officeNo) {
          this.contactNumberList.push({ id: contact.officeNo, value: contact.officeNo })
        }
        if (contact.premisesNo) {
          this.contactNumberList.push({ id: contact.premisesNo, value: contact.premisesNo })
        }
        if (contact.contactNo) { //for new contact
          // this.contactNumberList.push({ id: contact.contactNo, value:contact.contactNoCountryCode+' '+contact.contactNo })
          this.contactNo = contact.contactNo;
          // this.countrycode = contact.contactNoCountryCode
          this.messagingGroupForm.get('contactNo').setValue(contact.contactNo)
        }

      }
    })
  }

  patchValue(response?: any) {
    if (response?.resources) {
      this.messagingGroupForm.patchValue(response.resources);
      this.messagingGroupForm.get('contactNumber').setValue(response.resources.contactNumber);
      this.messagingGroupForm.get('contactNo').setValue(response.resources.contactNumber);
    }
    this.messagingGroupForm.get('createdUserId').setValue(this.loggedUser.userId);
    this.messagingGroupForm.get('modifiedUserId').setValue(this.loggedUser.userId);
    this.messagingGroupForm.get('partitionId').setValue(this.config?.data?.partitionId);
  }

  loadDropdownData(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.contactList = resp.resources;
              break;
            case 1:
              this.alarmTypeList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getMessagingGroupById() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_MESSAGING_GROUP,
      this.config?.data?.editableObject?.customerMessagingGroupId)
      .subscribe(response => {
        // response.resources.contactNo = response.resources.contactNumberCountryCode + ' ' + response.resources.contactNumber
        if (response?.isSuccess && response?.statusCode == 200) {
          response.resources.customerMessagingAlarmGroupsList.forEach(element => {
            element.id = element.alarmGroupId
          });
          this.callDetailsData = response.resources;
          this.patchValue(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  btnCloseClick() {
    this.rxjsService.setDialogOpenProperty(false);
    this.ref.close(false);
  }
  onChangeName(val) {
    //  this.messagingGroupForm.get('contactNo').setValue(null)
  }

  onSubmit() {
    if (this.messagingGroupForm.invalid) {
      Object.keys(this.messagingGroupForm.controls).forEach((key) => {
        this.messagingGroupForm.controls[key].markAsDirty();
      });
      return
    }
    let formValue = this.messagingGroupForm.value
    formValue.contactNumber = this.contactNo
    formValue.customerMessagingAlarmGroupsList.forEach(element => {
      element.alarmGroupId = element.id
    });
    delete formValue.contactNo
    let crudService = formValue.customerMessagingGroupId ? this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_MESSAGING_GROUP, this.messagingGroupForm.value) : this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_MESSAGING_GROUP, this.messagingGroupForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.ref.close(true);
      }
    });

  }

  close() {
    this.ref.close();
  }
}



