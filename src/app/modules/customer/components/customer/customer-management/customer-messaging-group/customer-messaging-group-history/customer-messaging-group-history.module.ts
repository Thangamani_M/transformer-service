import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerMessagingGroupHistoryComponent } from "./customer-messaging-group-history.component";

@NgModule({
  declarations: [CustomerMessagingGroupHistoryComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [CustomerMessagingGroupHistoryComponent],
  entryComponents: [],
})
export class CustomerMessagingGroupHistoryModule { }