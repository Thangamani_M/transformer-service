import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerMessagingGroupAddEditDialogModule } from "../customer-messaging-group-add-edit-dialog/customer-messaging-group-add-edit-dialog.module";
import { CustomerMessagingGroupListComponent } from "./customer-messaging-group-list.component";

@NgModule({
  declarations: [CustomerMessagingGroupListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    CustomerMessagingGroupAddEditDialogModule,
  ],
  exports: [CustomerMessagingGroupListComponent],
  entryComponents: [],
})
export class CustomerMessagingGroupListModule { }