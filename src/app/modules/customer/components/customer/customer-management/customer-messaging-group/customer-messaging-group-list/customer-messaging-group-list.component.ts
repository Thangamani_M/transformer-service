import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { DialogService } from 'primeng/api';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { CustomerMessagingGroupAddEditDialogComponent } from '../customer-messaging-group-add-edit-dialog/customer-messaging-group-add-edit-dialog.component';
@Component({
  selector: 'app-customer-messaging-group-list',
  templateUrl: './customer-messaging-group-list.component.html'
})
export class CustomerMessagingGroupListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  @Input() partitionId;
  @Input() customerAddressId;
  @Input() customerId;
  @Input() permission;

  constructor(private rxjsService: RxjsService, private datePipe: DatePipe,
    private crudService: CrudService, private momentService: MomentService, public dialogService: DialogService,
    private snackbarService : SnackbarService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' },],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Messaging Group',
            dataKey: 'customerMessagingGroupId',
            captionFontSize: '21px',
            enableBreadCrumb: false,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'contactName', header: 'Contact Name', width: '200px' },
              { field: 'contactNo', header: 'Contact Number', width: '200px' },
              { field: 'customerMessagingAlarmGroups', header: 'Alarm Group', width: '200px' },
              { field: 'createdDate', header: 'Created Date', width: '200px' },
              { field: 'isActive', header: 'Status', width: '200px' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOMER_MESSAGING_GROUP,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    // this.rxjsService.getCustomerAddresId().subscribe(customerAddressId => {
    //   this.customerAddressId = customerAddressId

    //   this.rxjsService.getCustomerDate().subscribe(customer => {
    //     this.customerId = customer.customerId

    //     let otherParams = {}
    //     this.getRequiredListData(null, null, otherParams);
    //   })
    // })
    // this.getRequiredListData();
    console.log("permission",this.permission)
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      let otherParams = {}
      if (this.customerId && this.customerAddressId) {
        this.getRequiredListData(null, null, otherParams);
      }
      if (changes?.partitionId?.currentValue != changes?.partitionId?.previousValue) {
        this.partitionId = changes?.partitionId?.currentValue;
      }
    }

  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams = { ...otherParams, customerId: this.customerId, IsAll: true, CustomerAddressId: this.customerAddressId}
    if (this.partitionId) {
      otherParams['partitionId'] = this.partitionId;
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.contactNo = val.contactNumberCountryCode + ' ' + val.contactNumber
          val.createdDate = val.createdDate ? this.datePipe.transform(val.createdDate, 'dd-MM-yyyy h:mm a') : '';
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    });
  }



  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.CREATE:
        if (!this.permission?.canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        this.rxjsService.setDialogOpenProperty(true);
        this.openAddEditPage(CrudType.CREATE, row)
        break
      case CrudType.VIEW:
        if (!this.permission?.canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        this.rxjsService.setDialogOpenProperty(true);
        this.openAddEditPage(CrudType.VIEW, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    const ref = this.dialogService.open(CustomerMessagingGroupAddEditDialogComponent, {
      showHeader: false,
      baseZIndex: 1000,
      width: '650px',
      data: { editableObject, header: 'Messaging Group', partitionId:this.partitionId, customerId: this.customerId, addressId: this.customerAddressId},
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        let otherParams = {}
        this.getRequiredListData(null, null, otherParams)
      }
    });

  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}
