import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerMessagingGroupAddEditDialogComponent } from "./customer-messaging-group-add-edit-dialog.component";

@NgModule({
  declarations: [CustomerMessagingGroupAddEditDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [CustomerMessagingGroupAddEditDialogComponent],
  entryComponents: [CustomerMessagingGroupAddEditDialogComponent],
})
export class CustomerMessagingGroupAddEditDialogModule { }