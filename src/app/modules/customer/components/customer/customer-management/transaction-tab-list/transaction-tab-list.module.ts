import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { TransactionTabListComponent } from "./transaction-tab-list.component";


@NgModule({
    declarations: [TransactionTabListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  entryComponents: [],
  exports: [TransactionTabListComponent],
  providers: []
})
export class TransactionTabListModule { }