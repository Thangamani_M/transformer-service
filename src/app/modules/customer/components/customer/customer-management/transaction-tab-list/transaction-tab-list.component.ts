
import { Component, OnInit, QueryList, ViewChildren, Input } from '@angular/core';
import { ComponentProperties, ModulesBasedApiSuffix, prepareGetRequestHttpParams, CrudType, RolesAndPermissionsObj, TabsList, IApplicationResponse, LoggedInUserModel, RxjsService, SnackbarService, ResponseMessageTypes, debounceTimeForSearchkeyword, CustomDirectiveConfig } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '@app/reducers';
import { MatMenuItem } from '@angular/material';
import { combineLatest, of } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { DialogService } from 'primeng/api';
import { loggedInUserData } from '@modules/others';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { MomentService } from '@app/shared/services/moment.service';
import { Table } from 'primeng/table';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';


@Component({
  selector: 'app-transaction-tab-list',
  templateUrl: './transaction-tab-list.component.html'
})
export class TransactionTabListComponent extends PrimeNgTableVariablesModel implements OnInit {

  @ViewChildren(Table) tables: QueryList<Table>;
  @Input() addressId:string;
  @Input() customerID:string;
  observableResponse;
  selectedTabIndex: any = 0;
  componentProperties = new ComponentProperties();
  primengTableConfigProperties: any;
  dataList: any;
  loading: boolean;
  public bradCrum: MatMenuItem[];
  status: any = [];
  ticketStatusList = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  totalRecords: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  searchKeyword: FormControl;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  today: any = new Date();
  searchColumns: any;
  row: any = {};
  feature: string;
  featureIndex: string;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  constructor(private crudService: CrudService,private tableFilterFormService: TableFilterFormService,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder) {
      super()
    this.primengTableConfigProperties = {
      tableCaption: "Recent Activities",
      selectedTransactionTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Recent Activities', relativeRouterUrl: '' }, { displayName: 'Recent Activities' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'TRANSACTIONS',
            dataKey: 'debtorId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'debtorRefNo', header: 'Debtor Ref No',width: '120px' },
            { field: 'debtorName', header: 'Name' ,width: '120px' },
            { field: 'accountNo', header: 'Account Number' ,width: '120px' },
            { field: 'bdiNumber', header: 'BDI Number' ,width: '120px' },
            { field: 'contactNumber', header: 'Contact No',width: '120px'  },
            { field: 'transaction', header: '', width: '120px', isLink: true, nosort: true },
            ],
            moduleName: ModulesBasedApiSuffix.SALES,
            apiSuffixModel: SalesModuleApiSuffixModels.CUSTOMER_DEBTOR_LIST,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true
          },
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? params['params']['tab'] ? +params['params']['tab'] : 0 : 0;
      this.feature = (Object.keys(params['params']).length > 0) ? params['params']['feature_name'] ? params['params']['feature_name'] : '' : '';
      this.featureIndex = (Object.keys(params['params']).length > 0) ? params['params']['featureIndex'] ? params['params']['featureIndex'] : '' : '';
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
    this.status = [
      { label: 'Active', value: true },
      { label: 'In-Active', value: false },
    ]
  }

  ngOnInit(): void {

    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }


  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;

    let obj1 = {
      customerId:this.customerID,
      addressId:this.addressId
    };
    if(otherParams){
      otherParams = {...otherParams, ...obj1};
    }else{
      otherParams = obj1;
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      SalesModuleApiSuffixModels.CUSTOMER_DEBTOR_LIST,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        data.resources.forEach(element => {
          if(element.accountNo){
            let len = element.accountNo.length - 4 ;
            let first = element.accountNo.substring(0,len);
            let last = element.accountNo.substring(len,element.accountNo.length);
            let str =''
            for(let i=0;i<first.length;i++){
             str=str+'*';
            }
            element.accountNo = str+''+last;
          }
          element.transaction = 'View All Transactions';

        });
        this.dataList = data.resources

        this.totalRecords = data.totalCount || this.dataList.length;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;

      }
    })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {

    console.log(type, row, unknownVar);
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row,unknownVar);
        break;
      case CrudType.GET:
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], row['searchColumns'])
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row,unknownVar);
        break;
        case CrudType.VIEW:
          this.openAddEditPage(CrudType.EDIT, row,unknownVar);
          break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete()
          }
        } else {
          this.onOneOrManyRowsDelete(row)
        }
        break;
      case CrudType.EXPORT:
        break;
      default:
    }
  }


  openAddEditPage(type: CrudType | string, editableObject?: object | string,unknownVar?): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigateByUrl("event-management/event-configuration/stack-config/add-edit");
            break;
          case 1:
            this.router.navigateByUrl("event-management/event-configuration/stack-areas/add-edit");
            break;


        }

        break;
      case CrudType.EDIT:
        switch (this.selectedTabIndex) {
          case 0:
            let queryParams = { id: this.customerID , debtorId:editableObject['debtorId'], addressId: this.addressId };
            if(this.feature && this.featureIndex) {
              queryParams['feature_name'] = this.feature;
              queryParams['featureIndex'] = this.featureIndex;
            }
            if(unknownVar =="transaction"){
              this.router.navigate(["customer/manage-customers/view-transactions/all-transactions"], { queryParams: {...queryParams, fromUrl:'customer'}});
            }else{
              this.router.navigate(["customer/manage-customers/view-transactions"], { queryParams: queryParams});
            }
            break;

        }
    }
  }


  onTabChange(event) {
    // this.selectedFilterStatus = null;
    // this.selectedFilterDate = null;
    this.tables.forEach(table => { //to set default row count list
      table.rows = 10
    })

    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    // this.router.navigate(['/event-management/event-configuration'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()

  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        selectAll: (this.totalRecords == this.selectedRows.length) ? true : false,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getRequiredListData()
      }
    });
  }



  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

}

