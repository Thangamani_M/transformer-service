import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CreditNotesControlComponent } from "./credit-notes-control.component";



@NgModule({
    declarations: [CreditNotesControlComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: CreditNotesControlComponent, data: { title: 'Credit Note' }
        },
    ])
  ],
  entryComponents: [],
})
export class CreditNotesControlModule { }