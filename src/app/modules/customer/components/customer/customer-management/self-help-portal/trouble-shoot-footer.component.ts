import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { countryCodes, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Observable } from 'rxjs';
import { SelectionPasswordDialogComponent } from '../client-testing/selection-password-dialog/selection-password-dialog.component';

@Component({
  selector: 'app-trouble-shoot-footer',
  templateUrl: './trouble-shoot-footer.component.html',
})
export class TroubleShootFooterComponent implements OnInit {

  @Input() customerId: string;
  @Input() customerAddressId: string;
  @Input() initiationId: string;
  @Input() callType: number;
  isClientTestingDialog: boolean;
  isDisableFields: boolean = false;
  isDisableButton: boolean = true;
  customerData: any;
  loggedUser: any;
  clientTestingDialogForm: FormGroup;
  formConfigs = formConfigs;
  countryCodes = countryCodes;
  signalDurationDropDown: any = [];
  stringConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  
  constructor(private rxjsService: RxjsService, private router: Router, private httpCancelService: HttpCancelService,
    private crudService: CrudService, public dialogService: DialogService, private store: Store<AppState>, private formBuilder: FormBuilder,) {
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.loggedUser = userData;
      });
      this.rxjsService.getCustomerDate().subscribe((data: boolean) => {
        this.customerData = data;
      });
  }

  ngOnInit(): void {
    this.createClientTestingForm();
  }

  // Create Client Testing Form
  createClientTestingForm() {
    this.clientTestingDialogForm = this.formBuilder.group({
      createdUserId: [this.loggedUser?.userId ? this.loggedUser?.userId : null],
      keyHolderId: [''],
      isAltSMSNumber: [false],
      altContactCompanyName: [''],
      altContactName: ['', Validators.required],
      customerId: [this.customerData?.customerId ? this.customerData?.customerId : null],
      customerAddressId: [this.customerAddressId ? this.customerAddressId : null],
      isPasswordVerified: ['true'],
      signalTestingDurationId: ['', Validators.required],
      altContactNo: ['', Validators.required],
      altContactNoCountryCode: ['+27', Validators.required],
      isActive: ['true'],
      techTestingConfigId: ['']
    })
  }


  bookTechnician() {
    this.router.navigate(['customer/manage-customers/call-initiation'], { queryParams: { customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType, callCreationPopup: true } });
  }

  redirectToViewPage() {
    this.rxjsService.setGoToTroubleShootProperty(true);
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.customerAddressId,
      customerTab: 4,
      monitoringTab: 6,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  openClientTesting() {
    this.isClientTestingDialog = true;
    this.getSignalDurations();
  }

  getSignalDurations() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_TECHNICAL_TESTING_CONFIG, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.signalDurationDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  showOptions(event) {
    if (event.checked) {
      this.isDisableFields = true
    } else {
      this.isDisableFields = false;
    }
  }

  onVerify(): void {
    const ref = this.dialogService.open(SelectionPasswordDialogComponent, {
      header: 'Selection Password',
      showHeader: true,
      baseZIndex: 1000,
      width: '650px',
      data: {
        id: this.clientTestingDialogForm.value.keyHolderId,
        type: 1 // 2- client api
      },
    });
    ref.onClose.subscribe((result) => {
      if (result.valid) {

        this.isDisableButton = false;
      } else {
        this.isDisableButton = true;

      }
    });
  }

  onActive(): void {
    if (this.clientTestingDialogForm.invalid) {
      this.clientTestingDialogForm.markAllAsTouched();
      return;
    }

    let formValue = this.clientTestingDialogForm.value;
    formValue.isAltSMSNumber = formValue.isAltSMSNumber == true ? true : false;
    formValue.isPasswordVerified = formValue.isPasswordVerified == true ? true : false;
    formValue.isActive = formValue.isActive == true ? true : false;
    formValue.techTestingConfigId = formValue.signalTestingDurationId
    delete formValue.signalTestingDurationId
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CLIENT_TESTING_OTHER, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.clientTestingDialogForm.get('isAltSMSNumber').setValue(false);
        this.clientTestingDialogForm.get('altContactName').setValue(null);
        this.clientTestingDialogForm.get('keyHolderId').setValue(null);
        this.clientTestingDialogForm.get('altContactNo').setValue(null);
        this.clientTestingDialogForm.get('altContactCompanyName').setValue(null);
        this.clientTestingDialogForm.get('signalTestingDurationId').setValue(null);
        this.isDisableFields = false;
        this.isDisableButton = true;
      }
    })
  }
}
