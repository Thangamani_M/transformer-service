import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, SharedModuleApiSuffixModels } from '@app/shared';

@Component({
    selector: 'trouble-shoot-system',
    templateUrl: './trouble-shoot-system.component.html',
    // styleUrls: ['./lss-registration.scss']
})

export class TroubleShootSystemComponent {
customerId: any;
systemTypeList= [];
initiationId: any;
customerAddressId: any;
callType: any;

constructor(private rxjsService: RxjsService, private router: Router,private crudService: CrudService,  private activatedRoute: ActivatedRoute) { 
  this.rxjsService.getTroubleShootAlarm().subscribe((data) => {
    this.customerId = data['customerId']
    this.initiationId = data['initiationId']
    this.customerAddressId = data['customerAddressId']
    this.callType = data['callType']
  });
  this.activatedRoute.queryParams.subscribe(params => {
    this.initiationId =  params?.initiationId ? params.initiationId : null;
    this.customerId =  params?.customerId ? params.customerId : null;
    this.customerAddressId = params?.customerAddressId ? params.customerAddressId : null;
    this.callType = params?.callType ? params.callType : null;
    this.rxjsService.setCustomerDate(this.customerId);
  });
}

ngOnInit(): void {
this.systemList();
}

systemList(): void {
  this.rxjsService.setGlobalLoaderProperty(true);
      this.crudService.get(ModulesBasedApiSuffix.SHARED,
          SharedModuleApiSuffixModels.UX_SELF_HELP_SYSTEM_TYPES, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.systemTypeList= response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }

    goToAlarm(system): void {
    this.router.navigate(['customer/manage-customers/trouble-shoot-alarm'],{ queryParams: { id: system.id, customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType, } });
}

getQueryParams() {
  return { customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType,};
}

  bookTechnician(){
    this.router.navigate(['customer/manage-customers/call-initiation'],{ queryParams: { customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType, callCreationPopup: true  } });
}

redirectToViewPage(){
  this.rxjsService.setViewCustomerData({
    customerId: this.customerId,
    addressId: this.customerAddressId,
    customerTab: 4,
    monitoringTab: 7,
  })
  this.rxjsService.navigateToViewCustomerPage();
}


}
