import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TroubleShootFooterComponent } from "./trouble-shoot-footer.component";


@NgModule({
    declarations: [TroubleShootFooterComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  entryComponents: [],
  exports: [TroubleShootFooterComponent]
})
export class TroubleShootFooterModule { }