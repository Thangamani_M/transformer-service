import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { BillingModuleApiSuffixModels } from '@modules/sales';

@Component({
  selector: 'trouble-shoot-alarm',
  templateUrl: './trouble-shoot-alarm.component.html',
  // styleUrls: ['./lss-registration.scss']
})

export class TroubleShootAlarmComponent {
  id: any;
  troubleShootAlarmList = [];
  customerId: any;
  initiationId: any;
  customerAddressId: any;
  callType: any;

  constructor(private rxjsService: RxjsService, private router: Router, private crudService: CrudService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params.id
    });
    this.rxjsService.getTroubleShootAlarm().subscribe((data) => {
      if (Object.keys(data)?.length) {
        this.customerId = data['customerId'];
        this.initiationId = data['initiationId'];
        this.customerAddressId = data['customerAddressId'];
        this.callType = data['callType'];
      }
    });
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params.id;
      this.initiationId =  params?.initiationId ? params.initiationId : null;
      this.customerId =  params?.customerId ? params.customerId : null;
      this.customerAddressId = params?.customerAddressId ? params.customerAddressId : null;
      this.callType = params?.callType ? params.callType : null;
    });
  }

  ngOnInit(): void {
    this.alarmList();
  }

  alarmList(): void {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.SHARED,
      BillingModuleApiSuffixModels.FAULT_DESCRIPTION_SYSTEM_TYPES, this.id, false, null, 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.troubleShootAlarmList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getQueryParams() {
    return { customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType,};
  }

  redirectToQuetionAnswer(alarm): void {
    this.router.navigate(['customer/manage-customers/trouble-shoot-alarm-quetion-answer'], { queryParams: { id: alarm.selfHelpSystemTypeConfigId, systemId: this.id, customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType, } });
  }

  bookTechnician() {
    this.router.navigate(['customer/manage-customers/call-initiation'], { queryParams: { customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType, callCreationPopup: true } });
  }

  redirectToViewPage() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.customerAddressId,
      customerTab: 4,
      monitoringTab: 7,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }
}
