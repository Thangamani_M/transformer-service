import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'trouble-shoot-alarm-quetion-answer',
  templateUrl: './trouble-shoot-alarm-quetion-answer.component.html',
  // styleUrls: ['./lss-registration.scss']
})

export class TroubleShootAlarmQuetionAnswerComponent {
  id: any;
  levelContent: any;
  solutionContent: any;
  // isLevel = true;
  isSolution = false;
  isLevelContent = true;
  isAnswer = false;
  troubleShootAlarmList = [];
  initialLevel: any;
  defaultQuetion: any;
  solutionObj: any;
  defaultQuestion2: any;
  isDefaultQuestion2 = false;
  valueAfterSolutionFlag: any;
  defaultQuestion2noContent: any;
  defaultQuestion2yesContent: any;
  level = 0;
  customerId: any;
  initiationId: any;
  customerAddressId: any;
  callType: any;
  firstDate: any = new Date();
  lastDate: any;
  diffMins: any = 0;
  clearTime: any;
  systemId: string;
  userData: UserLogin;

  constructor(private rxjsService: RxjsService,private route: Router, private dialog: MatDialog, private router: Router, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,) {
    this.rxjsService.getCustomerDate().subscribe((data) => {
      this.customerId= data;
    });
    this.rxjsService.getTroubleShootAlarm().subscribe((data) => {
      if(Object.keys(data)?.length) {
        this.customerId = data['customerId'];
        this.initiationId = data['initiationId'];
        this.customerAddressId = data['customerAddressId'];
        this.callType = data['callType'];
      }
    });
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params.id;
      this.systemId = params.systemId;
      this.initiationId =  params?.initiationId ? params.initiationId : null;
      this.customerId =  params?.customerId ? params.customerId : null;
      this.customerAddressId = params?.customerAddressId ? params.customerAddressId : null;
      this.callType = params?.callType ? params.callType : null;
    });
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    
    this.alarmList();
    // this.x;
    // this.levelOpen();
  }
  

  alarmList(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.SHARED,
      BillingModuleApiSuffixModels.QUETION_ANSWER, this.id, false, null, 1).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setDialogOpenProperty(false);
        if (response.isSuccess && response.statusCode == 200) {
          if(response?.resources?.length) {
            this.troubleShootAlarmList = response.resources;
            this.levelOpen();
            this.completionTime();
          } else {
            this.router.navigate(['/customer/manage-customers/trouble-shoot-system'],{ queryParams: this.getQueryParams() });
          }
        }
      });
  }

  completionTime() {
    this.clearTime = setInterval(() => {
      this.lastDate = new Date();
      var diffMs = this.lastDate - this.firstDate;
      this.diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
      if(this.levelContent?.estimatedTimeMin - this.diffMins == 0) {
        clearInterval(this.clearTime);
        this.router.navigate(['customer/manage-customers/call-initiation'],{ queryParams: { customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType  } });
        // this.onYesorNoAnswer({actionQuestion: 'Exit Troubleshouting'});
      }
      }, 1000);
  }

  getEstimateTime() {
    return this.levelContent?.estimatedTimeMin - this.diffMins == 0
  }

  levelOpen() {
    if(this.troubleShootAlarmList[this.level]) {
      this.initialLevel = this.troubleShootAlarmList[this.level];
      this.levelContent = this.initialLevel;
      this.isLevelContent = true;
    }
  }

  levelContentClick(content) {
    const reqObj = {
      callInitiationNoteId: 0,
      callInitiationId: this.initiationId,
      notes: `${this.levelContent?.question} : ${content.questionOptions}`,
      isActive: true,
      createdUserId: this.userData?.userId,
      modifiedUserId: this.userData?.userId,
    };
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.SHARED, TechnicalMgntModuleApiSuffixModels.CUSTOMER_SELF_HELP_QUESTION_ANSWER, reqObj)
    .subscribe((res: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if(res?.isSuccess && res?.statusCode == 200) {
        this.onAfterLevelContentAPI(content);
      }
    })
  }

  onAfterLevelContentAPI(content) {
    if(content.popUpMessage) {
      const dialogData = new ConfirmDialogModel("Confirm Action", content.popUpMessage);
      const dialogRef1 = this.dialog.open(ConfirmDialogPopupComponent, {
        minWidth: "400px",
        data: {...dialogData, isClose: true, isConfirm: false},
        disableClose: true,
      });
      dialogRef1.afterClosed().subscribe(result => {
        if (!result) {
          return;
        } else {
          this.onAfterLevelContent(content);
        }
      })
    } else {
      this.onAfterLevelContent(content);
    }
  }

  onAfterLevelContent(content) {
    this.solutionContent = content;
    if(content.solution && content.bookCallOrNextQuestion?.actionQuestion?.toLowerCase() != 'book a technician'){
    // if(content.solutionContent && content.bookCallOrNextQuestion?.actionQuestion?.toLowerCase() != 'book a technician'){
      this.solutionObj = content.solutionContent
      this.defaultQuetion = content.solutionContent.defaultQuestion1;
      this.isLevelContent = false;
      this.isAnswer = true;
    } else if(content.bookCallOrNextQuestion?.actionQuestion?.toLowerCase() == 'book a technician') {
      this.bookTechnician();
    } else {
      let object = content.bookCallOrNextQuestion;
      // const dialogData = new ConfirmDialogModel("Confirm Action", object.actionQuestion);
      // const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      //   minWidth: "400px",
      //   data: {...dialogData, isClose: true},
      //   disableClose: true
      // });
      // dialogRef.afterClosed().subscribe(result => {
      //   if (!result) {
      //     return;
      //   } else {
          this.onYesorNoAnswer(object);
      //   }
      // })
    }
  }

  answerClick(flag, value) {
    if (flag === 'Yes') {
      this.defaultQuestion2 = value.defaultQuestion1YesOptionContent.defaultQuestion2;
      if(this.defaultQuestion2) {
        this.isDefaultQuestion2 = true;
        this.isAnswer = false;
        this.defaultQuestion2noContent = value.defaultQuestion1YesOptionContent.defaultQuestion2noContent;
        this.defaultQuestion2yesContent = value.defaultQuestion1YesOptionContent.defaultQuestion2yesContent;
      } else {
        const object = value.defaultQuestion1YesOptionContent.bookCallOrNextQuestion;
        this.onAfterAnswerClick(object);
      }
    } else {
      const object = value.defaultQuestion1NoOptionContent.bookCallOrNextQuestion;
      this.onAfterAnswerClick(object);
    }
  }

  onAfterAnswerClick(object) {
    // const dialogData = new ConfirmDialogModel("Confirm Action", object.actionQuestion);
    // const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
    //   minWidth: "400px",
    //   data: {...dialogData, isClose: true},
    //   disableClose: true
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (!result) {
    //     return;
    //   } else {
        this.onYesorNoAnswer(object);
    //   }
    // })
  }

  onYesorNoAnswer(object) {
    if (object.actionQuestion === 'Go To Next Question') {
      this.isAnswer = false;
      this.level = object.level-1;
      // this.diffMins= 0;
      // this.firstDate = new Date();
      this.levelOpen();
    } else if (object.actionQuestion === 'Exit Troubleshooting') {
      this.onVoidCall();
      // this.router.navigate(['customer/manage-customers/call-initiation'],{ queryParams: { customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType  } });
    } else if (object.actionQuestion === 'Book a Technician') {
      this.onBookCall();
      // this.router.navigate(['customer/manage-customers/call-initiation'],{ queryParams: { customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType  } });
    }
  }

  onBookCall() {
    const dialogData = new ConfirmDialogModel("Confirm Action", "Please book a technician");
    const dialogRef1 = this.dialog.open(ConfirmDialogPopupComponent, {
      minWidth: "400px",
      data: {...dialogData, isClose: true, isConfirm: true, msgCenter: true},
      disableClose: true,
    });
    dialogRef1.afterClosed().subscribe(result => {
      if (!result) {
        return;
      } else {
        this.router.navigate(['customer/manage-customers/call-initiation'],{ queryParams: { customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType  } });
      }
    })
  }

  onVoidCall() {
    const dialogData = new ConfirmDialogModel("Confirm Action", "Does the customer still require the service call?");
    const dialogRef1 = this.dialog.open(ConfirmDialogPopupComponent, {
      minWidth: "400px",
      data: {...dialogData, isClose: true, isConfirm: true},
      disableClose: true,
    });
    dialogRef1.afterClosed().subscribe(result => {
      if (!result) {
        return;
      } else {
        this.onAfterVoidCall();
      }
    })
  }

  onAfterVoidCall() {
    const reqObj ={
      callInitiationId: this.initiationId,
      voidReason:"Issued resolved through troubleshooting",
      modifiedUserId: this.userData?.userId,
    }
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_VOID, reqObj)
    .subscribe((res: IApplicationResponse) => {
      if(res?.isSuccess && res?.statusCode == 200) {
        this.router.navigate(['customer/manage-customers/call-initiation'],{ queryParams: { customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType  } });
      }
    })
  }

  redirectToFaultDesc(): void {
    this.router.navigate(['customer/manage-customers/trouble-shoot-alarm'], { queryParams: { id: this.systemId, customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType } });
  }


  defaultQuestion2Click(value) {
    let object = value.bookCallOrNextQuestion;
    // const dialogData = new ConfirmDialogModel("Confirm Action", object.actionQuestion);
    // const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
    //   minWidth: "400px",
    //   data: {...dialogData, isClose: true},
    //   disableClose: true
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (!result) {
    //     return;
    //   } else {
        this.onYesorNoAnswer(object);
    //   }
    // })


  }

  bookTechnician(){
    this.router.navigate(['customer/manage-customers/call-initiation'],{ queryParams: { customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType, callCreationPopup: true  } });
}

redirectToViewPage(){
  this.rxjsService.setViewCustomerData({
    customerId: this.customerId,
    addressId: this.customerAddressId,
    customerTab: 4,
    monitoringTab: 7,
  })
  this.rxjsService.navigateToViewCustomerPage();
}

getQueryParams() {
  return { customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType,};
}

  ngOnDestroy() {
    if(this.clearTime) {
      clearInterval(this.clearTime);
    }
  }
}
