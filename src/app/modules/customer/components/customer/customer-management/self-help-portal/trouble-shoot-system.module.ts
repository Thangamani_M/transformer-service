import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TroubleShootFooterModule } from "./trouble-shoot-footer.module";
import { TroubleShootSystemComponent } from "./trouble-shoot-system.component";

import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations: [TroubleShootSystemComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: TroubleShootSystemComponent, data: { title: 'Trouble Shoot System' }, canActivate: [AuthGuard]
        },
    ]),
    TroubleShootFooterModule,
  ],
  entryComponents: [],
})
export class TroubleShootSystemModule { }