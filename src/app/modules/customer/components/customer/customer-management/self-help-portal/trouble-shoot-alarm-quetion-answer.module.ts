import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TroubleShootAlarmQuetionAnswerComponent } from "./trouble-shoot-alarm-quetion-answer.component";
import { TroubleShootFooterModule } from "./trouble-shoot-footer.module";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
    declarations: [TroubleShootAlarmQuetionAnswerComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: TroubleShootAlarmQuetionAnswerComponent, canActivate:[AuthGuard], data: { title: 'Trouble Shoot Alarm' }
        },
    ]),
    TroubleShootFooterModule,
  ],
  entryComponents: [],
})
export class TroubleShootAlarmQuetionAnswerModule { }