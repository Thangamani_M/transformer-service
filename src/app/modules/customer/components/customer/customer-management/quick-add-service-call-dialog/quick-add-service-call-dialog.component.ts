import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, defaultPopupType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CustomerModuleApiSuffixModels, CustomerVerificationType } from '@modules/customer/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { EventMgntModuleApiSuffixModels } from '../../../../../event-management/shared/enums/configurations.enum';
import { AddContactDialogComponent } from '../add-contact-dialog/add-contact-dialog.component';
import { QuickAddServiceCallModel, QuickCommentsListModel } from './quick-add-service-call.model';
@Component({
  selector: 'app-quick-add-service-call-dialog',
  templateUrl: './quick-add-service-call-dialog.component.html',
  styleUrls: ['./quick-add-service-call-dialog.component.scss']
})
export class QuickAddServiceCallDialogComponent implements OnInit {
  quickAddServiceCallForm: FormGroup;
  comments: FormArray;
  callDetailsData: any;
  loggedUser: any;
  contactList = [];
  faultDescription = [];
  showWarringMsg: boolean = false;
  contactNumberList = [];
  isCode50Enable: boolean = false;
  isButtonDisabled: boolean = true;
  isSubmitted: boolean;
  defaultPopupType = defaultPopupType.ANGULAR_MATERIAL_DRAGGABLE;

  constructor(private dialog: MatDialog, private router: Router, private activatedRoute: ActivatedRoute,
    public dialogRef: MatDialogRef<QuickAddServiceCallDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private store: Store<AppState>, private rxjsService: RxjsService, public _fb: FormBuilder, private crudService: CrudService) {
    this.rxjsService.setDialogOpenProperty(true);
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createForm();
    this.showWarringMsg = this.data.warningMsg ? true : false;
    this.onLoadValue();
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }

  onLoadValue() {
    let dropdownsAndData = [
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.UX_CUSTOMR_KEYHOLDERS_WITH_CUSTOMER_ID, undefined, null, prepareGetRequestHttpParams(null, null,
          { CustomerId: this.data.customerId })),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_QUICK_CALL),
      this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CODE_50_ENABLE,
        undefined, null, prepareGetRequestHttpParams(null, null,
          { CustomerId: this.data.customerId, CustomerAddressId: this.data.customerAddressId })),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.QUICK_ADD_SERVICE,
        undefined, null, prepareGetRequestHttpParams(null, null,
          { CustomerId: this.data.customerId, AddressId: this.data.customerAddressId }))
    ];
    this.loadActionTypes(dropdownsAndData);
  }

  loadActionTypes(dropdownsAndData) {
    this.isSubmitted = true;
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              resp.resources.forEach(element => {
                element.isNewContact = false
              });
              resp.resources.forEach(element => {
                this.contactList.push(element)
              });
              this.quickAddServiceCallForm.get('keyholderId').setValue(this.callDetailsData?.keyholderId ? this.callDetailsData?.keyholderId : this.callDetailsData?.callinitiationContactId)
              break;
            case 1:
              this.faultDescription = resp.resources;
              break;
            case 2:
              this.isCode50Enable = resp.resources.isCode50;
              break;
            case 3:
              this.onLoadQuickAdd(resp);
              break;
          }
        }
        if (response.length == ix + 1) {
          this.isSubmitted = false;
          this.rxjsService.setPopupLoaderProperty(false);
        }
      });
    });
  }

  createForm(): void {
    let quickAddServiceCallModel = new QuickAddServiceCallModel();
    this.quickAddServiceCallForm = this._fb.group({
      comments: this._fb.array([])
    });
    Object.keys(quickAddServiceCallModel).forEach((key) => {
      this.quickAddServiceCallForm.addControl(key, new FormControl({ value: quickAddServiceCallModel[key], disabled: true }));
    });
    this.quickAddServiceCallForm = setRequiredValidator(this.quickAddServiceCallForm, ["addressId", "customerId", "keyholderId", "contactNo", "faultDescriptionId", "isCode50"]);
    this.quickAddServiceCallForm.get('addressId').setValue(this.data?.customerAddressId)
    this.quickAddServiceCallForm.get('customerId').setValue(this.data?.customerId)
    this.quickAddServiceCallForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.quickAddServiceCallForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    this.onFormValuechanges();
  }

  onFormValuechanges() {
    this.quickAddServiceCallForm.get('keyholderId').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      let contact = this.contactList.find(x => x.keyHolderId == val)
      if (contact) {
        this.contactNumberList = [];
        if (contact.mobile1) {
          this.contactNumberList.push({ id: contact.mobile1, value: contact.mobile1 });
        }
        if (contact.mobile2) {
          this.contactNumberList.push({ id: contact.mobile2, value: contact.mobile2 });
        }
        if (contact.officeNo) {
          this.contactNumberList.push({ id: contact.officeNo, value: contact.officeNo });
        }
        if (contact.premisesNo) {
          this.contactNumberList.push({ id: contact.premisesNo, value: contact.premisesNo });
        }
        if (contact.contactNumber) { //for new contact
          this.contactNumberList.push({ id: contact.contactNumber, value: contact.contactNumber });
        }
        this.quickAddServiceCallForm.get('contactName').setValue(contact ? contact?.keyHolderName ? contact?.keyHolderName : contact?.customerName : null);
        this.quickAddServiceCallForm.get('isNewContact').setValue(contact ? contact?.isNewContact ? contact?.isNewContact : false : false);
      }
    });
  }

  onContactChange() {
    this.quickAddServiceCallForm.get('contactNo').setValue('');
    this.quickAddServiceCallForm.get('contactNoCountryCode').setValue('');
  }

  createCommentsListModel(quickCommentsListModel?: QuickCommentsListModel): FormGroup {
    let StockIdRequestDescriptionListModelControl = new QuickCommentsListModel(quickCommentsListModel);
    let formControls = {};
    Object.keys(StockIdRequestDescriptionListModelControl).forEach((key) => {
      if (key === 'comments') {
        formControls[key] = [{ value: StockIdRequestDescriptionListModelControl[key], disabled: true }, [Validators.required]];
      } else {
        formControls[key] = [{ value: StockIdRequestDescriptionListModelControl[key], disabled: false }];
      }
    });
    return this._fb.group(formControls);
  }

  editQuickAddServiceCall() {
    if (this.activatedRoute?.snapshot?.queryParams?.feature_name || this.data?.edit == true) {
      this.onAfterCustomerVerification();
    } else {
      let filterObj = { userId: this.loggedUser?.userId, customerId: this.data?.customerId, siteAddressId: this.data.customerAddressId, uniqueCallId: this.data.uniqueCallId ? this.data.uniqueCallId : null };
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CHECK, null, false,
        prepareRequiredHttpParams(filterObj))
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.onAfterCustomerVerification();
          } else {
            this.dialogRef.close();
            this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: this.data?.customerId, siteAddressId: this.data.customerAddressId, tab: CustomerVerificationType.QUICK_ADD_SERVICE_CALL_CREATION } })
          }
          this.rxjsService.setPopupLoaderProperty(false);
        });
    }
  }

  onAfterCustomerVerification() {
    if (!this.quickAddServiceCallForm.get('callInitiationId').value) {
      this.quickAddServiceCallForm.get('keyholderId').enable();
      this.quickAddServiceCallForm.get('contactNo').enable();
      this.quickAddServiceCallForm.get('faultDescriptionId').enable();
      this.quickAddServiceCallForm.get('isCode50').enable();
      this.isButtonDisabled = false;
      this.comments.enable();
    } else if (this.data?.edit == true) {
      this.getCommentsListArray.controls[this.getCommentsListArray.length - 1].enable();
      this.isButtonDisabled = false;
    }
  }

  get getCommentsListArray(): FormArray {
    if (!this.quickAddServiceCallForm) return;
    return this.quickAddServiceCallForm.get("comments") as FormArray;
  }

  onLoadQuickAdd(response) {
    let stockIdRequestAddEditModel = new QuickAddServiceCallModel(response.resources);
    this.callDetailsData = response.resources;
    this.quickAddServiceCallForm.patchValue(stockIdRequestAddEditModel);
    if (response.resources?.isNewContact) {
      let newcontactData = {
        keyHolderId: response.resources.callinitiationContactId,
        keyHolderName: response.resources.contactName,
        contactNumber: response.resources.contactNoCountryCode + ' ' + response.resources.contactNo,
        isNewContact: true
      }
      this.contactList.push(newcontactData);
      this.quickAddServiceCallForm.get('keyholderId').setValue(this.callDetailsData?.keyholderId ? this.callDetailsData?.keyholderId : this.callDetailsData?.callinitiationContactId ? this.callDetailsData?.callinitiationContactId : '');
    }
    this.quickAddServiceCallForm.get('contactNo').setValue(response.resources?.contactNo ? response.resources?.contactNoCountryCode + ' ' + response.resources?.contactNo : '');
    this.quickAddServiceCallForm.get('faultDescriptionId').setValue(response.resources?.faultDescriptionId ? response.resources?.faultDescriptionId : '');
    this.quickAddServiceCallForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.quickAddServiceCallForm.get('modifiedUserId').setValue(this.loggedUser.userId)
    this.comments = this.getCommentsListArray
    if (response.resources?.comments.length > 0) {
      response.resources.comments.forEach((stockIdRequestDescriptionListModel: QuickCommentsListModel) => {
        this.comments.push(this.createCommentsListModel(stockIdRequestDescriptionListModel));
      })
      this.comments.push(this.createCommentsListModel());
    } else {
      this.comments.push(this.createCommentsListModel());
    }
  }

  openAddContact(val) {
    const dialogRef = this.dialog.open(AddContactDialogComponent, {
      width: '750px',
      data: this.data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.contactList.push(result)
      }
    });
  }

  onSubmit() {
    if (this.quickAddServiceCallForm.invalid) {
      this.isSubmitted = false;
      return;
    }
    this.isSubmitted = true;
    let formValue = this.quickAddServiceCallForm.getRawValue()
    formValue.addressId = this.data?.customerAddressId
    formValue.customerId = this.data?.customerId
    let contactNo = formValue?.contactNo?.substr(formValue?.contactNo?.indexOf(' ') + 1)
    let contactNoCountryCode = formValue?.contactNo?.substr(0, formValue?.contactNo?.indexOf(' '))
    formValue.contactNo = contactNo;
    formValue.contactNoCountryCode = contactNoCountryCode;
    if (formValue.isNewContact) {
      formValue.keyholderId = null;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    let crudService = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.QUICK_ADD_SERVICE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      this.isSubmitted = false;
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setCustomerAddresId(this.callDetailsData.addressId);
        this.rxjsService.setLoadTechnicianProperty(true);
        this.dialogRef.close();
      }
    });
  }

  close() {
    this.dialogRef.close();
  }
}

