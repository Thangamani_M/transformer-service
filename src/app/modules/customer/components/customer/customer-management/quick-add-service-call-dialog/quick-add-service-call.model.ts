class QuickAddServiceCallModel {

    constructor(quickAddServiceCallModel?: QuickAddServiceCallModel) {
        this.callInitiationId = quickAddServiceCallModel ? quickAddServiceCallModel.callInitiationId == undefined ? '' : quickAddServiceCallModel.callInitiationId : '';
        this.customerId = quickAddServiceCallModel ? quickAddServiceCallModel.customerId == undefined ? '' : quickAddServiceCallModel.customerId : '';
        this.customerName = quickAddServiceCallModel ? quickAddServiceCallModel.customerName == undefined ? '' : quickAddServiceCallModel.customerName : '';
        this.customerRefNo = quickAddServiceCallModel ? quickAddServiceCallModel.customerRefNo == undefined ? '' : quickAddServiceCallModel.customerRefNo : '';
        this.customerAddress = quickAddServiceCallModel ? quickAddServiceCallModel.customerAddress == undefined ? '' : quickAddServiceCallModel.customerAddress : '';
        this.addressId = quickAddServiceCallModel ? quickAddServiceCallModel.addressId == undefined ? '' : quickAddServiceCallModel.addressId : '';
        this.faultDescriptionId = quickAddServiceCallModel ? quickAddServiceCallModel.faultDescriptionId == undefined ? '' : quickAddServiceCallModel.faultDescriptionId : '';
        this.callinitiationContactId = quickAddServiceCallModel ? quickAddServiceCallModel.callinitiationContactId == undefined ? '' : quickAddServiceCallModel.callinitiationContactId : '';
        this.keyholderId = quickAddServiceCallModel ? quickAddServiceCallModel.keyholderId == undefined ? '' : quickAddServiceCallModel.keyholderId : '';
        this.isNewContact = quickAddServiceCallModel ? quickAddServiceCallModel.isNewContact == undefined ? false : quickAddServiceCallModel.isNewContact : false;
          this.contactName = quickAddServiceCallModel ? quickAddServiceCallModel.contactName == undefined ? '' : quickAddServiceCallModel.contactName : '';
        this.contactNo = quickAddServiceCallModel ? quickAddServiceCallModel.contactNo == undefined ? '' : quickAddServiceCallModel.contactNo : '';
        this.contactNoCountryCode = quickAddServiceCallModel ? quickAddServiceCallModel.contactNoCountryCode == undefined ? '' : quickAddServiceCallModel.contactNoCountryCode : '';
        this.isCode50 = quickAddServiceCallModel ? quickAddServiceCallModel.isCode50 == undefined ? false : quickAddServiceCallModel.isCode50 : false;
        this.comments = quickAddServiceCallModel ? quickAddServiceCallModel.comments == undefined ? [] : quickAddServiceCallModel.comments : [];
        this.createdUserId = quickAddServiceCallModel ? quickAddServiceCallModel.createdUserId == undefined ? '' : quickAddServiceCallModel.createdUserId : '';
        this.modifiedUserId = quickAddServiceCallModel ? quickAddServiceCallModel.modifiedUserId == undefined ? '' : quickAddServiceCallModel.modifiedUserId : '';
    }

    callInitiationId: string;
    customerId: string;
    customerName: string
    customerRefNo: string
    customerAddress: string
    addressId: string
    faultDescriptionId: string;
    callinitiationContactId: string;
    keyholderId: string;
    isNewContact: boolean
    contactName: string;
    contactNo: string;
    contactNoCountryCode: string;
    isCode50: boolean
    comments: QuickCommentsListModel[]
    createdUserId: string
    modifiedUserId: string

}


class QuickCommentsListModel {

    constructor(commentsListModel?: QuickCommentsListModel) {
        this.callInitiationCommentId = commentsListModel == undefined ? undefined : commentsListModel.callInitiationCommentId == undefined ? undefined : commentsListModel.callInitiationCommentId;
        this.comments = commentsListModel ? commentsListModel.comments == undefined ? '' : commentsListModel.comments : '';
    }

    callInitiationCommentId?: string;
    comments: string;
}

export { QuickAddServiceCallModel, QuickCommentsListModel }