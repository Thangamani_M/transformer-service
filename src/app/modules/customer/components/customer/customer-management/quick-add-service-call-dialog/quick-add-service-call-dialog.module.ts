import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AddContactDialogModule } from "../add-contact-dialog/add-contact-dialog.module";
import { QuickAddServiceCallDialogComponent } from "./quick-add-service-call-dialog.component";

@NgModule({
  declarations: [QuickAddServiceCallDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    AddContactDialogModule,
  ],
  exports: [QuickAddServiceCallDialogComponent],
  entryComponents: [QuickAddServiceCallDialogComponent],
})
export class QuickAddServiceCallDialogModule { }