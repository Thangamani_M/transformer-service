import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerSharedModule } from "@modules/customer/shared/customer-shared.module";
import { CustomerVideofiedInfoComponent } from "./customer-videofied-info.component";

@NgModule({
  declarations: [CustomerVideofiedInfoComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CustomerSharedModule,
    FormsModule,
    MaterialModule,
    SharedModule,
  ],
  exports: [CustomerVideofiedInfoComponent],
  entryComponents: [],
})
export class CustomerVideofiedInfoModule { }