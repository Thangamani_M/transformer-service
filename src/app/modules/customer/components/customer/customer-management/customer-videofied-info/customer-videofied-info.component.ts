import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, getMatMultiData, getPDropdownData, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CUSTOMER_COMPONENT } from '@modules/customer/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';
@Component({
  selector: 'app-customer-videofied-info',
  templateUrl: './customer-videofied-info.component.html',
  styleUrls: ['./customer-videofied-info.component.scss']
})
export class CustomerVideofiedInfoComponent implements OnInit {
  customerAddressId: any;
  customerData: any;
  decoderAccountConfigId: any;
  eventTypeDetails: any;
  decoderAccountConfigForm: FormGroup;
  loggedUser: any;
  formConfigs = formConfigs;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  ownershipList: any = []
  rentedTypeList: any = []
  panelTypeList: any = []
  isPasswordShow = false;
  isAdtSimPasswordShow = false;
  isInstallerPasswordShow = false;
  customerId;
  addressId;
  @Input() permission
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService,
    private snackbarService: SnackbarService) {
    this.activatedRoute.paramMap.subscribe(res => {
      if (res) {
        this.customerId = res ? res.get('id') : '';

      }
    })

    this.activatedRoute.queryParamMap.subscribe(params => {
      if (params) {
        this.addressId = (Object.keys(params['params']).length > 0) ? params['params']['addressId'] ? params['params']['addressId'] : '' : '';
      }
    });

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createdecoderAccountConfigForm();
    this.loadActionTypes();
    this.getEventTypeDetailsById();
    // this.rxjsService.setGlobalLoaderProperty(false);
    // console.log(this.permission);
  }


  loadActionTypes() {
    let dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_OWNERSHIP_TYPES),
      this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
        EventMgntModuleApiSuffixModels.UX_RENTAL_ALARM_CONFIG),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_PANEL_TYPE_CONFIG)
    ];
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.ownershipList = resp.resources;
              break;
            case 1:
              this.rentedTypeList = resp.resources;
              break;
            case 2:
                let panelListResp = resp.resources;
              if (panelListResp && panelListResp.length) {
                let list = getPDropdownData(panelListResp);
                console.log('panelTypeList', list);
                this.panelTypeList = list;
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createdecoderAccountConfigForm(): void {
    this.decoderAccountConfigForm = this.formBuilder.group({
      videofiedSimId: [''],
      customerId: [this.customerId ? this.customerId : ''],
      addressId: [this.addressId ? this.addressId : ''],
      itemOwnershipId: [''],
      rentedAlarmId: [''],
      panelTypeConfigId: [],
      videofiedSim: [''],
      adtSim: [''],
      installerCode: [''],
      createdUserId: [this.loggedUser.userId ? this.loggedUser.userId : null]
    });
  }


  getEventTypeDetailsById() {
    let otherParams = {};
    otherParams['CustomerId'] = this.customerId;
    otherParams['AddressId'] = this.addressId;
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_INFO_DETAILS, null,
      false, prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.eventTypeDetails = response.resources;
          this.eventTypeDetails.rentedAlarmId = response.resources?.rentedAlarmId ? response.resources?.rentedAlarmId : '';
          this.decoderAccountConfigForm.patchValue(this.eventTypeDetails);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (!this.permission?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.decoderAccountConfigForm.invalid) {
      return;
    }
    let formValue = this.decoderAccountConfigForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.decoderAccountConfigId) ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_INFO, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.VIDEOFIED_INFO, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.getEventTypeDetailsById()
      }
    });
  }
  cancel() {
    this.getEventTypeDetailsById();
  }

  onInstallerCodeView() {
    const installerPermission = this.permission?.actions?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT?.INSTALLER_CODE)?.subMenu?.find(el => el?.menuName == PermissionTypes.VIEW);
    if (!installerPermission) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isInstallerPasswordShow = !this.isInstallerPasswordShow;
  }
}

