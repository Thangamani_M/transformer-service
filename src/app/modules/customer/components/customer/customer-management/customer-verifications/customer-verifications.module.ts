import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { CanDeactivateGuard, LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from "@app/shared/material.module";
import { CustomerVerificationAddEditComponent } from "./customer-verification.component";
import { CustomerVerificationsDialogComponent } from './customer-verifications-dialog/customer-verifications-dialog.component';



@NgModule({
    declarations: [CustomerVerificationAddEditComponent, CustomerVerificationsDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: CustomerVerificationAddEditComponent, data: { title: 'Customer Verification' }, canDeactivate: [CanDeactivateGuard]
        },
    ])
  ],
  exports: [],
  entryComponents: [CustomerVerificationsDialogComponent],
})
export class CustomerVerificationModule { }