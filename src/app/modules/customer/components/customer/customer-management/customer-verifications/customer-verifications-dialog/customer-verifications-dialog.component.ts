import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, HttpCancelService, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared/utils/customer-module.enums';

@Component({
  selector: 'app-customer-verifications-dialog',
  templateUrl: './customer-verifications-dialog.component.html'
})
export class CustomerVerificationsDialogComponent implements OnInit {

  reasonForm: FormGroup;
  finalObject: any;

  constructor(private rxjsService: RxjsService, private httpCancelService: HttpCancelService, 
    @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<CustomerVerificationsDialogComponent>,
    private crudService: CrudService, private formBuilder: FormBuilder) {
      this.rxjsService.setDialogOpenProperty(true);
      this.finalObject = data?.finalObject;
    }

  ngOnInit(): void {
    this.reasonForm = this.formBuilder.group({
      reason: ['', Validators.required],
    });
  }

  btnCloseClick() {
    this.reasonForm.reset();
    this.dialogRef.close(false);
  }

  onReasonSubmit() {
    if (this.reasonForm.invalid) {
      return;
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.finalObject.reason = this.reasonForm.get('reason').value;
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CANCEL_VERIFICATION, this.finalObject, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.dialogRef.close(response);
      }
    })

  }
}
