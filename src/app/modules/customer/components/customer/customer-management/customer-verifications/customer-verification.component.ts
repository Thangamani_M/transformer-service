import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, sidebarDataSelector$, SnackbarService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { CustomerManagementService, CustomerModuleApiSuffixModels } from '@modules/customer';
import { CustomerVerificationRedirect } from '@modules/customer/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { CustomerVerificationsDialogComponent } from './customer-verifications-dialog/customer-verifications-dialog.component';

declare var $;
@Component({
  selector: 'app-customer-verification',
  templateUrl: './customer-verification.component.html',
  styleUrls: ['./customer-verification.component.scss']
})

export class CustomerVerificationAddEditComponent {
  totalRecords;
  pageLimit: any = [10, 25, 50, 75, 100];
  columnFilterForm: FormGroup;
  otpForm: FormGroup;
  row: any = {};
  searchColumns: any;
  searchForm: FormGroup;
  observableResponse: any;
  dataList = [];
  selectedTabIndex = 0;
  loggedUser: any;
  customerId: any;
  siteAddressId: any;
  passwordList = [];
  IdNumberList = [];
  emailList = [];
  status = [];
  selectedRows: any = [];
  selectedIndex: any = 0;
  companyRegistartionList = [];
  passportNumberList = [];
  bankNameList = [];
  accountTypeList = [];
  postalAddressList = [];
  vatNumberList = [];
  otpList = [];
  otpContactNumbers = [];
  loading = true;
  finalObject: any = {};
  customerContactInfo: any;
  clickedMobileNumber: any;
  verified: number;
  otpContactName: any;
  otpContactNumber: any;
  otpContactNumberCountryCode: any;
  isAutoSaveEnable: boolean = true;
  isSignalMgmt: boolean;
  customerVerificationContactId: any = null
  id: any = {};
  custInfoList: any
  customerAddresses = []
  customerVerificationTabId: number;
  uniqueCallId;
  customerTab: number;
  monitoringTab: number;
  pageSize: number = 10;
  feature: string;
  featureIndex: string;
  redirectUrl: string = "";
  isButtonDisable: boolean;
  primengTableConfigProperties: any = {
    tableCaption: "Contact Info",
    breadCrumbItems: [{ displayName: 'Contact Info List', relativeRouterUrl: '' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Contact Info',
          dataKey: 'registrationId',
          enableBreadCrumb: true,
          enableReset: false,
          // enbableAddActionBtn: true,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'contactName', header: 'Contact Name', width: '200px' },
          { field: 'premisesNoWithCountryCode', header: 'Premises', width: '200px' },
          { field: 'officeNoWithCountryCode', header: 'Office', width: '200px' },
          { field: 'mobile1WithCountryCode', header: 'Mobile 1', width: '200px' },
          { field: 'mobile2WithCountryCode', header: 'Mobile 2', width: '200px' },
          { field: 'keyHolderWithCountryCode', header: 'Keyholder Contact Number', width: '220px' }
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_ID_NUMBER,
          moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        }
      ]
    }
  }

  constructor(private crudService: CrudService, private tableFilterFormService: TableFilterFormService, private router: Router,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>, private httpCancelService: HttpCancelService,
    private dialog: MatDialog, private rxjsService: RxjsService, private _fb: FormBuilder,
    private customerManagementService: CustomerManagementService, private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParams.subscribe(params => {
      this.customerId = params?.customerId;
      this.siteAddressId = params?.siteAddressId;
      this.customerVerificationTabId = params?.tab ? +params?.tab : 0;
      this.customerTab = params?.customerTab?.toString() ? +params?.customerTab : 0;
      this.monitoringTab = params?.monitoringTab?.toString() ? +params?.monitoringTab : null;
      this.isSignalMgmt = params?.isSignalMgmt ? params?.isSignalMgmt == 'true' ? true : false : false;
      this.feature = params?.feature_name;
      this.redirectUrl = params?.redirectUrl;
      this.featureIndex = params?.featureIndex;
    });

  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getCustomerData();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    let otherParams = {};
    if (this.siteAddressId) {
      this.getRequiredListData(null, null, otherParams);
    }
    this.columnFilterRequest();
    this.otpForm = this._fb.group({
      contactName: ['', Validators.required],
      contactNo: ['', Validators.required],
      contactNoCountryCode: [''],
      otp: ['']
    });
    this.otpForm.get('otp').disable();
    this.otpForm.get('contactName').valueChanges.subscribe((contactName: string) => {

    })
    this.verified = 0;
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
    this.getPasswordList();
  }
  primengTableConfigPropertiesObj: any= {
    tableComponentConfigs:{
      tabsList : [{},{},{},{},{},{},{},{},{},{},{}]
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(sidebarDataSelector$),
    ]
    ).subscribe((response) => {
      let customerManagemnt = response[0]?.['menuAccess'].find(item=> item.menuName =="Customer Management");
      let customers = customerManagemnt['subMenu'].find(item => item.menuName == "Customers");
      let quickAction = customers['subMenu'].find(item=> item.menuName =="Quick Action")
      let customerVerification = quickAction['subMenu'].find(item=> item.menuName =="Customer Verification");
      if (customerVerification) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, customerVerification['subMenu']);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    // [breadCrumbItem['queryParams']['key']]: breadCrumbItem['queryParams']['value']
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  getCustomerData() {
    this.customerManagementService.getCustomer(this.customerId).subscribe(resp => {
      if (resp.isSuccess && resp.statusCode == 200) {
        if (!this.siteAddressId) {
          this.siteAddressId = resp.resources.addresses.length > 0 ? resp.resources.addresses[0].addressId : null;
        }
        this.custInfoList = resp.resources;
        this.customerAddresses = resp.resources.addresses;
        this.changeAddress()
      }
    })
  }
  changeAddress() {
    let otherParams = {};
    this.getRequiredListData(null, null, otherParams);
  }

  getRequiredListData(pageIndex?: string, pageSize?: string,
    otherParams?: object) {
    this.loading = true;
    const preparams = {
      customerId: this.customerId,
      siteAddressId: this.siteAddressId
    }
    otherParams = { ...otherParams, ...preparams };
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CONTACT_INFO,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams), 1
    ).subscribe((response: IApplicationResponse) => {
      this.loading = false;
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.observableResponse = response;
        this.dataList = this.observableResponse.resources;
        this.dataList.forEach(element => {
          element.isClick = false
        });
        this.totalRecords = response.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  contactNumberClick(data, number, type) {
    if(number) {
      this.clickedMobileNumber = type;
      this.dataList.forEach(element => {
        element.isMobile1Click = false;
        element.isMobile2Click = false;
        element.iskeyHolderContactClick = false;
        element.isOfficeNoClick = false;
        element.isPremisesNoClick = false;
      });
      if (type === 'mobile1' && data?.mobile1WithCountryCode) {
        this.dataList.find(e => e.uniqueId == data.uniqueId).isMobile1Click = true;
      } else if (type === 'mobile2' && data?.mobile2WithCountryCode) {
        this.dataList.find(e => e.uniqueId == data.uniqueId).isMobile2Click = true;
      } else if (type === 'keyHolderContact' && data?.keyHolderWithCountryCode) {
        this.dataList.find(e => e.uniqueId == data.uniqueId).iskeyHolderContactClick = true;
      } else if (type === 'officeNoContact' && data?.officeNoWithCountryCode) {
        this.dataList.find(e => e.uniqueId == data.uniqueId).isOfficeNoClick = true;
      } else if (type === 'premisesNoContact' && data?.premisesNoWithCountryCode) {
        this.dataList.find(e => e.uniqueId == data.uniqueId).isPremisesNoClick = true;
      }
      this.customerContactInfo = data;
      this.rxjsService.setCustomerContactInfo(data);
    }
  }

  getButtonDisabled() {
    return (!this.isButtonDisable || !this.customerContactInfo || this.selectedIndex == 0);
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }
  onCRUDRequested(GET: any, row?: any): any {
    throw new Error('Method not implemented.');
  }





  onTabChanged(index: number) {
    this.selectedIndex = index;
    switch (index) {
      case 0:
        //this.finalObject = {};
        // this.getRequiredListData();
        break;
      case 1:
        // this.finalObject = {};
        // this.getPasswordList();
        this.getEnableButtonOnTabWise(this.passwordList);
        break;
      case 2:
        //this.finalObject = {};
        this.getIdNumberList();
        break;
      case 3:
        //this.finalObject = {};
        this.getEmailList();
        break;
      case 4:
        //this.finalObject = {};
        this.getCompanyRegistartionList();
        break;
      case 5:
        //this.finalObject = {};
        this.getPassportNumberList();
        break;
      case 6:
        //this.finalObject = {};
        this.getBankNameList();
        break;
      case 7:
        //this.finalObject = {};
        this.getAccountTypeList();
        break;
      case 8:
        //this.finalObject = {};
        this.getPostalAddressList();
        break;
      case 9:
        //this.finalObject = {};
        this.getVatNumberList();
        break;
      case 10:
        //this.finalObject = {};
        this.getOtpList();
    }
  }

  getEnableButtonOnTabWise(data) {
    this.isButtonDisable = data?.length > 0;
  }

  getPasswordList() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_PASSWORD, undefined, false,
      prepareGetRequestHttpParams(null, null,
        { customerId: this.customerId, siteAddressId: this.siteAddressId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.passwordList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  getIdNumberList() {
    this.IdNumberList = [];
    this.rxjsService.setGlobalLoaderProperty(true);
    this.getEnableButtonOnTabWise(this.IdNumberList);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_ID_NUMBER, undefined, false,
      prepareGetRequestHttpParams(null, null,
        { customerId: this.customerId, siteAddressId: this.siteAddressId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.IdNumberList = response.resources;
          }
          this.getEnableButtonOnTabWise(this.IdNumberList);
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  getEmailList() {
    this.emailList = [];
    this.getEnableButtonOnTabWise(this.emailList);
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_EMAIL, undefined, false,
      prepareGetRequestHttpParams(null, null,
        { customerId: this.customerId, siteAddressId: this.siteAddressId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.emailList = response.resources;
          }
          this.getEnableButtonOnTabWise(this.emailList);
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  getCompanyRegistartionList() {
    this.companyRegistartionList = [];
    this.getEnableButtonOnTabWise(this.companyRegistartionList);
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_COMPANY_REGISTRATION, undefined, false,
      prepareGetRequestHttpParams(null, null,
        { customerId: this.customerId, siteAddressId: this.siteAddressId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.companyRegistartionList = response.resources;
          }
          this.getEnableButtonOnTabWise(this.companyRegistartionList);
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  getPassportNumberList() {
    this.passportNumberList = [];
    this.getEnableButtonOnTabWise(this.passportNumberList);
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_COMPANY_PASSPORT_NUMBER, undefined, false,
      prepareGetRequestHttpParams(null, null,
        { customerId: this.customerId, siteAddressId: this.siteAddressId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.passportNumberList = response.resources;
          }
          this.getEnableButtonOnTabWise(this.passportNumberList);
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  getBankNameList() {
    this.bankNameList = [];
    this.getEnableButtonOnTabWise(this.bankNameList);
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_BANK_NAME, undefined, false,
      prepareGetRequestHttpParams(null, null,
        { customerId: this.customerId, siteAddressId: this.siteAddressId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.bankNameList = response.resources;
          }
          this.getEnableButtonOnTabWise(this.bankNameList);
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  getAccountTypeList() {
    this.accountTypeList = [];
    this.getEnableButtonOnTabWise(this.accountTypeList);
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_ACCOUNT_TYPE, undefined, false,
      prepareGetRequestHttpParams(null, null,
        { customerId: this.customerId, siteAddressId: this.siteAddressId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.accountTypeList = response.resources;
          }
          this.getEnableButtonOnTabWise(this.accountTypeList);
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  getPostalAddressList() {
    this.postalAddressList = [];
    this.getEnableButtonOnTabWise(this.postalAddressList);
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_POSTAL_ADDRESS, undefined, false,
      prepareGetRequestHttpParams(null, null,
        { customerId: this.customerId, siteAddressId: this.siteAddressId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.postalAddressList = response.resources;
          }
          this.getEnableButtonOnTabWise(this.postalAddressList);
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  getVatNumberList() {
    this.vatNumberList = [];
    this.getEnableButtonOnTabWise(this.vatNumberList);
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_VAT_NUMBER, undefined, false,
      prepareGetRequestHttpParams(null, null,
        { customerId: this.customerId, siteAddressId: this.siteAddressId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.vatNumberList = response.resources;
          }
          this.getEnableButtonOnTabWise(this.vatNumberList);
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  redirectToView() {
    if (this.isAutoSaveEnable) {
      this.onCancelClick();
    } else {
      this.onAfterVerify();
    }
  }

  redirectToCustomer() {
    if (this.isAutoSaveEnable) {
      this.onCancelClick();
    } else {
      if (!this.isAutoSaveEnable) {
        this.rxjsService.setViewCustomerData({
          customerId: this.customerId,
          addressId: this.siteAddressId,
          feature_name: this.feature,
          featureIndex: this.featureIndex,
          customerTab: this.customerTab,
          monitoringTab: this.monitoringTab,
        })
        this.rxjsService.navigateToViewCustomerPage();
        return;
      }
    }
  }

  //can deactivate auth guard don't delete this
  canDeactivate() {
    if (this.isAutoSaveEnable) {
      this.onCancelClick();
      return false;
    } else {
      return true;
    }
  }

  onCancelClick() {
    this.onRequestObj();
    const data = {
      finalObject: this.finalObject,
    }
    const ref = this.dialog.open(CustomerVerificationsDialogComponent, {
      width: '40vw',
      disableClose: true,
      data: { ...data, header: 'Reason for not doing customer verification', },
    });
    ref.afterClosed().subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.isAutoSaveEnable = false;
        this.onAfterVerify();
      }
    });
  }
  navigateToCustomer() {
    if (this.isSignalMgmt) {
      this.router.navigate([`/signal-management/signal-configuration/client-details/view`], { queryParams: { customerId: this.customerId, customerAddressId: this.siteAddressId } });
    } else {
      this.rxjsService.setViewCustomerData({
        customerId: this.customerId,
        addressId: this.siteAddressId,
        feature_name: this.feature,
        featureIndex: this.featureIndex,
        customerTab: this.customerTab,
        monitoringTab: this.monitoringTab,
      })
      this.rxjsService.navigateToViewCustomerPage();
    }
  }
  onAfterVerify() {
    if (!this.isAutoSaveEnable) {
      if (this.isSignalMgmt) {
        this.router.navigate([`/signal-management/signal-configuration/client-details/view`], { queryParams: { customerId: this.customerId, customerAddressId: this.siteAddressId } });
      } else {
        this.rxjsService.setViewCustomerData({
          customerId: this.customerId,
          addressId: this.siteAddressId,
          feature_name: this.feature,
          featureIndex: this.featureIndex,
          customerTab: this.customerTab,
          monitoringTab: this.monitoringTab,
        })

        this.rxjsService.navigateToViewCustomerPage();
        if (this.redirectUrl == CustomerVerificationRedirect.RISK_WATCH_SERVICE) {
          window.open(
            `billing/risk-watch-guard-service/add-service?customerId=${this.customerId}&addressId=${this.siteAddressId}`)
        }
        return
      }
    }
  }

  getOtpList() {
    this.otpList = [];
    this.getEnableButtonOnTabWise(this.otpList);
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_OTP, undefined, false,
      prepareGetRequestHttpParams(null, null,
        { customerId: this.customerId, siteAddressId: this.siteAddressId })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.otpList = response.resources;
          }
          this.getEnableButtonOnTabWise(this.otpList);
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  onCustomerInfo(e) {
    switch (this.selectedIndex) {
      case 1:
        this.finalObject.alarmCancellationPassword = e?.value;
        break;
      case 2:
        this.finalObject.idNumber = e?.value;
        break;
      case 3:
        this.finalObject.email = e?.value;
        break;
      case 4:
        this.finalObject.companyRegNo = e?.value;
        break;
      case 5:
        this.finalObject.passportNo = e?.value;
        break;
      case 6:
        this.finalObject.bankName = e?.value;
        break;
      case 7:
        this.finalObject.accountType = e?.value;
        break;
      case 8:
        this.finalObject.siteAddress = e?.value;
        break;
      case 9:
        this.finalObject.vatNumber = e?.value;
        break;
    }
  }

  onSubmit(): void {
    // if(primengTableConfigPropertiesObj)
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[this.selectedIndex].canVerify) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
  }
    this.onRequestObj();
    let reqObj;
    switch (this.selectedIndex) {
      case 1:
        reqObj = { ...this.finalObject, customerVerificationPasswordId: this.id?.customerVerificationPasswordId ? this.id?.customerVerificationPasswordId : '', customerVerificationContactId: this.customerVerificationContactId };
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_PASSWORD, reqObj, 1).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.id.customerVerificationPasswordId = response?.resources?.id;
            this.isAutoSaveEnable = !response?.resources?.isVerified;
            this.customerVerificationContactId = response?.resources?.customerVerificationContactId;
            this.onAfterVerify();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
        break;
      case 2:
        reqObj = { ...this.finalObject, customerVerificationIdNumberId: this.id?.customerVerificationIdNumberId ? this.id?.customerVerificationIdNumberId : '', customerVerificationContactId: this.customerVerificationContactId };
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_ID_NUMBER, reqObj, 1).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.id.customerVerificationIdNumberId = response?.resources?.id;
            this.isAutoSaveEnable = !response?.resources?.isVerified;
            this.customerVerificationContactId = response?.resources?.customerVerificationContactId;
            this.onAfterVerify();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
        break;
      case 3:
        reqObj = { ...this.finalObject, customerVerificationEmailId: this.id?.customerVerificationEmailId ? this.id?.customerVerificationEmailId : '', customerVerificationContactId: this.customerVerificationContactId };
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_EMAIL, reqObj, 1).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.id.customerVerificationEmailId = response?.resources?.id;
            this.isAutoSaveEnable = !response?.resources?.isVerified;
            this.customerVerificationContactId = response?.resources?.customerVerificationContactId;
            this.onAfterVerify();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
        break;
      case 4:
        reqObj = { ...this.finalObject, customerVerificationCompanyRegistrationId: this.id?.customerVerificationCompanyRegistrationId ? this.id?.customerVerificationCompanyRegistrationId : '', customerVerificationContactId: this.customerVerificationContactId };
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_COMPANY_REGISTRATION, reqObj, 1).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.id.customerVerificationCompanyRegistrationId = response?.resources?.id;
            this.isAutoSaveEnable = !response?.resources?.isVerified;
            this.customerVerificationContactId = response?.resources?.customerVerificationContactId;
            this.onAfterVerify();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
        break;
      case 5:
        reqObj = { ...this.finalObject, customerVerificationPassportId: this.id?.customerVerificationPassportId ? this.id?.customerVerificationPassportId : '', customerVerificationContactId: this.customerVerificationContactId };
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_COMPANY_PASSPORT_NUMBER, reqObj, 1).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.id.customerVerificationPassportId = response?.resources?.id;
            this.isAutoSaveEnable = !response?.resources?.isVerified;
            this.customerVerificationContactId = response?.resources?.customerVerificationContactId;
            this.onAfterVerify();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
        break;
      case 6:
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        reqObj = { ...this.finalObject, customerVerificationBankId: this.id?.customerVerificationBankId ? this.id?.customerVerificationBankId : '', customerVerificationContactId: this.customerVerificationContactId };
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_BANK_NAME, reqObj, 1).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.id.customerVerificationBankId = response?.resources?.id;
            this.isAutoSaveEnable = !response?.resources?.isVerified;
            this.customerVerificationContactId = response?.resources?.customerVerificationContactId;
            this.onAfterVerify();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
        break;
      case 7:
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        reqObj = { ...this.finalObject, customerVerificationAccountTypeId: this.id?.customerVerificationAccountTypeId ? this.id?.customerVerificationAccountTypeId : '', customerVerificationContactId: this.customerVerificationContactId };
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_ACCOUNT_TYPE, reqObj, 1).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.id.customerVerificationAccountTypeId = response?.resources?.id;
            this.isAutoSaveEnable = !response?.resources?.isVerified;
            this.customerVerificationContactId = response?.resources?.customerVerificationContactId;
            this.onAfterVerify();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
        break;
      case 8:
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        reqObj = { ...this.finalObject, customerVerificationAddressId: this.id?.customerVerificationAddressId ? this.id?.customerVerificationAddressId : '', customerVerificationContactId: this.customerVerificationContactId };
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_POSTAL_ADDRESS, reqObj, 1).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.id.customerVerificationAddressId = response?.resources?.id;
            this.isAutoSaveEnable = !response?.resources?.isVerified;
            this.customerVerificationContactId = response?.resources?.customerVerificationContactId;
            this.onAfterVerify();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
        break;
      case 9:
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        reqObj = { ...this.finalObject, customerVerificationVATNumberId: this.id?.customerVerificationVATNumberId ? this.id?.customerVerificationVATNumberId : '', customerVerificationContactId: this.customerVerificationContactId };
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_VAT_NUMBER, reqObj, 1).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.id.customerVerificationVATNumberId = response?.resources?.id;
            this.isAutoSaveEnable = !response?.resources?.isVerified;
            this.customerVerificationContactId = response?.resources?.customerVerificationContactId;
            this.onAfterVerify();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
        break;
      case 10:
        this.otpForm.get('otp').setValidators(Validators.required);
        this.otpForm.get('otp').updateValueAndValidity();
        if (this.otpForm.invalid) {
          this.otpForm.markAllAsTouched();
          return;
        }
        const finalObject = {
          customerId: this.customerId,
          contactNo: this.otpForm.value.contactNo,
          contactName: this.otpForm.value.contactName,
          contactNoCountryCode: this.otpForm.value.contactNoCountryCode,
          otp: this.otpForm.value.otp,
          createdUserId: this.loggedUser.userId,
          customerVerificationContactId: this.customerVerificationContactId,
          uniqueCallId: this.uniqueCallId ? this.uniqueCallId : '',
          customerVerificationTabId: this.customerVerificationTabId,
        }
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_VERIFY_OTP, finalObject, 1).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.isAutoSaveEnable = !response?.resources?.isVerified;
            this.customerVerificationContactId = !response?.resources?.customerVerificationContactId;
            this.onAfterVerify();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
        break;
    }
  }

  onRequestObj() {
    this.finalObject.siteAddressId = this.siteAddressId;
    this.finalObject.customerId = this.customerId;
    this.finalObject.mobile1 = this.clickedMobileNumber === 'mobile1' ? this.customerContactInfo.mobile1 : 0;
    this.finalObject.mobile2 = this.clickedMobileNumber === 'mobile2' ? this.customerContactInfo.mobile2 : 0;
    this.finalObject.officeNo = this.clickedMobileNumber === 'officeNoContact' ? this.customerContactInfo.officeNo : 0;
    this.finalObject.premisesNo = this.clickedMobileNumber === 'premisesNoContact' ? this.customerContactInfo.premisesNo : 0;
    this.finalObject.mobile1CountryCode = this.clickedMobileNumber === 'mobile1' ? this.customerContactInfo.mobile1CountryCode : '';
    this.finalObject.mobile2CountryCode = this.clickedMobileNumber === 'mobile2' ? this.customerContactInfo.mobile2CountryCode : '';
    this.finalObject.officeNoCountryCode = this.clickedMobileNumber === 'officeNoContact' ? this.customerContactInfo.officeNoCountryCode : '';
    this.finalObject.premisesNoCountryCode = this.clickedMobileNumber === 'premisesNoContact' ? this.customerContactInfo.premisesNoCountryCode : '';
    this.finalObject.keyHolderContactNo = this.clickedMobileNumber === 'keyHolderContact' ? this.customerContactInfo.keyHolderContactNo : 0;
    this.finalObject.keyHolderNoCountryCode = this.clickedMobileNumber === 'keyHolderContact' ? this.customerContactInfo.keyHolderNoCountryCode : '';
    this.finalObject.createdUserId = this.loggedUser.userId;
    this.finalObject.uniqueCallId = this.uniqueCallId ? this.uniqueCallId : '';
    this.finalObject.customerVerificationTabId = this.customerVerificationTabId;
  }

  changeContact(data) {
    var filteredObj = this.otpList.find(f => f.id === data.target.value);
    this.otpContactNumbers = filteredObj.contactNumbers;
    this.otpContactName = filteredObj.contactName;
    // this.otpForm.get('contactNo').setValue(this.otpList.find(e => e.contactNo == data.target.value).contactNo);
    // this.otpForm.get('contactNoCountryCode').setValue(this.otpList.find(e => e.contactNo == data.target.value).contactNoCountryCode);
    // this.otpForm.get('contactName').setValue(data.target.value);
    this.otpForm.get('otp').markAsUntouched();
  }

  changeContactNo(data) {
    var filteredNoObj = this.otpContactNumbers.find(f => f.contactNoWithCountryCode === data.target.value);
    this.otpContactNumber = filteredNoObj.contactNo;
    this.otpContactNumberCountryCode = filteredNoObj.contactNoCountryCode;
    this.otpForm.get('otp').markAsUntouched();

  }

  onOtpSubmit() {
    this.otpForm.get('otp').clearValidators();
    this.otpForm.get('otp').updateValueAndValidity();
    if (this.otpForm.invalid) {
      this.otpForm.markAllAsTouched();
      return;
    }
    this.onRequestObj();
    this.finalObject.contactNo = this.otpContactNumber;
    this.finalObject.contactName = this.otpContactName;
    this.finalObject.contactNoCountryCode = this.otpContactNumberCountryCode;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_SEND_OTP, this.finalObject, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.otpForm.get('otp').enable();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  exportExcel() { }
}
