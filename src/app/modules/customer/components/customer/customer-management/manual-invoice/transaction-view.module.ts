import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { TransactionViewComponent } from "./transaction-view.component";



@NgModule({
    declarations: [TransactionViewComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: TransactionViewComponent, data: { title: 'Transaction View' }
        },
    ])
  ],
  entryComponents: [],
})
export class TransactionViewModule { }