import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ManualInvoiceApproveDialogComponent } from './manual-invoice-approve-dialog.component';



@NgModule({
    declarations: [ManualInvoiceApproveDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  entryComponents: [ManualInvoiceApproveDialogComponent],
  exports: [ManualInvoiceApproveDialogComponent],
})
export class ManualInvoiceApproveDialogModule { }