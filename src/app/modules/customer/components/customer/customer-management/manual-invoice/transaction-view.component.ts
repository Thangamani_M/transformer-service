import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { BillingModuleApiSuffixModels } from '@modules/sales';
@Component({
  selector: 'transaction-view',
  templateUrl: './transaction-view.component.html',
  styleUrls: ['./manual-invoice.component.scss'],
})


export class TransactionViewComponent {
  manualInvoicItems = [];
  manualInvoiceDocuments = [];
  manualInvoiceApprovels = [];
  isSendCustomerCopy = false;
  comments: any;
  pendingInvoiceData: any;
  manualInvoiceId: any;
  declineStatus = false;
  contractId: any;
  customerId: any;
  addressId: any;
  debtorId: any;
  transactionType: any;
  totalExcAmount = 0;
  totalVatAmount = 0;
  totalIncAmount = 0;
  doaSummary=false
  constructor(private crudService: CrudService, private router: Router, private rxjsService: RxjsService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.manualInvoiceId = params.manualInvoiceApprovalId;
      this.contractId = params.contractId;
      this.customerId = params.customerId;
      this.addressId = params.addressId;
      this.debtorId = params.debtorId;
      this.transactionType = params.transactionType
    });
  }
  ngOnInit() {
    this.getManualInvoiceDetails();
  }
  getManualInvoiceDetails() {
    if (this.transactionType == 'pending-transaction') {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.PENDING_TRANSACTION,
        prepareGetRequestHttpParams(null, null, {
          manualInvoiceId: this.manualInvoiceId
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.pendingInvoiceData = response.resources;
            this.manualInvoicItems = response.resources.manualInvoicItems;
            this.manualInvoicItems.forEach(element => {
              this.totalExcAmount += element.itemPrice;
              this.totalVatAmount += element.vatAmount;
              this.totalIncAmount += element.totalPrice;
            });
            this.manualInvoiceDocuments = response.resources.manualInvoiceDocuments;
            this.comments = response.resources.comments;
            this.manualInvoiceApprovels = response.resources.manualInvoiceApprovels;
            this.manualInvoiceApprovels.forEach(element => {
              if (element.statusName === 'Declined')
                this.declineStatus = true;
            });
            //   this.manualInvoiceApprovalId = response.resources.manualInvoiceApprovalId;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else if (this.transactionType == 'transaction') {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.TRANSACTION,
        prepareGetRequestHttpParams(null, null, {
          invoiceId: this.manualInvoiceId
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.pendingInvoiceData = response.resources;
            this.manualInvoicItems = response.resources.manualInvoicItems;
            this.manualInvoicItems.forEach(element => {
              this.totalExcAmount += element.itemPrice;
              this.totalVatAmount += element.vatAmount;
              this.totalIncAmount += element.totalPrice;
            });
            this.manualInvoiceDocuments = response.resources.manualInvoiceDocuments;
            this.comments = response.resources.comments;
            this.manualInvoiceApprovels = response.resources.manualInvoiceApprovels;
            this.manualInvoiceApprovels.forEach(element => {
              if (element.statusName === 'Declined')
                this.declineStatus = true;
            });
            //   this.manualInvoiceApprovalId = response.resources.manualInvoiceApprovalId;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  selectedStatus: any

  statusDetails(e) {
    this.selectedStatus = e;
  }

  redirectTotransactionEditPage() {
    this.router.navigate(['customer/manage-customers/manual-invoice'], { queryParams: { contractId: this.contractId, customerId: this.customerId, addressId: this.addressId, debtorId: this.debtorId, manualInvoiceId: this.manualInvoiceId } });

  }

  cancelClick() {
    this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } });

  }

  openLinkInNewTab(link?) {
    if (link) {
      window.open(link, '_blank');
    }
  }
}
