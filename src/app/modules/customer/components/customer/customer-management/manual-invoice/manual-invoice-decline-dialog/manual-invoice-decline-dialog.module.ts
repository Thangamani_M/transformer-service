import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ManualInvoiceDeclineDialogComponent } from './manual-invoice-decline-dialog.component';



@NgModule({
    declarations: [ManualInvoiceDeclineDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  entryComponents: [ManualInvoiceDeclineDialogComponent],
  exports: [ManualInvoiceDeclineDialogComponent],
})
export class ManualInvoiceDeclineDialogModule { }