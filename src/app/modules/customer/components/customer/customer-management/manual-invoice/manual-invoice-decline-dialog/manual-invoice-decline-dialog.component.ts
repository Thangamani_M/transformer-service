import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RxjsService, CrudService, SnackbarService, IApplicationResponse, ModulesBasedApiSuffix, HttpCancelService, ResponseMessageTypes } from '@app/shared';
import { BillingModuleApiSuffixModels } from '@modules/sales/shared/utils/billing-module.enum';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-manual-invoice-decline-dialog',
  templateUrl: './manual-invoice-decline-dialog.component.html',
  styleUrls: ['./manual-invoice-decline-dialog.component.scss']
})
export class ManualInvoiceDeclineDialogComponent implements OnInit {

  declineInvoiceForm: FormGroup;
  public formData = new FormData();
  fileList: File[] = [];
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  listOfFiles: any[] = [];
  maxFilesUpload: Number = 5;
  isSubmited = false

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private httpCancelService: HttpCancelService,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private router: Router,) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.declineInvoiceForm = this.formBuilder.group({
      manualInvoiceApprovalId: [''],
      createdUserId: [''],
      comments: ['', Validators.required],
      declineReasonId: ['', Validators.required]
    });
  }

  btnCloseClick() {
    this.declineInvoiceForm.reset();
    this.ref.close(false);
  }

  removeSelectedFile(index) {
    this.listOfFiles.splice(index, 1);
    this.fileList.splice(index, 1);
  }

  uploadFiles(file) {
    if (file && file.length == 0)
      return;

    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }

    const supportedExtensions = [
      'jpeg',
      'jpg',
      'png',
      'gif',
      'pdf',
      'doc',
      'docx',
      'xls',
      'xlsx',
    ];

    for (let i = 0; i < file.length; i++) {
      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension)) {
        let filename = this.fileList.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push(selectedFile.name);
          this.myFileInputField.nativeElement.value = null;
        }
      } else {
        this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
      }
    }
  }

  onDeclineSubmit() {
    if (this.declineInvoiceForm.invalid) {
      return
    }

    if(this.declineInvoiceForm.dirty){
      this.isSubmited = true;
    }
    this.declineInvoiceForm.get('manualInvoiceApprovalId').setValue(this.config?.data?.manualInvoiceApprovalId);
    this.declineInvoiceForm.get('createdUserId').setValue(this.config?.data?.createdUserId);

    var formValue = this.declineInvoiceForm.value;
    if(this.formData.get('email')) {
      this.formData.delete('email');
    }
    this.formData.append('email', JSON.stringify(formValue));
    this.fileList.forEach(file => {
      this.formData.append('file', file);
    });

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_DECLINE, this.formData, 1)
      .subscribe((response: IApplicationResponse) => {
        this.isSubmited = false
        if (response.isSuccess && response.statusCode == 200) {
          this.formData = new FormData();
          this.ref.close();
          this.router.navigate(['billing/task-list']);
        }
      });

  }
}
