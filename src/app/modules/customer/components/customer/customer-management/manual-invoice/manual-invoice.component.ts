import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { CustomerManualInvoiceModel } from '@modules/customer/models/customer-manual-invoice.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
@Component({
  selector: 'app-customer-manual-invoice',
  templateUrl: './manual-invoice.component.html',
  styleUrls: ['./manual-invoice.component.scss'],
})

export class ManualInvoiceComponent {
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  customerManualInvoiceForm: FormGroup;
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  manualInvoiceId: any;
  public formData = new FormData();
  departmentList = [];
  transactionTypesList = [];
  transactionTypesDescriptionList = [];
  transactionDescriptiondocuments = [];
  totalSummary = {
    exclVAT: 0, vat: 0, inclVAT: 0
  };
  manualInvoiceDetails;
  transactionTypeDescription = 'INVOICE';
  date = new Date();
  todayDate: any = new Date();
  stockIdList = [];
  stockDescriptionList = [];
  stockPriceList = [];
  quantityList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  selectedStockId: any;
  isStockPriceList = false;
  listOfFiles1: any[] = [];
  listOfFiles2: any[] = [];
  listOfFiles3: any[] = [];
  // fileList = []
  fileList1: File[] = [];
  fileList2: File[] = [];
  fileList3: File[] = [];
  fileList1Uploaded = false;
  fileList2Uploaded = false;
  documentdetails: Array<any> = [];
  manualInvoiceDocuments: any = [];
  manualInvoiceItems = [];
  documentObj1 = {};
  documentObj2 = {};
  documentObj3 = {};
  docName: any;
  documentDetails: any;
  maxFilesUpload: Number = 5;
  submitted = false;
  loggedUser;
  customerId: any;
  debtorId: any;
  addressId: any
  contractId: any;
  path: any;
  getManualInvoiceDocuments = [];
  mainPriceList = []
  firstDay = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
  descriptionList: any[];
  startTodayDate = new Date();
  minDate =  new Date();
  constructor(private crudService: CrudService, private dialog: MatDialog,
    private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private httpCancelService: HttpCancelService, private snackbarService: SnackbarService, private rxjsService: RxjsService, private formBuilder: FormBuilder) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParams.subscribe(params => {
      this.customerId = params.customerId;
      this.debtorId = params.debtorId;
      this.addressId = params.addressId;
      this.contractId = params.contractId;
      this.manualInvoiceId = params.manualInvoiceId
    });
    let date = new Date(), y = date.getFullYear(), m = date.getMonth();
    this.minDate = new Date(y, m, 1);
  }

  ngOnInit() {
    this.createManualInvoiceForm();
    this.getDivisions();
    this.getTransactionTypes();
    if (this.manualInvoiceId) {
      this.getManualInvoiceById()
    } else {
      this.getManualInvoiceDetails();
    }
    this.onFormControlName();
  }
  createManualInvoiceForm() {
    let customerManualInvoiceModel = new CustomerManualInvoiceModel();
    this.customerManualInvoiceForm = this.formBuilder.group({});
    Object.keys(customerManualInvoiceModel).forEach((key) => {
      this.customerManualInvoiceForm.addControl(key, new FormControl(customerManualInvoiceModel[key]));
    });
    this.customerManualInvoiceForm = setRequiredValidator(this.customerManualInvoiceForm, ["documentDate", "invoiceTransactionDescriptionId", "departmentId"]);
    this.customerManualInvoiceForm.get("invoiceTransactionTypeId").setValue('b5aa4769-bebc-4b31-bb07-6b63f96f3c46');
    this.customerManualInvoiceForm.get("stockQty").setValidators([Validators.min(1)]);
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_TRANSACTION_TYPE_DESCRIPTION,
      prepareGetRequestHttpParams(null, null, {
        invoiceTransactionTypeId: this.customerManualInvoiceForm.value.invoiceTransactionTypeId
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.transactionTypesDescriptionList = response.resources.transactionDescriptions;
          this.transactionTypeDescription = response.resources.transactionTypeDescription
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  onFormControlName() {
    this.customerManualInvoiceForm.get('invoiceTransactionTypeId').valueChanges.subscribe((invoiceTransactionTypeId: string) => {
      this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_TRANSACTION_TYPE_DESCRIPTION,
        prepareGetRequestHttpParams(null, null, {
          invoiceTransactionTypeId: invoiceTransactionTypeId
        })).subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.transactionTypesDescriptionList = response.resources.transactionDescriptions;
            this.transactionTypeDescription = response.resources.transactionTypeDescription
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    })
    this.customerManualInvoiceForm.get('invoiceTransactionDescriptionId').valueChanges.subscribe((invoiceTransactionDescriptionId: string) => {
      this.requireDocumentList(invoiceTransactionDescriptionId);
    })

  }

  getManualInvoiceById() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_DEVICE_GET_BY_ID,
      prepareGetRequestHttpParams(null, null, {
        manualInvoiceId: this.manualInvoiceId
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.manualInvoiceDetails = response.resources;
          this.customerManualInvoiceForm.patchValue(response.resources);
          this.stockIdList = response.resources?.stockDetail?.stockIdList;
          this.stockDescriptionList = response.resources?.stockDetail?.stockDescriptionList;
          this.stockPriceList = response.resources?.stockDetail?.stockPriceList;
          this.stockPriceList.forEach(element => {
            element.vat = element.vatAmount;
          });
          this.mainPriceList = this.stockPriceList;
          this.manualInvoiceItems = response.resources.manualInvoicItems;
          this.getManualInvoiceDocuments = response.resources.manualInvoiceDocuments;
          this.manualInvoiceDocuments = response.resources.manualInvoiceDocuments;
          this.customerManualInvoiceForm.get('departmentId').setValue(response.resources.departMentId);
          this.customerManualInvoiceForm.get('requireDocId').setValue(response.resources.manualInvoiceDocuments[0]?.invoiceTransactionDescriptionSubTypeDocumentId);
          // this.customerManualInvoiceForm.get('optionalDocId').setValue(response.resources.manualInvoiceDocuments[1]?.invoiceTransactionDescriptionSubTypeDocumentId);
          this.customerManualInvoiceForm.get('telephonicDocId').setValue(response.resources.manualInvoiceDocuments[2]?.invoiceTransactionDescriptionSubTypeDocumentId);
          this.customerManualInvoiceForm.get('stockIdDescription').setValue(this.manualInvoiceItems[0].stockDescriptionId)
          this.customerManualInvoiceForm.get('stockId').setValue(this.manualInvoiceItems[0].stockId)
          this.customerManualInvoiceForm.get('stockQty').setValue(this.manualInvoiceItems[0].qty)
          // this.customerManualInvoiceForm.documentName
          this.rxjsService.setGlobalLoaderProperty(false);
        }

      });

  }

  getManualInvoiceDetails() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_CUSTOMER_DETAILS,
      prepareGetRequestHttpParams(null, null, {
        customerId: this.customerId,
        debtorId: this.debtorId,
        addressId: this.addressId,
        contractId: this.contractId
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.manualInvoiceDetails = response.resources;
          this.customerManualInvoiceForm.patchValue(response.resources);
          this.stockIdList = response.resources.stockDetail?.stockIdList;
          this.stockDescriptionList = response.resources.stockDetail?.stockDescriptionList;
          this.stockPriceList = response.resources.stockDetail.stockPriceList;
          this.stockPriceList.forEach(element => {
            element.vat = element.vatAmount;
          });
          this.mainPriceList = this.stockPriceList;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDivisions() {
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DEPARTMENTS,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.departmentList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTransactionTypes() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_TRANSACTION_TYPES,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.transactionTypesList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTransactionTypeDescription() {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_TRANSACTION_TYPE_DESCRIPTION,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.transactionTypesDescriptionList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  requireDocumentList(e) {
    this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.TRANSACTION_DESCRIPTION_DOCUMENTS,
      prepareGetRequestHttpParams(null, null, {
        invoiceTransactionDescriptionId: e
        // invoiceTransactionDescriptionId: '19b4597f-9353-483c-b17f-6426e51d1bd9'
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.transactionDescriptiondocuments = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

  }

  tempararyPriceList = [];

  selectedStocks(e) {
    this.selectedStockId = e.target.value;
    let temp = []
    // stockDescriptionList = []
    this.customerManualInvoiceForm.get("stockIdDescription").setValue("");
    temp = this.stockDescriptionList.find(value => value.stockId === e.target.value).stockDescriptionList;
    if (temp.length > 0) {
      if(temp.length ==1){
        this.customerManualInvoiceForm.get("stockIdDescription").setValue(temp[0]?.id)
      }
      this.descriptionList = temp

    }

  }


  selectedDescription(e) {
    // let tempList = [];
    // tempList = this.mainPriceList.filter(value => (value.stockId === this.selectedStockId && value.stockDescriptionId === e.target.value));
    // this.tempararyPriceList = [...this.tempararyPriceList, ...tempList]
  }

  addStockPrice() {
    if (!this.customerManualInvoiceForm.value.stockId) {
      this.snackbarService.openSnackbar('Please select Stock ID', ResponseMessageTypes.WARNING);
      return;
    }
    if (!this.customerManualInvoiceForm.value.stockIdDescription) {
      this.snackbarService.openSnackbar('Please select Stock ID Description', ResponseMessageTypes.WARNING);
      return;
    }
    if (!this.customerManualInvoiceForm.value.stockQty) {
      this.snackbarService.openSnackbar('Please Enter Stock Quantity', ResponseMessageTypes.WARNING);
      return;
    }

    if (this.customerManualInvoiceForm.value.stockQty == 0) {
      this.snackbarService.openSnackbar('Please Enter Valid Stock Quantity', ResponseMessageTypes.WARNING);
      return;
    }

    let found;
    found = this.tempararyPriceList.filter(value => (value.stockId === this.customerManualInvoiceForm.value.stockId && value.stockDescriptionId == this.customerManualInvoiceForm.value.stockIdDescription && value.qty == this.customerManualInvoiceForm.value.stockQty));
    let findIndex = this.tempararyPriceList.findIndex(value => (value.stockId === this.customerManualInvoiceForm.value.stockId && value.stockDescriptionId == this.customerManualInvoiceForm.value.stockIdDescription));
    if (found.length > 0) {
      this.snackbarService.openSnackbar('Invoice Item already exist', ResponseMessageTypes.WARNING);
      return;
    }
    let tempData = []
    tempData = this.mainPriceList.filter(value => (value.stockId === this.customerManualInvoiceForm.value.stockId && value.stockDescriptionId == this.customerManualInvoiceForm.value.stockIdDescription));
    if(findIndex === -1){
    tempData.forEach(element => {
      let qty = parseInt(this.qty);
      element.qty = qty;
      element.unitPrice = element.exclVAT;
      element.itemPrice = element.exclVAT * qty;
      element.vatAmount = element.vatAmount * qty
      element.inclVAT = (element.itemPrice + element.vatAmount);
      element.totalPrice = element.inclVAT;
    });
    this.tempararyPriceList = [...this.tempararyPriceList, ...tempData];
  }else{
    tempData.forEach(element => {
      let qty = parseInt(this.qty);
      this.tempararyPriceList[findIndex].qty = qty;
      this.tempararyPriceList[findIndex].unitPrice = element.exclVAT;
      this.tempararyPriceList[findIndex].itemPrice = element.exclVAT * qty;
      this.tempararyPriceList[findIndex].vatAmount = element.vat * qty
      this.tempararyPriceList[findIndex].inclVAT = (this.tempararyPriceList[findIndex].unitPrice + element.vat) * element.qty;
      this.tempararyPriceList[findIndex].totalPrice = this.tempararyPriceList[findIndex].inclVAT;
    });
  }

    this.isStockPriceList = true;

    this.manualInvoiceItems = this.tempararyPriceList

    this.stockPriceList = this.manualInvoiceItems;
    this.calculateTotal();
  }
  // exclVAT: 0, vat: 0, inclVAT:0
  calculateTotal() {
    this.totalSummary = {
      exclVAT: 0, vat: 0, inclVAT: 0
    };
    this.stockPriceList.forEach(element => {
      this.totalSummary.exclVAT += element.exclVAT * element.qty;
      this.totalSummary.vat += element.vatAmount;
      this.totalSummary.inclVAT += element.inclVAT;
    });
  }

  onDelete(idex, obj) {

    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe((dialogResult) => {
      if (dialogResult) {

        this.tempararyPriceList = this.tempararyPriceList.filter(e =>  e.stockDescriptionId != obj.stockDescriptionId)
        // this.tempararyPriceList = this.tempararyPriceList.filter(e => e.stockId != obj.stockId && e.stockDescriptionId != obj.stockDescriptionId)
        this.stockPriceList = this.stockPriceList.filter(e =>  e.stockDescriptionId != obj.stockDescriptionId)
        // this.stockPriceList = this.stockPriceList.filter(e => e.stockId != obj.stockId  && e.stockDescriptionId != obj.stockDescriptionId)
        this.manualInvoiceItems = this.tempararyPriceList
        if(this.stockPriceList.length ==0){
          this.isStockPriceList =false;
        }
        this.calculateTotal();
      }
    });


  }

  uploadFiles(file, docNumber) {
    if (file && file.length == 0)
      return;
    if (docNumber === 'doc1') {
      var numberOfFilesUploaded = this.listOfFiles1.length;
      if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
        this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
        return;
      }

      const supportedExtensions = [
        'jpeg',
        'jpg',
        'png',
        'gif',
        'pdf',
        'doc',
        'docx',
        'xls',
        'xlsx',
      ];

      for (let i = 0; i < file.length; i++) {
        let selectedFile = file[i];
        const path = selectedFile.name.split('.');
        const extension = path[path.length - 1];
        if (supportedExtensions.includes(extension)) {
          let filename = this.fileList1.find(x => x.name === selectedFile.name);
          if (filename == undefined) {
            this.fileList1.push(selectedFile);
            this.listOfFiles1.push(selectedFile.name);
            this.myFileInputField.nativeElement.value = null;
          }
        } else {
          this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
        }
      }
    }
    if (docNumber === 'doc2') {
      var numberOfFilesUploaded = this.listOfFiles2.length;
      if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
        this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
        return;
      }

      const supportedExtensions = [
        'jpeg',
        'jpg',
        'png',
        'gif',
        'pdf',
        'doc',
        'docx',
        'xls',
        'xlsx',
      ];

      for (let i = 0; i < file.length; i++) {
        let selectedFile = file[i];
        const path = selectedFile.name.split('.');
        const extension = path[path.length - 1];
        if (supportedExtensions.includes(extension)) {
          let filename = this.fileList2.find(x => x.name === selectedFile.name);
          if (filename == undefined) {
            this.fileList2.push(selectedFile);
            this.listOfFiles2.push(selectedFile.name);
            this.myFileInputField.nativeElement.value = null;
          }
        } else {
          this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
        }
      }
    }
    if (docNumber === 'doc3') {
      var numberOfFilesUploaded = this.listOfFiles3.length;
      if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
        this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
        return;
      }

      const supportedExtensions = [
        'jpeg',
        'jpg',
        'png',
        'gif',
        'pdf',
        'doc',
        'docx',
        'xls',
        'xlsx',
      ];

      for (let i = 0; i < file.length; i++) {
        let selectedFile = file[i];
        const path = selectedFile.name.split('.');
        const extension = path[path.length - 1];
        if (supportedExtensions.includes(extension)) {
          let filename = this.fileList3.find(x => x.name === selectedFile.name);
          if (filename == undefined) {
            this.fileList3.push(selectedFile);
            this.listOfFiles3.push(selectedFile.name);
            this.myFileInputField.nativeElement.value = null;
          }
        } else {
          this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
        }
      }
    }

  }

  removeSelectedFile(index, str) {
    if (str == 1) {
      this.listOfFiles1.splice(index, 1);
      // delete this.manualInvoiceDocuments[0]
      this.manualInvoiceDocuments.splice(0, 1);

    } else if (str == 2) {
      this.listOfFiles2.splice(index, 1);
      // delete this.manualInvoiceDocuments[1]
      this.manualInvoiceDocuments.splice(1, 1);


    } else {
      this.listOfFiles3.splice(index, 1);
      // delete this.manualInvoiceDocuments[2]
      this.manualInvoiceDocuments.splice(2, 1);


    }
    // this.fileList.splice(index, 1);
  }

  addOption1Doc() {
    this.fileList1Uploaded = false;
    this.fileList1.forEach(file => {
      this.formData.append('file', file);
    });
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_UPLOAD, this.formData, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.fileList1Uploaded = true
          // let index = response.resources.lastIndexOf("/") + 1;
          // let filename = response.resources.substr(index);
          this.documentObj1['documentPath'] = response.resources.documentPath;
          this.documentObj1['documentName'] = response.resources.documentName;
          // this.documentObj1['documentName'] = filename;
          if (this.manualInvoiceId) {
            this.documentObj1['invoiceTransactionDescriptionSubTypeDocumentId'] = this.customerManualInvoiceForm.value.requireDocId;
          }else{
            // this.documentObj1['invoiceTransactionDescriptionSubTypeDocumentId'] = null;
          }
          // this.manualInvoiceDocuments.splice(0,1);
          delete this.manualInvoiceDocuments[0]
          this.manualInvoiceDocuments[0] = this.documentObj1;
          // this.manualInvoiceDocuments.push(this.documentObj1)
          this.formData = new FormData();
        }
      });
  }

  addOption2Doc() {
    this.fileList2Uploaded = false
    this.fileList2.forEach(file => {
      this.formData.append('file', file);
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_UPLOAD, this.formData, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.fileList2Uploaded = true
          // let index = response.resources.lastIndexOf("/") + 1;
          // let filename = response.resources.substr(index);
          this.documentObj2['documentPath'] = response.resources.documentPath;
          this.documentObj2['documentName'] = response.resources.documentName;
          if (this.manualInvoiceId) {
            this.documentObj2['invoiceTransactionDescriptionSubTypeDocumentId'] = this.customerManualInvoiceForm.value.optionalDocId;
          }else{
            // this.documentObj2['invoiceTransactionDescriptionSubTypeDocumentId'] = null;
          }
          // this.manualInvoiceDocuments.splice(1,2);
          delete this.manualInvoiceDocuments[1]
          this.manualInvoiceDocuments[1] = this.documentObj2;
          this.formData = new FormData();

        }
      });
  }

  addOption3Doc() {
    this.fileList3.forEach(file => {
      this.formData.append('file', file);
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE_UPLOAD, this.formData, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          // let index = response.resources.lastIndexOf("/") + 1;
          // let filename = response.resources.substr(index);
          this.documentObj3['documentPath'] = response.resources.documentPath;
          this.documentObj3['documentName'] = response.resources.documentName;
          // this.documentObj3['documentName'] = filename;
          if (this.manualInvoiceId) {
            this.documentObj3['invoiceTransactionDescriptionSubTypeDocumentId'] = this.customerManualInvoiceForm.value.telephonicDocId;
          }else{
            // this.documentObj3['invoiceTransactionDescriptionSubTypeDocumentId'] = null;
          }
          // this.manualInvoiceDocuments.splice(2,3);
          delete this.manualInvoiceDocuments[2]
          this.manualInvoiceDocuments[2] = this.documentObj3;
          this.formData = new FormData();

        }
      });
  }

  qty: any;

  qtyChange(e) {
    this.qty = e.target.value;
  }

  requiredDoc(e) {
    this.documentObj1['invoiceTransactionDescriptionSubTypeDocumentId'] = e.target.value;
  }

  optionDoc(e) {
    this.documentObj2['invoiceTransactionDescriptionSubTypeDocumentId'] = e.target.value;
  }

  telephonicCallDoc(e) {
    this.documentObj3['invoiceTransactionDescriptionSubTypeDocumentId'] = e.target.value;
  }



  onSubmit() {

    if(!this.isStockPriceList){
      !this.customerManualInvoiceForm.get("stockId").value ? this.customerManualInvoiceForm.get("stockId").setErrors({ 'required': true }):"";
      !this.customerManualInvoiceForm.get("stockIdDescription").value ? this.customerManualInvoiceForm.get("stockIdDescription").setErrors({ 'required': true }):"";
      !this.customerManualInvoiceForm.get("stockQty").value ? this.customerManualInvoiceForm.get("stockQty").setErrors({ 'required': true }):"";
    }

    if (this.customerManualInvoiceForm.invalid) {
      return
    }
    this.customerManualInvoiceForm.get("optionalDocId").setValue(this.customerManualInvoiceForm.get("requireDocId").value);
    if(!this.fileList1Uploaded && this.fileList1.length !=0){
      return  this.snackbarService.openSnackbar("Please Upload Selected Required Documentation",ResponseMessageTypes.WARNING)
    }

    if(!this.fileList2Uploaded && this.fileList2.length !=0){
      return  this.snackbarService.openSnackbar("Please Upload Selected Optional/Telephonic Documentation",ResponseMessageTypes.WARNING)
    }

    if(!this.fileList1Uploaded && this.fileList1.length !=0 &&  !this.customerManualInvoiceForm.get('requireDocId').value){
      return  this.snackbarService.openSnackbar("Please select support - Required documentation",ResponseMessageTypes.WARNING)
    }

    if(!this.fileList2Uploaded && this.fileList2.length !=0 &&  this.customerManualInvoiceForm.get('requireDocId').value ==""){
      return  this.snackbarService.openSnackbar("Please select support - Required documentation",ResponseMessageTypes.WARNING)
    }

    this.customerManualInvoiceForm.get('createdUserId').setValue(this.loggedUser.userId);
    this.customerManualInvoiceForm.get('siteAddressId').setValue(this.addressId);
    this.customerManualInvoiceForm.get('contractId').setValue(this.contractId);

    let finalObj = this.customerManualInvoiceForm.value;
    finalObj.manualInvoiceItems = this.manualInvoiceItems;
    let _manualDoc = [];

    this.manualInvoiceDocuments.forEach(element => {
      if(element.documentName){
      _manualDoc.push(element);
      }
    });
    finalObj.manualInvoiceDocuments = _manualDoc;
    // finalObj.manualInvoiceDocuments = this.manualInvoiceDocuments;
    // return;
    if (this.manualInvoiceId) {
      finalObj.manualInvoiceId = this.manualInvoiceId;
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.update(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE, finalObj, 1)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            // this.getManualInvoiceById();
            this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } });

          }
        });
    } else {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.MANUAL_INVOICE, finalObj, 1)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            // this.getManualInvoiceDetails();
            this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } });


          }
        });
    }
  }

  cancelClick() {
    this.router.navigate(['customer/manage-customers/view-transactions'], { queryParams: { id: this.customerId, debtorId: this.debtorId, addressId: this.addressId } });

  }

}
