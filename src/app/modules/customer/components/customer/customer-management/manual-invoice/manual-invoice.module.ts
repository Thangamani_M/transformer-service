import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ManualInvoiceApproveDialogModule } from "./manual-invoice-approve-dialog/manual-invoice-approve-dialog.module";
import { ManualInvoiceComponent } from "./manual-invoice.component";



@NgModule({
    declarations: [ManualInvoiceComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    ManualInvoiceApproveDialogModule,
    RouterModule.forChild([
        {
            path: '', component: ManualInvoiceComponent, data: { title: 'Manual Invoice' }
        },
    ])
  ],
  entryComponents: [],
})
export class ManualInvoiceModule { }