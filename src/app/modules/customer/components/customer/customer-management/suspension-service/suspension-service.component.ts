import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CrudType, getMatMultiData, getNthDate, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { TicketSuspendServicesDetailsModel, TicketSuspendServiceDetailsModel, CustomerTicketAddEditComponent, UpgradeLeadDialogComponent } from '@modules/customer';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'app-suspension-service',
  templateUrl: './suspension-service.component.html',
  styleUrls: ['./suspension-service.component.scss']
})
export class SuspensionServiceComponent implements OnInit {

  selectedTabIndex: any = 0;
  primengTableConfigProperties: any;
  customerId: any;
  addressId: any;
  contractId: any;
  serviceSuspendId: any;
  suspendServicelationForm: FormGroup;
  contractList: any = [];
  serviceSuspendReasonList: any = [];
  serviceSuspendSubReasonList: any = [];
  reinstateServiceDate: any = new Date();
  billStartDate: any = new Date();
  suspendServiceDate: any = new Date();
  contractLastBillDate: any = new Date();
  submitted: boolean;
  loading: boolean;
  viewable: boolean;
  dataList: any;
  status: any = [];
  selectedRows: string[] = [];
  totalRecords: any = 0;
  userSubscription: any;
  isViewMore: boolean;
  isEquipement: boolean;
  listSubscription: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  searchColumns: any;
  observableResponse: any;
  isShowNoRecords: any = true;
  selectedRowData: any;
  leadCategoryList: any = [];
  sourceTypeList: any = [];
  customerAddressList: any = [];
  isApprover: any = false;
  feature: string;
  featureIndex: string;
  isLoading: boolean;

  constructor(private crudService: CrudService, private dialog: MatDialog, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private router: Router, private store: Store<AppState>, private formBuilder: FormBuilder, private datePipe: DatePipe, private dialogService: DialogService,) {
    this.activatedRoute.queryParamMap.subscribe((params: any) => {
      this.customerId = params?.params?.customerId ? params?.params?.customerId : '';
      this.addressId = params?.params?.addressId ? params?.params?.addressId : '';
      this.contractId = params?.params?.id ? params?.params?.id : '';
      this.viewable = params?.params?.viewable ? params?.params?.viewable : false;
      this.serviceSuspendId = params?.params?.serviceSuspendId ? params?.params?.serviceSuspendId : '';
      this.isApprover = params?.params?.isApprover ? params?.params?.isApprover : false;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Create Suspension",
      breadCrumbItems: [{ displayName: 'Customer Management:Dashboard', relativeRouterUrl: '' },
      { displayName: 'View Customer', relativeRouterUrl: `/customer/manage-customers/view/${this.customerId}`, queryParams: { addressId: this.addressId, navigateTab: 6, monitoringTab: 0 }, isSkipLocationChange: true }, { displayName: 'Create Suspension' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Create Suspension',
            dataKey: 'serviceId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'contractRefNo', header: ' Contract No', width: '80px' },
            { field: 'billId', header: 'Bill ID', width: '50px' },
            { field: 'serviceName', header: 'Service', width: '60px' },
            { field: 'suspendServiceDate', header: 'Termination Date', width: '100px', isDate: true },
            { field: 'contractStartDate', header: 'Contract Start Date', width: '100px', isDateTime: true },
            { field: 'billEndDate', header: 'Bill End Date', width: '100px', isDateTime: true },
            { field: 'contractPeriodName', header: 'ICP', width: '90px' },
            { field: 'paymentFrequency', header: 'Freq', width: '50px' },
            { field: 'balanceOfContract', header: 'Balance of Contract', width: '100px' },
            { field: 'contractStatus', header: 'Contract Status', width: '70px', isStatus: true, statusKey: 'statusClass' },],
            apiSuffixModel: SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_CONTRACT_DETAIL,
            moduleName: ModulesBasedApiSuffix.SALES,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }
    if (this.feature && this.featureIndex) {
      this.primengTableConfigProperties.breadCrumbItems[1].queryParams = { addressId: this.addressId, feature_name: this.feature, featureIndex: this.featureIndex, navigateTab: 6, monitoringTab: 0 };
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.onLoadValue();
  }

  onLoadValue() {
    this.loading = true;
    this.isShowNoRecords = false;
    this.dataList = [];
    this.selectedRows = [];
    if (this.viewable) {
      this.primengTableConfigProperties.tableCaption = 'View Suspension';
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = 'View Suspension';
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = 'Cancellation History';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/customer/manage-customers/ticket/cancellation-history';
      this.primengTableConfigProperties.breadCrumbItems[1].queryParams = { id: this.contractId, customerId: this.customerId, addressId: this.addressId };
      if (this.feature && this.featureIndex) {
        this.primengTableConfigProperties.breadCrumbItems[1].queryParams = { id: this.contractId, customerId: this.customerId, addressId: this.addressId, feature_name: this.feature, featureIndex: this.featureIndex };
      }
    }
    this.onLoadAllValue();
  }

  onLoadAllValue() {
    let api = [
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_SUBSCRIPTION_CANCEL_CONTRACTS, prepareRequiredHttpParams({ customerId: this.customerId, siteAddressId: this.addressId })),
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_SERVICE_SUSPEND_REASON),
    ]
    if (this.viewable) {
      api.push(
        this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_SUSPEND_DETAIL, null, false, prepareRequiredHttpParams({ serviceSuspendId: this.serviceSuspendId }))
          .pipe(map((res: IApplicationResponse) => {
            if (res?.isSuccess && res?.statusCode == 200 && res?.resources?.contracts) {
              res?.resources?.contracts?.forEach(val => {
                val.suspendServiceDate = val.suspendServiceDate ? this.datePipe.transform(val.suspendServiceDate, 'dd-MM-yyyy') : '';
                val.contractStartDate = val.contractStartDate ? this.datePipe.transform(val.contractStartDate, 'dd-MM-yyyy') : '';
                val.billEndDate = val.billEndDate ? this.datePipe.transform(val.billEndDate, 'dd-MM-yyyy') : '';
                val.statusClass = val.contractStatus?.toLowerCase() == 'active' ? 'status-label-green' : val.contractStatus?.toLowerCase() == 'inactive' || val.contractStatus?.toLowerCase() == 'in-active' ? 'status-label-pink' : '';
                return val;
              });
            }
            return res;
          })),
        this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_LEAD_CATEGORY),
        this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SOURCE_TYPES),
        this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
          SalesModuleApiSuffixModels.CURRENT_ADDRESS_INFO, undefined, false, prepareRequiredHttpParams({
            customerId: this.customerId
          }))
      )
    } else {
      let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
      salesModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
      const otherParams = { CustomerId: this.customerId, SiteAddressId: this.addressId, contractId: this.contractId };
      api.push(this.crudService.get(
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        salesModuleApiSuffixModels,
        undefined,
        false, prepareGetRequestHttpParams('0', '10', otherParams))
      )
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    forkJoin(api)?.subscribe((result: IApplicationResponse[]) => {
      result?.forEach(((res, ix) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          switch (ix) {
            case 0:
              this.contractList = res?.resources;
              this.setContactRefNo();
              break;
            case 1:
              this.serviceSuspendReasonList = res?.resources;
              break;
            case 2:
              if (this.viewable) {
                this.onPatchValue(res);
              } else {
                this.onFormSetValue();
                this.onLoadTableData(res);
              }
              break;
            case 3:
              this.leadCategoryList = getMatMultiData(res?.resources);
              break;
            case 4:
              res?.resources?.forEach((el, i) => {
                this.sourceTypeList.push({
                  label: el?.sourceGroupName,
                  items: [],
                });
                el?.sources?.forEach((el1, j) => {
                  this.sourceTypeList[i].items.push({
                    label: el1?.displayName, value: el1?.id
                  })
                });
              });
              break;
            case 5:
              this.customerAddressList = res?.resources[0];
              break;
          }
        }
      }))
      this.loading = false;
      // this.rxjsService.setGlobalLoaderProperty(false);
      this.isLoading = false;
    })
  }

  initForm(ticketSuspendServiceDetailsModel?: TicketSuspendServiceDetailsModel) {
    let ticketSuspendServiceModel = new TicketSuspendServiceDetailsModel(ticketSuspendServiceDetailsModel);
    this.suspendServicelationForm = this.formBuilder.group({});
    Object.keys(ticketSuspendServiceModel).forEach((key) => {
      if (typeof ticketSuspendServiceModel[key] === 'object') {
        this.suspendServicelationForm.addControl(key, new FormArray(ticketSuspendServiceModel[key]));
      } else {
        this.suspendServicelationForm.addControl(key, new FormControl(ticketSuspendServiceModel[key]));
      }
    });
    this.suspendServicelationForm = setRequiredValidator(this.suspendServicelationForm, ["contractId", "suspendServiceDate",
      "reinstateServiceDate", "billStartDate", "contractLastBillDate", "serviceSuspendReasonId", "serviceSuspendSubReasonId",
      "isEquipmentRemovalRequired", "isSiteVisitRequired", "notes", "services"]);
    this.onValueChanges();
  }

  onAfterChangeAddress(e) {
    this.addressId = e;
    // this.getContracts();
  }

  get getServicesArray(): FormArray {
    if (!this.suspendServicelationForm) return;
    return this.suspendServicelationForm?.get('services') as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onFormSetValue() {
    this.suspendServicelationForm.patchValue({
      contractId: this.contractId,
      contractLastBillDate: new Date(),
      billStartDate: getNthDate(new Date(), 1),
      createdUserId: this.loggedInUserData?.userId,
      customerId: this.customerId,
      siteAddressId: this.addressId,
    })
    this.suspendServicelationForm.get('contractLastBillDate').disable();
    this.suspendServicelationForm.get('billStartDate').disable();
  }

  onValueChanges() {
    this.suspendServicelationForm.get("serviceSuspendReasonId").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_SERVICE_SUSPEND_SUB_REASON, prepareRequiredHttpParams({ serviceSuspendReasonId: res })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response?.statusCode == 200 && response?.resources) {
            this.serviceSuspendSubReasonList = response.resources;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
    this.suspendServicelationForm.get("suspendServiceDate").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.reinstateServiceDate = getNthDate(res, 1);
      }
    });
  }

  setContactRefNo() {
    const contractRefNo = this.contractList?.find(el => el?.id == this.contractId);
    this.suspendServicelationForm.get('contractRefNo').setValue(contractRefNo?.displayName);
    this.suspendServicelationForm.get('contractRefNo').disable();
  }

  combineLatestNgrxStoreData() {
    this.userSubscription = combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search && !e?.col) {
      if (e?.search?.sortOrderColumn == 'isSelectedService') {
        return;
      }
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {})
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.isShowNoRecords = false;
    this.dataList = [];
    this.selectedRows = [];
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = { CustomerId: this.customerId, SiteAddressId: this.addressId, contractId: this.contractId, ...otherParams };
    let api = this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      salesModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams));
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
    }
    this.listSubscription = api.pipe(map((res: IApplicationResponse) => {
      if (res?.resources && res?.statusCode == 200 && this.selectedTabIndex == 0) {
        res?.resources?.forEach(val => {
          val.suspendServiceDate = val.suspendServiceDate ? this.datePipe.transform(val.suspendServiceDate, 'dd-MM-yyyy') : '';
          val.contractStartDate = val.contractStartDate ? this.datePipe.transform(val.contractStartDate, 'dd-MM-yyyy') : '';
          val.billEndDate = val.billEndDate ? this.datePipe.transform(val.billEndDate, 'dd-MM-yyyy') : '';
          val.isSelectedService = val.suspendServiceDate ? true : false;
          val.statusClass = val.contractStatus?.toLowerCase() == 'active' ? 'status-label-green' : val.contractStatus?.toLowerCase() == 'inactive' || val.contractStatus?.toLowerCase() == 'in-active' ? 'status-label-pink' : '';
          val.checkHidden = val.isSelectedService;
          if (this.serviceSuspendId) {
            const filterValue = this.getServicesArray.controls?.find(el => el?.get('serviceId').value == val?.serviceId);
            val.checkedValue = filterValue ? true : false;
          }
          return val;
        });
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.onLoadTableData(data);
    })
  }

  onLoadTableData(data) {
    if (data.isSuccess && data?.statusCode == 200) {
      data?.resources?.forEach(el => {
        if (el?.isSelectedService) {
          this.selectedRows.push(el);
        }
      });
      this.observableResponse = data?.resources;
      this.dataList = this.observableResponse;
      this.totalRecords = data?.totalCount;
      this.isShowNoRecords = this.dataList?.length ? false : true;
    } else {
      this.observableResponse = null;
      this.dataList = this.observableResponse
      this.totalRecords = 0;
      this.isShowNoRecords = true;
    }
    this.loading = false;
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        // this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["../add-edit"], { relativeTo: this.activatedRoute });
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['../view'], { relativeTo: this.activatedRoute, queryParams: { id: editableObject['wdRequestId'] } });
            break;
        }
    }
  }

  initSuspendServiceFormArray(ticketSuspendServicesDetailsModel?: TicketSuspendServicesDetailsModel) {
    let ticketSuspendServicesModel = new TicketSuspendServicesDetailsModel(ticketSuspendServicesDetailsModel);
    let ticketSuspendServicesFormArray = this.formBuilder.group({});
    Object.keys(ticketSuspendServicesModel).forEach((key) => {
      ticketSuspendServicesFormArray.addControl(key, new FormControl({ value: ticketSuspendServicesModel[key], disabled: true }));
    });
    this.getServicesArray.push(ticketSuspendServicesFormArray);
  }

  onShowViewPage(e) {
    this.isViewMore = e;
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
    this.clearFormArray(this.getServicesArray);
    if (this.selectedRows?.length) {
      this.selectedRows.forEach((el: any) => {
        if (!el?.suspendServiceDate) {
          this.initSuspendServiceFormArray({
            serviceSuspendDetailId: '',
            serviceId: el?.serviceId,
          })
        }
      });
    }
  }

  onBack() {
    if (this.serviceSuspendId && !this.getDOASubs()) {
      let queryParamsCancelHistory = { id: this.suspendServicelationForm.get('contractId').value, customerId: this.customerId, addressId: this.addressId };
      if (this.feature && this.featureIndex) {
        queryParamsCancelHistory['feature_name'] = this.feature;
        queryParamsCancelHistory['featureIndex'] = this.featureIndex;
      }
      this.router.navigate(['./customer/manage-customers/ticket/cancellation-history'], { queryParams: queryParamsCancelHistory });
    } else if (this.serviceSuspendId && this.getDOASubs()) {
      this.router.navigate(['./my-tasks/my-customer'], { queryParams: { tab: 1 } });
    } else {
      this.rxjsService.setViewCustomerData({
        customerId: this.customerId,
        addressId: this.addressId,
        feature_name: this.feature,
        featureIndex: this.featureIndex,
        customerTab: 6,
        monitoringTab: null,
      })
      this.rxjsService.navigateToViewCustomerPage();
    }
  }

  onReinstateSubscription() {
    const message = `Are you sure you want to Reinitstate this Suspension?`;
    const dialogData = new ConfirmDialogModel("Suspension Request", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      const reqObj = {
        serviceSuspendId: this.serviceSuspendId,
        modifiedUserId: this.loggedInUserData?.userId,
      }
      this.submitted = true;
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_SUSPEND_REINSTATE_SUBSCRIPTION, reqObj)
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.onBack();
          }
          this.submitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    })
  }

  onRetractSubscription() {
    const message = `Are you sure you want to Retract this Suspension?`;
    const dialogData = new ConfirmDialogModel("Suspension Request", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      const reqObj = {
        serviceSuspendId: this.serviceSuspendId,
        modifiedUserId: this.loggedInUserData?.userId,
      }
      this.submitted = true;
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_SUSPEND_RETRACT_SUBSCRIPTION, reqObj)
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.onBack();
          }
          this.submitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    })
  }

  openSuspendSubscription() {
    if (this.suspendServicelationForm.invalid) {
      this.suspendServicelationForm.markAllAsTouched();
      return;
    }
    const message = `Are you sure want to Suspension?`;
    const dialogData = new ConfirmDialogModel("Suspension Request", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      this.onSuspendService();
    })
  }

  openApprove() {
    if (this.suspendServicelationForm.invalid) {
      this.suspendServicelationForm.markAllAsTouched();
      return;
    }
    const message = `Are you sure want to Approve Suspension?`;
    const dialogData = new ConfirmDialogModel("Suspension Request", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      const reqObj = {
        serviceSuspendId: this.serviceSuspendId,
        modifiedUserId: this.loggedInUserData?.userId,
      }
      this.submitted = true;
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_SUSPEND_DOA_APPROVAL, reqObj)
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.onBack();
          }
          this.submitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    })
  }

  openDecline() {
    if (this.suspendServicelationForm.invalid) {
      this.suspendServicelationForm.markAllAsTouched();
      return;
    }
    const message = `Are you sure want to Decline Suspension?`;
    const dialogData = new ConfirmDialogModel("Suspension Request", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      const reqObj = {
        serviceSuspendId: this.serviceSuspendId,
        modifiedUserId: this.loggedInUserData?.userId,
      }
      this.submitted = true;
      this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SERVICE_SUSPEND_DOA_DECLINE, reqObj)
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.onBack();
          }
          this.submitted = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    })
  }

  onSuspendService() {
    if (this.getServicesArray?.length == 0) {
      this.snackbarService.openSnackbar("Please select atleast one service", ResponseMessageTypes.WARNING);
      return;
    }
    const reqObj = {
      ...this.suspendServicelationForm.getRawValue()
    }
    reqObj['reinstateServiceDate'] = this.suspendServicelationForm.get('reinstateServiceDate').value?.toISOString();
    reqObj['suspendServiceDate'] = reqObj['suspendServiceDate'].toISOString();
    reqObj['contractLastBillDate'] = reqObj['contractLastBillDate'].toISOString();
    reqObj['billStartDate'] = reqObj['billStartDate'].toISOString();
    reqObj['customerId'] = this.customerId;
    reqObj['siteAddressId'] = this.addressId;
    reqObj['services'] = [];
    this.getServicesArray.controls?.forEach((el: any) => {
      reqObj['services'].push({ serviceSuspendDetailId: '', serviceId: el?.get('serviceId').value, })
    });
    this.submitted = true;
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_SUSPEND, reqObj)
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.selectedRowData = [];
          this.serviceSuspendId = res?.resources;
          this.viewable = true;
          // this.onLoadValue();
          let queryParamsSuspendService = { id: this.contractId, customerId: this.customerId, addressId: this.addressId, serviceSuspendId: this.serviceSuspendId, viewable: this.viewable };
          if (this.feature && this.featureIndex) {
            queryParamsSuspendService['feature_name'] = this.feature;
            queryParamsSuspendService['featureIndex'] = this.featureIndex;
          }
          this.router.navigate(['/customer/manage-customers/ticket/suspend-service/view'], { queryParams: queryParamsSuspendService });
        }
        this.submitted = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onPatchValue(res) {
    if (res?.isSuccess && res?.statusCode == 200) {
      this.suspendServicelationForm.reset();
      this.suspendServicelationForm.patchValue({
        reinstateServiceDate: res?.resources?.reinstateServiceDate ? new Date(res?.resources?.reinstateServiceDate) : '',
        suspendServiceDate: res?.resources?.suspendServiceDate ? new Date(res?.resources?.suspendServiceDate) : '',
        contractLastBillDate: res?.resources?.contractLastBillDate ? new Date(res?.resources?.contractLastBillDate) : '',
        billStartDate: res?.resources?.billStartDate ? new Date(res?.resources?.billStartDate) : '',
        serviceSuspendReasonId: res?.resources?.serviceSuspendReasonId,
        serviceSuspendSubReasonId: res?.resources?.serviceSuspendSubReasonId,
        isEquipmentRemovalRequired: res?.resources?.isEquipmentRemovalRequired,
        isSiteVisitRequired: res?.resources?.isSiteVisitRequired,
        status: res?.resources?.status ? res?.resources?.status : '',
        notes: res?.resources?.notes,
        customerId: res?.resources?.customerId,
        siteAddressId: res?.resources?.siteAddressId,
        createdUserId: res?.resources?.createdUserId,
        leadId: res?.resources?.leadId,
        leadRefNo: res?.resources?.leadRefNo,
        ticketId: res?.resources?.ticketId,
        ticketRefNo: res?.resources?.ticketRefNo,
        actionTypeID: res?.resources?.actionTypeID,
        contractId: res?.resources?.contractId,
      }, { emitEvent: false });
      res?.resources?.services?.forEach(el => {
        this.initSuspendServiceFormArray({
          serviceSuspendDetailId: el?.serviceSuspendDetailId,
          serviceId: el?.serviceId,
        });
      });
      this.setContactRefNo();
      this.dataList = res?.resources?.contracts;
      this.totalRecords = 0;
      this.isShowNoRecords = this.dataList?.length ? false : true;
      this.suspendServicelationForm.get('serviceSuspendReasonId').patchValue(res?.resources?.serviceSuspendReasonId);
      this.suspendServicelationForm.get('isEquipmentRemovalRequired').patchValue(res?.resources?.isEquipmentRemovalRequired);
      this.suspendServicelationForm.get('isSiteVisitRequired').patchValue(res?.resources?.isSiteVisitRequired);
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].checkBox = false;
      this.suspendServicelationForm.disable({ emitEvent: false });
    }
  }

  getRetractSubs() {
    return this.suspendServicelationForm.get('status')?.value?.toLowerCase() == 'future supension' || (this.suspendServicelationForm.get('status')?.value?.toLowerCase() == 'doa pending' && this.isApprover == false) || this.suspendServicelationForm.get('status')?.value?.toLowerCase() == 'doa approved-fs';
  }

  getReinstateSubs() {
    return this.suspendServicelationForm.get('status')?.value?.toLowerCase() == 'suspended';
  }

  getDOASubs() {
    return this.suspendServicelationForm.get('status')?.value?.toLowerCase() == 'doa pending' && this.isApprover == 'true';
  }

  getHeight() {
    return this.isViewMore ? '35vh' : '48vh';
  }

  onRedirectToPage(val) {
    if (val == 'ticket') {
      this.router.navigate(['/customer/manage-customers/ticket/view'], {
        queryParams: {
          id: this.suspendServicelationForm.get("ticketId").value, customerId: this.customerId,
          isSuspendTicket: true,
          addressId: this.addressId, 
          serviceSuspendId: this.serviceSuspendId,
          fromComponent: 'Tickets',
        }
      });
    } else if (val == 'lead') {
      this.router.navigate(['/sales/task-management/leads/lead-info/view'], { queryParams: { leadId: this.suspendServicelationForm.get("leadId").value, } });// selectedIndex:1
    }
  }

  onServiceCallTicket() {
    this.openTicketModal(this.suspendServicelationForm?.get('actionTypeID').value);
  }

  openTicketModal(ticketTypeId) {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialogService.open(CustomerTicketAddEditComponent, { baseZIndex: 1000,
      width: '700px', showHeader: false,
      data: {
        custId: this.customerId, addressId: this.addressId, ticketTypeId: ticketTypeId, isDisableTickeTypeId: true,
        isSuspendTicket: true, serviceSuspendId: this.serviceSuspendId, sourceTypeList: this.sourceTypeList, leadCategoryList: this.leadCategoryList
      }
    });
    dialogReff.onClose.subscribe(result => {
      if (result) {
        this.suspendServicelationForm?.get('ticketRefNo').setValue(result?.refNo);
        this.suspendServicelationForm?.get('ticketId').setValue(result?.id);
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  openLeadUpgradeModal() {
    this.rxjsService.setDialogOpenProperty(true);
    const ref = this.dialogService.open(UpgradeLeadDialogComponent, {
      header: 'Create Lead Upgrade',
      baseZIndex: 1000,
      width: '700px',
      closable: false,
      showHeader: false,
      data: {
        leadCategoryList: this.leadCategoryList, sourceTypeList: this.sourceTypeList, serviceSuspendId: this.serviceSuspendId,
        row: {
          addressName: this.customerAddressList?.address, customerId: this.customerId, createdUserId: this.loggedInUserData?.userId,
          addressId: this.addressId, titleId: this.customerAddressList?.titleId, firstName: this.customerAddressList?.firstName, lastName: this.customerAddressList?.lastName,
        }
      }
    });
    ref.onClose.subscribe(result => {
      if (result) {
        this.suspendServicelationForm?.get('leadRefNo').setValue(result?.refNo);
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }
}
