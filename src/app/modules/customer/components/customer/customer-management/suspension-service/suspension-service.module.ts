import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SuspensionServiceComponent } from "@modules/customer";
import { CustomerTicketViewPageModule } from "../customer-ticket-view-page/customer-ticket-view-page.module";
import { CustomerTicketAddEditModule } from "../customer-ticket/customer-ticket-add-edit.module";
import { UpgradeLeadDialogModule } from "./upgrade-lead-dialog/upgrade-lead-dialog.module";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [SuspensionServiceComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule,
    CustomerTicketViewPageModule,
    CustomerTicketAddEditModule,
    UpgradeLeadDialogModule,
    RouterModule.forChild([
      {path: '', component: SuspensionServiceComponent, data: {title: 'Create Suspension'}, canActivate:[AuthGuard]},
      {path: 'view', component: SuspensionServiceComponent, data: {title: 'View Suspension'}, canActivate:[AuthGuard]},
    ])
  ],
  exports: [],
  entryComponents: [],
})
export class SuspensionServiceModule { }