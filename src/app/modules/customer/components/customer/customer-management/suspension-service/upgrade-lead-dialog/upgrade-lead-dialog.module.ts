import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { UpgradeLeadDialogComponent } from "@modules/customer";

@NgModule({
  declarations: [UpgradeLeadDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule,
  ],
  exports: [UpgradeLeadDialogComponent],
  entryComponents: [UpgradeLeadDialogComponent],
})
export class UpgradeLeadDialogModule { }