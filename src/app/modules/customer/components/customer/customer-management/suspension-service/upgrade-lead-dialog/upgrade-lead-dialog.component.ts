import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { UpgradeLeadDetailModel } from '@modules/customer';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-upgrade-lead-dialog',
  templateUrl: './upgrade-lead-dialog.component.html',
  styleUrls: ['./upgrade-lead-dialog.component.scss']
})
export class UpgradeLeadDialogComponent implements OnInit {

  upgradeLeadDialogForm: FormGroup;
  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, private datePipe: DatePipe,
    public ref: DynamicDialogRef, private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService,) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.initForm();
  }

  btnCloseClick() {
    this.upgradeLeadDialogForm.reset();
    this.ref.close(false);
  }

  initForm(upgradeLeadFormModel?: UpgradeLeadDetailModel) {
    let upgradeLeadModel = new UpgradeLeadDetailModel(upgradeLeadFormModel);
    this.upgradeLeadDialogForm = this.formBuilder.group({});
    Object.keys(upgradeLeadModel).forEach((key) => {
      if (typeof upgradeLeadModel[key] === 'object') {
        this.upgradeLeadDialogForm.addControl(key, new FormArray(upgradeLeadModel[key]));
      } else {
        this.upgradeLeadDialogForm.addControl(key, new FormControl(this.config?.data?.row ? this.config?.data?.row[key] : upgradeLeadModel[key]));
      }
    });
    this.upgradeLeadDialogForm = setRequiredValidator(this.upgradeLeadDialogForm, ["sourceId", "leadCategoryId"]);
    this.upgradeLeadDialogForm.get('addressName').disable();
  }

  onSubmitDialog() {
    if (this.upgradeLeadDialogForm?.invalid) {
      return;
    }
    const reqObj = {
      serviceSuspendId: this.config?.data?.serviceSuspendId,
      leadUpgradePostDTO: {
        ...this.upgradeLeadDialogForm?.getRawValue()
      }
    };
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SUBSCRIPTION_SUSPEND_UPGRADE, reqObj)
    .subscribe((res: IApplicationResponse) => {
      if(res?.isSuccess && res?.statusCode == 200) {
        this.ref.close(res?.resources);
      }
    })
  }
}
