import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ZoneSuspendModelComponent } from "./zone-suspend-model.component";

@NgModule({
  declarations: [ZoneSuspendModelComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [ZoneSuspendModelComponent],
  entryComponents: [ZoneSuspendModelComponent],
})
export class ZoneSuspendModelModule { }