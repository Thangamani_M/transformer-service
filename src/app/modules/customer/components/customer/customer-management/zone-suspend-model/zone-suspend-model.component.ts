import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, HttpCancelService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { combineLatest, Observable } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-zone-suspend-model',
  templateUrl: './zone-suspend-model.component.html',
})
export class ZoneSuspendModelComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {};
  customerAddressId='';
  customerId='';
  loggedUser: UserLogin;
  zoneSuspendForm: FormGroup;
  ZoneTime;
  minDateTODate: Date;
  minDateTODate1: Date;
  minDateTODate2: Date;
  TimeFormat: number;
  timeDelay: any;
  saDateTime: any;

  constructor(private crudService: CrudService,
    private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private httpCancelService: HttpCancelService,
    private rxjsService: RxjsService, private _fb: FormBuilder,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private datePipe: DatePipe, private momentService: MomentService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Credit Control', relativeRouterUrl: '' }, { displayName: 'Vendor Registration' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'occurrenceBookSignalId',
            enableBreadCrumb: true,
            enableScrollable: true,
            checkBox: true,
            enableStatusActiveAction: true,
            enableFieldsSearch: true,
            columns: [{ field: 'signalDateTime', header: 'Time Of Signal', width: '250px' },
            { field: 'alarmType', header: 'Alarm', width: '150px' },
            { field: 'description', header: 'Alarm Desc', width: '150px' },
            { field: 'zoneNo', header: 'ZoneCode', width: '150px' },
            { field: 'zoneDescription', header: 'ZoneDescription', width: '150px' },
            { field: 'rawSignal', header: 'Raw Data', width: '150px' },
            { field: 'decoder', header: 'Decoder', width: '150px' },
            { field: 'accountCode', header: 'Transmitter', width: '150px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.OCCURANCE_SIGNALS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
          },
        ]
      }
    }
    this.rxjsService.getCustomerAddresId()
      .subscribe(data => {
        this.customerAddressId = data
      });
    this.rxjsService.getCustomerDate().subscribe(data => {
      this.customerId = data['customerId']
    });
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.zoneSuspendForm = this._fb.group({
      createdUserId: [this.loggedUser.userId],
      occurrenceBookSignals: [[],],
      zoneSuspendToDateTime: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.getZoneSuspendDelayTime();
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] });
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let obj1 = {
      CustomerAddressId: this.customerAddressId,
      CustomerId: this.customerId
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    let EventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    EventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.dataList.map(item => {
          item.signalDateTime = this.datePipe.transform(item?.signalDateTime, 'dd-MM-yyyy, HH:mm:ss');
        })
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
      this.loading = false;
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj)
        break;
    }
  }

  getZoneSuspendDelayTime(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    const params = { customerId: this.customerId, CustomerAddressId: this.customerAddressId }
    otherParams = { ...otherParams, ...params };
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.ZONE_SUSPEND_DELAY_MINS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.ZoneTime = response.resources;
        this.timeDelay = response?.resources?.timeDelay;
        this.saDateTime = this.datePipe.transform(response?.resources?.currentTime, 'dd-MM-yyyy, HH:mm:ss');
        if (this.timeDelay >= 60) {
          this.TimeFormat = this.timeDelay / 60;
          if (24 >= this.TimeFormat) {
            this.minDateTODate = new Date();
            this.minDateTODate1 = new Date(this.minDateTODate.getFullYear(), this.minDateTODate.getMonth(), this.minDateTODate.getDate(), this.minDateTODate.getHours() + 3, this.minDateTODate.getMinutes() + 30);
            this.minDateTODate2 = new Date(this.minDateTODate.getFullYear(), this.minDateTODate.getMonth(), this.minDateTODate.getDate(), this.minDateTODate.getHours() + this.TimeFormat + 3, this.minDateTODate.getMinutes() + 30);
          }
          else {
            let dateTime = this.TimeFormat / 24
            this.minDateTODate = new Date();
            this.minDateTODate1 = new Date(this.minDateTODate.getFullYear(), this.minDateTODate.getMonth(), this.minDateTODate.getMonth(), this.minDateTODate.getHours() + 3, this.minDateTODate.getMinutes() + 30);
            this.minDateTODate2 = new Date(this.minDateTODate.getFullYear(), this.minDateTODate.getMonth(), this.minDateTODate.getDate() + dateTime, this.minDateTODate.getHours() + this.TimeFormat + 3, this.minDateTODate.getMinutes() + 30);
          }
        }
        else {
          this.minDateTODate = new Date();
          this.minDateTODate1 = new Date(this.minDateTODate.getFullYear(), this.minDateTODate.getMonth(), this.minDateTODate.getDate());
          this.minDateTODate2 = new Date(this.minDateTODate.getFullYear(), this.minDateTODate.getMonth(), this.minDateTODate.getDate(), this.minDateTODate.getHours(), this.minDateTODate + this.timeDelay);
        }
      }
      this.rxjsService.setPopupLoaderProperty(false);
    });
  }

  onSubmit() {
    if (this.zoneSuspendForm.invalid) {
      return;
    }
    if (this.selectedRows.length == 0) {
      this.snackbarService.openSnackbar("Please select atleast one item to suspend", ResponseMessageTypes.WARNING);
      return;
    }
    let formValue = this.zoneSuspendForm.value;
    var occurrenceBookSignals = [];
    formValue.createdUserId = formValue.createdUserId,
      formValue.zoneSuspendToDateTime = this.momentService.toDateTime(this.zoneSuspendForm.get('zoneSuspendToDateTime').value);
    this.selectedRows.forEach(element => {
      occurrenceBookSignals = element['occurrenceBookSignalId'];
      formValue.occurrenceBookSignals.push(occurrenceBookSignals);
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ZONE_SUSPEND_POST_API, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.zoneSuspendForm.reset();
        this.ref.close();
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {});
    }
  }

  cancelDialog() {
    this.ref.close();
  }

  onSelectedRows(e) {
    this.selectedRows = e;
  }
}
