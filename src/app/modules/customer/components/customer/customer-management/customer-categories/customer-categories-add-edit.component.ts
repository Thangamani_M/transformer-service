import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CustomerCategoriesModel } from '@modules/customer/models';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { Observable } from 'rxjs';
@Component({
    selector: 'app-customer-categories-add-edit',
    templateUrl: './customer-categories-add-edit.component.html',
})

export class CustomerCategoriesAddEditComponent implements OnInit {
    formConfigs = formConfigs;
    stringConfig = new CustomDirectiveConfig({ isAStringOnly: true, shouldPasteKeyboardEventBeRestricted: true });
    customerCategoriesAddEditForm: FormGroup;
    loggedUser: UserLogin;
    categories: any
    custom_categories: any
    fieldType = "text";
    booleanValues = [
        { displayName: true, id: true },
        { displayName: false, id: false },
    ]
    @Output() outputData = new EventEmitter<any>();

    constructor(
        public config: DynamicDialogConfig,
        public ref: DynamicDialogRef,
        private crudService: CrudService,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private store: Store<AppState>, private rxjsService: RxjsService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
        this.getCustomCategory();
        this.createCustomerCategoriesForm();
    }

    createCustomerCategoriesForm(): void {
        let customerCategoriesFormModel = new CustomerCategoriesModel(this.config.data);
        this.customerCategoriesAddEditForm = this.formBuilder.group({});
        Object.keys(customerCategoriesFormModel).forEach((key) => {
            this.customerCategoriesAddEditForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
                new FormControl(customerCategoriesFormModel[key]));
        });

        this.customerCategoriesAddEditForm = setRequiredValidator(this.customerCategoriesAddEditForm, ["customCategoryConfigId", "value"]);
        this.customerCategoriesAddEditForm.get("customerId").setValue(this.config.data.customerId);
        this.customerCategoriesAddEditForm.get("customerAddressId").setValue(this.config.data.customerAddressId);
        this.customerCategoriesAddEditForm.get("value").setValue(this.config.data.value);

    }

    getCustomCategory() {
        this.crudService.dropdown(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORIES_DROPDOWN, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.resources) {
                    this.categories = response.resources;
                    let category = this.categories.find(item => item.id == this.config.data.customCategoryConfigId)
                    this.decideTextBoxType(category.isValueType, category.customFieldTypeName)
                }
            })
    }

    getCustomCategoryList(id, defaultValue) {
        let object = { CustomCategoryListId: id }
        this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOM_CATEGORIES_LIST_ITEM, undefined, false, prepareRequiredHttpParams(object))
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.resources) {
                    this.custom_categories = response.resources;
                    this.customerCategoriesAddEditForm.get("value").setValue(this.config.data.value ? this.config.data.value : defaultValue)
                }

            })
    }
    // Deceide text box type based on confiured field name .
    decideTextBoxType = (valueType, type) => {
        if (valueType == true) {
            if (type == "Date") {
                this.fieldType = "date"
                this.customerCategoriesAddEditForm.get('value').setValue(new Date(this.config.data.value))
            } else if (type == "DateTime") {
                this.fieldType = "datetime"
                this.customerCategoriesAddEditForm.get('value').setValue(new Date(this.config.data.value))
            } else if (type == "Boolean") {
                this.custom_categories = this.booleanValues
            } else {
                this.fieldType = "text"
            }
        } else {
            this.onChangeCategory();
            this.fieldType = "dropdown"
        }
    }
    // select change
    onChangeCategory() {
        let _id = this.customerCategoriesAddEditForm.get('customCategoryConfigId').value
        this.customerCategoriesAddEditForm.get("value").setValue("")
        let category = this.categories.find(item => item.id == _id)
        if (category.isValueType == false) { // It will show dropdown.
            this.fieldType = "dropdown";
            this.getCustomCategoryList(category.customCategoryListId, category.customCategoryListItemId)
        } else {
            this.fieldType = "text";
            let type = category.customFieldTypeName

            let minLength = category.customFieldMinLength ? category.customFieldMinLength : 1
            let maxLength = category.customFieldMaxLength ? category.customFieldMaxLength : 5
            let minValue = category.customFieldMinValue ? category.customFieldMinValue : 1
            let maxValue = category.customFieldMaxValue ? category.customFieldMaxValue : 10000
            // default validations
            let validators = [Validators.required, Validators.min(minValue), Validators.max(maxValue),
            Validators.minLength(minLength), Validators.maxLength(maxLength)]

            switch (type) {
                case "Integer":
                    this.customerCategoriesAddEditForm.get("value").setValidators([Validators.pattern(/^([0-9][0-9]*)$/), ...validators])
                    break;
                case "Decimal":
                    this.customerCategoriesAddEditForm.get("value").setValidators([Validators.pattern(/^\d+(\.\d{0,2})?$/), ...validators])
                    break;
                case "Money":
                    if (category.isSpecialCharactorPermitted) {
                        this.customerCategoriesAddEditForm.get("value").setValidators([Validators.pattern(/^\d+(\.\d{0,2})?$/), ...validators])
                    } else {
                        this.customerCategoriesAddEditForm.get("value").setValidators([Validators.pattern(/^\d+(\.\d{0,2})?$/), ...validators])
                    }
                    break;
                case "Date":
                    this.fieldType = "date";
                    if (category.isDefaultToCurrentDate) {
                        this.customerCategoriesAddEditForm.get("value").setValue(new Date())
                    }
                    break;
                case "DateTime":
                    this.fieldType = "datetime";
                    if (category.isDefaultToCurrentDate) {
                        this.customerCategoriesAddEditForm.get("value").setValue(new Date())
                    }
                    break;
                case "Boolean":
                    this.customerCategoriesAddEditForm.get("value").setValidators([Validators.required])
                    this.custom_categories = this.booleanValues;
                    break;

                default:
                    this.customerCategoriesAddEditForm.get("value").setValidators(validators)
                    break;
            }
        }

    }
    btnCloseClick() {
        this.rxjsService.setDialogOpenProperty(false);
        this.ref.close(false);
      }


    onSubmit(): void {
        if (this.customerCategoriesAddEditForm.invalid) return;
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let crudService: Observable<IApplicationResponse> = !this.customerCategoriesAddEditForm.value.customerCategoryValueId ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            EventMgntModuleApiSuffixModels.CUSTOMER_CATEGORIES, this.customerCategoriesAddEditForm.value) :
            this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
                EventMgntModuleApiSuffixModels.CUSTOMER_CATEGORIES, this.customerCategoriesAddEditForm.value);
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.dialogClose();
                this.customerCategoriesAddEditForm.reset();
                this.outputData.emit(true)
            }
        })
    }

    dialogClose(): void {
        this.ref.close(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }
}