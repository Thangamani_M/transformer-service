import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TieredMenuModule } from "primeng/tieredmenu";
import { CustomerCategoriesAddEditComponent } from "./customer-categories-add-edit.component";

@NgModule({
  declarations: [CustomerCategoriesAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    TieredMenuModule,
  ],
  exports: [CustomerCategoriesAddEditComponent],
  entryComponents: [CustomerCategoriesAddEditComponent],
})
export class CustomerCategoriesAddEditModule { }