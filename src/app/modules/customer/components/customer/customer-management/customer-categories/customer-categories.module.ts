import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TieredMenuModule } from "primeng/tieredmenu";
import { CustomerCategoriesAddEditModule } from "./customer-categories-add-edit.module";
import { CustomerCategoryComponent } from "./customer-categories.component";

@NgModule({
  declarations: [CustomerCategoryComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    TieredMenuModule,
    CustomerCategoriesAddEditModule,
  ],
  exports: [CustomerCategoryComponent],
  entryComponents: [],
})
export class CustomerCategoriesModule { }