import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDialogRef, MatSelectModule, MAT_DIALOG_DATA } from "@angular/material";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { NewAddressModule } from "@modules/sales/components/task-management/lead-info/new-address/new-address.module";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { NgxPrintModule } from "ngx-print";
import { SignaturePadModule } from "ngx-signaturepad";
import { AutoCompleteModule } from "primeng/autocomplete";
import { AddressInfoComponent } from "./address-info.component";
import { RelocationListComponent } from './relocation-list.component';
import { RelocationModule } from "./relocation.module";

import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
    declarations: [ RelocationListComponent ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RelocationModule,
    RouterModule.forChild([
      {
        path: '', component: RelocationListComponent, canActivate:[AuthGuard], data: { title: 'Relocation' }
    },
    ]),
  ],
  entryComponents: [ AddressInfoComponent ],
  exports: [  ],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }]
})
export class RelocationListModule { }