import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDialogRef, MatSelectModule, MAT_DIALOG_DATA } from "@angular/material";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { NewAddressModule } from "@modules/sales/components/task-management/lead-info/new-address/new-address.module";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { NgxPrintModule } from "ngx-print";
import { SignaturePadModule } from "ngx-signaturepad";
import { AutoCompleteModule } from "primeng/autocomplete";
import { AddressInfoComponent } from "./address-info.component";
import { RelocationComponent } from "./relocation.component";



@NgModule({
    declarations: [ RelocationComponent, AddressInfoComponent ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatSelectModule,
    AutoCompleteModule,
    NgxPrintModule,
    SignaturePadModule,
    NewAddressModule,
    // RouterModule.forChild([
    //   {
    //     path: '', component: RelocationListComponent, data: { title: 'Relocation' }
    // },
    // ]),
  ],
  entryComponents: [ AddressInfoComponent ],
  exports: [ RelocationComponent ],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }]
})
export class RelocationModule { }