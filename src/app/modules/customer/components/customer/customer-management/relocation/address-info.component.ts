import { Component, Inject, OnInit, Output, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-address-info',
  templateUrl: './address-info.component.html',
})
export class AddressInfoComponent implements OnInit {

  wasteDisposalStocksArray = []
  addressDetails:any;
  checkedList = [];
  details:any;
  @Output() outputData = new EventEmitter<any>();
  @Output() selectedItemCode = new EventEmitter<any>();
  masterSelected:boolean;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.addressDetails = this.data.data;
  }

  checkUncheckAll(event) {
  

  }

  onCheckboxChange(obj){
  this.details = obj;
  }

  submit() {
    this.outputData.emit(this.details);
  }
}
