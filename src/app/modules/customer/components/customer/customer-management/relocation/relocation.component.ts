import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import {
  loggedInUserData, selectDynamicEagerLoadingLeadCatgoriesState$, selectStaticEagerLoadingCustomerTypesState$,
  selectStaticEagerLoadingSiteTypesState$, selectStaticEagerLoadingTitlesState$
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  debounceTimeForSearchkeyword, defaultCountryCode, destructureAfrigisObjectAddressComponents, disableFormControls, enableFormControls, formConfigs, getFullFormatedAddress, IApplicationResponse, LeadGroupTypes, LoggedInUserModel, ModulesBasedApiSuffix,
  prepareAutoCompleteListFormatForMultiSelection, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControlError, ResponseMessageTypes, SERVER_REQUEST_DATE_TRANSFORM, setRequiredValidator, SnackbarService
} from '@app/shared';
import { LeafLetFullMapViewModalComponent } from '@app/shared/components/leaf-let/leaf-let-full-map-view.component';
import { CrudService, RxjsService } from '@app/shared/services';
import { RelocationModel } from '@modules/customer';
import { NewAddressPopupComponent, SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-relocation',
  templateUrl: './relocation.component.html',
  styleUrls: ['./relocation.component.scss']
})
export class RelocationComponent implements OnInit {
  formConfigs = formConfigs;
  relocationForm: FormGroup;
  serviceCategories = [];
  installedSystem = [];
  @Input() customerInfo;
  installedSystemForRelocated = [];
  currentAddresses;
  customerId = "";
  currentAddressItems = [];
  oldAddressItems = [];
  addressId;
  loggedInUserData: LoggedInUserModel;
  titles;
  customerTypes;
  siteTypes;
  leadCategories;
  sourceTypes;
  customerDetails;
  startTodayDate = new Date();
  isFormSubmitted = false;
  getCustomerData;
  shouldShowLocationPinBtn = false;
  latLongObj;
  isCustomAddressSaved = false;
  getLoopableObjectRequestObservable: Observable<IApplicationResponse>;
  selectedAddressOption;
  selectedCurrentAddressObj;
  selectedRelocatedAddressObj;
  feature: string;
  featureIndex: string;

  constructor(private crudService: CrudService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private rxjsService: RxjsService,
    private router: Router, private datePipe: DatePipe,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
  }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      this.customerId = params['params']['id'];
    });
    this.combineLatestNgrxStoreData();
    this.getCustomerCurrentAddresses();
    this.createRelocationForm();
    this.getForkJoinRequests();
    this.onFormControlChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingTitlesState$), this.store.select(selectStaticEagerLoadingSiteTypesState$),
      this.store.select(selectStaticEagerLoadingCustomerTypesState$), this.store.select(selectDynamicEagerLoadingLeadCatgoriesState$),
      this.activatedRoute.queryParams, this.rxjsService.getCustomerDate()])
      .subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
        this.titles = response[1];
        this.siteTypes = response[2];
        this.customerTypes = response[3];
        this.leadCategories = response[4];
        if (response[5]['addressId']) {
          this.addressId = response[5]['addressId'];
        }
        if (response[5]['feature_name']) {
          this.feature = response[5]['feature_name'];
        }
        if (response[5]['featureIndex']) {
          this.featureIndex = response[5]['featureIndex'];
        }
        if (response[6]) {
          this.getCustomerData = response[6];
        }
      });
  }

  onFormControlChanges(): void {
    this.onFormatedAddressFormControlValueChanges();
    this.relocationForm.get('isAfrigisSearch').valueChanges.subscribe((isAfrigisSearch: boolean) => {
      this.relocationForm.get('formatedAddress').setValue("");
      if (isAfrigisSearch) {
        this.relocationForm.get('addressId').setValue(this.relocationForm.get('addressId').value ?
          this.relocationForm.get('addressId').value : "");
        this.relocationForm.get('isAddressExtra').setValue(false);
        this.clearAddressFormGroupValues();
      }
    });
    this.relocationForm.get("isAddressComplex").valueChanges.subscribe((isAddressComplex: boolean) => {
      if (!isAddressComplex) {
        this.relocationForm.get("rebuildingNumber").setValue(null);
        this.relocationForm.get("rebuildingName").setValue(null);
      }
    });
    this.relocationForm.get("isAddressExtra").valueChanges.subscribe((isAddressExtra: boolean) => {
      if (isAddressExtra) {
        this.relocationForm = enableFormControls(this.relocationForm, ["estateStreetNo", "estateStreetName", "estateName"]);
      } else {
        this.relocationForm = disableFormControls(this.relocationForm, ["estateStreetNo", "estateStreetName", "estateName"]);
        this.relocationForm.get("estateStreetNo").setValue(null);
        this.relocationForm.get("estateStreetName").setValue(null);
        this.relocationForm.get("estateName").setValue(null);
      }
    });
  }

  onFormatedAddressFormControlValueChanges(): void {
    this.relocationForm
      .get("formatedAddress")
      .valueChanges.pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged()).subscribe((searchtext) => {
          if (!searchtext || this.isCustomAddressSaved == true) {
            this.getLoopableObjectRequestObservable = of();
          }
          else {
            this.getLoopableObjectRequestObservable = this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS, null, true,
              prepareRequiredHttpParams({
                searchtext,
                isAfrigisSearch: this.relocationForm.get("isAfrigisSearch").value
              }));
          }
        });
  }

  openFindPinMap() {
    this.openFullMapViewWithConfig();
  }

  openFullMapViewWithConfig() {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(LeafLetFullMapViewModalComponent, {
      width: "100vw",
      maxHeight: "100vh",
      disableClose: true,
      data: {
        fromUrl: 'Two Table Architecture Custom Address',
        boundaryRequestId: null,
        boundaryRequestRefNo: null,
        isDisabled: false,
        boundaryRequestDetails: {},
        boundaries: [],
        latLong: this.latLongObj,
        shouldShowLegend: false
      },
    });
    dialogReff.afterClosed().subscribe((result) => {
      if (result?.hasOwnProperty('latLong')) {
        this.latLongObj = result.latLong;
        this.relocationForm.get("relocationPin").setValue(`${result.latLong.lat.toFixed(4)}, ${result.latLong.lng.toFixed(4)}`)
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  clearAddressFormGroupValues(): void {
    this.relocationForm.patchValue({
      relocationPin: null,
      resuburb: null, recity: null,
      reprovince: null, repostalCode: null,
      restreetAddress: null, restreetNumber: null,
      rebuildingName: null, rebuildingNumber: null,
      estateName: null, estateStreetNo: null,
      estateStreetName: null, addressConfidentLevelName: null
    });
  }

  onSelectedItemOption(selectedObject): void {
    if (!selectedObject) return;
    this.selectedAddressOption = selectedObject;
    if (this.relocationForm.get('isAfrigisSearch').value) {
      this.relocationForm.get("seoId").setValue(selectedObject["seoid"]);
      this.getAddressFullDetails(selectedObject["seoid"]);
      this.relocationForm.get('addressId').setValue(this.relocationForm.get('addressId').value ? this.relocationForm.get('addressId').value : null);
    } else {
      selectedObject["longitude"] = selectedObject["longitude"] ? selectedObject["longitude"] : "";
      selectedObject["latitude"] = selectedObject["latitude"] ? selectedObject["latitude"] : "";
      if (selectedObject["latitude"] && selectedObject["longitude"]) {
        selectedObject["latLong"] = `${selectedObject["latitude"]}, ${selectedObject["longitude"]}`;
      } else {
        selectedObject["latLong"] = "";
      }
      this.relocationForm.patchValue(
        {
          addressId: selectedObject['addressId'],
          seoId: "",
          latitude: selectedObject["latitude"],
          longitude: selectedObject["longitude"],
          relocationPin: selectedObject["latLong"],
          resuburb: selectedObject["suburbName"],
          recity: selectedObject["cityName"],
          reprovince: selectedObject["provinceName"],
          repostalCode: selectedObject["postalCode"],
          restreetAddress: selectedObject["streetName"],
          restreetNumber: selectedObject["streetNo"],
          estateName: selectedObject["estateName"],
          estateStreetName: selectedObject["estateStreetName"],
          estateStreetNo: selectedObject["estateStreetNo"],
          rebuildingName: selectedObject["buildingName"],
          rebuildingNo: selectedObject["buildingNo"],
          addressConfidentLevelName: selectedObject["addressConfidentLevelName"],
          addressConfidentLevelId: selectedObject["addressConfidentLevelId"]
        }, { emitEvent: false, onlySelf: true });
    }
  }

  getAddressFullDetails(seoid: string): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ADDRESS_DETAILS,
      undefined, false, prepareRequiredHttpParams({ seoid }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.relocationForm.get("addressConfidentLevelName").setValue(response.resources.addressConfidenceLevel);
          response.resources.addressDetails.forEach((addressObj) => {
            this.relocationForm.get("addressConfidentLevelId").setValue(addressObj.confidence);
            this.patchAddressFormGroupValues(addressObj, addressObj["address_components"]);
          });
          this.relocationForm.updateValueAndValidity();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  patchAddressFormGroupValues(addressObj, addressComponents: Object[]) {
    addressObj.geometry.location.lat = addressObj.geometry.location.lat
      ? addressObj.geometry.location.lat : "";
    addressObj.geometry.location.lng = addressObj.geometry.location.lng
      ? addressObj.geometry.location.lng : "";
    if (addressObj.geometry.location.lat && addressObj.geometry.location.lng) {
      addressObj.geometry.location.latLong = `${addressObj.geometry.location.lat}, ${addressObj.geometry.location.lng}`;
    } else {
      addressObj.geometry.location.latLong = "";
    }
    let destructureAfrigisObjectAddressComponentsObj: any = destructureAfrigisObjectAddressComponents(addressComponents);
    destructureAfrigisObjectAddressComponentsObj.latitude = addressObj.geometry.location.lat;
    destructureAfrigisObjectAddressComponentsObj.longitude = addressObj.geometry.location.lng;
    destructureAfrigisObjectAddressComponentsObj.relocationPin = addressObj.geometry.location.latLong;
    destructureAfrigisObjectAddressComponentsObj.addressConfidentLevelId = addressObj.confidence;
    this.selectedRelocatedAddressObj = destructureAfrigisObjectAddressComponentsObj;
    this.relocationForm.patchValue(
      {
        latitude: destructureAfrigisObjectAddressComponentsObj.latitude,
        longitude: destructureAfrigisObjectAddressComponentsObj.longitude,
        relocationPin: destructureAfrigisObjectAddressComponentsObj.relocationPin,
        rebuildingName: destructureAfrigisObjectAddressComponentsObj.buildingName,
        rebuildingNumber: destructureAfrigisObjectAddressComponentsObj.buildingNo,
        estateName: destructureAfrigisObjectAddressComponentsObj.estateName,
        estateStreetName: destructureAfrigisObjectAddressComponentsObj.estateStreetName,
        estateStreetNo: destructureAfrigisObjectAddressComponentsObj.estateStreetNo,
        resuburb: destructureAfrigisObjectAddressComponentsObj.suburbName,
        recity: destructureAfrigisObjectAddressComponentsObj.cityName,
        reprovince: destructureAfrigisObjectAddressComponentsObj.provinceName,
        repostalCode: destructureAfrigisObjectAddressComponentsObj.postalCode,
        restreetAddress: destructureAfrigisObjectAddressComponentsObj.streetName,
        restreetNumber: destructureAfrigisObjectAddressComponentsObj.streetNo,
      },
      { emitEvent: false, onlySelf: true }
    );
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SOURCE_TYPES, undefined, true, null),
      this.getCustomerCurrentAddresses()])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200 && respObj.resources) {
            switch (ix) {
              case 0:
                this.sourceTypes = respObj.resources;
                break;
              case 1:
                this.currentAddresses = respObj.resources;
                this.getCustomerDetailsById();
                break;
            }
          }
        });
      });
  }

  getCustomerDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      SalesModuleApiSuffixModels.GET_CUSTOMER_BY_ID, this.customerId, false, null).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.customerDetails = response.resources;
          let customerDetails = JSON.parse(JSON.stringify(response.resources));
          if (this.customerDetails) {
            this.customerDetails.mobile1 = this.customerDetails.mobileNumber1.split(' ')[1];
            this.customerDetails.mobile1CountryCode = this.customerDetails.mobileNumber1.split(' ')[0];
            if (this.customerDetails.mobileNumber2) {
              this.customerDetails.mobile2 = this.customerDetails.mobileNumber2.split(' ')[1];
              this.customerDetails.mobile2CountryCode = this.customerDetails.mobileNumber2.split(' ')[0];
            }
            else {
              this.customerDetails.mobile2 = "";
              this.customerDetails.mobile2CountryCode = defaultCountryCode;
            }
            if (customerDetails.officeNo) {
              this.customerDetails.officeNo = customerDetails.officeNo.split(' ')[1];
              this.customerDetails.officeNoCountryCode = customerDetails.officeNo.split(' ')[0];
            }
            else {
              this.customerDetails.officeNo = "";
              this.customerDetails.officeNoCountryCode = defaultCountryCode;
            }
            if (customerDetails.premisesNo) {
              this.customerDetails.premisesNo = customerDetails.premisesNo.split(' ')[1];
              this.customerDetails.premisesNoCountryCode = customerDetails.premisesNo.split(' ')[0];
            }
            else {
              this.customerDetails.premisesNo = "";
              this.customerDetails.premisesNoCountryCode = defaultCountryCode;
            }
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getCustomerCurrentAddresses() {
    return this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      SalesModuleApiSuffixModels.CURRENT_ADDRESS_INFO, undefined, false, prepareRequiredHttpParams({
        customerId: this.customerId
      }))
  }

  selectedAddress() {
    this.selectedCurrentAddressObj = null;
    let formValue = this.relocationForm.getRawValue();
    this.addressId = formValue.currentAddressId;
    let selectedAddress = this.currentAddresses.find(e => e.addressId == this.addressId);
    let selectedleadCategories = [];
    if (selectedAddress.leadCategories) {
      selectedAddress?.leadCategories?.split(",")?.forEach(results =>{ selectedleadCategories.push(this.leadCategories.find(element => element.id == results));
      })
      selectedAddress.leadCategoryId = selectedleadCategories;
    }
    selectedAddress.locationPin = selectedAddress.longitude + " " + selectedAddress.latitude;
    selectedAddress.buildingNumber = selectedAddress.buildingNo;
    this.relocationForm.patchValue(selectedAddress);
    if (selectedAddress.installedSystems && selectedAddress.installedSystems.length > 0) {
      this.installedSystem = selectedAddress.installedSystems;
    }
    if (selectedAddress.activeSubscriptions && selectedAddress.activeSubscriptions.length > 0) {
      this.serviceCategories = selectedAddress.activeSubscriptions;
    }
    this.selectedCurrentAddressObj = selectedAddress;
  }

  getInstalledSystemInfo(addressId) {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      SalesModuleApiSuffixModels.GET_INSTALLED_SYSTEM,
      undefined,
      false, prepareGetRequestHttpParams(null, null, { addressId })).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200 && response.isSuccess) {
          this.installedSystemForRelocated = response.resources;
          if (this.installedSystemForRelocated && this.installedSystemForRelocated.length > 0) {
            this.installedSystemForRelocated.forEach(element => {
              element.installedItems.forEach((obj, i) => {
                let dt = {
                  itemId: ''
                }
                dt.itemId = obj;
                element.installedItems[i] = dt;
              });
            });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  openAddressPopup(): void {
    this.relocationForm.value.createdUserId = this.loggedInUserData.userId;
    this.relocationForm.value.modifiedUserId = this.loggedInUserData.userId;
    let leadForm = { ...this.relocationForm.value, ...this.relocationForm.getRawValue() };
    leadForm.contactInfo = this.customerDetails;
    leadForm = { ...leadForm, ...this.getCustomerData };
    const dialogReff = this.dialog.open(NewAddressPopupComponent, {
      width: "750px",
      disableClose: true,
      data: leadForm,
    });
    dialogReff.afterClosed().subscribe((result) => {
      if (result) {
        this.isCustomAddressSaved = false;
      }
    });
    dialogReff.componentInstance.outputData.subscribe((result) => {
      if (result) {
        this.isCustomAddressSaved = true;
        this.shouldShowLocationPinBtn = result.isAddressType == false ? true : false;
        this.relocationForm.patchValue({
          formatedAddress: getFullFormatedAddress(result),
          addressConfidentLevelId: result.addressConfidentLevelId,
          rebuildingName: result.buildingName,
          rebuildingNumber: result.buildingNo,
          estateName: result.estateName,
          estateStreetName: result.estateStreetName,
          estateStreetNo: result.estateStreetNo,
          resuburb: result.suburbName,
          recity: result.cityName,
          reprovince: result.provinceName,
          repostalCode: result.postalCode,
          restreetAddress: result.streetName,
          restreetNumber: result.streetNo
        });
        this.rxjsService.setFormChangeDetectionProperty(true);
      }
      else {
        this.isCustomAddressSaved = false;
      }
    });
  }

  createRelocationForm() {
    let relocationModel = new RelocationModel();
    this.relocationForm = this.formBuilder.group({});
    Object.keys(relocationModel).forEach((key) => {
      this.relocationForm.addControl(key, new FormControl(relocationModel[key]));
    });
    this.relocationForm = setRequiredValidator(this.relocationForm, ["currentAddressId", "formatedAddress", "transferDate", "movingDate", "relocationPin", "leadCategoryId", "siteTypeId", "sourceId"]);
    this.relocationForm = disableFormControls(this.relocationForm, ['estateStreetNo', 'estateStreetName', 'estateName', 'addressConfidentLevelName']);
  }

  checked(value, obj, event) {
    let checkedObj = obj;
    if (value) {
      if (event.checked) {
        if (checkedObj.items.length > 0) {
          checkedObj.items.forEach(element => {
            element.isWillingToTransferCurrentAddressItem = true;
            element.isWillingToUseRelocationAddressItem = false;
          });
        }
        this.currentAddressItems.push(checkedObj);
      } else {
        const index = this.currentAddressItems.findIndex(e => e.systemType == checkedObj.systemType);//Find the index of stored id
        this.currentAddressItems.splice(index, 1);
      }
    } else {
      if (event.checked) {
        if (checkedObj.installedItems && checkedObj.installedItems.length > 0) {
          checkedObj.installedItems.forEach((element, i) => {
            element.isWillingToTransferCurrentAddressItem = false;
            element.isWillingToUseRelocationAddressItem = true;
          });
        }
        checkedObj.items = checkedObj.installedItems;
        this.oldAddressItems.push(checkedObj);
      } else {
        const index = this.oldAddressItems.findIndex(e => e.systemTypeName == checkedObj.systemTypeName);//Find the index of stored id
        this.oldAddressItems.splice(index, 1);
      }
    }
  }

  cancel() {
    this.rxjsService.setRelocation(false);
  }

  validateAtleastOneFieldToBeMandatory(): void {
    if ((this.relocationForm.get("isAddressComplex").value && !this.relocationForm.get("rebuildingNumber").value &&
      !this.relocationForm.get("rebuildingName").value) ||
      (this.relocationForm.get("isAddressExtra").value && !this.relocationForm.get("estateName").value &&
        !this.relocationForm.get("estateStreetName").value && !this.relocationForm.get("estateStreetNo").value)) {
      this.setAtleastOneFieldRequiredError();
    }
    else {
      this.relocationForm = removeFormControlError(this.relocationForm, "atleastOneOfTheFieldsIsRequired");
    }
  }

  setAtleastOneFieldRequiredError() {
    if (this.relocationForm.get("isAddressComplex").value) {
      this.relocationForm.get("rebuildingNumber").setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.relocationForm.get("rebuildingName").setErrors({ atleastOneOfTheFieldsIsRequired: true });
    }
    if (this.relocationForm.get("isAddressExtra").value) {
      this.relocationForm.get("estateName").setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.relocationForm.get("estateStreetNo").setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.relocationForm.get("estateStreetName").setErrors({ atleastOneOfTheFieldsIsRequired: true });
    }
  }

  onSubmit() {
    this.isFormSubmitted = true;
    this.validateAtleastOneFieldToBeMandatory();
    if (this.relocationForm.invalid) {
      this.relocationForm.get("movingDate").markAsTouched();
      this.relocationForm.get("transferDate").markAsTouched();
      this.relocationForm.get("leadCategoryId").markAllAsTouched();
      return;
    }
    if (!this.relocationForm.value.resuburb || !this.relocationForm.value.recity ||
      !this.relocationForm.value.reprovince) {
      this.snackbarService.openSnackbar("Province Name / Suburb Name / City Name is required..!!", ResponseMessageTypes.ERROR);
      return;
    }
    let detailObj = this.relocationForm.getRawValue();
    let location = detailObj.relocationPin;
    location = location.split(",");
    let installedArray = this.currentAddressItems.concat(this.oldAddressItems);
    let mergedArray = [];
    installedArray.forEach(element => {
      element.items.forEach(obj => {
        mergedArray.push(obj);
      });
    });
    let selectedleadCategoryId = [];
    this.relocationForm.get('leadCategoryId')?.value?.forEach(result =>{selectedleadCategoryId.push(result?.id)})
    let payload = {
      customerId: this.customerId,
      currentAddressId: detailObj.currentAddressId,
      transferDate: this.datePipe.transform(detailObj.transferDate, SERVER_REQUEST_DATE_TRANSFORM),
      reason: detailObj.reason,
      movingDate: this.datePipe.transform(detailObj.movingDate, SERVER_REQUEST_DATE_TRANSFORM),
      createdUserId: this.loggedInUserData.userId,
      titleId: detailObj.titleId,
      firstName: detailObj.firstName,
      lastName: detailObj.lastName,
      companyName: detailObj.companyName,
      companyRegNo: detailObj.companyRegNo,
      said: detailObj.said,
      sourceId: detailObj.sourceId,
      leadGroupId: LeadGroupTypes.RELOCATION,
      customerTypeId: detailObj.customerTypeId,
      contactId: detailObj.contactId,
      siteTypeId: detailObj.siteTypeId,
      leadCategoryId: selectedleadCategoryId,
      addressInfo: !this.relocationForm.get('isAfrigisSearch').value ? this.selectedAddressOption : this.selectedRelocatedAddressObj,
      relocationItems: mergedArray
    }
    payload.addressInfo.buildingName = this.relocationForm.get('rebuildingName').value;
    payload.addressInfo.buildingNo = this.relocationForm.get('rebuildingNumber').value;
    payload.addressInfo.estateName = this.relocationForm.get('estateName').value;
    payload.addressInfo.estateStreetName = this.relocationForm.get('estateStreetName').value;
    payload.addressInfo.estateStreetNo = this.relocationForm.get('estateStreetNo').value;
    this.crudService.create(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.CREATE_RELOCATION, payload).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.rxjsService.setRelocation(false);
          this.rxjsService.setGlobalLoaderProperty(false);
          this.redirectToLeads(response.resources);
        }
        this.isFormSubmitted = false;
      });
  }

  redirectToLeads(leadId) {
    this.router.navigate(['/sales/task-management/leads/lead-info/view'], { queryParams: { leadId: leadId, selectedIndex: 1 } });
  }
}