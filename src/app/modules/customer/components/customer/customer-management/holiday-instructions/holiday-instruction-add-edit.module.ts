import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { HolidayInstructionCustomerAddEditComponent } from "./holiday-instruction-add-edit.component";


@NgModule({
    declarations: [HolidayInstructionCustomerAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: HolidayInstructionCustomerAddEditComponent, data: { title: 'Holiday Instructions add-edit' }
        },
    ])
  ],
  exports: [],
  entryComponents: [],
})
export class HolidayInstructionCustomerAddEditModule { }