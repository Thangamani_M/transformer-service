import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { addNullOrStringDefaultFormControlValue, convertISOStringtoDateAndTodayDate, countryCodes, CustomDirectiveConfig, DynamicConfirmByMessageConfirmationType, formConfigs, getNthDate, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, onBlurActionOnDuplicatedFormControl, onFormControlChangesToFindDuplicate, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormValidators, ReusablePrimeNGTableFeatureService, RxjsService, SERVER_REQUEST_DATE_TRANSFORM, setRequiredValidator } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { CustomerModuleApiSuffixModels, HolidayInstructionContactsModel, HolidayInstructionEmergencyContactsModel } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { HolidayInstructionContactsInfo, HolidayInstructionModelAddEditModel } from '@modules/sales/models/holiday-instruction';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
declare var $;
@Component({
  selector: 'app-holiday-instruction-add-edit',
  templateUrl: './holiday-instruction-add-edit.component.html'
})
export class HolidayInstructionCustomerAddEditComponent implements OnInit {
  @ViewChildren('contactRows') contactRows: QueryList<ElementRef>;
  @ViewChildren('contactInput') contactInput: QueryList<ElementRef>;
  @ViewChildren('emergencyContactRows') emergencyContactRows: QueryList<ElementRef>;
  @ViewChildren('emergencyContactInput') emergencyContactInput: QueryList<ElementRef>;
  formConfigs = formConfigs;
  cancelForm: FormGroup;
  holidayInstructionsForm: FormGroup;
  holidayInstructionContacts: FormArray;
  holidayInstructionEmergencyContacts: FormArray;
  holidayInstructionId = '';
  loggedInUserData: LoggedInUserModel;
  holidayInstructionDetail;
  numbers = [];
  customerId = '';
  addressId = '';
  cancelHolidayInstruction = false;
  cancelHolidayInstructionDialog = false;
  todayDate = new Date();
  countryCodes = countryCodes;
  maximumAllowedPatrols;
  isSaveBtnDisabled = true;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  maxHolidayInstructionFromDate;

  constructor(private activatedRoute: ActivatedRoute, private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService, private datePipe: DatePipe,
    private momentService: MomentService, public rxjsService: RxjsService, private store: Store<AppState>, private crudService: CrudService, private formBuilder: FormBuilder) {
    this.combineLatestNgrxStoreData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.activatedRoute.queryParams]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.holidayInstructionId = response[1]?.id;
      this.customerId = response[1]?.customerId;
      this.addressId = response[1]?.addressId;
    });
  }

  ngOnInit(): void {
    this.createHolidayInstructionAddForm();
    this.createCancelForm();
    this.onFormControlChange();
    if (this.holidayInstructionId) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.getHolidayInstructionsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          let dateProps = response.resources?.isAdditionalPaidPatrols ? ['holidayInstructionFromDate', 'holidayInstructionToDate',
            'paidPatrolStartDate', 'paidPatrolEndDate'] : ['holidayInstructionFromDate', 'holidayInstructionToDate'];
          response = convertISOStringtoDateAndTodayDate(response, dateProps);
          this.holidayInstructionDetail = response.resources;
          this.holidayInstructionContacts = this.getHolidayInstructionContactArray;
          while (this.getHolidayInstructionContactArray.length) {
            this.getHolidayInstructionContactArray.removeAt(this.getHolidayInstructionContactArray.length - 1);
          }
          this.holidayInstructionEmergencyContacts = this.getHolidayinstructionEmergencyContactArray;
          while (this.getHolidayinstructionEmergencyContactArray.length) {
            this.getHolidayinstructionEmergencyContactArray.removeAt(this.getHolidayinstructionEmergencyContactArray.length - 1);
          }
          if (response.resources.holidayInstructionContacts.length == 0) {
            this.getHolidayInstructionContactArray.push(this.createNewItem());
            this.getHolidayinstructionEmergencyContactArray.push(this.createNewItem('emergencyContacts'));
            // this.getHolidayinstructionEmergencyContactArray.push(this.createNewItem('emergencyContacts'));
          }
          else {
            response.resources.holidayInstructionContacts.forEach((holidayInstructionContactObj: HolidayInstructionContactsModel) => {
              this.getHolidayInstructionContactArray.push(this.createNewItem('contacts', holidayInstructionContactObj));
            });
            response.resources.holidayInstructionEmergencyContacts.forEach((HolidayInstructionEmergencyContacts: HolidayInstructionEmergencyContactsModel) => {
              this.getHolidayinstructionEmergencyContactArray.push(this.createNewItem('emergencyContacts', HolidayInstructionEmergencyContacts));
            });
          }
          if (response.resources.holidayInstructionEmergencyContacts.length == 0 && response.resources.holidayInstructionContacts.length > 0) {
            this.getHolidayinstructionEmergencyContactArray.push(this.createNewItem('emergencyContacts'));
          }
          this.holidayInstructionContacts = this.getHolidayInstructionContactArray;
          this.holidayInstructionEmergencyContacts = this.getHolidayinstructionEmergencyContactArray;
          const holidayInstructionModelAddEditModel = new HolidayInstructionModelAddEditModel(response.resources);
          this.holidayInstructionsForm.patchValue(holidayInstructionModelAddEditModel);
          if (response.resources.holidayInstructionContacts.length > 0 && response.resources.holidayInstructionEmergencyContacts.length > 0) {
            this.focusAllFields();
          };
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    } else {
      this.holidayInstructionContacts = this.getHolidayInstructionContactArray;
      this.getHolidayInstructionContactArray.push(this.createNewItem());
      this.holidayInstructionEmergencyContacts = this.getHolidayinstructionEmergencyContactArray;
      this.getHolidayinstructionEmergencyContactArray.push(this.createNewItem('emergencyContacts'));
    }
    this.rxjsService.setGlobalLoaderProperty(false);
    this.holidayInstructionsForm.get('holidayInstructionFromDate').valueChanges.subscribe(() => {
      this.holidayInstructionsForm.get('holidayInstructionToDate').setValue(null);
    });
  }

  createNewItem(itemType = 'contacts', object?): FormGroup {
    let objectModel = itemType == 'contacts' ? new HolidayInstructionContactsModel(object) : new HolidayInstructionEmergencyContactsModel(object);
    let formControls = {};
    if (itemType == 'contacts') {
      Object.keys(objectModel).forEach((key) => {
        if (key === 'contactPersonName' || key === 'contactNumber') {
          formControls[key] = [{ value: objectModel[key], disabled: false }, [Validators.required]]
        } else {
          formControls[key] = [{ value: objectModel[key], disabled: false }]
        }
      });
    }
    else {
      Object.keys(objectModel).forEach((key) => {
        if (key === 'emergencyContactPersonName' || key === 'emergencyContactNumber') {
          formControls[key] = [{ value: objectModel[key], disabled: false }, [Validators.required]]
        } else {
          formControls[key] = [{ value: objectModel[key], disabled: false }]
        }
      });
    }
    return this.formBuilder.group(formControls);
  }

  getHolidayInstructionsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.HOLIDAY_INSTRUCTION_DETAILS,
      null,
      false,
      prepareGetRequestHttpParams(null, null,
        { holidayInstructionId: this.holidayInstructionId })
    );
  }
  get getHolidayInstructionContactArray(): FormArray {
    if (!this.holidayInstructionsForm) return;
    return this.holidayInstructionsForm.get("holidayInstructionContacts") as FormArray;
  }

  get getHolidayinstructionEmergencyContactArray(): FormArray {
    if (!this.holidayInstructionsForm) return;
    return this.holidayInstructionsForm.get("holidayInstructionEmergencyContacts") as FormArray;
  }

  onFormControlChanges(formControlName: string, formArray: FormArray, formArrayName: string, currentCursorIndex: number): void {
    let formControlNames: string[];
    let formArrayOfArrays = formArrayName == 'holidayInstructionContacts' ? [formArray, this.holidayInstructionEmergencyContacts] : [formArray, this.holidayInstructionContacts];
    if (formControlName == 'contactPersonName' || formControlName == 'emergencyContactPersonName') {
      formControlNames = ['contactPersonName', 'emergencyContactPersonName'];
    }
    else {
      formControlNames = ['contactNumber', 'emergencyContactNumber'];
    }
    onFormControlChangesToFindDuplicate(formControlNames, formArrayOfArrays, formArrayName, currentCursorIndex, this.holidayInstructionsForm);
  }

  onBlur(formControlName: string, formArray: FormArray, e, index: number, formArrayName?: string) {
    let formControlNames: string[];
    if (formControlName == 'contactPersonName' || formControlName == 'emergencyContactPersonName') {
      formControlNames = ['contactPersonName', 'emergencyContactPersonName'];
    }
    else {
      formControlNames = ['contactNumber', 'emergencyContactNumber'];
    }
    if (formArrayName) {
      index = this.getHolidayInstructionContactArray.length + index;
    }
    onBlurActionOnDuplicatedFormControl(formControlNames, [this.holidayInstructionContacts, this.holidayInstructionEmergencyContacts], e, index);
  }

  createHolidayInstructionAddForm(): void {
    let holidayInstructionModelAddEditModel = new HolidayInstructionModelAddEditModel();
    this.holidayInstructionsForm = this.formBuilder.group({});
    Object.keys(holidayInstructionModelAddEditModel).forEach((key) => {
      if (key == 'holidayInstructionContacts' || key == 'holidayInstructionEmergencyContacts') {
        this.holidayInstructionsForm.addControl(key, new FormArray([]));
      }
      else {
        this.holidayInstructionsForm.addControl(key, new FormControl(holidayInstructionModelAddEditModel[key]));
      }
    });
    this.holidayInstructionsForm = setRequiredValidator(this.holidayInstructionsForm, ["holidayInstructionFromDate", "holidayInstructionToDate", 'comments']);
  }

  createHolidayInstructionContactFormGroup(emergencyContactInfo?: HolidayInstructionContactsModel): FormGroup {
    let holidayInstructionContactsModel = new HolidayInstructionContactsModel(emergencyContactInfo ? emergencyContactInfo : undefined);
    let formControls = {};
    Object.keys(holidayInstructionContactsModel).forEach((key) => {
      if (key === 'contactPersonName' || key === 'contactNumberCountryCode' || key === 'contactNumber') {
        formControls[key] = [{ value: holidayInstructionContactsModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: holidayInstructionContactsModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  createHolidayInstructionEmergencyContactFormGroup(emergencyContactInfo?: HolidayInstructionEmergencyContactsModel): FormGroup {
    let holidayInstructionContactsModel = new HolidayInstructionEmergencyContactsModel(emergencyContactInfo ? emergencyContactInfo : undefined);
    let formControls = {};
    Object.keys(holidayInstructionContactsModel).forEach((key) => {
      if (key === 'emergencyContactPersonName' || key === 'emergencyContactNumberCountryCode' || key === 'emergencyContactNumber') {
        formControls[key] = [{ value: holidayInstructionContactsModel[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: holidayInstructionContactsModel[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  addItem(itemType = 'contacts'): void {
    if (itemType == 'contacts') {
      if (this.getHolidayInstructionContactArray.invalid) {
        this.focusInAndOutFormArrayFields(itemType);
        return;
      };
      this.holidayInstructionContacts = this.getHolidayInstructionContactArray;
      let holidayInstructionContactsInfoModel = new HolidayInstructionContactsInfo();
      this.holidayInstructionContacts.insert(0, this.createNewItem(itemType, holidayInstructionContactsInfoModel));
    }
    else {
      if (this.getHolidayinstructionEmergencyContactArray.invalid) {
        this.focusInAndOutFormArrayFields(itemType);
        return;
      };
      this.holidayInstructionEmergencyContacts = this.getHolidayinstructionEmergencyContactArray;
      let holidayInstructionEmergencyContactsModel = new HolidayInstructionEmergencyContactsModel();
      this.holidayInstructionEmergencyContacts.insert(0, this.createNewItem(itemType, holidayInstructionEmergencyContactsModel));
    }
  }

  focusAllFields() {
    setTimeout(() => {
      this.focusInAndOutFormArrayFields();
      this.focusInAndOutFormArrayFields('emergencyContacts');
    }, 100);
  }

  focusInAndOutFormArrayFields(itemType = 'contacts'): void {
    if (itemType == 'contacts') {
      this.contactRows.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      });
      this.contactInput.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      });
    }
    else {
      this.emergencyContactRows.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      });
      this.emergencyContactInput.forEach((item) => {
        item.nativeElement.focus();
        item.nativeElement.blur();
      });
    }
  }

  removeItem(itemType = 'contacts', i: number): void {
    if (itemType == 'contacts') {
      if (!this.getHolidayInstructionContactArray.controls[i].value.holidayInstructionContactId) {
        this.getHolidayInstructionContactArray.removeAt(i);
        return;
      }
    }
    else {
      if (!this.getHolidayinstructionEmergencyContactArray.controls[i].value.holidayInstructionEmergencyContactId) {
        this.getHolidayinstructionEmergencyContactArray.removeAt(i);
        return;
      }
    }
    if (itemType == 'contacts') {
      this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, null, DynamicConfirmByMessageConfirmationType.DANGER).
        onClose?.subscribe(dialogResult => {
          if (!dialogResult) return;
          if (this.getHolidayInstructionContactArray.controls[i].value.holidayInstructionContactId && this.getHolidayInstructionContactArray.length > 1) {
            let payload = {
              ids: this.getHolidayInstructionContactArray.controls[i].value.holidayInstructionContactId,
              isDeleted: true,
              modifiedUserId: this.loggedInUserData.userId
            }
            this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.HOLIDAY_INSTRUCTION_CONTACTS,
              undefined,
              prepareRequiredHttpParams(payload)).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                  this.getHolidayInstructionContactArray.removeAt(i);
                }
                if (this.getHolidayInstructionContactArray.length === 0) {
                  this.addItem();
                };
              });
          }
          else {
            this.getHolidayInstructionContactArray.removeAt(i);
          }
        });
    }
    else {
      this.reusablePrimeNGTableFeatureService.openDynamicConfirmByMessageDialog(`Are you sure you want to delete this?`, null, DynamicConfirmByMessageConfirmationType.DANGER).
        onClose?.subscribe(dialogResult => {
          if (!dialogResult) return;
          if (this.getHolidayinstructionEmergencyContactArray.controls[i].value.holidayInstructionEmergencyContactId && this.getHolidayinstructionEmergencyContactArray.length > 1) {
            let payload = {
              ids: this.getHolidayinstructionEmergencyContactArray.controls[i].value.holidayInstructionEmergencyContactId,
              isDeleted: true,
              modifiedUserId: this.loggedInUserData.userId
            }
            this.crudService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.HOLIDAY_INSTRUCTION_EMERGENCY_CONTACTS,
              undefined,
              prepareRequiredHttpParams(payload)).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                  this.getHolidayinstructionEmergencyContactArray.removeAt(i);
                }
                if (this.getHolidayinstructionEmergencyContactArray.length === 0) {
                  this.addItem('emergencyContacts');
                };
              });
          }
          else {
            this.getHolidayinstructionEmergencyContactArray.removeAt(i);
          }
        });
    }
  }

  onSubmit(): void {
    if (this.holidayInstructionsForm.invalid) {
      return;
    }
    this.holidayInstructionsForm.value.createdUserId = this.loggedInUserData.userId;
    this.holidayInstructionsForm.value.customerId = this.customerId;
    this.holidayInstructionsForm.value.addressId = this.addressId;
    this.holidayInstructionsForm.value.holidayInstructionFromDate = this.datePipe.transform(this.holidayInstructionsForm.value.holidayInstructionFromDate, SERVER_REQUEST_DATE_TRANSFORM);
    this.holidayInstructionsForm.value.holidayInstructionToDate = this.datePipe.transform(this.holidayInstructionsForm.value.holidayInstructionToDate, SERVER_REQUEST_DATE_TRANSFORM);
    if (this.holidayInstructionsForm.value.isAdditionalPaidPatrols) {
      this.holidayInstructionsForm.value.paidPatrolStartDate = this.datePipe.transform(this.holidayInstructionsForm.value.paidPatrolStartDate, SERVER_REQUEST_DATE_TRANSFORM);
      this.holidayInstructionsForm.value.paidPatrolEndDate = this.datePipe.transform(this.holidayInstructionsForm.value.paidPatrolEndDate, SERVER_REQUEST_DATE_TRANSFORM);
    }
    this.holidayInstructionsForm.value.holidayInstructionContacts.forEach((holidayInstructionContactModel: HolidayInstructionContactsInfo, index: number) => {
      // Because of some duplication form control logics we directly fetch the value from the nativeElement 'this.contactRows'
      holidayInstructionContactModel.contactNumber = this.contactRows?.['_results'][index].nativeElement.value.replace(/\s/g, "");
    });
    this.holidayInstructionsForm.value.holidayInstructionEmergencyContacts.forEach((holidayInstructionEmergencyContactsModel: HolidayInstructionEmergencyContactsModel, index: number) => {
      // Because of some duplication form control logics we directly fetch the value from the nativeElement 'this.emergencyContactRows'
      holidayInstructionEmergencyContactsModel.emergencyContactNumber = this.emergencyContactRows?.['_results'][index].nativeElement.value.replace(/\s/g, "");
    });
    if (this.holidayInstructionsForm.value.holidayInstructionId) {
      this.holidayInstructionsForm.value.modifiedUserId = this.loggedInUserData.userId;
    }
    this.holidayInstructionsForm.value.holidayInstructionToDate = this.momentService.toMoment(this.holidayInstructionsForm.value.holidayInstructionToDate).format('YYYY-MM-DDThh:mm:ss[Z]');
    this.holidayInstructionsForm.value.holidayInstructionFromDate = this.momentService.toMoment(this.holidayInstructionsForm.value.holidayInstructionFromDate).format('YYYY-MM-DDThh:mm:ss[Z]');
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.HOLIDAY_INSTRUCTION, this.holidayInstructionsForm.value).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setViewCustomerData({
          customerId: this.customerId,
          addressId: this.addressId,
          customerTab: 0,
          monitoringTab: null,
        })
        this.rxjsService.navigateToViewCustomerPage();
      }
    });
  }

  backPage() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId,
      customerTab: 0,
      monitoringTab: null,
    });
    this.rxjsService.navigateToViewCustomerPage();
  }

  createCancelForm(): void {
    this.cancelForm = this.formBuilder.group({
      cancelReason: ['', [Validators.required]],
      holidayInstructionId: [this.holidayInstructionId],
      isCancelled: [true],
      modifiedUserId: [this.loggedInUserData.userId]
    });
  }

  cancellationHolidayInstruction() {
    this.cancelHolidayInstruction = !this.cancelHolidayInstruction;
  }

  onCancel() {
    if (this.cancelForm.invalid) {
      return;
    }
    this.crudService.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.HOLIDAY_INSTRUCTION_CANCEL, this.cancelForm.value, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setDialogOpenProperty(false);
        this.cancelHolidayInstructionDialog = false;
      }
    });
  }

  onFormControlChange() {
    this.holidayInstructionsForm.get('holidayInstructionFromDate').valueChanges.subscribe((holidayInstructionFromDate) => {
      this.maxHolidayInstructionFromDate = getNthDate(holidayInstructionFromDate, 20);
    });
    this.holidayInstructionsForm.get('isAdditionalPaidPatrols').valueChanges.subscribe((isAdditionalPaidPatrols) => {
      if (isAdditionalPaidPatrols) {
        this.getMaxPaidPatrolAllowed();
      }
      else {
        this.holidayInstructionsForm = addNullOrStringDefaultFormControlValue(this.holidayInstructionsForm, [{ controlName: 'paidPatrolStartDate', defaultValue: null },
        { controlName: 'paidPatrolEndDate', defaultValue: null }, { controlName: 'numberOfDailyPaidPatrols', defaultValue: null }]);
        this.holidayInstructionsForm = removeFormValidators(this.holidayInstructionsForm, ["numberOfDailyPaidPatrols", "paidPatrolStartDate", 'paidPatrolEndDate']);
      }
    });
  }

  getMaxPaidPatrolAllowed() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.MAXIMU_ALLOWED_PATROLS,
      undefined, null, prepareGetRequestHttpParams(null, null,
        { CustomerId: this.customerId, AddressId: this.addressId }))
      .subscribe(response => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.maximumAllowedPatrols = response.resources?.maxHolidayPaidPatrolAllowed;
          this.numbers = [];
          for (let i = 1; i <= this.maximumAllowedPatrols; i++) {
            this.numbers.push(i);
          }
          this.holidayInstructionsForm.get('numberOfDailyPaidPatrols').setValue(this.holidayInstructionDetail?.numberOfDailyPaidPatrols ?
            this.holidayInstructionDetail.numberOfDailyPaidPatrols : null);
          this.holidayInstructionsForm = setRequiredValidator(this.holidayInstructionsForm, ["numberOfDailyPaidPatrols", "paidPatrolStartDate", 'paidPatrolEndDate']);
          this.holidayInstructionsForm.get('paidPatrolStartDate').valueChanges.subscribe((paidPatrolStartDate) => {
            this.holidayInstructionsForm.get('paidPatrolEndDate').setValue(null);
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
}
