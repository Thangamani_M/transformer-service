import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { HolidayInstructionlistComponent } from "./holiday-instruction.component";



@NgModule({
    declarations: [HolidayInstructionlistComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [HolidayInstructionlistComponent],
  entryComponents: [],
})
export class HolidayInstructionModule { }