import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { HolidayInstructionModule } from "./holiday-instruction.module";
import { HolidayInstructionsListComponent } from './holiday-instructions-list.component';



@NgModule({
    declarations: [HolidayInstructionsListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    HolidayInstructionModule,
    RouterModule.forChild([
        {
            path: '', component: HolidayInstructionsListComponent, data: { title: 'Holiday Instructions' }
        },
        {
            path: 'add-edit', loadChildren: () => import('./holiday-instruction-add-edit.module').then(m => m.HolidayInstructionCustomerAddEditModule),
        },
    ])
  ],
  exports: [],
  entryComponents: [],
})
export class HolidayInstructionListModule { }