import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { loggedInUserData, selectStaticEagerLoadingCustomerTypesState$ } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';

@Component({
  selector: 'app-holiday-instruction',
  templateUrl: './holiday-instruction.component.html',
//   styleUrls: ['./customerlist.component.scss'],
  providers: [DialogService]
})
export class HolidayInstructionlistComponent implements OnInit {
  @Input() inputObject = { fromComponentType: 'customer', customerId: "", addressId: "", partitionId: "" };
  @Input() permission;

  observableResponse;
  selectedTabIndex = 0;
  // status: any = [];
  dataList: any;
  totalRecords: any;
  selectedColumns: any[];
  selectedRows: string[] = [];
  selectedRow: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  loading = false;
  searchColumns: any;
  columnFilterForm: FormGroup;
  row: any = {};
  customerType: any;
  searchForm: FormGroup;
  status = [];
  otherParams = {};
  deletConfirm: boolean = false;
  statusConfirm: boolean = false;
  addConfirm: boolean = false;
  showDialogSpinner: boolean = false;
  customerId: any;
  addressId: any;
  pageSize: number = 10;
  loggedInUserData: LoggedInUserModel;

  primengTableConfigProperties: any = {
    tableCaption: "Holiday Instructions",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Customers Management' }, { displayName: 'Holiday Instructions', relativeRouterUrl: '/customer/dashboard' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Holiday Instructions',
          dataKey: 'customerId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [
            { field: 'holidayInstructionRefNo', header: 'Id'},
            { field: 'holidayInstructionFromDate', header: 'Holiday From Date'  },
          { field: 'holidayInstructionToDate', header: 'Holiday Date To'},
          { field: 'contactPersonName', header: 'Contact Person'},
          { field: 'contactNumber', header: 'Contact Number'},
          { field: 'paidPatrol', header: 'Paid Patrol'},
          { field: 'createdBy', header: 'Created By'},
          { field: 'holidayInstructionStatus', header: 'Status '},
          ],
          shouldShowDeleteActionBtn: false,
          ebableAddActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: CustomerModuleApiSuffixModels.HOLIDAY_INSTRUCTION_LIST,
          moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        }]
    }
  }

  constructor(
    private httpService: CrudService,private tableFilterFormService: TableFilterFormService,
    private router: Router,
    private rxjsService: RxjsService,
    private dialogService: DialogService,
    private store: Store<AppState>,
    private momentService: MomentService,
    private _fb: FormBuilder,
    private snackbarService : SnackbarService
  ) {
    this.columnFilterForm = this._fb.group({});

  }

  ngOnInit() {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
  this.getCustomers();
  this.columnFilterRequest();
  this.combineLatestNgrxStoreData();
  }



    loadPaginationLazy(event) {
      this.otherParams = {};
      let row = {}
      row['pageIndex'] = event.first / event.rows;
      row["pageSize"] = event.rows;
      row["sortOrderColumn"] = event.sortField;
      row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
      row['searchColumns'] = event.filters;
      this.row = row;
      this.onCRUDRequested(CrudType.GET, this.row);
    }

    combineLatestNgrxStoreData() {
      combineLatest([this.store.select(selectStaticEagerLoadingCustomerTypesState$),
      this.store.select(loggedInUserData),
    ])
        .pipe(take(1))
        .subscribe((response) => {
          this.customerType = response[0];
          this.loggedInUserData = new LoggedInUserModel(response[1]);

        });
    }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.otherParams = this.searchColumns;
          // this.row['searchColumns'] = this.searchColumns;
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams']} )
    }
    // [breadCrumbItem['queryParams']['key']]: breadCrumbItem['queryParams']['value']
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object): void {

    // let otherParams = {};
    if (CrudType.GET === type && Object.keys(row).length > 0) {
      // logic for split columns and its values to key value pair

      Object.keys(row['searchColumns']).forEach((key) => {
        if(key == 'status'){
          let status = row['searchColumns'][key]['value'];
          key = 'status';
          this.otherParams[key] = status.id;
        } else if(key == 'customerType') {
          let type = row['searchColumns'][key]['value'];
          key = 'customerType';
          this.otherParams[key] = type.id;
        }
        else{
          this.otherParams[key] = row['searchColumns'][key]['value'];
        }
      });
      this.otherParams['sortOrder'] = row['sortOrder'];
      if (row['sortOrderColumn']) {
        this.otherParams['sortOrderColumn'] = row['sortOrderColumn'];
      }
    }
    switch (type) {
      case CrudType.CREATE:
        if(this.inputObject.fromComponentType =="customer" && !this.permission.canCreate){
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditCustomerPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        Object.keys(this.otherParams).forEach((key) => {
          if (key.toLowerCase().includes('date')) {
            this.otherParams[key] = this.momentService.localToUTC(this.otherParams[key]);
          } else {
            this.otherParams[key] = this.otherParams[key];
          }
        });
        switch (this.selectedTabIndex) {
          case 0:
            this.getCustomers(
              row["pageIndex"], row["pageSize"], this.otherParams);
            break;
        }
        break;
      case CrudType.EDIT:
        if(this.inputObject.fromComponentType =="customer" && !this.permission.canEdit){
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditCustomerPage(CrudType.EDIT, row);
        break;
    }
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  openAddEditCustomerPage(type: CrudType | string, editableObject): void {
    if(type === 'create') {
      this.router.navigate(['/customer/manage-customers/holiday-instructions/add-edit'],  { queryParams: { customerId: this.inputObject.customerId, addressId: this.inputObject.addressId } })

    } else {
      this.router.navigate(['/customer/manage-customers/holiday-instructions/add-edit'], { queryParams: { id: editableObject['holidayInstructionId'], customerId: this.inputObject.customerId , addressId: this.inputObject.addressId} })

    }
  }

  getCustomers(pageIndex?: string, pageSize?: string, otherParams?: object): void{
    const params = { customerId: this.inputObject.customerId, addressId: this.inputObject.addressId }
    // const params = { customerId: '0CD0C399-F88C-45E2-9EE9-B9CA5C767F0C', addressId: 'DE3F55C3-14C8-489A-810D-FF261C386DC6' }
    otherParams = { ...otherParams, ...params };
    this.loading = true;
    this.httpService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.HOLIDAY_INSTRUCTION_LIST,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }

  doDeleteSingleRow(){}
  doChangeStatus(){}
  onClose(){}
}
