import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { IApplicationResponse, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared/utils';
import { SignalSpecialsModel } from '@modules/customer/models/signal-specials-model';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-signal-special-add-edit-component',
  templateUrl: './signal-special-add-edit.component.html'
})
export class SignalSpecialAddEditComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  @Input() id: string;
  signalSpecialsAddEditForm: FormGroup;
  userData: any;
  alarmDropDown: any = [];
  signalSpecialId: any;
  customerData: any;
  isDisableFields: boolean = false;
  customerAddressId: any;
  occurrenceBookSignalId: any = [];
  signalHistoryData: any;
  partitionId: any;

  constructor(private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data,
    private rxjsService: RxjsService,
    private httpCancelService: HttpCancelService,
    private crudService: CrudService,
    public dialogRef: MatDialogRef<SignalSpecialAddEditComponent>,
    private dialog: MatDialog,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    });

    this.partitionId = this.data.partitionId;
    this.signalSpecialId = this.data.signalSpecialId;
    this.rxjsService.getCustomerDate().subscribe((data: any) => {
      this.customerData = data;
    });
    this.rxjsService.getCustomerAddresId().subscribe((id: any) => {
      this.customerAddressId = id;
    });
    if (this.signalSpecialId) {
      this.signalSpecialId = this.data.signalSpecialId;
    } else {
      this.occurrenceBookSignalId = this.data?.occurrenceBookSignalId;
    }
  }

  ngOnInit() {
    this.createSignalSpecialsAddEditForm();
    this.rxjsService.setDialogOpenProperty(true);
    if (this.occurrenceBookSignalId) {
      this.getSignalSpecialsDetailsByOccurrenceBookSignalId().subscribe((response: IApplicationResponse) => {
        this.signalHistoryData = response.resources;
        this.signalSpecialsAddEditForm.get('decoder').setValue(this.signalHistoryData.decoder);
        this.signalSpecialsAddEditForm.get('accountCode').setValue(this.signalHistoryData.accountCode);
        this.signalSpecialsAddEditForm.get('alarmTypeId').setValue("");
        this.signalSpecialsAddEditForm.get('eventCode').setValue(this.signalHistoryData.eventCode);
        this.signalSpecialsAddEditForm.get('zoneCode').setValue(this.signalHistoryData.zoneNo);
        this.signalSpecialsAddEditForm.get('originalAlarm').setValue(this.signalHistoryData.alarmType);
        this.signalSpecialsAddEditForm.get('setName').setValue(this.signalHistoryData.setName);
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.getAlarmTypes();
    if (this.signalSpecialId) {
      this.getSignalSpecialsDetailsById(this.signalSpecialId);
    }
  }

  createSignalSpecialsAddEditForm(): void {
    let customerZoneModel = new SignalSpecialsModel();
    this.signalSpecialsAddEditForm = this.formBuilder.group({
    });
    Object.keys(customerZoneModel).forEach((key) => {
      this.signalSpecialsAddEditForm.addControl(key, new FormControl(customerZoneModel[key]));
    });
    this.signalSpecialsAddEditForm = setRequiredValidator(this.signalSpecialsAddEditForm, ["decoder", "alarmTypeId"]);
    this.signalSpecialsAddEditForm.get('createdUserId').setValue(this.userData.userId);
    this.signalSpecialsAddEditForm.get('modifiedUserId').setValue(this.userData.userId);
  }

  getAlarmTypes() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_ALARM_TYPE_ALLOW_TAGGING, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.alarmDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getSignalSpecialsDetailsByOccurrenceBookSignalId(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_SIGNAL_ID,
      this.occurrenceBookSignalId,false

    );
  }

  //Get Details 
  getSignalSpecialsDetailsById(signalSpecialId: string) {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_SPECIALS, signalSpecialId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.signalSpecialsAddEditForm.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSubmit(): void {
    if (this.signalSpecialsAddEditForm.invalid) {
      this.signalSpecialsAddEditForm.markAllAsTouched();
      return;
    }
    let formValue = this.signalSpecialsAddEditForm.value;
    formValue.customerId = this.customerData.customerId;
    formValue.customerAddressId = this.customerAddressId;
    formValue.partitionId = this.partitionId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = (!this.signalSpecialId) ? this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_SPECIALS, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.SIGNAL_SPECIALS, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.rxjsService.setDialogOpenProperty(false);
        this.rxjsService.setGlobalLoaderProperty(false);
        this.dialogRef.close(true);
        this.outputData.emit(true);
      }
    })
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
    this.dialog.closeAll();
    this.dialogRef.close(true);
  }

}
