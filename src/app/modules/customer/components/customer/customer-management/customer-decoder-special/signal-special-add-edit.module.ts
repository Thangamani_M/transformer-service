import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SignalSpecialAddEditComponent } from "./signal-special-add-edit.component";

@NgModule({
  declarations: [SignalSpecialAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [SignalSpecialAddEditComponent],
  entryComponents: [SignalSpecialAddEditComponent],
})
export class SignalSpecialAddEditModule { }