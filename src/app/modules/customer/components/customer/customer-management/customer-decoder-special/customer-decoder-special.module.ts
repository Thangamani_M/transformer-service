import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerDecoderSpecialListComponent } from "./customer-decoder-special-list.component";
import { SignalSpecialAddEditModule } from "./signal-special-add-edit.module";

@NgModule({
  declarations: [CustomerDecoderSpecialListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    SignalSpecialAddEditModule
  ],
  exports: [CustomerDecoderSpecialListComponent],
  entryComponents: [],
})
export class CustomerDecoderSpecialListModule { }