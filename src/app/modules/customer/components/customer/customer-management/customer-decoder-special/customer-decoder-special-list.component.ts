import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ReusablePrimeNGTableFeatureService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { DialogService } from 'primeng/api';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { SignalSpecialAddEditComponent } from './signal-special-add-edit.component';
@Component({
  selector: 'app-customer-decoder-special-list',
  templateUrl: './customer-decoder-special-list.component.html'
})

export class CustomerDecoderSpecialListComponent extends PrimeNgTableVariablesModel implements OnInit {

  primengTableConfigProperties: any;
  row: any = {};
  @Input() partitionId;
  @Input() customerId;
  @Input() customerAddressId;
  dataList: any;

  constructor(private rxjsService: RxjsService,
    private dialog: MatDialog,
    private reusablePrimeNGTableFeatureService: ReusablePrimeNGTableFeatureService,
    private crudService: CrudService,
    private momentService: MomentService,
    public dialogService: DialogService,
    public datePipe: DatePipe) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' },],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'signalSpecialId',
            captionFontSize: '21px',
            enableBreadCrumb: false,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: true,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'decoder', header: 'Decoder', width: '200px' },
            { field: 'accountCode', header: 'Transmitter', width: '200px' },
            { field: 'eventCode', header: 'Actual Info', width: '200px' },
            { field: 'alarmType', header: 'Alarm', width: '200px' },
            { field: 'zone', header: 'Zone', width: '200px' },
            { field: 'zoneDesc', header: 'Zone Description', width: '200px' },
            { field: 'originalAlarm', header: 'Original Alarm', width: '200px' },
            { field: 'createdDate', header: 'Date Added', width: '200px' }],
            apiSuffixModel: EventMgntModuleApiSuffixModels.SIGNAL_SPECIALS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      let otherParams = {}
      if (this.customerId && this.customerAddressId) {
        this.getRequiredListData(null, null, otherParams);
      }
    }

  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.partitionId == '') {
      const params = { customerId: this.customerId, IsAll: true, CustomerAddressId: this.customerAddressId }
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { customerId: this.customerId, IsAll: true, CustomerAddressId: this.customerAddressId, partitionId: this.partitionId }
      otherParams = { ...otherParams, ...params };
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.dataList.map(item=>{
          item.createdDate = this.datePipe.transform(item?.createdDate, 'dd-MM-yyyy, HH:mm:ss');
        })
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.CREATE:
        this.rxjsService.setDialogOpenProperty(true);
        this.openAddEditPage(CrudType.CREATE, row)
        break
      case CrudType.VIEW:
        this.rxjsService.setDialogOpenProperty(true);
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.DELETE:
        this.reusablePrimeNGTableFeatureService.openDynamicDeleteDialogByParams(this.selectedTabIndex, this.selectedRows,
          this.primengTableConfigProperties, row,)?.onClose?.subscribe((result) => {
            if (result) {
              this.selectedRows = [];
              this.getRequiredListData();
            }
          });
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: any): void {
    const dialogReff1 = this.dialog.open(SignalSpecialAddEditComponent, { width: '800px', disableClose: true, data: { ...editableObject, partitionId: this.partitionId } });
    dialogReff1.afterClosed().subscribe(result => {
      if (!result) return;
    });
    dialogReff1.componentInstance.outputData.subscribe(ele => {
      if (ele) {
        this.dataList = this.getRequiredListData();
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }

}
