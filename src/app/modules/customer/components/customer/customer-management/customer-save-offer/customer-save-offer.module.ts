import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { PrimengConfirmDialogPopupModule } from "@app/shared/components/primeng-confirm-dialog-popup/primeng-confirm-dialog-popup.module";
import { MaterialModule } from '@app/shared/material.module';
import { CustomerSaveOfferComponent } from "@modules/customer";
import { CustomerTicketViewPageModule } from "../customer-ticket-view-page/customer-ticket-view-page.module";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
@NgModule({
  declarations: [CustomerSaveOfferComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule,
    CustomerTicketViewPageModule,
    PrimengConfirmDialogPopupModule,
    RouterModule.forChild([
      {path: '', component: CustomerSaveOfferComponent, canActivate:[AuthGuard],data: {title: 'Save Offer'}},
      {path: 'add-edit', loadChildren: () => import('./customer-save-offer-add-edit/customer-save-offer-add-edit.module').then(m => m.CustomerSaveOfferAddEditModule)},
      {path: 'view', loadChildren: () => import('./customer-save-offer-add-edit/customer-save-offer-add-edit.module').then(m => m.CustomerSaveOfferAddEditModule)},
    ])
  ],
  exports: [],
  entryComponents: [],
})
export class CustomerSaveOfferModule { }
