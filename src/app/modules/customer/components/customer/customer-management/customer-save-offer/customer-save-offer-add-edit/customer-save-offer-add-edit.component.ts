import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, getNthMonths, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, OtherService, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setMinMaxDateValidator, setRequiredValidator, SnackbarService } from '@app/shared';
import { CustomerModuleApiSuffixModels, CustomerSaveOfferDetailsModel, CustomerTicketAddEditComponent, SaveOfferDiscountServicesDetailsModel, SaveOfferDiscountSummaryDetailsModel } from '@modules/customer';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'app-customer-save-offer-add-edit',
  templateUrl: './customer-save-offer-add-edit.component.html',
  styleUrls: ['./customer-save-offer-add-edit.component.scss']
})
export class CustomerSaveOfferAddEditComponent implements OnInit {

  selectedTabIndex: any = 0;
  primengTableConfigProperties: any;
  customerId: any;
  loading: boolean;
  addressId: any;
  ticketId: any;
  contractId: any;
  serviceId: any;
  saveOfferId: any;
  saveOfferForm: FormGroup;
  contractList: any = [];
  ticketCancelReasonList: any = [];
  ticketCancelSubReasonList: any = [];
  freeMonthList: any = Array.from({ length: 12 }, (_, i) => i + 1);
  noCostSaveOfferReasonList: any = [];
  departmentList: any = [];
  accessTypeList: any = [];
  submitted: boolean;
  viewable: boolean;
  status: any = [];
  selectedRows: string[] = [];
  totalRecords: any = 0;
  userSubscription: any;
  isViewMore: boolean;
  loggedInUserData: LoggedInUserModel;
  startTodayDate: any = new Date();
  startMaxTodayDate: any = new Date();
  endTodayDate: any = new Date();
  customerServiceDetail: any;
  currLevel: number = 0;
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 3 } });
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, });
  feature: string;
  featureIndex: string;
  isLoading: boolean;

  constructor(private crudService: CrudService, private dialog: MatDialog, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private otherService: OtherService,
    private router: Router, private store: Store<AppState>, private formBuilder: FormBuilder, private snackbarService: SnackbarService, private datePipe: DatePipe,
    public dialogService: DialogService,) {
    this.primengTableConfigProperties = {
      tableCaption: "Save Offer",
      breadCrumbItems: [{ displayName: 'Customer Management:Dashboard', relativeRouterUrl: '' },
      { displayName: 'Save Offer', relativeRouterUrl: `/customer/manage-customers/ticket/save-offer`, queryParams: { id: this.contractId, serviceId: this.serviceId, customerId: this.customerId, addressId: this.addressId } }, { displayName: 'Save Offer Add Edit' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Save Offer',
            dataKey: 'serviceId',
            formArrayName: 'saveOfferDiscountServices',
            columns: [
              { field: 'serviceName', displayName: 'Service Category', type: 'input_text', className: 'col-2', isTooltip: true },
              { field: 'isDiscountInRand', displayName: '', type: 'input_pcheckbox', className: 'col-1', isClickEvent: true, value: true },
              { field: 'isDiscountInPercentage', displayName: '', type: 'input_pcheckbox', className: 'col-1', isClickEvent: true, value: true },
              { field: 'valueInRand', displayName: 'Value', type: 'input_number', className: 'col-2', isClickEvent: true, isTooltip: true, validateInput: this.isADecimalOnly, min: 1 },
              { field: 'subtotal', displayName: 'Sub Total', type: 'input_text', className: 'col-2', isTooltip: true },
              { field: 'discountValue', displayName: 'Disc Value', type: 'input_text', className: 'col-2', isTooltip: true },
              { field: 'adjustedTotal', displayName: 'Adjusted Total', type: 'input_text', className: 'col-2', isTooltip: true },
              { field: 'vat', displayName: 'VAT', type: 'input_text', className: 'col-1', isTooltip: true },
              { field: 'totalValue', displayName: 'Total', type: 'input_text', className: 'col-2', isTooltip: true },
            ],
            enableBreadCrumb: true,
            hideFormArrayAction: true,
            apiSuffixModel: SalesModuleApiSuffixModels.SUBSCRIPTION_CANCEL_CONTRACT_DETAIL,
            moduleName: ModulesBasedApiSuffix.SALES,
          }
        ]
      }
    }
    this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    this.updateDisabledDates({ year: new Date().getFullYear(), month: (new Date().getMonth() + 2) });
    this.activatedRoute.queryParamMap.subscribe((params: any) => {
      this.customerId = params?.params?.customerId ? params?.params?.customerId : '';
      this.addressId = params?.params?.addressId ? params?.params?.addressId : '';
      this.ticketId = params?.params?.ticketId ? params?.params?.ticketId : '';
      this.contractId = params?.params?.id ? params?.params?.id : '';
      this.serviceId = params?.params?.serviceId ? params?.params?.serviceId : '';
      this.viewable = params?.params?.viewable ? params?.params?.viewable : false;
      this.saveOfferId = params?.params?.saveOfferId ? params?.params?.saveOfferId : '';
      this.feature = (Object.keys(params['params']).length > 0) ? params['params']['feature_name'] : '';
      this.featureIndex = (Object.keys(params['params']).length > 0) ? params['params']['featureIndex'] : '';
      this.primengTableConfigProperties.breadCrumbItems[1].queryParams = { id: this.contractId, serviceId: this.serviceId, customerId: this.customerId, addressId: this.addressId };
      if (this.feature && this.featureIndex) {
        this.primengTableConfigProperties.breadCrumbItems[1].queryParams = { id: this.contractId, serviceId: this.serviceId, customerId: this.customerId, addressId: this.addressId, feature_name: this.feature, featureIndex: this.featureIndex };
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.initForm();
    this.onLoadValue();
    this.patchSaveOfferValue();
    if (this.viewable) {
      this.primengTableConfigProperties.tableCaption = 'View Save Offer';
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = 'View Save Offer';
    }
  }

  initForm(customerSaveOfferDetailsModel?: CustomerSaveOfferDetailsModel) {
    const customerSaveOfferModel = new CustomerSaveOfferDetailsModel(customerSaveOfferDetailsModel);
    this.saveOfferForm = this.formBuilder.group({});
    Object.keys(customerSaveOfferModel).forEach((key) => {
      if (typeof customerSaveOfferModel[key] === 'object') {
        if (customerSaveOfferModel[key]?.length == 0) {
          this.saveOfferForm.addControl(key, new FormArray(customerSaveOfferModel[key]));
        } else if (Object.keys(customerSaveOfferModel[key])?.length == 0) {
          this.saveOfferForm.addControl(key, new FormGroup(customerSaveOfferModel[key]));
          this.initFormIntoForm(this.saveOfferForm.get(key));
        }
      } else {
        this.saveOfferForm.addControl(key, new FormControl(customerSaveOfferModel[key]));
      }
    });
    this.saveOfferForm = setRequiredValidator(this.saveOfferForm, ["customerId", "addressId", "contractId", "isComplaint",
      "isServiceCallRequired", "isFreeMonths", "isDiscountServiceMonthlyFee", "isNoCostSaveOffer", "isFreeAccess", "createdUserId"]);
    this.saveOfferForm = setMinMaxDateValidator(this.saveOfferForm, [
      { formControlName: 'numberOfFreeMonthStartDate', compareWith: 'numberOfFreeMonthEndDate', type: 'min' },
      { formControlName: 'numberOfFreeMonthEndDate', compareWith: 'numberOfFreeMonthStartDate', type: 'max' }
    ]);
    this.saveOfferForm.disable({ emitEvent: false });
    if (!this.saveOfferId) {
      this.saveOfferForm.get('isComplaint').enable({ emitEvent: false });
      this.saveOfferForm.get('isServiceCallRequired').enable({ emitEvent: false });
      this.saveOfferForm.get('isFreeMonths').enable({ emitEvent: false });
      this.saveOfferForm.get('isDiscountServiceMonthlyFee').enable({ emitEvent: false });
      this.saveOfferForm.get('isNoCostSaveOffer').enable({ emitEvent: false });
      this.saveOfferForm.get('isFreeAccess').enable({ emitEvent: false });
      this.onValueChanges();
    }
  }

  initFormIntoForm(form, saveOfferDiscountSummaryDetailsModel?: SaveOfferDiscountSummaryDetailsModel) {
    const saveOfferDiscountSummaryModel: any = new SaveOfferDiscountSummaryDetailsModel(saveOfferDiscountSummaryDetailsModel);
    Object.keys(saveOfferDiscountSummaryModel)?.forEach((key: any) => {
      if (typeof saveOfferDiscountSummaryModel[key] == 'object') {
        form.addControl(key, new FormArray(saveOfferDiscountSummaryModel[key]));
      } else {
        form.addControl(key, new FormControl(saveOfferDiscountSummaryModel[key]));
      }
    });
  }

  onValueChanges() {
    this.saveOfferForm.get("isComplaint").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.saveOfferForm.get('isCancellation').setValue(false, { emitEvent: false });
        this.saveOfferForm.get('complaintNotes').enable({ emitEvent: false });
        this.saveOfferForm.get('ticketCancellationReasonId').disable({ emitEvent: false });
        this.saveOfferForm.get('ticketCancellationReasonId').reset('', { emitEvent: false });
        this.saveOfferForm.get('ticketCancellationReasonId').markAsUntouched();
        this.saveOfferForm.get('ticketCancellationSubReasonId').disable({ emitEvent: false });
        this.saveOfferForm.get('ticketCancellationSubReasonId').reset('', { emitEvent: false });
        this.saveOfferForm.get('ticketCancellationSubReasonId').markAsUntouched();
        this.saveOfferForm = setRequiredValidator(this.saveOfferForm, ["complaintNotes"]);
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, ["ticketCancellationReasonId", "ticketCancellationSubReasonId",]);
      } else if (res == false) {
        this.saveOfferForm.get('isCancellation').setValue(true, { emitEvent: false });
        this.saveOfferForm.get('complaintNotes').enable({ emitEvent: false });
        // this.saveOfferForm.get('complaintNotes').reset('', { emitEvent: false });
        // this.saveOfferForm.get('complaintNotes').markAsUntouched();
        this.saveOfferForm.get('ticketCancellationReasonId').enable({ emitEvent: false });
        this.saveOfferForm.get('ticketCancellationSubReasonId').enable({ emitEvent: false });
        this.saveOfferForm = setRequiredValidator(this.saveOfferForm, ["complaintNotes", "ticketCancellationReasonId", "ticketCancellationSubReasonId"]);
        // this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, ["complaintNotes"]);
      }
    });
    this.saveOfferForm.get("ticketCancellationReasonId").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      this.onLoadSubReason(res);
    });
    this.saveOfferForm.get("numberOfFreeMonthId").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res && this.saveOfferForm.get("numberOfFreeMonthStartDate").value) {
        const endDate = getNthMonths(new Date(this.saveOfferForm.get("numberOfFreeMonthStartDate").value), res);
        this.saveOfferForm.get("numberOfFreeMonthEndDate").setValue(endDate, { emitEvent: false });
      }
    });
    this.saveOfferForm.get("numberOfFreeMonthStartDate").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        const endDate = getNthMonths(new Date(res), this.saveOfferForm.get("numberOfFreeMonthId").value);
        this.saveOfferForm.get("numberOfFreeMonthEndDate").setValue(endDate, { emitEvent: false });
      }
    });
    this.saveOfferForm.get("isServiceCallRequired").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, [
          "isFreeMonths", "isDiscountServiceMonthlyFee", "isNoCostSaveOffer", "isFreeAccess",]);
      } else if (res == false) {
        this.getCheckBoxValidators();
      }
    });
    this.saveOfferForm.get("isFreeMonths").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.saveOfferForm.get('numberOfFreeMonthId').enable({ emitEvent: false });
        this.saveOfferForm.get('numberOfFreeMonthStartDate').enable({ emitEvent: false });
        this.saveOfferForm = setRequiredValidator(this.saveOfferForm, ["numberOfFreeMonthId", "numberOfFreeMonthStartDate", "numberOfFreeMonthEndDate"]);
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, [
          "isServiceCallRequired", "isDiscountServiceMonthlyFee", "isNoCostSaveOffer", "isFreeAccess",]);
      } else if (res == false) {
        this.saveOfferForm.get('numberOfFreeMonthId').disable({ emitEvent: false });
        this.saveOfferForm.get('numberOfFreeMonthId').markAsUntouched();
        this.saveOfferForm.get('numberOfFreeMonthId').reset('', { emitEvent: false });
        this.saveOfferForm.get('numberOfFreeMonthStartDate').disable({ emitEvent: false });
        this.saveOfferForm.get('numberOfFreeMonthStartDate').markAsUntouched();
        this.saveOfferForm.get('numberOfFreeMonthStartDate').reset('', { emitEvent: false });
        this.saveOfferForm.get('numberOfFreeMonthEndDate').disable({ emitEvent: false });
        this.saveOfferForm.get('numberOfFreeMonthEndDate').markAsUntouched();
        this.saveOfferForm.get('numberOfFreeMonthEndDate').reset('', { emitEvent: false });
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, ["numberOfFreeMonthId", "numberOfFreeMonthStartDate", "numberOfFreeMonthEndDate"]);
        this.getCheckBoxValidators();
      }
    });
    this.saveOfferForm.get("isDiscountServiceMonthlyFee").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res?.toString()) {
        this.onResetSeriveSummary();
      } else {
        this.onEnableDisableServiceArray();
      }
    });
    this.saveOfferForm.get("isNoCostSaveOffer").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.saveOfferForm.get('noCostSaveOfferReasonConfigId').enable({ emitEvent: false });
        this.saveOfferForm.get('departmentId').enable({ emitEvent: false });
        this.saveOfferForm.get('noCostSaveOfferNotes').enable({ emitEvent: false });
        this.saveOfferForm = setRequiredValidator(this.saveOfferForm, ["noCostSaveOfferReasonConfigId", "departmentId"]);
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, [
          "isServiceCallRequired", "isFreeMonths", "isDiscountServiceMonthlyFee", "isFreeAccess"]);
      } else if (res == false) {
        this.saveOfferForm.get('noCostSaveOfferReasonConfigId').disable({ emitEvent: false });
        this.saveOfferForm.get('noCostSaveOfferReasonConfigId').markAsUntouched();
        this.saveOfferForm.get('noCostSaveOfferReasonConfigId').reset('', { emitEvent: false });
        this.saveOfferForm.get('departmentId').disable({ emitEvent: false });
        this.saveOfferForm.get('departmentId').markAsUntouched();
        this.saveOfferForm.get('departmentId').reset('', { emitEvent: false });
        this.saveOfferForm.get('noCostSaveOfferNotes').disable({ emitEvent: false });
        this.saveOfferForm.get('noCostSaveOfferNotes').markAsUntouched();
        this.saveOfferForm.get('noCostSaveOfferNotes').reset('', { emitEvent: false });
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, ["departmentId", "noCostSaveOfferNotes", "noCostSaveOfferReasonConfigId"]);
        this.getCheckBoxValidators();
      }
    });
    this.saveOfferForm.get("isFreeAccess").valueChanges.pipe(debounceTime(100), distinctUntilChanged()).subscribe(res => {
      if (res) {
        this.saveOfferForm.get('freeAccessTypeId').enable({ emitEvent: false });
        this.saveOfferForm = setRequiredValidator(this.saveOfferForm, ["freeAccessTypeId"]);
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, [
          "isServiceCallRequired", "isFreeMonths", "isDiscountServiceMonthlyFee", "isNoCostSaveOffer"]);
      } else if (res == false) {
        this.saveOfferForm.get('freeAccessTypeId').disable({ emitEvent: false });
        this.saveOfferForm.get('freeAccessTypeId').markAsUntouched();
        this.saveOfferForm.get('freeAccessTypeId').reset('', { emitEvent: false });
        this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, ["freeAccessTypeId"]);
        this.getCheckBoxValidators();
      }
    });
  }

  getCheckBoxValidators() {
    if (!this.saveOfferForm.get('isServiceCallRequired').value && !this.saveOfferForm.get('isDiscountServiceMonthlyFee').value
      && !this.saveOfferForm.get('isNoCostSaveOffer').value && !this.saveOfferForm.get('isFreeMonths').value
      && !this.saveOfferForm.get('isFreeAccess').value) {
      this.saveOfferForm = setRequiredValidator(this.saveOfferForm, ["isServiceCallRequired", "isDiscountServiceMonthlyFee", "isNoCostSaveOffer", "isFreeMonths", "isFreeAccess"]);
    }
  }

  onLoadSubReason(res) {
    if (res) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TICKET_CANCELLATION_SUB_REASON, prepareRequiredHttpParams({ ticketCancellationReasonId: res })).subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.ticketCancelSubReasonList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  initSaveOfferDiscountFormArray(saveOfferDiscountDetailsModel?: SaveOfferDiscountServicesDetailsModel) {
    let saveOfferDiscountModel = new SaveOfferDiscountServicesDetailsModel(saveOfferDiscountDetailsModel);
    let saveOfferDiscountFormArray = this.formBuilder.group({});
    Object.keys(saveOfferDiscountModel).forEach((key) => {
      saveOfferDiscountFormArray.addControl(key, new FormControl({ value: saveOfferDiscountModel[key], disabled: true }));
    });
    this.getServicesOfferDiscountArray.push(saveOfferDiscountFormArray);
  }

  onAfterChangeAddress(e) {
    this.addressId = e;
  }

  get getServicesOfferDiscountArray(): FormArray {
    if (!this.saveOfferForm) return;
    return this.saveOfferForm?.get('saveOfferDiscountServices') as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onLoadValue() {
    let api = [
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TICKET_CANCELLATION_REASON),
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.UX_NO_COST_SAVE_OFFER),
      this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_MONTHS),
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_DEPARTMENTS),
      this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.UX_ACCESS_TYPE),
    ]
    if (this.viewable) {
      api.push(this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SAVE_OFFER_DETAILS, null, false, prepareRequiredHttpParams({ saveOfferId: this.saveOfferId })));
    } else {
      api.push(this.crudService.dropdown(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SAVE_OFFER_SERVICE_DETAILS, prepareRequiredHttpParams({ contractId: this.contractId })));
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    forkJoin(api).subscribe((result: IApplicationResponse[]) => {
      result?.forEach((res: IApplicationResponse, ix: number) => {
        switch (ix) {
          case 0:
            this.ticketCancelReasonList = res?.resources;
            break;
          case 1:
            this.noCostSaveOfferReasonList = res?.resources;
            break;
          case 2:
            this.freeMonthList = res?.resources;
            break;
          case 3:
            this.departmentList = res?.resources;
            break;
          case 4:
            this.accessTypeList = res?.resources;
            break;
          case 5:
            if (this.viewable) {
              this.onLoadSaveOffer(res);
            } else {
              this.onLoadSalesServiceDetail(res);
            }
            break;
        }
      })
      // this.rxjsService.setGlobalLoaderProperty(false);
      this.isLoading = false;
    })

  }

  patchSaveOfferValue() {
    this.saveOfferForm.patchValue({
      doaApprovalLevel: 0,
      addressId: this.addressId,
      contractId: this.contractId,
      createdUserId: this.loggedInUserData?.userId,
      customerId: this.customerId,
    }, { emitEvent: false })
  }

  onLoadSalesServiceDetail(response: IApplicationResponse) {
    if (response?.isSuccess && response?.statusCode == 200) {
      this.customerServiceDetail = response?.resources;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName += ` : ${this.customerServiceDetail?.contractRefNo}`;
      this.saveOfferForm.get('contractRefNo').setValue(this.customerServiceDetail?.contractRefNo);
      this.onResetSeriveSummary();
    }
  }

  combineLatestNgrxStoreData() {
    this.userSubscription = combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onShowViewPage(e) {
    this.isViewMore = e;
  }

  onChangeFormArray(e: any) {
    if (!this.saveOfferForm.get('isDiscountServiceMonthlyFee').value) {
      return;
    }
    if (!this.saveOfferForm.disabled) {
      this.onAnyOneValidServiceDiscount();
      if (e?.column?.field == 'isDiscountInRand' && this.getServicesOfferDiscountArray.controls[e?.index].get('isDiscountInRand').value) {
        this.getServicesOfferDiscountArray.controls[e?.index].get('isDiscountInPercentage').setValue(false);
      } else if (e?.column?.field == 'isDiscountInPercentage' && this.getServicesOfferDiscountArray.controls[e?.index].get('isDiscountInPercentage').value) {
        this.getServicesOfferDiscountArray.controls[e?.index].get('isDiscountInRand').setValue(false);
      }
      if ((e?.column?.field == 'isDiscountInPercentage' || e?.column?.field == 'isDiscountInRand' || e?.column?.field == 'valueInRand') &&
        (this.getServicesOfferDiscountArray.controls[e?.index].get('isDiscountInPercentage').value || this.getServicesOfferDiscountArray.controls[e?.index].get('isDiscountInRand').value
          || this.getServicesOfferDiscountArray.controls[e?.index].get('valueInRand').value)) {
        if (this.getServicesOfferDiscountArray.controls[e?.index].get('isDiscountInPercentage').value || this.getServicesOfferDiscountArray.controls[e?.index].get('isDiscountInRand').value) {
          this.onAnyOneValidServiceDiscount(e?.index);
        }
        this.calculateSalesServicedetail(e?.index);
      } else {
        this.onAnyOneValidServiceDiscount(e?.index);
        this.calculateSalesServicedetail(e?.index);
      }
    }
  }

  getNotesType() {
    return this.saveOfferForm?.get('isComplaint').value == true ? 'Complaint' : 
      this.saveOfferForm?.get('isComplaint').value == false ? 'Cancellation' : '';
  }

  validateServiceDiscount() {
    let findArr = [];
    this.getServicesOfferDiscountArray.controls?.forEach((el: any) => {
      if (el?.get('isDiscountInPercentage').value || el?.get('isDiscountInRand').value) {
        findArr?.push(true);
      }
    });
    return findArr?.length;
  }

  onAnyOneValidServiceDiscount(ix?: number) {
    if (!this.validateServiceDiscount()) {
      this.onResetSeriveSummary();
      return;
    }
    this.getServicesOfferDiscountArray.controls?.forEach((el: any, i: number) => {
      if (!el?.get('isDiscountInPercentage').value && !el?.get('isDiscountInRand').value) {
        el.get('valueInRand').setValue('');
        el.get('valueInRand').disable();
        el = clearFormControlValidators(el, ["isDiscountInPercentage", "isDiscountInRand", "valueInRand", "discountValue"]);
      } else if (ix == i || (el?.get('isDiscountInPercentage').value || el?.get('isDiscountInRand').value)) {
        el = setRequiredValidator(el, ["isDiscountInPercentage", "isDiscountInRand", "valueInRand"]);
        el.get('valueInRand').enable();
        if (el?.get('isDiscountInRand').value) {
          el.get('valueInRand').setValidators([Validators.required, Validators.min(1)]);
        } else if (el?.get('isDiscountInPercentage').value) {
          el.get('valueInRand').setValidators([Validators.required, Validators.min(1), Validators.max(100)]);
        }
        el.get('valueInRand').updateValueAndValidity();
      }
      this.getServicesOfferDiscountArray.controls[i].updateValueAndValidity();
    });
  }

  //Calculation function for sales service formarray
  calculateSalesServicedetail(index) {
    const value = this.getServicesOfferDiscountArray.controls[index].get('valueInRand').value ? this.getServicesOfferDiscountArray.controls[index].get('valueInRand').value?.replace(/%/g, '') : 0;
    // const discFlag = this.getServicesOfferDiscountArray.controls[index].get('isDiscountInPercentage').value;
    const subtotal = this.otherService.transformCurrToNum(this.getServicesOfferDiscountArray.controls[index].get('subtotal').value);
    let discValue = 0;
    if (this.getServicesOfferDiscountArray.controls[index].get('isDiscountInRand').value == true) {
      discValue = value;
    } else if (this.getServicesOfferDiscountArray.controls[index].get('isDiscountInPercentage').value == true) {
      discValue = subtotal * (value / 100);
    } else {
      return;
    }
    this.getServicesOfferDiscountArray.controls[index].get('discountValue').setValue(discValue ?
      this.otherService.transformDecimal(discValue) : '', { emitEvent: false });
    const adjTotal = subtotal - discValue;
    this.getServicesOfferDiscountArray.controls[index].get('adjustedTotal').setValue(
      this.otherService.transformDecimal(adjTotal), { emitEvent: false });
    const vat = this.otherService.transformCurrToNum(this.getServicesOfferDiscountArray.controls[index].get('vat').value);
    const total = adjTotal + vat;
    this.getServicesOfferDiscountArray.controls[index].get('totalValue').setValue(
      this.otherService.transformDecimal(total), { emitEvent: false });
    this.onCalculateSaleServiceSummary();
  }

  //Calculation function for sales service summary
  onCalculateSaleServiceSummary() {
    let dedecAmt = 0;
    let totPercent: any = 0;
    let futureMonthlyInst = 0;
    this.getServicesOfferDiscountArray.controls.forEach(el => {
      if (el) {
        dedecAmt += this.otherService.transformCurrToNum(el.get('discountValue').value);
        futureMonthlyInst += this.otherService.transformCurrToNum(el.get('adjustedTotal').value);
        if (el?.get('isDiscountInPercentage').value == true) {
          totPercent += +el.get('valueInRand').value;
        } else if (el?.get('isDiscountInRand').value == true) {
          const discountValue = this.otherService.transformCurrToNum(el.get('discountValue').value);
          const subtotalPer = this.otherService.transformCurrToNum(el.get('subtotal').value);
          totPercent += (discountValue / subtotalPer) * 100;
        }
      }
    });
    this.saveOfferForm.get('saveOfferDiscountSummary.deductionAmount').setValue(
      this.otherService.transformDecimal(dedecAmt), { emitEvent: false });
    this.saveOfferForm.get('saveOfferDiscountSummary.percentageOfDeduction').setValue(
      ((totPercent / this.getServicesOfferDiscountArray.length).toFixed(0)) + '%', { emitEvent: false });
    this.saveOfferForm.get('saveOfferDiscountSummary.futureMonthlyInst').setValue(
      this.otherService.transformDecimal(futureMonthlyInst), { emitEvent: false });
  }

  onBack() {
    let queryParams = { id: this.contractId, serviceId: this.serviceId, customerId: this.customerId, addressId: this.addressId };
    if (this.feature && this.featureIndex) {
      queryParams['feature_name'] = this.feature;
      queryParams['featureIndex'] = this.featureIndex;
    }
    this.router.navigate(['./customer/manage-customers/ticket/save-offer'], { queryParams: queryParams });
  }

  onServiceCallTicket() {
    this.openTicketModal(this.saveOfferForm?.get('serviceCallTicketTypeId').value);
  }

  onNotifyCreditControl() {
    this.openTicketModal(this.saveOfferForm?.get('creditControlTicketTypeId').value);
  }

  onFreeAccessTicket() {
    this.openTicketModal(this.saveOfferForm?.get('freeAccessTypeId').value);
  }

  openTicketModal(ticketTypeId) {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialogService.open(CustomerTicketAddEditComponent, { baseZIndex: 1000, width: '700px', showHeader: false, data: { custId: this.customerId, addressId: this.addressId, ticketTypeId: ticketTypeId, isDisableTickeTypeId: true } });
    dialogReff.onClose.subscribe(result => {
      if (result) {
        if (result?.ticketTypeId == this.saveOfferForm?.get('serviceCallTicketTypeId').value) {
          this.saveOfferForm?.get('serviceCallTicketRefNo').setValue(result?.ticketRefNo);
          this.saveOfferForm?.get('serviceCallTicketId').setValue(result?.ticketId);
        } else if (result?.ticketTypeId == this.saveOfferForm?.get('creditControlTicketTypeId').value) {
          this.saveOfferForm?.get('creditControlTicketRefNo').setValue(result?.ticketRefNo);
          this.saveOfferForm?.get('creditControlTicketId').setValue(result?.ticketId);
        } else if (result?.ticketTypeId == this.saveOfferForm?.get('freeAccessTypeId').value) {
          this.saveOfferForm?.get('freeAccessTicketRefNo').setValue(result?.ticketRefNo);
          this.saveOfferForm?.get('freeAccessTicketId').setValue(result?.ticketId);
        }
        return;
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  getSaveOfferVaild() {
    return this.saveOfferForm.get('isServiceCallRequired').value == false && this.saveOfferForm.get('isFreeMonths').value == false && this.saveOfferForm.get('isDiscountServiceMonthlyFee').value == false && this.saveOfferForm.get('isNoCostSaveOffer').value == false && this.saveOfferForm.get('isFreeAccess').value == false;
  }

  onSubmit() {
    if (this.saveOfferForm.invalid) {
      this.saveOfferForm.markAllAsTouched();
      return;
    } else if (this.saveOfferForm.get('isServiceCallRequired').value == false && this.saveOfferForm.get('isFreeMonths').value == false && this.saveOfferForm.get('isDiscountServiceMonthlyFee').value == false && this.saveOfferForm.get('isNoCostSaveOffer').value == false && this.saveOfferForm.get('isFreeAccess').value == false) {
      this.snackbarService.openSnackbar("Please select atleast one offer", ResponseMessageTypes.WARNING);
      return;
    } else if (!this.saveOfferForm.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    const message = `Are you sure you want to process the save offer?`;
    const dialogData = new ConfirmDialogModel("Cancel Request", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      this.onSaveOfferProcess();
    })
  }

  onSaveOfferProcess() {
    const reqObj = {
      ...this.saveOfferForm.getRawValue()
    }
    reqObj.saveOfferDiscountServices.forEach(el => {
      if (el) {
        el.isDiscountInRand = el?.isDiscountInRand || false;
        el.IsDiscountInPercentage = el?.isDiscountInPercentage || false;
        el.valueInPercentage = el?.valueInPercentage ? this.otherService.transformCurrToNum(el?.valueInPercentage) : '';
        el.subtotal = el?.subtotal ? this.otherService.transformCurrToNum(el?.subtotal) : '';
        el.discountValue = el?.discountValue ? this.otherService.transformCurrToNum(el?.discountValue) : 0;
        el.adjustedTotal = el?.adjustedTotal ? this.otherService.transformCurrToNum(el?.adjustedTotal) : '';
        el.vat = el?.vat ? this.otherService.transformCurrToNum(el?.vat) : '';
        el.totalValue = el?.totalValue ? this.otherService.transformCurrToNum(el?.totalValue) : '';
        if(el?.isDiscountInPercentage) {
          el.valueInPercentage = el.valueInRand ? +el.valueInRand : '';
        }
        if(!el?.isDiscountInPercentage) {
          el.valueInRand = el.valueInRand ? +el.valueInRand : '';
        }
      }
    });
    reqObj['numberOfFreeMonthStartDate'] = reqObj['numberOfFreeMonthStartDate'] ? this.datePipe.transform(reqObj['numberOfFreeMonthStartDate'], 'yyyy-MM-dd') : null
    reqObj['numberOfFreeMonthEndDate'] = reqObj['numberOfFreeMonthEndDate'] ? this.datePipe.transform(reqObj['numberOfFreeMonthEndDate'], 'yyyy-MM-dd') : null
    delete reqObj['serviceCallTicketTypeId'];
    delete reqObj['creditControlTicketTypeId'];
    Object.keys(reqObj.saveOfferDiscountSummary).forEach((el: any) => {
      if (el == 'percentageOfDeduction') {
        reqObj.saveOfferDiscountSummary['percentageOfDeduction'] = reqObj.saveOfferDiscountSummary['percentageOfDeduction']?.replace("%", "");
      } else {
        reqObj.saveOfferDiscountSummary[el] = this.otherService.transformCurrToNum(reqObj.saveOfferDiscountSummary[el]);
      }
    });
    this.submitted = true;
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SAVE_OFFER, reqObj)
      .subscribe((res: IApplicationResponse) => {
        this.submitted = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res?.isSuccess && res?.statusCode == 200) {
          this.saveOfferId = res?.resources;
          this.onAfterSaveOffer();
        }
      })
  }

  onAfterSaveOffer() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.SAVE_OFFER_DETAILS, null, false, prepareRequiredHttpParams({ saveOfferId: this.saveOfferId }))
      .subscribe((res: IApplicationResponse) => {
        this.onLoadSaveOffer(res);
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onLoadSaveOffer(res: IApplicationResponse) {
    if (res?.isSuccess && res?.statusCode == 200) {
      this.customerServiceDetail = res?.resources;
      this.saveOfferForm.reset();
      this.clearFormArray(this.getServicesOfferDiscountArray);
      this.saveOfferForm.patchValue({
        addressId: res?.resources?.addressId,
        complaintNotes: res?.resources?.complaintNotes,
        contractId: res?.resources?.contractId,
        contractRefNo: res?.resources?.contractRefNo,
        createdUserId: res?.resources?.createdUserId,
        customerId: res?.resources?.customerId,
        departmentId: res?.resources?.departmentId ? res?.resources?.departmentId : '',
        doaApprovalLevel: res?.resources?.doaApprovalLevel,
        freeAccessTypeId: res?.resources?.freeAccessTypeId ? res?.resources?.freeAccessTypeId : '',
        isCancellation: res?.resources?.isCancellation,
        isComplaint: res?.resources?.isComplaint,
        isDiscountServiceMonthlyFee: res?.resources?.isDiscountServiceMonthlyFee,
        isFreeAccess: res?.resources?.isFreeAccess,
        isFreeMonths: res?.resources?.isFreeMonths,
        isNoCostSaveOffer: res?.resources?.isNoCostSaveOffer,
        isServiceCallRequired: res?.resources?.isServiceCallRequired,
        noCostSaveOfferNotes: res?.resources?.noCostSaveOfferNotes,
        noCostSaveOfferReasonConfigId: res?.resources?.noCostSaveOfferReasonConfigId ? res?.resources?.noCostSaveOfferReasonConfigId : '',
        numberOfFreeMonthEndDate: res?.resources?.numberOfFreeMonthEndDate ? new Date(res?.resources?.numberOfFreeMonthEndDate) : null,
        numberOfFreeMonthId: res?.resources?.numberOfFreeMonthId ? res?.resources?.numberOfFreeMonthId : '',
        numberOfFreeMonthStartDate: res?.resources?.numberOfFreeMonthStartDate ? new Date(res?.resources?.numberOfFreeMonthStartDate) : null,
        ticketCancellationReasonId: res?.resources?.ticketCancellationReasonId ? res?.resources?.ticketCancellationReasonId : '',
        ticketCancellationSubReasonId: res?.resources?.ticketCancellationSubReasonId ? res?.resources?.ticketCancellationSubReasonId : '',
        serviceCallTicketTypeId: res?.resources?.serviceCallTicketTypeId ? res?.resources?.serviceCallTicketTypeId : '',
        creditControlTicketTypeId: res?.resources?.creditControlTicketTypeId ? res?.resources?.creditControlTicketTypeId : '',
        creditControlTicketId: res?.resources?.creditControlTicketId ? res?.resources?.creditControlTicketId : '',
        creditControlTicketRefNo: res?.resources?.creditControlTicketRefNo ? res?.resources?.creditControlTicketRefNo : '',
        freeAccessTicketId: res?.resources?.freeAccessTicketId ? res?.resources?.freeAccessTicketId : '',
        freeAccessTicketRefNo: res?.resources?.freeAccessTicketRefNo ? res?.resources?.freeAccessTicketRefNo : '',
        serviceCallTicketId: res?.resources?.serviceCallTicketId ? res?.resources?.serviceCallTicketId : '',
        serviceCallTicketRefNo: res?.resources?.serviceCallTicketRefNo ? res?.resources?.serviceCallTicketRefNo : '',
      }, { emitEvent: false });
      if (res?.resources?.ticketCancellationReasonId) {
        this.onLoadSubReason(res?.resources?.ticketCancellationReasonId);
      }
      if (res?.resources?.saveOfferDiscountServices) {
        res?.resources?.saveOfferDiscountServices?.forEach(el => {
          if (el) {
            this.onSetFormArray(el);
          }
        });
      }
      if (res?.resources?.saveOfferDiscountSummary) {
        this.onSetServiceSummary(res?.resources?.saveOfferDiscountSummary);
      }
      this.saveOfferForm.disable({ emitEvent: false });
      this.getServicesOfferDiscountArray.disable({ emitEvent: false });
    }
  }

  getStyleClass() {
    return !this.saveOfferId && this.saveOfferForm?.get('isFreeMonths').value ? 'start-date-enabled' : '';
  }

  onRedirectToTicketPage(val) {
    if (val == "service") {
      this.router.navigate(['/customer/manage-customers/ticket/view'], { queryParams: { id: this.saveOfferForm.get("serviceCallTicketId").value, customerId: this.customerId, addressId: this.addressId, fromComponent: 'Tickets' } });
    } else if (val == "credit") {
      this.router.navigate(['/customer/manage-customers/ticket/view'], { queryParams: { id: this.saveOfferForm.get("creditControlTicketId").value, customerId: this.customerId, addressId: this.addressId, fromComponent: 'Tickets' } });
    } else if (val == "freeAccess") {
      this.router.navigate(['/customer/manage-customers/ticket/view'], { queryParams: { id: this.saveOfferForm.get("freeAccessTicketId").value, customerId: this.customerId, addressId: this.addressId, fromComponent: 'Tickets' } });
    }
  }

  onResetSeriveSummary() {
    if (this.getServicesOfferDiscountArray?.length) {
      this.clearFormArray(this.getServicesOfferDiscountArray);
    }
    this.customerServiceDetail?.saveOfferDiscountServices?.forEach(el => {
      if (el) {
        this.onSetFormArray(el);
      }
    });
    this.onSetServiceSummary(this.customerServiceDetail?.saveOfferDiscountSummary);
    this.onEnableDisableServiceArray();
  }

  onEnableDisableServiceArray() {
    if (this.saveOfferForm.get("isDiscountServiceMonthlyFee").value) {
      this.getServicesOfferDiscountArray?.controls?.forEach((el: any) => {
        if (el && !this.saveOfferId) {
          el.get('isDiscountInRand').enable();
          el.get('isDiscountInPercentage').enable();
          // el.get('valueInRand').enable();
          el = setRequiredValidator(el, ["isDiscountInPercentage", "isDiscountInRand", "valueInRand"]);
          this.saveOfferForm = clearFormControlValidators(this.saveOfferForm, [
            "isFreeMonths", "isServiceCallRequired", "isNoCostSaveOffer", "isFreeAccess",]);
        }
      });
    } else if (this.saveOfferForm.get("isDiscountServiceMonthlyFee").value == false || !this.saveOfferForm.get("isDiscountServiceMonthlyFee").value) {
      this.getServicesOfferDiscountArray?.controls?.forEach((el: any) => {
        if (el) {
          el.disable();
          el.get('isDiscountInRand').reset(null, { emitEvent: false });
          el.get('isDiscountInPercentage').reset(null, { emitEvent: false });
          // el.get('valueInRand').reset('', { emitEvent: false });
          el = clearFormControlValidators(el, ["isDiscountInPercentage", "isDiscountInRand", "valueInRand", "discountValue"]);
        }
        this.getCheckBoxValidators();
      });
    }
  }

  onSetFormArray(el) {
    var obj = {
      subscriptionServiceId: el?.subscriptionServiceId,
      serviceName: el?.serviceName,
      isDiscountInRand: el?.isDiscountInRand,
      isDiscountInPercentage: el?.isDiscountInPercentage,
      valueInRand: el?.valueInRand ? this.otherService.transformDecimal(el?.valueInRand) : '',
      valueInPercentage: el?.valueInPercentage ? `${el?.valueInPercentage}%` : '',
      subtotal: el?.subtotal ? this.otherService.transformDecimal(el?.subtotal) : '',
      discountValue: el?.discountValue ? this.otherService.transformDecimal(el?.discountValue) : '',
      adjustedTotal: el?.adjustedTotal ? this.otherService.transformDecimal(el?.adjustedTotal) : '',
      vat: el?.vat ? this.otherService.transformDecimal(el?.vat) : '',
      totalValue: el?.totalValue ? this.otherService.transformDecimal(el?.totalValue) : '',
    };
    if (el?.isDiscountInPercentage && this.saveOfferId) {
      obj['valueInRand'] = obj?.valueInPercentage;
    }
    this.initSaveOfferDiscountFormArray(obj);
  }

  onSetServiceSummary(el) {
    this.saveOfferForm.get('saveOfferDiscountSummary').setValue({
      currentMonthlyInst: el?.currentMonthlyInst?.toString() ? this.otherService.transformDecimal(el?.currentMonthlyInst) : '',
      percentageOfDeduction: el?.percentageOfDeduction?.toString() ? `${el?.percentageOfDeduction}%` : '',
      deductionAmount: el?.deductionAmount?.toString() ? this.otherService.transformDecimal(el?.deductionAmount) : '',
      futureMonthlyInst: el?.futureMonthlyInst?.toString() ? this.otherService.transformDecimal(el?.futureMonthlyInst) : '',
      freeServices: '',
    });
  }

  updateDisabledDates(event) {
    if ((event.month > new Date().getMonth() + 1 && new Date().getFullYear() == event.year) || (new Date().getFullYear() < event.year)) {
      this.startTodayDate = new Date(event.year, event.month - 1, 1);
      this.startMaxTodayDate = new Date(event.year, event.month - 1, 1);
    }
  }

  getHeight() {
    return this.isViewMore ? '35vh' : '48vh';
  }

  ngOnDestroy() {
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }
}
