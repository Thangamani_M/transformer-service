import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerTicketViewPageModule } from "../../customer-ticket-view-page/customer-ticket-view-page.module";
import { CustomerTicketAddEditModule } from "../../customer-ticket/customer-ticket-add-edit.module";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

import { CustomerSaveOfferAddEditComponent } from "./customer-save-offer-add-edit.component";

@NgModule({
  declarations: [CustomerSaveOfferAddEditComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    CustomerTicketViewPageModule,
    CustomerTicketAddEditModule,
    RouterModule.forChild([
        {path: '', component: CustomerSaveOfferAddEditComponent,canActivate:[AuthGuard], data: {title: 'Save Offer Add Edit'}},
    ])
  ],
  exports: [],
  entryComponents: [],
})
export class CustomerSaveOfferAddEditModule { }
