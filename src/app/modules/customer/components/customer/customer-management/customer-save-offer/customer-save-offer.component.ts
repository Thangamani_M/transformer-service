import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, PrimengConfirmDialogPopupComponent, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CustomerModuleApiSuffixModels, CUSTOMER_COMPONENT } from '@modules/customer';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-customer-save-offer',
  templateUrl: './customer-save-offer.component.html'
})
export class CustomerSaveOfferComponent implements OnInit {

  @Input() loading: boolean;
  dataList: any;
  status: any = [];
  selectedRows: string[] = [];
  totalRecords: any;
  userSubscription: any;
  listSubscription: any;
  contractId: any;
  contractName: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  primengTableConfigProperties: any;
  primengTableSaveOfferConfigProperties: any;
  selectedTabIndex: any = 0;
  loggedInUserData: any;
  searchColumns: any;
  isViewMore: boolean;
  observableResponse: any;
  isShowNoRecords: any = true;
  customerId: any;
  customerAddressId: any;
  selectedRowData: any;
  contractDetail: any;
  serviceId: any;
  saveOfferAlertDialog: any;
  feature: string;
  featureIndex: string;
  isLoading: boolean;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private datePipe: DatePipe, private dialogService: DialogService,
    private crudService: CrudService, private store: Store<AppState>, private rxjsService: RxjsService, private snackbarService : SnackbarService) {
    this.primengTableConfigProperties = {
      tableCaption: 'View Save Offers',
      breadCrumbItems: [{ displayName: 'Customer Management:Dashboard', relativeRouterUrl: '' },
      { displayName: 'View Customer', relativeRouterUrl: `/customer/manage-customers/view/`, isSkipLocationChange: true }, { displayName: 'View Save Offers' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'View Save Offers',
            dataKey: 'saveOfferId',
            enableAction: false,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'saveOfferRefNo', header: 'Save Offer ID', width: '80px' },
            { field: 'requestDate', header: 'Req. Date', width: '70px', isDateTime: true },
            { field: 'saveOfferReason', header: 'Save Offer Reason', width: '120px' },
            { field: 'serviceCall', header: 'Service Call', width: '70px' },
            { field: 'freeMonths', header: 'Free Months', width: '80px' },
            { field: 'discounts', header: 'Discounts %', width: '70px' },
            { field: 'noCostSaveOffer', header: 'No Cost Save Offer', width: '100px' },
            { field: 'freeAccess', header: 'Fee Access', width: '70px' },
            { field: 'status', header: 'Status', width: '80px', isValue: true },
            { field: 'declineReason', header: 'Decline Reason', width: '100px' },],
            apiSuffixModel: CustomerModuleApiSuffixModels.SAVE_OFFER,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
          },
        ]
      }
    }

    this.primengTableSaveOfferConfigProperties = {
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableAddActionBtn: true,
            enableClearfix: true,
          },
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.contractId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.serviceId = (Object.keys(params['params']).length > 0) ? params['params']['serviceId'] : '';
      this.customerId = (Object.keys(params['params']).length > 0) ? params['params']['customerId'] : '';
      this.customerAddressId = (Object.keys(params['params']).length > 0) ? params['params']['addressId'] : '';
      this.feature = (Object.keys(params['params']).length > 0) ? params['params']['feature_name'] : '';
      this.featureIndex = (Object.keys(params['params']).length > 0) ? params['params']['featureIndex'] : '';
      this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl += this.customerId;
      this.primengTableConfigProperties.breadCrumbItems[1].queryParams = {addressId: this.customerAddressId, navigateTab: 6};
      if(this.feature && this.featureIndex) {
        this.primengTableConfigProperties.breadCrumbItems[1].queryParams = {addressId: this.customerAddressId, navigateTab: 6, feature_name: this.feature, featureIndex: this.featureIndex};
      }
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreDataOne()
    this.combineLatestNgrxStoreData();
    // this.getRequiredListData();
    this.onLoadValue();
  }
  combineLatestNgrxStoreDataOne() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)])
      .subscribe((response) => {
        let permissions = response[0][CUSTOMER_COMPONENT.CUSTOMER];
        let customerManagement = permissions.find(item => item.menuName == "Customer Dashboard")
        if (customerManagement) {
          let subscription = customerManagement['subMenu'].find(item =>item.menuName.includes("SUBSCRIPTION"))
          let subscriptionQuickAction = subscription['subMenu'].find(item =>item.menuName.includes("Quick Action"))
          let saveOffer = subscriptionQuickAction['subMenu'].find(item =>item.menuName.includes("Save Offer"))
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, saveOffer['subMenu']);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
  }

  onShowViewPage(e) {
    this.isViewMore = e;
  }

  onAfterChangeAddress(e) {
    this.customerAddressId = e;
    // this.getContracts();
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }

  combineLatestNgrxStoreData() {
    this.userSubscription = combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onLoadValue() {
    this.loading = true;
    this.isShowNoRecords = false;
    this.dataList = [];
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    const otherParams = { customerId: this.customerId, addressId: this.customerAddressId, contractId: this.contractId };
    let api = [this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.BILL_OF_SERVICE_DEBTOR_SERVICE, null, null,
      prepareRequiredHttpParams({ CustomerId: this.customerId, SiteAddressId: this.customerAddressId,  contractDbId: this?.contractId, serviceId: this.serviceId })),
      this.crudService.get(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      salesModuleApiSuffixModels, undefined, false, prepareGetRequestHttpParams(null, null, otherParams)),
    ];
      forkJoin(api).subscribe((response: IApplicationResponse[]) => {
        response?.forEach((res: IApplicationResponse, ix: number) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            switch(ix) {
              case 0:
                this.contractDetail = res?.resources;
                this.primengTableConfigProperties.tableCaption += this.contractDetail?.contractId ? ` : ${this.contractDetail?.contractId}` : '';
                this.primengTableConfigProperties.breadCrumbItems[2].displayName += this.contractDetail?.contractId ? ` : ${this.contractDetail?.contractId}` : '';
                break;
              case 1:
                this.onPatchTableValue(res);
                break;
            }
          }
        })
        this.isLoading = false;
        // this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.isShowNoRecords = false;
    this.dataList = [];
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = { customerId: this.customerId, addressId: this.customerAddressId, contractId: this.contractId, ...otherParams };
    let api = this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      salesModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams));
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
    }
    this.listSubscription = api.pipe(map((res: IApplicationResponse) => {
			if(res?.resources) {
				res?.resources?.forEach(val => {
					// val.requestDate = this.datePipe.transform(val.requestDate, 'dd-MM-yyyy');
					val.monthId = val.monthId ? (val.monthId == 1 ? `${val.monthId} month` : val.monthId > 1 ? `${val.monthId} months` : 'No') : 'No';
          val.discounts = (val?.percentageOfDeduction && val?.deductionAmount) ? `R ${val?.deductionAmount} - ${val?.percentageOfDeduction}%` :
            val?.deductionAmount ? `R ${val?.deductionAmount}` : val?.percentageOfDeduction ? `${val?.percentageOfDeduction}%` : '';
					return val;
				});
			}
			return res;
		})).subscribe((data: IApplicationResponse) => {
      this.onPatchTableValue(data);
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onPatchTableValue(data) {
      if (data.isSuccess && data?.statusCode == 200) {
        this.observableResponse = data?.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data?.totalCount ? data?.totalCount : 0;
        this.isShowNoRecords = true;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse;
        this.totalRecords = 0;
        this.isShowNoRecords = true;
      }
      this.loading = false;
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: object | any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.rxjsService.setGlobalLoaderProperty(true);
            this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.VALIDATE_SAVE_OFFER_EXIST, null, false,
              prepareRequiredHttpParams({customerId: this.customerId, addressId: this.customerAddressId, contractId: this.contractId}))
            .subscribe((res: IApplicationResponse) => {
              if(res?.isSuccess && res?.statusCode == 200) {
                if(res?.resources?.isExist) {
                  this.openConfirmDialog({title: 'Save Offer', message: res?.resources?.message, isConfirm: true, msgCenter: true});
                } else {
                  let queryParams = { id: this.contractId, customerId: this.customerId, addressId: this.customerAddressId, serviceId: this.serviceId };
                  if(this.feature && this.featureIndex) {
                    queryParams['feature_name'] = this.feature;
                    queryParams['featureIndex'] = this.featureIndex;
                  }
                  this.router.navigate(['customer/manage-customers/ticket/save-offer/add-edit'], { queryParams: queryParams });
                }
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            })
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            let queryParams = { id: this.contractId, customerId: this.customerId, addressId: this.customerAddressId, serviceId: this.serviceId, saveOfferId : editableObject['saveOfferId'], viewable: true };
            if(this.feature && this.featureIndex) {
              queryParams['feature_name'] = this.feature;
              queryParams['featureIndex'] = this.featureIndex;
            }
            this.router.navigate(['customer/manage-customers/ticket/save-offer/view'], { queryParams: queryParams });
            break;
        }
    }
  }

  openConfirmDialog(dialogData: any) {
    this.saveOfferAlertDialog = this.dialogService.open(PrimengConfirmDialogPopupComponent, {
      // maxWidth: "400px",
      showHeader: false,
      width: "450px",
      data: dialogData,
      baseZIndex: 5000,
      // disableClose: true
    });
    // dialogRef.afterClosed().subscribe(dialogResult => {
    this.saveOfferAlertDialog.onClose.subscribe(dialogResult => {
      if(dialogResult) {
        let queryParams = { id: this.contractId, customerId: this.customerId, addressId: this.customerAddressId, serviceId: this.serviceId };
        if(this.feature && this.featureIndex) {
          queryParams['feature_name'] = this.feature;
          queryParams['featureIndex'] = this.featureIndex;
        }
        this.router.navigate(['customer/manage-customers/ticket/save-offer/add-edit'], { queryParams: queryParams });
      }
    });
  }

  getHeight() {
    return this.isViewMore ? '35vh' : '48vh';
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }
}
