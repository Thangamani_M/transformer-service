import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CancelPatrolComponent } from "./cancel-patrol.component";

@NgModule({
  declarations: [CancelPatrolComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [CancelPatrolComponent],
  entryComponents: [CancelPatrolComponent],
})
export class CancelPatrolModule { }