import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { ComponentProperties } from '@app/shared/utils';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-visibility-patrol-info',
  templateUrl: './visibility-patrol-info.component.html'
})
export class VisibilityPatrolInfoComponent implements OnInit {
  customerId = '';
  loggedUser: any;
  customerData: any;
  customerAddressId: any;
  componentProperties = new ComponentProperties();
  visibilityPatrolRequestForm: FormGroup;
  startTodayDate = new Date();
  minDateTODate: Date;
  minDateTODate1: Date;
  constructor(private crudService: CrudService,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    public dialogService: DialogService,
    private store: Store<AppState>,
    private rxjsService: RxjsService, private _fb: FormBuilder) {

    this.customerId = this.config.data.customerId

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.rxjsService.getCustomerDate().subscribe((data: boolean) => {
      this.customerData = data;
      this.customerId = this.customerData.customerId;
    });
    this.rxjsService.getCustomerAddresId().subscribe((data: boolean) => {
      this.customerAddressId = data;
    });
  }


  ngOnInit(): void {
    this.createVisibilityPatrolForm();
    this.getVisibilityPatrolData();
    this.rxjsService.setDialogOpenProperty(true);
  }
  onDateChange(event) {
   // this.minDateMoving = new Date(this.visibilityPatrolRequestForm.controls.fromDate.value);
   let _date = this.visibilityPatrolRequestForm.get('fromDate').value;
   this.minDateTODate = new Date(_date);
   this.minDateTODate1 =new Date(this.minDateTODate.getFullYear(), this.minDateTODate.getMonth(), this.minDateTODate.getDate());
  }
  createVisibilityPatrolForm() {
    let formModel = {
      "visibilityPatrolId": '',
      "customerId": this.customerId,
      "customerAddressId": this.customerAddressId,
      "fromDate": "",
      "toDate": "",
      "referenceId": null,
      "referenceTypeId": null,
      "createdUserId": this.loggedUser.userId
    }

    this.visibilityPatrolRequestForm = this._fb.group({
    });
    Object.keys(formModel).forEach((key) => {
      this.visibilityPatrolRequestForm.addControl(key, new FormControl(formModel[key]));
    });
    this.visibilityPatrolRequestForm = setRequiredValidator(this.visibilityPatrolRequestForm, ["fromDate", "toDate"]);
    this.visibilityPatrolRequestForm.get('fromDate').setValue("")
    this.visibilityPatrolRequestForm.get('toDate').setValue("")
  }

  onSubmitVisibilityPatrolForm() {
    if (this.visibilityPatrolRequestForm.invalid) {
      return;
    }

    // .toDateString();
    this.visibilityPatrolRequestForm.get("customerAddressId").setValue(this.customerAddressId);
    this.visibilityPatrolRequestForm.get("customerId").setValue(this.customerData.customerId);
    let obj = this.visibilityPatrolRequestForm.getRawValue();
    obj.fromDate = obj.fromDate.toDateString();
    obj.toDate = obj.toDate.toDateString();
    this.crudService.create(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.VISIBILITY_PATROL, obj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.dialogClose(true)
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
  }
  minDateMoving = new Date();
  minDateVisibilityPatrol = new Date();
  visibilityPatrolData: any;
  getVisibilityPatrolData() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.VISIBILITY_PATROL,
      undefined,
      false,
      prepareRequiredHttpParams({
        customerId: this.customerId,
        customerAddressId: this.customerAddressId
      })

    ).subscribe((response: IApplicationResponse) => {
      this.visibilityPatrolData = response.resources;
      if (response.isSuccess && response.resources && response.statusCode === 200) {
        if (response.resources.fromDate) {
          this.visibilityPatrolData.fromDate = new Date(response.resources.fromDate)
          this.visibilityPatrolData.toDate = new Date(response.resources.toDate)
          // this.visibilityPatrolRequestForm.disable()
        }
        this.visibilityPatrolRequestForm.patchValue(this.visibilityPatrolData);
      } else {
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  cancelVisibilityPatrol = false;

  cancelledReason = ""
  cancelVisibilityPatrolClick() {
    this.cancelledReason = '';
    this.cancelVisibilityPatrol = true;
  }
  dialogClose(event): void {
    this.ref.close(event);
  }

  btnCloseClick() {
    this.rxjsService.setDialogOpenProperty(false);
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
