import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CancelPatrolModule } from "./cancel-patrol/cancel-patrol.module";
import { PatrolHistoryListComponent } from "./patrol-history.component";
import { VisibilityPatrolInfoModule } from "./visibility-patrol-info/visibility-patrol-info.module";

@NgModule({
  declarations: [PatrolHistoryListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    CancelPatrolModule,
    VisibilityPatrolInfoModule,
  ],
  exports: [PatrolHistoryListComponent],
  entryComponents: [],
})
export class PatrolHistoryListModule { }