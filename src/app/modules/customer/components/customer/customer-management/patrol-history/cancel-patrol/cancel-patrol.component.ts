import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AppState } from '@app/reducers';
import { IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  selector: 'app-cancel-patrol-component',
  templateUrl: './cancel-patrol.component.html'
})
export class CancelPatrolComponent implements OnInit {
  loggedUser: any;
  cancelledReason: any
  patrolId: any
  constructor(private crudService: CrudService,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    public dialogService: DialogService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private _fb: FormBuilder) {


    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

  }


  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
  }

  onSubmit() {
    if (!this.cancelledReason) {
      this.snackbarService.openSnackbar("Reason for cancel is required", ResponseMessageTypes.ERROR);
      return;
    }
    let postObj = {
      patrolCategoryId: this.config.data.patrolCategoryId,
      cancelReason: this.cancelledReason,
      modifiedUserId: this.loggedUser.userId,
      customerId: this.config.data.customerId,
      addressId: this.config.data.addressId,

    }
    this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CANCEL_PATROL_COMMON, postObj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.dialogClose(true);
          this.ref.close(true)
        }
      })
  }
  dialogClose(event): void {
    this.ref.close(event);
  }


  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
