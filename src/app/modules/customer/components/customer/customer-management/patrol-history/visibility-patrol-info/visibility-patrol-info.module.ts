import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { VisibilityPatrolInfoComponent } from "./visibility-patrol-info.component";

@NgModule({
  declarations: [VisibilityPatrolInfoComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [VisibilityPatrolInfoComponent],
  entryComponents: [VisibilityPatrolInfoComponent],
})
export class VisibilityPatrolInfoModule { }