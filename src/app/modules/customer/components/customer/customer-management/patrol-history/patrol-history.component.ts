import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AppState } from '@app/reducers';
import { CrudType, CustomDirectiveConfig, formConfigs, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { VisibilityPatrolInfoComponent } from "../../customer-management/patrol-history/visibility-patrol-info/visibility-patrol-info.component";
import { CancelPatrolComponent } from './cancel-patrol/cancel-patrol.component';
enum PatrolType {
  HOLIDAY_PATROL = 'Holiday Patrol',
  VISIBILITY_REQUEST = 'Visibility Request',
  PAID_PATROL = 'Paid Patrol',
  POST_INCIDENT_PATROL = 'Post Incident Patrol'

}
@Component({
  selector: 'app-patrol-history-list',
  templateUrl: './patrol-history.component.html',
  styleUrls: ['./patrol-history.component.scss'],
})



export class PatrolHistoryListComponent extends PrimeNgTableVariablesModel implements OnInit {
  @ViewChildren(Table) tables: QueryList<Table>;
  @Input() partitionId;
  @Input() customerAddressId;
  @Input() customerId;
  @Input() permission;
  clientTestingForm: FormGroup;
  loggedInUserData: LoggedInUserModel;
  visibilityPatrolForm: FormGroup;
  visiBilityChecked = false;
  cancelledReason = ""
  selectedPatrol: any
  minDateMoving = new Date();
  minDateVisibilityPatrol = new Date();
  visibilityPatrolData: any;
  noOfPatrol = Array(100);

  constructor(private crudService: CrudService,
    public dialogService: DialogService,
    private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private momentService: MomentService,
    private rxjsService: RxjsService,
    private dialog: MatDialog,
    private datePipe: DatePipe) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: " ",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' }, { displayName: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: '',
            enableBreadCrumb: false,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'createdDate', header: 'Time Of Patrol', width: '150px' },
              { field: 'patrolTypeName', header: 'Patrol Type', width: '100px' },
              { field: 'vehicleRegistrationId', header: 'Vehicle', width: '100px' },
              { field: 'officer', header: 'Officer', width: '100px' },
              { field: 'slipNumber', header: 'Slip Number', width: '130px' },
              { field: 'occurrenceBookNumber', header: 'OBN Number', width: '130px' },
              { field: 'feedback', header: 'Feedback', width: '200px' },
            ],
            apiSuffixModel: EventMgntModuleApiSuffixModels.CUSTOMER_PATROL_HISTORY,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            shouldShowCreateActionBtn: true,

          },

        ]

      }
    }

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      let otherParams = {}
      if (this.customerId && this.customerAddressId) {
        this.getRequiredListData(null, null, otherParams);
        this.getActivePatrols()
      }
    }

  }
  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    if (this.partitionId == '') {
      const params = { customerId: this.customerId, CustomerAddressId: this.customerAddressId }
      otherParams = { ...otherParams, ...params };
    }
    else {
      const params = { customerId: this.customerId, CustomerAddressId: this.customerAddressId, partitionId: this.partitionId }
      otherParams = { ...otherParams, ...params };
    }
    let eventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    eventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      eventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {

        this.dataList = data.resources;
        data?.resources?.forEach(val => {
          val.createdDate = val.createdDate ? this.datePipe.transform(val.createdDate, 'dd-MM-yyyy h:mm a') : '';
          return val;
        })

        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null
        this.totalRecords = 0;

      }
    })
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        // this.addConfirm = true;
        if (!this.permission?.canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.CREATE:
        if (!this.permission?.canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.rxjsService.setDialogOpenProperty(true);
        this.openAddEditPage(CrudType.CREATE, row)
        break
    }
  }


  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    const ref = this.dialogService.open(VisibilityPatrolInfoComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: "460px",
      data: { customerId: this.customerId, header: `Visibility Request`, },
    });
    ref.onClose.subscribe((resp) => {
    })
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
    this.dialog.closeAll();
  }

  checkedVisibilty() {
    if (this.visiBilityChecked) {
      this.visibilityPatrolForm.get('fromDate').enable()
      this.visibilityPatrolForm.get('toDate').enable()
    } else {
      this.visibilityPatrolForm.get('fromDate').disable()
      this.visibilityPatrolForm.get('toDate').disable()
    }
  }



  cancelVisibilityPatrolClick() {
    this.cancelledReason = '';
    this.cancelVisibilityPatrol = true;
  }

  onSubmitVisibilityPatrolForm(patrol) {
    if (!patrol.fromDate || !patrol.toDate) {
      return this.snackbarService.openSnackbar(`${patrol.fromDateLabel} and ${patrol.toDateLabel} are required`, ResponseMessageTypes.ERROR);
    }
    patrol.editObj.startDate = patrol.fromDate.toDateString();
    patrol.editObj.endDate = patrol.toDate.toDateString();
    patrol.editObj.isPaidPatrol = patrol.isPaidPatrol == '1' ? true : false,
      patrol.editObj.numberOfPatrols = patrol.numberOfPatrols,

      this.crudService.update(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.PATROL_EDIT_COMMON, patrol.editObj)
        .subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);

          if (response.isSuccess) {
            patrol.isChecked = false;
            patrol.isDisabled = true;
            this.getActivePatrols();
          }
        })
  }

  cancelVisibilityPatrol = false;

  onDateChange(event) {
    this.minDateMoving = new Date(this.visibilityPatrolForm.controls.fromDate.value);
  }

  getActionsOfPatrol(type) {
    let actions = {
      isShowEdit: true,
      isShowCancel: true,
      isShowPatrolType: false,
      fromDateLabel: 'Initiated Date',
      toDateLabel: 'Suspended Date',
      isChecked: false,
      isDisabled: true
    }
    switch (type) {
      case PatrolType.HOLIDAY_PATROL:
        actions.fromDateLabel = 'Start Date'
        actions.toDateLabel = 'End Date'
        actions.isShowPatrolType = true
        break;
      default:
        break;
    }
    return actions
  }

  activePatrolList = []
  getActivePatrols() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.ACTIVE_PATROL_CUSTOMERS,
      undefined,
      false,
      prepareRequiredHttpParams({
        customerId: this.customerId,
        customerAddressId: this.customerAddressId,
        partitionId: this.partitionId
      })).subscribe(response => {
        this.rxjsService.setGlobalLoaderProperty(false);

        if (response.isSuccess && response.resources) {
          this.activePatrolList = [];
          for (let patrol of response.resources) {
            let editObj = {
              patrolCategoryId: patrol.patrolCategoryId,
              customerId: this.customerId,
              addressId: this.customerAddressId,
              isPaidPatrol: patrol.isPaidPatrol,
              numberOfPatrols: patrol.numberOfPatrols,
              startDate: patrol.startDate,
              endDate: patrol.endDate,
              modifiedUserId: this.loggedInUserData.userId
            }
            let _data = this.getActionsOfPatrol(patrol.patrolCategoryName);
            patrol.isPaidPatrol = patrol.isPaidPatrol ? '1' : '0'
            patrol.fromDate = new Date(patrol.startDate)
            patrol.editObj = editObj
            patrol.toDate = patrol.endDate ? new Date(patrol.endDate) : null

            this.activePatrolList.push({ ...patrol, ..._data })
          }
        }
      })
  }

  enableActions(patrol) {
    patrol.isDisabled = !patrol.isDisabled;
  }

  editPatrol(patrol) {
    if (!this.permission?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.onSubmitVisibilityPatrolForm(patrol);
  }

  cancelPatrol(patrol) {
    if (!this.permission?.canCancel) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.selectedPatrol = patrol.patrolCategoryName;
    const ref = this.dialogService.open(CancelPatrolComponent, {
      header: `Cancel ${patrol.patrolCategoryName}`,
      showHeader: true,
      baseZIndex: 10000,
      width: "460px",
      data: {
        patrolCategoryId: patrol.patrolCategoryId, customerId: this.customerId,
        addressId: this.customerAddressId
      },
    });
    ref.onClose.subscribe((resp) => {
      if (resp)
        this.getActivePatrols()
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search) {
      this.onCRUDRequested(e.type, e.data)
    } else if (e.data && e.search) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data) {
      this.onCRUDRequested(e.type, {})
    }
  }
}
