import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CustomerlistComponent } from "./customerlist.component";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
@NgModule({
  declarations: [CustomerlistComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
      {
        path: '', component: CustomerlistComponent, canActivate: [AuthGuard], data: { title: 'Customer List' }
      },
    ])
  ],
  entryComponents: [],
})
export class CustomerListModule { }
