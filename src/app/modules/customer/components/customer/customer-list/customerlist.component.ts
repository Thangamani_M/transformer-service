import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, SessionService } from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { loggedInUserData, selectStaticEagerLoadingCustomerTypesState$ } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { take } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-customerlist',
  templateUrl: './customerlist.component.html',
  // styleUrls: ['./customerlist.component.scss'],
  providers: [DialogService]
})
export class CustomerlistComponent extends PrimeNgTableVariablesModel {
  customerType: any;
  listSubscription: any;
  status = [];
  deletConfirm: boolean = false;
  statusConfirm: boolean = false;
  addConfirm: boolean = false;
  showDialogSpinner: boolean = false;
  loggedUser: UserLogin;
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties: any = {
    tableCaption: "Customers",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Customers Management' }, { displayName: 'Customers', relativeRouterUrl: '/customer/dashboard' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'InterBranch transfer request',
          dataKey: 'customerId',
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'customerRefNo', header: 'Customer ID', width: '160px' },
          { field: 'customerName', header: 'Customer Name', width: '200px' },
          { field: 'customerType', header: 'Customer Type', width: '140px', placeholder: 'Select type', type: 'dropdown', options: [] },
          { field: 'companyName', header: 'Company Name', width: '200px' },
          { field: 'mobileNumber1', header: 'Mobile Number 1', width: '130px', isPhoneMask: true },
          { field: 'contactNumber', header: 'Contact Number', width: '130px', isPhoneMask: true },
          { field: 'email', header: 'Email', width: '200px' },
          { field: 'createdDate', header: 'Created On', width: '160px', isDateTime: true },
          { field: 'status', header: 'Status', width: '120px', type: 'dropdown', options: [] },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: CustomerModuleApiSuffixModels.Customer,
          moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        }]
    }
  }

  constructor(
    private httpService: CrudService,
    private router: Router,
    private rxjsService: RxjsService,
    private dialogService: DialogService,
    private store: Store<AppState>,
    private sessionService: SessionService
  ) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.rxjsService.setRelocation(false);
    this.getCustomers();
    this.getStatuses();
    this.combineLatestNgrxStoreData();
    this.removeSelectedTab()
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getCustomerType() {
    this.httpService.get(ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_CUSTOMER_TYPES, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        this.customerType = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getStatuses() {
    this.httpService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.CUSTOMER_STATUS, undefined, false, null, 1).subscribe((response: IApplicationResponse) => {
        this.status = getPDropdownData(response.resources);
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[8].options = this.status;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(selectStaticEagerLoadingCustomerTypesState$),
    this.store.select(loggedInUserData),
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[2].options = getPDropdownData(response[0]);
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      });
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getCustomers(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.VIEW:
        this.openAddEditCustomerPage(row);
        break;
    }
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedUser.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((resp) => {
      if (!resp) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  openAddEditCustomerPage(editableObject): void {
    this.rxjsService.setViewCustomerData({
      customerId: editableObject['customerId'],
      addressId: editableObject['addressId'],
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  getCustomers(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
    }
    otherParams = { ...otherParams, ...{ userId: this.loggedUser.userId } };
    let customerModuleApiSuffixModels: CustomerModuleApiSuffixModels;
    let module = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.moduleName
    customerModuleApiSuffixModels = this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.apiSuffixModel;
    this.listSubscription = this.httpService.get(module, customerModuleApiSuffixModels, undefined, false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }

  removeSelectedTab() {
    this.sessionService.removeItem('selectedContractId')
    this.sessionService.removeItem('index')
    this.sessionService.removeItem('agreementTab')
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
  }
}
