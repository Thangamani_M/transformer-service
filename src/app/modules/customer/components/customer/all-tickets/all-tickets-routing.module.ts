import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllTicketsListComponent } from './all-tickets-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const allTickets: Routes = [
  {
    path: '', component: AllTicketsListComponent, canActivate: [AuthGuard], data: { title: 'All tickets' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(allTickets)],

})

export class AllTicketsRoutingModule { }
