import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { MomentService } from '@app/shared/services/moment.service';
import { TicketManualAddModel } from '@modules/customer/models';
import { CUSTOMER_COMPONENT } from '@modules/customer/shared';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user/shared';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';


@Component({
  selector: 'app-ticket-all-list',
  templateUrl: './all-tickets-list.component.html',
  styleUrls: ['./all-tickets-list.component.scss']
})
export class AllTicketsListComponent implements OnInit {

  observableResponse;
  selectedTabIndex = 0;
  dataList: any;
  totalRecords: any;
  selectedColumns: any[];
  status: any = [];
  selectedRows: string[] = [];
  selectedRow: any;
  row: any = {};
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loading = false;
  componentProperties = new ComponentProperties();
  serviceMappingId: any;
  loggedInUserData: LoggedInUserModel;
  ticketForm: any;
  ticketTypeList = [];
  ticketGroupList = [];
  assigntoList = [];
  selectedOptions: any = [];
  ticketStatusList: any = [];
  showFilterForm: boolean = false;
  ticketPriorityList: any;
  ticketdetails: any;
  filterData: any;
  groupId: string;
  book1: any;
  pageSize: number = 10;
  fromDate: any = new Date();
  todayDate: any = new Date();
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  startTodayDate = 0;
  filterParams;
  primengTableConfigProperties: any = {
    tableCaption: "Tickets",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Customers Management', relativeRouterUrl: '/customer/dashboard' }, { displayName: 'Ticket List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Tickets',
          dataKey: 'ticketId',
          enableBreadCrumb: true,
          enableAction: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: false,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          enableSecondHyperLink: true,
          cursorSecondLinkIndex: 4,
          columns: [
            { field: 'ticketRefNo', header: 'Ticket ID' },
            { field: 'ticketTypeName', header: 'Action Type' },
            { field: 'userGroupName', header: 'User Group' },
            { field: 'assignedToName', header: 'Assigned To' },
            { field: 'customerName', header: 'Customer' },
            { field: 'createdDate', header: 'Created Date',isDateTime:true ,width:'150px'},
            { field: 'lapsedTime', header: 'Lapsed Time' },
            { field: 'ticketStatusName', header: 'Status' }
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: false,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          shouldShowFilterActionBtn: true,
          apiSuffixModel: SalesModuleApiSuffixModels.ALL_TICKETS,
          moduleName: ModulesBasedApiSuffix.SALES,
        }]
    }
  }
  ticketFlagList: any;
  userGroupIds:any = [];
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private crudService: CrudService, public dialogService: DialogService, private store: Store<AppState>,
    private momentService: MomentService, private router: Router, private rxjsService: RxjsService, private formBuilder: FormBuilder, private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getList();
    this.createTicketManualAddForm();
    this.gettickettypes();
    this.getticketusergroup();
    this.getticketPriority();
    this.getticketstatus();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CUSTOMER_COMPONENT.TICKETS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  createTicketManualAddForm(TicketModelData?: TicketManualAddModel): void {
    let ticketModel = new TicketManualAddModel(TicketModelData);
    this.ticketForm = this.formBuilder.group({});
    Object.keys(ticketModel).forEach((key) => {
      this.ticketForm.addControl(key, new FormControl(TicketManualAddModel[key]));
    });
  }
  gettickettypes() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_TICKETTYPES, undefined, true, null, 1).subscribe((response: IApplicationResponse) => {
      this.ticketTypeList = response.resources;

    });
  }
  getticketstatus() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_TICKETSTATUS, undefined, true, null, 1).subscribe((response: IApplicationResponse) => {
      this.ticketStatusList = response.resources;

    });
  }
  getticketusergroup() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_GROUPS, undefined, true).subscribe((response: IApplicationResponse) => {
      this.ticketGroupList = response.resources;

    });
  }
  getticketPriority() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKET_PRIORITY, undefined, true).subscribe((response: IApplicationResponse) => {
      this.ticketPriorityList = response.resources;
      setTimeout(() => {
        if (this.data.ticketId) {
          this.getTicketById(this.data.ticketId)
        }
      }, 2000);
    });
  }
  getTicketById(ticketId: string) {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKETS, this.data.ticketId, false, null, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200) {
          this.ticketdetails = response.resources;
          this.ticketForm.patchValue(this.ticketdetails);

          this.getassignedto(this.ticketdetails.userGroupId);
        } else {
          this.ticketdetails = null;
        }
        this.rxjsService.setGlobalLoaderProperty(false);

      });
  }
  getassignedto(id: any) {
    this.crudService.create(ModulesBasedApiSuffix.SALES, UserModuleApiSuffixModels.UX_ASSINGN_TO, { groupIds: id.toString() }).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setDialogOpenProperty(false);
      if (response.statusCode == 200) {
        this.assigntoList = response.resources;
      } else {
      }
    });
  }

  getFlagColor() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TICKET_FLAG, undefined, true).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.ticketFlagList = response.resources;
      }
    });
  }
  OnUserGroupChange(event: any) {
    this.userGroupIds = [];
    event?.value?.forEach(res =>{
      this.userGroupIds.push(res?.id);
    })
    this.groupId = this.userGroupIds;
    if (this.groupId.length > 0) {
      this.getassignedto(this.groupId);
      this.rxjsService.setGlobalLoaderProperty(false);
    }

  }
  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }
  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    if (this.filterData) {
      this.getListFilter(row["pageIndex"], row["pageSize"], this.filterData)
    } else {
      this.onCRUDRequested(CrudType.GET, row);
    }
  }
  getList(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.ALL_TICKETS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse;
        this.dataList.forEach(ele => {
          ele.isUpdateFlag = false;
        });
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }

  getListFilter(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    this.loading = true;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.ALL_FILTERED_TICKETS,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.observableResponse = response.resources;
        this.dataList = this.observableResponse;
        this.dataList.forEach(ele => {
          ele.isUpdateFlag = false;
        });
        this.totalRecords = response.totalCount;
        this.loading = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }
  showFlag(data) {
    this.dataList.forEach(ele => {
      if (ele.ticketId == data.ticketId) {
        ele.isUpdateFlag = true;
      } else {
        ele.isUpdateFlag = false;
      }
    });
  }

  updateFlag(dt, rowData) {
    let formData = {
      "ticketId": rowData.ticketId,
      "flagColorId": dt.id,
      "modifiedUserId": this.loggedInUserData.userId
    }
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.FLAG_UPDATE, formData, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.getList();

        }
      });
  }

  onCRUDRequesteds(type: CrudType | string, row?: object, field?: any): void {

    if (field) {

      switch (field) {
        case "ticketRefNo":
          this.router.navigate(['/customer/manage-customers/ticket/view'], { queryParams: { id: row['ticketId'], customerId: row['customerId'], fromComponent: "Tickets" } });
          break;
        case "customerName":
          this.rxjsService.setViewCustomerData({
            customerId: row['customerId'],
            addressId: row['addressId'],
            customerTab: 0,
            monitoringTab: null,
          })
          this.rxjsService.navigateToViewCustomerPage();
          break;
        case "filter":
          this.displayAndLoadFilterData();
          break;
        case CrudType.GET:

          this.row = row ? row : { pageIndex: 0, pageSize: 10 };
          field = { ...this.filterData, ...field }
          if (field?.ticketTypeName)
            field.ticketType = field.ticketTypeName;
          if (field && field['createdDate']) {
            field['createdDate'] = (field['createdDate'] && field['createdDate'] != null) ? this.momentService.toMoment(field['createdDate']).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
          }

          this.getList(row["pageIndex"], row["pageSize"], field)
          break;
      }
    } else {

      let otherParams = {};
      if (CrudType.GET === type && Object.keys(row).length > 0) {
        // logic for split columns and its values to key value pair

        Object.keys(row['searchColumns']).forEach((key) => {
          otherParams[key] = row['searchColumns'][key]['value'];
        });
        otherParams['sortOrder'] = row['sortOrder'];
        if (row['sortOrderColumn']) {
          otherParams['sortOrderColumn'] = row['sortOrderColumn'];
        }
      }
      switch (type) {
        case CrudType.CREATE:
          this.openAddEditPage(CrudType.CREATE, row);
          break;
        case CrudType.GET:

          switch (this.selectedTabIndex) {
            case 0:
              if (otherParams && otherParams['ticketTypeName'])
                otherParams['ticketType'] = otherParams['ticketTypeName'];

              if (otherParams && otherParams['userGroupName'])
                otherParams['userGroup'] = otherParams['userGroupName'];

              if (otherParams && otherParams['assignedToName'])
                otherParams['assignedTo'] = otherParams['assignedToName'];
              if (otherParams && otherParams['createdDate']) {
                otherParams['createdDate'] = (otherParams['createdDate'] && otherParams['createdDate'] != null) ? this.momentService.toMoment(otherParams['createdDate']).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
              }
              this.getList(
                row["pageIndex"], row["pageSize"], otherParams);
              break;
          }
          break;
        case CrudType.EDIT:
          this.openAddEditPage(CrudType.EDIT, row);
          break;
        case "applyFilter":
          if (this.filterData?.ticketTypeName)
            this.filterData.ticketType = field.ticketTypeName;


          this.getListFilter(row["pageIndex"], row["pageSize"], this.filterData)
          break;
      }
    }

  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['customer/action-types/add-edit'], { skipLocationChange: true });
        break;
      case CrudType.EDIT:
        this.router.navigate(['/customer/manage-customers/ticket/view'], { queryParams: { id: editableObject['ticketId'], customerId: editableObject['customerId'] } });

        break;
    }
  }
  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.row = row;      
        unknownVar= {...unknownVar,...  this.filterParams};
        this.getList(row["pageIndex"], row["pageSize"], unknownVar);
        break;
        case CrudType.VIEW:
          this.router.navigate(['/customer/manage-customers/ticket/view'], { queryParams: { id: row['ticketId'], customerId: row['customerId'], fromComponent: "Tickets" } });
          break;
        case CrudType.EDIT:
            this.rxjsService.setViewCustomerData({
            customerId: row['customerId'],
            addressId: row['addressId'],
            customerTab: 0,
            monitoringTab: null,
            })
            this.rxjsService.navigateToViewCustomerPage();
          break;
        case CrudType.FILTER:
          this.displayAndLoadFilterData();
          break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  navigateToList() {
  }


  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }
  change(value) {
    let row = {};
    row['pageIndex'] = 0;
    row["pageSize"] = 10;
    this.onCRUDRequested(CrudType.GET, this.row)
  }
  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  onSubmit() {
    let assignedToMultipleArr = [];
    let ticketStatusNameMultipleArr = [];
    this.ticketForm.get('assignedToMultiple')?.value?.forEach(result=>{assignedToMultipleArr.push(result?.displayName)});
    this.ticketForm.get('ticketStatusNameMultiple')?.value?.forEach(result=>{ticketStatusNameMultipleArr.push(result?.displayName);
})
    this.filterData = Object.assign({},
      this.ticketForm.get('ticketTypeMultiple').value == '' ? null : { ticketTypeMultiple: this.ticketForm.get('ticketTypeMultiple').value },
      this.ticketForm.get('userGroupMultiple').value == '' ? null : { userGroupMultiple: this.ticketForm.get('userGroupMultiple').value },
      this.ticketForm.get('assignedToMultiple').value == '' ? null : { assignedToMultiple: assignedToMultipleArr },
      this.ticketForm.get('createdStartDate').value ? { createdStartDate: this.datePipe.transform(this.ticketForm.get('createdStartDate').value, 'yyyy-MM-dd') } : null,
      this.ticketForm.get('createdEndDate').value ? { createdEndDate: this.datePipe.transform(this.ticketForm.get('createdEndDate').value, 'yyyy-MM-dd') } : null,
      this.ticketForm.get('closedStartDate').value ? { closedEndDate: this.datePipe.transform(this.ticketForm.get('closedStartDate').value, 'yyyy-MM-dd') } : null,
      this.ticketForm.get('closedEndDate').value ? { closedEndDate: this.datePipe.transform(this.ticketForm.get('closedEndDate').value, 'yyyy-MM-dd') } : null,
      this.ticketForm.get('lapsedStartTime').value ? { lapsedStartTime: Number(this.ticketForm.get('lapsedStartTime').value) } : null,
      this.ticketForm.get('lapsedEndTime').value ? { lapsedEndTime: Number(this.ticketForm.get('lapsedEndTime').value) } : null,
      this.ticketForm.get('ticketStatusNameMultiple').value == '' ? null : { ticketStatusNameMultiple: ticketStatusNameMultipleArr },
    );

    if (this.filterData.ticketStatusNameMultiple) {
      this.filterData.ticketStatusNameMultiple = this.filterData.ticketStatusNameMultiple.toString();
    }
    let userGroupMultipleArr = [];
    if (this.filterData.userGroupMultiple) {
      this.filterData.userGroupMultiple.forEach(element => {
        let data = this.ticketGroupList.filter(x => x.id == element?.id);
        userGroupMultipleArr.push(data[0].displayName);
      });
      this.filterData.userGroupMultiple = userGroupMultipleArr;
    }
    Object.keys(this.filterData).forEach(key => {
      if (this.filterData[key] === '' || this.filterData[key] === null) {
        delete this.filterData[key]
      }
    });
    let row = {};
    row['pageIndex'] = 0;
    row["pageSize"] = 10;
    this.filterParams = this.filterData;
    this.observableResponse = this.onCRUDRequested('get', row, null);
    this.showFilterForm = !this.showFilterForm;
  }
  resetForm() {
    this.ticketForm.reset();
    this.row['pageIndex'] = 0;
    this.row['pageSize'] = 10;
    this.filterData = null;this.filterParams=null;    
    this.observableResponse = this.getList(this.row['pageIndex'], this.row['pageSize'], null);
    this.showFilterForm = !this.showFilterForm;
  }
}
