import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-ob-number-list',
  templateUrl: './ob-number-list.component.html'
})
export class ObNumberListComponent implements OnInit {

  @Input() loading: boolean;
  dataList: any;
  status: any = [];
  selectedRows: string[] = [];
  totalRecords: any;
  userSubscription: any;
  listSubscription: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  primengTableConfigProperties: any;
  @Input() selectedTabIndex: any;
  loggedInUserData: any;
  searchColumns: any;
  @Input() observableResponse: any;
  isShowNoRecords: any = true;
  @Input() customerId: any;
  @Input() customerAddressId: any;
  @Input() selectedRowData: any;
  row?: any = {};
  first?: number = 0;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    private crudService: CrudService, private store: Store<AppState>, private rxjsService: RxjsService,) {
    this.primengTableConfigProperties = {
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {},
          {
            caption: 'OB Number List',
            dataKey: 'occurrenceBookSignalId',
            enableAction: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'stockType', header: 'Stock Type', width: '50px' },
            { field: 'stockCode', header: 'Stock Code', width: '100px' },
            { field: 'stockDescription', header: 'Stock Description', width: '150px' },
            { field: 'invoice', header: 'Invoice', width: '50px' },
            { field: 'transfer', header: 'Transfer', width: '50px' },
            { field: 'used', header: 'Used', width: '50px' },
            { field: 'reserved', header: 'Reserved', width: '50px' },
            { field: 'request', header: 'Request', width: '50px' },
            { field: 'sellingPrice', header: 'Selling Price', width: '70px' },],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.CALL_INITIATION,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          },
          {},
          {
            caption: 'OB Number List',
            dataKey: 'occurrenceBookSignalId',
            enableAction: true,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'occurrenceBookNumber', header: 'OB Number', width: '200px' },
            { field: 'signalDateTime', header: 'Signal Time', width: '200px' },
            { field: 'decoder', header: 'Decoder', width: '200px' },
            { field: 'accountCode', header: 'Xmitter', width: '200px' },
            { field: 'alarmType', header: 'Alarm', width: '200px' },
            { field: 'zoneNo', header: 'Zone', width: '200px' },
            { field: 'companyName', header: 'Actual', width: '200px' },
            { field: 'eventCode', header: 'Raw Data', width: '200px' },],
            apiSuffixModel: EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_SIGNAL_CUSTOMER_ADDRESS,
            moduleName: ModulesBasedApiSuffix.EVENT_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          },
          {
            caption: 'Audit',
            dataKey: 'callInitiationDocumentId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'userName', header: 'User Name' },
            { field: 'field', header: 'Field' },
            { field: 'dateTime', header: 'Date & Time', isDateTime: true },
            { field: 'oldValue', header: 'Old Value' },
            { field: 'newValue', header: 'New Value' }
            ],
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_AUDIT_LOG,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  ngOnChanges() {
    this.first = 0;
    this.onTableChanges();
  }

  onTableChanges() {
    if (this.selectedTabIndex == 1) {
      this.dataList = this.observableResponse;
      this.totalRecords = 0;
      this.isShowNoRecords = this.dataList?.length ? false : true;
    } else if (this.selectedTabIndex == 3 || this.selectedTabIndex == 4) {
      this.dataList = this.observableResponse?.resources;
      this.totalRecords = this.observableResponse?.totalCount ? this.observableResponse?.totalCount : 0;
      this.isShowNoRecords = true;
    } else {
      this.dataList = this.observableResponse;
      this.totalRecords = 0;
      this.isShowNoRecords = true;
    }
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }

  combineLatestNgrxStoreData() {
    this.userSubscription = combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.isShowNoRecords = false;
    this.dataList = [];
    let EventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    EventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = { CustomerId: this.customerId, CustomerAddressId: this.customerAddressId, ...otherParams };
    let api = this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      EventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams));
    if (this.selectedTabIndex == 1) {
      api = this.crudService.get(
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        EventMgntModuleApiSuffixModels,
        this.selectedRowData?.callInitiationId,
        false, null);
    } if (this.selectedTabIndex == 4) {
      otherParams = { callInitiationId: this.selectedRowData?.callInitiationId, ...otherParams };
      api = this.crudService.get(
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        EventMgntModuleApiSuffixModels, null,
        false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams));
    }
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
    }
    this.listSubscription = api.pipe(map((res: IApplicationResponse) => {
			if(res?.resources && this.selectedTabIndex == 3) {
				res?.resources?.forEach(val => {
					val.signalDateTime = this.datePipe.transform(val.signalDateTime, 'medium');
					return val;
				});
			}
			return res;
		})).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && this.selectedTabIndex == 1) {
        this.observableResponse = data.resources['stockDetails'];
      } else if (this.selectedTabIndex == 3 || this.selectedTabIndex == 4) {
        this.observableResponse = data;
      }  else {
        this.observableResponse = null;
      }
      this.onTableChanges();
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["../add-edit"], { relativeTo: this.activatedRoute });
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['../view'], { relativeTo: this.activatedRoute, queryParams: { id: editableObject['wdRequestId'] } });
            break;
        }
    }
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }
}
