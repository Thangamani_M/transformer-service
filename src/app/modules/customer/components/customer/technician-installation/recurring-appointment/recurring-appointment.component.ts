import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { forkJoin, Observable } from 'rxjs';


enum FrequencyType {
  WEEKLY = 'Weekly',
  MONTHLY = 'Monthly',
  YEARLY = 'Yearly',
}

@Component({
  selector: 'app-recurring-appointment',
  templateUrl: './recurring-appointment.component.html'
})
export class RecurringAppointmentComponent implements OnInit {

  showRecurringScheduleDialog: boolean = false
  recurringScheduleFom: FormGroup
  frequencyList: any = []
  recurringPeriodList: any = []
  recurringWeeklyList: any = []
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  minDate = new Date();
  status = [];
  recurringMonthlyList: any = []
  recuringDetails: any

  constructor(private rxjsService: RxjsService, private _fb: FormBuilder, public config: DynamicDialogConfig,
    public ref: DynamicDialogRef, private httpCancelService: HttpCancelService, private crudService: CrudService, private dateTimeAdapter: DateTimeAdapter<any>,
    private momentService: MomentService) {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.config?.data?.isServiceCallEscalation) {
    }
    this.dateTimeAdapter.setLocale('en-GB');
    let months = this.momentService.getLongMonthsList()

    this.recurringMonthlyList = months
  }

  ngOnInit(): void {
    let dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_FREQUENCY),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.REQUISITION_CONFIGURATION_PERIODS),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.REQUISITION_CONFIGURATION_WEEKLY_PERIODS)
    ];
    this.loadRecurringScheduleDropdownData(dropdownsAndData);
    this.createRecurringScheduleForm()
    setTimeout(() => {
      this.getDetails()
    }, 1000);
  }


  createRecurringScheduleForm() {
    this.recurringScheduleFom = this._fb.group({
      recurringAppointmentId: [""],
      callInitiationId: [this.config?.data?.installationId ? this.config?.data?.installationId : null],
      // appointmentId: [this.appointmentId ? this.appointmentId : null],
      recurringAppointmentFrequencyId: ["10", Validators.required],
      recurringAppointmentFrequencyName: ["Weekly", Validators.required],
      startDate: ["", Validators.required],
      endDate: ['', Validators.required],
      isNoEndDate: [false, Validators.required],
      recurType: [null],
      recurDay: ["", Validators.required],
      recurMonthNumber: [''],
      details: [[], Validators.required],
      requisitionWeekPeriodId: [''],
      createdUserId: [this.config?.data?.createdUserId ? this.config?.data?.createdUserId : ''],
    });
    this.recurringScheduleFom.get('startDate').valueChanges.subscribe((val) => {
      if (val) {
        this.recurringScheduleFom.get('endDate').setValue(null)
      }
    })
    this.recurringScheduleFom.get('isNoEndDate').valueChanges.subscribe((event) => {
      if (event) {
        this.recurringScheduleFom = clearFormControlValidators(this.recurringScheduleFom, ["endDate"]);
        this.recurringScheduleFom.get('endDate').setValue(null)
      } else {
        this.recurringScheduleFom = setRequiredValidator(this.recurringScheduleFom, ['endDate']);
        this.recurringScheduleFom.get('endDate').setValue(this.recuringDetails?.endDate ? this.recuringDetails?.endDate : null)
      }
    })
    this.recurringScheduleFom.get('recurringAppointmentFrequencyId').valueChanges.subscribe((typeId) => {
      let frequencyData = this.frequencyList.find(x => x.id == typeId)
      if (!frequencyData) {
        return
      }
      this.recurringScheduleFom.get('recurringAppointmentFrequencyName').setValue(frequencyData.displayName)
      if (frequencyData.displayName == FrequencyType.WEEKLY) {
        this.recurringScheduleFom = setRequiredValidator(this.recurringScheduleFom, ['recurDay', 'details']);
        this.recurringScheduleFom = clearFormControlValidators(this.recurringScheduleFom, ["requisitionWeekPeriodId", "recurMonthNumber"]);
        this.recurringScheduleFom.get('recurType').setValue(null)
        this.recurringScheduleFom.get('recurDay').setValue(null)
        this.recurringScheduleFom.get('details').setValue(null)
      } else if (frequencyData.displayName == FrequencyType.MONTHLY) {
        this.recurringScheduleFom.get('recurType').setValue(1)
      } else if (frequencyData.displayName == FrequencyType.YEARLY) {
        this.recurringScheduleFom.get('recurType').setValue(1)
        this.recurringScheduleFom.get('recurDay').setValue(1)
      }
    });
    this.recurringScheduleFom.get('recurType').valueChanges.subscribe((recurType) => {
      if (recurType == '1' || recurType == 1) {
        this.recurringScheduleFom = setRequiredValidator(this.recurringScheduleFom, ['recurDay', 'recurMonthNumber']);
        this.recurringScheduleFom.get('recurMonthNumber').setValidators([Validators.required, , Validators.min(1), Validators.max(12)]);
        this.recurringScheduleFom.get('recurDay').setValidators([Validators.required, , Validators.min(1), Validators.max(30)]);
        this.recurringScheduleFom = clearFormControlValidators(this.recurringScheduleFom, ["requisitionWeekPeriodId", "details"]);
        // this.recurringScheduleFom.get('recurDay').setValue(null)
        // this.recurringScheduleFom.get('recurMonthNumber').setValue(null)
        if (this.recurringScheduleFom.get('recurringAppointmentFrequencyName').value == FrequencyType.YEARLY) {
          this.recurringScheduleFom.get('recurDay').setValue(1)
        }
        this.recurringScheduleFom.get('recurMonthNumber').setValue(this.recuringDetails?.recurMonthNumber ? this.recuringDetails?.recurMonthNumber : null)

      } else if (recurType == '2' || recurType == 2) {
        this.recurringScheduleFom = setRequiredValidator(this.recurringScheduleFom, ['requisitionWeekPeriodId', 'details', 'recurMonthNumber']);
        this.recurringScheduleFom.get('recurMonthNumber').setValidators([Validators.required, , Validators.min(1), Validators.max(12)]);
        this.recurringScheduleFom = clearFormControlValidators(this.recurringScheduleFom, ["recurDay"]);
        // this.recurringScheduleFom.get('requisitionWeekPeriodId').setValue(null)
        // this.recurringScheduleFom.get('details').setValue(null)
        // this.recurringScheduleFom.get('recurMonthNumber').setValue(null)
        if (this.recurringScheduleFom.get('recurringAppointmentFrequencyName').value == FrequencyType.YEARLY) {
          this.recurringScheduleFom.get('recurDay').setValue(1)
          this.recurringScheduleFom.get('recurMonthNumber').setValue({ id: this.recuringDetails?.recurMonthNumber ? this.recuringDetails?.recurMonthNumber : null })
        } else {
          this.recurringScheduleFom.get('recurMonthNumber').setValue(this.recuringDetails?.recurMonthNumber ? this.recuringDetails?.recurMonthNumber : null)
        }
      }
    })


  }

  loadRecurringScheduleDropdownData(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.frequencyList = resp.resources;
              break;
            case 1:
              // this.recurringPeriodList = resp.resources;
              resp.resources.forEach(element => {
                element.requisitionPeriodId = element.id
                // let data = { label: element.displayName, value: { requisitionPeriodId: element.id } }
                // this.recurringPeriodList.push(data)
              });
              this.recurringPeriodList = resp.resources;
              break;
            case 2:
              this.recurringWeeklyList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getDetails() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RECURRING_APPOINTMENT, null, false,
      prepareGetRequestHttpParams(null, null, { callInitiationId: this.config?.data?.installationId }))

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          if (response.resources.recurringAppointmentFrequencyId == 0) {
            return
          }
          response.resources.startDate = new Date(response.resources.startDate)
          response.resources.endDate = new Date(response.resources.endDate)
          this.recuringDetails = response.resources
          this.recurringScheduleFom.patchValue(response.resources)
          if (!response.resources.recurringAppointmentFrequencyName) {
            this.recurringScheduleFom.get('recurringAppointmentFrequencyName').setValue(FrequencyType.WEEKLY)
          }
          if (response.resources.recurringAppointmentFrequencyName == FrequencyType.MONTHLY || response.resources.recurringAppointmentFrequencyName == FrequencyType.YEARLY) {
            this.recurringScheduleFom.get('requisitionWeekPeriodId').setValue(response.resources.requisitionWeekPeriodId == 0 ? null : response.resources.requisitionWeekPeriodId)
            if (response.resources.requisitionWeekPeriodId != 0) {
              this.recurringScheduleFom.get('recurType').setValue('2')
              this.recurringScheduleFom.get('details').setValue(response.resources.details[0])
            } else {
              this.recurringScheduleFom.get('recurType').setValue('1')
            }
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  public checkValidity(): void {
    Object.keys(this.recurringScheduleFom.controls).forEach((key) => {
      this.recurringScheduleFom.controls[key].markAsDirty();
    });
  }

  onRecuringSchedule() {
    if (this.recurringScheduleFom.invalid) {
      this.checkValidity()
      return
    }
    let formValue = this.recurringScheduleFom.value
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (formValue.recurringAppointmentFrequencyName == FrequencyType.WEEKLY) {
      let formValue1 = {
        "recurringAppointmentId": formValue.recurringAppointmentId,
        "callInitiationId": formValue.callInitiationId,
        "recurringAppointmentFrequencyId": formValue.recurringAppointmentFrequencyId,
        "startDate": formValue.startDate.toDateString(),
        "endDate": formValue.endDate ? formValue.endDate.toDateString() : null,
        "isNoEndDate": formValue.isNoEndDate,
        "recurDay": formValue.recurDay,
        "details": formValue.details,
        "createdUserId": formValue.createdUserId,
      }
      let crudService: Observable<IApplicationResponse> = !formValue.recurringAppointmentId ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RECURRING_APPOINTMENT, formValue1) : this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RECURRING_APPOINTMENT, formValue1)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response?.statusCode == 200) {
          this.ref.close(true);
        }
      })
    } else if (formValue.recurringAppointmentFrequencyName == FrequencyType.MONTHLY) {
      let formValue1 = formValue.recurType == '1' ? {
        "recurringAppointmentId": formValue.recurringAppointmentId,
        "callInitiationId": formValue.callInitiationId,
        "recurringAppointmentFrequencyId": formValue.recurringAppointmentFrequencyId,
        "startDate": formValue.startDate.toDateString(),
        "endDate": formValue.endDate ? formValue.endDate.toDateString() : null,
        "isNoEndDate": formValue.isNoEndDate,
        "recurDay": formValue.recurDay,
        "recurMonthNumber": formValue.recurMonthNumber,
        "requisitionWeekPeriodId": 0,
        "createdUserId": formValue.createdUserId,
      } :
        {
          "recurringAppointmentId": formValue.recurringAppointmentId,
          "callInitiationId": formValue.callInitiationId,
          "recurringAppointmentFrequencyId": formValue.recurringAppointmentFrequencyId,
          "startDate": formValue.startDate.toDateString(),
          "endDate": formValue.endDate ? formValue.endDate.toDateString() : null,
          "isNoEndDate": formValue.isNoEndDate,
          "recurMonthNumber": formValue.recurMonthNumber,
          "details": [formValue.details],
          "requisitionWeekPeriodId": formValue.requisitionWeekPeriodId,
          "createdUserId": formValue.createdUserId,
        }
      let crudService: Observable<IApplicationResponse> = !formValue.recurringAppointmentId ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RECURRING_APPOINTMENT, formValue1) : this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RECURRING_APPOINTMENT, formValue1)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response?.statusCode == 200) {
          this.ref.close(true);
        }
      })
    } else if (formValue.recurringAppointmentFrequencyName == FrequencyType.YEARLY) {
      let formValue1 = formValue.recurType == '1' ? {
        "recurringAppointmentId": formValue.recurringAppointmentId,
        "callInitiationId": formValue.callInitiationId,
        "recurringAppointmentFrequencyId": formValue.recurringAppointmentFrequencyId,
        "startDate": formValue.startDate.toDateString(),
        "endDate": formValue.endDate ? formValue.endDate.toDateString() : null,
        "isNoEndDate": formValue.isNoEndDate,
        "recurDay": formValue.recurDay,
        "recurMonthNumber": formValue.recurMonthNumber,
        "requisitionWeekPeriodId": 0,
        "createdUserId": formValue.createdUserId,
      } :
        {
          "recurringAppointmentId": formValue.recurringAppointmentId,
          "callInitiationId": formValue.callInitiationId,
          "recurringAppointmentFrequencyId": formValue.recurringAppointmentFrequencyId,
          "startDate": formValue.startDate.toDateString(),
          "endDate": formValue.endDate ? formValue.endDate.toDateString() : null,
          "isNoEndDate": formValue.isNoEndDate,
          "recurMonthNumber": formValue.recurMonthNumber.id,
          "details": [formValue.details],
          "requisitionWeekPeriodId": formValue.requisitionWeekPeriodId,
          "createdUserId": formValue.createdUserId,
        }

      let crudService: Observable<IApplicationResponse> = !formValue.recurringAppointmentId ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RECURRING_APPOINTMENT, formValue1) : this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RECURRING_APPOINTMENT, formValue1)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response?.statusCode == 200) {
          this.ref.close(true);
        }
      })
    }


  }

  btnCloseClick() {
    this.ref.close(false);
  }

}
