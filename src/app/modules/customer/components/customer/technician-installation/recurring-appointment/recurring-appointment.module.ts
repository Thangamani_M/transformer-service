import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { RecurringAppointmentComponent } from './recurring-appointment.component';
@NgModule({
  declarations: [RecurringAppointmentComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    MatCheckboxModule
  ],
  exports: [RecurringAppointmentComponent],
  entryComponents: [RecurringAppointmentComponent],
})
export class RecurringAppointmentModule { }
