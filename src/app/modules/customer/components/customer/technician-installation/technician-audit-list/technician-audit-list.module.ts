import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicianAuditListComponent } from './technician-audit-list.component';

@NgModule({
  declarations: [ TechnicianAuditListComponent ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule
  ],
  exports: [TechnicianAuditListComponent],
  entryComponents: [],
})
export class TechnicianAuditListModule { }
