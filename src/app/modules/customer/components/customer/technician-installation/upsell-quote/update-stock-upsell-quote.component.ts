import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { formConfigs, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, setRequiredValidator } from '@app/shared/utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared';
import { select, Store } from '@ngrx/store';

@Component({
  // changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-update-stock-upsell-quote-component',
  styles: [`::ng-deep p-table.upsell-quote-stock-items .ui-table .ui-table-scrollable-wrapper .ui-table-scrollable-view .ui-table-scrollable-body {
    max-height: 30vh !important;
  }`],
  templateUrl: './update-stock-upsell-quote.component.html',
})
export class UpdateStockUpsellQuoteModalComponent implements OnInit {
  leadId: string;
  formConfigs = formConfigs;
  userData: any;
  @Output() outputData = new EventEmitter<any>();
  loading: boolean;
  isButtonDisabled: boolean = false;
  componentList: any = [];
  componentGroupList: any = [];
  itemBrandList: any = [];
  stockTypes: any = [];
  details: any;
  quantity = null;
  pageSize: number = 10;
  pageLimit: any = [10, 25, 50, 75, 100];
  primengTableConfigProperties: any = {
    tableCaption: "Stock Items",
    breadCrumbItems: [{ displayName: 'Sales', relativeRouterUrl: '' }, { displayName: 'Raw Lead List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Stock Items',
          dataKey: 'itemId',
          ebableAddActionBtn: false,
          enableBreadCrumb: true,
          enableReset: false,
          enableGlobalSearch: false,
          reorderableColumns: false,
          resizableColumns: false,
          enableScrollable: true,
          checkBox: true,
          enableRowDelete: false,
          enableFieldsSearch: true,
          enableHyperLink: true,
          cursorLinkIndex: 0,
          columns: [{ field: 'stockCode', header: 'Stock Code' },
          { field: 'stockDescription', header: 'Description' },
          { field: 'unitPrice', header: 'Unit Price' },
          { field: 'qty', header: 'Quantity' },
          { field: 'subTotal', header: 'Sub Total' },
          { field: 'vat', header: 'vat ' },
          { field: 'total', header: 'Total' },
          ],
          shouldShowDeleteActionBtn: false,
          shouldShowCreateActionBtn: true,
          areCheckboxesRequired: false,
          isDateWithTimeRequired: true,
          apiSuffixModel: SalesModuleApiSuffixModels.SALES_API_STOCK_ITEMS_DATA,
          moduleName: ModulesBasedApiSuffix.SALES,
        },
      ]

    }
  }
  dataList: any;
  totalRecords: any;
  systemTypes: any;
  componentGroup = null;
  component = null;
  brand = null;
  updateStockForm: FormGroup;
  displayName: any;
  salesAreasSelectedOption: any;
  selectedOption: any = [];
  selectedRows: any = [];
  tempSelectedRows: any = [];

  constructor(private formBuilder: FormBuilder, private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    @Inject(MAT_DIALOG_DATA) public data: any, private crudService: CrudService, private dialog: MatDialog,
    private store: Store<AppState>) {
    this.leadId = this.activatedRoute.snapshot.queryParams.leadId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.rxjsService.setDialogOpenProperty(true);
    this.updateStockForm = this.formBuilder.group({
      componentId: [''],
      componentGroupId: [''],
      itemBrandId: [''],
      stockCode: '',
      stockDescription: '',
      stockTypeId: ['']
    });
    this.updateStockForm = setRequiredValidator(this.updateStockForm, ["stockTypeId"]);
    this.getDropdownData();
    this.getStockTypes();
    setTimeout(() => {
      this.getModalData();
    }, 2000);

    this.crudService
      .dropdown(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.UX_SYSTEM_TYPES
      ).subscribe(resp => {
        this.systemTypes = resp.resources;
        this.details = this.systemTypes?.filter(e => e.id == this.data.data.systemTypeId)
        this.displayName = this.details[0]['displayName']
      })

  }
  getStockTypes(){
    this.crudService
    .dropdown(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WARRANTY_PERIOD_STOCK_TYPES
    ).subscribe(resp => {
    // this.stockTypes = resp.resources;
      let TmpArray = resp.resources;
      for (let i = 0; i < TmpArray.length; i++) {
        let temp = {};
        temp['display'] = TmpArray[i].displayName;
        temp['value'] = TmpArray[i].id.toString();
        this.stockTypes.push(temp);
      }

      if(this.stockTypes && this.stockTypes.length>0)
         this.updateStockForm.get('stockTypeId').setValue(this.stockTypes[0].value);


      // this.details = this.systemTypes.filter(e => e.id == this.data.data.systemTypeId)
      // this.displayName = this.details[0]['displayName']
    })
  }
  reset() {

    this.updateStockForm.get('componentGroupId').setValue('');
    this.updateStockForm.get('componentId').setValue('');
    this.updateStockForm.get('itemBrandId').setValue('');
    this.updateStockForm.get('stockTypeId').setValue('');
    this.getModalData();

  }
  userListOption: any = [];
  componentGroupChange(e) {

    let isstechid = this.updateStockForm.get('componentGroupId').value;

    if (isstechid.length != 0) {
      this.getModalData();
    } else if (isstechid != "") {
      this.getModalData();
    }
  }

  componentChange(e) {
    let isstechid = this.updateStockForm.get('componentId').value;

    if (isstechid.length != 0) {
      this.getModalData();
    } else if (isstechid != "") {
      this.getModalData();
    }
  }
  stockTypeChange(e) {

    let isstechid = this.updateStockForm.get('stockTypeId').value;

    if (isstechid.length != 0) {
      this.getModalData();
    } else if (isstechid != "") {
      this.getModalData();
    }
  }
  itemBrandChange(e) {
    let isstechid = this.updateStockForm.get('itemBrandId').value;

    if (isstechid.length != 0) {
      this.getModalData();
    } else if (isstechid != "") {
      this.getModalData();
    }
  }

  disbledAdd: boolean = false;

  getModalData(pageIndex?: string, pageSize?: string,
    otherParams?: object) {
    this.rxjsService.setDialogOpenProperty(true);

     if(this.updateStockForm.invalid) {
       return;
     }
    let obj = {
      systemTypeId: this.data.data.systemTypeId,
      customerId: this.data.data.customerId,
    }

    if (this.updateStockForm.value.stockCode) {
      obj['stockCode'] = this.updateStockForm.value.stockCode
    }

    if (this.updateStockForm.value.stockDescription) {
      obj['stockDescription'] = this.updateStockForm.value.stockDescription
    }
    if (this.updateStockForm.value.componentGroupId) {
      obj['componentGroupId'] = this.updateStockForm.value.componentGroupId
    };
    if (this.updateStockForm.value.componentId) {
      obj['componentId'] = this.updateStockForm.value.componentId
    };
    if (this.updateStockForm.value.stockCode) {
      obj['itemBrandId'] = this.updateStockForm.value.itemBrandId
    };

    if (this.updateStockForm.value.stockTypeId) {
      obj['stockTypeId'] = this.updateStockForm.value.stockTypeId
    };
    Object.keys(obj).forEach(key => {
      if (obj[key] == "" || obj[key] == null) {
        delete obj[key]
      }
    });
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = SalesModuleApiSuffixModels.DIRECT_SALES_ITEMS;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      salesModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, obj), 1
    ).subscribe((response: IApplicationResponse) => {
      this.selectedRows = [];
      this.totalRecords = response.totalCount;
      if (response.resources && response.isSuccess) {
        this.dataList = response.resources;
        this.dataList.forEach(element => {
          element.isChecked = false;
          element.value = 0;
          this.data.data.directSaleQuotationItems.forEach(item => {
            if (item.stockCode == element.stockCode) {
              element.isChecked = true;
              element.comments = item.comments;
              element.qty = item.qty;
              element.value = item.value;
              element.subTotal = item.subTotal;
              element.vat = item.vat;
              element.total = item.total;
            }
          });
        });

        this.dataList.forEach((element, index) => {
          this.tempSelectedRows.forEach(item => {
            if (item.stockCode == element.stockCode) {
              element.isChecked = item.isChecked;
              element.qty = item.qty;
              element.value = item.value;
              element.subTotal = item.subTotal;
              element.vat = item.vat;
              element.total = item.total;
              element.comments = item.comments;
            }
          }
          );

        });
        this.dataList.forEach(element => {
          element.tax = element.vat;
          if (element.isChecked) {
            this.disbledAdd = true;
            this.selectedRows.push(element)
          }
        });
        this.tempSelectedRows = this.retrieveUniqueObjects([...this.selectedRows, ...this.tempSelectedRows]);
        console.log('this.dataList',this.dataList)
      }
      else {
        this.disbledAdd = false;
        this.dataList = [];
      }
    })
  }

  onChecked(data) {
    if (data) {
      data.isChecked = !data.isChecked
      if (data.isChecked) {
        this.disbledAdd = true;
        this.tempSelectedRows.push(data);
      } else {
        this.selectedRows = this.selectedRows?.filter(item => item.itemId !== data.itemId)
        this.data.data.leadItems = this.data.data.leadItems?.filter(item => item.itemId !== data.itemId)
        this.tempSelectedRows = this.tempSelectedRows.filter(item => item.itemId !== data.itemId)
        if (this.tempSelectedRows.length == 0) {
          this.disbledAdd = false;
        }
      }
    } else {
      this.dataList.forEach(element => {
        element.tax = element.vat;

        // this.disbledAdd = true;
        // this.selectedRows.push(element)

      });
      if (this.selectedRows.length > 0) {
        this.disbledAdd = true
      } else {
        this.disbledAdd = false
      }
      this.tempSelectedRows = this.selectedRows;
    }

  }

  onQuantityChange(type, serviceSubItem: object): void {
    serviceSubItem["qty"] =
      type === "plus"
        ? serviceSubItem["qty"] + 1
        : serviceSubItem["qty"] === 1
          ? serviceSubItem["qty"]
          : serviceSubItem["qty"] - 1;
    serviceSubItem['qty'] = serviceSubItem['qty']
    serviceSubItem['unitPrice'] = serviceSubItem['unitPrice']
    serviceSubItem['tax'] = serviceSubItem['tax']
    serviceSubItem['vat'] = serviceSubItem['vat']
    serviceSubItem['subTotal'] = serviceSubItem['qty'] * serviceSubItem['unitPrice']
    serviceSubItem['vat'] = serviceSubItem['qty'] * serviceSubItem['tax']
    serviceSubItem['total'] = serviceSubItem['subTotal'] + (serviceSubItem['vat']


    )
  }

  retrieveUniqueObjects = (list: Array<object>): Array<object> => {
    return [...new Set(list.map(obj => obj['itemId']))].map(itemId => {
      return list.find(obj => obj['itemId'] === itemId)
    });
  }

  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row['searchColumns'] = event.filters;
    this.tempSelectedRows = this.retrieveUniqueObjects([...this.selectedRows, ...this.tempSelectedRows]);
    this.getModalData(row['pageIndex'], row["pageSize"]);
  }

  quantityChange(data, index) {
    this.dataList.find(v => v.itemId == data.itemId).subTotal = data.qty * data.unitPrice;
    this.dataList.find(v => v.itemId == data.itemId).total = this.dataList.find(v => v.itemId == data.itemId).subTotal + data.vat;

  }

  trim(arr, key) {
    var values = {};
    return arr?.filter(function (item) {
      var val = item[key];
      var exists = values[val];
      values[val] = true;
      return !exists;
    });
  }

  submit() {
    if (this.tempSelectedRows.length == 1) {
      if (this.tempSelectedRows[0]?.techDiscountingProcessName) {
        this.snackbarService.openSnackbar("Please select atleast one item without discount", ResponseMessageTypes.WARNING);
        return;
      }
    }

    this.outputData.emit(this.tempSelectedRows);
    this.dialog.closeAll();
  }

  getDropdownData(pageIndex?: string, pageSize?: string,
    otherParams?: object): void {
    let salesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    salesModuleApiSuffixModels = SalesModuleApiSuffixModels.SALES_API_STOCK_ITEMS_DROPDOWNS;
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      salesModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, {
        SystemTypeId: this.data.data.systemTypeId
      }), 1
    ).subscribe((response: IApplicationResponse) => {
      if (response.resources.componentList.length > 0) {
        this.componentList = response.resources.componentList;
        this.componentList.forEach(element => {
          element.id = element.id.toString();
        });
      }
      if (response.resources.componentGroupList.length > 0) {
        this.componentGroupList = response.resources.componentGroupList;
        this.componentGroupList.forEach(element => {
          element.id = element.id.toString();
        });
      }
      if (response.resources.itemBrandList.length > 0) {
        this.itemBrandList = response.resources.itemBrandList;
      }
    })
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
