import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerUpgradeModalComponent, CUSTOMER_COMPONENT, InventoryModuleApiSuffixModels, SalesModuleApiSuffixModels, selectDynamicEagerLoadingLeadCatgoriesState$, selectDynamicEagerLoadingSourcesState$, selectLeadCreationUserDataState$, selectLeadHeaderDataState$, selectStaticEagerLoadingCustomerTypesState$, selectStaticEagerLoadingSiteTypesState$, selectStaticEagerLoadingTitlesState$ } from '@app/modules';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, convertTreeToList, countryCodes, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, landLineNumberMaskPattern, LoggedInUserModel, mobileNumberMaskPattern, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareAutoCompleteListFormatForMultiSelection, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { LeadCreationUserDataModel, LeadHeaderData } from '@modules/sales/models';
import { LeadOutcomeStatusNames } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { UpdateStockUpsellQuoteModalComponent } from './update-stock-upsell-quote.component';


declare var $;

@Component({
  selector: 'app-upsell-quote-item-info',
  templateUrl: './upsell-quote-item-info.component.html',
  styleUrls: ['./upsell-quote-item-info.component.scss'],
})

export class UpsellQuoteItemInfoComponent {
  formConfigs = formConfigs;
  selectedTabIndex = 0;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  alphaNumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  leadForm: FormGroup;
  rawLeadId: string;
  leadId: string;
  customerId: string;
  addressList = [];
  today: any = new Date()
  searchColumns: any
  filteredSchemes = [];
  selectedAddressOption = {};
  selectedLocationOption = {};
  selectedCityOption = {};
  selectedSuburbOption = {};
  selectedCommercialProducts: any;
  loggedInUserData: LoggedInUserModel;
  isSchemeMandatory: boolean;
  todayDate: Date = new Date();
  siteType: string;
  pageTitle: string = "Create";
  btnName = "Next";
  fromPage: string;
  tmpCurrentURL: string;
  tmpListName: string;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  lssSchemes = [];
  customerTypes = [];
  leadCategories = [];
  titles = [];
  siteTypes = [];
  latLongList = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  countryCodes = countryCodes;
  customerTypeName: string;
  commercialCustomerType = 'Commercial';             // edit here
  residentialCustomerType = 'Residential';            // edit here
  siteTypeName: string;
  mobileNumberMaskPattern = mobileNumberMaskPattern;
  landLineNumberMaskPattern = landLineNumberMaskPattern;
  existingCustomers = [];
  selectedExistingCustomer;
  objectKeys = Object.keys;
  subTotalSummary = {
    unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0
  };
  mobileNumberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  valueChangesCount = 0;
  addressModalMessage = "";
  existingAddresses = [];
  installedRentedItems = [];
  existingCustomerAddress;
  selectedInstalledRentedItems = [];
  sourceTypes = [];
  isFormSubmitted = false;
  isEnableQuoteGenerateButton = false;
  isGenerateQuote: boolean=false;
  dataList = [];
  totalCount = 0;
  loading: boolean;
  pageLimit: any = [10, 25, 50, 75, 100];
  openAddContactDialog:boolean=false;
  addNewContactForm: FormGroup;
  existingLeadsObservableResponse: IApplicationResponse;
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  isLoading=false;
  directSaleQuotationVersionId;
  isDelete=false;
  primengTableConfigProperties = {
    tableCaption: "Template",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Customers', relativeRouterUrl: '' }, { displayName: 'Ticket List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: "Found Existing Customer Profile",
          dataKey: 'customerId',
          reorderableColumns: false,
          resizableColumns: false,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          columns: [{ field: 'isChecked', header: 'Select' },
          { field: 'customerRefNo', header: 'Customer ID' },
          { field: 'fullName', header: 'Customer Name' },
          { field: 'email', header: 'Email' },
          { field: 'mobile1', header: 'Mobile No' },
          { field: 'customerTypeName', header: 'Customer Type' },
          { field: 'fullAddress', header: 'Address' },
          { field: 'isOpenLeads', header: 'Open Lead' }]
        }]
    }
  }
  leadHeaderData: LeadHeaderData;
  fromUrl = 'Leads List';
  isLeadGroupUpgrade = false;
  reconnectionMessage = '';
  selectedExistingCustomerProfile = { isOpenLeads: false };
  isDataFetchedFromSelectedCustomerProfile = false;
  countryId = formConfigs.countryId;
  callType: string;
  @ViewChild('existing_address_modal_installed_items', { static: false }) existing_address_modal_installed_items: ElementRef<any>;
  @ViewChild('existing_lead_modal', { static: false }) existing_lead_modal: ElementRef<any>;
  @ViewChild('existing_address_footprint_modal', { static: false }) existing_address_footprint_modal: ElementRef<any>;
  @ViewChild('confirm_reconnection_modal', { static: false }) confirm_reconnection_modal: ElementRef<any>;
  @ViewChild('input', { static: false }) row;
  provinceList: any;
  cities: any;
  suburbs: any;
  id: any;
  details: any;
  systemTypes = [];
  systemTypesData: any = [];
  selectedSystemId: any;
  itemData: any;
  saleId: any;
  isUpsell = false;
  addressId: any = '';
  customerRequestDetails: any = [];
  contactNoCountryCode: any;
  requestedByContact;
  contactNumber: any = [];
  callInitiationId: any;
  contactNo: any
  isFromCustomer: boolean = false;
  itemImageUrl = "";
  isContinuousDelivery = false
  isVoid: boolean = false;
  labourDetails;
  isAddUpsell = false;
  actionPermissionObj: any;

  constructor( private formBuilder: FormBuilder,private crudService: CrudService, private httpCancelService: HttpCancelService, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, private dialog: MatDialog,
    private rxjsService: RxjsService, private router: Router, private store: Store<AppState>) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.id = params['params']['id'];
      this.addressId = params['params']['addressId'];
      this.callInitiationId = params['params']['callInitiationId'];
      this.leadId = params['params']['leadId'];
      this.saleId = params['params']['saleId'];
      this.fromPage = params['params']['pageFrom'];
      this.callType = params['params']['callType'];
      this.isVoid = (params['params']['isVoid'] && params['params']['isVoid'] == 'true') ? true : false

    });
    this.rxjsService.getUpsellingQuoteProperty().subscribe((isUpsell: boolean) => {
      this.isUpsell = isUpsell;
    });

    // this.rxjsService.getisFromupsellData().subscribe((isFromCustomer: boolean) => {
    //   this.isFromCustomer = isFromCustomer;
    // });
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "customer") {
          this.isFromCustomer = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
          this.isFromCustomer = false;
      }
    });
    this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "customer") {
        this.isFromCustomer = true;
      } else if (val?.url?.split('/')[1] == "technical-management") {
          this.isFromCustomer = false;
      }
    })
    this.combineLatestNgrxStoreData();
  }

  addUpsellQuote() {
    if(!this.getUpsellQuoteActionPermission(PermissionTypes.ADD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isAddUpsell = true;
  }

  openUpgrade() {
    if(!this.getUpsellQuoteActionPermission(CUSTOMER_COMPONENT.ACCEPTANCE)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isAddUpsell = false;
    let data = {
      label: '',
      customerId: ''
    };
    data['label'] = 'upsell';
    data['customerId'] = this.id;
    data['addressId'] = this.details?.addressId;
    data['callInitiationId'] = this.callInitiationId;
    $('.quick-action-sidebar').addClass('hide-quick-action-menu');
    const dialogReff = this.dialog.open(CustomerUpgradeModalComponent, { width: '850px', data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {

    });
  }
  isCancelUpgrade = false;
  referenceId = null;
  cancelUpgrade() {
    this.isCancelUpgrade = true;
    this.isAddUpsell = false;
  }

  requestedData = {}
  onChangeRequestedBy(value) {
  
    if(this.requestedByContact && this.requestedByContact.length > 0){
      let cn = this.requestedByContact.filter(x=>x.id== value);
      this.contactNumber=cn;
    }

    let found = this.customerRequestDetails.find(e => e.id == this.referenceId);
    if (found) {
      this.requestedData = found;
      let foundcontactNumber = this.contactNumber.find(e => e.id == this.referenceId);

      this.contactNoCountryCode = found.contactNoCountryCode;
      this.contactNo = this.contactNoCountryCode + ' ' + found.contactNo;
    
    }
  }
  onLoadContactNumber(value){
    if(this.requestedByContact && this.requestedByContact.length > 0){
      let cn = this.requestedByContact.filter(x=>x.id== value);
      this.contactNumber=cn;
    }
  }
  goBack() {
    if (this.isFromCustomer) {
      this.router.navigate(['customer/manage-customers/call-initiation'], { queryParams: { customerId: this.id, customerAddressId: this.addressId ? this.addressId : this.details?.addressId, initiationId: this.callInitiationId, callType: this.callType } });

    } else {
      this.router.navigate(['technical-management/call-status-report'],{queryParams: {tab:1}});
    }

  }

  openImg = false;
  getImageUrl(dt) {
    this.itemImageUrl = dt.itemImageUrl;
    this.openImg = true;
  }

  getSystemTypes() {
    this.crudService
      .dropdown(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.UX_SYSTEM_TYPES
      ).subscribe(res => {
        this.rxjsService.setGlobalLoaderProperty(true);

        this.systemTypes = res.resources;
        this.systemTypes.forEach(element => {
          element.isDisabled = false;
        });
        this.getCustomerDetailsById();

      })
  }

  getDetailsById(): void {

    if (!this.id) return;
    this.rxjsService.setGlobalLoaderProperty(true);

    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_HEADER_DETAILS, undefined, false, prepareRequiredHttpParams({
      customerId: this.id,

    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.details = response.resources;
        if (this.details?.directSaleQuotationVersionId) {
          this.rxjsService.setGlobalLoaderProperty(true);
          this.getItemData(this.details?.directSaleQuotationVersionId);
        }
       // this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getRequestedBy() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UPSEL_CUSTOMER_REQUESTED_BY, undefined, false, prepareRequiredHttpParams({
          customerId: this.id
        })
      ).subscribe(res => {
        this.rxjsService.setGlobalLoaderProperty(false);
       // this.requestedByContact = res.resources;
        let arr=[];
        res.resources.forEach((element,i) => {
          // element.customId = i+1;
          if(element.displayName.includes('- Mobile1') ||
            element.displayName.includes('- OfficeNo') ||
            element.displayName.includes('- Mobile2') ||
            element.displayName.includes('- Premises') ){
          }else {
            arr.push(element);
          }
          if(element.displayName.includes('- Mobile1')){
            let displayName = element.displayName.split('-');
            element.displayName = displayName[0];
            arr.push(element);
          }
        });
        this.requestedByContact = res.resources;

        this.customerRequestDetails = arr;
      //  this.onChangeRequestedBy(this.referenceId);
        this.contactNo = this.details?.contactNoCountryCode ? this.details?.contactNoCountryCode + " " + this.details?.contactNo : '';
        if(this.contactNo){
          let contacts = this.contactNo.split(' ');
          if(contacts && contacts.length >0)
            this.contactNo = contacts[1];

        }
      })
  }

  getCustomerDetailsById(): void {
    if (!this.id) return;
    this.rxjsService.setGlobalLoaderProperty(true);

    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UPSELL_QUOTE_HEADER_DETAILS, undefined, false, prepareRequiredHttpParams({
      customerId: this.id,
      addressId: this.addressId ? this.addressId : "",
      callInitiationId: this.callInitiationId
    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.details = response.resources;
        this.directSaleQuotationVersionId = response.resources?.directSaleQuotationVersionId;
        this.customerId = this.details?.customerId;
        this.referenceId = this.details?.referenceId;
        this.contactNo = this.details?.contactNoCountryCode ? this.details?.contactNoCountryCode + " " + this.details.contactNo : '';
   
        this.contactNoCountryCode = this.details?.contactNoCountryCode;
        if(this.contactNo){
          let contacts = this.contactNo.split(' ');
          if(contacts && contacts.length >0)
            this.contactNo = contacts[1];
        }

        // if(this.details.directSaleQuotationVersionId){
        //   this.getItemData(this.details.directSaleQuotationVersionId);
        // }
        // this.
        if (this.details && this.details?.directSaleQuotationVersionId) {
          this.getItemData(this.details?.directSaleQuotationVersionId);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      this.getRequestedBy();
    });
  }
  changecontactNo(value){
    let filter = this.contactNumber.filter(x=>x.contactNo == value);
    if(filter?.length >0) {
      this.contactNoCountryCode = filter[0]?.contactNoCountryCode;
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(selectLeadCreationUserDataState$),
    this.store.select(loggedInUserData),
    this.store.select(selectStaticEagerLoadingTitlesState$),
    this.store.select(selectStaticEagerLoadingSiteTypesState$),
    this.store.select(selectStaticEagerLoadingCustomerTypesState$),
    this.store.select(selectDynamicEagerLoadingLeadCatgoriesState$),
    this.store.select(selectLeadHeaderDataState$),
    this.store.select(selectDynamicEagerLoadingSourcesState$),
    this.store.select(currentComponentPageBasedPermissionsSelector$),
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.leadCreationUserDataModel = new LeadCreationUserDataModel(response[0]);
        this.loggedInUserData = new LoggedInUserModel(response[1]);
        this.titles = response[2];
        this.siteTypes = response[3];
        this.customerTypes = response[4];
        this.sourceTypes = response[7];
        this.rxjsService.setGlobalLoaderProperty(false);
        this.leadHeaderData = response[6];
        this.leadCategories = prepareAutoCompleteListFormatForMultiSelection(response[5]);
        let permission = response[8][CUSTOMER_COMPONENT.CUSTOMER]?.find(el => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD);
        if (permission) {
          let techPermission = convertTreeToList([permission?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.TECHNICAL)]);
          const objName = this.callType == '1' ? CUSTOMER_COMPONENT.INSTALLATION : this.callType == '2' ? CUSTOMER_COMPONENT.SERVICE :
            this.callType == '3' ? CUSTOMER_COMPONENT.SPECIAL_PROEJCT : '---';
          const callActionPermissionObj = techPermission?.find(el => el?.[objName])?.[objName];
          if(callActionPermissionObj) {
            const quickActionPermissionObj = callActionPermissionObj?.find(el => el?.menuName == PermissionTypes.QUICK_ACTION);
            if(quickActionPermissionObj) {
              this.actionPermissionObj = quickActionPermissionObj?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.UPSELL_QUOTE);
            }
          }
        }

      });
  }

  getUpsellQuoteActionPermission(actionTypeMenuName) {
    return this.actionPermissionObj?.subMenu?.find(el => el?.menuName == actionTypeMenuName);
  }

  getUpsellQuoteEditPermission() {
    return !this.getUpsellQuoteActionPermission(PermissionTypes.EDIT);
  }

  ngOnInit(): void {

    this.getSystemTypes();
    this.createAddNewContactForm();
    this.getLabourSalesDetails();
  }
  getLabourSalesDetails(){
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_ITEMS_LABOUR_PRICE, undefined, false, prepareRequiredHttpParams({
      customerId: this.id
    }), 1).subscribe((response: IApplicationResponse) => {
           this.labourDetails= response.resources;
    });
  }
  addOption() {
    let obj = {
      'directSaleQuotationItems': [],
      'systemTypeId': null,
      'systemTypes': this.systemTypes.filter(e => !e.isDisabled),
      'noOfPartitions': 0
    }
    this.isEnableQuoteGenerateButton = false;

    if (this.systemTypesData.length > 0) {
      if (this.systemTypesData[0].directSaleQuotationItems.length > 0) {
        this.systemTypesData.splice(0, 0, obj);
      }
    } else {
      this.systemTypesData.push(obj);
    }
  }

  onArrowClick(str) {
    if (str == 'forward') {
      if (this.isFromCustomer) {
        this.router.navigate(['/customer/upsell-quote/quote-info'], { queryParams: { id: this.id, callInitiationId: this.callInitiationId, addressId: this.addressId ? this.addressId : this.details?.addressId, callType: this.callType } });
      } else {
        this.router.navigate(['technical-management/call-status-report/upsell-quote/quote-info'], { queryParams: { id: this.id, callInitiationId: this.callInitiationId, addressId: this.addressId ? this.addressId : this.details?.addressId, callType: this.callType } });
      }
    }else {
      // this.router.navigate(['/inventory/counter-sales/add-edit'], { queryParams: {id: this.saleId } })
    }
  }


  openUpdateStock(dt, i) {
    if (!dt.systemTypeId) {
      this.snackbarService.openSnackbar("System Type is required", ResponseMessageTypes.WARNING);
      return;
    }
    let obj = dt.systemTypes.filter(x=>x.displayName=="Discounts");


    this.selectedSystemId = dt.systemTypeId;
    dt.customerId = this.id;
    const details = dt;
    const dialogReff = this.dialog.open(UpdateStockUpsellQuoteModalComponent, { width: '950px', data: { data: details, index: i, leadData: this.leadHeaderData }, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {

    });
    dialogReff.componentInstance.outputData.subscribe(ele => {

      this.systemTypes.forEach(e => {
        if (e.id == this.selectedSystemId) {
          e.isDisabled = true;
        }
      })
      this.isEnableQuoteGenerateButton = false;

      this.systemTypesData.find(e => parseInt(e.systemTypeId) == this.selectedSystemId).directSaleQuotationItems = ele;

      if(this.systemTypesData && this.systemTypesData.length >0){
      }


      this.systemTypesData.forEach((element,index) => {
        let labourComponentHour=0;let qty=0;
        element.directSaleQuotationItems.forEach(item => {
          if (item.techDiscountingProcessName && (element.systemTypeId == this.selectedSystemId)) {
            item.isFixed = "";
            item.subTotal = 0;
            item.vat = 0;
            item.total = 0;
          }
        //  item.labourComponentHour=1; // hard coded to test the flow this line has to be remove
          let qtyT = item.qty * item.labourComponentHour;
          qty = qty + item.qty;
          labourComponentHour = labourComponentHour + qtyT;
        });

         if(index == i){
          let temp = {
            comments: 'dummy',
            componentGroupName: null,
            directSaleQuotationItemId: null,
            isChecked: false,
            isFixed: false,
            isLabourPrice: 1,
            isVideofied: false,
            itemId: this.labourDetails?.itemId,
            itemImageUrl: null,
            itemPricingConfigId: null,
            labourComponentHour: labourComponentHour,
            qty:labourComponentHour,
            stockCode:  this.labourDetails.stockCode,
            stockDescription: this.labourDetails.stockDescription,
            subTotal: null,
            systemTypeId:element.systemTypeId,
            systemTypeName: null,
            tax:null,
            taxPercentage: this.labourDetails.taxPercentage,
            techDiscountingProcessName: null,
            total:null,
            unitPrice: this.labourDetails.labourPrice,
            unitTaxPrice: null,
            value:null,
            vat: null,
            flag:true
          }
          temp.subTotal =  (this.labourDetails.labourPrice * temp.labourComponentHour)
          temp.vat =  (temp.subTotal * temp.taxPercentage)/100;
          temp.unitTaxPrice =  (temp.unitPrice * temp.taxPercentage)/100;
          temp.total = temp.vat + temp.subTotal;
          //if the chosen option is Discount then dont need to add labour value
          if((obj && obj.length>0 && parseInt(obj[0].id) != parseInt(dt.systemTypeId) || !obj || obj.length==0)){
            element.directSaleQuotationItems.push(temp);
          }         
         }
      });

      this.calculateTotalAmountSummary();
    });
  }


  onQuantityChange(type, serviceSubItem: object,index,subIndex): void {
    let previousVal = this.systemTypesData[index].directSaleQuotationItems[subIndex].qty;

    this.isEnableQuoteGenerateButton = false;
    serviceSubItem["qty"] =
      type === "plus"
        ? serviceSubItem["qty"] + 1
        : serviceSubItem["qty"] === 1
          ? serviceSubItem["qty"]
          : serviceSubItem["qty"] - 1;
    serviceSubItem['subTotal'] = serviceSubItem['qty'] * serviceSubItem['unitPrice']
    serviceSubItem['vat'] = (serviceSubItem['qty'] * serviceSubItem['unitTaxPrice']);
    serviceSubItem['total'] = serviceSubItem['subTotal'] + serviceSubItem['vat'];
    serviceSubItem['total'] = serviceSubItem['total'];
    serviceSubItem['subTotal'] = serviceSubItem['subTotal']
    serviceSubItem['vat'] = serviceSubItem['vat']
    serviceSubItem['total'] = serviceSubItem['total']

    let stockCode = this.systemTypesData[index].directSaleQuotationItems[this.systemTypesData[index].directSaleQuotationItems.length -1].stockCode;
    if(stockCode == this.labourDetails.stockCode){

      let qty = this.systemTypesData[index].directSaleQuotationItems[subIndex].qty - previousVal;
      let labourComponentHour =  serviceSubItem['labourComponentHour'];

      this.systemTypesData[index].directSaleQuotationItems[this.systemTypesData[index].directSaleQuotationItems.length -1].qty  =  this.systemTypesData[index].directSaleQuotationItems[this.systemTypesData[index].directSaleQuotationItems.length -1].qty + (labourComponentHour * qty);
      let updatedQty =  this.systemTypesData[index].directSaleQuotationItems[this.systemTypesData[index].directSaleQuotationItems.length -1].qty ;
      this.systemTypesData[index].directSaleQuotationItems[this.systemTypesData[index].directSaleQuotationItems.length -1].subTotal  = updatedQty * this.labourDetails.labourPrice;
      let subTotal  =  this.systemTypesData[index].directSaleQuotationItems[this.systemTypesData[index].directSaleQuotationItems.length -1].subTotal;
      this.systemTypesData[index].directSaleQuotationItems[this.systemTypesData[index].directSaleQuotationItems.length -1].vat = ( subTotal * this.labourDetails.taxPercentage)/100;
      let vat = this.systemTypesData[index].directSaleQuotationItems[this.systemTypesData[index].directSaleQuotationItems.length -1].vat;
      this.systemTypesData[index].directSaleQuotationItems[this.systemTypesData[index].directSaleQuotationItems.length -1].total = vat + subTotal;
      let unitPrice = this.systemTypesData[index].directSaleQuotationItems[this.systemTypesData[index].directSaleQuotationItems.length -1].unitPrice;
      this.systemTypesData[index].directSaleQuotationItems[this.systemTypesData[index].directSaleQuotationItems.length -1].unitTaxPrice =  (unitPrice * this.labourDetails.taxPercentage)/100;
    }
    this.calculateTotalAmountSummary();
  }

  calculateTotalAmountSummary(): void {
    this.subTotalSummary = { unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0 };
    this.objectKeys(this.systemTypesData).forEach((systemTypeId, ix: number) => {
      this.systemTypesData[systemTypeId]["totalAmount"] = { unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0 };
      this.subTotalSummary = { unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0 };
      this.systemTypesData[systemTypeId]["directSaleQuotationItems"].forEach((obj) => {

              
        if (obj.techDiscountingProcessName) {
          if (obj.subTotal > 0) {
            if (obj.isFixed == "true" || obj.isFixed == true) {
              this.subTotalSummary.subTotal -= this.subTotalSummary.subTotal * (obj.subTotal / 100);
            } else {
              this.subTotalSummary.subTotal -= obj.subTotal;

            }
          }
        } else {
          this.subTotalSummary.subTotal += obj.subTotal;
        }

        if(obj.techDiscountingProcessName){
          this.subTotalSummary.vat =  this.subTotalSummary.vat - obj.vat;
          this.subTotalSummary.unitPrice = this.subTotalSummary.unitPrice  - obj.unitPrice;
          this.subTotalSummary.totalPrice = this.subTotalSummary.totalPrice - obj.total;
        }else {
          this.subTotalSummary.vat += obj.vat;
          this.subTotalSummary.unitPrice += obj.unitPrice;
          this.subTotalSummary.totalPrice += obj.total;
        }
        // this.subTotalSummary.vat += obj.vat;
        // this.subTotalSummary.unitPrice += obj.unitPrice;
        // this.subTotalSummary.totalPrice += obj.total;
        this.subTotalSummary.totalPrice = this.subTotalSummary.totalPrice;

        this.systemTypesData[systemTypeId]["totalAmount"] = this.subTotalSummary;
      });
    });

  }
  calculateTotalAmount(isFixed): void {
    this.subTotalSummary = { unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0 };
    this.objectKeys(this.systemTypesData).forEach((systemTypeId, ix: number) => {
     // this.systemTypesData[systemTypeId]["totalAmount"] = { unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0 };
       this.subTotalSummary = { unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0 };
       let subTotal=0;
      this.systemTypesData[systemTypeId]["directSaleQuotationItems"].forEach((obj) => {
            if(obj.systemTypeName != "Miscellaneous"){
              this.subTotalSummary.unitPrice = this.subTotalSummary.unitPrice + obj.unitPrice;
              this.subTotalSummary.subTotal = this.subTotalSummary.subTotal + obj.subTotal;
              this.subTotalSummary.vat = this.subTotalSummary.vat + obj.vat;
              this.subTotalSummary.totalPrice = this.subTotalSummary.totalPrice + obj.total;
            }
            else
                subTotal =  obj.subTotal;
             
      });

      if(isFixed == true || isFixed =="true"){        
          let subTotals  = ( this.subTotalSummary.subTotal * subTotal) /100;
          let vat  = ( this.subTotalSummary.vat * subTotal)/100;
          let totalPrice  = ( this.subTotalSummary.totalPrice * subTotal) /100;
          let unitPrice  = ( this.subTotalSummary.unitPrice * subTotal) /100;

        this.subTotalSummary.unitPrice = this.subTotalSummary.unitPrice - unitPrice;
        this.subTotalSummary.subTotal = this.subTotalSummary.subTotal - subTotals;
        this.subTotalSummary.vat = this.subTotalSummary.vat - vat;
        this.subTotalSummary.totalPrice = this.subTotalSummary.totalPrice - totalPrice;
      }else {
        this.subTotalSummary.unitPrice = this.subTotalSummary.unitPrice - subTotal;
        this.subTotalSummary.subTotal = this.subTotalSummary.subTotal - subTotal;
        this.subTotalSummary.vat = this.subTotalSummary.vat - subTotal;
        this.subTotalSummary.totalPrice = this.subTotalSummary.totalPrice - subTotal;

      }
  
     this.systemTypesData[systemTypeId]["totalAmount"] = this.subTotalSummary;
    });

  }
  redirectCustomerView() {
    this.rxjsService.setViewCustomerData({
      customerId: this.id,
      addressId: this.addressId ? this.addressId : this.details?.addressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  getCommentError(f, comments) {
    return (comments?.touched || f?.submitted) && !comments?.valid;
  }

  checkIsVideofied = false;
  generateQuote() {
    if(this.getUpsellQuoteEditPermission()) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if(!this.getValidCommments()) {
      this.snackbarService.openSnackbar("Please fill all the comments", ResponseMessageTypes.WARNING);
      return;
    }
    this.systemTypesData.forEach(element => {

      if (element.directSaleQuotationItems.length > 0) {
        element.directSaleQuotationItems.forEach(item => {
          // item.isVideofied = true;
          if (item.isVideofied) {
            this.checkIsVideofied = true;
          }
        });
      }
    });
    const dialogData = new ConfirmDialogModel("Confirm Action", `Are you sure you want to Generate quote?`);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.isConfirmQuotation = true;
        //if ,!this.isVideofiedSimExists && this.checkIsVideofied
        if (this.checkIsVideofied) {
          const dialogData1 = new ConfirmDialogModel("Alert", `Would you like to order a new Videofied Sim Card?`);
          const dialogRefs = this.dialog.open(ConfirmDialogPopupComponent, {
            maxWidth: "400px",
            data: dialogData1,
            disableClose: true
          });
          this.httpCancelService.cancelPendingRequestsOnFormSubmission();
          dialogRefs.afterClosed().subscribe(dialogResult => {
            if (dialogResult) {
              this.isVideofiedSimRequired = true;
              this.submit();
              //  this.submit();
            } else {
              this.submit();
            }
          });
        } else {
          this.submit();
        }

      }
    });
  }
  createAddNewContactForm() {
    this.addNewContactForm = this.formBuilder.group({
      contactName: ['', Validators.required],
      contactNoCountryCode: ['+27', Validators.required],
      contactNo: ['', Validators.required],
      isPrimary: [null, Validators.required],
      keyholderId: null,
      callInitiationContactId: null,
      customerId: this.customerId ? this.customerId : null,
      createdUserId: this.loggedInUserData.userId

    })
  }
  openAddContact(val) {
    this.addNewContactForm.reset();
    this.addNewContactForm.get('contactNoCountryCode').setValue('+27');
    this.openAddContactDialog = true
  }
  addContactSubmit(){
      if(this.addNewContactForm.get('contactName').value==null ||this.addNewContactForm.get('contactName').value==''
        ||this.addNewContactForm.get('contactNo').value==''  ||this.addNewContactForm.get('contactNo').value==null)
      {
          return;
      }
     let  finalObj= {
      customerId: this.customerId,
      callInitiationId: this.callInitiationId,
      referenceId: this.referenceId,
      contactName: this.addNewContactForm.get('contactName').value,
      contactNoCountryCode: this.addNewContactForm.get('contactNoCountryCode').value,
      contactNo: this.addNewContactForm.get('contactNo').value?.replace(/\s/g, ""),
      isActive: true,
      createdUserId: this.loggedInUserData.userId
    }
    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.TECHNICAL_UPSELLING_REQUESTEDBY_CONTACT, finalObj)
    .subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.openAddContactDialog = false;
        this.getRequestedBy();
      }
      this.rxjsService.setPopupLoaderProperty(false);
    })
  }

  isLeadItems = false;
  isOnlyOneItem = false;
  isVideofiedSimRequired = false;
  isVideofiedSimExists = false;

  isConfirmQuotation = false;

  getValidCommments() {
    let arr = [];
    this.systemTypesData?.forEach(el => {
      el?.directSaleQuotationItems?.forEach(el1 => {
        if(!el1?.comments) {
          arr?.push(el1?.comments);
        }
      });
    })
    return arr?.every(el => el);
  }
  showCommentError(control, f: any) {
    return (control?.touched || f?.submitted) && control?.hasError('required');
  }
  submit() {
    if(this.getUpsellQuoteEditPermission()) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if(!this.getValidCommments()) {
      // this.snackbarService.openSnackbar("Please fill all the comments", ResponseMessageTypes.WARNING);
      return;
    }
    let dt = new Date();
    if (!this.referenceId) {
      this.snackbarService.openSnackbar("Select Requested by User", ResponseMessageTypes.ERROR);
      return;
    }
    dt.setDate(dt.getDate() + 21);
    let postObj = {
      "directSaleId": this.itemData ? this.itemData.directSaleId : null,
      "addressId": this.details?.addressId,
      "customerId": this.details?.customerId,
      "callInitiationId": this.callInitiationId,
      "referenceId": this.referenceId,
      "contactNo": this.contactNo,
      "contactNoCountryCode": this.contactNoCountryCode,
      "directSaleQuotationId": this.itemData ? this.itemData.directSaleQuotationId : null,
      "directSaleQuotationVersionId": this.itemData ? this.itemData.directSaleQuotationVersionId : null,
      "expiryDate": dt,
      "createdUserId": this.loggedInUserData.userId,
      "isQuoteGenerated": this.isGenerateQuote ? true : false,
      "isConfirmQuotation": this.isConfirmQuotation,
      "isCounterSale": false,
      "isVideofiedSimRequired": this.isVideofiedSimRequired,
      "isVideofiedSimExists": this.isVideofiedSimExists,
      "directSaleQuotationSystemTypes": this.systemTypesData,
      "isContinuousDelivery": this.isContinuousDelivery,
    }

    this.rxjsService.setFormChangeDetectionProperty(true);

    if (this.systemTypesData.length > 0) {
      this.systemTypesData.forEach(element => {
        if (element.directSaleQuotationItems.length == 0) {
          this.isLeadItems = true;
        }

        if (element.directSaleQuotationItems.length == 1) {
          element.directSaleQuotationItems.forEach(item => {
            if (item.techDiscountingProcessName) {
              this.isOnlyOneItem = true;
            } else {
              this.isOnlyOneItem = false;
            }
          });


        }
      });
    } else {
      this.isLeadItems = true;
    }

    if (this.isOnlyOneItem) {
      this.snackbarService.openSnackbar("Add one more item in option", ResponseMessageTypes.WARNING);
      return;
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();



    if (!this.isLeadItems) {
      let crudService: Observable<IApplicationResponse> = this.crudService.create(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.DIRECT_SALES_ITEMS,
        postObj, 1
      );
      crudService
        .pipe(
          tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          })
        )
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.isConfirmQuotation = false;
            this.getItemData(response.resources);
          }
        });
    } else {
      this.snackbarService.openSnackbar("Option can not be empty", ResponseMessageTypes.WARNING);

    }
  }

  onDelete(index, obj) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    this.isDelete=true;
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe((dialogResult) => {
      if (dialogResult) {

        if (obj.systemTypeName) {
          this.crudService
            .delete(
              ModulesBasedApiSuffix.SALES,
              SalesModuleApiSuffixModels.DIRECT_SALES_DELETE_SYSTEM_TYPE,
              undefined,
              prepareRequiredHttpParams({
                directSaleQuotationVersionId: this.itemData?.directSaleQuotationVersionId,
                systemTypeId: obj.systemTypeId,
                createdUserId: this.loggedInUserData.userId,
                isCounterSale: false
              }), 1
            )
            .subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                this.isEnableQuoteGenerateButton = false;
                this.isDelete=false;
                this.rxjsService.setGlobalLoaderProperty(false);
                this.systemTypesData.splice(index, 1);

                if (obj.systemTypeId) {
                  let fitr = this.systemTypes.find(e => e.id == obj.systemTypeId);
                  fitr.isDisabled = false;
                }
              }
              this.isDelete=false;
              this.rxjsService.setGlobalLoaderProperty(false);
            });
        } else {
          this.systemTypesData.splice(index, 1);
          this.isDelete=false;
          if (obj.systemTypeId) {
            let fitr = this.systemTypes.find(e => e.id == obj.systemTypeId);
            fitr.isDisabled = false;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      }
    });
  }

  onDeleteItem(item, stock) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe((dialogResult) => {
      if (dialogResult) {

        if (stock.directSaleQuotationItemId) {
          this.crudService
            .delete(
              ModulesBasedApiSuffix.SALES,
              SalesModuleApiSuffixModels.DIRECT_SALES_ITEMS,
              undefined,
              prepareRequiredHttpParams({
                DirectSaleQuotationVersionId: this.itemData?.directSaleQuotationVersionId,
                directSaleQuotationItemId: stock["directSaleQuotationItemId"],
                createdUserId: this.loggedInUserData.userId,
                isCounterSale: false
              }), 1
            )
            .subscribe((response: IApplicationResponse) => {
              this.isEnableQuoteGenerateButton = false;
              if (response.isSuccess && response.statusCode === 200) {
                this.rxjsService.setGlobalLoaderProperty(false);
                this.systemTypesData.forEach(element => {
                  if (element.systemTypeId == item.systemTypeId) {
                    element.directSaleQuotationItems.forEach((stockItem, index) => {
                      if (stockItem.stockCode == stock.stockCode) {
                        element.directSaleQuotationItems.splice(index, 1)
                      }
                    });
                  }
                });
                this.calculateTotalAmountSummary();

              }
              this.rxjsService.setGlobalLoaderProperty(false);
            });
        } else {
          this.isEnableQuoteGenerateButton = false;

          this.systemTypesData.forEach(element => {
            if (element.systemTypeId == item.systemTypeId) {
              element.directSaleQuotationItems.forEach((stockItem, index) => {
                if (stockItem.stockCode == stock.stockCode) {
                  element.directSaleQuotationItems.splice(index, 1)
                }
              });
            }
          });
          this.calculateTotalAmountSummary();
          this.rxjsService.setGlobalLoaderProperty(false);
        }

      }
    });

  }

  changeDropdown(product: object,isFixed) {
    product['subTotal'] = 0;
    product['vat'] = 0
    product['total'] = 0
    product['total'] = 0
    this.calculateTotalAmount(isFixed);
  }

  onChange(product: object) {
    this.isEnableQuoteGenerateButton = false;

    if (product['isFixed']) {
      if (product['subTotal'] > 100) {
        this.snackbarService.openSnackbar("Sub Total Can not be greater than 100", ResponseMessageTypes.WARNING);
        return;
      }
    }
    // product['subTotal'] = product['subTotal'] ? parseFloat(product['subTotal']) : 0;
    // product['vat'] = product['vat'] - (product['subTotal'] * product['taxPercentage']) / 100;
    // product['total'] =  product['total'] - (product['subTotal'] + product['vat']);
    // product['total'] = product['total'];
    //  this.calculateTotalAmountSummary();
     this.calculateTotalAmount(product['isFixed']);
  }

  getItemData(id) {
    let crudService: Observable<IApplicationResponse> = this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.DIRECT_SALES_ITEMS_GET_DETAILS,
      undefined, false, prepareRequiredHttpParams({
        customerId: this.id,
        directSaleQuotationVersionId: id
      })
    );
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.itemData = response.resources;
      if (response.isSuccess && response.resources && response.statusCode === 200) {
        this.isGenerateQuote = response.resources?.isQuoteGenerated;
        this.referenceId =  response.resources?.referenceId;
        this.onLoadContactNumber(this.referenceId);
      //  this.onChangeRequestedBy(this.referenceId);
        this.contactNo = response.resources?.contactNoCountryCode ? response.resources?.contactNoCountryCode + " " + response.resources?.contactNo : '';

        if(this.contactNo){
          let contacts = this.contactNo.split(' ');

          if(contacts && contacts.length >0)
            this.contactNo = contacts[1];
        }
        this.isEnableQuoteGenerateButton = this.itemData.isQuoteGenerated;
        this.isVideofiedSimRequired = this.itemData.isVideofiedSimRequired;
        this.isVideofiedSimExists = this.itemData.isVideofiedSimExists;
        this.isContinuousDelivery = this.itemData.isContinuousDelivery;
        this.isLoading=true;
        if (this.itemData && this.itemData.directSaleQuotationSystemTypes) {
          this.systemTypesData = this.itemData.directSaleQuotationSystemTypes;   
        
          // this.systemTypesData.push(this.itemData.directSaleQuotationSystemTypes)
          let totalValue = 0;
          this.systemTypesData.forEach((element, index) => {
            element.index = index + 1;
            totalValue = 0;

            if(element.directSaleQuotationLabourPriceItem){
              let  directSaleQuotationLabourPriceItem =  element.directSaleQuotationLabourPriceItem;
              let tem = {
                comments: directSaleQuotationLabourPriceItem.comments,
                componentGroupName: directSaleQuotationLabourPriceItem.componentGroupName,
                directSaleQuotationItemId: directSaleQuotationLabourPriceItem.directSaleQuotationItemId,
                flag:  directSaleQuotationLabourPriceItem.flag,
                isChecked: directSaleQuotationLabourPriceItem.isChecked,
                isFixed:  directSaleQuotationLabourPriceItem.isFixed,
                isLabourPrice: directSaleQuotationLabourPriceItem.isLabourPrice,
                isVideofied: directSaleQuotationLabourPriceItem.isVideofied,
                itemId: directSaleQuotationLabourPriceItem.itemId,
                itemImageUrl: directSaleQuotationLabourPriceItem.itemImageUrl,
                itemPricingConfigId: directSaleQuotationLabourPriceItem.itemPricingConfigId,
                labourComponentHour: directSaleQuotationLabourPriceItem.labourComponentHour,
                qty: directSaleQuotationLabourPriceItem.qty,
                stockCode: directSaleQuotationLabourPriceItem.stockCode,
                stockDescription:  directSaleQuotationLabourPriceItem.stockDescription,
                subTotal:directSaleQuotationLabourPriceItem.subTotal,
                systemTypeId: directSaleQuotationLabourPriceItem.systemTypeId,
                systemTypeName: directSaleQuotationLabourPriceItem.systemTypeName,
                tax: null,
                taxPercentage:  directSaleQuotationLabourPriceItem.taxPercentage,
                techDiscountingProcessName:  directSaleQuotationLabourPriceItem.techDiscountingProcessName,
                total: directSaleQuotationLabourPriceItem.total,
                unitPrice: directSaleQuotationLabourPriceItem.unitPrice,
                unitTaxPrice: directSaleQuotationLabourPriceItem.unitTaxPrice,
                value: 0,
                vat:  directSaleQuotationLabourPriceItem.vat
              }
              tem.flag = false;
              element.directSaleQuotationItems.push(tem)
            }
            element.directSaleQuotationItems.forEach((e, index) => {
              e.tax = e?.vat;
              e.value = 0;
              e.flag= e?.isLabourPrice ? true : false;
              if (e.isMiscellaneous) {
                element.directSaleQuotationItems.splice(index, 1)
              }

              if (e.techDiscountingProcessName) {
              } else {
                totalValue = totalValue + e.subTotal;

              }
            });
            element.totalValue = JSON.parse(JSON.stringify(totalValue));
            this.systemTypes.forEach(e => {
              if (e.id == element.systemTypeId) {
                e.isDisabled = true;
              }
            })
            element.systemTypes = this.systemTypes.filter(e => e.id == element.systemTypeId);
          });
          this.systemTypesData.forEach((element, index) => {
            let sorted = [];
            element.directSaleQuotationItems.forEach((e, index) => {
              if (e.techDiscountingProcessName && e.isFixed) {
                e.subTotal = (e.subTotal * 100) / element.totalValue;
                e.subTotal = parseFloat(e.subTotal).toFixed(2);
              }
            });

          });

          this.systemTypesData.forEach((element, index) => {
            let sorted = [];
            element.directSaleQuotationItems.forEach((e, index) => {

              if (e.techDiscountingProcessName) {
                sorted.push(e);
                element.directSaleQuotationItems.splice(index, 1);
                element.directSaleQuotationItems = [...element.directSaleQuotationItems, ...sorted]

              }
            });

          });

          // this.systemTypesData =this.systemTypesData.filter(e=> e.isMiscellaneous == false)
          this.calculateTotalAmountSummary();
          this.rxjsService.setGlobalLoaderProperty(false);
        } else {
          this.details = {};
          this.rxjsService.setGlobalLoaderProperty(false);

        }

      }
      else{
        this.isLoading=false;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
       
    });
  }


}
