import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, NoSalesQuotationModel, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs,
  HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix,
  RxjsService, setRequiredValidator
} from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
@Component({
    selector: 'upsell-no-sales-up-pop-up',
    templateUrl: './upsell-no-sales-pop-up.component.html'
})

export class UpsellNoSalesPopUpComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    formConfigs = formConfigs;
    alphaConfig = new CustomDirectiveConfig({ isAStringWithHyphenDash: true });
    stringConfig = new CustomDirectiveConfig({ isAStringOnlyNoSpace: true });
    noSalesQuotationForm: FormGroup;
    lssResourceId: any;
    loggedUser: UserLogin;
    regionList: any;
    outcomeReasonId: any;
    isCometetorShow = true;
    promotionOfferShow = false;
    newsLetterShow = false;
    motivations: any;
    newsLetterTypes: any;
    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        private dialog: MatDialog,
        private crudService: CrudService,
        public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private store: Store<AppState>, private rxjsService: RxjsService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
        this.createLssResourceForm();
        this.getMotivation();
        this.getNewsLetterTypes();
    }

    getMotivation() {
        this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.SALES_API_UX_MOTIVATION_REASON,
            undefined,
            false,
            null,
            1
        ).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.motivations = response.resources;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    radioChange(e) {
        if (e.value) {
            this.isCometetorShow = true;
            this.noSalesQuotationForm.controls['competitorName'].setValidators([Validators.required]);
            this.noSalesQuotationForm.controls['outcomeReasonId'].setValidators([Validators.required]);
            this.noSalesQuotationForm.controls['isCustomerNotBuyFromCompetitor'].setValue(false);
        } else {
            this.isCometetorShow = false;
            this.noSalesQuotationForm.controls['competitorName'].setValue(null);
            this.noSalesQuotationForm.controls['competitorName'].clearValidators();
            this.noSalesQuotationForm.controls['competitorName'].updateValueAndValidity();
            this.noSalesQuotationForm.controls['outcomeReasonId'].setValue(null);
            this.noSalesQuotationForm.controls['outcomeReasonId'].clearValidators();
            this.noSalesQuotationForm.controls['outcomeReasonId'].updateValueAndValidity();
            this.noSalesQuotationForm.controls['isCustomerNotBuyFromCompetitor'].setValue(true);
        }
    }

    showOptions(e) {
        if (e.checked) {
            this.promotionOfferShow = true;
            this.noSalesQuotationForm.controls['promotionalOfferNewsLetterTypeId'].setValidators([Validators.required]);
        } else {
            this.promotionOfferShow = false;
            this.noSalesQuotationForm.controls['promotionalOfferNewsLetterTypeId'].setValue(null);
            this.noSalesQuotationForm.controls['promotionalOfferNewsLetterTypeId'].clearValidators();
            this.noSalesQuotationForm.controls['promotionalOfferNewsLetterTypeId'].updateValueAndValidity();
        }
    }

    showNewsOptions(e) {
        if (e.checked) {
            this.newsLetterShow = true;
            this.noSalesQuotationForm.controls['newsLetterTypeId'].setValidators([Validators.required]);
        } else {
            this.newsLetterShow = false;
            this.noSalesQuotationForm.controls['newsLetterTypeId'].setValue(null);
            this.noSalesQuotationForm.controls['newsLetterTypeId'].clearValidators();
            this.noSalesQuotationForm.controls['newsLetterTypeId'].updateValueAndValidity()
        }
    }

    getNewsLetterTypes() {
        this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.SALES_API_UX_NEWS_LETTER_TYPES,
            undefined,
            false,
            null,
            1
        ).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.newsLetterTypes = response.resources;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    createLssResourceForm(): void {
        let noSalesQuotationModel = new NoSalesQuotationModel();
        if (noSalesQuotationModel.outcomeReasonId) {
            this.outcomeReasonId = noSalesQuotationModel.outcomeReasonId;
        }
        this.noSalesQuotationForm = this.formBuilder.group({});
        Object.keys(noSalesQuotationModel).forEach((key) => {
            this.noSalesQuotationForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
                new FormControl(noSalesQuotationModel[key]));
        });
        this.noSalesQuotationForm = setRequiredValidator(this.noSalesQuotationForm, ["competitorName", "outcomeReasonId"]);
    }

    onSubmit(): void {
        if (this.noSalesQuotationForm.invalid) {
            return;
        }
        this.noSalesQuotationForm.value.customerId = this.data.customerId;
        this.noSalesQuotationForm.value.noSaleId = '';
        let obj = this.noSalesQuotationForm.getRawValue();
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        obj.isCounterSale = false;
        obj.customerId = this.data?.customerId;
        obj.directSaleQuotationVersionId = this.data?.directSaleQuotationVersionId;
        obj.noSaleId = '';

        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UPSELL_QUOTE_NO_SALE,
            obj, 1).subscribe((response) => {
                if (response.isSuccess && response.statusCode === 200) {

                    this.dialog.closeAll();
                    this.noSalesQuotationForm.reset();
                    this.outputData.emit(true);
                }
            })
    }

    dialogClose(): void {
        this.dialogRef.close(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }

}
