import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from "@app/shared";
import { SalesModuleApiSuffixModels } from "@modules/sales";

@Component({
    selector: 'app-upsell-quote-accept-quote-details',
    templateUrl: './upsell-quote-accept-quote-details.component.html',
    styleUrls: ['./upsell-quote-accept-quote-details.component.scss'],
})
export class UpsellQuoteAcceptQuoteDetailsComponent {
    customerId;
    addressId;
    callType;
    directSaleQuotationVersionId;
    callInitiationId;
    directSaleQuotationSystemTypes=[];
    customerDetails;
    isCustomer: boolean;
    constructor(private router: Router, private activateRoute: ActivatedRoute, private crudService: CrudService,private rxjsService:RxjsService){
        this.customerId = this.activateRoute.snapshot.queryParams.id;
        this.callInitiationId    = this.activateRoute.snapshot.queryParams.callInitiationId;
        this.addressId    = this.activateRoute.snapshot.queryParams.addressId;
        this.callType    = this.activateRoute.snapshot.queryParams.callType;
        this.directSaleQuotationVersionId = this.activateRoute.snapshot.queryParams.versionId;
        this.router.events.subscribe((val: any) => { 
            if (val?.url?.split('/')[1] == "customer") {
                this.isCustomer = true;
            } else if (val?.url?.split('/')[1] == "technical-management") {
                this.isCustomer = false;
            }
        });
        this.activateRoute.url.subscribe((val: any) => {
            if (val?.url?.split('/')[1] == "customer") {
                this.isCustomer = true;
            } else if (val?.url?.split('/')[1] == "technical-management") {
                this.isCustomer = false;
            }
        })
    }
    ngOnInit() {
        this.rxjsService.setGlobalLoaderProperty(true);
        this.getCustomerDetail();   
        this.getSaleItemDetails();
    }
    getCustomerDetail(){
        let params={customerId:this.customerId,callInitiationId:this.callInitiationId}
        this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UPSELL_QUOTE_HEADER_DETAILS, undefined, false, prepareRequiredHttpParams(params), 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {  
              this.customerDetails = response.resources;
            }
          });
    }
    getSaleItemDetails(){
        this.rxjsService.setGlobalLoaderProperty(true);
        let params={customerId:this.customerId,directSaleQuotationVersionId:this.directSaleQuotationVersionId}
        this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_ITEMS_GET_DETAILS, undefined, false, prepareRequiredHttpParams(params), 1).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {   
              this.directSaleQuotationSystemTypes = response.resources.directSaleQuotationSystemTypes;  
              this.rxjsService.setGlobalLoaderProperty(false); 
            }else{
                this.rxjsService.setGlobalLoaderProperty(false);
            }        
          });
    }

    navigateToUpsellPage() {
        if(this.isCustomer) {
            this.router.navigate(['/customer/upsell-quote/quote-info'], {queryParams: {id:this.customerId,callInitiationId:this.callInitiationId,addressId: this.addressId,callType: this.callType}});
        } else {
            this.router.navigate(['/technical-management/call-status-report/upsell-quote/quote-info'], {queryParams: {id:this.customerId,callInitiationId:this.callInitiationId,addressId: this.addressId,callType: this.callType}});
        }
    }
}