import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
    ConfirmDialogPopupComponent, CrudService, formConfigs,
    HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix,
    RxjsService
} from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
@Component({
    selector: 'quote-decline-pop-up-sell',
    templateUrl: './quote-decline-pop-up-sell.component.html'
})

export class QuoteDeclinePopUpSellComponent implements OnInit {
    @Output() outputData = new EventEmitter<any>();
    formConfigs = formConfigs;
    declinedReasonAddEditForm: FormGroup;
    loggedUser: UserLogin;
    outComeReason: any;
    constructor(@Inject(MAT_DIALOG_DATA) public data,
        private dialog: MatDialog,
        private crudService: CrudService,
        public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
        public declineDialogRef: MatDialogRef<QuoteDeclinePopUpSellComponent>,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private store: Store<AppState>, private rxjsService: RxjsService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
    }

    ngOnInit(): void {

        this.rxjsService.setDialogOpenProperty(true);
        this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.SALES_API_UX_OUTCOME_REASON,
            this.data.outcomeId,
            false,
            null,
            1
        ).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.outComeReason = response.resources;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });

        this.declinedReasonAddEditForm = this.formBuilder.group({
            outcomeReasonId: ['', Validators.required],
            comments: ['']

        })
    }

    onSubmit(): void {
        if (this.declinedReasonAddEditForm.invalid) return;
        this.declinedReasonAddEditForm.value.outcomeId = this.data?.outcomeId;
        let obj = this.declinedReasonAddEditForm.value;
        obj.modifiedUserId = this.loggedUser?.userId;
        obj.directSaleQuotationVersionId = this.data?.directSaleQuotationVersionId;
        obj.isCounterSale=false;

      

        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_QUOTATION_DECLINE, obj, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.declineDialogRef.close({ isSubmitted: true });
                this.dialog.closeAll();
                this.outputData.emit(true);
                this.declinedReasonAddEditForm.reset();
            }
        })
    }

    dialogClose(): void {
        this.dialogRef.close(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }

}
