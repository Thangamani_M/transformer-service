import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { loggedInUserData, SalesModuleApiSuffixModels } from '@app/modules';
import { AppState } from '@app/reducers';
import {
    ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs,
    HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator
} from '@app/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
@Component({
    selector: 'upsell-quote-pending-pop-up',
    templateUrl: './upsell-quote-pending-pop-up.component.html',
    styleUrls: ['./upsell-quote-pending-pop-up.component.scss'],
})

export class UpsellquotePendingPopUpComponent implements OnInit {
    formConfigs = formConfigs;
    alphaConfig = new CustomDirectiveConfig({ isAStringWithHyphenDash: true });
    stringConfig = new CustomDirectiveConfig({ isAStringOnlyNoSpace: true });
    pendingQuotationForm: FormGroup;
    loggedUser: UserLogin;
    probabilityClosure: any;
    contactDetails: any;
    conactError = false;
    today = new Date();
    @Output() outputData = new EventEmitter<any>();

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        private dialog: MatDialog,
        private crudService: CrudService,
        public dialogRef: MatDialogRef<ConfirmDialogPopupComponent>,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private store: Store<AppState>, private rxjsService: RxjsService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
        this.createQuotationForm();
        this.getProbabilityClosure();
        this.getRequestedBy();

    }

    getRequestedBy(){
        this.crudService
        .get(
          ModulesBasedApiSuffix.SALES,
          SalesModuleApiSuffixModels.UPSEL_CUSTOMER_REQUESTED_BY,undefined,false,prepareRequiredHttpParams({
           customerId: this.data.customerId,
          })
        ).subscribe(res=>{
          this.rxjsService.setGlobalLoaderProperty(false);

          this.contactDetails = res.resources;

        })
      }
    getProbabilityClosure() {
        this.crudService.get(
            ModulesBasedApiSuffix.SALES,
            SalesModuleApiSuffixModels.SALES_API_PROBABILITY_CLOSURE,
            undefined,
            false,
            null,
            1
        ).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.probabilityClosure = response.resources;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }


    radioChange(e, type) {
        this.pendingQuotationForm.controls['contactNumber'].setValue(e);
        this.pendingQuotationForm.controls['contactNumberCountryCode'].setValue(type);
    }

    createQuotationForm(): void {
        let pendingQuoatationModel ={
            "directSaleQuotationOutcomeId": "",
            "directSaleQuotationVersionId": "",
            "customerCallBackId": "",
            "customerId": "",
            "probabilityOfClosureId": 0,
            "callbackDateTime": "",
            "contactNumber": 0,
            "contactNumberCountryCode": "",
            "comments": "",
            "createdUserId": ""
          }
        this.pendingQuotationForm = this.formBuilder.group({});
        Object.keys(pendingQuoatationModel).forEach((key) => {
            this.pendingQuotationForm.addControl(key, (key == 'createdUserId' || key == 'modifiedUserId') ? new FormControl(this.loggedUser.userId) :
                new FormControl(pendingQuoatationModel[key]));
        });
        this.pendingQuotationForm = setRequiredValidator(this.pendingQuotationForm, ["probabilityOfClosureId", "contactNumber", "contactNumberCountryCode","callbackDateTime", "comments"]);
    }

    onSubmit(): void {
        this.pendingQuotationForm.value.customerId = this.data.customerId;
        if(this.pendingQuotationForm.controls['contactNumber'].status === 'INVALID') {
            this.conactError = true;
        } else {
            this.conactError = false;
        }
        this.pendingQuotationForm.value.directSaleQuotationVersionId = this.data.directSaleQuotationVersionId;
        this.pendingQuotationForm.value.directSaleQuotationOutcomeId = this.data.outcomeId;
        let obj = this.pendingQuotationForm.getRawValue();
        obj.isCounterSale = false;
        obj.directSaleQuotationVersionId = this.data?.directSaleQuotationVersionId;
        obj.directSaleQuotationOutcomeId = this.data?.outcomeId;
        obj.customerId = this.data?.customerId;

        if (this.pendingQuotationForm.invalid) return;
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UPSELL_QUOTE_PENDING,obj, 1).subscribe((response) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.dialog.closeAll();
                this.pendingQuotationForm.reset();
                this.outputData.emit(true)
            }
        })
    }

    dialogClose(): void {
        this.dialogRef.close(false);
        this.outputData.emit(false);
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }

}
