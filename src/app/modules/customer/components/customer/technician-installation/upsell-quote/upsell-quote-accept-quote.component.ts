import { DatePipe, DecimalPipe, ViewportScroller } from '@angular/common';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { CUSTOMER_COMPONENT, SalesModuleApiSuffixModels, selectDynamicEagerLoadingLeadCatgoriesState$, selectDynamicEagerLoadingSourcesState$, selectLeadCreationUserDataState$, selectLeadHeaderDataState$, selectStaticEagerLoadingCustomerTypesState$, selectStaticEagerLoadingPaymentOptionsState$, selectStaticEagerLoadingSiteTypesState$, selectStaticEagerLoadingTitlesState$ } from '@app/modules';
import { AppState } from '@app/reducers';
import { addFormControls, adjustDateFormatAsPerPCalendar, clearFormControlValidators, convertTreeToList, countryCodes, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, formConfigs, HttpCancelService, IApplicationResponse, landLineNumberMaskPattern, LoggedInUserModel, mobileNumberMaskPattern, ModulesBasedApiSuffix, OtherService, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareAutoCompleteListFormatForMultiSelection, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControls, ResponseMessageTypes, RxjsService, SERVER_REQUEST_DATE_TRANSFORM, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { InstallationPriceModel, LeadCreationUserDataModel, LeadHeaderData } from '@modules/sales/models';
import { BillingModuleApiSuffixModels, LeadOutcomeStatusNames } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take, tap } from 'rxjs/operators';
import { UpsellNoSalesPopUpComponent } from './no-sales/upsell-no-sales-pop-up.component';
import { QuoteDeclinePopUpSellComponent } from './quote-decline/quote-decline-pop-up-sell.component';
import { UpsellquotePendingPopUpComponent } from './quote-pending/upsell-quote-pending-pop-up.component';
import { UpsellDocumentUploadComponent } from './upsell-document-upload.component';
import { UpsellQuotePaymentModel } from '@app/modules/customer/models';


declare var $;
enum PaymentMethods {
  ON_INVOICE = 1,
  DEBIT_ORDER = 2
}

const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
// QuoteDeclinePopUpComponent
@Component({
  selector: 'app-upsell-quote-accept-quote',
  templateUrl: './upsell-quote-accept-quote.component.html',
  styleUrls: ['./upsell-quote-accept-quote.component.scss'],
  providers: [DatePipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})

export class UpsellQuoteAcceptQuoteComponent {

  formConfigs = formConfigs;
  selectedTabIndex = 0;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  alphaNumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  leadForm: FormGroup;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  status = [];
  paymentInvoiceForm: FormGroup;
  paymentInstallments: FormArray;
  rawLeadId: string;
  leadId: string;
  customerId: string;
  addressList = [];
  today: any = new Date()
  searchColumns: any
  filteredSchemes = [];
  selectedAddressOption = {};
  selectedLocationOption = {};
  selectedCityOption = {};
  selectedSuburbOption = {};
  selectedCommercialProducts: any;
  loggedInUserData: LoggedInUserModel;
  isSchemeMandatory: boolean;
  todayDate = new Date();
  siteType: string;
  pageTitle: string = "Create";
  btnName = "Next";
  fromPage: string;
  tmpCurrentURL: string;
  tmpListName: string;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  lssSchemes = [];
  customerTypes = [];
  leadCategories = [];
  titles = [];
  siteTypes = [];
  latLongList = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  countryCodes = countryCodes;
  customerTypeName: string;
  commercialCustomerType = 'Commercial';             // edit here
  residentialCustomerType = 'Residential';            // edit here
  siteTypeName: string;
  mobileNumberMaskPattern = mobileNumberMaskPattern;
  landLineNumberMaskPattern = landLineNumberMaskPattern;
  existingCustomers = [];
  selectedExistingCustomer;
  objectKeys = Object.keys;
  subTotalSummary = {
    unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0
  };
  paymentMethodName:string;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  mobileNumberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  valueChangesCount = 0;
  addressModalMessage = "";
  existingAddresses = [];
  installedRentedItems = [];
  existingCustomerAddress;
  selectedInstalledRentedItems = [];
  sourceTypes = [];
  isFormSubmitted = false;
  dataList = [];
  totalCount = 0;
  loading: boolean;
  pageLimit: any = [10, 25, 50, 75, 100];
  existingLeadsObservableResponse: IApplicationResponse;
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  pageSize: number = 10;
  leadHeaderData: LeadHeaderData;
  fromUrl = 'Leads List';
  isLeadGroupUpgrade = false;
  reconnectionMessage = '';
  selectedExistingCustomerProfile = { isOpenLeads: false };
  isDataFetchedFromSelectedCustomerProfile = false;
  countryId = formConfigs.countryId;
  primengTableConfigProperties: any;
  parseHtml;
  isAcceptTermsandConditions = false;
  contractTermsandConditions = [];
  isContractBtnDisabled = true;
  selectedTermsAndConditions = [];
  isAcceptTermsAndConditionsBtnDisabled = true;
  selectedIndex;
  isSubmit:boolean=false;
  @ViewChild('existing_address_modal_installed_items', { static: false }) existing_address_modal_installed_items: ElementRef<any>;
  @ViewChild('existing_lead_modal', { static: false }) existing_lead_modal: ElementRef<any>;
  @ViewChild('existing_address_footprint_modal', { static: false }) existing_address_footprint_modal: ElementRef<any>;
  @ViewChild('confirm_reconnection_modal', { static: false }) confirm_reconnection_modal: ElementRef<any>;
  // @ViewChild('input', { static: false }) row;
  provinceList: any;
  cities: any;
  suburbs: any;
  details: any;
  systemTypes = [];
  systemTypesData: any = [];
  selectedSystemId: any;
  itemData: any;
  selectedQuote: any;
  headerDetails: any;
  searchKeyword: FormControl;
  searchForm: FormGroup;
  totalObj = {
    subTotal: 0,
    total: 0
  }
  columnFilterForm: FormGroup;
  totalRecords: number;
  row: any = {}
  paymentMethods: any;
  invoiceDetails: any;
  doRunData: any;
  fileName11: any;
  file_Name: any;
  debitOrderDetails: any;
  noOfPayments: any = [];
  callInitiationId: any;
  contactNo: string;
  callInitiationData: any;
  outcome: any=[];
  installationsTypes:any=[];
  isItemAndOnInvoiceOnly = false;
  qrcode: any;
  barcode: any;
  payAtBarcode: any;
  payAtQRcode: any;
  outcomeStatus:boolean=false;
  openAddContactDialog:boolean=false;
  termsAndConditionPopup:boolean=false;
  addNewContactForm: FormGroup;
  IsInstallationDebtor:boolean=false;
  fileName;
  actionPermissionObj: any;
  isCustomer: boolean;
  callType: string;
  addressId: string;
  installationPaymentOptions: FormArray;
  directSaleQuotationVersionIds=[];
  constructor(private _vps: ViewportScroller, private sanitizer: DomSanitizer,private crudService: CrudService, private momentService: MomentService,
    private datePipe: DatePipe, private tableFilterFormService: TableFilterFormService,
    private decimalPipe: DecimalPipe, private otherService: OtherService,
    private dialogService: DialogService,
    private httpCancelService: HttpCancelService, private _fb: FormBuilder, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private dialog: MatDialog,
    private rxjsService: RxjsService, private router: Router, private store: Store<AppState>) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.callInitiationId = params['params']['callInitiationId'];
      this.customerId = params['params']['id'];
      this.callType = params['params']['callType'];
      this.addressId = params['params']['addressId'];
      this.fromPage = params['params']['pageFrom'];
      this.callType = params['params']['callType'];
    });
    this.primengTableConfigProperties = {
      tableCaption: "Upsell Quote",
      selectedTabIndex: 0,
      breadCrumbItems: [{displayName: 'Customer Management'},{ displayName: 'Customer : Upsell Quote', relativeRouterUrl: 'customer/manage-customers/call-initiation', queryParams: {customerId: this.customerId, customerAddressId: this.addressId, initiationId: this.callInitiationId, callType: this.callType} }, { displayName: 'Quote acceptance', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Upsell Quote',
            dataKey: 'documentId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'description', header: 'Description' },
            { field: 'documnetType', header: 'Document Type' },
            { field: 'createdDate', header: 'Created Date' },
            { field: 'uploadedBy', header: 'Uploaded By' },
            ],
            apiSuffixModel: SalesModuleApiSuffixModels.DIRECT_SALES_DOCUMENT_UPLOAD,
            moduleName: ModulesBasedApiSuffix.SALES,
            enableMultiDeleteActionBtn: true,
            ebableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.rxjsService.getCallInitiationData().subscribe((data: any) => {
      this.callInitiationData = data;
    });
    this.router.events.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "customer") {
          this.isCustomer = true;
          this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Customer Management';
          this.primengTableConfigProperties.breadCrumbItems[1].displayName = 'Customer : Upsell Quote';
          this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/customer/manage-customers/call-initiation';
          this.primengTableConfigProperties.breadCrumbItems[1].queryParams = {customerId: this.customerId, customerAddressId: this.addressId, initiationId: this.callInitiationId, callType: this.callType};
      } else if (val?.url?.split('/')[1] == "technical-management") {
          this.isCustomer = false;
          this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
          this.primengTableConfigProperties.breadCrumbItems[1].displayName = 'Call Status Report';
          this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/call-status-report';
          this.primengTableConfigProperties.breadCrumbItems[1].queryParams = {tab: 1};
      }
  });
  this.activatedRoute.url.subscribe((val: any) => {
      if (val?.url?.split('/')[1] == "customer") {
          this.isCustomer = true;
          this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Customer Management';
          this.primengTableConfigProperties.breadCrumbItems[1].displayName = 'Customer : Upsell Quote';
          this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/customer/manage-customers/call-initiation';
          this.primengTableConfigProperties.breadCrumbItems[1].queryParams = {customerId: this.customerId, customerAddressId: this.addressId, initiationId: this.callInitiationId, callType: this.callType};
      } else if (val?.url?.split('/')[1] == "technical-management") {
          this.isCustomer = false;
          this.primengTableConfigProperties.breadCrumbItems[0].displayName = 'Technicial Management';
          this.primengTableConfigProperties.breadCrumbItems[1].displayName = 'Call Status Report';
          this.primengTableConfigProperties.breadCrumbItems[1].relativeRouterUrl = '/technical-management/call-status-report';
          this.primengTableConfigProperties.breadCrumbItems[1].queryParams = {tab: 1};
      }
  })
    this.combineLatestNgrxStoreData();
  }
  ngOnInit(): void {
    for (let i = 2; i < 6; i++) {
      this.noOfPayments.push(i);
    }
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.createpaymentInvoiceForm();
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.getDetailsById();
    this.getListById();
    this.getDocumentById();
    this.getPaymentMethods();
    this.getDoRunata();
    this.getAgreementOutcomeById();
    this.createAddNewContactForm();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  getPaymentMethods() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_DOG_TYPES, undefined, false, undefined, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.paymentMethods = response.resources.paymentMethods;

      }
    });
  }
  acceptedTotal: any = 0
  openPayment = false;
  proceed() {
    // if(!this.getUpsellQuoteActionPermission(CUSTOMER_COMPONENT.PROCEED_TO_PAYMENT)) {
    //   return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    // }
    //  this.process = true;
    // this.acceptedTotal = 18533.45;
    // this.openPayment = true;


    this.acceptedTotal = 0;
    this.details.forEach(element => {
      if (element.outcomeId == 1 && !element.isQuotationPaymentPaid) {
        this.acceptedTotal = this.acceptedTotal + element.total;
      }
    });

    if (this.acceptedTotal > 0) {
      this.openPayment = true;
    } else {
      this.snackbarService.openSnackbar("Total payment is already done", ResponseMessageTypes.ERROR);
      return;
    }
    if(this.paymentMethodName == 'On Invoice/EFT'  || this.paymentMethodName?.toLowerCase() == 'On Invoice/EFT'?.toLocaleLowerCase()) {
      this.getInvoiceDetails();
    }else if(this.paymentMethodName == 'Debit Order'  || this.paymentMethodName?.toLowerCase() == 'Debit Order'?.toLocaleLowerCase()) {
      this.getDebitOrderDetails();
    }
  }
  enableProceedButton = false;
  redirectCustomerView() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.callInitiationData?.customerAddressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  getAgreementOutcomeById(): void {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.SALES_API_UX_OUTCOME,
      null,
      false,
      null,
      1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        response.resources.forEach((ele)=>{
          if(ele.displayName !=  "Expired") {
            this.outcome.push(ele);
          }
        })
        // this.outcome = response.resources;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  goBack() {
    this.router.navigate(['customer/manage-customers/call-initiation'], { queryParams: { customerId: this.customerId, customerAddressId: this.addressId, initiationId: this.callInitiationId, callType: this.callType } });
  }

  appointmentCreate = false;
  createJob(flag) {
    let postObj = {
      "DirectSaleId": this.headerDetails?.directSaleId,
      "CustomerId": this.headerDetails?.customerId,
      "AddressId": this.headerDetails?.addressId,
      "CreatedUserId": this.loggedInUserData?.userId
    }

    // this.router.navigate(['/customer/upsell-quote/invoice'], { queryParams: {id: this.id } })
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, SalesModuleApiSuffixModels.CREATE_JOB, postObj, 1).subscribe((response) => {
      this.rxjsService.setGlobalLoaderProperty(true);
      if (response.isSuccess && response.statusCode == 200) {
        this.process = false;
        // if(flag == 'yes') {
        //   this.appointmentCreate = true
        // }else {
        //   this.appointmentCreate = false
        // }
        flag == 'yes' ? this.appointmentCreate = true : this.appointmentCreate = false;
        /// validate api call //
        // this.appointmentCreate = false;
        // let params = {directSaleQuotationVersionIds:this.directSaleQuotationVersionIds?.toString()}
        // this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_PAYMENT, undefined, false, prepareRequiredHttpParams({
        //   DirectSaleQuotationVersionIds: this.directSaleQuotationVersionIds
        // }), 1).subscribe((response) => {
        //   if(response.resources?.message==null || !response.resources?.message || response.resources?.message==''){
        //     this.appointmentCreate = true;
        //   }else {
        //     this.appointmentCreate = false;
        //     this.snackbarService.openSnackbar(response.resources?.message, ResponseMessageTypes.WARNING);
        //   }
        //   this.rxjsService.setGlobalLoaderProperty(false);
        // });
        this.rxjsService.setGlobalLoaderProperty(false);
       // flag == 'yes' ? this.appointmentCreate = true : this.appointmentCreate = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setDialogOpenProperty(false);
    })
  }

  createAppointment() {
    let postObj = {
      "DirectSaleId": this.headerDetails?.directSaleId,
      "CallInitiationId": this.callInitiationId,
      "CreatedUserId": this.loggedInUserData?.userId
    }

    // this.router.navigate(['/customer/upsell-quote/invoice'], { queryParams: {id: this.id } })
    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, SalesModuleApiSuffixModels.CREATE_APPOINTMENT, postObj, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.appointmentCreate = false;
      //  this.getListById();
      }
      this.rxjsService.setDialogOpenProperty(false);
    })
  }

  getListById(): void {
    if (!this.customerId) return;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UPSELL_QUOTE_QUOTATION_LIST, undefined, false, prepareRequiredHttpParams({
      customerId: this.customerId,
      callInitiationId:this.callInitiationId
    }), 1).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(true);
      if (response.isSuccess && response.statusCode === 200) {
        this.details = response.resources;
        if(this.paymentMethodName == 'On Invoice/EFT'  || this.paymentMethodName?.toLowerCase() == 'On Invoice/EFT'?.toLocaleLowerCase()) {
          this.getInvoiceDetails();
        }else if(this.paymentMethodName == 'Debit Order'  || this.paymentMethodName?.toLowerCase() == 'Debit Order'?.toLocaleLowerCase()) {
          this.getDebitOrderDetails();
        }
        let subTotal = 0;
        this.details.forEach(element => {

          if(element.outcomeId == 3){
            element.outcomeFlag = false;
          }
          else
             element.outcomeFlag = element.outcomeId ? true:false;

          if (element.outcomeId == 1) {
           // if(element.isQuotationPaymentPaid)
              // this.selectedQuoteString = this.selectedQuoteString + ' ' + element.directSaleQuotationVersionNumber

            this.enableProceedButton = true;
          //  console.log('this.selectedQuoteString1',this.selectedQuoteString)
          }
          this.totalObj.subTotal += element.subTotal;
          this.totalObj.total += element.total;
          this.totalObj.subTotal = parseFloat(this.totalObj.subTotal.toFixed(2))
          this.totalObj.total = parseFloat(this.totalObj.total.toFixed(2))

          // element.doaStatus = 'Approved' ;
          element.expiryDate = this.datePipe.transform(element.expiryDate, "d MMM y");
        });
        this.rxjsService.setGlobalLoaderProperty(false);
      }else {
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getDocumentById(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    if (!this.customerId) return;
    let obj1 = {};
    obj1 = {
      customerId: this.customerId ? this.customerId : '',
      callInitiationId: this.callInitiationId ? this.callInitiationId : ''
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UPSELL_DOCUMENT_LIST, undefined, false, prepareRequiredHttpParams(otherParams), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  obj: any;
  onFileSelected(files: FileList): void {
    // this.showFailedImport = false;
    const fileObj = files.item(0);
    this.fileName = fileObj.name;
    // if (this.fileName) {
    //   this.isFileSelected = true;
    // }
    const fileExtension = fileObj.name.slice(fileObj.name.length - 5, fileObj.name.length);
    if (fileExtension !== '.xlsx') {
     // this.snackbarService.openSnackbar("Please select .xlsx file extension only", ResponseMessageTypes.WARNING);
     // return;
    }
    this.obj = fileObj;
    if (this.obj.name) {
    }
  }
  addDocumnent = false;
  addDocument(data?: any) {
    this.addDocumnent = true;
    data = data ? data : {};
    data['customerId'] = this.customerId;
    data['directSaleId'] = this.headerDetails?.directSaleId;
    data['createdUserId'] = this.loggedInUserData?.userId;
    const dialogReff1 = this.dialog.open(UpsellDocumentUploadComponent, { width: '700px', data, disableClose: false });
    dialogReff1.componentInstance['outputData'].subscribe(ele => {
      if (ele) {
        this.getDocumentById();
      } else {

      }
    });
  }

  deleteDocument() {

  }
  onClickUpsellQuote(val){
    if(this.isCustomer) {
      this.router.navigate(['/customer/upsell-quote/details'], { queryParams: {id: this.customerId ,versionId:val.directSaleQuotationVersionId,callInitiationId:this.callInitiationId, addressId: this.addressId, callType: this.callType} });
    } else {
      this.router.navigate(['/technical-management/call-status-report/upsell-quote/details'], { queryParams: {id: this.customerId ,versionId:val.directSaleQuotationVersionId,callInitiationId:this.callInitiationId, addressId: this.addressId, callType: this.callType} });
    }
  }
  onCRUDRequested(type: CrudType | string, row?: object): void { }

  getDetailsById(): void {
    if (!this.customerId) return;

    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UPSELL_QUOTE_HEADER_DETAILS, undefined, false, prepareRequiredHttpParams({
      customerId: this.customerId,
      callInitiationId: this.callInitiationId
    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.headerDetails = response.resources;
        this.paymentMethodName = response.resources.paymentmethodName;
        this.contactNo = this.headerDetails?.contactNoCountryCode ? this.headerDetails?.contactNoCountryCode + " " + this.headerDetails?.contactNo : '';

        this.rxjsService.setGlobalLoaderProperty(false);
      }else {
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  confirmationAlert = false;
  changeOutcome(dt,index) {
    this.selectedIndex = index;
    this.selectedQuote = dt;
    let filter = this.outcome.filter(x=>x.id==dt.outcomeId);
    if (filter && filter.length >0 && filter[0].displayName=='Decline') {
      this.enableProceedButton = false;
      let data = {};
      data = dt;
      const dialogReff1 = this.dialog.open(QuoteDeclinePopUpSellComponent, { width: '700px', data, disableClose: false });
      dialogReff1.componentInstance['outputData'].subscribe(ele => {
        if (ele) {
          this.getListById();
          this.outcomeStatus = true;
        } else {
          this.outcomeStatus = false;
        }
      });
      dialogReff1.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        this.outcomeStatus  = false;
      })
    } else if (filter && filter.length >0 && filter[0].displayName=='Pending') {
      this.enableProceedButton = false;
      let data = {};
      data = dt;
      data['customerId'] = this.headerDetails?.customerId;
      const dialogReff2 = this.dialog.open(UpsellquotePendingPopUpComponent, { width: '700px', data, disableClose: false });
      dialogReff2.componentInstance['outputData'].subscribe(ele => {
        if (ele) {
          this.getListById();
          this.outcomeStatus = true;
        } else {
          this.outcomeStatus  = false;
        }
      });
      dialogReff2.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        this.outcomeStatus  = false;
      //  this.getListById();
      })
    } else if (filter && filter.length >0 && filter[0].displayName=='No Sale') {
      this.enableProceedButton = false;
      let data = {};
      data = dt;
      data['customerId'] = this.headerDetails?.customerId;
      const dialogReff3 = this.dialog.open(UpsellNoSalesPopUpComponent, { width: '700px', data, disableClose: false });
      dialogReff3.componentInstance['outputData'].subscribe(ele => {
        if (ele) {
          this.getListById();
          this.outcomeStatus = true;
        } else {
          this.outcomeStatus = false;
        }
      });
      dialogReff3.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
        this.outcomeStatus  = false;
       // this.getListById();
      })
    }
    else if(filter && filter.length >0 && filter[0].displayName=='Expired') {
      this.confirmationAlert = false;
    }
    else {
      this.confirmationAlert = true;
    }
    // this.outcomeStatus = true;
  }
  createAddNewContactForm() {
    this.addNewContactForm = this.formBuilder.group({
      contactName: ['', Validators.required],
      contactNoCountryCode: ['+27', Validators.required],
      contactNo: ['', Validators.required],
      isPrimary: [null, Validators.required],
      keyholderId: null,
      callInitiationContactId: null,
      customerId: this.customerId ? this.customerId : null,
      createdUserId: this.loggedInUserData.userId

    })
  }

  onHideAccept(){
    this.outcomeStatus  = false;
  }

  getContractTermsAndConditions() {
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.TECHNICAL_UPSELLING_TERMS_AND_CONDITIONS ,
        undefined, false,null, 1
      )
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          // this.callIdNo = response.resources.callId;
          this.rxjsService.setPopupLoaderProperty(false);
          response.resources.technicalUpsellingTermsandConditions.forEach((contractTermsandConditions) => {
            contractTermsandConditions.isCheckboxDisabled = true;
            contractTermsandConditions.isTermsAndConditionsRead = false;
          })
          this.contractTermsandConditions = response.resources.technicalUpsellingTermsandConditions;
          this.contractTermsandConditions.forEach((contractTermsandCondition, index: number) => {
            if (index == 0) {
              this.selectedTermsAndConditions = contractTermsandCondition;
            }
            if (contractTermsandCondition.parentTermsandConditionId === null || !contractTermsandCondition.parentTermsandConditionId) {
              this.parseHtml = JSON.stringify(contractTermsandCondition.termsandConditionDescription);
              this.parseHtml = JSON.parse(this.parseHtml);
              this.parseHtml = this.sanitizer.bypassSecurityTrustHtml(this.parseHtml);
            }
            contractTermsandCondition.isChecked = false;
          })
        }
        setTimeout(() => {
          this.rxjsService.setPopupLoaderProperty(false);
          this.rxjsService.setGlobalLoaderProperty(false);
        },500);
      });
  }
  scrollFn(anchor: string) {
    this._vps.scrollToAnchor(anchor);
  }

  onOpenTermsAndConditionsIndex(selectionTypeVia: string, index: number, contractTermsandCondition, anchor: string) {
    if (selectionTypeVia == 'link') {
      contractTermsandCondition.isCheckboxDisabled = contractTermsandCondition.isChecked ? true : false;
      contractTermsandCondition.isTermsAndConditionsRead = true;
    }
    else if (selectionTypeVia == 'checkbox') {
      contractTermsandCondition.isChecked = true;
      contractTermsandCondition.isCheckboxDisabled = true;
    }
    let element = document.getElementById(anchor);
    if (element) {
      element.scrollIntoView(); // scroll to a particular element
    }
    this.onTermsAndConditionsClicked();
  }

  onTermsAndConditionsClicked(type?: string) {
    let tempArray = [];
    this.contractTermsandConditions.forEach((value, index) => {
      if (!value.parentTermsandConditionId || value.parentTermsandConditionId === null) {
        this.contractTermsandConditions.splice(index, 1);
      }
      tempArray = this.contractTermsandConditions;
    });
    this.isContractBtnDisabled = (tempArray.filter(ct => !ct['isChecked']).length == 0 && this.isAcceptTermsandConditions &&
      tempArray.length > 0) ? false : true;
    this.isAcceptTermsAndConditionsBtnDisabled = this.contractTermsandConditions.filter(c => c['isChecked']).length !== this.contractTermsandConditions.length;
    if (type == 'final accept' && this.isAcceptTermsandConditions || type == undefined && this.isAcceptTermsandConditions) {
      this.isAcceptTermsAndConditionsBtnDisabled = true;
    }
  }

  termsAndCondtionCancelAccept() {
    if(this.details && this.details.length >0){
      this.details[this.selectedIndex].outcomeId=null;
    }

    this.confirmationAlert = false;
    this.outcomeStatus  = false;
    this.termsAndConditionPopup=false;
    this.rxjsService.setGlobalLoaderProperty(false);
   // this.getListById();
  }
  cancelAccept() {
    if(this.details && this.details.length >0){
      this.details[this.selectedIndex].outcomeId=null;
    }
    this.confirmationAlert=false;
    this.outcomeStatus  = false;
     this.termsAndConditionPopup=false;
     this.rxjsService.setGlobalLoaderProperty(false);
  }
  onArrowClick(str) {
    if (str == 'forward') {
      this.router.navigate(['/customer/counter-sales/quote-info'], { queryParams: { id: this.customerId, callInitiationId: this.callInitiationId, addressId: this.addressId, callType: this.callType } });

    } else {
      if (this.isCustomer) {
      this.router.navigate(['/customer/upsell-quote/item-info'], { queryParams: { id: this.customerId, callInitiationId: this.callInitiationId, addressId: this.addressId, callType: this.callType } });
      } else {
        this.router.navigate(['technical-management/call-status-report/upsell-quote/item-info'], { queryParams: { id: this.customerId, callInitiationId: this.callInitiationId, addressId: this.addressId, callType: this.callType } });
      }
    }
  }
  process = false;
  selectedQuoteString = "";

  acceptQuoteYes(){
    this.confirmationAlert = false;
    this.termsAndConditionPopup = true;
    this.getContractTermsAndConditions();
  }
  onChangePaymentOption() {
    let payment = this.paymentInvoiceForm.value.paymentOptionId;
    let found;
    this.installationsTypes.forEach(element => {
      if (element.paymentOptionId == payment) {
        found = element.paymentPercentage;
      }
    });
    this.installationPaymentOptions = this.getInstllationOptionDetils;
    if (found?.length > 0) {
      this.installationPaymentOptions?.clear();
      found.forEach((element: UpsellQuotePaymentModel,index) => {
        let elementCopy = { ...element };
        // if payment method id is 1 then by default it is oninvoice
        elementCopy.amount=null;
        elementCopy.paymentDate = null;
        elementCopy.createdUserId = this.loggedInUserData.userId;
        elementCopy.paymentMethodId = this.isItemAndOnInvoiceOnly ? '1' : "";
        this.installationPaymentOptions.push(this.createInstallationPaymentOptions(elementCopy));
        let filter = this.paymentMethods.filter(x=>x.displayName == 'On Invoice/EFT');
        if(filter?.length >0){
          this.installationPaymentOptions.controls[index].get('paymentMethodId').setValue(filter[0].id);
         // this.installationPaymentOptions.controls[index].get('paymentMethodId').disable();
         // this.paymentInvoiceForm?.get('installationPaymentOptions').setValue(this.installationPaymentOptions);

          //this.paymentInvoiceForm?.get('installationPaymentOptions')[index].get('paymentMethodId').setValue(filter[0].id);
          //  this.paymentInvoiceForm?.get('installationPaymentOptions')?.controls[index].get('paymentMethodId').setValue(filter[0].id);

        }
      });
    }

  }
  getDateFormat() {
    return adjustDateFormatAsPerPCalendar();
  }
  onPaymentMethodChanged(index: number) {
    // this.installationPaymentOptions.controls[index].get('shouldDisable').setValue(false);
    // if (this.installationPaymentOptions.controls[this.selectedIndex].get('paymentMethodId').value == PaymentMethods.DEBIT_ORDER &&
    //   this.installationPaymentOptions.controls[index].get('paymentDate').value) {
    //   this.validateSelectedDate(this.installationPaymentOptions.controls[index].get('paymentDate').value);
    // }
  }
  validateSelectedDate(event:any,index:any) {

    let paymentDate = this.datePipe.transform(event, SERVER_REQUEST_DATE_TRANSFORM);
    this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.VALIDATE_UPSELL_PAYMENT_DATE, undefined, false, prepareGetRequestHttpParams(null, null, {
      callInitiationId: this.callInitiationId,
      PaymentDate:paymentDate,
      customerId:this.customerId
    })).subscribe((resp) => {
      if(resp?.isSuccess){
        this.rxjsService.setPopupLoaderProperty(false);
      }else {
        this.installationPaymentOptions.controls[index].get('paymentDate').setValue(null);
        this.rxjsService.setPopupLoaderProperty(false);
      }
      // this.installationPaymentOptions.controls[this.selectedIndex].get('isSelectedDateValid').setValue(resp.isSuccess);
      // this.installationPaymentOptions.controls[this.selectedIndex].get('invalidMessage').setValue(resp.message);
      setTimeout(() => {
        this.rxjsService.setPopupLoaderProperty(false);
        this.rxjsService.setGlobalLoaderProperty(false);
      },500);
    });
  }
  acceptQuote() {
    let obj = {};
    obj['modifiedUserId'] = this.loggedInUserData.userId;
    obj['directSaleQuotationVersionId'] = this.selectedQuote.directSaleQuotationVersionId;
    obj['outcomeId'] = this.selectedQuote.outcomeId;
    obj['isCounterSale'] = false;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_QUOTATION_ACCEPT, obj, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.confirmationAlert = false;
      //  this.selectedQuoteString = '';
        this.details.forEach(element => {
          if (element.outcomeId == 1) {
            if(element.isQuotationPaymentPaid)
             //  this.selectedQuoteString = this.selectedQuoteString + ',' + element.directSaleQuotationVersionNumber

            this.enableProceedButton = true;
          }

        });
        this.enableProceedButton  = true;
      }
      this.termsAndConditionPopup = false;
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(selectLeadCreationUserDataState$),
    this.store.select(loggedInUserData),
    this.store.select(selectStaticEagerLoadingTitlesState$),
    this.store.select(selectStaticEagerLoadingSiteTypesState$),
    this.store.select(selectStaticEagerLoadingCustomerTypesState$),
     this.store.select(selectDynamicEagerLoadingLeadCatgoriesState$),
    this.store.select(selectLeadHeaderDataState$),
    this.store.select(selectDynamicEagerLoadingSourcesState$),
    this.store.select(currentComponentPageBasedPermissionsSelector$),
    this.store.select(selectStaticEagerLoadingPaymentOptionsState$)
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.leadCreationUserDataModel = new LeadCreationUserDataModel(response[0]);
        this.loggedInUserData = new LoggedInUserModel(response[1]);
        this.titles = response[2];
        this.siteTypes = response[3];
        this.customerTypes = response[4];
        this.sourceTypes = response[7];
        this.installationsTypes = response[9];
        this.rxjsService.setGlobalLoaderProperty(false);
        this.leadHeaderData = response[6];
        this.leadCategories = prepareAutoCompleteListFormatForMultiSelection(response[5]);
        let permission = response[8][CUSTOMER_COMPONENT.CUSTOMER]?.find(el => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD);
        if (permission) {
          let techPermission = convertTreeToList([permission?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.TECHNICAL)]);
          const objName = this.callType == '1' ? CUSTOMER_COMPONENT.INSTALLATION : this.callType == '2' ? CUSTOMER_COMPONENT.SERVICE :
            this.callType == '3' ? CUSTOMER_COMPONENT.SPECIAL_PROEJCT : '---';
          const callActionPermissionObj = techPermission?.find(el => el?.[objName])?.[objName];
          if(callActionPermissionObj) {
            const quickActionPermissionObj = callActionPermissionObj?.find(el => el?.menuName == PermissionTypes.QUICK_ACTION);
            if(quickActionPermissionObj) {
              this.actionPermissionObj = quickActionPermissionObj?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.UPSELL_QUOTE);
            }
          }
        }

      });
  }

  getUpsellQuoteActionPermission(actionTypeMenuName) {
    return this.actionPermissionObj?.subMenu?.find(el => el?.menuName == actionTypeMenuName);
  }

  getUpsellQuoteEditPermission() {
    return !this.getUpsellQuoteActionPermission(PermissionTypes.EDIT);
  }

  getDoRunata() {
    this.crudService.get(ModulesBasedApiSuffix.BILLING, SalesModuleApiSuffixModels.UX_DORUN,  undefined, false,prepareRequiredHttpParams({
      customerId: this.customerId
    }), 1).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.doRunData = response.resources;
        }
      });

  }

  getInvoiceDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.INVOICE_PAYMENT_UPSELL_QUOTE, undefined, false, prepareRequiredHttpParams({
      customerId: this.customerId,
      directSaleQuotationId: this.details[0]?.directSaleQuotationId
    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.resources && response.statusCode === 200) {
        this.invoiceDetails = response.resources;
        this.onPaymentProccedChange( this.invoiceDetails);
      //  this.paymentInvoiceForm.get("paymentMethodName").setValue(response.resources?.paymentMethodName);
        this.payAtBarcode = this.invoiceDetails?.quotationVersionPayment[0]?.payAtBarcode
        this.payAtQRcode = this.invoiceDetails?.quotationVersionPayment[0]?.payAtQRcode;
        this.invoiceDetails.debtorDetails.forEach(element => {
          if (element.accountNo) {
            let len = element.accountNo.length - 4;
            let first = element.accountNo.substring(0, len);
            let last = element.accountNo.substring(len, element.accountNo.length);
            let str = ''
            for (let i = 0; i < first.length; i++) {
              str = str + '*';
            }
            element.accountNo = str + '' + last;
          }
        });
        response.resources?.quotationVersionPayment.forEach(ele => {
          this.directSaleQuotationVersionIds.push(ele.directSaleQuotationVersionId);
        });
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  createpaymentInvoiceForm() {
    // let tempTotal = '';
    // this.acceptedTotal = +this.acceptedTotal;
    // if (this.acceptedTotal.toString().indexOf('.') !== -1) {
    //   tempTotal = this.decimalPipe.transform(this.acceptedTotal, '.2', 'en-GB').toString().replace(/,/g, "");
    // }
    // else {
    //   tempTotal = this.decimalPipe.transform(this.acceptedTotal, '0.', 'en-GB').toString().replace(/,/g, "");
    // }
    let model = {
      "totalPaymentAmount": "",
      "paymentAmount": "",
      "paymentDate": "",
      "isPurchaseOrder": false,
      "bankName": '',
      "accountHolderName": '',
      "noOfPayments": "",
      "bankBranch": '',
      "purchaseOrderDate": "",
      "purchaseOrderNo": "",
      "paymentMethodId": 1,
      "paymentMethodName": "",
      "paymentOptionId":"",
      "mobile1": "",
      "mobile1CountryCode": "",
      "file": '',
      "debtorAccountDetailId": '',
      "debitOrderRunCodeId":'',
      "createdUserId": this.loggedInUserData.userId,
      "quotationVersionPayment": [],
      "debtorDetails": '',
      "startDate":"",
      "totalPaymentAmountWithRand":""
    }
    this.paymentInvoiceForm = this.formBuilder.group({
      paymentInstallments: this.formBuilder.array([]),
      installationPaymentOptions: this.formBuilder.array([])
    });
    Object.keys(model).forEach((key) => {
      this.paymentInvoiceForm.addControl(key, new FormControl(model[key]));
    });

    this.paymentInvoiceForm = setRequiredValidator(this.paymentInvoiceForm, ["paymentOptionId"]);
    this.onvalueChange();

    this.changePurchaseOrder();
  }
  onPaymentProccedChange(invoiceDetails) {
    let debtorDetails=[];
    debtorDetails = invoiceDetails?.debtorDetails;
    this.paymentInvoiceForm.get('totalPaymentAmount').setValue(invoiceDetails?.totalPaymentAmount);
    this.paymentInvoiceForm.get('totalPaymentAmountWithRand').setValue('R ' +invoiceDetails?.totalPaymentAmount);
    this.paymentInvoiceForm.get('quotationVersionPayment').setValue(invoiceDetails?.quotationVersionPayment);
    this.paymentInvoiceForm.get('debtorAccountDetailId').setValue(invoiceDetails?.debtorAccountDetailId);
    this.paymentInvoiceForm.get('paymentMethodName').setValue(this.paymentMethodName);
    if(!invoiceDetails?.debtorAccountDetailId){
      if(debtorDetails?.length >0)
       this.paymentInvoiceForm.get('debtorAccountDetailId').setValue(debtorDetails[0].debtorAccountDetailId);
    }
    this.changeBankAccount();
    this.changePurchaseOrder();
  }
  get getInstllationOptionDetils(): FormArray {
    if (!this.paymentInvoiceForm) return;
    return this.paymentInvoiceForm.get("installationPaymentOptions") as FormArray;
  }
  createInstallationPaymentOptions(paymentModel?: UpsellQuotePaymentModel): FormGroup {
    let installationPriceModel = new UpsellQuotePaymentModel(paymentModel);
    let formControls = {};
    Object.keys(installationPriceModel).forEach((key) => {
      formControls[key] = [installationPriceModel[key], []];
      if (key == 'paymentMethodId') {
        formControls[key] = [ {value:installationPriceModel[key]} , [Validators.required]];
      }
      else if (key == 'paymentDate') {
        formControls[key] = [  installationPriceModel[key], [Validators.required]];
      }
      // else if (key == 'shouldDisable' && this.isItemAndOnInvoiceOnly) {
      //   formControls[key] = [{ value: false, disabled: false }, [Validators.required]];
      // }
    });

    return this.formBuilder.group(formControls);
  }
  onvalueChange(){
    this.paymentInvoiceForm.get("paymentDate").valueChanges.subscribe((paymentDate: any) => {
      if(paymentDate){
        paymentDate = (paymentDate && paymentDate!=null)  ? this.momentService.toMoment(paymentDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
        this.checkDate(paymentDate,'paymentDate')
      }
    });
    this.paymentInvoiceForm?.get("startDate")?.valueChanges.subscribe((startDate: any) => {
      if(startDate){
        startDate = (startDate && startDate!=null)  ? this.momentService.toMoment(startDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
        this.checkDate(startDate,'startDate')
      }
    });
  }
  checkDate(paymentDate,type,event?:any){
    if(event=='custom'){
      paymentDate = (paymentDate && paymentDate!=null)  ? this.momentService.toMoment(paymentDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.BILLING,
      BillingModuleApiSuffixModels.VALIDATE_UPSELL_PAYMENT_DATE,
      null,
      false,
      prepareRequiredHttpParams({
        customerId: this.customerId,
        CallInitiationId: this.callInitiationId,
        PaymentDate:paymentDate
      }),1
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
      }else {
        if(type=='paymentDate')
          this.paymentInvoiceForm.get("paymentDate").reset();
        else if(type=='startDate')
          this.paymentInvoiceForm.get("startDate").reset();

      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  //Create FormArray controls
  createTransactionListModel(obj): FormGroup {
    let transactionsModel;
    if (obj) {
      transactionsModel = obj;
    } else {
      transactionsModel = {
        "debitDate": "",
        "amount": 0
      }
    }

    // transactionsModel.invoicePaymentId = this.selectedIdPo[0];
    let formControls = {};
    Object.keys(transactionsModel).forEach((key) => {
      formControls[key] = [{ value: transactionsModel[key], disabled: false }, [Validators.required]]
      // }
    });
    return this._fb.group(formControls);
  }



  //Create FormArray
  get getTransactionsListArray(): FormArray {
    if (!this.paymentInvoiceForm) return;
    return this.paymentInvoiceForm.get("paymentInstallments") as FormArray;
  }

  uploadFiles(files) {
    this.fileName11 = files[0];
    this.file_Name = files[0]['name'];
    this.paymentInvoiceForm.controls.file.setValue(this.file_Name);
  }

  changePaymentMethod() {
    let method = this.paymentInvoiceForm.value.paymentMethodId;
    if (method == 2) {
      this.paymentInvoiceForm = addFormControls(this.paymentInvoiceForm, [{ controlName: "debitOrderRunCodeId" }, { controlName: "description" }, { controlName: "notes" }, { controlName: "customerName" }, { controlName: "paymentOptionId" }, { controlName: "startDate" }]);
      this.paymentInvoiceForm.get('paymentOptionId').setValue(true);
      this.paymentInvoiceForm.get('debitOrderRunCodeId').setValue("");
      if(!this.paymentInvoiceForm?.get('debitOrderRunCodeId')?.value && this.doRunData?.length) {
        this.paymentInvoiceForm?.get('debitOrderRunCodeId')?.setValue(this.doRunData[0]?.id);
      }
      let descAmt = this.acceptedTotal?.toString() ? this.otherService.transformDecimal(this.acceptedTotal) : 'R 0.00';
      let desc = 'Debit Order Payment-Thank you Submitted for total ' + descAmt;
      this.paymentInvoiceForm.get('description').setValue(desc);
     // this.paymentInvoiceForm = removeFormControls(this.paymentInvoiceForm, ["paymentDate", "isPurchaseOrder", "purchaseOrderDate", "purchaseOrderNo"]);
      this.getDebitOrderDetails();
    } else {
      this.paymentInvoiceForm = addFormControls(this.paymentInvoiceForm, [{ controlName: "paymentDate" }, { controlName: "isPurchaseOrder" }, { controlName: "purchaseOrderDate" }, { controlName: "purchaseOrderNo" }]);
      this.paymentInvoiceForm = removeFormControls(this.paymentInvoiceForm, ["debitOrderRunCodeId", "debtorAccountDetailId", "description", "notes", "customerName", "paymentOptionId", "startDate"]);
      this.getInvoiceDetails();
    }
  }
  getDebitOrderDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DO_PAYMENT_UPSELL_QUOTE, undefined, false, prepareRequiredHttpParams({
      customerId: this.customerId,
      directSaleQuotationId: this.details[0].directSaleQuotationId
    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.resources && response.statusCode === 200) {
        this.debitOrderDetails = response.resources;
        this.invoiceDetails = response.resources;
        this.invoiceDetails.debtorDetails.forEach(element => {
          if (element.accountNo) {
            let len = element.accountNo.length - 4;
            let first = element.accountNo.substring(0, len);
            let last = element.accountNo.substring(len, element.accountNo.length);
            let str = ''
            for (let i = 0; i < first.length; i++) {
              str = str + '*';
            }
            element.accountNo = str + '' + last;
          }
        });
        let filter = this.doRunData.filter(x=>x.displayName=='1' || x.displayName==1);
        if(filter?.length > 0)
           this.paymentInvoiceForm.get("debitOrderRunCodeId").setValue(filter[0].id);

        this.onPaymentProccedChange( this.invoiceDetails);
        //  this.paymentInvoiceForm.get("paymentMethodName").setValue(response.resources?.paymentMethodName);
          this.payAtBarcode = this.invoiceDetails?.quotationVersionPayment[0]?.payAtBarcode
          this.payAtQRcode = this.invoiceDetails?.quotationVersionPayment[0]?.payAtQRcode;
          this.invoiceDetails.debtorDetails.forEach(element => {
            if (element.accountNo) {
              let len = element.accountNo.length - 4;
              let first = element.accountNo.substring(0, len);
              let last = element.accountNo.substring(len, element.accountNo.length);
              let str = ''
              for (let i = 0; i < first.length; i++) {
                str = str + '*';
              }
              element.accountNo = str + '' + last;
            }
          });
          response.resources?.quotationVersionPayment.forEach(ele => {
            this.directSaleQuotationVersionIds.push(ele.directSaleQuotationVersionId);
          });
          this.rxjsService.setGlobalLoaderProperty(false);

        // this.onPaymentProccedChange(this.debitOrderDetails);
        // response.resources?.quotationVersionPayment.forEach(ele => {
        //   this.directSaleQuotationVersionIds.push(ele.directSaleQuotationVersionId);
        // });
        // this.paymentInvoiceForm.get("paymentMethodName").setValue(response.resources?.paymentMethodName);
        // this.paymentInvoiceForm.get("mobile1").setValue(this.debitOrderDetails.debtorDetails[0].mobile1)
        // this.paymentInvoiceForm.get("mobile1CountryCode").setValue(this.debitOrderDetails.debtorDetails[0].mobile1CountryCode)
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  changeBankAccount() {
    let id = this.paymentInvoiceForm.value.debtorAccountDetailId;
    let found = this.invoiceDetails.debtorDetails.find(e => e.debtorAccountDetailId == id);
    this.paymentInvoiceForm.get("bankName").setValue(found.bankName);
    this.paymentInvoiceForm.get("accountHolderName").setValue(found.accountHolderName);
    this.paymentInvoiceForm.get("bankBranch").setValue(found.bankBranchCode);
  }

  changePurchaseOrder() {
    let check = this.paymentInvoiceForm.value.isPurchaseOrder;
    if (check) {
      this.paymentInvoiceForm.get("purchaseOrderDate").enable();
      this.paymentInvoiceForm.get("purchaseOrderNo").enable();
      this.paymentInvoiceForm.get("purchaseOrderNo").enable();
      this.paymentInvoiceForm = setRequiredValidator(this.paymentInvoiceForm, ["purchaseOrderDate", "purchaseOrderNo"]);

    } else {
      this.paymentInvoiceForm = clearFormControlValidators(this.paymentInvoiceForm, ["purchaseOrderDate", "purchaseOrderNo"]);
      this.paymentInvoiceForm.get("purchaseOrderDate").disable();
      this.paymentInvoiceForm.get("purchaseOrderNo").disable();
      this.paymentInvoiceForm.get("purchaseOrderDate").setValue(null);
      this.paymentInvoiceForm.get("purchaseOrderNo").setValue(null);
    }

  }

  changePaymentType() {
    if (this.paymentInstallments) {
      this.paymentInstallments.clear();
    }
  }


  onChangeAmount(i) {
    this.getTransactionsListArray.controls[i].get('amount').valueChanges.subscribe((amt: string) => {

      if (amt == '') {
        amt = '0'
        this.getTransactionsListArray.controls[i].get('amount').setErrors({ 'xyz': true });
      } else {
        this.getTransactionsListArray.controls[i].get('amount').setErrors(null);
      }
      if (parseFloat(amt) > this.acceptedTotal) {
        this.getTransactionsListArray.controls[i].get('amount').setErrors({ 'dgdgdgdg': true });
      } else {
        this.getTransactionsListArray.controls[i].get('amount').setErrors(null);

      }
      let remainAmt = this.acceptedTotal - parseFloat(amt);
      let total = 0;

      let noOfPayments;
      for (let j = 0; j < this.paymentInstallments.value.length; j++) {
        if (!this.paymentInstallments.value[j].amount) {
          this.paymentInstallments.value[j].amount = 0;
        }
        if (i == j) {

        } else {
          if (this.paymentInvoiceForm.get('paymentOptionId').value) {
            noOfPayments = this.details.invoices.length;
          } else {
            noOfPayments = this.paymentInvoiceForm.get('noOfPayments').value
          }

          let cal = remainAmt / (noOfPayments - 1);
          this.paymentInstallments.value[j].amount = cal;
          this.getTransactionsListArray.controls[j].get('amount').setValue(cal);

        }

        total = parseFloat(this.paymentInstallments.value[j].amount) + total;
      }

      if (this.paymentInvoiceForm.get('paymentOptionId').value) {

      } else {
        // this.totalAmt = total;
        let descAmt = this.acceptedTotal?.toString() ? this.otherService.transformDecimal(this.acceptedTotal) : 'R 0.00';
        let desc = 'Debit Order Payment-Thank you Submitted for total ' + descAmt;
        this.paymentInvoiceForm.get('description').setValue(desc);
      }

      // }

    });
  }

  assign() {
    let noOfPayments;
    noOfPayments = this.paymentInvoiceForm.value.noOfPayments;

    if (!this.paymentInvoiceForm.value.startDate) {
      this.snackbarService.openSnackbar("Start Date is required", ResponseMessageTypes.ERROR);
      return;
    }

    if (!this.paymentInvoiceForm.value.noOfPayments) {
      this.snackbarService.openSnackbar("No of Payments is required", ResponseMessageTypes.ERROR);
      return;
    }

    let dt: Number;
    if (this.paymentInstallments) {
      this.paymentInstallments.clear();
    }

    this.paymentInstallments = this.getTransactionsListArray;
    let d = new Date(this.paymentInvoiceForm.value.startDate);
    for (var i = 0; i < noOfPayments; i++) {
      // d.setDate(d.getDate() + 30)
      let transactionsModel = {
        "debitDate": null,
        "amount": 0
      }
      transactionsModel['amount'] = this.acceptedTotal / noOfPayments;
      transactionsModel['amount'] = parseFloat(transactionsModel['amount'].toFixed(2));
      // transactionsModel['invoicePaymentId'] = this.details.invoices[this.details.invoices.length - 1].invoicePaymentId;
      // transactionsModel['debitDate'] = d
      transactionsModel['debitDate'] = new Date(d.getFullYear(), d.getMonth() + (i + 1), d.getDate());
      let startDate =  (transactionsModel['debitDate'] && transactionsModel['debitDate'] != null) ? this.momentService.toMoment(transactionsModel['debitDate']).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
      this.checkDate(startDate ,'startDate')
      this.paymentInstallments.insert(i, this.createTransactionListModel(transactionsModel));
    }
  }
  formData = new FormData();
  submitpayment() {
    if(this.paymentInvoiceForm.invalid)
      return;

    this.formData= new FormData();
    // if(this.paymentInvoiceForm.value.isPurchaseOrder)
    //    this.isSubmit=true;

    // if (this.paymentInvoiceForm.invalid) {
    //   return;
    // }

    this.rxjsService.setDialogOpenProperty(true);
    this.rxjsService.setGlobalLoaderProperty(false)
    this.rxjsService.setPopupLoaderProperty(true)
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let obj;
    obj = JSON.parse(JSON.stringify(this.paymentInvoiceForm.value));
    delete obj.paymentInstallments;

    obj.debtorDetails = this.invoiceDetails?.debtorDetails;

    if (this.paymentMethodName == 'Debit Order' ) {
      // if (typeof obj.mobile1 === "string") {
      //   obj.mobile1 = obj.mobile1.split(" ").join("")
      //   obj.mobile1 = obj.mobile1.replace(/"/g, "");
      // }
      // obj.quotationVersionPayment = this.invoiceDetails.quotationVersionPayment
      // obj.debitOrderPaymentInstallments = [
      //   {
      //     "startDate": obj.startDate,
      //     "totalPayableAmount": this.acceptedTotal,
      //     "noOfPayments": obj.paymentOptionId ? 1 : obj.noOfPayments,
      //     "paymentInstallments": obj.paymentOptionId ? null : this.paymentInstallments.value
      //   }
      // ]

      // obj.paymentOptionId = obj.paymentOptionId ? 0 : 1;
      // obj.totalPaymentAmount = this.acceptedTotal;
      // obj.directSaleProcessPaymentInstallationOptions = obj.installationPaymentOptions;
      // delete obj.installationPaymentOptions;
      // delete obj.debitOrderPaymentInstallments;
      // delete obj.paymentDate;
      // this.formData = new FormData();
      // // this.formData.append("file", null, null);
      // this.formData.append("json", JSON.stringify(obj));
      // console.log('JSON.stringify(obj)',JSON.stringify(obj));
      obj.quotationVersionPayment = this.invoiceDetails.quotationVersionPayment
      obj.totalPaymentAmount = this.otherService.transformCurrToNum(obj.totalPaymentAmount);
      obj.directSaleProcessPaymentInstallationOptions =obj.installationPaymentOptions;
      obj.directSaleProcessPaymentInstallationOptions.forEach((element)=>{
        element.paymentDate =  this.momentService.toMoment(element.paymentDate).format('YYYY-MM-DD HH:mm:ss.ms');
      });
       delete obj.installationPaymentOptions;
       delete obj.debitOrderPaymentInstallments;
        delete obj.paymentDate;
      if(this.obj){
        this.formData.append("file", this.obj);
      }
      obj.isCounterSale = false;
      this.formData.append("json", JSON.stringify(obj));

      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DO_PAYMENT_UPSELL_QUOTE, this.formData, 1).subscribe((response) => {
        this.isSubmit=false;
        this.rxjsService.setGlobalLoaderProperty(true);
        if (response.resources && response.isSuccess) {
          this.openPayment = false;
          this.getQuotationVersionPayment();
          this.process = true;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
        this.rxjsService.setPopupLoaderProperty(false)
      })
    } else if(this.paymentMethodName == 'On Invoice/EFT' ){
      obj.quotationVersionPayment = this.invoiceDetails.quotationVersionPayment
      obj.totalPaymentAmount = this.otherService.transformCurrToNum(obj.totalPaymentAmount);
      obj.directSaleProcessPaymentInstallationOptions =obj.installationPaymentOptions;
      obj.directSaleProcessPaymentInstallationOptions.forEach((element)=>{
         element.paymentDate =  this.momentService.toMoment(element.paymentDate).format('YYYY-MM-DD HH:mm:ss.ms');
      });
      delete obj.installationPaymentOptions;
      if(this.obj){
        this.formData.append("file", this.obj);
      }
      obj.isCounterSale = false;
      this.formData.append("json", JSON.stringify(obj));

      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.INVOICE_PAYMENT_UPSELL_QUOTE, this.formData, 1).subscribe((response) => {
        this.isSubmit=false;
        this.rxjsService.setGlobalLoaderProperty(true);
        if (response.isSuccess && response.isSuccess) {
          this.openPayment = false;
          this.getQuotationVersionPayment();
          this.process = true;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
        this.rxjsService.setPopupLoaderProperty(false)
      })
    }

  }
  getQuotationVersionPayment() {
    let numArr=[];
    let formValue;formValue = this.paymentInvoiceForm.value.quotationVersionPayment;
    let quotationVersionPayment;
    quotationVersionPayment = formValue;
    for(let i=0;i<quotationVersionPayment?.length;i++){
      numArr.push(quotationVersionPayment[i].directSaleQuotationNumber);
    }
    this.selectedQuoteString = numArr?.toString();
  }
  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.row = row;

    let otherParams = {};
    if (this.searchForm.value.searchKeyword) {
      otherParams["search"] = this.searchForm.value.searchKeyword;
    }
    if (Object.keys(this.row).length > 0) {
      // logic for split columns and its values to key value pair

      if (this.row['searchColumns']) {
        Object.keys(this.row['searchColumns']).forEach((key) => {
          if (key.toLowerCase().includes('date')) {
            otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
          } else {
            otherParams[key] = this.row['searchColumns'][key];
          }
        });
      }
      if (this.row['sortOrderColumn']) {
        otherParams['sortOrder'] = this.row['sortOrder'];
        otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
      }
    }
    this.getDocumentById(this.row["pageIndex"], this.row["pageSize"], otherParams)

    // this.getDocumentById();

  }



  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.getDocumentById());
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.getDocumentById());
        })
      )
      .subscribe();
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = [];
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }

    if(!deletableIds || deletableIds?.length <=0){
      this.snackbarService.openSnackbar("Please select atleast one document to delete.",ResponseMessageTypes.WARNING);
      return;
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        isdeleted: false,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getDocumentById();
      }
    });
  }


  isLeadItems = false;
  isConfirmQuotation = false;
  submit() {
    let dt = new Date()
    dt.setDate(dt.getDate() + 21);
    let postObj = {
      "directSaleId": this.details.directSaleId,
      "addressId": this.details.addressId,
      "customerId": this.details.customerId,
      "directSaleQuotationId": this.itemData ? this.itemData.directSaleQuotationId : null,
      "directSaleQuotationVersionId": this.itemData ? this.itemData.directSaleQuotationVersionId : null,
      "expiryDate": dt,
      "createdUserId": this.loggedInUserData.userId,
      "isQuoteGenerated": this.details.isQuoteGenerated ? true : false,
      "isConfirmQuotation": this.isConfirmQuotation,
      "isCounterSale": true,
      "directSaleQuotationSystemTypes": this.systemTypesData
    }

    this.rxjsService.setFormChangeDetectionProperty(true);

    if (this.systemTypesData.length > 0) {
      this.systemTypesData.forEach(element => {
        if (element.directSaleQuotationItems.length == 0) {
          this.isLeadItems = true;
        }
      });
    } else {
      this.isLeadItems = true;
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    if (!this.isLeadItems) {
      let crudService: Observable<IApplicationResponse> = this.crudService.create(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.DIRECT_SALES_ITEMS,
        postObj, 1
      );
      crudService
        .pipe(
          tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          })
        )
        .subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(true);
          if (response.isSuccess && response.statusCode === 200) {
            // this.getItemData(response.resources);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else {
      this.snackbarService.openSnackbar("Option can not be empty", ResponseMessageTypes.WARNING);

    }
  }

  // getPaymentHeight() {
  //   return this.paymentInvoiceForm.get('paymentMethodId').value == 1 ? '39vh' : ;
  // }

  exportExcel() { }

}
