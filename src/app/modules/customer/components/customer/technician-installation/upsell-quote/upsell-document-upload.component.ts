import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { selectStaticEagerLoadingDocumentTypesState$ } from '@app/modules/';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, setRequiredValidator } from '@app/shared';
import { CrudService, HttpCancelService, RxjsService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-upsell-document-upload',
  templateUrl: './upsell-document-upload.component.html',
  // styleUrls: ['./document-upload.component.scss']
})
export class UpsellDocumentUploadComponent implements OnInit {
  supportDocumentUploadForm: FormGroup;
  @Output() outputData = new EventEmitter<any>();

  userData: any;
  document_name: any;
  fileName11: any;
  file_Name: any;
  fileId: "";
  departments: any;
  documentTypes: any;
  documentTypeName: any;
  customerId: any;
  addressId = "";
  getData: any;
  documentTypeList: any;
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  formData = new FormData();

  constructor(private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data,
    private httpCancelService: HttpCancelService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.customerId = params.customerId;
      this.fileId = params.fileId;
      this.addressId = params.addressId;
    });
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.combineLatestNgrxStoreData();
  }

  ngOnInit(): void {
    this.rxjsService.setPopupLoaderProperty(true)

    this.createUploadForm();
    if (this.data.documentId) {
      this.getDocumentDetailsByid();
    }

  }



  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(selectStaticEagerLoadingDocumentTypesState$)]
    ).subscribe((response) => {
      this.documentTypeList = response[0];
    });
  }

  createUploadForm() {
    let supportDocumentUploadModel = {
      "documentName": '',
      "customerId": this.data.customerId,
      "directSaleId": this.data.directSaleId,
      "createdUserId": this.userData.userId
    };
    this.supportDocumentUploadForm = this.formBuilder.group({});
    Object.keys(supportDocumentUploadModel).forEach((key) => {
      this.supportDocumentUploadForm.addControl(key, new FormControl(supportDocumentUploadModel[key]));
    });
    this.supportDocumentUploadForm = setRequiredValidator(this.supportDocumentUploadForm, this.fileId ? ["documentTypeId", "fileName"] :
      ["documentName"]);
    this.rxjsService.setPopupLoaderProperty(false)

  }

  getDocumentDetailsByid() {
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.COUNTER_SALES_DOCUMENT_DETAILS,
      undefined,
      false,
      prepareGetRequestHttpParams(null, null,
        { documentId: this.data.documentId })
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setPopupLoaderProperty(false)
      this.rxjsService.setGlobalLoaderProperty(false)
      if (response.isSuccess && response.statusCode == 200) {
        this.getData = response.resources;
        // this.file_Name = response.resources.fileName;
        this.supportDocumentUploadForm.patchValue(response.resources)
      }
    })
  }

  uploadFiles(files) {
    this.fileName11 = files[0];
    this.file_Name = files[0]['name'];
    this.supportDocumentUploadForm.controls.documentName.setValue(this.file_Name);
  }

  onSubmit() {
    if (this.supportDocumentUploadForm.invalid) {
      return;
    }
    this.formData = new FormData();
    this.rxjsService.setPopupLoaderProperty(true);
    if (!this.fileName11) {

    } else {
      this.formData.append("file", this.fileName11, this.fileName11['name']);
    }
    this.formData.append("json", JSON.stringify(this.supportDocumentUploadForm.value));
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (!this.supportDocumentUploadForm.invalid) {
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UPSELL_DOCUMENT_UPLOAD, this.formData)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.rxjsService.setPopupLoaderProperty(false);
            this.dialog.closeAll();
            this.outputData.emit(true);
          }
        })
    } else {
      this.formData = new FormData();
    }
  }

  cancel() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId, 
      customerTab: 5,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

}
