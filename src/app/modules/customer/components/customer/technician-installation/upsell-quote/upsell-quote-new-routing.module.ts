import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UpsellQuoteAcceptQuoteDetailsComponent } from './upsell-quote-accept-quote-details.component';
import { UpsellQuoteAcceptQuoteComponent } from './upsell-quote-accept-quote.component';
import { UpsellQuoteInvoiceQuoteComponent } from './upsell-quote-invoice-quote.component';
import { UpsellQuoteItemInfoComponent } from './upsell-quote-item-info.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


const auditCycleModuleRoutes: Routes = [
    { path: 'item-info', component: UpsellQuoteItemInfoComponent, canActivate:[AuthGuard],data: { title: 'Upsell Quote - Item Info' } },
    { path: 'quote-info', component: UpsellQuoteAcceptQuoteComponent, canActivate:[AuthGuard],data: { title: 'Upsell Quote - Quote Info' } },
    { path: 'details', component: UpsellQuoteAcceptQuoteDetailsComponent, canActivate:[AuthGuard],data: { title: 'Upsell Quote - Details' } },
    { path: 'invoice', component: UpsellQuoteInvoiceQuoteComponent,canActivate:[AuthGuard], data: { title: 'Upsell Quote - invoice' } }
];

@NgModule({
    imports: [RouterModule.forChild(auditCycleModuleRoutes)],

})

export class UpsellRoutingModule { }
