import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AdvanceSearchModule } from '@modules/customer/components/advanced-search/advanced-search.module';
import { UpsellNoSalesPopUpComponent } from './no-sales/upsell-no-sales-pop-up.component';
import { QuoteDeclinePopUpSellComponent } from './quote-decline/quote-decline-pop-up-sell.component';
import { UpsellquotePendingPopUpComponent } from './quote-pending/upsell-quote-pending-pop-up.component';
import { UpdateStockUpsellQuoteModalComponent } from './update-stock-upsell-quote.component';
import { UpsellDocumentUploadComponent } from './upsell-document-upload.component';
import { UpsellQuoteAcceptQuoteDetailsComponent } from './upsell-quote-accept-quote-details.component';
import { UpsellQuoteAcceptQuoteComponent } from './upsell-quote-accept-quote.component';
import { UpsellQuoteInvoiceQuoteComponent } from './upsell-quote-invoice-quote.component';
import { UpsellQuoteItemInfoComponent } from './upsell-quote-item-info.component';
import { UpsellRoutingModule } from './upsell-quote-new-routing.module';


@NgModule({
  declarations: [UpsellQuoteItemInfoComponent, UpsellQuoteInvoiceQuoteComponent, UpsellDocumentUploadComponent, UpsellQuoteAcceptQuoteComponent, QuoteDeclinePopUpSellComponent,
    UpsellquotePendingPopUpComponent, UpsellNoSalesPopUpComponent,UpsellQuoteAcceptQuoteDetailsComponent,UpdateStockUpsellQuoteModalComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    UpsellRoutingModule,
    MaterialModule,
    AdvanceSearchModule
  ],
  entryComponents: [UpsellDocumentUploadComponent, QuoteDeclinePopUpSellComponent, UpsellquotePendingPopUpComponent,UpdateStockUpsellQuoteModalComponent, UpsellNoSalesPopUpComponent]
})
export class UpsellQuoteModule { }
