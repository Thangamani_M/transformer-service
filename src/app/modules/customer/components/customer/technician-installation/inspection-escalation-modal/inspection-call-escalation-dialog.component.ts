import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, RxjsService, setRequiredValidator, ModulesBasedApiSuffix, IApplicationResponse, prepareGetRequestHttpParams } from '@app/shared';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';

@Component({
  selector: 'app-inspection-call-escalation-dialog',
  templateUrl: './inspection-call-escalation-dialog.component.html'
  //   styleUrls: ['./inspection-call-escalation-dialog.component.scss']
})
export class InspectionCallEscalationDialogComponent implements OnInit {

  inspectionEscalationForm: FormGroup;
  anotherInspectionForm: FormGroup;
  serviceCallList = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public popupData: any,
    private dialogRef: MatDialogRef<InspectionCallEscalationDialogComponent>,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    // let messageSpan = document.createElement('span');
    // messageSpan.innerHTML = this.popupData['message'];
    // document.getElementById('parentContainer').appendChild(messageSpan);
    if (this.popupData['type'] == 'appointment') {
      this.createAppointmentForm();
      this.getEscalationDetails();
    } else if (this.popupData['type'] == 'anotherinspection') {
      this.createAnotherInspection();
      this.serviceCallList = this.popupData['pendingInspectionCalls'];
      // if(this.serviceCallList.length > 0) {
        this.anotherInspectionForm.get('serviceCallNumber').setValue(this.serviceCallList?.length ? this.serviceCallList[0].callInitiationId : '');
      // }
    }
  }

  createAppointmentForm() {
    this.inspectionEscalationForm = this.formBuilder.group({});
    this.inspectionEscalationForm.addControl('customerInspectionEscalationId', new FormControl());
    this.inspectionEscalationForm.addControl('customerInspectionId', new FormControl());
    this.inspectionEscalationForm.addControl('preferredDate', new FormControl(null));
    this.inspectionEscalationForm.addControl('comments', new FormControl());
    this.inspectionEscalationForm.addControl('createdUserId', new FormControl());
    this.inspectionEscalationForm = setRequiredValidator(this.inspectionEscalationForm, ['comments']);
  }

  createAnotherInspection() {
    this.anotherInspectionForm = this.formBuilder.group({});
    this.anotherInspectionForm.addControl('serviceCallNumber', new FormControl());
    this.anotherInspectionForm = setRequiredValidator(this.anotherInspectionForm, ['serviceCallNumber']);
  }

  getEscalationDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_ESCALATION, undefined, false, prepareGetRequestHttpParams(null, null,{
        CustomerInspectionId: this.popupData['CustomerInspectionId']
      })).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.hasOwnProperty('resources') && response.resources != undefined) {
          this.inspectionEscalationForm.get('customerInspectionEscalationId').setValue(response.resources.customerInspectionEscalationId);
          this.inspectionEscalationForm.get('comments').setValue(response.resources.comments);
          this.inspectionEscalationForm.get('preferredDate').setValue(response?.resources?.preferredDate ? new Date(response.resources.preferredDate) : null);
        }
      })
  }

  submitEscalation() {
    if(this.inspectionEscalationForm.invalid) {
      this.inspectionEscalationForm.markAllAsTouched();
      return;
    }
    this.inspectionEscalationForm.get('createdUserId').setValue(this.popupData['createdUserId']);
    this.inspectionEscalationForm.get('customerInspectionId').setValue(this.popupData['CustomerInspectionId']);

    this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_ESCALATION, this.inspectionEscalationForm.value).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.hasOwnProperty('resources')) {
         this.dialogRef.close(true);
        }
      })
  }

  navigateToInspectionPage(){
    if(this.anotherInspectionForm.invalid) {
      this.anotherInspectionForm.markAllAsTouched();
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.dialogRef.close(this.anotherInspectionForm.get('serviceCallNumber').value);
  }
}
