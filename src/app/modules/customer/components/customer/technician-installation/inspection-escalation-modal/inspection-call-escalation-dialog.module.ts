import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { InspectionCallEscalationDialogComponent } from './inspection-call-escalation-dialog.component';

@NgModule({
  declarations: [ InspectionCallEscalationDialogComponent ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule
  ],
  exports: [InspectionCallEscalationDialogComponent],
  entryComponents: [InspectionCallEscalationDialogComponent],
})
export class InspectionCallEscalationDialogModule { }
