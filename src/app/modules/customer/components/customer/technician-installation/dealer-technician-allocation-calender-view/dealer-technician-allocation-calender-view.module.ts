import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { MultiSelectModule } from 'primeng/multiselect';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DayViewSchedulerDealerTechAllocationComponent } from './day-view-scheduler-dealer-tech-allocation/day-view-scheduler-dealer-tech-allocation.component';
import { DealerTechnicianAllocationCalenderViewRoutingModule } from './dealer-technician-allocation-calender-view-routing.module';
import { DealerTechnicianAllocationCalenderViewComponent } from './dealer-technician-allocation-calender-view.component';

@NgModule({
  declarations: [DealerTechnicianAllocationCalenderViewComponent, DayViewSchedulerDealerTechAllocationComponent],
  imports: [
    CommonModule,
    DealerTechnicianAllocationCalenderViewRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MultiSelectModule,
    RadioButtonModule,
    ConfirmDialogModule,
    RadioButtonModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
  ]
})
export class DealerTechnicianAllocationCalenderViewModule { }
