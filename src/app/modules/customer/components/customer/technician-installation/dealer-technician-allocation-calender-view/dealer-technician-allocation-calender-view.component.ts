
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { adjustDateFormatAsPerPCalendar, CrudService, CrudType, CURRENT_YEAR_TO_NEXT_FIFTY_YEARS, CustomDirectiveConfig, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { OutOfOfficeModuleApiSuffixModels } from '@modules/out-of-office/enums/out-of-office-employee-request.enum';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { CalendarDayViewBeforeRenderEvent, CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarEventTitleFormatter, CalendarMonthViewBeforeRenderEvent, CalendarView, CalendarWeekViewBeforeRenderEvent } from 'angular-calendar';
import { isSameDay, isSameMonth } from 'date-fns';
import { ConfirmationService } from 'primeng/api';
import { Observable, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { AppointmentBookingModel, BookedSlotsListModel } from '../technician-allocation-calender-view/appintment-booking.model';
import { CustomEventTitleFormatter } from '../technician-allocation-calender-view/custom-event-title-formatter.provider';

enum scheduleProcessType {
  SCHEDULE = 'schedule',
  RESCHEDULE = 'reschedule',
  UNASSIGN = 'unassain',
  UNASSIGN_RESCHEDULE = 'unassing-reschedule',
}

enum actionType {
  CLICKED = 'clicked',
  EDITED = 'edited',
  DRAGGED = 'dragged',
}

@Component({
  selector: 'app-dealer-technician-allocation-calender-view',
  templateUrl: './dealer-technician-allocation-calender-view.component.html',
  providers: [
    ConfirmationService,
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter,
    },
  ],
})
export class DealerTechnicianAllocationCalenderViewComponent implements OnInit {
  refresh: Subject<any> = new Subject();
  isPublicHolidayMsg: boolean = false
  loadingEvents: boolean = false
  modalData: any
  showDialog: boolean
  showUnassignDialog: boolean
  scheduleForm: FormGroup
  searchTechForm: FormGroup
  appointmentFom: FormGroup
  unAssignedFom: FormGroup
  reAppointmentFom: FormGroup
  installationId: any
  nextAppointmentDate: any
  loggedUser: any
  selectedUser: any
  activeDayIsOpen: boolean = false
  appointmentDate: any
  commentsDropDown: any = []
  estimatedTime: any
  appointmentId: any
  appointmentLogId: any

  callType: any
  customerId: any
  showRescheduleDialog: boolean = false
  // reSchedule: boolean = false


  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isHypenOnly = new CustomDirectiveConfig({ isHypenOnly: true });
  isMltiDotsRestrict = new CustomDirectiveConfig({ isMltiDotsRestrict: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  alertMsg: any = ''
  alertDialog: boolean = false
  showConfirmScheduleAlert: boolean = false
  customerAddressId: string
  initialLoad: boolean

  calenderStartHour: any = '0'
  calenderEndHour: any = '23'

  isReschedule: boolean = false
  isAlertCheckbox: boolean = false
  isAlertCheckboxValue: boolean = false
  selectedEventsByAppointmentId: any
  selectedTechnicianId: any

  constructor(private _fb: FormBuilder, private snakbarService: SnackbarService, private momentService: MomentService, private crudService: CrudService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>,) {
    this.installationId = this.activatedRoute.snapshot.queryParams.installationId;
    this.nextAppointmentDate = this.activatedRoute.snapshot.queryParams.nextAppointmentDate;
    this.viewDate = this.nextAppointmentDate ? new Date(this.nextAppointmentDate) : new Date();
    this.estimatedTime = this.activatedRoute.snapshot.queryParams.estimatedTime;
    this.appointmentId = this.activatedRoute.snapshot.queryParams.appointmentId;
    this.appointmentLogId = this.activatedRoute.snapshot.queryParams.appointmentLogId;
    this.callType = this.activatedRoute.snapshot.queryParams.callType;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.customerAddressId = this.activatedRoute.snapshot.queryParams.customerAddressId;


    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

  }

  ngOnInit(): void {
    // this.initialLoad = true
    this.rxjsService.setGlobalLoaderProperty(false)
    this.createScheduleForm()
    this.createSearchForm()
    this.createAppointmentForm()
    this.createReAppointmentForm()
    this.createUnassignedForm()
    this.searchKeywordRequest()
    this.scheduleFormRequest()
    this.setView('day')
    this.getReasonDropdown()
    this.reLoadData()



  }

  getReasonDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_APPOINTMENT_RESCHEDULE_REASON, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.commentsDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  createScheduleForm() {
    this.scheduleForm = this._fb.group({
      callInitiationId: [this.installationId ? this.installationId : ''],
      userId: [this.loggedUser ? this.loggedUser.userId : ''],
      appointmentDate: [this.nextAppointmentDate ? this.nextAppointmentDate : new Date()],
      scheduleType: ['2'],
      // periodType: ['day'],
      estimatedTime: [this.estimatedTime ? this.estimatedTime : '30'],
      estimatedTimeSelected: [this.estimatedTime ? this.estimatedTime : ''],
      search: ['']
    });
    // this.reLoadData()
  }




  createAppointmentForm(appointmentBookingModel?: AppointmentBookingModel) {
    let stockSwapRequestModelcontrol = new AppointmentBookingModel(appointmentBookingModel);
    this.appointmentFom = this._fb.group({
      bookedSlots: this._fb.array([])
    });
    Object.keys(stockSwapRequestModelcontrol).forEach((key) => {
      this.appointmentFom.addControl(key, new FormControl(stockSwapRequestModelcontrol[key]));
    });


    this.appointmentFom.get('callInitiationId').setValue(this.installationId ? this.installationId : '')
    this.appointmentFom.get('createdUserId').setValue(this.loggedUser.userId)
    this.appointmentFom.get('modifiedUserId').setValue(this.loggedUser.userId)

  }

  timeExceedValidationOnchange(selectedFromTime, selectedToTim) {

    // appointmentStartTime and appointmentStartTime between time validation starts here
    let isSameDay = this.momentService.isSameDay(selectedFromTime, selectedToTim)
    if (isSameDay) {
      var selectedToTime = selectedToTim
    } else {
      let selctTimeOnly = this.momentService.getTimeDiff(selectedFromTime, selectedToTim)
      var selectedToTime: any = this.momentService.setTimetoRequieredDate(selectedToTim, selctTimeOnly)
    }
    let add1Minuts = this.momentService.addMinutes(selectedFromTime, 1)
    let reduce1Minuts = this.momentService.reduceMinutes(selectedToTime, 1)

    let selectedFromTime1 = this.momentService.convertNormalToRailayTime(new Date(add1Minuts))
    let selectedToTime1 = this.momentService.convertNormalToRailayTime(new Date(reduce1Minuts))

    // let selectedFromTimeValidation = this.momentService.betWeenTime(this.validationData['appointmentStartTime'], this.validationData['appointmentEndTime'], selectedFromTime1)

    // preferredTimeSlot validation start here
    // if (this.validationData['preferredTimeSlot']) {
    //   if (this.validationData['preferredTimeSlot'].toLowerCase().includes('am')) {
    //     if (this.isAdminRole()) {  // TAM can able to schedule
    //       this.validationData['appointmentEndTime'] = this.validationData['appointmentEndTime'];
    //     } else {
    //       this.validationData['appointmentEndTime'] = '11:59:00';
    //     }
    //     var alertmsge = 'You can able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentStartTime']).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentEndTime']).toLocaleUpperCase()
    //     var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData['appointmentStartTime'], '11:59:00', selectedToTime1)
    //   } else {
    //     var alertmsge = 'You can able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentStartTime']).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentEndTime']).toLocaleUpperCase()
    //     var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData['appointmentStartTime'], this.validationData['appointmentEndTime'], selectedToTime1)
    //   }
    // } else {
    //   var alertmsge = 'You can able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentStartTime']).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentEndTime']).toLocaleUpperCase()
    //   var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData['appointmentStartTime'], this.validationData['appointmentEndTime'], selectedToTime1)
    // }
    // preferredTimeSlot validation ends here

    // if (this.filterForm.get('includeStandbyTechnician').value) { // remove validation if includeStandbyTechnician true
    //   selectedFromTimeValidation = true;
    //   selectedToTimeValidation = true
    // }

    // if (isSameDay && selectedFromTimeValidation && selectedToTimeValidation) {
    //   this.showConfirmScheduleAlert = false
    return Promise.resolve(false);
    // } else {
    //   if (this.isAdminRole()) {
    //     if (isSameDay) {
    //       this.splitSingleAppointment(selectedFromTime, selectedToTime)
    //       this.showConfirmScheduleAlert = false
    //       return Promise.resolve(false);
    //     } else {
    //       this.splitMultipleAppointment(selectedFromTime, selectedToTime)
    //       return Promise.resolve(false);
    //     }
    //   } else {
    //     let isMultipleappointment = this.isMultipleAppointmentCheck(selectedFromTime)
    //     if (!isMultipleappointment) {
    //       this.showConfirmScheduleAlert = false
    //       return Promise.resolve(false);
    //     } else {
    //       if (this.isAdminRole()) {  // TAM can able to schedule
    //         this.showConfirmScheduleAlert = true
    //         return Promise.resolve(false);
    //       } else {
    //         this.snakbarService.openSnackbar(alertmsge, ResponseMessageTypes.WARNING)
    //         this.showConfirmScheduleAlert = false
    //         return Promise.resolve(true);
    //       }
    //     }
    //   }
    // }
  }

  //Create FormArray controls
  createBookedSlotsListModel(bookedSlotsListModel?: BookedSlotsListModel): FormGroup {
    let bookedSlotsListModelControl = new BookedSlotsListModel(bookedSlotsListModel);
    let formControls = {};
    Object.keys(bookedSlotsListModelControl).forEach((key) => {
      // if (key === 'stockSwapId' || key === 'stockSwapItemId') {
      formControls[key] = [{ value: bookedSlotsListModelControl[key], disabled: false }]
      // } else {
      formControls[key] = [{ value: bookedSlotsListModelControl[key], disabled: false }, [Validators.required]]
      // }
    });

    let formBuilder = this._fb.group(formControls)

    formBuilder.get('appointmentTimeFrom').valueChanges.subscribe(val => {
      if (val) {

        let addToMints1 = this.momentService.addMinits(val, this.scheduleForm.get('estimatedTime').value)


        let appointmentTimeTo = new Date(addToMints1)

        let timeExceedvalidate = this.timeExceedValidationOnchange(val, appointmentTimeTo)
        timeExceedvalidate.then(x => {
          if (x) {
            formBuilder.get('appointmentTimeTo').setValue(null)
          } else {

            // let isMultipleappointment = this.isMultipleAppointmentCheck(val)
            // if (!isMultipleappointment) { // false - muliple appoinment
            //   this.setSplitEstimationTimeToAllArray()
            // } else {
            formBuilder.get('appointmentTimeTo').setValue(appointmentTimeTo)
            // }
          }
        })
      }
    })


    return formBuilder;
  }

  //Create FormArray
  get getBookedSlotsListArray(): FormArray {
    if (!this.appointmentFom) return;
    return this.appointmentFom.get("bookedSlots") as FormArray;
  }

  createReAppointmentForm() {
    this.reAppointmentFom = this._fb.group({
      appointmentId: [this.appointmentId ? this.appointmentId : ''],
      // appointmentLogId: [this.appointmentLogId ? this.appointmentLogId : ''],
      appointmentTimeFrom: ["", Validators.required],
      appointmentTimeTo: ['', Validators.required],
      callInitiationId: [this.installationId ? this.installationId : ''],
      estimatedTime: [this.estimatedTime ? this.estimatedTime : ''],
      technicianId: [''],
      // isAppointmentChangable: [''],
      // isSpecialAppointment: [''],
      createdUserId: [this.loggedUser ? this.loggedUser.userId : ''],
      modifiedUserId: [this.loggedUser ? this.loggedUser.userId : ''],
      appointmentDate: [this.nextAppointmentDate ? this.nextAppointmentDate : new Date()],
    });
  }

  createUnassignedForm() {
    this.unAssignedFom = this._fb.group({
      appointmentId: [this.appointmentId ? this.appointmentId : null],
      appointmentLogId: [this.appointmentLogId ? this.appointmentLogId : null],
      appointmentDate: [this.appointmentDate ? this.appointmentDate : new Date()],
      estimatedTime: [this.estimatedTime ? this.estimatedTime : null],
      comments: ["", Validators.required],
      appointmentTimeFrom: ["", Validators.required],
      appointmentTimeTo: ['', Validators.required],
      // isAppointmentChangable: [null],
      // isFixedAppointment: [null],
      // isSpecialAppointment: [null],
      // isOverCapacity: [null],
      isReschedule: [null],
      isConfirm: [null],
      modifiedUserId: [this.loggedUser ? this.loggedUser.userId : ''],
    });
  }



  createSearchForm() {
    this.searchTechForm = this._fb.group({
      search: [''],
    });
  }

  scheduleFormRequest() {
    this.scheduleForm.valueChanges
      .pipe(

        switchMap(obj => {

          return this.reLoadData()
        })
      )
      .subscribe();
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        // this.addConfirm = true;
        // this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        break;
      case CrudType.FILTER:
        break;
      default:
    }
  }










  // appinement booking calender view starts
  view: CalendarView = CalendarView.Week;

  CalendarView = CalendarView;
  yesterday = this.momentService.getYesterday()
  minDate: Date = new Date(this.yesterday);


  setView(view) {
    this.view = view;
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }




  async onHoursSegmentClick(event) {
    var daytime = event.date
    this.selectedTechnicianId = event?.user?.technicianId
    let isAfter = this.momentService.isAfter(daytime)
    if (!isAfter) {
      this.snakbarService.openSnackbar('You can not schedule for previous day/time ', ResponseMessageTypes.WARNING)
      return
    }

    this.viewDate = new Date(daytime)

    this.modalData = { daytime };

    let checkAvail = await this.checkTechnicianAvailability(daytime)
    if (!checkAvail) {
      return
    }

    if (this.appointmentId && (!this.appointmentLogId || this.isReschedule)) {
      let finalChek = await this.onReSelectTime(daytime)
      this.showRescheduleDialog = true
    } else {
      let selectedEvent: any = this.selectedEventsByAppointmentId
      let isSameDay = selectedEvent ? this.momentService.isSameDay(selectedEvent.start, daytime) : true
      let isTechnicianSame = selectedEvent ? (event.user.technicianId == selectedEvent.meta.user.id ? true : false) : true

      if (isSameDay && isTechnicianSame) {
        let finalChek = await this.onSelectTime(daytime)

        this.showDialog = true
      } else {

        let selectedEvent: any = this.selectedEventsByAppointmentId

        let unassignEstimationTime: any = this.momentService.getTimeDiff(selectedEvent.start, selectedEvent.end)
        this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

        let finalChek = await this.onSelectTimeUnassign(daytime)

        this.showUnassignDialog = true
        this.appointmentDate = daytime
        // selectedEvent.start = daytime;

        // for end time
        let addToMints1 = this.momentService.addMinits(daytime, this.scheduleForm.get('estimatedTime').value)
        // selectedEvent.end = new Date(addToMints1);
        //for end time

        // this.events = [...this.events];
        this.unAssignedFom.get('appointmentId').setValue(selectedEvent.appointmentId)
        this.unAssignedFom.get('appointmentLogId').setValue(selectedEvent.appointmentLogId)
        this.unAssignedFom.get('appointmentDate').setValue(new Date(daytime))
        this.unAssignedFom.get('appointmentTimeFrom').setValue(daytime)
        this.unAssignedFom.get('appointmentTimeTo').setValue(new Date(addToMints1))
        this.unAssignedFom.get('isReschedule').setValue(true)
      }


    }

    this.appointmentDate = daytime
    // this.appointmentFom.get('appointmentLogId').setValue(null)

  }


  async handleEvent(action: string, event: any) {
    this.viewDate = new Date(event.start)

    this.modalData = { event };

    if (!event.isAppointmentChangable) {
      if (event.lable) {

        let unassignEstimationTime: any = this.momentService.getTimeDiff(event.start, event.end)
        this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

        let finalChek = await this.onSelectTimeUnassign(event.start)

        this.showUnassignDialog = true
        this.appointmentDate = event.start
        this.unAssignedFom.get('appointmentId').setValue(event.appointmentId)
        this.unAssignedFom.get('appointmentLogId').setValue(event.appointmentLogId)
        this.unAssignedFom.get('appointmentDate').setValue(event.start)
        this.unAssignedFom.get('isReschedule').setValue(false)

      }
      return
    }
    if (event.lable && event.isAppointmentChangable) {
      if (action === 'Clicked') {
        // this.onSelectTime(event.start)
        let finalChek = await this.onSelectTime(event.start)
        if (finalChek) {
          return
        }
        // this.appointmentFom.get('appointmentLogId').setValue(event.appointmentLogId)
        // this.showDialog = true
        this.appointmentDate = event.start

      } else if (action === 'Edited') {

        let unassignEstimationTime: any = this.momentService.getTimeDiff(event.start, event.end)
        this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

        let finalChek = await this.onSelectTimeUnassign(event.start)
        if (finalChek) {
          return
        }
        this.showUnassignDialog = true
        this.appointmentDate = event.start
        this.unAssignedFom.get('appointmentId').setValue(event.appointmentId)
        this.unAssignedFom.get('appointmentLogId').setValue(event.appointmentLogId)
        this.unAssignedFom.get('appointmentDate').setValue(new Date(event.start))
        this.unAssignedFom.get('isReschedule').setValue(false)

      }

    }
  }

  async handleEventDragged(event, daytime, endTime) {
    if (event.isProvisionalBooking) {
      //   this.snakbarService.openSnackbar('You can not Re-schedule without approved ', ResponseMessageTypes.WARNING)
      return
    }
    let isAfter = this.momentService.isAfter(daytime)
    if (!isAfter) {
      this.snakbarService.openSnackbar('You can not schedule for previous day/time ', ResponseMessageTypes.WARNING)
      return
    }
    this.viewDate = new Date(daytime)


    this.modalData = { event };

    let checkAvail = await this.checkTechnicianAvailability(daytime)
    if (!checkAvail) {
      return
    }

    if (!event.isAppointmentChangable) {
      if (event.lable) {

        let isSameDay = this.momentService.isSameDay(event.start, daytime)

        if (isSameDay) {
          // this.onSelectTime(event.start)
          let finalChek = await this.onSelectTime(daytime)
          if (finalChek) {
            return
          }
          // this.appointmentFom.get('appointmentLogId').setValue(event.appointmentLogId)
          // this.showDialog = true
          this.appointmentDate = daytime
          event.start = daytime;
          event.end = endTime;
          this.events = [...this.events];
          this.onAppointment()

        } else {

          let unassignEstimationTime: any = this.momentService.getTimeDiff(event.start, event.end)
          this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

          let finalChek = await this.onSelectTimeUnassign(event.start)
          if (finalChek) {
            return
          }
          this.showUnassignDialog = true
          this.appointmentDate = daytime
          event.start = daytime;
          event.end = endTime;
          this.events = [...this.events];
          this.unAssignedFom.get('appointmentId').setValue(event.appointmentId)
          this.unAssignedFom.get('appointmentLogId').setValue(event.appointmentLogId)
          this.unAssignedFom.get('appointmentDate').setValue(event.start)
          this.unAssignedFom.get('appointmentTimeFrom').setValue(event.start)
          this.unAssignedFom.get('appointmentTimeTo').setValue(event.end)
          this.unAssignedFom.get('isReschedule').setValue(true)
        }

      }
      return
    }
    if (event.lable && event.isAppointmentChangable) {

      let isSameDay = this.momentService.isSameDay(event.start, daytime)
      let isTechnicianSame = this.selectedEventsByAppointmentId ? (event.meta.user.id == this.selectedEventsByAppointmentId.meta.user.id ? true : false) : true

      if (isSameDay && isTechnicianSame) {
        // this.onSelectTime(event.start)
        let finalChek = await this.onSelectTime(daytime)

        // this.appointmentFom.get('appointmentLogId').setValue(event.appointmentLogId)
        // this.showDialog = true
        this.appointmentDate = daytime
        event.start = daytime;
        event.end = endTime;
        this.events = [...this.events];
        this.onAppointment()

      } else {

        let unassignEstimationTime: any = this.momentService.getTimeDiff(event.start, event.end)
        this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

        let finalChek = await this.onSelectTimeUnassign(daytime)
        if (finalChek) {
          return
        }
        this.showUnassignDialog = true
        this.appointmentDate = daytime
        event.start = daytime;
        event.end = endTime;
        this.events = [...this.events];
        this.unAssignedFom.get('appointmentId').setValue(event.appointmentId)
        this.unAssignedFom.get('appointmentLogId').setValue(event.appointmentLogId)
        this.unAssignedFom.get('appointmentDate').setValue(new Date(event.start))
        this.unAssignedFom.get('appointmentTimeFrom').setValue(event.start)
        this.unAssignedFom.get('appointmentTimeTo').setValue(event.end)
        this.unAssignedFom.get('isReschedule').setValue(true)

      }

    }
  }




  splitSingleAppointment(startDate, endDate) {
    let bookedSlotsModelData: BookedSlotsListModel = {
      appointmentDate: startDate,
      minAppointmentDate: startDate,
      appointmentTimeFrom: startDate,
      appointmentTimeTo: endDate,
      estimatedTime: this.estimatedTime
    }
    this.getBookedSlotsListArray.clear()
    this.getBookedSlotsListArray.push(this.createBookedSlotsListModel(bookedSlotsModelData));
  }


  async onSelectTime(event) {
    // let addFromMints = this.momentService.toHoursMints24(event)
    // this.appointmentFom.get('appointmentTimeFrom').setValue(event)
    // let addToMints = this.momentService.toHoursMints12(event)
    let addToMints1 = this.momentService.addMinits(event, this.scheduleForm.get('estimatedTime').value)
    // this.appointmentFom.get('appointmentTimeTo').setValue(new Date(addToMints1))

    let appointmentTimeFrom = event
    let appointmentTimeTo = new Date(addToMints1)
    // let bookedSlotsModelData: BookedSlotsListModel = {
    //   appointmentDate: event,
    //   appointmentTimeFrom: event,
    //   appointmentTimeTo: appointmentTimeTo,
    //   estimatedTime: this.estimatedTime
    // }
    // this.getBookedSlotsListArray.clear()
    // this.getBookedSlotsListArray.push(this.createBookedSlotsListModel(bookedSlotsModelData));
    this.splitSingleAppointment(appointmentTimeFrom, appointmentTimeTo)

    return Promise.resolve(false);

  }

  async onReSelectTime(event) {
    // let addFromMints = this.momentService.toHoursMints24(event)
    this.reAppointmentFom.get('appointmentTimeFrom').setValue(event)

    // let addToMints = this.momentService.toHoursMints12(event)
    let addToMints1 = this.momentService.addMinits(event, this.scheduleForm.get('estimatedTime').value)
    this.reAppointmentFom.get('appointmentTimeTo').setValue(new Date(addToMints1))

    return Promise.resolve(false);


  }

  async onSelectTimeUnassign(event, val?) {
    // let addFromMints = this.momentService.toHoursMints24(event)
    this.unAssignedFom.get('appointmentTimeFrom').setValue(event)

    // let addToMints = this.momentService.toHoursMints12(event)
    let addToMints1 = this.momentService.addMinits(event, this.scheduleForm.get('estimatedTimeSelected').value)
    this.unAssignedFom.get('appointmentTimeTo').setValue(new Date(addToMints1))

    return Promise.resolve(false);

  }


  onAppointment() {
    if (this.appointmentFom.invalid) {
      return
    }
    this.showDialog = false

    this.doAppintment()


  }


  onUnassigne() {
    if (this.unAssignedFom.invalid) {
      return
    }
    this.showUnassignDialog = false
    let formValue = this.unAssignedFom.value
    formValue.appointmentId = this.appointmentId
    formValue.appointmentLogId = this.appointmentLogId
    formValue.technicianId = this.selectedTechnicianId
    // formValue.appointmentDate = this.appointmentDate.toLocaleString()
    formValue.appointmentDate = this.momentService.toFormateType(formValue.appointmentDate, 'YYYY-MM-D')
    formValue.appointmentTimeFrom = this.momentService.toHoursMints24(formValue.appointmentTimeFrom)
    formValue.appointmentTimeTo = this.momentService.toHoursMints24(formValue.appointmentTimeTo)
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.DEALER_APPOINTMENT_UNASSIGN_RESCHEDULE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      this.showCommonAlerAfterResponse(response)

      if (response.isSuccess) {
        // this.appointmentLogId = null;
        this.unAssignedFom.reset()
        this.unAssignedFom.get('modifiedUserId').setValue(this.loggedUser ? this.loggedUser.userId : '')
      }
    })

  }

  onReAppointment() {
    if (this.reAppointmentFom.invalid) {
      return
    }
    this.showRescheduleDialog = false
    this.doReSchedule()

  }



  // appinement booking calender view ends


  // tech allocation calender view starts

  viewDate = new Date();

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-pencil"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      },
    },
    // {
    //   label: '<i class="fa fa-trash"></i>',
    //   a11yLabel: 'Delete',
    //   onClick: ({ event }: { event: CalendarEvent }): void => {
    //     this.events = this.events.filter((iEvent) => iEvent !== event);
    //     this.handleEvent('Deleted', event);
    //   },
    // },
  ];

  // users = users;
  users = [];

  colors: any = {
    red: {
      primary: '#ad2121',
      secondary: '#FAE3E3',
    },
    blue: {
      primary: '#1e90ff',
      secondary: '#D1E8FF',
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA',
    },
    orange: {
      primary: '#ffa500',
      secondary: '#fbe2b2',
    },
    green: {
      primary: '#006A54',
      secondary: '#e8fde7',
    },
  };

  events: CalendarEvent[] = []

  beforeMonthViewRender(renderEvent: CalendarMonthViewBeforeRenderEvent): void {
    renderEvent.body.forEach((day) => {
      const dayOfMonth = day.date.getDate();
      // if (dayOfMonth > 5 && dayOfMonth < 10 && day.inMonth) {
      // day.cssClass = 'calendar-bg-red';
      // }
    });
  }

  beforeWeekViewRender(renderEvent: CalendarWeekViewBeforeRenderEvent) {
    renderEvent.hourColumns.forEach((hourColumn, index) => {

      hourColumn.hours.forEach((hour) => {
        hour.segments.forEach((segment) => {

          var dayIndex = hourColumn.events.length > 0 ? (hourColumn.events[0]?.event['isPublicHoliday'] ? index : null) : null

          if (
            // segment.date.getHours() >= 9 &&
            // segment.date.getHours() <= 12 &&
            segment.date.getDay() === 0 ||
            segment.date.getDay() === 6 ||
            segment.date.getDay() === dayIndex
          ) {
            segment.cssClass = 'calendar-bg-red';
          }
        });
      });
    });
  }

  beforeDayViewRender(renderEvent: CalendarDayViewBeforeRenderEvent) {
    renderEvent.hourColumns.forEach((hourColumn, index) => {
      hourColumn.hours.forEach((hour) => {
        hour.segments.forEach((segment) => {
          var dayIndex = hourColumn.events.length > 0 ? (hourColumn?.events[0]?.event['isPublicHoliday'] ? index : null) : null
          if (
            // segment.date.getHours() >= 9 &&
            // segment.date.getHours() <= 12 &&
            segment.date.getDay() === 0 ||
            segment.date.getDay() === 6 ||
            segment.date.getDay() === dayIndex
          ) {
            segment.cssClass = 'calendar-bg-red';
          }
        });
      });
    });
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {

    if (!newStart || !newEnd) {
      return
    }
    // if (newStart < this.minDate) {
    //   return
    // }
    let isAfter = this.momentService.isAfter(newStart)
    if (!isAfter) {
      this.snakbarService.openSnackbar('You can not schedule for previous day/time ', ResponseMessageTypes.WARNING)
      return
    }
    let isSameDay = this.momentService.isSameDayTime(event.start, newStart)
    if (isSameDay) {
      return
    }
    // event.start = newStart;
    // event.end = newEnd;
    // this.events = [...this.events];
    // this.refresh.next();

    // this.onHoursSegmentDragged(event, newStart, newEnd)
    this.handleEventDragged(event, newStart, newEnd)
  }

  userChanged({ event, newUser }) {
    event.color = newUser.color;
    event.meta.user = newUser;
    this.events = [...this.events];
  }

  userSelected(user) {
    if (user.isSelectable)
      this.selectedUser = user
  }

  onDayChange() {

    this.scheduleForm.get('appointmentDate').setValue(this.viewDate)

  }


  convertData(date) {// Fri Feb 20 2015 19:29:31 GMT+0530 (India Standard Time)
    var isoDate = new Date(date.getTime() - date.getTimezoneOffset() * 60000).toISOString();
    //OUTPUT : 2015-02-20T19:29:31.238Z
    return isoDate
  }

  searchKeywordRequest() {
    this.searchTechForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          this.scheduleForm.get('search').setValue(val.search)
          return of(this.reLoadData())
        })
      )
      .subscribe();
  }

  getTechniciansList(otherParams?: object) {
    if (Object.keys(otherParams).length > 0) {
      // logic for split columns and its values to key value pair

      Object.keys(otherParams).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          otherParams[key] = this.momentService.localToUTCDateTime(otherParams[key])
          // otherParams[key] =  this.convertData(otherParams[key])


          // otherParams[key] = this.momentService.localToUTC(otherParams[key]);
        } else {
          otherParams[key] = otherParams[key];
        }
      });
    }
    // this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.loadingEvents = true
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.DEALER_APPOINTMENT_TECHNICIAN_CALENDAR, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);

        // this.initialLoad = false
        this.loadingEvents = false
        if (response.resources) {
          this.isReschedule = response.resources.showRescheduleDialog
          this.selectedUser = null
          this.events = []
          this.users = []
          // if (this.isReschedule) {
          this.appointmentId = this.appointmentId ? this.appointmentId : response.resources.appointmentId
          // }
          // this.appointmentLogId = this.appointmentLogId ? this.appointmentLogId : response.resources.appointmentLogId


          response.resources.rows.forEach(element => {
            element.appointmentId = response.resources.appointmentId
            element.id = element.technicianId,
              element.name = element.technicianName,
              // element.title = response.resources.serviceCallNumber,
              // element.start = element.appointmentDate ? new Date(element.appointmentDate) : null,
              // element.end = element.appointmentDate ? new Date(element.appointmentDate) : null
              element.meta = {
                user: {
                  id: element.technicianId,
                  name: element.technicianName
                }
              }
            if (element.techAllocations.length > 0) {
              element.techAllocations.forEach(element1 => {
                let fromHour = element1.appointmentTimeFrom ? element1.appointmentTimeFrom.split(':') : null
                let toHour = element1.appointmentTimeTo ? element1.appointmentTimeTo.split(':') : null
                let existAppoitmnt = {
                  id: element1.appointmentId,
                  name: null,
                  lable: element1.serviceCallNumber,
                  title: ' <span title="' + element1.hoverText + '">' + element1.serviceCallNumber + ' </span> ',
                  start: element1.appointmentTimeFrom ? new Date(this.momentService.addHoursMinits(element1.appointmentDate, fromHour[0], fromHour[1])) : null,
                  end: element1.appointmentTimeTo ? new Date(this.momentService.addHoursMinits(element1.appointmentDate, toHour[0], toHour[1])) : null,
                  // start: new Date(this.momentService.addHoursMinits(element1.appointmentDate, "10", "0")),
                  // end:  new Date(this.momentService.addHoursMinits(element1.appointmentDate, '10', "30")),
                  color: element1.cssClass === 'status-label-blue' ? this.colors.blue : element1.cssClass === 'status-label-red' ? this.colors.red : element1.cssClass === 'status-label-orange' ? this.colors.orange : null,
                  meta: {
                    user: {
                      id: element.technicianId,
                      name: element.technicianName
                    }
                  }
                }
                this.events.push(existAppoitmnt)
              })
            }
          });
          this.users = response.resources.rows

          // if (!this.selectedEventsByAppointmentId) {
          //   this.selectedEventsByAppointmentId = this.appointmentId ? this.events.find(x => x.id == this.appointmentId) : null
          // }
          if (this.appointmentId) {
            let fromHour = response.resources.appointmentTimeFrom ? response.resources.appointmentTimeFrom.split(':') : null
            let toHour = response.resources.appointmentTimeTo ? response.resources.appointmentTimeTo.split(':') : null
            this.selectedEventsByAppointmentId = {
              appointmentId: response.resources.appointmentId ? response.resources.appointmentId : null,
              appointmentLogId: response.resources.appointmentLogId ? response.resources.appointmentLogId : null,
              start: response.resources.appointmentTimeFrom ? new Date(this.momentService.addHoursMinits(response.resources.appointmentDate, fromHour[0], fromHour[1])) : null,
              end: response.resources.appointmentTimeTo ? new Date(this.momentService.addHoursMinits(response.resources.appointmentDate, toHour[0], toHour[1])) : null,
              meta: { user: { id: response.resources.technicianId } }
            }
          }

          setTimeout(() => {
            if ($(".cal-time-events-wrapper")?.length == 0) {
              $('.cal-time-events').wrapAll('<div class="cal-time-events-wrapper"></div>');
            }
            // $('.cal-time-events').wrapAll('<div class="cal-time-events-wrapper"></div>');

            // if (this.validationData.appointmentStartTime == '09:00:00') {
            $('.cal-time-events-wrapper').animate({
              scrollTop: 540
            }, 1000, function () {
            });


            // }
          }, 1000);


        }
      });
  } 

  onDaySelect() {
    this.scheduleForm.get('appointmentDate').setValue(this.viewDate)

  }


  navigateToPage() {
    this.router.navigate(['/customer', 'manage-customers', 'dealer-call'], {
      queryParams: {
        // customerId: this.installationId
        customerId: this.customerId,
        customerAddressId: this.customerAddressId,
        appointmentId: this.appointmentId ? this.appointmentId : null,
        initiationId: this.installationId,
        callType: this.callType // 2- service call, 1- installation call

      }, skipLocationChange: true
    });
  }



  doReSchedule() {
    let formValue = this.reAppointmentFom.value
    formValue.appointmentId = this.appointmentId ? this.appointmentId : null
    // formValue.appointmentDate = this.appointmentDate.toLocaleString()
    formValue.appointmentDate = this.momentService.toFormateType(this.viewDate, 'YYYY-MM-D')
    formValue.appointmentTimeFrom = this.momentService.toHoursMints24(formValue.appointmentTimeFrom)
    formValue.appointmentTimeTo = this.momentService.toHoursMints24(formValue.appointmentTimeTo)
    formValue.technicianId = this.selectedTechnicianId

    // formValue.isAppointmentChangable = formValue.isAppointmentChangable ? formValue.isAppointmentChangable : formValue.technicianId ? false : true,
    // formValue.isReschedule = formValue.isReschedule ? true : false,
    // formValue.isSpecialAppointment = '',

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.DEALER_APPOINTMENT_RESCHEDULE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      this.showCommonAlerAfterResponse(response)
    })
  }

  doAppintment() {
    let formValue = this.appointmentFom.value
    // formValue.preferredDate = this.momentService.toFormateType(formValue.preferredDate, 'YYYY-MM-DD')
    formValue.appointmentId = this.appointmentId ? this.appointmentId : null
    formValue.technicianId = this.selectedTechnicianId
    // formValue.isAppointmentChangable = formValue.isAppointmentChangable ? formValue.isAppointmentChangable : formValue.technicianId ? false : true,
    // this.viewDate = this.appointmentFom.value.bookedSlots.lentht > 0 ? this.appointmentFom.value.bookedSlots[0].appointmentDate : this.viewDate

    formValue.bookedSlots.forEach(element => {
      // element.appointmentLogId = formValue.bookedSlots.lenth > 1 ? this.appointmentLogId : null
      element.appointmentLogId = this.appointmentLogId ? this.appointmentLogId : null
      element.appointmentDate = this.momentService.toFormateType(element.appointmentDate, 'YYYY-MM-DD')
      element.appointmentTimeFrom = this.momentService.toHoursMints24(element.appointmentTimeFrom)
      element.appointmentTimeTo = this.momentService.toHoursMints24(element.appointmentTimeTo)
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    let crudService: Observable<IApplicationResponse> = (!this.appointmentLogId) ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.DEALER_APPOINTMENT, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.DEALER_APPOINTMENT, formValue)
    crudService.subscribe((response: IApplicationResponse) => {

      this.showCommonAlerAfterResponse(response)

    })
  }



  checkTechnicianAvailability(selectedDateTime) {
    return new Promise((resolve, reject) => {
      let filteredData = Object.assign({},
        { callInitiationId: this.installationId },
        { estimatedTime: this.estimatedTime },
        { technicianId: this.selectedTechnicianId },
        { appointmentId: this.appointmentId },
        { appointmentDate: this.momentService.toFormateType(selectedDateTime, 'YYYY-MM-DD HH:mm') },
      );
      let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => ((v == null || v == '') ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.DEALER_APPOINTMENT_VALIDATE_TECHNICIAN_CALENDAR, null, false,
        prepareGetRequestHttpParams(null, null, filterdNewData))
        .subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.isSuccess) {
            if (response.resources.responseMessage) {
              this.snakbarService.openSnackbar(response.resources.alertMessage ? response.resources.alertMessage : 'There is no available dates with suitable Technician for your Call', ResponseMessageTypes.WARNING)
              this.reLoadData()
              resolve(false);
            } else {
              resolve(true);
            }
          } else {
            resolve(false);
          }
        });

    });
  }

  openAlertDialog(msg) {
    this.alertDialog = true
    this.alertMsg = msg
  }

  reLoadData() {

    // this.scheduleForm.get('appointmentDate').setValue(this.viewDate)
    let scheduledData = this.scheduleForm.value
    scheduledData.appointmentDate = this.viewDate
    Object.keys(scheduledData).forEach(key => {
      if (scheduledData[key] === "") {
        delete scheduledData[key]
      }
    });
    let filterdNewData1 = Object.entries(scheduledData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

    return of(this.getTechniciansList(filterdNewData1));

  }

  getYearRange() {
    return CURRENT_YEAR_TO_NEXT_FIFTY_YEARS;
  }

  getDateFormat() {
    return adjustDateFormatAsPerPCalendar();
  }

  showCommonAlerAfterResponse(response) {
    if (response.isSuccess) {
      // if (this.callType == '2') {
      // this.reSchedule = false
      if (response.resources) {
        // let jsonStri = JSON.parse(response.resources)
        let jsonStri = response.resources
        this.appointmentId = jsonStri.appointmentId
        this.appointmentLogId = jsonStri.appointmentLogId
        if (jsonStri.responseMessage) {
          this.isAlertCheckbox = true
          this.openAlertDialog(jsonStri.responseMessage)
        }
      }
      // }
      this.reLoadData()
    } else if (!response.isSuccess && response.statusCode == 409) {
      // let jsonStri = JSON.parse(response.resources)
      let jsonStri = response.resources
      this.appointmentId = jsonStri.appointmentId
      this.appointmentLogId = jsonStri.appointmentLogId
      if (jsonStri.responseMessage) {
        this.isAlertCheckbox = false
        this.openAlertDialog(jsonStri.responseMessage)
      }
      this.reLoadData()

    }
  }


}

