import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DealerTechnicianAllocationCalenderViewComponent } from './dealer-technician-allocation-calender-view.component';

const routes: Routes = [ 
      { path:'', component: DealerTechnicianAllocationCalenderViewComponent, data: { title: 'Dealer Tech Allocation Calender View' }},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class DealerTechnicianAllocationCalenderViewRoutingModule { }
