import { Component, OnInit } from '@angular/core';
import { OtherService } from '@app/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-pr-amount-details-dialog',
  templateUrl: './pr-amount-details-dialog.component.html',
})
export class PrAmountDetailsDialogComponent implements OnInit {

  totalDiffFormat: any;

  constructor(public config: DynamicDialogConfig, public ref: DynamicDialogRef, private otherService: OtherService,) { }

  ngOnInit(): void {
    this.getItemTotalDiff();
  }

  close() {
    this.ref.close();
  }

  getItemTotalDiff() {
    var totalDiff = 0
    this.config?.data?.prItemDetails?.forEach((equipmentListModel: any) => {
      totalDiff += Number(equipmentListModel?.exclVAT) * Number(equipmentListModel?.qtyRequired);

    })
    this.totalDiffFormat = this.otherService.transformDecimal(totalDiff);
  }
}
