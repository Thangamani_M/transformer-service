import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { PrAmountDetailsDialogComponent } from './pr-amount-details-dialog.component';

@NgModule({
  declarations: [  PrAmountDetailsDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule
  ],
  exports: [PrAmountDetailsDialogComponent],
  entryComponents: [PrAmountDetailsDialogComponent],
})
export class PRCallAmountDialogModule { }
