import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, convertTreeToList, CrudService, currentComponentPageBasedPermissionsSelector$, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CUSTOMER_COMPONENT } from '@modules/customer';
import { CallInitiationReferToSaleModel } from '@modules/customer/models/inititation-call-refer-to-sale.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';

@Component({
  selector: 'app-refer-to-sale',
  templateUrl: './refer-to-sale.component.html',
  styleUrls: ['./refer-to-sale.component.scss']
})
export class ReferToSaleComponent implements OnInit {

  userData: UserLogin;
  customerId: string;
  initiationId: string;
  installationId: string;
  isSubmitted: any = false;
  // customerId: string;
  quotationVersionId: string;
  callInitiationId: string;
  callType: string;
  referToSaleDetails: any = {}
  referToSaleForm: FormGroup;
  investigateDialogform: FormGroup;
  feedbackList: any = [];
  flagList: any = [];
  referToSaleFlagType: any = '';
  countryCodes = [{ displayName: '+27' }, { displayName: '+91' }, { displayName: '+45' }];
  formConfigs = formConfigs;
  iscompleteddisabled: any = false;
  @ViewChild('input', { static: false }) row;
  @ViewChild('Feedback', { static: false }) feedbackElement: ElementRef;
  isInvestigation: boolean = false;
  isStatusBlank = false;
  appointmentId: any = null;
  referToSalesFlagListArray = [];
  addressId;
  referToSalesCompleteDialog: boolean = false;
  investigateDialog: boolean = false;
  investigateIndex;
  investigateDialogformErrMsg;
  investigateDialogformIsSubmit: boolean = false;
  actionPermissionObj: any;

  constructor(private rxjsService: RxjsService, private snackbarService: SnackbarService, private momentService: MomentService, private dialogService: DialogService,
    private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private formBuilder: FormBuilder, private crudService: CrudService, private datePipe: DatePipe) {
    this.installationId = this.activatedRoute.snapshot.queryParams.installationId;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.quotationVersionId = this.activatedRoute.snapshot.queryParams.quotationVersionId;
    this.callInitiationId = this.activatedRoute.snapshot.queryParams.callInitiationId;
    this.callType = this.activatedRoute.snapshot.queryParams.callType;
    this.addressId = this.activatedRoute.snapshot.queryParams.customerAddressId;
    this.appointmentId = this.activatedRoute.snapshot.queryParams.appointmentId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    // this.getReferToSalesDetailById();
    this.combineLatestNgrxStoreData();
    this.isAcknowledge = false;
    this.createOnHoldCreationFrom()
    let dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        TechnicalMgntModuleApiSuffixModels.UX_REFERTOSALES_FLAG_TYPE),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_REFERTO_SALES_FEEDBACK),
      this.crudService.get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.REFER_TO_SALES, undefined, null, prepareGetRequestHttpParams(null, null,
          {
            CustomerId: this.customerId,
            appointmentId: this.appointmentId,
            createdUserId: this.userData.userId
          }
        )),
    ];
    this.loadActionTypes(dropdownsAndData);
    if (this.referToSalesFlagListFormArray.length == 1) {

      if (!this.referToSalesFlagListFormArray.value[0].referToSalesFlagTypeId) {
        this.referToSalesFlagListFormArray.removeAt(0);
      }

    }
    this.formControlChange();
    this.investigateDialogs();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][CUSTOMER_COMPONENT.CUSTOMER]?.find(el => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD);
      if (permission) {
        let techPermission = convertTreeToList([permission?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.TECHNICAL)]);
        const objName = this.callType == '1' ? CUSTOMER_COMPONENT.INSTALLATION : this.callType == '2' ? CUSTOMER_COMPONENT.SERVICE :
          this.callType == '3' ? CUSTOMER_COMPONENT.SPECIAL_PROEJCT : '---';
        const callActionPermissionObj = techPermission?.find(el => el?.[objName])?.[objName];
        if (callActionPermissionObj) {
          const quickActionPermissionObj = callActionPermissionObj?.find(el => el?.menuName == PermissionTypes.QUICK_ACTION);
          if (quickActionPermissionObj) {
            this.actionPermissionObj = quickActionPermissionObj?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.REFER_TO_SALES);
          }
        }
      }
    });
  }

  getReferToSalesActionPermission(actionTypeMenuName) {
    return this.actionPermissionObj?.subMenu?.find(el => el?.menuName == actionTypeMenuName);
  }

  formControlChange() {
    this.referToSaleForm.get('creatorsContactNoCountryCode').valueChanges.subscribe((creatorsContactNoCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(creatorsContactNoCountryCode);
      if (this.referToSaleForm.get('creatorsContactNo').value) {
        setTimeout(() => {
          this.row.nativeElement.focus();
          this.row.nativeElement.blur();
        }, 4000);
      }

    });
    this.referToSaleForm.get('creatorsContactNo').valueChanges.subscribe((mobileNumber2: string) => {
      this.setPhoneNumberLengthByCountryCode(this.referToSaleForm.get('creatorsContactNoCountryCode').value);

    });

    this.referToSaleForm.get('referToSaleFlagType').valueChanges.subscribe((val: string) => {


      if (val != '') {


        if (this.referToSalesFlagListFormArray.value.find(x => x.referToSalesFlagTypeId == val) != undefined) {

          this.snackbarService.openSnackbar("Flag type already added", ResponseMessageTypes.WARNING);

        }
      }

    })

  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.referToSaleForm.get('creatorsContactNo').setValidators([Validators.minLength(formConfigs.nineDigits),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        break;
      default:
        this.referToSaleForm.get('creatorsContactNo').setValidators([Validators.minLength(formConfigs.indianContactNumberMaxLength),
        Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);

        break;
    }

  }
  // new FormControl({value : response[1].resources[i]['sellableFrom'],disabled : (response[1].resources[i]['sellable'] == true ? false : true)},response[1].resources[i]['sellable'] == true ? Validators.required : null),

  createOnHoldCreationFrom(callInitiationReferToSaleModel?: CallInitiationReferToSaleModel) {
    let callInitiationReferToSaleCreateModel = new CallInitiationReferToSaleModel(callInitiationReferToSaleModel);
    this.referToSaleForm = this.formBuilder.group({});
    Object.keys(callInitiationReferToSaleCreateModel).forEach((key) => {
      if (key == 'referToSalesFlagList') {
        let referToSalesFlagListFormArray = this.formBuilder.array([
          this.formBuilder.group(
            {
              referToSalesId: '',
              referToSalesRefNo: '',
              createdDate: '',
              createdBy: '',
              referToSalesFlagTypeId: '',
              referToSalesFlagTypeName: '',
              referToSalesStatusName: '',
              description: '',
              isAcknowledge: false,
              acknowledgeById: '',
              acknowledgeBy: '',
              acknowledgeDate: '',
              acknowledgeRefertoSalesFeedbackId: '',
              acknowledgeFeedbackDescription: ''
            })
        ]);
        this.referToSaleForm.addControl(key, referToSalesFlagListFormArray);
      } else {
        this.referToSaleForm.addControl(key, new FormControl(callInitiationReferToSaleCreateModel[key]));
      }


    });
    // this.referToSaleForm.get('creatorsContactNoCountryCode').setValue('+27');
    this.referToSaleForm = setRequiredValidator(this.referToSaleForm, ["creatorsContactNo", "creatorsContactNoCountryCode"]);

  }

  get referToSalesFlagListFormArray(): FormArray {
    if (this.referToSaleForm !== undefined) {
      return (<FormArray>this.referToSaleForm.get('referToSalesFlagList'));
    }
  }
  investigateDialogs() {
    // investigateDialogform
    this.investigateDialogform = new FormGroup({
      acknowledgeBy: new FormControl(null),
      acknowledgeById: new FormControl(null),
      acknowledgeDate: new FormControl(null),
      acknowledgeDateShow: new FormControl(null),
      acknowledgeRefertoSalesFeedbackId: new FormControl(null),
      acknowledgeFeedbackDescription: new FormControl(null),
    });
    this.onValueChanges();
    this.investigateDialogform = setRequiredValidator(this.investigateDialogform, ['acknowledgeRefertoSalesFeedbackId']);
  }
  onValueChanges() {
    this.investigateDialogform
      .get("acknowledgeRefertoSalesFeedbackId")
      .valueChanges.subscribe((acknowledgeRefertoSalesFeedbackId: string) => {
        if (this.investigateDialogformIsSubmit) {
          if (acknowledgeRefertoSalesFeedbackId)
            this.investigateDialogformErrMsg = '';
          else
            this.investigateDialogformErrMsg = 'Feedback is required';
        }

      });
  }
  referToSaleFormArrayAdd() {
    this.referToSaleForm = clearFormControlValidators(this.referToSaleForm, ["description"]);

    let filteredData = this.referToSalesFlagListArray.filter((item) => {
      return item.referToSalesFlagTypeId == this.referToSaleForm.get('referToSaleFlagType').value;
    });

    if (filteredData && filteredData.length > 0) {
      this.snackbarService.openSnackbar("Refer to Sales Flag Type exists already ", ResponseMessageTypes.WARNING);
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);

    if (this.referToSaleForm.get('referToSaleFlagType').value == '' || this.referToSaleForm.get('referToSaleFlagType').value == null) {
      this.snackbarService.openSnackbar("select Refer to Sales Flag Type", ResponseMessageTypes.WARNING);

    }
    if (this.referToSalesFlagListFormArray.value.find(x => x.referToSalesFlagTypeId == this.referToSaleForm.get('referToSaleFlagType').value) == undefined) {
      this.referToSalesFlagListFormArray.push(this.formBuilder.group(
        {
          referToSalesId: '',
          referToSalesRefNo: '',
          createdDate: '',
          createdBy: '',
          isAcknowledge: false,
          acknowledgeById: '',
          acknowledgeBy: '',
          acknowledgeDate: null,
          acknowledgeRefertoSalesFeedbackId: null,
          acknowledgeFeedbackDescription: null,
          referToSalesFlagTypeId: this.referToSaleForm.get('referToSaleFlagType').value,
          referToSalesFlagTypeName: [{ value: this.flagList.find(x => x.id == this.referToSaleForm.get('referToSaleFlagType').value).displayName, disabled: true }],
          referToSalesStatusName: '',
          isLeadEnabled: this.flagList.find(x => x.id == this.referToSaleForm.get('referToSaleFlagType').value).isLeadEnabled,
          description: this.referToSaleForm.get('description').value,
        }))
      this.referToSaleForm.get('referToSaleFlagType').patchValue('');
      this.referToSaleForm.get('description')?.setValue(null);
      for (let i = 0; i < this.referToSalesFlagListFormArray.length; i++) {
        //this.referToSalesFlagListFormArray[i] = setRequiredValidator(this.referToSalesFlagListFormArray[i], ["description"]);
      }


      this.referToSalesFlagListFormArray.value.find(x => x.referToSalesStatusName == '' || x.referToSalesStatusName == 'New') != undefined ? this.referToSaleForm.controls['description'].enable() : this.referToSaleForm.controls['description'].enable();
      this.isStatusBlank = true;

    }
    else if (this.referToSalesFlagListFormArray.value.find(x => x.referToSalesFlagTypeId == this.referToSaleForm.get('referToSaleFlagType').value) != undefined) {

      this.snackbarService.openSnackbar("Flag type already added", ResponseMessageTypes.WARNING);

    }



  }

  deletereferToSaleFormArray(index) {
    if (index > -1) {
      if (!this.getReferToSalesActionPermission(PermissionTypes.DELETE)) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      // this.addedStockCount = this.addedStockCount - parseInt(this.auditCycleConfigFormArray.value[index].stockCount)
      this.referToSalesFlagListFormArray.removeAt(index);
      this.referToSalesFlagListFormArray.value.find(x => x.referToSalesStatusName == '' || x.referToSalesStatusName == 'New') != undefined ? this.referToSaleForm.controls['description'].enable() : this.referToSaleForm.controls['description'].enable();
      this.isStatusBlank = this.referToSalesFlagListFormArray.value.find(x => x.referToSalesStatusName == '') ? true : false;
    }
  }



  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.flagList = resp.resources;
              break;
            case 1:
              this.feedbackList = resp.resources;
              break;
            case 2:
              this.referToSaleDetails = resp.resources;
              this.isStatusBlank = false;
              // this.isAcknowledge = this.referToSaleDetails.isAcknowledge == true ? true : false;
              this.referToSaleForm.controls['appointmentId'].patchValue(resp?.resources?.appointmentId == null ? '' : resp?.resources?.appointmentId);
              //this.referToSaleForm.controls['creatorsContactNoCountryCode'].patchValue(resp?.resources?.creatorsContactNoCountryCode == null ? '' : resp?.resources?.creatorsContactNoCountryCode);
              if (this.referToSaleDetails?.creatorsContactNoCountryCode) {
                this.referToSaleForm.controls['creatorsContactNoCountryCode'].setValue(this.referToSaleDetails?.creatorsContactNoCountryCode);

              } else {
                this.referToSaleForm.get('creatorsContactNoCountryCode').setValue('+27');
              }

              if (this.referToSaleDetails?.creatorsContactNo) {
                this.referToSaleForm.controls['creatorsContactNo'].setValue(this.referToSaleDetails?.creatorsContactNo);

              }
              //else{
              //  // this.referToSaleForm.controls['creatorsContactNo'].setValue('');
              // }
              //this.referToSaleForm.controls['creatorsContactNo'].patchValue(resp?.resources?.creatorsContactNo == null ? '' : resp?.resources?.creatorsContactNo);
              this.referToSaleForm.controls['description'].patchValue(resp.resources?.description == null ? '' : resp.resources?.description);

              this.referToSaleForm.controls['isCode50'].patchValue(resp?.resources?.isCode50 == null ? false : resp.resources?.isCode50);
              this.referToSaleForm.controls['createdUserId'].patchValue(resp?.resources?.isCode50 == null ? false : resp.resources?.isCode50);
              this.referToSaleForm.controls['isAcknowledge'].patchValue(resp?.resources?.isAcknowledge == null ? false : resp.resources?.isAcknowledge);
              this.referToSaleForm.controls['acknowledgeById'].patchValue(resp?.resources?.acknowledgeById == null ? '' : resp.resources?.acknowledgeById);
              this.referToSaleForm.controls['acknowledgeDate'].patchValue(resp?.resources?.acknowledgeDate == null ? '' : resp.resources?.acknowledgeDate);
              this.referToSaleForm.controls['acknowledgeRefertoSalesFeedbackId'].patchValue(resp?.resources?.acknowledgeRefertoSalesFeedbackId == null ? '' : resp.resources?.acknowledgeRefertoSalesFeedbackId);
              this.referToSaleForm.controls['acknowledgeFeedbackDescription'].patchValue(resp.resources?.acknowledgeFeedbackDescription == null ? '' : resp?.resources?.acknowledgeFeedbackDescription);
              if (resp.resources?.acknowledgeFeedbackDescription)
                this.referToSaleForm.get('acknowledgeFeedbackDescription').setValue(resp.resources?.acknowledgeFeedbackDescription);

              this.referToSaleForm.controls['acknowledgeBy'].patchValue(resp?.resources?.acknowledgeBy == null ? '' : resp?.resources?.acknowledgeBy);
              if (resp && resp.resources && resp.resources?.callCreatedDate) {
                this.referToSaleForm?.get('callCreatedDate').setValue(this.datePipe.transform(resp.resources?.callCreatedDate, 'dd-MM-yyy'));
                this.referToSaleDetails.callCreatedDate = this.datePipe.transform(resp.resources?.callCreatedDate, 'dd-MM-yyy');
              }

              if (resp.resources?.referToSalesFlagList != null) {
                // resp?.resources?.referToSalesFlagList[0].referToSalesStatusName == "Done" ? this.referToSaleForm.controls['creatorsContactNoCountryCode'].disable() : false;
                ///resp?.resources?.referToSalesFlagList[0].referToSalesStatusName == "Done" ? this.referToSaleForm.controls['referToSaleFlagType'].disable() : false;
                // resp?.resources?.referToSalesFlagList[0].referToSalesStatusName == "Completed" ? this.referToSaleForm.controls['creatorsContactNo'].disable() : false;
                /// resp?.resources?.referToSalesFlagList[0].referToSalesStatusName == "Done" ? this.referToSaleForm.controls['isCode50'].disable() : false;
                ///resp?.resources?.referToSalesFlagList[0].referToSalesStatusName == "Done" ? this.referToSaleForm.controls['acknowledgeRefertoSalesFeedbackId'].disable() : false;
                /// resp?.resources?.referToSalesFlagList[0].referToSalesStatusName == "Done" ? this.referToSaleForm.controls['creatorsContactNo'].disable() : false;
                ///resp?.resources?.referToSalesFlagList[0].referToSalesStatusName == "Done" ? this.referToSaleForm.controls['acknowledgeFeedbackDescription'].disable() : false;
                // resp?.resources?.referToSalesFlagList[0].referToSalesStatusName == "Done" ? this.referToSalesFlagListFormArray.controls[0].get('referToSalesStatusName').disable() : false;
                /// this.iscompleteddisabled = resp?.resources?.referToSalesFlagList[0].referToSalesStatusName == "Done" ? 'disabled' : false;

                this.referToSalesFlagListFormArray.clear();

                if (resp?.resources?.referToSalesFlagList != null) {
                  this.isInvestigation = resp?.resources?.referToSalesFlagList.find(x => x.referToSalesStatusName == 'New') != undefined ? true : false;

                }
                this.isInvestigation == true ? this.referToSaleForm.controls['description'].enable() : this.referToSaleForm.controls['description'].enable();
                this.referToSalesFlagListArray = resp.resources.referToSalesFlagList;

                let filteredData = this.referToSalesFlagListArray?.filter((item) => {
                  return item.referToSalesStatusName == 'Done';
                });
                let flag = false;
                if (filteredData && filteredData.length > 0) {
                  // this.referToSaleForm.get('creatorsContactNo').disable();
                  // this.referToSaleForm.get('referToSaleFlagType').disable();
                  this.iscompleteddisabled = 'disabled';
                  flag = true;
                }
                if (resp?.resources?.acknowledgeFeedbackDescription) {
                  this.referToSaleForm?.get('acknowledgeFeedbackDescription')?.setValue(resp?.resources?.acknowledgeFeedbackDescription);
                }
                // if (resp?.resources?.acknowledgeRefertoSalesFeedbackId) {
                //   this.referToSaleForm?.get('acknowledgeFeedbackDescription')?.setValue(resp?.resources?.acknowledgeRefertoSalesFeedbackId);
                // }

                for (let i = 0; i < resp?.resources?.referToSalesFlagList.length; i++) {

                  this.referToSalesFlagListFormArray.push(this.formBuilder.group(
                    {
                      referToSalesId: resp?.resources?.referToSalesFlagList[i].referToSalesId,
                      referToSalesRefNo: resp?.resources?.referToSalesFlagList[i].referToSalesRefNo,
                      createdDate: this.datePipe.transform(resp?.resources?.referToSalesFlagList[i].createdDate, 'dd-MM-yyy'),
                      createdBy: resp?.resources?.referToSalesFlagList[i].createdBy,
                      isAcknowledge: resp?.resources?.referToSalesFlagList[i].isAcknowledge,
                      acknowledgeById: resp?.resources?.referToSalesFlagList[i].acknowledgeById,
                      acknowledgeBy: resp?.resources?.referToSalesFlagList[i]?.createdBy,
                      acknowledgeDate: resp?.resources?.referToSalesFlagList[i]?.acknowledgeDate,
                      acknowledgeRefertoSalesFeedbackId: resp?.resources?.referToSalesFlagList[i].acknowledgeRefertoSalesFeedbackId,
                      acknowledgeFeedbackDescription: resp?.resources?.referToSalesFlagList[i].acknowledgeFeedbackDescription,
                      description: flag ? new FormControl({ value: resp?.resources?.referToSalesFlagList[i].description, disabled: true }, Validators.required) : resp?.resources?.referToSalesFlagList[i].description,
                      referToSalesFlagTypeId: resp?.resources?.referToSalesFlagList[i].referToSalesFlagTypeId,
                      referToSalesFlagTypeName: [{ value: resp?.resources?.referToSalesFlagList[i].referToSalesFlagTypeName, disabled: true }],
                      referToSalesStatusName: new FormControl({ value: resp?.resources?.referToSalesFlagList[i].referToSalesStatusName, disabled: true }),
                    }));
                  // this.referToSalesFlagListFormArray[i].get('referToSalesFlagTypeId').disable();
                  // this.referToSalesFlagListFormArray[i] = setRequiredValidator(this.referToSalesFlagListFormArray[i], ["description"]);
                }

              }

              // this.referToSaleForm.patchValue();
              break;

          }


        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  referToSalesDialog: boolean = false;
  isAcknowledge: boolean = false;
  investigateReferToSales() {
    this.referToSalesDialog = true;

    this.rxjsService.setGlobalLoaderProperty(false);

  }

  investigateReferToSalesYes() {
    this.referToSalesDialog = false;
    this.isAcknowledge = true;
    setTimeout(() => {
      this.feedbackElement.nativeElement.focus();
    });
    //this.referToSaleForm = setRequiredValidator(this.referToSaleForm, ["acknowledgeFeedbackDescription","acknowledgeRefertoSalesFeedbackId"]);
    this.referToSaleForm.controls['isAcknowledge'].patchValue(true);
  }
  investigateReferToSalesNo() {
    this.referToSalesDialog = false;
    this.isAcknowledge = false;
    this.referToSaleForm.controls['isAcknowledge'].patchValue(false);
    this.referToSaleForm.controls['acknowledgeRefertoSalesFeedbackId'].patchValue(null);
    this.referToSaleForm.controls['acknowledgeFeedbackDescription'].patchValue(null);

  }
  investigateReferToSalesSave() {
    this.investigateDialogformIsSubmit = true;
    this.investigateDialogformErrMsg = null;
    if (this.investigateDialogform.invalid) {
      this.investigateDialogformErrMsg = 'Feedback is required';
      return;
    }
    this.investigateDialogformIsSubmit = false;
    this.investigateDialogformErrMsg = null;
    this.referToSalesFlagListFormArray?.controls[this.investigateIndex]?.get('isAcknowledge').setValue(true);
    this.referToSalesFlagListFormArray?.controls[this.investigateIndex]?.get('acknowledgeById').setValue(this.investigateDialogform.value.acknowledgeById);
    this.referToSalesFlagListFormArray?.controls[this.investigateIndex]?.get('acknowledgeDate').setValue(this.investigateDialogform.value.acknowledgeDate);
    this.referToSalesFlagListFormArray?.controls[this.investigateIndex]?.get('acknowledgeBy').setValue(this.investigateDialogform.value.acknowledgeBy);
    this.referToSalesFlagListFormArray?.controls[this.investigateIndex]?.get('acknowledgeRefertoSalesFeedbackId').setValue(this.investigateDialogform.value.acknowledgeRefertoSalesFeedbackId);
    this.referToSalesFlagListFormArray?.controls[this.investigateIndex]?.get('acknowledgeFeedbackDescription').setValue(this.investigateDialogform.value.acknowledgeFeedbackDescription);

    this.investigateDialog = false;
    this.investigateDialogform.reset();
  }
  investigateReferToSalesCancel() {
    this.investigateDialog = false;

    if (this.referToSalesFlagListFormArray?.controls[this.investigateIndex]?.get('referToSalesStatusName').value != 'Investigation') {
      this.referToSalesFlagListFormArray?.controls[this.investigateIndex]?.get('isAcknowledge').setValue(false);
      this.referToSalesFlagListFormArray?.controls[this.investigateIndex]?.get('acknowledgeById').setValue(null);
      this.referToSalesFlagListFormArray?.controls[this.investigateIndex]?.get('acknowledgeDate').setValue(null);
      this.referToSalesFlagListFormArray?.controls[this.investigateIndex]?.get('acknowledgeBy').setValue(null);
      this.referToSalesFlagListFormArray?.controls[this.investigateIndex]?.get('acknowledgeRefertoSalesFeedbackId').setValue(null);
      this.referToSalesFlagListFormArray?.controls[this.investigateIndex]?.get('acknowledgeFeedbackDescription').setValue(null);
    }
  }
  rowInvestigate(index) {
    if (!this.getReferToSalesActionPermission(PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.investigateDialogform.reset();

    this.investigateIndex = index;
    this.investigateDialog = true;
    // this.datePipe.transform(val.runDate, 'dd-MM-yyyy') : ''

    let acknowledgeById = this.referToSalesFlagListFormArray.value[index].acknowledgeById ? this.referToSalesFlagListFormArray.value[index].acknowledgeById : this.userData.userId;
    let acknowledgeBy = this.referToSalesFlagListFormArray.value[index].acknowledgeBy ? this.referToSalesFlagListFormArray.value[index].acknowledgeBy : this.userData.displayName;
    let acknowledgeDate = this.referToSalesFlagListFormArray.value[index].acknowledgeDate ? this.referToSalesFlagListFormArray.value[index].acknowledgeDate : new Date();
    let acknowledgeDateShow = this.datePipe.transform(acknowledgeDate, 'dd-MM-yyyy hh:mm:ss');
    this.investigateDialogform.get('acknowledgeById').setValue(acknowledgeById);
    this.investigateDialogform.get('acknowledgeBy').setValue(acknowledgeBy);
    this.investigateDialogform.get('acknowledgeDate').setValue(acknowledgeDate);
    this.investigateDialogform.get('acknowledgeDateShow').setValue(acknowledgeDateShow);
    if (this.referToSalesFlagListFormArray.value && this.referToSalesFlagListFormArray.value[index].acknowledgeRefertoSalesFeedbackId)
      this.investigateDialogform.get('acknowledgeRefertoSalesFeedbackId').setValue(this.referToSalesFlagListFormArray.value[index].acknowledgeRefertoSalesFeedbackId);
    if (this.referToSalesFlagListFormArray.value && this.referToSalesFlagListFormArray.value[index].acknowledgeFeedbackDescription)
      this.investigateDialogform.get('acknowledgeFeedbackDescription').setValue(this.referToSalesFlagListFormArray.value[index].acknowledgeFeedbackDescription);
  }
  getType(val) {
    if (val && this.flagList && this.flagList.length > 0) {
      return this.flagList.find(el => el?.id == val)?.displayName;
    }
  }
  getReferToSalesDetailById() {
    // if (this.initiationId || this.customerId) {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.REFER_TO_SALES, undefined, null, prepareGetRequestHttpParams(null, null,
        { CustomerId: this.customerId, appointmentId: this.appointmentId, createdUserId: this.userData.userId }
      )).subscribe((response: IApplicationResponse) => {
        // let stockOrderModel = new InstallationCallinitiationAddEditModel(response.resources);
        this.referToSaleDetails = response.resources;

      });
    // this.getNextavailableAppointment()
    // }

    this.rxjsService.setGlobalLoaderProperty(false);
  }
  editFlag(index) {
    if (!this.getReferToSalesActionPermission(PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    // this.referToSalesFlagListFormArray.controls[index].get('referToSalesFlagTypeName').enabled;

    this.referToSalesFlagListFormArray.controls[index].get('referToSalesFlagTypeId').enable();
  }
  onCreate() {
    if (!this.getReferToSalesActionPermission(PermissionTypes.ADD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.referToSaleForm.get('referToSaleFlagType').value || this.referToSaleForm.get('referToSaleFlagType').value == null || this.referToSaleForm.get('referToSaleFlagType').value == '') {

      this.referToSaleForm = clearFormControlValidators(this.referToSaleForm, ["description"]);
    }
    else {

      this.referToSaleForm = setRequiredValidator(this.referToSaleForm, ["description"]);
    }


    // if (this.isAcknowledge == true) {
    //   this.referToSaleForm = setRequiredValidator(this.referToSaleForm, ["acknowledgeFeedbackDescription", "acknowledgeRefertoSalesFeedbackId"]);
    // }

    this.isSubmitted = true;

    if (this.referToSaleForm.invalid) {
      this.referToSaleForm.markAllAsTouched();
      return
    }

    if ((this.referToSalesFlagListFormArray && this.referToSalesFlagListFormArray.length == 0) && (this.referToSaleForm.get('referToSaleFlagType').value == '' || this.referToSaleForm.get('referToSaleFlagType').value == null)) {
      this.snackbarService.openSnackbar("Atleast One Refer to sales flag type is required", ResponseMessageTypes.ERROR);
      return;
    }
    let filteredData = this.referToSalesFlagListArray.filter((item) => {
      return item.referToSalesFlagTypeId == this.referToSaleForm.get('referToSaleFlagType').value;
    });

    if (filteredData && filteredData.length > 0) {
      this.snackbarService.openSnackbar("Refer to Sales Flag Type exists already ", ResponseMessageTypes.WARNING);
      return;
    }

    //  if(this.referToSalesFlagListFormArray.length == 0){
    // this.snackbarService.openSnackbar("Atleast One Refer to sales flag type is required", ResponseMessageTypes.ERROR);
    // return;
    // }

    // this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.referToSaleForm.value.referToSalesFlagList.length == 0) {
      let createObj = Object.assign({}, {
        appointmentId: this.appointmentId,
        creatorsContactNoCountryCode: this.referToSaleForm.value.creatorsContactNoCountryCode,
        creatorsContactNo: this.referToSaleForm.getRawValue().creatorsContactNo,
        isCode50: this.referToSaleForm.value.isCode50,
        description: this.referToSaleForm.value.description,
        createdUserId: this.userData.userId,
        customerId: this.customerId,
        addressId: this.addressId,
        initiationId: this.initiationId,
        callInitiationId: this.callInitiationId,
        referToSalesFlagList: [
          // {
          // referToSalesStatusId:'',
          // refertoSalesFlagTypeId:''
          // },

        ]
      });

      if (this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.referToSalesFlagTypeId && this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.referToSalesStatusName && this.referToSalesFlagListFormArray.value[(this.referToSalesFlagListFormArray.value.length - 1)]?.referToSalesFlagTypeId != '') {
        createObj.referToSalesFlagList.push({
          referToSalesStatusId: this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.referToSalesStatusName,
          refertoSalesFlagTypeId: this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.referToSalesFlagTypeId,
          description: this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.description,
          isAcknowledge: this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.isAcknowledge,
          acknowledgeById: this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.acknowledgeById ? this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.acknowledgeById : null,
          acknowledgeDate: this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.acknowledgeDate ? this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.acknowledgeDate : null,
          acknowledgeRefertoSalesFeedbackId: this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.acknowledgeRefertoSalesFeedbackId ? this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.acknowledgeRefertoSalesFeedbackId : null,
          acknowledgeFeedbackDescription: this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.acknowledgeFeedbackDescription ? this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.acknowledgeFeedbackDescription : null
        })
      }

      if (this.referToSalesFlagListFormArray.value.length > 1 || this.referToSalesFlagListFormArray.value.length == 0) {
        if (this.referToSalesFlagListFormArray.value.length > 1) {
          for (let i = 0; i < (this.referToSaleForm.value.referToSalesFlagList.length - 1); i++) {
            createObj.referToSalesFlagList.push({
              referToSalesStatusId: this.referToSaleForm.value.referToSalesFlagList[i]?.referToSalesStatusName,
              refertoSalesFlagTypeId: this.referToSaleForm.value.referToSalesFlagList[i]?.referToSalesFlagTypeId,
              description: this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.description,
              isAcknowledge: this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.isAcknowledge,
              acknowledgeById: this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.acknowledgeById,
              acknowledgeDate: this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.acknowledgeDate,
              acknowledgeRefertoSalesFeedbackId: this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.acknowledgeRefertoSalesFeedbackId,
              acknowledgeFeedbackDescription: this.referToSaleForm.value.referToSalesFlagList[(this.referToSalesFlagListFormArray.value.length - 1)]?.acknowledgeFeedbackDescription
            })
          }
        }


      }
      if (this.referToSaleForm.get('referToSaleFlagType') && this.referToSaleForm.get('referToSaleFlagType').value) {
        createObj.referToSalesFlagList.push({
          referToSalesId: null,
          refertoSalesFlagTypeId: this.referToSaleForm.get('referToSaleFlagType').value,
          description: this.referToSaleForm.get('description').value,
          isAcknowledge: this.referToSaleForm.get('isAcknowledge').value,
          acknowledgeById: this.referToSaleForm.get('isAcknowledge').value && this.referToSaleForm.get('acknowledgeById').value ? this.referToSaleForm.get('acknowledgeById').value : null,
          acknowledgeDate: this.referToSaleForm.get('acknowledgeDate').value ? this.referToSaleForm.get('acknowledgeDate').value : null,
          acknowledgeRefertoSalesFeedbackId: this.referToSaleForm.get('isAcknowledge').value && this.referToSaleForm.get('acknowledgeRefertoSalesFeedbackId').value ? this.referToSaleForm.get('acknowledgeRefertoSalesFeedbackId').value : null,
          acknowledgeFeedbackDescription: this.referToSaleForm.get('isAcknowledge').value && this.referToSaleForm.get('acknowledgeFeedbackDescription').value ? this.referToSaleForm.get('acknowledgeFeedbackDescription').value : null
        })
      }
      if (/\s/.test(createObj.creatorsContactNo)) {
        // It has any kind of whitespace
        createObj.creatorsContactNo = createObj.creatorsContactNo.replace(/\s/g, "");
      }

      createObj.createdUserId = this.userData.userId;
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.REFER_TO_SALES, createObj).subscribe((response) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.referToSaleForm = clearFormControlValidators(this.referToSaleForm, ["description"]);
          let dropdownsAndData = [
            this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
              TechnicalMgntModuleApiSuffixModels.UX_REFERTOSALES_FLAG_TYPE),
            this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
              TechnicalMgntModuleApiSuffixModels.UX_REFERTO_SALES_FEEDBACK),
            this.crudService.get(
              ModulesBasedApiSuffix.SALES,
              SalesModuleApiSuffixModels.REFER_TO_SALES, undefined, null, prepareGetRequestHttpParams(null, null,
                { CustomerId: this.customerId, appointmentId: this.appointmentId, createdUserId: this.userData.userId }
              )),
          ];
          this.loadActionTypes(dropdownsAndData);
          this.formControlChange();
          this.referToSaleForm.get('referToSaleFlagType').patchValue('');
          // this.ngOnInit();
        }

      })
      // else{
      //   this.snackbarService.openSnackbar("add atleast one Refer to Sales Flags Loaded For Job", ResponseMessageTypes.WARNING);


      // }
    }
    else {
      let createObj = Object.assign({}, {
        appointmentId: this.appointmentId,
        creatorsContactNoCountryCode: this.referToSaleForm.value.creatorsContactNoCountryCode,
        creatorsContactNo: this.referToSaleForm.getRawValue().creatorsContactNo,
        isCode50: this.referToSaleForm.value.isCode50,
        description: this.referToSaleForm.getRawValue().description,
        isAcknowledge: this.isInvestigation == false ? false : this.referToSaleForm.value.isAcknowledge,
        acknowledgeById: this.isInvestigation == false ? null : this.referToSaleForm.value.isAcknowledge == true ? (this.referToSaleForm.value.acknowledgeById == '' ? this.userData.userId : this.referToSaleForm.value.acknowledgeById) : null,
        acknowledgeDate: this.isInvestigation == false ? null : this.referToSaleForm.value.isAcknowledge == true ? this.referToSaleForm.value.acknowledgeDate : null,
        acknowledgeRefertoSalesFeedbackId: this.isInvestigation == false ? null : this.referToSaleForm.value.acknowledgeRefertoSalesFeedbackId,
        acknowledgeFeedbackDescription: this.isInvestigation == false ? null : this.referToSaleForm.value.isAcknowledge == true ? this.referToSaleForm.value.acknowledgeFeedbackDescription : null,
        createdUserId: this.userData.userId,
        callInitiationId: this.callInitiationId,
        customerId: this.customerId,
        addressId: this.addressId,
        initiationId: this.initiationId,
        referToSalesFlagList: [


        ]
      })
   
      for (let i = 0; i < this.referToSaleForm.value.referToSalesFlagList.length; i++) {
        let obj;
        if (i == (this.referToSaleForm.value.referToSalesFlagList.length - 1)) {
          if (this.referToSaleForm.value.referToSalesFlagList[i].referToSalesFlagTypeId != '') {
            let description = this.referToSaleForm.value.referToSalesFlagList[i]?.description ? this.referToSaleForm.value.referToSalesFlagList[i]?.description : this.referToSalesFlagListFormArray?.controls[i]?.get('description').value;
            obj = Object.assign({},
              this.referToSaleForm.value.referToSalesFlagList[i].referToSalesId == '' ? null : { referToSalesId: this.referToSaleForm.value.referToSalesFlagList[i].referToSalesId },
              { referToSalesStatusId: this.referToSaleForm.value.referToSalesFlagList[i]?.referToSalesStatusName },
              { refertoSalesFlagTypeId: this.referToSaleForm.value.referToSalesFlagList[i]?.referToSalesFlagTypeId },
              { description: description},
              { isAcknowledge: this.referToSaleForm.value.referToSalesFlagList[i]?.isAcknowledge },
              { acknowledgeById: (this.referToSaleForm.value.referToSalesFlagList[i]?.isAcknowledge && this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeById) ? this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeById : null },
              { acknowledgeDate: (this.referToSaleForm.value.referToSalesFlagList[i]?.isAcknowledge && this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeDate) ? this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeDate : null },
              { acknowledgeRefertoSalesFeedbackId: (this.referToSaleForm.value.referToSalesFlagList[i]?.isAcknowledge && this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeRefertoSalesFeedbackId) ? this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeRefertoSalesFeedbackId : null },
              { acknowledgeFeedbackDescription: (this.referToSaleForm.value.referToSalesFlagList[i]?.isAcknowledge && this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeFeedbackDescription) ? this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeFeedbackDescription : null }

            )

            createObj.referToSalesFlagList.push(
              obj

            )
          }
        } else {
          let description = this.referToSaleForm.value.referToSalesFlagList[i]?.description ? this.referToSaleForm.value.referToSalesFlagList[i]?.description : this.referToSalesFlagListFormArray?.controls[i]?.get('description').value;
          obj = Object.assign({},
            this.referToSaleForm.value.referToSalesFlagList[i].referToSalesId == '' ? null : { referToSalesId: this.referToSaleForm.value.referToSalesFlagList[i].referToSalesId },
            { referToSalesStatusId: this.referToSaleForm.value.referToSalesFlagList[i]?.referToSalesStatusName },
            { refertoSalesFlagTypeId: this.referToSaleForm.value.referToSalesFlagList[i]?.referToSalesFlagTypeId },
            { description: description },
            { isAcknowledge: this.referToSaleForm.value.referToSalesFlagList[i]?.isAcknowledge },
            { acknowledgeById: (this.referToSaleForm.value.referToSalesFlagList[i]?.isAcknowledge && this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeById) ? this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeById : null },
            { acknowledgeDate: (this.referToSaleForm.value.referToSalesFlagList[i]?.isAcknowledge && this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeDate) ? this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeDate : null },
            { acknowledgeRefertoSalesFeedbackId: (this.referToSaleForm.value.referToSalesFlagList[i]?.isAcknowledge && this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeRefertoSalesFeedbackId) ? this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeRefertoSalesFeedbackId : null },
            { acknowledgeFeedbackDescription: (this.referToSaleForm.value.referToSalesFlagList[i]?.isAcknowledge && this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeFeedbackDescription) ? this.referToSaleForm.value.referToSalesFlagList[i]?.acknowledgeFeedbackDescription : null }

          )

          createObj.referToSalesFlagList.push(
            obj

          )
        }

      }

      if (this.referToSaleForm.get('referToSaleFlagType') && this.referToSaleForm.get('referToSaleFlagType').value) {
        createObj.referToSalesFlagList.push({
          referToSalesId: null,
          refertoSalesFlagTypeId: this.referToSaleForm.get('referToSaleFlagType').value,
          description: this.referToSaleForm.get('description').value,
          isAcknowledge: false,
          acknowledgeById: null,
          acknowledgeDate: null,
          acknowledgeRefertoSalesFeedbackId: null,
          acknowledgeFeedbackDescription: null,
        })
      }
      if (/\s/.test(createObj.creatorsContactNo)) {
        // It has any kind of whitespace
        createObj.creatorsContactNo = createObj.creatorsContactNo.replace(/\s/g, "");
      }
      createObj.createdUserId = this.userData.userId;
      let flag;
      if (this.referToSaleForm.value.referToSalesFlagList.length > 0) {
        flag = this.referToSaleForm.value.referToSalesFlagList.filter(x => (x.referToSalesId && (x.referToSalesId != null || x.referToSalesId != '')));
      }
      if (flag && flag.length > 0) { //referToSalesId exists so update
        this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.REFER_TO_SALES, createObj).subscribe((response) => {
          this.afterSubmit();
          // this.ngOnInit();
        });
      } else {
        this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.REFER_TO_SALES, createObj).subscribe((response) => {
          if (response?.isSuccess && response?.statusCode == 200) {
            this.afterSubmit();
          }
        });
      }

    }

  }
  afterSubmit() {
    this.referToSaleForm = clearFormControlValidators(this.referToSaleForm, ["description"]);
    let dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
        TechnicalMgntModuleApiSuffixModels.UX_REFERTOSALES_FLAG_TYPE),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_REFERTO_SALES_FEEDBACK),
      this.crudService.get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.REFER_TO_SALES, undefined, null, prepareGetRequestHttpParams(null, null,
          { CustomerId: this.customerId, appointmentId: this.appointmentId, createdUserId: this.userData.userId }
        )),
    ];
    this.loadActionTypes(dropdownsAndData);
    this.formControlChange();
    this.referToSaleForm.get('referToSaleFlagType').patchValue('');
  }
  onCompleteRefertoSales() {
    if (!this.getReferToSalesActionPermission(PermissionTypes.COMPLETE)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.referToSalesCompleteDialog = true;
  }
  onCompleteRefertoSalesYes() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    let createObj = {
      appointmentId: this.appointmentId,
      createdUserId: this.userData.userId,

    }

    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.REFER_TO_SALES_COMPLETE, createObj).subscribe((response) => {
      let dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.SALES,
          TechnicalMgntModuleApiSuffixModels.UX_REFERTOSALES_FLAG_TYPE),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_REFERTO_SALES_FEEDBACK),
        this.crudService.get(
          ModulesBasedApiSuffix.SALES,
          SalesModuleApiSuffixModels.REFER_TO_SALES, undefined, null, prepareGetRequestHttpParams(null, null,
            { CustomerId: this.customerId, appointmentId: this.appointmentId, createdUserId: this.userData.userId }
          )),
      ];
      this.loadActionTypes(dropdownsAndData);
    });
    this.referToSalesCompleteDialog = false;
  }
  onCompleteRefertoSalesNo() {
    this.referToSalesCompleteDialog = false;
  }
  onCancel() {
    // this.router.navigate(['/customer/manage-customers/call-initiation'], {
    //   queryParams: {
    //     customerId: this.customerId,
    //     initiationId: this.initiationId,
    //     appointmentId: this.appointmentId ? this.appointmentId : null,
    //     customerAddressId:this.addressId,
    //     callType: this.callType // 2- service call, 1- installation call
    //   }, skipLocationChange: true
    // });
    this.navigateToPage();
  }

  navigateToPage() {
    this.router.navigate(['/customer', 'manage-customers', 'call-initiation'], {
      queryParams: {
        customerId: this.customerId,
        initiationId: this.installationId,
        callType: this.callType,
        customerAddressId: this.addressId,
        appointmentId: this.appointmentId ? this.appointmentId : null,
      }, skipLocationChange: true
    });
  }

  blurEvent(e) { }
}
