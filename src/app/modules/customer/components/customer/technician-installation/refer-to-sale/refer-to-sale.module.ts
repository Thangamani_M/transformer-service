import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ReferToSaleComponent } from "./refer-to-sale.component";

import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
    declarations: [ReferToSaleComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: ReferToSaleComponent, data: { title: 'Refer To Sale' }, canActivate: [AuthGuard]
        },
    ])
  ],
  entryComponents: [],
})
export class ReferToSaleModule { }