import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, countryCodes, CrudService, CustomDirectiveConfig, debounceTimeForSearchkeyword, formConfigs, getPDropdownData, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { AppointmentInfoModel, RadioRemovalBasicInfoModel, CallInitiationContactsFormArrayModel, PartitionModelFormArrayModel, RadioRemovalCallAddEditModel, RadioRemovalServiceInfoModel, CustomerModuleApiSuffixModels, NkaServiceCallModel, CallContactDialogComponent } from '@modules/customer';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, filter, map, switchMap, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-radio-removal-call-add-edit',
  templateUrl: './radio-removal-call-add-edit.component.html',
  styleUrls: ['./radio-removal-call-add-edit.component.scss']
})
export class RadioRemovalCallAddEditComponent implements OnInit {
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  title: string;
  initiatonCallCreationForm: FormGroup;
  initiationDetailsInfo: any;
  debtorList: any = [];
  contactList: any = [];
  contactNumberList: any = [];
  priorityList: any = [];
  alternativContactList: any = [];
  contactAlternativeNumberList: any = [];
  feedBackList: any = [];
  projectList: any = [];
  sysyemTypeList: any = [];
  SystemTypeSubCategoryList: any = [];
  ServiceSubTypeList: any = [];
  faultDescriptionList: any = [];
  diagnosisList: any = [];
  jobTypeList: any = [];
  protected ngUnsubscribe: Subject<void> = new Subject<void>();
  panelList: any = [];
  callType: any = '2';
  initiationId: string;
  customerId: string;
  addressId: string;
  callCreationTemplate: boolean;
  isManualChange: boolean;
  userData: UserLogin;
  isSubmitted: boolean;
  cepFormatadoValue: string;
  callInitiationContacts: any = [];
  serviceSubTypeId: string;
  radioRemovalWorkListId: string;
  listOfFiles: any[] = [];
  fileList: File[] = [];
  maxFilesUpload: Number = 5;
  selectedIndex = 0;
  countryCodes = countryCodes;
  formConfigs = formConfigs;
  isNavigationFirstTime: boolean = true


  constructor(private formBuilder: FormBuilder, private crudService: CrudService, private rxjsService: RxjsService, private momentService: MomentService,
    private router: Router, private activatedRoute: ActivatedRoute, private datePipe: DatePipe, private dateAdapter: DateAdapter<Date>, private store: Store<AppState>,
    private httpCancelService: HttpCancelService, private snackbarService: SnackbarService, private dialogService: DialogService,) {
    this.dateAdapter.setLocale('en-GB'); //dd/MM/yyyy
    this.initiationId = this.activatedRoute.snapshot.queryParams.initiationId;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.callType = this.activatedRoute.snapshot.queryParams.callType;
    this.radioRemovalWorkListId = this.activatedRoute.snapshot.queryParams.radioRemovalWorkListId;
    this.addressId = this.activatedRoute.snapshot.queryParams ? this.activatedRoute.snapshot.queryParams.addressId : null;
    let troubleShootObject = {
      customerId: this.customerId,
      initiationId: this.initiationId,
      customerAddressId: this.addressId,
      callType: this.callType
    }
    this.rxjsService.setTroubleShootAlarm(troubleShootObject);
    this.rxjsService.setCallInitiationData(this.activatedRoute.snapshot.queryParams);
    this.title = this.callType == '2' ? 'Service' : '---';
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.initForm();
    this.onLoadDropdownValues();
    this.getCallInitiationDetailsById();
  }

  onLoadDropdownValues() {
    let dropdownsAndData = [
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.UX_CUSTOMER_DEBTORS, undefined,
        false, prepareRequiredHttpParams({
          customerId: this.customerId,
          addressId: this.addressId
        })).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE,
        undefined,
        false, prepareRequiredHttpParams({
          ShowDescriptionOnly: true
        })).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_PANEL_TYPE_CONFIG)
        .pipe(map(result => result), catchError(error => of(error))),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_DEALER_CALL_FEEDBACK_CONFIG)
        .pipe(map(result => result), catchError(error => of(error))),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.UX_PRIORITY)
        .pipe(map(result => result), catchError(error => of(error))),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        InventoryModuleApiSuffixModels.UX_SPECIAL_PROJECT,
        undefined,
        false, prepareRequiredHttpParams({
          IsOpen: true
        })).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_QUICK_CALL)
        .pipe(map(result => result), catchError(error => of(error))),

    ];
    this.loadActionTypes(dropdownsAndData);
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.debtorList = resp.resources;
              break;
            case 1:
              this.sysyemTypeList = resp.resources;
              break;
            case 2:
              this.panelList = getPDropdownData(resp.resources);
              break;
            case 3:
              this.feedBackList = resp.resources;
              break;
            case 4:
              this.priorityList = resp.resources;
              break;
            case 5:
              this.projectList = resp.resources;
              break;
            case 6:
              this.faultDescriptionList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  initForm(radioRemovalCallAddEditModel?: RadioRemovalCallAddEditModel) {
    const radioRemovalCallModel = new RadioRemovalCallAddEditModel(radioRemovalCallAddEditModel);
    this.initiatonCallCreationForm = this.formBuilder.group({});
    Object.keys(radioRemovalCallModel).forEach((key) => {
      if (typeof radioRemovalCallModel[key] === 'object') {
        if (radioRemovalCallModel[key]?.length == 0) {
          this.initiatonCallCreationForm.addControl(key, new FormArray(radioRemovalCallModel[key]));
        } else if (Object.keys(radioRemovalCallModel[key])?.length == 0) {
          this.initiatonCallCreationForm.addControl(key, new FormGroup(radioRemovalCallModel[key]));
          if (key == 'basicInfo') {
            const basicInfoDetailsModel = new RadioRemovalBasicInfoModel();
            this.initFormInfoForm(this.initiatonCallCreationForm.get(key), basicInfoDetailsModel);
          } else if (key == 'serviceInfo') {
            const serviceInfoDetailsModel = new RadioRemovalServiceInfoModel();
            this.initFormInfoForm(this.initiatonCallCreationForm.get(key), serviceInfoDetailsModel);
          } else if (key == 'appointmentInfo') {
            const appointmentInfoDetailsModel = new AppointmentInfoModel();
            this.initFormInfoForm(this.initiatonCallCreationForm.get(key), appointmentInfoDetailsModel);
          } else if (key == 'nkaServiceCall') {
            const nkaServiceCallDetailsModel = new NkaServiceCallModel();
            this.initFormInfoForm(this.initiatonCallCreationForm.get(key), nkaServiceCallDetailsModel);
          }
        }
      } else {
        this.initiatonCallCreationForm.addControl(key, new FormControl(radioRemovalCallModel[key]));
      }
    });
    this.onSetValidators();
    this.onEnableDisableFormControl();
    this.onCallInitiationValueChanges();
  }

  onSetValidators() {
    this.initiatonCallCreationForm.get("basicInfo.contactId").setValidators([Validators.required]);
    this.initiatonCallCreationForm.get("basicInfo.contactNumber").setValidators([Validators.required]);
    this.initiatonCallCreationForm.get("basicInfo.debtorId").setValidators([Validators.required]);
    this.initiatonCallCreationForm.get("basicInfo.isAddressVerified").setValidators([Validators.required]);
    this.initiatonCallCreationForm.get("basicInfo").updateValueAndValidity();
    this.initiatonCallCreationForm.get("serviceInfo.systemTypeId").setValidators([Validators.required]);
    this.initiatonCallCreationForm.get("serviceInfo.systemTypeSubCategoryId").setValidators([Validators.required]);
    this.initiatonCallCreationForm.get("serviceInfo.serviceSubTypeId").setValidators([Validators.required]);
    this.initiatonCallCreationForm.get("serviceInfo.faultDescriptionId").setValidators([Validators.required]);
    this.initiatonCallCreationForm.get("serviceInfo.jobTypeId").setValidators([Validators.required]);
    this.initiatonCallCreationForm.get("serviceInfo").updateValueAndValidity();
  }

  onEnableDisableFormControl() {
    this.initiatonCallCreationForm.get('nkaServiceCall.poNumber').disable();
    this.initiatonCallCreationForm.get('nkaServiceCall.path').disable();
    this.initiatonCallCreationForm.get('nkaServiceCall.poAmount').disable();
    this.initiatonCallCreationForm.get('nkaServiceCall.poComment').disable();
    this.initiatonCallCreationForm.get('basicInfo.isPRCall').disable();
    this.initiatonCallCreationForm.get('basicInfo.isSaveOffer').disable();
    this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').disable();
    this.initiatonCallCreationForm.get('appointmentInfo.wireman').disable();
    this.initiatonCallCreationForm.get('basicInfo.isInWarrenty').disable();
    this.initiatonCallCreationForm.get('basicInfo.isRecall').disable();
    this.initiatonCallCreationForm.get('basicInfo.isCode50').disable();
    this.initiatonCallCreationForm.get('basicInfo.ismainPartation').disable();
    this.initiatonCallCreationForm.get('basicInfo.ismainPartation').disable();
    this.initiatonCallCreationForm.get('basicInfo.isAddressVerified').setValue(false);
  }

  initFormInfoForm(form, formDetailsModel: any) {
    Object.keys(formDetailsModel)?.forEach((key: any) => {
      if (typeof formDetailsModel[key] == 'object') {
        form.addControl(key, new FormArray(formDetailsModel[key]));
      } else {
        form.addControl(key, new FormControl(formDetailsModel[key]));
      }
    });
  }

  initPartitionsFormArray(partitionModelFormArrayModel?: PartitionModelFormArrayModel) {
    let partitionModel = new PartitionModelFormArrayModel(partitionModelFormArrayModel);
    let partitionFormArray = this.formBuilder.group({});
    Object.keys(partitionModel).forEach((key) => {
      partitionFormArray.addControl(key, new FormControl({ value: partitionModel[key], disabled: true }));
    });
    this.partitionsFormArray.push(partitionFormArray);
  }

  get partitionsFormArray(): FormArray {
    if (this.initiatonCallCreationForm !== undefined) {
      return (<FormArray>this.initiatonCallCreationForm.get('basicInfo.partitions'));
    }
  }

  initCallInitiationContactsFormArray(callInitiationContactsFormArrayModel?: CallInitiationContactsFormArrayModel) {
    let callInitiationContactsModel = new CallInitiationContactsFormArrayModel(callInitiationContactsFormArrayModel);
    let callInitiationContactsFormArray = this.formBuilder.group({});
    Object.keys(callInitiationContactsModel).forEach((key) => {
      callInitiationContactsFormArray.addControl(key, new FormControl({ value: callInitiationContactsModel[key], disabled: true }));
    });
    this.callInitiationContactsArray.push(callInitiationContactsFormArray);
  }

  get callInitiationContactsArray(): FormArray {
    if (!this.initiatonCallCreationForm) return;
    return this.initiatonCallCreationForm?.get('callInitiationContacts') as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onCallInitiationValueChanges() {
    this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').valueChanges.subscribe((val) => {
      if (val) {
        this.getSystemSubCategory(val);
      }
      else {
        this.SystemTypeSubCategoryList = [];
        this.ServiceSubTypeList = [];
        this.jobTypeList = [];
      }
    })

    this.initiatonCallCreationForm.get('appointmentInfo.nextAvailableAppointmentCheckbox').valueChanges.subscribe((val) => {
      if (val) {
        this.onSelectTime(this.initiatonCallCreationForm.get('appointmentInfo.nextAvailableAppointment').value)
      } else {
        this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').setValue(this.initiationDetailsInfo ? this.initiationDetailsInfo.scheduledStartDate ? new Date(this.initiationDetailsInfo.scheduledStartDate) : null : null)
        this.initiatonCallCreationForm.get('appointmentInfo.scheduledEndDate').setValue(this.initiationDetailsInfo ? this.initiationDetailsInfo.scheduledEndDate ? new Date(this.initiationDetailsInfo.scheduledEndDate) : null : null)
      }
    })

    this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').valueChanges.subscribe((val) => {
      if (val) {
        this.getServiceSubType(val);
      }
      else {
        this.ServiceSubTypeList = [];
        this.jobTypeList = [];
      }
    })

    this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').valueChanges.subscribe((val) => {
      this.serviceSubTypeId = val;
      if (val) {
        this.getFaultDescription(val);
      }
      else {
        this.jobTypeList = [];
      }
    })

    this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').valueChanges.subscribe((val) => {
      if (val) {
        this.isNavigationFirstTime = true;
        this.getJobType(val);
      }
      else {
        this.jobTypeList = [];
        this.getNextavailableAppointment()
      }
    })

    this.initiatonCallCreationForm.get('serviceInfo.jobTypeId').valueChanges.subscribe(val => {
      if (val) {
        this.getEstimateTime(val);
      }
    })

    this.initiatonCallCreationForm.get('basicInfo.contactId').valueChanges.subscribe(val => {
      if (!val) {
        this.initiatonCallCreationForm.get('basicInfo.contactNumber').setValue('');
        return
      }
      let contact = this.contactList.find(x => x.keyHolderId == val)
      if (contact) {
        this.contactNumberList = []
        if (contact.mobile1) {
          this.contactNumberList.push({ id: contact.mobile1, value: contact.mobile1 })
        }
        if (contact.mobile2) {
          this.contactNumberList.push({ id: contact.mobile2, value: contact.mobile2 })
        }
        if (contact.officeNo) {
          this.contactNumberList.push({ id: contact.officeNo, value: contact.officeNo })
        }
        if (contact.premisesNo) {
          this.contactNumberList.push({ id: contact.premisesNo, value: contact.premisesNo })
        }
        if (contact.isNewContact) {
          if (contact.contactNumber) { //for new contact
            this.contactNumberList.push({ id: contact.contactNumber, value: contact.contactNumber })
          }
        }


        let contactData = {
          keyholderId: contact.isNewContact ? null : contact.keyHolderId,
          contactName: contact.keyHolderName,
          contactNo: contact.contactNumber.substr(contact.contactNumber.indexOf(' ') + 1),
          contactNoCountryCode: contact.contactNumber.substr(0, contact.contactNumber.indexOf(' ')),
          isPrimary: true,
          callInitiationId: this.initiationId,
          callInitiationContactId: null,
          createdUserId: this.userData.userId
        }

        let primaryData = this.callInitiationContacts.find(x => x.isPrimary == true)
        if (primaryData) {
          this.callInitiationContacts.find(x => x.isPrimary == true).keyholderId = contact.isNewContact ? null : contact.keyHolderId
          this.callInitiationContacts.find(x => x.isPrimary == true).contactName = contact.keyHolderName
          this.callInitiationContacts.find(x => x.isPrimary == true).contactNo = contact.contactNumber.substr(contact.contactNumber.indexOf(' ') + 1)
          this.callInitiationContacts.find(x => x.isPrimary == true).contactNoCountryCode = contact.contactNumber.substr(0, contact.contactNumber.indexOf(' '))
          this.callInitiationContacts.find(x => x.isPrimary == true).createdUserId = this.userData.userId
        } else {
          this.callInitiationContacts.push(contactData)
        }
      }
    })

    this.initiatonCallCreationForm.get('basicInfo.alternateContactId').valueChanges.subscribe(val => {
      if (!val) {
        this.initiatonCallCreationForm.get('basicInfo.alternateContactNumber').setValue('');
        this.initiatonCallCreationForm = clearFormControlValidators(this.initiatonCallCreationForm, ["alternateContactNumber"]);
        return
      }
      this.initiatonCallCreationForm.get("basicInfo.alternateContactNumber").setValidators([Validators.required]);
      this.initiatonCallCreationForm.get('basicInfo').updateValueAndValidity()
      let contact = this.alternativContactList.find(x => x.keyHolderId == val)
      if (contact) {
        this.contactAlternativeNumberList = []
        if (contact.mobile1) {
          this.contactAlternativeNumberList.push({ id: contact.mobile1, value: contact.mobile1 })
        }
        if (contact.mobile2) {
          this.contactAlternativeNumberList.push({ id: contact.mobile2, value: contact.mobile2 })
        }
        if (contact.officeNo) {
          this.contactAlternativeNumberList.push({ id: contact.officeNo, value: contact.officeNo })
        }
        if (contact.premisesNo) {
          this.contactAlternativeNumberList.push({ id: contact.premisesNo, value: contact.premisesNo })
        }
        if (contact.isNewContact) {
          if (contact.contactNumber) { //for new contact
            this.contactAlternativeNumberList.push({ id: contact.contactNumber, value: contact.contactNumber })
          }
        }

        let contactData = {
          keyholderId: contact.isNewContact ? null : contact.keyHolderId,
          contactName: contact?.keyHolderName,
          contactNo: contact?.contactNumber?.substr(contact?.contactNumber?.indexOf(' ') + 1),
          contactNoCountryCode: contact?.contactNumber?.substr(0, contact?.contactNumber?.indexOf(' ')),
          isPrimary: false,
          callInitiationId: this.initiationId,
          callInitiationContactId: null,
          createdUserId: this.userData.userId
        }
        let primaryData = this.callInitiationContacts.find(x => x.isPrimary == false)
        if (primaryData) {
          this.callInitiationContacts.find(x => x.isPrimary == false).keyholderId = contact.isNewContact ? null : contact.keyHolderId
          this.callInitiationContacts.find(x => x.isPrimary == false).contactName = contact.keyHolderName
          this.callInitiationContacts.find(x => x.isPrimary == false).contactNo = contact.contactNumber.substr(contact.contactNumber.indexOf(' ') + 1)
          this.callInitiationContacts.find(x => x.isPrimary == false).contactNoCountryCode = contact.contactNumber.substr(0, contact.contactNumber.indexOf(' '))
          this.callInitiationContacts.find(x => x.isPrimary == false).createdUserId = this.userData.userId
        } else {
          this.callInitiationContacts.push(contactData)
        }
      }
    })

    this.initiatonCallCreationForm.get('basicInfo.contactNumber').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      let primaryData = this.callInitiationContacts.find(x => x.isPrimary == true)
      if (primaryData) {
        this.callInitiationContacts.find(x => x.isPrimary == true).contactNo = val.substr(val.indexOf(' ') + 1)
        this.callInitiationContacts.find(x => x.isPrimary == true).contactNoCountryCode = val.substr(0, val.indexOf(' '))
        this.callInitiationContacts.find(x => x.isPrimary == true).createdUserId = this.userData.userId
      }

    })

    this.initiatonCallCreationForm.get('basicInfo.alternateContactNumber').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      let primaryData = this.callInitiationContacts.find(x => x.isPrimary == false)
      if (primaryData) {
        this.callInitiationContacts.find(x => x.isPrimary == false).contactNo = val.substr(val.indexOf(' ') + 1)
        this.callInitiationContacts.find(x => x.isPrimary == false).contactNoCountryCode = val.substr(0, val.indexOf(' '))
      }

    })

    this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').valueChanges
      .pipe(
        filter(val => val ? val.length == 5 : null),
        takeUntil(this.ngUnsubscribe),
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          if (!val) {
            return
          }
          return of(this.getNextavailableAppointment());
        })
      )
      .subscribe();
    this.initiatonCallCreationForm.get('basicInfo.specialProjectId').valueChanges.subscribe(val => {
      if (val) {
        let projectData = this.projectList.find(x => x.id.toLowerCase() == val.toLowerCase());
        if (!projectData) return;
        this.initiationDetailsInfo.projectEndDate = projectData['projectEndDate'];
      }
    })

    this.initiatonCallCreationForm.get('basicInfo.isInstallPreCheck').valueChanges.subscribe(val => {
      if (val) {
        this.initiatonCallCreationForm = clearFormControlValidators(this.initiatonCallCreationForm, ["debtorId", "contactId", "contactNumber"]);//"isAddressVerified", 
        this.initiatonCallCreationForm.get('isAddressVerified').setValue(false);
        this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').setValidators([]);
        this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').setValidators([]);
        this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').setValidators([]);
        this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').setValidators([]);
        this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').updateValueAndValidity();
        this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').updateValueAndValidity();
        this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').updateValueAndValidity();
        this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').updateValueAndValidity();
        this.callCreationTemplate = true
        return
      }
      this.initiatonCallCreationForm.get('basicInfo.debtorId').setValidators([Validators.required]);
      this.initiatonCallCreationForm.get('basicInfo.contactId').setValidators([Validators.required]);
      this.initiatonCallCreationForm.get('basicInfo.contactNumber').setValidators([Validators.required]);
      this.initiatonCallCreationForm.get('basicInfo').updateValueAndValidity();
      this.initiatonCallCreationForm.get('basicInfo.isAddressVerified').setValue(null);
      this.initiatonCallCreationForm.get('basicInfo.debtorId').updateValueAndValidity();
      this.initiatonCallCreationForm.get('basicInfo.isAddressVerified').updateValueAndValidity();
      this.initiatonCallCreationForm.get('basicInfo.contactId').updateValueAndValidity();
      this.initiatonCallCreationForm.get('basicInfo.contactNumber').updateValueAndValidity();
      this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').setValidators([Validators.required]);
      this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').setValidators([Validators.required]);
      this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').setValidators([Validators.required]);
      this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').setValidators([Validators.required]);
      this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').updateValueAndValidity();
      this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').updateValueAndValidity();
      this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').updateValueAndValidity();
      this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').updateValueAndValidity();
      this.callCreationTemplate = false


    })

  }

  getSystemSubCategory(val) {
    if(val) {
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE_SUB_CATEGORY, undefined, null, prepareRequiredHttpParams(
          { SystemTypeId: val, ShowDescriptionOnly: true }
        )).subscribe((response) => {
          this.SystemTypeSubCategoryList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
  }

  getServiceSubType(val) {
    if(val) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_SERVICE_SUB_TYPES, undefined, null, prepareRequiredHttpParams(
          { SystemTypeSubCategoryId: val, ShowDescriptionOnly: true }
        )).subscribe((response) => {
          this.ServiceSubTypeList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
  }

  getFaultDescription(val) {
    if(val) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_SERVICE_SUB_TYPES, undefined, null, prepareRequiredHttpParams(
          { ServiceSubTypeId: val, ShowMapped: true }
        )).subscribe((response) => {
          this.faultDescriptionList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);

        })
    }
  }

  getJobType(val) {
    if(val) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SKILL_MATRIX_CONFIG_JOBTYPE_SKILL_LEVEL, undefined, null
        , prepareRequiredHttpParams(
          {
            ServiceSubTypeId: this.initiatonCallCreationForm.getRawValue().serviceInfo.serviceSubTypeId,
            FaultDescriptionId: val
          }
        )
      ).subscribe((response) => {
        this.jobTypeList = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
        let faultDesc = this.faultDescriptionList.find(x => x.faultDescriptionId == val)
  
        if (faultDesc) {
          this.initiatonCallCreationForm.get('serviceInfo.jobTypeId').setValue(faultDesc.jobTypeId ? faultDesc.jobTypeId : this.initiationDetailsInfo.jobTypeId)
        }
      })
    }
  }

  getEstimateTime(val) {
    let jobType = this.jobTypeList.find(x => x.jobTypeId == Number(val))
  if (this.callType == '2') { // service call

    if (jobType) {
      if (this.isManualChange) {
        let estimatedSplt = jobType.estimatedTime?.split(':')
        this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(jobType ? estimatedSplt[0] + ':' + estimatedSplt[1] : null)
        this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(jobType ? estimatedSplt[0] + ':' + estimatedSplt[1] : null)
      } else {
        if (this.initiationDetailsInfo?.appointmentInfo?.estimatedTime) {
          this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(this.initiationDetailsInfo?.appointmentInfo?.estimatedTime)
        } else {
          let estimatedSplt = jobType.estimatedTime?.split(':')
          this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(jobType ? estimatedSplt[0] + ':' + estimatedSplt[1] : null)
          this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(jobType ? estimatedSplt[0] + ':' + estimatedSplt[1] : null)
        }
      }
    } else {
      this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(this.initiationDetailsInfo?.appointmentInfo ? this.initiationDetailsInfo?.appointmentInfo?.estimatedTime : null)
      this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(this.initiationDetailsInfo?.appointmentInfo ? this.initiationDetailsInfo?.appointmentInfo?.estimatedTime : null)
    }
  } 
  }

  onSelectTime(event) {
    this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').setValue(event)
    let estimatedTime = this.getFormatEstimationTime()
    let spltTime = estimatedTime?.split(':')
    let addToMints1 = this.momentService.addHoursMinits(event, spltTime[0], spltTime[1])
    this.initiatonCallCreationForm.get('appointmentInfo.scheduledEndDate').setValue(new Date(addToMints1))
  }

  getFormatEstimationTime() {

    var estimatedTime: string = this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value
    return estimatedTime
  }

  getNextavailableAppointment() {

    if (!this.initiationId || !this.initiationDetailsInfo?.basicInfo?.callInitiationId) {
      return
    }
    let otherParams = {}
    otherParams['CallInitiationId'] = this.initiationId ? this.initiationId : this.initiationDetailsInfo?.basicInfo?.callInitiationId ? this.initiationDetailsInfo?.basicInfo?.callInitiationId : null
    otherParams['NoOfDates'] = 1

    let estimatedTime = this.getFormatEstimationTime()
    otherParams['estimatedTime'] = estimatedTime
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CAPACITY_APPOINTMETN_SLOT, null, false,
      prepareRequiredHttpParams(otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.resources && response.statusCode === 200) {
          let nextAvailableDateTime = this.momentService.setTimetoRequieredDate(response.resources[0].appointmentDate, response.resources[0].appointmentTimeFrom)
          this.initiatonCallCreationForm.get('appointmentInfo').get('nextAvailableAppointment').setValue(new Date(nextAvailableDateTime))
          if (this.initiatonCallCreationForm.get('appointmentInfo').get('nextAvailableAppointmentCheckbox').value) {
            this.onSelectTime(nextAvailableDateTime)
          }
        } else {
          this.initiatonCallCreationForm.get('appointmentInfo').get('nextAvailableAppointment').setValue(null)
          if (this.initiatonCallCreationForm.get('appointmentInfo').get('nextAvailableAppointmentCheckbox').value) {
            this.initiatonCallCreationForm.get('appointmentInfo').get('scheduledStartDate').setValue(null)
            this.initiatonCallCreationForm.get('appointmentInfo').get('scheduledEndDate').setValue(null)
          }
        }
      });

  }

  uploadPOFiles(file) {
    this.fileList = [];
    if (file && file.length == 0)
      return;
    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }
    const supportedExtensions = [
      'jpeg',
      'jpg',
      'png',
      'gif',
      'pdf',
      'doc',
      'docx',
      'xls',
      'xlsx',
    ];
    for (let i = 0; i < file.length; i++) {
      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension)) {
        let filename = this.fileList.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push(selectedFile.name);
          this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments')?.setValue(selectedFile.name);
        }
      } else {
        this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
      }
    }
    this.onAddPOFiles()
  }

  onAddPOFiles(): void {
    if (this.fileList.length == 0) {
      return
    }
    let data = {
      CallInitiationId: this.initiationDetailsInfo?.basicInfo?.callInitiationId,
      CreatedUserId: this.userData.userId,
      CallInitiationDocumentType: 'NKA PO',
    }
    let formData = new FormData();
    formData.append('callInitiationDocumentDTO', JSON.stringify(data));
    this.fileList.forEach(file => {
      formData.append('file', file);
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_DOCUMENTS, formData, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          const currentFileUrl = response?.resources?.find(el => el?.docName == this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments').value);
          this.initiatonCallCreationForm.get('nkaServiceCall.attachmentUrl').setValue(currentFileUrl?.path);
          if (this.isPOInfoDisabled()) {
            this.initiatonCallCreationForm.get('nkaServiceCall.poNumber').enable();
            this.initiatonCallCreationForm.get('nkaServiceCall.poAmount').enable();
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onOpenAttachment() {
    if (this.initiatonCallCreationForm.get('nkaServiceCall.attachmentUrl').value) {
      var link = document.createElement("a");
      if (link.download !== undefined) {
        link.setAttribute("href", this.initiatonCallCreationForm.get('nkaServiceCall.attachmentUrl').value);
        link.setAttribute("download", this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments').value);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }

  isPOInfoDisabled() {
    return this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments')?.value
      && this.initiationDetailsInfo?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po required'
      && this.initiationDetailsInfo?.basicInfo?.isNKACustomer && this.initiationDetailsInfo?.nkaServiceCall?.ownedUserId == this.userData?.userId;
  }

  isPOAcceptDisabled() {
    return (this.initiationDetailsInfo?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po supplied' || this.initiationDetailsInfo?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po rejected') && this.initiationDetailsInfo?.nkaServiceCall?.isPOAccepted == null;
  }

  //Get Details
  getInitiationCallDetailsById(): Observable<IApplicationResponse> {
    if (this.callType == '2') { // service call
      if (this.initiationId) {
        return this.crudService.get(
          ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.RADIO_REMOVAL_CALL,
          this.initiationId);
      }
      return this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.RADIO_REMOVAL_CALL_CUSTOMER, null, false,
        prepareRequiredHttpParams({ customerId: this.customerId, addressId: this.addressId }))
    }
  };

  getCallInitiationDetailsById() {
    if (this.initiationId || this.customerId) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.getInitiationCallDetailsById().subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.initiationDetailsInfo = response?.resources;
          this.callInitiationContacts = response?.resources?.contacts ? response?.resources?.contacts : []
          this.getContactListDropdown(response?.resources?.appointmentInfo ? response?.resources?.appointmentInfo?.quotationVersionId : null);

          if (this.callInitiationContacts.length > 0) {
            this.callInitiationContacts.forEach(element => {
              if (!element.keyholderId) {
                let newcontactData = {
                  keyHolderId: element.callInitiationContactId,
                  keyHolderName: element.contactName,
                  contactNumber: element.contactNoCountryCode + ' ' + element.contactNo,
                  isNewContact: true
                }
                if (element.isPrimary) {
                  this.contactList.push(newcontactData)
                } else {
                  this.alternativContactList.push(newcontactData)
                }
              }
              if (this.callType == '2') { // service call
                if (element.keyholderId) {
                }
              }
              if (element.isPrimary) {
                this.initiatonCallCreationForm.get('basicInfo.contactId').setValue(element.keyholderId ? element.keyholderId : element.callInitiationContactId)
                this.initiatonCallCreationForm.get('basicInfo.contactNumber').setValue(element.contactNumber ? element.contactNumber : element.contactNoCountryCode + ' ' + element.contactNo)
              } else {
                this.initiatonCallCreationForm.get('basicInfo.alternateContactId').setValue(element.keyholderId ? element.keyholderId : element.callInitiationContactId)
                this.initiatonCallCreationForm.get('basicInfo.alternateContactNumber').setValue(element.contactNumber ? element.contactNumber : element.contactNoCountryCode + ' ' + element.contactNo)

              }
            });
          }

          this.initiatonCallCreationForm.patchValue({
            partitionIds: [],
            techInstruction: response?.resources?.techInstruction,
            notes: response?.resources?.notes,
          })

          this.initiatonCallCreationForm.controls['basicInfo'].patchValue({
            callInitiationId: response?.resources?.basicInfo?.callInitiationId,
            customerId: response?.resources?.basicInfo?.customerId,
            quotationNumber: response?.resources?.basicInfo?.quotationNumber,
            isNew: response?.resources?.basicInfo?.isNew,
            isScheduled: response?.resources?.basicInfo?.isScheduled,
            isTempDone: response?.resources?.basicInfo?.isTempDone,
            isInvoiced: response?.resources?.basicInfo?.isInvoiced,
            isOnHold: response?.resources?.basicInfo?.isOnHold,
            isVoid: response?.resources?.basicInfo?.isVoid,
            isReferToSales: response?.resources?.basicInfo?.isReferToSales,
            isInstallPreCheck: response?.resources?.basicInfo?.isInstallPreCheck,
            isInWarrenty: response?.resources?.basicInfo?.isInWarrenty,
            isRecall: response?.resources?.basicInfo?.isRecall,
            isCode50: response?.resources?.basicInfo?.isCode50,
            isPRCall: response?.resources?.basicInfo?.isPRCall,
            isSaveOffer: response?.resources?.basicInfo?.isSaveOffer,
            isCon: response?.resources?.basicInfo?.isCon,
            isAddressVerified: response?.resources?.basicInfo?.isAddressVerified ? true : null,
            priorityId: response?.resources?.basicInfo?.priorityId == 0 ? 100 : response?.resources?.basicInfo?.priorityId,
            panelTypeConfigId: response?.resources?.basicInfo?.panelTypeConfigId ? response?.resources?.basicInfo?.panelTypeConfigId : '',
            specialProjectId: response?.resources?.basicInfo?.specialProjectId,
            projectEndDate: response?.resources?.basicInfo?.projectEndDate,
            addressId: response?.resources?.basicInfo?.addressId,
            debtorId: response?.resources?.basicInfo?.debtorId ? response?.resources?.basicInfo?.debtorId : '',
          });

          this.initiatonCallCreationForm.controls['serviceInfo'].patchValue({
            callInitiationServiceInfoId: response?.resources?.serviceInfo?.callInitiationServiceInfoId,
            systemTypeId: response?.resources?.serviceInfo?.systemTypeId ? response?.resources?.serviceInfo?.systemTypeId : '',
            systemTypeSubCategoryId: response?.resources?.serviceInfo?.systemTypeSubCategoryId ? response?.resources?.serviceInfo?.systemTypeSubCategoryId : '',
            serviceSubTypeId: response?.resources?.serviceInfo?.serviceSubTypeId ? response?.resources?.serviceInfo?.serviceSubTypeId : '',
            faultDescriptionId: response?.resources?.serviceInfo?.faultDescriptionId ? response?.resources?.serviceInfo?.faultDescriptionId : '',
            jobTypeId: response?.resources?.serviceInfo?.jobTypeId ? response?.resources?.serviceInfo?.jobTypeId : '',
            description: response?.resources?.serviceInfo?.description,
          }, {emitEvent: false});
          this.getSystemSubCategory(response?.resources?.serviceInfo?.systemTypeId);
          this.getServiceSubType(response?.resources?.serviceInfo?.systemTypeSubCategoryId);
          this.getFaultDescription(response?.resources?.serviceInfo?.serviceSubTypeId);
          this.getJobType(response?.resources?.serviceInfo?.faultDescriptionId);
          this.getEstimateTime(response?.resources?.serviceInfo?.jobTypeId);
          if (response?.resources?.appointmentInfo) {
            this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').setValue(response?.resources?.appointmentInfo?.scheduledStartDate ? new Date(response?.resources?.appointmentInfo?.scheduledStartDate) : null);
            this.initiatonCallCreationForm.get('appointmentInfo.scheduledEndDate').setValue(response?.resources?.appointmentInfo?.scheduledEndDate ? new Date(response?.resources?.appointmentInfo?.scheduledEndDate) : null);
            this.initiatonCallCreationForm.get('appointmentInfo.appointmentId').setValue(response?.resources?.appointmentInfo?.appointmentId ? response?.resources?.appointmentInfo?.appointmentId : null);
            this.initiatonCallCreationForm.get('appointmentInfo.appointmentLogId').setValue(response?.resources?.appointmentInfo?.appointmentLogId ? response?.resources?.appointmentInfo?.appointmentLogId : null);
            if (response?.resources?.appointmentInfo?.estimatedTime) {
              let estimatedTime = response?.resources?.appointmentInfo?.estimatedTime?.split(':')
              let estimatTimeIs = estimatedTime[0] + ':' + estimatedTime[1]
              this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(estimatTimeIs);
              this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(estimatTimeIs);
              this.initiationDetailsInfo.appointmentInfo.estimatedTime = estimatTimeIs
              this.initiationDetailsInfo.appointmentInfo.estimatedTimeStatic = estimatTimeIs
            } else {
              this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(null);
              this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(null);
            }
            this.initiatonCallCreationForm.get('appointmentInfo.technician').setValue(response?.resources?.appointmentInfo?.technician ? response?.resources?.appointmentInfo?.technician : "");
            this.initiatonCallCreationForm.get('appointmentInfo.actualTime').setValue(response?.resources?.appointmentInfo?.actualTime ? response?.resources?.appointmentInfo?.actualTime : null);
            this.initiatonCallCreationForm.get('appointmentInfo.preferredDate').setValue(response?.resources?.appointmentInfo?.preferredDate ? new Date(response?.resources?.appointmentInfo?.preferredDate) : null);
            this.initiatonCallCreationForm.get('appointmentInfo.preferredTime').setValue(response?.resources?.appointmentInfo?.preferredTime ? response?.resources?.appointmentInfo?.preferredTime : null);
            this.initiatonCallCreationForm.get('appointmentInfo.isAppointmentChangable').setValue(response?.resources?.appointmentInfo?.isAppointmentChangable ? response?.resources?.appointmentInfo?.isAppointmentChangable : false);
            this.initiatonCallCreationForm.get('appointmentInfo.techArea').setValue(response?.resources?.appointmentInfo?.techArea ? response?.resources?.appointmentInfo?.techArea : null);
          }
        }
      })

    }
  }

  getContactListDropdown(quotationVersionId) {
    this.contactList = []
    this.alternativContactList = []
    if (!this.customerId) {
      return
    }
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.UX_CUSTOMR_KEYHOLDERS_WITH_CUSTOMER_ID, undefined, null, prepareRequiredHttpParams({ CustomerId: this.customerId }
      )).subscribe((response) => {
        if (response.resources) {
          if (response.resources.length > 0) {
            response.resources.forEach(element => {
              element.contactNumber = element.mobile1
              element.keyHolderName = element.customerName
              element.isNewContact = false
              this.contactList.push(element)
              this.alternativContactList.push(element)
            });
          }
          this.initiatonCallCreationForm.get('basicInfo.contactId').setValue(this.initiatonCallCreationForm.get('basicInfo.contactId').value)
          this.initiatonCallCreationForm.get('basicInfo.alternateContactId').setValue(this.initiatonCallCreationForm.get('basicInfo.alternateContactId').value)
        }
        this.rxjsService.setGlobalLoaderProperty(false);

      })
  }

  isAddressChecked(val) {
    this.initiatonCallCreationForm.get('basicInfo.isAddressVerified').setValue(val.checked ? true : null)
  }

  changeStatus(data) {

  }

  openAddContact(val) {
    const header = 'Add New '+ (val ? 'Contact' : 'Alternate Contact');
    let contactList = this.contactList;
    if(!val) {
      contactList = this.alternativContactList;
    }
    const ref = this.dialogService.open(CallContactDialogComponent, {
      header: header,
      baseZIndex: 1000,
      width: '700px',
      closable: false,
      showHeader: false,
      data: {
        initiationId: this.initiationId,
        isPrimary: val,
        customerId: this.customerId,
        createdUserId: this.userData?.userId,
        contactList: contactList,
      },
    });
    ref.onClose.subscribe((res) => {
      if (res) {
        if (res?.isPrimary) {
          this.contactList.push(res);
          this.initiatonCallCreationForm.get('basicInfo.contactId').setValue(res?.keyHolderId);
          this.initiatonCallCreationForm.get('basicInfo.contactNumber').setValue(res?.contactNumber);
        } else {
          this.alternativContactList.push(res);
          this.initiatonCallCreationForm.get('basicInfo.alternateContactId').setValue(res?.keyHolderId);
          this.initiatonCallCreationForm.get('basicInfo.alternateContactNumber').setValue(res?.contactNumber);
        }
      }
    });
  }

  getRating() {
    if (this?.initiationDetailsInfo) {
      return this?.initiationDetailsInfo?.basicInfo?.callRating ? Math.round(this?.initiationDetailsInfo?.basicInfo?.callRating) : '';
    }
  }

  onChangePartitions(event) {
    if (this.initiatonCallCreationForm.getRawValue().installationPartitions.length > 0) {
      let count = this.initiatonCallCreationForm.getRawValue().installationPartitions.filter(x => x.mainPartition == true)
      if (count.length > 0) {
        let estimatedSplt = this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').value.split(':')
        let hours = Number(estimatedSplt[0]) * count.length
        let minuts = Number(estimatedSplt[1]) * count.length
        let emptyTimeDate = this.momentService.setTime('0:0')
        let addHoursMints = this.momentService.addHoursMinits(emptyTimeDate, hours, minuts)
        let convetRailwayTime = this.momentService.convertNormalToRailayTime(new Date(addHoursMints))
        let finaltestimateSplt = convetRailwayTime.split(':')
        this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(Number(finaltestimateSplt[0]) + ':' + Number(finaltestimateSplt[1]))
      }
    }
  }

  onSendJobCardEmail() {
    let data = {
      callInitiationId: this.initiationDetailsInfo?.basicInfo?.callInitiationId,
      createdUserId: this.userData.userId
    }
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.THIRD_PARTY_SUBCONTRACTOR_JOBCARD, data)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
        }
      });
  }

  guardaValorCEPFormatado(evento) {
    this.cepFormatadoValue = evento;
    this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(evento)
  }

  isTechnicalAreaMngr() {
    if (this.userData.roleName.toLowerCase() == 'technical area manager') {
      return true
    } else {
      return false
    }
  }

  onSubmit(bol?) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (!this.isTechnicalAreaMngr()) {
      this.rxjsService.getFormChangeDetectionProperty().subscribe(val => {
      })
      if (this.initiatonCallCreationForm.invalid) {
        this.initiatonCallCreationForm.markAllAsTouched();
        this.isSubmitted = false;
        return false;
      }
    }
    this.isSubmitted = true;
    let Obj = this.initiatonCallCreationForm.getRawValue();
    Obj.appointmentInfo.scheduledStartDate = Obj.appointmentInfo.scheduledStartDate ? Obj.appointmentInfo.scheduledStartDate.toLocaleString() : null
    Obj.appointmentInfo.scheduledEndDate = Obj.appointmentInfo.scheduledEndDate ? Obj.appointmentInfo.scheduledEndDate.toLocaleString() : null
    let saveObj = {
      createdUserId: this.userData.userId,
      appointmentId: this.initiationDetailsInfo.appointmentId,
      notes: Obj.notes,
      techInstruction: Obj.techInstruction,
      partitionIds: [],
      basicInfo: {
        callInitiationId: this.initiationDetailsInfo?.basicInfo?.callInitiationId ? this.initiationDetailsInfo?.basicInfo?.callInitiationId : this.initiationId,
        customerId: this.customerId ? this.customerId : this.initiationDetailsInfo?.basicInfo?.customerId,
        addressId: this.addressId ? this.addressId : this.initiationDetailsInfo?.basicInfo.addressId,
        debtorId: Obj?.basicInfo?.debtorId ? Obj?.basicInfo?.debtorId : null,
        isAddressVerified: Obj?.basicInfo?.isAddressVerified,
        isNew: this.initiationDetailsInfo?.basicInfo?.isNew,
        isScheduled: this.initiationDetailsInfo?.basicInfo?.isScheduled,
        isRecall: this.initiationDetailsInfo?.basicInfo?.isRecall,
        priorityId: Obj?.basicInfo?.priorityId,
        panelTypeConfigId: Obj?.basicInfo?.panelTypeConfigId ? Obj?.basicInfo?.panelTypeConfigId : null,
        callInitiationStatusId: this.initiationDetailsInfo?.basicInfo?.callInitiationStatusId ? this.initiationDetailsInfo?.basicInfo?.callInitiationStatusId : null,
        technicianCallTypeId: this.initiationDetailsInfo?.basicInfo?.technicianCallTypeId ? this.initiationDetailsInfo?.basicInfo?.technicianCallTypeId : null,
      },
      serviceInfo: {
        callInitiationServiceInfoId: null,
        systemTypeId: Obj?.serviceInfo?.systemTypeId,
        systemTypeSubCategoryId: Obj?.serviceInfo?.systemTypeSubCategoryId,
        serviceSubTypeId: Obj?.serviceInfo?.serviceSubTypeId,
        faultDescriptionId: Obj?.serviceInfo?.faultDescriptionId,
        jobTypeId: Obj?.serviceInfo?.jobTypeId,
        description: Obj?.serviceInfo?.description ? Obj?.serviceInfo?.description : null,
      },
      appointmentInfo: null,
      callInitiationContacts: this.callInitiationContacts,
    }
    if (Obj?.basicInfo?.partitions) {
      Obj?.basicInfo?.partitions.forEach((element) => {
        if (element.mainPartition == true) {
          saveObj.partitionIds.push(element.partitionId)
        }
      })
    }
    // this.initiatonCallCreationForm.value.ismainPartation == true ? saveObj.basicInfo.partitionIds.push(this.initiationpartitions.find(x => x.partitionCode == this.initiatonCallCreationForm.value.mainPartationCustomerId).partitionId) : saveObj.basicInfo.partitionIds;
    if (this.initiatonCallCreationForm.getRawValue()?.basicInfo?.ismainPartation) {
      if (this.initiationDetailsInfo?.partitions) {
        saveObj.partitionIds.push(this.initiationDetailsInfo?.partitions.find(x => x.partitionCode == this.initiatonCallCreationForm.getRawValue()?.basicInfo?.mainPartationCustomerId.split(" ")[0]).partitionId)
      } else {
        saveObj.partitionIds
      }
    } else {
      saveObj.partitionIds
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RADIO_REMOVAL_CALL, saveObj)
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.isSubmitted = false;
      if (response.isSuccess == true && response.statusCode == 200) {
        this.initiatonCallCreationForm.markAsUntouched()       // this.navigateToPage();
        if (response.resources) {
          this.initiationId = response.resources
          if (bol == 1) {
            this.isNavigationFirstTime = false
            this.navigateToTechAllocationParams()
          } else if (bol == 2) {
            let troubleShootObject = {
              customerId: this.customerId,
              initiationId: this.initiationId,
              customerAddressId: this.addressId,
              callType: this.callType
            }
            this.isNavigationFirstTime = false;
          }
        }
      }
      this.initiatonCallCreationForm.get('basicInfo.isAddressVerified').markAsUntouched();
      this.isManualChange = false
      this.getCallInitiationDetailsById();
    });
  }

  cancelClick() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.addressId, 
      radioRemovalWorkListId: this.radioRemovalWorkListId, 
      isRadio: true,
      customerTab: 3,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }


  navigateToTechAllocation() {
    if (this.initiatonCallCreationForm.invalid) {
      this.autoSubmitClick() // to show error validation
      return
    }
    if (this.initiationDetailsInfo?.basicInfo?.isExecuGuardCustomer && this.initiationDetailsInfo?.execuGuardServiceCall) {
      if (this.initiationDetailsInfo?.execuGuardServiceCall?.execuGuardServiceCallStatusName != 'Approved') {
        this.snackbarService.openSnackbar('Execuguard Request has not been approved. Unable to schedule a call', ResponseMessageTypes.WARNING);
        return;
      }
    }
    if (this.initiationDetailsInfo?.basicInfo?.isNKACustomer && this.initiationDetailsInfo?.nkaServiceCall) {
      if (!this.initiationDetailsInfo?.nkaServiceCall?.isPOAccepted) {
        this.snackbarService.openSnackbar('NKA PO Request has not been accepted. Unable to schedule a call', ResponseMessageTypes.WARNING);
        return;
      }
    }
    if (this.initiationDetailsInfo?.basicInfo?.isNKACustomer && this.userData?.userId == this.initiationDetailsInfo?.nkaServiceCall?.ownedUserId && this.initiationDetailsInfo?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() != 'po supplied') {
      this.snackbarService.openSnackbar('The customer must fill the po details', ResponseMessageTypes.WARNING);
      return;
    }
    if (!this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value) {
      this.initiatonCallCreationForm.markAllAsTouched();
      return
    }
    if (!this.initiationId) {
      this.autoSaveAlertNavigate(1)
      return
    } else {
       this.navigateToTechAllocationParams()
    }
  }

  navigateToTechAllocationParams() {
    this.router.navigate(['/customer/tech-allocation-calender-view'], {
      queryParams: {
        nextAppointmentDate: this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').value ? this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').value : new Date(),
        installationId: this.initiationDetailsInfo?.basicInfo ? this.initiationDetailsInfo?.basicInfo.callInitiationId ? this.initiationDetailsInfo?.basicInfo.callInitiationId : this.initiationId : this.initiationId,
        customerId: this.initiationDetailsInfo?.basicInfo ? this.initiationDetailsInfo?.basicInfo.customerId : this.customerId,
        customerAddressId: this.initiationDetailsInfo?.basicInfo ? this.initiationDetailsInfo?.basicInfo.addressId : this.addressId,
        appointmentId: this.initiatonCallCreationForm.get('appointmentInfo.appointmentId').value ? this.initiatonCallCreationForm.get('appointmentInfo.appointmentId').value : null,
        appointmentLogId: this.initiatonCallCreationForm.get('appointmentInfo.appointmentLogId').value ? this.initiatonCallCreationForm.get('appointmentInfo.appointmentLogId').value : null,
        estimatedTime: this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value ? this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value : null,
        callType: this.callType ? this.callType : null , // 1- installation call, 2- service call
        radioRemovalWorkListId: this.radioRemovalWorkListId ? this.radioRemovalWorkListId : 1
      }
    });
  }

  autoSubmitClick() {
    let element: HTMLElement = document.querySelector('.autoSave') as HTMLElement;
    element.click();
  }

  autoSaveAlertNavigate(val) {
    if (this.isNavigationFirstTime) {
        if (this.initiatonCallCreationForm.touched) {
          if (confirm("You have unsaved changes! Do you want to save this?")) {
          this.autoSubmitClick()
          if (this.initiatonCallCreationForm.invalid || !this.initiatonCallCreationForm?.get('isAddressVerified')?.value) {
            this.initiatonCallCreationForm.markAllAsTouched();
            return false;
          }
          this.onSubmit(val) // 1- for calendar navigation
        } else {
          if (val == 1) {
            this.snackbarService.openSnackbar('Please save and proceed!', ResponseMessageTypes.WARNING);
          } else {
            return true
          }
        }
      } 
    }
  }

  canDeactivate() {
    // autosave functionality starts here
    if (this.isNavigationFirstTime) {
        if (this.initiatonCallCreationForm.touched) {
          if (confirm("You have unsaved changes! Do you want to save this?")) {
          this.autoSubmitClick()
          if (this.initiatonCallCreationForm.invalid || !this.initiatonCallCreationForm?.get('isAddressVerified')?.value) {
            this.initiatonCallCreationForm.markAllAsTouched();
            return false;
          }
          return true
        } else {
          return true
        }
      } else {
        return true
      }
    } else {
      return true
    }
    // autosave functionality ends here

  }

  openActualTimeDialog(){

  }

}
