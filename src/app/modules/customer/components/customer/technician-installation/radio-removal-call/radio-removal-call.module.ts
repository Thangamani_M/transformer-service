import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { CanDeactivateGuard, LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { CallContactDialogModule } from '../call-contact-dialog/call-contact-dialog.module';
import { CallEscalationModule } from '../call-escalation-dialog/call-escalation-dialog.module';
import { RadioRemovalCallAddEditComponent } from './radio-removal-call-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
  declarations: [RadioRemovalCallAddEditComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule.forChild([
        { path: '', component: RadioRemovalCallAddEditComponent, data: { title: 'Radio Removal Call' }, canActivate:[AuthGuard], canDeactivate: [CanDeactivateGuard]}
    ]),
    FormsModule,
    LayoutModule,
    SharedModule,
    CallEscalationModule,
    ConfirmDialogModule,
    CallContactDialogModule,
  ]
})
export class RadioRemovalCallModule { }
