import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { countryCodes, formConfigs, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-call-contact-dialog',
  templateUrl: './call-contact-dialog.component.html'
})
export class CallContactDialogComponent implements OnInit {

  addNewContactForm: FormGroup;
  countryCodes = countryCodes;
  formConfigs = formConfigs;
  
  constructor(public config: DynamicDialogConfig, private formBuilder: FormBuilder, public ref: DynamicDialogRef,
    private snackbarService: SnackbarService) { }

  ngOnInit(): void {
    this.createAddNewContactForm();
  }

  btnCloseClick() {
    this.ref.close(false);
  }

  createAddNewContactForm() {
    this.addNewContactForm = this.formBuilder.group({
      contactName: ['', Validators.required],
      contactNoCountryCode: ['+27', Validators.required],
      contactNo: ['', Validators.required],
      isPrimary: [this.config?.data?.isPrimary, Validators.required],
      callInitiationId: this.config?.data?.initiationId ? this.config?.data?.initiationId : null,
      keyholderId: null,
      callInitiationContactId: null,
      customerId: this.config?.data?.customerId ? this.config?.data?.customerId : null,
      createdUserId: this.config?.data?.createdUserId,
    })
  }

  openAddContact(val) {
    this.addNewContactForm.get('contactName').setValue(null)
    this.addNewContactForm.get('contactNo').setValue(null)
    this.addNewContactForm.get('isPrimary').setValue(val)
  }

  validateAlreadyExists() {
    const contactNumber = this.addNewContactForm.value?.contactNoCountryCode + ' ' + this.addNewContactForm.value?.contactNo.replace(/\s/g, "");
    const el = this.config?.data?.contactList.find(el => el.contactNumber == contactNumber);
    if(el) {
      return el;
    }
    return false;
  }

  addContactSubmit() {
    if (this.addNewContactForm.invalid) {
      this.addNewContactForm.markAllAsTouched();
      return;
    }
    if(this.validateAlreadyExists()) {
      this.snackbarService.openSnackbar("This number is already exists", ResponseMessageTypes.WARNING);
      return;
    }
    let formData = this.addNewContactForm.value;
    formData.callInitiationId = this.config?.data?.initiationId ? this.config?.data?.initiationId : null;
    formData.contactNo = formData.contactNo.replace(/\s/g, "");
    let newcontactData = {
      keyHolderId: Math.random(),
      keyHolderName: formData.contactName,
      contactNumber: formData.contactNoCountryCode + ' ' + formData.contactNo,
      isNewContact: true,
      isPrimary: formData.isPrimary,
    }
    this.ref.close(newcontactData);
  }

}
