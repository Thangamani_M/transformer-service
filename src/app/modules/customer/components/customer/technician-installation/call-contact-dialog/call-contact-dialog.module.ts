import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CallContactDialogComponent } from '@modules/customer';

@NgModule({
  declarations: [CallContactDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule,
  ],
  exports: [CallContactDialogComponent],
  entryComponents: [CallContactDialogComponent],
})
export class CallContactDialogModule { }
