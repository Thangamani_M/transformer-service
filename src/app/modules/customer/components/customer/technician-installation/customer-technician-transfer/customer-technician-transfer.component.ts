import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { convertTreeToList, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, debounceTimeForSearchkeyword, LoggedInUserModel, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, prepareRequiredHttpParams, PrimengStatusConfirmDialogComponent, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { CUSTOMER_COMPONENT } from '@modules/customer/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-customer-technician-transfer',
  templateUrl: './customer-technician-transfer.component.html'
})
export class CustomerTechnicianTransferComponent implements OnInit {

  userData: UserLogin;
  dataList: any;
  dateFormat = 'MMM dd, yyyy';
  observableResponse: any;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties: any;
  selectedTabIndex: any = 0;
  searchColumns: any
  row: any = {}
  loading: boolean;
  columnFilterForm: FormGroup;
  searchForm: FormGroup;
  showFilterForm = false;
  consumableConfigFilterForm: FormGroup;
  divisionListFilter = [];
  status: any = [];
  technicianTypeListFilter = [];
  divisionIdsToSend = '';
  technicianTypeIdToSend = '';
  selectedDivisionOptions = [];
  selectedTechnicianTypeOptions = [];
  selectedRows: string[] = [];
  callInitiationId: string;
  // installationId: string;
  customerId: string;
  pageSize: number = 10;
  // quotationVersionId: string;
  callType = '';
  today = new Date();
  addressId:any
  actionPermissionObj: any;

  constructor(
    private crudService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    public dialogService: DialogService,
    private router: Router,
    private _fb: FormBuilder,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private momentService: MomentService,
    private snackbarService: SnackbarService,
    private datePipe: DatePipe,
    private activatedRoute: ActivatedRoute
  ) {
    this.callInitiationId = this.activatedRoute.snapshot.queryParams?.callInitiationId;
    // this.installationId = this.activatedRoute.snapshot.queryParams.installationId;
    this.customerId = this.activatedRoute.snapshot.queryParams?.customerId;
    this.addressId = this.activatedRoute.snapshot.queryParams?.addressId;
    // this.quotationVersionId = this.activatedRoute.snapshot.queryParams.quotationVersionId;
    this.callType = this.activatedRoute.snapshot.queryParams?.callType;
    // this.callInitiationId='054A79E5-6965-4766-9AB7-CFEF0BAEA4DD';

    this.primengTableConfigProperties = {
      tableCaption: "Tech Transfer List",
      // breadCrumbItems: [{ displayName: 'Technical Management', relativeRouterUrl: '' }, { displayName: 'Tech Transfer List' }],
      breadCrumbItems: [{ displayName: 'Customer Management: Technician Installation', relativeRouterUrl: '' }, {
        displayName: 'Installation', relativeRouterUrl: '/customer/manage-customers/call-initiation', queryParams: {
          customerId: this.customerId,
          customerAddressId: this.addressId,
          initiationId: this.callInitiationId,
          callType: this.callType
        }
      }, { displayName: 'Tech Transfer List' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Transferred',
            dataKey: 'technicianStockOrderCollectionItemId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            rowExpantable: true,
            rowExpantableIndex: 0,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'stockCode', header: 'Stock Code', width: '200px' },
              { field: 'stockDescription', header: 'Stock Description', width: '200px' },
              { field: 'quantity', header: 'Qty', width: '200px' },
              { field: 'technicianStockAllocation', header: 'Tech Stock Allocation', width: '200px' },
              { field: 'technicianName', header: 'Tech Name', width: '200px' }
            ],
            isInternalColumn: true,
            internalColumnKey: 'transferredTRF',
            internalColumns: [
              { field: 'createdDate', width: '200px' },
              { field: 'transferNumber', width: '200px' },
              { field: 'serialNumber', width: '200px' }
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECH_TRANSFER_LIST,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          },
          {
            caption: 'Picked',
            dataKey: 'packerJobItemId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            rowExpantable: true,
            rowExpantableIndex: 0,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'stockCode', header: 'Stock Code', width: '200px' },
              { field: 'stockDescription', header: 'Stock Description', width: '200px' },
              { field: 'quantity', header: 'Qty', width: '200px' },
              { field: 'technicianStockAllocation', header: 'Tech Stock Location Picked For', width: '200px' },
              { field: 'technicianName', header: 'Tech Name Picked For', width: '200px' }
            ],
            isInternalColumn: true,
            internalColumnKey: 'techTransferPackerTRF',
            internalColumns: [
              { field: 'jobDate', width: '200px' },
              { field: 'jobNumber', width: '200px' },
              { field: 'serialNumber', width: '200px' }
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECH_TRANSFER_PICKED_LIST,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }, {
            caption: 'Stock Return',
            dataKey: 'packerJobItemId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            rowExpantable: true,
            rowExpantableIndex: 0,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'stockCode', header: 'Stock Code', width: '200px' },
              { field: 'stockDescription', header: 'Stock Description', width: '200px' },
              { field: 'quantity', header: 'Qty', width: '200px' },
              { field: 'technicianStockAllocation', header: 'Tech Stock Location Picked For', width: '200px' },
              { field: 'technicianName', header: 'Tech Name Picked For', width: '200px' }
            ],
            isInternalColumn: true,
            internalColumnKey: 'techTransferPackerTRF',
            internalColumns: [
              { field: 'jobDate', width: '200px' },
              { field: 'jobNumber', width: '200px' },
              { field: 'serialNumber', width: '200px' }
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            // apiSuffixModel: TechnicalMgntModuleApiSuffixModels.TECH_TRANSFER_STOCK_RETURN,
            // moduleName: ModulesBasedApiSuffix.TECHNICIAN,

          }
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData();
    if (this.callInitiationId)
      this.getTechTransferListDetails(null, null, { CallInitiationId: this.callInitiationId });
    else {
      this.observableResponse = null;
      this.dataList = this.observableResponse
      this.totalRecords = 0;
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][CUSTOMER_COMPONENT.CUSTOMER]?.find(el => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD);
      if (permission) {
        let techPermission = convertTreeToList([permission?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.TECHNICAL)]);
        const objName = this.callType == '1' ? CUSTOMER_COMPONENT.INSTALLATION : this.callType == '2' ? CUSTOMER_COMPONENT.SERVICE :
          this.callType == '3' ? CUSTOMER_COMPONENT.SPECIAL_PROEJCT : '---';
        const callActionPermissionObj = techPermission?.find(el => el?.[objName])?.[objName];
        if(callActionPermissionObj) {
          const quickActionPermissionObj = callActionPermissionObj?.find(el => el?.menuName == PermissionTypes.QUICK_ACTION);
          if(quickActionPermissionObj) {
            this.actionPermissionObj = quickActionPermissionObj?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.VIEW_TRANSFERS);
          }
        }
      }
    });
  }

  getViewTransActionPermission(actionTypeMenuName) {
    return this.actionPermissionObj?.subMenu?.find(el => el?.menuName == actionTypeMenuName);
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onTabChange(event) {
    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    const queryParams = {
        customerId: this.customerId,
        addressId: this.addressId,
        callInitiationId: this.callInitiationId,
        callType: this.callType,
        tab: this.selectedTabIndex,
    }
    this.router.navigate(['/customer/manage-customers/tech-transfer'], { queryParams: queryParams })
    // this.getTechTransferListDetails(null, null, {CallInitiationId: this.callInitiationId})
    if (this.callInitiationId)
      this.getTechTransferListDetails(null, null, { CallInitiationId: this.callInitiationId });
    else {
      this.observableResponse = null;
      this.dataList = this.observableResponse
      this.totalRecords = 0;
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  getTechTransferListDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let TechnicalMgntModuleApiSuffixModels: TechnicalMgntModuleApiSuffixModels;
    TechnicalMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        let responseKey = this.selectedTabIndex == 0 ? 'transferredList' : 'techTransferPackerList';
        this.observableResponse = data.resources[responseKey];
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
    })
  }


  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams'], skipLocationChange: true })
    }
    // [breadCrumbItem['queryParams']['key']]: breadCrumbItem['queryParams']['value']
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;

      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        otherParams['IsStatus'] = this.selectedTabIndex == 0 ? true : '';
        otherParams['CallInitiationId'] = this.callInitiationId;
        this.getTechTransferListDetails(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;

      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;

      case CrudType.DELETE:

        break;
      // case CrudType.FILTER:
      //     this.displayAndLoadFilterData();
      // break;

      default:
    }
  }

  exportList() {
    if(!this.getViewTransActionPermission(PermissionTypes.EXPORT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.dataList.length != 0) {
      let fileName = 'Tech Transfer ' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
      let columnNames = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns.map(v => v.header);
      let columnInternameNames = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns.map(v => v.field);
      let header = columnNames.join(',');
      let csv = header;
      csv += '\r\n';

      this.dataList.map(c => {
        let rowValue = [];
        // csv += [c["poNumber"], c["orderDate"], c["warehouseName"],c['requisitionNumber'],c['stockOrderNumber'],c['poBarcode'],c['supplierName'],c['status']].join(',');
        columnInternameNames.forEach(element => {
          // csv += c[element] ? c[element].join(',') : '';
          rowValue.push(c[element]);
        });
        csv += rowValue.join(',');
        csv += '\r\n';
      })
      var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
      var link = document.createElement("a");
      if (link.download !== undefined) {
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
    else {
      // this.snackbarService.openSnackbar('Please select atleast one checkbox',ResponseMessageTypes.WARNING);
    }
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      ).subscribe();
  }

  requestStock() {
    if(!this.getViewTransActionPermission(PermissionTypes.EDIT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECH_TRANSFER_STOCK_RETURN, undefined, false, prepareRequiredHttpParams({
      callInitiationId: this.callInitiationId,
      createdUserId: this.userData.userId
    })).subscribe(response => {
      if (response.isSuccess && response.statusCode == 200) {

      }
      this.rxjsService.setGlobalLoaderProperty(false);

    })
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        // this.router.navigate(['/technical-management', 'consumerable-configuration', 'add-edit']);
        break;

      case CrudType.VIEW:
        // this.router.navigate(['/inventory/grv-creation/view']);
        // this.router.navigate(['/inventory/grv-creation/view'], { queryParams: { id: editableObject['goodsReturnId'] } });
        break;
    }
  }

}
