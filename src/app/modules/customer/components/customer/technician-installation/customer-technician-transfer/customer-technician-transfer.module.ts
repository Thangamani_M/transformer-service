import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { CustomerTechnicianTransferComponent } from "./customer-technician-transfer.component";

import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
    declarations: [CustomerTechnicianTransferComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: CustomerTechnicianTransferComponent, data: { title: 'Tech Transfer' }, canActivate: [AuthGuard]
        },
    ])
  ],
  entryComponents: [],
})
export class CustomerTechnicianTransferModule { }