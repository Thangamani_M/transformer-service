import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { StockSwapRequestComponent } from './stock-swap-request/stock-swap-request.component';
import { StockSwapRoutingModule } from './stock-swap-routing.module';



@NgModule({
  declarations: [StockSwapRequestComponent],
  imports: [
    CommonModule,
    StockSwapRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
  ]
})
export class StockSwapModule { }
