import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, OtherService, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { StockSwapItemsListModel, StockSwapRequestModel } from '@modules/customer/models/stock-swap-request.model';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';


@Component({
  selector: 'app-stock-swap-request',
  templateUrl: './stock-swap-request.component.html'
})
export class StockSwapRequestComponent implements OnInit {

  stockSwapRequestForm: FormGroup;
  stockSwapItems: FormArray;
  statusList: any = [];
  contactPersonList: any = [];
  contactNumberList: any = []
  swapStockCodeList: any = [];
  replaceStockCodeList: any = [];
  replaceStockDescList: any = [];
  dropdownsAndData = [];
  userData: UserLogin;
  callInitiationId: string;
  quotationVersionId: string;
  customerId: string;
  billOfMaterialId: any;
  districtId: any;
  customerAddressId: any;
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  callType: any;
  showTotalError: boolean;
  stockSwapDetails: any;

  constructor(private formBuilder: FormBuilder,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private snackbarService: SnackbarService,
    private otherService: OtherService,
    private activatedRoute: ActivatedRoute, private httpCancelService: HttpCancelService,
    private router: Router, private snakbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.callInitiationId = this.activatedRoute.snapshot.queryParams?.callInitiationId;
    this.quotationVersionId = this.activatedRoute.snapshot.queryParams?.quotationVersionId ? this.activatedRoute.snapshot.queryParams.quotationVersionId : null;
    this.customerId = this.activatedRoute.snapshot.queryParams?.customerId;
    this.callType = this.activatedRoute.snapshot.queryParams?.callType;
    this.customerAddressId = this.activatedRoute.snapshot.queryParams?.customerAddressId;
    this.billOfMaterialId = this.activatedRoute.snapshot.queryParams?.billOfMaterialId;
    this.districtId = this.activatedRoute.snapshot.queryParams?.districtId;
  }

  ngOnInit(): void {
    this.createStockSwapRequestForm();
    let otherParams = {}
    // if (this.quotationVersionId) {
    //   otherParams['QuotationVersionId'] = this.quotationVersionId
    // }
    otherParams['CustomerId'] = this.customerId;
    otherParams['customerAddressId'] = this.customerAddressId;
    otherParams['CallInitiationId'] = this.callInitiationId;
    this.dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_UX),
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.UX_CUSTOMR_KEYHOLDERS_WITH_CUSTOMER_ID, null, false, prepareGetRequestHttpParams(null, null, otherParams)),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_DETAILS, null, false, prepareGetRequestHttpParams(null, null, otherParams)),
      this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.STOCK_SWAP_DETAILS,
        undefined,
        false,
        prepareGetRequestHttpParams('', '', {
          CallInitiationId: this.callInitiationId
        })
      )
    ];
    this.loadDropdownData(this.dropdownsAndData);
  }


  loadDropdownData(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.statusList = resp.resources
              break;
            case 1:
              this.contactPersonList = resp.resources;
              break;
            case 2:
              this.swapStockCodeList = resp.resources;
              break;
            case 3:
              this.stockSwapDetails = resp.resources;
              this.onLoadValue(resp);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getReplaceStockList(event, type = 'stockCode') {
    if (!this.swapStockCodeList.warehouseId) {
      this.snakbarService.openSnackbar('Warehoue is not assigned', ResponseMessageTypes.WARNING)
      return;
    }
    if (type == 'stockCode') {
      this.stockSwapRequestForm.get('replaceStockCodeItemDescription').setValue(null);
    } else if (type == 'stockDesc') {
      this.stockSwapRequestForm.get('replaceStockCodeId').setValue(null);
    }
    let otherParams = Object.assign({},
      this.swapStockCodeList?.districtId ? { districtId: this.swapStockCodeList?.districtId } : null,
      this.swapStockCodeList?.itemPricingConfigId ? { itemPricingConfigId: this.swapStockCodeList?.itemPricingConfigId } : null,
      this.swapStockCodeList?.warehouseId ? { warehouseId: this.swapStockCodeList?.warehouseId } : null,
      { callInitiationId: this.callInitiationId },
      { stockType: 'Y' },
      this.swapStockCodeList?.technicianId ? { technicianId: this.swapStockCodeList?.technicianId } : null,
      type == 'stockCode' ? { itemCode: event.query } : null,
      type == 'stockDesc' ? { displayName: event.query } : null,
    )
    let filterdNewData = Object.entries(otherParams).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEARCH_STOCK_DETAILS_TECHNICIAN,
      undefined,
      false,
      prepareGetRequestHttpParams('', '', filterdNewData)
    ).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        if (type == 'stockCode') {
          this.replaceStockCodeList = response.resources;
        } else if (type == 'stockDesc') {
          this.replaceStockDescList = response.resources;
        }
      }
    });
  }

  createStockSwapRequestForm(stockSwapRequestModel?: StockSwapRequestModel) {
    let stockSwapRequestModelcontrol = new StockSwapRequestModel(stockSwapRequestModel);
    this.stockSwapRequestForm = this.formBuilder.group({
      stockSwapItems: this.formBuilder.array([])
    });
    Object.keys(stockSwapRequestModelcontrol).forEach((key) => {
      this.stockSwapRequestForm.addControl(key, new FormControl(stockSwapRequestModelcontrol[key]));
    });
    this.stockSwapRequestForm = setRequiredValidator(this.stockSwapRequestForm, ['swapOutReason']);
    this.stockSwapRequestForm.get('createdUserId').setValue(this.userData.userId)
    this.stockSwapRequestForm.get('modifiedUserId').setValue(this.userData.userId)
    this.onFormValueChanges();
  }

  isShowTotalError() {
    return this.showTotalError && this.stockSwapRequestForm.get('isApproval')?.touched;
  }

  onFormValueChanges() {
    this.stockSwapRequestForm.get('isApproval').valueChanges.subscribe(val => {
      if (!val) {
        this.stockSwapRequestForm.get('isNotApproval').setValue(this.stockSwapDetails?.isInternalApproval, {emitEvent: false});
        this.stockSwapRequestForm = clearFormControlValidators(this.stockSwapRequestForm, ["contactPersonId", "contactPersonNumber"]);
        if (this.getTotalValue()) {
          this.showTotalError = true;
          return
        }
        return
      }
      if (val) {
        this.showTotalError = false;
        this.stockSwapRequestForm = setRequiredValidator(this.stockSwapRequestForm, ["contactPersonId", "contactPersonNumber"]);
        this.stockSwapRequestForm.get('contactPersonId').updateValueAndValidity();
        this.stockSwapRequestForm.get('contactPersonNumber').updateValueAndValidity();
        this.stockSwapRequestForm.get('isNotApproval').setValue(false, { emitEvent: false });
      }
    })
    this.stockSwapRequestForm.get('isNotApproval').valueChanges.subscribe(value => {
      if (value) {
        this.showTotalError = false;
        this.stockSwapRequestForm.get('isApproval').setValue(false, { emitEvent: false });
        this.stockSwapRequestForm = setRequiredValidator(this.stockSwapRequestForm, ["contactPersonId", "contactPersonNumber"]);
      } else {
        this.stockSwapRequestForm.get('isApproval').setValue(this.stockSwapDetails?.isInternalApproval, {emitEvent: false});
        this.stockSwapRequestForm = clearFormControlValidators(this.stockSwapRequestForm, ["contactPersonId", "contactPersonNumber"]);
        if (this.getTotalValue()) {
          this.showTotalError = true;
          return
        }
      }
    })
    this.stockSwapRequestForm.get('contactPersonId').valueChanges.subscribe(value => {
      if (value) {
        let keyData = this.contactPersonList.find(x => x.keyHolderId == value);
        let contactNum = keyData ? keyData.mobile1 : keyData.mobile2 ? keyData.mobile2 : keyData.officeNo ? keyData.officeNo : keyData.premisesNo ? keyData.premisesNo : null;
        this.stockSwapRequestForm.get('contactPersonNumber').setValue(contactNum);
        this.contactNumberList = []
        if (keyData?.mobile1) {
          this.contactNumberList.push(keyData ? keyData.mobile1 : null)
        }
        if (keyData?.mobile2) {
          this.contactNumberList.push(keyData ? keyData.mobile2 : null)
        }
        if (keyData?.officeNo) {
          this.contactNumberList.push(keyData ? keyData.officeNo : null)
        }
        if (keyData?.premisesNo) {
          this.contactNumberList.push(keyData ? keyData.premisesNo : null)
        }
      } else {
        this.contactNumberList = [];
        this.stockSwapRequestForm.get('contactPersonNumber').setValue('');
      }
    });
  }

  //Create FormArray controls
  createStockSwapItemsListModel(stockSwapItemsListModel?: StockSwapItemsListModel): FormGroup {
    let stockSwapItemsListModelControl = new StockSwapItemsListModel(stockSwapItemsListModel);
    let formControls = {};
    Object.keys(stockSwapItemsListModelControl).forEach((key) => {
      if (key === 'stockSwapId' || key === 'stockSwapItemId') {
        formControls[key] = [{ value: stockSwapItemsListModelControl[key], disabled: false }]
      } else {
        formControls[key] = [{ value: stockSwapItemsListModelControl[key], disabled: false }, [Validators.required]]
      }
    });

    formControls['createdUserId'] = [{ value: this.userData.userId, disabled: false }]
    formControls['modifiedUserId'] = [{ value: this.userData.userId, disabled: false }]
    return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getStockSwapItemsListArray(): FormArray {
    if (!this.stockSwapRequestForm) return;
    return this.stockSwapRequestForm.get("stockSwapItems") as FormArray;
  }

  onLoadValue(response) {
    if (response?.resources) {
      let stockSwapRequestModel = new StockSwapRequestModel(response.resources);
      this.stockSwapRequestForm.patchValue(stockSwapRequestModel)
      this.stockSwapRequestForm.get('isApproval').setValue(response.resources.isCustomerAgreed, { emitEvent: false });
      this.stockSwapRequestForm.get('isNotApproval').setValue(response.resources.isInternalApproval, { emitEvent: false });
      this.stockSwapRequestForm.get('createdUserId').setValue(this.userData.userId);
      this.stockSwapRequestForm.get('modifiedUserId').setValue(this.userData.userId);
      if (this.stockSwapRequestForm.get('isApproval').value || this.stockSwapRequestForm.get('isNotApproval').value) {
        this.stockSwapRequestForm = setRequiredValidator(this.stockSwapRequestForm, ["contactPersonId", "contactPersonNumber"]);
      }
      this.stockSwapItems = this.getStockSwapItemsListArray;
      this.stockSwapItems.clear();
      if (response.resources.stockSwapItems.length > 0) {
        response.resources.stockSwapItems.forEach((stockSwapItemsListModel: StockSwapItemsListModel) => {
          stockSwapItemsListModel.actualItemPrice = stockSwapItemsListModel?.actualSellingPrice?.toString() ? this.otherService.transformDecimal(stockSwapItemsListModel?.actualSellingPrice) : '';
          stockSwapItemsListModel.actualSellingPrice = stockSwapItemsListModel?.actualSellingPrice?.toString() ? this.otherService.transformDecimal(stockSwapItemsListModel?.actualSellingPrice) : '';
          stockSwapItemsListModel.differenceAmount = stockSwapItemsListModel?.differenceAmount?.toString() ? this.otherService.transformDecimal(stockSwapItemsListModel?.differenceAmount) : '';
          stockSwapItemsListModel.swapItemPrice = stockSwapItemsListModel?.swapSellingPrice?.toString() ? this.otherService.transformDecimal(stockSwapItemsListModel?.swapSellingPrice) : '';
          stockSwapItemsListModel.swapSellingPrice = stockSwapItemsListModel?.swapSellingPrice?.toString() ? this.otherService.transformDecimal(stockSwapItemsListModel?.swapSellingPrice) : '';
          this.stockSwapItems.push(this.createStockSwapItemsListModel(stockSwapItemsListModel));
        })
        this.getTotalDiff();
      }
    } else {
      let stockSwapRequestModel = new StockSwapRequestModel();
      stockSwapRequestModel.billofmaterialid = this.billOfMaterialId;
      stockSwapRequestModel.callInitiationId = this.callInitiationId;
      this.stockSwapRequestForm.patchValue(stockSwapRequestModel);
      this.stockSwapRequestForm.get('isApproval').setValue(null, { emitEvent: false });
      this.stockSwapRequestForm.get('isApproval').disable();
      this.stockSwapRequestForm.get('isNotApproval').setValue(null, { emitEvent: false });
      this.stockSwapRequestForm.get('isNotApproval').disable();
      this.stockSwapRequestForm.get('createdUserId').setValue(this.userData.userId);
      this.stockSwapRequestForm.get('modifiedUserId').setValue(this.userData.userId);
      this.stockSwapItems = this.getStockSwapItemsListArray;
      let stockSwapItemsListModel = new StockSwapItemsListModel(null);
      this.stockSwapItems.push(this.createStockSwapItemsListModel(stockSwapItemsListModel));
      this.getTotalDiff();
    }
    if (this.stockSwapRequestForm.get('stockSwapStatus').value.toLowerCase().includes('approve')) {
      this.stockSwapRequestForm.disable();
    }
  }

  // getScanSwipData(serialNumber, type) {
  //   this.crudService.get(
  //     ModulesBasedApiSuffix.INVENTORY,
  //     InventoryModuleApiSuffixModels.STOCK_BALANCE_SERIAL,
  //     undefined,
  //     false,
  //     prepareGetRequestHttpParams('', '', {
  //       SerialNumber: serialNumber
  //     })
  //   ).subscribe(response => {
  //     this.rxjsService.setGlobalLoaderProperty(false);
  //     if (response.isSuccess == true && response.statusCode == 200) {
  //       this.stockSwapItems = this.getStockSwapItemsListArray
  //       let stockSwapItemsListModel = new StockSwapItemsListModel(response.resources)
  //       if (type == 1) {
  //         stockSwapItemsListModel.swapStockCode = response.resources.itemCode;
  //         stockSwapItemsListModel.swapSellingPrice = response.resources.costPrice;
  //         stockSwapItemsListModel.swapItemId = response.resources.itemId;
  //         this.stockSwapItems.push(this.createStockSwapItemsListModel(stockSwapItemsListModel));
  //         this.stockSwapRequestForm.get('swapStockCodeId').setValue(response.resources.itemId)

  //       } else {
  //         this.stockSwapRequestForm.get('replaceStockCodeId').setValue(response.resources.itemId)
  //         this.stockSwapRequestForm.get('replaceStockCodeItemDescription').setValue(response.resources.itemName)

  //         let index = this.stockSwapItems.length - 1
  //         this.stockSwapItems[index].actualStockCode = response.resources.itemCode
  //         this.stockSwapItems[index].actualSellingPrice = response.resources.costPrice
  //         this.stockSwapItems[index].actualItemId = response.resources.itemId
  //         this.stockSwapItems[index].differenceAmount = this.stockSwapItems[index].actualSellingPrice - this.stockSwapItems[index].swapSellingPrice
  //         this.stockSwapItems[index].differencePercentage = this.stockSwapItems[index].actualSellingPrice - this.stockSwapItems[index].swapSellingPrice
  //         this.getTotalDiff()
  //       }

  //     }
  //   });
  // }

  calculateStockSwapIncl(data) {
    const vat = (+data?.costPrice * +this.swapStockCodeList?.taxPercentage) / 100
    return data?.costPrice + vat;
  }

  getTotalDiff(isLoad = true) {
    this.stockSwapItems = this.getStockSwapItemsListArray;
    var totalDiff = 0
    this.stockSwapItems.value.forEach((stockSwapItemsListModel: StockSwapItemsListModel) => {
      const differenceAmount = stockSwapItemsListModel?.differenceAmount?.toString() ? this.otherService.transformCurrToNum(stockSwapItemsListModel?.differenceAmount) : 0;
      totalDiff += differenceAmount;
    })
    this.stockSwapRequestForm.get('totalDiff').setValue(totalDiff?.toString() ? this.otherService.transformDecimal(totalDiff) : '', { emitEvent: false });
    if (totalDiff <= 0) {
      this.stockSwapRequestForm.get('isApproval').disable({ emitEvent: false });
      this.stockSwapRequestForm.get('isNotApproval').disable({ emitEvent: false });
    } else if (totalDiff > 0) {
      this.stockSwapRequestForm.get('isNotApproval').enable({ emitEvent: false });
      this.stockSwapRequestForm.get('isApproval').enable({ emitEvent: false });
    }
    if (!isLoad) {
      this.stockSwapRequestForm.get('isApproval').setValue(false, { emitEvent: false });
      this.stockSwapRequestForm.get('isNotApproval').setValue(false, { emitEvent: false });
    }
  }

  getAlreadyExist() {
    let findExist = [];
    this.getStockSwapItemsListArray?.getRawValue()?.forEach((el: any) => {
      if (el?.actualItemId == this.stockSwapRequestForm?.get('swapStockCodeId').value) {
        findExist.push(el?.actualItemId);
      }
    })
    return findExist?.length;
  }

  swapChanged(newSerialNumber: string) {
    if (this.getStockSwapItemsListArray.invalid) {
      let index = this.stockSwapItems.value.length - 1;
      this.getStockSwapItemsListArray.removeAt(index);
    }
    if (this.getAlreadyExist()) {
      this.snackbarService.openSnackbar("Item already exists", ResponseMessageTypes.WARNING);
      this.stockSwapRequestForm?.get('swapStockCodeId').setValue('');
      return;
    }
    let actualData = this.swapStockCodeList.bomQuotationItems.find(x => x.itemId == newSerialNumber)
    if (actualData) {
      let stockSwapItemsListModel = new StockSwapItemsListModel(actualData)
      // stockSwapItemsListModel.swapStockCode = swapData.itemCode;
      // stockSwapItemsListModel.swapSellingPrice = swapData.unitPrice;
      // stockSwapItemsListModel.swapItemId = swapData.itemId; 
      stockSwapItemsListModel.actualStockCode = actualData?.itemCode;
      stockSwapItemsListModel.actualSellingPrice = actualData?.total?.toString() ? this.otherService.transformDecimal(actualData?.total) : '';
      stockSwapItemsListModel.actualItemPrice = actualData?.total?.toString() ? this.otherService.transformDecimal(actualData?.total) : '';
      stockSwapItemsListModel.actualItemId = actualData.itemId;
      this.stockSwapItems.push(this.createStockSwapItemsListModel(stockSwapItemsListModel));
    }
  }

  replacementChanged(newSerialNumber: string, type = 'stockCode') {
    let replaceData = type == 'stockCode' ? this.replaceStockCodeList?.find(x => x?.id == newSerialNumber) : this.replaceStockDescList?.find(x => x?.id == newSerialNumber)
    if (replaceData) {
      let index = this.stockSwapItems.value.length - 1
      if (type == 'stockCode') {
        this.stockSwapRequestForm.get('replaceStockCodeItemDescription').setValue({ displayName: replaceData?.displayName });
      } else if (type == 'stockDesc') {
        this.stockSwapRequestForm.get('replaceStockCodeId').setValue({ itemCode: replaceData?.itemCode });
      }
      this.stockSwapItems.controls[index].get('swapStockCode').setValue(replaceData?.itemCode);
      const stockSwapPriceInclVat = this.calculateStockSwapIncl(replaceData);
      this.stockSwapItems.controls[index].get('swapSellingPrice').setValue(stockSwapPriceInclVat?.toString() ? this.otherService.transformDecimal(stockSwapPriceInclVat) : '');
      this.stockSwapItems.controls[index].get('swapItemPrice').setValue(stockSwapPriceInclVat?.toString() ? this.otherService.transformDecimal(stockSwapPriceInclVat) : '');
      this.stockSwapItems.controls[index].get('swapItemId').setValue(replaceData.id)
      const actualSellingPrice = this.stockSwapItems.controls[index].get('actualSellingPrice').value?.toString() ? this.otherService.transformCurrToNum(this.stockSwapItems.controls[index].get('actualSellingPrice').value) : 0;
      let differenceAmount = Number(stockSwapPriceInclVat) - Number(actualSellingPrice)
      let differencePercentage = (Number(differenceAmount) / Number(actualSellingPrice)) * 100
      this.stockSwapItems.controls[index].get('differenceAmount').setValue(differenceAmount?.toString() ? this.otherService.transformDecimal(differenceAmount) : '');
      this.stockSwapItems.controls[index].get('differencePercentage').setValue(Number(differencePercentage).toFixed(2))
      this.getTotalDiff(false);
    }
  }

  navigateToPage() {
    this.router.navigate(['/customer', 'manage-customers', 'bill-of-material'], {
      queryParams: {
        installationId: this.callInitiationId,
        customerId: this.customerId,
        customerAddressId: this.customerAddressId,
        callType: this.callType,
        quotationVersionId: this.quotationVersionId ? this.quotationVersionId : null,
        callInitiationId: this.callInitiationId
      }
    });
  }

  getTotalValue() {
    const totalDiff = this.stockSwapRequestForm.get('totalDiff').value?.toString() ? this.otherService.transformCurrToNum(this.stockSwapRequestForm.get('totalDiff').value) : 0;
    return +totalDiff > 0 && (!this.stockSwapRequestForm.get('isApproval').value && !this.stockSwapRequestForm.get('isNotApproval').value);
  }


  onSubmit() {
    this.showTotalError = false;
    if (this.stockSwapRequestForm.invalid) {
      this.stockSwapRequestForm.markAllAsTouched();
      return
    }
    if (this.getTotalValue()) {
      this.stockSwapRequestForm.markAllAsTouched();
      this.showTotalError = true;
      return
    }
    let formValue = { ...this.stockSwapRequestForm.getRawValue() };
    if (formValue.isApproval) {
      formValue.isCustomerAgreed = true;
      formValue.isInternalApproval = false;
    } else if (formValue.isNotApproval) {
      formValue.isCustomerAgreed = false;
      formValue.isInternalApproval = true;
    } else {
      formValue.isCustomerAgreed = false;
      formValue.isInternalApproval = false;
    }
    formValue.totalDiff = formValue?.totalDiff?.toString() ? this.otherService.transformCurrToNum(formValue?.totalDiff) : null;
    formValue.replaceStockCodeItemDescription = formValue?.replaceStockCodeItemDescription?.displayName ? formValue?.replaceStockCodeItemDescription?.displayName : '';
    formValue?.stockSwapItems?.forEach(el => {
      el.actualItemPrice = el?.actualItemPrice?.toString() ? this.otherService.transformCurrToNum(el?.actualItemPrice) : null;
      el.actualSellingPrice = el?.actualSellingPrice?.toString() ? this.otherService.transformCurrToNum(el?.actualSellingPrice) : null;
      el.differenceAmount = el?.differenceAmount?.toString() ? this.otherService.transformCurrToNum(el?.differenceAmount) : null;
      el.swapItemPrice = el?.swapItemPrice?.toString() ? this.otherService.transformCurrToNum(el?.swapItemPrice) : null;
      el.swapSellingPrice = el?.swapSellingPrice?.toString() ? this.otherService.transformCurrToNum(el?.swapSellingPrice) : null;
    });
    delete formValue?.isNotApproval;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission(); ''
    let crudService: Observable<IApplicationResponse> = (!this.stockSwapRequestForm.get('stockSwapId').value) ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.STOCK_SWAP, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.STOCK_SWAP, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        // this.getStockSwapData();
        this.navigateToPage()
        // this.router.navigateByUrl('/billing/stock-id-mapping/list?tab=0');
      }
    })
  }

}

