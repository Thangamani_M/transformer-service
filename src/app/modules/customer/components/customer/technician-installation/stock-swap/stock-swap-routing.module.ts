import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockSwapRequestComponent } from './stock-swap-request/stock-swap-request.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: '', redirectTo: 'stock-swap-request', data: { title: 'Stock Swap Request' }
  },
  {
    path: 'stock-swap-request', component: StockSwapRequestComponent, data: { title: 'Stock Swap Request' }, canActivate:[AuthGuard]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class StockSwapRoutingModule { }
