import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { adjustDateFormatAsPerPCalendar, convertTreeToList, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CURRENT_YEAR_TO_NEXT_FIFTY_YEARS, CustomDirectiveConfig, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CUSTOMER_COMPONENT } from '@modules/customer/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { OutOfOfficeModuleApiSuffixModels } from '@modules/out-of-office/enums/out-of-office-employee-request.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { CalendarDayViewBeforeRenderEvent, CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarEventTitleFormatter, CalendarMonthViewBeforeRenderEvent, CalendarView, CalendarWeekViewBeforeRenderEvent } from 'angular-calendar';
import { addMinutes, isSameDay, isSameMonth, startOfDay } from 'date-fns';
import { ConfirmationService, DialogService } from 'primeng/api';
import { combineLatest, forkJoin, Observable, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { RecurringAppointmentComponent } from '../recurring-appointment/recurring-appointment.component';
import { AppointmentBookingModel, BookedSlotsListModel, ProvisonalBookingApprovalModel } from './appintment-booking.model';
import { CustomEventTitleFormatter } from './custom-event-title-formatter.provider';

class ValidationDate {
  technicianCallTypeId: any;
  techAreaId: any;
  technicianId: any;
  appointmentStartTime: any;
  appointmentEndTime: any;
  divisionId: any;
  branchId: any;
  cutoffTime: any;
  preferredTimeSlot: any;
}
@Component({
  selector: 'app-technician-allocation-calender-view',
  templateUrl: './technician-allocation-calender-view.component.html',
  styleUrls: ['./technician-allocation-calender-view.component.scss'],
  providers: [
    ConfirmationService,
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter,
    },
  ],
})
export class TechnicianAllocationCalenderViewComponent implements OnInit {
  refresh: Subject<any> = new Subject();

  loadingEvents: boolean = false
  showDialog: boolean
  showUnassignDialog: boolean
  filterForm: FormGroup
  showFilterForm: boolean = false
  scheduleForm: FormGroup
  searchTechForm: FormGroup
  appointmentFom: FormGroup
  unAssignedFom: FormGroup
  reAppointmentFom: FormGroup
  provisonalBookingApprovalForm: FormGroup
  installationId: any
  nextAppointmentDate: any
  loggedUser: any
  selectedUser: any
  periodType: any = []
  activeDayIsOpen: boolean = false
  appointmentDate: any
  // commentsDropDown: any = [
  //   { id: 'Customer Unavailability', displayName: 'Customer Unavailability' },
  //   { id: 'Resource Issue', displayName: 'Resource Issue' }
  // ]
  commentsDropDown: any = []
  estimatedTime: any;
  travelTime: any;
  appointmentId: any
  appointmentLogId: any

  divisionList: any = []
  branchList: any = []
  techAreaList: any = []
  technitianList: any = []
  callType: any
  customerId: any
  showRescheduleDialog: boolean = false
  // reSchedule: boolean = false
  serviceCallNo: string;

  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isHypenOnly = new CustomDirectiveConfig({ isHypenOnly: true });
  isMltiDotsRestrict = new CustomDirectiveConfig({ isMltiDotsRestrict: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  alertMsg: any = ''
  alertDialog: boolean = false
  isOverCapacity: boolean
  validationData: ValidationDate
  isPublicHolidayMsg: boolean
  showConfirmScheduleAlert: boolean = false
  customerAddressId: string
  editClickAction: any

  provisionalBookingApprovalDialog: boolean = false
  calenderStartHour: any = '0'
  calenderEndHour: any = '23'
  isManualSelect: boolean = false
  isTechnicianManualSelect: boolean = false

  canBookAppointment: boolean = false
  isReschedule: boolean = false
  isUnassignReschedule: boolean = false
  isAlertCheckbox: boolean = false
  isAlertCheckboxValue: boolean = false
  selectedEventsByAppointmentId: any
  showResult: boolean = false
  radioRemovalWorkListId: any
  alertMessage: string
  canBookOverCapacity: boolean = false
  overCapacityAlertMessage: string;
  actionPermissionObj: any;

  constructor(private _fb: FormBuilder, private dialogService: DialogService, private datepipe: DatePipe, private snakbarService: SnackbarService, private confirmationService: ConfirmationService, private momentService: MomentService, private crudService: CrudService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>) {
    this.installationId = this.activatedRoute.snapshot.queryParams?.installationId;
    this.nextAppointmentDate = this.activatedRoute.snapshot.queryParams?.nextAppointmentDate;
    this.viewDate = this.nextAppointmentDate ? new Date(this.nextAppointmentDate) : new Date();
    this.estimatedTime = this.activatedRoute.snapshot.queryParams?.estimatedTime;
    this.travelTime = this.activatedRoute.snapshot.queryParams?.travelTime;
    this.appointmentId = this.activatedRoute.snapshot.queryParams?.appointmentId;
    this.appointmentLogId = this.activatedRoute.snapshot.queryParams?.appointmentLogId;
    this.callType = this.activatedRoute.snapshot.queryParams?.callType;
    this.customerId = this.activatedRoute.snapshot.queryParams?.customerId;
    this.customerAddressId = this.activatedRoute.snapshot.queryParams?.customerAddressId;
    this.radioRemovalWorkListId = this.activatedRoute.snapshot.queryParams?.radioRemovalWorkListId;


    this.periodType = [
      { id: 'day', displayName: 'Daily' },
      { id: 'week', displayName: 'Weekly' },
    ]

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

  }

  ngOnInit(): void {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.combineLatestNgrxStoreData();
    this.createFilterForm();
    this.createScheduleForm();
    this.createSearchForm();
    this.createAppointmentForm();
    this.createProvisonalBookingApprovalForm();
    this.createReAppointmentForm();
    this.createUnassignedForm();
    this.searchKeywordRequest();
    this.scheduleFormRequest();
    this.getAllDropdownIds();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][CUSTOMER_COMPONENT.CUSTOMER]?.find(el => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD);
      if (permission) {
        let techPermission = convertTreeToList([permission?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.TECHNICAL)]);
        const objName = this.callType == '1' ? CUSTOMER_COMPONENT.INSTALLATION : this.callType == '2' ? CUSTOMER_COMPONENT.SERVICE :
          this.callType == '3' ? CUSTOMER_COMPONENT.SPECIAL_PROEJCT : '---';
        this.actionPermissionObj = techPermission?.find(el => el?.[objName])?.[objName]?.find(el1 => el1?.menuName == CUSTOMER_COMPONENT?.APPOINTMENT_BOOKING);
      }
    });
  }

  findActionPermission(value: string) {
    return this.actionPermissionObj?.subMenu?.find(el => el?.menuName == value);
  }

  /**
   * pre fetched dropdown values
   * appoitment should be allowed appointmentStartTime between appointmentEndTime
   */
  getAllDropdownIds() {
    let api = [this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_CALL_INITIATION_LOCATION, null, false,
      prepareGetRequestHttpParams(null, null, { CallInitiationId: this.installationId })),
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.UX_DIVISIONS, null, false, null),
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_APPOINTMENT_RESCHEDULE_REASON, null, false, null)]
    forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp?.isSuccess && resp?.statusCode == 200) {
          switch (ix) {
            case 0:
              this.onPatchCallInitiationLocation(resp);
              break;
            case 1:
              this.divisionList = resp.resources;
              break;
            case 2:
              this.commentsDropDown = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onPatchCallInitiationLocation(response) {
    if (response?.isSuccess && response?.statusCode == 200) {
      if (response.resources) {
        this.validationData = response.resources
        if (this.validationData['appointmentStartTime']) {
          this.calenderStartHour = this.validationData['appointmentStartTime'].split(':')[0]
        }
        if (this.validationData['appointmentEndTime']) {
          this.calenderEndHour = this.validationData['appointmentEndTime'].split(':')[0]
        }
        this.filterForm.get('divisionId').setValue(this.validationData['divisionId'] ? { id: this.validationData['divisionId'], displayName: this.validationData['divisionName'] } : '')
        this.filterForm.get('branchIds').setValue(this.validationData['branchId'] ? [{ branchId: response.resources.branchId, branchName: response.resources.branchName }] : '')
        this.filterForm.get('techAreaIds').setValue(this.validationData['techAreaId'] ? [{ id: response.resources.techAreaId, displayName: response.resources.techAreaName }] : '')
        this.filterForm.get('technicianId').setValue(this.validationData['technicianId'] ? { id: response.resources.technicianId, displayName: response.resources.technicianName } : '')
      } else {
        this.validationData = {
          technicianCallTypeId: null,
          techAreaId: null,
          technicianId: null,
          appointmentStartTime: '00:00:00',
          appointmentEndTime: null,
          divisionId: null,
          branchId: null,
          cutoffTime: null,
          preferredTimeSlot: null,
        }
      }
    }
    this.reLoadData()
  }

  createScheduleForm() {
    this.scheduleForm = this._fb.group({
      callInitiationId: [this.installationId ? this.installationId : ''],
      userId: [this.loggedUser ? this.loggedUser.userId : ''],
      appointmentDate: [this.nextAppointmentDate ? this.nextAppointmentDate : new Date()],
      scheduleType: ['1'],
      periodType: ['week'],
      estimatedTime: [this.estimatedTime ? this.estimatedTime : '30'],
      travelTime: [this.travelTime ? this.travelTime : ''],
      estimatedTimeSelected: [this.estimatedTime ? this.estimatedTime : ''],
      search: ['']
    });
    // this.reLoadData()
    this.onScheduleFormControlValueChanges();
  }

  onScheduleFormControlValueChanges() {
    this.scheduleForm.get('scheduleType').valueChanges.subscribe(val => {
      if (val == '1') {
        this.periodType = [
          { id: 'day', displayName: 'Daily' },
          { id: 'week', displayName: 'Weekly' },
        ]
        this.scheduleForm.get('periodType').setValue('week')
        this.setView('week')
      } else {
        this.periodType = [
          { id: 'day', displayName: 'Daily' },
        ]
        this.scheduleForm.get('periodType').setValue('day')
        this.setView('day')
      }
    })
  }

  createFilterForm() {
    this.filterForm = this._fb.group({
      divisionId: [''],
      branchIds: [[]],
      techAreaIds: [[]],
      technicianId: [''],
      includeSpecialProjectTechnician: [false],
      includeStandbyTechnician: [false],
    });
    if (!this.findActionPermission(PermissionTypes.FILTER)) {
      this.filterForm.disable();
    }
    this.onFilterFormControlValueChanges();
  }

  onFilterFormControlValueChanges() {
    this.filterForm.get('divisionId').valueChanges.subscribe(val => {
      if (val) {
        this.getBranchByDivisionId(val.id)
        if (this.isManualSelect) {
          this.filterForm.get('branchIds').setValue([])
          this.filterForm.get('techAreaIds').setValue([])
          this.filterForm.get('technicianId').setValue('')
        }
      }
    })
    this.filterForm.get('branchIds').valueChanges.subscribe(val => {
      if (val && val.length > 0) {
        let vals = []
        val.forEach(element => {
          vals.push(element.branchId)
        });
        this.getTechAreaByBranchId(vals.toString())
        if (this.isManualSelect) {
          this.filterForm.get('techAreaIds').setValue([])
          this.filterForm.get('technicianId').setValue('')
        }
      }
    })
    this.filterForm.get('techAreaIds').valueChanges.subscribe(val => {
      if (val && val.length > 0) {
        let vals = []
        val.forEach(element => {
          vals.push(element.id)
        });
        this.getTechnitianByTechAreaId(vals.toString())
        if (this.isManualSelect) {
          this.filterForm.get('technicianId').setValue('')
        }
      }
    })
    this.filterForm.valueChanges.subscribe(val => {
      if (val) {
        if (this.isManualSelect) {
          this.showResult = true
        } else {
          this.showResult = false
        }
      }
    })
  }


  createAppointmentForm(appointmentBookingModel?: AppointmentBookingModel) {
    let stockSwapRequestModelcontrol = new AppointmentBookingModel(appointmentBookingModel);
    this.appointmentFom = this._fb.group({
      bookedSlots: this._fb.array([])
    });
    Object.keys(stockSwapRequestModelcontrol).forEach((key) => {
      this.appointmentFom.addControl(key, new FormControl(stockSwapRequestModelcontrol[key]));
    });


    this.appointmentFom.get('callInitiationId').setValue(this.installationId ? this.installationId : '')
    this.appointmentFom.get('createdUserId').setValue(this.loggedUser.userId)
    this.appointmentFom.get('modifiedUserId').setValue(this.loggedUser.userId)

  }


  //Create FormArray controls
  createBookedSlotsListModel(bookedSlotsListModel?: BookedSlotsListModel): FormGroup {
    let bookedSlotsListModelControl = new BookedSlotsListModel(bookedSlotsListModel);
    let formControls = {};
    Object.keys(bookedSlotsListModelControl).forEach((key) => {
      // if (key === 'stockSwapId' || key === 'stockSwapItemId') {
      formControls[key] = [{ value: bookedSlotsListModelControl[key], disabled: false }]
      // } else {
      formControls[key] = [{ value: bookedSlotsListModelControl[key], disabled: false }, [Validators.required]]
      // }
    });
    let formBuilder = this._fb.group(formControls)
    formBuilder.get('appointmentDate').valueChanges.subscribe(val => {
      if (val) {
        let branchIds = []
        let techAreaIds = []
        if (this.filterForm.get('branchIds').value && this.filterForm.get('branchIds').value.length > 0) {
          this.filterForm.get('branchIds').value.forEach(element => {
            branchIds.push(element.branchId)
          });
        }
        if (this.filterForm.get('techAreaIds').value && this.filterForm.get('techAreaIds').value.length > 0) {
          this.filterForm.get('techAreaIds').value.forEach(element => {
            techAreaIds.push(element.id)
          });
        }
        let filteredData = Object.assign({},
          { divisionId: this.filterForm.get('divisionId').value ? this.filterForm.get('divisionId').value.id : '' },
          { branchIds: this.filterForm.get('branchIds').value ? branchIds.toString() : '' },
          { techAreaIds: this.filterForm.get('techAreaIds').value ? techAreaIds.toString() : '' },
          { technicianId: this.filterForm.get('technicianId').value ? this.filterForm.get('technicianId').value.id : '' },
          { includeSpecialProjectTechnician: this.filterForm.get('includeSpecialProjectTechnician').value ? this.filterForm.get('includeSpecialProjectTechnician').value : false },
          { callInitiationId: this.installationId },
          { estimatedTime: formBuilder.get('estimatedTime').value },
          { travelTime: formBuilder.get('travelTime').value },
          { noOfDates: 1 },
          { startDate: this.momentService.toFormateType(val, 'YYYY-MM-DD') });
        let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CAPACITY_APPOINTMETN_SLOT, null, false,
          prepareGetRequestHttpParams(null, null, filterdNewData))
          .subscribe((response: IApplicationResponse) => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if (response.resources && response.statusCode === 200) {
              formBuilder.get('appointmentDate').setValue(response.resources.length > 0 ? new Date(response.resources[0].appointmentDate) : val, { emitEvent: false })
            } else {
              formBuilder.get('appointmentDate').setValue(null)
            }
          });
      }
    })
    formBuilder.get('appointmentTimeFrom').valueChanges.subscribe(val => {
      if (val) {
        let addToMints1 = this.momentService.addMinits(val, this.scheduleForm.get('estimatedTime').value)
        addToMints1 = this.momentService.addMinits(addToMints1, this.scheduleForm.get('travelTime').value)
        let appointmentTimeTo = new Date(addToMints1)
        let timeExceedvalidate = this.timeExceedValidationOnchange(val, appointmentTimeTo)
        timeExceedvalidate.then(x => {
          if (x) {
            formBuilder.get('appointmentTimeTo').setValue(null)
          } else {
            let isMultipleappointment = this.isMultipleAppointmentCheck(val)
            if (!isMultipleappointment) { // false - muliple appoinment
              this.setSplitEstimationTimeToAllArray()
            } else {
              formBuilder.get('appointmentTimeTo').setValue(appointmentTimeTo)
            }
          }
        })
      }
    })
    return formBuilder;
  }

  //Create FormArray
  get getBookedSlotsListArray(): FormArray {
    if (!this.appointmentFom) return;
    return this.appointmentFom.get("bookedSlots") as FormArray;
  }

  createProvisonalBookingApprovalForm(provisonalBookingApprovalModel?: ProvisonalBookingApprovalModel) {
    let stockSwapRequestModelcontrol = new ProvisonalBookingApprovalModel(provisonalBookingApprovalModel);
    this.provisonalBookingApprovalForm = this._fb.group({
      bookedSlots: this._fb.array([])
    });
    Object.keys(stockSwapRequestModelcontrol).forEach((key) => {
      this.provisonalBookingApprovalForm.addControl(key, new FormControl(stockSwapRequestModelcontrol[key]));
    });

    this.provisonalBookingApprovalForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.provisonalBookingApprovalForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  //Create FormArray
  get getProvisonalBookedSlotsListArray(): FormArray {
    if (!this.appointmentFom) return;
    return this.provisonalBookingApprovalForm.get("bookedSlots") as FormArray;
  }

  createReAppointmentForm() {
    this.reAppointmentFom = this._fb.group({
      appointmentId: [this.appointmentId ? this.appointmentId : ''],
      // appointmentLogId: [this.appointmentLogId ? this.appointmentLogId : ''],
      appointmentTimeFrom: ["", Validators.required],
      appointmentTimeTo: ['', Validators.required],
      callInitiationId: [this.installationId ? this.installationId : ''],
      estimatedTime: [this.estimatedTime ? this.estimatedTime : ''],
      travelTime: [this.travelTime ? this.travelTime : ''],
      technicianId: [''],
      isAppointmentChangable: [''],
      // isSpecialAppointment: [''],
      createdUserId: [this.loggedUser ? this.loggedUser.userId : ''],
      modifiedUserId: [this.loggedUser ? this.loggedUser.userId : ''],
      appointmentDate: [this.nextAppointmentDate ? this.nextAppointmentDate : new Date()],
    });
  }

  createUnassignedForm() {
    this.unAssignedFom = this._fb.group({
      appointmentId: [this.appointmentId ? this.appointmentId : null],
      appointmentLogId: [this.appointmentLogId ? this.appointmentLogId : null],
      appointmentDate: [this.appointmentDate ? this.appointmentDate : new Date()],
      estimatedTime: [this.estimatedTime ? this.estimatedTime : null],
      travelTime: [this.travelTime ? this.travelTime : null],
      comments: ["", Validators.required],
      appointmentTimeFrom: ["", Validators.required],
      appointmentTimeTo: ['', Validators.required],
      isAppointmentChangable: [null],
      isFixedAppointment: [null],
      isSpecialAppointment: [null],
      isOverCapacity: [null],
      isReschedule: [null],
      isConfirm: [null],
      modifiedUserId: [this.loggedUser ? this.loggedUser.userId : ''],
    });
  }



  createSearchForm() {
    this.searchTechForm = this._fb.group({
      search: [''],
    });
  }

  scheduleFormRequest() {
    this.scheduleForm.valueChanges
      .pipe(

        switchMap(obj => {

          return this.reLoadData()
        })
      )
      .subscribe();
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        // this.addConfirm = true;
        // this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      default:
    }
  }



  submitFilter() {
    if (!this.findActionPermission(PermissionTypes.FILTER)) {
      return this.snakbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.showResult = false
    this.reLoadData()

    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    if (!this.findActionPermission(PermissionTypes.FILTER)) {
      return this.snakbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.filterForm.reset()
    this.branchList = []
    this.techAreaList = []
    this.technitianList = []
    this.showFilterForm = !this.showFilterForm;
    this.reLoadData()

  }


  /**
   * appinement booking calender view starts
   */
  view: CalendarView = CalendarView.Week;

  CalendarView = CalendarView;
  yesterday = this.momentService.getYesterday()
  minDate: Date = new Date(this.yesterday);


  setView(view) {
    this.view = view;
  }

  /**
   * day click event not inUse
   * @param param0
   */
  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }



  /**
   * you will trigger while clicking on each timeslot
   * @param daytime you will get while clicking on timeslot
   * @returns
   */
  async onHoursSegmentClick(daytime) {
    if ((daytime.getDay() === 0 || daytime.getDay() === 6 || this.findPublicholiday(daytime)) && !this.filterForm.get('includeStandbyTechnician').value) {
      return;
    }

    if (this.alertMessage) {
      this.snakbarService.openSnackbar(this.alertMessage, ResponseMessageTypes.WARNING)
    }

    let isAfter = this.momentService.isAfter(daytime)
    if (!isAfter) {
      this.snakbarService.openSnackbar('You can not schedule for previous day/time ', ResponseMessageTypes.WARNING)
      return
    }

    if (!this.filterForm.get('includeStandbyTechnician').value) { // remove validation if includeStandbyTechnician true
      if (!this.isRoleAccessForSameDaySchedule()) { // TAM only can schedule
        let isToday = this.momentService.isSameDay(daytime, new Date())
        if (isToday) {
          this.snakbarService.openSnackbar('Not allowed to schedule for current date', ResponseMessageTypes.WARNING)
          return
        }
      }
    }

    if (this.technitianList.length == 0) {
      this.snakbarService.openSnackbar('There is no available dates with suitable Technician for your Call', ResponseMessageTypes.WARNING)
      return
    }

    /**
     * cuttofftime comes from getAlldropdown() method
     * selected time should not exceed then cuttofftime
     */
    let cutoffvalidate = await this.cutOffTimeValidation(daytime)
    if (cutoffvalidate) {
      return
    }
    if (this.appointmentId && (!this.appointmentLogId || this.isReschedule)) {
      this.editClickAction = 'Edited'
      let finalChek = await this.onReSelectTime(daytime)
      if (finalChek) {
        return
      }
      /**
      * public holidy comes from api, we need to show into calender
      */
      let publicHolidayvalidate = await this.isPublicHolidayValidation(daytime)
      this.isPublicHolidayMsg = publicHolidayvalidate
      this.showRescheduleDialog = true
    } else {
      // let selectedEvent: any = this.events.find(x => x.id == this.appointmentId)
      let selectedEvent: any = this.selectedEventsByAppointmentId
      let isSameDay = selectedEvent ? this.momentService.isSameDay(selectedEvent.start, daytime) : true


      if (((isSameDay || selectedEvent?.isProvisionalBooking) && !this.isUnassignReschedule) || (selectedEvent?.isProvisionalBooking && this.isUnassignReschedule)) { // if provisonal booking - call update api only whether same or otherday

        //provisonal booking starts here
        // if (selectedEvent?.isProvisionalBooking) {
        //   // let isAdminRole = this.isRoleAccess()
        //   // if (isAdminRole) {
        //   this.getProvisonalBookingList(selectedEvent?.appointmentId)
        //   // } else {
        //   //   this.snakbarService.openSnackbar('You can not Re-schedule without approved ', ResponseMessageTypes.WARNING)
        //   // }
        //   return
        // }
        //provisonal booking ends here

        this.editClickAction = null
        let finalChek = await this.onSelectTime(daytime)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(daytime)
        this.isPublicHolidayMsg = publicHolidayvalidate
        // this.showDialog = true
      } else {

        // let selectedEvent: any = this.events.find(x => x.id == this.appointmentId)
        let selectedEvent: any = this.selectedEventsByAppointmentId

        //provisonal booking starts here
        if (selectedEvent?.isProvisionalBooking) {
          // let isAdminRole = this.isRoleAccess()
          // if (isAdminRole) {
          this.getProvisonalBookingList(selectedEvent?.appointmentId)
          // } else {
          //   this.snakbarService.openSnackbar('You can not Re-schedule without approved ', ResponseMessageTypes.WARNING)
          // }
          return
        }
        //provisonal booking ends here

        this.editClickAction = 'Edited'

        let unassignEstimationTime: any = this.momentService.getTimeDiff(selectedEvent.start, selectedEvent.end)
        this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

        let finalChek = await this.onSelectTimeUnassign(daytime)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(daytime)
        this.isPublicHolidayMsg = publicHolidayvalidate
        this.showUnassignDialog = true
        this.appointmentDate = daytime
        // selectedEvent.start = daytime;

        // for end time
        let addToMints1 = this.momentService.addMinits(daytime, this.scheduleForm.get('estimatedTime').value)
        addToMints1 = this.momentService.addMinits(addToMints1, this.scheduleForm.get('travelTime').value)
        // selectedEvent.end = new Date(addToMints1);
        //for end time

        // this.events = [...this.events];
        this.unAssignedFom.get('appointmentId').setValue(selectedEvent.appointmentId)
        this.unAssignedFom.get('appointmentLogId').setValue(selectedEvent.appointmentLogId)
        this.unAssignedFom.get('appointmentDate').setValue(new Date(daytime))
        this.unAssignedFom.get('appointmentTimeFrom').setValue(daytime)
        this.unAssignedFom.get('appointmentTimeTo').setValue(new Date(addToMints1))
        this.unAssignedFom.get('isReschedule').setValue(true)
      }





    }

    this.appointmentDate = daytime
    this.viewDate = new Date(daytime)

  }

  getProvisonalBookingList(appointmentId) {
    this.getProvisonalBookedSlotsListArray.clear()

    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.APPOINTMENTS_LOGS_PROVISONAL_BOOKING,
      undefined,
      false,
      prepareGetRequestHttpParams(null, null, {
        AppointmentId: appointmentId
      })
    ).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.provisionalBookingApprovalDialog = true
        this.provisonalBookingApprovalForm.get('appointmentId').setValue(appointmentId)
        if (response.resources) {
          response.resources.forEach(element => {
            let sliptappointmentTimeFrom = this.momentService.setTimetoRequieredDate(element.appointmentDate, element.appointmentTimeFrom)
            let sliptappointmentTimeTo = this.momentService.setTimetoRequieredDate(element.appointmentDate, element.appointmentTimeTo)

            let bookedSlotsModelData: BookedSlotsListModel = {
              appointmentDate: new Date(element.appointmentDate),
              minAppointmentDate: new Date(element.appointmentDate),
              appointmentTimeFrom: new Date(sliptappointmentTimeFrom),
              appointmentTimeTo: new Date(sliptappointmentTimeTo),
              estimatedTime: element.estimatedTime,
            }
            this.getProvisonalBookedSlotsListArray.push(this.createBookedSlotsListModel(bookedSlotsModelData));
          });


        }
      }
    })
  }

  /**
   * will trigger when we click on the appointement
   * @param action whether edit/open
   * @param event selected data from list
   * @returns
   */
  async handleEvent(action: string, event: any) {
    if (event?.lable?.toLowerCase()?.indexOf('recurring') != -1) {
      // this.snakbarService.openSnackbar('You can not edit the call', ResponseMessageTypes.WARNING)
      return;
    }
    if (this.alertMessage) {
      this.snakbarService.openSnackbar(this.alertMessage, ResponseMessageTypes.WARNING)
    }
    this.viewDate = new Date(event.start)
    //provisonal booking starts here
    if (event?.isProvisionalBooking) {
      // let isAdminRole = this.isRoleAccess()
      // if (isAdminRole) {
      this.getProvisonalBookingList(event.appointmentId)
      // } else {
      //   this.snakbarService.openSnackbar('You can not Re-schedule without approved ', ResponseMessageTypes.WARNING)
      // }
      return
    }
    //provisonal booking ends here

    if (!event.isAppointmentChangable) {
      if (event.lable) {
        this.editClickAction = 'Edited'

        let unassignEstimationTime: any = this.momentService.getTimeDiff(event.start, event.end)
        this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

        let finalChek = await this.onSelectTimeUnassign(event.start)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(event.start)
        this.isPublicHolidayMsg = publicHolidayvalidate
        this.showUnassignDialog = true
        this.appointmentDate = event.start
        this.unAssignedFom.get('appointmentId').setValue(event.appointmentId)
        this.unAssignedFom.get('appointmentLogId').setValue(event.appointmentLogId)
        this.unAssignedFom.get('appointmentDate').setValue(event.start)
        this.unAssignedFom.get('isReschedule').setValue(false)

      }
      return
    }
    if (event.lable && event.isAppointmentChangable) {
      if (action === 'Clicked') {
        // this.onSelectTime(event.start)
        let finalChek = await this.onSelectTime(event.start)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(event.start)
        this.isPublicHolidayMsg = publicHolidayvalidate
        // this.appointmentFom.get('appointmentLogId').setValue(event.appointmentLogId)
        // this.showDialog = true
        this.appointmentDate = event.start

      } else if (action === 'Edited') {
        this.editClickAction = action

        let unassignEstimationTime: any = this.momentService.getTimeDiff(event.start, event.end)
        this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

        let finalChek = await this.onSelectTimeUnassign(event.start)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(event.start)
        this.isPublicHolidayMsg = publicHolidayvalidate
        this.showUnassignDialog = true
        this.appointmentDate = event.start
        this.unAssignedFom.get('appointmentId').setValue(event.appointmentId)
        this.unAssignedFom.get('appointmentLogId').setValue(event.appointmentLogId)
        this.unAssignedFom.get('appointmentDate').setValue(new Date(event.start))
        this.unAssignedFom.get('isReschedule').setValue(false)

      }

    }


  }

  /**
   * to drag and drop the appointment
   * @param event
   * @param daytime drag from time
   * @param endTime drop to time
   * @returns
   */
  async handleEventDragged(event, daytime, endTime) {
    if (this.alertMessage) {
      this.snakbarService.openSnackbar(this.alertMessage, ResponseMessageTypes.WARNING)
    }
    if (event?.isProvisionalBooking) {
      //   this.snakbarService.openSnackbar('You can not Re-schedule without approved ', ResponseMessageTypes.WARNING)
      return
    }
    let isAfter = this.momentService.isAfter(daytime)
    if (!isAfter) {
      this.snakbarService.openSnackbar('You can not schedule for previous day/time ', ResponseMessageTypes.WARNING)
      return
    }
    if (!this.filterForm.get('includeStandbyTechnician').value) { // remove validation if includeStandbyTechnician true
      if (!this.isRoleAccessForSameDaySchedule()) { // TAM only can schedule
        let isToday = this.momentService.isSameDay(daytime, new Date())
        if (isToday) {
          this.snakbarService.openSnackbar('Not allowed to schedule for current date', ResponseMessageTypes.WARNING)
          return
        }
      }
    }


    let cutoffvalidate = await this.cutOffTimeValidation(daytime)
    if (cutoffvalidate) {
      return
    }

    if (!event.isAppointmentChangable) {
      if (event.lable) {

        let isSameDay = this.momentService.isSameDay(event.start, daytime)

        if (isSameDay) {
          // this.onSelectTime(event.start)
          let finalChek = await this.onSelectTime(daytime)
          if (finalChek) {
            return
          }
          let publicHolidayvalidate = await this.isPublicHolidayValidation(daytime)
          this.isPublicHolidayMsg = publicHolidayvalidate
          // this.appointmentFom.get('appointmentLogId').setValue(event.appointmentLogId)
          // this.showDialog = true
          this.appointmentDate = daytime
          event.start = daytime;
          event.end = endTime;
          this.events = [...this.events];
          this.onAppointment()

        } else {
          this.editClickAction = 'Edited'

          let unassignEstimationTime: any = this.momentService.getTimeDiff(event.start, event.end)
          this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

          let finalChek = await this.onSelectTimeUnassign(event.start)
          if (finalChek) {
            return
          }
          let publicHolidayvalidate = await this.isPublicHolidayValidation(event.start)
          this.isPublicHolidayMsg = publicHolidayvalidate
          this.showUnassignDialog = true
          this.appointmentDate = daytime
          event.start = daytime;
          event.end = endTime;
          this.events = [...this.events];
          this.unAssignedFom.get('appointmentId').setValue(event.appointmentId)
          this.unAssignedFom.get('appointmentLogId').setValue(event.appointmentLogId)
          this.unAssignedFom.get('appointmentDate').setValue(event.start)
          this.unAssignedFom.get('appointmentTimeFrom').setValue(event.start)
          this.unAssignedFom.get('appointmentTimeTo').setValue(event.end)
          this.unAssignedFom.get('isReschedule').setValue(true)
        }

      }
      return
    }
    if (event.lable && event.isAppointmentChangable) {

      let isSameDay = this.momentService.isSameDay(event.start, daytime)

      if (isSameDay) {
        // this.onSelectTime(event.start)
        let finalChek = await this.onSelectTime(daytime)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(daytime)
        this.isPublicHolidayMsg = publicHolidayvalidate
        // this.appointmentFom.get('appointmentLogId').setValue(event.appointmentLogId)
        // this.showDialog = true
        this.appointmentDate = daytime
        event.start = daytime;
        event.end = endTime;
        this.events = [...this.events];
        this.onAppointment()

      } else {
        this.editClickAction = 'Edited'

        let unassignEstimationTime: any = this.momentService.getTimeDiff(event.start, event.end)
        this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

        let finalChek = await this.onSelectTimeUnassign(daytime)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(daytime)
        this.isPublicHolidayMsg = publicHolidayvalidate
        this.showUnassignDialog = true
        this.appointmentDate = daytime
        event.start = daytime;
        event.end = endTime;
        this.events = [...this.events];
        this.unAssignedFom.get('appointmentId').setValue(event.appointmentId)
        this.unAssignedFom.get('appointmentLogId').setValue(event.appointmentLogId)
        this.unAssignedFom.get('appointmentDate').setValue(new Date(event.start))
        this.unAssignedFom.get('appointmentTimeFrom').setValue(event.start)
        this.unAssignedFom.get('appointmentTimeTo').setValue(event.end)
        this.unAssignedFom.get('isReschedule').setValue(true)

      }

    }

    this.viewDate = new Date(daytime)
  }


  /**
   * pre booking
   * @param appointmentDate
   */
  advacedBookedValidation(appointmentDate) {
    let appointmentDate1 = this.momentService.toFormateType(appointmentDate, 'YYYY-MM-DD')
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.APPOINTMENTS_VALIDATE_ADVANCE_BOOKING, null, false,
      prepareGetRequestHttpParams(null, null, {
        AppointmentDate: appointmentDate1,
        CallInitiationId: this.installationId
      }))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.resources) {
          if (response.resources.alertMessage) {
            this.snakbarService.openSnackbar(response.resources.alertMessage, ResponseMessageTypes.WARNING)
          }
        } else {
        }
      }, error => {
      });


  }

  /**
   * cutofftime should not exceeded then current system time
   * @param startTime
   * @returns
   */
  cutOffTimeValidation(startTime) {
    if (!this.validationData['cutoffTime']) {
      return Promise.resolve(false);
    }
    if (this.filterForm.get('includeStandbyTechnician').value) { // remove validation if includeStandbyTechnician true
      return Promise.resolve(false);
    }

    // to check cuttoff time is exceed then current systme time
    let time = this.momentService.convertNormalToRailayTime(new Date())
    // let time1 = this.momentService.convertNormalToRailayTime(new Date())
    let time1 = this.momentService.convertNormalToRailayTime(this.validationData['cutoffTime'])
    let diff = this.momentService.timeDuration(time, time1)
    if (diff) {
      return Promise.resolve(false);
    } else {
      if (this.isRoleAccess()) {  // TAM can able to schedule
        this.showConfirmScheduleAlert = true
        return Promise.resolve(false);
      } else {

        // after one day we can schedule - validation starts here
        var nextDay = this.momentService.addDays(new Date(), 1)
        let isSameDay = this.momentService.isSameDay(startTime, nextDay)
        let isToday = this.momentService.isSameDay(startTime, new Date())
        if (isSameDay) {
          // if (this.isRoleAccessForCutoffTime()) {
          //   return Promise.resolve(false);
          // } else {
          // this.snakbarService.openSnackbar('You can not schedule. Cutoff time is ' + this.momentService.convertRailayToNormalTime(this.validationData.cutoffTime).toLocaleUpperCase(), ResponseMessageTypes.WARNING)
          this.snakbarService.openSnackbar('You can not schedule. Cutoff time is ' + this.validationData['cutoffTime'], ResponseMessageTypes.WARNING)
          this.showConfirmScheduleAlert = false
          return Promise.resolve(true);
          // }
        } else if (isToday) {
          if (this.isRoleAccessForSameDaySchedule()) { //TAm only can schedule
            this.showConfirmScheduleAlert = false
            return Promise.resolve(false);
          } else {
            this.snakbarService.openSnackbar('Not allowed to schedule for current date', ResponseMessageTypes.WARNING)
            this.showConfirmScheduleAlert = false
            return Promise.resolve(true);
          }

        } else {
          this.showConfirmScheduleAlert = false
          return Promise.resolve(false);
        }
        // after one day we can schedule - validation ends here

      }
    }
  }

  timeExceedValidationOnchange(selectedFromTime, selectedToTim) {

    // appointmentStartTime and appointmentStartTime between time validation starts here
    let isSameDay = this.momentService.isSameDay(selectedFromTime, selectedToTim)
    if (isSameDay) {
      var selectedToTime = selectedToTim
    } else {
      let selctTimeOnly = this.momentService.getTimeDiff(selectedFromTime, selectedToTim)
      var selectedToTime: any = this.momentService.setTimetoRequieredDate(selectedToTim, selctTimeOnly)
    }
    let add1Minuts = this.momentService.addMinutes(selectedFromTime, 1)
    let reduce1Minuts = this.momentService.reduceMinutes(selectedToTime, 1)

    let selectedFromTime1 = this.momentService.convertNormalToRailayTime(new Date(add1Minuts))
    let selectedToTime1 = this.momentService.convertNormalToRailayTime(new Date(reduce1Minuts))

    let selectedFromTimeValidation = this.momentService.betWeenTime(this.validationData['appointmentStartTime'], this.validationData['appointmentEndTime'], selectedFromTime1)

    // preferredTimeSlot validation start here
    if (this.validationData['preferredTimeSlot']) {
      if (this.validationData['preferredTimeSlot'].toLowerCase().includes('am')) {
        if (this.isAdminRole()) {  // TAM can able to schedule
          this.validationData['appointmentEndTime'] = this.validationData['appointmentEndTime'];
        } else {
          this.validationData['appointmentEndTime'] = '11:59:00';
        }
        var alertmsge = 'You can able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentStartTime']).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentEndTime']).toLocaleUpperCase()
        var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData['appointmentStartTime'], '11:59:00', selectedToTime1)
      } else {
        var alertmsge = 'You can able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentStartTime']).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentEndTime']).toLocaleUpperCase()
        var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData['appointmentStartTime'], this.validationData['appointmentEndTime'], selectedToTime1)
      }
    } else {
      var alertmsge = 'You can able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentStartTime']).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentEndTime']).toLocaleUpperCase()
      var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData['appointmentStartTime'], this.validationData['appointmentEndTime'], selectedToTime1)
    }
    // preferredTimeSlot validation ends here

    if (this.filterForm.get('includeStandbyTechnician').value) { // remove validation if includeStandbyTechnician true
      selectedFromTimeValidation = true;
      selectedToTimeValidation = true
    }

    if (isSameDay && selectedFromTimeValidation && selectedToTimeValidation) {
      this.showConfirmScheduleAlert = false
      return Promise.resolve(false);
    } else {
      if (this.isAdminRole()) {
        if (isSameDay) {
          this.splitSingleAppointment(selectedFromTime, selectedToTime)
          this.showConfirmScheduleAlert = false
          return Promise.resolve(false);
        } else {
          this.splitMultipleAppointment(selectedFromTime, selectedToTime)
          return Promise.resolve(false);
        }
      } else {
        let isMultipleappointment = this.isMultipleAppointmentCheck(selectedFromTime)
        if (!isMultipleappointment) {
          this.showConfirmScheduleAlert = false
          return Promise.resolve(false);
        } else {
          if (this.isAdminRole()) {  // TAM can able to schedule
            this.showConfirmScheduleAlert = true
            return Promise.resolve(false);
          } else {
            this.snakbarService.openSnackbar(alertmsge, ResponseMessageTypes.WARNING)
            this.showConfirmScheduleAlert = false
            return Promise.resolve(true);
          }
        }
      }
    }
  }


  timeExceedValidation(selectedFromTime, selectedToTim) {
    // appointmentStartTime and appointmentStartTime between time validation starts here
    let isSameDay = this.momentService.isSameDay(selectedFromTime, selectedToTim)
    if (isSameDay) {
      var selectedToTime = selectedToTim
    } else {
      let selctTimeOnly = this.momentService.getTimeDiff(selectedFromTime, selectedToTim)
      var selectedToTime: any = this.momentService.setTimetoRequieredDate(selectedToTim, selctTimeOnly)
    }
    let add1Minuts = this.momentService.addMinutes(selectedFromTime, 1)
    let reduce1Minuts = this.momentService.reduceMinutes(selectedToTime, 1)
    let selectedFromTime1 = this.momentService.convertNormalToRailayTime(new Date(add1Minuts))

    let selectedToTime1 = this.momentService.convertNormalToRailayTime(new Date(reduce1Minuts))

    let selectedFromTimeValidation = this.momentService.betWeenTime(this.validationData['appointmentStartTime'], this.validationData['appointmentEndTime'], selectedFromTime1)

    // preferredTimeSlot validation start here
    if (this.validationData['preferredTimeSlot']) {
      if (this.validationData['preferredTimeSlot'].toLowerCase().includes('am')) {
        if (this.isAdminRole()) {  // TAM can able to schedule
          this.validationData['appointmentEndTime'] = this.validationData['appointmentEndTime'];
        } else {
          this.validationData['appointmentEndTime'] = '11:59:00';
        }
        var alertmsge = 'You are only able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentStartTime']).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentEndTime']).toLocaleUpperCase()
        var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData['appointmentStartTime'], '11:59:00', selectedToTime1)
      } else {
        var alertmsge = 'You are only able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentStartTime']).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentEndTime']).toLocaleUpperCase()
        var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData['appointmentStartTime'], this.validationData['appointmentEndTime'], selectedToTime1)
      }
    } else {
      var alertmsge = 'You are only able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentStartTime']).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData['appointmentEndTime']).toLocaleUpperCase()
      var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData['appointmentStartTime'], this.validationData['appointmentEndTime'], selectedToTime1)
    }
    // preferredTimeSlot validation ends here

    if (this.filterForm.get('includeStandbyTechnician').value) { // remove validation if includeStandbyTechnician true
      selectedFromTimeValidation = true;
      selectedToTimeValidation = true
    }

    if (isSameDay && selectedFromTimeValidation && selectedToTimeValidation) {
      this.splitSingleAppointment(selectedFromTime, selectedToTime)
      this.showConfirmScheduleAlert = false
      this.showDialog = this.editClickAction === 'Edited' ? false : true
      return Promise.resolve(false);
    } else {
      // this.snakbarService.openSnackbar('Can not schedule from ' + this.momentService.convertRailayToNormalTime(selectedFromTime).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(selectedToTime).toLocaleUpperCase() + '. Schedule time must be lesser than ' + this.momentService.convertRailayToNormalTime(this.validationData.cutoffTime).toLocaleUpperCase(), ResponseMessageTypes.WARNING)

      if (this.isAdminRole()) {
        if (isSameDay) {
          this.splitSingleAppointment(selectedFromTime, selectedToTime)
          this.showConfirmScheduleAlert = false
          this.showDialog = this.editClickAction === 'Edited' ? false : true
          return Promise.resolve(false);
        } else {
          this.splitMultipleAppointment(selectedFromTime, selectedToTime)
          return Promise.resolve(false);
        }
      } else {
        // same block start
        let isMultipleappointment = this.isMultipleAppointmentCheck(selectedFromTime)
        if (!isMultipleappointment) {
          this.splitMultipleAppointment(selectedFromTime, selectedToTime)
          this.showConfirmScheduleAlert = false
          // this.showDialog = this.editClickAction === 'Edited' ? false : true
          return Promise.resolve(false);
        } else {
          if (this.isAdminRole()) {  // TAM can able to schedule
            this.splitSingleAppointment(selectedFromTime, selectedToTime)
            this.showConfirmScheduleAlert = true
            this.showDialog = this.editClickAction === 'Edited' ? false : true
            return Promise.resolve(false);
          } else {
            this.snakbarService.openSnackbar(alertmsge, ResponseMessageTypes.WARNING)
            this.showConfirmScheduleAlert = false
            return Promise.resolve(true);
          }
        }
        // same block ends

      }


    }
  }

  isPublicHolidayValidation(selectedDate) {
    this.users.forEach(element => {
      element.isSameDay = this.momentService.isSameDay(element.start, selectedDate)
    });
    let checkHoliday = this.users.find(x => x.isSameDay)
    if (checkHoliday) {
      return Promise.resolve(checkHoliday.isPublicHoliday);
    } else {
      // return Promise.resolve(checkHoliday.isPublicHoliday);
      return Promise.resolve(false);
    }
  }

  /**
   * to check multiple or signle appointment
   * @param selectedFromTime
   * @returns
   */
  isMultipleAppointmentCheck(selectedFromTime?: any) {
    let totalTimeApplicable: any = this.momentService.getTimeDiff(this.validationData['appointmentStartTime'], this.validationData['appointmentEndTime'])
    let timeDuration: any = this.momentService.betWeenTime('00:00:00', totalTimeApplicable, this.estimatedTime + ':00',)
    // let numberOfDayNeed = Number(this.scheduleForm.value.estimatedTime) / Number(selectedFromTimeValidation)
    // return timeDuration  // true - allow multiple appintment; false- not allow multiple appintment
    if (timeDuration) { //single appintment

      // let isAdminRole = this.isAdminRole()
      // if (isAdminRole) {
      //   let timeDuration1: any = this.momentService.betWeenTime('00:00:00', this.validationData.appointmentEndTime,selectedFromTime)
      //   return timeDuration1
      // } else {
      //   return true
      // }
      return true
    } else { // multiple appintment
      // let reduce1Minuts = this.momentService.addMinutes(selectedFromTime, 1)
      // let selectedFromTime1 = this.momentService.convertNormalToRailayTime(new Date(reduce1Minuts))
      // let selectedFromTimeValidation = this.momentService.betWeenTime(this.validationData.appointmentStartTime, this.validationData.appointmentEndTime, selectedFromTime1)
      // if (!selectedFromTimeValidation) {
      //   if (this.loggedUser.roleName.toLowerCase() == 'technical area manager' || this.loggedUser.roleName.toLowerCase() == 'super admin') {  // TAM can able to schedule
      //     this.showConfirmScheduleAlert = true
      //   } else {
      //     this.snakbarService.openSnackbar('You can not schedule from ' + this.momentService.convertRailayToNormalTime(selectedFromTime).toLocaleUpperCase(), ResponseMessageTypes.WARNING)
      //     this.showConfirmScheduleAlert = false
      //   }
      //   return true
      // } else {
      //   return false
      // }
      return false
    }
  }

  isAccessCheck(selectedFromTime) {
    let reduce1Minuts = this.momentService.addMinutes(selectedFromTime, 1)
    let selectedFromTime1 = this.momentService.convertNormalToRailayTime(new Date(reduce1Minuts))
    // let selectedFromTimeValidation = this.momentService.betWeenTime(this.validationData.appointmentStartTime, this.validationData.appointmentEndTime, selectedFromTime1)
    let selectedFromTimeValidation = this.momentService.betWeenTime('00:00:00', this.validationData['appointmentEndTime'], selectedFromTime1)
    return selectedFromTimeValidation
  }

  isAdminRole() {
    // if (this.loggedUser.roleName.toLowerCase() == 'technical area manager' || this.loggedUser.roleName.toLowerCase() == 'super admin') {  // TAM can able to schedule
    //   return true
    // } else {
    //   return false
    // }
    return false
  }

  isRoleAccess() {
    // if (this.loggedUser.roleName.toLowerCase() == 'technical area manager' || this.loggedUser.roleName.toLowerCase() == 'super admin') {  // TAM can able to schedule
    //   return true
    // } else {
    //   return false
    // }
    return false
  }

  isRoleAccessForPostSchedule() {
    if (this.loggedUser.roleName.toLowerCase() == 'technical area manager') {  // TAM can able to schedule
      return true
    } else {
      return false
    }
  }

  isRoleAccessForSameDaySchedule() {
    if (this.loggedUser.roleName.toLowerCase() == 'technical area manager') {  // TAM can able to schedule
      return true
    } else {
      return false
    }
  }

  isRoleAccessForCutoffTime() {
    if (this.loggedUser.roleName.toLowerCase() == 'technical area manager' || this.loggedUser.roleName.toLowerCase() == 'technical co-ordinator') {  // no need to validate the cutoff time for TAM and technical co-ordinator
      return true
    } else {
      return false
    }
  }

  /**
   * to split single appointment for give time
   * @param startDate
   * @param endDate
   */
  splitSingleAppointment(startDate, endDate) {
    let bookedSlotsModelData: BookedSlotsListModel = {
      appointmentDate: startDate,
      minAppointmentDate: startDate,
      appointmentTimeFrom: startDate,
      appointmentTimeTo: endDate,
      estimatedTime: this.estimatedTime
    }
    this.getBookedSlotsListArray.clear()
    this.getBookedSlotsListArray.push(this.createBookedSlotsListModel(bookedSlotsModelData));
  }

  /**
   * to split multiple appoinment
   * @param startDate
   * @param endDate
   */
  splitMultipleAppointment(startDate, endDate) {
    this.getBookedSlotsListArray.clear()


    // preferredTimeSlot validation start here
    if (this.validationData['preferredTimeSlot']) {
      if (this.validationData['preferredTimeSlot'].toLowerCase().includes('am')) {
        var appointmentEndTime = '11:59:00'
      } else {
        var appointmentEndTime: string = this.validationData['appointmentEndTime']

      }
    } else {
      var appointmentEndTime: string = this.validationData['appointmentEndTime']
    }
    // preferredTimeSlot validation ends here

    let totalTime: any = this.momentService.getTimeDiff(this.validationData['appointmentStartTime'], appointmentEndTime)

    //check user access starts
    let isAccessCheck = this.isAccessCheck(startDate)
    if (isAccessCheck) {
      var appointmentEndTime = appointmentEndTime
    } else {
      var appointmentEndTime = '24:00:00'
    }
    //check user access ends

    // let endDate1 = this.momentService.toFormateType(endDate, 'HH:mm:ss')
    // let differenceTime: any = this.momentService.getTimeDiff(startDate, endDate)

    // add first appintment
    let firstArraydifferenceTime: any = this.momentService.getTimeDiff(startDate, this.momentService.setTimetoRequieredDate(startDate, appointmentEndTime))
    let bookedSlotsModelData: BookedSlotsListModel = {
      appointmentDate: startDate,
      minAppointmentDate: startDate,
      appointmentTimeFrom: startDate,
      appointmentTimeTo: this.momentService.setTimetoRequieredDate(startDate, appointmentEndTime),
      estimatedTime: firstArraydifferenceTime
    }
    this.getBookedSlotsListArray.push(this.createBookedSlotsListModel(bookedSlotsModelData));

    // number of days calculation starts here
    let spltedEstimated = this.estimatedTime.split(':')
    let spltedfirstArraydifferenceTime = firstArraydifferenceTime.split(':')
    let restEstimatedTime = Number(spltedEstimated[0] + '.' + spltedEstimated[1]) - Number(spltedfirstArraydifferenceTime[0] + '.' + spltedfirstArraydifferenceTime[1])
    let spltedTotalTime = totalTime.split(':')
    // let totalNumberOfDay = Number(restEstimatedTime) / Number(spltedTotalTime[0] + '.' + spltedTotalTime[1])
    let numberOfDayNeed = Number(restEstimatedTime.toFixed(2)) / Number(spltedTotalTime[0] + '.' + spltedTotalTime[1])
    // let restEstimatedTimeFormate = String(restEstimatedTime.toFixed(2)).split('.')

    // let restEstimatedTimeFormate1 =
    //   (restEstimatedTimeFormate.length == 1) ? restEstimatedTimeFormate[0] + ':' + '00' :
    //     restEstimatedTimeFormate[0] + ':' + restEstimatedTimeFormate[1]
    // number of days calculation ends here



    let branchIds = []
    let techAreaIds = []

    if (this.filterForm.get('branchIds').value && this.filterForm.get('branchIds').value.length > 0) {
      this.filterForm.get('branchIds').value.forEach(element => {
        branchIds.push(element.branchId)
      });
    }
    if (this.filterForm.get('techAreaIds').value && this.filterForm.get('techAreaIds').value.length > 0) {
      this.filterForm.get('techAreaIds').value.forEach(element => {
        techAreaIds.push(element.id)
      });
    }
    let nextDay = this.momentService.addDaysWithFormate(startDate, 1, 'YYYY-MM-DD')
    let filteredData = Object.assign({},
      { divisionId: this.filterForm.get('divisionId').value ? this.filterForm.get('divisionId').value.id : '' },
      { branchIds: this.filterForm.get('branchIds').value ? branchIds.toString() : '' },
      { techAreaIds: this.filterForm.get('techAreaIds').value ? techAreaIds.toString() : '' },
      { technicianId: this.filterForm.get('technicianId').value ? this.filterForm.get('technicianId').value.id : '' },
      { includeSpecialProjectTechnician: this.filterForm.get('includeSpecialProjectTechnician').value ? this.filterForm.get('includeSpecialProjectTechnician').value : false },
      { callInitiationId: this.installationId },
      { estimatedTime: firstArraydifferenceTime },
      // { estimatedTime: restEstimatedTimeFormate1 },
      { noOfDates: Math.ceil(numberOfDayNeed) },
      { startDate: this.momentService.toFormateType(nextDay, 'YYYY-MM-DD') });
    // { startDate: nextDay });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )


    // add next appintment
    // this.httpCancelService.cancelPendingRequestsOnFormSubmission()
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CAPACITY_APPOINTMETN_SLOT,
      undefined,
      false,
      prepareGetRequestHttpParams(null, null, filterdNewData)
    ).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.showDialog = this.editClickAction === 'Edited' ? false : true

        if (response.resources) {
          if (response.resources.length > 0) {
            this.filterForm.get('technicianId').setValue({ id: response.resources[0].technicianId, displayName: response.resources[0].technicianName }, { emitEvent: false })
          }
          response.resources.forEach(element => {
            let sliptappointmentTimeFrom = this.momentService.setTimetoRequieredDate(element.appointmentDate, element.appointmentTimeFrom)
            let sliptappointmentTimeTo = this.momentService.addMinits(sliptappointmentTimeFrom, element.estimatedTime)

            let bookedSlotsModelData: BookedSlotsListModel = {
              appointmentDate: new Date(element.appointmentDate),
              minAppointmentDate: new Date(element.appointmentDate),
              appointmentTimeFrom: sliptappointmentTimeFrom,
              appointmentTimeTo: this.momentService.setTimetoRequieredDate(element.appointmentDate, appointmentEndTime),
              estimatedTime: element.estimatedTime,
            }
            this.getBookedSlotsListArray.push(this.createBookedSlotsListModel(bookedSlotsModelData));
          });

          // rest estimation time calculation starts here
          this.setSplitEstimationTimeToAllArray()
          // rest estimation time calculation ends here
        }
      } else {
        this.showDialog = false
      }
    })

  }

  setSplitEstimationTimeToAllArray() {
    let totalLength = this.getBookedSlotsListArray.value.length
    let lastIndex = totalLength - 1
    // preferredTimeSlot validation start here
    if (this.validationData['preferredTimeSlot']) {
      if (this.validationData['preferredTimeSlot'].toLowerCase().includes('am')) {
        var appointmentEndTime = '11:59:00'
      } else {
        var appointmentEndTime: string = this.validationData['appointmentEndTime']

      }
    } else {
      var appointmentEndTime: string = this.validationData['appointmentEndTime']
    }
    // preferredTimeSlot validation ends here

    //check user access starts
    // let isAccessCheck = this.isAccessCheck(this.viewDate)
    // if (isAccessCheck) {
    //   var appointmentEndTime = appointmentEndTime
    // } else {
    //   var appointmentEndTime = '24:00:00'
    // }
    //check user access ends

    var initialDateTime = this.momentService.setTimetoRequieredDate(this.getBookedSlotsListArray.value[0].appointmentTimeFrom, '0:00:00')
    var estimatedDateTime = this.momentService.setTimetoRequieredDate(this.getBookedSlotsListArray.value[0].appointmentTimeFrom, this.estimatedTime)
    let initialArrydifferenceTime: any = this.momentService.getTimeDiff(this.getBookedSlotsListArray.value[0].appointmentTimeFrom, this.getBookedSlotsListArray.value[0].appointmentTimeTo)
    this.getBookedSlotsListArray.controls[0].get('estimatedTime').setValue(initialArrydifferenceTime)
    let spltedinitialArrydifferenceTime = initialArrydifferenceTime.split(':')

    var hour = Number(spltedinitialArrydifferenceTime[0])
    var minute = Number(spltedinitialArrydifferenceTime[1])
    var initialDateTime1
    this.getBookedSlotsListArray.value.forEach((element, index) => {
      if (index != lastIndex && index != 0) {
        let differenceTime: any = this.momentService.getTimeDiff(element.appointmentTimeFrom, this.momentService.setTimetoRequieredDate(element.appointmentTimeFrom, appointmentEndTime))

        let splteddifferenceTime = differenceTime.split(':')

        hour += Number(splteddifferenceTime[0]);
        minute += Number(splteddifferenceTime[1]);

        element.appointmentTimeTo = this.momentService.setTimetoRequieredDate(element.appointmentDate, appointmentEndTime)
        this.getBookedSlotsListArray.controls[index].get('appointmentTimeTo').setValue(new Date(element.appointmentTimeTo))
        this.getBookedSlotsListArray.controls[index].get('estimatedTime').setValue(differenceTime)

        // hours.push(moment({ hour }).format('h:mm A'));
        // hours.push(
        //   moment({
        //     hour,
        //     minute: minutes
        //   }).format('h:mm A')
        // );
        // hours.push(moment({hour, minute }).format('h:mm a'));


      } else {
        // let lastEndTimePending: any = this.momentService.getTimeDiff(estimatedDateTime, initialDateTime1)

        // let lastToTime = this.momentService.setTimetoRequieredDate(element.appointmentTimeFrom, lastEndTimePending)
        // this.getBookedSlotsListArray.controls[lastIndex].get('appointmentTimeTo').setValue(lastToTime)
      }

    });
    var initialDateTime1: any = this.momentService.addHoursMinits(initialDateTime, hour, minute)
    let lastEndTimePending: any = this.momentService.getTimeDiff(new Date(initialDateTime1), estimatedDateTime)
    let splitedlastEndTimePending = lastEndTimePending.split(':')
    let lastToTime = this.momentService.addHoursMinits(this.getBookedSlotsListArray.controls[lastIndex].get('appointmentTimeFrom').value, splitedlastEndTimePending[0], splitedlastEndTimePending[1])

    this.getBookedSlotsListArray.controls[lastIndex].get('appointmentTimeTo').setValue(new Date(lastToTime))
    this.getBookedSlotsListArray.controls[lastIndex].get('estimatedTime').setValue(lastEndTimePending)

  }


  async onSelectTime(event) {
    // let addFromMints = this.momentService.toHoursMints24(event)
    // this.appointmentFom.get('appointmentTimeFrom').setValue(event)
    // let addToMints = this.momentService.toHoursMints12(event)
    let addToMints1 = this.momentService.addMinits(event, this.scheduleForm.get('estimatedTime').value);
    // this.appointmentFom.get('appointmentTimeTo').setValue(new Date(addToMints1))
    addToMints1 =this.momentService.addMinits(addToMints1, this.scheduleForm.get('travelTime').value);
    let appointmentTimeFrom = event;
    let appointmentTimeTo = new Date(addToMints1);
    // let bookedSlotsModelData: BookedSlotsListModel = {
    //   appointmentDate: event,
    //   appointmentTimeFrom: event,
    //   appointmentTimeTo: appointmentTimeTo,
    //   estimatedTime: this.estimatedTime
    // }
    // this.getBookedSlotsListArray.clear()
    // this.getBookedSlotsListArray.push(this.createBookedSlotsListModel(bookedSlotsModelData));


    let timeExceedvalidate = await this.timeExceedValidation(appointmentTimeFrom, appointmentTimeTo)
    if (timeExceedvalidate) {
      // this.appointmentFom.get('appointmentTimeTo').setValue(null)
      return Promise.resolve(true);
    } else {
      return Promise.resolve(false);
    }
  }

  async onReSelectTime(event) {
    // let addFromMints = this.momentService.toHoursMints24(event)
    this.reAppointmentFom.get('appointmentTimeFrom').setValue(event)

    // let addToMints = this.momentService.toHoursMints12(event)
    let addToMints1 = this.momentService.addMinits(event, this.scheduleForm.get('estimatedTime').value);
    addToMints1 = this.momentService.addMinits(addToMints1, this.scheduleForm.get('travelTime').value)
    this.reAppointmentFom.get('appointmentTimeTo').setValue(new Date(addToMints1))
    let timeExceedvalidate = await this.timeExceedValidation(this.reAppointmentFom.get('appointmentTimeFrom').value, this.reAppointmentFom.get('appointmentTimeTo').value)
    if (timeExceedvalidate) {
      this.reAppointmentFom.get('appointmentTimeTo').setValue(null)
      return Promise.resolve(true);
    } else {
      return Promise.resolve(false);
    }

  }

  async onSelectTimeUnassign(event, val?) {
    // let addFromMints = this.momentService.toHoursMints24(event)
    this.unAssignedFom.get('appointmentTimeFrom').setValue(event)

    // let addToMints = this.momentService.toHoursMints12(event)
    let addToMints1 = this.momentService.addMinits(event, this.scheduleForm.get('estimatedTimeSelected').value)
    this.unAssignedFom.get('appointmentTimeTo').setValue(new Date(addToMints1))
    let timeExceedvalidate = await this.timeExceedValidation(this.unAssignedFom.get('appointmentTimeFrom').value, this.unAssignedFom.get('appointmentTimeTo').value)
    if (timeExceedvalidate) {
      this.unAssignedFom.get('appointmentTimeTo').setValue(null)
      return Promise.resolve(true);
    } else {
      return Promise.resolve(false);
    }
  }

  /**
   * create appointment
   * @returns
   */
  onAppointment() {
    if (this.appointmentFom.invalid) {
      // this.checkValidity()
      return
    }
    this.showDialog = false
    let isMultipleappointment = this.isMultipleAppointmentCheck()
    if (!isMultipleappointment) { // multiple
      if (this.showConfirmScheduleAlert) {
        this.confirmSchedule()
      } else {
        this.doAppintment()
      }
    } else {
      this.checkTechnicianAvailability(1)
    }

  }

  /**
   * create reschedule appointment
   * @returns
   */
  onReAppointment() {
    if (this.reAppointmentFom.invalid) {
      return
    }
    this.showRescheduleDialog = false
    //  -------------
    // if (this.validationData['technicianId']) {
    //   this.checkOverCapacity(3, this.appointmentDate)
    // } else {
    let isMultipleappointment = this.isMultipleAppointmentCheck()
    if (!isMultipleappointment) { // multiple
      this.doReAppointment()
    } else {
      this.checkTechnicianAvailability(2)
    }

    // }
  }

  onUnassigne() {
    this.checkTechnicianAvailability(3)
  }

  /**
   * post schedule for allocate technician
   * @returns
   */
  onPostScheduleSubmit(): void {  // post schedule

    if (!this.selectedUser) {
      return;
    }

    if (this.selectedUser.technicianId) {
      this.checkOverCapacity(4, this.appointmentDate, this.selectedUser.technicianId)
    }
    //  else {
    //   this.doPostSchedule()
    // }

  }

  onProvisonalBookingApproval() {
    if (this.provisonalBookingApprovalForm.invalid) {
      return
    }
    this.provisionalBookingApprovalDialog = false
    let formValue = this.provisonalBookingApprovalForm.value

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.APPOINTMETNTS_APPROVE, formValue)
      .subscribe((response: IApplicationResponse) => {
        // this.showCommonAlerAfterResponse(response)
        this.reLoadData()
      })

  }

  /**
   * create appointemnt
   */
  doAppintment() {
    let formValue = this.appointmentFom.value
    formValue.isOverCapacity = this.isOverCapacity
    formValue.preferredDate = this.momentService.toFormateType(formValue.preferredDate, 'YYYY-MM-DD')
    formValue.appointmentId = this.appointmentId ? this.appointmentId : null
    formValue.technicianId = this.scheduleForm.get('scheduleType').value == '1' ? this.filterForm.get('technicianId').value?.id : '',
      formValue.isFixedAppointment = (formValue.technicianId && this.isTechnicianManualSelect) ? true : false,

      formValue.bookedSlots.forEach(element => {
        element.appointmentLogId = formValue.bookedSlots.lenth > 1 ? this.appointmentLogId : null
        // element.appointmentLogId = this.appointmentLogId ? this.appointmentLogId : null
        element.appointmentDate = this.momentService.toFormateType(element.appointmentDate, 'YYYY-MM-DD')
        element.appointmentTimeFrom = this.momentService.toHoursMints24(element.appointmentTimeFrom)
        element.appointmentTimeTo = this.momentService.toHoursMints24(element.appointmentTimeTo)
      });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    let crudService: Observable<IApplicationResponse> = (!this.appointmentLogId) ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.APPOINTMETNTS_EXTENDED, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.APPOINTMETNTS_EXTENDED, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (!this.isTechnicianManualSelect) {
        this.filterForm.get('technicianId').setValue(null)
      }
      this.showCommonAlerAfterResponse(response)

    })
  }

  /**
   * create reschedule appointment
   */
  doReAppointment() {
    let formValue = this.reAppointmentFom.value
    formValue.appointmentId = this.appointmentId ? this.appointmentId : null
    // formValue.appointmentDate = this.appointmentDate.toLocaleString()
    formValue.appointmentDate = this.datepipe.transform(this.appointmentDate, 'yyyy-MM-dd HH:mm:ss');
    formValue.appointmentTimeFrom = this.momentService.toHoursMints24(formValue.appointmentTimeFrom)
    formValue.appointmentTimeTo = this.momentService.toHoursMints24(formValue.appointmentTimeTo)
    formValue.technicianId = this.scheduleForm.get('scheduleType').value == '1' ? this.filterForm.get('technicianId').value?.id : '',
      // formValue.isAppointmentChangable = formValue.isAppointmentChangable ? formValue.isAppointmentChangable : formValue.technicianId ? false : true,
      // formValue.isReschedule = formValue.isReschedule ? true : false,
      // formValue.isSpecialAppointment = '',
      formValue.isOverCapacity = this.isOverCapacity

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.APPOINTMETNTS_RESCHEDULE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      this.showCommonAlerAfterResponse(response)
    })
  }

  /**
   * unassign existing appintment and reschedule to selected date
   * @returns
   */
  doUnassignReschedule() {
    if (this.unAssignedFom.invalid) {
      return
    }
    this.showUnassignDialog = false
    let formValue = this.unAssignedFom.value
    formValue.isOverCapacity = this.isOverCapacity
    formValue.technicianId = this.scheduleForm.get('scheduleType').value == '1' ? this.filterForm.get('technicianId').value?.id : '',
      formValue.isFixedAppointment = (formValue.technicianId && this.isTechnicianManualSelect) ? true : false,
      // formValue.appointmentDate = this.appointmentDate.toLocaleString()
      formValue.appointmentDate = this.datepipe.transform(this.appointmentDate, 'yyyy-MM-dd HH:mm:ss');
    formValue.appointmentTimeFrom = this.momentService.toHoursMints24(formValue.appointmentTimeFrom)
    formValue.appointmentTimeTo = this.momentService.toHoursMints24(formValue.appointmentTimeTo)
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    let crudService: Observable<IApplicationResponse> = (!this.unAssignedFom.get('appointmentId').value) ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.APPOINTMETNTS_UNASSIGN_RESCHEDULE, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.APPOINTMETNTS_UNASSIGN_RESCHEDULE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      this.showCommonAlerAfterResponse(response)

      if (response.isSuccess) {
        // this.appointmentLogId = null;
        this.unAssignedFom.reset()
        this.unAssignedFom.get('modifiedUserId').setValue(this.loggedUser ? this.loggedUser.userId : '')
      }
    })

  }

  /**
   * assing technician for selected appintment
   */
  doPostSchedule() {
    let formValue = {
      appointmentLogId: this.appointmentLogId ? this.appointmentLogId : this.appointmentId,
      appointmentId: this.appointmentId,
      modifiedUserId: this.loggedUser.userId,
      technicianId: this.selectedUser.technicianId,
      isOverCapacity: this.isOverCapacity

    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.APPOINTMETNT_TECHNICIAN_ALLOCATION, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        // this.router.navigateByUrl('/out-of-office/manager-request?tab=0');
        this.reLoadData()
      }
    })
  }

  /**
    * appinement booking calender view ends
    */


  /**
   * tech allocation calender view starts
   */

  viewDate = new Date();

  actions: CalendarEventAction[] = [// For edit button
    {
      label: '<i class="fa fa-pencil"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      },
    },
    // {
    //   label: '<i class="fa fa-trash"></i>',
    //   a11yLabel: 'Delete',
    //   onClick: ({ event }: { event: CalendarEvent }): void => {
    //     this.events = this.events.filter((iEvent) => iEvent !== event);
    //     this.handleEvent('Deleted', event);
    //   },
    // },
  ];

  // users = users;
  users = [];

  colors: any = {
    red: {
      primary: '#ad2121',
      secondary: '#FAE3E3',
    },
    blue: {
      primary: '#1e90ff',
      secondary: '#D1E8FF',
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA',
    },
    orange: {
      primary: '#ffa500',
      secondary: '#fbe2b2',
    },
    green: {
      primary: '#006A54',
      secondary: '#e8fde7',
    },
  };

  events: CalendarEvent[] = []

  findPublicholiday(date) {
    return this.users?.find(el => el?.start?.toLocaleDateString() == date?.toLocaleDateString() && el?.isPublicHoliday);
  }
  /**
   * default calender render purpose
   * @param renderEvent
   */
  beforeMonthViewRender(renderEvent: CalendarMonthViewBeforeRenderEvent): void {
    renderEvent.body.forEach((day) => {
      const dayOfMonth = day.date.getDate();
      // if (dayOfMonth > 5 && dayOfMonth < 10 && day.inMonth) {
      // day.cssClass = 'calendar-bg-red';
      // }
    });
  }

  /**
   * calender render purpose
   * @param renderEvent
   */
  beforeWeekViewRender(renderEvent: CalendarWeekViewBeforeRenderEvent) {
    renderEvent.hourColumns.forEach((hourColumn, index) => {

      hourColumn.hours.forEach((hour) => {
        hour.segments.forEach((segment) => {

          var dayIndex = hourColumn.events.length > 0 ? (hourColumn.events[0]?.event['isPublicHoliday'] ? index : null) : null
          // const publicHolidayvalidate = this.findPublicholiday(segment.date);
          if (
            // segment.date.getHours() >= 9 &&
            // segment.date.getHours() <= 12 &&
            segment.date.getDay() === 0 ||
            segment.date.getDay() === 6 ||
            segment.date.getDay() === dayIndex
          ) {
            segment.cssClass = 'calendar-bg-red';
          }
        });
      });
    });
  }

  /**
   * calender render purpose
   * @param renderEvent
   */
  beforeDayViewRender(renderEvent: CalendarDayViewBeforeRenderEvent) {
    renderEvent.hourColumns.forEach((hourColumn, index) => {
      hourColumn.hours.forEach((hour) => {
        hour.segments.forEach((segment) => {
          var dayIndex = hourColumn.events.length > 0 ? (hourColumn?.events[0]?.event['isPublicHoliday'] ? index : null) : null
          if (
            // segment.date.getHours() >= 9 &&
            // segment.date.getHours() <= 12 &&
            segment.date.getDay() === 0 ||
            segment.date.getDay() === 6 ||
            segment.date.getDay() === dayIndex
          ) {
            segment.cssClass = 'calendar-bg-red';
          }
        });
      });
    });
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {

    if (!newStart || !newEnd) {
      return
    }
    // if (newStart < this.minDate) {
    //   return
    // }
    let isAfter = this.momentService.isAfter(newStart)
    if (!isAfter) {
      this.snakbarService.openSnackbar('You can not schedule for previous day/time ', ResponseMessageTypes.WARNING)
      return
    }
    let isSameDay = this.momentService.isSameDayTime(event.start, newStart)
    if (isSameDay) {
      return
    }
    // event.start = newStart;
    // event.end = newEnd;
    // this.events = [...this.events];
    // this.refresh.next();

    // this.onHoursSegmentDragged(event, newStart, newEnd)
    this.handleEventDragged(event, newStart, newEnd)
  }

  userChanged({ event, newUser }) {
    event.color = newUser.color;
    event.meta.user = newUser;
    this.events = [...this.events];
  }

  userSelected(user) {
    if (user.isSelectable)
      this.selectedUser = user
  }

  onDayChange() {

    this.scheduleForm.get('appointmentDate').setValue(this.viewDate)
    this.advacedBookedValidation(this.viewDate)

  }


  convertData(date) {// Fri Feb 20 2015 19:29:31 GMT+0530 (India Standard Time)
    var isoDate = new Date(date.getTime() - date.getTimezoneOffset() * 60000).toISOString();
    //OUTPUT : 2015-02-20T19:29:31.238Z
    return isoDate
  }

  searchKeywordRequest() {
    this.searchTechForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          this.scheduleForm.get('search').setValue(val.search)
          // Object.keys(this.scheduleForm.value).forEach(key => {
          //   if (this.scheduleForm.value[key] === "") {
          //     delete this.scheduleForm.value[key]
          //   }
          // });
          // Object.keys(this.filterForm.value).forEach(key => {
          //   if (this.filterForm.value[key] === "" || this.filterForm.value[key]) {
          //     delete this.filterForm.value[key]
          //   }
          // });
          // let filterdNewData = Object.entries(this.filterForm.value).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

          // let searchData = Object.assign({}, filterdNewData, this.scheduleForm.value)
          // let filterdNewData1 = Object.entries(searchData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

          // return of(this.getTechniciansList(filterdNewData1));
          return of(this.reLoadData())
        })
      )
      .subscribe();
  }

  /**
   * load calender list data and technician list data
   * @param otherParams
   */
  getTechniciansList(otherParams?: object) {
    if (Object.keys(otherParams).length > 0) {
      // logic for split columns and its values to key value pair

      Object.keys(otherParams).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          otherParams[key] = this.momentService.localToUTCDateTime(otherParams[key])
          // otherParams[key] =  this.convertData(otherParams[key])


          // otherParams[key] = this.momentService.localToUTC(otherParams[key]);
        } else {
          otherParams[key] = otherParams[key];
        }
      });
    }
    // this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.loadingEvents = true
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, otherParams['scheduleType'] == '1' ? OutOfOfficeModuleApiSuffixModels.APPOINTMETNT_CALENDER : OutOfOfficeModuleApiSuffixModels.APPOINTMETNT_TECHNICIAN_CALENDER, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);

        this.loadingEvents = false
        if (response.resources) {
          this.alertMessage = response.resources.alertMessage
          this.canBookAppointment = response.resources.canBookAppointment
          this.isReschedule = response.resources.showRescheduleDialog
          this.isUnassignReschedule = response.resources.showUnassignRescheduleDialog
          this.canBookOverCapacity = response.resources.canBookOverCapacity
          this.overCapacityAlertMessage = response.resources.overCapacityAlertMessage;
          if (otherParams['scheduleType'] == '1') {
            this.serviceCallNo = response?.resources?.serviceCallNumber;
          }
          this.selectedUser = null
          this.events = []
          this.users = []
          // if (this.isReschedule || this.isUnassignReschedule) {
          //   this.appointmentId = this.appointmentId ? this.appointmentId : response.resources.appointmentId
          // }
          this.appointmentId = response.resources?.appointmentId ? response.resources?.appointmentId : this.appointmentId
          this.appointmentLogId = response.resources?.appointmentLogId ? response.resources?.appointmentLogId : this.appointmentLogId

          if (this.scheduleForm.get('scheduleType').value == '1') {
            response.resources.rows.forEach((element, index) => {
              element.appointmentId = response.resources.appointmentId
              element.id = element.technicianId,
                element.name = element.technicianName,
                // element.isPublicHoliday = index == 2 ? true : false,
                element.title = response.resources.serviceCallNumber,
                element.start = element.appointmentDate ? new Date(element.appointmentDate) : null,
                element.end = element.appointmentDate ? new Date(element.appointmentDate) : null
              // element.allDay = true
              if (element.isPublicHoliday) {
                this.events.push({
                  start: element.appointmentDate ? new Date(element.appointmentDate) : null,
                  title: element?.holidayName ? element.holidayName : 'Public Holiday',
                  allDay: true,
                  meta: {
                    type: 'holiday',
                  },
                  color: this.colors.green
                })
              }
              if (element.appointments.length > 0) {
                element.appointments.forEach(element1 => {
                  let fromHour = element1.appointmentTimeFrom ? element1.appointmentTimeFrom.split(':') : null
                  let toHour = element1.appointmentTimeTo ? element1.appointmentTimeTo.split(':') : null
                  // let hoverTextHtml = element1.hoverText.replace("</br>", "\n");
                  let existAppoitmnt = {
                    id: element1?.appointmentId,
                    appointmentId: element1?.appointmentId,
                    appointmentLogId: element1?.appointmentLogId,
                    appointmentTimeFrom: element1?.appointmentTimeFrom,
                    appointmentTimeTo: element1?.appointmentTimeTo,
                    isAppointmentChangable: element1?.isAppointmentChangeable,
                    isSpecialAppointment: element1?.isSpecialAppointment,
                    isProvisionalBooking: element1?.isProvisionalBooking,
                    isPublicHoliday: element.isPublicHoliday,
                    hoverText: element1.hoverText,
                    lable: element1.serviceCallNumber,
                    name: null,
                    title: ' <span title="' + element1.hoverText + '">' + element1.serviceCallNumber + ' </span> ',
                    start: element1?.appointmentTimeFrom ? new Date(this.momentService.addHoursMinits(element.appointmentDate, fromHour[0], fromHour[1])) : null,
                    end: element1?.appointmentTimeTo ? new Date(this.momentService.addHoursMinits(element.appointmentDate, toHour[0], toHour[1])) : null,
                    color: element1?.cssClass === 'status-label-blue' ? this.colors.blue : element1.cssClass === 'status-label-red' ? this.colors.red : element1.cssClass === 'status-label-orange' ? this.colors.orange : null,
                    actions: element1?.serviceCallNumber && element1?.serviceCallNumber?.toLowerCase()?.indexOf('recurring') == -1 ? this.actions : null,
                    draggable: element1?.serviceCallNumber && element1?.serviceCallNumber?.toLowerCase()?.indexOf('recurring') == -1 ? (element1.isProvisionalBooking ? false : true) : false,
                    // actions: element1.serviceCallNumber ? this.actions : null,
                    // draggable: element1.serviceCallNumber ? (element1.isProvisionalBooking ? false : true) : false,
                  }
                  this.events.push(existAppoitmnt)
                })




              } else {
                //showing holiday background color purpose only stars here
                if (element.isPublicHoliday) {
                  let existAppoitmnt = {
                    isPublicHoliday: element.isPublicHoliday,
                    start: element.appointmentDate ? startOfDay(new Date(element.appointmentDate)) : null,
                    end: element.appointmentDate ? addMinutes(new Date(element.appointmentDate), 1) : null,
                    color: this.colors.red,
                    title: '',
                    id: null,
                    appointmentId: null,
                    appointmentLogId: null,
                    appointmentTimeFrom: null,
                    appointmentTimeTo: null,
                    isAppointmentChangable: null,
                    isSpecialAppointment: null,
                    isProvisionalBooking: null,
                    hoverText: null,
                    lable: '',
                    name: null,
                    draggable: false,
                  }
                  this.events.push(existAppoitmnt)
                }
                //showing holiday background color purpose only ends here
              }
            });
            this.users = response.resources.rows
            // this.events = response.resources.rows


            let exist = this.appointmentId ? this.events.find(x => x.id == this.appointmentId) : null
            if (exist) {
              this.selectedEventsByAppointmentId = exist
            } else {
              this.selectedEventsByAppointmentId = this.selectedEventsByAppointmentId
            }

          } else {
            this.technitianList = response.resources.technicians
            response.resources.rows.forEach(element => {
              element.appointmentId = response.resources.appointmentId
              element.id = element.technicianId,
                element.name = element.technicianName,
                // element.title = response.resources.serviceCallNumber,
                // element.start = element.appointmentDate ? new Date(element.appointmentDate) : null,
                // element.end = element.appointmentDate ? new Date(element.appointmentDate) : null
                element.meta = {
                  user: {
                    id: element.technicianId,
                    name: element.technicianName
                  }
                }
              if (element.technAllocations.length > 0) {
                element.technAllocations.forEach(element1 => {
                  let fromHour = element1.appointmentTimeFrom ? element1.appointmentTimeFrom.split(':') : null
                  let toHour = element1.appointmentTimeTo ? element1.appointmentTimeTo.split(':') : null
                  let existAppoitmnt = {
                    id: element1.appointmentId,
                    name: null,
                    lable: element1.serviceCallNumber,
                    title: ' <span title="' + element1.hoverText + '">' + element1.serviceCallNumber + ' </span> ',
                    start: element1.appointmentTimeFrom ? new Date(this.momentService.addHoursMinits(element1.appointmentDate, fromHour[0], fromHour[1])) : null,
                    end: element1.appointmentTimeTo ? new Date(this.momentService.addHoursMinits(element1.appointmentDate, toHour[0], toHour[1])) : null,
                    // start: new Date(this.momentService.addHoursMinits(element1.appointmentDate, "10", "0")),
                    // end:  new Date(this.momentService.addHoursMinits(element1.appointmentDate, '10', "30")),
                    color: element1.cssClass === 'status-label-blue' ? this.colors.blue : element1.cssClass === 'status-label-red' ? this.colors.red : element1.cssClass === 'status-label-orange' ? this.colors.orange : null,
                    meta: {
                      user: {
                        id: element.technicianId,
                        name: element.technicianName
                      }
                    }
                  }
                  this.events.push(existAppoitmnt)
                })
              }
            });
            this.users = response.resources.rows
            // this.events = response.resources.rows
          }

          setTimeout(() => {
            if ($(".cal-time-events-wrapper")?.length == 0) {
              $('.cal-time-events').wrapAll('<div class="cal-time-events-wrapper"></div>');
            }


            // if (this.validationData.appointmentStartTime == '09:00:00') {
            // if (!this.validationData.preferredTimeSlot || this.validationData.preferredTimeSlot.toLowerCase().includes('am')) {
            $('.cal-time-events-wrapper').animate({
              scrollTop: this.momentService.scrollCalenderByTime(this.validationData['appointmentStartTime'])
            }, 1000, function () {
            });

            // }

            // }
          }, 1000);


        }
      },
        error => {
          this.loadingEvents = false
        });
  }

  onDaySelect() {
    this.scheduleForm.get('appointmentDate').setValue(this.viewDate)
    this.advacedBookedValidation(this.viewDate)

  }


  getBranchByDivisionId(divisionId): void {

    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_BRANCHES, prepareRequiredHttpParams({
      divisionId: divisionId
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.branchList = response.resources;
          if (this.validationData['branchId']) {
            let exist = this.branchList.find(x => x.branchId == this.validationData['branchId'])
            this.filterForm.get('branchIds').setValue(exist ? [{ branchId: this.validationData['branchId'], branchName: this.validationData['branchName'] }] : [])
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  getTechAreaByBranchId(branchIds): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_AREAS, prepareRequiredHttpParams(
      this.validationData['technicianCallTypeId'] ? {
        branchIds: branchIds,
        TechnicianCallTypeId: this.validationData['technicianCallTypeId']
      } :
        {
          branchIds: branchIds,
        }
    ))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.techAreaList = response.resources;
          // if (this.validationData.techAreaId) {
          //   let exist = this.techAreaList.find(x => x.id == this.validationData.techAreaId)
          //   this.filterForm.get('techAreaIds').setValue(exist ? [{ id: this.validationData.techAreaId, displayName: this.validationData.techAreaName }] : [])
          // }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTechnitianByTechAreaId(techAreaIds): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_TECHNICIAN, prepareRequiredHttpParams({
      techAreaIds: techAreaIds,
      CallInitiationId: this.installationId
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.technitianList = response.resources;
          if (this.validationData['technicianId']) {
            let exist = this.technitianList.find(x => x.id == this.validationData['technicianId'])
            this.filterForm.get('technicianId').setValue(exist ? { id: this.validationData['technicianId'], displayName: this.validationData['technicianName'] } : null)
          }

        }

        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  navigateToPage() {
    if (this.radioRemovalWorkListId) {
      this.router.navigate(['/customer', 'manage-customers', 'radio-removal-call'], {
        queryParams: {
          // customerId: this.installationId
          customerId: this.customerId,
          customerAddressId: this.customerAddressId,
          appointmentId: this.appointmentId ? this.appointmentId : null,
          initiationId: this.installationId,
          callType: this.callType, // 2- service call, 1- installation call
          radioRemovalWorkListId: this.radioRemovalWorkListId ? this.radioRemovalWorkListId : null
        }, skipLocationChange: true
      });
    } else {
      this.router.navigate(['/customer', 'manage-customers', 'call-initiation'], {
        queryParams: {
          // customerId: this.installationId
          customerId: this.customerId,
          customerAddressId: this.customerAddressId,
          appointmentId: this.appointmentId ? this.appointmentId : null,
          initiationId: this.installationId,
          callType: this.callType // 2- service call, 1- installation call

        }, skipLocationChange: true
      });
    }

  }





  confirmSchedule() {
    // let isMuitiple = this.isMultipleAppointmentCheck(this.appointmentFom.get('bookedSlots').value[0].appointmentTimeFrom)
    // if (isMuitiple) {
    //   var msg = 'Are you sure that you want to post schedule for <b>' + this.momentService.toFormateType(this.appointmentFom.get('bookedSlots').value[0].appointmentTimeFrom,'YYYY-MM-DD hh:mm a') + ' to ' + this.momentService.toFormateType(this.appointmentFom.get('bookedSlots').value[0].appointmentTimeTo,'YYYY-MM-DD hh:mm a') + '</b>'
    // } else {
    let lastIndex = this.appointmentFom.get('bookedSlots').value.length - 1
    var msg = 'Are you sure that you want to post schedule for <b>' + this.momentService.toFormateType(this.appointmentFom.get('bookedSlots').value[0].appointmentTimeFrom, 'DD-MM-YYYY hh:mm a') + ' to ' + this.momentService.toFormateType(this.appointmentFom.get('bookedSlots').value[lastIndex].appointmentTimeTo, 'DD-MM-YYYY hh:mm a') + '</b>'
    // }
    this.confirmationService.confirm({
      header: this.callType == '1' ? 'Installation Call' : this.callType == '2' ? 'Service Call' :
        this.callType == '3' ? 'Special Projects' : '---',
      // message: 'Are you sure that you want to post schedule for ' + this.appointmentFom.get('bookedSlots').value[0].appointmentTimeFrom + ' to ' + this.appointmentFom.get('bookedSlots').value[0].appointmentTimeTo,
      message: msg,
      accept: () => {
        //Actual logic to perform a confirmation
        this.doAppintment()
      },
      reject: () => {
        this.reLoadData()
      }
    });
  }

  confirmPostSchedule() { //only for 4 -post schedule
    // let isMuitiple = this.isMultipleAppointmentCheck(this.appointmentFom.get('bookedSlots').value[0].appointmentTimeFrom)
    // if (isMuitiple) {
    //   var msg = 'Are you sure that you want to post schedule for <b>' + this.momentService.toFormateType(this.appointmentFom.get('bookedSlots').value[0].appointmentTimeFrom,'YYYY-MM-DD hh:mm a') + ' to ' + this.momentService.toFormateType(this.appointmentFom.get('bookedSlots').value[0].appointmentTimeTo,'YYYY-MM-DD hh:mm a') + '</b>'
    // } else {
    var msg = 'Are you sure that you want to post schedule for <b>' + this.momentService.toFormateType(this.viewDate, 'DD-MM-YYYY') + '</b>'
    // }
    this.confirmationService.confirm({
      header: this.callType == '1' ? 'Installation Call' : this.callType == '2' ? 'Service Call' :
        this.callType == '3' ? 'Special Projects' : '---',
      message: msg,
      accept: () => {
        //Actual logic to perform a confirmation
        this.doPostSchedule()

      },
      reject: () => {
        this.reLoadData()
      }
    });
  }

  confirmTechnician(type) {
    // 1-Creating New appointment with Technician
    // 2-During Tech Allocation with Technician
    // 3-During Rescheduling Process with Technician

    this.confirmationService.confirm({
      header: this.callType == '1' ? 'Installation Call' : this.callType == '2' ? 'Service Call' :
        this.callType == '3' ? 'Special Projects' : '---',
      message: 'Booking Scheduled is over capacity',
      acceptLabel: 'Aknowledge',
      accept: () => {
        //Actual logic to perform a confirmation
        if (type == 1) {
          if (this.showConfirmScheduleAlert) {
            this.confirmSchedule()
          } else {
            this.doAppintment()
          }
        } else if (type == 2) {
          this.doReAppointment()
        } else if (type == 3) {
          this.doUnassignReschedule()
        } else if (type == 4) {
          if (this.isRoleAccessForPostSchedule()) {
            this.confirmPostSchedule()
          } else {
            this.doPostSchedule()
          }
        }
      },
      reject: () => {
        if (type == 1) {
          this.showDialog = false
        } else if (type == 2) {
          this.showRescheduleDialog = false
        } else if (type == 3) {
          this.showUnassignDialog = false
        } else if (type == 4) {
        }
        this.reLoadData()
      }
    });
  }







  checkOverCapacity(type, appointmentDate, technicianId) {

    let appointmentDate1 = this.momentService.toFormateType(appointmentDate, 'YYYY-MM-DD')
    let addToMints1 = this.momentService.addMinits(appointmentDate, this.scheduleForm.get('estimatedTime').value)
    addToMints1 = this.momentService.addMinits(addToMints1, this.scheduleForm.get('travelTime').value)
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHALLOCATION_CAPACITY, null, false,
      prepareGetRequestHttpParams(null, null, {
        AppointmentDate: appointmentDate1,
        CallInitiationId: this.installationId,
        AppointmentTimeFrom: this.momentService.toFormateType(appointmentDate, 'HH:MM'),
        AppointmentTimeTo: this.momentService.toFormateType(new Date(addToMints1), 'HH:MM'),
        technicianId: technicianId
      }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.isOverCapacity = response.resources.isOverCapacity
          if (this.isOverCapacity) {
            if (this.canBookOverCapacity) {
              this.confirmTechnician(type)
            } else {
              this.snakbarService.openSnackbar(this.overCapacityAlertMessage, ResponseMessageTypes.WARNING)
            }
          } else {
            if (type == 1) {
              if (this.showConfirmScheduleAlert) {
                this.confirmSchedule()
              } else {
                this.doAppintment()
              }
            } else if (type == 2) {
              this.doReAppointment()
            } else if (type == 3) {
              this.doUnassignReschedule()
            } else if (type == 4) {
              if (this.isRoleAccessForPostSchedule()) {
                this.confirmPostSchedule()
              } else {
                this.doPostSchedule()
              }
            }
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  checkTechnicianAvailability(type) {
    let lastIndex = this.appointmentFom.get('bookedSlots').value.length - 1
    let branchIds = []
    let techAreaIds = []

    if (this.filterForm.get('branchIds').value && this.filterForm.get('branchIds').value.length > 0) {
      this.filterForm.get('branchIds').value.forEach(element => {
        branchIds.push(element.branchId)
      });
    }
    if (this.filterForm.get('techAreaIds').value && this.filterForm.get('techAreaIds').value.length > 0) {
      this.filterForm.get('techAreaIds').value.forEach(element => {
        techAreaIds.push(element.id)
      });
    }
    let filteredData = Object.assign({},
      { divisionId: this.filterForm.get('divisionId').value ? this.filterForm.get('divisionId').value.id : '' },
      { branchIds: this.filterForm.get('branchIds').value ? branchIds.toString() : '' },
      { techAreaIds: this.filterForm.get('techAreaIds').value ? techAreaIds.toString() : '' },
      { technicianId: this.filterForm.get('technicianId').value ? this.filterForm.get('technicianId').value.id : '' },
      { includeSpecialProjectTechnician: this.filterForm.get('includeSpecialProjectTechnician').value ? this.filterForm.get('includeSpecialProjectTechnician').value : false },
      { callInitiationId: this.installationId },
      { estimatedTime: this.estimatedTime },
      { travelTime: this.travelTime },
      // { appointmentFrom: this.appointmentFom.get('bookedSlots').value[0].appointmentTimeFrom },
      // { appointmentTo: this.appointmentFom.get('bookedSlots').value[lastIndex].appointmentTimeTo },
      { appointmentFrom: this.momentService.toFormateType(this.appointmentFom.get('bookedSlots').value[0].appointmentTimeFrom, 'YYYY-MM-DD HH:mm') },
      // { appointmentTo: this.momentService.toFormateType(this.appointmentFom.get('bookedSlots').value[lastIndex].appointmentTimeTo, 'YYYY-MM-DD HH:mm') },
      { appointmentTo: this.momentService.toFormateType(this.appointmentFom.get('bookedSlots').value[0].appointmentTimeTo, 'YYYY-MM-DD HH:mm') },
    );
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => ((v == null || v == '') ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    if (!this.unAssignedFom.value.isReschedule && type == 3) { // no need to call capacity api and tech allocation availability while unassign. but need to call capacity api for unassign-reschedule
      this.doUnassignReschedule();
    } else if (this.unAssignedFom.value.isReschedule || type != 3) {
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHALLOCATION_TECHNICIAN_AVAILABILITY, null, false,
        prepareGetRequestHttpParams(null, null, filterdNewData))
        .subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response?.isSuccess) {
            if (response?.resources?.canBookCall) {
              //auto appointment start here
              if (response.resources.technicians.length > 0) {
                this.filterForm.get('technicianId').setValue({ id: response.resources.technicians[0].technicianId, displayName: response.resources.technicians[0].technicianName })
                // if (!this.unAssignedFom.value.isReschedule && type == 3) { // no need to call capacity api while unassign. but need to call capacity api for unassign-reschedule
                //   this.doUnassignReschedule()
                // } else {
                this.checkOverCapacity(type, this.appointmentDate, response.resources.technicians[0].technicianId)
                // }
              } else {

              }
              //auto appointment ends here
            } else {
              this.snakbarService.openSnackbar(response.resources.alertMessage ? response.resources.alertMessage : 'There is no available dates with suitable Technician for your Call', ResponseMessageTypes.WARNING)
              this.reLoadData()
            }
          }
        });
    }
  }

  openAlertDialog(msg) {
    this.alertDialog = true
    this.alertMsg = msg
  }

  reLoadData() {
    this.showResult = false

    let branchIds = []
    let techAreaIds = []

    if (this.filterForm.get('branchIds').value && this.filterForm.get('branchIds').value.length > 0) {
      this.filterForm.get('branchIds').value.forEach(element => {
        branchIds.push(element.branchId)
      });
    }
    if (this.filterForm.get('techAreaIds').value && this.filterForm.get('techAreaIds').value.length > 0) {
      this.filterForm.get('techAreaIds').value.forEach(element => {
        techAreaIds.push(element.id)
      });
    }
    let filteredData = Object.assign({},
      { divisionId: this.filterForm.get('divisionId').value ? this.filterForm.get('divisionId').value.id : '' },
      { branchIds: this.filterForm.get('branchIds').value ? branchIds.toString() : '' },
      { techAreaIds: this.filterForm.get('techAreaIds').value ? techAreaIds.toString() : '' },
      { technicianId: this.filterForm.get('technicianId').value ? this.filterForm.get('technicianId').value.id : '' },
      { includeSpecialProjectTechnician: this.filterForm.get('includeSpecialProjectTechnician').value ? this.filterForm.get('includeSpecialProjectTechnician').value : false },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "") {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

    let scheduledData = this.scheduleForm.value
    scheduledData.appointmentDate = this.viewDate
    Object.keys(scheduledData).forEach(key => {
      if (scheduledData[key] === "") {
        delete scheduledData[key]
      }
    });
    let searchData = Object.assign({}, filterdNewData, scheduledData)
    let filterdNewData1 = Object.entries(searchData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

    return of(this.getTechniciansList(filterdNewData1));

  }

  showCommonAlerAfterResponse(response) {
    if (response.isSuccess) {
      // if (this.callType == '2') {
      // this.reSchedule = false
      if (response.resources) {
        let jsonStri = JSON.parse(response.resources)
        this.appointmentId = jsonStri.AppointmentId
        this.appointmentLogId = jsonStri.AppointmentLogId
        if (jsonStri.ResponseMessage) {
          this.isAlertCheckbox = true
          this.openAlertDialog(jsonStri.ResponseMessage)
        }
      }
      // }
      this.reLoadData()
    } else if (!response.isSuccess && response.statusCode == 409) {
      let jsonStri = JSON.parse(response.resources)
      this.appointmentId = jsonStri.AppointmentId
      this.appointmentLogId = jsonStri.AppointmentLogId
      if (jsonStri.ResponseMessage) {
        this.isAlertCheckbox = false
        this.openAlertDialog(jsonStri.ResponseMessage)
      }
      this.reLoadData()

    }
  }


  openRecurringAppointmentDialog() {
    const ref = this.dialogService.open(RecurringAppointmentComponent, {
      header: 'Recurring Schedule',
      baseZIndex: 1000,
      width: '950px',
      closable: true,
      showHeader: true,
      data: {
        installationId: this.installationId,
        createdUserId: this.loggedUser?.userId,
        showSubmitBtn: true
      },
    });
    ref.onClose.subscribe((res) => {
      if (res) {
        this.reLoadData();
      }
    });
  }

  getYearRange() {
    return CURRENT_YEAR_TO_NEXT_FIFTY_YEARS;
  }

  getDateFormat() {
    return adjustDateFormatAsPerPCalendar();
  }

  getBrowserClass() {
    return window.navigator?.userAgent?.indexOf('Firefox') != -1 ? true : false;
  }

  navigateToRoaster() {
    this.router.navigate(['/customer/stand-by-roster'], {
      queryParams: {
        customerId: this.customerId,
        addressId: this.customerAddressId,
        appointmentId: this.appointmentId ? this.appointmentId : null,
        installationId: this.scheduleForm.get('callInitiationId').value,
        callType: this.callType, // 2- service call, 1- installation call
        nextAppointmentDate: this.scheduleForm.get('appointmentDate').value,
        appointmentLogId: this.appointmentLogId,
        estimatedTime: this.scheduleForm.get('estimatedTime').value,
        travelTime: this.scheduleForm.get('travelTime').value,
      }
    })
  }
}
