import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { MultiSelectModule } from 'primeng/multiselect';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RecurringAppointmentModule } from '../recurring-appointment/recurring-appointment.module';
import { DayViewSchedulerTechAllocationComponent } from './day-view-scheduler-tech-allocation/day-view-scheduler-tech-allocation.component';
import { TechnicianAllocationCalenderViewRoutingModule } from './technician-allocation-calender-view-routing.module';
import { TechnicianAllocationCalenderViewComponent } from './technician-allocation-calender-view.component';

@NgModule({
  declarations: [TechnicianAllocationCalenderViewComponent, DayViewSchedulerTechAllocationComponent],
  imports: [
    CommonModule,
    TechnicianAllocationCalenderViewRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    ConfirmDialogModule,
    MultiSelectModule,
    RadioButtonModule,
    RecurringAppointmentModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    })
  ]
})
export class TechnicianAllocationCalenderViewModule { }
