
class AppointmentBookingModel {
    constructor(stockSwapRequestModel?: AppointmentBookingModel) {
        this.callInitiationId = stockSwapRequestModel ? stockSwapRequestModel.callInitiationId == undefined ? '' : stockSwapRequestModel.callInitiationId : '';
        this.isAppointmentChangeable = stockSwapRequestModel ? stockSwapRequestModel.isAppointmentChangeable == undefined ? null : stockSwapRequestModel.isAppointmentChangeable : null;
        this.isFixedAppointment = stockSwapRequestModel ? stockSwapRequestModel.isFixedAppointment == undefined ? null : stockSwapRequestModel.isFixedAppointment : null;
        this.isOverCapacity = stockSwapRequestModel ? stockSwapRequestModel.isOverCapacity == undefined ? null : stockSwapRequestModel.isOverCapacity : null;
        this.technicianId = stockSwapRequestModel ? stockSwapRequestModel.technicianId == undefined ? '' : stockSwapRequestModel.technicianId : '';
        this.createdUserId = stockSwapRequestModel ? stockSwapRequestModel.createdUserId == undefined ? '' : stockSwapRequestModel.createdUserId : '';
        this.modifiedUserId = stockSwapRequestModel ? stockSwapRequestModel.modifiedUserId == undefined ? '' : stockSwapRequestModel.modifiedUserId : '';
        this.bookedSlots = stockSwapRequestModel ? stockSwapRequestModel.bookedSlots == undefined ? [] : stockSwapRequestModel.bookedSlots : [];

    }
    callInitiationId: string
    replaceStockCodeId: string
    isAppointmentChangeable: boolean
    isFixedAppointment: boolean
    isOverCapacity: boolean
    technicianId: string
    createdUserId: string
    modifiedUserId: string
    bookedSlots: BookedSlotsListModel[];
}

class BookedSlotsListModel {
    constructor(stockSwapItemsListModel: BookedSlotsListModel) {
        this.appointmentDate = stockSwapItemsListModel ? stockSwapItemsListModel.appointmentDate == undefined ? '' : stockSwapItemsListModel.appointmentDate : '';
        this.minAppointmentDate = stockSwapItemsListModel ? stockSwapItemsListModel.minAppointmentDate == undefined ? '' : stockSwapItemsListModel.minAppointmentDate : '';
        this.appointmentTimeFrom = stockSwapItemsListModel ? stockSwapItemsListModel.appointmentTimeFrom == undefined ? '' : stockSwapItemsListModel.appointmentTimeFrom : '';
        this.appointmentTimeTo = stockSwapItemsListModel ? stockSwapItemsListModel.appointmentTimeTo == undefined ? '' : stockSwapItemsListModel.appointmentTimeTo : '';
        this.estimatedTime = stockSwapItemsListModel ? stockSwapItemsListModel.estimatedTime == undefined ? null : stockSwapItemsListModel.estimatedTime : null;
    }
    appointmentDate: any;
    minAppointmentDate?: any;
    appointmentTimeFrom: any;
    appointmentTimeTo: any;
    estimatedTime: number;
}

class ProvisonalBookingApprovalModel {
    constructor(stockSwapRequestModel?: ProvisonalBookingApprovalModel) {
        this.appointmentId = stockSwapRequestModel ? stockSwapRequestModel.appointmentId == undefined ? '' : stockSwapRequestModel.appointmentId : '';
        this.isConfirmed = stockSwapRequestModel ? stockSwapRequestModel.isConfirmed == undefined ? false : stockSwapRequestModel.isConfirmed : false;
        this.createdUserId = stockSwapRequestModel ? stockSwapRequestModel.createdUserId == undefined ? '' : stockSwapRequestModel.createdUserId : '';
        this.modifiedUserId = stockSwapRequestModel ? stockSwapRequestModel.modifiedUserId == undefined ? '' : stockSwapRequestModel.modifiedUserId : '';
        this.bookedSlots = stockSwapRequestModel ? stockSwapRequestModel.bookedSlots == undefined ? [] : stockSwapRequestModel.bookedSlots : [];

    }
    appointmentId: string
    isConfirmed: boolean
    createdUserId: string
    modifiedUserId: string
    bookedSlots: BookedSlotsListModel[];
}


export { AppointmentBookingModel, BookedSlotsListModel, ProvisonalBookingApprovalModel };

