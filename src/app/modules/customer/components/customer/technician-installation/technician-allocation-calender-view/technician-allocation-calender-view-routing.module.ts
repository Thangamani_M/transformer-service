import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TechnicianAllocationCalenderViewComponent } from './technician-allocation-calender-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
      { path:'', component: TechnicianAllocationCalenderViewComponent, data: { title: 'Tech Allocation Calender View' }, canActivate:[AuthGuard]},

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class TechnicianAllocationCalenderViewRoutingModule { }
