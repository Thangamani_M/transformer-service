import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { RadioCallEscalationDialogComponent } from '@modules/customer';
@NgModule({
  declarations: [RadioCallEscalationDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule
  ],
  exports: [RadioCallEscalationDialogComponent],
  entryComponents: [RadioCallEscalationDialogComponent],
})
export class RadioCallEscalationModule { }
