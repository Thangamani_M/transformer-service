import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CrudService, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-radio-call-escalation-dialog',
  templateUrl: './radio-call-escalation-dialog.component.html'
})
export class RadioCallEscalationDialogComponent implements OnInit {

  todayDate: any;
  callEscalationDialogForm: FormGroup;
  iscallEscalationSubmit: boolean;
  callEscalationsubscritption: any;

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig,
    public ref: DynamicDialogRef, private crudService: CrudService,
    private momentService: MomentService,) {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.config?.data?.isRadioCallEscalation) {
      this.iscallEscalationSubmit = this.config?.data?.feedback && this.config?.data?.radioRemovalEscalationId;
    }
  }

  ngOnInit(): void {
    this.todayDate = this.config?.data?.preferredDate ? new Date(this.config?.data?.preferredDate) : new Date();
    this.initForm();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.callEscalationDialogForm.get('PreferredDateTime').setValue(this.config?.data?.preferredDate ? new Date(this.config?.data?.preferredDate) : null);
    }, 500)
  }

  initForm() {
    this.callEscalationDialogForm = new FormGroup({
      initiationId: new FormControl({ value: this.config?.data?.initiationId, disabled: false }),
      radioRemovalWorkListId: new FormControl({ value: this.config?.data?.radioRemovalWorkListId, disabled: false }),
      preferredDate: new FormControl(null),
      comments: new FormControl({
        value: this.config?.data?.comments ? this.config?.data?.comments : '',
        disabled: this.config?.data?.radioRemovalEscalationId
      }),
      feedback: new FormControl({
        value: this.config?.data?.feedback ? this.config?.data?.feedback : '',
        disabled: this.config?.data?.feedback
      }),
      createdUserId: new FormControl({ value: this.config?.data?.createdUserId, disabled: false }),
    })
    if(this.config?.data?.isRadioCallEscalation) {
      this.callEscalationDialogForm = setRequiredValidator(this.callEscalationDialogForm, ["feedback"]);
      this.callEscalationDialogForm.get('comments').disable();
    } else if(this.config?.data?.isRadioEscalation) {
      this.callEscalationDialogForm = setRequiredValidator(this.callEscalationDialogForm, ["initiationId", "comments"]);
    }
  }

  btnCloseClick(val?:any) {
    if(val) {
      const rowData = {
        initiationId: this.config?.data?.validateData[0]?.id,
        initiationName: this.config?.data?.validateData[0]?.displayName,
        validateType: this.config?.data?.validateType,
      }
      this.ref.close(rowData);
    } else {
      this.ref.close();
    }
  }

  onSubmitCallEscalationDialog() {
    if (this.iscallEscalationSubmit || !this.callEscalationDialogForm?.valid) {
      return;
    }
    const callEscalationObject = this.callEscalationDialogForm.getRawValue();
    delete callEscalationObject.initiationId;
    if (callEscalationObject?.preferredDate) {
      callEscalationObject.preferredDate = this.momentService.toMoment(callEscalationObject?.preferredDate).format('YYYY-MM-DD HH:mm:ss.ms');
      // callEscalationObject.preferredDate = callEscalationObject?.preferredDate?.toISOString();
    } else {
      callEscalationObject.preferredDate = null;
    }
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    if (this.callEscalationDialogForm.valid) {
      this.iscallEscalationSubmit = true;
      let api = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RADIO_REMOVAL_CALL_ESCALATION, callEscalationObject)
      if(this.config?.data?.isRadioCallEscalation) {
        delete callEscalationObject.comments;
        delete callEscalationObject.radioRemovalWorkListId;
        callEscalationObject.radioRemovalApprovalId = this.config?.data?.radioRemovalApprovalId;
        callEscalationObject.radioRemovalEscalationId = this.config?.data?.radioRemovalEscalationId;
        api = this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RADIO_REMOVAL_CALL_ESCALATION_APPROVAL, callEscalationObject)
      } else {
        delete callEscalationObject.feedback;
        callEscalationObject.radioRemovalEscalationId = null;
      }
      api.subscribe((res: any) => {
        this.rxjsService.setDialogOpenProperty(false);
        this.iscallEscalationSubmit = false;
        if (res?.isSuccess == true && res?.statusCode == 200) {
          this.ref.close(res);
        }
        // this.config?.data?.validateType ? this.ref.close() : this.ref.close(res);
      })
    }
  }

}
