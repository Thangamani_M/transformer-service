import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { CanDeactivateGuard, SharedModule } from '@app/shared';
import { PrimengConfirmDialogPopupModule } from '@app/shared/components/primeng-confirm-dialog-popup/primeng-confirm-dialog-popup.module';
import { MaterialModule } from '@app/shared/material.module';
import { CustomerSharedModule } from '@modules/customer/shared/customer-shared.module';
import { InspectionCallBackDialogModule } from '@modules/technical-management/components/inspection-call/inspection-call-back-dialog/inspection-call-back-dialog.module';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ActualTimeDialogModule } from '../actual-time-dialog/actual-time-dialog.module';
import { CallEscalationModule } from '../call-escalation-dialog/call-escalation-dialog.module';
import { RecurringAppointmentModule } from '../recurring-appointment/recurring-appointment.module';
import { ServiceCallInwarrantyDialogModule } from '../service-call-inwarranty-dialog/service-call-inwarranty-dialog.module';
import { ServiceCallWaringDialogModule } from '../service-call-waring-dialog/service-call-waring-dialog.module';
import { TechnicalAssistantModule } from '../technical-assistant/technical-assistant.module';
import { InitiationCallCreationAddEditComponent } from './initiation-call-creation-add-edit.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { PRCallAmountDialogModule } from '../pr-amount-details-dialog/pr-amount-details-dialog.module';

@NgModule({
  declarations: [InitiationCallCreationAddEditComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule.forChild([
        { path: '', component: InitiationCallCreationAddEditComponent, data: { title: 'Call Initiation' }, canActivate: [AuthGuard], canDeactivate: [CanDeactivateGuard]}
    ]),
    FormsModule,
    SharedModule,
    CallEscalationModule,
    ActualTimeDialogModule,
    ConfirmDialogModule,
    CustomerSharedModule,
    InspectionCallBackDialogModule,
    RecurringAppointmentModule,
    TechnicalAssistantModule,
    PrimengConfirmDialogPopupModule,
    ServiceCallWaringDialogModule,
    ServiceCallInwarrantyDialogModule,
    PRCallAmountDialogModule,
  ]
})
export class InitiationCallCreationModule { }
