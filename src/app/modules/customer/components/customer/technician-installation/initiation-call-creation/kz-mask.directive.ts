import { Directive, HostListener, Input, Output, EventEmitter, ElementRef } from '@angular/core';

import {
  NG_VALUE_ACCESSOR, ControlValueAccessor
} from '@angular/forms';

@Directive({
  selector: '[kzMask]',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: KzMaskDirective,
    multi: true
  }]
})
export class KzMaskDirective implements ControlValueAccessor {

  onTouched: any;
  onChange: any;

  @Input() kzMask: string;
  @Output() retornoKzMask = new EventEmitter();

  constructor(private el: ElementRef) { }

  writeValue(value: any): void {
    // if(!value){
    //   return
    // }
    this.el.nativeElement.value = value
    
    // var valor = value.replace(/\D/g, '');
    // var pad = this.kzMask.replace(/\D/g, '').replace(/9/g, '_');
    // var valorMask = valor + pad.substring(0, pad.length - valor.length);

  

    // var valorMaskPos = 0;
    // valor = '';
    // for (var i = 0; i < this.kzMask.length; i++) {
    //   if (isNaN(parseInt(this.kzMask.charAt(i)))) {
    //     valor += this.kzMask.charAt(i);
    //   } else {
    //     valor += valorMask[valorMaskPos++];
    //   }
    // }

    // if (valor.indexOf('_') > -1) {
    //   valor = valor.substr(0, valor.indexOf('_'));
    // }

    // if (valor.indexOf(':') > -1) {  // for minutes validation
    //   var spltvalor = valor.split(':');
    //   spltvalor[1]
    //   if(Number(spltvalor[1]) <= 59){
    //     valor = valor
    //   }else{
    //     valor = spltvalor[0] + ':'
    //   }
    // }

    // this.el.nativeElement.value = valor;
    // this.retornoKzMask.emit(this.el.nativeElement.value);

  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  @HostListener('keyup', ['$event'])
  onKeyup($event: any) {
    // codigo
    if(!$event.target.value){
      return
    }
    var valor = $event.target.value.replace(/\D/g, '');
    var pad = this.kzMask.replace(/\D/g, '').replace(/9/g, '_');
    var valorMask = valor + pad.substring(0, pad.length - valor.length);

    // retorna caso pressionado backspace
    if ($event.keyCode === 8) {
      this.onChange(valor);
      return;
    }

    if (valor.length <= pad.length) {
      this.onChange(valor);
    }

    var valorMaskPos = 0;
    valor = '';
    for (var i = 0; i < this.kzMask.length; i++) {
      if (isNaN(parseInt(this.kzMask.charAt(i)))) {
        valor += this.kzMask.charAt(i);
      } else {
        valor += valorMask[valorMaskPos++];
      }
    }

    if (valor.indexOf('_') > -1) {
      valor = valor.substr(0, valor.indexOf('_'));
    }

    if (valor.indexOf(':') > -1) {  // for minutes validation
      var spltvalor = valor.split(':');
      spltvalor[1]
      if(Number(spltvalor[1]) <= 59){
        valor = valor
      }else{
        valor = spltvalor[0] + ':'
      }
    }

    $event.target.value = valor;
    this.retornoKzMask.emit($event.target.value);
  }

  @HostListener('blur', ['$event'])
  onBlur($event: any) {
    // codigo
    if(!$event.target.value){
      return
    }
    if ($event.target.value.length === this.kzMask.length) {
      return;
    }
    this.onChange('');
    $event.target.value = '';

    this.retornoKzMask.emit($event.target.value);

  }

 

 

}