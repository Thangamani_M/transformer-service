import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DateAdapter, MatDialog, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { InstallationCallinitiationAddEditModel, OnHoldModel, VoidModel } from '@app/modules/customer/models';
import { AppState } from '@app/reducers';
import { agentLoginDataSelector, clearFormControlValidators, ConfirmDialogModel, ConfirmDialogPopupComponent, convertTreeToList, countryCodes, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, debounceTimeForSearchkeyword, ExtensionModalComponent, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, OtherService, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareAutoCompleteListFormatForMultiSelection, prepareGetRequestHttpParams, prepareRequiredHttpParams, PrimengConfirmDialogPopupComponent, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { ActualTimeDialogComponent } from '@modules/customer';
import { ActualTimingListModel, ActualTimingModel } from '@modules/customer/models/actual-timing.model';
import { CallCreationTemplateListModel, CallCreationTemplateModel } from '@modules/customer/models/call-creation-template.model';
import { CustomerModuleApiSuffixModels, CUSTOMER_COMPONENT } from '@modules/customer/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales';
import { InspectionCallBackDialogComponent } from '@modules/technical-management/components/inspection-call/inspection-call-back-dialog/inspection-call-back-dialog.component';
import { RequestType } from '@modules/technical-management/components/technical-area-manager-worklist/request-type.enum';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { ConfirmationService, DialogService } from 'primeng/api';
import { combineLatest, forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, filter, map, switchMap, takeUntil } from 'rxjs/operators';
import { CallEscalationDialogComponent } from '../call-escalation-dialog';
import { CallType } from '../customer-technical/call-type.enum';
import { PrAmountDetailsDialogComponent } from '../pr-amount-details-dialog/pr-amount-details-dialog.component';
import { RecurringAppointmentComponent } from '../recurring-appointment/recurring-appointment.component';
import { ServiceCallInwarrantyDialogComponent } from '../service-call-inwarranty-dialog/service-call-inwarranty-dialog.component';
import { ServiceCallWaringDialogComponent } from '../service-call-waring-dialog/service-call-waring-dialog.component';
import { TechnicalAssistantComponent } from "../technical-assistant/technical-assistant.component";

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL'
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY'
  }
};


enum NotesTypes {
  SERVICE_CALL_NOTES = 'Service Call Notes',
  TECH_SPECIAL_INSTRUCTION_NOTES = 'Tech Special Instruction Notes'
}
@Component({
  selector: 'app-initiation-call-creation-add-edit',
  templateUrl: './initiation-call-creation-add-edit.component.html',
  styleUrls: ['./initiation-call-creation-add-edit.component.scss'],
  providers: [ConfirmationService,
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]

})
export class InitiationCallCreationAddEditComponent implements OnInit {

  loading = false;
  holdInfoDialog: boolean = false;
  voidInfoDialog: boolean = false;
  referToSalesDialog: boolean = false;
  callCreationDialog: boolean = false;
  title: any;
  initiationDetailsData: any = {};
  initiationBasicInfo: any = {};
  initiationLightingCover: any = {};
  initiationServiceInfo: any = {};
  initiationAppointmentInfo: any = {};
  userData: UserLogin;
  initiationId: any;
  minFUpDate = new Date();
  initiatonCallCreationForm: FormGroup;
  onHoldPopupForm: FormGroup;
  voidInfoForm: FormGroup;
  reasonDropDown: any = [];
  sysyemTypeList: any = [];
  SystemTypeSubCategoryList: any = [];
  ServiceSubTypeList: any = [];
  faultDescriptionList: any = [];
  jobTypeList: any = [];
  projectList: any;
  panelList: any = [];
  serviceSubTypeId = '';
  diagnosisList = [];
  selectedIndex = 0;
  contactList: any = [];
  contactNumberList: any = [];
  isRebookDialog: boolean;
  isRebookAlertDialog: boolean;
  showRebookComments: boolean;
  isRebookSubmit: boolean;
  rebookDialogForm: FormGroup;
  callEscalationsubscritption: any;
  rebookReasonList: any;
  rebookalertMesssage: any;
  alternativContactList: any = [];
  contactAlternativeNumberList: any = [];
  callInitiationContacts: any = [];
  isWarningNotificationDialog: boolean = false;
  warningNotificationMessage: any = '';
  formConfigs = formConfigs;
  mobileNumberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  openCloseMonitoringForm: FormGroup;
  countryCodes = countryCodes;
  @ViewChildren('contactRows') contactRows: QueryList<ElementRef>;
  addNewContactForm: FormGroup;
  openAddContactDialog: boolean = false;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  customerId: string;
  callType: any;
  callCreationTemplate: boolean;
  callCreationTemplateForm: FormGroup;
  callCreationTemplateList: FormArray;
  paymentDropdown: any = [];
  timeDropdown: any = [
    { id: 'AM', displayName: 'AM' },
    { id: 'PM', displayName: 'PM' }
  ];
  actualTimeDialog: boolean = false;
  actualTimingForm: FormGroup;
  actualTimingList: FormArray;
  prCallDialog: boolean;
  prCallForm: FormGroup;
  requestType: RequestType;
  priorityList: any = [];
  escalationPopup: boolean = false;
  autoCallCreationPopup: boolean = false;
  customerAddressId: any = '';
  listOfFiles: any[] = [];
  fileList: File[] = [];
  maxFilesUpload: Number = 5;
  selfHelpSystemTypeConfigId: any;
  warrantyRecallReferenceId: any;
  actualTimingDateList = [];
  alertDialog: boolean = false;
  alertMsg: string = ''
  protected ngUnsubscribe: Subject<void> = new Subject<void>();
  cepFormatadoValue: string;
  debtorList: any[];
  isManualChange: boolean = false;
  blockedPanel: boolean = false;
  loopIssueRender: boolean;
  isAutoSaveEnable: boolean = true;
  isNavigationFirstTime: boolean = true;
  isCallNotesDialog: boolean = false;
  callNotesType: string;
  notesForm: FormGroup;
  showTroubleShootBtn: boolean = false;
  callCreationTempDetails: any;
  isResubmitDialog: boolean;
  resubmitDetails: any = {};
  reSubmitForm: FormGroup;
  isSubmitDialog: boolean;
  agentExtensionNo: any;
  multipleSubscription: any;
  callCreationCreationSubscription: any;
  technAssistDialog: any;
  recurringAppointementDialog: any;
  autoSaveAlertDialog: any;
  canDeactivateDialog: any;
  serviceCallDialog: any;
  inwarrantyEscalationDialog: any;
  systemTypeSubscription: any;
  faultDescriptionSubscription: any;
  serviceSubTypesSubscription: any;
  jobTypeSubscription: any;
  contactListSubscription: any;
  jobCardUrl: any;
  IsjobCardUrl = false;
  isUpselling: boolean;
  isInstallPreCheck: boolean;
  actionPermissionObj: any;

  constructor(private rxjsService: RxjsService, private httpCancelService: HttpCancelService, private dialog: MatDialog, private momentService: MomentService, private dialogService: DialogService,
    private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private crudService: CrudService, private datePipe: DatePipe, private dateAdapter: DateAdapter<Date>, private otherService: OtherService,
    private sanitizer: DomSanitizer
  ) {
    this.dateAdapter.setLocale('en-GB'); //dd/MM/yyyy
    this.initiationId = this.activatedRoute?.snapshot?.queryParams?.initiationId ? this.activatedRoute?.snapshot?.queryParams?.initiationId : '';
    this.customerId = this.activatedRoute?.snapshot?.queryParams?.customerId ? this.activatedRoute?.snapshot?.queryParams?.customerId : '';
    this.callType = this.activatedRoute?.snapshot?.queryParams?.callType;
    this.escalationPopup = this.activatedRoute?.snapshot?.queryParams ? this.activatedRoute?.snapshot?.queryParams?.escalationPopup == 'true' ? true : false : false;
    this.autoCallCreationPopup = this.activatedRoute?.snapshot?.queryParams ? this.activatedRoute?.snapshot?.queryParams?.callCreationPopup == 'true' ? true : false : false;
    this.customerAddressId = this.activatedRoute?.snapshot?.queryParams ? this.activatedRoute?.snapshot?.queryParams?.customerAddressId : null;
    this.warrantyRecallReferenceId = this.activatedRoute?.snapshot?.queryParams ? this.activatedRoute?.snapshot?.queryParams?.warrantyRecallReferenceId : null;
    this.isUpselling = this.activatedRoute?.snapshot?.queryParams ? this.activatedRoute?.snapshot?.queryParams?.isUpselling == 'true' ? true : false : false;
    this.isInstallPreCheck = this.activatedRoute?.snapshot?.queryParams ? this.activatedRoute?.snapshot?.queryParams?.isInstallPreCheck == 'true' ? true : false : false;
    let troubleShootObject = {
      customerId: this.customerId,
      initiationId: this.initiationId,
      customerAddressId: this.customerAddressId,
      callType: this.callType
    }
    this.rxjsService.setTroubleShootAlarm(troubleShootObject)
    this.rxjsService.setCallInitiationData(this.activatedRoute?.snapshot?.queryParams);
    /**
     * callType 1 - Installation call
     * callType 2 - Service call
     * callType 3 - Special call
    */
    this.title = this.callType == '1' ? 'Installation' : this.callType == '2' ? 'Service' :
      this.callType == '3' ? 'Special Projects' : '---';

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.store.pipe(select(agentLoginDataSelector)).subscribe((extension: string) => {
      this.agentExtensionNo = extension;
    });
  }

  // INSTALLATION_GET_METHOD
  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createInstallationcallCreationFrom();
    this.createCallCreationTemplateForm()
    this.createActualTimingForm()
    this.createAddNewContactForm()
    this.createOnHoldCreationFrom();
    this.createVoidInfoCreationFrom();
    this.initRebookDialogForm();
    this.createPrCallForm();
    this.createNotesForm();
    this.createReSubmitDialog();
    this.openCallCreationDialog(false); // for call creation template validation check
    if (this.autoCallCreationPopup) {
      this.openCallCreationDialog(true);
    }
    this.getAllDropdown();
    this.onCallInitiationValueChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][CUSTOMER_COMPONENT.CUSTOMER]?.find(el => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD);
      if (permission) {
        let techPermission = convertTreeToList([permission?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.TECHNICAL)]);
        const objName = this.callType == '1' ? CUSTOMER_COMPONENT.INSTALLATION : this.callType == '2' ? CUSTOMER_COMPONENT.SERVICE :
          this.callType == '3' ? CUSTOMER_COMPONENT.SPECIAL_PROEJCT : '---';
        this.actionPermissionObj = techPermission?.find(el => el?.[objName])?.[objName];
      }
    });
  }

  getQuickActionPermission(actionTypeMenuName) {
    return this.actionPermissionObj?.find(el => el?.menuName == PermissionTypes.QUICK_ACTION)?.subMenu?.find(el => el?.menuName == actionTypeMenuName);
  }

  findActionPermission(value: string) {
    return this.actionPermissionObj?.find(el => el?.menuName == value);
  }

  getAppointmentActionPermission(actionTypeMenuName) {
    return this.actionPermissionObj?.find(el => el?.menuName == CUSTOMER_COMPONENT.APPOINTMENT_BOOKING)?.subMenu?.find(el => el?.menuName == actionTypeMenuName);
  }

  getActualTimingActionPermission(actionTypeMenuName) {
    return this.actionPermissionObj?.find(el => el?.menuName == CUSTOMER_COMPONENT.ACTUAL_TIMING)?.subMenu?.find(el => el?.menuName == actionTypeMenuName);
  }

  ngAfterViewInit(): void {
    this.loopIssueRender = true
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    // setTimeout(() => {
    //   this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue('1234');
    //   // this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').markAsTouched()
    //   // (<any>this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime')).nativeElement.focus();
    //   // this.estimatedTimeInput.nativeElement.focus()
    // }, 3000);
  }

  /**
   * to load all the dropdown values
   */
  getAllDropdown() {
    let dropdownsAndData = [
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
        CustomerModuleApiSuffixModels.UX_CUSTOMER_DEBTORS, undefined,
        false, prepareRequiredHttpParams({
          customerId: this.customerId,
          addressId: this.customerAddressId
        })).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_PANEL_TYPE_CONFIG)
        .pipe(map(result => result), catchError(error => of(error))),
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_DIAGNOSIS)
        .pipe(map(result => result), catchError(error => of(error))),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.UX_PRIORITY)
        .pipe(map(result => result), catchError(error => of(error))),
    ]
    if (this.callType == '1' || this.isUpselling) { // initiation call
      dropdownsAndData.push(
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE,
          undefined,
          false, prepareRequiredHttpParams(
            {
              ShowDescriptionOnly: true
            })).pipe(map(result => result), catchError(error => of(error))),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE_SUB_CATEGORIES)
          .pipe(map(result => result), catchError(error => of(error))),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          TechnicalMgntModuleApiSuffixModels.JOBTYPE),
      )
    } else if ((this.callType == '2' || this.callType == '3') && !this.isUpselling) { // service call
      dropdownsAndData.push(
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE,
          undefined,
          false, prepareRequiredHttpParams(
            {
              ShowDescriptionOnly: true,
              CallInitiationId: this.initiationId
            })).pipe(map(result => result), catchError(error => of(error))),
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          InventoryModuleApiSuffixModels.UX_SPECIAL_PROJECT,
          undefined,
          false, prepareRequiredHttpParams({
            IsOpen: true
          })).pipe(map(result => result), catchError(error => of(error))),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_QUICK_CALL)
          .pipe(map(result => result), catchError(error => of(error))),
      )
    }
    dropdownsAndData.push(this.getContactDetailsAPI(), this.getInitiationCallDetailsById());
    this.loadActionTypes(dropdownsAndData);
  }

  guardaValorCEPFormatado(evento) {
    this.cepFormatadoValue = evento;
    this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(evento)
  }

  /**
   * to bidnd details
   */
  getCallInitiationDetailsById() {
    if (this.initiationId || this.customerId) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.onLoadContactDetails();
      this.getInitiationCallDetailsById().subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.onPatchValue(response);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      },
        error => {

        });
    }
  }

  onPatchValue(response) {
    let stockOrderModel = new InstallationCallinitiationAddEditModel(response.resources);
    this.initiationDetailsData = response.resources;
    this.isUpselling = response.resources?.basicInfo?.isUpselling;
    this.initiationId = this.initiationDetailsData?.basicInfo?.callInitiationId ? this.initiationDetailsData?.basicInfo?.callInitiationId : this.initiationId
    if (this.escalationPopup && this.initiationId) {
      this.openCallEscalationDialog()
    }
    this.callInitiationContacts = response?.resources?.callInitiationContacts ? response?.resources?.callInitiationContacts : [];
    this.initiationBasicInfo = Object.assign({}, response?.resources?.basicInfo);
    this.callCreationTemplate = this.isInstallPreCheck ? this.isInstallPreCheck : this.initiationBasicInfo?.isInstallPreCheck;
    this.initiationLightingCover = response?.resources?.lightingCover;
    this.initiationServiceInfo = response?.resources?.serviceInfo;
    this.initiationAppointmentInfo = response?.resources?.appointmentInfo;
    let basicInfo = Object.assign({}, response?.resources?.basicInfo);
    if (basicInfo.installationPartitions) {
      basicInfo.installationPartitions = basicInfo?.installationPartitions?.filter(x => x.mainPartition != "True");
    }
    this.onPatchBasicInfo();
    this.onPatchServiceInfo();
    this.onPatchNkaCustomer();
    this.onPatchAppointmentInfo();
    this.initiatonCallCreationForm.markAsUntouched();
  }

  onPatchBasicInfo() {
    if (this.initiationBasicInfo?.installationPartitions) {
      this.initiatonCallCreationForm.controls['ismainPartation'].patchValue(this.initiationBasicInfo?.installationPartitions?.find(x => x.mainPartition == "True")?.callInitiationPartitionId == null ? false : true);
      this.initiatonCallCreationForm.controls['mainPartationCustomerId'].patchValue(this.initiationBasicInfo?.installationPartitions?.find(x => x.mainPartition == "True")?.partitionCode + ' - ' + this.initiationBasicInfo?.installationPartitions?.find(x => x.mainPartition == "True")?.partitionName);
      this.installationPartitionsFormArray.clear()
      for (let i = 0; i < this.initiationBasicInfo?.installationPartitions?.length; i++) {
        if (this.initiationBasicInfo?.installationPartitions[i]?.mainPartition == 'False') {
          this.initiationBasicInfo.installationPartitions[i].mainPartition = this.initiationBasicInfo?.installationPartitions[i]?.callInitiationPartitionId == null ? false : true
          let addServiceSubTypeObj = this.formBuilder.group(this.initiationBasicInfo?.installationPartitions[i])
          this.initiationBasicInfo.installationPartitions[i].callInitiationPartitionId == null ? addServiceSubTypeObj.enable() : addServiceSubTypeObj.enable()
          this.installationPartitionsFormArray.push(addServiceSubTypeObj);
        }
      }
    }
    if (this.initiationBasicInfo) {
      this.initiatonCallCreationForm.patchValue({
        "callInitiationId": this.initiationBasicInfo?.callInitiationId,
        "customerId": this.initiationBasicInfo?.customerId,
        "quotationNumber": this.initiationBasicInfo?.quotationNumber,
        "isNew": this.initiationBasicInfo?.isNew,
        "isScheduled": this.initiationBasicInfo?.isScheduled,
        "isTempDone": this.initiationBasicInfo?.isTempDone,
        "isInvoiced": this.initiationBasicInfo?.isInvoiced,
        "isOnHold": this.initiationBasicInfo?.isOnHold,
        "isVoid": this.initiationBasicInfo?.isVoid,
        "isReferToSales": this.initiationBasicInfo?.isReferToSales,
        "isInstallPreCheck": this.isInstallPreCheck ? this.isInstallPreCheck : this.initiationBasicInfo?.isInstallPreCheck,
        "isInWarrenty": this.initiationBasicInfo?.isInWarrenty,
        "isRecall": this.initiationBasicInfo?.isRecall,
        "isCode50": this.initiationBasicInfo?.isCode50,
        "isPRCall": this.initiationBasicInfo?.isPRCall,
        "isSaveOffer": this.initiationBasicInfo?.isSaveOffer,
        "isCon": this.initiationBasicInfo?.isCon,
        "isAddressVerified": this.initiationBasicInfo?.isAddressVerified ? true : null,
        "priorityid": this.initiationBasicInfo?.priorityid == 0 ? 100 : this.initiationBasicInfo?.priorityid,
        "panelTypeConfigId": this.initiationBasicInfo?.panelTypeConfigId ? { value: this.initiationBasicInfo?.panelTypeConfigId } : '',
        "specialProjectId": this.initiationBasicInfo?.specialProjectId,
        "projectEndDate": this.initiationBasicInfo?.projectEndDate,
        "addressId": this.initiationBasicInfo?.addressId,
        "partitionIds": [],
        "techInstruction": this.initiationBasicInfo?.techInstruction,
        "notes": this.initiationBasicInfo?.notes,
        "debtorAdditionalInfoId": this.initiationBasicInfo?.debtorAdditionalInfoId,
        "debtorId": this.initiationBasicInfo?.debtorId
      }, { emitEvent: false });
      const travelTime = this.initiationBasicInfo?.travelTime ? new Date(new Date().toDateString()+' '+ this.initiationBasicInfo?.travelTime) : null;
      this.initiatonCallCreationForm.get('appointmentInfo.travelTime').setValue(travelTime);
      if (this.initiationBasicInfo?.debtorId && this.initiationBasicInfo?.debtorAdditionalInfoId) {
        let debtorId = this.debtorList?.find(el => (el?.debtorId == this.initiationBasicInfo?.debtorId
          && el?.debtorAdditionalInfoId == this.initiationBasicInfo?.debtorAdditionalInfoId));
        if (!debtorId) {
          debtorId = this.debtorList?.find(el => el?.debtorId == this.initiationBasicInfo?.debtorId)
        }
        this.initiatonCallCreationForm?.get('debtor')?.setValue(debtorId, { emitEvent: false });
      } else if (this.initiationBasicInfo?.debtorId && !this.initiationBasicInfo?.debtorAdditionalInfoId) {
        const debtorId = this.debtorList?.find(el => el?.debtorId == this.initiationBasicInfo?.debtorId);
        this.initiatonCallCreationForm?.get('debtor')?.setValue(debtorId, { emitEvent: false })
      }
      if (this.initiationBasicInfo?.isInstallPreCheck || this.isInstallPreCheck) {
        this.initiatonCallCreationForm = clearFormControlValidators(this.initiatonCallCreationForm, ["debtor", "debtorId", "isAddressVerified", "contactId", "contactNumber"]);
        this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').clearValidators();
        this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').clearValidators();
        this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').clearValidators();
        this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').clearValidators();
        this.callCreationTemplate = true
      } else {
        this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["debtor", "debtorId", "isAddressVerified", "contactId", "contactNumber"]);
        this.initiatonCallCreationForm.get('debtorId').updateValueAndValidity();
        this.initiatonCallCreationForm.get('isAddressVerified').updateValueAndValidity();
        this.initiatonCallCreationForm.get('contactId').updateValueAndValidity({ emitEvent: false });
        this.initiatonCallCreationForm.get('contactNumber').updateValueAndValidity({ emitEvent: false });
        if (this.callType == '1' || this.isUpselling) {
          this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').setValidators([Validators.required]);
          this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').setValidators([Validators.required]);
          this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').clearValidators();
          this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').clearValidators();
          this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').disable({ emitEvent: false });
          this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').disable({ emitEvent: false });
          if (this.isUpselling) {
            this.onLoadDropdownByCondition();
          }
        } else if ((this.callType == '2' || this.callType == '3') && !this.isUpselling) {
          this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').setValidators([Validators.required]);
          this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').setValidators([Validators.required]);
          this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').setValidators([Validators.required]);
          this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').setValidators([Validators.required]);
        }
        this.callCreationTemplate = false;
      }
      this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').updateValueAndValidity({ emitEvent: false });
      this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').updateValueAndValidity({ emitEvent: false });
      this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').updateValueAndValidity({ emitEvent: false });
      this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').updateValueAndValidity({ emitEvent: false });
    }
  }

  onPatchAppointmentInfo() {
    if (this.initiationDetailsData?.assignedDiagnosis) {
      this.initiationDetailsData?.assignedDiagnosis?.forEach(element => {
        element.id = element.diagnosisId
      });
      this.initiatonCallCreationForm.get('appointmentInfo.diagnosisId').setValue(this.initiationDetailsData?.assignedDiagnosis);
    }
    if (this.initiationAppointmentInfo) {
      this.initiatonCallCreationForm.get('appointmentInfo.wireman').setValue(this.initiationAppointmentInfo.wireMan);
      this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').setValue(this.initiationAppointmentInfo?.scheduledStartDate ? new Date(this.initiationAppointmentInfo?.scheduledStartDate) : null);
      this.initiatonCallCreationForm.get('appointmentInfo.scheduledEndDate').setValue(this.initiationAppointmentInfo?.scheduledEndDate ? new Date(this.initiationAppointmentInfo?.scheduledEndDate) : null);
      this.initiatonCallCreationForm.get('appointmentInfo.appointmentId').setValue(this.initiationAppointmentInfo?.appointmentId ? this.initiationAppointmentInfo?.appointmentId : null);
      this.initiatonCallCreationForm.get('appointmentInfo.appointmentLogId').setValue(this.initiationAppointmentInfo?.appointmentLogId ? this.initiationAppointmentInfo?.appointmentLogId : null);
      if (this.initiationAppointmentInfo?.estimatedTime) {
        let estimatedTime = this.initiationAppointmentInfo?.estimatedTime?.split(':')
        let estimatTimeIs = estimatedTime[0] + ':' + estimatedTime[1]
        this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(estimatTimeIs);
        this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(estimatTimeIs);
        this.initiationDetailsData.appointmentInfo.estimatedTime = estimatTimeIs
        this.initiationDetailsData.appointmentInfo.estimatedTimeStatic = estimatTimeIs
      } else {
        this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(null);
        this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(null);
      }
      this.initiatonCallCreationForm.get('appointmentInfo.technician').setValue(this.initiationAppointmentInfo?.technician ? this.initiationAppointmentInfo?.technician : "");
      this.initiatonCallCreationForm.get('appointmentInfo.actualTime').setValue(this.initiationAppointmentInfo?.actualTime ? this.initiationAppointmentInfo?.actualTime : null);
      this.initiatonCallCreationForm.get('appointmentInfo.preferredDate').setValue(this.initiationAppointmentInfo?.preferredDate ? new Date(this.initiationAppointmentInfo?.preferredDate) : null);
      this.initiatonCallCreationForm.get('appointmentInfo.preferredTime').setValue(this.initiationAppointmentInfo?.preferredTime ? this.initiationAppointmentInfo?.preferredTime : null);
      this.initiatonCallCreationForm.get('appointmentInfo.isAppointmentChangable').setValue(this.initiationAppointmentInfo?.isAppointmentChangable ? this.initiationAppointmentInfo?.isAppointmentChangable : false);
      this.initiatonCallCreationForm.get('appointmentInfo.techArea').setValue(this.initiationAppointmentInfo?.techArea ? this.initiationAppointmentInfo?.techArea : null);
      this.initiatonCallCreationForm.get('appointmentInfo.isNextAvailableAppointment').setValue(false);
    }
    this.getContactListDropdown()
  }

  onPatchServiceInfo() {
    this.getEstimatedTime(this.initiationServiceInfo?.jobTypeId);
    this.initiatonCallCreationForm.controls['serviceInfo'].patchValue({
      'callInitiationServiceInfoId': this.initiationServiceInfo?.callInitiationServiceInfoId,
      'systemTypeId': this.initiationServiceInfo?.systemTypeId ? this.initiationServiceInfo?.systemTypeId : '',
      'systemTypeSubCategoryId': this.initiationServiceInfo?.systemTypeSubCategoryId ? this.initiationServiceInfo?.systemTypeSubCategoryId : '',
      'serviceSubTypeId': this.initiationServiceInfo?.serviceSubTypeId ? this.initiationServiceInfo?.serviceSubTypeId : '',
      'faultDescriptionId': this.initiationServiceInfo?.faultDescriptionId ? this.initiationServiceInfo?.faultDescriptionId : '',
      'jobTypeId': this.initiationServiceInfo?.jobTypeId ? this.initiationServiceInfo?.jobTypeId : '',
      'description': this.initiationServiceInfo?.description,
      wireman: this.initiationServiceInfo?.wireman,
    }, { emitEvent: false });
    // for loading dropdown values for manually calling valuechanges in service info
  }

  onPatchNkaCustomer() {
    this.initiatonCallCreationForm.get('nkaServiceCall').patchValue({
      isPOAccepted: this.initiationDetailsData?.nkaServiceCall?.isPOAccepted == true,
      isPONotAccepted: this.initiationDetailsData?.nkaServiceCall?.isPOAccepted == false,
      poNumber: this.initiationDetailsData?.nkaServiceCall ? this.initiationDetailsData?.nkaServiceCall?.poNumber : '',
      poAttachments: this.initiationDetailsData?.nkaServiceCall ? this.initiationDetailsData?.nkaServiceCall?.docName : '',
      // path: this.initiationDetailsData?.nkaServiceCall ? this.initiationDetailsData?.nkaServiceCall?.path : undefined,
      attachmentUrl: this.initiationDetailsData?.nkaServiceCall?.path ? this.initiationDetailsData?.nkaServiceCall?.path : '',
      poAmount: this.initiationDetailsData?.nkaServiceCall?.poAmount ? this.otherService.transformDecimal(this.initiationDetailsData?.nkaServiceCall?.poAmount) : '',
      poComment: this.initiationDetailsData?.nkaServiceCall ? this.initiationDetailsData?.nkaServiceCall?.poComment : '',
    }, { emitEvent: false })
    this.nkaValueChanges();
    if (this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po required' && this.initiationDetailsData?.basicInfo?.isNKACustomer && this.userData?.userId == this.initiationDetailsData?.nkaServiceCall?.ownedUserId) {
      this.initiatonCallCreationForm.get('nkaServiceCall.path').enable();
      this.initiatonCallCreationForm.get('nkaServiceCall.poComment').enable();
    } else {
      this.initiatonCallCreationForm.get('nkaServiceCall.poNumber').disable();
      this.initiatonCallCreationForm.get('nkaServiceCall.poAmount').disable();
      this.initiatonCallCreationForm.get('nkaServiceCall.path').disable();
      this.initiatonCallCreationForm.get('nkaServiceCall.poComment').disable();
    }
    if (this.isPOInfoDisabled()) {
      this.initiatonCallCreationForm.get('nkaServiceCall.poNumber').enable();
      this.initiatonCallCreationForm.get('nkaServiceCall.poAmount').enable();
    }
    this.onCustomerServiceType();
  }

  nkaValueChanges() {
    this.initiatonCallCreationForm.get('nkaServiceCall.isPOAccepted').valueChanges
      .subscribe(value => {
        if (value == true) {
          this.initiatonCallCreationForm.get('nkaServiceCall.isPONotAccepted').setValue(null, { emitEvent: false });
          this.initiatonCallCreationForm.get('nkaServiceCall.isPOAccepted').setValue(true, { emitEvent: false });
          if (!this.initiatonCallCreationForm.get('nkaServiceCall.poNumber').value || !this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments').value ||
            !this.initiatonCallCreationForm.get('nkaServiceCall.poAmount').value) {
            this.onPORequired();
          }
          this.onDisablePOComments();
        } else if (value == false) {
          this.initiatonCallCreationForm.get('nkaServiceCall.isPOAccepted').setValue(false, { emitEvent: false });
        }
      });
    this.initiatonCallCreationForm.get('nkaServiceCall.isPONotAccepted').valueChanges
      .subscribe(value => {
        if (value == true) {
          this.initiatonCallCreationForm.get('nkaServiceCall.isPOAccepted').setValue(null, { emitEvent: false });
          this.initiatonCallCreationForm.get('nkaServiceCall.isPONotAccepted').setValue(true, { emitEvent: false });
          this.onEnablePOComments();
        } else if (value == false) {
          this.initiatonCallCreationForm.get('nkaServiceCall.isPONotAccepted').setValue(false, { emitEvent: false });
          this.onDisablePOComments();
        }
      });
  }

  /**
   * validation for NKA customer
   * @returns
   */
  getNKACustomer() {
    return (this.initiationDetailsData?.nkaServiceCall?.isPOAccepted == false || this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po rejected' || this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po declined') && this.initiationDetailsData?.basicInfo?.isVoid == false && this.initiationDetailsData?.basicInfo?.isInvoiced == false;
  }
  /**
   * validation for Execguard customer
   * @returns
   */
  getExecGuardCustomer() {
    return this.initiationDetailsData?.execuGuardServiceCall?.execuGuardServiceCallStatusName?.toLowerCase() == 'declined' && this.initiationDetailsData?.basicInfo?.isExecuGuardCustomer && this.initiationDetailsData?.basicInfo?.isVoid == false && this.initiationDetailsData?.basicInfo?.isInvoiced == false;
  }

  onCustomerServiceType() {
    if (this.getExecGuardCustomer()) {
      this.onOpenResubmit();
    } else if (this.getNKACustomer()) {
      this.onOpenResubmit();
      this.reSubmitForm.get('comment').setValidators([Validators.required, Validators.minLength(3), Validators.maxLength(5000)]);
      this.reSubmitForm.get('comment').updateValueAndValidity();
    }
  }
  /*----- Resubmit dialog starts ------*/
  createReSubmitDialog() {
    this.reSubmitForm = new FormGroup({
      comment: new FormControl('', [Validators.required]),
      createdUserId: new FormControl('', [Validators.required]),
    })
  }

  onOpenResubmit(val = true) {
    if (val) {
      this.resubmitDetails['header'] = this.getExecGuardCustomer() ? "Execuguard Request Resubmit" : "NKA Request";
      this.resubmitDetails['msg'] = this.getExecGuardCustomer() ? "Execuguard request has been declined. Please resubmit." : "Po is not accepted. Are you re-submitting the request?";
      this.resubmitDetails['api'] = this.getExecGuardCustomer() ? TechnicalMgntModuleApiSuffixModels.EXEC_SERVICE_CALL_DECLINE : TechnicalMgntModuleApiSuffixModels.NKA_SERVICE_CALL_RESUBMIT;
      this.resubmitDetails['id'] = this.getExecGuardCustomer() ? { execuGuardServiceCallId: this.initiationDetailsData?.execuGuardServiceCall?.execuGuardServiceCallId } : { NKAServiceCallId: this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallId };
      this.reSubmitForm.get('comment').setValue('');
      this.reSubmitForm.get('createdUserId').setValue(this.userData?.userId);
    }
    this.isSubmitDialog = false;
    this.isResubmitDialog = val;
    this.rxjsService.setDialogOpenProperty(val);
  }

  onResubmit() {
    this.isSubmitDialog = true;
    if (this.initiationDetailsData?.execuGuardServiceCall?.execuGuardServiceCallStatusName?.toLowerCase() == 'declined' || 
      this.initiationDetailsData?.nkaServiceCall?.isPOAccepted == false || this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po declined') {
      if (this.reSubmitForm.invalid) {
        this.reSubmitForm.markAllAsTouched();
        return;
      }
      this.rxjsService.setGlobalLoaderProperty(true);
      const reqObj = {
        ...this.resubmitDetails['id'],
        ...this.reSubmitForm.getRawValue(),
      }
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, this.resubmitDetails['api'], reqObj)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getCallInitiationDetailsById();
            this.onOpenResubmit(false);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }
  /*----- Resubmit Dialog ends --------*/

  onEnablePOComments() {
    if (this.isPOAcceptDisabled()) {
      this.initiatonCallCreationForm.get('nkaServiceCall.poComment').enable();
      this.initiatonCallCreationForm.get('nkaServiceCall.poComment').markAsUntouched();
    }
  }

  onDisablePOComments() {
    if (this.isPOAcceptDisabled()) {
      this.initiatonCallCreationForm.get('nkaServiceCall.poComment').disable();
      this.initiatonCallCreationForm.get('nkaServiceCall.poComment').markAsUntouched();
    }
  }

  showPOCommentInvalid() {
    return !this.initiatonCallCreationForm.get('nkaServiceCall.poComment')?.value && this.initiatonCallCreationForm.get('nkaServiceCall.isPONotAccepted')?.value && this.initiatonCallCreationForm.get('nkaServiceCall.poComment')?.touched;
  }

  onCallInitiationValueChanges() {
    this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').valueChanges.subscribe((val) => {
      if (this.callType == '1' || this.isUpselling) {
        return
      }
      if (val) {
        this.getSystemCategory(val);
      }
      else {
        this.SystemTypeSubCategoryList = [];
        this.ServiceSubTypeList = [];
        this.jobTypeList = [];
      }
    })
    this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').valueChanges.subscribe((val) => {
      if (this.callType == '1' || this.isUpselling) {
        return
      }
      if (val) {
        this.getServiceSubTypes(val);
      }
      else {
        this.ServiceSubTypeList = [];
        this.jobTypeList = [];
      }
    })
    this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').valueChanges.subscribe((val) => {
      if (this.callType == '1' || this.isUpselling) {
        return
      }
      this.serviceSubTypeId = val;
      if (val) {
        this.getFaultDescription(val);
      }
      else {
        this.jobTypeList = [];
      }
    })
    this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').valueChanges.subscribe((val) => {
      if (this.callType == '1' || this.isUpselling) {
        return
      }
      if (val) {
        this.isNavigationFirstTime = true;
        this.getSelfHelpConfigId();
        this.rxjsService.setFormChangeDetectionProperty(true);
        this.getJobId(val);
      }
      else {
        this.jobTypeList = [];
        this.getNextavailableAppointment();
      }
    })
    this.initiatonCallCreationForm.get('serviceInfo.jobTypeId').valueChanges.subscribe(val => {
      if (val) {
        this.getEstimatedTime(val);
      }
    })
    this.initiatonCallCreationForm.get('contactId').valueChanges.subscribe(val => {
      if (!val) {
        this.initiatonCallCreationForm.get('contactNumber').setValue('');
        return
      }
      this.onAfterContactChanges(val);
    })
    this.initiatonCallCreationForm.get('alternateContactId').valueChanges.subscribe(val => {
      if (!val) {
        this.initiatonCallCreationForm.get('alternateContactNumber').setValue('');
        this.initiatonCallCreationForm = clearFormControlValidators(this.initiatonCallCreationForm, ["alternateContactNumber"]);
        return
      }
      this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["alternateContactNumber"]);
      this.initiatonCallCreationForm.get('alternateContactNumber').updateValueAndValidity()
      this.onAfterAlterContactChanges(val);
    })
    this.initiatonCallCreationForm.get('contactNumber').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      let primaryData = this.callInitiationContacts.find(x => x.isPrimary == true)
      if (primaryData) {
        this.callInitiationContacts.find(x => x.isPrimary == true).contactNo = val.substr(val.indexOf(' ') + 1)
        this.callInitiationContacts.find(x => x.isPrimary == true).contactNoCountryCode = val.substr(0, val.indexOf(' '))
        this.callInitiationContacts.find(x => x.isPrimary == true).createdUserId = this.userData.userId
      }
    })
    this.initiatonCallCreationForm.get('alternateContactNumber').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      let primaryData = this.callInitiationContacts.find(x => x.isPrimary == false)
      if (primaryData) {
        this.callInitiationContacts.find(x => x.isPrimary == false).contactNo = val.substr(val.indexOf(' ') + 1)
        this.callInitiationContacts.find(x => x.isPrimary == false).contactNoCountryCode = val.substr(0, val.indexOf(' '))
      }
    })
    /**
     * depending on estimatedTime. we have to bind nextAppointmentDate
     */
    this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').valueChanges
      .pipe(
        // tap(val=>val.length == 5),
        filter(val => val ? val.length == 5 : null),
        takeUntil(this.ngUnsubscribe),
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          if (!val) {
            return
          }
          return of(this.getNextavailableAppointment());
        })
      )
      .subscribe();
    this.initiatonCallCreationForm.get('specialProjectId').valueChanges.subscribe(val => {
      if (val) {
        let projectData = this.projectList.find(x => x.id.toLowerCase() == val.toLowerCase());
        if (!projectData) return;
        this.initiationBasicInfo.projectEndDate = projectData['projectEndDate'];
      }
    })
    /**
     * if InstallPrcheck is true, we need to remove all mantadory validation.
     */
    this.initiatonCallCreationForm.get('isInstallPreCheck').valueChanges.subscribe(val => {
      if (val) {
        this.initiatonCallCreationForm = clearFormControlValidators(this.initiatonCallCreationForm, ["debtor", "debtorId", "isAddressVerified", "contactId", "contactNumber"]);
        this.initiatonCallCreationForm.get('isAddressVerified').setValue(this.initiatonCallCreationForm.get('isAddressVerified').value ? true : null, { emitEvent: false })
        this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').clearValidators();
        this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').clearValidators();
        this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').clearValidators();
        this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').clearValidators();

        this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').updateValueAndValidity({ emitEvent: false })
        this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').updateValueAndValidity({ emitEvent: false })
        this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').updateValueAndValidity({ emitEvent: false })
        this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').updateValueAndValidity({ emitEvent: false })
        this.callCreationTemplate = true
        return
      }
      this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["debtor", "debtorId", "isAddressVerified", "contactId", "contactNumber"]);
      this.initiatonCallCreationForm.get('isAddressVerified').setValue(null, { emitEvent: false })
      this.initiatonCallCreationForm.get('debtor').updateValueAndValidity()
      this.initiatonCallCreationForm.get('debtorId').updateValueAndValidity()
      this.initiatonCallCreationForm.get('isAddressVerified').updateValueAndValidity()
      this.initiatonCallCreationForm.get('contactId').updateValueAndValidity({ emitEvent: false });
      this.initiatonCallCreationForm.get('contactNumber').updateValueAndValidity({ emitEvent: false });
      if (this.callType == '1') {
        this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').setValidators([Validators.required])
        this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').setValidators([Validators.required])
      } else if (this.callType == '2' || this.callType == '3') {
        this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').setValidators([Validators.required])
        this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').setValidators([Validators.required])
        this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').setValidators([Validators.required])
        this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').setValidators([Validators.required])
      }
      this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').updateValueAndValidity({ emitEvent: false })
      this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').updateValueAndValidity({ emitEvent: false })
      this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').updateValueAndValidity({ emitEvent: false })
      this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').updateValueAndValidity({ emitEvent: false })
      this.callCreationTemplate = false
    })
  }

  onAfterContactChanges(val) {
    let contact = this.contactList.find(x => x.keyHolderId == val);
    if (contact) {
      this.contactNumberList = []
      if (contact.mobile1 && !contact.isNewContact) {
        this.contactNumberList.push({ id: contact.mobile1, value: contact.mobile1 })
      }
      if (contact.mobile2 && !contact.isNewContact) {
        this.contactNumberList.push({ id: contact.mobile2, value: contact.mobile2 })
      }
      if (contact.officeNo && !contact.isNewContact) {
        this.contactNumberList.push({ id: contact.officeNo, value: contact.officeNo })
      }
      if (contact.premisesNo && !contact.isNewContact) {
        this.contactNumberList.push({ id: contact.premisesNo, value: contact.premisesNo })
      }
      if (contact.isNewContact) {
        if (contact.contactNumber) { //for new contact
          this.contactNumberList.push({ id: contact.contactNumber, value: contact.contactNumber })
        }
      }
      let contactData = {
        keyholderId: contact.isNewContact ? null : contact.keyHolderId,
        contactName: contact.keyHolderName,
        contactNo: contact.contactNumber.substr(contact.contactNumber.indexOf(' ') + 1),
        contactNoCountryCode: contact.contactNumber.substr(0, contact.contactNumber.indexOf(' ')),
        isPrimary: true,
        callInitiationId: this.initiationId,
        callInitiationContactId: null,
        createdUserId: this.userData.userId
      }
      let primaryData = this.callInitiationContacts.find(x => x.isPrimary == true)
      if (primaryData) {
        this.callInitiationContacts.find(x => x.isPrimary == true).keyholderId = contact.isNewContact ? null : contact.keyHolderId
        this.callInitiationContacts.find(x => x.isPrimary == true).contactName = contact.keyHolderName
        this.callInitiationContacts.find(x => x.isPrimary == true).contactNo = contact.contactNumber.substr(contact.contactNumber.indexOf(' ') + 1)
        this.callInitiationContacts.find(x => x.isPrimary == true).contactNoCountryCode = contact.contactNumber.substr(0, contact.contactNumber.indexOf(' '))
        // this.callInitiationContacts.find(x => x.isPrimary == true).callInitiationContactId = contact.isNewContact ? contact.keyHolderId : primaryData.callInitiationContactId ? primaryData.callInitiationContactId : null
        // this.callInitiationContacts.find(x => x.isPrimary == true).callInitiationContactId = contact.isNewContact ? null : contact.keyHolderId
        // this.callInitiationContacts.find(x => x.isPrimary == true).callInitiationContactId = contact.isNewContact ? (primaryData.callInitiationContactId ? primaryData.callInitiationContactId : null) : contact.keyHolderId
        this.callInitiationContacts.find(x => x.isPrimary == true).createdUserId = this.userData.userId
      } else {
        this.callInitiationContacts.push(contactData)
      }
    }
  }

  onAfterAlterContactChanges(val) {
    let contact = this.alternativContactList.find(x => x.keyHolderId == val)
    if (contact) {
      this.contactAlternativeNumberList = []
      if (contact.mobile1 && !contact.isNewContact) {
        this.contactAlternativeNumberList.push({ id: contact.mobile1, value: contact.mobile1 })
      }
      if (contact.mobile2 && !contact.isNewContact) {
        this.contactAlternativeNumberList.push({ id: contact.mobile2, value: contact.mobile2 })
      }
      if (contact.officeNo && !contact.isNewContact) {
        this.contactAlternativeNumberList.push({ id: contact.officeNo, value: contact.officeNo })
      }
      if (contact.premisesNo && !contact.isNewContact) {
        this.contactAlternativeNumberList.push({ id: contact.premisesNo, value: contact.premisesNo })
      }
      if (contact.isNewContact) {
        if (contact.contactNumber) { //for new contact
          this.contactAlternativeNumberList.push({ id: contact.contactNumber, value: contact.contactNumber })
        }
      }
      let contactData = {
        keyholderId: contact.isNewContact ? null : contact.keyHolderId,
        contactName: contact?.keyHolderName,
        contactNo: contact?.contactNumber?.substr(contact?.contactNumber?.indexOf(' ') + 1),
        contactNoCountryCode: contact?.contactNumber?.substr(0, contact?.contactNumber?.indexOf(' ')),
        isPrimary: false,
        callInitiationId: this.initiationId,
        callInitiationContactId: null,
        createdUserId: this.userData.userId
      }
      let primaryData = this.callInitiationContacts.find(x => x.isPrimary == false)
      if (primaryData) {
        this.callInitiationContacts.find(x => x.isPrimary == false).keyholderId = contact.isNewContact ? null : contact.keyHolderId
        this.callInitiationContacts.find(x => x.isPrimary == false).contactName = contact.keyHolderName
        this.callInitiationContacts.find(x => x.isPrimary == false).contactNo = contact.contactNumber.substr(contact.contactNumber.indexOf(' ') + 1)
        this.callInitiationContacts.find(x => x.isPrimary == false).contactNoCountryCode = contact.contactNumber.substr(0, contact.contactNumber.indexOf(' '))
        // this.callInitiationContacts.find(x => x.isPrimary == false).callInitiationContactId = contact.isNewContact ? contact.keyHolderId : primaryData.callInitiationContactId ? primaryData.callInitiationContactId : null
        // this.callInitiationContacts.find(x => x.isPrimary == false).callInitiationContactId = contact.isNewContact ? null : contact.keyHolderId
        // this.callInitiationContacts.find(x => x.isPrimary == false).callInitiationContactId = contact.isNewContact ? (primaryData.callInitiationContactId ? primaryData.callInitiationContactId : null) : contact.keyHolderId
        this.callInitiationContacts.find(x => x.isPrimary == false).createdUserId = this.userData.userId
      } else {
        this.callInitiationContacts.push(contactData)
      }
    }
  }

  onChangeNextAvailableCheck(event) {
    if (event.checked) {
      this.onSelectTime(this.initiatonCallCreationForm.get('appointmentInfo.nextAvailableAppointment').value)
    } else {
      this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').setValue(this.initiationAppointmentInfo ? this.initiationAppointmentInfo.scheduledStartDate ? new Date(this.initiationAppointmentInfo.scheduledStartDate) : null : null)
      this.initiatonCallCreationForm.get('appointmentInfo.scheduledEndDate').setValue(this.initiationAppointmentInfo ? this.initiationAppointmentInfo.scheduledEndDate ? new Date(this.initiationAppointmentInfo.scheduledEndDate) : null : null)
    }
    this.initiatonCallCreationForm.get('appointmentInfo.isNextAvailableAppointment').setValue(event.checked)
  }

  onChangeResetServiceDropdonw() {
    this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').setValue(null)
  }

  getSystemCategoryAPI() {
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE_SUB_CATEGORY, undefined, null, prepareGetRequestHttpParams(null, null,
        {
          SystemTypeId: this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value, ShowDescriptionOnly: true,
          CallInitiationId: this.initiationId
        }
      ))
  }

  getSystemCategory(val) {
    if (!val) {
      this.SystemTypeSubCategoryList = [];
      this.ServiceSubTypeList = [];
      this.jobTypeList = [];
    } else {
      this.systemTypeSubscription = this.getSystemCategoryAPI().subscribe((response) => {
        this.onAfterSystemCategory(response);
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  onAfterSystemCategory(response) {
    if (response.isSuccess) {
      this.SystemTypeSubCategoryList = response.resources;
    } else {
      this.SystemTypeSubCategoryList = []
    }
  }

  getFaultDescriptionAPI() {
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_SERVICE_SUB_TYPES, undefined, null, prepareGetRequestHttpParams(null, null,
        {
          ServiceSubTypeId: this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').value, ShowMapped: true,
          CallInitiationId: this.initiationId
        }
      ));
  }

  getFaultDescription(val) {
    if (!val) {
      this.jobTypeList = [];
    } else {
      this.faultDescriptionSubscription = this.getFaultDescriptionAPI().subscribe((response) => {
        this.onAfterFaultDescription(response);
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  onAfterFaultDescription(response) {
    if (response.isSuccess) {
      this.faultDescriptionList = response.resources;
    } else {
      this.faultDescriptionList = [];
    }
  }

  getServiceSubTypesAPI() {
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.UX_SERVICE_SUB_TYPES, undefined, null, prepareGetRequestHttpParams(null, null,
        {
          SystemTypeSubCategoryId: this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').value, ShowDescriptionOnly: true,
          CallInitiationId: this.initiationId
        }
      ))
  }

  getServiceSubTypes(val) {
    if (!val) {
      this.ServiceSubTypeList = [];
      this.jobTypeList = [];
    } else {
      this.serviceSubTypesSubscription = this.getServiceSubTypesAPI().subscribe((response) => {
        this.onAfterServiceSubTypes(response);
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  onAfterServiceSubTypes(response) {
    if (response.isSuccess) {
      this.ServiceSubTypeList = response.resources;
    } else {
      this.ServiceSubTypeList = []
    }
  }

  getJobIdAPI() {
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.SKILL_MATRIX_CONFIG_SKILL_MAPPED_JOB_TYPES, undefined, null
      , prepareGetRequestHttpParams(null, null,
        {
          ServiceSubTypeId: this.initiatonCallCreationForm.value.serviceInfo.serviceSubTypeId,
          FaultDescriptionId: this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').value,
          CallInitiationId: this.initiationId
        }
      )
    );
  }

  getJobId(val) {
    if (!val) {
      this.jobTypeList = [];
      this.getNextavailableAppointment()
    } else {
      if (!this.initiatonCallCreationForm?.getRawValue()?.serviceInfo?.serviceSubTypeId) {
        return
      }
      this.jobTypeSubscription = this.getJobIdAPI().subscribe((response) => {
        this.onAfterJobType(response);
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  onAfterJobType(response) {
    if (response.isSuccess) {
      this.jobTypeList = [response.resources];
      if (response.resources) {
        this.initiatonCallCreationForm.get('serviceInfo.jobTypeId').setValue(response.resources.jobTypeId ? response.resources.jobTypeId : this.initiationServiceInfo.jobTypeId)
        this.getEstimatedTime(this.initiatonCallCreationForm.get('serviceInfo.jobTypeId').value);
      }
    } else {
      this.jobTypeList = []
    }
  }

  /**
   * depending on jobTypeId, need to bind estimateTime. for Service call and Special project
   * @param val jobTypeId
   */
  getEstimatedTime(val) {
    let jobType = this.jobTypeList.find(x => x.jobTypeId == Number(val))
    if (this.callType == '2' || this.callType == '3') { // service call

      if (jobType) {
        if (this.isManualChange) {
          let estimatedSplt = jobType.estimatedTime?.split(':')
          this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(jobType ? estimatedSplt[0] + ':' + estimatedSplt[1] : null)
          this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(jobType ? estimatedSplt[0] + ':' + estimatedSplt[1] : null)
        } else {
          if (this.initiationDetailsData?.appointmentInfo?.estimatedTime) {
            this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(this.initiationDetailsData?.appointmentInfo?.estimatedTime)
          } else {
            let estimatedSplt = jobType.estimatedTime?.split(':')
            this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(jobType ? estimatedSplt[0] + ':' + estimatedSplt[1] : null)
            this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(jobType ? estimatedSplt[0] + ':' + estimatedSplt[1] : null)
          }
        }
      } else {
        this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(this.initiationDetailsData?.appointmentInfo ? this.initiationDetailsData?.appointmentInfo?.estimatedTime : null)
        this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(this.initiationDetailsData?.appointmentInfo ? this.initiationDetailsData?.appointmentInfo?.estimatedTime : null)
      }
    }
  }

  getRating() {
    if (this?.initiationDetailsData) {
      return this?.initiationDetailsData?.basicInfo?.callRating ?
        Math.round(this?.initiationDetailsData?.basicInfo?.callRating) : '';
    }
  }

  createAddNewContactForm() {
    this.addNewContactForm = this.formBuilder.group({
      contactName: ['', Validators.required],
      contactNoCountryCode: ['+27', Validators.required],
      contactNo: ['', [Validators.required, Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
      Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]],
      isPrimary: [null, Validators.required],
      callInitiationId: this.initiationId ? this.initiationId : null,
      keyholderId: null,
      callInitiationContactId: null,
      customerId: this.customerId ? this.customerId : null,
      createdUserId: this.userData.userId
    })
    this.addNewContactForm.get('contactNoCountryCode').valueChanges.subscribe((contactNoCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(contactNoCountryCode);
    });
    this.addNewContactForm.get('contactNo').valueChanges.subscribe(() => {
      this.setPhoneNumberLengthByCountryCode(this.addNewContactForm.get('contactNoCountryCode').value, false);
    });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string, isSelectedcountryCode = true) {
    switch (countryCode) {
      case "+27":
        this.addNewContactForm.get('contactNo').setValidators([Validators.required, Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        this.addNewContactForm.updateValueAndValidity();
        if(isSelectedcountryCode) {
          this.showErrorOnSelectCountryCode(formConfigs.southAfricanContactNumberMaxLength);
        }
        break;
      default:
        this.addNewContactForm.get('contactNo').setValidators([Validators.required, Validators.minLength(formConfigs.indianContactNumberMaxLength),
        Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        this.addNewContactForm.updateValueAndValidity();
        if(isSelectedcountryCode) {
          this.showErrorOnSelectCountryCode(formConfigs.indianContactNumberMaxLength);
        }
        break;
    }
  }

  showErrorOnSelectCountryCode(maxLength: number) {
    const number = this.addNewContactForm.get('contactNo').value?.replace(/ /g, "");
    if (number?.toString()?.length > maxLength) {
      this.addNewContactForm.get('contactNo').setErrors({ maxlength: {
        requiredLength: maxLength
      } });
    } else if (number?.toString()?.length < maxLength) {
      this.addNewContactForm.get('contactNo').setErrors({ minlength: {
        requiredLength: maxLength
      } });
    } else {
      this.addNewContactForm.get('contactNo').setErrors(null);
    }
    this.addNewContactForm.updateValueAndValidity();
    return false;
  }

  createPrCallForm() {
    this.prCallForm = this.formBuilder.group({
      serviceCallRequestId: [''],
      callInitiationId: [this.initiationId, Validators.required],
      motivation: ['', Validators.required],
      createdUserId: this.userData.userId
    })
  }

  createNotesForm() {
    this.notesForm = this.formBuilder.group({
      callInitiationId: [this.initiationId],
      customerId: [this.customerId, Validators.required],
      addressId: [this.customerAddressId, Validators.required],
      notes: ['', Validators.required],
      isCallInitiationNotes: [true, Validators.required],
      isTechInstruction: [false, Validators.required],
      createdUserId: this.userData.userId
    })
  }

  createCallCreationTemplateForm(callCreationTemplateModel?: CallCreationTemplateModel) {
    let callCreationTemplateModelControl = new CallCreationTemplateModel(callCreationTemplateModel);
    this.callCreationTemplateForm = this.formBuilder.group({
      callCreationList: this.formBuilder.array([])
    });
    Object.keys(callCreationTemplateModelControl).forEach((key) => {
      this.callCreationTemplateForm.addControl(key, new FormControl(callCreationTemplateModelControl[key]));
    });

  }

  createActualTimingForm(actualTimingModel?: ActualTimingModel) {
    let actualTimingModelControl = new ActualTimingModel(actualTimingModel);
    this.actualTimingForm = this.formBuilder.group({
      actualTimingList: this.formBuilder.array([])
    });
    Object.keys(actualTimingModelControl).forEach((key) => {
      this.actualTimingForm.addControl(key, new FormControl(actualTimingModelControl[key]));
    });

  }

  //Create FormArray
  get getcallCreationListArray(): FormArray {
    if (!this.callCreationTemplateForm) return;
    return this.callCreationTemplateForm.get("callCreationList") as FormArray;
  }

  //Create FormArray
  get getactualTimingListArray(): FormArray {
    if (!this.actualTimingForm) return;
    return this.actualTimingForm.get("actualTimingList") as FormArray;
  }

  //Create FormArray controls
  createcallCreationTemplateListModel(callCreationListModel?: CallCreationTemplateListModel): FormGroup {
    let callCrationListFormControlModel = new CallCreationTemplateListModel(callCreationListModel);
    let formControls = {};
    Object.keys(callCrationListFormControlModel).forEach((key) => {
      if (key == 'value') {
        if (callCrationListFormControlModel['questionName'].toLowerCase() == 'trouble shooting') {
          formControls[key] = [{ value: callCrationListFormControlModel[key], disabled: false }]
        } else {
          if (callCrationListFormControlModel['isYesNo'] && !callCrationListFormControlModel[key]) {
            formControls[key] = [{ value: callCrationListFormControlModel[key], disabled: false }]
          } else {
            if (this.callCreationTemplate) {
              formControls[key] = [{ value: callCrationListFormControlModel[key], disabled: false }]
            } else {
              formControls[key] = [{ value: callCrationListFormControlModel[key], disabled: false }, [Validators.required]]
            }
          }
        }
      } else {
        formControls[key] = [{ value: callCrationListFormControlModel[key], disabled: false }]
      }
    });
    let formContrlsGroup = this.formBuilder.group(formControls)
    if (formContrlsGroup.get('isYesNo').value) {
      formContrlsGroup.get('isTogglevalue').setValue((formContrlsGroup.get('value').value) ? true : false);
    };
    formContrlsGroup.get('isTogglevalue').valueChanges.subscribe(val => {
      if (val) {
        if (!formContrlsGroup.get('isTextBox').value) {
          formContrlsGroup.get('value').setValue(val)
        }
        formContrlsGroup.get('value').setValidators([Validators.required])
      } else {
        formContrlsGroup.get('value').clearValidators();
        formContrlsGroup.get('value').setValue(null)
      }
      formContrlsGroup.get('value').updateValueAndValidity()

    })
    return formContrlsGroup;
  }

  onRedirectTroubleShootChekAutoSave() {
    if (this.getEditPermission()) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.initiationId) {
      this.autoSaveAlertNavigate(2) // 2- redirect to troubleshoot
      return
    } else {
      this.autoSaveAlertNavigate(2) // 2- redirect to troubleshoot
      return;
    }
  }

  onRedirectTroubleShoot() {
    if (this.getEditPermission()) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value
      && !this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').value && !this.selfHelpSystemTypeConfigId) {
      this.router.navigate(['/customer/manage-customers/trouble-shoot-alarm'], { queryParams: { id: this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value, customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType } });
    } else if (this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value && this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').value && this.selfHelpSystemTypeConfigId) {
      this.router.navigate(['/customer/manage-customers/trouble-shoot-alarm-quetion-answer'], { queryParams: { id: this.selfHelpSystemTypeConfigId, systemId: this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value, customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType } });
    } else {
      this.router.navigate(['/customer/manage-customers/trouble-shoot-system'], { queryParams: { customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType } });
      // this.snackbarService.openSnackbar("The selected system type and fault description does not have active questions. Please select different system type.", ResponseMessageTypes.WARNING);
      // this.isNavigationFirstTime = true;
      return;
    }
  }

  getSelfConfigIdAPI() {
    return this.crudService.get(ModulesBasedApiSuffix.SHARED,
      BillingModuleApiSuffixModels.FAULT_DESCRIPTION_SYSTEM_TYPES, this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value, false, null, 1)
  }

  getSelfHelpConfigId() {
    if (this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value && this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').value) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.getSelfConfigIdAPI().subscribe((response: IApplicationResponse) => {
        this.onAfterSelfHelpConfig(response);
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  onAfterSelfHelpConfig(response) {
    if (response?.isSuccess && response?.statusCode == 200 && response.resources?.length) {
      this.selfHelpSystemTypeConfigId = response.resources?.find(el => el?.faultDescriptionId == this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').value)?.selfHelpSystemTypeConfigId;
    }
  }

  onValidateTroubleShoot() {
    this.crudService.get(ModulesBasedApiSuffix.SHARED,
      BillingModuleApiSuffixModels.SELF_HELP_TROUBLESHOOT_ENABLE, null, false, prepareRequiredHttpParams({ customerId: this.customerId, addressId: this.customerAddressId }))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess && response.statusCode == 200) {
          this.showTroubleShootBtn = response?.resources;
        }
      })
  }

  //Create FormArray controls
  createactualTimingListModel(actualTimingListModel?: ActualTimingListModel): FormGroup {
    let actualTimingListModelContol = new ActualTimingListModel(actualTimingListModel);
    let formControls = {};
    Object.keys(actualTimingListModelContol).forEach((key) => {
      if (key == 'onRouteDate') { //  || key == 'startTime' || key == 'endTime'
        formControls[key] = [{ value: actualTimingListModelContol[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: actualTimingListModelContol[key], disabled: false }]
      }
    });
    let formContrlsGroup = this.formBuilder.group(formControls)
    formContrlsGroup.get('startTime').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      let endTimeMinTime = this.momentService.toMoment(this.momentService.addMinits(val, 1)).seconds(0).format();
      formContrlsGroup.get('endTimeMinDate').setValue(endTimeMinTime, { emitEvent: false });
      formContrlsGroup.get('endTime').setValue(null, { emitEvent: false });
      formContrlsGroup.get('actualTime').setValue(null, { emitEvent: false });
      formContrlsGroup.updateValueAndValidity()
    })
    formContrlsGroup.get('endTime').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      let currentStartTime = this.momentService.toMoment(formContrlsGroup.get('startTime').value).seconds(0).format();
      let currentEndTime = this.momentService.toMoment(formContrlsGroup.get('endTime').value).seconds(0).format();
      let addToMints1 = this.momentService.getTimeDiff(formContrlsGroup.get('startTime').value, formContrlsGroup.get('endTime').value)
      formContrlsGroup.get('actualTime').setValue(addToMints1);
    })
    formContrlsGroup.get('onRouteDate').valueChanges.subscribe(val => {
      if (val) {
        let appointmentLogData = this.actualTimingDateList.find(element => element['displayName'] == val);
        formContrlsGroup.get('appointmentLogId').setValue(appointmentLogData ? appointmentLogData['id'] : '');
      } else {
        formContrlsGroup.get('appointmentLogId').setValue('');
      }
    })
    formContrlsGroup.get('onRouteTime').valueChanges.subscribe(val => {
      if (!val) {
        return;
      }
      // actualTimingListModel.startTimeMinDate = actualTimingListModel.onRouteTime ?  this.momentService.toMoment(this.momentService.addMinits(actualTimingListModel.onRouteTime, 1)).seconds(0).format() : null;
      let startTimeMinTime = this.momentService.toMoment(this.momentService.addMinits(val, 1)).seconds(0).format();
      formContrlsGroup.get('startTimeMinDate').setValue(startTimeMinTime, { emitEvent: false });
      formContrlsGroup.get('startTime').setValue(null, { emitEvent: false });
      formContrlsGroup.get('endTime').setValue(null, { emitEvent: false });
      formContrlsGroup.get('actualTime').setValue(null, { emitEvent: false });
    })
    return formContrlsGroup;
  }

  onChangeActualTime(key, val, i) {
    this.actualTimingList.value.forEach((element, index) => {
      if (i != index) {
        let convertedOnRouteDate = this.getactualTimingListArray.controls[i].get('onRouteDate').value;
        let convertedElementOnRouteDate = this.getactualTimingListArray.controls[index].get('onRouteDate').value;
        let isSameDay = this.momentService.isSameDay(convertedOnRouteDate, convertedElementOnRouteDate)
        if (isSameDay) {
          let currentChangedTime = this.momentService.toMoment(val.value).seconds(0).format();
          let exist = this.momentService.betWeenDateTime(element.startTime, element.endTime, new Date(currentChangedTime));
          let isStartSameDay = this.momentService.isSameDayTime(element.startTime, new Date(currentChangedTime));
          let isEndSameDay = this.momentService.isSameDayTime(element.endTime, new Date(currentChangedTime));
          if (exist || isStartSameDay || isEndSameDay) {
            this.getactualTimingListArray.controls[i].get(key).setValue(null, { emitEvent: false });
            this.snackbarService.openSnackbar('Time already exist', ResponseMessageTypes.WARNING);
          }
        }
      }
    });
  }

  onSubmitCallCreationTemplate() {
    if (this.getEditPermission()) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.callCreationTemplateForm.invalid) {
      return
    }
    if (!this.callCreationTemplateForm.dirty) {
      this.snackbarService.openSnackbar("No changes were detected", ResponseMessageTypes.WARNING)
    }
    if (this.initiationId && this.callCreationTemplateForm.dirty) {
      if (this.callType == 1) {
        this.callCreationTempDetails = this.getcallCreationListArray.getRawValue();
      } else {
        this.onUpdateCallCreation();
      }
    } else if (!this.initiationId) {
      this.callCreationTempDetails = this.getcallCreationListArray.getRawValue();
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.callCreationDialog = false
  }

  onUpdateCallCreation() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION_QUESTIONS, this.callCreationTemplateForm?.getRawValue()?.callCreationList)
      .subscribe(res => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res?.isSuccess && res?.statusCode == 200) {
          this.openCallCreationDialog(false);
        }
      })
  }

  //Add Details
  addActualTiming(): void {
    if (!this.getActualTimingActionPermission(PermissionTypes?.ADD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.getactualTimingListArray.invalid) {
      this.getactualTimingListArray.markAllAsTouched();
      return;
    };
    this.actualTimingList = this.getactualTimingListArray;
    let actualTimingListModel = new ActualTimingListModel(null);
    actualTimingListModel.appointmentId = this.initiationDetailsData?.appointmentInfo?.appointmentId ? this.initiationDetailsData?.appointmentInfo?.appointmentId : ''
    actualTimingListModel.onRouteDate = "";
    actualTimingListModel.createdUserId = this.userData.userId ? this.userData.userId : ''
    actualTimingListModel.modifiedUserId = this.userData.userId ? this.userData.userId : ''
    this.actualTimingList.insert(0, this.createactualTimingListModel(actualTimingListModel));
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeActualTiming(i: number): void {
    if (!this.getActualTimingActionPermission(PermissionTypes?.DELETE)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.getactualTimingListArray.controls[i].value.appointmentActualTimingId) {
      this.getactualTimingListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getactualTimingListArray.controls[i].value.appointmentActualTimingId && this.getactualTimingListArray.length > 1) {
        this.crudService.delete(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.APPOINTMENTS_ACTUAL_TIMING,
          undefined,
          prepareRequiredHttpParams({
            appointmentActualTimingId: this.getactualTimingListArray.controls[i].value.appointmentActualTimingId,
            modifiedUserId: this.userData.userId
          })
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getactualTimingListArray.removeAt(i);
          }
          if (this.getactualTimingListArray.length === 0) {
            this.addActualTiming();
          };
        });
      }
      else {
        this.getactualTimingListArray.removeAt(i);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onSubmitActualTiming() {
    if (!this.getActualTimingActionPermission(PermissionTypes?.ADD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.actualTimingForm.invalid) {
      return
    }
    let formValue = this.actualTimingForm.value
    formValue.actualTimingList.forEach(element => {
      element.onRouteDate = element.onRouteDate ? this.momentService.toFormateType(element.onRouteDate, 'YYYY-MM-DD') : null
      element.startTime = element.startTime ? this.momentService.toFormateType(element.startTime, 'HH:mm') : null
      element.endTime = element.endTime ? this.momentService.toFormateType(element.endTime, 'HH:mm') : null
      element.onRouteTime = element.onRouteTime ? this.momentService.toFormateType(element.onRouteTime, 'HH:mm') : null
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.APPOINTMENTS_ACTUAL_TIMING, formValue.actualTimingList)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.actualTimeDialog = false
          this.getCallInitiationDetailsById();
        }
      });
  }

  openReturnForRepair() {
    if (!this.initiationDetailsData?.basicInfo?.callInitiationId) {
      return;
    }
    this.router.navigate(['/technical-management/return-for-repair/edit'], {
      queryParams: {
        callInitiationId: this.initiationBasicInfo.callInitiationId
      }, skipLocationChange: false
    });
  }

  openAddContact(val) {
    this.addNewContactForm.get('contactName').setValue(null)
    this.addNewContactForm.get('contactNo').setValue(null)
    this.openAddContactDialog = true
    this.addNewContactForm.get('isPrimary').setValue(val)
    this.addNewContactForm.markAsUntouched();
  }

  /**
   * new contact add purpose. hanldled UI side. we are passing the new added contact when click service call save button
   * @returns
   */
  addContactSubmit() {
    this.addNewContactForm.markAsUntouched();
    if (this.addNewContactForm.invalid) {
      this.addNewContactForm.markAllAsTouched();
      return
    }
    let formData = this.addNewContactForm.value
    formData.callInitiationId = this.initiationId ? this.initiationId : null
    formData.contactNo = formData.contactNo.replace(/\s/g, "")
    this.openAddContactDialog = false
    let newcontactData = {
      keyHolderId: Math.random(),
      keyHolderName: formData.contactName,
      contactNumber: formData.contactNoCountryCode + ' ' + formData.contactNo,
      isNewContact: true,
      isPrimary: formData.isPrimary
    }
    if (formData.isPrimary) {
      this.contactList.push(newcontactData)
    } else {
      this.alternativContactList.push(newcontactData)
    }
  }

  get installationPartitionsFormArray(): FormArray {
    if (this.initiatonCallCreationForm !== undefined) {
      return (<FormArray>this.initiatonCallCreationForm.get('installationPartitions'));
    }
  }

  getContactListDropdown() {
    let api = [];
    if (this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value && this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').value) {
      api.push(this.getSelfConfigIdAPI().pipe(map(result => result), catchError(error => of(error))));
    } else {
      api.push(of([]));
    }
    if (this.callType != '1' && !this.isUpselling) {
      if (this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value) {
        api.push(this.getSystemCategoryAPI().pipe(map(result => result), catchError(error => of(error))))
      } else {
        api.push(of([]));
      }
      if (this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').value) {
        api.push(this.getServiceSubTypesAPI().pipe(map(result => result), catchError(error => of(error))))
      } else {
        api.push(of([]));
      }
      if (this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').value) {
        api.push(this.getFaultDescriptionAPI().pipe(map(result => result), catchError(error => of(error))))
      } else {
        api.push(of([]));
      }
      if (this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').value && this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').value) {
        api.push(this.getJobIdAPI().pipe(map(result => result), catchError(error => of(error))))
      } else {
        api.push(of([]));
      }
    }
    if (api?.length) {
      this.contactListSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
        if (window.location.toString().includes('localhost')) {
          console.log(response);
        }
        response?.forEach((res, ix) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            switch (ix) {
              case 0:
                this.onAfterSelfHelpConfig(res);
                break;
              case 1:
                this.onAfterSystemCategory(res);
                break;
              case 2:
                this.onAfterServiceSubTypes(res);
                break;
              case 3:
                this.onAfterFaultDescription(res);
                break;
              case 4:
                this.onAfterJobType(res);
                break;
            }
          }
        })
        this.onCloseLoader();
      })
    }
  }

  onSelectTime(event) {
    // let addFromMints = this.momentService.toHoursMints24(event)
    this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').setValue(event)
    // let addToMints = this.momentService.toHoursMints12(event)
    // let addToMints1 = this.momentService.addMinits(event, this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value)
    let estimatedTime = this.getFormatEstimationTime()
    let spltTime = estimatedTime?.split(':')
    let addToMints1 = this.momentService.addHoursMinits(event, spltTime[0], spltTime[1])
    this.initiatonCallCreationForm.get('appointmentInfo.scheduledEndDate').setValue(new Date(addToMints1))
  }

  getFormatEstimationTime() {
    var estimatedTime: string = this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value
    return estimatedTime
  }

  loadActionTypes(dropdownsAndData) {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.multipleSubscription = forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      if (window.location.toString().includes('localhost')) {
        console.log(response);
      }
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          // if (this.callType == '1' || this.isUpselling) { // initiation call
          switch (ix) {
            case 0:
              this.debtorList = resp.resources;
              break;
            case 1:
              this.panelList = prepareAutoCompleteListFormatForMultiSelection(resp.resources);
              break;
            case 2:
              this.diagnosisList = resp.resources;
              break;
            case 3:
              this.priorityList = resp.resources;
              break;
            case 4:
              //   this.reasonDropDown = resp.resources;
              //   break;
              // case 5:
              this.sysyemTypeList = resp.resources;
              break;
            case 5:
              if (this.callType == '1' || this.isUpselling) { // initiation call
                this.SystemTypeSubCategoryList = resp.resources;
              } else if ((this.callType == '2' || this.callType == '3') && !this.isUpselling) { // service call
                this.projectList = resp.resources;
              }
              break;
            case 6:
              if (this.callType == '1' || this.isUpselling) { // initiation call
                this.jobTypeList = resp.resources;
                if (this.initiationServiceInfo?.jobTypeId) {
                  this.getEstimatedTime(this.initiationServiceInfo?.jobTypeId);
                }
              } else if ((this.callType == '2' || this.callType == '3') && !this.isUpselling) { // service call
                this.faultDescriptionList = resp.resources;
              }
              break;
            case 7:
              this.onPatchContactDetails(resp);
              break;
            case 8:
              this.onPatchValue(resp);
              break;
          }
        }
      })
    });
  }

  onCloseLoader() {
    if (this.multipleSubscription && this.multipleSubscription?.closed
      && this.initiationBasicInfo?.isCon
      // && this.contactListSubscription && this.contactListSubscription?.closed
      && this.callCreationCreationSubscription && this.callCreationCreationSubscription?.closed) {
      this.rxjsService.setGlobalLoaderProperty(false);
    } else if (this.multipleSubscription && this.multipleSubscription?.closed
      && this.contactListSubscription && this.contactListSubscription?.closed
      && this.callCreationCreationSubscription && this.callCreationCreationSubscription?.closed) {
      this.rxjsService.setGlobalLoaderProperty(false);
    } else if (this.multipleSubscription && this.multipleSubscription?.closed
      && this.callCreationCreationSubscription && this.callCreationCreationSubscription?.closed && !this.initiationId) {
      this.rxjsService.setGlobalLoaderProperty(false);
    } else if (this.multipleSubscription && this.multipleSubscription?.closed
      && this.contactListSubscription && this.contactListSubscription?.closed && (!this.initiationId || this.initiationId)) {
      this.rxjsService.setGlobalLoaderProperty(false);
    } else if (this.callCreationCreationSubscription && this.callCreationCreationSubscription?.closed &&
      (!this.initiationId || this.initiationId)) {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    this.onFormDisabled();
    if (this.getEditPermission()) {
      this.initiatonCallCreationForm.disable({ emitEvent: false });
      this.callCreationTemplateForm.disable();
    }
  }

  getEditPermission() {
    return !this.findActionPermission(PermissionTypes.EDIT) && this.initiationId;
  }

  createInstallationcallCreationFrom(esclatioListModel?: InstallationCallinitiationAddEditModel) {
    let initiationModel = new InstallationCallinitiationAddEditModel(esclatioListModel);
    this.initiatonCallCreationForm = this.formBuilder.group({});
    Object.keys(initiationModel).forEach((key) => {
      if (key == 'serviceInfo') {
        if (this.callType == '1' || this.isUpselling) { // initiation call
          let serviceInfo = this.formBuilder.group({
            'callInitiationServiceInfoId': '',
            'systemTypeId': ['', Validators.required],
            'systemTypeSubCategoryId': ['', Validators.required],
            'serviceSubTypeId': '',
            'faultDescriptionId': '',
            'jobTypeId': ['', Validators.required],
            'description': '',
          });
          this.initiatonCallCreationForm.addControl(key, serviceInfo); //technicianCollJobModel[key]
        } else if ((this.callType == '2' || this.callType == '3') && !this.isUpselling) { // service call
          let serviceInfo = this.formBuilder.group({
            'callInitiationServiceInfoId': '',
            'systemTypeId': ['', Validators.required],
            'systemTypeSubCategoryId': ['', Validators.required],
            'serviceSubTypeId': ['', Validators.required],
            'faultDescriptionId': ['', Validators.required],
            'jobTypeId': ['', Validators.required],
            'description': '',
          });
          this.initiatonCallCreationForm.addControl(key, serviceInfo); //technicianCollJobModel[key]
        }
      } else if (key == 'appointmentInfo') {
        let appointmentInfo = this.formBuilder.group({
          'appointmentId': '',
          'appointmentLogId': '',
          'nextAvailableAppointment': '',
          'isNextAvailableAppointment': false,
          // 'nextAvailableAppointmentCheckbox': false,
          'scheduledStartDate': '',
          'scheduledEndDate': '',
          'diagnosisId': '',
          'estimatedTime': '',
          'estimatedTimeStatic': '',
          'actualTime': '',
          'travelTime': '',
          "technician": '',
          "technicianId": '',
          "preferredDate": '',
          "preferredTime": '',
          "isAppointmentChangable": false,
          "techArea": '',
          'wireman': ''
        });
        this.initiatonCallCreationForm.addControl(key, appointmentInfo); //technicianCollJobModel[key]
      } else if (key == 'installationPartitions') {
        let skillMatrixServiceSubTypeFormArray = this.formBuilder.array([]);
        this.initiatonCallCreationForm.addControl(key, skillMatrixServiceSubTypeFormArray);
      } else if (key == 'specialProjectId') {
        if (this.callType == '3') {
          this.initiatonCallCreationForm.addControl(key, new FormControl(initiationModel[key], Validators.required));
        } else {
          this.initiatonCallCreationForm.addControl(key, new FormControl(initiationModel[key]));
        }
      } else if (key == 'nkaServiceCall') {
        let nkaServiceCall = this.formBuilder.group({
          'isPOAccepted': null,
          'isPONotAccepted': null,
          'poNumber': '',
          'poAmount': '',
          'poComment': '',
          'poAttachments': '',
          'attachmentUrl': '',
          'path': undefined,
        });
        nkaServiceCall.get('isPOAccepted').disable();
        nkaServiceCall.get('isPONotAccepted').disable();
        nkaServiceCall.get('poNumber').disable();
        // nkaServiceCall.get('poAttachments').disable();
        nkaServiceCall.get('path').disable();
        nkaServiceCall.get('poAmount').disable();
        nkaServiceCall.get('poComment').disable();
        this.initiatonCallCreationForm.addControl(key, nkaServiceCall);
      } else {
        this.initiatonCallCreationForm.addControl(key, new FormControl(initiationModel[key]));
      }
    });
    // this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["contact","serviceInfo.systemTypeId","serviceInfo.systemTypeSubCategoryId","serviceInfo.jobTypeId"]);
    if (this.callType == '1') {
      // this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["contactId", "isAddressVerified", "panelTypeConfigId"]);
      this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["contactId", "contactNumber", "isAddressVerified"]);
    } else {
      this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["contactId", "contactNumber", "isAddressVerified", "debtor", "debtorId"]);
    }
  }

  // For contact details drodpdown api (primary contact and alternate contact)
  getContactDetailsAPI() {
    let param: any = { customerId: this.customerId, addressId: this.customerAddressId };
    if (this.initiationId) {
      param = { callInitiationId: this.initiationId };
    }
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION_CONTACT_DETAILS, null, false,
      prepareRequiredHttpParams(param)).pipe(map(result => result), catchError(error => of(error)))
  }

  onLoadContactDetails() {
    this.contactList = [];
    this.contactNumberList = [];
    this.alternativContactList = [];
    this.contactAlternativeNumberList = [];
    this.getContactDetailsAPI()?.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.onPatchContactDetails(res);
      }
    })
  }

  onPatchContactDetails(response) {
    response?.resources?.primaryContacts?.forEach(element => {
      this.contactList.push({
        keyHolderId: element?.keyholderId ? element?.keyholderId : element?.callInitiationContactId,
        keyHolderName: element?.contactName,
        contactNumber: element?.mobile1,
        isNewContact: element?.keyholderId ? false : true,
        mobile1: element?.mobile1,
        mobile2: element?.mobile2,
        officeNo: element?.officeNo,
        premisesNo: element?.premisesNo,
      })
    });
    response?.resources?.alternativeContacts?.forEach(element => {
      this.alternativContactList.push({
        keyHolderId: element?.keyholderId ? element?.keyholderId : element?.callInitiationContactId,
        keyHolderName: element?.contactName,
        contactNumber: element?.mobile1,
        isNewContact: element?.keyholderId ? false : true,
        mobile1: element?.mobile1,
        mobile2: element?.mobile2,
        officeNo: element?.officeNo,
        premisesNo: element?.premisesNo,
      })
    });
    const selectedPrimaryContact = response?.resources?.primaryContacts?.find(el => el?.selectedContact);
    this.initiatonCallCreationForm?.get('contactId').setValue(selectedPrimaryContact?.keyholderId || selectedPrimaryContact?.callInitiationContactId || '', { emitEvent: false });
    this.onAfterContactChanges(this.initiatonCallCreationForm?.get('contactId').value);
    this.initiatonCallCreationForm?.get('contactNumber').setValue(selectedPrimaryContact?.selectedContact || '');
    const selectedAlterContact = response?.resources?.alternativeContacts?.find(el => el?.selectedContact);
    this.initiatonCallCreationForm?.get('alternateContactId').setValue(selectedAlterContact?.keyholderId || selectedAlterContact?.callInitiationContactId || '', { emitEvent: false });
    this.onAfterAlterContactChanges(this.initiatonCallCreationForm?.get('alternateContactId').value);
    this.initiatonCallCreationForm?.get('alternateContactNumber').setValue(selectedAlterContact?.selectedContact || '');
    // this.onLoadContacts();
  }
  // End For contact details drodpdown api (primary contact and alternate contact)

  //Get Details
  getInitiationCallDetailsById(): Observable<IApplicationResponse> {
    if (this.callType == '1') { // initiation call
      return this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        SalesModuleApiSuffixModels.INSTALLATION_GET_METHOD,
        this.initiationId).pipe(map(result => result), catchError(error => of(error)))
    } else if (this.callType == '2' || this.callType == '3') { // service call
      if (this.initiationId) {
        return this.crudService.get(
          ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION,
          this.initiationId).pipe(map(result => result), catchError(error => of(error)))
      } else {
        return this.crudService.get(
          ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION_CUSTOMER,
          // this.customerId
          undefined,
          false, prepareGetRequestHttpParams(null, null,
            this.warrantyRecallReferenceId ? {
              CustomerId: this.customerId,
              AddressId: this.customerAddressId,
              warrantyRecallReferenceId: this.warrantyRecallReferenceId ? this.warrantyRecallReferenceId : null
            } :
              {
                CustomerId: this.customerId,
                AddressId: this.customerAddressId,
              }
          )
        ).pipe(map(result => result), catchError(error => of(error)))
      }
    }
  };

  onLoadDropdownByCondition() {
    let api = [];
    if (this.callType == '1' || this.isUpselling) { // initiation call
      api = [
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE,
          undefined,
          false, prepareGetRequestHttpParams(null, null,
            {
              ShowDescriptionOnly: true
            })).pipe(map(result => result), catchError(error => of(error))),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE_SUB_CATEGORIES)
          .pipe(map(result => result), catchError(error => of(error))),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          TechnicalMgntModuleApiSuffixModels.JOBTYPE)
          .pipe(map(result => result), catchError(error => of(error))),
      ]
    } else if ((this.callType == '2' || this.callType == '3') && !this.isUpselling) { // service call
      api = [
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE,
          undefined,
          false, prepareRequiredHttpParams(
            {
              ShowDescriptionOnly: true,
              CallInitiationId: this.initiationId
            })).pipe(map(result => result), catchError(error => of(error))),
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          InventoryModuleApiSuffixModels.UX_SPECIAL_PROJECT,
          undefined,
          false, prepareRequiredHttpParams({
            IsOpen: true
          })).pipe(map(result => result), catchError(error => of(error))),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_QUICK_CALL)
          .pipe(map(result => result), catchError(error => of(error))),
      ]
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      if (window.location.toString().includes('localhost')) {
        console.log(response);
      }
      response?.forEach((res: IApplicationResponse, ix: number) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          switch (ix) {
            case 0:
              this.sysyemTypeList = res?.resources;
              break;
            case 1:
              if (this.callType == '1' || this.isUpselling) { // initiation call
                this.SystemTypeSubCategoryList = res?.resources;
              } else if ((this.callType == '2' || this.callType == '3') && !this.isUpselling) { // service call
                this.projectList = res?.resources;
              }
              break;
            case 2:
              if (this.callType == '1' || this.isUpselling) { // initiation call
                this.jobTypeList = res?.resources;
                if (this.initiationServiceInfo?.jobTypeId) {
                  this.getEstimatedTime(this.initiationServiceInfo?.jobTypeId);
                }
              } else if ((this.callType == '2' || this.callType == '3') && !this.isUpselling) { // service call
                this.faultDescriptionList = res?.resources;
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  getNextavailableAppointment() {
    // if (!this.initiationId || !this.initiationDetailsData?.basicInfo?.callInitiationId) {
    //   return
    // }
    if (!this.initiationId) {
      return
    }
    let otherParams = {}
    otherParams['CallInitiationId'] = this.initiationId ? this.initiationId : this.initiationDetailsData?.basicInfo?.callInitiationId ? this.initiationDetailsData?.basicInfo?.callInitiationId : null
    otherParams['NoOfDates'] = 1
    let estimatedTime = this.getFormatEstimationTime()
    otherParams['estimatedTime'] = estimatedTime
    if (!estimatedTime) {
      return
    }
    // this.httpCancelService.cancelPendingRequestsOnNavigationChanged()
    if (this.initiationBasicInfo?.isVoid == false && this.initiationBasicInfo?.isInvoiced == false && this.initiationBasicInfo?.isTempDone == false) {
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CAPACITY_APPOINTMETN_SLOT, null, false,
        prepareGetRequestHttpParams(null, null, otherParams))
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.initiatonCallCreationForm.get('appointmentInfo').get('technicianId').setValue(response.resources[0].technicianId)
            let nextAvailableDateTime = this.momentService.setTimetoRequieredDate(response.resources[0].appointmentDate, response.resources[0].appointmentTimeFrom)
            this.initiatonCallCreationForm.get('appointmentInfo').get('nextAvailableAppointment').setValue(new Date(nextAvailableDateTime))
            if (this.initiatonCallCreationForm.get('appointmentInfo').get('nextAvailableAppointment').value) {
              if (this.initiatonCallCreationForm.get('appointmentInfo').get('isNextAvailableAppointment').value) {
                this.onSelectTime(nextAvailableDateTime)
              }
            }
          } else {
            this.initiatonCallCreationForm.get('appointmentInfo').get('nextAvailableAppointment').setValue(null)
            // if (this.initiatonCallCreationForm.get('appointmentInfo').get('nextAvailableAppointment').value) {
            //   this.initiatonCallCreationForm.get('appointmentInfo').get('scheduledStartDate').setValue(null)
            //   this.initiatonCallCreationForm.get('appointmentInfo').get('scheduledEndDate').setValue(null)
            // }
          }
          // this.rxjsService.setGlobalLoaderProperty(false);
          this.onCloseLoader();
        });
    }
  }

  createOnHoldCreationFrom(onHoldModel?: OnHoldModel) {
    let onHoldPopupModel = new OnHoldModel(onHoldModel);
    this.onHoldPopupForm = this.formBuilder.group({});
    Object.keys(onHoldPopupModel).forEach((key) => {
      this.onHoldPopupForm.addControl(key, new FormControl(onHoldPopupModel[key]));
    });
    // this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["contact","serviceInfo.systemTypeId","serviceInfo.systemTypeSubCategoryId","serviceInfo.jobTypeId"]);
    this.onHoldPopupForm = setRequiredValidator(this.onHoldPopupForm, ["OnHoldReason", "OnHoldFollowUpDate"]);
  }

  onHoldValidationCheck() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.ON_HOLD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_ON_HOLD_VALIDATION,
      this.initiationId
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        if (response.resources.validateMessage) {
          this.warningNotificationMessage = response.resources;
          this.isWarningNotificationDialog = true;
        } else {
          this.openHoldInfoDialog()
        }
      }
    });
  }

  openHoldInfoDialog() {
    this.onHoldPopupForm.reset();
    this.onHoldPopupForm.controls['OnHoldReason'].patchValue(this.initiationBasicInfo.onHoldReason);
    this.minFUpDate = this.initiationBasicInfo?.onHoldFollowUpDate ? new Date(this.initiationBasicInfo?.onHoldFollowUpDate) : new Date();
    this.onHoldPopupForm.controls['OnHoldFollowUpDate'].patchValue(this.initiationBasicInfo.onHoldFollowUpDate ? this.minFUpDate : '');
    this.holdInfoDialog = true;
  }

  openUpgradeLead() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT?.UPSELL_QUOTE)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    if (!this.initiationDetailsData?.basicInfo?.callInitiationId) {
      return;
    }
    if(   !this.initiationDetailsData.appointmentInfo 
       || !this.initiationDetailsData.appointmentInfo?.scheduledStartDate
       || !this.initiationDetailsData.appointmentInfo?.scheduledEndDate 
       || !this.initiationDetailsData.appointmentInfo?.technicianId 
      ) {
        this.snackbarService.openSnackbar("Call not scheduled.", ResponseMessageTypes.WARNING);
        return;
    }
   
    // if (this.initiationDetailsData?.appointmentInfo?.technicianId && this.initiationDetailsData?.appointmentInfo?.scheduledEndDate && this.initiationDetailsData?.appointmentInfo?.scheduledStartDate) {
    this.rxjsService.setUpsellingQuoteProperty(true);
    this.rxjsService.setisFromupsellData(true);
    this.router.navigate(['/customer/upsell-quote/item-info'], {
      queryParams: {
        id: this.customerId,
        callInitiationId: this.initiationId,
        addressId: this.customerAddressId,
        callType: this.callType,
        // isVoid: this.initiationBasicInfo?.isVoid ? this.initiationBasicInfo?.isVoid : this.initiationBasicInfo?.isOnHold,
        // isVoid: this.initiationBasicInfo?.isVoid,
        isVoid: this.initiationBasicInfo?.isVoid ? this.initiationBasicInfo?.isVoid : this.initiationBasicInfo?.isInvoiced,
      }
    });
    // }
  }

  openPrCallDialog(type) {
    if (!this.initiationDetailsData?.basicInfo?.callInitiationId) {
      return;
    }
    // this.prCallForm.reset();
    this.requestType = type
    this.prCallForm.get('callInitiationId').setValue(this.initiationDetailsData?.basicInfo?.callInitiationId ? this.initiationDetailsData?.basicInfo?.callInitiationId : this.initiationId);
    if (type?.toLowerCase() == RequestType.PR_CALL_REQUEST) {
      if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.PR_CALL)) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.prCallForm.controls['motivation'].patchValue(this.initiationDetailsData?.prCall ? this.initiationDetailsData?.prCall?.motivation ? this.initiationDetailsData?.prCall?.motivation : null : null);
      this.prCallForm.controls['serviceCallRequestId'].patchValue(this.initiationDetailsData?.prCall ? this.initiationDetailsData?.prCall?.serviceCallRequestId ? this.initiationDetailsData?.prCall?.serviceCallRequestId : null : null);
    } else {
      if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.SAVE_OFFER)) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      this.prCallForm.controls['motivation'].patchValue(this.initiationDetailsData?.saveOfferCall ? this.initiationDetailsData?.saveOfferCall?.motivation ? this.initiationDetailsData?.saveOfferCall?.motivation : null : null);
      this.prCallForm.controls['serviceCallRequestId'].patchValue(this.initiationDetailsData?.saveOfferCall ? this.initiationDetailsData?.saveOfferCall?.serviceCallRequestId ? this.initiationDetailsData?.saveOfferCall?.serviceCallRequestId : null : null);
    }
    this.prCallDialog = true;
  }

  onHoldApply() {
    const onholdPermission = this.getQuickActionPermission(CUSTOMER_COMPONENT?.UPSELL_QUOTE)?.subMenu?.find(el => el?.menuName == PermissionTypes.EDIT);
    if (!onholdPermission) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.onHoldPopupForm.invalid) {
      this.onHoldPopupForm.markAllAsTouched();
      return;
    }
    let onHoldSaveObj = this.onHoldPopupForm.value;
    onHoldSaveObj.CallInitiationId = this.initiationId;
    onHoldSaveObj.ModifiedUserId = this.userData.userId;
    onHoldSaveObj.OnHoldFollowUpDate = this.datePipe.transform(onHoldSaveObj.OnHoldFollowUpDate, 'yyyy-MM-dd');
    this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_ON_HOLD,
      onHoldSaveObj
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.getCallInitiationDetailsById();
        this.holdInfoDialog = false;
        if (response.resources.validateMessage || response.resources) {
          this.warningNotificationMessage = response.resources;
          this.isWarningNotificationDialog = true;
        }
      }
    });
  }

  createVoidInfoCreationFrom(voidModel?: VoidModel) {
    let voidPopupModel = new VoidModel(voidModel);
    this.voidInfoForm = this.formBuilder.group({});
    Object.keys(voidPopupModel).forEach((key) => {
      this.voidInfoForm.addControl(key, new FormControl(voidPopupModel[key]));
    });
    // this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["contact","serviceInfo.systemTypeId","serviceInfo.systemTypeSubCategoryId","serviceInfo.jobTypeId"]);
    this.voidInfoForm = setRequiredValidator(this.voidInfoForm, ["VoidReason"]);
  }

  onVoidValidationCheck() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.VOID)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.initiationDetailsData?.basicInfo?.callInitiationId) {
      return;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_VOID_VALIDATION,
      this.initiationId
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        if (response.resources.validateMessage) {
          this.warningNotificationMessage = response.resources;
          this.isWarningNotificationDialog = true;
        } else {
          this.openVoidInfoDialog()
        }
      }
    });
  }

  openVoidInfoDialog() {
    this.voidInfoForm.reset();
    this.voidInfoForm.controls['VoidReason'].patchValue(this.initiationBasicInfo.voidReason)
    this.voidInfoDialog = true;
  }

  applyVoidInfo() {
    const voidEditPermission = this.getQuickActionPermission(CUSTOMER_COMPONENT.VOID)?.subMenu?.find(el => el?.menuName == PermissionTypes.EDIT) ? true : false;
    if (!voidEditPermission) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.voidInfoForm.invalid) {
      this.voidInfoForm.markAllAsTouched();
      return;
    }
    let voidInfoSaveObj = this.voidInfoForm.value
    voidInfoSaveObj.CallInitiationId = this.initiationId;
    voidInfoSaveObj.ModifiedUserId = this.userData.userId;
    this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_VOID,
      voidInfoSaveObj
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        // this.holdInfoDialog = false;
        this.getCallInitiationDetailsById();
        this.voidInfoDialog = false;
        if (response.resources.validateMessage) {
          this.warningNotificationMessage = response.resources;
          this.isWarningNotificationDialog = true;
        }
      }
    });
  }


  openReferToSalesDialog() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.REFER_TO_SALES)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.initiationDetailsData?.basicInfo?.callInitiationId) {
      return;
    }
    if (!this.initiationAppointmentInfo?.quotationVersionId) {
      return;
    }
    // if (this.initiationBasicInfo?.isVoid || this.initiationBasicInfo?.isInvoiced || this.initiationBasicInfo?.isOnHold) {
    // if (this.initiationBasicInfo?.isVoid || this.initiationBasicInfo?.isInvoiced) {
    //   return
    // }
    this.router.navigate(['/customer', 'manage-customers', 'refer-to-sale'], {
      queryParams: {
        appointmentId: this.initiatonCallCreationForm.get('appointmentInfo.appointmentId').value,
        installationId: this.initiationId,
        customerId: this.initiationBasicInfo.customerId,
        callType: this.callType,
        quotationVersionId: this.initiationAppointmentInfo?.quotationVersionId,
        callInitiationId: this.initiationBasicInfo.callInitiationId,
        customerAddressId: this.customerAddressId,
        isVoid: this.initiationBasicInfo?.isVoid ? this.initiationBasicInfo?.isVoid : this.initiationBasicInfo?.isInvoiced,
      }, skipLocationChange: true
    });
    // this.voidInfoDialog = true;
  }

  openCallCreationDialog(isPopupOpen) {
    this.rxjsService.setGlobalLoaderProperty(true);
    // this.callCreationDialog = isPopupOpen;
    let otherParams = {}
    if (this.initiationId) {
      otherParams['callInitiationId'] = this.initiationId
    }
    let faultDesc = this.faultDescriptionList.find(x => x.faultDescriptionId == this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').value)
    if (faultDesc) {
      if (faultDesc.faultDescriptionName == 'System Overactive') {
        otherParams['isOverActive'] = true
      } else {
        otherParams['isOverActive'] = false
      }
    } else {
      otherParams['isOverActive'] = false
    }
    otherParams['isInstallationCall'] = this.callType == '1' ? true : false
    otherParams['isServiceCall'] = this.callType == '2' ? true : false
    otherParams['isSpecialProjectCall'] = this.callType == '3' ? true : false
    otherParams['isDealerCall'] = this.callType == '4' ? true : false
    otherParams['isInspectionCall'] = this.callType == '5' ? true : false
    otherParams['CustomerId'] = this.customerId
    otherParams['AddressId'] = this.customerAddressId
    let api = [
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_CALL_CREATION_PAYMENT).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION_QUESTIONS, null, false,
        prepareGetRequestHttpParams(null, null, otherParams)).pipe(map(result => result), catchError(error => of(error))),
    ]
    if (isPopupOpen) {
      api.push(this.crudService.get(ModulesBasedApiSuffix.SHARED, BillingModuleApiSuffixModels.SELF_HELP_TROUBLESHOOT_ENABLE, null, false,
        prepareRequiredHttpParams({ customerId: this.customerId, addressId: this.customerAddressId })).pipe(map(result => result), catchError(error => of(error))))
    }
    this.callCreationCreationSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      if (window.location.toString().includes('localhost')) {
        console.log(response);
      }
      response?.forEach((res: IApplicationResponse, ix: number) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          switch (ix) {
            case 0:
              this.paymentDropdown = res.resources;
              break;
            case 1:
              this.callCreationDialog = isPopupOpen;
              let callCreationModel = new CallCreationTemplateModel(res.resources);
              // if (response.resources.length == 0) {
              //   this.callCreationTemplateList = this.getcallCreationListArray;
              //   this.callCreationTemplateList.push(this.createcallCreationListModel(null));
              // } else {
              this.createCallCreationTemplateForm();
              this.callCreationTemplateList = this.getcallCreationListArray;
              res?.resources?.forEach((callCreationListModel: CallCreationTemplateListModel) => {
                callCreationListModel.callInitiationId = this.initiationId ? this.initiationId : ''
                callCreationListModel.customerId = this.customerId ? this.customerId : ''
                callCreationListModel.createdUserId = this.userData.userId ? this.userData.userId : ''
                this.callCreationTemplateList.push(this.createcallCreationTemplateListModel(callCreationListModel));
              });
              if (!this.initiationId && this.callCreationTempDetails) {
                this.getcallCreationListArray.patchValue(this.callCreationTempDetails);
              }
              break;
            case 2:
              this.showTroubleShootBtn = res?.resources;
              break;
          }
        }
      })
      this.onCloseLoader();
      //this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onShowTroubleshooting() {
    return this.showTroubleShootBtn;
  }


  changeStatus(data) {

  }

  openActualTimeDialog() {
    const actualTimeView = this.getActualTimingActionPermission(PermissionTypes.ADD);
    if (!actualTimeView) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let otherParams = {}
    if (!this.initiationId || !this.initiationDetailsData?.appointmentInfo?.appointmentId || !this.initiationDetailsData?.appointmentInfo?.scheduledStartDate) {
      return
    } else {
      otherParams['appointmentId'] = this.initiationDetailsData?.appointmentInfo?.appointmentId
    }
    // otherParams['isAll'] =
    // this.actualTimeDialog = true; //hide p-dialog
    if (!this.initiatonCallCreationForm.get('appointmentInfo.techArea').value || !this.initiatonCallCreationForm.get('appointmentInfo.technician').value) {
      this.snackbarService.openSnackbar("Actual timing can not be updated as technician has not scheduled to the job.", ResponseMessageTypes.WARNING);
      return;
    }
    // call for 2 apis
    let dropdownsAndData = [];
    dropdownsAndData = [
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.APPOINTMENTS_ACTUAL_TIMING_DATES, null, false,
        prepareGetRequestHttpParams(null, null, {
          appointmentId: this.initiationDetailsData?.appointmentInfo?.appointmentId ? this.initiationDetailsData?.appointmentInfo?.appointmentId : ''
        })).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.
        APPOINTMENTS_ACTUAL_TIMING, null, false,
        prepareGetRequestHttpParams(null, null, otherParams)).pipe(map(result => result), catchError(error => of(error))),
    ];
    // this.loadActionTypesForActualTimings(dropdownsAndData);
    this.OnLoadActualTime(dropdownsAndData);
  }

  // loadActionTypesForActualTimings(dropdownsAndData) {
  //   forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
  //     response.forEach((resp: IApplicationResponse, ix: number) => {
  //       if (resp.isSuccess && resp.statusCode === 200) {
  //         switch (ix) {
  //           case 0:
  //             this.actualTimingDateList = resp.resources;
  //             break;
  //           case 1:
  //             // this.getactualTimingListArray.clear();
  //             this.createActualTimingForm()
  //             let actualTimingModel = new ActualTimingModel(resp.resources);
  //             if (resp.resources.length == 0) {
  //               this.actualTimingList = this.getactualTimingListArray;
  //               let actualTimingListModel = new ActualTimingListModel(null);
  //               actualTimingListModel.appointmentId = this.initiationDetailsData?.appointmentInfo?.appointmentId ? this.initiationDetailsData?.appointmentInfo?.appointmentId : '';
  //               // actualTimingListModel.onRouteDate = this.initiationDetailsData.appointmentInfo.scheduledStartDate ? this.initiationDetailsData.appointmentInfo.scheduledStartDate : "";
  //               actualTimingListModel.onRouteDate = "";
  //               actualTimingListModel.createdUserId = this.userData.userId ? this.userData.userId : '';
  //               actualTimingListModel.modifiedUserId = this.userData.userId ? this.userData.userId : '';
  //               actualTimingListModel.appointmentLogId = ''; // added for appointmentLogId date
  //               this.actualTimingList.push(this.createactualTimingListModel(actualTimingListModel));;
  //             } else {
  //               this.actualTimingList = this.getactualTimingListArray;
  //               resp.resources.forEach((actualTimingListModel: ActualTimingListModel) => {
  //                 actualTimingListModel.appointmentId = this.initiationDetailsData?.appointmentInfo?.appointmentId ? this.initiationDetailsData?.appointmentInfo?.appointmentId : ''
  //                 // actualTimingListModel.startTime = actualTimingListModel.startTime ? this.momentService.setTime(actualTimingListModel.startTime) : null,
  //                 actualTimingListModel.startTime = actualTimingListModel.startTime ? this.momentService.toMoment(this.momentService.setTime(actualTimingListModel.startTime)).seconds(0).format() : null,
  //                   // actualTimingListModel.endTime = actualTimingListModel.endTime ? this.momentService.setTime(actualTimingListModel.endTime) : null,
  //                   actualTimingListModel.endTime = actualTimingListModel.endTime ? this.momentService.toMoment(this.momentService.setTime(actualTimingListModel.endTime)).seconds(0).format() : null,
  //                   actualTimingListModel.onRouteTime = actualTimingListModel.onRouteTime ? this.momentService.setTime(actualTimingListModel.onRouteTime) : null,
  //                   actualTimingListModel.createdUserId = this.userData.userId ? this.userData.userId : ''
  //                 actualTimingListModel.modifiedUserId = this.userData.userId ? this.userData.userId : '';
  //                 actualTimingListModel.appointmentLogId = actualTimingListModel.appointmentLogId ? actualTimingListModel.appointmentLogId : ''; // added for appointmentLogId date
  //                 actualTimingListModel.onRouteDate = actualTimingListModel.onRouteDate && actualTimingListModel.onRouteDate.split('T').length > 0 ? actualTimingListModel.onRouteDate.split('T')[0] : '';
  //                 if (actualTimingListModel.onRouteDate) {
  //                   let formateDate = actualTimingListModel.onRouteDate.split("-")
  //                   let onRouteDate = actualTimingListModel.onRouteDate ? formateDate[2] + '-' + formateDate[1] + '-' + formateDate[0] : null
  //                   actualTimingListModel.onRouteDate = onRouteDate ? new Date(onRouteDate) : ''
  //                 }
  //                 actualTimingListModel.startTimeMinDate = actualTimingListModel.onRouteTime ? this.momentService.toMoment(this.momentService.addMinits(actualTimingListModel.onRouteTime, 1)).seconds(0).format() : null;
  //                 actualTimingListModel.endTimeMinDate = actualTimingListModel.startTime ? this.momentService.toMoment(this.momentService.addMinits(actualTimingListModel.startTime, 1)).seconds(0).format() : null,
  //                   this.actualTimingList.push(this.createactualTimingListModel(actualTimingListModel));
  //               });
  //             }
  //             break;
  //         }
  //       }
  //     })
  //     this.rxjsService.setGlobalLoaderProperty(false);
  //   });
  // }

  OnLoadActualTime(dropdownsAndData) {
    this.rxjsService.setGlobalLoaderProperty(true);
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      if (window.location.toString().includes('localhost')) {
        console.log(response);
      }
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.actualTimingDateList = resp.resources;
              break;
            case 1:
              const ref = this.dialog.open(ActualTimeDialogComponent, {
                width: '1250px',
                data: {
                  header: 'Actual Timing',
                  actualTimingModelData: resp.resources,
                  userData: this.userData,
                  initiationDetailsData: this.initiationDetailsData,
                  actualTimingDateList: this.actualTimingDateList,
                  api: TechnicalMgntModuleApiSuffixModels.APPOINTMENTS_ACTUAL_TIMING,
                  isInvoiced: this.isInvoiced(),
                  isVoid: this.isVoid(),
                  addpermission: this.getActualTimingActionPermission(PermissionTypes.ADD),
                  deletepermission: this.getActualTimingActionPermission(PermissionTypes.DELETE),
                },
                disableClose: true,
              });
              ref.afterClosed().subscribe((res) => {
                if (res) {
                  this.getCallInitiationDetailsById();
                }
              });
              break;
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  isDisabledForExecuGuardNkaServiceCall() {
    if (this.initiationDetailsData?.basicInfo?.isExecuGuardCustomer && this.initiationDetailsData?.execuGuardServiceCall) {
      if (this.initiationDetailsData?.execuGuardServiceCall?.execuGuardServiceCallStatusName != 'Approved') {
        return true
      } else {
        return false
      }
    } else if (this.initiationDetailsData?.basicInfo?.isNKACustomer && this.initiationDetailsData?.nkaServiceCall) {
      if (!this.initiationDetailsData?.nkaServiceCall?.isPOAccepted) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  }

  checkExecuguardPR() {
    return new Promise((resolve, reject) => {
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.APPOINTMENTS_VALIDATION, this.initiationId)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            if (response.resources?.validateMessage) {
              this.snackbarService.openSnackbar(response.resources?.validateMessage, ResponseMessageTypes.WARNING);
              resolve(true)
            } else {
              resolve(false)
            }
          } else {
            resolve(false)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }, error => {
          resolve(false)
        });
    });
  }

  async navigateToTechAllocation() {
    if (this.isInvoiced()) {
      return
    }
    if (this.isVoid()) {
      return
    }
    if (this.initiatonCallCreationForm.invalid) {
      this.autoSubmitClick() // to show error validation
      return
    }
    if (this.initiationId) {
      let validate = await this.checkExecuguardPR()
      if (validate) {
        return
      }
    }
    if (this.initiationDetailsData?.basicInfo?.isExecuGuardCustomer && this.initiationDetailsData?.execuGuardServiceCall) {
      if (this.initiationDetailsData?.execuGuardServiceCall?.execuGuardServiceCallStatusName != 'Approved') {
        this.snackbarService.openSnackbar('Execuguard Request has not been approved. Unable to schedule a call', ResponseMessageTypes.WARNING);
        return;
      }
    }
    if (this.initiationDetailsData?.basicInfo?.isNKACustomer && this.initiationDetailsData?.nkaServiceCall) {
      if (!this.initiationDetailsData?.nkaServiceCall?.isPOAccepted) {
        this.snackbarService.openSnackbar('NKA PO Request has not been accepted. Unable to schedule a call', ResponseMessageTypes.WARNING);
        return;
      }
    }
    if (this.initiationDetailsData?.basicInfo?.isNKACustomer && this.userData?.userId == this.initiationDetailsData?.nkaServiceCall?.ownedUserId && this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() != 'po supplied') {
      this.snackbarService.openSnackbar('The customer must fill the po details', ResponseMessageTypes.WARNING);
      return;
    }
    if (!this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value || this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value == '00:00') {
      this.initiatonCallCreationForm.markAllAsTouched();
      this.snackbarService.openSnackbar('Estimated Time is required', ResponseMessageTypes.WARNING);
      return
    }
    if (!this.callCreationTemplate) {
      if (this.callCreationTemplateForm.invalid || this.callCreationTemplateForm?.getRawValue()?.callCreationList.length == 0) {
        this.snackbarService.openSnackbar("Call Creation Template is required", ResponseMessageTypes.WARNING);
        return false
      }
    }
    if (!this.initiationId) {
      if (this.initiationDetailsData?.basicInfo?.isExecuGuardCustomer) {
        this.snackbarService.openSnackbar('Execuguard Request has not been approved. Unable to schedule a call', ResponseMessageTypes.WARNING);
        return;
      }
      if (this.initiationDetailsData?.basicInfo?.isNKACustomer) {
        this.snackbarService.openSnackbar('NKA PO Request has not been accepted. Unable to schedule a call', ResponseMessageTypes.WARNING);
        return;
      }
      if (this.initiationDetailsData?.basicInfo?.isNKACustomer && this.userData?.userId == this.initiationDetailsData?.nkaServiceCall?.ownedUserId && this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() != 'po supplied') {
        this.snackbarService.openSnackbar('The customer must fill the po details', ResponseMessageTypes.WARNING);
        return;
      }
      this.autoSaveAlertNavigate(1)
      return
    } else {
      // if (this.momentService.isDateValid(this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value)) {
      //   let estimatedTime = this.momentService.toHoursMints24(this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value)
      //   this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(estimatedTime)
      // }
      // let estimatedTime = this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value
      if (this.initiatonCallCreationForm.touched || this.callCreationTemplateForm.touched) {
        this.onSubmit(1)
      } else {
        this.navigateToTechAllocationParams()
      }
    }
  }

  navigateToTechAllocationParams() {
    // const techAllocationView = this.getAppointmentActionPermission(PermissionTypes.VIEW);
    if (!this.findActionPermission(CUSTOMER_COMPONENT.APPOINTMENT_BOOKING)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.router.navigate(['/customer/tech-allocation-calender-view'], {
      queryParams: {
        nextAppointmentDate: this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').value ? this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').value : new Date(),
        installationId: this.initiationBasicInfo ? this.initiationBasicInfo.callInitiationId ? this.initiationBasicInfo.callInitiationId : this.initiationId : this.initiationId,
        customerId: this.initiationBasicInfo ? this.initiationBasicInfo.customerId ? this.initiationBasicInfo.customerId : this.customerId : this.customerId,
        customerAddressId: this.initiationBasicInfo ? this.initiationBasicInfo.addressId ? this.initiationBasicInfo.addressId : this.customerAddressId : this.customerAddressId,
        appointmentId: this.initiatonCallCreationForm.get('appointmentInfo.appointmentId').value ? this.initiatonCallCreationForm.get('appointmentInfo.appointmentId').value : null,
        appointmentLogId: this.initiatonCallCreationForm.get('appointmentInfo.appointmentLogId').value ? this.initiatonCallCreationForm.get('appointmentInfo.appointmentLogId').value : null,
        estimatedTime: this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value ? this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value : null,
        travelTime: this.initiatonCallCreationForm.get('appointmentInfo.travelTime').value ? this.momentService.toFormateType(this.initiatonCallCreationForm.get('appointmentInfo.travelTime').value, 'HH:mm') : null,
        callType: this.callType ? this.callType : null  // 1- installation call, 2- service call
      }
    });
  }

  /* --- Open Call Escalation Dialog ---- */
  openCallEscalationDialog() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.ESCALAATION)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.initiationDetailsData?.basicInfo?.callInitiationId) {
      return;
    }
    // if (!this.initiationDetailsData?.appointmentInfo?.appointmentLogId) {
    //   return;
    // }
    if (this.callEscalationsubscritption && !this.callEscalationsubscritption.closed) {
      this.callEscalationsubscritption.unsubscribe();
    }
    this.callEscalationsubscritption = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.QUICK_CALL_ESCALATION, null, false, prepareRequiredHttpParams({ CallInitiationId: this.initiationId })).subscribe((res: any) => {
        if (res?.isSuccess == true && res?.statusCode == 200) {
          const escalationEditPermission = this.getQuickActionPermission(CUSTOMER_COMPONENT.ESCALAATION)?.subMenu?.find(el => el?.menuName == PermissionTypes.EDIT) ? true : false;
          const ref = this.dialogService.open(CallEscalationDialogComponent, {
            header: 'Escalation',
            baseZIndex: 1000,
            width: '400px',
            // closable: false,
            showHeader: false,
            data: {
              header: 'Escalation',
              ...res?.resources,
              createdUserId: this.userData?.userId,
              isServiceCallEscalation: false,
              editPermission: escalationEditPermission,
              // isSubmitButtonEnable: this.initiationBasicInfo?.isVoid ? this.initiationBasicInfo?.isVoid : this.initiationBasicInfo?.isOnHold  // for void status -only can view the data
              // isSubmitButtonEnable: this.initiationBasicInfo?.isVoid  // for void status -only can view the data
              isSubmitButtonEnable: this.initiationBasicInfo?.isVoid ? this.initiationBasicInfo?.isVoid : this.initiationBasicInfo?.isInvoiced,
            },
          });
          ref.onClose.subscribe((res) => {
            if (res) {
              this.onReloadPageAfterCloseDialog();
            }
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  /* --- Close Call Escalation Dialog ---- */

  /* --- Open Rebook Dialog ---- */
  openCloseRebookDialog(val?) {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.REBOOK)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let isNew = this.initiatonCallCreationForm.get("isNew").value
    if (this.initiationBasicInfo?.isLastRecountReached) {
      this.snackbarService.openSnackbar("Rebook count is reached final count...", ResponseMessageTypes.WARNING);
      return;
    } else if (!this.initiationAppointmentInfo?.scheduledStartDate) {
      this.snackbarService.openSnackbar("Job is not scheduled", ResponseMessageTypes.WARNING);
      return;
    } else if (!this.initiationAppointmentInfo?.arrivedTime) { // Action timing popup start time if not exists show the msg
      this.snackbarService.openSnackbar("Appointment start time has not been recorded", ResponseMessageTypes.WARNING);
      return;
      // } else if (this.initiationBasicInfo?.isLastRecountReached == false && !isNew && this.initiationAppointmentInfo?.actualTime) {
    } else {
      this.isRebookDialog = val;
      this.rxjsService.setDialogOpenProperty(val);
      this.rebookDialogForm.reset();
      this.rebookDialogForm.get('rebookReason')?.setValue('');
      this.isRebookSubmit = false;
      this.isRebookAlertDialog = false;
      if (this.initiationBasicInfo?.isVoid || this.initiationBasicInfo?.isInvoiced || this.initiationBasicInfo?.isTempDone) {
        this.rebookDialogForm.get('rebookReason')?.disable()
        return;
      }
      this.onLoadRebookReasonList();
    }
  }

  initRebookDialogForm() {
    this.rebookDialogForm = new FormGroup({
      rebookReason: new FormControl(''),
      rebookComments: new FormControl()
    })
    this.rebookDialogForm = setRequiredValidator(this.rebookDialogForm, ["rebookReason"]);
    this.rebookDialogForm.controls?.rebookReason?.valueChanges.subscribe((val: any) => {
      if (val) {
        const rebookReasonName = this.rebookReasonList.find(el => el?.id == val);
        if (rebookReasonName?.displayName?.toLowerCase() == "other") {
          this.rebookDialogForm?.controls?.rebookComments?.setValidators([Validators.required]);
          this.rebookDialogForm?.controls?.rebookComments?.updateValueAndValidity();
          this.rebookDialogForm?.controls?.rebookComments?.setValue("");
          this.showRebookComments = true;
        } else if (rebookReasonName?.displayName?.toLowerCase() !== "other") {
          this.rebookDialogForm = clearFormControlValidators(this.rebookDialogForm, ["rebookComments"]);
          this.rebookDialogForm?.controls?.rebookComments?.setValue(val?.displayName);
          this.showRebookComments = false;
        }
      }
    })
  }

  onLoadRebookReasonList() {
    // this.rxjsService.setFormChangeDetectionProperty(true);
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.REBOOK_REASON_LIST).subscribe((res: any) => {
        if (res?.isSuccess == true && res?.statusCode == 200) {
          this.rebookReasonList = res?.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSubmitRebookDialog() {
    const rebookEditPermission = this.getQuickActionPermission(CUSTOMER_COMPONENT.REBOOK)?.subMenu?.find(el => el?.menuName == PermissionTypes.EDIT) ? true : false;
    if (!rebookEditPermission) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.rebookDialogForm.invalid) {
      this.rebookDialogForm.markAllAsTouched();
      return;
    }
    if (this.initiationBasicInfo?.isVoid || this.initiationBasicInfo?.isInvoiced || this.initiationBasicInfo?.isTempDone) {
      return;
    }
    var rebookDialogObj = {
      callInitiationId: this.initiationBasicInfo?.callInitiationId,
      callInitiationRebookReasonId: this.rebookDialogForm.value?.rebookReason,
      Reason: this.rebookDialogForm.value?.rebookComments ? this.rebookDialogForm.value?.rebookComments : null,
      createdUserId: this.userData?.userId
    }
    if (this.rebookDialogForm.valid && this.userData?.userId && this.initiationBasicInfo?.callInitiationId) {
      this.isRebookSubmit = true;
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.REBOOK_REASON, rebookDialogObj).subscribe((res: any) => {
          if (res?.isSuccess == true && res?.statusCode == 200) {
            if (JSON.parse(res?.resources)?.AlertMessage) {
              this.rebookalertMesssage = JSON.parse(res?.resources)?.AlertMessage;
              this.isRebookAlertDialog = true;
            } else {
              this.openCloseRebookDialog(false);
            }
            this.getCallInitiationDetailsById();
          } else {
            this.openCloseRebookDialog(false);
          }
        })
    } else {
      return;
    }
  }
  /* --- Close Rebook Dialog ---- */

  /* --- Open Rebook Confirm Dialog ---- */
  btnRebookNoClick() {
    if (this.callType == '2') { // service call
      var rebookConfirmDialogObj = {
        callInitiationId: this.initiationBasicInfo?.callInitiationId,
        createdUserId: this.userData?.userId
      }
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.REBOOK_NOCHARGE, rebookConfirmDialogObj).subscribe((res: any) => {
          if (res?.isSuccess == true && res?.statusCode == 200) {
            this.cancelClick();
          }
          this.openCloseRebookDialog(false);
        });
    } else {
      this.openCloseRebookDialog(false);
      this.getCallInitiationDetailsById();
    }
  }
  /* --- Close Rebook Confirm Dialog ---- */

  // navigateToPage() {
  //   this.router.navigate(['/customer', 'manage-customers', 'bill-of-material'], {
  //     queryParams: {
  //       installationId: this.initiationId,
  //       // licenseTypeId: this.licenseTypeId
  //     }
  //   });
  // }

  navigateToPage() {
    // const bomView = this.getQuickActionPermission(CUSTOMER_COMPONENT.BILL_OF_MATERIALS)?.subMenu?.find(el => el?.menuName == PermissionTypes.VIEW);
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.BILL_OF_MATERIALS)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.initiationDetailsData?.basicInfo?.callInitiationId) {
      return;
    }
    if (!this.initiationAppointmentInfo?.quotationVersionId && this.callType == 1) {
      return;
    }
    this.router.navigate(['/customer', 'manage-customers', 'bill-of-material'], {
      queryParams: {
        installationId: this.initiationId,
        customerId: this.initiationBasicInfo?.customerId,
        customerAddressId: this.initiationBasicInfo?.addressId,
        quotationVersionId: this.initiationAppointmentInfo?.quotationVersionId,
        callInitiationId: this.initiationBasicInfo?.callInitiationId,
        // isVoid: this.initiationBasicInfo?.isVoid ? this.initiationBasicInfo?.isVoid : this.initiationBasicInfo?.isOnHold ? this.initiationBasicInfo?.isOnHold : this.initiationBasicInfo?.isInvoiced,
        isVoid: this.initiationBasicInfo?.isVoid ? this.initiationBasicInfo?.isVoid : this.initiationBasicInfo?.isInvoiced,
        callType: this.callType,
        appointmentId: this.initiationDetailsData?.appointmentInfo?.appointmentId,
        isUpselling: this.isUpselling,
        isNKACustomer: this.initiationDetailsData?.basicInfo?.isNKACustomer,
        isFeedback: this.initiationDetailsData?.basicInfo?.isFeedback,
        appointmentLogId: this.initiatonCallCreationForm.get('appointmentInfo.appointmentLogId').value ? this.initiatonCallCreationForm.get('appointmentInfo.appointmentLogId').value : null
      }, skipLocationChange: false
    });
  }

  navigateToTechTransfer() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.VIEW_TRANSFERS)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.initiationDetailsData?.basicInfo?.callInitiationId) {
      return;
    }
    this.router.navigate(['/customer', 'manage-customers', 'tech-transfer'], {
      queryParams: {
        // installationId: this.initiationId,
        customerId: this.initiationBasicInfo?.customerId,
        addressId: this.initiationBasicInfo?.addressId,
        // quotationVersionId: this.initiationAppointmentInfo?.quotationVersionId,
        callInitiationId: this.initiationBasicInfo?.callInitiationId,
        callType: this.callType
      }, skipLocationChange: true
    });
  }

  navigateToQuotationTransfer() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.QUOTE)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    if (!this.initiationDetailsData?.basicInfo?.callInitiationId) {
      return;
    }
    if (this.isInvoiced()) {
      return
    }
    this.router.navigate(['/customer', 'manage-customers', 'quotation-items'], {
      queryParams: {
        customerId: this.initiationBasicInfo?.customerId,
        customerAddressId: this.customerAddressId,
        initiationId: this.initiationId,
        callType: this.callType,
        quotationVersionId: this.initiationAppointmentInfo?.quotationVersionId,
      }, skipLocationChange: true
    });
  }

  navigateToSiteHazard() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.SITE_HAZARD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.initiationDetailsData?.basicInfo?.callInitiationId) {
      return;
    }
    this.router.navigate(['/customer', 'manage-customers', 'site-hazard'], {
      queryParams: {
        customerId: this.initiationBasicInfo.customerId,
        customerAddressId: this.initiationBasicInfo.addressId,
        AppointmentId: this.initiationAppointmentInfo?.appointmentId,
        installationId: this.initiationId,
        callType: this.callType
      }, skipLocationChange: true
    });
  }

  navigateToLeadhandOverCertificate() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.LEAD_HAND_OVER_CERTIFICATE)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.initiationDetailsData?.basicInfo?.callInitiationId) {
      return;
    }
    if (!this.initiationBasicInfo?.leadId) {
      return this.snackbarService.openSnackbar("Lead Id must not be empty", ResponseMessageTypes.WARNING);
    }
    if (!this.initiationBasicInfo?.saleOrderId) {
      return this.snackbarService.openSnackbar("Sales Order Id must not be empty", ResponseMessageTypes.WARNING);
    }
    if (!this.initiationAppointmentInfo?.quotationVersionId) {
      return this.snackbarService.openSnackbar("Quotation Version Id must not be empty", ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/customer', 'manage-customers', 'lead-hand-over-certificate'], {
      queryParams: {
        customerName: this.initiationBasicInfo?.customerName,
        customerRefNo: this.initiationBasicInfo?.customerRefNo,
        debtorCode: this.initiationBasicInfo?.debtorCode,
        systemTypeId: this.initiationServiceInfo?.systemTypeId,
        customerId: this.initiationBasicInfo?.customerId,
        customerAddressId: this.initiationBasicInfo?.addressId,
        SaleOrderId: this.initiationBasicInfo?.saleOrderId,
        LeadId: this.initiationBasicInfo?.leadId,
        customerAddress: this.initiationBasicInfo?.customerAddress,
        QutationVersionId: this.initiationAppointmentInfo?.quotationVersionId,
        AppointmentId: this.initiationAppointmentInfo?.appointmentId,
        installationId: this.initiationId,
        callType: this.callType
      }, skipLocationChange: true
    });
  }

  openTechnicalAssistant() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.TECHNICAL_ASSISTANT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.initiationAppointmentInfo?.appointmentId) {
      return this.snackbarService.openSnackbar('Appointment Id must not be empty', ResponseMessageTypes.WARNING);
    }
    if (!this.initiationAppointmentInfo?.appointmentLogId) {
      return this.snackbarService.openSnackbar('Appointment Log Id must not be empty', ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setDialogOpenProperty(true);
    const techAssistantEditPermission = this.getQuickActionPermission(CUSTOMER_COMPONENT.TECHNICAL_ASSISTANT)?.subMenu?.find(el => el?.menuName == PermissionTypes.EDIT) ? true : false;
    this.technAssistDialog = this.dialogService.open(TechnicalAssistantComponent, {
      header: 'Technical Assistant',
      baseZIndex: 1000,
      width: '550px',
      closable: false,
      showHeader: false,
      data: {
        AppointmentId: this.initiationAppointmentInfo?.appointmentId,
        AppointmentLogId: this.initiationAppointmentInfo?.appointmentLogId,
        // isVoid: this.initiationBasicInfo?.isVoid
        editPermission: techAssistantEditPermission,
        isVoid: this.initiationBasicInfo?.isVoid ? this.initiationBasicInfo?.isVoid : this.initiationBasicInfo?.isInvoiced,
      },
    });
    this.technAssistDialog.onClose.subscribe((res) => {
      if (res) {
        this.onReloadPageAfterCloseDialog();
      }
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  openInspectionCallBackDialog() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.CALLBACK)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.initiationId || this.initiationBasicInfo?.isInvoiced) {
      return;
    }
    const callbackEditPermission = this.getQuickActionPermission(CUSTOMER_COMPONENT.CALLBACK)?.subMenu?.find(el => el?.menuName == PermissionTypes.EDIT) ? true : false;
    const ref = this.dialog.open(InspectionCallBackDialogComponent, {
      width: '550px',
      data: {
        customerId: this.customerId,
        // customerInspectionId: this.customerInspectionId
        referenceId: this.initiationId,
        header: 'Call Back',
        editPermission: callbackEditPermission,
      },
      disableClose: true,
    });
    // ref.onClose.subscribe((result) =>{
    ref.afterClosed().subscribe((result) => {
      if (result) {
        this.onReloadPageAfterCloseDialog();
      }
    });
  }

  onReloadPageAfterCloseDialog() {
    if (this.initiatonCallCreationForm?.touched) {
      this.onSubmit();
    } else {
      this.getCallInitiationDetailsById();
    }
  }

  navigateCustomerView() {
    this.rxjsService.setViewCustomerData({
      customerId: this.initiationBasicInfo?.customerId,
      addressId: this.customerAddressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  isAddressChecked(val) {
    this.initiatonCallCreationForm.get('isAddressVerified').setValue(val.checked ? true : null)
  }

  onSubmit(bol?: Number | String) {
    if (this.getEditPermission()) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    // if (!this.isTechnicalAreaMngr()) {
    this.rxjsService.getFormChangeDetectionProperty().subscribe(val => {
    })
    const debtorDetails = this.initiatonCallCreationForm.get('debtor').value;
    this.initiatonCallCreationForm.get('debtorAdditionalInfoId').setValue(debtorDetails?.debtorAdditionalInfoId);
    this.initiatonCallCreationForm.get('debtorId').setValue(debtorDetails?.debtorId);
    if (this.initiatonCallCreationForm.invalid) {
      // if (this.initiatonCallCreationForm.invalid || !this.initiatonCallCreationForm?.get('isAddressVerified')?.value) {
      this.initiatonCallCreationForm.markAllAsTouched();
      return false
    }
    if (!this.callCreationTemplate) {
      if (this.callCreationTemplateForm.invalid || this.callCreationTemplateForm?.getRawValue()?.callCreationList.length == 0) {
        this.snackbarService.openSnackbar("Call Creation Template is required", ResponseMessageTypes.WARNING);
        return false
      }
    }
    // }
    if (this.initiationDetailsData?.basicInfo?.isNKACustomer && this.userData?.userId == this.initiationDetailsData?.nkaServiceCall?.ownedUserId) {
      if ((!this.initiatonCallCreationForm.get('nkaServiceCall.poNumber').disabled && !this.initiatonCallCreationForm.get('nkaServiceCall.poNumber').value)
        || (!this.initiatonCallCreationForm.get('nkaServiceCall.poAmount').disabled && !this.initiatonCallCreationForm.get('nkaServiceCall.poAmount').value)) {
        this.onPORequired();
        // this.snackbarService.openSnackbar("Please provide all PO details", ResponseMessageTypes.WARNING);
        return;
      }
      if (!this.initiatonCallCreationForm.get('nkaServiceCall.poComment')?.value && this.initiatonCallCreationForm.get('nkaServiceCall.isPONotAccepted')?.value) {
        this.initiatonCallCreationForm.get('nkaServiceCall.poComment')?.markAsTouched();
        return;
      }
    }
    let Obj = this.initiatonCallCreationForm?.getRawValue();
    // Obj.appointmentInfo.isNextAvailableAppointment = Obj.appointmentInfo.nextAvailableAppointmentCheckbox ? Obj.appointmentInfo.nextAvailableAppointmentCheckbox : false
    // if (this.momentService.isDateValid(Obj.appointmentInfo.estimatedTime)) {
    //   Obj.appointmentInfo.estimatedTime = this.momentService.toHoursMints24(Obj.appointmentInfo.estimatedTime)
    // }
    Obj.appointmentInfo.nextAvailableAppointment = Obj.appointmentInfo.nextAvailableAppointment ? this.datePipe.transform(Obj.appointmentInfo.nextAvailableAppointment, 'yyyy-MM-dd h:mm:ss') : null
    Obj.appointmentInfo.scheduledStartDate = Obj.appointmentInfo.scheduledStartDate ? this.datePipe.transform(Obj.appointmentInfo.scheduledStartDate, 'yyyy-MM-dd h:mm:ss') : null
    Obj.appointmentInfo.scheduledEndDate = Obj.appointmentInfo.scheduledEndDate ? this.datePipe.transform(Obj.appointmentInfo.scheduledEndDate, 'yyyy-MM-dd h:mm:ss') : null

    let saveObj = {
      "createdUserId": this.userData.userId,
      "appointmentId": this.initiationBasicInfo.appointmentId,
      "basicInfo": {
        "callInitiationId": this.initiationBasicInfo.callInitiationId ? this.initiationBasicInfo.callInitiationId : this.initiationId,
        "customerId": this.initiationBasicInfo.customerId,
        "quotationNumber": this.initiationBasicInfo.quotationNumber,
        "isNew": Obj.isNew,
        "isScheduled": Obj.isScheduled,
        "isTempDone": Obj.isTempDone,
        "isInvoiced": Obj.isInvoiced,
        "isOnHold": Obj.isOnHold,
        "isVoid": Obj.isVoid,
        "isReferToSales": Obj.isReferToSales,
        "isInstallPreCheck": Obj.isInstallPreCheck,
        "isNKACustomer": this.initiationDetailsData?.basicInfo?.isNKACustomer,
        "isExecuGuardCustomer": this.initiationDetailsData?.basicInfo?.isExecuGuardCustomer,
        "isInWarrenty": Obj.isInWarrenty,
        "isRecall": Obj.isRecall,
        "isCode50": Obj.isCode50,
        "isPRCall": Obj.isPRCall,
        "isSaveOffer": Obj.isSaveOffer,
        "isCon": Obj.isCon,
        "isAddressVerified": Obj.isAddressVerified ? true : null,
        "contact": Obj.contact,
        "contactNumber": Obj.contactNumber,
        "alternateContact": Obj.alternateContact,
        "alternateContactNumber": Obj.alternateContactNumber,
        "Priorityid": Obj.priorityid,
        "panelTypeConfigId": Obj.panelTypeConfigId ? Obj.panelTypeConfigId.value : null,
        "specialProjectId": Obj.specialProjectId,
        "partitionIds": [],
        "techInstruction": Obj.techInstruction,
        "notes": Obj.notes,
        "isInstallationCall": this.callType == '1' ? true : false,
        "isServiceCall": this.callType == '2' ? true : false,
        "isSpecialProjectCall": this.callType == '3' ? true : false,
        "isDealerCall": this.callType == '4' ? true : false,
        "isInspectionCall": this.callType == '5' ? true : false,
        "addressId": this.customerAddressId ? this.customerAddressId : this.initiationBasicInfo.addressId,
        "recallCount": this.initiationDetailsData?.basicInfo?.recallCount,
        "warrantyRecallReferenceId": this.initiationBasicInfo?.warrantyRecallReferenceId,
        "warrantyServiceCallNo": this.initiationDetailsData?.basicInfo?.warrantyServiceCallNo,
        "debtorId": Obj.debtorId,
        "debtorAdditionalInfoId": Obj.debtorAdditionalInfoId ? Obj.debtorAdditionalInfoId : null,
        "travelTime": Obj?.appointmentInfo?.travelTime ? this.momentService.toFormateType(Obj?.appointmentInfo?.travelTime, 'HH:mm') : null,
      },
      "serviceInfo": Obj.serviceInfo,
      "appointmentInfo": Obj.appointmentInfo,
      "callInitiationContacts": this.callInitiationContacts,
      // "callInitiationQuestions": !Obj.isInstallPreCheck ? this.callCreationTemplateForm.value.callCreationList : null
    }
    if (Obj.appointmentInfo.diagnosisId) {
      let diagnossIds = []
      Obj.appointmentInfo.diagnosisId?.forEach(element => {
        diagnossIds.push(element.id)
      });
      saveObj['diagnosis'] = diagnossIds;
      delete saveObj?.appointmentInfo?.diagnosisId;
    }
    if (!Obj.isInstallPreCheck) {
      saveObj['callInitiationQuestions'] = this.callCreationTemplateForm?.getRawValue()?.callCreationList;
    }
    saveObj['IsInstallPreCheckUpdate'] = Obj.isInstallPreCheck;
    delete saveObj?.appointmentInfo?.travelTime;
    if (Obj.installationPartitions) {
      Obj.installationPartitions.forEach((element) => {
        if (element.mainPartition == true) {
          saveObj.basicInfo.partitionIds.push(element.partitionId)
        }
      })
    }
    // saveObj.serviceInfo.diagnosisId = Obj.appointmentInfo.diagnosisId;
    if (saveObj?.appointmentInfo?.wireman?.toString()) {
      saveObj.serviceInfo.wireman = Obj.appointmentInfo.wireman;
      delete saveObj?.appointmentInfo?.wireman;
    }
    // this.initiatonCallCreationForm.value.ismainPartation == true ? saveObj.basicInfo.partitionIds.push(this.initiationBasicInfo.installationPartitions.find(x => x.partitionCode == this.initiatonCallCreationForm.value.mainPartationCustomerId).partitionId) : saveObj.basicInfo.partitionIds;
    if (this.initiatonCallCreationForm.value?.ismainPartation) {
      if (this.initiationBasicInfo?.installationPartitions) {
        saveObj?.basicInfo?.partitionIds?.push(this.initiationBasicInfo.installationPartitions.find(x => x.partitionCode == this.initiatonCallCreationForm.value.mainPartationCustomerId.split(" ")[0]).partitionId)
      } else {
        saveObj.basicInfo.partitionIds
      }
    } else {
      saveObj.basicInfo.partitionIds
    }
    if (this.initiationDetailsData?.basicInfo?.isNKACustomer) {
      saveObj['NKAServiceCall'] = {
        NKAServiceCallId: this.initiationDetailsData?.nkaServiceCall ? this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallId : null,
        CallInitiationId: this.initiationBasicInfo?.callInitiationId ? this.initiationBasicInfo?.callInitiationId : this.initiationId ? this.initiationId : null,
        PONumber: this.initiatonCallCreationForm.get('nkaServiceCall.poNumber').value,
        POAmount: this.initiatonCallCreationForm.get('nkaServiceCall.poAmount').value ? this.otherService.transformCurrToNum(this.initiatonCallCreationForm.get('nkaServiceCall.poAmount').value) : null,
        POComment: this.initiatonCallCreationForm.get('nkaServiceCall.poComment').value,
        IsPOAccepted: this.initiatonCallCreationForm.get('nkaServiceCall.isPOAccepted').value ? true : this.initiatonCallCreationForm.get('nkaServiceCall.isPONotAccepted').value ? false : null,
      }
    }
    if (this.initiationDetailsData?.basicInfo?.isExecuGuardCustomer) {
      saveObj['ExecuGuardServiceCall'] = {
        ExecuGuardServiceCallId: this.initiationDetailsData?.execuGuardServiceCall ? this.initiationDetailsData?.execuGuardServiceCall?.execuGuardServiceCallId : null,
        CallInitiationId: this.initiationBasicInfo?.callInitiationId ? this.initiationBasicInfo?.callInitiationId : this.initiationId,
      }
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.callType == '1'
      ? this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSTALLATION, saveObj)
      : this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION, saveObj)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.initiatonCallCreationForm.markAsUntouched()       // this.navigateToPage();
        // this.navigateCustomerView()
        if (response.resources) {
          this.initiationId = response.resources
          if (bol == 1) {
            this.isNavigationFirstTime = false
            this.navigateToTechAllocationParams()
          } else if (bol == 2) {
            let troubleShootObject = {
              customerId: this.customerId,
              initiationId: this.initiationId,
              customerAddressId: this.customerAddressId,
              callType: this.callType
            }
            this.rxjsService.setTroubleShootAlarm(troubleShootObject);
            this.isNavigationFirstTime = false;
            this.onRedirectTroubleShoot();
          }
        }
        this.initiatonCallCreationForm.get('isAddressVerified').markAsUntouched();
        this.isManualChange = false
        this.getNextavailableAppointment()
        this.getCallInitiationDetailsById();
        this.openCallCreationDialog(false)
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onPRCallCreate() {
    const permissionType = (this.requestType?.toLowerCase() == RequestType.PR_CALL_REQUEST) ? CUSTOMER_COMPONENT.PR_CALL : CUSTOMER_COMPONENT.SAVE_OFFER;
    const rebookEditPermission = this.getQuickActionPermission(permissionType)?.subMenu?.find(el => el?.menuName == PermissionTypes.EDIT) ? true : false;
    if (!rebookEditPermission) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.prCallForm.invalid) {
      return
    }
    // let formValue = this.prCallForm.value
    // formValue.serviceCallRequestId = this.initiationDetailsData.prCall? this.initiationDetailsData.prCall.serviceCallRequestId ? this.initiationDetailsData.prCall.serviceCallRequestId : null : null
    this.rxjsService.setDialogOpenProperty(true);
    let crudService = (this.requestType?.toLowerCase() == RequestType.PR_CALL_REQUEST) ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.PR_CALL, this.prCallForm.value)
      : this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SAVE_OFFER, this.prCallForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      // if (response.isSuccess == true && response.statusCode == 200) {
      //   this.prCallDialog = false;
      //   this.getCallInitiationDetailsById();
      //   this.rxjsService.setDialogOpenProperty(false);
      // }
      if (response?.isSuccess && response?.statusCode == 200) {
        this.prCallDialog = false;
        this.alertDialog = true
        this.alertMsg = response.resources
        this.getCallInitiationDetailsById();
        this.rxjsService.setDialogOpenProperty(false);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  cancelClick() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.customerAddressId,
      customerTab: 3,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  onSendJobCardEmail() {
    if (!this.findActionPermission(PermissionTypes.EMAIL)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let data = {
      callInitiationId: this.initiationDetailsData?.basicInfo?.callInitiationId,
      createdUserId: this.userData.userId
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.THIRD_PARTY_SUBCONTRACTOR_JOBCARD, data)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onPrintJobCard() {
    if (!this.findActionPermission(PermissionTypes.PRINT)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let data = {
      callInitiationId: this.initiationDetailsData?.basicInfo?.callInitiationId,
      createdUserId: this.userData.userId
    }
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_JOBCARD, data)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200 && response.resources) {
          this.jobCardUrl = this.sanitizer.bypassSecurityTrustResourceUrl(response.resources + '#zoom=FitH')
          var link = document.createElement("a");
          if (link.download !== undefined) {
            link.setAttribute("href", response.resources);
            link.setAttribute("download", response.resources);
            document.body.appendChild(link);
            // link.click();
            this.IsjobCardUrl = true
            document.body.removeChild(link);
          }
          //   import("file-saver").then(FileSaver => {
          //   FileSaver.saveAs(response.resources, this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments').value);
          // });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        // var popup:any = window.open(response.resources, "_blank", "width=200, height=200") ;
        //     popup.location = URL;
        // }
      });
  }

  uploadPOFiles(file) {
    if (this.getEditPermission()) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.fileList = [];
    if (file && file.length == 0)
      return;
    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }
    const supportedExtensions = [
      'jpeg',
      'jpg',
      'png',
      'gif',
      'pdf',
      'doc',
      'docx',
      'xls',
      'xlsx',
    ];
    for (let i = 0; i < file.length; i++) {
      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension)) {
        let filename = this.fileList.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push(selectedFile.name);
          this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments')?.setValue(selectedFile.name);
        }
      } else {
        this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
      }
    }
    this.onAddPOFiles()
  }

  onAddPOFiles(): void {
    if (this.fileList.length == 0) {
      return
    }
    let data = {
      CallInitiationId: this.initiationDetailsData?.basicInfo?.callInitiationId,
      CreatedUserId: this.userData.userId,
      CallInitiationDocumentType: 'NKA PO',
    }
    let formData = new FormData();
    formData.append('callInitiationDocumentDTO', JSON.stringify(data));
    this.fileList.forEach(file => {
      formData.append('file', file);
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_DOCUMENTS, formData, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          // this.getCallInitiationDetailsById();
          const currentFileUrl = response?.resources?.find(el => el?.docName == this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments').value);
          this.initiatonCallCreationForm.get('nkaServiceCall.attachmentUrl').setValue(currentFileUrl?.path);
          if (this.isPOInfoDisabled()) {
            this.initiatonCallCreationForm.get('nkaServiceCall.poNumber').enable();
            this.initiatonCallCreationForm.get('nkaServiceCall.poAmount').enable();
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onOpenAttachment() {
    if (this.initiatonCallCreationForm.get('nkaServiceCall.attachmentUrl').value) {
      var link = document.createElement("a");
      if (link.download !== undefined) {
        link.setAttribute("href", this.initiatonCallCreationForm.get('nkaServiceCall.attachmentUrl').value);
        link.setAttribute("download", this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments').value);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
      // import("file-saver").then(FileSaver => {
      //   FileSaver.saveAs(this.initiatonCallCreationForm.get('nkaServiceCall.attachmentUrl').value, this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments').value);
      // });
    }
  }

  isPOInfoDisabled() {
    return this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments')?.value
      && this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po required'
      && this.initiationDetailsData?.basicInfo?.isNKACustomer && this.initiationDetailsData?.nkaServiceCall?.ownedUserId == this.userData?.userId;
  }

  isPOAcceptDisabled() {
    return (this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po supplied' || this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po rejected') && this.initiationDetailsData?.nkaServiceCall?.isPOAccepted == null;
    // return this.initiationDetailsData?.nkaServiceCall?.isPOAccepted == null;
  }

  onNKAReSubmit(): void {
    const message = `Po is not accepted. Are you re-submitting the request?`;
    const dialogData = new ConfirmDialogModel("NKA Request", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      width: "500px",
      data: {
        ...dialogData,
        isClose: true,
        msgCenter: true,
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if ((this.initiationDetailsData?.nkaServiceCall?.isPOAccepted == false || this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po declined') && dialogResult) {
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.NKA_SERVICE_CALL_RESUBMIT,
          { NKAServiceCallId: this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallId, CreatedUserId: this.userData?.userId },
        ).subscribe((response: IApplicationResponse) => {
          if (response?.isSuccess && response?.statusCode == 200) {
            this.getCallInitiationDetailsById();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onExecuReSubmit(): void {
    const message = `Execuguard request has been declined. Please resubmit.`;
    const dialogData = new ConfirmDialogModel("Execuguard Request Resubmit", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      width: "500px",
      data: {
        ...dialogData,
        isClose: true,
        msgCenter: true,
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.initiationDetailsData?.execuGuardServiceCall?.execuGuardServiceCallStatusName == 'Declined' && dialogResult) {
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.EXEC_SERVICE_CALL_DECLINE,
          { ExecuGuardServiceCallId: this.initiationDetailsData?.execuGuardServiceCall?.execuGuardServiceCallId, CreatedUserId: this.userData?.userId },
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getCallInitiationDetailsById();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onPORequired(): void {
    const message = `Please provide all PO details`;
    const dialogData = new ConfirmDialogModel("NKA Request", message);
    if (this.dialog.openDialogs.length) return;
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      width: "500px",
      data: {
        ...dialogData,
        isClose: true,
        msgCenter: true,
        isAlert: true,
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() != 'po supplied' && this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() != 'po rejected') {
        this.initiatonCallCreationForm.get('nkaServiceCall.isPOAccepted').setValue(null);
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onChangePartitions(event) {
    if (this.initiatonCallCreationForm.value.installationPartitions.length > 0) {
      let count = this.initiatonCallCreationForm.value.installationPartitions.filter(x => x.mainPartition == true)
      if (count.length > 0) {
        let estimatedSplt = this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').value.split(':')
        let hours = Number(estimatedSplt[0]) * count.length;
        let minuts = Number(estimatedSplt[1]) * count.length;
        let emptyTimeDate = this.momentService.setTime('0:0');
        let addHoursMints = this.momentService.addHoursMinits(emptyTimeDate, hours, minuts);
        let convetRailwayTime = this.momentService.convertNormalToRailayTime(new Date(addHoursMints));
        let finaltestimateSplt = convetRailwayTime.split(':');
        if (typeof finaltestimateSplt[0] == 'number' && typeof finaltestimateSplt[1] == 'number') {
          this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(finaltestimateSplt[0] + ':' + finaltestimateSplt[1])
        }
      }
    }
  }

  openRecurringAppointmentDialog() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.RECURRING_APPOINTMENTS)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.initiationDetailsData?.basicInfo?.callInitiationId) {
      return;
    }
    const editPermission = this.getQuickActionPermission(CUSTOMER_COMPONENT.RECURRING_APPOINTMENTS)?.subMenu?.find(el => el?.menuName == PermissionTypes.EDIT);
    this.recurringAppointementDialog = this.dialogService.open(RecurringAppointmentComponent, {
      header: 'Recurring Schedule',
      baseZIndex: 1000,
      width: '950px',
      closable: true,
      showHeader: true,
      data: {
        installationId: this.initiationId,
        createdUserId: this.userData?.userId,
        editPermission: editPermission,
        // showSubmitBtn: (this.initiationBasicInfo?.isVoid || this.initiationBasicInfo?.isOnHold) ? false : true // for void status - can view the page
        // showSubmitBtn: (this.initiationBasicInfo?.isVoid) ? false : true // for void status - can view the page
        showSubmitBtn: (this.initiationBasicInfo?.isVoid || this.initiationBasicInfo?.isInvoiced) ? false : true // for void/invoiced status - can view the page
      },
    });
    this.recurringAppointementDialog.onClose.subscribe((res) => {
      if (res) {
        this.onReloadPageAfterCloseDialog();
      }
    });
  }

  autoSubmitClick() {
    let element: HTMLElement = document.querySelector('.autoSave') as HTMLElement;
    element.click();
  }

  autoSaveAlertNavigate(val) {
    if (this.isNavigationFirstTime) {
      if (this.initiatonCallCreationForm.touched || this.callCreationTemplateForm.touched || this.notesForm.touched) {
        const message = `You have unsaved changes! Do you want to save this?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        // const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        this.autoSaveAlertDialog = this.dialogService.open(PrimengConfirmDialogPopupComponent, {
          // maxWidth: "400px",
          showHeader: false,
          width: "450px",
          data: { ...dialogData, msgCenter: true },
          baseZIndex: 5000,
          // disableClose: true
        });
        // dialogRef.afterClosed().subscribe(dialogResult => {
        this.autoSaveAlertDialog.onClose.subscribe(dialogResult => {
          // if (confirm("You have unsaved changes! Do you want to save this?")) {
          if (dialogResult === true) {
            // this.autoSubmitClick()
            if (this.initiatonCallCreationForm.invalid || !this.initiatonCallCreationForm?.get('isAddressVerified')?.value) {
              this.callCreationDialog = false
              this.initiatonCallCreationForm.markAllAsTouched();
              return false;
            }
            if (!this.callCreationTemplate) {
              if (this.callCreationTemplateForm.invalid || this.callCreationTemplateForm?.getRawValue()?.callCreationList.length == 0) {
                this.snackbarService.openSnackbar("Call Creation Template is required", ResponseMessageTypes.WARNING);
                return false
              }
            }
            this.onSubmit(val); // 1- for calendar navigation
            // return true
          } else if (dialogResult === false) {
            if (val == 1) {
              this.snackbarService.openSnackbar('Please save and proceed!', ResponseMessageTypes.WARNING);
            } else {
              return true;
            }
          }
        })
      } else {
        this.onRedirectTroubleShoot();
      }
    }
  }

  canDeactivate() {
    // autosave functionality starts here
    if (this.isNavigationFirstTime) {
      if (this.initiatonCallCreationForm.touched || this.callCreationTemplateForm.touched || this.notesForm.touched) {
        const message = `You have unsaved changes! Do you want to save this?`;
        const dialogData = new ConfirmDialogModel("Confirm Action", message);
        // const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        this.canDeactivateDialog = this.dialogService.open(PrimengConfirmDialogPopupComponent, {
          // maxWidth: "400px",
          showHeader: false,
          width: "450px",
          data: dialogData,
          baseZIndex: 5000,
          // disableClose: true
        });
        // return dialogRef.afterClosed().pipe(map(dialogResult => {
        return this.canDeactivateDialog.onClose.pipe(map(dialogResult => {
          // if (confirm("You have unsaved changes! Do you want to save this?")) {
          if (dialogResult === true) {
            this.autoSubmitClick()
            if (this.initiatonCallCreationForm.invalid || !this.initiatonCallCreationForm?.get('isAddressVerified')?.value) {
              this.callCreationDialog = false;
              this.initiatonCallCreationForm.markAllAsTouched();
              return false;
            }
            if (!this.callCreationTemplate) {
              if (this.callCreationTemplateForm.invalid || this.callCreationTemplateForm?.getRawValue()?.callCreationList.length == 0) {
                this.snackbarService.openSnackbar("Call Creation Template is required", ResponseMessageTypes.WARNING);
                return false;
              }
            }
            // setTimeout(() => {
            // this.navigateToTechAllocation()
            return true;
            // }, 2000);
          } else if (dialogResult === false) {
            // if (this.initiationBasicInfo.callInitiationId) {
            return true;
            // } else {
            //   this.snackbarService.openSnackbar('Please save and proceed!', ResponseMessageTypes.WARNING);
            // }
          }
        }))
      } else {
        return true;
      }
    } else {
      return true;
    }
    // autosave functionality ends here
  }

  isTechnicalAreaMngr() {
    if (this.userData.roleName.toLowerCase() == 'technical area manager') {
      return true
    } else {
      return false
    }
  }

  onSendGRVNotification() {
    this.isWarningNotificationDialog = false
    let formValue = {
      isPickedStockOrder: this.warningNotificationMessage?.isPickedStockOrder,
      callInitiationId: this.initiationId,
      createdUserId: this.userData.userId
    }
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_GRV_NOTIFICATION, formValue)
      .subscribe(res => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.getCallInitiationDetailsById()
          this.isWarningNotificationDialog = true
          this.warningNotificationMessage = {
            isGRV: false,
            validateMessage: "Notification sent to " + res.resources
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  openCallNotesPopup(type) {
    this.isCallNotesDialog = true
    this.callNotesType = type
    this.notesForm.get('notes').reset()
    if (type == NotesTypes.SERVICE_CALL_NOTES) {
      this.notesForm.get('isCallInitiationNotes').setValue(true)
      this.notesForm.get('isTechInstruction').setValue(false)
    } else if (type == NotesTypes.TECH_SPECIAL_INSTRUCTION_NOTES) {
      this.notesForm.get('isCallInitiationNotes').setValue(false)
      this.notesForm.get('isTechInstruction').setValue(true)
    }
  }

  onNotesCreate() {
    if (this.notesForm.invalid) {
      return
    }
    this.rxjsService.setDialogOpenProperty(true);
    let crudService = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION_NOTES, this.notesForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.isCallNotesDialog = false;
        // this.alertDialog = true
        // this.alertMsg = response.resources
        this.initiationId = response.resources;
        this.onReloadPageAfterCloseDialog();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  callDail(val = 'salesagent'): void {
    let num = this.initiationBasicInfo?.salesAgentContactCountryCode && this.initiationBasicInfo?.salesAgentContactNo
      ? `${this.initiationBasicInfo?.salesAgentContactCountryCode} ${this.initiationBasicInfo?.salesAgentContactNo}` : '';
    if (!this.agentExtensionNo) {
      this.dialog.closeAll();
      this.snackbarService.openSnackbar("Agent login is required", ResponseMessageTypes.WARNING);
      const dialogReff = this.dialog.open(ExtensionModalComponent, { width: '450px', disableClose: true });
    } else if (this.agentExtensionNo && num) {
      let data = {
        customerContactNumber: num,
        customerId: this.customerId,
        clientName: this.initiationBasicInfo?.salesAgent && this.initiationBasicInfo?.salesAgent
      }
      this.dialog.closeAll();
      this.rxjsService.setCustomerContactNumber(data);
      this.rxjsService.setExpandOpenScape(true);
    }
  }

  checkAllValidationCalls(event, callType: CallType, type) {
    if (!event.checked) {
      this.unSelectWarrentyRecall(type);
      return;
    }
    if (CallType.SERVICE_CALL == callType) {
      if (type == 'In Warranty') {
        this.initiatonCallCreationForm?.get('isRecall').setValue(false);
      } else if (type == 'Recall') {
        this.initiatonCallCreationForm?.get('isInWarrenty').setValue(false);
      }
      this.initiationBasicInfo.warrantyRecallReferenceId = this.initiationDetailsData.basicInfo?.warrantyRecallReferenceId;
      this.initiationBasicInfo.warrantyServiceCallNo = this.initiationDetailsData.basicInfo?.warrantyServiceCallNo;
      this.checkValidateServiceCall(type)
    } else if (CallType.INSPECTION == callType) {
      // this.checkValidateInspectionCall()
    } else if (CallType.SPECIALPROJECT_CALL == callType) {
      // this.checkValidateSpecialProject()
    } else if (CallType.RADIO_REMOVAL == callType) {
      // this.checkValidateRadioRemovals()
    }
  }

  checkValidateServiceCall(type) {
    let otherParams = {
      CustomerId: this.customerId,
      AddressId: this.customerAddressId
    };
    if (type == 'Recall') {
      otherParams['isRecall'] = true;
    }
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION_VALIDATION,
      undefined, false, prepareRequiredHttpParams(otherParams)).subscribe(response => {
        // this.isDealerCall = response.resources.isDealerCall
        if (response?.isSuccess && response?.statusCode == 200) {
          // this.navigateToInstallationByServiceCall()
          // } else {
          if (response.resources.validations?.length > 0) {
            let validations = response?.resources?.validations?.find(x => x.validateType == type)
            if (validations) {
              this.openServiceCallDialog(validations, type)
            } else {
              this.unSelectWarrentyRecall(type);
            }
          } else {
            this.unSelectWarrentyRecall(type);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  unSelectWarrentyRecall(type) {
    this.initiatonCallCreationForm?.get('isInWarrenty').setValue(this.initiationDetailsData?.basicInfo?.isInWarrenty);
    this.initiatonCallCreationForm?.get('isRecall').setValue(this.initiationDetailsData?.basicInfo?.isRecall);
    this.initiationBasicInfo.warrantyRecallReferenceId = this.initiationDetailsData.basicInfo?.warrantyRecallReferenceId;
    this.initiationBasicInfo.warrantyServiceCallNo = this.initiationDetailsData.basicInfo?.warrantyServiceCallNo;
  }

  openServiceCallDialog(rowData, type) {
    rowData['isDealerCall'] = false
    this.serviceCallDialog = this.dialogService.open(ServiceCallWaringDialogComponent, {
      showHeader: true,
      header: "Service Call " + rowData.validateType,
      baseZIndex: 10000,
      width: '550px',
      data: rowData,
    });
    this.serviceCallDialog.onClose.subscribe((result) => {
      if (result) {
        if (result == 1) {

        } else if (result == 2) {
          this.unSelectWarrentyRecall(type);
        } else if (result == 3) {
          this.openInwarrantyEscalationDialog(rowData, type)
        } else if (result == 4) { // view termination
          // this.openInwarrantyEscalationDialog(rowData)
          // this.rxjsService.setTabIndexForCustomerComponent(6)
        }
      } else {
        this.unSelectWarrentyRecall(type);
      }
    });
  }

  openInwarrantyEscalationDialog(rowData, type) {
    this.inwarrantyEscalationDialog = this.dialogService.open(ServiceCallInwarrantyDialogComponent, {
      showHeader: true,
      header: "Service Call " + rowData.validateType,
      baseZIndex: 1000,
      width: '550px',
      data: {
        ...rowData,
        createdUserId: this.userData?.userId,
      },
    });
    this.inwarrantyEscalationDialog.onClose.subscribe((result) => {
      if (result) {
        this.initiationBasicInfo.warrantyRecallReferenceId = result?.id;
        this.initiationBasicInfo.warrantyServiceCallNo = result?.displayName;
        this.initiatonCallCreationForm.markAllAsTouched();
      } else {
        this.unSelectWarrentyRecall(type);
      }
    });
  }

  downloadInvoice() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.TECHNICIAN_INCENTIVE_REVENUE, null, false, prepareRequiredHttpParams({
        IsInstallationService: true,
        IsExculded: false,
        TechIncentiveDetailId: this.initiationId
      })).subscribe((res: any) => {
        if (res?.isSuccess && res?.resources) {
          this.downloadExportedFile(res?.url, 'Invoice');
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  downloadExportedFile(url, fileName) {
    var link = document.createElement("a");
    if (link.download !== undefined) {
      link.setAttribute("href", url);
      link.setAttribute("download", fileName);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }

  isInvoiced() {
    return this.initiationBasicInfo?.isInvoiced
  }

  isVoid() {
    return this.initiationBasicInfo?.isVoid
  }

  onFormDisabled() {
    if (this.isInvoiced()) {
      this.initiatonCallCreationForm.disable({ emitEvent: false });
      this.callCreationTemplateForm.disable();
      this.addNewContactForm.disable();
      if (this.isTechnicalAreaMngr()) {
        this.initiatonCallCreationForm.get('appointmentInfo.diagnosisId').enable();
        this.initiatonCallCreationForm.get('appointmentInfo.diagnosisId').setValidators(Validators.required);
        this.initiatonCallCreationForm.get('appointmentInfo.diagnosisId').updateValueAndValidity();
      }
    } else if (this.isVoid()) {
      this.initiatonCallCreationForm.disable({ emitEvent: false });
      this.callCreationTemplateForm.disable({ emitEvent: false });
      this.addNewContactForm.disable({ emitEvent: false });
    } else if (this.initiationDetailsData?.basicInfo?.isTempDone) {
      if (this.isTechnicalAreaMngr()) {
        this.initiatonCallCreationForm.get('appointmentInfo.diagnosisId').enable();
        this.initiatonCallCreationForm.get('appointmentInfo.diagnosisId').setValidators(Validators.required);
        this.initiatonCallCreationForm.get('appointmentInfo.diagnosisId').updateValueAndValidity();
      }
    } else {
      this.initiatonCallCreationForm.get('appointmentInfo.diagnosisId').disable();
    }
  }

  onReqDiagnosis() {
    return this.isTechnicalAreaMngr() && (this.initiationDetailsData?.basicInfo?.isTempDone || this.initiationDetailsData?.basicInfo?.isInvoiced);
  }

  onButtonDisabled() {
    return !this.isTechnicalAreaMngr() && (this.initiationDetailsData?.basicInfo?.isTempDone || this.initiationDetailsData?.basicInfo?.isInvoiced);
  }

  onPRAmtClick(type = 'prCall') {
    if (!this.initiationDetailsData?.prCall?.allApprovedAmount?.toString() && type == 'prCall') {
      return;
    }
    if (!this.initiationDetailsData?.saveOfferCall?.allApprovedAmount?.toString() && type == 'saveOffer') {
      return;
    }
    let api = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.PR_CALL_ITEMS, null, false, prepareRequiredHttpParams({ callInitiationId: this.initiationId }));
    if (this.initiationDetailsData?.prCall?.allApprovedAmount?.toString() && type == 'saveOffer') {
      api = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SAVE_OFFER_ITEMS, null, false, prepareRequiredHttpParams({ callInitiationId: this.initiationId }));
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    api.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess && res?.statusCode == 200) {
        this.onPRAmtDialog(res?.resources);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  onPRAmtDialog(data) {
    const ref = this.dialogService.open(PrAmountDetailsDialogComponent, {
      header: 'Request Number',
      baseZIndex: 1000,
      width: '980px',
      closable: false,
      showHeader: false,
      data: { prItemDetails: data },
    });
    ref.onClose.subscribe((res: any) => {
      if (res) {

      }
    });
  }

  public ngOnDestroy(): void {
    // This aborts all HTTP requests.
    this.ngUnsubscribe.next();
    // This completes the subject properlly.
    this.ngUnsubscribe.complete();
    if (this.multipleSubscription) {
      this.multipleSubscription.unsubscribe();
    }
    if (this.callCreationCreationSubscription) {
      this.callCreationCreationSubscription.unsubscribe();
    }
    if (this.technAssistDialog) {
      this.technAssistDialog.close();
    }
    if (this.recurringAppointementDialog) {
      this.recurringAppointementDialog.close();
    }
    if (this.autoSaveAlertDialog) {
      this.autoSaveAlertDialog.close();
    }
    if (this.canDeactivateDialog) {
      this.canDeactivateDialog.close();
    }
    if (this.serviceCallDialog) {
      this.serviceCallDialog.close();
    }
    if (this.inwarrantyEscalationDialog) {
      this.inwarrantyEscalationDialog.close();
    }
    if (this.systemTypeSubscription) {
      this.systemTypeSubscription.unsubscribe();
    }
    if (this.faultDescriptionSubscription) {
      this.faultDescriptionSubscription.unsubscribe();
    }
    if (this.serviceSubTypesSubscription) {
      this.serviceSubTypesSubscription.unsubscribe();
    }
    if (this.jobTypeSubscription) {
      this.jobTypeSubscription.unsubscribe();
    }
    if (this.contactListSubscription) {
      this.contactListSubscription.unsubscribe();
    }
  }

}
