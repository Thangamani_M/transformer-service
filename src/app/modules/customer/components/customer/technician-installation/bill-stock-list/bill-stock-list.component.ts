import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { map } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';

@Component({
  selector: 'app-bill-stock-list',
  templateUrl: './bill-stock-list.component.html'
})
export class BillStockListComponent implements OnInit {

  @Input() loading: boolean;
  dataList: any;
  status: any = [];
  selectedRows: string[] = [];
  totalRecords: any;
  userSubscription: any;
  listSubscription: any;
  pageLimit: any = [10, 25, 50, 75, 100];
  primengTableConfigProperties: any;
  @Input() selectedTabIndex: any = 0;
  loggedInUserData: any;
  searchColumns: any;
  @Input() observableResponse: any;
  isShowNoRecords: any = true;
  @Input() customerId: any;
  @Input() customerAddressId: any;
  @Input() ContractDetails: any;
  @Input() selectedRowData: any;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    private crudService: CrudService, private store: Store<AppState>, private rxjsService: RxjsService,) {
    this.primengTableConfigProperties = {
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Bill Stock List',
            dataKey: 'occurrenceBookSignalId',
            enableAction: false,
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'stockId', header: 'Stock ID', width: '100px' },
            { field: 'stockDescription', header: 'Stock Description', width: '200px' },
            { field: 'endDate', header: 'End Date', width: '160px' },
            { field: 'qty', header: 'Qty', width: '80px' },
            { field: 'unitPrice', header: 'Unit Price', width: '100px' },
            { field: 'totalExcl', header: 'Total Excl.', width: '100px' },
            { field: 'vat', header: 'VAT', width: '70px' },
            { field: 'totalIncl', header: 'Total Incl.', width: '100px' },],
            apiSuffixModel: CustomerModuleApiSuffixModels.BILL_OF_SERVICE_STOCK_DETAILS,
            moduleName: ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    // this.getRequiredListData();
  }

  ngOnChanges() {
    
    this.onTableChanges();
  }

  onTableChanges() {
    if (this.selectedTabIndex == 0) {
      this.dataList = this.observableResponse;
      this.totalRecords = 0;
      this.isShowNoRecords = this.dataList?.length ? false : true;
    } else {
      this.dataList = this.observableResponse;
      this.totalRecords = 0;
      this.isShowNoRecords = true;
    }
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }

  combineLatestNgrxStoreData() {
    this.userSubscription = combineLatest([this.store.select(loggedInUserData)]).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    this.isShowNoRecords = false;
    this.dataList = [];
    let EventMgntModuleApiSuffixModels: EventMgntModuleApiSuffixModels;
    EventMgntModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = { CustomerId: this.customerId, SiteAddressId: this.customerAddressId, ServiceId: this.ContractDetails?.serviceId, ContractDbId: this.ContractDetails?.contractDbId, ...otherParams };
    let api = this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      EventMgntModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams));
    if (this.listSubscription && !this.listSubscription.closed) {
      this.listSubscription.unsubscribe();
    }
    this.listSubscription = api.subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && this.selectedTabIndex == 1) {
        this.observableResponse = data.resources['stockDetails'];
      } else if (this.selectedTabIndex == 3) {
        this.observableResponse = data;
      } else {
        this.observableResponse = null;
      }
      this.onTableChanges();
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], searchObj)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["../add-edit"], { relativeTo: this.activatedRoute });
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['../view'], { relativeTo: this.activatedRoute, queryParams: { id: editableObject['wdRequestId'] } });
            break;
        }
    }
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }
}
