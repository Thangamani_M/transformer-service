import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CrudService, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-service-call-inwarranty-dialog',
  templateUrl: './service-call-inwarranty-dialog.component.html'
})
export class ServiceCallInwarrantyDialogComponent implements OnInit {

  showDialogSpinner: boolean = false;
  data: any = [];
  escalationForm: FormGroup;
  callEscalationsubscritption: any;
  preferredDateFlag: boolean = false;
  commentsFlag: boolean = false;
  todayDate: Date = new Date();
  isExists: boolean = false;
  selectedMoment = new Date();
  private isCallEscalationExists = new BehaviorSubject<boolean>(false);

  constructor(public ref: DynamicDialogRef, public _fb: FormBuilder, public config: DynamicDialogConfig,
    private crudService: CrudService, private rxjsService: RxjsService, private snackbarService: SnackbarService,) { }

  ngOnInit(): void {
    this.escalationForm = this._fb.group({
      initiationId: [{ value: '', disabled: false }, Validators.required],
      // PreferredDateTime:[{value: '', disabled: false},Validators.required],
      // Comments:[{value: '', disabled: false},Validators.required]
    })
    if (this.config.data.validateData.length == 1) {
      this.escalationForm.get('initiationId').setValue(this.config.data.validateData[0].id)
    }
  }


  onSubmit() {
    if (this.escalationForm.invalid) {
      return
    }
    this.rxjsService.setFormChangeDetectionProperty(true);

    let initiationId = this.escalationForm.get('initiationId').value;
    const displayName = this.config.data.validateData?.find(el => el?.id == initiationId)?.displayName;
    this.ref.close({id: initiationId, displayName: displayName}); // this.ref.close(res);

  }

  close() {
    this.ref.close();
  }

  onSelect(value) {
  }

}

