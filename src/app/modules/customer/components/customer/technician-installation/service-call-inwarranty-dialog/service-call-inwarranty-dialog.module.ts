import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ServiceCallInwarrantyDialogComponent } from './service-call-inwarranty-dialog.component';

@NgModule({
  declarations: [ServiceCallInwarrantyDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule
  ],
  exports: [ServiceCallInwarrantyDialogComponent],
  entryComponents: [ServiceCallInwarrantyDialogComponent],
})
export class ServiceCallInwarrantyDialogModule { }
