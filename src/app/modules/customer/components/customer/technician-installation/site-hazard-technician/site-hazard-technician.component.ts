import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { CustomerModuleApiSuffixModels } from '@modules/customer';
import { SiteHazardModel } from '@modules/customer/models/site-hazard.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';

@Component({
  selector: 'app-site-hazard-technician',
  templateUrl: './site-hazard-technician.component.html'
})
export class SiteHazardTechnicianComponent implements OnInit {
  customerId: string;
  customerAddressId: string;
  sitehazardtechnicianForm: FormGroup;
  loggedUser: any;
  sitehazardtech: any;
  AppointmentId:any;
  installationId:any;
  callType:any;
  constructor(private crudService: CrudService,private router: Router,
    private activatedRoute: ActivatedRoute,
    public dialogService: DialogService,
    private store: Store<AppState>,
    private rxjsService: RxjsService, private formBuilder: FormBuilder,) {
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.customerAddressId = this.activatedRoute.snapshot.queryParams.customerAddressId;
    // this.customerId = this.activatedRoute.snapshot.queryParams.customerId ? this.activatedRoute.snapshot.queryParams.customerId : '';
    // this.customerAddressId = this.activatedRoute.snapshot.queryParams.customerAddressId ? this.activatedRoute.snapshot.queryParams.customerAddressId : '';
    this.AppointmentId = this.activatedRoute.snapshot.queryParams.AppointmentId;
    this.installationId = this.activatedRoute.snapshot.queryParams.installationId;
    this.callType = this.activatedRoute.snapshot.queryParams.callType;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createsitehazardForm();
    this.getsitehazardList();
  }
  //Create Form
  createsitehazardForm(): void {
    let SiteHazardConfigModel = new SiteHazardModel();
    // create form controls dynamically from model class
    this.sitehazardtechnicianForm = this.formBuilder.group({
    });
    Object.keys(SiteHazardConfigModel).forEach((key) => {
      this.sitehazardtechnicianForm.addControl(key, new FormControl(SiteHazardConfigModel[key]));
    });
  }

  //Get MEthod
  getsitehazardList() {
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_INSTALLATION_SITE_HAZARD, undefined, false,
      prepareRequiredHttpParams({ addressId: this.customerAddressId, CustomerId: this.customerId }))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.sitehazardtech = response.resources
          this.sitehazardtechnicianForm.patchValue(this.sitehazardtech);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
  }

  //Get MEthod
  //  getsitehazardList() {
  //         this.resources= {"isSuccess":true,"message":null,"exceptionMessage":null,
  // "statusCode":200,"pageIndex":null,"itemCount":null,"totalCount":0,"resources":{"siteHazardId":"764299e9-8093-4ca0-858b-5edefd644827",
  // "contractId":"54a52901-f93d-4dd9-a459-4822c276af8b","leadId":"ff8ea639-f9e1-4253-8a2c-2baa85124eb3","customerId":"9ab06209-e0a2-45c0-84bf-e1cb3be1e2fe",
  // "partitionId":"5775b997-1b1e-4326-91f7-3f115e0f422e","quotationVersionId":"04cba551-0c56-4241-bae4-0e422cd2559e","specialInstructionId":null,
  // "noOfDogs":"2","isInstallationRequired":true,"isViciousDogs":true,"viciousDogDescription":null,"isConfinedSpace":true,"confinedSpaceDescription":"Test",
  // "isExtendedLadders":false,"extendedLadderDescription":"","isConstructionSite":false,"constructionSiteDescription":"","isHarmfulChemical":false,"harmfulChemicalDescription":"",
  // "isLoudNoiseArea":false,"loudNoiseAreaDescription":"","isAsbestos":false,"asbestosDescription":"","description":null,"dogTypes":[{"dogDetailId":"306d2943-08f8-49ed-bec4-8643a89e253d",
  // "dogTypeDetailId":"19f19427-eb2d-4e94-892b-7b38f83373c0","dogTypeId":2,"dogType":"Medium"},{"dogDetailId":"306d2943-08f8-49ed-bec4-8643a89e253d","dogTypeDetailId":"ad1e25e9-d152-43d4-8336-8ee580bb3c86",
  // "dogTypeId":3,"dogType":"Large"}],"isUnsafe":true,"unsafeDescription":"Testing ","isConfinedSpaceInstalled":true,"confinedSpaceInstalledDescription":"DF",
  // "viciousDogsDescription":"Testing "}}
  //         this.sitehazardtech = this.resources.resources
  //         this.sitehazardtechnicianForm.patchValue(this.sitehazardtech);
  //        this.rxjsService.setGlobalLoaderProperty(false);
  //       //}
  //     //})
  // }

  navigateToPage() {
    this.router.navigate(['/customer', 'manage-customers', 'call-initiation'], {
      queryParams: { 
        // customerId: this.installationId 
        customerId: this.customerId,
        customerAddressId: this.customerAddressId,
        initiationId: this.installationId,
        appointmentId: this.AppointmentId ? this.AppointmentId : null,
        callType: this.callType
      },skipLocationChange:true
    });
  }

}
