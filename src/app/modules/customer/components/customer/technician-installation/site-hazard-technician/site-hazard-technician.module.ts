import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SiteHazardTechnicianComponent } from "./site-hazard-technician.component";



@NgModule({
    declarations: [SiteHazardTechnicianComponent ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: SiteHazardTechnicianComponent, data: { title: 'Site Hazard' }
        },
    ])
  ],
  entryComponents: [],
})
export class SiteHazardTechnicianModule  { }