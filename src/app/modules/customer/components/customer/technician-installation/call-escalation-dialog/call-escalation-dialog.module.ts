import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CallEscalationDialogComponent } from './call-escalation-dialog.component';

@NgModule({
  declarations: [ CallEscalationDialogComponent ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MaterialModule,
  ],
  exports: [CallEscalationDialogComponent],
  entryComponents: [CallEscalationDialogComponent],
})
export class CallEscalationModule { }
