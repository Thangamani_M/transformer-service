import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CrudService, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { CallType } from '../customer-technical/call-type.enum';



@Component({
  selector: 'app-call-escalation-dialog',
  templateUrl: './call-escalation-dialog.component.html',
  styleUrls: ['./call-escalation-dialog.component.scss'],
})
export class CallEscalationDialogComponent implements OnInit {

  todayDate: any = new Date();//new Date(new Date().toLocaleDateString())
  callEscalationDialogForm: FormGroup;
  iscallEscalationSubmit: boolean;
  callEscalationsubscritption: any;

  constructor(private rxjsService: RxjsService, public ref: DynamicDialogRef,
    public config: DynamicDialogConfig, private crudService: CrudService,
    private momentService: MomentService, private snackbarService: SnackbarService) {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.config?.data?.isServiceCallEscalation) {
      this.iscallEscalationSubmit = this.config?.data?.feedback && this.config?.data?.callInitiationEscalationId;
    }
  }

  ngOnInit(): void {
    // this.todayDate = this.config?.data?.preferredDate ? new Date(this.config?.data?.preferredDate) : new Date();
    this.initForm();
  }

  initForm() {
    this.callEscalationDialogForm = new FormGroup({
      initiationId: new FormControl({ value: '', disabled: false }),
      CallEscalationRequestNumber: new FormControl({ value: this.config?.data?.callInitiationNumber, disabled: true }),
      PreferredDateTime: new FormControl({
        value: null,
        disabled: this.config?.data?.feedback
      }),
      Comments: new FormControl({
        value: this.config?.data?.comments ? this.config?.data?.comments : '',
        disabled: this.config?.data?.callInitiationEscalationId
      }),
      Feedback: new FormControl({
        value: this.config?.data?.feedback ? this.config?.data?.feedback : '',
        disabled: this.config?.data?.feedback
      }),
    })
    if ((!this.config?.data?.callInitiationEscalationId || !this.config?.data?.isServiceCallEscalation) && !this.config?.data?.validateType) {
      this.callEscalationDialogForm = setRequiredValidator(this.callEscalationDialogForm, ["CallEscalationRequestNumber", "Comments"])
    } else if (this.config?.data?.callInitiationEscalationId && this.config?.data?.isServiceCallEscalation && !this.config?.data?.validateType) {
      this.callEscalationDialogForm = setRequiredValidator(this.callEscalationDialogForm, ["Comments", "Feedback"])
    } else if (this.config?.data?.validateType) {
      this.callEscalationDialogForm = setRequiredValidator(this.callEscalationDialogForm, ["initiationId", "Comments"]);
    }
    if(this.config?.data?.isServiceCallEscalation && this.config?.data?.feedback) {
      this.callEscalationDialogForm.get('PreferredDateTime').disable();
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.callEscalationDialogForm.get('PreferredDateTime').setValue(this.config?.data?.preferredDate ? new Date(this.config?.data?.preferredDate) : null);
    }, 500)
  }

  getEnableDisablePreferredDate() {
    return this.iscallEscalationSubmit || (this.config?.data?.comments && !this.config?.data?.isServiceCallEscalation)
  }

  onSelect(event) {
    let id = event;
    if (this.callEscalationsubscritption && !this.callEscalationsubscritption.closed) {
      this.callEscalationsubscritption.unsubscribe();
    }
    this.callEscalationsubscritption = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.QUICK_CALL_ESCALATION, null, false, prepareRequiredHttpParams({ CallInitiationId: id })).subscribe((res: any) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res?.isSuccess == true && res?.statusCode == 200 && res?.resources) {
          this.config.data.callInitiationEscalationId = res?.resources?.callInitiationEscalationId;
          this.onChangeControlValues(res);
          if (!res?.resources?.preferredDate) {
            this.onEnableDisableCon('enable');
          } else {
            this.onEnableDisableCon('disable');
          }
        } else {
          this.onEnableDisableCon('enable');
          this.onChangeControlValues();
        }
      });
  }

  onEnableDisableCon(val) {
    if (val == 'enable') {
      this.callEscalationDialogForm.get('PreferredDateTime').enable();
      this.callEscalationDialogForm.get('Comments').enable();
    } else if (val == 'disable') {
      this.callEscalationDialogForm.get('PreferredDateTime').disable();
      this.callEscalationDialogForm.get('Comments').disable();
    }
  }

  onChangeControlValues(res?) {
    this.callEscalationDialogForm.patchValue({
      PreferredDateTime: res?.resources?.preferredDate ? res?.resources?.preferredDate : null,
      Comments: res?.resources?.comments ? res?.resources?.comments : '',
    });
  }

  btnCloseClick() {
    this.rxjsService.setDialogOpenProperty(false);
    this.ref.close(false);
  }

  onSubmitCallEscalationDialog() {
    if(!this.config?.data?.editPermission && !this.config?.data?.isServiceCallEscalation) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.iscallEscalationSubmit || !this.callEscalationDialogForm?.valid) {
      return;
    }
    let preferredDate = null;
    if (this.callEscalationDialogForm.value?.PreferredDateTime) {
      preferredDate = this.momentService.toMoment(this.callEscalationDialogForm?.get('PreferredDateTime')?.value).format('YYYY-MM-DD HH:mm:ss.ms');
    }
    const callEscalationObject = {
      CallInitiationEscalationId: this.config?.data?.callInitiationEscalationId ? this.config?.data?.callInitiationEscalationId : null,
      CallInitiationId: this.config?.data?.callInitiationId ? this.config?.data?.callInitiationId : this.callEscalationDialogForm.value?.initiationId,
      PreferredDate: preferredDate,
      Comments: this.callEscalationDialogForm?.get('Comments')?.value,
      Feedback: this.callEscalationDialogForm.value.Feedback,
      CreatedUserId: this.config?.data?.createdUserId,
    }
    let condition = this.config?.data?.callInitiationEscalationId && !this.config?.data?.isServiceCallEscalation;
    if (this.config?.data?.header == 'Service Call Escalation' && this.config?.data?.isServiceCallEscalation) {
      condition = this.config?.data?.feedback && this.config?.data?.isServiceCallEscalation;
    } else if (this.config?.data?.vavalidateTypelidateType) {
      condition = this.config?.data?.callInitiationEscalationId;
    }
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    if (this.callEscalationDialogForm.valid && !condition) {
      this.iscallEscalationSubmit = true;
      let api = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.QUICK_CALL_ESCALATION, callEscalationObject)
      if (this.config?.data?.callInitiationEscalationId) {
        if(this.config?.data?.isServiceCallEscalation) {
          callEscalationObject['taskListId'] = this.config?.data?.row?.requestId;
          callEscalationObject['callInitiationEscalationApprovalId'] = this.config?.data?.row?.referenceId;
        }
        api = this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.QUICK_CALL_ESCALATION_APPROVAL, callEscalationObject);
        delete callEscalationObject.CallInitiationId;
        delete callEscalationObject.Comments;
      } else if (!this.config?.data?.callInitiationEscalationId) {
        delete callEscalationObject.Feedback;
      }
      api.subscribe((res: any) => {
        if (res?.isSuccess == true && res?.statusCode == 200) {

        }
        this.config?.data?.validateType ? this.ref.close() : this.ref.close(res);
        this.rxjsService.setDialogOpenProperty(false);
        this.iscallEscalationSubmit = false;
      })
    } else if (this.config?.data?.callInitiationEscalationId && !this.config?.data?.isServiceCallEscalation) { // view customer
      this.snackbarService.openSnackbar("Call Escalation is already submitted", ResponseMessageTypes.WARNING);
      this.ref.close(false);
    } else if (this.config?.data?.feedback && this.config?.data?.isServiceCallEscalation) { // technical area manager list
      this.snackbarService.openSnackbar("Service Call Escalation is already submitted", ResponseMessageTypes.WARNING);
      this.ref.close(false);
    }
  }

  redirectToService = () => {
    const row = this.config?.data?.row;
    window.open(`${window.location.origin}/customer/manage-customers/call-initiation?customerId=${row.customerId}&customerAddressId=${row.addressId}&initiationId=${row.callInitiationId}&callType=${row.callType == CallType.SERVICE_CALL ? 2 : row.callType == CallType.INSTALLATION_CALL ? 1 : row.callType == CallType.SPECIALPROJECT_CALL ? 3 : 4}`);
  }
}
