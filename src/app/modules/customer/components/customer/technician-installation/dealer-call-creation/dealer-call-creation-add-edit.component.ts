import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DateAdapter, MatDialog, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, ConfirmDialogModel, ConfirmDialogPopupComponent, convertTreeToList, countryCodes, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareAutoCompleteListFormatForMultiSelection, prepareGetRequestHttpParams, prepareRequiredHttpParams, PrimengConfirmDialogPopupComponent, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { ActualTimeDialogComponent } from '@modules/customer';
import { ActualTimingListModel, ActualTimingModel } from '@modules/customer/models/actual-timing.model';
import { CallCreationTemplateListModel, CallCreationTemplateModel } from '@modules/customer/models/call-creation-template.model';
import { DealerCallAddEditModel, OnHoldModel, VoidModel } from '@modules/customer/models/dealer-call.module';
import { CustomerModuleApiSuffixModels, CUSTOMER_COMPONENT } from '@modules/customer/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { InspectionCallBackDialogComponent } from '@modules/technical-management/components/inspection-call/inspection-call-back-dialog/inspection-call-back-dialog.component';
import { RequestType } from '@modules/technical-management/components/technical-area-manager-worklist/request-type.enum';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { ConfirmationService, DialogService } from 'primeng/api';
import { combineLatest, forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, map} from 'rxjs/operators';
import { CallEscalationDialogComponent } from '../call-escalation-dialog';
import { RecurringAppointmentComponent } from '../recurring-appointment/recurring-appointment.component';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL'
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY'
  }
};

@Component({
  selector: 'app-dealer-call-creation-add-edit',
  templateUrl: './dealer-call-creation-add-edit.component.html',
  styleUrls: ['./dealer-call-creation-add-edit.component.scss'],
  providers: [ConfirmationService,
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]

})

export class DealerCallCreationAddEditComponent implements OnInit {
  loading = false;
  holdInfoDialog: boolean = false;
  voidInfoDialog: boolean = false;
  referToSalesDialog: boolean = false;
  installationId: string;
  callCreationDialog: boolean = false;
  title: any;
  initiationBasicInfo: any = {};
  initiationLightingCover: any = {};
  initiationServiceInfo: any = {};
  initiationAppointmentInfo: any = {};
  userData: UserLogin;
  initiationId: any;
  minFUpDate = new Date();
  // yesterday = this.momentService.getYesterday()
  // minDate: Date = new Date(this.yesterday);

  initiatonCallCreationForm: FormGroup;
  onHoldPopupForm: FormGroup;
  voidInfoForm: FormGroup;
  reasonDropDown: any = [];
  initiationDetailsData: any = [];
  sysyemTypeList: any = [];
  SystemTypeSubCategoryList: any = [];
  ServiceSubTypeList: any = [];
  faultDescriptionList: any = [];
  jobTypeList: any = [];
  projectList: any;
  panelList: any = [];
  serviceSubTypeId = ''
  feedBackList = [];
  selectedIndex = 0;
  contactList: any = []
  contactNumberList: any = []
  isRebookDialog: boolean;
  isRebookAlertDialog: boolean;
  showRebookComments: boolean;
  isRebookSubmit: boolean;
  rebookDialogForm: FormGroup;
  callEscalationsubscritption: any;
  rebookReasonList: any;
  rebookalertMesssage: any;
  alternativContactList: any = []
  contactAlternativeNumberList: any = []
  callInitiationContacts: any = []
  isWarningNotificationDialog: boolean = false;
  warningNotificationMessage: any = '';
  formConfigs = formConfigs;
  mobileNumberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  openCloseMonitoringForm: FormGroup;
  countryCodes = countryCodes;
  @ViewChildren('contactRows') contactRows: QueryList<ElementRef>;
  addNewContactForm: FormGroup;
  openAddContactDialog: boolean = false;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  customerId: string;
  callType: any;
  callCreationTemplate: boolean;
  callCreationTemplateForm: FormGroup;
  callCreationTemplateList: FormArray;
  // paymentDropdown: any = [
  //   { id: 'Debit Order', displayName: 'Debit Order' },
  //   { id: 'Credit Order', displayName: 'Credit Order' }
  // ];
  paymentDropdown: any = []
  timeDropdown: any = [
    { id: 'AM', displayName: 'AM' },
    { id: 'PM', displayName: 'PM' }
  ];
  actualTimeDialog: boolean = false;
  actualTimingForm: FormGroup;
  actualTimingList: FormArray;
  prCallDialog: boolean;
  prCallForm: FormGroup;
  requestType: RequestType;
  priorityList: any = [];
  escalationPopup: boolean = false;
  autoCallCreationPopup: boolean = false;
  customerAddressId: any = '';
  listOfFiles: any[] = [];
  fileList: File[] = [];
  maxFilesUpload: Number = 5;
  selfHelpSystemTypeConfigId: any;
  warrantyRecallReferenceId: any
  actualTimingDateList = [];
  alertDialog: boolean = false
  alertMsg: string = ''
  protected ngUnsubscribe: Subject<void> = new Subject<void>();
  cepFormatadoValue: string;
  debtorList: any[]
  isManualChange: boolean = false
  blockedPanel: boolean = false;
  loopIssueRender: boolean;
  isAutoSaveEnable: boolean = true
  isNavigationFirstTime: boolean = true
  appointmentId: any;
  dealerTechnicianDialog: boolean = false;
  actionPermissionObj: any;

  constructor(private confirmationService: ConfirmationService,
    private rxjsService: RxjsService, private httpCancelService: HttpCancelService, private dialog: MatDialog, private momentService: MomentService, private dialogService: DialogService,
    private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private crudService: CrudService, private datePipe: DatePipe, private dateAdapter: DateAdapter<Date>,
  ) {
    this.dateAdapter.setLocale('en-GB'); //dd/MM/yyyy
    this.initiationId = this.activatedRoute.snapshot.queryParams.initiationId;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.callType = this.activatedRoute.snapshot.queryParams.callType;
    this.escalationPopup = this.activatedRoute.snapshot.queryParams ? this.activatedRoute.snapshot.queryParams.escalationPopup : false;
    this.autoCallCreationPopup = this.activatedRoute.snapshot.queryParams ? this.activatedRoute.snapshot.queryParams.callCreationPopup : false;
    this.customerAddressId = this.activatedRoute.snapshot.queryParams ? this.activatedRoute.snapshot.queryParams.customerAddressId : null;
    // this.warrantyRecallReferenceId = this.activatedRoute.snapshot.queryParams ? this.activatedRoute.snapshot.queryParams.warrantyRecallReferenceId : null;
    // this.initiationId = '78036e57-3eae-41c8-9c8c-1fdef28578ce';
    this.appointmentId = this.activatedRoute.snapshot.queryParams.appointmentId ? this.activatedRoute.snapshot.queryParams.appointmentId : null;
    let troubleShootObject = {
      customerId: this.customerId,
      initiationId: this.initiationId,
      customerAddressId: this.customerAddressId,
      callType: this.callType
    }
    this.rxjsService.setTroubleShootAlarm(troubleShootObject)
    this.rxjsService.setCallInitiationData(this.activatedRoute.snapshot.queryParams);
    // this.callCreationTemplate = this.callType == '1' ? true : false
    // this.title = this.callType == '1' ? 'Installation' : this.callType == '2' ? 'Service' :
    // this.callType == '3' ? 'Special Projects' : '---';
    this.title = this.callType == '1' ? 'Dealer Call' : this.callType == '2' ? 'Dealer Call' :
      this.callType == '3' ? 'Dealer Call' : '---';
    if (this.escalationPopup && this.initiationId) {
      this.openCallEscalationDialog()
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }


  // INSTALLATION_GET_METHOD
  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createInstallationcallCreationFrom();
    this.createCallCreationTemplateForm()
    this.createActualTimingForm()
    this.createAddNewContactForm()
    this.createOnHoldCreationFrom();
    this.createVoidInfoCreationFrom();
    this.initRebookDialogForm();
    this.onLoadRebookReasonList();
    this.getDropdown();
    this.createPrCallForm();

    this.openCallCreationDialog(false) // for call creation template validation check
    if (this.autoCallCreationPopup) {
      this.openCallCreationDialog(true);
    }
    // this.getNextavailableAppointment()
    if (this.callType == '1') { // initiation call
      let dropdownsAndData = [
        this.crudService.get(ModulesBasedApiSuffix.DEALER,
          CustomerModuleApiSuffixModels.UX_DEALER_CUSTOMER_DEBTORS, undefined,
          false, prepareRequiredHttpParams({
            customerId: this.customerId,
            addressId: this.customerAddressId
          })).pipe(map(result => result), catchError(error => of(error))),
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE,
          undefined,
          false, prepareGetRequestHttpParams(null, null,
            {
              ShowDescriptionOnly: true
            })).pipe(map(result => result), catchError(error => of(error))),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE_SUB_CATEGORIES)
          .pipe(map(result => result), catchError(error => of(error))),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          TechnicalMgntModuleApiSuffixModels.JOBTYPE)
          .pipe(map(result => result), catchError(error => of(error))),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_PANEL_TYPE_CONFIG)
          .pipe(map(result => result), catchError(error => of(error))),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_DEALER_CALL_FEEDBACK_CONFIG)
          .pipe(map(result => result), catchError(error => of(error))),
        //     this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        //       TechnicalMgntModuleApiSuffixModels.UX_SERVICE_SUB_TYPES),
        //       this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        //         TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_SERVICE_SUB_TYPES),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.UX_PRIORITY)
          .pipe(map(result => result), catchError(error => of(error))),

      ];
      this.loadActionTypes(dropdownsAndData);
    } else if (this.callType == '2' || this.callType == '3') { // service call

      let dropdownsAndData = [
        this.crudService.get(ModulesBasedApiSuffix.DEALER,
          CustomerModuleApiSuffixModels.UX_DEALER_CUSTOMER_DEBTORS, undefined,
          false, prepareRequiredHttpParams({
            customerId: this.customerId,
            addressId: this.customerAddressId
          })).pipe(map(result => result), catchError(error => of(error))),
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE,
          undefined,
          false, prepareGetRequestHttpParams(null, null,
            {
              ShowDescriptionOnly: true
            })).pipe(map(result => result), catchError(error => of(error))),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_PANEL_TYPE_CONFIG)
          .pipe(map(result => result), catchError(error => of(error))),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_DEALER_CALL_FEEDBACK_CONFIG)
          .pipe(map(result => result), catchError(error => of(error))),
        // this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        //     TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE_SUB_CATEGORY),
        //     this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        //       TechnicalMgntModuleApiSuffixModels.UX_SERVICE_SUB_TYPES),
        //       this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        //         TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_SERVICE_SUB_TYPES),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.UX_PRIORITY)
          .pipe(map(result => result), catchError(error => of(error))),
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          InventoryModuleApiSuffixModels.UX_SPECIAL_PROJECT,
          undefined,
          false, prepareGetRequestHttpParams(null, null,
            {
              IsOpen: true
            })).pipe(map(result => result), catchError(error => of(error))),
        this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_QUICK_CALL)
          .pipe(map(result => result), catchError(error => of(error))),

      ];
      this.loadActionTypes(dropdownsAndData);
    }
    this.getCallInitiationDetailsById();
    this.onCallInitiationValueChanges();
  }

  ngAfterViewInit(): void {
    this.loopIssueRender = true
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    // setTimeout(() => {
    //   this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue('1234');
    //   // this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').markAsTouched()
    //   // (<any>this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime')).nativeElement.focus();
    //   // this.estimatedTimeInput.nativeElement.focus()
    // }, 3000);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][CUSTOMER_COMPONENT.CUSTOMER]?.find(el => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD);
      if (permission) {
        let techPermission = convertTreeToList([permission?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.TECHNICAL)]);
        this.actionPermissionObj = techPermission?.find(el => el?.[CUSTOMER_COMPONENT.DEALER_CALL])?.[CUSTOMER_COMPONENT.DEALER_CALL];
      }
    });
  }

  getQuickActionPermission(actionTypeMenuName) {
    return this.actionPermissionObj?.find(el => el?.menuName == PermissionTypes.QUICK_ACTION)?.subMenu?.find(el => el?.menuName == actionTypeMenuName);
  }

  findActionPermission(value: string) {
    return this.actionPermissionObj?.find(el => el?.menuName == value);
  }

  getAppointmentActionPermission(actionTypeMenuName) {
    return this.actionPermissionObj?.find(el => el?.menuName == CUSTOMER_COMPONENT.APPOINTMENT_BOOKING)?.subMenu?.find(el => el?.menuName == actionTypeMenuName);
  }

  getActualTimingActionPermission(actionTypeMenuName) {
    return this.actionPermissionObj?.find(el => el?.menuName == CUSTOMER_COMPONENT.ACTUAL_TIMING)?.subMenu?.find(el => el?.menuName == actionTypeMenuName);
  }

  getEditPermission() {
    return !this.findActionPermission(PermissionTypes.EDIT) && this.initiationId;
  }

  guardaValorCEPFormatado(evento) {
    this.cepFormatadoValue = evento;
    this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(evento)
  }

  getCallInitiationDetailsById() {
    // this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.initiationId || this.customerId) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.getInitiationCallDetailsById().subscribe((response: IApplicationResponse) => {
        let stockOrderModel = new DealerCallAddEditModel(response.resources);

        this.initiationDetailsData = response.resources;
        // if (this.callType == '1') {
        this.getContactListDropdown(response?.resources?.appointmentInfo ? response?.resources?.appointmentInfo?.quotationVersionId : null)
        // } else {
        //   this.contactList = []
        // }
        this.callInitiationContacts = response?.resources?.contacts ? response?.resources?.contacts : []
        if (this.callInitiationContacts.length > 0) {
          this.callInitiationContacts.forEach(element => {
            if (!element.keyholderId) {
              let newcontactData = {
                keyHolderId: element.callInitiationContactId,
                keyHolderName: element.contactName,
                contactNumber: element.contactNoCountryCode + ' ' + element.contactNo,
                isNewContact: true
              }

              // removing dublicate contact added
              // let contactExist = this.contactList.find(x => x.keyHolderName == newcontactData.keyHolderName && x.contactNumber == newcontactData.contactNumber)
              if (element.isPrimary) {
                this.contactList.push(newcontactData)
              } else {
                this.alternativContactList.push(newcontactData)
              }
            }
            if (this.callType == '2') { // service call
              if (element.keyholderId) {
                // let contactExist = this.contactList.find(x => x.keyholderId == element.keyholderId)
                // if (!contactExist) {
                //   let newcontactData = {
                //     keyHolderId: element.keyholderId,
                //     keyHolderName: element.contactName,
                //     contactNumber: element.contactNoCountryCode + ' ' + element.contactNo,
                //     isNewContact: true
                //   }
                //   this.contactList.push(newcontactData)
                // }
              }
            }
            if (element.isPrimary) {
              this.initiatonCallCreationForm.get('contactId').setValue(element.keyholderId ? element.keyholderId : element.callInitiationContactId)
              this.initiatonCallCreationForm.get('contactNumber').setValue(element.contactNumber ? element.contactNumber : element.contactNoCountryCode + ' ' + element.contactNo)
            } else {
              this.initiatonCallCreationForm.get('alternateContactId').setValue(element.keyholderId ? element.keyholderId : element.callInitiationContactId)
              this.initiatonCallCreationForm.get('alternateContactNumber').setValue(element.contactNumber ? element.contactNumber : element.contactNoCountryCode + ' ' + element.contactNo)

            }
          });
        }
        this.initiationBasicInfo = Object.assign({}, response?.resources?.basicInfo);
        // this.initiationLightingCover = response?.resources?.lightingCover;
        this.initiationServiceInfo = response?.resources?.serviceInfo;
        this.initiationAppointmentInfo = response?.resources?.appointmentInfo;
        // let basicInfo = Object.assign({}, response?.resources?.basicInfo);
        // if (partitions) {
        //   partitions = partitions?.filter(x => x.mainPartition != "True");
        // }


        if (response?.resources?.partitions?.length > 0) {
          this.initiatonCallCreationForm.controls['ismainPartation'].patchValue(response?.resources?.partitions?.find(x => x.mainPartition == true)?.callInitiationPartitionId == null ? false : true);
          this.initiatonCallCreationForm.controls['mainPartationCustomerId'].patchValue(response?.resources?.partitions?.find(x => x.mainPartition == true).partitionCode + ' - ' + response?.resources?.partitions?.find(x => x.mainPartition == true).partitionName)
          this.installationPartitionsFormArray.clear()
          for (let i = 0; i < response?.resources?.partitions?.length; i++) {
            if (response?.resources?.partitions[i]?.mainPartition == 'False') {
              response.resources.partitions[i].mainPartition = response?.resources?.partitions[i]?.callInitiationPartitionId == null ? false : true
              let addServiceSubTypeObj = this.formBuilder.group(response?.resources?.partitions[i])
              response.resources.partitions[i].callInitiationPartitionId == null ? addServiceSubTypeObj.enable() : addServiceSubTypeObj.enable()

              this.installationPartitionsFormArray.push(addServiceSubTypeObj);
            }

          }

        }
        if (response.resources?.basicInfo) {
          this.initiatonCallCreationForm.patchValue({
            "callInitiationId": response?.resources?.basicInfo?.callInitiationId,
            "customerId": response?.resources?.basicInfo?.customerId,
            // "quotationNumber": response?.resources?.basicInfo?.quotationNumber,
            "isNew": response?.resources?.basicInfo?.isNew,
            "isScheduled": response?.resources?.basicInfo?.isScheduled,
            "isCompleted": response?.resources?.basicInfo?.isCompleted,
            "isInvoiced": response?.resources?.basicInfo?.isInvoiced,
            // "isOnHold": response?.resources?.basicInfo?.isOnHold,
            "isVoid": response?.resources?.basicInfo?.isVoid,
            // "isReferToSales": response?.resources?.basicInfo?.isReferToSales,
            // "isInstallPreCheck": response?.resources?.basicInfo?.isInstallPreCheck,
            // "isInWarrenty": response?.resources?.basicInfo?.isInWarrenty,
            // "isRecall": response?.resources?.basicInfo?.isRecall,
            // "isCode50": response?.resources?.basicInfo?.isCode50,
            // "isPRCall": response?.resources?.basicInfo?.isPRCall,
            // "isSaveOffer": response?.resources?.basicInfo?.isSaveOffer,
            // "isCon": response?.resources?.basicInfo?.isCon,
            "isAddressVerified": response?.resources?.basicInfo?.isAddressVerified ? true : null,
            // "isAddressVerified": true,
            // "contact": response?.resources?.basicInfo?.contact,
            // "contactNumber": response?.resources?.basicInfo?.contactNumber,
            // "alternateContact": response?.resources?.basicInfo?.alternateContact,
            // "alternateContactNumber": response?.resources?.basicInfo?.alternateContactNumber,
            "priorityId": response?.resources?.basicInfo?.priorityId == 0 ? 100 : response?.resources?.basicInfo?.priorityId,
            "panelTypeConfigId": response?.resources?.basicInfo?.panelTypeConfigId ? { value: response?.resources?.basicInfo?.panelTypeConfigId } : '',
            // "specialProjectId": response?.resources?.basicInfo?.specialProjectId,
            // "projectEndDate": response?.resources?.basicInfo?.projectEndDate,
            "addressId": response?.resources?.basicInfo?.addressId,
            "partitionIds": [],
            // "techInstruction": response?.resources?.basicInfo?.techInstruction,
            // "notes": response?.resources?.basicInfo?.notes,
            "debtorId": response?.resources?.basicInfo?.debtorId,
            "feedbackId": response?.resources?.basicInfo?.feedbackId
          }, { emitEvent: false })
        }

        this.initiatonCallCreationForm.controls['serviceInfo'].patchValue({
          'callInitiationServiceInfoId': response?.resources?.serviceInfo?.callInitiationServiceInfoId,
          'systemTypeId': response?.resources?.serviceInfo?.systemTypeId ? response?.resources?.serviceInfo?.systemTypeId : '',
          'systemTypeSubCategoryId': response?.resources?.serviceInfo?.systemTypeSubCategoryId ? response?.resources?.serviceInfo?.systemTypeSubCategoryId : '',
          'serviceSubTypeId': response?.resources?.serviceInfo?.serviceSubTypeId ? response?.resources?.serviceInfo?.serviceSubTypeId : '',
          'faultDescriptionId': response?.resources?.serviceInfo?.faultDescriptionId ? response?.resources?.serviceInfo?.faultDescriptionId : '',
          'jobTypeId': response?.resources?.serviceInfo?.jobTypeId ? response?.resources?.serviceInfo?.jobTypeId : '',
          'description': response?.resources?.serviceInfo?.description,
        });
        if (response?.resources?.serviceInfo?.systemTypeId) {
          this.getSelfHelpConfigId(response?.resources?.serviceInfo?.systemTypeId);
        }
        // this.initiatonCallCreationForm.get('appointmentInfo.nextAvailableAppointment').setValue(response.resources?.appointmentInfo.nextAvailableAppointment ? new Date(response.resources?.appointmentInfo.nextAvailableAppointment) : null);
        if (this.initiationAppointmentInfo) {
          this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').setValue(response?.resources?.appointmentInfo?.scheduledStartDate ? new Date(response?.resources?.appointmentInfo?.scheduledStartDate) : null);
          this.initiatonCallCreationForm.get('appointmentInfo.scheduledEndDate').setValue(response?.resources?.appointmentInfo?.scheduledEndDate ? new Date(response?.resources?.appointmentInfo?.scheduledEndDate) : null);
          this.initiatonCallCreationForm.get('appointmentInfo.appointmentId').setValue(response?.resources?.appointmentInfo?.appointmentId ? response?.resources?.appointmentInfo?.appointmentId : null);
          this.initiatonCallCreationForm.get('appointmentInfo.appointmentLogId').setValue(response?.resources?.appointmentInfo?.appointmentLogId ? response?.resources?.appointmentInfo?.appointmentLogId : null);
          if (response?.resources?.appointmentInfo?.estimatedTime) {
            let estimatedTime = response?.resources?.appointmentInfo?.estimatedTime?.split(':')
            let estimatTimeIs = estimatedTime[0] + ':' + estimatedTime[1]
            this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(estimatTimeIs);
            this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(estimatTimeIs);
            this.initiationDetailsData.appointmentInfo.estimatedTime = estimatTimeIs
            this.initiationDetailsData.appointmentInfo.estimatedTimeStatic = estimatTimeIs
          } else {
            this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(null);
            this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(null);
          }
          this.initiatonCallCreationForm.get('appointmentInfo.technician').setValue(response?.resources?.appointmentInfo?.technician ? response?.resources?.appointmentInfo?.technician : "");
          this.initiatonCallCreationForm.get('appointmentInfo.actualTime').setValue(response?.resources?.appointmentInfo?.actualTime ? response?.resources?.appointmentInfo?.actualTime : null);
          // this.initiatonCallCreationForm.get('appointmentInfo.preferredDate').setValue(response?.resources?.appointmentInfo?.preferredDate ? new Date(response?.resources?.appointmentInfo?.preferredDate) : null);
          // this.initiatonCallCreationForm.get('appointmentInfo.preferredTime').setValue(response?.resources?.appointmentInfo?.preferredTime ? response?.resources?.appointmentInfo?.preferredTime : null);
          // this.initiatonCallCreationForm.get('appointmentInfo.isAppointmentChangable').setValue(response?.resources?.appointmentInfo?.isAppointmentChangable ? response?.resources?.appointmentInfo?.isAppointmentChangable : false);
          // this.initiatonCallCreationForm.get('appointmentInfo.techArea').setValue(response?.resources?.appointmentInfo?.techArea ? response?.resources?.appointmentInfo?.techArea : null);
          // this.initiatonCallCreationForm.get('appointmentInfo.actualTime').setValue(response?.resources?.appointmentInfo?.actualTime ? response?.resources?.appointmentInfo?.actualTime : null);
          // this.initiatonCallCreationForm.get('appointmentInfo.isAppointmentChangable').setValue(response?.resources?.appointmentInfo?.isAppointmentChangable ? response?.resources?.appointmentInfo?.isAppointmentChangable : false);
          // this.initiatonCallCreationForm.get('appointmentInfo.techArea').setValue(response?.resources?.appointmentInfo?.techArea ? response?.resources?.appointmentInfo?.techArea : null);
        }
        // this.initiatonCallCreationForm.get('nkaServiceCall').patchValue({
        //   isPOAccepted: this.initiationDetailsData?.nkaServiceCall?.isPOAccepted == true,
        //   isPONotAccepted: this.initiationDetailsData?.nkaServiceCall?.isPOAccepted == false,
        //   poNumber: this.initiationDetailsData?.nkaServiceCall ? this.initiationDetailsData?.nkaServiceCall?.poNumber : '',
        //   poAttachments: this.initiationDetailsData?.nkaServiceCall ? this.initiationDetailsData?.nkaServiceCall?.docName : '',
        //   attachmentUrl: this.initiationDetailsData?.nkaServiceCall?.path ? this.initiationDetailsData?.nkaServiceCall?.path : '',
        //   poAmount: this.initiationDetailsData?.nkaServiceCall?.poAmount ? this.otherService.transformDecimal(this.initiationDetailsData?.nkaServiceCall?.poAmount) : '',
        //   poComment: this.initiationDetailsData?.nkaServiceCall ? this.initiationDetailsData?.nkaServiceCall?.poComment : '',
        // })

        // this.initiatonCallCreationForm.get('nkaServiceCall.isPOAccepted').valueChanges
        //   .subscribe(value => {
        //     if (value == true) {
        //       this.initiatonCallCreationForm.get('nkaServiceCall.isPONotAccepted').setValue(null, { emitEvent: false });
        //       this.initiatonCallCreationForm.get('nkaServiceCall.isPOAccepted').setValue(true, { emitEvent: false });
        //     } else if (value == false) {
        //       this.initiatonCallCreationForm.get('nkaServiceCall.isPOAccepted').setValue(false, { emitEvent: false });
        //     }
        //   });
        // this.initiatonCallCreationForm.get('nkaServiceCall.isPONotAccepted').valueChanges
        //   .subscribe(value => {
        //     if (value == true) {
        //       this.initiatonCallCreationForm.get('nkaServiceCall.isPOAccepted').setValue(null, { emitEvent: false });
        //       this.initiatonCallCreationForm.get('nkaServiceCall.isPONotAccepted').setValue(true, { emitEvent: false });
        //     } else if (value == false) {
        //       this.initiatonCallCreationForm.get('nkaServiceCall.isPONotAccepted').setValue(false, { emitEvent: false });
        //     }
        //   });
        // if (this.initiationDetailsData?.nkaServiceCall?.isPOAccepted == false || this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po rejected' || this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po declined') {
        //   this.onNKAReSubmit();
        // }
        // if (this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po required' && this.initiationDetailsData?.basicInfo?.isNKACustomer && this.userData?.userId == this.initiationDetailsData?.nkaServiceCall?.ownedUserId) {
        //   this.initiatonCallCreationForm.get('nkaServiceCall.path').enable();
        //   this.initiatonCallCreationForm.get('nkaServiceCall.poComment').enable();
        // } else {
        //   this.initiatonCallCreationForm.get('nkaServiceCall.poNumber').disable();
        //   this.initiatonCallCreationForm.get('nkaServiceCall.poAmount').disable();
        //   this.initiatonCallCreationForm.get('nkaServiceCall.path').disable();
        //   this.initiatonCallCreationForm.get('nkaServiceCall.poComment').disable();
        // }
        // if (this.isPOInfoDisabled()) {
        //   this.initiatonCallCreationForm.get('nkaServiceCall.poNumber').enable();
        //   this.initiatonCallCreationForm.get('nkaServiceCall.poAmount').enable();
        // }
        if (this.initiationDetailsData?.execuGuardServiceCall?.execuGuardServiceCallStatusName?.toLowerCase() == 'declined' && this.initiationDetailsData?.basicInfo?.isExecuGuardCustomer) {
          this.onExecuReSubmit();
        }
      },
        error => {

        });
      // this.getNextavailableAppointment()
    }

    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onCallInitiationValueChanges() {
    this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').valueChanges.subscribe((val) => {

      if (this.callType == '1') {
        return
      }
      if (val) {
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_SYSTEM_TYPE_SUB_CATEGORY, undefined, null, prepareGetRequestHttpParams(null, null,
            { SystemTypeId: val, ShowDescriptionOnly: true }
          )).subscribe((response) => {
            this.SystemTypeSubCategoryList = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);

          })

      }
      else {
        this.SystemTypeSubCategoryList = [];
        this.ServiceSubTypeList = [];
        // this.faultDescriptionList = [];
        this.jobTypeList = [];
      }
    })

    // this.initiatonCallCreationForm.get('appointmentInfo.nextAvailableAppointmentCheckbox').valueChanges.subscribe((val) => {
    //   if (val) {
    //     this.onSelectTime(this.initiatonCallCreationForm.get('appointmentInfo.nextAvailableAppointment').value)
    //   } else {
    //     this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').setValue(this.initiationAppointmentInfo ? this.initiationAppointmentInfo.scheduledStartDate ? new Date(this.initiationAppointmentInfo.scheduledStartDate) : null : null)
    //     this.initiatonCallCreationForm.get('appointmentInfo.scheduledEndDate').setValue(this.initiationAppointmentInfo ? this.initiationAppointmentInfo.scheduledEndDate ? new Date(this.initiationAppointmentInfo.scheduledEndDate) : null : null)
    //   }
    // })

    this.initiatonCallCreationForm.get('serviceInfo.systemTypeSubCategoryId').valueChanges.subscribe((val) => {
      if (this.callType == '1') {
        return
      }
      if (val) {
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.UX_SERVICE_SUB_TYPES, undefined, null, prepareGetRequestHttpParams(null, null,
            { SystemTypeSubCategoryId: val, ShowDescriptionOnly: true }
          )).subscribe((response) => {
            this.ServiceSubTypeList = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);

          })

      }
      else {
        this.ServiceSubTypeList = [];
        // this.faultDescriptionList = [];
        this.jobTypeList = [];
      }
    })

    this.initiatonCallCreationForm.get('serviceInfo.serviceSubTypeId').valueChanges.subscribe((val) => {
      if (this.callType == '1') {
        return
      }
      this.serviceSubTypeId = val;
      if (val) {
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.FAULT_DESCRIPTION_SERVICE_SUB_TYPES, undefined, null, prepareGetRequestHttpParams(null, null,
            { ServiceSubTypeId: val, ShowMapped: true }
          )).subscribe((response) => {
            this.faultDescriptionList = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);

          })
      }
      else {
        // this.faultDescriptionList = [];
        this.jobTypeList = [];
      }
    })

    this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').valueChanges.subscribe((val) => {

      if (this.callType == '1') { // initiation call
        return
      }
      if (val) {
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
          // TechnicalMgntModuleApiSuffixModels.SKILL_MATRIX_CONFIG_JOBTYPE_SKILL_LEVEL, this.initiatonCallCreationForm.value.serviceInfo.serviceSubTypeId == '' ? this.serviceSubTypeId : this.initiatonCallCreationForm.value.serviceInfo.serviceSubTypeId
          TechnicalMgntModuleApiSuffixModels.SKILL_MATRIX_CONFIG_JOBTYPE_SKILL_LEVEL, undefined, null
          , prepareGetRequestHttpParams(null, null,
            {
              ServiceSubTypeId: this.initiatonCallCreationForm.value.serviceInfo.serviceSubTypeId,
              FaultDescriptionId: val
            }
          )
        ).subscribe((response) => {
          this.jobTypeList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
          let faultDesc = this.faultDescriptionList.find(x => x.faultDescriptionId == val)

          if (faultDesc) {
            // this.jobTypeList = [
            //   {
            //     estimatedTime: faultDesc.estimatedTime,
            //     jobTypeId: faultDesc.jobTypeId,
            //     jobTypeName: faultDesc.jobTypeName
            //   }
            // ];

            this.initiatonCallCreationForm.get('serviceInfo.jobTypeId').setValue(faultDesc.jobTypeId ? faultDesc.jobTypeId : this.initiationServiceInfo.jobTypeId)
          }
          // let jobType = this.jobTypeList.find(x => x.jobTypeId == this.initiatonCallCreationForm.get('serviceInfo.jobTypeId').value)
          // if (this.callType == '2' || this.callType == '3') { // service call
          //   if (jobType) {
          //     let estimatedSplt = jobType.estimatedTime.split(':')
          //     this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(jobType ? estimatedSplt[0] + ':' + estimatedSplt[1] : null)
          //     this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(jobType ? estimatedSplt[0] + ':' + estimatedSplt[1] : null)
          //   } else {
          //     this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(this.initiationDetailsData.appointmentInfo ? this.initiationDetailsData.appointmentInfo.estimatedTime : null)
          //     this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(this.initiationDetailsData.appointmentInfo ? this.initiationDetailsData.appointmentInfo.estimatedTime : null)
          //     this.initiatonCallCreationForm.get('serviceInfo.jobTypeId').setValue(null)
          //   }
          // } else {
          //   this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(this.initiationDetailsData.appointmentInfo ? this.initiationDetailsData.appointmentInfo.estimatedTime : null)
          //   this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(this.initiationDetailsData.appointmentInfo ? this.initiationDetailsData.appointmentInfo.estimatedTime : null)
          // }
          // if (this.callType == '2') {
          //   this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(jobType ? jobType.estimatedCallTime : null)
          // } else {
          //   this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(this.initiationDetailsData.appointmentInfo ? this.initiationDetailsData.appointmentInfo.estimatedTime : null)

          // }
          // this.getNextavailableAppointment()
        })

      }
      else {
        this.jobTypeList = [];
        // this.getNextavailableAppointment()
      }
    })

    this.initiatonCallCreationForm.get('serviceInfo.jobTypeId').valueChanges.subscribe(val => {
      if (val) {
        let jobType = this.jobTypeList.find(x => x.jobTypeId == Number(val))
        if (this.callType == '2' || this.callType == '3') { // service call

          if (jobType) {
            if (this.isManualChange) {
              let estimatedSplt = jobType.estimatedTime?.split(':')
              this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(jobType ? estimatedSplt[0] + ':' + estimatedSplt[1] : null)
              this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(jobType ? estimatedSplt[0] + ':' + estimatedSplt[1] : null)
            } else {
              if (this.initiationDetailsData?.appointmentInfo?.estimatedTime) {
                this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(this.initiationDetailsData?.appointmentInfo?.estimatedTime)
              } else {
                let estimatedSplt = jobType.estimatedTime?.split(':')
                this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(jobType ? estimatedSplt[0] + ':' + estimatedSplt[1] : null)
                this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(jobType ? estimatedSplt[0] + ':' + estimatedSplt[1] : null)
              }
            }
          } else {
            this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(this.initiationDetailsData?.appointmentInfo ? this.initiationDetailsData?.appointmentInfo?.estimatedTime : null)
            this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').setValue(this.initiationDetailsData?.appointmentInfo ? this.initiationDetailsData?.appointmentInfo?.estimatedTime : null)
          }
        } else {
          // this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(this.initiationDetailsData.appointmentInfo ? this.initiationDetailsData.appointmentInfo.estimatedTime : null)
        }
        // this.getNextavailableAppointment()
      }
    })

    this.initiatonCallCreationForm.get('contactId').valueChanges.subscribe(val => {
      if (!val) {
        this.initiatonCallCreationForm.get('contactNumber').setValue('');
        return
      }
      let contact = this.contactList.find(x => x.keyHolderId == val)
      if (contact) {

        // this.initiatonCallCreationForm.get('contactNumber').setValue(contact ? contact.contactNumber : null)

        this.contactNumberList = []
        if (contact.mobile1) {
          this.contactNumberList.push({ id: contact.mobile1, value: contact.mobile1 })
        }
        if (contact.mobile2) {
          this.contactNumberList.push({ id: contact.mobile2, value: contact.mobile2 })
        }
        if (contact.officeNo) {
          this.contactNumberList.push({ id: contact.officeNo, value: contact.officeNo })
        }
        if (contact.premisesNo) {
          this.contactNumberList.push({ id: contact.premisesNo, value: contact.premisesNo })
        }
        if (contact.isNewContact) {
          if (contact.contactNumber) { //for new contact
            this.contactNumberList.push({ id: contact.contactNumber, value: contact.contactNumber })
          }
        }


        let contactData = {
          // keyholderId: contact.keyHolderId,
          keyholderId: contact.isNewContact ? null : contact.keyHolderId,
          contactName: contact.keyHolderName,
          contactNo: contact.contactNumber.substr(contact.contactNumber.indexOf(' ') + 1),
          contactNoCountryCode: contact.contactNumber.substr(0, contact.contactNumber.indexOf(' ')),
          isPrimary: true,
          callInitiationId: this.initiationId,
          callInitiationContactId: null,
          createdUserId: this.userData.userId
        }

        let primaryData = this.callInitiationContacts.find(x => x.isPrimary == true)
        if (primaryData) {
          this.callInitiationContacts.find(x => x.isPrimary == true).keyholderId = contact.isNewContact ? null : contact.keyHolderId
          this.callInitiationContacts.find(x => x.isPrimary == true).contactName = contact.keyHolderName
          this.callInitiationContacts.find(x => x.isPrimary == true).contactNo = contact.contactNumber.substr(contact.contactNumber.indexOf(' ') + 1)
          this.callInitiationContacts.find(x => x.isPrimary == true).contactNoCountryCode = contact.contactNumber.substr(0, contact.contactNumber.indexOf(' '))
          // this.callInitiationContacts.find(x => x.isPrimary == true).callInitiationContactId = contact.isNewContact ? contact.keyHolderId : primaryData.callInitiationContactId ? primaryData.callInitiationContactId : null
          // this.callInitiationContacts.find(x => x.isPrimary == true).callInitiationContactId = contact.isNewContact ? null : contact.keyHolderId
          // this.callInitiationContacts.find(x => x.isPrimary == true).callInitiationContactId = contact.isNewContact ? (primaryData.callInitiationContactId ? primaryData.callInitiationContactId : null) : contact.keyHolderId
          this.callInitiationContacts.find(x => x.isPrimary == true).createdUserId = this.userData.userId
        } else {
          this.callInitiationContacts.push(contactData)
        }
      }
    })

    this.initiatonCallCreationForm.get('alternateContactId').valueChanges.subscribe(val => {
      if (!val) {
        this.initiatonCallCreationForm.get('alternateContactNumber').setValue('');
        this.initiatonCallCreationForm = clearFormControlValidators(this.initiatonCallCreationForm, ["alternateContactNumber"]);
        return
      }
      this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["alternateContactNumber"]);
      this.initiatonCallCreationForm.get('alternateContactNumber').updateValueAndValidity()
      let contact = this.alternativContactList.find(x => x.keyHolderId == val)
      if (contact) {
        // this.initiatonCallCreationForm.get('alternateContactNumber').setValue(contact ? contact?.contactNumber : null)
        this.contactAlternativeNumberList = []
        if (contact.mobile1) {
          this.contactAlternativeNumberList.push({ id: contact.mobile1, value: contact.mobile1 })
        }
        if (contact.mobile2) {
          this.contactAlternativeNumberList.push({ id: contact.mobile2, value: contact.mobile2 })
        }
        if (contact.officeNo) {
          this.contactAlternativeNumberList.push({ id: contact.officeNo, value: contact.officeNo })
        }
        if (contact.premisesNo) {
          this.contactAlternativeNumberList.push({ id: contact.premisesNo, value: contact.premisesNo })
        }
        if (contact.isNewContact) {
          if (contact.contactNumber) { //for new contact
            this.contactAlternativeNumberList.push({ id: contact.contactNumber, value: contact.contactNumber })
          }
        }

        let contactData = {
          // keyholderId: contact?.keyHolderId,
          keyholderId: contact.isNewContact ? null : contact.keyHolderId,
          contactName: contact?.keyHolderName,
          contactNo: contact?.contactNumber?.substr(contact?.contactNumber?.indexOf(' ') + 1),
          contactNoCountryCode: contact?.contactNumber?.substr(0, contact?.contactNumber?.indexOf(' ')),
          isPrimary: false,
          callInitiationId: this.initiationId,
          callInitiationContactId: null,
          createdUserId: this.userData.userId
        }
        let primaryData = this.callInitiationContacts.find(x => x.isPrimary == false)
        if (primaryData) {
          this.callInitiationContacts.find(x => x.isPrimary == false).keyholderId = contact.isNewContact ? null : contact.keyHolderId
          this.callInitiationContacts.find(x => x.isPrimary == false).contactName = contact.keyHolderName
          this.callInitiationContacts.find(x => x.isPrimary == false).contactNo = contact.contactNumber.substr(contact.contactNumber.indexOf(' ') + 1)
          this.callInitiationContacts.find(x => x.isPrimary == false).contactNoCountryCode = contact.contactNumber.substr(0, contact.contactNumber.indexOf(' '))
          // this.callInitiationContacts.find(x => x.isPrimary == false).callInitiationContactId = contact.isNewContact ? contact.keyHolderId : primaryData.callInitiationContactId ? primaryData.callInitiationContactId : null
          // this.callInitiationContacts.find(x => x.isPrimary == false).callInitiationContactId = contact.isNewContact ? null : contact.keyHolderId
          // this.callInitiationContacts.find(x => x.isPrimary == false).callInitiationContactId = contact.isNewContact ? (primaryData.callInitiationContactId ? primaryData.callInitiationContactId : null) : contact.keyHolderId
          this.callInitiationContacts.find(x => x.isPrimary == false).createdUserId = this.userData.userId
        } else {
          this.callInitiationContacts.push(contactData)
        }
      }
    })

    this.initiatonCallCreationForm.get('contactNumber').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      let primaryData = this.callInitiationContacts.find(x => x.isPrimary == true)
      if (primaryData) {
        this.callInitiationContacts.find(x => x.isPrimary == true).contactNo = val.substr(val.indexOf(' ') + 1)
        this.callInitiationContacts.find(x => x.isPrimary == true).contactNoCountryCode = val.substr(0, val.indexOf(' '))
        this.callInitiationContacts.find(x => x.isPrimary == true).createdUserId = this.userData.userId
      }

    })

    this.initiatonCallCreationForm.get('alternateContactNumber').valueChanges.subscribe(val => {
      if (!val) {
        return
      }
      let primaryData = this.callInitiationContacts.find(x => x.isPrimary == false)
      if (primaryData) {
        this.callInitiationContacts.find(x => x.isPrimary == false).contactNo = val.substr(val.indexOf(' ') + 1)
        this.callInitiationContacts.find(x => x.isPrimary == false).contactNoCountryCode = val.substr(0, val.indexOf(' '))
      }

    })

    // this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').valueChanges.
    // subscribe(val => {
    //   if (val) {
    //     this.getNextavailableAppointment()
    //   }
    // })

    // this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').valueChanges
    //   .pipe(
    //     // tap(val=>val.length == 5),
    //     filter(val => val ? val.length == 5 : null),
    //     takeUntil(this.ngUnsubscribe),
    //     debounceTime(debounceTimeForSearchkeyword),
    //     distinctUntilChanged(),
    //     switchMap(val => {
    //       if (!val) {
    //         return
    //       }
    //       // if(val.length != 5){
    //       //   return
    //       // }
    //       return of(this.getNextavailableAppointment());
    //     })
    //   )
    //   .subscribe();


    this.initiatonCallCreationForm.get('specialProjectId').valueChanges.subscribe(val => {
      if (val) {
        let projectData = this.projectList.find(x => x.id.toLowerCase() == val.toLowerCase());
        if (!projectData) return;
        this.initiationBasicInfo.projectEndDate = projectData['projectEndDate'];
      }
    })


    this.initiatonCallCreationForm.get('feedbackId').valueChanges.subscribe(val => {
      if (val) {
        let feedBackData = this.feedBackList.find(x => x.id == val)?.isReferToFADT;
        // this.alertDialog = true
        if(feedBackData) {
          this.alertMsg = "By reffering the service call to Fidelity-ADT, I have acknowledge that the service call call-out fee as well as any warranty equipment replaced will be invoiced to DEALERS NAME'S Debtors Accoutn and DEALERS NAME will be liable for the invoice raised."
          const dialogData = { title: 'Alert', message: this.alertMsg, type: 'feedback', OkButtonName: 'Acknowledge', CancelButtonName: 'Cancel', isConfirm: true, isClose: true, msgCenter: true, isAlert: false };
          this.openConfigDialog(dialogData);
        }
      }
    })
  }

  openConfigDialog(dialogData) {
    const dialogRef = this.dialogService.open(PrimengConfirmDialogPopupComponent, {
      width: "550px",
      data: dialogData,
      showHeader: false,
    });
    dialogRef.onClose.subscribe(result => {
      if (!result) {
        if(dialogData?.type == 'feedback') {
          this.initiatonCallCreationForm.get('feedbackId').setValue('', {emitEvent: false});
        }
        return;
      } else {
        return;
      }
    });
  }
  // onChangeEstimatedTime(event) {
  //   if (!event.target.value) {
  //     return
  //   }
  //   this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(event.target.value)
  //   // this.getNextavailableAppointment()
  // }

  getRating() {
    if (this?.initiationDetailsData) {
      return this?.initiationDetailsData?.basicInfo?.callRating ?
        Math.round(this?.initiationDetailsData?.basicInfo?.callRating) : '';
    }
  }

  createAddNewContactForm() {
    this.addNewContactForm = this.formBuilder.group({
      contactName: ['', Validators.required],
      contactNoCountryCode: ['+27', Validators.required],
      contactNo: ['', [Validators.required, Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]],
      isPrimary: [null, Validators.required],
      callInitiationId: this.initiationId ? this.initiationId : null,
      keyholderId: null,
      callInitiationContactId: null,
      customerId: this.customerId ? this.customerId : null,
      createdUserId: this.userData.userId

    })
    this.addNewContactForm.get('contactNoCountryCode').valueChanges.subscribe((contactNoCountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(contactNoCountryCode);
    });
    this.addNewContactForm.get('contactNo').valueChanges.subscribe(() => {
      this.setPhoneNumberLengthByCountryCode(this.addNewContactForm.get('contactNoCountryCode').value, false);
    });
  }

  setPhoneNumberLengthByCountryCode(countryCode: string, isSelectedcountryCode = true) {
    switch (countryCode) {
      case "+27":
        this.addNewContactForm.get('contactNo').setValidators([Validators.required, Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        this.addNewContactForm.updateValueAndValidity();
        if(isSelectedcountryCode && !this.addNewContactForm.get('contactNo')?.invalid) {
          this.showErrorOnSelectCountryCode(formConfigs.southAfricanContactNumberMaxLength);
        }
        break;
      default:
        this.addNewContactForm.get('contactNo').setValidators([Validators.required, Validators.minLength(formConfigs.indianContactNumberMaxLength),
        Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        this.addNewContactForm.updateValueAndValidity();
        if(isSelectedcountryCode && !this.addNewContactForm.get('contactNo')?.invalid) {
          this.showErrorOnSelectCountryCode(formConfigs.indianContactNumberMaxLength);
        }
        break;
    }
  }

  showErrorOnSelectCountryCode(maxLength: number) {
    const number = this.addNewContactForm.get('contactNo').value?.replace(/ /g, "");
    if (number?.toString()?.length > maxLength) {
      this.addNewContactForm.get('contactNo').setErrors({ maxlength: {
        requiredLength: maxLength
      } });
    } else if (number?.toString()?.length < maxLength) {
      this.addNewContactForm.get('contactNo').setErrors({ minlength: {
        requiredLength: maxLength
      } });
    } else {
      this.addNewContactForm.get('contactNo').setErrors(null);
    }
    this.addNewContactForm.updateValueAndValidity();
    return false;
  }

  createPrCallForm() {
    this.prCallForm = this.formBuilder.group({
      serviceCallRequestId: [''],
      callInitiationId: [this.initiationId, Validators.required],
      motivation: ['', Validators.required],
      createdUserId: this.userData.userId
    })
  }

  createCallCreationTemplateForm(callCreationTemplateModel?: CallCreationTemplateModel) {
    let callCreationTemplateModelControl = new CallCreationTemplateModel(callCreationTemplateModel);
    this.callCreationTemplateForm = this.formBuilder.group({
      callCreationList: this.formBuilder.array([])
    });
    Object.keys(callCreationTemplateModelControl).forEach((key) => {
      this.callCreationTemplateForm.addControl(key, new FormControl(callCreationTemplateModelControl[key]));
    });

  }

  createActualTimingForm(actualTimingModel?: ActualTimingModel) {
    let actualTimingModelControl = new ActualTimingModel(actualTimingModel);
    this.actualTimingForm = this.formBuilder.group({
      actualTimingList: this.formBuilder.array([])
    });
    Object.keys(actualTimingModelControl).forEach((key) => {
      this.actualTimingForm.addControl(key, new FormControl(actualTimingModelControl[key]));
    });

  }



  //Create FormArray
  get getcallCreationListArray(): FormArray {
    if (!this.callCreationTemplateForm) return;
    return this.callCreationTemplateForm.get("callCreationList") as FormArray;
  }

  //Create FormArray
  get getactualTimingListArray(): FormArray {
    if (!this.actualTimingForm) return;
    return this.actualTimingForm.get("actualTimingList") as FormArray;
  }


  //Create FormArray controls
  createcallCreationTemplateListModel(callCreationListModel?: CallCreationTemplateListModel): FormGroup {
    let callCrationListFormControlModel = new CallCreationTemplateListModel(callCreationListModel);
    let formControls = {};
    Object.keys(callCrationListFormControlModel).forEach((key) => {
      if (key == 'value') {
        // if (callCrationListFormControlModel['questionName'].toLowerCase() == 'preferred timing' || callCrationListFormControlModel['questionName'].toLowerCase() == 'preferred time' || callCrationListFormControlModel['questionName'].toLowerCase() == 'trouble shooting') {
        if (callCrationListFormControlModel['questionName'].toLowerCase() == 'trouble shooting') {
          formControls[key] = [{ value: callCrationListFormControlModel[key], disabled: false }]
        } else {
          if (callCrationListFormControlModel['isYesNo'] && !callCrationListFormControlModel[key]) {
            formControls[key] = [{ value: callCrationListFormControlModel[key], disabled: false }]
          } else {
            formControls[key] = [{ value: callCrationListFormControlModel[key], disabled: false }, [Validators.required]]
          }
        }
      }
      //  else if (key == 'isTogglevalue') {
      // if (!callCrationListFormControlModel['isDropdown'] && !callCrationListFormControlModel['isTextBox'] && callCrationListFormControlModel['isYesNo']) {
      //   formControls[key] = [{ value: null, disabled: false }, [Validators.required]]
      //   } else {
      //   formControls[key] = [{ value: callCrationListFormControlModel[key], disabled: false }]
      // }
      // }
      else {
        formControls[key] = [{ value: callCrationListFormControlModel[key], disabled: false }]
      }
    });
    let formContrlsGroup = this.formBuilder.group(formControls)
    if (formContrlsGroup.get('isYesNo').value) {
      formContrlsGroup.get('isTogglevalue').setValue((formContrlsGroup.get('value').value && (formContrlsGroup.get('value').value == 'true' || formContrlsGroup.get('value').value == true)) ? true : false);
    };
    formContrlsGroup.get('isTogglevalue').valueChanges.subscribe(val => {
      if (val) {
        if (!formContrlsGroup.get('isTextBox').value) {
          // formContrlsGroup.get('value').setValue(formContrlsGroup.get('isTextBox').value)
          formContrlsGroup.get('value').setValue(val)
        }
        formContrlsGroup.get('value').setValidators([Validators.required])
      } else {
        formContrlsGroup.get('value').setValidators([])
        formContrlsGroup.get('value').setValue((val))
      }
      formContrlsGroup.get('value').updateValueAndValidity()

    })
    return formContrlsGroup;
  }

  onRedirectTroubleShootChekAutoSave() {
    if (!this.initiationId) {
      this.autoSaveAlertNavigate(2) // 2- redirect to troubleshoot
      return
    } else {
      this.onRedirectTroubleShoot()
    }
  }

  onRedirectTroubleShoot() {
    if (this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value
      && !this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').value && !this.selfHelpSystemTypeConfigId) {
      this.router.navigate(['/customer/manage-customers/trouble-shoot-alarm'], { queryParams: { id: this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value } });
    } else if (this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value && this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').value && this.selfHelpSystemTypeConfigId) {
      this.router.navigate(['/customer/manage-customers/trouble-shoot-alarm-quetion-answer'], { queryParams: { id: this.selfHelpSystemTypeConfigId, systemId: this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value } });
    } else {
      this.router.navigate(['/customer/manage-customers/trouble-shoot-system'], { queryParams: { customerId: this.customerId } });
    }
  }

  getSelfHelpConfigId(systemTypeId) {
    if (this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value) {
      this.crudService.get(ModulesBasedApiSuffix.SHARED,
        BillingModuleApiSuffixModels.FAULT_DESCRIPTION_SYSTEM_TYPES, systemTypeId ? systemTypeId : this.initiatonCallCreationForm.get('serviceInfo.systemTypeId').value, false, null, 1)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200 && response.resources?.length) {
            this.selfHelpSystemTypeConfigId = response.resources[0]?.selfHelpSystemTypeConfigId;
          }
        })
    }
  }

  //Create FormArray controls
  createactualTimingListModel(actualTimingListModel?: ActualTimingListModel): FormGroup {
    let actualTimingListModelContol = new ActualTimingListModel(actualTimingListModel);
    let formControls = {};
    Object.keys(actualTimingListModelContol).forEach((key) => {
      if (key == 'onRouteDate') { //  || key == 'startTime' || key == 'endTime'
        formControls[key] = [{ value: actualTimingListModelContol[key], disabled: false }, [Validators.required]]
      } else {
        formControls[key] = [{ value: actualTimingListModelContol[key], disabled: false }]
      }
    });
    let formContrlsGroup = this.formBuilder.group(formControls)

    formContrlsGroup.get('startTime').valueChanges.subscribe(val => {
      if (!val) {
        return
      }

      let endTimeMinTime = this.momentService.toMoment(this.momentService.addMinits(val, 1)).seconds(0).format();
      formContrlsGroup.get('endTimeMinDate').setValue(endTimeMinTime, { emitEvent: false });
      formContrlsGroup.get('endTime').setValue(null, { emitEvent: false });
      formContrlsGroup.get('actualTime').setValue(null, { emitEvent: false });
    })
    formContrlsGroup.get('endTime').valueChanges.subscribe(val => {
      if (!val) {
        return
      }

      let currentStartTime = this.momentService.toMoment(formContrlsGroup.get('startTime').value).seconds(0).format();
      let currentEndTime = this.momentService.toMoment(formContrlsGroup.get('endTime').value).seconds(0).format();
      // let addToMints1 = this.momentService.getTimeDiff(formContrlsGroup.get('startTime').value, formContrlsGroup.get('endTime').value)
      let addToMints1 = this.momentService.getTimeDiff(currentStartTime, currentEndTime);
      formContrlsGroup.get('actualTime').setValue(addToMints1);
    })
    formContrlsGroup.get('onRouteDate').valueChanges.subscribe(val => {
      if (val) {
        let appointmentLogData = this.actualTimingDateList.find(element => element['displayName'] == val);
        formContrlsGroup.get('appointmentLogId').setValue(appointmentLogData['id'] ? appointmentLogData['id'] : '');
      } else {
        formContrlsGroup.get('appointmentLogId').setValue('');
      }
    })
    formContrlsGroup.get('onRouteTime').valueChanges.subscribe(val => {
      if (!val) {
        return;
      }
      // actualTimingListModel.startTimeMinDate = actualTimingListModel.onRouteTime ?  this.momentService.toMoment(this.momentService.addMinits(actualTimingListModel.onRouteTime, 1)).seconds(0).format() : null;
      let startTimeMinTime = this.momentService.toMoment(this.momentService.addMinits(val, 1)).seconds(0).format();
      formContrlsGroup.get('startTimeMinDate').setValue(startTimeMinTime, { emitEvent: false });
      formContrlsGroup.get('startTime').setValue(null, { emitEvent: false });
      formContrlsGroup.get('endTime').setValue(null, { emitEvent: false });
      formContrlsGroup.get('actualTime').setValue(null, { emitEvent: false });
    })
    return formContrlsGroup;
  }



  onChangeActualTime(key, val, i) {
    this.actualTimingList.value.forEach((element, index) => {
      if (i != index) {
        let onRouteDate = this.getactualTimingListArray.controls[i].get('onRouteDate').value;
        let routeDateSplit = onRouteDate.split('-');
        let convertedOnRouteDate = new Date(routeDateSplit[2], Number(routeDateSplit[1]) - 1, routeDateSplit[0]);

        let elementOnRouteDate = element['onRouteDate'];
        let elementRouteDateSplit = onRouteDate.split('-');
        let convertedElementOnRouteDate = new Date(elementRouteDateSplit[2], Number(elementRouteDateSplit[1]) - 1, elementRouteDateSplit[0]);
        if (+convertedOnRouteDate != +convertedElementOnRouteDate) {

        } else {
          let currentChangedTime = this.momentService.toMoment(val.value).seconds(0).format();
          let exist = this.momentService.betWeenDateTime(element.startTime, element.endTime, currentChangedTime);
          let isStartSameDay = this.momentService.isSameDayTime(element.startTime, currentChangedTime);
          let isEndSameDay = this.momentService.isSameDayTime(element.endTime, currentChangedTime);
          if (exist || isStartSameDay || isEndSameDay) {
            this.getactualTimingListArray.controls[i].get(key).setValue(null, { emitEvent: false });
            this.snackbarService.openSnackbar('Time already exist', ResponseMessageTypes.WARNING);
          }
        }
      }
    });
  }

  onChangeOnRouteDate(val, i) {

  }
  onSubmitCallCreationTemplate() {
    if (this.getEditPermission()) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.callCreationTemplateForm.invalid) {
      return
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.callCreationDialog = false


  }

  //Add Details
  addActualTiming(): void {
    if (!this.getActualTimingActionPermission(PermissionTypes?.ADD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.getactualTimingListArray.invalid) {
      this.getactualTimingListArray.markAllAsTouched();
      // this.focusInAndOutFormArrayFields();
      return;
    };

    this.actualTimingList = this.getactualTimingListArray;
    let actualTimingListModel = new ActualTimingListModel(null);
    actualTimingListModel.appointmentId = this.initiationDetailsData?.appointmentInfo?.appointmentId ? this.initiationDetailsData?.appointmentInfo?.appointmentId : ''
    // actualTimingListModel.onRouteDate = this.initiationDetailsData.appointmentInfo.scheduledStartDate ? this.initiationDetailsData.appointmentInfo.scheduledStartDate : ""
    // actualTimingListModel.onRouteDate = actualTimingListModel.onRouteDate && actualTimingListModel.onRouteDate.split('T').length > 0 ? actualTimingListModel.onRouteDate.split('T')[0]: '';
    actualTimingListModel.onRouteDate = "";
    actualTimingListModel.createdUserId = this.userData.userId ? this.userData.userId : ''
    actualTimingListModel.modifiedUserId = this.userData.userId ? this.userData.userId : ''
    this.actualTimingList.insert(0, this.createactualTimingListModel(actualTimingListModel));

    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeActualTiming(i: number): void {
    if (!this.getActualTimingActionPermission(PermissionTypes?.DELETE)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.getactualTimingListArray.controls[i].value.appointmentActualTimingId) {
      this.getactualTimingListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {

      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getactualTimingListArray.controls[i].value.appointmentActualTimingId && this.getactualTimingListArray.length > 1) {

        this.crudService.delete(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.APPOINTMENTS_ACTUAL_TIMING,
          undefined,
          prepareRequiredHttpParams({
            appointmentActualTimingId: this.getactualTimingListArray.controls[i].value.appointmentActualTimingId,
            modifiedUserId: this.userData.userId
          })
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getactualTimingListArray.removeAt(i);
            // this.isDuplicate = false;
            // this.isDuplicateNumber = false;
          }
          if (this.getactualTimingListArray.length === 0) {
            this.addActualTiming();
          };
        });
      }
      else {
        this.getactualTimingListArray.removeAt(i);
        // this.isDuplicate = false;
        // this.isDuplicateNumber = false;
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  onSubmitActualTiming() {
    if (!this.getActualTimingActionPermission(PermissionTypes?.ADD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.actualTimingForm.invalid) {
      return
    }

    let formValue = this.actualTimingForm.value
    formValue.actualTimingList.forEach(element => {

      // if (element.onRouteDate) {
      //   let formateDate = element.onRouteDate.split("-")
      //   element.onRouteDate = element.onRouteDate ? formateDate[2] + '-' + formateDate[1] + '-' + formateDate[0] : null
      // }
      element.onRouteDate = element.onRouteDate ? this.momentService.toFormateType(element.onRouteDate, 'YYYY-MM-DD') : null
      element.startTime = element.startTime ? this.momentService.toFormateType(element.startTime, 'HH:mm') : null
      element.endTime = element.endTime ? this.momentService.toFormateType(element.endTime, 'HH:mm') : null
      element.onRouteTime = element.onRouteTime ? this.momentService.toFormateType(element.onRouteTime, 'HH:mm') : null
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.APPOINTMENTS_ACTUAL_TIMING, formValue.actualTimingList)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.actualTimeDialog = false
          this.getCallInitiationDetailsById();
        }
      });

  }

  openReturnForRepair() {
    http://localhost:4200/inventory/return-for-repair
    this.router.navigate(['/technical-management/return-for-repair/add-edit'], {
      queryParams: {
        callInitiationId: this.initiationBasicInfo.callInitiationId
      }, skipLocationChange: true
    });
  }

  openAddContact(val) {
    this.addNewContactForm.get('contactName').setValue(null)
    this.addNewContactForm.get('contactNo').setValue(null)
    this.openAddContactDialog = true
    this.addNewContactForm.get('isPrimary').setValue(val)
    this.addNewContactForm.markAsUntouched();
  }

  addContactSubmit() {
    this.addNewContactForm.markAsUntouched();
    if (this.addNewContactForm.invalid) {
      this.addNewContactForm.markAllAsTouched();
      return
    }
    let formData = this.addNewContactForm.value;
    formData.callInitiationId = this.initiationId ? this.initiationId : null;
    formData.contactNo = formData.contactNo.replace(/\s/g, "");
    this.openAddContactDialog = false;
    let newcontactData = {
      keyHolderId: Math.random(),
      keyHolderName: formData.contactName,
      contactNumber: formData.contactNoCountryCode + ' ' + formData.contactNo,
      isNewContact: true,
      isPrimary: formData.isPrimary
    }
    if (formData.isPrimary) {
      this.contactList.push(newcontactData);
    } else {
      this.alternativContactList.push(newcontactData);
    }
  }

  get installationPartitionsFormArray(): FormArray {
    if (this.initiatonCallCreationForm !== undefined) {
      return (<FormArray>this.initiatonCallCreationForm.get('installationPartitions'));
    }
  }

  getContactListDropdown(quotationVersionId) {
    this.contactList = []
    this.alternativContactList = []
    if (!this.customerId) {
      return
    }
    this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.UX_CUSTOMR_KEYHOLDERS_WITH_CUSTOMER_ID, undefined, null, prepareGetRequestHttpParams(null, null,
        { CustomerId: this.customerId }
      )).subscribe((response) => {
        if (response.resources) {
          if (response.resources.length > 0) {
            response.resources.forEach(element => {
              // let contactExist = response.resources.find(x => x.keyholderId == element.keyholderId)
              // if (!contactExist) {
              element.contactNumber = element.mobile1
              element.keyHolderName = element.customerName
              element.isNewContact = false
              this.contactList.push(element)
              this.alternativContactList.push(element)
              // }
            });
          }
          this.initiatonCallCreationForm.get('contactId').setValue(this.initiatonCallCreationForm.get('contactId').value)
          this.initiatonCallCreationForm.get('alternateContactId').setValue(this.initiatonCallCreationForm.get('alternateContactId').value)

        }
        this.rxjsService.setGlobalLoaderProperty(false);

      })
  }

  onSelectTime(event) {
    // let addFromMints = this.momentService.toHoursMints24(event)
    this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').setValue(event)

    // let addToMints = this.momentService.toHoursMints12(event)
    // let addToMints1 = this.momentService.addMinits(event, this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value)
    let estimatedTime = this.getFormatEstimationTime()
    let spltTime = estimatedTime?.split(':')
    let addToMints1 = this.momentService.addHoursMinits(event, spltTime[0], spltTime[1])
    this.initiatonCallCreationForm.get('appointmentInfo.scheduledEndDate').setValue(new Date(addToMints1))
  }

  getFormatEstimationTime() {

    var estimatedTime: string = this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value
    return estimatedTime
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          if (this.callType == '1') { // initiation call
            switch (ix) {
              case 0:
                this.debtorList = resp.resources;
                break;
              case 1:
                this.sysyemTypeList = resp.resources;
                break;
              case 2:
                this.SystemTypeSubCategoryList = resp.resources;
                break;
              case 3:
                this.jobTypeList = resp.resources;
                break;
              case 4:
                this.panelList = prepareAutoCompleteListFormatForMultiSelection(resp.resources);
                break;
              case 5:
                this.feedBackList = resp.resources;
                break;
              case 6:
                this.priorityList = resp.resources;
                break;
            }
          } else if (this.callType == '2' || this.callType == '3') { // service call
            switch (ix) {
              case 0:
                this.debtorList = resp.resources;
                break;
              case 1:
                this.sysyemTypeList = resp.resources;
                break;
              case 2:
                this.panelList = prepareAutoCompleteListFormatForMultiSelection(resp.resources);
                break;
              case 3:
                this.feedBackList = resp.resources;
                break;
              case 4:
                this.priorityList = resp.resources;
                break;
              case 5:
                this.projectList = resp.resources;
                break;
              case 6:
                this.faultDescriptionList = resp.resources;
                break;
            }
          }

        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createInstallationcallCreationFrom(esclatioListModel?: DealerCallAddEditModel) {
    let initiationModel = new DealerCallAddEditModel(esclatioListModel);
    this.initiatonCallCreationForm = this.formBuilder.group({});
    Object.keys(initiationModel).forEach((key) => {
      if (key == 'serviceInfo') {
        if (this.callType == '1') { // initiation call
          let serviceInfo = this.formBuilder.group({
            'callInitiationServiceInfoId': '',
            'systemTypeId': ['', Validators.required],
            'systemTypeSubCategoryId': ['', Validators.required],
            'serviceSubTypeId': '',
            'faultDescriptionId': '',
            'jobTypeId': ['', Validators.required],
            'description': '',
          });
          this.initiatonCallCreationForm.addControl(key, serviceInfo); //technicianCollJobModel[key]

        } else if (this.callType == '2' || this.callType == '3') { // service call
          let serviceInfo = this.formBuilder.group({
            'callInitiationServiceInfoId': '',
            'systemTypeId': ['', Validators.required],
            'systemTypeSubCategoryId': ['', Validators.required],
            'serviceSubTypeId': ['', Validators.required],
            'faultDescriptionId': ['', Validators.required],
            'jobTypeId': ['', Validators.required],
            'description': '',
          });
          this.initiatonCallCreationForm.addControl(key, serviceInfo); //technicianCollJobModel[key]

        }
      } else if (key == 'appointmentInfo') {
        let appointmentInfo = this.formBuilder.group({
          'appointmentId': '',
          'appointmentLogId': '',
          // 'nextAvailableAppointment': '',
          // 'nextAvailableAppointmentCheckbox': false,
          'scheduledStartDate': '',
          'scheduledEndDate': '',
          // 'diagnosisId': '',
          'estimatedTime': '',
          'estimatedTimeStatic': '',
          'actualTime': '',
          "technician": '',
          // "preferredDate": '',
          // "preferredTime": '',
          // "isAppointmentChangable": false,
          // "techArea": '',
          // 'wireman': ''
        });
        this.initiatonCallCreationForm.addControl(key, appointmentInfo); //technicianCollJobModel[key]
      } else if (key == 'installationPartitions') {
        let skillMatrixServiceSubTypeFormArray = this.formBuilder.array([]);
        this.initiatonCallCreationForm.addControl(key, skillMatrixServiceSubTypeFormArray);
      } else if (key == 'specialProjectId') {
        if (this.callType == '3') {
          this.initiatonCallCreationForm.addControl(key, new FormControl(initiationModel[key], Validators.required));
        } else {
          this.initiatonCallCreationForm.addControl(key, new FormControl(initiationModel[key]));
        }
      } else if (key == 'nkaServiceCall') {
        // let nkaServiceCall = this.formBuilder.group({
        //   'isPOAccepted': null,
        //   'isPONotAccepted': null,
        //   'poNumber': '',
        //   'poAmount': '',
        //   'poComment': '',
        //   'poAttachments': '',
        //   'attachmentUrl': '',
        //   'path': undefined,
        // });
        // nkaServiceCall.get('isPOAccepted').disable();
        // nkaServiceCall.get('isPONotAccepted').disable();
        // nkaServiceCall.get('poNumber').disable();
        // nkaServiceCall.get('path').disable();
        // nkaServiceCall.get('poAmount').disable();
        // nkaServiceCall.get('poComment').disable();
        // this.initiatonCallCreationForm.addControl(key, nkaServiceCall);
      } else {
        this.initiatonCallCreationForm.addControl(key, new FormControl(initiationModel[key]));

      }
    });
    // this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["contact","serviceInfo.systemTypeId","serviceInfo.systemTypeSubCategoryId","serviceInfo.jobTypeId"]);
    if (this.callType == '1') {
      // this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["contactId", "isAddressVerified", "panelTypeConfigId"]);
      this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["contactId", "contactNumber","isAddressVerified"]);// "isAddressVerified"
    } else {
      this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["contactId", "contactNumber", "isAddressVerified","debtorId"]);//, "isAddressVerified"
    }
  }

  //Get Details
  getInitiationCallDetailsById(): Observable<IApplicationResponse> {
    if (this.callType == '1') { // initiation call
      return this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.DEALER_CALL,
        this.initiationId)
    } else if (this.callType == '2' || this.callType == '3') { // service call
      if (this.initiationId) {
        return this.crudService.get(
          ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.DEALER_CALL,
          this.initiationId)
      } else {
        return this.crudService.get(
          ModulesBasedApiSuffix.TECHNICIAN,
          TechnicalMgntModuleApiSuffixModels.DEALER_CALL_CUSTOMER,
          // this.customerId
          undefined,
          false, prepareGetRequestHttpParams(null, null,
            this.warrantyRecallReferenceId ? {
              CustomerId: this.customerId,
              AddressId: this.customerAddressId,
              // warrantyRecallReferenceId: this.warrantyRecallReferenceId ? this.warrantyRecallReferenceId : null
            } :
              {
                CustomerId: this.customerId,
                AddressId: this.customerAddressId,
              }
          )
        )
      }
    }
  };

  getDropdown() {
    // this.rxjsService.setFormChangeDetectionProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_REASONS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.reasonDropDown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  // getNextavailableAppointment() {

  //   if (!this.initiationId || !this.initiationDetailsData?.basicInfo?.callInitiationId) {
  //     return
  //   }
  //   let otherParams = {}
  //   otherParams['CallInitiationId'] = this.initiationId ? this.initiationId : this.initiationDetailsData?.basicInfo?.callInitiationId ? this.initiationDetailsData?.basicInfo?.callInitiationId : null
  //   otherParams['NoOfDates'] = 1

  //   let estimatedTime = this.getFormatEstimationTime()
  //   otherParams['estimatedTime'] = estimatedTime
  //   // this.httpCancelService.cancelPendingRequestsOnNavigationChanged()
  //   this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CAPACITY_APPOINTMETN_SLOT, null, false,
  //     prepareGetRequestHttpParams(null, null, otherParams))
  //     .subscribe((response: IApplicationResponse) => {
  //       this.rxjsService.setGlobalLoaderProperty(false);
  //       if (response.resources && response.statusCode === 200) {
  //         let nextAvailableDateTime = this.momentService.setTimetoRequieredDate(response.resources[0].appointmentDate, response.resources[0].appointmentTimeFrom)
  //         this.initiatonCallCreationForm.get('appointmentInfo').get('nextAvailableAppointment').setValue(new Date(nextAvailableDateTime))
  //         if (this.initiatonCallCreationForm.get('appointmentInfo').get('nextAvailableAppointmentCheckbox').value) {
  //           this.onSelectTime(nextAvailableDateTime)
  //         }
  //       } else {
  //         this.initiatonCallCreationForm.get('appointmentInfo').get('nextAvailableAppointment').setValue(null)
  //         if (this.initiatonCallCreationForm.get('appointmentInfo').get('nextAvailableAppointmentCheckbox').value) {
  //           this.initiatonCallCreationForm.get('appointmentInfo').get('scheduledStartDate').setValue(null)
  //           this.initiatonCallCreationForm.get('appointmentInfo').get('scheduledEndDate').setValue(null)
  //         }
  //       }
  //     });

  // }

  createOnHoldCreationFrom(onHoldModel?: OnHoldModel) {
    let onHoldPopupModel = new OnHoldModel(onHoldModel);
    this.onHoldPopupForm = this.formBuilder.group({});
    Object.keys(onHoldPopupModel).forEach((key) => {

      this.onHoldPopupForm.addControl(key, new FormControl(onHoldPopupModel[key]));


    });
    // this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["contact","serviceInfo.systemTypeId","serviceInfo.systemTypeSubCategoryId","serviceInfo.jobTypeId"]);
    this.onHoldPopupForm = setRequiredValidator(this.onHoldPopupForm, ["OnHoldReason", "OnHoldFollowUpDate"]);

  }

  confirm() {
    this.confirmationService.confirm({
      message: 'You have unsaved changes! Do you want to save this?',
      accept: () => {
        //Actual logic to perform a confirmation
      },
      reject: () => {
        //Actual logic to perform a confirmation
      },
    });
  }

  onHoldValidationCheck() {
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_ON_HOLD_VALIDATION,
      this.initiationId
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {

        if (response.resources.validateMessage) {
          this.warningNotificationMessage = response.resources;
          this.isWarningNotificationDialog = true;
        } else {
          this.openHoldInfoDialog()
        }

      }
    });
  }

  onDealerDebtorCodeClick() {
    if(this.initiationDetailsData?.basicInfo?.dealerApprovalUrl) {
      window.open(this.initiationDetailsData?.basicInfo?.dealerApprovalUrl);
    }
  }

  openHoldInfoDialog() {
    this.onHoldPopupForm.reset();
    this.onHoldPopupForm.controls['OnHoldReason'].patchValue(this.initiationBasicInfo.onHoldReason);
    this.minFUpDate = this.initiationBasicInfo?.onHoldFollowUpDate ? new Date(this.initiationBasicInfo?.onHoldFollowUpDate) : new Date();
    this.onHoldPopupForm.controls['OnHoldFollowUpDate'].patchValue(this.initiationBasicInfo.onHoldFollowUpDate ? this.minFUpDate : '');
    this.holdInfoDialog = true;
  }

  openUpgradeLead() {
    // if (this.initiationDetailsData?.appointmentInfo?.technicianId && this.initiationDetailsData?.appointmentInfo?.scheduledEndDate && this.initiationDetailsData?.appointmentInfo?.scheduledStartDate) {
    this.rxjsService.setUpsellingQuoteProperty(true);
    this.rxjsService.setisFromupsellData(true);
    this.router.navigate(['/customer/upsell-quote/item-info'], {
      queryParams: {
        id: this.customerId,
        callInitiationId: this.initiationId,
        callType: this.callType,
        isVoid: this.initiationBasicInfo?.isVoid ? this.initiationBasicInfo?.isVoid : this.initiationBasicInfo?.isOnHold,

      }
    });
    // }

  }

  openPrCallDialog(type) {
    if (!this.initiationDetailsData?.basicInfo?.callInitiationId) {
      return;
    }
    // this.prCallForm.reset();
    this.requestType = type
    this.prCallForm.get('callInitiationId').setValue(this.initiationDetailsData?.basicInfo?.callInitiationId ? this.initiationDetailsData?.basicInfo?.callInitiationId : this.initiationId);
    if (type?.toLowerCase() == RequestType.PR_CALL_REQUEST) {
      this.prCallForm.controls['motivation'].patchValue(this.initiationDetailsData?.prCall ? this.initiationDetailsData?.prCall?.motivation ? this.initiationDetailsData?.prCall?.motivation : null : null);
      this.prCallForm.controls['serviceCallRequestId'].patchValue(this.initiationDetailsData?.prCall ? this.initiationDetailsData?.prCall?.serviceCallRequestId ? this.initiationDetailsData?.prCall?.serviceCallRequestId : null : null);
    } else {
      this.prCallForm.controls['motivation'].patchValue(this.initiationDetailsData?.saveOfferCall ? this.initiationDetailsData?.saveOfferCall?.motivation ? this.initiationDetailsData?.saveOfferCall?.motivation : null : null);
      this.prCallForm.controls['serviceCallRequestId'].patchValue(this.initiationDetailsData?.saveOfferCall ? this.initiationDetailsData?.saveOfferCall?.serviceCallRequestId ? this.initiationDetailsData?.saveOfferCall?.serviceCallRequestId : null : null);
    }
    this.prCallDialog = true;
  }

  onHoldApply() {
    if (this.onHoldPopupForm.invalid) {
      this.onHoldPopupForm.markAllAsTouched();
      return;
    }
    let onHoldSaveObj = this.onHoldPopupForm.value;
    onHoldSaveObj.CallInitiationId = this.initiationId;
    onHoldSaveObj.ModifiedUserId = this.userData.userId;
    onHoldSaveObj.OnHoldFollowUpDate = this.datePipe.transform(onHoldSaveObj.OnHoldFollowUpDate, 'yyyy-MM-dd');

    this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_ON_HOLD,
      onHoldSaveObj
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.getCallInitiationDetailsById();
        this.holdInfoDialog = false;
        if (response.resources.validateMessage) {
          this.warningNotificationMessage = response.resources;
          this.isWarningNotificationDialog = true;
        }
      }
    });
  }

  createVoidInfoCreationFrom(voidModel?: VoidModel) {
    let voidPopupModel = new VoidModel(voidModel);
    this.voidInfoForm = this.formBuilder.group({});
    Object.keys(voidPopupModel).forEach((key) => {

      this.voidInfoForm.addControl(key, new FormControl(voidPopupModel[key]));


    });
    // this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["contact","serviceInfo.systemTypeId","serviceInfo.systemTypeSubCategoryId","serviceInfo.jobTypeId"]);
    this.voidInfoForm = setRequiredValidator(this.voidInfoForm, ["VoidReason"]);

  }

  onVoidValidationCheck() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.VOID)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_VOID_VALIDATION,
      this.initiationId
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {

        if (response.resources.validateMessage) {
          this.warningNotificationMessage = response.resources;
          this.isWarningNotificationDialog = true;
        } else {
          this.openVoidInfoDialog()
        }

      }
    });
  }

  openVoidInfoDialog() {
    this.voidInfoForm.reset();
    this.voidInfoForm.controls['VoidReason'].patchValue(this.initiationBasicInfo.voidReason)
    this.voidInfoDialog = true;
  }

  applyVoidInfo() {
    const voidEditPermission = this.getQuickActionPermission(CUSTOMER_COMPONENT.VOID)?.subMenu(el => el?.menuName == PermissionTypes.EDIT) ? true : false;
    if(!voidEditPermission) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.voidInfoForm.invalid) {
      this.voidInfoForm.markAllAsTouched();
      return;
    }
    let voidInfoSaveObj = this.voidInfoForm.value
    voidInfoSaveObj.CallInitiationId = this.initiationId;
    voidInfoSaveObj.ModifiedUserId = this.userData.userId;


    this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_VOID,
      voidInfoSaveObj
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        // this.holdInfoDialog = false;
        this.getCallInitiationDetailsById();
        this.voidInfoDialog = false;
        if (response.resources.validateMessage) {
          this.warningNotificationMessage = response.resources;
          this.isWarningNotificationDialog = true;
        }

      }
    });
  }


  openReferToSalesDialog() {
    if (!this.initiationAppointmentInfo?.quotationVersionId) {
      return;
    }
    if (this.initiationBasicInfo?.isVoid || this.initiationBasicInfo?.isInvoiced || this.initiationBasicInfo?.isOnHold) {
      return
    }
    this.router.navigate(['/customer', 'manage-customers', 'refer-to-sale'], {
      queryParams: {
        appointmentId: this.initiatonCallCreationForm.get('appointmentInfo.appointmentId').value,
        installationId: this.initiationId,
        customerId: this.initiationBasicInfo.customerId,
        callType: this.callType,
        quotationVersionId: this.initiationAppointmentInfo?.quotationVersionId,
        callInitiationId: this.initiationBasicInfo.callInitiationId
      }, skipLocationChange: true
    });
    // this.voidInfoDialog = true;
  }

  openCallCreationDialog(isPopupOpen) {
    // this.callCreationDialog = isPopupOpen;

    let otherParams = {}
    if (this.initiationId) {
      otherParams['callInitiationId'] = this.initiationId
    }

    let faultDesc = this.faultDescriptionList.find(x => x.faultDescriptionId == this.initiatonCallCreationForm.get('serviceInfo.faultDescriptionId').value)
    if (faultDesc) {
      if (faultDesc.faultDescriptionName == 'System Overactive') {
        otherParams['isOverActive'] = true
      } else {
        otherParams['isOverActive'] = false
      }
    } else {
      otherParams['isOverActive'] = false
    }
    // otherParams['isInstallationCall'] = this.callType == '1' ? true : false
    // otherParams['isServiceCall'] = this.callType == '2' ? true : false
    // otherParams['isSpecialProjectCall'] = this.callType == '3' ? true : false
    // otherParams['isDealerCall'] = this.callType == '4' ? true : false
    otherParams['isDealerCall'] = true
    // otherParams['isInspectionCall'] = this.callType == '5' ? true : false
    otherParams['CustomerId'] = this.customerId
    otherParams['AddressId'] = this.customerAddressId

    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_CALL_CREATION_PAYMENT)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.paymentDropdown = response.resources
        }
      });

    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION_QUESTIONS, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.callCreationDialog = isPopupOpen;

        if (response.resources && response.statusCode === 200) {
          let callCreationModel = new CallCreationTemplateModel(response.resources);
          // if (response.resources.length == 0) {
          //   this.callCreationTemplateList = this.getcallCreationListArray;
          //   this.callCreationTemplateList.push(this.createcallCreationListModel(null));
          // } else {
          this.createCallCreationTemplateForm();
          this.callCreationTemplateList = this.getcallCreationListArray;
          response.resources.forEach((callCreationListModel: CallCreationTemplateListModel) => {
            callCreationListModel.callInitiationId = this.initiationId ? this.initiationId : ''
            callCreationListModel.customerId = this.customerId ? this.customerId : ''
            callCreationListModel.createdUserId = this.userData.userId ? this.userData.userId : ''
            this.callCreationTemplateList.push(this.createcallCreationTemplateListModel(callCreationListModel));
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  changeStatus(data) {

  }

  openActualTimeDialog() {
    let otherParams = {}
    if (!this.initiationId || !this.initiationDetailsData?.appointmentInfo?.appointmentId || !this.initiationDetailsData?.appointmentInfo?.scheduledStartDate) {
      return
    } else {
      otherParams['appointmentId'] = this.initiationDetailsData?.appointmentInfo?.appointmentId
    }
    // otherParams['isAll'] =

    // this.actualTimeDialog = true; //hide p-dialog

    // call for 2 apis
    let dropdownsAndData = [];
    dropdownsAndData = [
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.APPOINTMENTS_ACTUAL_TIMING_DATES, null, false,
        prepareGetRequestHttpParams(null, null, {
          appointmentId: this.initiationDetailsData?.appointmentInfo?.appointmentId ? this.initiationDetailsData?.appointmentInfo?.appointmentId : ''
        })),
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.
        APPOINTMENTS_ACTUAL_TIMING, null, false,
        prepareGetRequestHttpParams(null, null, otherParams))
    ];
    // this.loadActionTypesForActualTimings(dropdownsAndData);
    this.OnLoadActualTime(dropdownsAndData);
  }

  loadActionTypesForActualTimings(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.actualTimingDateList = resp.resources;
              break;

            case 1:
              // this.getactualTimingListArray.clear();
              this.createActualTimingForm()
              let actualTimingModel = new ActualTimingModel(resp.resources);
              if (resp.resources.length == 0) {
                this.actualTimingList = this.getactualTimingListArray;
                let actualTimingListModel = new ActualTimingListModel(null);
                actualTimingListModel.appointmentId = this.initiationDetailsData?.appointmentInfo?.appointmentId ? this.initiationDetailsData?.appointmentInfo?.appointmentId : '';
                // actualTimingListModel.onRouteDate = this.initiationDetailsData.appointmentInfo.scheduledStartDate ? this.initiationDetailsData.appointmentInfo.scheduledStartDate : "";
                actualTimingListModel.onRouteDate = "";
                actualTimingListModel.createdUserId = this.userData.userId ? this.userData.userId : '';
                actualTimingListModel.modifiedUserId = this.userData.userId ? this.userData.userId : '';
                actualTimingListModel.appointmentLogId = ''; // added for appointmentLogId date
                this.actualTimingList.push(this.createactualTimingListModel(actualTimingListModel));;
              } else {
                this.actualTimingList = this.getactualTimingListArray;
                resp.resources.forEach((actualTimingListModel: ActualTimingListModel) => {
                  actualTimingListModel.appointmentId = this.initiationDetailsData?.appointmentInfo?.appointmentId ? this.initiationDetailsData?.appointmentInfo?.appointmentId : ''
                  // actualTimingListModel.startTime = actualTimingListModel.startTime ? this.momentService.setTime(actualTimingListModel.startTime) : null,
                  actualTimingListModel.startTime = actualTimingListModel.startTime ? this.momentService.toMoment(this.momentService.setTime(actualTimingListModel.startTime)).seconds(0).format() : null,
                    // actualTimingListModel.endTime = actualTimingListModel.endTime ? this.momentService.setTime(actualTimingListModel.endTime) : null,
                    actualTimingListModel.endTime = actualTimingListModel.endTime ? this.momentService.toMoment(this.momentService.setTime(actualTimingListModel.endTime)).seconds(0).format() : null,
                    actualTimingListModel.onRouteTime = actualTimingListModel.onRouteTime ? this.momentService.setTime(actualTimingListModel.onRouteTime) : null,
                    actualTimingListModel.createdUserId = this.userData.userId ? this.userData.userId : ''
                  actualTimingListModel.modifiedUserId = this.userData.userId ? this.userData.userId : '';
                  actualTimingListModel.appointmentLogId = actualTimingListModel.appointmentLogId ? actualTimingListModel.appointmentLogId : ''; // added for appointmentLogId date
                  actualTimingListModel.onRouteDate = actualTimingListModel.onRouteDate && actualTimingListModel.onRouteDate.split('T').length > 0 ? actualTimingListModel.onRouteDate.split('T')[0] : '';
                  if (actualTimingListModel.onRouteDate) {
                    let formateDate = actualTimingListModel.onRouteDate.split("-")
                    actualTimingListModel.onRouteDate = actualTimingListModel.onRouteDate ? formateDate[2] + '-' + formateDate[1] + '-' + formateDate[0] : null
                  }
                  actualTimingListModel.startTimeMinDate = actualTimingListModel.onRouteTime ? this.momentService.toMoment(this.momentService.addMinits(actualTimingListModel.onRouteTime, 1)).seconds(0).format() : null;
                  actualTimingListModel.endTimeMinDate = actualTimingListModel.startTime ? this.momentService.toMoment(this.momentService.addMinits(actualTimingListModel.startTime, 1)).seconds(0).format() : null,
                    this.actualTimingList.push(this.createactualTimingListModel(actualTimingListModel));
                });
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  OnLoadActualTime(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.actualTimingDateList = resp.resources;
              break;

            case 1:
              const ref = this.dialog.open(ActualTimeDialogComponent, {
                width: '1250px',
                data: {
                  header: 'Actual Timing',
                  actualTimingModelData : resp.resources,
                  userData: this.userData,
                  initiationDetailsData: this.initiationDetailsData,
                  actualTimingDateList: this.actualTimingDateList,
                  api: TechnicalMgntModuleApiSuffixModels.APPOINTMENTS_ACTUAL_TIMING,
                },
                disableClose: true,
              });
              ref.afterClosed().subscribe((res) => {
                if (res) {
                  this.getCallInitiationDetailsById();
                }
              });
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }



  navigateToTechAllocation() {
    if (this.initiatonCallCreationForm.invalid) {
      this.autoSubmitClick() // to show error validation
      return
    }
    // if (this.initiationDetailsData?.basicInfo?.isNKACustomer && this.userData?.userId == this.initiationDetailsData?.nkaServiceCall?.ownedUserId && this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName != 'poSupplied') {
    //   this.snackbarService.openSnackbar('The customer must fill the po details', ResponseMessageTypes.WARNING);
    //   return;
    // }
    // && (!this.initiationId || !this.initiationDetailsData?.basicInfo?.callInitiationId)
    if (!this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value) {
      this.initiatonCallCreationForm.markAllAsTouched();
      this.snackbarService.openSnackbar('Estimation time is required', ResponseMessageTypes.WARNING);
      return
    }
    if (!this.initiationId) {
      this.autoSaveAlertNavigate(1)
      return
    } else {
      // if (this.momentService.isDateValid(this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value)) {
      //   let estimatedTime = this.momentService.toHoursMints24(this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value)
      //   this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(estimatedTime)
      // }
      // let estimatedTime = this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value

      this.navigateToTechAllocationParams()
    }
  }

  navigateToTechAllocationParams() {
    // const techAllocationView = this.getAppointmentActionPermission(PermissionTypes.VIEW);
    if (!this.findActionPermission(CUSTOMER_COMPONENT.APPOINTMENT_BOOKING)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/customer/dealer-tech-allocation-calender-view'], {
      queryParams: {
        nextAppointmentDate: this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').value ? this.initiatonCallCreationForm.get('appointmentInfo.scheduledStartDate').value : new Date(),
        installationId: this.initiationBasicInfo ? this.initiationBasicInfo.callInitiationId ? this.initiationBasicInfo.callInitiationId : this.initiationId : this.initiationId,
        customerId: this.initiationBasicInfo ? this.initiationBasicInfo.customerId : this.customerId,
        customerAddressId: this.initiationBasicInfo ? this.initiationBasicInfo.addressId : this.customerAddressId,
        appointmentId: this.initiatonCallCreationForm.get('appointmentInfo.appointmentId').value ? this.initiatonCallCreationForm.get('appointmentInfo.appointmentId').value : null,
        appointmentLogId: this.initiatonCallCreationForm.get('appointmentInfo.appointmentLogId').value ? this.initiatonCallCreationForm.get('appointmentInfo.appointmentLogId').value : null,
        estimatedTime: this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value ? this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').value : null,
        callType: this.callType ? this.callType : null  // 1- installation call, 2- service call
      }
    });
  }

  /* --- Open Call Escalation Dialog ---- */
  openCallEscalationDialog() {
    // if (!this.initiationDetailsData?.appointmentInfo?.appointmentLogId) {
    //   return;
    // }
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.ESCALAATION)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.callEscalationsubscritption && !this.callEscalationsubscritption.closed) {
      this.callEscalationsubscritption.unsubscribe();
    }
    this.callEscalationsubscritption = this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.QUICK_CALL_ESCALATION, null, false, prepareRequiredHttpParams({ CallInitiationId: this.initiationId })).subscribe((res: any) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res?.message) {
          this.snackbarService.openSnackbar("Cannot open escalation try after sometimes", ResponseMessageTypes.WARNING);
          return;
        }
        if (res?.isSuccess == true && res?.statusCode == 200) {
          const escalationEditPermission = this.getQuickActionPermission(CUSTOMER_COMPONENT.ESCALAATION)?.subMenu?.find(el => el?.menuName == PermissionTypes.EDIT) ? true : false;
          const ref = this.dialogService.open(CallEscalationDialogComponent, {
            header: 'Escalation',
            baseZIndex: 1000,
            width: '400px',
            // closable: false,
            showHeader: false,
            data: {
              ...res?.resources,
              createdUserId: this.userData?.userId,
              isServiceCallEscalation: false,
              editPermission: escalationEditPermission,
              isSubmitButtonEnable: this.initiationBasicInfo?.isVoid ? this.initiationBasicInfo?.isVoid : this.initiationBasicInfo?.isOnHold  // for void status -only can view the data
            },
          });
          ref.onClose.subscribe((res) => {
            if (res) {
              this.getCallInitiationDetailsById();
            }
          });
        }
      });
  }
  /* --- Close Call Escalation Dialog ---- */

  /* --- Open Rebook Dialog ---- */
  openCloseRebookDialog(val?) {
    let isNew = this.initiatonCallCreationForm.get("isNew").value
    if (this.initiationBasicInfo?.isLastRecountReached) {
      this.snackbarService.openSnackbar("Rebook count is reached final count...", ResponseMessageTypes.WARNING);
      return;
    } else if (!this.initiationAppointmentInfo?.scheduledStartDate) {
      this.snackbarService.openSnackbar("Job is not scheduled", ResponseMessageTypes.WARNING);
      return;
    } else if (!this.initiationAppointmentInfo?.arrivedTime) { // Action timing popup start time if not exists show the msg
      this.snackbarService.openSnackbar("Appointment start time has not been recorded", ResponseMessageTypes.WARNING);
      return;
      // } else if (this.initiationBasicInfo?.isLastRecountReached == false && !isNew && this.initiationAppointmentInfo?.actualTime) {
    } else {
      this.isRebookDialog = val;
      this.rxjsService.setDialogOpenProperty(val);
      this.rebookDialogForm.reset();
      this.isRebookSubmit = false;
      this.isRebookAlertDialog = false;
    }
  }

  initRebookDialogForm() {
    this.rebookDialogForm = new FormGroup({
      rebookReason: new FormControl(''),
      rebookComments: new FormControl()
    })
    this.rebookDialogForm = setRequiredValidator(this.rebookDialogForm, ["rebookReason"]);
    this.rebookDialogForm.controls?.rebookReason?.valueChanges.subscribe((val: any) => {
      if (val) {
        const rebookReasonName = this.rebookReasonList.find(el => el?.value == val);
        if (rebookReasonName?.display?.toLowerCase() == "other") {
          this.rebookDialogForm?.controls?.rebookComments?.setValidators([Validators.required]);
          this.rebookDialogForm?.controls?.rebookComments?.updateValueAndValidity();
          this.rebookDialogForm?.controls?.rebookComments?.setValue("");
          this.showRebookComments = true;
        } else if (rebookReasonName?.display?.toLowerCase() !== "other") {
          this.rebookDialogForm = clearFormControlValidators(this.rebookDialogForm, ["rebookComments"]);
          this.rebookDialogForm?.controls?.rebookComments?.setValue(val?.display);
          this.showRebookComments = false;
        }
      }
    })
  }

  onLoadRebookReasonList() {
    // this.rxjsService.setFormChangeDetectionProperty(true);
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.REBOOK_REASON_LIST).subscribe((res: any) => {
        if (res?.isSuccess == true && res?.statusCode == 200) {
          const rebookReasonList = [];
          res?.resources?.forEach(el => rebookReasonList.push({ display: el.displayName, value: el.id }));
          this.rebookReasonList = rebookReasonList;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  onSubmitRebookDialog() {
    var rebookDialogObj = {
      callInitiationId: this.initiationBasicInfo?.callInitiationId,
      callInitiationRebookReasonId: this.rebookDialogForm.value?.rebookReason,
      Reason: this.rebookDialogForm.value?.rebookComments ? this.rebookDialogForm.value?.rebookComments : null,
      createdUserId: this.userData?.userId
    }
    if (this.rebookDialogForm.valid && this.userData?.userId && this.initiationBasicInfo?.callInitiationId) {
      this.isRebookSubmit = true;
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.REBOOK_REASON, rebookDialogObj).subscribe((res: any) => {
          if (res?.isSuccess == true && res?.statusCode == 200) {
            if (JSON.parse(res?.resources)?.AlertMessage) {
              this.rebookalertMesssage = JSON.parse(res?.resources)?.AlertMessage;
              this.isRebookAlertDialog = true;
            } else {
              this.openCloseRebookDialog(false);
            }
            this.getCallInitiationDetailsById();
          } else {
            this.openCloseRebookDialog(false);
          }
        })
    } else {
      return;
    }
  }
  /* --- Close Rebook Dialog ---- */

  /* --- Open Rebook Confirm Dialog ---- */
  btnRebookNoClick() {
    if (this.callType == '2') { // service call
      var rebookConfirmDialogObj = {
        callInitiationId: this.initiationBasicInfo?.callInitiationId,
        createdUserId: this.userData?.userId
      }
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.REBOOK_NOCHARGE, rebookConfirmDialogObj).subscribe((res: any) => {
          if (res?.isSuccess == true && res?.statusCode == 200) {
            this.cancelClick();
          }
          this.openCloseRebookDialog(false);
        });
    } else {
      this.openCloseRebookDialog(false);
      this.getCallInitiationDetailsById();

    }
  }
  /* --- Close Rebook Confirm Dialog ---- */

  // navigateToPage() {
  //   this.router.navigate(['/customer', 'manage-customers', 'bill-of-material'], {
  //     queryParams: {
  //       installationId: this.installationId,
  //       // licenseTypeId: this.licenseTypeId
  //     }
  //   });
  // }

  navigateToPage() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.BILL_OF_MATERIALS)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.initiationAppointmentInfo?.quotationVersionId && this.callType == 1) {
      return;
    }
    this.router.navigate(['/customer', 'manage-customers', 'bill-of-material'], {
      queryParams: {
        installationId: this.initiationId,
        customerId: this.initiationBasicInfo?.customerId,
        customerAddressId: this.initiationBasicInfo?.addressId,
        quotationVersionId: this.initiationAppointmentInfo?.quotationVersionId,
        callInitiationId: this.initiationBasicInfo?.callInitiationId,
        isVoid: this.initiationBasicInfo?.isVoid ? this.initiationBasicInfo?.isVoid : this.initiationBasicInfo?.isOnHold ? this.initiationBasicInfo?.isOnHold : this.initiationBasicInfo?.isInvoiced,
        callType: this.callType,
        appointmentId: this.initiationDetailsData?.appointmentInfo?.appointmentId,
        isFeedback: this.initiationDetailsData?.basicInfo?.isFeedback,
        appointmentLogId: this.initiatonCallCreationForm.get('appointmentInfo.appointmentLogId').value ? this.initiatonCallCreationForm.get('appointmentInfo.appointmentLogId').value : null
      }, skipLocationChange: true
    });
  }

  navigateToTechTransfer() {
    this.router.navigate(['/customer', 'manage-customers', 'tech-transfer'], {
      queryParams: {
        installationId: this.initiationId,
        customerId: this.initiationBasicInfo.customerId,
        // quotationVersionId: this.initiationAppointmentInfo?.quotationVersionId,
        callInitiationId: this.initiationBasicInfo.callInitiationId,
        callType: this.callType
      }, skipLocationChange: true
    });
  }
  navigateToQuotationTransfer() {
    this.router.navigate(['/customer', 'manage-customers', 'quotation-items'], {
      queryParams: {
        customerId: this.initiationBasicInfo.customerId,
        quotationVersionId: this.initiationAppointmentInfo?.quotationVersionId,
      }, skipLocationChange: true
    });
  }

  navigateToSiteHazard() {
    this.router.navigate(['/customer', 'manage-customers', 'site-hazard'], {
      queryParams: {
        customerId: this.initiationBasicInfo.customerId,
        customerAddressId: this.initiationBasicInfo.addressId,
      }, skipLocationChange: true
    });
  }

  navigateToLeadhandOverCertificate() {
    this.router.navigate(['/customer', 'manage-customers', 'lead-hand-over-certificate'], {
      queryParams: {
        customerName: this.initiationBasicInfo.customerName,
        customerRefNo: this.initiationBasicInfo.customerRefNo,
        debtorCode: this.initiationBasicInfo.debtorCode,
        systemTypeId: this.initiationServiceInfo.systemTypeId,
        customerId: this.initiationBasicInfo.customerId,
        customerAddressId: this.initiationBasicInfo.addressId,
        SaleOrderId: this.initiationBasicInfo.saleOrderId,
        LeadId: this.initiationBasicInfo.leadId,
        customerAddress: this.initiationBasicInfo.customerAddress,
        QutationVersionId: this.initiationAppointmentInfo.quotationVersionId,
      }, skipLocationChange: true
    });
  }
  // navigateInstallationJobCard(){

  //   const ref = this.dialogService.open(TechnicalAssistantComponent, {
  //     header: 'Technical Assistant',
  //     baseZIndex: 1000,
  //     width: '950px',
  //     closable: true,
  //     showHeader: true,
  //     data: {
  //       AppointmentId:this.initiationAppointmentInfo.appointmentId,
  //       AppointmentLogId:this.initiationAppointmentInfo.appointmentLogId,
  //     },
  //   });
  //   ref.onClose.subscribe((res) => {
  //     if (res) {

  //     }
  //   });
  // }

  openInspectionCallBackDialog() {
    if (!this.getQuickActionPermission(CUSTOMER_COMPONENT.CALLBACK)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if(!this.initiationId || this.initiationBasicInfo?.isInvoiced) {
      return;
    }
    const callbackEditPermission = this.getQuickActionPermission(CUSTOMER_COMPONENT.CALLBACK)?.subMenu?.find(el => el?.menuName == PermissionTypes.EDIT) ? true : false;
    // const ref = this.dialogService.open(InspectionCallBackDialogComponent, {
    const ref = this.dialog.open(InspectionCallBackDialogComponent, {
      // header: 'Call Back',
      width: '550px',
      data: {
        customerId: this.customerId,
        // customerInspectionId: this.customerInspectionId
        referenceId: this.initiationId,
        editPermission: callbackEditPermission,
        header: 'Call Back',
      },
      disableClose: true,
    });
    // ref.onClose.subscribe((result) =>{
    ref.afterClosed().subscribe((result) => {
      if (result) {
        this.getCallInitiationDetailsById();
      }
    });
  }

  navigateCustomerView() {
    this.rxjsService.setViewCustomerData({
      customerId: this.initiationBasicInfo?.customerId,
      addressId: this.customerAddressId,
      customerTab: 0,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();

  }

  isAddressChecked(val) {
    this.initiatonCallCreationForm.get('isAddressVerified').setValue(val.checked ? true : null)
  }

  onSubmit(bol?:Number | String) {
    if (this.getEditPermission()) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setFormChangeDetectionProperty(true);

    if (!this.isTechnicalAreaMngr()) {
      this.rxjsService.getFormChangeDetectionProperty().subscribe(val => {
      })
      if (this.initiatonCallCreationForm.invalid) {
        // if (this.initiatonCallCreationForm.invalid || !this.initiatonCallCreationForm?.get('isAddressVerified')?.value) {
        this.initiatonCallCreationForm.markAllAsTouched();
        return false
      }
      if (!this.callCreationTemplate) {
        if (this.callCreationTemplateForm.invalid || this.callCreationTemplateForm.value.callCreationList.length == 0) {
          this.snackbarService.openSnackbar("Call Creation Template is required", ResponseMessageTypes.WARNING);
          return false
        }
      }
    }

    let Obj = this.initiatonCallCreationForm.value;

    // Obj.appointmentInfo.isNextAvailableAppointment = Obj.appointmentInfo.nextAvailableAppointmentCheckbox ? Obj.appointmentInfo.nextAvailableAppointmentCheckbox : false

    // if (this.momentService.isDateValid(Obj.appointmentInfo.estimatedTime)) {
    //   Obj.appointmentInfo.estimatedTime = this.momentService.toHoursMints24(Obj.appointmentInfo.estimatedTime)
    // }

    // Obj.appointmentInfo.nextAvailableAppointment = Obj.appointmentInfo.nextAvailableAppointment ? Obj.appointmentInfo.nextAvailableAppointment.toLocaleString() : null
    Obj.appointmentInfo.scheduledStartDate = Obj.appointmentInfo.scheduledStartDate ? Obj.appointmentInfo.scheduledStartDate.toLocaleString() : null
    Obj.appointmentInfo.scheduledEndDate = Obj.appointmentInfo.scheduledEndDate ? Obj.appointmentInfo.scheduledEndDate.toLocaleString() : null

    let saveObj = {
      "createdUserId": this.userData.userId,
      "appointmentId": this.initiationBasicInfo.appointmentId,
      "techInstruction": Obj.techInstruction,
      "partitionIds": [],
      "basicInfo": {
        "callInitiationId": this.initiationBasicInfo.callInitiationId ? this.initiationBasicInfo.callInitiationId : this.initiationId,
        "customerId": this.initiationBasicInfo.customerId,
        // "quotationNumber": this.initiationBasicInfo.quotationNumber,
        "isNew": Obj.isNew,
        "isScheduled": Obj.isScheduled,
        "isCompleted": Obj.isCompleted,
        "isInvoiced": Obj.isInvoiced,
        // "isOnHold": Obj.isOnHold,
        "isVoid": Obj.isVoid,
        // "isReferToSales": Obj.isReferToSales,
        // "isInstallPreCheck": Obj.isInstallPreCheck,
        // "isNKACustomer": this.initiationDetailsData?.basicInfo?.isNKACustomer,
        // "isExecuGuardCustomer": this.initiationDetailsData?.basicInfo?.isExecuGuardCustomer,
        // "isInWarrenty": Obj.isInWarrenty,
        // "isRecall": Obj.isRecall,
        // "isCode50": Obj.isCode50,
        // "isPRCall": Obj.isPRCall,
        // "isSaveOffer": Obj.isSaveOffer,
        // "isCon": Obj.isCon,
        "isAddressVerified": Obj.isAddressVerified ? Obj.isAddressVerified : null,
        "contact": Obj.contact,
        "contactNumber": Obj.contactNumber,
        "alternateContact": Obj.alternateContact,
        "alternateContactNumber": Obj.alternateContactNumber,
        "priorityId": Obj.priorityId,
        "panelTypeConfigId": Obj.panelTypeConfigId ? Obj.panelTypeConfigId.value : null,
        // "specialProjectId": Obj.specialProjectId,
        // "notes": Obj.notes,
        "partitionIds": [],
        "isInstallationCall": this.callType == '1' ? true : false,
        "isServiceCall": this.callType == '2' ? true : false,
        "isSpecialProjectCall": this.callType == '3' ? true : false,
        "isDealerCall": this.callType == '4' ? true : false,
        "isInspectionCall": this.callType == '5' ? true : false,
        "addressId": this.customerAddressId ? this.customerAddressId : this.initiationBasicInfo.addressId,
        // "recallCount": this.initiationDetailsData?.basicInfo?.recallCount,
        // "warrantyRecallReferenceId": this.initiationDetailsData?.basicInfo?.warrantyRecallReferenceId,
        // "warrantyServiceCallNo": this.initiationDetailsData?.basicInfo?.warrantyServiceCallNo,
        "debtorId": Obj.debtorId ? Obj.debtorId : null,
        "feedbackId": Obj.feedbackId ? Obj.feedbackId : null
      },
      "serviceInfo": Obj.serviceInfo,
      "appointmentInfo": Obj.appointmentInfo,
      "callInitiationContacts": this.callInitiationContacts,
      "callCreationTemplate": this.callCreationTemplateForm.value.callCreationList
    }
    if (Obj.installationPartitions) {
      Obj.installationPartitions.forEach((element) => {
        if (element.mainPartition == true) {
          saveObj.partitionIds.push(element.partitionId)
        }
      })
    }
    // saveObj.serviceInfo.diagnosisId = Obj.appointmentInfo.diagnosisId;
    // saveObj.serviceInfo.wireman = Obj.appointmentInfo.wireman;
    // this.initiatonCallCreationForm.value.ismainPartation == true ? saveObj.basicInfo.partitionIds.push(this.initiationpartitions.find(x => x.partitionCode == this.initiatonCallCreationForm.value.mainPartationCustomerId).partitionId) : saveObj.basicInfo.partitionIds;
    if (this.initiatonCallCreationForm.value.ismainPartation) {
      if (this.initiationDetailsData?.partitions) {
        // let partitionCode = this.initiatonCallCreationForm.value.mainPartationCustomerId.split(" ")[0]
        // let partitionId = this.initiationDetailsData.partitions.find(x => x.partitionCode == partitionCode)
        // saveObj.partitionIds.push(partitionId?.partitionId)
        saveObj.partitionIds.push(this.initiationDetailsData?.partitions.find(x => x.partitionCode == this.initiatonCallCreationForm.value.mainPartationCustomerId.split(" ")[0]).partitionId)
      } else {
        saveObj.partitionIds
      }
    } else {
      saveObj.partitionIds
    }
    // if (this.initiationDetailsData?.basicInfo?.isNKACustomer) {
    //   saveObj['NKAServiceCall'] = {
    //     NKAServiceCallId: this.initiationDetailsData?.nkaServiceCall ? this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallId : null,
    //     CallInitiationId: this.initiationBasicInfo?.callInitiationId ? this.initiationBasicInfo?.callInitiationId : this.initiationId,
    //     PONumber: this.initiatonCallCreationForm.get('nkaServiceCall.poNumber').value,
    //     POAmount: this.initiatonCallCreationForm.get('nkaServiceCall.poAmount').value.toString().replace(/ /g, ""),
    //     POComment: this.initiatonCallCreationForm.get('nkaServiceCall.poComment').value,
    //     IsPOAccepted: this.initiatonCallCreationForm.get('nkaServiceCall.isPOAccepted').value ? true : this.initiatonCallCreationForm.get('nkaServiceCall.isPONotAccepted').value ? false : null,
    //   }
    // }
    // if (this.initiationDetailsData?.basicInfo?.isExecuGuardCustomer) {
    //   saveObj['ExecuGuardServiceCall'] = {
    //     ExecuGuardServiceCallId: this.initiationDetailsData?.execuGuardServiceCall ? this.initiationDetailsData?.execuGuardServiceCall?.execuGuardServiceCallId : null,
    //     CallInitiationId: this.initiationBasicInfo?.callInitiationId ? this.initiationBasicInfo?.callInitiationId : this.initiationId,
    //   }
    // }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    // let crudService: Observable<IApplicationResponse> = this.callType == '1'
    //   ? this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.DEALER_CALL, saveObj)
    //   : this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.DEALER_CALL, saveObj)

    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.DEALER_CALL, saveObj)

    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.initiatonCallCreationForm.markAsUntouched()       // this.navigateToPage();
        // this.navigateCustomerView()
        if (response.resources) {
          this.initiationId = response.resources;
          if (bol == 1) {
            this.isNavigationFirstTime = false
            this.navigateToTechAllocationParams()
          } else if (bol == 2) {
            let troubleShootObject = {
              customerId: this.customerId,
              initiationId: this.initiationId,
              customerAddressId: this.customerAddressId,
              callType: this.callType
            }
            this.rxjsService.setTroubleShootAlarm(troubleShootObject)
            this.isNavigationFirstTime = false
            this.onRedirectTroubleShoot()
          }
        }
        this.initiatonCallCreationForm.get('isAddressVerified').markAsUntouched();
        this.isManualChange = false
        this.getCallInitiationDetailsById();
        // this.getNextavailableAppointment()
        this.openCallCreationDialog(false)

      }
    });
  }

  onPRCallCreate() {
    if (this.prCallForm.invalid) {
      return
    }
    // let formValue = this.prCallForm.value
    // formValue.serviceCallRequestId = this.initiationDetailsData.prCall? this.initiationDetailsData.prCall.serviceCallRequestId ? this.initiationDetailsData.prCall.serviceCallRequestId : null : null
    this.rxjsService.setDialogOpenProperty(true);
    let crudService = (this.requestType?.toLowerCase() == RequestType.PR_CALL_REQUEST) ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.PR_CALL, this.prCallForm.value)
      : this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SAVE_OFFER, this.prCallForm.value)
    crudService.subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      // if (response.isSuccess == true && response.statusCode == 200) {
      //   this.prCallDialog = false;
      //   this.getCallInitiationDetailsById();
      //   this.rxjsService.setDialogOpenProperty(false);
      // }

      if (response.isSuccess) {
        this.prCallDialog = false;
        this.alertDialog = true
        this.alertMsg = response.resources
        this.getCallInitiationDetailsById();
        this.rxjsService.setDialogOpenProperty(false);
      }

    });
  }

  cancelClick() {
    this.rxjsService.setViewCustomerData({
      customerId: this.customerId,
      addressId: this.customerAddressId,
      customerTab: 3,
      monitoringTab: null,
    })
    this.rxjsService.navigateToViewCustomerPage();
  }

  onSendJobCardEmail() {
    let data = {
      callInitiationId: this.initiationDetailsData?.basicInfo?.callInitiationId,
      createdUserId: this.userData.userId
    }
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.THIRD_PARTY_SUBCONTRACTOR_JOBCARD, data)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
        }
      });
  }


  uploadPOFiles(file) {
    this.fileList = [];
    if (file && file.length == 0)
      return;
    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }
    const supportedExtensions = [
      'jpeg',
      'jpg',
      'png',
      'gif',
      'pdf',
      'doc',
      'docx',
      'xls',
      'xlsx',
    ];
    for (let i = 0; i < file.length; i++) {
      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension)) {
        let filename = this.fileList.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push(selectedFile.name);
          this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments')?.setValue(selectedFile.name);
        }
      } else {
        this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
      }
    }
    this.onAddPOFiles()
  }

  onAddPOFiles(): void {
    if (this.fileList.length == 0) {
      return
    }
    let data = {
      CallInitiationId: this.initiationDetailsData?.basicInfo?.callInitiationId,
      CreatedUserId: this.userData.userId,
      CallInitiationDocumentType: 'NKA PO',
    }
    let formData = new FormData();
    formData.append('callInitiationDocumentDTO', JSON.stringify(data));
    this.fileList.forEach(file => {
      formData.append('file', file);
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_DOCUMENTS, formData, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          // this.getCallInitiationDetailsById();
          const currentFileUrl = response?.resources?.find(el => el?.docName == this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments').value);
          this.initiatonCallCreationForm.get('nkaServiceCall.attachmentUrl').setValue(currentFileUrl?.path);
          if (this.isPOInfoDisabled()) {
            this.initiatonCallCreationForm.get('nkaServiceCall.poNumber').enable();
            this.initiatonCallCreationForm.get('nkaServiceCall.poAmount').enable();
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onOpenAttachment() {
    if (this.initiatonCallCreationForm.get('nkaServiceCall.attachmentUrl').value) {
      var link = document.createElement("a");
      if (link.download !== undefined) {
        link.setAttribute("href", this.initiatonCallCreationForm.get('nkaServiceCall.attachmentUrl').value);
        link.setAttribute("download", this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments').value);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
      // import("file-saver").then(FileSaver => {
      //   FileSaver.saveAs(this.initiatonCallCreationForm.get('nkaServiceCall.attachmentUrl').value, this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments').value);
      // });
    }
  }

  isPOInfoDisabled() {
    return this.initiatonCallCreationForm.get('nkaServiceCall.poAttachments')?.value
      && this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po required'
      && this.initiationDetailsData?.basicInfo?.isNKACustomer && this.initiationDetailsData?.nkaServiceCall?.ownedUserId == this.userData?.userId;
  }

  isPOAcceptDisabled() {
    return (this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po supplied' || this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po rejected') && this.initiationDetailsData?.nkaServiceCall?.isPOAccepted == null;
  }

  onNKAReSubmit(): void {
    const message = `Po is not accepted. Are you re-submitting the request?`;
    const dialogData = new ConfirmDialogModel("PO Request", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      width: "800px",
      data: {
        ...dialogData,
        isClose: true,
        msgCenter: true,
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if ((this.initiationDetailsData?.nkaServiceCall?.isPOAccepted == false || this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallSatusName?.toLowerCase() == 'po declined') && dialogResult) {
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.NKA_SERVICE_CALL_RESUBMIT,
          { NKAServiceCallId: this.initiationDetailsData?.nkaServiceCall?.nkaServiceCallId, CreatedUserId: this.userData?.userId },
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getCallInitiationDetailsById();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onExecuReSubmit(): void {
    const message = `Po is not accepted. Are you re-submitting the request?`;
    const dialogData = new ConfirmDialogModel("PO Request", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      width: "800px",
      data: {
        ...dialogData,
        isClose: true,
        msgCenter: true,
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.initiationDetailsData?.execuGuardServiceCall?.execuGuardServiceCallStatusName == 'Declined' && dialogResult) {
        this.rxjsService.setGlobalLoaderProperty(true);
        this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.EXEC_SERVICE_CALL_DECLINE,
          { ExecuGuardServiceCallId: this.initiationDetailsData?.execuGuardServiceCall?.execuGuardServiceCallId, CreatedUserId: this.userData?.userId },
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getCallInitiationDetailsById();
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onChangePartitions(event) {
    if (this.initiatonCallCreationForm.value.installationPartitions.length > 0) {
      let count = this.initiatonCallCreationForm.value.installationPartitions.filter(x => x.mainPartition == true)
      if (count.length > 0) {
        let estimatedSplt = this.initiatonCallCreationForm.get('appointmentInfo.estimatedTimeStatic').value.split(':')

        let hours = Number(estimatedSplt[0]) * count.length
        let minuts = Number(estimatedSplt[1]) * count.length
        let emptyTimeDate = this.momentService.setTime('0:0')
        let addHoursMints = this.momentService.addHoursMinits(emptyTimeDate, hours, minuts)
        let convetRailwayTime = this.momentService.convertNormalToRailayTime(new Date(addHoursMints))
        let finaltestimateSplt = convetRailwayTime.split(':')
        this.initiatonCallCreationForm.get('appointmentInfo.estimatedTime').setValue(Number(finaltestimateSplt[0]) + ':' + Number(finaltestimateSplt[1]))
      }
    }
  }

  dealerDetails: any
  openDealerTechnicianPopup(){
    if (this.initiationBasicInfo?.dealerBranchId) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.crudService.get(ModulesBasedApiSuffix.DEALER, DealerModuleApiSuffixModels.DEALER_BRANCH, this.initiationBasicInfo?.dealerBranchId, false,
        prepareRequiredHttpParams({ "dealerId": this.initiationBasicInfo?.dealerId }))
        .subscribe((response: IApplicationResponse) => {
          if (response && response.resources && response.isSuccess) {
            this.dealerTechnicianDialog = true
            this.dealerDetails = response.resources
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  openRecurringAppointmentDialog() {
    const ref = this.dialogService.open(RecurringAppointmentComponent, {
      header: 'Recurring Schedule',
      baseZIndex: 1000,
      width: '950px',
      closable: true,
      showHeader: true,
      data: {
        installationId: this.initiationId,
        createdUserId: this.userData?.userId,
        showSubmitBtn: (this.initiationBasicInfo?.isVoid || this.initiationBasicInfo?.isOnHold) ? false : true // for void status - can view the page
      },
    });
    ref.onClose.subscribe((res) => {
      if (res) {

      }
    });
  }

  autoSubmitClick() {
    let element: HTMLElement = document.querySelector('.autoSave') as HTMLElement;
    element.click();
  }

  autoSaveAlertNavigate(val) {
    if (this.isNavigationFirstTime) {
      if (this.initiatonCallCreationForm.touched || this.callCreationTemplateForm.touched) {
        if (confirm("You have unsaved changes! Do you want to save this?")) {
          this.autoSubmitClick()
          if (this.initiatonCallCreationForm.invalid || !this.initiatonCallCreationForm?.get('isAddressVerified')?.value) {
            this.callCreationDialog = false
            this.initiatonCallCreationForm.markAllAsTouched();
            return false;
          }
          if (!this.callCreationTemplate) {
            if (this.callCreationTemplateForm.invalid || this.callCreationTemplateForm.value.callCreationList.length == 0) {
              this.snackbarService.openSnackbar("Call Creation Template is required", ResponseMessageTypes.WARNING);
              return false
            }
          }
          this.onSubmit(val) // 1- for calendar navigation
          // return true


        } else {
          if (val == 1) {
            this.snackbarService.openSnackbar('Please save and proceed!', ResponseMessageTypes.WARNING);
          } else {
            return true
          }
        }
      } else {
        this.onRedirectTroubleShoot()
      }
    }
  }

  canDeactivate() {
    // autosave functionality starts here
    if (this.isNavigationFirstTime) {
      if (this.initiatonCallCreationForm.touched || this.callCreationTemplateForm.touched) {
        if (confirm("You have unsaved changes! Do you want to save this?")) {
          this.autoSubmitClick()
          if (this.initiatonCallCreationForm.invalid || !this.initiatonCallCreationForm?.get('isAddressVerified')?.value) {
            this.callCreationDialog = false
            this.initiatonCallCreationForm.markAllAsTouched();
            return false;
          }
          if (!this.callCreationTemplate) {
            if (this.callCreationTemplateForm.invalid || this.callCreationTemplateForm.value.callCreationList.length == 0) {
              this.snackbarService.openSnackbar("Add Call Creation Template", ResponseMessageTypes.WARNING);
              return false
            }
          }
          // setTimeout(() => {
          // this.navigateToTechAllocation()
          return true
          // }, 2000);

        } else {
          // if (this.initiationBasicInfo.callInitiationId) {
          return true
          // } else {
          //   this.snackbarService.openSnackbar('Please save and proceed!', ResponseMessageTypes.WARNING);
          // }
        }
      } else {
        return true
      }
    } else {
      return true
    }
    // autosave functionality ends here

  }

  isTechnicalAreaMngr() {
    if (this.userData.roleName.toLowerCase() == 'technical area manager') {
      return true
    } else {
      return false
    }
  }

  public ngOnDestroy(): void {
    // This aborts all HTTP requests.
    this.ngUnsubscribe.next();
    // This completes the subject properlly.
    this.ngUnsubscribe.complete();
    // this.rxjsService.ser

  }

}
