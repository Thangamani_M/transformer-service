import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CanDeactivateGuard, SharedModule } from '@app/shared';
import { PrimengConfirmDialogPopupModule } from '@app/shared/components/primeng-confirm-dialog-popup/primeng-confirm-dialog-popup.module';
import { MaterialModule } from '@app/shared/material.module';
import { InspectionCallBackDialogModule } from '@modules/technical-management/components/inspection-call/inspection-call-back-dialog/inspection-call-back-dialog.module';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ActualTimeDialogModule } from '../actual-time-dialog/actual-time-dialog.module';
import { CallEscalationModule } from '../call-escalation-dialog/call-escalation-dialog.module';
import { RecurringAppointmentModule } from '../recurring-appointment/recurring-appointment.module';
import { DealerCallCreationAddEditComponent } from './dealer-call-creation-add-edit.component';

import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
@NgModule({
  declarations: [DealerCallCreationAddEditComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule.forChild([
        { path: '', component: DealerCallCreationAddEditComponent, data: { title: 'Dealer Call' }, canActivate: [AuthGuard], canDeactivate: [CanDeactivateGuard]}
    ]),
    FormsModule,
    SharedModule,
    CallEscalationModule,
    ActualTimeDialogModule,
    ConfirmDialogModule,
    InspectionCallBackDialogModule,
    RecurringAppointmentModule,
    PrimengConfirmDialogPopupModule,
  ]
})
export class DealerCallCreationAddEditModule { }
