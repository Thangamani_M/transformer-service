import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ServiceCallWaringDialogComponent } from './service-call-waring-dialog.component';

@NgModule({
  declarations: [ ServiceCallWaringDialogComponent ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule
  ],
  exports: [ServiceCallWaringDialogComponent],
  entryComponents: [ServiceCallWaringDialogComponent],
})
export class ServiceCallWaringDialogModule { }
