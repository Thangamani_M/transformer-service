import { Component, OnInit } from '@angular/core';
import { CrudService } from '@app/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-credit-control-notification-dialog',
  templateUrl: './credit-control-notification-dialog.component.html'
})
export class CreditControlNotificationDialogComponent implements OnInit {


  showDialogSpinner: boolean = false;
  data: any = [];
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, private crudService: CrudService) { }

  ngOnInit(): void {
  }

  close() {
    this.ref.close();
  }

}

  
