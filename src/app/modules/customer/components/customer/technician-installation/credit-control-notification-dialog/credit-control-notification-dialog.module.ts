import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { CreditControlNotificationDialogComponent } from './credit-control-notification-dialog.component';



@NgModule({
  declarations: [CreditControlNotificationDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule
  ],
  exports:[CreditControlNotificationDialogComponent],
  entryComponents:[CreditControlNotificationDialogComponent]
})
export class CreditControlNotificationDialogModule { }
