import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TechnicalAssistantComponent } from "./technical-assistant.component";

@NgModule({
  declarations: [TechnicalAssistantComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
  ],
  exports: [TechnicalAssistantComponent],
  entryComponents: [TechnicalAssistantComponent],
})
export class TechnicalAssistantModule { }