import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, getPDropdownData, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-technical-assistant',
  templateUrl: './technical-assistant.component.html'
})
export class TechnicalAssistantComponent implements OnInit {
  techAssistantDropdown: any = [];
  ContaCts: any;
  COntacts: any;
  technicalAssistantList: any = [];
  AppointmentId: string;
  validation: any;
  AppointmentLogId: string;
  WiremanID: any;
  loggedUser: any;
  TechnicalAssistantId: any;
  TechAssistantDetails: any;
  TechAssistantDetailsLIst: any;
  TechnicalAssistantemployeeId: any;
  technicalassistantForm: FormGroup;
  techAssistantonchange: any;
  // onsetcontacts:any;
  last_valid_selection = null;
  isVoid: boolean = false;
  isANumberWithZero = new CustomDirectiveConfig({ isANumberWithZero: true });
  constructor(private crudService: CrudService, private snackbarService: SnackbarService, public config: DynamicDialogConfig, public ref: DynamicDialogRef, private store: Store<AppState>, private rxjsService: RxjsService, private _fb: FormBuilder, private activatedRoute: ActivatedRoute,) {
    this.AppointmentId = this.config?.data?.AppointmentId;
    this.AppointmentLogId = this.config?.data?.AppointmentLogId;
    this.isVoid = this.config?.data?.isVoid;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createTechAssistantForm();
    this.getTechAssistantList();
  }

  btnCloseClick(val = false) {
    this.ref.close(val);
  }

  createTechAssistantForm() {
    this.technicalassistantForm = this._fb.group({
      isWiremanAvailable: ["", Validators.required],
      wiremanId: ["", Validators.required],
      wiremanCount: ["", Validators.required],
      AppointmentId: [this.AppointmentId ? this.AppointmentId : null],
      AppointmentLogId: [this.AppointmentLogId ? this.AppointmentLogId : null],
      wiremanContacts: [""],
    })
  }

  getTechAssistantList() {
    if (!this.AppointmentId && !this.AppointmentLogId) {
      return
    }
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.WORK_LIST_WIREMAN, null, false,
      prepareRequiredHttpParams({ AppointmentId: this.AppointmentId, AppointmentLogId: this.AppointmentLogId}))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false)
        if (response.isSuccess) {
          this.TechAssistantDetails = response.resources;
          this.WiremanID = this.TechAssistantDetails.wiremanId;
          let selectedContacts = []
          this.TechAssistantDetails.contacts?.forEach(element => {
            if (element?.isSelected) {
              selectedContacts.push(element?.employeeId)
            }
            return element;
          });
          this.technicalassistantForm.patchValue({
            isWiremanAvailable: this.TechAssistantDetails?.isWiremanAvailable === true ? 'Yes' : this.TechAssistantDetails?.isWiremanAvailable === false ? 'No' : this.TechAssistantDetails?.isWiremanAvailable,
            wiremanCount: this.TechAssistantDetails?.wiremanCount
          });
          this.TechAssistantDetailsLIst = getPDropdownData(response.resources.contacts, "employeeName", "employeeId");
          this.technicalassistantForm?.get('wiremanContacts').patchValue(selectedContacts);
        }
      });
  }

  getwiremancount() {
    return this.technicalassistantForm?.get('wiremanCount')?.value ? this.technicalassistantForm.get('wiremanCount').value : 0;
  }
  onSetContacts(index) {
    
    this.validation = index;
    if (index > 0) {
      this.technicalassistantForm.get('isWiremanAvailable').setValue(this.validation > 0 === true ? 'Yes' : this.validation < 0 === false ? 'No' : this.validation ? this.validation : '--')
    } else {
      this.technicalassistantForm.get('isWiremanAvailable').setValue(this.validation > 0 === true ? 'Yes' : this.validation < 0 === false ? 'No' : this.validation ? this.validation : '--')
    }
    this.technicalassistantForm?.get('wiremanContacts').patchValue(null);
  }
  changeTechnicalType(val) {
    this.last_valid_selection = "";
    let ii = 0;
    val.value.forEach(element => {
      if (ii == val.value.length - 1) {
        this.last_valid_selection += element['employeeId'];
        ii++;
      }
      else {
        this.last_valid_selection += element['employeeId'] + ',';
        ii++;
      }
    });
  }

  ontechnicalassistant() {
    if(!this.config?.data?.editPermission) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.technicalassistantForm.pristine) {
      this.snackbarService.openSnackbar("No Changes Were Detected ", ResponseMessageTypes.WARNING);
      return
    }
    if (this.validation == 0) {
      this.snackbarService.openSnackbar("Technicial Assistant Count Should Not be Zero ", ResponseMessageTypes.WARNING);
      return
    }
    if (this.validation > 0) {
      this.technicalassistantForm.controls['wiremanContacts'].enable()
      this.technicalassistantForm.get('wiremanContacts').setValidators([Validators.required])
      this.technicalassistantForm.controls['wiremanContacts'].updateValueAndValidity();
    }
    let formValue = this.technicalassistantForm.value;
    // formValue.wiremanContacts = this.last_valid_selection,
    //   formValue.AppointmentId = '5ce40394-d7e2-4e14-b18c-e082c946e6d9';
    // formValue.AppointmentLogId = '5c7473e3-8ce4-4992-9807-6ac098077c85';
    formValue.AppointmentId = this.AppointmentId,
      formValue.AppointmentLogId = this.AppointmentLogId,
      formValue.createdUserId = this.loggedUser.userId;
    formValue.wiremanId = this.WiremanID;
    formValue.wiremanContacts = formValue.wiremanContacts.toString();
    this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.WORK_LIST_WIREMAN,
      formValue
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.btnCloseClick(true);
      }
    });
  }

}
