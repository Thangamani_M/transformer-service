import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { LeadHandOverCertificateComponent } from "./lead-hand-over-certificate.component";

import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


@NgModule({
    declarations: [LeadHandOverCertificateComponent ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: LeadHandOverCertificateComponent, data: { title: 'Lead Hand Over Certificate' }, canActivate: [AuthGuard]
        },
    ])
  ],
  entryComponents: [],
})
export class LeadHandOverCertificateModule  { }