import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';

@Component({
  selector: 'app-lead-hand-over-certificate',
  templateUrl: './lead-hand-over-certificate.component.html'
})
export class LeadHandOverCertificateComponent implements OnInit {
  leadhandovercertificateForm:FormGroup;
  customerId:string;
  customerAddressId:string;
  customerName:string;
  customerAddress:string;
  customerRefNo:string;
  debtorCode:string;
  loggedUser: any;
  LeadId:string;
  QutationVersionId:string;
  systemTypeId:string;
  SaleOrderId:string;
  callType:string;
  installationId:string;
  AppointmentId:string;
  // viewable:boolean=false;
  // locationcomponents:boolean= false;
  // newinstallation:boolean=false;
  // confirmatiobycustomer:boolean=false;
  // ExistingEquipment:boolean=false;
  leadhandovertech:any;
  constructor(private crudService: CrudService,   private router: Router,private activatedRoute: ActivatedRoute,public dialogService: DialogService,
    private store: Store<AppState>,private rxjsService: RxjsService,) {
      this.systemTypeId = this.activatedRoute.snapshot.queryParams.systemTypeId;
      this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
      this.customerAddressId = this.activatedRoute.snapshot.queryParams.customerAddressId;
      this.SaleOrderId = this.activatedRoute.snapshot.queryParams.SaleOrderId;
      this.LeadId = this.activatedRoute.snapshot.queryParams.LeadId;
      this.QutationVersionId= this.activatedRoute.snapshot.queryParams.QutationVersionId;
      this.customerName=this.activatedRoute.snapshot.queryParams.customerName,
      this.debtorCode=this.activatedRoute.snapshot.queryParams.debtorCode,
      this.customerAddress= this.activatedRoute.snapshot.queryParams.customerAddress,
      this.customerRefNo=this.activatedRoute.snapshot.queryParams.customerRefNo
      this.AppointmentId=this.activatedRoute.snapshot.queryParams.AppointmentId
      this.installationId=this.activatedRoute.snapshot.queryParams.installationId
      this.callType=this.activatedRoute.snapshot.queryParams.callType
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.loggedUser = userData;
      });
    }

  ngOnInit(): void {
    this.getleadhandovercertificateList();
  }

  //Get MEthod
  getleadhandovercertificateList() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.LEAD_HAND_OVER_CERTIFICATE, undefined, false,
      prepareRequiredHttpParams({ SaleOrderId: this.SaleOrderId,LeadId:this.LeadId,systemTypeId:this.systemTypeId,QutationVersionId: this.QutationVersionId}))
      .subscribe((response: IApplicationResponse) => {
        if (response && response.resources && response.isSuccess) {
          this.leadhandovertech = response.resources.handOverCertificateCategory;

         this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
  }

  navigateToPage() {
    this.router.navigate(['/customer', 'manage-customers', 'call-initiation'], {
      queryParams: {
        // customerId: this.installationId
        customerId: this.customerId,
        customerAddressId: this.customerAddressId,
        initiationId: this.installationId,
        appointmentId: this.AppointmentId ? this.AppointmentId : null,
        callType: this.callType
      },skipLocationChange:true
    });
  }
}
