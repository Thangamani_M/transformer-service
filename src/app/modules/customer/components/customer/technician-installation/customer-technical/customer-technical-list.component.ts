import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatMenuItem } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  ConfirmDialogModel, currentComponentPageBasedPermissionsSelector$, moduleBasedComponentPermissionsSelector$,
  ConfirmDialogPopupComponent,
  CrudService, CrudType, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService, prepareDynamicTableTabsFromPermissions, convertTreeToList, PermissionTypes
} from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { InspectionCallEscalationDialogComponent } from '@modules/customer/components/customer/technician-installation/inspection-escalation-modal';
import { CustomerVerificationType } from '@modules/customer/shared/enum/customer-verification.enum';
import { CustomerModuleApiSuffixModels, CUSTOMER_COMPONENT } from '@modules/customer/shared/utils/customer-module.enums';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { loggedInUserData } from '@modules/others/auth.selectors';
import { UserLogin } from '@modules/others/models/others-module-models';
import { SalesModuleApiSuffixModels } from '@modules/sales/shared/utils/sales-module.enums';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService, DynamicDialogRef } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { CallEscalationDialogComponent } from '../call-escalation-dialog';
import { CreditControlNotificationDialogComponent } from '../credit-control-notification-dialog/credit-control-notification-dialog.component';
import { RadioCallEscalationDialogComponent } from '../radio-call-escalation-dialog';
import { ServiceCallInwarrantyDialogComponent } from '../service-call-inwarranty-dialog/service-call-inwarranty-dialog.component';
import { ServiceCallWaringDialogComponent } from '../service-call-waring-dialog/service-call-waring-dialog.component';
import { SpecialProjectWaringDialogComponent } from '../special-project-waring-dialog/special-project-waring-dialog.component';
import { CallType, ValidateType } from './call-type.enum';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-customer-technical-list',
  templateUrl: './customer-technical-list.component.html',
  styleUrls: ['./customer-technical-list.component.scss'],
  providers: [DynamicDialogRef]
})

export class CustomerTechnicaListComponent extends PrimeNgTableVariablesModel {

  @Input() customerId;
  @Input() customerAddressId: any;
  @Input() partitionIds: any
  @Input() permission: any
  userData: UserLogin;
  technicallDetail: any;
  showQuickAction: boolean = false;
  selectedServiceIndex: any = 0;
  loggedInUserData: LoggedInUserModel;
  obNumberList: any;
  selectedRowData: any;
  isLoading: boolean;
  isDetailsView: boolean = false;
  autoIndex: number = 0;
  autoIndexSpecialProject: number = 0;
  serviceValidationsList: any = [];
  specilProjectValidationsList: any = [];
  appointmentId: any;
  isDealerCall: boolean = false;
  isRadio: boolean | string;
  radioRemovalWorkListId: string;
  uniqueCallId;
  @Output() redirectToCustomerView = new EventEmitter<any>();
  allPermission: any = []
  rolePermission: any = [];

  constructor(
    private crudService: CrudService, private tableFilterFormService: TableFilterFormService,
    private sanitizer: DomSanitizer,
    private commonService: CrudService,
    private router: Router,
    private _fb: FormBuilder, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private datePipe: DatePipe,
    private store: Store<AppState>, private momentService: MomentService,
    public dialogService: DialogService,
    private dialogRef: DynamicDialogRef,
    private dialog: MatDialog,
    private snackbarService: SnackbarService
  ) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Customer Technical",
      breadCrumbItems: [{}],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Automated',
            dataKey: 'stagingBayReportId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableSecondHyperLink: true,
            cursorSecondLinkIndex: 1,
            cursorLinkIndex: 0,
            columns: [{ field: 'callInitiationNumber', header: 'Service Call No', width: "140px" },
            { field: 'customerCallType', header: 'Call Type', width: "130px" },
            // { field: 'requestTypeName', header: 'Call Type', width: "150px" },
            { field: 'callInitiationStatus', header: 'Status', width: "120px" },
            { field: 'quotationNumber', header: 'Quote Numbers', width: "150px" },
            { field: 'salesRef', header: 'Sales Ref', width: "180px" },
            { field: 'callCreationDate', header: 'Call Creation Date', width: "160px" },
            { field: 'scheduledStartDate', header: 'Scheduled', width: "160px" },
            { field: 'completedDate', header: 'Completed', width: "160px" },
            { field: 'technicianName', header: 'Technician', width: "150px" },
            { field: 'fault', header: 'Fault', width: "150px" },
            { field: 'flag', header: 'Flag', width: "80px" },
            { field: 'partitions', header: 'Partitions', width: "100px" },
            { field: 'sapStock', header: 'SAP Stock', width: "110px" },
            { field: 'sapRevenue', header: 'SAP Revenue', width: "130px" },
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: SalesModuleApiSuffixModels.CALL_INITIATION,
            moduleName: ModulesBasedApiSuffix.TECHNICIAN,
          }
        ]
      }
    }

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.rxjsService.getUniqueCallId().subscribe((openscapeConfigDetail) => {
      if (openscapeConfigDetail) {
        this.uniqueCallId = openscapeConfigDetail.UniqueCallid;
      }
    });
    this.appointmentId = this.activatedRoute.snapshot.queryParams.appointmentId;
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? params['params']['tab'] ? +params['params']['tab'] : 0 : 0;
      this.isRadio = (Object.keys(params['params']).length > 0) ? params['params']['isRadio'] ? params['params']['isRadio'] : false : false;
      this.radioRemovalWorkListId = (Object.keys(params['params']).length > 0) ? params['params']['radioRemovalWorkListId'] ? params['params']['radioRemovalWorkListId'] : '' : '';
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      //  this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });

    this.rxjsService.getLoadTechnicianProperty().subscribe(isLoad => {
      if (isLoad) {
        this.getRequiredListData();
        this.getTechicalDetatils();
      }
    })

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.allPermission = this.permission?.actions?.subMenu?.find(item => item.menuName == "Add");
    this.selectedTabIndex = 0
  }

  ngOnChanges(changes: SimpleChanges) {
    if ((changes?.customerAddressId?.previousValue != changes?.customerAddressId?.currentValue) || (changes?.partitionIds?.previousValue != changes?.partitionIds?.currentValue)) {
      this.getTechicalDetatils();
      this.getRequiredListData();
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
}

  getTechicalDetatils() {
    if (!this.customerId && !this.customerAddressId) {
      return
    }
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CUSTOMER_TECHNICAL_DETAILS, null, null,
      prepareRequiredHttpParams({ customerid: this.customerId, addressid: this.customerAddressId }))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess) {
          this.technicallDetail = res?.resources;
        }
      })
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][CUSTOMER_COMPONENT.CUSTOMER]?.find(el => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD);
      if (permission) {
        let techPermission = convertTreeToList([permission?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.TECHNICAL)]);
        this.rolePermission = techPermission;
      }
    });
  }

  getCallType(i: number) {
    if(this.rolePermission) {
      return this.rolePermission?.find(el => el[CUSTOMER_COMPONENT.CALL_TYPE])?.[CUSTOMER_COMPONENT.CALL_TYPE]?.[i];
    }
  }

  getDocumentPermission() {
    if(this.rolePermission) {
      return this.rolePermission?.find(el => el[CUSTOMER_COMPONENT.CALL_TYPE])?.[CUSTOMER_COMPONENT.CALL_TYPE]?.[2]?.subMenu;
    }
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    if (!this.customerId && !this.customerAddressId) {
      return
    }
    this.loading = true;
    if (!otherParams) {
      otherParams = {}
      otherParams['CustomerId'] = this.customerId;
      otherParams['AddressId'] = this.customerAddressId;
      if (this.partitionIds) {
        otherParams['partitionIds'] = this.partitionIds;
      }
    }
    let SalesModuleApiSuffixModels: SalesModuleApiSuffixModels;
    SalesModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      SalesModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.requestTypeNameAliceName = val.isDealerCall ? 'Dealer' : val.requestTypeName
          val.callCreationDate = this.datePipe.transform(val.callCreationDate, 'dd-MM-yyyy, HH:mm:ss');
          val.scheduledStartDate = this.datePipe.transform(val.scheduledStartDate, 'dd-MM-yyyy, HH:mm:ss');
          val.completedDate = this.datePipe.transform(val.completedDate, 'dd-MM-yyyy, HH:mm:ss');
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      this.rxjsService.setLoadTechnicianProperty(false)
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  technicallInititationDetail;
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | any): void {

    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {...unknownVar};
        otherParams['CustomerId'] = this.customerId;
        otherParams['AddressId'] = this.customerAddressId;
        if (this.partitionIds) {
          otherParams['partitionIds'] = this.partitionIds;
        }
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EDIT:
        if (unknownVar == 'customerCallType') {
          this.selectedRowData = row;
          let SalesModuleApiSuffixModels: SalesModuleApiSuffixModels;
          SalesModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
          this.isLoading = true;
          this.commonService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            SalesModuleApiSuffixModels,
            row['callInitiationId'],
            false
          ).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {

              this.technicallInititationDetail = data.resources;
              if (data.resources?.notes) {
                this.technicallInititationDetail.notes = this.sanitizer.bypassSecurityTrustHtml(data.resources?.notes?.replaceAll('\n', ' <br> '));
              }
            } else {
              // this.observableResponse = null;
              this.technicallInititationDetail = null;
              // this.totalRecords = 0;

            }
            if (this.selectedServiceIndex == 3 || this.selectedServiceIndex == 4) {
              this.onLoadObNumberList();
            }
            this.isLoading = false;
          })
          this.isDetailsView = true;
        }
        break;
      //   case CrudType.FILTER:
      //     this.displayAndLoadFilterData();
      //     break;
      default:
    }
  }

  onServiceTabChangeEvent(event) {
    // this.row = {}
    // this.columnFilterForm = this._fb.group({})
    // this.columnFilterForm = this.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    // this.columnFilterRequest();
    // this.dataList = [];
    // this.totalRecords = null;
    this.selectedServiceIndex = event.index;
    if (this.selectedServiceIndex == 0) {
      this.technicallInititationDetail = '';
      this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.CALL_INITIATION,
        this.selectedRowData.callInitiationId,
        false, null).subscribe((data: IApplicationResponse) => {
          this.loading = false;
          this.rxjsService.setGlobalLoaderProperty(false);
          if (data?.isSuccess) {
            this.technicallInititationDetail = data.resources;
            if (data.resources?.notes) {
              this.technicallInititationDetail.notes = this.sanitizer.bypassSecurityTrustHtml(data.resources?.notes?.replaceAll('\n', ' <br> '));
            }
          }
        });
    }
  }

  onLoadObNumberList() {
    this.isLoading = true;
    let api = this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT,
      EventMgntModuleApiSuffixModels.OCCURANCE_BOOK_SIGNAL_CUSTOMER_ADDRESS,
      undefined, false, prepareGetRequestHttpParams("0", "10", { CustomerId: this.customerId, CustomerAddressId: this.customerAddressId }));
    if (this.selectedServiceIndex == 4) {
      api = this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_AUDIT_LOG, null,
        false, prepareGetRequestHttpParams("0", "10", { callInitiationId: this.selectedRowData?.callInitiationId }));
    }
    api.pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          if (this.selectedServiceIndex == 3) {
            val.signalDateTime = this.datePipe.transform(val.signalDateTime, 'medium');
          }
          return val;
        });
      }
      return res;
    })).subscribe((res: IApplicationResponse) => {
      this.obNumberList = res;
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        // this.router.navigate(['/inventory','daily-staging-bay','add-edit'], {});
        break;
      case CrudType.VIEW:
        if (editableObject['isDealerCall']) {
          if (editableObject['requestTypeName'].toLowerCase() == 'inspection') {
            this.router.navigate(['/customer/manage-customers/inspection-call'], { queryParams: { customerInspectionId: editableObject['callInitiationId'], customerId: this.customerId, isNew: 'customerlist' } });
          } else {
            this.router.navigate(['/customer/manage-customers/dealer-call'], {
              queryParams: {
                customerId: this.customerId,
                customerAddressId: this.customerAddressId,
                initiationId: editableObject['callInitiationId'],
                appointmentId: editableObject['appointmentId'] ? editableObject['appointmentId'] : null,
                // appointmentId:this.appointmentId,
                callType: editableObject['requestTypeName'] == CallType.INSTALLATION_CALL ? 1 : (editableObject['requestTypeName'] == CallType.SERVICE_CALL ? 2 : 3) // 2- service call, 1- installation call
              }, skipLocationChange: false
            });
          }
        } else {
          if (editableObject['requestTypeName'].toLowerCase() == 'inspection') {
            this.router.navigate(['/customer/manage-customers/inspection-call'], { queryParams: { customerInspectionId: editableObject['callInitiationId'], customerId: this.customerId, isNew: 'customerlist' } });
          } else if (editableObject['requestTypeName'].toLowerCase() == 'radio removal') {
            this.router.navigate(['/customer', 'manage-customers', 'radio-removal-call'], {
              queryParams: {
                customerId: this.customerId, addressId: this.customerAddressId,
                initiationId: editableObject['callInitiationId'], radioRemovalWorkListId: this.radioRemovalWorkListId, appointmentId: editableObject['appointmentId'] ? editableObject['appointmentId'] : null, callType: 2
              }
            });
          } else if (editableObject['isUpselling']) {
            this.router.navigate(['/customer/manage-customers/call-initiation'], {
              queryParams: {
                customerId: this.customerId,
                customerAddressId: this.customerAddressId,
                initiationId: editableObject['callInitiationId'],
                appointmentId: editableObject['appointmentId'] ? editableObject['appointmentId'] : null,
                isUpselling: editableObject['isUpselling'],
                // appointmentId:this.appointmentId,
                callType: editableObject['requestTypeName'] == CallType.INSTALLATION_CALL ? 1 : (editableObject['requestTypeName'] == CallType.SERVICE_CALL ? 2 : 3) // 2- service call, 1- installation call
              }, skipLocationChange: false
            });
          } else {
            this.router.navigate(['/customer/manage-customers/call-initiation'], {
              queryParams: {
                customerId: this.customerId,
                customerAddressId: this.customerAddressId,
                initiationId: editableObject['callInitiationId'],
                appointmentId: editableObject['appointmentId'] ? editableObject['appointmentId'] : null,
                // appointmentId:this.appointmentId,
                callType: editableObject['requestTypeName'] == CallType.INSTALLATION_CALL ? 1 : (editableObject['requestTypeName'] == CallType.SERVICE_CALL ? 2 : 3) // 2- service call, 1- installation call
              }, skipLocationChange: false
            });
          }
        }
        break;
    }
  }

  ticketQuickActionOpen() {
    if (!this.permission?.canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.showQuickAction = !this.showQuickAction;
  }

  // navigateToInstallation() {
  //   this.router.navigate(['/customer/manage-customers/call-initiation'], {
  //     queryParams: {
  //       initiationId: this.customerId,
  //       callType: 1 // 2- service call, 1- installation call
  //     }, skipLocationChange: true
  //   });
  // }

  navigateToInstallationByServiceCall() {
    if (this.isDealerCall) {
      var routerUrl = '/customer/manage-customers/dealer-call'
    } else {
      var routerUrl = '/customer/manage-customers/call-initiation'
    }
    this.router.navigate([routerUrl], {
      queryParams: {
        customerId: this.customerId,
        customerAddressId: this.customerAddressId,
        initiationId: "",
        appointmentId: this.appointmentId ? this.appointmentId : null,
        callType: 2 // 2- service call, 1- installation call
      }, skipLocationChange: false
    });
  }

  navigateToInstallationBySpecialProject() {
    if (this.isDealerCall) {
      var routerUrl = '/customer/manage-customers/dealer-call'
    } else {
      var routerUrl = '/customer/manage-customers/call-initiation'
    }
    this.router.navigate([routerUrl], {
      queryParams: {
        customerId: this.customerId,
        customerAddressId: this.customerAddressId,
        initiationId: "",
        callType: 3 // 2- service call, 1- installation call, -special project
      }, skipLocationChange: false
    });
  }

  // check credit controller aler
  checkCreditControllerAlert(callType: CallType | any, isDealer?: any) {
    let isAccessDeined = this.getPermissionByActionType(!isDealer ? callType : 'Dealer Call');
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    const custVerficationType = isDealer ? CustomerVerificationType.DEALER_CALL : CustomerVerificationType.SERVICE_CALL
    if (callType?.toLowerCase() == 'service') {
      this.onCustomerVerficationCheck(custVerficationType, callType);
    } else {
      this.onAfterCheckCreditControllerAlert(callType);
    }
  }

  onCustomerVerficationCheck(tab, callType) {
    if (this.activatedRoute?.snapshot?.queryParams?.feature_name) {
      this.onAfterCheckCreditControllerAlert(callType);
    } else {
      this.rxjsService.setGlobalLoaderProperty(true);
      let filterObj = { userId: this.loggedInUserData?.userId, customerId: this.customerId, siteAddressId: this.customerAddressId, uniqueCallId: this.uniqueCallId ? this.uniqueCallId : null };
      this.crudService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.CUSTOMER_VERIFICATION_CHECK, null, false,
        prepareRequiredHttpParams(filterObj))
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200) {
            this.onAfterCheckCreditControllerAlert(callType);
          } else {
            this.rxjsService.setGlobalLoaderProperty(false);
            var isSignalMgmt = this.router.url?.indexOf('signal-management') != -1;
            this.router.navigate(['/customer/manage-customers/customer-verification'], { queryParams: { customerId: this.customerId, siteAddressId: this.customerAddressId, tab: tab, customerTab: 3, isSignalMgmt: isSignalMgmt }, skipLocationChange: true });
          }
        })
    }
  }

  onAfterCheckCreditControllerAlert(callType: CallType) {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.commonService.get(ModulesBasedApiSuffix.BILLING, TechnicalMgntModuleApiSuffixModels.CREDIT_CONTROLLER_NOTIFICATION,
      undefined,
      false, prepareGetRequestHttpParams(null, null,
        {
          CustomerId: this.customerId,
          AddressId: this.customerAddressId,
          createduserId: this.loggedInUserData.userId,
        }))
      .subscribe(response => {
        this.rxjsService.setGlobalLoaderProperty(false);

        if (response.isSuccess) {
          if (response.resources?.outstandingBalance > 0) {
            let data = {
              validateMessage: 'Customer BDI no: ' + response.resources?.bdiNumber + ' has a outstanding balance of R ' + response.resources?.outstandingBalance,
              callType: callType
            }
            this.openCreditControlNotificationDialog(data)
          } else {
            this.checkAllValidationCalls(callType)
          }
        } else {
          this.checkAllValidationCalls(callType)
        }
      })
  }


  // service call

  checkValidateServiceCall() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.commonService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION_VALIDATION,
      undefined,
      false, prepareGetRequestHttpParams(null, null,
        {
          CustomerId: this.customerId,
          AddressId: this.customerAddressId
        }))
      .subscribe(response => {
        // this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        this.isDealerCall = response.resources.isDealerCall
        if (response.resources.isSuccess) {
          this.navigateToInstallationByServiceCall()
        } else {

          if (response.resources.validations.length > 0) {

            this.autoIndex = 0
            this.serviceValidationsList = response.resources.validations
            this.openServiceCallDialog(this.serviceValidationsList[0])


            // let findDataEsc = response.resources.validations.find(x => x.validateType == ValidateType.ESCALAATION)
            // if (findDataEsc) {
            //   this.openServiceCallDialog(findDataEsc)
            // } else {
            //   response.resources.validations.forEach(element => {
            //     this.openServiceCallDialog(element)
            //   });
            // }
            // let findDataWar = response.resources.validations.find(x => x.validateType == ValidateType.INWARRANTY)
            // if (findDataWar) {
            //   this.openServiceCallDialog(findDataWar)
            // }
          }


        }
      })
  }


  // special project call

  checkValidateSpecialProject() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.commonService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SPECIAL_PROEJCT_VALIDATION, undefined,
      false, prepareGetRequestHttpParams(null, null,
        {
          CustomerId: this.customerId,
          AddressId: this.customerAddressId
        }))
      .subscribe(response => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        this.isDealerCall = response.resources.isDealerCall
        // if (response.resources.isSuccess) {
        //   this.navigateToInstallationBySpecialProject()
        // } else {
        //   this.openSpecialProjectDialog(response.resources)
        // }
        if (response.resources.isSuccess) {
          this.navigateToInstallationBySpecialProject()
        } else {
          if (response.resources.validations.length > 0) {
            this.autoIndexSpecialProject = 0
            this.specilProjectValidationsList = response.resources.validations
            this.openSpecialProjectDialog(this.specilProjectValidationsList[0])

          }


        }
      })
  }

  // check validate inspection call
  checkValidateInspectionCall() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.commonService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_VALIDATION, undefined, false, prepareGetRequestHttpParams(null, null,
      {
        CustomerId: this.customerId, AddressId: this.customerAddressId
      })).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.isDealerCall = response.resources.isDealerCall
        if (response.isSuccess == true && response.statusCode == 200) {
          if (response.resources.isSuccess) {
            const alertDialog = this.dialog.open(InspectionCallEscalationDialogComponent, {
              width: '500px',
              data: {
                header: 'Alert',
                message: response.resources.validateMessage,
                buttons: {
                  cancel: 'No',
                  create: 'Yes'
                },
                type: 'alert'
              },
              disableClose: true
            });
            alertDialog.afterClosed().subscribe(result => {
              // if (!result) return;
              if (result) {
                // second pop up starts
                const appointmentDialog = this.dialog.open(InspectionCallEscalationDialogComponent, {
                  width: '500px',
                  data: {
                    header: 'Inspection Call Escalation',
                    type: 'appointment',
                    createdUserId: this.userData.userId,
                    CustomerInspectionId: response.resources.customerInspectionId
                  },
                  disableClose: true
                });
                appointmentDialog.afterClosed().subscribe(result => {
                  if (!result) return;
                  this.rxjsService.setDialogOpenProperty(false);
                });
                // second pop up ends
              } else {
                const appointmentDialog = this.dialog.open(InspectionCallEscalationDialogComponent, {
                  width: '500px',
                  data: {
                    header: 'Create a new inspection call',
                    type: 'anotherinspection',
                    // createdUserId: this.userData.userId,
                    // CustomerInspectionId: response.resources.customerInspectionId
                    pendingInspectionCalls: response.resources.pendingInspectionCalls.length > 0 ? response.resources.pendingInspectionCalls : []
                  },
                  disableClose: true
                });
                appointmentDialog.afterClosed().subscribe(result => {
                  if (!result) return;
                  // this.rxjsService.setDialogOpenProperty(false);
                  this.router.navigate(['/customer/manage-customers/inspection-call'], { queryParams: { referenceId: result, customerId: this.customerId, isNew: 'customerlist' } });
                });
              }
              this.rxjsService.setDialogOpenProperty(false);

            });
          } else {
            const appointmentDialog = this.dialog.open(InspectionCallEscalationDialogComponent, {
              width: '500px',
              data: {
                header: 'Create a new inspection call',
                type: 'anotherinspection',
                pendingInspectionCalls: response.resources.pendingInspectionCalls.length > 0 ? response.resources.pendingInspectionCalls : []
              },
              disableClose: true
            });
            appointmentDialog.afterClosed().subscribe(result => {
              if (!result) return;
              // this.rxjsService.setDialogOpenProperty(false);
              this.router.navigate(['/customer/manage-customers/inspection-call'], { queryParams: { referenceId: result, customerId: this.customerId, isNew: 'customerlist' } });
            });
          }
        }
      });
  }

  // checkValidateDealerCall() {
  //   this.router.navigate(['/customer/manage-customers/dealer-call'], { queryParams: { customerId: this.customerId, customerAddressId: this.customerAddressId, callType: 2 } });

  // }

  //radio removals
  checkValidateRadioRemovals() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.commonService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RADIO_REMOVAL_CALL_VALIDATION,
      undefined,
      false, prepareRequiredHttpParams({
        CustomerId: this.customerId, AddressId: this.customerAddressId, radioRemovalWorkListId: this.radioRemovalWorkListId,
      }))
      .subscribe(response => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.resources?.isSuccess) {
          this.router.navigate(['/customer/manage-customers/radio-removal-call'], {
            queryParams: {
              customerId: this.customerId, addressId: this.customerAddressId, radioRemovalWorkListId: this.radioRemovalWorkListId,
              initiationId: "", appointmentId: this.appointmentId ? this.appointmentId : null, callType: 2,
            }
          })
        } else {
          if (response.resources.validations.length > 0 &&
            response.resources.validations[0]?.validateType?.toLowerCase() != 'escalation') {
            this.onConfirmDialog(response.resources.validations[0]?.validateMessage);
          } else if (response.resources.validations.length > 0 && response.resources.validations[0]?.validateType?.toLowerCase() == 'escalation') {
            this.serviceValidationsList = response.resources.validations[0].validateData;
            const ref = this.dialogService.open(RadioCallEscalationDialogComponent, {
              showHeader: false,
              header: "Radio Removal Call " + response.resources.validations[0]?.validateType,
              baseZIndex: 10000,
              width: '550px',
              data: response.resources.validations[0],
            });
            ref.onClose.subscribe((result) => {
              if (result && result?.validateType?.toLowerCase() == 'escalation') {
                this.openRadioCallEscalationDialog(result);
              }
            })
          }
        }
      })
  }

  onConfirmDialog(message) {
    const dialogData = new ConfirmDialogModel("Alert", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      width: "40vw",
      data: { ...dialogData, isConfirm: false, msgCenter: true, isClose: true },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
      return;
    });
  }

  checkAllValidationCalls(callType: CallType | any) {
    let isAccessDeined = this.getPermissionByActionType(callType);
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (CallType.SERVICE_CALL == callType) {
      this.checkValidateServiceCall()
    } else if (CallType.INSPECTION == callType) {
      this.checkValidateInspectionCall()
      // }else if(CallType.INSTALLATION_CALL == callType){
      //   this.checkValidateServiceCall()
    } else if (CallType.SPECIALPROJECT_CALL == callType) {
      this.checkValidateSpecialProject()
    } else if (CallType.RADIO_REMOVAL == callType) {
      this.checkValidateRadioRemovals()
    }
  }

  openCreditControlNotificationDialog(data) {
    const ref = this.dialogService.open(CreditControlNotificationDialogComponent, {
      showHeader: true,
      header: 'Notification',
      baseZIndex: 10000,
      width: '550px',
      data: data,
    });
    ref.onClose.subscribe((result) => {
      // if (result) {
      this.checkAllValidationCalls(data?.callType)
      // }
    });
  }

  openServiceCallDialog(rowData) {
    rowData['isDealerCall'] = this.isDealerCall
    const ref = this.dialogService.open(ServiceCallWaringDialogComponent, {
      showHeader: true,
      header: "Service Call " + rowData.validateType,
      baseZIndex: 10000,
      width: '550px',
      data: rowData,
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        if (result == 1) {
          this.openServiceCallEscalationDialog(rowData)

        } else if (result == 2) {
          // this.navigateToInstallationByServiceCall()
          if (this.autoIndex < (this.serviceValidationsList.length - 1)) {
            this.autoIndex++;
            this.openServiceCallDialog(this.serviceValidationsList[this.autoIndex])
          } else {
            this.navigateToInstallationByServiceCall()
          }

        } else if (result == 3) {
          this.openInwarrantyEscalationDialog(rowData)
        } else if (result == 4) { // view termination
          // this.openInwarrantyEscalationDialog(rowData)
          // this.rxjsService.setTabIndexForCustomerComponent(6)
          this.redirectToCustomerView.emit({ navigateTab: 6, monitoringTab: 0 });
        }
      } else {
        if (this.autoIndex < (this.serviceValidationsList.length - 1)) {
          this.autoIndex++;
          this.openServiceCallDialog(this.serviceValidationsList[this.autoIndex])
        }

      }
    });
  }

  openInwarrantyEscalationDialog(rowData) {
    const ref1 = this.dialogService.open(ServiceCallInwarrantyDialogComponent, {
      showHeader: true,
      header: "Service Call " + rowData.validateType,
      baseZIndex: 1000,
      width: '550px',
      data: {
        ...rowData,
        createdUserId: this.userData?.userId,
      },
    });
    ref1.onClose.subscribe((result) => {
      // this.dialogService.
      this.dialogRef.close()
      if (result) {
        setTimeout(() => {
          if (this.isDealerCall) {
            var routerUrl = '/customer/manage-customers/dealer-call'
          } else {
            var routerUrl = '/customer/manage-customers/call-initiation'
          }
          let queryParamsObj = {
            customerId: this.customerId,
            customerAddressId: this.customerAddressId,
          }
          if (rowData.validateType == ValidateType.INWARRANTY) {
            queryParamsObj['warrantyRecallReferenceId'] = result?.id;
          }
          if (rowData.validateType == ValidateType.ESCALAATION) {
            queryParamsObj['initiationId'] = result?.id;
            queryParamsObj['escalationPopup'] = true;
          }
          if (this.appointmentId) {
            queryParamsObj['appointmentId'] = this.appointmentId;
          }
          queryParamsObj['callType'] = 2;// 2- service call, 1- installation call
          this.router.navigate([routerUrl], {
            queryParams: queryParamsObj, skipLocationChange: false
          });
        }, 100);

      }
    });
  }

  openServiceCallEscalationDialog(rowData) {
    const objName = CUSTOMER_COMPONENT.SERVICE;
    const escalationEditPermission = this.rolePermission?.find(el => el?.[objName])?.[objName]?.find(el => el?.menuName == PermissionTypes.QUICK_ACTION)?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.ESCALAATION)?.subMenu?.find(el => el?.menuName == PermissionTypes.EDIT);
    const ref = this.dialogService.open(CallEscalationDialogComponent, {
      width: '550px',
      header: "Service Call " + rowData.validateType,
      data: {
        ...rowData,
        createdUserId: this.userData?.userId,
        editPermission: escalationEditPermission,
      },
      showHeader: false,
    });
    ref.onClose.subscribe((result) => {
      this.dialogRef.close()
      if (result) {
        if (this.isDealerCall) {
          var routerUrl = '/customer/manage-customers/dealer-call'
        } else {
          var routerUrl = '/customer/manage-customers/call-initiation'
        }
        this.router.navigate([routerUrl], {
          queryParams: {
            customerId: this.customerId,
            customerAddressId: this.customerAddressId,
            appointmentId: this.appointmentId ? this.appointmentId : null,
            warrantyRecallReferenceId: (rowData.validateType == ValidateType.INWARRANTY) ? result : null,
            initiationId: (rowData.validateType == ValidateType.ESCALAATION) ? result : null,
            callType: 2, // 2- service call, 1- installation call
            escalationPopup: (rowData.validateType == ValidateType.ESCALAATION) ? true : false
          }, skipLocationChange: false
        });
      }
    });
  }

  openRadioCallEscalationDialog(rowData) {
    const ref1 = this.dialogService.open(RadioCallEscalationDialogComponent, {
      showHeader: false,
      header: "Radio Removal Call " + rowData.validateType,
      baseZIndex: 1000,
      width: '550px',
      data: {
        ...rowData,
        createdUserId: this.userData?.userId,
        radioRemovalWorkListId: this.radioRemovalWorkListId,
        isRadioEscalation: true,
      },
    });
    ref1.onClose.subscribe((result) => {
      this.dialogRef.close()
      if (result) {
        // var routerUrl = '/customer/manage-customers/radio-removal-call'
        // this.router.navigate([routerUrl], {
        //   queryParams: {
        //     customerId: this.customerId,
        //     customerAddressId: this.customerAddressId,
        //     appointmentId: this.appointmentId ? this.appointmentId : null,
        //     warrantyRecallReferenceId: (rowData.validateType == ValidateType.INWARRANTY) ? result : null,
        //     initiationId: (rowData.validateType == ValidateType.ESCALAATION) ? result : null,
        //     callType: 2, // 2- service call, 1- installation call
        //     escalationPopup: (rowData.validateType == ValidateType.ESCALAATION) ? true : false
        //   }, skipLocationChange: true
        // });
      }
    });
  }

  // openServiceCallWarrantyMessageDialog(rowData) {
  //   const ref2 = this.dialogService.open(ServiceCallWarrantyWarningDialogComponent, {
  //     showHeader: true,
  //     header: "Service Call " + rowData.validateType,
  //     baseZIndex: 10000,
  //     width: '550px',
  //     data: rowData,
  //   });
  //   ref2.onClose.subscribe((result) => {
  //     if (result) {
  //       if (result == 1) {
  //         this.openServiceCallWarrantyDialog(rowData)

  //       } else if (result == 2) {
  //         this.navigateToInstallationByServiceCall()
  //       }
  //     }
  //   });
  // }

  // openServiceCallWarrantyDialog(rowData) {
  //   const ref = this.dialogService.open(ServiceCallWarrantyDialogComponent, {
  //     showHeader: true,
  //     header: "Service Call " + rowData.validateType,
  //     baseZIndex: 10000,
  //     width: '550px',
  //     data: rowData,
  //   });
  //   ref.onClose.subscribe((result) => {
  //     this.dialogRef.close()
  //     if (result) {
  //       this.router.navigate(['/customer/manage-customers/call-initiation'], {
  //         queryParams: {
  //           customerId: this.customerId,
  //           customerAddressId: this.customerAddressId,
  //           warrantyRecallReferenceId: (rowData.validateType == ValidateType.INWARRANTY) ? result : null,
  //           initiationId: (rowData.validateType == ValidateType.ESCALAATION) ? result : null,
  //           callType: 2, // 2- service call, 1- installation call
  //           escalationPopup: (rowData.validateType == ValidateType.ESCALAATION) ? true : false
  //         }, skipLocationChange: true
  //       });
  //     }
  //   });
  // }

  openSpecialProjectDialog(rowData) {
    rowData['isDealerCall'] = this.isDealerCall
    const ref = this.dialogService.open(SpecialProjectWaringDialogComponent, {
      showHeader: true,
      header: rowData.validateType ? rowData.validateType : "Special Project",
      baseZIndex: 10000,
      width: '550px',
      data: rowData,
    });
    ref.onClose.subscribe((result) => {
      // if (result) {
      //   if (result == 2) {
      //     this.navigateToInstallationBySpecialProject()
      //   }
      // }
      if (result) {
        // if (result == 1) {
        //   this.openServiceCallEscalationDialog(rowData)

        // } else
        if (result == 2) {
          if (this.autoIndexSpecialProject < (this.specilProjectValidationsList.length - 1)) {
            this.autoIndexSpecialProject++;
            this.openSpecialProjectDialog(this.specilProjectValidationsList[this.autoIndexSpecialProject])
          } else {
            this.navigateToInstallationBySpecialProject()
          }

          // } else if (result == 3) {
          //   this.openInwarrantyEscalationDialog(rowData)
        } else if (result == 4) { // view termination
          // this.rxjsService.setTabIndexForCustomerComponent(6)
          this.redirectToCustomerView.emit({ navigateTab: 6, monitoringTab: 0 });
        }
      } else {
        if (this.autoIndexSpecialProject < (this.specilProjectValidationsList.length - 1)) {
          this.autoIndexSpecialProject++;
          this.openSpecialProjectDialog(this.specilProjectValidationsList[this.autoIndexSpecialProject])
        }

      }
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  getPermissionByActionType(actionTypeMenuName): boolean {
    let foundObj = this.allPermission?.subMenu.find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }
}
