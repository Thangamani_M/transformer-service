enum CallType{
    SERVICE_CALL = 'Service',
    INSTALLATION_CALL = 'Installation',
    SPECIALPROJECT_CALL = 'Special Project',
    INSPECTION = 'Inspection',
    RADIO_REMOVAL = 'Radio Removal',
}

enum ValidateType{
    DEBTOR = 'Debtor',
    TECH_AREA = 'Tech Area',
    ESCALAATION = 'Escalation',
    INWARRANTY = 'In Warranty',
}

export { CallType, ValidateType };
