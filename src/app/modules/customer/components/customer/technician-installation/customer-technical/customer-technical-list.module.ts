import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CallEscalationModule } from '../call-escalation-dialog/call-escalation-dialog.module';
import { CreditControlNotificationDialogModule } from '../credit-control-notification-dialog/credit-control-notification-dialog.module';
import { DocumentListModule } from '../document-list/document-list.module';
import { InspectionCallEscalationDialogModule } from '../inspection-escalation-modal/inspection-call-escalation-dialog.module';
import { ObNumberListModule } from '../ob-number-list/ob-number-list.module';
import { RadioCallEscalationModule } from '../radio-call-escalation-dialog/radio-call-escalation-dialog.module';
import { ServiceCallInwarrantyDialogModule } from '../service-call-inwarranty-dialog/service-call-inwarranty-dialog.module';
import { ServiceCallWaringDialogModule } from '../service-call-waring-dialog/service-call-waring-dialog.module';
import { SpecialProjectWaringDialogModule } from '../special-project-waring-dialog/special-project-waring-dialog.module';
import { TechnicianAuditListModule } from '../technician-audit-list/technician-audit-list.module';
import { CustomerTechnicaListComponent } from './customer-technical-list.component';

@NgModule({
  declarations: [ CustomerTechnicaListComponent ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule,
    DocumentListModule,
    ObNumberListModule,
    TechnicianAuditListModule,
    ServiceCallWaringDialogModule,
    CallEscalationModule,
    RadioCallEscalationModule,
    ServiceCallInwarrantyDialogModule,
    SpecialProjectWaringDialogModule,
    InspectionCallEscalationDialogModule,
    CreditControlNotificationDialogModule,
  ],
  exports: [CustomerTechnicaListComponent],
  entryComponents: [],
})
export class CustomerTechnicaListModule { }
