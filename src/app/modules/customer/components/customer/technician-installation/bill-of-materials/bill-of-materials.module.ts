import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { PrimengConfirmDialogPopupModule } from '@app/shared/components/primeng-confirm-dialog-popup/primeng-confirm-dialog-popup.module';
import { MaterialModule } from '@app/shared/material.module';
import { BillOfMaterialScanModalComponent, BillOfMaterialUpdateComponent, BillOfMaterialValidationModalComponent, BillOfMaterialZoneModalComponent, DiscountOnWarrantyMaintenanceModalComponent, JobCardAcceptanceComponent, ProcessPaymentModalComponent, ServiceForDiscountModalComponent } from '@modules/customer';
// import { BillOfMaterialsRoutingModule } from '.';
import { SignaturePadModule } from 'ngx-signaturepad';
import { DynamicDialogModule } from 'primeng/components/dynamicdialog/dynamicdialog';
import { CustomerDecoderAddDialogModule } from '../../customer-management/customer-decoder/customer-decoder-add-dialog/customer-decoder-add-dialog.module';
import { CustomerZoneAddEditModule } from '../../customer-management/customer-zone/customer-zone-add-edit.module';
import { BillOfMaterialsRoutingModule } from './bill-of-materials-routing.module';
import { NKADoaRequestDialogModule } from './nka-doa-request-dialog/nka-doa-request-dialog.module';
import { ProcessPaymentInstallationModule } from './process-payment-installation-model/process-payment-installation-model.module';

@NgModule({
    declarations: [
        BillOfMaterialUpdateComponent,
        JobCardAcceptanceComponent,
        BillOfMaterialScanModalComponent,
        DiscountOnWarrantyMaintenanceModalComponent,
        ServiceForDiscountModalComponent,
        ProcessPaymentModalComponent,
        BillOfMaterialValidationModalComponent,
    BillOfMaterialZoneModalComponent],
    imports: [
        CommonModule,
        BillOfMaterialsRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        DynamicDialogModule,
        SignaturePadModule,
        CustomerDecoderAddDialogModule,
        CustomerZoneAddEditModule,
        ProcessPaymentInstallationModule,
        PrimengConfirmDialogPopupModule,
        NKADoaRequestDialogModule,
    ],
    entryComponents: [
        BillOfMaterialScanModalComponent,
        ServiceForDiscountModalComponent,
        BillOfMaterialZoneModalComponent,
        DiscountOnWarrantyMaintenanceModalComponent,
        BillOfMaterialValidationModalComponent,
        ProcessPaymentModalComponent,
    ]
})
export class BillOfMaterialsModule { }