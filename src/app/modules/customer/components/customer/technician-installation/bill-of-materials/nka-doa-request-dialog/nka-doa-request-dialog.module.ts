import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NkaDoaRequestDialogComponent } from './nka-doa-request-dialog.component';

@NgModule({
  declarations: [NkaDoaRequestDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  exports: [NkaDoaRequestDialogComponent],
  entryComponents: [NkaDoaRequestDialogComponent],
})
export class NKADoaRequestDialogModule { }
