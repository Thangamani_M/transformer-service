import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { CrudService, RxjsService } from '@app/shared';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-bill-of-material-zone-modal',
  templateUrl: './bill-of-material-zone-modal.component.html'
})
export class BillOfMaterialZoneModalComponent implements OnInit {

  zoneList = [
    {id: '1', displayName: '1'},
    {id: '1', displayName: '2'},
    {id: '1', displayName: '3'},
    {id: '1', displayName: '4'}
  ];

  equipmentList = [
    {id: '1', displayName: 'Camera'}
  ];
  ownershipList = [
    {id: '1', displayName: 'Cash'},
    {id: '1', displayName: 'Office'}
  ];

  constructor(@Inject(MAT_DIALOG_DATA) public popupData: any) { }

  ngOnInit(): void {
    // let messageSpan = document.createElement('span');
    // messageSpan.innerHTML = this.popupData['message'];
    // document.getElementById('parentContainer').appendChild(messageSpan);
  }

}
