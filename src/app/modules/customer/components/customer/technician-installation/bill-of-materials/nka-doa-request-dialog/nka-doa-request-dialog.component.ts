import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RxjsService, CrudService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, IApplicationResponse, ResponseMessageTypes, SnackbarService, ConfirmDialogModel, PrimengConfirmDialogPopupComponent, prepareRequiredHttpParams, setRequiredValidator, OtherService, clearFormControlValidators, CustomDirectiveConfig } from '@app/shared';
import { NKADoaRequestDialog, NKADoaRequestItemArray } from '@modules/customer/models/bill-of-material.model';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-nka-doa-request-dialog',
  templateUrl: './nka-doa-request-dialog.component.html',
  styles: [`::ng-deep .escalate-text, .label-text {
    font-size: 14px;
  }`]
})
export class NkaDoaRequestDialogComponent implements OnInit {

  nkaDoaRequestForm: FormGroup;
  stockCodeErrorMessage: string;
  showStockCodeError: boolean;
  isStockError: boolean;
  filteredPRStockCodes: any = [];
  isLoading: boolean;
  isSubmitted: boolean;
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  tabsList: any = [
    {
      dataKey: 'NKAPurchaseOrderRequestItemId',
      formArrayName: 'itemRequestDetailsArray',
      columns: [
        { field: 'itemCode', displayName: 'Stock Code', type: 'input_text', className: 'col-1 pr-0', isTooltip: true },
        { field: 'itemName', displayName: 'Stock Description', type: 'input_text', className: 'col-3', isTooltip: true },
        { field: 'quantity', displayName: 'Qty', type: 'input_text', className: 'col-1 p-0', validateInput: this.numberConfig, maxlength: 8, isClickEvent: true },
        { field: 'unitPriceExclVat', displayName: 'Unit Price Excl. Vat', type: 'input_text', className: 'col-2 pr-0', inputClassName: 'text-right' },
        { field: 'subTotalExclVat', displayName: 'Sub Total Excl. Vat', type: 'input_text', className: 'col-2 pr-0', inputClassName: 'text-right' },
        { field: 'subTotalInclVat', displayName: 'Sub Total Incl. Vat', type: 'input_text', className: 'col-2 pr-0', inputClassName: 'text-right' },
      ],
      isRemoveFormArray: true,
      formArrayActionClass: 'col-1',
    }
  ];

  constructor(private rxjsService: RxjsService, @Inject(MAT_DIALOG_DATA) public data: any, private formBuilder: FormBuilder,
    public ref: MatDialogRef<NkaDoaRequestDialogComponent>, private crudService: CrudService, private snackbarService: SnackbarService,
    public dialogService: DialogService, private otherService: OtherService,) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.initForm();
    this.onLoadValue();
  }

  initForm(nkaDoaRequestDialog?: NKADoaRequestDialog) {
    let nkaDoaRequestModel = new NKADoaRequestDialog(nkaDoaRequestDialog);
    this.nkaDoaRequestForm = this.formBuilder.group({});
    Object.keys(nkaDoaRequestModel).forEach((key) => {
      if (typeof nkaDoaRequestModel[key] === 'object') {
        this.nkaDoaRequestForm.addControl(key, new FormArray(nkaDoaRequestModel[key]));
      } else {
        this.nkaDoaRequestForm.addControl(key, new FormControl(nkaDoaRequestModel[key]));
      }
    });
    this.nkaDoaRequestForm = setRequiredValidator(this.nkaDoaRequestForm, ['comments']);
    this.onFormValueChanges();
  }

  onFormValueChanges() {
    this.nkaDoaRequestForm.get('equipment').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        if (searchText != null) {
          if (!searchText) {
            if (searchText === '') {
              this.stockCodeErrorMessage = '';
              this.showStockCodeError = false;
              this.isStockError = false;
            }
            else {
              this.stockCodeErrorMessage = 'Stock code is not available in the system';
              this.showStockCodeError = true;
              this.isStockError = true;
            }
            return this.filteredPRStockCodes = [];
          } else {
            if (typeof searchText == 'object') {
              searchText = searchText?.itemCode;
            }
            let searchStockObject = Object.assign({},
              searchText == null ? null : { ItemCode: searchText },
              // this.stockCodeDescriptionForm.get('stockType').value == null ? null : { stockType: this.stockCodeDescriptionForm.get('stockType').value },
              this.data?.billOfMaterialModel?.callInitiationId == null ? null : { callInitiationId: this.data?.billOfMaterialModel?.callInitiationId },
              this.data?.billOfMaterialModel?.districtId == null ? null : { districtId: this.data?.billOfMaterialModel?.districtId },
              this.data?.billOfMaterialModel?.warehouseId == null ? null : { warehouseId: this.data?.billOfMaterialModel?.warehouseId },
              this.data?.billOfMaterialModel?.itemPricingConfigId == null ? null : { itemPricingConfigId: this.data?.billOfMaterialModel?.itemPricingConfigId },
              this.data?.billOfMaterialModel?.technicianId == null ? null : { technicianId: this.data?.billOfMaterialModel?.technicianId });
            if (this.data?.stockType?.toLowerCase() == 'm') {
              return of([]);
            } else if (this.data?.stockType?.toLowerCase() == 'y') {
              if (this.data?.billOfMaterialModel?.technicianId) {
                this.stockCodeErrorMessage = '';
                this.showStockCodeError = false;
                this.isStockError = false;
                return this.crudService.get(
                  ModulesBasedApiSuffix.TECHNICIAN,
                  TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEARCH_STOCK_DETAILS_TECHNICIAN, null, true,
                  prepareGetRequestHttpParams(null, null, searchStockObject))
              } else {
                this.stockCodeErrorMessage = 'Stock code is not available against technician stock location';
                this.showStockCodeError = true;
                this.isStockError = true;
                return this.filteredPRStockCodes = [];
              }
            }
          }
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredPRStockCodes = response.resources;
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;
        } else {
          this.filteredPRStockCodes = [];
          this.stockCodeErrorMessage = 'Stock code is not available in the system';
          this.showStockCodeError = true;
          this.isStockError = true;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onLoadValue() {
    if (this.data?.response?.equipments) {
      this.data?.response?.equipments?.forEach(el => {
        let newItem = {};
        if (el?.itemId == this.data?.newData?.itemId) {
          let quantity = +el?.quantity + +this.data?.newData?.quantity;
          let subTotal = quantity * el?.unitPriceExclVat;
          let vatAmount = subTotal * (this.data?.tax / 100);
          let totalAmount = subTotal + vatAmount;
          newItem = {
            NKAPurchaseOrderRequestItemId: el?.nkaPurchaseOrderRequestItemId,
            NKAPurchaseOrderRequestId: this.data?.response?.nkaPurchaseOrderRequestId,
            itemId: el?.itemId,
            itemCode: el?.itemCode,
            itemName: el?.itemName,
            quantity: quantity,
            unitPriceExclVat: el?.unitPriceExclVat ? this.otherService.transformDecimal(el?.unitPriceExclVat) : '',
            subTotalExclVat: subTotal ? this.otherService.transformDecimal(subTotal?.toFixed(2)) : '',
            subTotalInclVat: totalAmount ? this.otherService.transformDecimal(totalAmount?.toFixed(2)) : '',
            itemPricingConfigId: this.otherService.transformDecimal(el?.itemPricingConfigId),
          };
        } else {
          newItem = {
            NKAPurchaseOrderRequestItemId: el?.nkaPurchaseOrderRequestItemId,
            NKAPurchaseOrderRequestId: this.data?.response?.nkaPurchaseOrderRequestId,
            itemId: el?.itemId,
            itemCode: el?.itemCode,
            itemName: el?.itemName,
            quantity: el?.quantity,
            unitPriceExclVat: el?.unitPriceExclVat ? this.otherService.transformDecimal(el?.unitPriceExclVat) : '',
            subTotalExclVat: el?.subTotalExclVat ? this.otherService.transformDecimal(el?.subTotalExclVat?.toFixed(2)) : '',
            subTotalInclVat: el?.subTotalInclVat ? this.otherService.transformDecimal(el?.subTotalInclVat?.toFixed(2)) : '',
            itemPricingConfigId: this.otherService.transformDecimal(el?.itemPricingConfigId),
          };
        }
        this.initFormArray(newItem);
      });
      if(!this.onValidateAlreadyExist()) {
        this.createNewItem();
      }
    } else {
      this.createNewItem();
    }
  }

  onValidateAlreadyExist() {
    let found = [];
    this.data?.response?.equipments?.forEach(el => {
      if (el?.itemId == this.data?.newData?.itemId) {
        found.push(true);
      }
    });
    return found?.length;
  }

  createNewItem() {
    this.data.newData.unitPriceExclVat = this.data?.newData?.unitPriceExclVat ? this.otherService.transformDecimal(this.data?.newData?.unitPriceExclVat?.toFixed(2)) : '';
    this.data.newData.subTotalExclVat = this.data?.newData?.subTotalExclVat ? this.otherService.transformDecimal(this.data?.newData?.subTotalExclVat?.toFixed(2)) : '';
    this.data.newData.subTotalInclVat = this.data?.newData?.subTotalInclVat ? this.otherService.transformDecimal(this.data?.newData?.subTotalInclVat?.toFixed(2)) : '';
    this.initFormArray(this.data?.newData);
  }

  initFormArray(nkaDoaRequestItemArray?: NKADoaRequestItemArray) {
    let nkaDoaRequestModel = new NKADoaRequestItemArray(nkaDoaRequestItemArray);
    let nkaDoaRequestFormArray = this.formBuilder.group({});
    Object.keys(nkaDoaRequestModel).forEach((key) => {
      nkaDoaRequestFormArray.addControl(key, new FormControl({ value: nkaDoaRequestModel[key], disabled: key == 'quantity' ? false : true }));
    });
    nkaDoaRequestFormArray = setRequiredValidator(nkaDoaRequestFormArray, ['itemId', 'itemCode', 'itemName', 'quantity', 'unitPriceExclVat', 'subTotalExclVat', 'subTotalInclVat']);
    this.itemRequestFormArray.push(nkaDoaRequestFormArray);
    this.calculateTotal();
  }

  calculateTotal() {
    this.nkaDoaRequestForm?.get('totalStockItems').setValue(0);
    let total = 0;
    this.itemRequestFormArray.getRawValue()?.forEach((item) => {
      total += +this.otherService.transformCurrToNum(item.subTotalInclVat);
      this.nkaDoaRequestForm?.get('totalStockItems').setValue(total ? this.otherService.transformDecimal(+total?.toFixed(2)) : '');
    })
  }

  changeEscalate() {
    this.data.isEscalate = true;
  }

  get itemRequestFormArray(): FormArray {
    if (!this.nkaDoaRequestForm) return;
    return this.nkaDoaRequestForm.get('itemRequestDetailsArray') as FormArray;
  }

  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  addEquipmentLabor() {
    if(!this.nkaDoaRequestForm.get('equipment').value) {
      this.nkaDoaRequestForm = setRequiredValidator(this.nkaDoaRequestForm, ["equipment"]);
      this.nkaDoaRequestForm.get('equipment').markAsTouched();
      return;
    }
    let addEquipmentLaborVal = this.nkaDoaRequestForm.get('equipment').value;
    if (addEquipmentLaborVal?.itemCode) {
      // let item = this.filteredPRStockCodes?.find(x => x?.itemCode == addEquipmentLaborVal);
      let filterItem = this.itemRequestFormArray.getRawValue()?.filter(x => x?.itemCode == addEquipmentLaborVal?.itemCode);
      if (filterItem && filterItem.length > 0) {
        this.snackbarService.openSnackbar('Stock Code is already added.', ResponseMessageTypes.WARNING)
        return;
      }
      let newItem = {
        NKAPurchaseOrderRequestItemId: null,
        NKAPurchaseOrderRequestId: null,
        itemId: addEquipmentLaborVal?.id,
        itemCode: addEquipmentLaborVal?.itemCode,
        itemName: addEquipmentLaborVal?.displayName,
        quantity: '',
        unitPriceExclVat: addEquipmentLaborVal?.costPrice ? this.otherService.transformDecimal(addEquipmentLaborVal?.costPrice) : '',
        subTotalExclVat: '0',
        subTotalInclVat: '0',
        itemPricingConfigId: addEquipmentLaborVal?.itemPricingConfigId,
      };
      this.initFormArray(newItem);
      this.nkaDoaRequestForm.get('equipment').setValue('', {emitEvent: false});
      this.nkaDoaRequestForm = clearFormControlValidators(this.nkaDoaRequestForm, ["equipment"]);
      this.nkaDoaRequestForm.get('equipment').markAsUntouched();
    }
  }

  displayFn(val) {
    if (val) { return val?.itemCode ? val?.itemCode : val; } else { return val }
  }

  changeValueItem(e) {
    if (e?.column?.field == 'quantity') {
      let val = this.itemRequestFormArray?.controls[e?.index]?.get(e?.column?.field)?.value;
      let subTotal = val * this.otherService.transformCurrToNum(this.itemRequestFormArray?.controls[e?.index]?.get('unitPriceExclVat').value);
      let vatAmount = subTotal * (this.data?.tax / 100);
      let totalAmount = subTotal + vatAmount;
      this.itemRequestFormArray?.controls[e?.index]?.get('subTotalExclVat').patchValue(subTotal ? this.otherService.transformDecimal(subTotal?.toFixed(2)) : '');
      this.itemRequestFormArray?.controls[e?.index]?.get('subTotalInclVat').patchValue(totalAmount ? this.otherService.transformDecimal(totalAmount?.toFixed(2)) : ''); // vatAmount
      this.calculateTotal();
    }
  }

  removeNKARequestItem(i) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialogService.open(PrimengConfirmDialogPopupComponent, {
      width: "400px",
      data: dialogData,
      showHeader: false,
      baseZIndex: 8000,
    });
    dialogRef.onClose.subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.itemRequestFormArray?.controls[i]?.get('NKAPurchaseOrderRequestItemId')?.value) {
        this.isSubmitted = true;
        this.crudService.delete(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.NKA_SERVICE_CALL, null, prepareRequiredHttpParams({ ids: this.itemRequestFormArray?.controls[i]?.get('NKAPurchaseOrderRequestItemId')?.value, modifiedUserId: this.data?.userId }))
          .subscribe((response: IApplicationResponse) => {
            if (response?.isSuccess && response?.statusCode == 200) {
              this.clearFormArray(this.itemRequestFormArray);
              this.onLoadAPI();
            }
            this.isSubmitted = false;
            this.rxjsService.setGlobalLoaderProperty(false);
          })
      } else {
        this.itemRequestFormArray.removeAt(i);
      }
    });
  }

  onLoadAPI() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.NKA_SERVICE_CALL_PURCHASE_ORDER,
      null, null, prepareRequiredHttpParams({ callInitiationId: this.data?.billOfMaterialModel?.callInitiationId }))
      .subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200) {
          this.data.response = response?.resources;
          this.onLoadValue();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  btnCloseClick() {
    this.rxjsService.setDialogOpenProperty(false);
    this.ref.close(false);
  }

  onSubmitDialog() {
    this.nkaDoaRequestForm?.markAsUntouched();
    if (this.nkaDoaRequestForm?.invalid) {
      this.nkaDoaRequestForm?.markAllAsTouched();
      return;
    }
    if (this.itemRequestFormArray?.invalid) {
      this.itemRequestFormArray?.markAllAsTouched();
      return;
    }
    const reqObj = this.getValue();
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.NKA_SERVICE_CALL_PURCHASE_ORDER, reqObj)
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode) {
          this.ref.close(true);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
      })
  }

  getValue() {
    let requestBody = {
      NKAPurchaseOrderRequestId: this.data?.response?.nkaPurchaseOrderRequestId ? this.data?.response?.nkaPurchaseOrderRequestId : null,
      callInitiationId: this.data?.billOfMaterialModel?.callInitiationId,
      comments: this.nkaDoaRequestForm?.get('comments')?.value,
      createdUserId: this.data?.userId,
      items: [],
    }
    this.itemRequestFormArray?.getRawValue()?.forEach((el: any) => {
      if (el) {
        requestBody.items.push({
          NKAPurchaseOrderRequestItemId: el?.NKAPurchaseOrderRequestItemId,
          NKAPurchaseOrderRequestId: el?.NKAPurchaseOrderRequestId,
          itemId: el?.itemId,
          quantity: el?.quantity,
          unitPriceExclVat: el?.unitPriceExclVat ? this.otherService.transformCurrToNum(el?.unitPriceExclVat) : '',
          subTotalExclVat: el?.subTotalExclVat ? this.otherService.transformCurrToNum(el?.subTotalExclVat) : '0',
          subTotalInclVat: el?.subTotalExclVat ? this.otherService.transformCurrToNum(el?.subTotalInclVat) : '0',
        })
      }
    })
    return requestBody;
  }

}
