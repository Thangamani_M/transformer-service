import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { CrudService, RxjsService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, IApplicationResponse, setRequiredValidator, CustomDirectiveConfig, prepareRequiredHttpParams } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';

@Component({
    selector: 'app-service-for-discount-modal',
    templateUrl: './service-for-discount-modal.component.html'
})
export class ServiceForDiscountModalComponent implements OnInit {

    serviceForDiscountForm: FormGroup;
    userData: UserLogin;
    showForm = false;
    BillofMaterialId: string;
    DiscountAmountExVat: string;
    DiscountAmountInVat: string;
    createdUserId: string;
    technicialCallDiscountRequestId: string;
    itemId: string;

    constructor(
        @Inject(MAT_DIALOG_DATA) public popupData: any,
        private formBuilder: FormBuilder,
        private dialogRef: MatDialogRef<ServiceForDiscountModalComponent>,
        private crudService: CrudService,
        private rxjsService: RxjsService,
        private store: Store<AppState>) { 
        // this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        //     if (!userData) return;
        //     this.userData = userData;
        // })
        this.BillofMaterialId = popupData['BillofMaterialId'];
        this.DiscountAmountExVat = popupData['DiscountAmountExVat'];
        this.DiscountAmountInVat = popupData['DiscountAmountInVat'];
        this.createdUserId = popupData['createdUserId'];
        this.technicialCallDiscountRequestId = popupData['technicialCallDiscountRequestId'];    
        this.itemId = popupData['itemId'];    
    }

    ngOnInit(): void {
        this.createForm();
        if (this.technicialCallDiscountRequestId) {
            this.getDetails();
            this.displayForm();
        }
    }

    btnClose() {
        this.dialogRef.close(this.popupData);
    }

    createForm() {
        this.serviceForDiscountForm = this.formBuilder.group({});
        this.serviceForDiscountForm.addControl('BillofMaterialId', new FormControl(this.BillofMaterialId));
        this.serviceForDiscountForm.addControl('Comment', new FormControl());
        this.serviceForDiscountForm.addControl('itemId', new FormControl(this.itemId));
        this.serviceForDiscountForm.addControl('DiscountAmountExVat', new FormControl(this.DiscountAmountExVat));
        this.serviceForDiscountForm.addControl('DiscountAmountInVat', new FormControl(this.DiscountAmountInVat));
        this.serviceForDiscountForm.addControl('CreatedUserId', new FormControl(this.createdUserId));
        // this.serviceForDiscountForm = setRequiredValidator(this.serviceForDiscountForm, ['specialProjectTypeName', 'isActive']);
    }

    getDetails() {
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SERVICE_CALL_DISCOUNT, null, false, prepareRequiredHttpParams({ TechnicialCallDiscountRequestId: this.technicialCallDiscountRequestId })).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess == true && response.statusCode == 200) {
                    this.serviceForDiscountForm.get('Comment').setValue(response.resources['motivation']);
                    this.serviceForDiscountForm.get('Comment').disable();
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    displayForm() {
        this.showForm = true;
    }

    updateServiceForDiscount() {
        this.crudService.create(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SERVICE_CALL_DISCOUNT,
            this.serviceForDiscountForm.value
          ).subscribe((response: IApplicationResponse) => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if (response.isSuccess == true && response.statusCode == 200) {
            this.dialogRef.close(true);
            }
          });
    }

}