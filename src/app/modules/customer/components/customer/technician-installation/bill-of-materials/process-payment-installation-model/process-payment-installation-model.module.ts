import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DynamicDialogModule } from 'primeng/components/dynamicdialog/dynamicdialog';
import { ProcessPaymentInstallationModelComponent } from './process-payment-installation-model.component';

@NgModule({
    declarations: [ProcessPaymentInstallationModelComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        DynamicDialogModule,
    ],
    entryComponents: [ProcessPaymentInstallationModelComponent],
    exports: [ProcessPaymentInstallationModelComponent],
})
export class ProcessPaymentInstallationModule { }