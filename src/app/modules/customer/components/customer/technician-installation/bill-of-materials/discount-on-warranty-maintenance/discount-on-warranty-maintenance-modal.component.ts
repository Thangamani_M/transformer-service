import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { CrudService, RxjsService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, IApplicationResponse, setRequiredValidator, CustomDirectiveConfig, prepareRequiredHttpParams, setMinMaxValidator } from '@app/shared';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';

@Component({
    selector: 'app-discount-on-warranty-maintenance-modal',
    templateUrl: './discount-on-warranty-maintenance-modal.component.html'
})
export class DiscountOnWarrantyMaintenanceModalComponent implements OnInit {

    serviceForDiscountForm: FormGroup;
    userData: UserLogin;
    showForm = false;
    BillofMaterialId: string;
    DiscountAmountExVat: string;
    DiscountAmountInVat: string;
    createdUserId: string;
    technicialCallDiscountRequestId: string;

    //
    stocksAppliedForWarranty: any = [];
    isNumericOnly = new CustomDirectiveConfig({ isANumberWithZero: true });

    constructor(
        @Inject(MAT_DIALOG_DATA) public popupData: any,
        private formBuilder: FormBuilder,
        private dialogRef: MatDialogRef<DiscountOnWarrantyMaintenanceModalComponent>,
        private crudService: CrudService,
        private rxjsService: RxjsService,
        private store: Store<AppState>) {
        // this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        //     if (!userData) return;
        //     this.userData = userData;
        // })
        this.stocksAppliedForWarranty = popupData['stocksAppliedForWarranty'];        
    }

    ngOnInit(): void {
        this.createForm();       
    }

    createForm() {
        this.serviceForDiscountForm = this.formBuilder.group({});
        this.serviceForDiscountForm.addControl('stocksForWarrantyList', this.formBuilder.array([]));
        let stocksForWarrantyFormArray = this.stocksForWarrantyFormArray;
        this.stocksAppliedForWarranty.forEach(element => {
            element = element.value;
            let stocksAppliedForWarrantyFormGroup = this.formBuilder.group({
                itemId: [{ value: element['itemId'], disabled: true }],
                itemCode: [{ value: element['itemCode'], disabled: true }],
                itemName: [{ value: element['itemName'], disabled: true }], // reserved: element['reserved'],
                // request: [{ value: element['WarrantyQty']? element['WarrantyQty'] : element['request'], disabled: false }],
                request: [{ value: element['request'], disabled: true }], // Original Request qty
                unitPrice: [{ value: (+element['unitPrice']).toFixed(2), disabled: true }],
                subTotal: [{ value: (+element['subTotal']).toFixed(2), disabled: true }],
                IsWarranty: [{ value: element['IsWarranty'], disabled: true }],
                previousWarrantyQty: [{ value: element['WarrantyQty'], disabled: true }],
                reserved: [{ value: element['reserved'], disabled: true }],
                used: [{ value: element['used'], disabled: true }],
                WarrantyQty: [ element['WarrantyQty'], [Validators.required]], // , disabled: false
                isRecordChanged: false
            });
            let compareWith = element['stockType'].toLowerCase() == 'y'? 'reserved': 'used';
            stocksAppliedForWarrantyFormGroup = setMinMaxValidator(stocksAppliedForWarrantyFormGroup, [
                { formControlName: 'WarrantyQty', compareWith: compareWith, type: 'minEqual' } // compareWith: 'request'
            ]);
            stocksAppliedForWarrantyFormGroup.get('WarrantyQty').valueChanges.subscribe(qty => {
                qty = +qty;
                if (qty > 0){
                    // stocksAppliedForWarrantyFormGroup.get('subTotal').setValue(+(qty * stocksAppliedForWarrantyFormGroup.get('unitPrice').value.toFixed(2)).toFixed(2));
                    stocksAppliedForWarrantyFormGroup.get('subTotal').setValue(+(qty * +stocksAppliedForWarrantyFormGroup.get('unitPrice').value).toFixed(2));
                } else {
                    stocksAppliedForWarrantyFormGroup.get('subTotal').setValue(0);
                }
                // stocksAppliedForWarrantyFormGroup.get('WarrantyQty').setValue(qty);
                stocksAppliedForWarrantyFormGroup.get('isRecordChanged').setValue(true);
            });
            stocksAppliedForWarrantyFormGroup.get('WarrantyQty').setValue(element['WarrantyQty'] ? element['WarrantyQty'] : element[compareWith]); // element['request']
            stocksForWarrantyFormArray.push(stocksAppliedForWarrantyFormGroup);
        });
    }

    get stocksForWarrantyFormArray(): FormArray {
        if (this.serviceForDiscountForm !== undefined) {
            return (<FormArray>this.serviceForDiscountForm.get('stocksForWarrantyList'));
        }
    }

    updateWarrantyData() {
        if (this.serviceForDiscountForm.invalid) {
            this.serviceForDiscountForm.markAllAsTouched();
            return;
        }       
        this.rxjsService.setFormChangeDetectionProperty(true);
        this.dialogRef.close(this.stocksForWarrantyFormArray.getRawValue());
    }

}