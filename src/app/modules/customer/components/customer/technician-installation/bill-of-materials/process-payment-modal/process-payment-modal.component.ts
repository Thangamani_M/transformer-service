import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { accountNoEncrption, adjustDateFormatAsPerPCalendar, clearFormControlValidators, countryCodes, CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, OtherService, ResponseMessageTypes, RxjsService, setMinMaxValidator, setRequiredValidator, SnackbarService } from '@app/shared';
import { ProcessPayment } from '@modules/customer/models/bill-of-material.model';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
    selector: 'app-process-payment-modal',
    templateUrl: './process-payment-modal.component.html',
    styleUrls: ['./process-payment-modal.component.scss']
})
export class ProcessPaymentModalComponent implements OnInit {

    accountDetailsList = [];
    noOfPaymentsList = [
        // {id: '1', displayName: '1'},
        { id: '2', displayName: '2' },
        { id: '3', displayName: '3' },
        // {id: '4', displayName: '4'},
        // {id: '5', displayName: '5'}
    ];
    paymentForm: FormGroup;
    isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
    nameDesignation = '';
    todayDate: any = new Date();
    public signaturePadOptions: Object = {
        'minWidth': 1,
        'canvasWidth': 320,
        'canvasHeight': 70
    };
    @ViewChild(SignaturePad, { static: false }) signaturePad: any;
    selectedFiles = new Array();
    totalFileSize = 0;
    countryCodes = countryCodes;

    constructor(
        private datePipe: DatePipe,
        private formBuilder: FormBuilder,
        public ref: DynamicDialogRef,
        public config: DynamicDialogConfig,
        private crudService: CrudService,
        private rxjsService: RxjsService,
        private otherService: OtherService,
        private snackbarService: SnackbarService) {
        this.nameDesignation = (config?.data['customerName'] && config?.data['designation']) ? config?.data['customerName'] 
            + ' - ' + config?.data['designation'] : config?.data['customerName'] ? config?.data['customerName'] : '';
        this.rxjsService.setDialogOpenProperty(true);
    }

    ngOnInit(): void {
        this.createForm();
        this.onLoadValue();
    }

    btnClose() {
        this.ref.close(false);
    }

    drawComplete() {
    }

    getDateFormat() {
        return adjustDateFormatAsPerPCalendar();
    }

    clearPad() {
        this.signaturePad.clear();
    }

    createForm() {
        let paymentModel = new ProcessPayment();
        this.paymentForm = this.formBuilder.group({});
        let techJobCardPaymentDetails = this.formBuilder.array([]);
        Object.keys(paymentModel).forEach((key) => {
            if (typeof paymentModel[key] !== 'object') {
                this.paymentForm.addControl(key, new FormControl({
                    value: paymentModel[key],
                    disabled: (key.toLowerCase() == 'podate') || key.toLowerCase() == 'ponumber' || key.toLowerCase() == 'bankbranch' || key.toLowerCase() == 'bankname' || key.toLowerCase() == 'totalamount'
                }));
            } else {
                this.paymentForm.addControl(key, techJobCardPaymentDetails);
            }
        });
        this.paymentForm = setRequiredValidator(this.paymentForm, ['paymentMethodId', 'debitOrderRunCodeId']);
        this.paymentForm.get('smsVerificationCountryCode').disable();

        this.paymentForm.get('debtorAccountDetailId').valueChanges.subscribe(accountData => {
            if (!accountData) return;
            let stockDetails = this.accountDetailsList.find(element => element['debtorAccountDetailId'] == accountData);
            if (stockDetails) {
                this.paymentForm.get('bankName').setValue(stockDetails.bankName, { emitEvent: false });
                this.paymentForm.get('bankBranch').setValue(stockDetails.branch, { emitEvent: false });
                if (this.paymentForm.get('paymentMethodId').value == 2) {
                    this.paymentForm.get('smsVerificationNumber').setValue(stockDetails.mobile1, { emitEvent: false });
                } else {
                    this.paymentForm.get('poDate').setValue(new Date(stockDetails.purchaseOrderDate), { emitEvent: false });
                    this.paymentForm.get('poNumber').setValue(stockDetails.purchaseOrderNo, { emitEvent: false });
                }                
            }
        });
        this.paymentForm.get('noOfPayments').valueChanges.subscribe(val => {
            if(val) {
                this.otherService.clearFormArray(this.payementItemsFormArray);
            }
        })
        this.paymentForm.get('paymentMethodId').valueChanges.subscribe(accountData => {
            if (accountData == 2) {
                this.paymentForm.get('isSinglePayment').setValue(true);
                // this.paymentForm.get('transactionDescription').setValue('Debit Order Payment-Thank you Submitted for total R ' + this.paymentForm.get('totalAmount').value);

                // this.paymentForm.get('paymentDate').markAsUntouched();
                this.paymentForm = setRequiredValidator(this.paymentForm, ['isSinglePayment', 'debtorAccountDetailId', 'debitOrderRunCodeId']);
                // this.paymentForm = clearFormControlValidators(this.paymentForm, ["paymentDate"]);
                this.paymentForm.get('paymentDate').clearValidators();
                this.paymentForm.get('paymentDate').markAsUntouched();
                this.paymentForm.get('paymentDate').markAsPristine();
                this.paymentForm.get('paymentDate').updateValueAndValidity();
                this.addPaymentsToTable(1, false);
                this.selectedFiles = [];
                this.paymentForm.get('poAttachment').setValue('');
                this.paymentForm.get('poAttachment').clearValidators();
                this.paymentForm.get('poAttachment').updateValueAndValidity();
                this.paymentForm.get('purchaseOrder').setValue(false, {emitEvent: false});
            } else {
                this.addPaymentsToTable(0, false);
                this.paymentForm.get('debtorAccountDetailId').markAsUntouched();
                this.paymentForm = setRequiredValidator(this.paymentForm, ['paymentDate']);
                this.paymentForm = clearFormControlValidators(this.paymentForm, ['isSinglePayment', 'debitOrderRunCodeId']);
                this.paymentForm.get('paymentDate').markAsUntouched();
                this.paymentForm.get('paymentDate').markAsPristine();
                this.paymentForm.get('paymentDate').updateValueAndValidity();
            }
        });

        this.paymentForm.get('isSinglePayment').valueChanges.subscribe(paymentData => {
            if (paymentData) {
                this.addPaymentsToTable(1, false);
                this.paymentForm = clearFormControlValidators(this.paymentForm, ['multipleStartDate', 'noOfPayments']);
                this.paymentForm.get('multipleStartDate').markAsUntouched();
                this.paymentForm.get('noOfPayments').markAsUntouched();
            } else {
                this.paymentForm.get('multipleTotalPayment').setValue(this.config?.data?.paymentDetails['totalAmount'] ? this.otherService.transformDecimal(this.config?.data?.paymentDetails['totalAmount']) : '');
                this.paymentForm.get('multipleTotalPayment').disable({ emitEvent: false });
                this.addPaymentsToTable(0, false);
            }
        });

        this.paymentForm.get('purchaseOrder').valueChanges.subscribe(paymentData => {
            if(paymentData) {
                this.paymentForm.get('poAttachment').setValidators([Validators.required]);
            } else {
                this.paymentForm.get('poAttachment').clearValidators();
            }
            this.paymentForm.get('poAttachment').updateValueAndValidity();
        });
    }

    onFileChange(event) {
        const supportedExtensions = ['jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'];
        for (var i = 0; i < event.target.files.length; i++) {
          let selectedFile = event.target.files[i];
          const path = selectedFile.name.split('.');
          const extension = path[path.length - 1];
          if (supportedExtensions.includes(extension)) {
            this.selectedFiles.push(event.target.files[i]);
            this.totalFileSize += event.target.files[i].size;
            this.paymentForm.get('poAttachment').setValue(selectedFile.name);
          }
          else {
    
          }
        }
      }

    addPaymentsToTable(length, isButtonClicked) {
        if (isButtonClicked && !this.paymentForm.get('multipleStartDate').value) {
            this.snackbarService.openSnackbar("Start Date is required", ResponseMessageTypes.ERROR);
            this.paymentForm = setRequiredValidator(this.paymentForm, ['multipleStartDate']);
            this.paymentForm.get('multipleStartDate').markAsTouched();
            return;
        }
        if (isButtonClicked && !length) {
            this.snackbarService.openSnackbar("No of Payments is required", ResponseMessageTypes.ERROR);
            this.paymentForm = setRequiredValidator(this.paymentForm, ['noOfPayments']);
            this.paymentForm.get('noOfPayments').markAsTouched();
            return;
        }
        let paymentItemsFormArray = this.payementItemsFormArray;
        paymentItemsFormArray.clear();
        const totalAmount = this.paymentForm.get('totalAmount').value ? this.otherService.transformCurrToNum(this.paymentForm.get('totalAmount').value) : 0;
        for (let index = 0; index < length; index++) {
            let payment = 0;
            let nextPaymentDate;
            if (length == 1) {
                payment = +totalAmount;
            } else if (length == 2) {
                if (index == 0) {
                    payment = +totalAmount / 2;
                } else if (index == 1) {
                    payment = +totalAmount / 2;
                }
            } else if (length == 3) {
                if (index == 0) {
                    payment = +totalAmount / 2;
                } else if (index == 1) {
                    payment = +totalAmount / 4;
                } else if (index == 2) {
                    payment = +totalAmount / 4;
                }
            }

            if (length != 1) {
                let d = new Date(this.paymentForm.get('multipleStartDate').value);
                nextPaymentDate = new Date(d.getFullYear(), d.getMonth() + index , d.getDate()); // (index + 1)
            } else {
                nextPaymentDate = this.paymentForm.get('paymentDate').value;
            }

            let paymentFormGroup = this.formBuilder.group({
                paymentDate: nextPaymentDate,
                paymentAmount: payment?.toString() ? this.otherService.transformDecimal(payment) : '',
                totalOutstandingAmount: this.paymentForm.get('totalAmount').value?.toString() ? this.otherService.transformDecimal(this.paymentForm.get('totalAmount').value) : '',
                // 0: 0
                // controlIndex: index
            });
            // if (length == 1)
                paymentFormGroup.get('paymentAmount').disable({ emitEvent: false });

            paymentFormGroup = setRequiredValidator(paymentFormGroup, ['paymentDate', 'paymentAmount']);
            paymentFormGroup = setMinMaxValidator(paymentFormGroup, [
                { formControlName: 'paymentAmount', compareWith: 'totalOutstandingAmount', type: 'minEqual', skipZero: true }
            ]);
            // paymentFormGroup = setMinValidatorWithValue(paymentFormGroup, [{formControlName: 'paymentAmount', minValue: 1}]);
            // paymentFormGroup = setMaxValidatorWithValue(paymentFormGroup, [{formControlName: 'paymentAmount', maxValue: 
            // +this.paymentForm.get('totalAmount').value}]);
            // paymentFormGroup = setMinMaxValidator(paymentFormGroup, [
            // { formControlName: 'paymentAmount', compareWith: '0', type: 'min' }
            // ]);
            paymentFormGroup.get('paymentAmount').valueChanges.subscribe(payment => {
                // var controlIndex = this.payementItemsFormArray.controls.findIndex(control => control.get('controlIndex').value == paymentFormGroup.get('controlIndex').value);
                let paymentFormArray = this.payementItemsFormArray.controls;
                const totalAmt = this.paymentForm.get('totalAmount').value ? this.otherService.transformCurrToNum(this.paymentForm.get('totalAmount').value) : 0;
                const amount = payment?.toString() ? this.otherService.transformCurrToNum(payment) : 0;
                let remainingAmount = (+totalAmt - +amount) / (length - 1);
                for (let j = 0; j < paymentFormArray.length; j++) {
                    if (index == j) { }
                    else {
                        paymentFormArray[j]['controls']['paymentAmount'].setValue(remainingAmount <= 0 ? 'R 0.00' : this.otherService.transformDecimal(remainingAmount.toFixed(2)), { emitEvent: false });
                    }
                }
            });
            paymentItemsFormArray.push(paymentFormGroup);
        }
    }

    onFocusPayAmt(i) {
        const amt = this.payementItemsFormArray?.controls[i]?.get('paymentAmount')?.value?.toString() ? this.otherService.transformCurrToNum(this.payementItemsFormArray?.controls[i]?.get('paymentAmount')?.value) : 'R 0.00';
        this.payementItemsFormArray?.controls[i].get('paymentAmount').setValue(amt);
    }

    onBlurPayAmt(i) {
        const amt = this.payementItemsFormArray?.controls[i]?.get('paymentAmount')?.value?.toString() ? this.otherService.transformDecimal(this.payementItemsFormArray?.controls[i]?.get('paymentAmount')?.value) : 'R 0.00';
        this.payementItemsFormArray?.controls[i].get('paymentAmount').setValue(amt);
    }

    onLoadValue() {
        this.accountDetailsList = accountNoEncrption(this.config?.data?.paymentDetails?.debtorAccounts);
        this.paymentForm.get('totalAmount').setValue(this.config?.data?.paymentDetails?.totalAmount?.toString() ? this.otherService.transformDecimal(this.config?.data?.paymentDetails?.totalAmount) : 'R 0.00');
        this.paymentForm.get('createdUserId').setValue(this.config?.data['createdUserId']);
        this.paymentForm.get('billOfMaterialId').setValue(this.config?.data['billofMaterialId']);
        this.paymentForm.get('customerName').setValue(this.config?.data?.['customerName']);
        this.paymentForm.get('designation').setValue(this.config?.data?.['designation']);
        this.paymentForm.get('paymentMethodId').setValue('2');
        this.paymentForm.get('debitOrderRunCodeId').setValue(this.config?.data?.orderRunCodesList[0]?.id);
        this.paymentForm.get('isSinglePayment').setValue(true);
        this.paymentForm.get('transactionDescription').setValue(this.config?.data?.paymentDetails?.serviceCallNumber);
        this.paymentForm.get('smsVerificationCountryCode').setValue('+27');
    }

    getAccountNumber() {
        return this.accountDetailsList?.find(el => el?.debtorAccountDetailId == this.paymentForm.get('debtorAccountDetailId').value)?.displayName;
    }

    get payementItemsFormArray(): FormArray {
        if (this.paymentForm !== undefined) {
            return (<FormArray>this.paymentForm.get('techJobCardPaymentDetails'));
        }
    }

    processPayment() {
        if (this.paymentForm.invalid) {
            this.paymentForm.markAllAsTouched();
            return;
        }
        if (!this.payementItemsFormArray?.length && this.paymentForm?.get('isSinglePayment')?.value ==  false && this.paymentForm?.get('paymentMethodId').value == 2) {
            this.snackbarService.openSnackbar("Please click the assign button", ResponseMessageTypes.WARNING);
            return;
        }
        let dataTosend = { ...this.paymentForm.getRawValue() };
        if (this.paymentForm.get('paymentMethodId').value == 1) {
            dataTosend['techJobCardPaymentDetails'].push({
                paymentDate: this.paymentForm.get('paymentDate').value ? this.datePipe.transform(this.paymentForm.get('paymentDate').value, 'yyyy-MM-dd') : null,
                paymentAmount: this.paymentForm.get('totalAmount').value ? this.otherService.transformCurrToNum(this.paymentForm.get('totalAmount').value) : null,
            })
        } else if (this.paymentForm.get('paymentMethodId').value == 2) {
            dataTosend['techJobCardPaymentDetails']?.forEach(el => {
                el.paymentDate = el?.paymentDate ? this.datePipe.transform(el?.paymentDate, 'yyyy-MM-dd') : null;
                el.paymentAmount = el?.paymentAmount ? this.otherService.transformCurrToNum(el?.paymentAmount) : null;
                el.totalOutstandingAmount = el?.totalOutstandingAmount ? this.otherService.transformCurrToNum(el?.totalOutstandingAmount) : null;
            });
        }
        dataTosend['paymentDate'] = dataTosend['paymentDate'] ? this.datePipe.transform(dataTosend['paymentDate'], 'yyyy-MM-dd') : null;
        dataTosend['smsVerificationNumber'] = dataTosend['smsVerificationNumber'] ? `${dataTosend['smsVerificationCountryCode']} ${dataTosend['smsVerificationNumber']}` : null;
        dataTosend['totalAmount'] = dataTosend['totalAmount'] ? this.otherService.transformCurrToNum(dataTosend['totalAmount']) : null;
        delete dataTosend['multipleStartDate'];
        delete dataTosend['multipleTotalPayment'];
        delete dataTosend['noOfPayments'];
        delete dataTosend['purchaseOrder'];
        delete dataTosend['poAttachment'];
        delete dataTosend['smsVerificationCountryCode'];

        let formData = new FormData();
        formData.append('dto', JSON.stringify(dataTosend));
        if (this.signaturePad && this.signaturePad.signaturePad._data.length > 0) {
            // let fileName = batchModel.get('pickedBatchBarCode').value + "-signature.jpeg";
            const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
            const imageFile = new File([imageBlob], 'this.imageName', { type: 'image/jpeg' });
            formData.append('file', imageFile);
        }

        if (this.selectedFiles.length > 0) {
            for (const file of this.selectedFiles) {
                formData.append('po', file);
            }
        }


        this.crudService.create(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_JOB_CARD_ACCEPTANCE_PAYMENT_PROCESS,
            formData
        ).subscribe((response: IApplicationResponse) => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if (response.isSuccess == true && response.statusCode == 200) {
                if (response.resources) {
                    window.open(response.resources, '_blank');
                }
                this.ref.close(true);
            }
        });
    }

    dataURItoBlob(dataURI) {
        const byteString = window.atob(dataURI);
        const arrayBuffer = new ArrayBuffer(byteString.length);
        const int8Array = new Uint8Array(arrayBuffer);
        for (let i = 0; i < byteString.length; i++) {
            int8Array[i] = byteString.charCodeAt(i);
        }
        const blob = new Blob([int8Array], { type: 'image/jpeg' });
        return blob;
    }

}