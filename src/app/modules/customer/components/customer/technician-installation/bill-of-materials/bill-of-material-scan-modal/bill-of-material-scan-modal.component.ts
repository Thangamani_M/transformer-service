import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { CrudService, RxjsService } from '@app/shared';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-bill-of-material-scan-modal',
  templateUrl: './bill-of-material-scan-modal.component.html',
//   styleUrls: ['./bill-of-material-scan-modal.component.scss']
})
export class BillOfMaterialScanModalComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public popupData: any) { }

  ngOnInit(): void {
    let messageSpan = document.createElement('span');
    messageSpan.innerHTML = this.popupData['message'];
    document.getElementById('parentContainer').appendChild(messageSpan);
  }
}
