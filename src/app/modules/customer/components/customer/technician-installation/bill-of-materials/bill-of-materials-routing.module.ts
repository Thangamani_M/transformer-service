import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CanDeactivateGuard as CanDeactivate } from '@app/shared/services/authguards';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


import { BillOfMaterialUpdateComponent } from './bill-of-material-update';
import { JobCardAcceptanceComponent } from './job-card-acceptance/job-card-acceptance.component';

const routes: Routes = [
    {
        path: '', component: BillOfMaterialUpdateComponent, canActivate: [AuthGuard], canDeactivate: [CanDeactivate], data: { title: 'Bill Of Materials' }
    },
    {
        path: 'job-card-acceptance', component: JobCardAcceptanceComponent, canActivate: [AuthGuard], data: { title: 'Job Card Acceptance' }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class BillOfMaterialsRoutingModule { }