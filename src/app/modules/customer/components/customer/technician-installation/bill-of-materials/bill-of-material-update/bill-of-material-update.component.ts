import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatCheckbox, MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, convertTreeToList, CrudService, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, OtherService, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, prepareRequiredHttpParams, PrimengConfirmDialogPopupComponent, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { BillOfMaterialModel, PrCallStockItemModel, StockCodeDescriptionFormArrayModel, StockCodeDescriptionModel } from '@modules/customer/models/bill-of-material.model';
import { CUSTOMER_COMPONENT } from '@modules/customer/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { DiscountOnWarrantyMaintenanceModalComponent, ServiceForDiscountModalComponent } from '..';
import { CustomerDecoderAddDialogComponent } from '../../../customer-management/customer-decoder/customer-decoder-add-dialog/customer-decoder-add-dialog.component';
import { CustomerZoneAddEditComponent } from '../../../customer-management/customer-zone/customer-zone-add-edit.component';
import { BillOfMaterialScanModalComponent } from '../bill-of-material-scan-modal';
import { BillOfMaterialValidationModalComponent } from '../bill-of-material-validation-modal';
import { BillOfMaterialZoneModalComponent } from '../bill-of-material-zone-modal';
import { NkaDoaRequestDialogComponent } from '../nka-doa-request-dialog/nka-doa-request-dialog.component';

@Component({
  selector: 'app-bill-of-material-update',
  templateUrl: './bill-of-material-update.component.html',
  styleUrls: ['./bill-of-material-update.component.scss']
})
export class BillOfMaterialUpdateComponent implements OnInit {

  billOfMaterialForm: FormGroup;
  stockCodeDescriptionForm: FormGroup;
  prCallRequestForm: FormGroup;
  billOfMaterialModel: any = {};
  prCallResponse: any = {};
  status: any = [];
  userData: UserLogin;
  installationId: string;
  customerId: string;
  quotationVersionId: string;
  callInitiationId: string;
  isLoading: boolean;
  @ViewChild('selectAll', { static: false }) selectAll: MatCheckbox;
  selectedCheckBox = '';
  selectedCheckBoxBOMArray = [];
  isADecimalOnly = new CustomDirectiveConfig({ isADecimalOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  standardTax: any;
  serialNumberError = false;
  stockTypeList = [
    { value: 'Y', displayName: 'Y' },
    { value: 'M', displayName: 'M' }
  ];
  // variables for auto select
  isStockCodeBlank: boolean = false;
  isStockDescriptionSelected: boolean = false;
  isStockOrderClear: boolean = false;
  stockCodeErrorMessage: any = '';
  stockCodeErrorMessagePR: any = '';
  showStockCodeError: boolean = false;
  showStockCodeErrorPR: boolean = false;
  isStockError: boolean = false;
  StockDescErrorMessage: any = '';
  showStockDescError: boolean = false;
  isStockDescError: boolean = false;
  isFeedback: any;
  filteredStockCodes = [];
  filteredPRStockCodes = [];
  filteredStockDescription: any = [];
  stockExistError = false;
  callType = '';
  openAddContactDialog = false;
  isFirstStockNumDel = false;
  warrantyMaintenanceDetail = [];
  appointmentId: string = '';
  appointmentLogId: string = '';
  openKitDetailsDialog: boolean = false;
  modalDetails: any;
  stockCode: any;
  customerAddressId: any
  alertDialog: boolean = false
  alertMsg: string = '';
  serialNumbersData: any;
  openSerialNumbersDialog = false;
  goodsReturnData: any = {};
  isVoid: boolean = false;
  // isEscalate: boolean = false;
  isEscalateSubmit: boolean = false;
  TechnicialCallDiscountRequestApprovalId;
  isGRVConfirmationDialog: boolean = false;
  isScanServiceCallDialog: boolean = false;
  newCallInitiationId: string;
  searchScannedItemResponse: any;
  isServiceCallSumbmit: boolean;
  openDoaReqMsgDialog: boolean;
  stockCodeData: any = [];
  isAutoSaveEnable: boolean = true;
  isUpselling: boolean;
  actionPermissionObj: any;
  isNKACustomer: boolean;
  jobCardAcceptanceSubscription: any;

  constructor(private formBuilder: FormBuilder,
    private crudService: CrudService,
    public dialogService: DialogService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackbarService: SnackbarService,
    private otherService: OtherService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.installationId = this.activatedRoute.snapshot.queryParams.installationId;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.customerAddressId = this.activatedRoute.snapshot.queryParams.customerAddressId;
    this.quotationVersionId = this.activatedRoute.snapshot.queryParams.quotationVersionId;
    this.callInitiationId = this.activatedRoute.snapshot.queryParams.callInitiationId;
    this.callType = this.activatedRoute.snapshot.queryParams.callType;
    this.appointmentId = this.activatedRoute.snapshot.queryParams.appointmentId;
    this.appointmentLogId = this.activatedRoute.snapshot.queryParams.appointmentLogId;
    this.isFeedback = this.activatedRoute.snapshot.queryParams?.isFeedback == 'true';
    this.isUpselling = this.activatedRoute.snapshot.queryParams?.isUpselling == 'true';
    this.isNKACustomer = this.activatedRoute.snapshot.queryParams?.isNKACustomer == 'true';
    this.isVoid = (this.activatedRoute.snapshot.queryParams?.isVoid && this.activatedRoute.snapshot.queryParams?.isVoid == 'true') ? true : false
    this.TechnicialCallDiscountRequestApprovalId = this.activatedRoute.snapshot.queryParams.TechnicialCallDiscountRequestApprovalId;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createBillOfMaterialForm();
    this.createStockCodeDescriptionForm();
    this.createPRCallStockItemForm();
    this.getBillOfMaterialData();
    this.onFormValueChanges();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][CUSTOMER_COMPONENT.CUSTOMER]?.find(el => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD);
      if (permission) {
        let techPermission = convertTreeToList([permission?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.TECHNICAL)]);
        const objName = this.callType == '1' ? CUSTOMER_COMPONENT.INSTALLATION : this.callType == '2' ? CUSTOMER_COMPONENT.SERVICE :
          this.callType == '3' ? CUSTOMER_COMPONENT.SPECIAL_PROEJCT : '---';
        const callActionPermissionObj = techPermission?.find(el => el?.[objName])?.[objName];
        if (callActionPermissionObj) {
          const quickActionPermissionObj = callActionPermissionObj?.find(el => el?.menuName == PermissionTypes.QUICK_ACTION);
          if (quickActionPermissionObj) {
            this.actionPermissionObj = quickActionPermissionObj?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.BILL_OF_MATERIALS);
          }
        }
      }
    });
  }

  getBOMActionPermission(actionTypeMenuName) {
    return this.actionPermissionObj?.subMenu?.find(el => el?.menuName == actionTypeMenuName);
  }

  getTitle() {
    return this.callType == '1' && this.billOfMaterialModel?.isDealerCall ? 'Dealer' : this.callType == '1' ? 'Installation' : this.callType == '2' ? 'Service' :
      this.callType == '3' ? 'Special Projects' : '';
  }

  onFormValueChanges() {
    this.billOfMaterialForm.get('bomInvoice.totalAmount').valueChanges.subscribe(value => {
      if (value?.toString()) {
        value = value ? this.otherService.transformCurrToNum(value) : 0;
      }
      if (value > 0) {
        // let diffAmount = value - this.billOfMaterialForm.get('targetOrderAmount').value;
        let diffAmount = this.billOfMaterialForm.get('targetOrderAmount').value - value;
        this.billOfMaterialForm.get('differenceAmount').setValue(this.otherService.transformDecimal(-Math.abs(diffAmount)));
        this.billOfMaterialForm.get('totalAmount').setValue(value);
      } else if (!value) {
        value = 0;
        if (this.billOfMaterialModel['isPRCall'] || this.billOfMaterialModel['isSaveOffer']) {
          let billOfMaterialItemsFormArray = this.billOfMaterialItemsFormArray;
          let prCallData = billOfMaterialItemsFormArray.controls.find(element => (element.get('itemId').value == this.billOfMaterialModel['prCallDiscountItemId'] && this.billOfMaterialModel['isPRCall']) ||
            (element.get('itemId').value == this.billOfMaterialModel['saveOfferDiscountItemId'] && this.billOfMaterialModel['isSaveOffer']));
          if (prCallData) {
            value = value + Math.abs(prCallData.get('subTotal').value);
          }
        }
        let diffAmount = this.billOfMaterialForm.get('targetOrderAmount').value - value;
        this.billOfMaterialForm.get('differenceAmount').setValue(this.otherService.transformDecimal(-Math.abs(diffAmount)));
        if (this.billOfMaterialModel['isPRCall'] || this.billOfMaterialModel['isSaveOffer']) {
          this.billOfMaterialForm.get('totalAmount').setValue(0);
        } else {
          this.billOfMaterialForm.get('totalAmount').setValue(this.billOfMaterialForm.get('bomInvoice.totalAmount').value);
        }
      }
    });
    // Auto select stock code and description and stock type
    this.stockCodeDescriptionForm.get('stockType').valueChanges.subscribe(value => {
      this.stockCodeDescriptionForm.get('stockCode').setValue('', { emitEvent: false });
      this.stockCodeDescriptionForm.get('stockDescription').setValue('', { emitEvent: false });
      this.stockExistError = false;
    });
    this.prCallRequestForm.get('Equipment').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        searchText = searchText == null ? searchText : searchText.trim();

        if (searchText != null) {
          if (!searchText) {
            if (searchText === '') {
              this.stockCodeErrorMessagePR = '';
              this.showStockCodeErrorPR = false;
              this.isStockError = false;
            }
            else {
              this.stockCodeErrorMessagePR = 'Stock code is not available in the system';
              this.showStockCodeErrorPR = true;
              this.isStockError = true;
            }
            return this.filteredPRStockCodes = [];
          } else {
            let searchStockObject = Object.assign({},
              searchText == null ? null : { ItemCode: searchText, CallInitiationId: this.callInitiationId },
              // this.stockCodeDescriptionForm.get('stockType').value == null ? null : { StockType: this.stockCodeDescriptionForm.get('stockType').value },
              this.billOfMaterialModel['districtId'] == null ? null : { DistrictId: this.billOfMaterialModel['districtId'] },
              this.billOfMaterialModel['warehouseId'] == null ? null : { WarehouseId: this.billOfMaterialModel['warehouseId'] },
              this.billOfMaterialModel['itemPricingConfigId'] == null ? null : { ItemPricingConfigId: this.billOfMaterialModel['itemPricingConfigId'] },
              this.billOfMaterialModel['technicianId'] == null ? null : { TechnicianId: this.billOfMaterialModel['technicianId'] });
            if (this.stockCodeDescriptionForm.get('stockType').value?.toLowerCase() == 'm') {
              // this.stockCodeErrorMessage = '';
              // this.showStockCodeError = false;
              // this.isStockError = false;
              // return this.crudService.get(
              //   ModulesBasedApiSuffix.TECHNICIAN,
              //   TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEARCH_STOCK_DETAILS_TECHNICIAN, null, true,
              //   prepareGetRequestHttpParams(null, null, searchStockObject))
            } else {
              if (this.billOfMaterialModel['technicianId']) {
                this.stockCodeErrorMessagePR = '';
                this.showStockCodeErrorPR = false;
                this.isStockError = false;
                return this.crudService.get(
                  ModulesBasedApiSuffix.TECHNICIAN,
                  TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEARCH_STOCK_DETAILS_TECHNICIAN, null, true,
                  prepareGetRequestHttpParams(null, null, searchStockObject))
              } else {
                this.stockCodeErrorMessagePR = 'Stock code is not available against technician stock location';
                this.showStockCodeErrorPR = true;
                this.isStockError = true;
                return this.filteredPRStockCodes = [];
              }
            }
          }
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredPRStockCodes = response.resources;
          this.stockCodeErrorMessagePR = '';
          this.showStockCodeError = false;
          this.isStockError = false;
        } else {
          this.filteredPRStockCodes = [];
          this.stockCodeErrorMessagePR = 'Stock code is not available in the system';
          this.showStockCodeError = true;
          this.isStockError = true;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.stockCodeDescriptionForm.get('stockCode').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        searchText = searchText == null ? searchText : searchText.trim();
        this.isStockDescError = false
        this.StockDescErrorMessage = '';
        this.showStockDescError = false;
        this.filteredStockDescription = [];
        this.stockExistError = false;
        if (this.isStockError == false) {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
        }
        else {
          this.isStockError = false;
        }

        if (searchText != null && this.stockCodeDescriptionForm.get('stockType').value != '') {
          if (this.isStockCodeBlank == false) {
            this.stockCodeDescriptionForm.get('stockDescription').reset(null, { emitEvent: false });
            this.stockCodeDescriptionForm.get('stockUnitPriceExcl').reset(null, { emitEvent: false });
            this.stockCodeDescriptionForm.get('stockRequestedQty').reset(null, { emitEvent: false });
          }
          else {
            this.isStockCodeBlank = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.stockCodeErrorMessage = '';
              this.showStockCodeError = false;
              this.isStockError = false;
            }
            else {
              this.stockCodeErrorMessage = 'Stock code is not available in the system';
              this.showStockCodeError = true;
              this.isStockError = true;
            }
            return this.filteredStockCodes = [];
          } else {
            let searchStockObject = Object.assign({},
              searchText == null ? null : { ItemCode: searchText },
              this.callInitiationId ? { CallInitiationId: this.callInitiationId } : null,
              this.stockCodeDescriptionForm.get('stockType').value == null ? null : { StockType: this.stockCodeDescriptionForm.get('stockType').value },
              this.billOfMaterialModel['districtId'] == null ? null : { DistrictId: this.billOfMaterialModel['districtId'] },
              this.billOfMaterialModel['warehouseId'] == null ? null : { WarehouseId: this.billOfMaterialModel['warehouseId'] },
              this.billOfMaterialModel['itemPricingConfigId'] == null ? null : { ItemPricingConfigId: this.billOfMaterialModel['itemPricingConfigId'] },
              this.billOfMaterialModel['technicianId'] == null ? null : { TechnicianId: this.billOfMaterialModel['technicianId'] });
            if (this.stockCodeDescriptionForm.get('stockType').value?.toLowerCase() == 'm') {
              this.stockCodeErrorMessage = '';
              this.showStockCodeError = false;
              this.isStockError = false;
              return this.crudService.get(
                ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEARCH_STOCK_DETAILS_TECHNICIAN, null, true,
                prepareGetRequestHttpParams(null, null, searchStockObject))
            } else {
              // if (this.billOfMaterialModel['technicianId']) {
              this.stockCodeErrorMessage = '';
              this.showStockCodeError = false;
              this.isStockError = false;
              return this.crudService.get(
                ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEARCH_STOCK_DETAILS_TECHNICIAN, null, true,
                prepareGetRequestHttpParams(null, null, searchStockObject))
              // } else {
              //   this.stockCodeErrorMessage = 'Stock code is not available against technician stock location';
              //   this.showStockCodeError = true;
              //   this.isStockError = true;
              //   return this.filteredStockCodes = [];
              // }
            }
          }
        }
        else {
          if (this.stockCodeDescriptionForm.get('stockType').value == '') {
            this.stockCodeDescriptionForm.get('stockType').markAsTouched();
          }
          return this.filteredStockCodes = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources?.length > 0) {
          // this.filteredStockCodes = response.resources;
          this.onPatchStockItemArray(response);
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;
          // this.stockCodeDescriptionForm.get('stockCode').setValue('', {emitEvent: false});
          // this.stockCodeDescriptionForm.get('stockCode').markAsUntouched();
        } else if (this.billOfMaterialModel['technicianId'] && response?.resources?.length == 0) {
          this.filteredStockCodes = [];
          this.stockCodeErrorMessage = 'Stock code is not available against technician stock location';
          this.showStockCodeError = true;
          this.isStockError = true;
          this.clearFormArray(this.stockAddItemFormarry);
          this.stockCodeDescriptionForm.markAsTouched();
        } else {
          this.filteredStockCodes = [];
          this.stockCodeErrorMessage = 'Stock code is not available in the system';
          this.showStockCodeError = true;
          this.isStockError = true;
          this.clearFormArray(this.stockAddItemFormarry);
          this.stockCodeDescriptionForm.markAsTouched();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.stockCodeDescriptionForm.get('stockDescription').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        searchText = searchText == null ? searchText : searchText.trim();
        this.stockExistError = false;
        if (searchText != null && this.stockCodeDescriptionForm.get('stockType').value != '') {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;
          this.filteredStockCodes = [];

          if (this.isStockDescError == false) {
            this.StockDescErrorMessage = '';
            this.showStockDescError = false;
          }
          else {
            this.showStockDescError = false;
          }
          if (this.isStockDescriptionSelected == false) {
            // this.stockCodeDescriptionForm.get('stockCode').patchValue(null, {emitEvent: false});
            this.stockCodeDescriptionForm.get('stockCode').reset(null, { emitEvent: false });
            // this.stockCodeDescriptionForm.get('stockUnitPriceExcl').patchValue(null, {emitEvent: false});
            this.stockCodeDescriptionForm.get('stockUnitPriceExcl').reset(null, { emitEvent: false });
            // this.stockCodeDescriptionForm.get('stockRequestedQty').patchValue(null, {emitEvent: false});
            this.stockCodeDescriptionForm.get('stockRequestedQty').reset(null, { emitEvent: false });
          }
          else {
            this.isStockDescriptionSelected = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.StockDescErrorMessage = '';
              this.showStockDescError = false;
              this.isStockDescError = false;
            }
            else {
              this.StockDescErrorMessage = 'Stock description is not available in the system';
              this.showStockDescError = true;
              this.isStockDescError = true;
            }
            return this.filteredStockDescription = [];
          } else {
            let searchStockDescObject = Object.assign({},
              searchText == null ? null : { displayName: searchText, },
              this.callInitiationId ? { CallInitiationId: this.callInitiationId } : null,
              this.stockCodeDescriptionForm.get('stockType').value == null ? null : { StockType: this.stockCodeDescriptionForm.get('stockType').value },
              this.billOfMaterialModel['districtId'] == null ? null : { DistrictId: this.billOfMaterialModel['districtId'] },
              this.billOfMaterialModel['warehouseId'] == null ? null : { WarehouseId: this.billOfMaterialModel['warehouseId'] },
              this.billOfMaterialModel['itemPricingConfigId'] == null ? null : { ItemPricingConfigId: this.billOfMaterialModel['itemPricingConfigId'] },
              this.billOfMaterialModel['technicianId'] == null ? null : { TechnicianId: this.billOfMaterialModel['technicianId'] });

            if (this.stockCodeDescriptionForm.get('stockType').value?.toLowerCase() == 'm') {
              this.StockDescErrorMessage = '';
              this.showStockDescError = false;
              this.isStockDescError = false;
              return this.crudService.get(
                ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEARCH_STOCK_DETAILS_TECHNICIAN, null, true,
                prepareGetRequestHttpParams(null, null, searchStockDescObject))
            } else {
              // if (this.billOfMaterialModel['technicianId']) {
              this.StockDescErrorMessage = '';
              this.showStockDescError = false;
              this.isStockDescError = false;
              return this.crudService.get(
                ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEARCH_STOCK_DETAILS_TECHNICIAN, null, true,
                prepareGetRequestHttpParams(null, null, searchStockDescObject))
              // } else {
              //   this.StockDescErrorMessage = 'Stock description is not available against technician stock location';
              //   this.showStockDescError = true;
              //   this.isStockDescError = true;
              //   return this.filteredStockDescription = [];
              // }
            }
          }
        } else {
          if (this.stockCodeDescriptionForm.get('stockType').value == '') {
            this.stockCodeDescriptionForm.get('stockType').markAsTouched();
          }
          return this.filteredStockDescription = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          // this.filteredStockDescription = response.resources;
          this.onPatchStockItemArray(response);
          this.isStockDescriptionSelected = false;
          this.StockDescErrorMessage = '';
          this.showStockDescError = false;
          this.isStockDescError = false;
          this.stockCodeDescriptionForm.get('stockDescription').setValue('', { emitEvent: false });
          this.stockCodeDescriptionForm.get('stockDescription').markAsUntouched();
        } else if (this.billOfMaterialModel['technicianId'] && response?.resources?.length == 0) {
          this.onLoadStockFormArray();
          this.filteredStockDescription = [];
          this.isStockDescriptionSelected = false;
          this.StockDescErrorMessage = 'Stock description is not available against technician stock location';
          this.showStockDescError = true;
          this.isStockDescError = true;
          // this.clearFormArray(this.stockAddItemFormarry);
          this.stockCodeDescriptionForm.markAsTouched();
        } else {
          this.onLoadStockFormArray();
          this.filteredStockDescription = [];
          this.isStockDescriptionSelected = false;
          this.StockDescErrorMessage = 'Stock description is not available in the system';
          this.showStockDescError = true;
          this.isStockDescError = true;
          this.clearFormArray(this.stockAddItemFormarry);
          this.stockCodeDescriptionForm.markAsTouched();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.billOfMaterialForm.get('isStock').valueChanges.subscribe(value => {
      if (value) {
        this.billOfMaterialForm.get('isDiscount').setValue(false);
        this.stockCodeDescriptionForm.get('stockType').setValue('Y');
        this.onLoadStockFormArray();
      } else {
        this.stockCodeDescriptionForm.get('stockType').setValue('');
        this.clearFormArray(this.stockAddItemFormarry);
        this.stockCodeDescriptionForm.markAsTouched();
      }
    });
    this.billOfMaterialForm.get('isDiscount').valueChanges.subscribe(value => {
      if (value) {
        this.billOfMaterialForm.get('isStock').setValue(false);
        this.stockCodeDescriptionForm.get('stockType').setValue('M');
        this.onLoadStockFormArray();
      } else {
        this.stockCodeDescriptionForm.get('stockType').setValue('');
        this.clearFormArray(this.stockAddItemFormarry);
        this.stockCodeDescriptionForm.markAsTouched();
      }
    });
  }

  onLoadStockFormArray() {
    // if(!this.stockCodeDescriptionForm.get('stockType').value) {
    //   return;
    // }
    let searchStockDescObject = Object.assign({},
      this.callInitiationId ? { CallInitiationId: this.callInitiationId } : null,
      this.stockCodeDescriptionForm.get('stockType').value == null ? null : { stockType: this.stockCodeDescriptionForm.get('stockType').value },
      this.billOfMaterialModel['districtId'] == null ? null : { districtId: this.billOfMaterialModel['districtId'] },
      this.billOfMaterialModel['warehouseId'] == null ? null : { warehouseId: this.billOfMaterialModel['warehouseId'] },
      this.billOfMaterialModel['itemPricingConfigId'] == null ? null : { itemPricingConfigId: this.billOfMaterialModel['itemPricingConfigId'] },
      this.billOfMaterialModel['technicianId'] == null ? null : { technicianId: this.billOfMaterialModel['technicianId'] });
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEARCH_STOCK_DETAILS_TECHNICIAN, null, true,
      prepareGetRequestHttpParams(null, null, searchStockDescObject))
      .subscribe((res: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res?.isSuccess && res?.statusCode == 200) {
          this.onPatchStockItemArray(res);
        }
      });
  }

  onPatchStockItemArray(res) {
    this.clearFormArray(this.stockAddItemFormarry);
    res?.resources.forEach(el => {
      this.initFormArray({
        id: el?.id,
        stockType: el?.stockType,
        stockCode: el?.itemCode,
        stockDescription: el?.displayName,
        stockUnitPriceExcl: el?.costPrice,
        stockRequestedQty: '',
        stockTypeId: el?.stockTypeId,
        itemId: el?.id,
        kitConfigId: el?.kitConfigId,
        kitTypeName: el?.kitTypeName,
        systemTypeId: el?.systemTypeId,
        itemPricingConfigId: el?.itemPricingConfigId,
        isNotSerialized: el?.isNotSerialized,
        costPrice: el?.costPrice,
        displayName: el?.displayName,
        isDecoder: el?.isDecoder,
      });
    })
    this.stockCodeDescriptionForm.markAsUntouched();
  }

  initFormArray(stockCodeDescriptionFormArrayModel?: StockCodeDescriptionFormArrayModel) {
    let stockCodeDescriptionModel = new StockCodeDescriptionFormArrayModel(stockCodeDescriptionFormArrayModel);
    let stockCodeDescriptionFormArray = this.formBuilder.group({});
    Object.keys(stockCodeDescriptionModel).forEach((key) => {
      stockCodeDescriptionFormArray.addControl(key, new FormControl(stockCodeDescriptionModel[key]));
    });
    stockCodeDescriptionFormArray = setRequiredValidator(stockCodeDescriptionFormArray,
      ["stockRequestedQty"]);
    stockCodeDescriptionFormArray.get('stockType').disable();
    stockCodeDescriptionFormArray.get('stockCode').disable();
    stockCodeDescriptionFormArray.get('stockDescription').disable();
    stockCodeDescriptionFormArray.get('stockUnitPriceExcl').disable();
    this.stockAddItemFormarry.push(stockCodeDescriptionFormArray);
  }

  onValidateStockItemFormArray() {
    const arr = [];
    this.stockAddItemFormarry?.controls.forEach(el => {
      arr.push(el?.invalid);
    })
    return arr.every(el => el == true);
  }

  onValidateLighting() {
    const arr = [];
    this.stockAddItemFormarry?.controls.forEach(el => {
      if (this.billOfMaterialModel['lightningDiscountItemId'] == el?.get('id')?.value && el?.get('stockRequestedQty').value > 1) {
        arr.push(el?.invalid);
      }
    })
    return arr.every(el => el == true);
  }

  getStockItemAlreadyExist() {
    const arr = [];
    this.billOfMaterialItemsFormArray?.getRawValue()?.forEach(el => {
      this.stockAddItemFormarry?.getRawValue()?.forEach(el1 => {
        if (el.itemCode == el1.stockCode && el.IsDeleted == false && el1.stockRequestedQty) {
          arr.push(el.itemCode);
        }
      });
    })
    const returnText = arr.length ? arr?.join(", ") : '';
    return returnText;
  }

  getDiscountItemExist() {
    const arr = [];
    this.stockAddItemFormarry?.getRawValue()?.forEach(el => {
      const findYItem = this.billOfMaterialItemsFormArray?.getRawValue()?.filter(el => el?.stockType?.toLowerCase() == 'y');
      if (this.billOfMaterialModel?.technicianDiscountItems?.indexOf(el.itemId) > -1 && findYItem?.length == 0) {
        arr.push(el.itemId);
      }
    })
    return arr.length;
  }

  addBOMMultiToTable() {
    if (this.onValidateStockItemFormArray()) {
      this.stockAddItemFormarry?.markAllAsTouched();
      return;
    }
    if (!this.standardTax) {
      return this.snackbarService.openSnackbar("Tax not found", ResponseMessageTypes.WARNING);
    }
    if (this.getStockItemAlreadyExist()) {
      this.snackbarService.openSnackbar("Item has been added already", ResponseMessageTypes.WARNING);
      return;
    }
    // if (this.getDiscountItemExist()) {
    //   this.snackbarService.openSnackbar("Discount Item cann't be added without Y items", ResponseMessageTypes.WARNING);
    //   return;
    // }
    this.stockAddItemFormarry?.controls.forEach(el => {
      if (el.invalid) {
        return;
      } else {
        let billOfMaterialItemsFormArray = this.billOfMaterialItemsFormArray;
        let billOfMaterialItemFormGroup = this.formBuilder.group({
          checkBillOfMaterial: false,
          IsDeleted: false,
          billOfMaterialItemId: '',
          billOfMaterialId: '',
          stockTypeId: el?.get('stockTypeId')?.value,
          stockType: el?.get('stockType')?.value,
          itemId: el?.get('itemId')?.value,
          itemCode: el?.get('stockCode')?.value,
          itemName: el?.get('stockDescription')?.value,
          invoice: 0,
          transfer: 0,
          used: 0,
          reserved: 0,
          request: 0,// +this.stockCodeDescriptionForm.get('stockRequestedQty').value,
          unitPrice: (+el?.get('stockUnitPriceExcl')?.value)?.toFixed(2),
          subTotal: '',
          vATAmount: '',
          totalAmount: '',
          createdUserId: this.userData?.userId,
          modifiedUserId: this.userData?.userId,
          IsWarranty: false,
          WarrantyQty: null, // check with backend developer
          kitConfigId: el?.get('kitConfigId')?.value,
          kitTypeName: el?.get('kitTypeName')?.value,
          systemTypeId: el?.get('systemTypeId')?.value,
          itemPricingConfigId: el?.get('itemPricingConfigId')?.value,
          isNotSerialized: el?.get('isNotSerialized')?.value,
        });
        if (el?.get('stockType').value.toLowerCase() == 'y') {
          if (el?.get('isNotSerialized').value) {
            billOfMaterialItemFormGroup.get('request').setValue(+el?.get('stockRequestedQty').value);
          } else if (this.billOfMaterialModel['lightningDiscountItemId'] == el?.get('id').value || this.getDiscountValueItems(el?.get('id').value)) {
            if (!this.billOfMaterialModel?.billOfMaterialId) {
              this.snackbarService.openSnackbar("Please save the bill of materials first", ResponseMessageTypes.WARNING);
              return;
            }
            el?.get('stockRequestedQty').setValue(1);
            billOfMaterialItemFormGroup.get('request').setValue(+el?.get('stockRequestedQty').value);
          } else {
            billOfMaterialItemFormGroup.get('request').setValue(+el?.get('stockRequestedQty').value);
          }
        } else if (el?.get('stockType').value.toLowerCase() == 'm') {
          // billOfMaterialItemFormGroup.get('used').setValue(+el?.get('stockRequestedQty').value)
          billOfMaterialItemFormGroup.get('used').setValue(1);
          el?.get('stockRequestedQty').setValue(1);
        }

        // let subTotal = billOfMaterialItemFormGroup.get('request').value * billOfMaterialItemFormGroup.get('unitPrice').value;

        let subTotal = +el?.get('stockRequestedQty').value * +billOfMaterialItemFormGroup.get('unitPrice').value;
        billOfMaterialItemFormGroup.get('subTotal').setValue(subTotal.toFixed(2)); //+

        let vatAmount = +billOfMaterialItemFormGroup.get('subTotal').value * (this.standardTax / 100);
        billOfMaterialItemFormGroup.get('vATAmount').setValue(vatAmount.toFixed(2)); //+

        let totalAmount = +billOfMaterialItemFormGroup.get('subTotal').value + +billOfMaterialItemFormGroup.get('vATAmount').value;
        billOfMaterialItemFormGroup.get('totalAmount').setValue(totalAmount.toFixed(2)); //+

        if (this.billOfMaterialModel['lightningDiscountItemId'] == el?.get('id').value || this.getDiscountValueItems(el?.get('id').value)) { // for lightning cover
          billOfMaterialItemFormGroup.get('unitPrice').setValue(typeof +billOfMaterialItemFormGroup.get('unitPrice').value == 'number'
            ? (this.otherService.transformDecimal(-Math.abs(billOfMaterialItemFormGroup.get('unitPrice').value))) : -Math.abs(billOfMaterialItemFormGroup.get('unitPrice').value));
          billOfMaterialItemFormGroup.get('subTotal').setValue(-Math.abs(billOfMaterialItemFormGroup.get('subTotal').value));
          billOfMaterialItemFormGroup.get('vATAmount').setValue(-Math.abs(billOfMaterialItemFormGroup.get('vATAmount').value));
          billOfMaterialItemFormGroup.get('totalAmount').setValue(-Math.abs(billOfMaterialItemFormGroup.get('totalAmount').value));
        }
        // if installation call, if current stock item's amount + previous total amount > target order amount then show
        // warning and dont allow to add stock item in grid else allow it to add
        // this only applicable to installation call
        if ((this.callType == '1' || this.isUpselling) && !this.billOfMaterialModel?.isDealerCall && (+billOfMaterialItemFormGroup.get('totalAmount').value + +this.billOfMaterialForm.get('bomInvoice.totalAmount').value) > +this.billOfMaterialForm.get('targetOrderAmount').value) {
          this.searchScannedItemResponse = {
            itemId: el?.get('itemId')?.value,
            qty: el?.get('stockRequestedQty')?.value,
            itemPricingConfigId: el?.get('itemPricingConfigId')?.value,
          };
          // this.snackbarService.openSnackbar('Total Amount can not exceed Target Order Amount', ResponseMessageTypes.WARNING);
          this.isScanServiceCallDialog = true;
          return;
        }
        let bomsubTotal = +this.billOfMaterialForm.get('bomInvoice.subTotal').value + +billOfMaterialItemFormGroup.get('subTotal').value;
        let bomvATAmount = +this.billOfMaterialForm.get('bomInvoice.vatAmount').value + +billOfMaterialItemFormGroup.get('vATAmount').value;
        let bomtotalAmount = +this.billOfMaterialForm.get('bomInvoice.totalAmount').value + +billOfMaterialItemFormGroup.get('totalAmount').value;

        let differenceAmount = this.otherService.transformCurrToNum(this.billOfMaterialForm.get('differenceAmount').value);
        // if (+billOfMaterialItemFormGroup.get('totalAmount').value < differenceAmount) {
        //   this.snackbarService.openSnackbar("Total amount doesn't exceeds differenece amount", ResponseMessageTypes.WARNING);
        //   return;
        // }
        //condition for checking PR Call or Save offer call
        if (this.billOfMaterialModel['isPRCall'] == true || this.billOfMaterialModel['isSaveOffer'] == true) {
          //checking total amount is greater than differ amount
          if (+billOfMaterialItemFormGroup.get('subTotal').value > Math.abs(differenceAmount)) {

            //serviceCallRequestId is present
            if (this.billOfMaterialModel.serviceCallRequestId) {
              this.crudService.get(
                ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.PR_CALL,
                null, null, prepareRequiredHttpParams({ serviceCallRequestId: this.billOfMaterialModel.serviceCallRequestId, callInitiationId: this.billOfMaterialModel.callInitiationId })
              ).subscribe((response: IApplicationResponse) => {
                if (response?.isSuccess && response?.statusCode == 200) {
                  let equipments = null
                  if (response.resources)
                    equipments = response.resources.equipments;
                  if (equipments && equipments.length > 0) {
                    let equipmentsArr = equipments.filter(x => x.itemCode == el?.get('stockCode').value);
                    if (equipmentsArr && equipmentsArr.length > 0) {
                      this.snackbarService.openSnackbar('Stock Code is already added Try adding other Stock Code', ResponseMessageTypes.WARNING);
                      this.rxjsService.setGlobalLoaderProperty(false);
                      return;
                    } else {
                      this.prCallResponse = equipments;
                      //----start-- //
                      this.openDoaReqMsgDialog = true;
                      this.PrStockItemsFormArray.clear();
                      let PrStockItem = this.formBuilder.group({
                        itemId: el?.get('itemId').value,
                        stockCode: el?.get('stockCode').value,
                        stockDesc: el?.get('stockDescription').value,
                        stockQty: el?.get('stockRequestedQty').value,
                        stockUnitPriceExVat: this.otherService.transformDecimal(el?.get('stockUnitPriceExcl').value),
                        stockSubTotalExVat: this.otherService.transformDecimal(subTotal),
                        stockSubTotalIncVat: this.otherService.transformDecimal(totalAmount), //vatAmount,
                        ItemPricingConfigId: el?.get('itemPricingConfigId').value
                      });
                      PrStockItem.get('stockQty').valueChanges.subscribe((val) => {
                        let subTotal = val * this.otherService.transformCurrToNum(PrStockItem.get('stockUnitPriceExVat').value);
                        // billOfMaterialItemFormGroup.get('subTotal').setValue(+subTotal.toFixed(2));
                        let vatAmount = subTotal * (this.standardTax / 100);
                        // billOfMaterialItemFormGroup.get('vATAmount').setValue(+vatAmount.toFixed(2));
                        let totalAmount = subTotal + vatAmount;
                        // billOfMaterialItemFormGroup.get('totalAmount').setValue(+totalAmount.toFixed(2));
                        PrStockItem.controls['stockSubTotalExVat'].patchValue(this.otherService.transformDecimal(subTotal));
                        PrStockItem.controls['stockSubTotalIncVat'].patchValue(this.otherService.transformDecimal(totalAmount)); // vatAmount
                        this.prCallRequestForm.controls['totalStockItems'].setValue('R 0.00');
                        let total = 0
                        this.PrStockItemsFormArray.value.forEach((item) => {
                          total += this.otherService.transformCurrToNum(item.stockSubTotalIncVat);
                          this.prCallRequestForm.controls['totalStockItems'].setValue(this.otherService.transformDecimal(+total));

                        })
                      })
                      this.PrStockItemsFormArray.push(PrStockItem);
                      this.prCallRequestForm.controls['totalStockItems'].setValue(this.otherService.transformDecimal(+totalAmount)); // vatAmount
                      //----end-- //

                      //set the pr call prefetch values
                      this.prCallResponse.forEach(element => {
                        let PrStockItem = this.formBuilder.group({
                          itemId: element.itemId,
                          stockCode: element.itemCode,
                          stockDesc: element.itemName,
                          stockQty: element.qtyRequired,
                          stockUnitPriceExVat: this.otherService.transformDecimal(element.exclVAT / element.qtyRequired),
                          stockSubTotalExVat: this.otherService.transformDecimal(element.exclVAT),
                          stockSubTotalIncVat: this.otherService.transformDecimal(element.inclVAT),
                          serviceCallRequestItemId: element?.ServiceCallRequestItemId,
                          ItemPricingConfigId: element.itemPricingConfigId,
                        });
                        PrStockItem.get('stockQty').valueChanges.subscribe((val) => {
                          let subTotal = val * (element.exclVAT / element.qtyRequired);
                          // billOfMaterialItemFormGroup.get('subTotal').setValue(+subTotal.toFixed(2));
                          let vatAmount = subTotal * (this.standardTax / 100);
                          // billOfMaterialItemFormGroup.get('vATAmount').setValue(+vatAmount.toFixed(2));
                          let totalAmount = subTotal + vatAmount;
                          // billOfMaterialItemFormGroup.get('totalAmount').setValue(+totalAmount.toFixed(2));
                          PrStockItem.controls['stockSubTotalExVat'].patchValue(this.otherService.transformDecimal(subTotal));
                          PrStockItem.controls['stockSubTotalIncVat'].patchValue(this.otherService.transformDecimal(totalAmount)); // vatAmount
                          this.prCallRequestForm.controls['totalStockItems'].setValue('R 0.00');
                          let total = 0
                          this.PrStockItemsFormArray.value.forEach((item) => {
                            total += this.otherService.transformCurrToNum(item.stockSubTotalIncVat);
                            this.prCallRequestForm.controls['totalStockItems'].setValue(this.otherService.transformDecimal(+total));

                          })
                        })
                        this.PrStockItemsFormArray.push(PrStockItem);
                        this.prCallRequestForm.controls['totalStockItems'].setValue(this.otherService.transformDecimal(+totalAmount)); // vatAmount
                      });

                    }
                  }
                  else {
                    //----start-- //
                    this.openDoaReqMsgDialog = true;
                    this.PrStockItemsFormArray.clear();
                    let PrStockItem = this.formBuilder.group({
                      itemId: el?.get('itemId').value,
                      stockCode: el?.get('stockCode').value,
                      stockDesc: el?.get('stockDescription').value,
                      stockQty: el?.get('stockRequestedQty').value,
                      stockUnitPriceExVat: this.otherService.transformDecimal(el?.get('stockUnitPriceExcl').value),
                      stockSubTotalExVat: this.otherService.transformDecimal(subTotal),
                      stockSubTotalIncVat: this.otherService.transformDecimal(totalAmount), //vatAmount,
                      ItemPricingConfigId: el?.get('itemPricingConfigId').value
                    });
                    PrStockItem.get('stockQty').valueChanges.subscribe((val) => {
                      let subTotal = val * this.otherService.transformCurrToNum(PrStockItem.get('stockUnitPriceExVat').value);
                      // billOfMaterialItemFormGroup.get('subTotal').setValue(+subTotal.toFixed(2));
                      let vatAmount = subTotal * (this.standardTax / 100);
                      // billOfMaterialItemFormGroup.get('vATAmount').setValue(+vatAmount.toFixed(2));
                      let totalAmount = subTotal + vatAmount;
                      // billOfMaterialItemFormGroup.get('totalAmount').setValue(+totalAmount.toFixed(2));
                      PrStockItem.controls['stockSubTotalExVat'].patchValue(this.otherService.transformDecimal(subTotal));
                      PrStockItem.controls['stockSubTotalIncVat'].patchValue(this.otherService.transformDecimal(totalAmount)); // vatAmount
                      this.prCallRequestForm.controls['totalStockItems'].setValue('R 0.00');
                      let total = 0
                      this.PrStockItemsFormArray?.value?.forEach((item) => {
                        total += this.otherService.transformCurrToNum(item.stockSubTotalIncVat);
                        this.prCallRequestForm.controls['totalStockItems'].setValue(this.otherService.transformDecimal(+total));

                      })
                    })
                    this.PrStockItemsFormArray.push(PrStockItem);
                    this.prCallRequestForm.controls['totalStockItems'].setValue(this.otherService.transformDecimal(+totalAmount)); // vatAmount
                    //----end-- //
                  }
                }
                this.rxjsService.setGlobalLoaderProperty(false);
              });
            } else {
              //----start-- //
              this.openDoaReqMsgDialog = true;
              this.PrStockItemsFormArray.clear();
              let PrStockItem = this.formBuilder.group({
                itemId: el?.get('itemId').value,
                stockCode: el?.get('stockCode').value,
                stockDesc: el?.get('stockDescription').value,
                stockQty: el?.get('stockRequestedQty').value,
                stockUnitPriceExVat: this.otherService.transformDecimal(el?.get('stockUnitPriceExcl').value),
                stockSubTotalExVat: this.otherService.transformDecimal(subTotal),
                stockSubTotalIncVat: this.otherService.transformDecimal(totalAmount), //vatAmount,
                ItemPricingConfigId: el?.get('itemPricingConfigId').value,
              });
              PrStockItem.get('stockQty').valueChanges.subscribe((val) => {
                let subTotal = val * this.otherService.transformCurrToNum(PrStockItem.get('stockUnitPriceExVat').value);
                // billOfMaterialItemFormGroup.get('subTotal').setValue(+subTotal.toFixed(2));
                let vatAmount = subTotal * (this.standardTax / 100);
                // billOfMaterialItemFormGroup.get('vATAmount').setValue(+vatAmount.toFixed(2));
                let totalAmount = subTotal + vatAmount;
                // billOfMaterialItemFormGroup.get('totalAmount').setValue(+totalAmount.toFixed(2));
                PrStockItem.controls['stockSubTotalExVat'].patchValue(this.otherService.transformDecimal(subTotal));
                PrStockItem.controls['stockSubTotalIncVat'].patchValue(this.otherService.transformDecimal(totalAmount)); // vatAmount
                this.prCallRequestForm.controls['totalStockItems'].setValue('R 0.00');
                let total = 0
                this.PrStockItemsFormArray.value.forEach((item) => {
                  total += this.otherService.transformCurrToNum(item.stockSubTotalIncVat);
                  this.prCallRequestForm.controls['totalStockItems'].setValue(this.otherService.transformDecimal(+total));

                })
              })
              this.PrStockItemsFormArray.push(PrStockItem);
              this.prCallRequestForm.controls['totalStockItems'].setValue(this.otherService.transformDecimal(+totalAmount)); // vatAmount
              //----end-- //


            }
          } else {
            // let prCallDetails = this.billOfMaterialItemsFormArray.controls.find(element => element.get('itemName').value.toLowerCase() == 'discounts allowed');
            let prCallDetails = this.billOfMaterialItemsFormArray.controls.find(element => (element.get('itemId').value == this.billOfMaterialModel['prCallDiscountItemId'] && this.billOfMaterialModel['isPRCall']) ||
              (element.get('itemId').value == this.billOfMaterialModel['saveOfferDiscountItemId'] && this.billOfMaterialModel['isSaveOffer']));
            if (prCallDetails) {
              const unitPricePRDetails = Math.abs(prCallDetails.get('unitPrice').value) + +billOfMaterialItemFormGroup.get('unitPrice').value;
              prCallDetails.get('unitPrice').setValue(-Math.abs(unitPricePRDetails).toFixed(2));
              const subTotalPRDetails = Math.abs(prCallDetails.get('subTotal').value) + +billOfMaterialItemFormGroup.get('subTotal').value;
              prCallDetails.get('subTotal').setValue(-Math.abs(subTotalPRDetails).toFixed(2));
              const vatAmountPRDetails = Math.abs(prCallDetails.get('vATAmount').value) + +billOfMaterialItemFormGroup.get('vATAmount').value;
              prCallDetails.get('vATAmount').setValue(-Math.abs(vatAmountPRDetails).toFixed(2));
              const totalAmountPRDetails = Math.abs(prCallDetails.get('totalAmount').value) + +billOfMaterialItemFormGroup.get('totalAmount').value
              prCallDetails.get('totalAmount').setValue(-Math.abs(totalAmountPRDetails).toFixed(2));
              this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue('R 0.00');
            }
            this.sortStockCodes(billOfMaterialItemFormGroup);
            this.resetStockDescForm();
            // this.stockCodeDescriptionForm.get('stockType').setValue('', { emitEvent: false });
          }
        } else if (this.isNKACustomer && +billOfMaterialItemFormGroup.get('subTotal').value > Math.abs(differenceAmount)) { //For NKA customer
          if (!this.billOfMaterialModel?.billOfMaterialId) {
            this.snackbarService.openSnackbar("Please save the bill of materials first", ResponseMessageTypes.WARNING);
            return;
          }
          this.crudService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.NKA_SERVICE_CALL_PURCHASE_ORDER,
            null, null, prepareRequiredHttpParams({ callInitiationId: this.billOfMaterialModel.callInitiationId }))
            .subscribe((response: IApplicationResponse) => {
              if (response?.isSuccess && response?.statusCode == 200) {
                let data = {
                  stockType: this.stockCodeDescriptionForm?.get('stockType')?.value,
                  billOfMaterialModel: this.billOfMaterialModel,
                  tax: this.standardTax,
                  userId: this.userData?.userId,
                  newData: {
                    NKAPurchaseOrderRequestItemId: null,
                    NKAPurchaseOrderRequestId: null,
                    itemId: el?.get('itemId').value,
                    itemCode: el?.get('stockCode').value,
                    itemName: el?.get('stockDescription').value,
                    quantity: el?.get('stockRequestedQty').value,
                    unitPriceExclVat: el?.get('stockUnitPriceExcl').value,
                    subTotalExclVat: subTotal,
                    subTotalInclVat: totalAmount, //vatAmount,
                    itemPricingConfigId: el?.get('itemPricingConfigId').value
                  },
                  response: response?.resources,
                }
                this.openNKADoaRequestDialog(data);
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            })
        }
        else {
          // setting values for final row ie. BOM Invoice
          this.billOfMaterialForm.get('bomInvoice.subTotal').setValue(bomsubTotal ? bomsubTotal.toFixed(2) : 0);
          this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue(bomvATAmount ? bomvATAmount.toFixed(2) : 0);
          this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue(bomtotalAmount ? bomtotalAmount.toFixed(2) : 0);

          if (el?.get('stockDescription').value.toLowerCase() == 'servicing discount technical') {
            // For servicing discount technical, open approval pop up, get approval from 3 managers.
            // we dont perform any other calculations, we just patch unitPrice with stockUnitPriceExcl and others as 0.
            // after final approval values related to servicing discount technical will come from backend bom main GET api.
            // since we dont change final BOM Invoice calculations when we add servicing discount technical there is no
            // need of extra logic while removing servicing discount technical from Remove Stock.
            // billOfMaterialItemFormGroup.get('unitPrice').valueChanges.subscribe(unitPrice => {
            //   unitPrice = this.otherService.transformCurrToNum(unitPrice);
            //   if (unitPrice > (this.otherService.transformCurrToNum(this.billOfMaterialForm.get('bomInvoice.totalAmount').value) * (this.billOfMaterialModel['technicalDiscountPercentage']) / 100)) {
            //     const dialogReff = this.dialog.open(ServiceForDiscountModalComponent, {
            //       width: '450px',
            //       data: {
            //         header: 'Service Call Discount Approval Request',
            //         createdUserId: this.userData?.userId,
            //         BillofMaterialId: this.billOfMaterialModel['billOfMaterialId'],
            //         itemId: billOfMaterialItemFormGroup?.get('itemId')?.value,
            //         DiscountAmountExVat: unitPrice,
            //         DiscountAmountInVat: unitPrice + (unitPrice * this.standardTax / 100),
            //         technicialCallDiscountRequestId: this.billOfMaterialModel['technicialCallDiscountRequestId']
            //       },
            //       disableClose: true
            //     });
            //     dialogReff.afterClosed().subscribe(result => {
            //       if (!result) return;
            //     });
            //   } else {
            //     // alert('Dont open dialog box');
            //   }
            // });
            billOfMaterialItemFormGroup.get('unitPrice').patchValue(el.get('stockUnitPriceExcl').value);
            billOfMaterialItemFormGroup.get('subTotal').setValue('R 0.00');
            billOfMaterialItemFormGroup.get('vATAmount').setValue('R 0.00');
            billOfMaterialItemFormGroup.get('totalAmount').setValue('R 0.00');
          }
          if (!this.billOfMaterialModel['isPRCall'] && !this.billOfMaterialModel['isPRcallSaveOfferMaxCount']) {
            // Discount Warranties starts
            this.crudService.get(
              ModulesBasedApiSuffix.TECHNICIAN,
              TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_WARRANTY_CHECK,
              undefined,
              false,
              prepareGetRequestHttpParams('', '', {
                CallInitiationId: this.callInitiationId,
                ItemId: el?.get('itemId').value,
                StockTypeId: el?.get('stockTypeId').value
              })
            ).subscribe((response: IApplicationResponse) => {
              this.rxjsService.setGlobalLoaderProperty(false);
              if (response.isSuccess == true && response.statusCode == 200) {
                // If this condition is true or stock code has isWarranty then we add 3 prices of that stock code
                // to BOM.Invoice row and also in Warranty Discount row ie. 2 rows
                // After adding, we deduct prices of Warranty Discount row from BOM.Invoice row
                // since request and warrantyQty is same
                // logic for changed warranty quantity is handled in openDiscountWarranties()
                let discountStockDetails = billOfMaterialItemsFormArray.controls.find(element => element.get('itemId').value.toLowerCase() == response.resources['itemId'].toLowerCase());
                if (response.resources['isWarranty'] || response.resources['isDiscountMaintenance']) {
                  // We are setting value in other than Discount Warranty but for isWarranty >> true items
                  billOfMaterialItemFormGroup.get('IsWarranty').setValue(true);

                  // billOfMaterialItemFormGroup.get('WarrantyQty').setValue(billOfMaterialItemFormGroup.get('request').value);
                  billOfMaterialItemFormGroup.get('WarrantyQty').setValue(+el?.get('stockRequestedQty').value);
                  this.warrantyMaintenanceDetail.push(billOfMaterialItemFormGroup);
                }
                if (discountStockDetails == undefined && (response.resources['isWarranty'] || response.resources['isDiscountMaintenance'])) {
                  let warsubTotal = subTotal;
                  let warVatAmount = vatAmount;
                  let warTotalAmount = totalAmount;
                  if (+response?.resources?.warrantyQty <= +el?.get('stockRequestedQty').value) {
                    billOfMaterialItemFormGroup.get('WarrantyQty').setValue(+response?.resources?.warrantyQty);
                    warsubTotal = +billOfMaterialItemFormGroup.get('WarrantyQty').value * +billOfMaterialItemFormGroup.get('unitPrice').value;
                    warVatAmount = +warsubTotal * (this.standardTax / 100);
                    warTotalAmount = +warsubTotal + +billOfMaterialItemFormGroup.get('vATAmount').value;
                  }
                  // Related to Discount Warrenties 1st time
                  let billOfMaterialWarrantyItemFormGroup = this.formBuilder.group({
                    checkBillOfMaterial: false,
                    IsDeleted: false,
                    billOfMaterialItemId: '',
                    billOfMaterialId: '',
                    stockTypeId: response.resources['stockTypeId'],
                    stockType: response.resources['stockType'],
                    itemId: response.resources['itemId'],
                    itemCode: response.resources['itemCode'],
                    itemName: response.resources['itemName'],
                    // kitConfigId: response.resources['kitConfigId'],
                    // kitTypeName: response.resources['kitTypeName'],
                    invoice: 0,
                    transfer: 0,
                    used: 0,
                    reserved: 0,
                    request: 0,// +billOfMaterialItemFormGroup.get('request').value,
                    unitPrice: -Math.abs(this.otherService.transformCurrToNum(billOfMaterialItemFormGroup.get('unitPrice').value)).toFixed(2),
                    subTotal: -Math.abs(+warsubTotal).toFixed(2),
                    vATAmount: vatAmount?.toString() ? -Math.abs(+warVatAmount).toFixed(2) : 0,
                    totalAmount: -Math.abs(+warTotalAmount).toFixed(2),
                    createdUserId: this.userData?.userId,
                    modifiedUserId: this.userData?.userId,
                    IsWarranty: false,
                    WarrantyQty: null
                  });

                  if (el?.get('stockType').value.toLowerCase() == 'y') {
                    if (el?.get('isNotSerialized').value) {
                      if (+response?.resources?.warrantyQty >= +el?.get('stockRequestedQty').value) {
                        billOfMaterialWarrantyItemFormGroup.get('request').setValue(+el?.get('stockRequestedQty').value);
                      } else if (+response?.resources?.warrantyQty < +el?.get('stockRequestedQty').value) {
                        billOfMaterialWarrantyItemFormGroup.get('request').setValue(+response?.resources?.warrantyQty);
                      }
                    } else if (+response?.resources?.warrantyQty >= +el?.get('stockRequestedQty').value) {
                      billOfMaterialWarrantyItemFormGroup.get('request').setValue(+el?.get('stockRequestedQty').value);
                    } else if (+response?.resources?.warrantyQty < +el?.get('stockRequestedQty').value) {
                      billOfMaterialWarrantyItemFormGroup.get('request').setValue(+response?.resources?.warrantyQty);
                    }
                  } else if (el?.get('stockType').value.toLowerCase() == 'm') {
                    billOfMaterialWarrantyItemFormGroup.get('used').setValue(+el?.get('stockRequestedQty').value);
                  }


                  billOfMaterialItemsFormArray.push(billOfMaterialWarrantyItemFormGroup);
                  // setting values for final row ie. BOM Invoice
                  this.billOfMaterialForm.get('bomInvoice.subTotal').setValue((this.billOfMaterialForm.get('bomInvoice.subTotal').value - Math.abs(billOfMaterialWarrantyItemFormGroup.get('subTotal').value)).toFixed(2));
                  this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue((this.billOfMaterialForm.get('bomInvoice.vatAmount').value - Math.abs(billOfMaterialWarrantyItemFormGroup.get('vATAmount').value)).toFixed(2));
                  this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue((this.billOfMaterialForm.get('bomInvoice.totalAmount').value - Math.abs(billOfMaterialWarrantyItemFormGroup.get('totalAmount').value)).toFixed(2));


                } else if (discountStockDetails != undefined && (response.resources['isWarranty'] || response.resources['isDiscountMaintenance'])) {
                  // if (discountStockDetails != undefined) {
                  let billOfMaterialWarrantyItemFormGroup = this.formBuilder.group(billOfMaterialItemFormGroup.getRawValue());
                  if (+response?.resources?.warrantyQty <= +el?.get('stockRequestedQty').value) {
                    billOfMaterialItemFormGroup.get('WarrantyQty').setValue(+response?.resources?.warrantyQty);
                    billOfMaterialWarrantyItemFormGroup.get('WarrantyQty').setValue(+response?.resources?.warrantyQty);
                  }
                  let warSubTotal = +billOfMaterialWarrantyItemFormGroup.get('WarrantyQty').value * +billOfMaterialItemFormGroup.get('unitPrice').value;
                  let warVatAmount = +warSubTotal * (this.standardTax / 100);
                  let warTotalAmount = +warSubTotal + +vatAmount;
                  billOfMaterialWarrantyItemFormGroup?.get('subTotal').setValue(warSubTotal);
                  billOfMaterialWarrantyItemFormGroup?.get('vATAmount').setValue(warVatAmount);
                  billOfMaterialWarrantyItemFormGroup?.get('totalAmount').setValue(warTotalAmount);
                  if (el?.get('stockType').value.toLowerCase() == 'y') {
                    if (el?.get('isNotSerialized').value) {
                      if (+response?.resources?.warrantyQty >= +el?.get('stockRequestedQty').value) {
                        billOfMaterialWarrantyItemFormGroup.get('request').setValue(+el?.get('stockRequestedQty').value);
                      } else if (+response?.resources?.warrantyQty < +el?.get('stockRequestedQty').value) {
                        billOfMaterialWarrantyItemFormGroup.get('request').setValue(+response?.resources?.warrantyQty);
                      }
                    } else if (+response?.resources?.warrantyQty >= +el?.get('stockRequestedQty').value) {
                      billOfMaterialWarrantyItemFormGroup.get('request').setValue(+el?.get('stockRequestedQty').value);
                    } else if (+response?.resources?.warrantyQty < +el?.get('stockRequestedQty').value) {
                      billOfMaterialWarrantyItemFormGroup.get('request').setValue(+response?.resources?.warrantyQty);
                    }
                  }
                  // }
                  // Related to Discount Warrenties other than 1st time
                  discountStockDetails.get('request').setValue(((Math.abs(+discountStockDetails.get('request').value) + +billOfMaterialWarrantyItemFormGroup.get('request').value)));
                  discountStockDetails.get('request').setValue(((Math.abs(+discountStockDetails.get('request').value) + +billOfMaterialWarrantyItemFormGroup.get('request').value)));
                  discountStockDetails.get('transfer').setValue(((Math.abs(+discountStockDetails.get('transfer').value) + +billOfMaterialWarrantyItemFormGroup.get('transfer').value)));
                  discountStockDetails.get('reserved').setValue(((Math.abs(+discountStockDetails.get('reserved').value) + +billOfMaterialWarrantyItemFormGroup.get('reserved').value)));
                  discountStockDetails.get('invoice').setValue(((Math.abs(+discountStockDetails.get('invoice').value) + +billOfMaterialWarrantyItemFormGroup.get('invoice').value)));
                  discountStockDetails.get('unitPrice').setValue((-Math.abs(Math.abs(+discountStockDetails.get('unitPrice').value) + +billOfMaterialWarrantyItemFormGroup.get('unitPrice').value)).toFixed(2));
                  discountStockDetails.get('subTotal').setValue((-Math.abs(Math.abs(+discountStockDetails.get('subTotal').value) + +billOfMaterialWarrantyItemFormGroup.get('subTotal').value)).toFixed(2));
                  discountStockDetails.get('vATAmount').setValue((-Math.abs(Math.abs(+discountStockDetails.get('vATAmount').value) + +billOfMaterialWarrantyItemFormGroup.get('vATAmount').value)).toFixed(2));
                  discountStockDetails.get('totalAmount').setValue((-Math.abs(Math.abs(+discountStockDetails.get('totalAmount').value) + +billOfMaterialWarrantyItemFormGroup.get('totalAmount').value)).toFixed(2));

                  // setting values for final row ie. BOM Invoice
                  this.billOfMaterialForm.get('bomInvoice.subTotal').setValue((this.billOfMaterialForm.get('bomInvoice.subTotal').value - billOfMaterialWarrantyItemFormGroup.get('subTotal').value).toFixed(2));
                  this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue((this.billOfMaterialForm.get('bomInvoice.vatAmount').value - billOfMaterialWarrantyItemFormGroup.get('vATAmount').value).toFixed(2));
                  this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue((this.billOfMaterialForm.get('bomInvoice.totalAmount').value - billOfMaterialWarrantyItemFormGroup.get('totalAmount').value).toFixed(2));

                } else {
                  // Not Related to Discount Warrenties but for other stock codes for which isWarranty or isMantainance is
                  // false
                  this.onCalculateFinalTotal()
                }
                this.sortStockCodes(billOfMaterialItemFormGroup);
                this.resetStockDescForm();

              }
              // this.stockCodeDescriptionForm.reset({ emitEvent: false });
              // this.stockCodeDescriptionForm.get('stockCode').reset(' ', { emitEvent: false });
              // this.stockCodeDescriptionForm.get('stockDescription').reset(' ', { emitEvent: false });
            });
          } else {
            this.sortStockCodes(billOfMaterialItemFormGroup);
            this.resetStockDescForm();
          }
          // Discount Warranties ends

          // this.stockCodeDescriptionForm.reset({ emitEvent: false });
          // this.stockCodeDescriptionForm.get('stockCode').reset(' ', { emitEvent: false });
          // this.stockCodeDescriptionForm.get('stockDescription').reset(' ', { emitEvent: false });
          // billOfMaterialItemsFormArray.push(billOfMaterialItemFormGroup);
        }
      }
    });
  }

  resetStockDescForm() {
    this.clearFormArray(this.stockAddItemFormarry);
    this.stockCodeDescriptionForm.reset('', { emitEvent: false });
    this.stockCodeDescriptionForm.get('stockType').reset('', { emitEvent: false });
    this.billOfMaterialForm.get('isStock').setValue(false, { emitEvent: false });
    this.billOfMaterialForm.get('isDiscount').setValue(false, { emitEvent: false });
    this.stockCodeDescriptionForm.markAsTouched();
  }

  openNKADoaRequestDialog(data) {
    const reqNumber = data?.response?.nkaPurchaseOrderRequestNumber ? data?.response?.nkaPurchaseOrderRequestNumber : ''
    const ref = this.dialog.open(NkaDoaRequestDialogComponent, {
      width: '1050px',
      data: { ...data, header: `NKA Purchase Order Request ${reqNumber}`, },
    });
    ref.afterClosed().subscribe((res) => {
      if (res) {
        // this.resetStockDescForm();
        this.getBillOfMaterialData();
      }
    });
  }

  getDiscountValueItems(itemId) {
    let findItem = [];
    this.billOfMaterialModel?.technicianDiscountItems?.forEach(el => {
      if (itemId == el) {
        findItem.push(true);
      }
    });
    return findItem?.length ? true : false;
  }

  enableUnitPrice(control) {
    return !(this.billOfMaterialModel?.lightningDiscountItemId == control.get('itemId').value) || !this.getDiscountValueItems(control?.get('itemId')?.value);
  }

  onFocusUnitPrice(i: number) {
    const unitPrice = Math.abs(this.otherService.transformCurrToNum(this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice').value));
    this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice').setValue(unitPrice);
  }

  onBlurUnitPrice(i: number, control) {
    const negaNum = this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice').value < 0 ? this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice').value :
      control?.get('stockType')?.value == 'M' ? -Math.abs(this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice').value) : this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice').value;
    if (negaNum?.toString()) {
      const unitPrice = this.otherService.transformDecimal(negaNum);
      if (this.billOfMaterialModel['availableLightningDiscount'] < Math.abs(this.otherService.transformCurrToNum(unitPrice)) && this.billOfMaterialModel['lightningDiscountItemId'] == this.billOfMaterialItemsFormArray?.controls[i]?.get('itemId').value) {
        this.snackbarService.openSnackbar(`Please enter lighting cover amount is less than equal to ${this.billOfMaterialModel['availableLightningDiscount']}`,
          ResponseMessageTypes.WARNING);
        return;
      }
      // const unitPrice = negaNum;
      this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice').setValue(unitPrice);
    }
  }

  onKeyUpReserved(i, control) {
    if (!this.onNotSerialized(control, i) && control?.get('reserved').value) {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_VADATE_NOTSERIALIZED, null, false,
        prepareRequiredHttpParams({
          userId: this.billOfMaterialModel['technicianId'], itemId: this.billOfMaterialItemsFormArray?.controls[i]?.get('itemId').value, callInitiationId: this.installationId,
          qty: this.billOfMaterialItemsFormArray?.controls[i]?.get('reserved').value, districtId: this.billOfMaterialModel?.districtId ? this.billOfMaterialModel?.districtId : null
        }))
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess && res?.statusCode == 200 && res?.resources?.isSuccess) {
            if (+this.billOfMaterialItemsFormArray?.controls[i]?.get('reserved').value == 0) {
              this.billOfMaterialItemsFormArray?.controls[i]?.get('reserved')?.setErrors(null, { emitEvent: false });
              this.billOfMaterialItemsFormArray?.controls[i]?.get('reserved').setValue(0, { emitEvent: false });
              this.billOfMaterialItemsFormArray?.controls[i]?.get('request').setValue(this.billOfMaterialModel?.bomQuotationItems[i]?.request, { emitEvent: false });
              this.billOfMaterialItemsFormArray?.controls[i]?.get('transfer').setValue(this.billOfMaterialModel?.bomQuotationItems[i]?.transfer, { emitEvent: false });
            } else if (+this.billOfMaterialItemsFormArray?.controls[i]?.get('request').value == 0 && +this.billOfMaterialItemsFormArray?.controls[i]?.get('transfer').value == 0) {
              if (+this.billOfMaterialItemsFormArray?.controls[i]?.get('reserved').value <= (+this.billOfMaterialModel?.bomQuotationItems[i]?.request + +this.billOfMaterialModel?.bomQuotationItems[i]?.transfer)) {
                let reservedTot = +this.billOfMaterialItemsFormArray?.controls[i]?.get('reserved').value;
                let requestTot = +this.billOfMaterialModel?.bomQuotationItems[i]?.request;
                let transferTot = +this.billOfMaterialModel?.bomQuotationItems[i]?.transfer;
                if (transferTot) {
                  if (transferTot >= reservedTot) {
                    transferTot -= reservedTot;
                  } else {
                    reservedTot -= transferTot;
                    transferTot = 0;
                  }
                  this.billOfMaterialItemsFormArray?.controls[i]?.get('transfer').setValue(transferTot);
                }
                if (requestTot) {
                  if (requestTot >= reservedTot) {
                    requestTot -= reservedTot;
                  } else {
                    reservedTot -= requestTot;
                    requestTot = 0;
                  }
                  this.billOfMaterialItemsFormArray?.controls[i]?.get('request').setValue(requestTot);
                }

              } else {
                this.snackbarService.openSnackbar("Requested quantity has met not allowed add more items", ResponseMessageTypes.WARNING);
                this.billOfMaterialItemsFormArray?.controls[i]?.get('reserved')?.setErrors({ invalid: true }, { emitEvent: false });
              }
            } else if (+this.billOfMaterialItemsFormArray?.controls[i]?.get('reserved').value != 0) {
              let reservedTot = +this.billOfMaterialItemsFormArray?.controls[i]?.get('reserved').value;
              let requestTot = +this.billOfMaterialModel?.bomQuotationItems[i]?.request;
              let transferTot = +this.billOfMaterialModel?.bomQuotationItems[i]?.transfer;
              if (reservedTot > (+this.billOfMaterialModel?.bomQuotationItems[i]?.request + +this.billOfMaterialModel?.bomQuotationItems[i]?.transfer)) {
                this.snackbarService.openSnackbar("Requested quantity has met not allowed add more items", ResponseMessageTypes.WARNING);
                this.billOfMaterialItemsFormArray?.controls[i]?.get('reserved')?.setErrors({ invalid: true }, { emitEvent: false });
                return;
              }
              if (transferTot != 0) {
                if (transferTot >= reservedTot) {
                  transferTot -= reservedTot;
                } else {
                  reservedTot -= transferTot;
                  transferTot = 0;
                }
                this.billOfMaterialItemsFormArray?.controls[i]?.get('transfer').setValue(transferTot);
                this.billOfMaterialItemsFormArray?.controls[i]?.get('request').setValue(this.billOfMaterialModel?.bomQuotationItems[i]?.request);
              }
              if (requestTot != 0 && +this.billOfMaterialItemsFormArray?.controls[i]?.get('reserved').value > +this.billOfMaterialModel?.bomQuotationItems[i]?.transfer) {
                if (requestTot >= +reservedTot) {
                  requestTot -= +reservedTot;
                } else {
                  reservedTot -= requestTot;
                  requestTot = 0;
                }
                this.billOfMaterialItemsFormArray?.controls[i]?.get('request').setValue(requestTot);
              }
            }
            this.billOfMaterialItemsFormArray?.controls[i]?.get('reserved')?.updateValueAndValidity({ emitEvent: false });
          } else if (res?.isSuccess && res?.statusCode == 200 && !res?.resources?.isSuccess) {
            this.snackbarService.openSnackbar(res?.resources?.validateMessage, ResponseMessageTypes.WARNING);
            this.billOfMaterialItemsFormArray?.controls[i]?.get('reserved').setValue(this.billOfMaterialModel?.bomQuotationItems[i]?.reserved);
            this.billOfMaterialItemsFormArray?.controls[i]?.get('reserved')?.setErrors(null, { emitEvent: false });
            this.billOfMaterialItemsFormArray?.controls[i]?.get('request').setValue(this.billOfMaterialModel?.bomQuotationItems[i]?.request, { emitEvent: false });
            this.billOfMaterialItemsFormArray?.controls[i]?.get('transfer').setValue(this.billOfMaterialModel?.bomQuotationItems[i]?.transfer, { emitEvent: false });
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    } else if (!this.onNotSerialized(control, i) && !control?.get('reserved').value) {
      this.billOfMaterialItemsFormArray?.controls[i]?.get('request').setValue(this.billOfMaterialModel?.bomQuotationItems[i]?.request, { emitEvent: false });
      this.billOfMaterialItemsFormArray?.controls[i]?.get('transfer').setValue(this.billOfMaterialModel?.bomQuotationItems[i]?.transfer, { emitEvent: false });
    }
  }

  onKeyUpUnitPrice(i, control) {
    if (this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice').value == '-') {
      return;
    }
    const unitPrice: any = this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice').value ? this.otherService.transformCurrToNum(this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice').value) : '';
    if (typeof unitPrice != 'number' && unitPrice) {
      return;
    } if (!unitPrice) {
      this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice').setValidators([Validators.required, Validators.min(0)]);
      this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice').updateValueAndValidity();
    }
    // this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice').setValue(this.otherService.transformDecimal(-Math.abs(unitPrice)))
    if (this.billOfMaterialModel['lightningDiscountItemId'] == control?.get('itemId').value || this.getDiscountValueItems(control?.get('itemId').value)) { //lightning cover
      if (this.billOfMaterialModel['availableLightningDiscount'] < Math.abs(unitPrice) && this.billOfMaterialModel['lightningDiscountItemId'] == control?.get('itemId').value) {
        this.snackbarService.openSnackbar(`Please enter lighting cover amount is less than equal to ${this.billOfMaterialModel['availableLightningDiscount']}`,
          ResponseMessageTypes.WARNING);
        return;
      }
      this.onCalculateUnitPriceChange(i, unitPrice);
      if (this.getDiscountValueItems(control?.get('itemId').value)) {
        let totalAmountForPositiveIncVat = 0;
        const totalAmountIncVat = Math.abs(this.otherService.transformCurrToNum(this.billOfMaterialItemsFormArray?.controls[i].get('totalAmount').value));
        this.billOfMaterialItemsFormArray?.getRawValue()?.forEach(el => {
          if (this.otherService.transformCurrToNum(el?.totalAmount) > 0) {
            totalAmountForPositiveIncVat += this.otherService.transformCurrToNum(el?.totalAmount);
          }
        });
        const discount: any = (totalAmountIncVat * 100) / totalAmountForPositiveIncVat;
        if (+discount?.toFixed(2) > +this.billOfMaterialModel?.technicalDiscountPercentage) {
          this.openServiceDiscountDialog(i);
        }
      }
    }
  }

  onCalculateUnitPriceChange(i: number, unitPrice: number) {
    let totalRequestQty = 0
    const used = +this.billOfMaterialItemsFormArray?.controls[i].get('used').value;
    const request = +this.billOfMaterialItemsFormArray?.controls[i].get('request').value;
    const transfer = +this.billOfMaterialItemsFormArray?.controls[i].get('transfer').value;
    const reserved = +this.billOfMaterialItemsFormArray?.controls[i].get('reserved').value;
    const invoice = +this.billOfMaterialItemsFormArray?.controls[i].get('invoice').value;
    totalRequestQty = used + request + transfer + reserved + invoice;
    const subtotalVat = (totalRequestQty * unitPrice);
    const vatAmount = (subtotalVat * this.billOfMaterialModel['taxPercentage']) / 100;
    const totalinclVat = subtotalVat + vatAmount;
    // this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice').setValue(-Math.abs(unitPrice));
    this.billOfMaterialItemsFormArray?.controls[i].get('subTotal').setValue(this.otherService.transformDecimal(-Math.abs(subtotalVat)));
    this.billOfMaterialItemsFormArray?.controls[i].get('vATAmount').setValue(this.otherService.transformDecimal(-Math.abs(vatAmount)));
    this.billOfMaterialItemsFormArray?.controls[i].get('totalAmount').setValue(this.otherService.transformDecimal(-Math.abs(totalinclVat)));
    this.onCalculateFinalTotal();
  }

  onCalculateFinalTotal() {
    let bomsubTotal = 0;
    let bomvATAmount = 0;
    let bomtotalAmount = 0;
    this.billOfMaterialItemsFormArray?.getRawValue()?.forEach(el => {
      bomsubTotal += this.otherService.transformCurrToNum(el?.subTotal);
      bomvATAmount += this.otherService.transformCurrToNum(el?.vATAmount);
      bomtotalAmount += this.otherService.transformCurrToNum(el?.totalAmount);
    })
    this.billOfMaterialForm.get('bomInvoice.subTotal').setValue(this.otherService.transformDecimal(bomsubTotal));
    this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue(this.otherService.transformDecimal(bomvATAmount));
    this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue(this.otherService.transformDecimal(bomtotalAmount));
  }

  openServiceDiscountDialog(i: number) {
    const dialogReff = this.dialog.open(ServiceForDiscountModalComponent, {
      width: '450px',
      data: {
        header: 'Service Call Discount Approval Request',
        createdUserId: this.userData?.userId,
        index: i,
        itemId: this.billOfMaterialItemsFormArray?.controls[i].get('itemId')?.value,
        BillofMaterialId: this.billOfMaterialModel['billOfMaterialId'],
        DiscountAmountExVat: Math.abs(this.otherService.transformCurrToNum(this.billOfMaterialItemsFormArray?.controls[i].get('subTotal').value)),
        DiscountAmountInVat: Math.abs(this.otherService.transformCurrToNum(this.billOfMaterialItemsFormArray?.controls[i].get('totalAmount').value)),
        technicialCallDiscountRequestId: this.billOfMaterialModel['technicialCallDiscountRequestId']
      },
      disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (!result?.technicialCallDiscountRequestId) {
        this.billOfMaterialItemsFormArray?.controls[i].get('unitPrice')?.setValue('R 0.00');
        this.onCalculateUnitPriceChange(i, 0);
      };
    });
  }

  getGoodsReturnDetails(StockOrderNumber) {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_GOODS_RETURN_DETAILS,
      undefined,
      false,
      prepareRequiredHttpParams({
        StockOrderNumber: StockOrderNumber // '041220004'
      })).subscribe(response => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          if (response.resources.hasOwnProperty('goodsReturnsItems') && response.resources['goodsReturnsItems'].length > 0) {
            this.goodsReturnData = response.resources['goodsReturnsItems'];
          }
        }
      });
  }

  createBillOfMaterialForm(billOfMaterialModel?: BillOfMaterialModel) {
    let materialModel = new BillOfMaterialModel(billOfMaterialModel);
    this.billOfMaterialForm = this.formBuilder.group({});
    let billOfMaterialItemDetails = this.formBuilder.array([]);
    Object.keys(materialModel).forEach((key) => {
      if (typeof materialModel[key] !== 'object') {
        this.billOfMaterialForm.addControl(key, new FormControl(materialModel[key]));

        // this.billOfMaterialForm.addControl(key, new FormControl({
        //   value: materialModel[key],
        //   // disabled: (key.toLowerCase() == 'targetorderamount') ? true : false // || key.toLowerCase() == 'differenceamount'
        // }));

      } else {
        if (key == 'bomInvoice') {
          let bomInvoice = this.formBuilder.group({
            // 'exclVAT': [{ value: '', disabled: true }],
            // 'subTotal': [{ value: '', disabled: true }],
            // 'totalAmount': [{ value: '', disabled: true }],
            // 'vatAmount': [{ value: '', disabled: true }]
            'exclVAT': '',
            'subTotal': '',
            'totalAmount': '',
            'vatAmount': ''
          });
          this.billOfMaterialForm.addControl(key, bomInvoice); //technicianCollJobModel[key]
        } else if (key == 'billOfMaterialItems') {
          this.billOfMaterialForm.addControl(key, billOfMaterialItemDetails);
        }
      }
    });

    this.billOfMaterialForm = setRequiredValidator(this.billOfMaterialForm, ['billOfMaterialItems']);
    this.billOfMaterialForm.get('stockCodeSerialNumber').setValidators([Validators.minLength(4)]);
    this.billOfMaterialForm.get('stockCodeSerialNumber').updateValueAndValidity();
  }

  createStockCodeDescriptionForm(stockCodeDescriptionModel?: StockCodeDescriptionModel) {
    let stockModel = new StockCodeDescriptionModel(stockCodeDescriptionModel);
    this.stockCodeDescriptionForm = this.formBuilder.group({});
    Object.keys(stockModel).forEach((key) => {
      if (typeof stockModel[key] !== 'object') {
        this.stockCodeDescriptionForm.addControl(key, new FormControl({
          value: stockModel[key],
          disabled: (key.toLowerCase() == 'stockUnitPriceExcl' || key.toLowerCase() == 'stockRequestedQty') ? true : false
        }));
      } else {
        this.stockCodeDescriptionForm.addControl(key, new FormArray([]));
      }
    });
    this.stockCodeDescriptionForm = setRequiredValidator(this.stockCodeDescriptionForm, ['stockType', 'stockCode', 'stockDescription']);
    this.stockCodeDescriptionForm.get('stockUnitPriceExcl').disable();
    this.stockCodeDescriptionForm.get('stockRequestedQty').disable();
  }

  createPRCallStockItemForm(prCallStockItemModel?: PrCallStockItemModel) {
    let PrCallReqModel = new PrCallStockItemModel(prCallStockItemModel);
    this.prCallRequestForm = this.formBuilder.group({});
    let PrStockItemDetails = this.formBuilder.array([]);
    Object.keys(PrCallReqModel).forEach((key) => {
      if (typeof PrCallReqModel[key] !== 'object') {
        this.prCallRequestForm.addControl(key, new FormControl(PrCallReqModel[key]));

        // this.billOfMaterialForm.addControl(key, new FormControl({
        //   value: materialModel[key],
        //   // disabled: (key.toLowerCase() == 'targetorderamount') ? true : false // || key.toLowerCase() == 'differenceamount'
        // }));

      } else {
        if (key == 'stockItems') {
          this.prCallRequestForm.addControl(key, PrStockItemDetails);
        }
      }
    });

    this.prCallRequestForm = setRequiredValidator(this.prCallRequestForm, ['comments']);
  }

  getBillOfMaterialData() {
    this.warrantyMaintenanceDetail = [];
    this.serialNumberError = false;
    let obj = {
      CustomerId: this.customerId,
      CallInitiationId: this.callInitiationId
    };
    if (this.quotationVersionId) {
      obj['QuotationVersionId'] = this.quotationVersionId;
    }
    let api = [this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_DETAILS,
      undefined, false, prepareRequiredHttpParams(obj)).pipe(map(result => result), catchError(error => of(error))),
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.STANDARD_TAX).pipe(map(result => result), catchError(error => of(error))),
    ]
    forkJoin(api).pipe(map((res: IApplicationResponse[]) => {
      res?.forEach((response: IApplicationResponse, ix: number) => {
        if (response?.isSuccess == true && response?.statusCode == 200) {
          switch (ix) {
            case 0:
              response?.resources?.bomQuotationItems?.forEach(val => {
                if (response?.resources?.lightningDiscountItemId == val?.itemId) {
                  val.unitPrice = this.otherService.transformDecimal(val.unitPrice);
                }
                return val;
              })
              break;
          }
        }
      })
      return res;
    })).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((res: IApplicationResponse, ix: number) => {
        if (res.isSuccess == true && res.statusCode == 200) {
          switch (ix) {
            case 0:
              this.billOfMaterialModel = res?.resources;
              this.onPatchValue();
              break;
            case 1:
              this.standardTax = res?.resources?.taxPercentage;
              break;
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
      if (this.isVoid) {
        this.billOfMaterialForm.disable();
        this.billOfMaterialForm.markAsUntouched();
        this.stockCodeDescriptionForm.disable();
        this.stockCodeDescriptionForm.markAsUntouched();
      }
      if (this.billOfMaterialModel['isInstallationOnlyCustomer'] && this.billOfMaterialModel['isRadio']) {
        this.openConfigDialog({ title: 'Notification', message: `Unable to load radio, Installed only customer` });
        return;
      }
    });
  }

  onPatchValue() {
    if (this.billOfMaterialModel.serviceCallRequestId == null) {
      this.isEscalateSubmit = false;
    }
    else {
      this.isEscalateSubmit = true;
    }
    let billOfMaterialModel = new BillOfMaterialModel(this.billOfMaterialModel, this.userData?.userId);
    let billOfMaterialItemsFormArray = this.billOfMaterialItemsFormArray;
    billOfMaterialItemsFormArray.clear();
    billOfMaterialModel['billOfMaterialItems'].forEach(element => {
      // let billOfMaterialItemDetailsFormArray = this.formBuilder.array([]);
      let billOfMaterialItemFormGroup = this.formBuilder.group({
        checkBillOfMaterial: element['checkBillOfMaterial'],
        billOfMaterialItemId: element['billOfMaterialItemId'],
        billOfMaterialId: element['billOfMaterialId'],
        stockTypeId: element['stockTypeId'],
        stockType: element['stockType'],
        itemId: element['itemId'],
        itemCode: element['itemCode'],
        itemName: element['itemName'],
        invoice: element['invoice'],
        transfer: element['transfer'],
        used: element['used'],
        reserved: element['reserved'],
        request: element['request'],
        unitPrice: element['unitPrice'],
        subTotal: element['subTotal'],
        kitConfigId: element['kitConfigId'],
        kitTypeName: element['kitTypeName'],
        // unitPrice: [{ value: element['unitPrice'], disabled: element['stockType'].toLowerCase() == 'y'? true: false }],
        // subTotal: [{ value: element['subTotal'], disabled: true }],
        vATAmount: element['vATAmount'] ? element['vATAmount'] : 0,
        totalAmount: element['totalAmount'],
        createdUserId: element['createdUserId'],
        modifiedUserId: element['modifiedUserId'],
        IsDeleted: element['IsDeleted'],
        IsWarranty: element['IsWarranty'],
        WarrantyQty: element['WarrantyQty'],
        systemTypeId: element['systemTypeId'],
        itemPricingConfigId: element['itemPricingConfigId'],
        isNotSerialized: element['isNotSerialized'],
      });
      // if (billOfMaterialItemFormGroup.get('itemName').value.toLowerCase() == 'servicing discount technical') {
      //   billOfMaterialItemFormGroup.get('unitPrice').valueChanges.subscribe(unitPrice => {
      //     unitPrice = this.otherService.transformCurrToNum(unitPrice);
      //     if (unitPrice > (this.otherService.transformCurrToNum(this.billOfMaterialForm.get('bomInvoice.totalAmount').value) * (this.billOfMaterialModel['technicalDiscountPercentage']) / 100)) {
      //       const dialogReff = this.dialog.open(ServiceForDiscountModalComponent, {
      //         width: '450px',
      //         data: {
      //           header: 'Service Call Discount Approval Request',
      //           createdUserId: this.userData?.userId,
      //           BillofMaterialId: this.billOfMaterialModel['billOfMaterialId'],
      //           itemId: billOfMaterialItemFormGroup?.get('itemId')?.value,
      //           DiscountAmountExVat: unitPrice,
      //           DiscountAmountInVat: unitPrice + (unitPrice * this.standardTax / 100),
      //           technicialCallDiscountRequestId: this.billOfMaterialModel['technicialCallDiscountRequestId']
      //         },
      //         disableClose: true
      //       });
      //       dialogReff.afterClosed().subscribe(result => {
      //         if (!result) return;
      //       });
      //     } else {
      //     }
      //   });
      // }
      if (element['IsWarranty']) {
        this.warrantyMaintenanceDetail.push(billOfMaterialItemFormGroup);
      }
      billOfMaterialItemsFormArray.push(billOfMaterialItemFormGroup);
    });
    this.billOfMaterialForm.patchValue(billOfMaterialModel, { emitEvent: false });
    this.billOfMaterialForm.get('createdUserId').setValue(this.userData?.userId);
    this.billOfMaterialForm.get('modifiedUserId').setValue(this.userData?.userId);
    // this.billOfMaterialForm.get('referenceId').setValue(this.quotationVersionId);
    this.billOfMaterialForm.get('referenceId').setValue(this.callInitiationId);
    this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue(billOfMaterialModel['bomInvoice']['totalAmount'] ? billOfMaterialModel['bomInvoice']['totalAmount'] : 0);
    // this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue(billOfMaterialModel['bomInvoice']['totalAmount']);
    this.billOfMaterialForm.get('isDiscount').setValue(false);
    this.billOfMaterialForm.get('isStock').setValue(false);
    if (this.billOfMaterialModel['stockOrderNumber'] && this.callType == '1' && !this.billOfMaterialModel?.isDealerCall) {
      this.getGoodsReturnDetails(this.billOfMaterialModel['stockOrderNumber']);
    } else {
      this.goodsReturnData = [];
    }
    // this.onCalculateFinalTotal();
    this.billOfMaterialItemsFormArray.markAsUntouched();
    this.billOfMaterialItemsFormArray.markAsPristine();
    this.billOfMaterialForm.get('stockCodeSerialNumber').markAsUntouched();
    this.stockCodeDescriptionForm.get('stockType').markAsUntouched();
    this.stockCodeDescriptionForm.get('stockRequestedQty').markAsUntouched();
  }

  get billOfMaterialItemsFormArray(): FormArray {
    if (this.billOfMaterialForm !== undefined) {
      return (<FormArray>this.billOfMaterialForm.get('billOfMaterialItems'));
    }
  }

  get PrStockItemsFormArray(): FormArray {
    if (this.prCallRequestForm !== undefined) {
      return (<FormArray>this.prCallRequestForm.get('stockItems'));
    }
  }

  get stockAddItemFormarry(): FormArray {
    if (this.stockCodeDescriptionForm !== undefined) {
      return this.stockCodeDescriptionForm.get('stockAddItemFormarry') as FormArray;
    }
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  getBillOfMaterialItemDetails() {
    // https://fidelity-technician-dev.azurewebsites.net/api/bill-of-material/bill-of-material-item-details?BillOfMaterialId=85DFCAEA-9A6E-4B24-8992-37536372E111&BillOfMaterialItemId=C7FCCD6A-76C4-4FC4-A2E4-0B4A646251C6
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_ITEM_DETAILS,
      undefined,
      false,
      prepareGetRequestHttpParams('', '', {
        BillOfMaterialId: '85DFCAEA-9A6E-4B24-8992-37536372E111',
        BillOfMaterialItemId: 'C7FCCD6A-76C4-4FC4-A2E4-0B4A646251C6'
      })
    ).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
      }
    })
  }

  openStockDetailsPopup() {
    const dialogRemoveStockCode = this.dialog.open(BillOfMaterialScanModalComponent, {
      width: '450px',
      data: {
        buttons: {
          cancel: 'No',
          create: 'Yes'
        }
      }, disableClose: true
    });

    dialogRemoveStockCode.afterClosed().subscribe(result => {
      if (!result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  openZonePopup() {
    if (!this.getBOMActionPermission(PermissionTypes.ADD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const dialogRemoveStockCode = this.dialog.open(BillOfMaterialZoneModalComponent, {
      width: '450px',
      data: {
        message: `Are you sure you want to<br />remove the stock code: ?`,
        buttons: {
          cancel: 'No',
          create: 'Yes'
        }
      }, disableClose: true
    });

    dialogRemoveStockCode.afterClosed().subscribe(result => {
      if (!result) return;

      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  navigateToPage() {
    if (this.billOfMaterialModel?.isDealerCall) {
      this.router.navigate(['/customer', 'manage-customers', 'dealer-call'], {
        queryParams: {
          // customerId: this.installationId
          customerId: this.customerId,
          customerAddressId: this.customerAddressId,
          initiationId: this.installationId,
          appointmentId: this.appointmentId ? this.appointmentId : null,
          callType: this.callType
        }, skipLocationChange: true
      });
    } else {
      this.router.navigate(['/customer', 'manage-customers', 'call-initiation'], {
        queryParams: {
          // customerId: this.installationId
          customerId: this.customerId,
          customerAddressId: this.customerAddressId,
          initiationId: this.installationId,
          appointmentId: this.appointmentId ? this.appointmentId : null,
          callType: this.callType
        }, skipLocationChange: true
      });
    }
  }

  checkAll(event?) {
    let formArray = this.billOfMaterialItemsFormArray;
    if (event.checked) {
      formArray['controls'].map(element => element.get('checkBillOfMaterial').setValue(true));
    } else {
      formArray['controls'].forEach(element => {
        formArray['controls'].map(element => element.get('checkBillOfMaterial').setValue(false));
      });
    }
  }

  getCheckedItems() {
    const selectedItems = this.billOfMaterialItemsFormArray['controls'].map(element => element.get('checkBillOfMaterial').value == true);
    return selectedItems?.every(el => el == true) && this.billOfMaterialItemsFormArray?.controls?.length;
  }

  removeStock() {
    let formArray = this.billOfMaterialItemsFormArray;
    let ids = [];
    ids = formArray.controls.filter(d => d.get('checkBillOfMaterial').value).map(d => d.get('itemId').value);
    if (ids.length <= 0)
      return;
    if (this.callType == '1' && !this.billOfMaterialModel?.isDealerCall) { // Installation
      // let ids = [];
      // ids = formArray.controls.filter(d => d.get('checkBillOfMaterial').value).map(d => d.get('itemId').value);
      // if (ids.length <= 0)
      //   return;

      let messageToDisplay = '';
      if (this.selectAll.checked)
        messageToDisplay = 'Are you sure you want to remove all items?';
      else
        messageToDisplay = 'Are you sure you want to remove the selected items?';

      const dialogReff = this.dialog.open(BillOfMaterialValidationModalComponent, {
        width: '450px',
        data: {
          buttons: {
            cancel: 'No',
            create: 'Yes'
          },
          message: messageToDisplay,
          header: 'Remove Stock',
        },
        disableClose: true
      });
      dialogReff.afterClosed().subscribe(result => {
        if (!result) { // No
          let event = {};
          event['checked'] = false;
          this.checkAll(event);
          if (this.selectAll.checked) {
            this.selectAll.checked = false;
          }
          return;
        } else { // Yes
          if (this.billOfMaterialModel['billOfMaterialId']) {
            this.crudService.get(
              ModulesBasedApiSuffix.TECHNICIAN,
              TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_REMOVE_VALIDATION,
              undefined,
              false,
              prepareGetRequestHttpParams('', '', {
                BillOfMaterialId: this.billOfMaterialModel['billOfMaterialId'],
                ItemIds: ids.join(',')
              })).subscribe(response => {
                this.rxjsService.setGlobalLoaderProperty(false);
                if (response.isSuccess == true && response.statusCode == 200) {
                  if (response.resources.isSuccess) {
                    this.removeStockByLoop(formArray); // this.performRemoveStock(formArray);
                  } else {
                    let dialogData = {};
                    let dialogWidth = '450px';
                    if (response.resources.messageType == '1') {
                      dialogWidth = '500px';
                      dialogData = {
                        buttons: {
                          cancel: 'Cancel',
                          create: 'Sent Stock Return Request'
                        },
                        message: response.resources.validateMessage,
                        header: 'Remove Stock',
                      };
                    } else if (response.resources.messageType == '2') {
                      dialogWidth = '550px';
                      dialogData = {
                        buttons: {
                          cancel: 'Ok'
                        },
                        message: response.resources.validateMessage,
                        header: 'Remove Stock',
                      };
                    } else if (response.resources.messageType == '3') {
                      dialogWidth = '570px';
                      dialogData = {
                        buttons: {
                          cancel: 'Ok'
                        },
                        message: response.resources.validateMessage,
                        header: 'Remove Stock',
                      };
                    }
                    const dialogReff = this.dialog.open(BillOfMaterialValidationModalComponent, {
                      width: dialogWidth,
                      data: dialogData,
                      disableClose: true
                    });
                    dialogReff.afterClosed().subscribe(result => {
                      // if (!result) {
                      let event = {};
                      event['checked'] = false;
                      this.checkAll(event);
                      if (this.selectAll.checked) {
                        this.selectAll.checked = false;
                      }
                      return;
                      // }
                    });
                  }
                }
              });
          } else {
            this.removeStockByLoop(formArray);
          }
        }
      });
    } else {
      // this.performRemoveStock(formArray);
      this.removeStockByLoop(formArray);
    }

  }

  removeStockDialog() {
    if (!this.getBOMActionPermission(PermissionTypes.DELETE)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const formArray = this.billOfMaterialItemsFormArray;
    const ids = formArray?.controls?.filter(d => d.get('checkBillOfMaterial').value).map(d => d.get('itemId').value);
    if (this.getBillOfMaterialsRemove() || ids?.length <= 0) {
      this.snackbarService.openSnackbar("Please select the atleast one item to remove", ResponseMessageTypes.WARNING);
      return;
    }
    let messageToDisplay = '';
    if (this.selectAll.checked) {
      messageToDisplay = 'Are you sure you want to remove all items?';
    } else {
      messageToDisplay = 'Are you sure you want to remove the selected items?';
    }
    const dialogReff = this.dialog.open(BillOfMaterialValidationModalComponent, {
      width: '450px',
      data: {
        buttons: {
          cancel: 'No',
          create: 'Yes'
        },
        message: messageToDisplay,
        header: 'Remove Stock',
      },
      disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (result) {
        this.onAfterRemoveStockCode(formArray, ids);
      } else { // No
        let event = {};
        event['checked'] = false;
        this.checkAll(event);
        if (this.selectAll.checked) {
          this.selectAll.checked = false;
        }
        return;
      }
    });
  }

  onAfterRemoveStockCode(formArray, ids) {
    // if (this.callType == '1') { // Installation
    if (this.billOfMaterialModel['billOfMaterialId']) {
      this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_REMOVE_VALIDATION,
        undefined,
        false,
        prepareGetRequestHttpParams('', '', {
          BillOfMaterialId: this.billOfMaterialModel['billOfMaterialId'],
          ItemIds: ids.join(',')
        })).subscribe(response => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.isSuccess == true && response.statusCode == 200) {
            if (response.resources.isSuccess) {
              this.removeStockByLoop(formArray); // this.performRemoveStock(formArray);
            } else {
              let dialogData = {};
              let dialogWidth = '450px';
              if (response.resources.messageType == '1') {
                dialogWidth = '500px';
                dialogData = {
                  buttons: {
                    cancel: 'Cancel',
                    create: 'Sent Stock Return Request'
                  },
                  message: response.resources.validateMessage,
                  header: 'Remove Stock',
                };
              } else if (response.resources.messageType == '2') {
                dialogWidth = '550px';
                dialogData = {
                  buttons: {
                    cancel: 'Ok'
                  },
                  message: response.resources.validateMessage,
                  header: 'Remove Stock',
                };
              } else if (response.resources.messageType == '3') {
                dialogWidth = '570px';
                dialogData = {
                  buttons: {
                    cancel: 'Ok'
                  },
                  message: response.resources.validateMessage,
                  header: 'Remove Stock',
                };
              }
              const dialogReff = this.dialog.open(BillOfMaterialValidationModalComponent, {
                width: dialogWidth,
                data: dialogData,
                disableClose: true
              });
              dialogReff.afterClosed().subscribe(result => {
                // if (!result) {
                let event = {};
                event['checked'] = false;
                this.checkAll(event);
                if (this.selectAll.checked) {
                  this.selectAll.checked = false;
                }
                return;
                // }
              });
            }
          }
        });
    } else {
      this.removeStockByLoop(formArray);
    }
    // } else {
    //   this.removeStockByLoop(formArray);
    // }
  }

  /*
  performRemoveStock(formArray: FormArray) {
    let discountStockDetails = this.billOfMaterialItemsFormArray.controls.find(element => element.get('itemName').value.toLowerCase() == 'discount warranties');

    // let prCallDetails = this.billOfMaterialItemsFormArray.controls.find(element => element.get('itemName').value.toLowerCase() == 'discounts allowed');

    formArray.controls.forEach(tableElement => {
      if (tableElement.get('checkBillOfMaterial').value && !tableElement.get('IsDeleted').value) {

        if (!tableElement.get('IsWarranty').value) {
          let subTotal = +this.billOfMaterialForm.get('bomInvoice.subTotal').value - +tableElement.get('subTotal').value;
          this.billOfMaterialForm.get('bomInvoice.subTotal').setValue(subTotal.toFixed(2));

          let vatAmount = +this.billOfMaterialForm.get('bomInvoice.vatAmount').value - +tableElement.get('vATAmount').value;
          this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue(vatAmount.toFixed(2));

          let totalAmount = +this.billOfMaterialForm.get('bomInvoice.totalAmount').value - +tableElement.get('totalAmount').value;
          this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue(totalAmount.toFixed(2));
        } else if (tableElement.get('IsWarranty').value) {
          // Greater than condition will never arise because we have validations on pop up that warrantyQty can not be
          // greater than requestQty

          let requestQuantity = tableElement.get('stockType').value.toLowerCase() == 'y' ? +tableElement.get('reserved').value : +tableElement.get('used').value;

          if (requestQuantity == tableElement.get('WarrantyQty').value) { //tableElement.get('request').value
            // subtract from Warranty Discount
            const subTotalDiscountStockDetails: number = Math.abs(discountStockDetails.get('subTotal').value) - tableElement.get('subTotal').value;
            discountStockDetails.get('subTotal').setValue(-Math.abs(subTotalDiscountStockDetails).toFixed(2));

            const vatAmountDiscountStockDetails: number = Math.abs(discountStockDetails.get('vATAmount').value) - tableElement.get('vATAmount').value;
            discountStockDetails.get('vATAmount').setValue(-Math.abs(vatAmountDiscountStockDetails).toFixed(2));

            const totalAmountDiscountStockDetails: number = Math.abs(discountStockDetails.get('totalAmount').value) - tableElement.get('totalAmount').value
            discountStockDetails.get('totalAmount').setValue(-Math.abs(totalAmountDiscountStockDetails).toFixed(2));

          } else {
            // setting values for final row ie. subtract from BOM Invoice
            let remainingQty = requestQuantity - tableElement.get('WarrantyQty').value; // tableElement.get('request').value
            let subTotalForRemainingQty: number = +(Math.abs(remainingQty) * tableElement.get('unitPrice').value.toFixed(2));
            let vatAmountForRemainingQty: number = +(subTotalForRemainingQty * (this.standardTax / 100)).toFixed(2);
            let totalAmountForRemainingQty: number = +(subTotalForRemainingQty + vatAmountForRemainingQty).toFixed(2);

            this.billOfMaterialForm.get('bomInvoice.subTotal').setValue((this.billOfMaterialForm.get('bomInvoice.subTotal').value - subTotalForRemainingQty).toFixed(2));
            this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue((this.billOfMaterialForm.get('bomInvoice.vatAmount').value - vatAmountForRemainingQty).toFixed(2));
            this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue((this.billOfMaterialForm.get('bomInvoice.totalAmount').value - totalAmountForRemainingQty).toFixed(2));

            let qtyConsideredForWarranty = tableElement.get('WarrantyQty').value;
            let subTotalForConsideredQty: number = +(Math.abs(qtyConsideredForWarranty) * tableElement.get('unitPrice').value.toFixed(2));
            let vatAmountForConsideredQty: number = +(subTotalForConsideredQty * (this.standardTax / 100)).toFixed(2);
            let totalAmountForConsideredQty: number = +(subTotalForConsideredQty + vatAmountForConsideredQty).toFixed(2);

            // subtract from Warranty Discount
            discountStockDetails.get('subTotal').setValue(-Math.abs(Math.abs(discountStockDetails.get('subTotal').value) - subTotalForConsideredQty).toFixed(2));
            discountStockDetails.get('vATAmount').setValue(-Math.abs(Math.abs(discountStockDetails.get('vATAmount').value) - vatAmountForConsideredQty).toFixed(2));
            discountStockDetails.get('totalAmount').setValue(-Math.abs(Math.abs(discountStockDetails.get('totalAmount').value) - totalAmountForConsideredQty).toFixed(2));
          }

          let warrantyMaintenanceDetailIndex = this.warrantyMaintenanceDetail.findIndex(control => control.get('itemId').value.toLowerCase() == tableElement.get('itemId').value.toLowerCase());
          if (warrantyMaintenanceDetailIndex >= 0) {
            this.warrantyMaintenanceDetail.splice(warrantyMaintenanceDetailIndex, 1);
          }
          if (this.warrantyMaintenanceDetail.length == 0) {
            // let discountStockDetails1 = this.billOfMaterialItemsFormArray.controls.find(element => element.get('itemName').value.toLowerCase() == 'discount warranties');
            discountStockDetails.get('IsDeleted').setValue(true);
          }
        }
        tableElement.get('IsDeleted').setValue(true);
        tableElement.get('checkBillOfMaterial').setValue(false);
      }
      // else {
      //   tableElement.get('IsDeleted').setValue(false);
      // }
    });
    if (this.selectAll.checked) {
      this.selectAll.checked = false;
    }
  }
  */


  performRemoveStock(formArray: FormArray) {
    let discountStockDetails = this.billOfMaterialItemsFormArray.controls.find(element => element.get('itemName').value.toLowerCase() == 'discount warranties');

    // let prCallDetails = this.billOfMaterialItemsFormArray.controls.find(element => element.get('itemName').value.toLowerCase() == 'discounts allowed');

    formArray.controls.forEach(tableElement => {
      if (tableElement.get('checkBillOfMaterial').value && !tableElement.get('IsDeleted').value) {

        if (!tableElement.get('IsWarranty').value) {
          let subTotal = +this.billOfMaterialForm.get('bomInvoice.subTotal').value - +tableElement.get('subTotal').value;
          this.billOfMaterialForm.get('bomInvoice.subTotal').setValue(subTotal.toFixed(2));
          let vatAmount = +this.billOfMaterialForm.get('bomInvoice.vatAmount').value - +tableElement.get('vATAmount').value;
          this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue(vatAmount.toFixed(2));
          let totalAmount = +this.billOfMaterialForm.get('bomInvoice.totalAmount').value - +tableElement.get('totalAmount').value;
          this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue(totalAmount.toFixed(2));
        } else if (tableElement.get('IsWarranty').value) {
          // Greater than condition will never arise because we have validations on pop up that warrantyQty can not be
          // greater than requestQty
          let requestQuantity = tableElement.get('stockType').value.toLowerCase() == 'y' ? +tableElement.get('reserved').value : +tableElement.get('used').value;
          if (requestQuantity == tableElement.get('WarrantyQty').value) {
            // subtract from Warranty Discount
            const subTotalDiscountStockDetails: number = Math.abs(discountStockDetails.get('subTotal').value) - tableElement.get('subTotal').value;
            discountStockDetails.get('subTotal').setValue(-Math.abs(subTotalDiscountStockDetails).toFixed(2));
            const vatAmountDiscountStockDetails: number = Math.abs(discountStockDetails.get('vATAmount').value) - tableElement.get('vATAmount').value;
            discountStockDetails.get('vATAmount').setValue(-Math.abs(vatAmountDiscountStockDetails).toFixed(2));
            const totalAmountDiscountStockDetails: number = Math.abs(discountStockDetails.get('totalAmount').value) - tableElement.get('totalAmount').value
            discountStockDetails.get('totalAmount').setValue(-Math.abs(totalAmountDiscountStockDetails).toFixed(2));
          } else {
            // setting values for final row ie. subtract from BOM Invoice
            let remainingQty = requestQuantity - tableElement.get('WarrantyQty').value;
            let subTotalForRemainingQty: number = +(Math.abs(remainingQty) * tableElement.get('unitPrice').value.toFixed(2));
            let vatAmountForRemainingQty: number = +(subTotalForRemainingQty * (this.standardTax / 100)).toFixed(2);
            let totalAmountForRemainingQty: number = +(subTotalForRemainingQty + vatAmountForRemainingQty).toFixed(2);

            this.billOfMaterialForm.get('bomInvoice.subTotal').setValue((this.billOfMaterialForm.get('bomInvoice.subTotal').value - subTotalForRemainingQty).toFixed(2));
            this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue((this.billOfMaterialForm.get('bomInvoice.vatAmount').value - vatAmountForRemainingQty).toFixed(2));
            this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue((this.billOfMaterialForm.get('bomInvoice.totalAmount').value - totalAmountForRemainingQty).toFixed(2));

            let qtyConsideredForWarranty = tableElement.get('WarrantyQty').value;
            let subTotalForConsideredQty: number = +(Math.abs(qtyConsideredForWarranty) * tableElement.get('unitPrice').value.toFixed(2));
            let vatAmountForConsideredQty: number = +(subTotalForConsideredQty * (this.standardTax / 100)).toFixed(2);
            let totalAmountForConsideredQty: number = +(subTotalForConsideredQty + vatAmountForConsideredQty).toFixed(2);

            // subtract from Warranty Discount
            discountStockDetails.get('subTotal').setValue(-Math.abs(Math.abs(discountStockDetails.get('subTotal').value) - subTotalForConsideredQty).toFixed(2));
            discountStockDetails.get('vATAmount').setValue(-Math.abs(Math.abs(discountStockDetails.get('vATAmount').value) - vatAmountForConsideredQty).toFixed(2));
            discountStockDetails.get('totalAmount').setValue(-Math.abs(Math.abs(discountStockDetails.get('totalAmount').value) - totalAmountForConsideredQty).toFixed(2));
          }
          let warrantyMaintenanceDetailIndex = this.warrantyMaintenanceDetail.findIndex(control => control.get('itemId').value.toLowerCase() == tableElement.get('itemId').value.toLowerCase());
          if (warrantyMaintenanceDetailIndex >= 0) {
            this.warrantyMaintenanceDetail.splice(warrantyMaintenanceDetailIndex, 1);
          }
          if (this.warrantyMaintenanceDetail.length == 0) {
            discountStockDetails.get('IsDeleted').setValue(true);
          }
        }
        tableElement.get('IsDeleted').setValue(true);
        tableElement.get('checkBillOfMaterial').setValue(false);
      }
    });
    if (this.selectAll.checked) {
      this.selectAll.checked = false;
    }
  }


  // Remove selected items from GRID and pass filtered array to function calculateBomInvoiceByLoop to calculate BOM Invoice
  removeStockByLoop(completeFormArray: FormArray) {
    let filteredArray = [];
    let discountStockDetails = this.billOfMaterialItemsFormArray.controls.find(element => element.get('itemName').value.toLowerCase() == 'discount warranties');
    let prCallDetails = this.billOfMaterialItemsFormArray.controls.find(element => (element.get('itemId').value == this.billOfMaterialModel['prCallDiscountItemId'] && this.billOfMaterialModel['isPRCall']) ||
      (element.get('itemId').value == this.billOfMaterialModel['saveOfferDiscountItemId'] && this.billOfMaterialModel['isSaveOffer']));
    // filteredArray = completeFormArray;
    completeFormArray.controls.forEach(tableElement => {
      // stock code which is selected for removal will have checkBillOfMaterial >> true else >> false
      // if checkBillOfMaterial == false, then push that stock code in filteredArray for final invoice, else dont push.

      // since we are passing filtered array to calculate final invoice so it should be filtered based on call type like
      // PR call, warranty discount etc. bcoz functionalities are different while adding them in Grid

      if (this.billOfMaterialModel['isPRCall'] || this.billOfMaterialModel['isSaveOffer']) { // PR Call
        if (!tableElement?.get('checkBillOfMaterial').value && !tableElement?.get('IsDeleted').value) {
          filteredArray.push(tableElement);
        } else {
          if (!tableElement.get('IsDeleted').value && prCallDetails) {
            const unitPricePRDetails = Math.abs(prCallDetails?.get('unitPrice').value) - tableElement?.get('unitPrice').value;
            prCallDetails?.get('unitPrice').setValue(-Math.abs(unitPricePRDetails).toFixed(2));
            const subTotalPRDetails = Math.abs(prCallDetails?.get('subTotal').value) - tableElement?.get('subTotal').value;
            prCallDetails?.get('subTotal').setValue(-Math.abs(subTotalPRDetails).toFixed(2));
            const vatAmountPRDetails = Math.abs(prCallDetails?.get('vATAmount').value) - tableElement?.get('vATAmount').value;
            prCallDetails?.get('vATAmount').setValue(-Math.abs(vatAmountPRDetails).toFixed(2));
            const totalAmountPRDetails = Math.abs(prCallDetails?.get('totalAmount').value) - tableElement?.get('totalAmount').value;
            prCallDetails?.get('totalAmount').setValue(-Math.abs(totalAmountPRDetails).toFixed(2));
          }
        }
        // } else if(this.isNKACustomer) {

      } else {
        if (tableElement.get('IsWarranty').value) { // Warranty Discount
          if (!tableElement.get('checkBillOfMaterial').value && !tableElement.get('IsDeleted').value) {
            filteredArray.push(tableElement);
          } else if (tableElement.get('checkBillOfMaterial').value) {
            let requestQuantity = tableElement.get('stockType').value.toLowerCase() == 'y' ? +tableElement.get('reserved').value : +tableElement.get('used').value;
            if (discountStockDetails) {
              if (requestQuantity == tableElement.get('WarrantyQty').value) {
                // if requestQuantity is same as WarrantyQty then subtract stock item's all prices from Warranty Discount
                const subTotalDiscountStockDetails = Math.abs(discountStockDetails.get('subTotal').value) - tableElement.get('subTotal').value;
                discountStockDetails.get('subTotal').setValue(-Math.abs(subTotalDiscountStockDetails).toFixed(2));
                const vatAmountDiscountStockDetails = Math.abs(discountStockDetails.get('vATAmount').value) - tableElement.get('vATAmount').value;
                discountStockDetails.get('vATAmount').setValue(-Math.abs(vatAmountDiscountStockDetails).toFixed(2));
                const totalAmountDiscountStockDetails = Math.abs(discountStockDetails.get('totalAmount').value) - tableElement.get('totalAmount').value
                discountStockDetails.get('totalAmount').setValue(-Math.abs(totalAmountDiscountStockDetails).toFixed(2));
              } else {
                // setting values for final row ie. subtract from BOM Invoice
                let qtyConsideredForWarranty = tableElement.get('WarrantyQty').value;
                let subTotalForConsideredQty = +(Math.abs(qtyConsideredForWarranty) * tableElement.get('unitPrice').value).toFixed(2);
                let vatAmountForConsideredQty = +(subTotalForConsideredQty * (this.standardTax / 100)).toFixed(2);
                let totalAmountForConsideredQty = +(subTotalForConsideredQty + vatAmountForConsideredQty).toFixed(2);

                // subtract from Warranty Discount
                discountStockDetails.get('subTotal').setValue(-Math.abs(Math.abs(discountStockDetails.get('subTotal').value) - subTotalForConsideredQty).toFixed(2));
                discountStockDetails.get('vATAmount').setValue(-Math.abs(Math.abs(discountStockDetails.get('vATAmount').value) - vatAmountForConsideredQty).toFixed(2));
                discountStockDetails.get('totalAmount').setValue(-Math.abs(Math.abs(discountStockDetails.get('totalAmount').value) - totalAmountForConsideredQty).toFixed(2));
              }
            }
            // If checked and IsWarranty is true, then remove stock code form warrantyMaintenanceDetailIndex array, and at // the end if warrantyMaintenanceDetailIndex lengh is zero, remove Discount Warranty from grid.
            let warrantyMaintenanceDetailIndex = this.warrantyMaintenanceDetail.findIndex(control => control.get('itemId').value.toLowerCase() == tableElement.get('itemId').value.toLowerCase());
            if (warrantyMaintenanceDetailIndex >= 0) {
              this.warrantyMaintenanceDetail.splice(warrantyMaintenanceDetailIndex, 1);
            }
            if (this.warrantyMaintenanceDetail.length == 0 && discountStockDetails) {
              discountStockDetails.get('IsDeleted').setValue(true);
            }
          }
        } else { // Normal Call
          if (!tableElement.get('checkBillOfMaterial').value && !tableElement.get('IsDeleted').value) {
            filteredArray.push(tableElement);
          }
        }
      }

      // if checkBillOfMaterial == true, then set isDeleted to true and checkBillOfMaterial to false
      if (tableElement.get('checkBillOfMaterial').value) {
        tableElement.get('IsDeleted').setValue(true);
        tableElement.get('checkBillOfMaterial').setValue(false, { emitEvent: false });
      }
    });

    this.calculateBomInvoiceByLoop(filteredArray);
    if (this.selectAll.checked) {
      this.selectAll.checked = false;
    }
  }

  getBillOfMaterialsRemove() {
    const arr = [];
    this.billOfMaterialItemsFormArray?.getRawValue().forEach(el => {
      if (el?.checkBillOfMaterial) {
        arr.push(el?.checkBillOfMaterial);
        // if (el?.IsDeleted) {
        //   arr.push(el?.IsDeleted);
      }
      return el;
    })
    // return arr?.length ? arr?.every(el => el == true) : false;
    return arr?.length ? false : true;
  }

  getBillOfMaterialsDelete() {
    const arr = [];
    this.billOfMaterialItemsFormArray?.getRawValue().forEach(el => {
      if (el?.IsDeleted) {
        arr.push(el?.IsDeleted);
      }
      return el;
    })
    return arr?.length ? arr?.every(el => el == true) : false;
  }

  calculateBomInvoiceByLoop(filteredArray) {
    let invoiceObject = this.createFinalInvoice(filteredArray);
    this.billOfMaterialForm.get('bomInvoice.subTotal').setValue(invoiceObject?.subTotalExclAmount?.toString() ? this.otherService.transformDecimal(invoiceObject?.subTotalExclAmount) : 0);
    this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue(invoiceObject.vatAmount?.toString() ? this.otherService.transformDecimal(invoiceObject?.vatAmount) : 0);
    this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue(invoiceObject.totalExclAmount?.toString() ? this.otherService.transformDecimal(invoiceObject?.totalExclAmount) : 0);
  }

  createFinalInvoice(filteredArray) {
    let subTotalExclAmount = 0;
    let vatAmount = 0;
    let totalExclAmount = 0;
    filteredArray.forEach(stockItemElement => {
      subTotalExclAmount += +stockItemElement.get('subTotal').value;
      vatAmount += +stockItemElement.get('vATAmount').value;
      totalExclAmount += +stockItemElement.get('totalAmount').value;
    });
    return {
      subTotalExclAmount: subTotalExclAmount.toFixed(2),
      vatAmount: vatAmount.toFixed(2),
      totalExclAmount: totalExclAmount.toFixed(2)
    }
  }

  filterArray(event) {
    if (event.checked) {
      if (event.source._elementRef.nativeElement.textContent.trim() && event.source._elementRef.nativeElement.textContent.trim().indexOf('Stock') > -1 && this.selectedCheckBox.indexOf('y') < 0) {
        this.selectedCheckBox += 'y';
        this.selectedCheckBoxBOMArray.push(...this.billOfMaterialItemsFormArray.controls.filter(element => element.get('stockType').value.toLowerCase() == 'y'));
      } else if (event.source._elementRef.nativeElement.textContent.trim() && event.source._elementRef.nativeElement.textContent.trim().indexOf('Discount') > -1 && this.selectedCheckBox.indexOf('m') < 0) {
        this.selectedCheckBox += 'm';
        this.selectedCheckBoxBOMArray.push(...this.billOfMaterialItemsFormArray.controls.filter(element => element.get('stockType').value.toLowerCase() == 'm'));
      }
    } else {
      this.selectedCheckBoxBOMArray = [];
      if (event.source._elementRef.nativeElement.textContent.trim() && event.source._elementRef.nativeElement.textContent.trim().indexOf('Stock') > -1 && this.selectedCheckBox.indexOf('m') > -1) {
        this.selectedCheckBox = 'm';
        this.selectedCheckBoxBOMArray.push(...this.billOfMaterialItemsFormArray.controls.filter(element => element.get('stockType').value.toLowerCase() == 'm'));
      } else if (event.source._elementRef.nativeElement.textContent.trim() && event.source._elementRef.nativeElement.textContent.trim().indexOf('Discount') > -1 && this.selectedCheckBox.indexOf('y') > -1) {
        this.selectedCheckBox = 'y';
        this.selectedCheckBoxBOMArray.push(...this.billOfMaterialItemsFormArray.controls.filter(element => element.get('stockType').value.toLowerCase() == 'y'));
      } else {
        this.selectedCheckBox = '';
        this.selectedCheckBoxBOMArray.push(...this.billOfMaterialItemsFormArray.controls);
      }
    }
    this.updateTotalValues();
  }

  updateTotalValues() {
    this.billOfMaterialForm.get('bomInvoice.subTotal').setValue('R 0.00');
    this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue('R 0.00');
    this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue('R 0.00');
    this.selectedCheckBoxBOMArray.forEach(element => {
      this.billOfMaterialForm.get('bomInvoice.subTotal').setValue(+this.billOfMaterialForm.get('bomInvoice.subTotal').value + element.get('subTotal').value);
      this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue(+this.billOfMaterialForm.get('bomInvoice.vatAmount').value + element.get('vATAmount').value);
      this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue(+this.billOfMaterialForm.get('bomInvoice.totalAmount').value + element.get('totalAmount').value);
    });
  }

  searchStockCodeBySerialNumber() {
    if (!this.getBOMActionPermission(PermissionTypes.ADD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.serialNumberError = false;
    let stockCodesData = this.billOfMaterialItemsFormArray;
    this.billOfMaterialForm.get('stockCodeSerialNumber').setValidators([Validators.required, Validators.minLength(4)]);
    this.billOfMaterialForm.get('stockCodeSerialNumber').updateValueAndValidity();
    if (this.billOfMaterialForm.get('stockCodeSerialNumber').invalid) {
      this.billOfMaterialForm.get('stockCodeSerialNumber').markAllAsTouched();
      return;
    }

    if (!this.billOfMaterialModel['technicianId']) {
      this.snackbarService.openSnackbar('Assign technician for this call', ResponseMessageTypes.WARNING);
      return;
    }

    for (let i = 0; i < this.goodsReturnData.length; i++) {
      if (this.goodsReturnData[i].hasOwnProperty('goodsReturnsItemsDetails') && this.goodsReturnData[i]['goodsReturnsItemsDetails'].length > 0) {
        let goodsReturnsItem = this.goodsReturnData[i]['goodsReturnsItemsDetails'].find(goodsReturnsItemsData => goodsReturnsItemsData['serialNumber'].toLowerCase() == this.billOfMaterialForm.get('stockCodeSerialNumber').value.toLowerCase() && goodsReturnsItemsData['goodsReturnItemDetailId']);
        if (goodsReturnsItem) {
          this.snackbarService.openSnackbar(`You can't add this serial number because GRV already completed in this serial number ${this.billOfMaterialForm.get('stockCodeSerialNumber').value}`, ResponseMessageTypes.WARNING)
          return;
        }
      }
    }
    let api = this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEARCH_SERIAL_NUMBER,
      undefined,
      false,
      prepareGetRequestHttpParams('', '', {
        UserId: this.billOfMaterialModel['technicianId'], //'84894142-b90b-43fb-8e54-fd865b1a49b2',
        DistrictId: this.billOfMaterialModel['districtId'],
        SerialNumber: this.billOfMaterialForm.get('stockCodeSerialNumber').value,
        CallInitiationId: this.callInitiationId
      })
    )
    if (this.billOfMaterialModel?.isDealerCall) {
      api = this.crudService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.DEALER_BILL_OF_MATERIAL_VALIDATE_SERIAL_NUMBER,
        undefined,
        false,
        prepareGetRequestHttpParams('', '', {
          SerialNumber: this.billOfMaterialForm.get('stockCodeSerialNumber').value,
          CallInitiationId: this.callInitiationId
        })
      )
    }
    api.subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response?.resources?.isSuccess) {
        if (response.isSuccess == true && response.statusCode == 200 && response.hasOwnProperty('resources') && response.resources) {
          let stockData = response.resources;
          this.searchScannedItemResponse = response.resources;
          let stockDetails = stockCodesData.controls.find(element => element.get('itemCode').value.toLowerCase() == stockData['itemCode'].toLowerCase());
          let stockIndex = stockCodesData.controls.findIndex((element, index) => element.get('itemCode').value.toLowerCase() == stockData['itemCode'].toLowerCase());
          if (stockDetails) {
            if (stockDetails.value.hasOwnProperty('billOfMaterialItemDetails') && stockDetails.value['billOfMaterialItemDetails'].length >= 0) {
              let serialNumberData = (<FormArray>stockDetails.get('billOfMaterialItemDetails')).controls.find(control => control.get('serialNumber').value.toLowerCase() === this.billOfMaterialForm.get('stockCodeSerialNumber').value.toLowerCase());
              const findAlreadyExistSerialNumber = this.serialNumbersData?.find(el => this.billOfMaterialForm.get('stockCodeSerialNumber').value);
              if (serialNumberData && +stockDetails?.get('reserved').value > 0 && findAlreadyExistSerialNumber) {
                // this.serialNumberError = true;
                this.removeAlreadyScanSerialNumberLocally(stockIndex);
                return;
              }
            }
            if (stockDetails.value.hasOwnProperty('billOfMaterialItemDetails') && stockDetails.value['billOfMaterialItemDetails'].length >= 0) {
              let serialNumberData = (<FormArray>stockDetails.get('billOfMaterialItemDetails')).controls.find(control => control.get('serialNumber').value.toLowerCase() === this.billOfMaterialForm.get('stockCodeSerialNumber').value.toLowerCase());
              if (serialNumberData) {
                // this.serialNumberError = true;
                return;
              } else {
                let serialNumberFormGroup = this.createSerialNumberFormGroupToAddInGrid();
                serialNumberFormGroup.get('billOfMaterialItemId').setValue(stockDetails.get('billOfMaterialItemId').value);
                (<FormArray>stockDetails.get('billOfMaterialItemDetails')).push(serialNumberFormGroup);
              }
            } else {
              let billOfMaterialItemDetails = this.formBuilder.array([]);
              (<FormGroup>stockDetails).addControl('billOfMaterialItemDetails', billOfMaterialItemDetails);
              let serialNumberFormGroup = this.createSerialNumberFormGroupToAddInGrid();
              (<FormArray>stockDetails.get('billOfMaterialItemDetails')).push(serialNumberFormGroup);
            }
            if (((this.callType == '1' || this.isUpselling) && !this.billOfMaterialModel?.isDealerCall) && +stockDetails.get('request').value <= 0 && +stockDetails.get('transfer').value <= 0) {
              this.isScanServiceCallDialog = true;
              return;
            }
            if (stockDetails.get('stockType').value.toLowerCase() == 'y') {
              stockDetails.get('reserved').setValue(stockDetails.get('reserved').value ? (+stockDetails.get('reserved').value + 1) : 1);
              if (+stockDetails.get('transfer').value > 0) {
                stockDetails.get('transfer').setValue(+stockDetails.get('transfer').value - 1);
              } else if (+stockDetails.get('request').value > 0) {
                stockDetails.get('request').setValue(+stockDetails.get('request').value - 1);
              }
            } else if (stockDetails.get('stockType').value.toLowerCase() == 'm') {
              stockDetails.get('used').setValue(stockDetails.get('used').value ?
                (+stockDetails.get('used').value + 1) : 1);
              if (+stockDetails.get('transfer').value > 0) {
                stockDetails.get('transfer').setValue(+stockDetails.get('transfer').value - 1);
              } else if (+stockDetails.get('request').value > 0) {
                stockDetails.get('request').setValue(+stockDetails.get('request').value - 1);
              }
              stockDetails.get('subTotal').setValue(stockDetails.get('unitPrice').value);
              let stockvatAmount = +stockDetails.get('subTotal').value * (this.standardTax / 100);
              stockDetails.get('vATAmount').setValue(stockvatAmount.toFixed(2));
              let totalAmount = +stockDetails.get('subTotal').value + +stockDetails.get('vATAmount').value;
              stockDetails.get('totalAmount').setValue(totalAmount.toFixed(2));
            }
            this.billOfMaterialItemsFormArray.controls[stockIndex].patchValue(
              stockDetails.value
            )
          } else {
            let stockCodeBySerialNumberFormGroup = this.createFormGroupToAddInGrid();
            stockCodeBySerialNumberFormGroup.get('stockTypeId').setValue(stockData['stockTypeId']);
            stockCodeBySerialNumberFormGroup.get('stockType').setValue(stockData['stockType']);
            stockCodeBySerialNumberFormGroup.get('itemId').setValue(stockData['itemId']);
            stockCodeBySerialNumberFormGroup.get('itemCode').setValue(stockData['itemCode']);
            stockCodeBySerialNumberFormGroup.get('itemName').setValue(stockData['itemName']);
            stockCodeBySerialNumberFormGroup.get('unitPrice').setValue(stockData['costPrice']);
            stockCodeBySerialNumberFormGroup.get('itemPricingConfigId').setValue(stockData['itemPricingConfigId']);
            // stockCodeBySerialNumberFormGroup.get('itemPricingConfigId').setValue(stockData['itemPricingConfigId']);

            if (stockCodeBySerialNumberFormGroup.get('stockType').value.toLowerCase() == 'y') {
              stockCodeBySerialNumberFormGroup.get('reserved').setValue(stockCodeBySerialNumberFormGroup.get('reserved').value ? (+stockCodeBySerialNumberFormGroup.get('reserved').value + 1) : 1);
              if (+stockCodeBySerialNumberFormGroup.get('transfer').value > 0) {
                stockCodeBySerialNumberFormGroup.get('transfer').setValue(+stockCodeBySerialNumberFormGroup.get('transfer').value - 1);
              } else if (+stockCodeBySerialNumberFormGroup.get('request').value > 0) {
                stockCodeBySerialNumberFormGroup.get('request').setValue(+stockCodeBySerialNumberFormGroup.get('request').value - 1);
              }
            } else if (stockCodeBySerialNumberFormGroup.get('stockType').value.toLowerCase() == 'm') {
              stockCodeBySerialNumberFormGroup.get('used').setValue(stockCodeBySerialNumberFormGroup.get('used').value ?
                (+stockCodeBySerialNumberFormGroup.get('used').value + 1) : 1);
              if (+stockCodeBySerialNumberFormGroup.get('transfer').value > 0) {
                stockCodeBySerialNumberFormGroup.get('transfer').setValue(+stockCodeBySerialNumberFormGroup.get('transfer').value - 1);
              } else if (+stockCodeBySerialNumberFormGroup.get('request').value > 0) {
                stockCodeBySerialNumberFormGroup.get('request').setValue(+stockCodeBySerialNumberFormGroup.get('request').value - 1);
              }
            }
            stockCodeBySerialNumberFormGroup.get('subTotal').setValue(stockCodeBySerialNumberFormGroup.get('unitPrice').value);
            let vatAmount = +stockCodeBySerialNumberFormGroup.get('subTotal').value * (this.standardTax / 100);
            stockCodeBySerialNumberFormGroup.get('vATAmount').setValue(vatAmount.toFixed(2));
            let totalAmount = +stockCodeBySerialNumberFormGroup.get('subTotal').value + +stockCodeBySerialNumberFormGroup.get('vATAmount').value;
            stockCodeBySerialNumberFormGroup.get('totalAmount').setValue(totalAmount.toFixed(2));
            let differenceAmount = this.otherService.transformCurrToNum(this.billOfMaterialForm.get('differenceAmount').value);

            // if installation call, if current stock item's amount + previous total amount > target order amount then show
            // warning and dont allow to add stock item in grid else allow it to add
            // this only applicable to installation call
            if ((this.callType == '1' || this.isUpselling) && !this.billOfMaterialModel?.isDealerCall && (Math.abs(stockCodeBySerialNumberFormGroup.get('totalAmount').value) + +this.billOfMaterialForm.get('bomInvoice.totalAmount').value) > +this.billOfMaterialForm.get('targetOrderAmount').value) {
              // this.snackbarService.openSnackbar('Total Amount can not exceed Target Order Amount', ResponseMessageTypes.WARNING)
              this.isScanServiceCallDialog = true;
              return;
            }
            // conditions start
            if (this.billOfMaterialModel['isPRCall'] == true || this.billOfMaterialModel['isSaveOffer'] == true) {
              if (+stockCodeBySerialNumberFormGroup.get('subTotal').value > Math.abs(differenceAmount)) {
                this.openDoaReqMsgDialog = true;
                this.PrStockItemsFormArray.clear();
                let PrStockItem = this.formBuilder.group({
                  itemId: stockCodeBySerialNumberFormGroup.get('itemId').value,
                  stockCode: stockCodeBySerialNumberFormGroup.get('itemCode').value,
                  stockDesc: stockCodeBySerialNumberFormGroup.get('itemName').value,
                  stockQty: 1,
                  stockUnitPriceExVat: this.otherService.transformDecimal(stockCodeBySerialNumberFormGroup.get('unitPrice').value),
                  stockSubTotalExVat: this.otherService.transformDecimal(stockCodeBySerialNumberFormGroup.get('subTotal').value),
                  stockSubTotalIncVat: this.otherService.transformDecimal(stockCodeBySerialNumberFormGroup.get('vATAmount').value),
                  ItemPricingConfigId: stockCodeBySerialNumberFormGroup.get('itemPricingConfigId').value
                })
                PrStockItem.get('stockQty').valueChanges.subscribe((val) => {
                  let subTotal = val * this.otherService.transformCurrToNum(PrStockItem.get('stockUnitPriceExVat').value);
                  let vatAmount = subTotal * (this.standardTax / 100);
                  let totalAmount = subTotal + vatAmount;
                  PrStockItem.controls['stockSubTotalExVat'].patchValue(this.otherService.transformDecimal(subTotal));
                  PrStockItem.controls['stockSubTotalIncVat'].patchValue(this.otherService.transformDecimal(totalAmount)); // vatAmount
                  this.prCallRequestForm.controls['totalStockItems'].setValue('R 0.00');
                  let total = 0
                  this.PrStockItemsFormArray.value.forEach((item) => {
                    total += this.otherService.transformCurrToNum(item.stockSubTotalIncVat);
                    this.prCallRequestForm.controls['totalStockItems'].setValue(this.otherService.transformDecimal(+total));
                  })
                })
                this.PrStockItemsFormArray.push(PrStockItem)
                this.prCallRequestForm.controls['totalStockItems'].setValue(this.otherService.transformDecimal(+totalAmount)); // vatAmount
              } else {
                // let prCallDetails = stockCodesData.controls.find(element => element.get('itemName').value.toLowerCase() == 'discounts allowed');
                let prCallDetails = stockCodesData.controls.find(element => (element.get('itemId').value == this.billOfMaterialModel['prCallDiscountItemId'] && this.billOfMaterialModel['isPRCall']) ||
                  (element.get('itemId').value == this.billOfMaterialModel['saveOfferDiscountItemId'] && this.billOfMaterialModel['isSaveOffer']));
                if (prCallDetails) {
                  const unitPricePRDetails = Math.abs(prCallDetails.get('unitPrice').value) + +stockCodeBySerialNumberFormGroup.get('unitPrice').value;
                  prCallDetails.get('unitPrice').setValue(-Math.abs(unitPricePRDetails).toFixed(2));
                  const subTotalPRDetails = Math.abs(prCallDetails.get('subTotal').value) + +stockCodeBySerialNumberFormGroup.get('subTotal').value;
                  prCallDetails.get('subTotal').setValue(-Math.abs(subTotalPRDetails).toFixed(2));
                  const vatAmountPRDetails = Math.abs(prCallDetails.get('vATAmount').value) + +stockCodeBySerialNumberFormGroup.get('vATAmount').value;
                  prCallDetails.get('vATAmount').setValue(-Math.abs(vatAmountPRDetails).toFixed(2));
                  const totalAmountPRDetails = Math.abs(prCallDetails.get('totalAmount').value) + +stockCodeBySerialNumberFormGroup.get('totalAmount').value
                  prCallDetails.get('totalAmount').setValue(-Math.abs(totalAmountPRDetails).toFixed(2));
                  this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue('R 0.00');
                  this.sortStockCodes(stockCodeBySerialNumberFormGroup);
                  // this.billOfMaterialForm.get('stockCodeSerialNumber').reset('', { emitEvent: false });
                }
              }
            } else if (this.isNKACustomer && +stockCodeBySerialNumberFormGroup.get('subTotal').value > Math.abs(differenceAmount)) { // NKA customer
              if (!this.billOfMaterialModel?.billOfMaterialId) {
                this.snackbarService.openSnackbar("Please save the bill of materials first", ResponseMessageTypes.WARNING);
                return;
              }
              this.crudService.get(
                ModulesBasedApiSuffix.TECHNICIAN,
                TechnicalMgntModuleApiSuffixModels.NKA_SERVICE_CALL_PURCHASE_ORDER,
                null, null, prepareRequiredHttpParams({ callInitiationId: this.billOfMaterialModel.callInitiationId }))
                .subscribe((response: IApplicationResponse) => {
                  if (response?.isSuccess && response?.statusCode == 200) {
                    let data = {
                      stockType: this.stockCodeDescriptionForm?.get('stockType')?.value,
                      billOfMaterialModel: this.billOfMaterialModel,
                      tax: this.standardTax,
                      userId: this.userData?.userId,
                      newData: {
                        NKAPurchaseOrderRequestItemId: null,
                        NKAPurchaseOrderRequestId: null,
                        itemId: stockCodeBySerialNumberFormGroup?.get('itemId').value,
                        itemCode: stockCodeBySerialNumberFormGroup?.get('itemCode').value,
                        itemName: stockCodeBySerialNumberFormGroup?.get('itemName').value,
                        quantity: 1,
                        unitPriceExclVat: stockCodeBySerialNumberFormGroup?.get('unitPrice').value,
                        subTotalExclVat: stockCodeBySerialNumberFormGroup.get('subTotal').value,
                        subTotalInclVat: totalAmount, //vatAmount,
                        itemPricingConfigId: stockCodeBySerialNumberFormGroup?.get('itemPricingConfigId').value
                      },
                      response: response?.resources,
                    }
                    this.openNKADoaRequestDialog(data);
                  }
                  this.rxjsService.setGlobalLoaderProperty(false);
                })
            }
            else {
              if (stockData['itemName'].toLowerCase() == 'servicing discount technical') {
                stockCodeBySerialNumberFormGroup = this.performServicingDiscountTechnicalOperation(stockCodeBySerialNumberFormGroup);
              }
              if (!this.billOfMaterialModel['isPRCall'] && !this.billOfMaterialModel['isPRcallSaveOfferMaxCount']) {
                // Discount Warranties starts
                this.crudService.get(
                  ModulesBasedApiSuffix.TECHNICIAN,
                  TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_WARRANTY_CHECK,
                  undefined,
                  false,
                  prepareGetRequestHttpParams('', '', {
                    CallInitiationId: this.callInitiationId,
                    ItemId: stockCodeBySerialNumberFormGroup.get('itemId').value,
                    StockTypeId: stockCodeBySerialNumberFormGroup.get('stockTypeId').value
                  })
                ).subscribe((response: IApplicationResponse) => {
                  this.rxjsService.setGlobalLoaderProperty(false);
                  if (response.isSuccess == true && response.statusCode == 200 && !response?.message) {
                    let discountStockDetails = stockCodesData.controls.find(element => element.get('itemId').value.toLowerCase() == response.resources['itemId'].toLowerCase());
                    if (response.resources['isWarranty'] || response.resources['isDiscountMaintenance']) {
                      stockCodeBySerialNumberFormGroup.get('IsWarranty').setValue(true);
                      stockCodeBySerialNumberFormGroup.get('WarrantyQty').setValue(1);
                      this.warrantyMaintenanceDetail.push(stockCodeBySerialNumberFormGroup);
                    }
                    if (discountStockDetails == undefined && (response.resources['isWarranty'] || response.resources['isDiscountMaintenance'])) {
                      let serialNumberlWarrantyItemFormGroup = this.createFormGroupToAddInGrid();
                      serialNumberlWarrantyItemFormGroup.get('stockTypeId').setValue(response.resources['stockTypeId']);
                      serialNumberlWarrantyItemFormGroup.get('stockType').setValue(response.resources['stockType']);
                      serialNumberlWarrantyItemFormGroup.get('itemId').setValue(response.resources['itemId']);
                      serialNumberlWarrantyItemFormGroup.get('itemCode').setValue(response.resources['itemCode']);
                      serialNumberlWarrantyItemFormGroup.get('itemName').setValue(response.resources['itemName']);
                      serialNumberlWarrantyItemFormGroup.get('unitPrice').setValue(-Math.abs(stockData['costPrice']).toFixed(2));
                      serialNumberlWarrantyItemFormGroup.get('subTotal').setValue(-Math.abs(stockData['costPrice']).toFixed(2));
                      serialNumberlWarrantyItemFormGroup.get('vATAmount').setValue(-Math.abs(vatAmount).toFixed(2));
                      serialNumberlWarrantyItemFormGroup.get('totalAmount').setValue(-Math.abs(totalAmount).toFixed(2));
                      if (stockCodeBySerialNumberFormGroup.get('stockType').value.toLowerCase() == 'y') {
                        serialNumberlWarrantyItemFormGroup.get('reserved').setValue(1);
                        // if(+serialNumberlWarrantyItemFormGroup.get('transfer').value > 0){
                        //   serialNumberlWarrantyItemFormGroup.get('transfer').setValue(+serialNumberlWarrantyItemFormGroup.get('transfer').value - 1);
                        // } else if (+serialNumberlWarrantyItemFormGroup.get('request').value > 0) {
                        //   serialNumberlWarrantyItemFormGroup.get('request').setValue(+serialNumberlWarrantyItemFormGroup.get('request').value - 1);
                        // }
                      } else if (this.stockCodeDescriptionForm.get('stockType').value?.toLowerCase() == 'm') {
                        serialNumberlWarrantyItemFormGroup.get('used').setValue(1);
                      }
                      stockCodesData.push(serialNumberlWarrantyItemFormGroup);
                    } else if (discountStockDetails != undefined && (response.resources['isWarranty'] || response.resources['isDiscountMaintenance'])) {
                      // Related to Discount Warrenties other than 1st time
                      discountStockDetails.get('unitPrice').setValue((-Math.abs(Math.abs(+discountStockDetails.get('unitPrice').value) + +stockCodeBySerialNumberFormGroup.get('unitPrice').value)).toFixed(2));
                      discountStockDetails.get('subTotal').setValue((-Math.abs(Math.abs(+discountStockDetails.get('subTotal').value) + +stockCodeBySerialNumberFormGroup.get('subTotal').value)).toFixed(2));
                      discountStockDetails.get('vATAmount').setValue((-Math.abs(Math.abs(+discountStockDetails.get('vATAmount').value) + +stockCodeBySerialNumberFormGroup.get('vATAmount').value)).toFixed(2));
                      discountStockDetails.get('totalAmount').setValue((-Math.abs(Math.abs(+discountStockDetails.get('totalAmount').value) + +stockCodeBySerialNumberFormGroup.get('totalAmount').value)).toFixed(2));
                    } else {
                      // Not Related to Discount Warrenties but for other stock codes for which isWarranty or isMantainance /// is false
                    }
                  }
                  // add serial number to stock
                  let billOfMaterialItemDetails = this.formBuilder.array([]);
                  stockCodeBySerialNumberFormGroup.addControl('billOfMaterialItemDetails', billOfMaterialItemDetails);
                  let serialNumberFormGroup = this.createSerialNumberFormGroupToAddInGrid();
                  (<FormArray>stockCodeBySerialNumberFormGroup.get('billOfMaterialItemDetails')).push(serialNumberFormGroup);
                  this.sortStockCodes(stockCodeBySerialNumberFormGroup);
                  this.calculateBomInvoiceByLoop(this.billOfMaterialItemsFormArray.controls);
                  // this.billOfMaterialForm.get('stockCodeSerialNumber').reset('', { emitEvent: false });
                });
              }
            }
          }
          if (stockData?.stockType?.toLowerCase() == "y") {
            this.serialNumbersData = [{ serialNo: this.billOfMaterialForm.get('stockCodeSerialNumber').value }];
            if (stockData?.isDecoder) {
              this.openDecoderAddEditDialog();
            } else {
              this.openZoneAddEditDialog();
            }
          }
          // this.updateBillOfMaterial();
        }
      } else if (response?.resources?.isSameBillOfMaterial && response?.resources?.validateMessage) {
        this.removeAlreadyInstalledItem(response?.resources?.isDecoder);
      } else {
        this.snackbarService.openSnackbar(response?.resources?.validateMessage, ResponseMessageTypes.WARNING)
        return;
      }
    });
  }

  removeAlreadyScanSerialNumberLocally(i: number) {
    const message = `Item has been scanned already, Would you like to remove the item from bill of materials?`;
    const dialogData = new ConfirmDialogModel("Notification", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      width: "500px",
      data: {
        ...dialogData,
        isClose: true,
        msgCenter: true,
        isConfirm: true,
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        if (this.callType == '1') {
          this.billOfMaterialItemsFormArray.controls[i].get('transfer').setValue(+this.billOfMaterialItemsFormArray.controls[i].get('transfer').value + 1);
        } else {
          this.billOfMaterialItemsFormArray.controls[i].get('request').setValue(+this.billOfMaterialItemsFormArray.controls[i].get('request').value + 1);
        }
        this.billOfMaterialItemsFormArray.controls[i].get('reserved').setValue(+this.billOfMaterialItemsFormArray.controls[i].get('reserved').value - 1);
        let findIndex = this.serialNumbersData?.findIndex(el => el?.serialNo == this.billOfMaterialForm?.get('stockCodeSerialNumber')?.value);
        this.serialNumbersData?.splice(findIndex, 1);
        let findSerialItemIndex = (<FormArray>(<FormGroup>this.billOfMaterialItemsFormArray.controls[i])?.get('billOfMaterialItemDetails'))?.controls?.findIndex(el => el?.get('serialNumber').value == this.billOfMaterialForm?.get('stockCodeSerialNumber')?.value);
        (<FormArray>(<FormGroup>this.billOfMaterialItemsFormArray.controls[i])?.get('billOfMaterialItemDetails')).removeAt(findSerialItemIndex);
        if (!(<FormArray>(<FormGroup>this.billOfMaterialItemsFormArray.controls[i])?.get('billOfMaterialItemDetails'))?.length) {
          (<FormGroup>this.billOfMaterialItemsFormArray.controls[i]).removeControl('billOfMaterialItemDetails');
        }
        this.billOfMaterialForm?.get('stockCodeSerialNumber')?.setValue('');
        this.billOfMaterialForm?.get('stockCodeSerialNumber')?.markAsUntouched();
      }
    })
  }

  removeAlreadyInstalledItem(isDecoder: boolean) {
    const message = `Item has been scanned already, Would you like to remove the item from bill of materials?`;
    const dialogData = new ConfirmDialogModel("Notification", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      width: "500px",
      data: {
        ...dialogData,
        isClose: true,
        msgCenter: true,
        isConfirm: true,
      },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.removeBillOfMaterialBySerialNo(isDecoder);
      }
    })
  }

  removeZoneData(isDecoder:  boolean) {
    let otherParams = { customerAddressId: this.customerAddressId, serialNo: this.billOfMaterialForm.get('stockCodeSerialNumber').value, ModifiedUserId: this.userData?.userId, customerId: this.customerId };
    let api = this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.ZONE_EQUIPMENT_MAPPING_SERIAL_NUMBER, null,
      prepareRequiredHttpParams(otherParams));
    if (isDecoder) {
      api = this.crudService.delete(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.CUSTOMER_ACCOUNT, null,
        prepareRequiredHttpParams(otherParams));
    }
      api.subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
        }
      })
  }

  removeBillOfMaterialBySerialNo(isDecoder:  boolean) {
    const removeObj = {
      billOfMaterialId: this.billOfMaterialModel['billOfMaterialId'],
      serialNo: this.billOfMaterialForm.get('stockCodeSerialNumber').value,
      modifiedUserId: this.userData?.userId,
    }
    this.crudService.delete(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SERIALNUMBER, null,
      prepareRequiredHttpParams(removeObj))
      .subscribe((res: IApplicationResponse) => {
        if (res?.isSuccess && res?.statusCode == 200) {
          this.getBillOfMaterialData();
          this.removeZoneData(isDecoder);
        }
      })
  }

  openDecoderAddEditDialog() {
    this.rxjsService.setCustomerDate({ customerId: this.customerId });
    this.rxjsService.setCustomerAddresId(this.customerAddressId);
    const editableObject = {
      isShowPartition: true,
      serialNo: this.billOfMaterialForm?.get('stockCodeSerialNumber')?.value,
      callInitiationId: this.callInitiationId,
      technicianId: this.billOfMaterialModel?.technicianId,
      header: 'Decoders',
      customerId: this.customerId, addressId: this.customerAddressId
    }
    const ref = this.dialogService.open(CustomerDecoderAddDialogComponent, {
      showHeader: false,
      baseZIndex: 1000,
      width: '650px',
      data: editableObject
    });
    ref.onClose.subscribe(result => {
      if (result) {
        this.updateBillOfMaterial();
      } else {
        // this.getBillOfMaterialData();
      }
      this.billOfMaterialForm?.get('stockCodeSerialNumber')?.setValue('');
      this.billOfMaterialForm?.get('stockCodeSerialNumber')?.markAsUntouched();
    });
  }

  openZoneAddEditDialog() {
    this.rxjsService.setCustomerDate({ customerId: this.customerId });
    this.rxjsService.setCustomerAddresId(this.customerAddressId);
    const editableObject = {
      isShowPartition: true,
      serialNo: this.billOfMaterialForm?.get('stockCodeSerialNumber')?.value,
      callInitiationId: this.callInitiationId,
      itemId: this.searchScannedItemResponse?.itemId,
      itemOwnershipTypeId: this.searchScannedItemResponse?.itemOwnershipTypeId,
      customerId: this.customerId,
      customerAddressId: this.customerAddressId,
    }
    const ref = this.dialogService.open(CustomerZoneAddEditComponent, { showHeader: false, baseZIndex: 1000, width: '700px', data: editableObject });
    ref.onClose.subscribe(result => {
      if (result) {
        this.updateBillOfMaterial();
      } else {
        // this.getBillOfMaterialData();
      }
      this.billOfMaterialForm?.get('stockCodeSerialNumber')?.setValue('');
      this.billOfMaterialForm?.get('stockCodeSerialNumber')?.markAsUntouched();
    });
  }

  createFormGroupToAddInGrid(): FormGroup {
    return this.formBuilder.group({
      billOfMaterialItemId: '',
      billOfMaterialId: '',
      checkBillOfMaterial: false,
      IsDeleted: false,
      invoice: 0,
      transfer: 0,
      used: 0,
      reserved: 0,
      request: 0,
      subTotal: 0,
      vATAmount: 0,
      totalAmount: 0,
      unitPrice: 0,
      IsWarranty: false,
      WarrantyQty: null,
      stockTypeId: '',
      stockType: '',
      itemId: '',
      itemCode: '',
      itemName: '',
      kitConfigId: null,
      kitTypeName: null,
      systemTypeId: '',
      itemPricingConfigId: '',
      createdUserId: this.userData?.userId,
      modifiedUserId: this.userData?.userId,
      isNotSerialized: false,
    });
  }

  createSerialNumberFormGroupToAddInGrid(): FormGroup {
    return this.formBuilder.group({
      billOfMaterialItemDetailId: null,
      billOfMaterialItemId: null,
      serialNumber: this.billOfMaterialForm.get('stockCodeSerialNumber').value,
      createdUserId: this.userData?.userId,
      modifiedUserId: this.userData?.userId
    });
  }

  // createPRCallFormGroupToAddInGrid(): FormGroup {
  //   return this.formBuilder.group({
  //     itemId: this.stockCodeDescriptionForm.get('itemId').value,
  //     stockCode: this.stockCodeDescriptionForm.get('stockCode').value,
  //     stockDesc: this.stockCodeDescriptionForm.get('stockDescription').value,
  //     stockQty: this.stockCodeDescriptionForm.get('stockRequestedQty').value,
  //     stockUnitPriceExVat: this.stockCodeDescriptionForm.get('stockUnitPriceExcl').value,
  //     stockSubTotalExVat: subTotal,
  //     stockSubTotalIncVat: vatAmount,
  //     ItemPricingConfigId: this.stockCodeDescriptionForm.get('itemPricingConfigId').value
  //   })
  // }

  performServicingDiscountTechnicalOperation(billOfMaterialItemFormGroup): FormGroup {
    // billOfMaterialItemFormGroup.get('unitPrice').valueChanges.subscribe(unitPrice => {
    //   unitPrice = this.otherService.transformCurrToNum(unitPrice);
    //   if (unitPrice > (this.otherService.transformCurrToNum(this.billOfMaterialForm.get('bomInvoice.totalAmount').value) * (this.billOfMaterialModel['technicalDiscountPercentage']) / 100)) {
    //     const dialogReff = this.dialog.open(ServiceForDiscountModalComponent, {
    //       width: '450px',
    //       data: {
    //         header: 'Service Call Discount Approval Request',
    //         createdUserId: this.userData?.userId,
    //         BillofMaterialId: this.billOfMaterialModel['billOfMaterialId'],
    //         DiscountAmountExVat: unitPrice,
    //         DiscountAmountInVat: unitPrice + (unitPrice * this.standardTax / 100),
    //         technicialCallDiscountRequestId: this.billOfMaterialModel['technicialCallDiscountRequestId']
    //       },
    //       disableClose: true
    //     });
    //     dialogReff.afterClosed().subscribe(result => {
    //       if (!result) return;
    //     });
    //   } else {
    //   }
    // });
    billOfMaterialItemFormGroup.get('unitPrice').patchValue(this.stockCodeDescriptionForm.get('stockUnitPriceExcl').value);
    billOfMaterialItemFormGroup.get('subTotal').setValue('R 0.00');
    billOfMaterialItemFormGroup.get('vATAmount').setValue('R 0.00');
    billOfMaterialItemFormGroup.get('totalAmount').setValue('R 0.00');
    return billOfMaterialItemFormGroup;
  }

  sortStockCodes(billOfMaterialFormGroup: FormGroup) {
    let billOfMaterialItemsFormArray = this.billOfMaterialItemsFormArray;
    let stockIndex = -1;
    let startIndex = 0;
    let finalIndex = 0
    if (billOfMaterialFormGroup.value['stockType'].toLowerCase() == 'y') {
      stockIndex = billOfMaterialItemsFormArray.value.map(element => element['stockType'].toLowerCase()).lastIndexOf('y');
      startIndex = 0;
      finalIndex = stockIndex;
    } else {
      stockIndex = billOfMaterialItemsFormArray.value.map(element => element['stockType'].toLowerCase()).indexOf('m');
      // lastIndexOf
      startIndex = stockIndex;
      finalIndex = billOfMaterialItemsFormArray.length == 0 ? 0 : billOfMaterialItemsFormArray.length - 1;
    }
    if (billOfMaterialFormGroup.value['stockType'].toLowerCase() == 'y' && finalIndex < 0) { // finalIndex < startIndex
      // in case of y and no y stock item present initially
      billOfMaterialItemsFormArray.insert(startIndex, billOfMaterialFormGroup);
      return;
    }
    if (billOfMaterialFormGroup.value['stockType'].toLowerCase() == 'm' && startIndex < 0) {
      // in case of m and no m stock item present initially
      billOfMaterialItemsFormArray.insert(finalIndex + 1, billOfMaterialFormGroup);
      return;
    }
    for (startIndex; startIndex <= finalIndex; startIndex++) {
      if (this.otherService.transformCurrToNum(billOfMaterialFormGroup.get('unitPrice').value) > this.otherService.transformCurrToNum(billOfMaterialItemsFormArray.controls[startIndex].get('unitPrice').value)) {
        billOfMaterialItemsFormArray.insert(startIndex, billOfMaterialFormGroup);
        break;
      } else {
        if (startIndex == finalIndex && this.otherService.transformCurrToNum(billOfMaterialFormGroup.get('unitPrice').value) <= this.otherService.transformCurrToNum(billOfMaterialItemsFormArray.controls[startIndex].get('unitPrice').value)) {
          billOfMaterialItemsFormArray.insert((finalIndex + 1), billOfMaterialFormGroup);
        }
      }
    }
  }

  openDiscountWarranties(discountWarrantyFormGroup) {
    if (!this.billOfMaterialModel['billOfMaterialId']) {
      return;
    }
    let stocksAppliedForWarranty = this.billOfMaterialItemsFormArray.controls.filter(element => element.get('IsWarranty').value && !element.get('IsDeleted').value);
    if (stocksAppliedForWarranty.length > 0) {
      const dialogReff = this.dialog.open(DiscountOnWarrantyMaintenanceModalComponent, {
        width: '800px',
        data: {
          header: 'Warranty Applied',
          createdUserId: this.userData?.userId,
          BillofMaterialId: this.billOfMaterialModel['billOfMaterialId'],
          stocksAppliedForWarranty: stocksAppliedForWarranty
        },
        disableClose: true
      });
      dialogReff.afterClosed().subscribe(result => {
        if (!result) return;
        result.forEach(stockWithWarranty => {
          if (stockWithWarranty['isRecordChanged']) {
            let stockData = stocksAppliedForWarranty.find(data => data.get('itemId').value == stockWithWarranty['itemId']);
            stockData.get('WarrantyQty').setValue(stockWithWarranty['WarrantyQty']);

            // let qty = stockData['WarrantyQty'] > 0 ? stockData['WarrantyQty'] : +stockData['request'];
            let qtyConsideredForWarranty: number = stockWithWarranty['previousWarrantyQty'] - stockWithWarranty['WarrantyQty'];
            let subTotalForAllowedWarrantyQty = +(Math.abs(qtyConsideredForWarranty) * stockData.get('unitPrice').value).toFixed(2);
            let vatAmountForAllowedWarrantyQty: number = +(subTotalForAllowedWarrantyQty * (this.standardTax / 100)).toFixed(2);
            let totalAmountForAllowedWarrantyQty: number = +(subTotalForAllowedWarrantyQty + vatAmountForAllowedWarrantyQty).toFixed(2);

            if (qtyConsideredForWarranty > 0) {
              // subtract from Warranty Discount
              const subTotalForAllowedWarranty: number = Math.abs(discountWarrantyFormGroup.get('subTotal').value) - subTotalForAllowedWarrantyQty;
              discountWarrantyFormGroup.get('subTotal').setValue(-Math.abs(subTotalForAllowedWarranty).toFixed(2));

              const vatAmountForAllowedWarranty: number = Math.abs((+discountWarrantyFormGroup.get('vATAmount').value)) - vatAmountForAllowedWarrantyQty;
              discountWarrantyFormGroup.get('vATAmount').setValue(-Math.abs(vatAmountForAllowedWarranty).toFixed(2));

              const totalAmountForAllowedWarranty: number = Math.abs((+discountWarrantyFormGroup.get('totalAmount').value)) - totalAmountForAllowedWarrantyQty;
              discountWarrantyFormGroup.get('totalAmount').setValue(-Math.abs(totalAmountForAllowedWarranty).toFixed(2));

              // setting values for final row ie. Add to BOM Invoice
              const subTotalForAllowedWarrantyBOM: number = (+this.billOfMaterialForm.get('bomInvoice.subTotal').value) + subTotalForAllowedWarrantyQty;
              this.billOfMaterialForm.get('bomInvoice.subTotal').setValue(subTotalForAllowedWarrantyBOM.toFixed(2));

              const vatAmountForAllowedWarrantyBOM: number = (+this.billOfMaterialForm.get('bomInvoice.vatAmount').value) + vatAmountForAllowedWarrantyQty;
              this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue(vatAmountForAllowedWarrantyBOM.toFixed(2));

              const totalAmountForAllowedWarrantyBOM: number = (+this.billOfMaterialForm.get('bomInvoice.totalAmount').value) + totalAmountForAllowedWarrantyQty;
              this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue(totalAmountForAllowedWarrantyBOM.toFixed(2));

            } else if (qtyConsideredForWarranty < 0) {
              // Add to Warranty Discount
              const subTotalForAllowedWarranty: number = Math.abs((+discountWarrantyFormGroup.get('subTotal').value)) + subTotalForAllowedWarrantyQty;
              discountWarrantyFormGroup.get('subTotal').setValue(-Math.abs(subTotalForAllowedWarranty).toFixed(2));

              const vatAmountForAllowedWarranty: number = Math.abs((+discountWarrantyFormGroup.get('vATAmount').value)) + vatAmountForAllowedWarrantyQty;
              discountWarrantyFormGroup.get('vATAmount').setValue(-Math.abs(vatAmountForAllowedWarranty).toFixed(2));

              const totalAmountForAllowedWarranty: number = Math.abs((+discountWarrantyFormGroup.get('totalAmount').value)) + totalAmountForAllowedWarrantyQty;
              discountWarrantyFormGroup.get('totalAmount').setValue(-Math.abs(totalAmountForAllowedWarranty).toFixed(2));

              // setting values for final row ie. subtract from BOM Invoice
              const subTotalForAllowedWarrantyBOM: number = (+this.billOfMaterialForm.get('bomInvoice.subTotal').value) - subTotalForAllowedWarrantyQty;
              this.billOfMaterialForm.get('bomInvoice.subTotal').setValue(subTotalForAllowedWarrantyBOM.toFixed(2));

              const vatAmountForAllowedWarrantyBOM: number = (+this.billOfMaterialForm.get('bomInvoice.vatAmount').value) - vatAmountForAllowedWarrantyQty;
              this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue(vatAmountForAllowedWarrantyBOM.toFixed(2));

              const totalAmountForAllowedWarrantyBOM: number = (+this.billOfMaterialForm.get('bomInvoice.totalAmount').value) - totalAmountForAllowedWarrantyQty;
              this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue(totalAmountForAllowedWarrantyBOM.toFixed(2));
            } else {
              // No change
            }
          }
        });
      });
    }
  }

  calculateLabourCharges() {
    if (!this.getBOMActionPermission(PermissionTypes.EDIT) && this.billOfMaterialModel?.billOfMaterialId) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const dialogRemoveStockCode = this.dialog.open(BillOfMaterialScanModalComponent, {
      width: '450px',
      data: {
        buttons: {
          cancel: 'No',
          create: 'Yes'
        },
        message: 'Are you sure want to calculate the final servicing labour charges?',
        header: 'Labour Charge Confirmation',
      }, disableClose: true
    });

    dialogRemoveStockCode.afterClosed().subscribe(result => {
      if (!result) return;
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_LABOUR_CHARGE,
        undefined,
        false,
        prepareGetRequestHttpParams(null, null, {
          DistrictId: this.billOfMaterialModel['districtId'],
          AppointmentId: this.appointmentId,
          AppointmentLogId: this.appointmentLogId,
          ItemPricingConfigId: this.billOfMaterialModel['itemPricingConfigId']
        })).subscribe(response => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.isSuccess == true && response.statusCode == 200 && response.resources) {
            // Related to Labour Charge
            let billOfMaterialItemsFormArray = this.billOfMaterialItemsFormArray;
            // let stockDetails = billOfMaterialItemsFormArray.value.find(element => element['itemId'] == response.resources.itemId);
            let stockDetails = billOfMaterialItemsFormArray.controls.find(element => element.get('itemId').value == response.resources.itemId);
            if (stockDetails == undefined) {
              let billOfMaterialLabourChargeFormGroup = this.formBuilder.group({
                checkBillOfMaterial: false,
                IsDeleted: false,
                billOfMaterialItemId: '',
                billOfMaterialId: '',
                stockTypeId: response.resources['stockTypeId'],
                stockType: response.resources['stockType'],
                itemId: response.resources['itemId'],
                itemCode: response.resources['itemCode'],
                itemName: response.resources['itemName'],
                kitConfigId: response.resources['kitConfigId'],
                kitTypeName: response.resources['kitTypeName'],
                invoice: 0,
                transfer: 0,
                used: 1,//0,
                reserved: 0,
                request: 0,
                unitPrice: response.resources['unitPrice'].toFixed(2),
                subTotal: response.resources['subTotal'].toFixed(2),
                vATAmount: response.resources['vat'].toFixed(2),
                totalAmount: response.resources['total'].toFixed(2),
                createdUserId: this.userData?.userId,
                modifiedUserId: this.userData?.userId,
                IsWarranty: false,
                WarrantyQty: null
              });
              // billOfMaterialItemsFormArray.push(billOfMaterialLabourChargeFormGroup);
              this.sortStockCodes(billOfMaterialLabourChargeFormGroup);
            } else {
              let bomsubTotal = +this.billOfMaterialForm.get('bomInvoice.subTotal').value - +stockDetails.get('subTotal').value;
              let bomvATAmount = +this.billOfMaterialForm.get('bomInvoice.vatAmount').value - +stockDetails.get('vATAmount').value;
              let bomtotalAmount = +this.billOfMaterialForm.get('bomInvoice.totalAmount').value - +stockDetails.get('totalAmount').value;
              this.billOfMaterialForm.get('bomInvoice.subTotal').setValue(bomsubTotal.toFixed(2));
              this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue(bomvATAmount.toFixed(2));
              this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue(bomtotalAmount.toFixed(2));

              stockDetails.get('used').setValue(1);
              stockDetails.get('unitPrice').setValue(response.resources['unitPrice'].toFixed(2));
              stockDetails.get('subTotal').setValue(response.resources['subTotal'].toFixed(2));
              stockDetails.get('vATAmount').setValue(response.resources['vat'].toFixed(2));
              stockDetails.get('totalAmount').setValue(response.resources['total'].toFixed(2));
            }
            let bomsubTotal = +this.billOfMaterialForm.get('bomInvoice.subTotal').value + +response.resources['subTotal'];
            let bomvATAmount = +this.billOfMaterialForm.get('bomInvoice.vatAmount').value + +response.resources['vat'];
            let bomtotalAmount = +this.billOfMaterialForm.get('bomInvoice.totalAmount').value + +response.resources['total'];
            this.billOfMaterialForm.get('bomInvoice.subTotal').setValue(bomsubTotal.toFixed(2));
            this.billOfMaterialForm.get('bomInvoice.vatAmount').setValue(bomvATAmount.toFixed(2));
            this.billOfMaterialForm.get('bomInvoice.totalAmount').setValue(bomtotalAmount.toFixed(2));
          }
        });
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  onNotSerialized(control, i) {
    if (control?.get('isNotSerialized')?.value == this.billOfMaterialItemsFormArray?.controls[i]?.get('isNotSerialized')?.value && control?.get('isNotSerialized')?.value && control?.get('stockType')?.value?.toLowerCase() == 'y' && this.billOfMaterialModel['technicianId']) {
      return this.billOfMaterialModel?.bomQuotationItems ? (this.billOfMaterialModel?.bomQuotationItems[i]?.request == 0 && this.billOfMaterialModel?.bomQuotationItems[i]?.transfer == 0) : true;
    }
    return true;
  }

  onValidateLightCover() {
    let condtion = false;
    this.billOfMaterialItemsFormArray.getRawValue()?.forEach((el, i) => {
      if (Math.abs(this.otherService.transformCurrToNum(el?.unitPrice)) > this.billOfMaterialModel['availableLightningDiscount'] && this.billOfMaterialModel['lightningDiscountItemId'] == el?.itemId) {
        condtion = true;
      }
    })
    return condtion;
  }

  onValidateNotSerialized() {
    let condtion = false;
    this.billOfMaterialItemsFormArray.getRawValue()?.forEach((el, i) => {
      if ((this.billOfMaterialModel['isPRCall'] && el?.isNotSerialized && el?.reserved > 1) || (el?.isNotSerialized && el?.reserved > 1)) {
        condtion = true;
      }
    })
    return condtion;
  }

  updateBillOfMaterial() {
    if ((!this.getBOMActionPermission(PermissionTypes.EDIT) && this.billOfMaterialModel?.billOfMaterialId) || (!this.getBOMActionPermission(PermissionTypes.ADD) && !this.billOfMaterialModel?.billOfMaterialId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.billOfMaterialItemsFormArray.invalid) {
      this.billOfMaterialItemsFormArray.markAllAsTouched();
      return;
    }
    if (this.onValidateLightCover()) {
      this.snackbarService.openSnackbar(`Please enter lighting cover amount is less than equal to ${this.billOfMaterialModel['availableLightningDiscount']}`,
        ResponseMessageTypes.WARNING);
      return;
    }
    if (this.onValidateNotSerialized()) {
      this.snackbarService.openSnackbar(`Requested quantity has met not allowed add more items`, ResponseMessageTypes.WARNING);
      return;
    }
    this.billOfMaterialForm?.markAsUntouched();
    this.billOfMaterialForm.value.TechnicialCallDiscountRequestApprovalId = this.TechnicialCallDiscountRequestApprovalId

    let submit$ = [];
    this.stockExistError = false;
    const reqBomObj = {
      ...this.billOfMaterialForm.value
    };
    reqBomObj['differenceAmount'] = typeof reqBomObj['differenceAmount'] == 'string' && reqBomObj['differenceAmount'] ? this.otherService.transformCurrToNum(reqBomObj['differenceAmount']) : 0;
    reqBomObj['totalAmount'] = typeof reqBomObj['totalAmount'] == 'string' && reqBomObj['totalAmount'] ? this.otherService.transformCurrToNum(reqBomObj['totalAmount']) : 0;
    reqBomObj['bomInvoice'].subTotal = typeof reqBomObj['bomInvoice'].subTotal == 'string' && reqBomObj['bomInvoice'].subTotal ? this.otherService.transformCurrToNum(reqBomObj['bomInvoice'].subTotal) : 0;
    reqBomObj['bomInvoice'].vatAmount = typeof reqBomObj['bomInvoice'].vatAmount == 'string' && reqBomObj['bomInvoice'].vatAmount ? this.otherService.transformCurrToNum(reqBomObj['bomInvoice'].vatAmount) : 0;
    reqBomObj['bomInvoice'].totalAmount = typeof reqBomObj['bomInvoice'].totalAmount == 'string' && reqBomObj['bomInvoice'].totalAmount ? this.otherService.transformCurrToNum(reqBomObj['bomInvoice'].totalAmount) : 0;
    reqBomObj?.billOfMaterialItems?.map(el => {
      if (el.itemId == this.billOfMaterialModel['lightningDiscountItemId'] || this.getDiscountValueItems(el.itemId)) {
        el.unitPrice = el?.unitPrice && typeof el?.unitPrice == 'string' ? this.otherService.transformCurrToNum(el?.unitPrice) : el?.unitPrice;
        el.subTotal = el?.subTotal && typeof el?.subTotal == 'string' ? this.otherService.transformCurrToNum(el?.subTotal) : el?.subTotal;
        el.vATAmount = el?.vATAmount && typeof el?.vATAmount == 'string' ? this.otherService.transformCurrToNum(el?.vATAmount) : el?.vATAmount;
        el.totalAmount = el?.totalAmount && typeof el?.totalAmount == 'string' ? this.otherService.transformCurrToNum(el?.totalAmount) : el?.totalAmount;
      }
      return el;
    });
    if (this.billOfMaterialModel['billOfMaterialId'] == undefined) {
      submit$.push(this.crudService.create(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL,
        reqBomObj
      ));
    } else {
      submit$.push(this.crudService.update(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL,
        reqBomObj
      ));
    }
    // if(!this.isFeedback) {
    //   submit$.push(this.crudService.create(
    //     ModulesBasedApiSuffix.TECHNICIAN,
    //     TechnicalMgntModuleApiSuffixModels.RATE_OUR_SERVICE_CALL,
    //     {
    //       callInitiationId: this.callInitiationId,
    //       createdUserId: this.userData?.userId,
    //     }
    //   ));
    // }
    forkJoin(submit$).subscribe((response: IApplicationResponse[]) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      response.forEach((respObj: IApplicationResponse, ix: number) => {
        if (respObj.isSuccess) {
          switch (ix) {
            case 0:
              // this.navigateToPage();
              this.getBillOfMaterialData();
              this.billOfMaterialForm.get('differenceAmount').reset();
              this.billOfMaterialForm.get('bomInvoice').reset();
              break;
          }
        }
      });
    });
  }

  isSubmitPrCall() {
    if (!this.getBOMActionPermission(PermissionTypes.EDIT) && this.billOfMaterialModel?.billOfMaterialId) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.prCallRequestForm.invalid) {
      this.prCallRequestForm.markAllAsTouched();
      return;
    }

    let savePRCall = {
      serviceCallRequestId: (this.billOfMaterialModel && this.billOfMaterialModel.serviceCallRequestId) ? this.billOfMaterialModel.serviceCallRequestId : null,
      callInitiationId: this.callInitiationId,
      motivation: this.prCallRequestForm.get('comments').value,
      createdUserId: this.userData?.userId,
      isAddionalApproval: true,
      items: []
    }

    this.prCallRequestForm.value.stockItems.forEach(x => {
      savePRCall.items.push({
        itemId: x.itemId,
        exclVAT: x.stockSubTotalExVat?.toString() ? this.otherService.transformCurrToNum(x.stockSubTotalExVat) : null,
        inclVAT: x.stockSubTotalIncVat?.toString() ? this.otherService.transformCurrToNum(x.stockSubTotalIncVat) : null,
        qtyRequired: x.stockQty,
        ItemPricingConfigId: x.itemPricingConfigId ? x.itemPricingConfigId : x.ItemPricingConfigId ? x.ItemPricingConfigId : null
      })
    })

    if (savePRCall.items && savePRCall.items.length == 0) {
      this.snackbarService.openSnackbar('Atleast one Equipment and Additional Labor must be added', ResponseMessageTypes.ERROR);
    }
    if (this.billOfMaterialModel.isPRCall) {
      this.crudService.create(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.PR_CALL,
        savePRCall
      ).subscribe((response: IApplicationResponse) => {
        // this.navigateToPage();
        if (response.isSuccess) {
          this.openAddContactDialog = false;
          // this.prCallDialog = false;
          this.alertDialog = true
          this.alertMsg = response.resources
          // this.getCallInitiationDetailsById();
          this.rxjsService.setDialogOpenProperty(false);
        }
      });
    }
    else {
      this.crudService.create(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.SAVE_OFFER,
        savePRCall
      ).subscribe((response: IApplicationResponse) => {
        this.navigateToPage();
      });

    }

  }

  onClosePRCall() {
    this.openAddContactDialog = false;
  }

  addEquipmentLabor() {
    if (!this.getBOMActionPermission(PermissionTypes.ADD)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let addEquipmentLaborVal = this.prCallRequestForm.get('Equipment').value;
    if (this.prCallRequestForm.get('Equipment').value) {
      let item = this.filteredPRStockCodes.find(x => x.itemCode == addEquipmentLaborVal);
      let prStockItemsArr = this.PrStockItemsFormArray.getRawValue();
      let filterItem = prStockItemsArr.filter(x => x.stockCode == addEquipmentLaborVal);
      if (filterItem && filterItem.length > 0) {
        this.snackbarService.openSnackbar('Stock Code is already added.', ResponseMessageTypes.WARNING)
        return;
      }
      let PrStockItem = this.formBuilder.group({
        itemId: item.id,
        stockCode: item.itemCode,
        stockDesc: item.displayName,
        stockQty: '',
        stockUnitPriceExVat: this.otherService.transformDecimal(item.costPrice),
        stockSubTotalExVat: 'R 0.00',
        stockSubTotalIncVat: 'R 0.00',
        itemPricingConfigId: item.itemPricingConfigId
      })
      PrStockItem.get('stockQty').valueChanges.subscribe((val) => {
        let subTotal = val * this.otherService.transformCurrToNum(PrStockItem.get('stockUnitPriceExVat').value);
        // billOfMaterialItemFormGroup.get('subTotal').setValue(+subTotal.toFixed(2));

        let vatAmount = subTotal * (this.standardTax / 100);
        // billOfMaterialItemFormGroup.get('vATAmount').setValue(+vatAmount.toFixed(2));

        let totalAmount = subTotal + vatAmount;
        // billOfMaterialItemFormGroup.get('totalAmount').setValue(+totalAmount.toFixed(2));

        PrStockItem.controls['stockSubTotalExVat'].patchValue(this.otherService.transformDecimal(subTotal));
        PrStockItem.controls['stockSubTotalIncVat'].patchValue(this.otherService.transformDecimal(totalAmount)); // vatAmount
        this.prCallRequestForm.controls['totalStockItems'].setValue('R 0.00');
        let total = 0
        this.PrStockItemsFormArray.value.forEach((item) => {
          total += this.otherService.transformCurrToNum(item.stockSubTotalIncVat);
          this.prCallRequestForm.controls['totalStockItems'].setValue(this.otherService.transformDecimal(+total));

        })
      })
      this.PrStockItemsFormArray.push(PrStockItem);
      this.prCallRequestForm.get('Equipment').reset('', { emitEvent: false });
      this.filteredPRStockCodes = [];
    }


  }
  onEquipmentLaborSelected(value) {
    let item = this.filteredPRStockCodes.find(x => x.itemCode == value);
    let PrStockItem = this.formBuilder.group({
      itemId: item.id,
      stockCode: item.itemCode,
      stockDesc: item.displayName,
      stockQty: '',
      stockUnitPriceExVat: this.otherService.transformDecimal(item.costPrice),
      stockSubTotalExVat: 'R 0.00',
      stockSubTotalIncVat: 'R 0.00',
      ItemPricingConfigId: item.ItemPricingConfigId,
    })
    PrStockItem.get('stockQty').valueChanges.subscribe((val) => {
      let subTotal = val * this.otherService.transformCurrToNum(PrStockItem.get('stockUnitPriceExVat').value);
      // billOfMaterialItemFormGroup.get('subTotal').setValue(+subTotal.toFixed(2));

      let vatAmount = subTotal * (this.standardTax / 100);
      // billOfMaterialItemFormGroup.get('vATAmount').setValue(+vatAmount.toFixed(2));

      let totalAmount = subTotal + vatAmount;
      // billOfMaterialItemFormGroup.get('totalAmount').setValue(+totalAmount.toFixed(2));

      PrStockItem.controls['stockSubTotalExVat'].patchValue(this.otherService.transformDecimal(subTotal));
      PrStockItem.controls['stockSubTotalIncVat'].patchValue(this.otherService.transformDecimal(totalAmount)); // vatAmount
      this.prCallRequestForm.controls['totalStockItems'].setValue('R 0.00');
      let total = 0
      this.PrStockItemsFormArray.value.forEach((item) => {
        total += this.otherService.transformCurrToNum(item.stockSubTotalIncVat);
        this.prCallRequestForm.controls['totalStockItems'].setValue(this.otherService.transformDecimal(+total));

      })
    })
    this.PrStockItemsFormArray.push(PrStockItem)
    // this.prCallRequestForm.controls['totalStockItems'].setValue(+vatAmount);
    // let subTotal = billOfMaterialItemFormGroup.get('request').value * billOfMaterialItemFormGroup.get('unitPrice').value;
    //   billOfMaterialItemFormGroup.get('subTotal').setValue(+subTotal.toFixed(2));

    //   let vatAmount = billOfMaterialItemFormGroup.get('subTotal').value * (this.standardTax / 100);
    //   billOfMaterialItemFormGroup.get('vATAmount').setValue(+vatAmount.toFixed(2));

    //   let totalAmount = billOfMaterialItemFormGroup.get('subTotal').value + billOfMaterialItemFormGroup.get('vATAmount').value;
    //   billOfMaterialItemFormGroup.get('totalAmount').setValue(+totalAmount.toFixed(2));


  }

  deleteStockNumbers(index) {
    if (!this.getBOMActionPermission(PermissionTypes.DELETE)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (index == 0) {
      this.isFirstStockNumDel = true;
    }
    else {
      this.isFirstStockNumDel = false;
    }
    let totalval = this.otherService.transformCurrToNum(this.prCallRequestForm.controls['totalStockItems'].value);
    totalval -= this.PrStockItemsFormArray.value[index].stockSubTotalIncVat;
    this.prCallRequestForm.controls['totalStockItems'].setValue(this.otherService.transformDecimal(totalval));
    this.PrStockItemsFormArray.removeAt(index);

  }

  onStatucCodeSelected(value, type) {
    let stockItemData;
    if (type === 'stockCode') {
      stockItemData = this.filteredStockCodes.find(x => x.itemCode === value);
      if (!stockItemData) {

      }
      else {
        this.isStockCodeBlank = true;
        this.stockCodeDescriptionForm.get('stockDescription').setValue(stockItemData.displayName, { emitEvent: false });
        this.stockCodeDescriptionForm.get('stockUnitPriceExcl').setValue(stockItemData.costPrice, { emitEvent: false });
        this.stockCodeDescriptionForm.get('itemId').setValue(stockItemData.id);
        this.stockCodeDescriptionForm.get('stockTypeId').setValue(stockItemData.stockTypeId);
        this.stockCodeDescriptionForm.get('kitConfigId').setValue(stockItemData.kitConfigId);
        this.stockCodeDescriptionForm.get('kitTypeName').setValue(stockItemData.kitTypeName);
        this.stockCodeDescriptionForm.get('systemTypeId').setValue(stockItemData.systemTypeId);
        this.stockCodeDescriptionForm.get('itemPricingConfigId').setValue(stockItemData.itemPricingConfigId);
        this.stockCodeDescriptionForm.get('isNotSerialized').setValue(stockItemData.isNotSerialized);
      }
    } else if (type === 'stockDescription') {
      stockItemData = this.filteredStockDescription.find(x => x.displayName === value);
      if (!stockItemData) {

      }
      else {
        this.isStockDescriptionSelected = true;
        this.stockCodeDescriptionForm.get('stockCode').setValue(stockItemData.itemCode, { emitEvent: false });
        this.stockCodeDescriptionForm.get('stockUnitPriceExcl').setValue(stockItemData.costPrice, { emitEvent: false });
        this.stockCodeDescriptionForm.get('itemId').setValue(stockItemData.id);
        this.stockCodeDescriptionForm.get('stockTypeId').setValue(stockItemData.stockTypeId);
        this.stockCodeDescriptionForm.get('kitConfigId').setValue(stockItemData.kitConfigId);
        this.stockCodeDescriptionForm.get('kitTypeName').setValue(stockItemData.kitTypeName);
        this.stockCodeDescriptionForm.get('systemTypeId').setValue(stockItemData.systemTypeId);
        this.stockCodeDescriptionForm.get('itemPricingConfigId').setValue(stockItemData.itemPricingConfigId);
        this.stockCodeDescriptionForm.get('isNotSerialized').setValue(stockItemData.isNotSerialized);
      }
    }
  }

  openKitDetailsModal(itemCode: any, kitConfigId: any): void {
    if (!itemCode || !kitConfigId) {
      return;
    }
    this.stockCode = itemCode;
    let params = new HttpParams().set('KitConfigId', kitConfigId);
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_STOCK_KIT_DETAILS, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.openKitDetailsDialog = true;
          this.modalDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  openStockSwap() {
    if (!this.getBOMActionPermission(PermissionTypes.EDIT) && this.billOfMaterialModel?.billOfMaterialId) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/customer/stock-swap/stock-swap-request'], {
      queryParams: {
        callInitiationId: this.callInitiationId,
        quotationVersionId: this.quotationVersionId,
        customerId: this.customerId,
        callType: this.callType,
        customerAddressId: this.customerAddressId,
        billOfMaterialId: this.billOfMaterialModel ? this.billOfMaterialModel.billOfMaterialId : null,
        districtId: this.billOfMaterialModel ? this.billOfMaterialModel.districtId : null
      }
    });
  }

  checkInvoiceValidation() {
    if (!this.getBOMActionPermission(PermissionTypes.EDIT) && this.billOfMaterialModel?.billOfMaterialId) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.billOfMaterialModel['technicianId']) {
      this.snackbarService.openSnackbar('Scheduling is required for the job', ResponseMessageTypes.WARNING);
      return;
    }
    // if(!this.billOfMaterialModel['diagnosisId']) {
    //   this.snackbarService.openSnackbar('Diagnosis is required for the job', ResponseMessageTypes.WARNING);
    //   return;
    // }
    if (this.callType == '1' && !this.billOfMaterialModel['isRadio']) {
      this.openConfigDialog({ title: 'Notification', message: `Unable to invoice the job as no radio is installed` });
      return;
    }
    this.rxjsService.setGlobalLoaderProperty(true);
    this.billOfMaterialForm?.markAsUntouched();
    if (this.jobCardAcceptanceSubscription && !this.jobCardAcceptanceSubscription.closed) {
      return;
    }
    this.jobCardAcceptanceSubscription = this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.JOB_CARD_ACCEPTANCE_VALIDATION, undefined, false, prepareRequiredHttpParams({
        CallInitiationId: this.callInitiationId,
        modifiedUserId: this.userData?.userId,
      }))
      .subscribe((res: IApplicationResponse) => {
        if (res.isSuccess == true && res.statusCode == 200) {
          if (res?.resources?.validateMessage && !res?.resources?.isSuccess) {
            this.openConfigDialog({ title: 'Notification', message: res?.resources?.validateMessage });
            this.rxjsService.setGlobalLoaderProperty(false);
            return;
          } else if (this.billOfMaterialModel?.isDealerCall && res?.resources?.isSuccess && res?.resources?.validateMessage) {
            this.snackbarService.openSnackbar(res?.resources?.validateMessage, ResponseMessageTypes.SUCCESS);
            this.navigateToPage();
            this.rxjsService.setGlobalLoaderProperty(false);
            return;
          }
          this.rxjsService.setGlobalLoaderProperty(true);
          this.crudService.get(
            ModulesBasedApiSuffix.TECHNICIAN,
            TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_INVOICE_VALIDATION,
            undefined,
            false,
            prepareGetRequestHttpParams('', '', {
              BillOfMaterialId: this.billOfMaterialModel['billOfMaterialId']
            })
          ).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess == true && response.statusCode == 200) {
              if (response.resources.isSuccess) {
                // call RATE_OUR_SERVICE_CALL before navigating
                if (!this.isFeedback) {
                  this.onDiagnosisSubmit();
                } else {
                  this.onNavigateToJobCardAcceptance();
                }
              } else {
                this.rxjsService.setGlobalLoaderProperty(false);
                const dialogReff = this.dialog.open(BillOfMaterialValidationModalComponent, {
                  width: '500px',
                  data: {
                    buttons: {
                      create: 'Ok'
                    },
                    message: response.resources.validateMessage,
                    header: 'Invoice',
                  },
                  disableClose: true
                });
                dialogReff.afterClosed().subscribe(result => {
                  return;
                });
              }
            }
          });
        }
      });
  }

  onDiagnosisSubmit() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.crudService.update(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.CALL_INITIATION_DIAGNOSIS_UPDATE,
      {
        callInitiationId: this.callInitiationId,
        modifiedUserId: this.userData?.userId,
      }
    ).subscribe(res => {
      this.rxjsService.setGlobalLoaderProperty(true);
      this.onRateOurServiceSubmit();
    });
  }

  onRateOurServiceSubmit() {
    this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.RATE_OUR_SERVICE_CALL,
      {
        callInitiationId: this.callInitiationId,
        source: 'Web',
        createdUserId: this.userData?.userId,
      }
    ).subscribe(response => {
      this.onNavigateToJobCardAcceptance();
    });
  }

  onNavigateToJobCardAcceptance() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.router.navigate(['/customer', 'manage-customers', 'bill-of-material', 'job-card-acceptance'], {
      queryParams: {
        billOfMaterialId: this.billOfMaterialModel ? this.billOfMaterialModel.billOfMaterialId : null,
        installationId: this.installationId,
        customerId: this.customerId,
        addressId: this.customerAddressId,
        quotationVersionId: this.quotationVersionId,
        callInitiationId: this.callInitiationId,
        callType: this.callType,
        appointmentId: this.appointmentId,
        isFeedback: this.isFeedback,
      }
    });
  }

  openConfigDialog(dialogData) {
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      width: "550px",
      data: { ...dialogData, isConfirm: false, isClose: false, msgCenter: true, isAlert: true },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      } else {
        return;
      }
    });
  }

  openCalculator() {
    window.open("Calculator:///", "_self");
  }

  isKitConfigId(control) {
    return control?.get('kitConfigId')?.value;
  }

  navigateAfterPrCall() {
    this.alertDialog = false;
    this.navigateToPage();
  }

  openSerialNumbers(control) {
    if (!control?.get('billOfMaterialItemId')?.value || !this.billOfMaterialModel['billOfMaterialId']) {
      control?.get('billOfMaterialItemDetails')?.value?.forEach(el => {
        this.serialNumbersData = [{ serialNo: el?.serialNumber }];
      });;
      this.openSerialNumbersDialog = true;
      return;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.TECHNICIAN,
      TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_ITEM_DETAILS,
      undefined,
      false,
      prepareGetRequestHttpParams('', '', {
        BillOfMaterialId: this.billOfMaterialModel['billOfMaterialId'],
        BillOfMaterialItemId: control?.get('billOfMaterialItemId')?.value,
      })
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.serialNumbersData = response.resources;
        control?.get('billOfMaterialItemDetails')?.getRawValue()?.forEach(el => {
          this.serialNumbersData?.push({ serialNo: el?.serialNumber });
        });;
        this.openSerialNumbersDialog = true;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }
  changeEscalate() {
    if ((!this.getBOMActionPermission(PermissionTypes.EDIT) && this.billOfMaterialModel?.billOfMaterialId) || (!this.getBOMActionPermission(PermissionTypes.ADD) && !this.billOfMaterialModel?.billOfMaterialId)) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.openDoaReqMsgDialog = false;
    this.openAddContactDialog = true;
  }

  autoSaveAlertNavigate() {
    if (this.billOfMaterialForm.touched || this.billOfMaterialItemsFormArray.touched) {
      const message = `You have unsaved changes! Do you want to save this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      // const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      const autoSaveAlertDialog = this.dialogService.open(PrimengConfirmDialogPopupComponent, {
        // maxWidth: "400px",
        showHeader: false,
        width: "450px",
        data: { ...dialogData, msgCenter: true },
        baseZIndex: 5000,
        // disableClose: true
      });
      // dialogRef.afterClosed().subscribe(dialogResult => {
      autoSaveAlertDialog.onClose.subscribe(dialogResult => {
        // if (confirm("You have unsaved changes! Do you want to save this?")) {
        if (dialogResult === true) {
          if (!this.billOfMaterialForm.get('stockCodeSerialNumber').value) {
            this.billOfMaterialForm.get('stockCodeSerialNumber').clearValidators();
            this.billOfMaterialForm.get('stockCodeSerialNumber').updateValueAndValidity();
          }
          // this.autoSubmitClick()
          if (this.billOfMaterialForm.invalid) {
            this.billOfMaterialForm.markAllAsTouched();
            return false;
          }
          this.updateBillOfMaterial();
          // return true
        }
      })
    }
  }

  canDeactivate() {
    // autosave functionality starts here
    if (this.billOfMaterialForm.touched || this.billOfMaterialItemsFormArray.touched) {
      const message = `You have unsaved changes! Do you want to save this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      // const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      const canDeactivateDialog = this.dialogService.open(PrimengConfirmDialogPopupComponent, {
        // maxWidth: "400px",
        showHeader: false,
        width: "450px",
        data: dialogData,
        baseZIndex: 5000,
        // disableClose: true
      });
      // return dialogRef.afterClosed().pipe(map(dialogResult => {
      return canDeactivateDialog.onClose.pipe(map(dialogResult => {
        // if (confirm("You have unsaved changes! Do you want to save this?")) {
        if (dialogResult === true) {
          this.autoSubmitClick()
          if (!this.billOfMaterialForm.get('stockCodeSerialNumber').value) {
            this.billOfMaterialForm.get('stockCodeSerialNumber').clearValidators();
            this.billOfMaterialForm.get('stockCodeSerialNumber').updateValueAndValidity();
          }
          if (this.billOfMaterialForm.invalid) {
            this.billOfMaterialForm.markAllAsTouched();
            return false;
          }
          // setTimeout(() => {
          // this.navigateToTechAllocation()
          return true;
          // }, 2000);

        } else if (dialogResult === false) {
          // if (this.initiationBasicInfo.callInitiationId) {
          return true;
          // } else {
          //   this.snackbarService.openSnackbar('Please save and proceed!', ResponseMessageTypes.WARNING);
          // }
        }
      }))
    } else {
      return true;
    }
    // autosave functionality ends here
  }

  autoSubmitClick() {
    let element: HTMLElement = document.querySelector('.autoSave') as HTMLElement;
    element.click();
  }

  openConfirmationDialog() {
    if (!this.getBOMActionPermission(PermissionTypes.EDIT) && this.billOfMaterialModel?.billOfMaterialId) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.isGRVConfirmationDialog = false;
    const isNotContainsYStock = this.billOfMaterialItemsFormArray.getRawValue()?.filter(bill => bill.stockType?.toLowerCase() == 'y');
    const isMovedTransfer = this.billOfMaterialItemsFormArray.getRawValue()?.filter(bill => bill.stockType?.toLowerCase() == 'y' && +bill.transfer > 0);
    if (!this.billOfMaterialItemsFormArray.getRawValue()?.length || !isNotContainsYStock?.length) {
      this.isGRVConfirmationDialog = false;
      this.snackbarService.openSnackbar("Unable to process GRV, no stock available", ResponseMessageTypes.WARNING);
      return;
    } else if (!isMovedTransfer?.length) {
      this.snackbarService.openSnackbar("Unable to process GRV, as there is no stock on transferred", ResponseMessageTypes.WARNING);
      return;
    } else if (!this.billOfMaterialModel?.billOfMaterialId) {
      this.snackbarService.openSnackbar("Please apply the Bill of Material", ResponseMessageTypes.WARNING)
      return;
    } else if (this.billOfMaterialForm.touched || this.billOfMaterialItemsFormArray.touched) {
      this.autoSaveAlertNavigate();
      return;
    } else {
      this.isGRVConfirmationDialog = true;
    }
  }

  createServiceCall() {
    if (!this.getBOMActionPermission(PermissionTypes.EDIT) && this.billOfMaterialModel?.billOfMaterialId) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const reqObj = {
      callInitiationId: this.callInitiationId,
      isSameAppointment: true,
      createdUserId: this.userData?.userId,
      newCallInitiationId: this.newCallInitiationId || this.billOfMaterialModel?.derivedCallInitiationId || null,
      itemId: this.searchScannedItemResponse?.itemId,
      qty: this.searchScannedItemResponse?.qty,
      SerialNumber: this.billOfMaterialForm.get('stockCodeSerialNumber').value,
      itemPricingConfigId: this.searchScannedItemResponse?.itemPricingConfigId,
    };
    this.isServiceCallSumbmit = true;
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.SERVICE_CALL_INITIATION_EXISTING_CALL, reqObj)
      .subscribe((res: IApplicationResponse) => {
        this.isServiceCallSumbmit = false;
        if (res?.isSuccess && res?.statusCode == 200) {
          this.newCallInitiationId = res?.resources;
          this.billOfMaterialForm.get('stockCodeSerialNumber').setValue('');
          this.isScanServiceCallDialog = false;
        }
      })
  }

  getDisabledCheckbox() {
    return this.getDisabledForm() || this.billOfMaterialItemsFormArray?.controls?.length == 0 || (!this.getBOMActionPermission(PermissionTypes.EDIT) && this.billOfMaterialModel?.billOfMaterialId) || (!this.getBOMActionPermission(PermissionTypes.ADD) && !this.billOfMaterialModel?.billOfMaterialId);
  }

  getDisabledForm() {
    return this.billOfMaterialModel?.isFaultyReturn || this.isVoid || (!this.getBOMActionPermission(PermissionTypes.EDIT) && this.billOfMaterialModel?.billOfMaterialId) || (!this.getBOMActionPermission(PermissionTypes.ADD) && !this.billOfMaterialModel?.billOfMaterialId) ? true : null;
  }

  getProcessGVDisabledForm() {
    return this.billOfMaterialModel?.isFaultyReturn || this.isVoid || !this.billOfMaterialModel?.stockOrderNumber || (!this.getBOMActionPermission(PermissionTypes.EDIT) && this.billOfMaterialModel?.billOfMaterialId) || (!this.getBOMActionPermission(PermissionTypes.ADD) && !this.billOfMaterialModel?.billOfMaterialId);
  }

  /* Navigate To GRV */
  navigateToGRVPage() {
    if (!this.getBOMActionPermission(PermissionTypes.EDIT) && this.billOfMaterialModel?.billOfMaterialId) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.billOfMaterialModel.stockOrderNumber == null) return;
    this.router.navigate(['/technical-management', 'technician-goods-return', 'add-edit'], {
      queryParams: {
        id: null,
        type: 'billOfMaterial',
        stockOrderNumber: this.billOfMaterialModel.stockOrderNumber,
        stockOrderId: this.billOfMaterialModel.stockOrderId,
        callInitiationId: this.callInitiationId,
      }, skipLocationChange: true
    });
  }
}
