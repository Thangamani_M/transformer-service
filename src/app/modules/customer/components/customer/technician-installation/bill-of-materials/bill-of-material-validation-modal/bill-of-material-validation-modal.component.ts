import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { CrudService, RxjsService } from '@app/shared';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-bill-of-material-validation-modal',
  templateUrl: './bill-of-material-validation-modal.component.html'
})
export class BillOfMaterialValidationModalComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public popupData: any) { }

  ngOnInit(): void {
    let messageSpan = document.createElement('span');
    messageSpan.innerHTML = this.popupData['message'];
    document.getElementById('parentContainer').appendChild(messageSpan);
  }
}
