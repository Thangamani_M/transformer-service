import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels, SalesModuleApiSuffixModels } from '@modules/sales';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin } from 'rxjs';
import { ProcessPaymentInstallationModelComponent } from '../process-payment-installation-model/process-payment-installation-model.component';
import { ProcessPaymentModalComponent } from '../process-payment-modal';

@Component({
    selector: 'app-job-card-acceptance',
    templateUrl: './job-card-acceptance.component.html'
})
export class JobCardAcceptanceComponent implements OnInit {

    userData: UserLogin;
    billOfMaterialId: string;
    callType: any;
    callTypeName: string;
    installationId: string;
    customerId: string;
    customerAddressId: string;
    quotationVersionId: string;
    callInitiationId: string;
    appointmentId: string;
    isFeedback: string;
    jobCardAcceptanceDetails: any = {};
    customerDetailsForm: FormGroup;
    jobCardDetail: any = {};
    installationPaymentSubscription: any;

    constructor(private formBuilder: FormBuilder,
      private dialogService: DialogService,
        private crudService: CrudService,
        private rxjsService: RxjsService,
        private store: Store<AppState>,
        private snackbarService: SnackbarService,
        private dialog: MatDialog,
        private activatedRoute: ActivatedRoute,
        private router: Router) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
        this.billOfMaterialId = this.activatedRoute.snapshot.queryParams.billOfMaterialId;
        this.callType = this.activatedRoute.snapshot.queryParams.callType;

        this.installationId = this.activatedRoute.snapshot.queryParams.installationId;
        this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
        this.customerAddressId = this.activatedRoute.snapshot.queryParams.addressId;
        this.quotationVersionId = this.activatedRoute.snapshot.queryParams.quotationVersionId;
        this.callInitiationId = this.activatedRoute.snapshot.queryParams.callInitiationId;
        this.appointmentId = this.activatedRoute.snapshot.queryParams.appointmentId;
        this.isFeedback = this.activatedRoute.snapshot.queryParams.isFeedback;

        this.callTypeName = this.callType == '1' ? 'Installation' : this.callType == '2' ? 'Service' :
            this.callType == '3' ? 'Special Projects' : '';
        this.onShowValue();
    }

    ngOnInit() {
        if (this.billOfMaterialId) {
            this.createCustomerForm();
            this.getJobCardById();
        }
    }

    createCustomerForm() {
      this.customerDetailsForm = this.formBuilder.group({});
      this.customerDetailsForm.addControl('customerName', new FormControl());
      this.customerDetailsForm.addControl('designation', new FormControl());
      this.customerDetailsForm.addControl('createdUserId', new FormControl(this.userData.userId));
      this.customerDetailsForm.addControl('isHandoverCertificate', new FormControl(false));
      this.customerDetailsForm = setRequiredValidator(this.customerDetailsForm, ['customerName']);
    }

    onShowValue(response?: any) {
      this.jobCardDetail = [
        { name: 'Customer', value: response?.resources?.customer ? response?.resources?.customer : '', order: 1 },
        { name: 'Date', value: response?.resources?.createdDate ? response?.resources?.createdDate : '', order: 2 },
        { name: 'Address', value: response?.resources?.address ? response?.resources?.address : '', order: 3 },
        { name: 'Technician', value: response?.resources?.technicianName ? response?.resources?.technicianName : '', order: 4 },
        { name: 'Debtors Code', value: response?.resources?.debtorRefNo ? response?.resources?.debtorRefNo : '', order: 5 },
        { name: 'BDI Number', value: response?.resources?.bdiNumber ? response?.resources?.bdiNumber : '', order: 6 },
        { name: 'Quote Number', value: response?.resources?.quoteNumber ? response?.resources?.quoteNumber : '', order: 7 },
        { name: 'Job Category', value: response?.resources?.jobCategory ? response?.resources?.jobCategory : '', order: 8 },
        { name: 'PO Number', value: response?.resources?.poNumber ? response?.resources?.poNumber : '', order: 9 },
        { name: 'Diagnosis', value: response?.resources?.diagnosis ? response?.resources?.diagnosis : '', order: 10 },
      ];
    }

    getJobCardById() {
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_JOB_CARD_ACCEPTANCE,
          undefined, false, prepareRequiredHttpParams({ billOfMaterialId: this.billOfMaterialId })
      ).subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
              this.jobCardAcceptanceDetails = response.resources;
              this.onShowValue(response);
              this.rxjsService.setGlobalLoaderProperty(false);
          }
      });
    }

    sendElectronicApproval() {
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEND_JOB_CARD_APPROVAL,
        {
          billOfMaterialId: this.billOfMaterialId,
          createdUserId: this.userData.userId,
        }
    ).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
            this.rxjsService.setGlobalLoaderProperty(false);
        }
    });
    }

    processInvoice() {
      if (this.customerDetailsForm.invalid) {
        this.customerDetailsForm.markAllAsTouched();
        return;
      }
      let dataTosend = {...this.customerDetailsForm.value};
      dataTosend.billOfMaterialId = this.billOfMaterialId;
      dataTosend.customerComments = this.jobCardAcceptanceDetails.comments;
      let formData = new FormData();
      formData.append('dto', JSON.stringify(dataTosend));
      this.crudService.create(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_JOB_CARD_ACCEPTANCE_PROCESS_INVOICE,
        formData
      ).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
            window.open(response.resources, '_blank');
        }
      });
    }
    escalate() {
      let data = {
          billofMaterialId: this.billOfMaterialId, 
          createdUserId: this.userData.userId 
      }
      this.crudService.create(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_TECHNICAL_INVOICE_REQUEST,
        data
      ).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
        }
      });
    }
    
    processPayment() {
      if(this.jobCardAcceptanceDetails?.totalAmount == 0) {
        this.createZeroPayment();
        return;
      }
      if(this.customerDetailsForm?.invalid) {
        this.customerDetailsForm.markAllAsTouched();
        return;
      }
      if(this.callType == 1) {
        this.onLoadInstallationPaymentDialogValue();
      } else {
        this.onLoadServicePaymentDialogValue();
      }
    }

    onLoadInstallationPaymentDialogValue() {
      let api: any = [
        this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_STATIC, undefined, false),
        this.crudService.get(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.QUOTATION_PAYMENT_OUTSTANDING, this.quotationVersionId),
      ]
      if(this.installationPaymentSubscription && !this.installationPaymentSubscription?.closed) {
        this.installationPaymentSubscription.unsubscribe();
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      this.rxjsService.setGlobalLoaderProperty(true);
      this.installationPaymentSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
          this.openInstallationCallPaymentDialog(...response);
          this.rxjsService.setGlobalLoaderProperty(false);
      })
    }

    openInstallationCallPaymentDialog(static_result?: any, paymentDetails?: any) {
      if(!this.quotationVersionId) {
        return;
      }
      if(!paymentDetails?.resources && paymentDetails?.message) {
        this.snackbarService.openSnackbar(paymentDetails?.message, ResponseMessageTypes.WARNING);
        return;
      }
      const dialogReff = this.dialogService.open(ProcessPaymentInstallationModelComponent, {
        header: 'Payment',
        width: '750px',
        showHeader: false,
        baseZIndex: 1000,
        data: {
          static_result: static_result?.resources,
          paymentDetails: paymentDetails,
          modifiedUserId: this.userData?.userId,
          quotationVersionId: this.quotationVersionId,
        },
      });
      dialogReff.onClose.subscribe(result => {
        this.rxjsService.setDialogOpenProperty(false);
        if (result) {

        }
      });
    }

    onLoadServicePaymentDialogValue() {
      const api  = [
        this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_PAYMENT_METHODS, prepareRequiredHttpParams({isCustomerBilling: true, isDealerBilling: false})),
        this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.CUSTOMER_DEBIT_ORDER_RUN_CODES, prepareRequiredHttpParams({customerId: this.customerId})),
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_JOB_CARD_ACCEPTANCE_PAYMENT_PROCESS_DETAILS,
            undefined, false, prepareRequiredHttpParams({
                BillOfMaterialId: this.billOfMaterialId
            }))
      ];
      this.rxjsService.setGlobalLoaderProperty(true);
      forkJoin(api).subscribe((response: IApplicationResponse[]) => {
        this.openServiceCallPaymentDialog(...response);
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }

    openServiceCallPaymentDialog(paymentMethodsList?: any, orderRunCodesList?: any, paymentDetails?: any) {
      const dialogReff = this.dialogService.open(ProcessPaymentModalComponent, {
        width: '750px',
        data: {
          header: 'Payment',
          createdUserId: this.userData.userId,
          billofMaterialId: this.billOfMaterialId,
          customerName: this.customerDetailsForm.get('customerName').value,
          designation: this.customerDetailsForm.get('designation').value,
          paymentMethodsList: paymentMethodsList?.resources,
          orderRunCodesList: orderRunCodesList?.resources,
          paymentDetails: paymentDetails?.resources,
        },
        showHeader: false,
        baseZIndex: 1000,
      });
      dialogReff.onClose.subscribe(result => {
        this.rxjsService.setDialogOpenProperty(false);
        if (!result) {
          this.rxjsService.setGlobalLoaderProperty(false);
          return;
        }
      });
    }

    createZeroPayment() {
      if(this.customerDetailsForm?.invalid) {
        this.customerDetailsForm.markAllAsTouched();
        return;
      }
      const reqObj = {
        addressId: this.customerAddressId,
        billOfMaterialId: this.billOfMaterialId,
        createdUserId: this.userData?.userId,
        customerName: this.customerDetailsForm.get('customerName').value,
        designation: this.customerDetailsForm.get('designation').value,
        isSinglePayment: false,
        notes: 'Zero Invoice Payment',
        paymentMethodId: 0
      }
      let formData = new FormData();
      formData.append('rawData', JSON.stringify(reqObj));
      this.crudService.create(
        ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_JOB_CARD_ACCEPTANCE_PAYMENT_PROCESS,
        formData
      ).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.navigateToCallInitiationPage();
        }
      });
    }

    navigateToCallInitiationPage() {
        this.router.navigate(['/customer', 'manage-customers', 'call-initiation'], {
          queryParams: { 
            // customerId: this.installationId 
            customerId: this.customerId,
            customerAddressId: this.customerAddressId,
            initiationId: this.installationId,
            callType: this.callType,
            appointmentId: this.appointmentId ? this.appointmentId : null,
          },skipLocationChange:true
        });
      }

      navigateToBillOfMaterialPage() {
        // if (!this.initiationAppointmentInfo?.quotationVersionId && this.callType == 1) {
        //   return;
        // }
        this.router.navigate(['/customer', 'manage-customers', 'bill-of-material'], {
          queryParams: {
            installationId: this.installationId,
            customerId: this.customerId,
            customerAddressId: this.customerAddressId,
            quotationVersionId: this.quotationVersionId,
            callInitiationId: this.callInitiationId,
            callType: this.callType,
            appointmentId: this.appointmentId,
            isFeedback: this.isFeedback,
          }, skipLocationChange: true
        });
      }
}