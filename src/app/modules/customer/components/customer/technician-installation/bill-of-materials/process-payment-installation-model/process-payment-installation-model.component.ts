import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { getNthDate, CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator, SnackbarService, OtherService } from '@app/shared';
import { ProcessPaymentDetailsModel, ProcessPaymentInstallationModel } from '@modules/customer/models/process-payment-installation.model';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-process-payment-installation-model',
  templateUrl: './process-payment-installation-model.component.html',
  styleUrls: ['./process-payment-installation-model.component.scss']
})
export class ProcessPaymentInstallationModelComponent implements OnInit {

  processPaymentDialogForm: FormGroup;
  selectedTabIndex: number = 0;
  isLoading: boolean;
  isSubmitted: boolean;
  primengTableConfigProperties: any;
  todayDate: any = new Date();

  constructor(private crudService: CrudService, private rxjsService: RxjsService, private otherService: OtherService,
    private snackbarService: SnackbarService, private formBuilder: FormBuilder,
    public config: DynamicDialogConfig, public ref: DynamicDialogRef,) {
    this.rxjsService.setDialogOpenProperty(true);
    this.primengTableConfigProperties = {
      tableComponentConfigs: {
        tabsList: [
          {
            dataKey: 'paymentId',
            formArrayName: 'paymentDetailsArray',
            columns: [
              { field: 'paidAmount', displayName: '', type: 'input_hidden', className: '', required: true, },
              { field: 'paymentMethodId', displayName: 'Payment Method', type: 'input_select', className: 'col-4', required: true, displayValue: 'displayName', assignValue: 'id', isDefaultOpt: true, defaultText: 'Select', options: this.config?.data?.static_result?.paymentMethods },
              { field: 'debitDate', displayName: 'Debit Date', type: 'input_date', className: 'col-8 fidelity-date-picker-con fdt-date-con p-relative mt-2', minDate: this.todayDate },
            ],
            hideFormArrayAction: true
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngAfterViewInit() {
    this.onLoadValue();
  }

  btnCloseClick() {
    this.ref.close();
  }

  initForm(processPaymentInstallationModel?: ProcessPaymentInstallationModel) {
    let processPaymentModel = new ProcessPaymentInstallationModel(processPaymentInstallationModel);
    this.processPaymentDialogForm = this.formBuilder.group({});
    Object.keys(processPaymentModel).forEach((key) => {
      if (typeof processPaymentModel[key] === 'object' && processPaymentModel[key] != null) {
        this.processPaymentDialogForm.addControl(key, new FormArray(processPaymentModel[key]));
      } else {
        this.processPaymentDialogForm.addControl(key, new FormControl({ value: processPaymentModel[key], disabled: true }));
      }
    });
    this.processPaymentDialogForm = setRequiredValidator(this.processPaymentDialogForm, ["paymentOptionId"]);
    if(this.config?.data?.paymentDetails?.resources?.isAllowedtoChangePaymentMethod) {
      this.processPaymentDialogForm?.get('paymentOptionId').enable();
    }
    this.onValueChanges();
  }

  initFormArray(processPaymentDetailsModel?: ProcessPaymentDetailsModel) {
    let processPaymentDetailModel = new ProcessPaymentDetailsModel(processPaymentDetailsModel);
    let processPaymentDetailsFormArray = this.formBuilder.group({});
    Object.keys(processPaymentDetailModel).forEach((key) => {
      processPaymentDetailsFormArray.addControl(key, new FormControl({ value: processPaymentDetailModel[key], disabled: false }));
    });
    processPaymentDetailsFormArray = setRequiredValidator(processPaymentDetailsFormArray, ["paidAmount", "paymentMethodId", "debitDate"]);
    // this.getProcessPaymentFormArray.insert(0, processPaymentDetailsFormArray);
    this.getProcessPaymentFormArray.push(processPaymentDetailsFormArray);
  }

  get getProcessPaymentFormArray(): FormArray {
    if (!this.processPaymentDialogForm) return;
    return this.processPaymentDialogForm.get('paymentDetailsArray') as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onValueChanges() {
    this.processPaymentDialogForm.get('paymentOptionId').valueChanges.subscribe((res) => {
      if(res) {
        this.clearFormArray(this.getProcessPaymentFormArray);
        if (res == this.config?.data?.paymentDetails?.resources?.paymentOptionId) {
          this.onPatchFormArray(this.config?.data?.paymentDetails);
        } else {
          for (let i = 0; i < res; i++) {
            const paidAmount = this.config?.data?.paymentDetails?.resources?.balanceAmount ? 
              (+this.config?.data?.paymentDetails?.resources?.balanceAmount/res)?.toFixed(2) : 0;
            this.initFormArray({
              paidAmount:  +paidAmount,
              paymentMethodId: '',
              debitDate: null,
            })
          }
        }
      }
    })
  }

  onLoadValue() {
    this.patchValue(this.config?.data?.paymentDetails);
  }

  patchValue(res) {
    this.processPaymentDialogForm.patchValue({
      paymentTransactionId: res?.resources?.paymentTransactionId ? res?.resources?.paymentTransactionId : '',
      quotationVersionId: res?.resources?.quotationVersionId ? res?.resources?.quotationVersionId : '',
      balanceAmount: res?.resources?.balanceAmount ? this.otherService?.transformDecimal(res?.resources?.balanceAmount) : '',
      isAllowedtoChangePaymentMethod: res?.resources?.isAllowedtoChangePaymentMethod?.toString() ? res?.resources?.isAllowedtoChangePaymentMethod : null,
      paymentMethodName: res?.resources?.paymentMethodName ? res?.resources?.paymentMethodName : '',
      debtorName: res?.resources?.debtorName ? res?.resources?.debtorName : '',
      paymentOptionId: res?.resources?.paymentOptionId ? res?.resources?.paymentOptionId : '',
      bdiNumber: res?.resources?.bdiNumber ? res?.resources?.bdiNumber : '',
      accountHolderName: res?.resources?.accountHolderName ? res?.resources?.accountHolderName : '',
      accountNo: res?.resources?.accountNo ? res?.resources?.accountNo : '',
      bankName: res?.resources?.bankName ? res?.resources?.bankName : '',
      bankBranchCode: res?.resources?.bankBranchCode ? res?.resources?.bankBranchCode : '',
      isPurchaseOrderExist: res?.resources?.isPurchaseOrderExist?.toString() ? res?.resources?.isPurchaseOrderExist : '',
      purchaseOrderNo: res?.resources?.purchaseOrderNo ? res?.resources?.purchaseOrderNo : '',
      purchaseOrderDate: res?.resources?.purchaseOrderDate ? new Date(res?.resources?.purchaseOrderDate) : null,
    }, {emitEvent: false});
    this.onPatchFormArray(res);
  }

  onPatchFormArray(res) {
    res?.resources?.payments?.forEach(element => {
      this.initFormArray({
        paidAmount: element?.paidAmount ? element?.paidAmount : '',
        paymentMethodId: element?.paymentMethodId ? element?.paymentMethodId : '',
        debitDate: element?.debitDate ? new Date(element?.debitDate) : null,
      })
    });
  }

  onValidatePaidAmountWithBalanceAmount() {
    let paidAmount = 0;
    let balance = 0;
    let missingValue = 0;
    this.getProcessPaymentFormArray.getRawValue()?.forEach((el: any) => {
      paidAmount += +el?.paidAmount;
    })
    if(paidAmount > this.config?.data?.paymentDetails?.resources?.balanceAmount){
      balance = +paidAmount - +this.config?.data?.paymentDetails?.resources?.balanceAmount;
      missingValue = +this.getProcessPaymentFormArray.controls[0].get('paidAmount').value - +balance?.toFixed(2);
      paidAmount = +paidAmount - +balance?.toFixed(2);
    } else if(paidAmount < this.config?.data?.paymentDetails?.resources?.balanceAmount){
      balance = +this.config?.data?.paymentDetails?.resources?.balanceAmount - +paidAmount;
      missingValue = +this.getProcessPaymentFormArray.controls[0].get('paidAmount').value + +balance?.toFixed(2);
      paidAmount = +paidAmount + +balance?.toFixed(2);
    }
    if(missingValue > 0) {
      this.getProcessPaymentFormArray.controls[0].get('paidAmount').setValue(+(missingValue?.toFixed(2)));
    }
    return paidAmount == this.config?.data?.paymentDetails?.resources?.balanceAmount;
  }

  onSubmitDialog() {
    if (this.isSubmitted || !this.processPaymentDialogForm?.valid) {
      this.processPaymentDialogForm?.markAllAsTouched();
      return;
    }
    if(!this.onValidatePaidAmountWithBalanceAmount()) {
      return;
    }
    this.isSubmitted = true;
    let reqObj = {
      paymentTransactionId: this.processPaymentDialogForm?.get('paymentTransactionId').value,
      quotationVersionId: this.config?.data?.quotationVersionId,
      modifiedUserId: this.config?.data?.modifiedUserId,
      payments: this.getProcessPaymentFormArray.getRawValue(),
    }
    if(this.config?.data?.paymentDetails?.isAllowedtoChangePaymentMethod) {
      reqObj['paymentOptionId'] = this.processPaymentDialogForm?.get('paymentOptionId').value;
    }
    this.crudService.create(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.QUOTATION_PAYMENT_OUTSTANDING, reqObj)
    .subscribe((res: IApplicationResponse) => {
      if(res?.isSuccess && res?.statusCode == 200) {
        this.ref.close(true);
      }
      this.isSubmitted = false;
      this.rxjsService.setDialogOpenProperty(false);
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

}
