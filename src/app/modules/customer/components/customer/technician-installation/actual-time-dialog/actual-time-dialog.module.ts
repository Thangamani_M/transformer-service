import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ActualTimeDialogComponent } from '@modules/customer';

@NgModule({
  declarations: [  ActualTimeDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  exports: [ActualTimeDialogComponent],
  entryComponents: [ActualTimeDialogComponent],
})
export class ActualTimeDialogModule { }
