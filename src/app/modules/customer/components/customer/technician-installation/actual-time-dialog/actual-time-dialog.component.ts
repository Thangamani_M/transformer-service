import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { adjustDateFormatAsPerPCalendar, ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CURRENT_YEAR_TO_NEXT_FIFTY_YEARS, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setMinMaxTimeValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { ActualTimingListModel, ActualTimingModel } from '@modules/customer/models/actual-timing.model';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';

@Component({
  selector: 'app-actual-time-dialog',
  templateUrl: './actual-time-dialog.component.html'
})
export class ActualTimeDialogComponent implements OnInit {

  actualTimingForm: FormGroup;
  actualTimingList: FormArray;
  actualTimingDateList = [];
  todayDate = new Date();

  constructor(private rxjsService: RxjsService, private httpCancelService: HttpCancelService, public dialogRef: MatDialogRef<ActualTimeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public config: any, private crudService: CrudService, private dialog: MatDialog,
    private momentService: MomentService, private snackbarService: SnackbarService, private formBuilder: FormBuilder,) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    this.loadActualTimings()
  }

  btnCloseClick() {
    this.rxjsService.setDialogOpenProperty(false);
    this.dialogRef.close(false);
  }

  loadActualTimings() {
    this.actualTimingDateList = this.config?.actualTimingDateList;
    this.createActualTimingForm()
    let actualTimingModel = new ActualTimingModel(this.config?.actualTimingModelData);
    if (this.config?.actualTimingModelData.length == 0) {
      this.actualTimingList = this.getactualTimingListArray;
      let actualTimingListModel = new ActualTimingListModel(null);
      actualTimingListModel.appointmentId = this.config?.initiationDetailsData?.appointmentInfo?.appointmentId ? this.config?.initiationDetailsData?.appointmentInfo?.appointmentId : '';
      // actualTimingListModel.onRouteDate = this.config?.initiationDetailsData.appointmentInfo.scheduledStartDate ? this.config?.initiationDetailsData.appointmentInfo.scheduledStartDate : "";
      actualTimingListModel.onRouteDate = "";
      actualTimingListModel.createdUserId = this.config?.userData.userId ? this.config?.userData.userId : '';
      actualTimingListModel.modifiedUserId = this.config?.userData.userId ? this.config?.userData.userId : '';
      actualTimingListModel.appointmentLogId = ''; // added for appointmentLogId date
      this.actualTimingList.push(this.createactualTimingListModel(actualTimingListModel));;
    } else {
      this.actualTimingList = this.getactualTimingListArray;
      this.config?.actualTimingModelData?.forEach((actualTimingListModel: ActualTimingListModel) => {
        actualTimingListModel.appointmentId = this.config?.initiationDetailsData?.appointmentInfo?.appointmentId ? this.config?.initiationDetailsData?.appointmentInfo?.appointmentId : ''
        // actualTimingListModel.startTime = actualTimingListModel?.startTime ? this.momentService.toMoment(this.momentService.setTime(actualTimingListModel.startTime)).seconds(0).format() : null,
        // actualTimingListModel.endTime = actualTimingListModel?.endTime ? this.momentService.toMoment(this.momentService.setTime(actualTimingListModel.endTime)).seconds(0).format() : null,
        actualTimingListModel.onRouteTime = actualTimingListModel?.onRouteTime ? this.momentService.setTime(actualTimingListModel.onRouteTime) : null,
        actualTimingListModel.startTime = actualTimingListModel?.startTime ? this.momentService.setTime(actualTimingListModel.startTime) : null,
        actualTimingListModel.endTime = actualTimingListModel?.endTime ? this.momentService.setTime(actualTimingListModel.endTime) : null,
        actualTimingListModel.createdUserId = this.config?.userData?.userId ? this.config?.userData.userId : ''
        actualTimingListModel.modifiedUserId = this.config?.userData?.userId ? this.config?.userData.userId : '';
        actualTimingListModel.appointmentLogId = actualTimingListModel?.appointmentLogId ? actualTimingListModel.appointmentLogId : ''; // added for appointmentLogId date
        actualTimingListModel.onRouteDate = actualTimingListModel?.onRouteDate ? new Date(actualTimingListModel.onRouteDate) : '';
        actualTimingListModel.startTimeMinDate = actualTimingListModel?.onRouteTime ? this.momentService.toMoment(this.momentService.addMinits(actualTimingListModel.onRouteTime, 1)).seconds(0).format() : null;
        actualTimingListModel.endTimeMinDate = actualTimingListModel?.startTime ? this.momentService.toMoment(this.momentService.addMinits(actualTimingListModel.startTime, 1)).seconds(0).format() : null,
        this.actualTimingList.push(this.createactualTimingListModel(actualTimingListModel));
      });
    }

  }

  isFormDisabled() {
    if (this.config?.isInvoiced || this.config?.isVoid) {
      return true
    } else {
      return false
    }
  }

  createActualTimingForm(actualTimingModel?: ActualTimingModel) {
    let actualTimingModelControl = new ActualTimingModel(actualTimingModel);
    this.actualTimingForm = this.formBuilder.group({
      actualTimingList: this.formBuilder.array([])
    });
    Object.keys(actualTimingModelControl).forEach((key) => {
      this.actualTimingForm.addControl(key, new FormControl(actualTimingModelControl[key]));
    });

  }

  //Create FormArray
  get getactualTimingListArray(): FormArray {
    if (!this.actualTimingForm) return;
    return this.actualTimingForm.get("actualTimingList") as FormArray;
  }

  //Create FormArray controls
  createactualTimingListModel(actualTimingListModel?: ActualTimingListModel): FormGroup {
    let actualTimingListModelContol = new ActualTimingListModel(actualTimingListModel);
    let formControls = {};
    Object.keys(actualTimingListModelContol).forEach((key) => {
      if (key == 'onRouteDate') { //  || key == 'startTime' || key == 'endTime'
        formControls[key] = [{ value: actualTimingListModelContol[key], disabled: this.isFormDisabled() }, [Validators.required]]
      } else {
        formControls[key] = [{ value: actualTimingListModelContol[key], disabled: this.isFormDisabled() }]
      }
    });
    let formContrlsGroup = this.formBuilder.group(formControls)
    formContrlsGroup = setMinMaxTimeValidator(formContrlsGroup, [
      { formControlName: 'onRouteTime', compareWith: 'startTime', type: 'min' },
      { formControlName: 'startTime', compareWith: 'onRouteTime', type: 'max' },
      { formControlName: 'startTime', compareWith: 'endTime', type: 'min' },
      { formControlName: 'endTime', compareWith: 'startTime', type: 'max' }
    ]);
    formContrlsGroup.get('startTime').valueChanges.subscribe(val => {
      if (!val) {
        formContrlsGroup.get('endTime').setValue(null, { emitEvent: false });
        formContrlsGroup.get('actualTime').setValue(null, { emitEvent: true });
        return
      }
      let endTimeMinTime = this.momentService.toMoment(this.momentService.addMinits(val, 1)).seconds(0).format();
      formContrlsGroup.get('endTimeMinDate').setValue(endTimeMinTime, { emitEvent: false });
      formContrlsGroup.get('endTime').setValue(null, { emitEvent: false });
      formContrlsGroup.get('actualTime').setValue(null, { emitEvent: false });
      formContrlsGroup.updateValueAndValidity()
    })
  
    formContrlsGroup.get('endTime').valueChanges.subscribe(val => {
      if (!val) {
        formContrlsGroup.get('actualTime').setValue(null, { emitEvent: true });
        return
      }

      let currentStartTime = this.momentService.toMoment(formContrlsGroup.get('startTime').value).seconds(0).format();
      let currentEndTime = this.momentService.toMoment(formContrlsGroup.get('endTime').value).seconds(0).format();
      // let addToMints1 = this.momentService.getTimeDiff(formContrlsGroup.get('startTime').value, formContrlsGroup.get('endTime').value)
      let addToMints1 = this.momentService.getTimeDiff(new Date(formContrlsGroup.get('startTime').value), new Date(formContrlsGroup.get('endTime').value))
      // let addToMints1 = this.momentService.getTimeDiff(currentStartTime, currentEndTime);
      formContrlsGroup.get('actualTime').setValue(addToMints1);
    })
    formContrlsGroup.get('onRouteDate').valueChanges.subscribe(val => {
      if (val) {
        let appointmentLogData = this.actualTimingDateList.find(element => element['displayName'] == val);
        formContrlsGroup.get('appointmentLogId').setValue(appointmentLogData ? appointmentLogData['id'] : '');
      } else {
        formContrlsGroup.get('appointmentLogId').setValue('');
      }
    })
    formContrlsGroup.get('onRouteTime').valueChanges.subscribe(val => {
      if (!val) {
        formContrlsGroup.get('startTime').setValue(null, { emitEvent: true });
        formContrlsGroup.get('endTime').setValue(null, { emitEvent: true });
        formContrlsGroup.get('actualTime').setValue(null, { emitEvent: true });
        return;
      }
      // actualTimingListModel.startTimeMinDate = actualTimingListModel.onRouteTime ?  this.momentService.toMoment(this.momentService.addMinits(actualTimingListModel.onRouteTime, 1)).seconds(0).format() : null;
      let startTimeMinTime = this.momentService.toMoment(this.momentService.addMinits(val, 1)).seconds(0).format();
      formContrlsGroup.get('startTimeMinDate').setValue(startTimeMinTime, { emitEvent: true });
      formContrlsGroup.get('startTime').setValue(null, { emitEvent: true });
      formContrlsGroup.get('endTime').setValue(null, { emitEvent: true });
      formContrlsGroup.get('actualTime').setValue(null, { emitEvent: true });
    })
    return formContrlsGroup;
  }

  getDateFormat() {
    return adjustDateFormatAsPerPCalendar();
  }

  getYearRange(obj) {
    if (obj?.get('onRouteDate')?.value) {
      const startYearRange = obj?.get('onRouteDate')?.value.getFullYear();
      const endYearRange = startYearRange+50;
      return `${startYearRange}:${endYearRange}`;
    } else {
      return CURRENT_YEAR_TO_NEXT_FIFTY_YEARS;
    }
  }

  getStartMinDate(obj) {
    return obj?.get('startTimeMinDate')?.value ? new Date(obj?.get('startTimeMinDate')?.value) : null;
  }

  getEndMinDate(obj) {
    return obj?.get('endTimeMinDate')?.value ? new Date(obj?.get('endTimeMinDate')?.value) : null;
  }


  onChangeActualTime(key, val, i) {
    this.actualTimingList.value.forEach((element, index) => {
      if (i != index) {
        let convertedOnRouteDate = this.getactualTimingListArray.controls[i].get('onRouteDate').value;
        let convertedElementOnRouteDate = this.getactualTimingListArray.controls[index].get('onRouteDate').value;
        // let routeDateSplit = onRouteDate.split('-');
        // let convertedOnRouteDate = new Date(routeDateSplit[2], Number(routeDateSplit[1]) - 1, routeDateSplit[0]);

        // let elementOnRouteDate = element['onRouteDate'];
        // let elementRouteDateSplit = onRouteDate.split('-');
        // let convertedElementOnRouteDate = new Date(elementRouteDateSplit[2], Number(elementRouteDateSplit[1]) - 1, elementRouteDateSplit[0]);
        let isSameDay = this.momentService.isSameDay(convertedOnRouteDate, convertedElementOnRouteDate)
        //  if (+convertedOnRouteDate != +convertedElementOnRouteDate) {
        if (isSameDay) {
          let currentChangedTime = this.momentService.toMoment(val.value).seconds(0).format();
          let exist = this.momentService.betWeenDateTime(element.startTime, element.endTime, new Date(currentChangedTime));
          let isStartSameDay = this.momentService.isSameDayTime(element.startTime, new Date(currentChangedTime));
          let isEndSameDay = this.momentService.isSameDayTime(element.endTime, new Date(currentChangedTime));
          if (exist || isStartSameDay || isEndSameDay) {
            this.getactualTimingListArray.controls[i].get(key).setValue(null, { emitEvent: false });
            this.snackbarService.openSnackbar('Time already exist', ResponseMessageTypes.WARNING);
          }
        }
      }
    });
  }

  onChangeOnRouteDate(val, i) {

  }

  //Add Details
  addActualTiming(): void {
    if (!this?.config?.addpermission) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.getactualTimingListArray.invalid) {
      this.getactualTimingListArray.markAllAsTouched();
      // this.focusInAndOutFormArrayFields();
      return;
    };

    this.actualTimingList = this.getactualTimingListArray;
    let actualTimingListModel = new ActualTimingListModel(null);
    actualTimingListModel.appointmentId = this.config?.initiationDetailsData?.appointmentInfo?.appointmentId ? this.config?.initiationDetailsData?.appointmentInfo?.appointmentId : ''
    // actualTimingListModel.onRouteDate = this.config?.initiationDetailsData.appointmentInfo.scheduledStartDate ? this.config?.initiationDetailsData.appointmentInfo.scheduledStartDate : ""
    // actualTimingListModel.onRouteDate = actualTimingListModel.onRouteDate && actualTimingListModel.onRouteDate.split('T').length > 0 ? actualTimingListModel.onRouteDate.split('T')[0]: '';
    actualTimingListModel.onRouteDate = "";
    actualTimingListModel.createdUserId = this.config?.userData.userId ? this.config?.userData.userId : ''
    actualTimingListModel.modifiedUserId = this.config?.userData.userId ? this.config?.userData.userId : ''
    this.actualTimingList.insert(0, this.createactualTimingListModel(actualTimingListModel));

    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  removeActualTiming(i: number): void {
    if (!this?.config?.deletepermission) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.getactualTimingListArray.controls[i].value.appointmentActualTimingId) {
      this.getactualTimingListArray.removeAt(i);
      return;
    }
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {

      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.getactualTimingListArray.controls[i].value.appointmentActualTimingId && this.getactualTimingListArray.length > 1) {

        this.crudService.delete(ModulesBasedApiSuffix.TECHNICIAN, this.config?.data?.api,
          undefined,
          prepareRequiredHttpParams({
            appointmentActualTimingId: this.getactualTimingListArray.controls[i].value.appointmentActualTimingId,
            modifiedUserId: this.config?.userData.userId
          })
        ).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.getactualTimingListArray.removeAt(i);
            // this.isDuplicate = false;
            // this.isDuplicateNumber = false;
          }
          if (this.getactualTimingListArray.length === 0) {
            this.addActualTiming();
          };
        });
      }
      else {
        this.getactualTimingListArray.removeAt(i);
        // this.isDuplicate = false;
        // this.isDuplicateNumber = false;
      }
    });
    this.rxjsService.setFormChangeDetectionProperty(true);

  }

  onValidateTiming() {
    let findArr = [];
    this.actualTimingForm?.getRawValue()?.actualTimingList?.forEach((el: any, i: number) => {
      if (this.momentService.toHoursMints24(el?.onRouteTime) >= this.momentService.toHoursMints24(el?.startTime) && el?.onRouteTime && el?.startTime) {
        this.getactualTimingListArray?.controls[i]?.get('startTime').setErrors({lesserGreaterThan: true}, {emitEvent: false});
        this.getactualTimingListArray?.controls[i]?.get('onRouteTime').setErrors({lesserGreaterThan: true}, {emitEvent: false});
        // this.getactualTimingListArray?.controls[i]?.get('startTime').setValue(null, {emitEvent: false});
        // this.getactualTimingListArray?.controls[i]?.get('onRouteTime').setValue(null, {emitEvent: false});
        findArr.push(true);
      } else if (this.momentService.toHoursMints24(el?.startTime) >= this.momentService.toHoursMints24(el?.endTime) && el?.endTime && el?.startTime) {
        this.getactualTimingListArray?.controls[i]?.get('startTime').setErrors({lesserGreaterThan: true}, {emitEvent: false});
        this.getactualTimingListArray?.controls[i]?.get('endTime').setErrors({lesserGreaterThan: true}, {emitEvent: false});
        // this.getactualTimingListArray?.controls[i]?.get('startTime').setValue(null, {emitEvent: false});
        // this.getactualTimingListArray?.controls[i]?.get('endTime').setValue(null, {emitEvent: false});
        findArr.push(true);
      } else {
        this.getactualTimingListArray?.controls[i]?.get('onRouteTime').setErrors(null, {emitEvent: false});
        this.getactualTimingListArray?.controls[i]?.get('startTime').setErrors(null, {emitEvent: false});
        this.getactualTimingListArray?.controls[i]?.get('endTime').setErrors(null, {emitEvent: false});
      }
      this.getactualTimingListArray?.controls[i]?.get('onRouteTime').updateValueAndValidity({emitEvent: false});
      this.getactualTimingListArray?.controls[i]?.get('startTime').updateValueAndValidity({emitEvent: false});
      this.getactualTimingListArray?.controls[i]?.get('endTime').updateValueAndValidity({emitEvent: false});
    });
    return findArr?.length;
  }

  onSubmitActualTiming() {
    if (!this?.config?.addpermission) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.actualTimingForm.invalid) {
      this.actualTimingForm.markAllAsTouched();
      return
    }
    if (this.onValidateTiming()) {
      this.actualTimingForm.markAllAsTouched();
      this.snackbarService.openSnackbar('Start Time must be greater than On Route Time. End Time must be greater than Start Time.', ResponseMessageTypes.WARNING);
      return;
    }

    let formValue = this.actualTimingForm.value
    formValue.actualTimingList.forEach(element => {

      // if (element.onRouteDate) {
      //   let formateDate = element.onRouteDate.split("-")
      //   element.onRouteDate = element.onRouteDate ? formateDate[2] + '-' + formateDate[1] + '-' + formateDate[0] : null
      // }
      element.onRouteDate = element.onRouteDate ? this.momentService.isDateValid(element.onRouteDate) ? this.momentService.toFormateType(element.onRouteDate, 'YYYY-MM-DD') : element.onRouteDate : null
      element.startTime = element.startTime ? this.momentService.isDateValid(element.startTime) ? this.momentService.toFormateType(element.startTime, 'HH:mm') : element.startTime : null
      element.endTime = element.endTime ? this.momentService.isDateValid(element.endTime) ? this.momentService.toFormateType(element.endTime, 'HH:mm') : element.endTime : null
      element.onRouteTime = element.onRouteTime ? this.momentService.isDateValid(element.onRouteTime) ? this.momentService.toFormateType(element.onRouteTime, 'HH:mm') : element.onRouteTime : null
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, this.config?.api, formValue.actualTimingList)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.rxjsService.setDialogOpenProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          // this.actualTimeDialog = false
          // this.getCallInitiationDetailsById();
          this.dialogRef.close(true);
        }
      });

  }

}
