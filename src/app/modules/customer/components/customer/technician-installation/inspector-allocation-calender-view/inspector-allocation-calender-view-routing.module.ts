import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InspectorAllocationCalenderViewComponent } from './inspector-allocation-calender-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [ 
      { path:'', component: InspectorAllocationCalenderViewComponent, data: { title: 'Inspector Allocation Calender View' }, canActivate:[AuthGuard]},
      // { path:'view', component: TechnicianAllocationCalenderViewComponent, data: { title: 'Consumable Configuration View' }},
      // { path:'add-edit', component: TechnicianAllocationCalenderViewComponent, data: { title: 'Consumable Configuration Add Edit' }}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class InspectorAllocationCalenderViewRoutingModule { }
