
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { adjustDateFormatAsPerPCalendar, clearFormControlValidators, convertTreeToList, CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CURRENT_YEAR_TO_NEXT_FIFTY_YEARS, CustomDirectiveConfig, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PermissionTypes, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CUSTOMER_COMPONENT } from '@modules/customer';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { OutOfOfficeModuleApiSuffixModels } from '@modules/out-of-office/enums/out-of-office-employee-request.enum';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { CalendarDayViewBeforeRenderEvent, CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarEventTitleFormatter, CalendarMonthViewBeforeRenderEvent, CalendarView, CalendarWeekViewBeforeRenderEvent } from 'angular-calendar';
import { addMinutes, isSameDay, isSameMonth, startOfDay } from 'date-fns';
import { ConfirmationService } from 'primeng/api';
import { combineLatest, forkJoin, Observable, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { CustomEventTitleFormatter } from '../technician-allocation-calender-view/custom-event-title-formatter.provider';

enum FrequencyType {
  WEEKLY = 'Weekly',
  MONTHLY = 'Monthly',
  YEARLY = 'Yearly',
}

@Component({
  selector: 'app-inspector-allocation-calender-view',
  templateUrl: './inspector-allocation-calender-view.component.html',
  providers: [ConfirmationService,
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter,
    },]
})
export class InspectorAllocationCalenderViewComponent implements OnInit {
  refresh: Subject<any> = new Subject();

  loadingEvents: boolean = false
  showDialog: boolean
  showUnassignDialog: boolean
  filterForm: FormGroup
  showFilterForm: boolean = false
  scheduleForm: FormGroup
  searchTechForm: FormGroup
  appointmentFom: FormGroup
  unAssignedFom: FormGroup
  reAppointmentFom: FormGroup
  customerInspectionId: any
  nextAppointmentDate: any
  loggedUser: any
  selectedUser: any
  periodType: any = []
  activeDayIsOpen: boolean = false
  appointmentDate: any
  commentsDropDown: any = []
  estimatedTime: any
  customerInspectionAppointmentId: any
  // appointmentLogId: any
  selectedEventsByAppointmentId: any

  divisionList: any = []
  branchList: any = []
  techAreaList: any = []
  technitianList: any = []
  callType: any
  customerId: any
  showRescheduleDialog: boolean = false
  // reSchedule: boolean = false
  showRecurringScheduleDialog: boolean = false
  recurringScheduleFom: FormGroup
  frequencyList: any = []
  recurringPeriodList: any = []
  recurringWeeklyList: any = []

  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  isHypenOnly = new CustomDirectiveConfig({ isHypenOnly: true });
  isMltiDotsRestrict = new CustomDirectiveConfig({ isMltiDotsRestrict: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });

  validationData: any
  isPublicHolidayMsg: boolean
  showConfirmScheduleAlert: boolean = false
  initialLoad: boolean
  isNew: any;
  editClickAction: any

  // initialLoad: boolean
  calenderStartHour: any = '0'
  calenderEndHour: any = '23'
  isManualSelect: boolean = false
  isTechnicianManualSelect: boolean = false

  canBookAppointment: boolean = false
  isReschedule: boolean = false
  isAlertCheckbox: boolean = false
  isAlertCheckboxValue: boolean = false
  showResult: boolean = false
  customerInspectionNumber: string;
  actionPermissionObj: any = [];

  constructor(private _fb: FormBuilder, private momentService: MomentService, private snakbarService: SnackbarService, private confirmationService: ConfirmationService, private crudService: CrudService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private router: Router, private httpCancelService: HttpCancelService, private store: Store<AppState>) {
    this.customerInspectionId = this.activatedRoute.snapshot.queryParams.customerInspectionId;
    this.nextAppointmentDate = this.activatedRoute.snapshot.queryParams.nextAppointmentDate;
    this.viewDate = this.nextAppointmentDate ? new Date(this.nextAppointmentDate) : new Date();
    this.estimatedTime = this.activatedRoute.snapshot.queryParams.estimatedTime;
    this.customerInspectionAppointmentId = this.activatedRoute.snapshot.queryParams.customerInspectionAppointmentId;
    // this.appointmentLogId = this.activatedRoute.snapshot.queryParams.appointmentLogId;
    this.callType = this.activatedRoute.snapshot.queryParams.callType;
    this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
    this.isNew = this.activatedRoute.snapshot.queryParams.isNew ? this.activatedRoute.snapshot.queryParams.isNew.toLowerCase() : '';
    // if (this.customerInspectionAppointmentId) {
    this.periodType = [
      { id: 'day', displayName: 'Daily' },
      { id: 'week', displayName: 'Weekly' },
    ]
    // } else {
    //   this.periodType = [
    //     { id: 'day', displayName: 'Daily' },
    //   ]
    // }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    // this.periodType = [
    //   { id: 'day', displayName: 'Daily' },
    //   { id: 'week', displayName: 'Weekly' },
    //   { id: 'month', displayName: 'Monthly' },
    // ]
  }

  ngOnInit(): void {
    // this.initialLoad = true
    this.rxjsService.setGlobalLoaderProperty(false)
    this.combineLatestNgrxStoreData();
    this.createScheduleForm()
    this.createFilterForm()
    this.createSearchForm()
    this.createAppointmentForm()
    this.createReAppointmentForm()
    this.createUnassignedForm()
    let dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN,
        TechnicalMgntModuleApiSuffixModels.UX_FREQUENCY),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.REQUISITION_CONFIGURATION_PERIODS),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.REQUISITION_CONFIGURATION_WEEKLY_PERIODS)
    ];
    this.loadRecurringScheduleDropdownData(dropdownsAndData);
    this.createRecurringScheduleForm()
    this.searchKeywordRequest()
    this.scheduleFormRequest()
    this.getAllDropdownIds()
    this.getDivisionDropdown()
    this.getReasonDropdown()
    

  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$),]
    ).subscribe((response) => {
      let permission = response[0][CUSTOMER_COMPONENT.CUSTOMER]?.find(el => el?.menuName == CUSTOMER_COMPONENT.CUSTOMER_DASHBOARD);
      if (permission) {
        let techPermission = convertTreeToList([permission?.subMenu?.find(el => el?.menuName == CUSTOMER_COMPONENT.TECHNICAL)]);
        const objName = CUSTOMER_COMPONENT.INSPECTION
        this.actionPermissionObj = techPermission?.find(el => el?.[objName])?.[objName]?.find(el1 => el1?.menuName == CUSTOMER_COMPONENT?.APPOINTMENT_BOOKING);
      }
    });
  }

  findActionPermission(value: string) {
    return this.actionPermissionObj?.subMenu?.find(el => el?.menuName == value);
  }

  getAllDropdownIds() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTION_LOCATION, null, false,
      prepareGetRequestHttpParams(null, null, { CustomerInspectionId: this.customerInspectionId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.validationData = response.resources

          // response.resources.preferredTimeSlot = 'PM'
          if (this.validationData.appointmentStartTime) {
            this.calenderStartHour = this.validationData.appointmentStartTime.split(':')[0]
          }
          if (this.validationData.appointmentEndTime) {
            this.calenderEndHour = this.validationData.appointmentEndTime.split(':')[0]
          }
          this.filterForm.get('divisionId').setValue(this.validationData.divisionId ? { id: this.validationData.divisionId, displayName: this.validationData.divisionName } : '')
          this.filterForm.get('branchIds').setValue(this.validationData.branchId ? [{ branchId: response.resources.branchId, branchName: response.resources.branchName }] : '')
          this.filterForm.get('techAreaIds').setValue(this.validationData.techAreaId ? [{ id: response.resources.techAreaId, displayName: response.resources.techAreaName }] : '')
          this.filterForm.get('inspectorId').setValue(this.validationData.inspectorId ? { id: response.resources.inspectorId, displayName: response.resources.inspectorName } : '')
        } else {
          this.validationData = {
            technicianCallTypeId: null,
            techAreaId: null,
            technicianId: null,
            appointmentStartTime: '00:00:00',
            appointmentEndTime: null,
            divisionId: null,
            branchId: null,
            cutoffTime: null,
            preferredTimeSlot: null,
          }
        }
        this.reLoadData()
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createScheduleForm() {
    this.scheduleForm = this._fb.group({
      customerInspectionId: [this.customerInspectionId ? this.customerInspectionId : ''],
      userId: [this.loggedUser ? this.loggedUser.userId : ''],
      appointmentDate: [this.nextAppointmentDate ? this.nextAppointmentDate : new Date()],
      // scheduleType: [this.customerInspectionAppointmentId ? '1' : '2'],
      // periodType: [this.customerInspectionAppointmentId ? 'week' : 'day'],
      scheduleType: ['1'],
      periodType: ['week'],
      estimatedTime: [this.estimatedTime ? this.estimatedTime : '30'],
      estimatedTimeSelected: [this.estimatedTime ? this.estimatedTime : ''],
      search: ['']
    });
    this.onScheduleFormValueChanges();
    // let obj = this.scheduleForm.value
    // Object.keys(obj).forEach(key => {
    //   if (obj[key] === "") {
    //     delete obj[key]
    //   }
    // });
    // let otherParams = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    // return of(this.getInspectorList(otherParams));
  }

  onScheduleFormValueChanges() {
    this.scheduleForm.get('scheduleType').valueChanges.subscribe(val => {
      if (val == '1') {
        this.periodType = [
          { id: 'day', displayName: 'Daily' },
          { id: 'week', displayName: 'Weekly' },
        ]
        this.scheduleForm.get('periodType').setValue('week')
        this.setView('week')
      } else {
        this.periodType = [
          { id: 'day', displayName: 'Daily' },
        ]
        this.scheduleForm.get('periodType').setValue('day')
        this.setView('day')
      }
    })
  }

  createFilterForm() {
    this.filterForm = this._fb.group({
      divisionId: [''],
      branchIds: [[]],
      techAreaIds: [[]],
      inspectorId: [''],
    });
    if (!this.findActionPermission(PermissionTypes.FILTER)) {
      this.filterForm.disable();
    }
    this.onFilterFormValueChanges();
  }

  onFilterFormValueChanges() {
    this.filterForm.get('divisionId').valueChanges.subscribe(val => {
      if (val) {
        this.getBranchByDivisionId(val.id)
        if (this.isManualSelect) {
          this.filterForm.get('branchIds').setValue([])
          this.filterForm.get('techAreaIds').setValue([])
          this.filterForm.get('inspectorId').setValue('')
        }
      } else {
        // if (this.initialLoad) {
        //   this.reLoadData()
        // }
      }
    })
    this.filterForm.get('branchIds').valueChanges.subscribe(val => {
      if (val && val.length > 0) {
        let vals = []
        val.forEach(element => {
          vals.push(element.branchId)
        });
        // this.filterForm.get('techAreaIds').setValue([])
        // this.filterForm.get('inspectorId').setValue('')
        this.getTechAreaByBranchId(vals.toString())
        if (this.isManualSelect) {
          this.filterForm.get('techAreaIds').setValue([])
          this.filterForm.get('inspectorId').setValue('')
        }

      }
    })
    this.filterForm.get('techAreaIds').valueChanges.subscribe(val => {
      if (val && val.length > 0) {
        let vals = []
        val.forEach(element => {
          vals.push(element.id)
        });
        // this.filterForm.get('inspectorId').setValue('')
        this.getInspectorByTechAreaId(vals.toString())
        if (this.isManualSelect) {
          this.filterForm.get('inspectorId').setValue('')
        }

      }
    })
    this.filterForm.valueChanges.subscribe(val => {
      if (val) {
        if (this.isManualSelect) {
          this.showResult = true
        } else {
          this.showResult = false
        }
      }
    })
  }


  createAppointmentForm() {
    this.appointmentFom = this._fb.group({
      customerInspectionAppointmentId: [this.customerInspectionAppointmentId ? this.customerInspectionAppointmentId : ''],
      // appointmentLogId: [this.appointmentLogId ? this.appointmentLogId : ''],
      scheduledStartDate: ["", Validators.required],
      scheduledEndDate: ['', Validators.required],
      customerInspectionId: [this.customerInspectionId ? this.customerInspectionId : ''],
      createdUserId: [this.loggedUser ? this.loggedUser.userId : ''],
      modifiedUserId: [this.loggedUser ? this.loggedUser.userId : ''],
      appointmentDate: [this.nextAppointmentDate ? new Date(this.nextAppointmentDate) : new Date()],
    });
  }

  createReAppointmentForm() {
    this.reAppointmentFom = this._fb.group({
      customerInspectionAppointmentId: [this.customerInspectionAppointmentId ? this.customerInspectionAppointmentId : ''],
      // appointmentLogId: [this.appointmentLogId ? this.appointmentLogId : ''],
      scheduledStartDate: ["", Validators.required],
      scheduledEndDate: ['', Validators.required],
      customerInspectionId: [this.customerInspectionId ? this.customerInspectionId : ''],
      estimatedTime: [this.estimatedTime ? this.estimatedTime : ''],
      inspectorId: [''],
      isAppointmentChangable: [''],
      isSpecialAppointment: [''],
      createdUserId: [this.loggedUser ? this.loggedUser.userId : ''],
      modifiedUserId: [this.loggedUser ? this.loggedUser.userId : ''],
      appointmentDate: [this.nextAppointmentDate ? this.nextAppointmentDate : new Date()],
    });
  }

  createUnassignedForm() {
    this.unAssignedFom = this._fb.group({
      customerInspectionAppointmentId: [this.customerInspectionAppointmentId ? this.customerInspectionAppointmentId : null],
      customerInspectionId: [this.customerInspectionId ? this.customerInspectionId : null],
      appointmentDate: [this.appointmentDate ? this.appointmentDate : new Date()],
      comments: ["", Validators.required],
      scheduledStartDate: ["", Validators.required],
      scheduledEndDate: ['', Validators.required],
      isAppointmentChangable: [null],
      isFixedAppointment: [null],
      isSpecialAppointment: [null],
      isOverCapacity: [null],
      isReschedule: [null],
      isConfirm: [null],
      modifiedUserId: [this.loggedUser ? this.loggedUser.userId : ''],
    });
  }


  createRecurringScheduleForm() {
    this.recurringScheduleFom = this._fb.group({
      recurringcustomerInspectionAppointmentId: [""],
      customerInspectionId: [this.customerInspectionId ? this.customerInspectionId : null],
      // customerInspectionAppointmentId: [this.customerInspectionAppointmentId ? this.customerInspectionAppointmentId : null],
      recurringAppointmentFrequencyId: ["10", Validators.required],
      recurringAppointmentFrequencyName: ["Weekly", Validators.required],
      startDate: ["", Validators.required],
      endDate: ['', Validators.required],
      isNoEndDate: [false, Validators.required],
      recurType: [null],
      recurDay: ["", Validators.required],
      recurMonthNumber: [''],
      details: [[], Validators.required],
      requisitionWeekPeriodId: [''],
      createdUserId: [this.loggedUser ? this.loggedUser.userId : ''],
    });
    this.recurringScheduleFom.get('isNoEndDate').valueChanges.subscribe((event) => {
      if (event) {
        this.recurringScheduleFom = clearFormControlValidators(this.recurringScheduleFom, ["endDate"]);
      } else {
        this.recurringScheduleFom = setRequiredValidator(this.recurringScheduleFom, ['endDate']);
      }
    })
    this.recurringScheduleFom.get('recurringAppointmentFrequencyId').valueChanges.subscribe((typeId) => {
      let frequencyData = this.frequencyList.find(x => x.id == typeId)
      if (!frequencyData) {
        return
      }
      this.recurringScheduleFom.get('recurringAppointmentFrequencyName').setValue(frequencyData.displayName)
      if (frequencyData.displayName == FrequencyType.WEEKLY) {
        this.recurringScheduleFom = setRequiredValidator(this.recurringScheduleFom, ['recurDay', 'details']);
        this.recurringScheduleFom = clearFormControlValidators(this.recurringScheduleFom, ["requisitionWeekPeriodId", "recurMonthNumber"]);
        this.recurringScheduleFom.get('recurType').setValue(null)
        this.recurringScheduleFom.get('recurDay').setValue(null)
        this.recurringScheduleFom.get('details').setValue(null)
      } else if (frequencyData.displayName == FrequencyType.MONTHLY) {
        this.recurringScheduleFom.get('recurType').setValue(1)
      } else if (frequencyData.displayName == FrequencyType.YEARLY) {
        this.recurringScheduleFom.get('recurType').setValue(1)
        this.recurringScheduleFom.get('recurDay').setValue(1)
      }
    });
    this.recurringScheduleFom.get('recurType').valueChanges.subscribe((recurType) => {
      if (recurType == '1' || recurType == 1) {
        this.recurringScheduleFom = setRequiredValidator(this.recurringScheduleFom, ['recurDay', 'recurMonthNumber']);
        this.recurringScheduleFom.get('recurMonthNumber').setValidators([Validators.required, , Validators.min(1), Validators.max(12)]);
        this.recurringScheduleFom.get('recurDay').setValidators([Validators.required, , Validators.min(1), Validators.max(30)]);
        this.recurringScheduleFom = clearFormControlValidators(this.recurringScheduleFom, ["requisitionWeekPeriodId", "details"]);
        // this.recurringScheduleFom.get('recurDay').setValue(null)
        // this.recurringScheduleFom.get('recurMonthNumber').setValue(null)
        if (this.recurringScheduleFom.get('recurringAppointmentFrequencyName').value == FrequencyType.YEARLY) {
          this.recurringScheduleFom.get('recurDay').setValue(1)
        }
      } else if (recurType == '2' || recurType == 2) {
        this.recurringScheduleFom = setRequiredValidator(this.recurringScheduleFom, ['requisitionWeekPeriodId', 'details', 'recurMonthNumber']);
        this.recurringScheduleFom.get('recurMonthNumber').setValidators([Validators.required, , Validators.min(1), Validators.max(12)]);
        this.recurringScheduleFom = clearFormControlValidators(this.recurringScheduleFom, ["recurDay"]);
        // this.recurringScheduleFom.get('requisitionWeekPeriodId').setValue(null)
        // this.recurringScheduleFom.get('details').setValue(null)
        // this.recurringScheduleFom.get('recurMonthNumber').setValue(null)
      }
    })


  }

  loadRecurringScheduleDropdownData(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.frequencyList = resp.resources;
              break;
            case 1:
              // this.recurringPeriodList = resp.resources;
              resp.resources.forEach(element => {
                let data = { label: element.displayName, value: { requisitionPeriodId: element.id } }
                this.recurringPeriodList.push(data)
              });
              break;
            case 2:
              this.recurringWeeklyList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createSearchForm() {
    this.searchTechForm = this._fb.group({
      search: [''],
    });
  }

  scheduleFormRequest() {
    this.scheduleForm.valueChanges
      .pipe(
        // debounceTime(debounceTimeForSearchkeyword),
        // distinctUntilChanged(),
        switchMap(obj => {
          // Object.keys(obj).forEach(key => {
          //   if (obj[key] === "") {
          //     delete obj[key]
          //   }
          // });
          // let otherParams = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          // return of(this.getInspectorList(otherParams));
          return of(this.reLoadData())
        })
      )
      .subscribe();
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        // this.addConfirm = true;
        // this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      default:
    }
  }



  submitFilter() {
    if (!this.findActionPermission(PermissionTypes.FILTER)) {
      return this.snakbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR);
    }
    // let branchIds = []
    // let techAreaIds = []

    // if (this.filterForm.get('branchIds').value && this.filterForm.get('branchIds').value.length > 0) {
    //   this.filterForm.get('branchIds').value.forEach(element => {
    //     branchIds.push(element.branchId)
    //   });
    // }
    // if (this.filterForm.get('techAreaIds').value && this.filterForm.get('techAreaIds').value.length > 0) {
    //   this.filterForm.get('techAreaIds').value.forEach(element => {
    //     techAreaIds.push(element.id)
    //   });
    // }
    // let filteredData = Object.assign({},
    //   { divisionId: this.filterForm.get('divisionId').value ? this.filterForm.get('divisionId').value.id : '' },
    //   { branchIds: this.filterForm.get('branchIds').value ? branchIds.toString() : '' },
    //   { techAreaIds: this.filterForm.get('techAreaIds').value ? techAreaIds.toString() : '' },
    //   { inspectorId: this.filterForm.get('inspectorId').value ? this.filterForm.get('inspectorId').value.id : '' },
    // );
    // Object.keys(filteredData).forEach(key => {
    //   if (filteredData[key] === "" || filteredData[key].length == 0) {
    //     delete filteredData[key]
    //   }
    // });
    // let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    // Object.keys(this.scheduleForm.value).forEach(key => {
    //   if (this.scheduleForm.value[key] === "") {
    //     delete this.scheduleForm.value[key]
    //   }
    // });
    // let searchData = Object.assign({}, filterdNewData, this.scheduleForm.value)
    // let filterdNewData1 = Object.entries(searchData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

    // this.getInspectorList(filterdNewData1);

    this.showResult = false
    this.reLoadData()

    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    if (!this.findActionPermission(PermissionTypes.FILTER)) {
      return this.snakbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR);
    }
    this.filterForm.reset()
    this.branchList = []
    this.techAreaList = []
    this.technitianList = []
    this.showFilterForm = !this.showFilterForm;
    // Object.keys(this.scheduleForm.value).forEach(key => {
    //   if (this.scheduleForm.value[key] === "") {
    //     delete this.scheduleForm.value[key]
    //   }
    // });
    // let otherParams = Object.entries(this.scheduleForm.value).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    // return of(this.getInspectorList(otherParams));
    return of(this.reLoadData())
  }









  // appinement booking calender view starts
  view: CalendarView = CalendarView.Week;

  CalendarView = CalendarView;
  yesterday = this.momentService.getYesterday()
  minDate: Date = new Date(this.yesterday);


  setView(view) {
    this.view = view;
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  async onHoursSegmentClick(daytime) {
    let isAfter = this.momentService.isAfter(daytime)
    if (!isAfter) {
      this.snakbarService.openSnackbar('You can not schedule for previous day/time ')
      return
    }

    if (this.technitianList.length == 0) {
      this.snakbarService.openSnackbar('There is no available dates with suitable Technician for your Call')
      return
    }

    this.viewDate = new Date(daytime)

    let cutoffvalidate = await this.cutOffTimeValidation(daytime)
    if (cutoffvalidate) {
      return
    }
    if (this.customerInspectionAppointmentId && (this.isReschedule)) {
      this.editClickAction = 'Edited'
      let finalChek = await this.onReSelectTime(daytime)
      if (finalChek) {
        return
      }
      let publicHolidayvalidate = await this.isPublicHolidayValidation(daytime)
      this.isPublicHolidayMsg = publicHolidayvalidate
      this.showRescheduleDialog = true
    } else {
      let selectedEvent: any = this.selectedEventsByAppointmentId
      let isSameDay = selectedEvent ? this.momentService.isSameDay(selectedEvent.start, daytime) : true


      if (isSameDay || selectedEvent.isProvisionalBooking) { // if provisonal booking - call update api only whether same or otherday
        this.editClickAction = null
        let finalChek = await this.onSelectTime(daytime)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(daytime)
        this.isPublicHolidayMsg = publicHolidayvalidate
        // this.showDialog = true
      } else {

        // let selectedEvent: any = this.events.find(x => x.id == this.customerInspectionAppointmentId)
        let selectedEvent: any = this.selectedEventsByAppointmentId

        this.editClickAction = 'Edited'

        let unassignEstimationTime: any = this.momentService.getTimeDiff(selectedEvent.start, selectedEvent.end)
        this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

        let finalChek = await this.onSelectTimeUnassign(daytime)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(daytime)
        this.isPublicHolidayMsg = publicHolidayvalidate
        this.showUnassignDialog = true
        this.appointmentDate = daytime
        // selectedEvent.start = daytime;

        // for end time
        let addToMints1 = this.momentService.addMinits(daytime, this.scheduleForm.get('estimatedTime').value)
        // selectedEvent.end = new Date(addToMints1);
        //for end time

        // this.events = [...this.events];
        this.unAssignedFom.get('customerInspectionAppointmentId').setValue(selectedEvent.customerInspectionAppointmentId)
        // this.unAssignedFom.get('customerInspectionId').setValue(selectedEvent.customerInspectionId)
        this.unAssignedFom.get('appointmentDate').setValue(new Date(daytime))
        this.unAssignedFom.get('scheduledStartDate').setValue(daytime)
        this.unAssignedFom.get('scheduledEndDate').setValue(new Date(addToMints1))
        this.unAssignedFom.get('isReschedule').setValue(true)
      }


    }

    this.appointmentDate = daytime

  }

  async handleEvent(action: string, event: any) {
    this.viewDate = new Date(event.start)


    if (!event.isAppointmentChangable) {
      if (event.lable) {
        this.editClickAction = 'Edited'

        let unassignEstimationTime: any = this.momentService.getTimeDiff(event.start, event.end)
        this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

        let finalChek = await this.onSelectTimeUnassign(event.start)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(event.start)
        this.isPublicHolidayMsg = publicHolidayvalidate
        this.showUnassignDialog = true
        this.appointmentDate = event.start
        this.unAssignedFom.get('customerInspectionAppointmentId').setValue(event.customerInspectionAppointmentId)
        // this.unAssignedFom.get('appointmentLogId').setValue(event.appointmentLogId)
        // this.unAssignedFom.get('appointmentDate').setValue(event.start)
        this.unAssignedFom.get('isReschedule').setValue(false)

      }
      return
    }
    if (event.lable && event.isAppointmentChangable) {
      if (action === 'Clicked') {
        // this.onSelectTime(event.start)
        let finalChek = await this.onSelectTime(event.start)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(event.start)
        this.isPublicHolidayMsg = publicHolidayvalidate
        // this.appointmentFom.get('appointmentLogId').setValue(event.appointmentLogId)
        // this.showDialog = true
        this.appointmentDate = event.start

      } else if (action === 'Edited') {
        this.editClickAction = action

        let unassignEstimationTime: any = this.momentService.getTimeDiff(event.start, event.end)
        this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

        let finalChek = await this.onSelectTimeUnassign(event.start)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(event.start)
        this.isPublicHolidayMsg = publicHolidayvalidate
        this.showUnassignDialog = true
        this.appointmentDate = event.start
        this.unAssignedFom.get('customerInspectionAppointmentId').setValue(event.customerInspectionAppointmentId)
        // this.unAssignedFom.get('appointmentLogId').setValue(event.appointmentLogId)
        // this.unAssignedFom.get('appointmentDate').setValue(new Date(event.start))
        this.unAssignedFom.get('isReschedule').setValue(false)

      }

    }
  }

  async handleEventDragged(event, daytime, endTime) {
    // if (event.isProvisionalBooking) {
    //   this.snakbarService.openSnackbar('You can not Re-schedule without approved ')
    //   return
    // }
    let isAfter = this.momentService.isAfter(daytime)
    if (!isAfter) {
      this.snakbarService.openSnackbar('You can not schedule for previous day/time ')
      return
    }
    this.viewDate = new Date(daytime)

    let cutoffvalidate = await this.cutOffTimeValidation(daytime)
    if (cutoffvalidate) {
      return
    }

    if (!event.isAppointmentChangable) {
      if (event.lable) {
        this.editClickAction = 'Edited'

        let unassignEstimationTime: any = this.momentService.getTimeDiff(event.start, event.end)
        this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

        let finalChek = await this.onSelectTimeUnassign(event.start)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(event.start)
        this.isPublicHolidayMsg = publicHolidayvalidate
        this.showUnassignDialog = true
        this.appointmentDate = daytime
        event.start = daytime;
        event.end = endTime;
        this.events = [...this.events];
        this.unAssignedFom.get('customerInspectionAppointmentId').setValue(event.customerInspectionAppointmentId)
        // this.unAssignedFom.get('appointmentLogId').setValue(event.appointmentLogId)
        // this.unAssignedFom.get('appointmentDate').setValue(event.start)
        this.unAssignedFom.get('scheduledStartDate').setValue(event.start)
        this.unAssignedFom.get('scheduledEndDate').setValue(event.end)
        this.unAssignedFom.get('isReschedule').setValue(true)

      }
      return
    }
    if (event.lable && event.isAppointmentChangable) {

      let isSameDay = this.momentService.isSameDay(event.start, daytime)

      if (isSameDay) {
        // this.onSelectTime(event.start)
        let finalChek = await this.onSelectTime(event.start)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(event.start)
        this.isPublicHolidayMsg = publicHolidayvalidate
        // this.appointmentFom.get('appointmentLogId').setValue(event.appointmentLogId)
        // this.showDialog = true
        this.appointmentDate = daytime
        event.start = daytime;
        event.end = endTime;
        this.events = [...this.events];
        this.onAppointment()

      } else {
        this.editClickAction = 'Edited'

        let unassignEstimationTime: any = this.momentService.getTimeDiff(event.start, event.end)
        this.scheduleForm.get('estimatedTimeSelected').setValue(unassignEstimationTime)

        let finalChek = await this.onSelectTimeUnassign(event.start)
        if (finalChek) {
          return
        }
        let publicHolidayvalidate = await this.isPublicHolidayValidation(event.start)
        this.isPublicHolidayMsg = publicHolidayvalidate
        this.showUnassignDialog = true
        this.appointmentDate = daytime
        event.start = daytime;
        event.end = endTime;
        this.events = [...this.events];
        this.unAssignedFom.get('customerInspectionAppointmentId').setValue(event.customerInspectionAppointmentId)
        // this.unAssignedFom.get('appointmentLogId').setValue(event.appointmentLogId)
        // this.unAssignedFom.get('appointmentDate').setValue(new Date(event.start))
        this.unAssignedFom.get('scheduledStartDate').setValue(event.start)
        this.unAssignedFom.get('scheduledEndDate').setValue(event.end)
        this.unAssignedFom.get('isReschedule').setValue(true)

      }

    }
  }

  cutOffTimeValidation(startTime) {
    if (!this.validationData.cutoffTime) {
      return Promise.resolve(false);
    }
    // to check cuttoff time is exceed then current systme time 
    let time = this.momentService.convertNormalToRailayTime(new Date())
    // let time1 = this.momentService.convertNormalToRailayTime(new Date())
    let time1 = this.momentService.convertNormalToRailayTime(this.validationData.cutoffTime)
    let diff = this.momentService.timeDuration(time, time1)
    if (diff) {
      return Promise.resolve(false);
    } else {
      if (this.isRoleAccess()) {  // TAM can able to schedule
        this.showConfirmScheduleAlert = true
        return Promise.resolve(false);
      } else {

        // after one day we can schedule - validation starts here 
        var nextDay = this.momentService.addDays(new Date(), 1)
        let isSameDay = this.momentService.isSameDay(startTime, nextDay)
        let isToday = this.momentService.isSameDay(startTime, new Date())
        if (isSameDay) {          // this.snakbarService.openSnackbar('You can not schedule. Cutoff time is ' + this.momentService.convertRailayToNormalTime(this.validationData.cutoffTime).toLocaleUpperCase())
          this.snakbarService.openSnackbar('You can not schedule. Cutoff time is ' + this.validationData.cutoffTime)
          this.showConfirmScheduleAlert = false
          return Promise.resolve(true);
        } else if (isToday) {
          this.snakbarService.openSnackbar('Not allowed to schedule for current date')
          this.showConfirmScheduleAlert = false
          return Promise.resolve(true);
        } else {
          this.showConfirmScheduleAlert = false
          return Promise.resolve(false);
        }
        // after one day we can schedule - validation ends here 

      }
    }
  }

  timeExceedValidationOnchange(selectedFromTime, selectedToTim) {

    // appointmentStartTime and appointmentStartTime between time validation starts here 
    let isSameDay = this.momentService.isSameDay(selectedFromTime, selectedToTim)
    if (isSameDay) {
      var selectedToTime = selectedToTim
    } else {
      let selctTimeOnly = this.momentService.getTimeDiff(selectedFromTime, selectedToTim)
      var selectedToTime: any = this.momentService.setTimetoRequieredDate(selectedToTim, selctTimeOnly)
    }
    let add1Minuts = this.momentService.addMinutes(selectedFromTime, 1)
    let reduce1Minuts = this.momentService.reduceMinutes(selectedToTime, 1)

    let selectedFromTime1 = this.momentService.convertNormalToRailayTime(new Date(add1Minuts))
    let selectedToTime1 = this.momentService.convertNormalToRailayTime(new Date(reduce1Minuts))

    let selectedFromTimeValidation = this.momentService.betWeenTime(this.validationData.appointmentStartTime, this.validationData.appointmentEndTime, selectedFromTime1)

    // preferredTimeSlot validation start here
    if (this.validationData.preferredTimeSlot) {
      if (this.validationData.preferredTimeSlot.toLowerCase().includes('am')) {
        if (this.isAdminRole()) {  // TAM can able to schedule
          this.validationData.appointmentEndTime = this.validationData.appointmentEndTime;
        } else {
          this.validationData.appointmentEndTime = '11:59:00';
        }
        var alertmsge = 'You can able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData.appointmentStartTime).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData.appointmentEndTime).toLocaleUpperCase()
        var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData.appointmentStartTime, '11:59:00', selectedToTime1)
      } else {
        var alertmsge = 'You can able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData.appointmentStartTime).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData.appointmentEndTime).toLocaleUpperCase()
        var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData.appointmentStartTime, this.validationData.appointmentEndTime, selectedToTime1)
      }
    } else {
      var alertmsge = 'You can able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData.appointmentStartTime).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData.appointmentEndTime).toLocaleUpperCase()
      var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData.appointmentStartTime, this.validationData.appointmentEndTime, selectedToTime1)
    }
    // preferredTimeSlot validation ends here

    if (isSameDay && selectedFromTimeValidation && selectedToTimeValidation) {
      this.showConfirmScheduleAlert = false
      return Promise.resolve(false);
    } else {
      if (this.isAdminRole()) {
        if (isSameDay) {
          this.splitSingleAppointment(selectedFromTime, selectedToTime)
          this.showConfirmScheduleAlert = false
          return Promise.resolve(false);
        } else {
          // ->   this.splitMultipleAppointment(selectedFromTime, selectedToTime)
          // return Promise.resolve(false);
        }
      } else {
        // let isMultipleappointment = this.isMultipleAppointmentCheck(selectedFromTime)
        // if (!isMultipleappointment) {
        //   this.showConfirmScheduleAlert = false
        //   return Promise.resolve(false);
        // } else {
        if (this.isAdminRole()) {  // TAM can able to schedule
          this.showConfirmScheduleAlert = true
          return Promise.resolve(false);
        } else {
          this.snakbarService.openSnackbar(alertmsge)
          this.showConfirmScheduleAlert = false
          return Promise.resolve(true);
        }
        // }
      }
    }
  }


  timeExceedValidation(selectedFromTime, selectedToTim) {
    // appointmentStartTime and appointmentStartTime between time validation starts here 
    let isSameDay = this.momentService.isSameDay(selectedFromTime, selectedToTim)
    if (isSameDay) {
      var selectedToTime = selectedToTim
    } else {
      let selctTimeOnly = this.momentService.getTimeDiff(selectedFromTime, selectedToTim)
      var selectedToTime: any = this.momentService.setTimetoRequieredDate(selectedToTim, selctTimeOnly)
    }
    let addMinuts = this.momentService.addMinutes(selectedFromTime, 1)
    let reduce1Minuts = this.momentService.reduceMinutes(selectedToTime, 1)

    let selectedFromTime1 = this.momentService.convertNormalToRailayTime(new Date(addMinuts))
    let selectedToTime1 = this.momentService.convertNormalToRailayTime(new Date(reduce1Minuts))

    let selectedFromTimeValidation = this.momentService.betWeenTime(this.validationData.appointmentStartTime, this.validationData.appointmentEndTime, selectedFromTime1)

    // preferredTimeSlot validation start here
    if (this.validationData.preferredTimeSlot) {
      if (this.validationData.preferredTimeSlot.toLowerCase().includes('am')) {
        if (this.isAdminRole()) {  // TAM can able to schedule
          this.validationData.appointmentEndTime = this.validationData.appointmentEndTime;
        } else {
          this.validationData.appointmentEndTime = '11:59:00';
        }
        var alertmsge = 'You are only able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData.appointmentStartTime).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData.appointmentEndTime).toLocaleUpperCase()
        var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData.appointmentStartTime, '11:59:00', selectedToTime1)
      } else {
        var alertmsge = 'You are only able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData.appointmentStartTime).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData.appointmentEndTime).toLocaleUpperCase()
        var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData.appointmentStartTime, this.validationData.appointmentEndTime, selectedToTime1)
      }
    } else {
      var alertmsge = 'You are only able to schedule between ' + this.momentService.convertRailayToNormalTime(this.validationData.appointmentStartTime).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(this.validationData.appointmentEndTime).toLocaleUpperCase()
      var selectedToTimeValidation = this.momentService.betWeenTime(this.validationData.appointmentStartTime, this.validationData.appointmentEndTime, selectedToTime1)
    }
    // preferredTimeSlot validation ends here


    if (isSameDay && selectedFromTimeValidation && selectedToTimeValidation) {
      this.splitSingleAppointment(selectedFromTime, selectedToTime)
      this.showConfirmScheduleAlert = false
      this.showDialog = this.editClickAction === 'Edited' ? false : true
      return Promise.resolve(false);
    } else {
      // this.snakbarService.openSnackbar('Can not schedule from ' + this.momentService.convertRailayToNormalTime(selectedFromTime).toLocaleUpperCase() + ' to ' + this.momentService.convertRailayToNormalTime(selectedToTime).toLocaleUpperCase() + '. Schedule time must be lesser than ' + this.momentService.convertRailayToNormalTime(this.validationData.cutoffTime).toLocaleUpperCase())

      if (this.isAdminRole()) {
        if (isSameDay) {
          this.splitSingleAppointment(selectedFromTime, selectedToTime)
          this.showConfirmScheduleAlert = false
          this.showDialog = this.editClickAction === 'Edited' ? false : true
          return Promise.resolve(false);
        } else {
          // -> this.splitMultipleAppointment(selectedFromTime, selectedToTime)
          return Promise.resolve(false);
        }
      } else {
        // same block start
        // let isMultipleappointment = this.isMultipleAppointmentCheck(selectedFromTime)
        // if (!isMultipleappointment) {
        //   this.splitMultipleAppointment(selectedFromTime, selectedToTime)
        //   this.showConfirmScheduleAlert = false
        //   this.showDialog = this.editClickAction === 'Edited' ? false : true
        //   return Promise.resolve(false);
        // } else {
        if (this.isAdminRole()) {  // TAM can able to schedule
          this.splitSingleAppointment(selectedFromTime, selectedToTime)
          this.showConfirmScheduleAlert = true
          this.showDialog = this.editClickAction === 'Edited' ? false : true
          return Promise.resolve(false);
        } else {
          this.snakbarService.openSnackbar(alertmsge)
          this.showConfirmScheduleAlert = false
          return Promise.resolve(true);
        }
        // }
        // same block ends

      }


    }
  }

  isPublicHolidayValidation(selectedDate) {
    this.users.forEach(element => {
      element.isSameDay = this.momentService.isSameDay(element.start, selectedDate)
    });
    let checkHoliday = this.users.find(x => x.isSameDay)
    if (checkHoliday) {
      return Promise.resolve(checkHoliday.isPublicHoliday);
    } else {
      // return Promise.resolve(checkHoliday.isPublicHoliday);
      return Promise.resolve(false);
    }
  }

  splitSingleAppointment(startDate, endDate) {
    this.appointmentFom.get('scheduledStartDate').setValue(startDate)
    this.appointmentFom.get('scheduledEndDate').setValue(endDate)
    this.appointmentFom.get('appointmentDate').setValue(endDate)
  }



  // popup sheculed function 

  async onSelectTime(event) {
    // let addFromMints = this.momentService.toHoursMints24(event)
    // this.appointmentFom.get('scheduledStartDate').setValue(event)
    // let addToMints = this.momentService.toHoursMints12(event)
    let addToMints1 = this.momentService.addMinits(event, this.scheduleForm.get('estimatedTime').value)
    // this.appointmentFom.get('scheduledEndDate').setValue(new Date(addToMints1))

    let scheduledStartDate = event
    let scheduledEndDate = new Date(addToMints1)
    // let bookedSlotsModelData: BookedSlotsListModel = {
    //   appointmentDate: event,
    //   scheduledStartDate: event,
    //   scheduledEndDate: scheduledEndDate,
    //   estimatedTime: this.estimatedTime
    // }
    // this.getBookedSlotsListArray.clear()
    // this.getBookedSlotsListArray.push(this.createBookedSlotsListModel(bookedSlotsModelData));


    let timeExceedvalidate = await this.timeExceedValidation(scheduledStartDate, scheduledEndDate)
    if (timeExceedvalidate) {
      // this.appointmentFom.get('scheduledEndDate').setValue(null)
      return Promise.resolve(true);
    } else {
      return Promise.resolve(false);
    }
  }

  async onReSelectTime(event) {
    // let addFromMints = this.momentService.toHoursMints24(event)
    this.reAppointmentFom.get('scheduledStartDate').setValue(event)

    // let addToMints = this.momentService.toHoursMints12(event)
    let addToMints1 = this.momentService.addMinits(event, this.scheduleForm.get('estimatedTime').value)
    this.reAppointmentFom.get('scheduledEndDate').setValue(new Date(addToMints1))
    let timeExceedvalidate = await this.timeExceedValidation(this.reAppointmentFom.get('scheduledStartDate').value, this.reAppointmentFom.get('scheduledEndDate').value)
    if (timeExceedvalidate) {
      this.reAppointmentFom.get('scheduledEndDate').setValue(null)
      return Promise.resolve(true);
    } else {
      return Promise.resolve(false);
    }

  }

  async onSelectTimeUnassign(event, val?) {
    // let addFromMints = this.momentService.toHoursMints24(event)
    this.unAssignedFom.get('scheduledStartDate').setValue(event)

    // let addToMints = this.momentService.toHoursMints12(event)
    let addToMints1 = this.momentService.addMinits(event, this.scheduleForm.get('estimatedTimeSelected').value)
    this.unAssignedFom.get('scheduledEndDate').setValue(new Date(addToMints1))
    let timeExceedvalidate = await this.timeExceedValidation(this.unAssignedFom.get('scheduledStartDate').value, this.unAssignedFom.get('scheduledEndDate').value)
    if (timeExceedvalidate) {
      this.unAssignedFom.get('scheduledEndDate').setValue(null)
      return Promise.resolve(true);
    } else {
      return Promise.resolve(false);
    }
  }


  isAccessCheck(selectedFromTime) {
    let reduce1Minuts = this.momentService.addMinutes(selectedFromTime, 1)
    let selectedFromTime1 = this.momentService.convertNormalToRailayTime(new Date(reduce1Minuts))
    // let selectedFromTimeValidation = this.momentService.betWeenTime(this.validationData.appointmentStartTime, this.validationData.appointmentEndTime, selectedFromTime1)
    let selectedFromTimeValidation = this.momentService.betWeenTime('00:00:00', this.validationData.appointmentEndTime, selectedFromTime1)
    return selectedFromTimeValidation
  }

  isAdminRole() {
    // if (this.loggedUser.roleName.toLowerCase() == 'technical area manager' || this.loggedUser.roleName.toLowerCase() == 'super admin') {  // TAM can able to schedule
    //   return true
    // } else {
    //   return false
    // }
    return false
  }

  isRoleAccess() {
    // if (this.loggedUser.roleName.toLowerCase() == 'technical area manager' || this.loggedUser.roleName.toLowerCase() == 'super admin') {  // TAM can able to schedule
    //   return true
    // } else {
    //   return false
    // }
    return false
  }


  checkTechnicianAvailability(type) {
    let branchIds = []
    let techAreaIds = []

    if (this.filterForm.get('branchIds').value && this.filterForm.get('branchIds').value.length > 0) {
      this.filterForm.get('branchIds').value.forEach(element => {
        branchIds.push(element.branchId)
      });
    }
    if (this.filterForm.get('techAreaIds').value && this.filterForm.get('techAreaIds').value.length > 0) {
      this.filterForm.get('techAreaIds').value.forEach(element => {
        techAreaIds.push(element.id)
      });
    }
    let filteredData = Object.assign({},
      { divisionId: this.filterForm.get('divisionId').value ? this.filterForm.get('divisionId').value.id : '' },
      { branchIds: this.filterForm.get('branchIds').value ? branchIds.toString() : '' },
      { techAreaIds: this.filterForm.get('techAreaIds').value ? techAreaIds.toString() : '' },
      { inspectorId: this.filterForm.get('inspectorId').value ? this.filterForm.get('inspectorId').value.id : '' },
      // { includeSpecialProjectTechnician: this.filterForm.get('includeSpecialProjectTechnician').value ? this.filterForm.get('includeSpecialProjectTechnician').value : false },
      // { callInitiationId: this.installationId },
      { CustomerInspectionId: this.customerInspectionId },
      { estimatedTime: this.estimatedTime },
      // { appointmentFrom: this.appointmentFom.get('bookedSlots').value[0].appointmentTimeFrom },
      // { appointmentTo: this.appointmentFom.get('bookedSlots').value[lastIndex].appointmentTimeTo },
      { appointmentFrom: this.momentService.toFormateType(this.appointmentFom.get('scheduledStartDate').value, 'YYYY-MM-DD HH:mm') },
      // { appointmentTo: this.momentService.toFormateType(this.appointmentFom.get('bookedSlots').value[lastIndex].appointmentTimeTo, 'YYYY-MM-DD HH:mm') },
      { appointmentTo: this.momentService.toFormateType(this.appointmentFom.get('scheduledEndDate').value, 'YYYY-MM-DD HH:mm') },
    );
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => ((v == null || v == '') ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.INSPECTOR_AVAILABILITY, null, false,
      prepareGetRequestHttpParams(null, null, filterdNewData))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess) {
          if (response.resources.canBookCall) {

            //auto appointment start here
            if (response.resources.inspectors.length > 0) {
              this.filterForm.get('inspectorId').setValue({ id: response.resources.inspectors[0].inspectorId, displayName: response.resources.inspectors[0].inspectorName })

            } else {

            }
            //auto appointment ends here

            if (type == 1) { //appointmen
              if (this.showConfirmScheduleAlert) {
                this.confirmSchedule()
              } else {
                this.doAppintment()
              }
            } else if (type == 2) {
              this.doReAppointment()
            }
          } else {
            this.snakbarService.openSnackbar(response.resources.alertMessage ? response.resources.alertMessage : 'There is no available dates with suitable Technician for your Call')
            this.reLoadData()
          }

        }
      });
  }

  openAlertDialog(msg) {
    // this.alertDialog = true
    // this.alertMsg = msg
  }

  onAppointment() {
    if (this.appointmentFom.invalid) {
      return
    }
    this.showDialog = false

    // this.confirmSchedule()
    this.checkTechnicianAvailability(1)

  }

  onUnassigne() {
    if (this.unAssignedFom.invalid) {
      return
    }
    this.showUnassignDialog = false
    let formValue = this.unAssignedFom.value
    formValue.customerInspectionId = this.customerInspectionId
    formValue.appointmentDate = this.appointmentDate
    formValue.scheduledStartDate = this.convertData(formValue.scheduledStartDate)
    formValue.scheduledEndDate = this.convertData(formValue.scheduledEndDate)
    formValue.inspectorId = this.scheduleForm.get('scheduleType').value == '1' ? this.filterForm.get('inspectorId').value?.id : '',
      formValue.isFixedAppointment = (formValue.inspectorId && this.isTechnicianManualSelect) ? true : false,

      // formValue.scheduledStartDate = this.momentService.toHoursMints24(formValue.scheduledStartDate)
      // formValue.scheduledEndDate = this.momentService.toHoursMints24(formValue.scheduledEndDate)
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    let crudService: Observable<IApplicationResponse> = (!this.unAssignedFom.get('customerInspectionAppointmentId').value) ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.INSPECTION_APPOINTMETNTS_UNASSIGN, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.INSPECTION_UNASSIGN_RESCHEDULE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        // this.reSchedule = true
        // this.customerInspectionAppointmentId = null;
        // this.appointmentLogId = null;
        this.unAssignedFom.reset()
        this.unAssignedFom.get('modifiedUserId').setValue(this.loggedUser ? this.loggedUser.userId : '')
        // let obj = this.scheduleForm.value
        // Object.keys(obj).forEach(key => {
        //   if (obj[key] === "") {
        //     delete obj[key]
        //   }
        // });
        // let otherParams = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
        // return of(this.getInspectorList(otherParams));
        return of(this.reLoadData())
      }
    })

  }

  onReAppointment() {
    if (this.reAppointmentFom.invalid) {
      return
    }
    this.showRescheduleDialog = false
    this.checkTechnicianAvailability(2)

  }

  doReAppointment() {
    let formValue = this.reAppointmentFom.value
    formValue.customerInspectionAppointmentId = this.customerInspectionAppointmentId ? this.customerInspectionAppointmentId : null
    formValue.appointmentDate = this.appointmentDate
    formValue.scheduledStartDate = formValue.scheduledStartDate.toLocaleString()
    formValue.scheduledEndDate = formValue.scheduledEndDate.toLocaleString()
    formValue.inspectorId = this.scheduleForm.get('scheduleType').value == '1' ? this.filterForm.get('inspectorId').value.id : '',
      formValue.isAppointmentChangable = formValue.inspectorId ? false : true,
      // formValue.isSpecialAppointment = '',

      this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.INSPECTION_APPOINTMETNTS_RESCHEDULE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        // if (this.callType == '2') {
        // this.reSchedule = false
        if (response.resources) {
          // let jsonStri = JSON.parse(response.resources)
          let jsonStri = response.resources
          // this.customerInspectionAppointmentId = jsonStri
          this.customerInspectionAppointmentId = jsonStri?.customerInspectionAppointmentId
          // this.appointmentLogId = jsonStri.AppointmentLogId
        }
        // }
        // let obj = this.scheduleForm.value
        // Object.keys(obj).forEach(key => {
        //   if (obj[key] === "") {
        //     delete obj[key]
        //   }
        // });
        // let otherParams = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
        // return of(this.getInspectorList(otherParams));
        return of(this.reLoadData())
      }
    })

  }

  // appinement booking calender view ends


  // tech allocation calender view starts

  viewDate = new Date();

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-pencil"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      },
    },
    // {
    //   label: '<i class="fa fa-trash"></i>',
    //   a11yLabel: 'Delete',
    //   onClick: ({ event }: { event: CalendarEvent }): void => {
    //     this.events = this.events.filter((iEvent) => iEvent !== event);
    //     this.handleEvent('Deleted', event);
    //   },
    // },
  ];

  // users = users;
  users = [];

  colors: any = {
    red: {
      primary: '#ad2121',
      secondary: '#FAE3E3',
    },
    blue: {
      primary: '#1e90ff',
      secondary: '#D1E8FF',
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA',
    },
    orange: {
      primary: '#ffa500',
      secondary: '#fbe2b2',
    },
    green: {
      primary: '#006A54',
      secondary: '#e8fde7',
    },
  };

  events: CalendarEvent[] = []

  beforeMonthViewRender(renderEvent: CalendarMonthViewBeforeRenderEvent): void {
    renderEvent.body.forEach((day) => {
      const dayOfMonth = day.date.getDate();
      // if (dayOfMonth > 5 && dayOfMonth < 10 && day.inMonth) {
      // day.cssClass = 'calendar-bg-red';
      // }
    });
  }

  beforeWeekViewRender(renderEvent: CalendarWeekViewBeforeRenderEvent) {
    renderEvent.hourColumns.forEach((hourColumn, index) => {

      hourColumn.hours.forEach((hour) => {
        hour.segments.forEach((segment) => {

          var dayIndex = hourColumn.events.length > 0 ? (hourColumn.events[0]?.event['isPublicHoliday'] ? index : null) : null

          if (
            // segment.date.getHours() >= 9 &&
            // segment.date.getHours() <= 12 &&
            segment.date.getDay() === 0 ||
            segment.date.getDay() === 6 ||
            segment.date.getDay() === dayIndex
          ) {
            segment.cssClass = 'calendar-bg-red';
          }
        });
      });
    });
  }

  beforeDayViewRender(renderEvent: CalendarDayViewBeforeRenderEvent) {
    renderEvent.hourColumns.forEach((hourColumn, index) => {
      hourColumn.hours.forEach((hour) => {
        hour.segments.forEach((segment) => {
          var dayIndex = hourColumn.events.length > 0 ? (hourColumn?.events[0]?.event['isPublicHoliday'] ? index : null) : null
          if (
            // segment.date.getHours() >= 9 &&
            // segment.date.getHours() <= 12 &&
            segment.date.getDay() === 0 ||
            segment.date.getDay() === 6 ||
            segment.date.getDay() === dayIndex
          ) {
            segment.cssClass = 'calendar-bg-red';
          }
        });
      });
    });
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {
    if (!newStart || !newEnd) {
      return
    }
    if (newStart.getDay() === 0 || newStart.getDay() === 6 || newEnd.getDay() === 0 || newEnd.getDay() === 6) {
      return;
    }
    // if (newStart < this.minDate) {
    //   return
    // }
    let isAfter = this.momentService.isAfter(newStart)
    if (!isAfter) {
      this.snakbarService.openSnackbar('You can not schedule for previous day/time ')
      return
    }
    let isSameDay = this.momentService.isSameDayTime(event.start, newStart)
    if (isSameDay) {
      return
    }
    // event.start = newStart;
    // event.end = newEnd;
    // this.events = [...this.events];
    // this.refresh.next();

    // this.onHoursSegmentDragged(event, newStart, newEnd)
    this.handleEventDragged(event, newStart, newEnd)

  }

  userChanged({ event, newUser }) {
    // event.color = newUser.color;
    event.meta.user = newUser;
    this.events = [...this.events];
  }

  userSelected(user) {
    if (user.isSelectable)
      this.selectedUser = user
  }

  onDayChange() {

    this.scheduleForm.get('appointmentDate').setValue(this.viewDate)
  }


  convertData(date) {// Fri Feb 20 2015 19:29:31 GMT+0530 (India Standard Time) 
    var isoDate = new Date(date.getTime() - date.getTimezoneOffset() * 60000).toISOString();
    //OUTPUT : 2015-02-20T19:29:31.238Z

    return isoDate
  }

  searchKeywordRequest() {
    this.searchTechForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          this.scheduleForm.get('search').setValue(val.search)
          // Object.keys(this.scheduleForm.value).forEach(key => {
          //   if (this.scheduleForm.value[key] === "") {
          //     delete this.scheduleForm.value[key]
          //   }
          // });
          // Object.keys(this.filterForm.value).forEach(key => {
          //   if (this.filterForm.value[key] === "" || this.filterForm.value[key]) {
          //     delete this.filterForm.value[key]
          //   }
          // });
          // let filterdNewData = Object.entries(this.filterForm.value).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

          // let searchData = Object.assign({}, filterdNewData, this.scheduleForm.value)
          // let filterdNewData1 = Object.entries(searchData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

          // return of(this.getInspectorList(filterdNewData1));
          return of(this.reLoadData())
        })
      )
      .subscribe();
  }

  getInspectorList(otherParams?: object) {
    if (Object.keys(otherParams).length > 0) {
      // logic for split columns and its values to key value pair

      Object.keys(otherParams).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          otherParams[key] = this.momentService.localToUTCDateTime(otherParams[key])
          // otherParams[key] =  this.convertData(otherParams[key])


          // otherParams[key] = this.momentService.localToUTC(otherParams[key]);
        } else {
          otherParams[key] = otherParams[key];
        }
      });
    }
    // this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.loadingEvents = true
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, otherParams['scheduleType'] == '1' ? OutOfOfficeModuleApiSuffixModels.INSPECTION_APPOINTMETNT_CALENDER : OutOfOfficeModuleApiSuffixModels.INSPECTION_APPOINTMETNT_TECHNICIAN_CALENDER, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        // this.initialLoad = false
        this.loadingEvents = false
        if (response.resources) {
          this.canBookAppointment = response.resources.canBookAppointment
          this.isReschedule = response.resources.showRescheduleDialog
          this.selectedUser = null
          this.events = []
          this.users = []
          // if (this.isReschedule) {
          //   this.customerInspectionAppointmentId = this.customerInspectionAppointmentId ? this.customerInspectionAppointmentId : response.resources.customerInspectionAppointmentId
          // }
          this.customerInspectionId = response.resources?.customerInspectionId ? response.resources?.customerInspectionId : this.customerInspectionId
          this.customerInspectionAppointmentId = response.resources?.customerInspectionAppointmentId ? response.resources?.customerInspectionAppointmentId : this.customerInspectionAppointmentId
          if (this.scheduleForm.get('scheduleType').value == '1') {
            this.customerInspectionNumber = response?.resources?.customerInspectionNumber;
            response.resources.rows.forEach(element => {
              element.customerInspectionAppointmentId = response.resources.customerInspectionAppointmentId
              element.id = element.inspectorId,
                element.name = element.inspectorName,
                element.title = response.resources.customerInspectionNumber,
                element.start = element.appointmentDate ? new Date(element.appointmentDate) : null,
                element.end = element.appointmentDate ? new Date(element.appointmentDate) : null
              // element.allDay = true
              if (element.isPublicHoliday) {
                this.events.push({
                  start: element.appointmentDate ? new Date(element.appointmentDate) : null,
                  title: element?.holidayName ? element.holidayName : 'Public Holiday',
                  allDay: true,
                  meta: {
                    type: 'holiday',
                  },
                  color: this.colors.green
                })
              }
              if (element.appointments.length > 0) {
                element.appointments.forEach(element1 => {
                  let fromHour = element1.scheduledStartDate ? element1.scheduledStartDate.split(':') : null
                  let toHour = element1.scheduledEndDate ? element1.scheduledEndDate.split(':') : null
                  let existAppoitmnt = {
                    id: element1.customerInspectionAppointmentId,
                    customerInspectionAppointmentId: element1.customerInspectionAppointmentId,
                    // appointmentLogId: element1.appointmentLogId,
                    scheduledStartDate: element1.scheduledStartDate,
                    scheduledEndDate: element1.scheduledEndDate,
                    isAppointmentChangable: element1.isAppointmentChangable,
                    isSpecialAppointment: element1.isSpecialAppointment,
                    isProvisionalBooking: element1.isProvisionalBooking,
                    isPublicHoliday: element.isPublicHoliday,
                    hoverText: element1.hoverText,
                    lable: element1.customerInspectionNumber,
                    name: null,
                    title: ' <span title="' + element1.hoverText + '">' + element1.customerInspectionNumber + ' </span> ',
                    // start: element1.scheduledStartDate ? new Date(this.momentService.addHoursMinits(element.appointmentDate, fromHour[0], fromHour[1])) : null,
                    // end: element1.scheduledEndDate ? new Date(this.momentService.addHoursMinits(element.appointmentDate, toHour[0], toHour[1])) : null,
                    start: element1.scheduledStartDate ? new Date(element1.scheduledStartDate) : null,
                    end: element1.scheduledEndDate ? new Date(element1.scheduledEndDate) : null,
                    // color: element1.isAppointmentChangable ? this.colors.red : this.colors.blue,
                    color: element1.cssClass === 'status-label-blue' ? this.colors.blue : element1.cssClass === 'status-label-red' ? this.colors.red : element1.cssClass === 'status-label-orange' ? this.colors.orange : null,
                    actions: element1.customerInspectionNumber ? this.actions : null,
                    draggable: element1.customerInspectionNumber ? true : false,
                  }
                  this.events.push(existAppoitmnt)
                })
              } else {
                //showing holiday background color purpose only stars here
                if (element.isPublicHoliday) {
                  let existAppoitmnt = {
                    isPublicHoliday: element.isPublicHoliday,
                    start: element.appointmentDate ? startOfDay(new Date(element.appointmentDate)) : null,
                    end: element.appointmentDate ? addMinutes(new Date(element.appointmentDate), 1) : null,
                    color: this.colors.red,
                    title: '',
                    id: null,
                    customerInspectionAppointmentId: null,
                    appointmentLogId: null,
                    scheduledStartDate: null,
                    scheduledEndDate: null,
                    isAppointmentChangable: null,
                    isSpecialAppointment: null,
                    isProvisionalBooking: null,
                    hoverText: null,
                    lable: '',
                    name: null,
                    draggable: false,
                  }
                  this.events.push(existAppoitmnt)
                }
                //showing holiday background color purpose only ends here
              }
            });
            this.users = response.resources.rows
            // this.events = response.resources.rows


            let exist = this.customerInspectionAppointmentId ? this.events.find(x => x.id == this.customerInspectionAppointmentId) : null
            if (exist) {
              this.selectedEventsByAppointmentId = exist
            } else {
              this.selectedEventsByAppointmentId = this.selectedEventsByAppointmentId
            }


          } else {
            response.resources.rows.forEach(element => {
              element.customerInspectionAppointmentId = response.resources.customerInspectionAppointmentId
              element.id = element.inspectorId,
                element.name = element.inspectorName,
                // element.title = response.resources.customerInspectionNumber,
                // element.start = element.appointmentDate ? new Date(element.appointmentDate) : null,
                // element.end = element.appointmentDate ? new Date(element.appointmentDate) : null
                element.meta = {
                  user: {
                    id: element.inspectorId,
                    name: element.inspectorName
                  }
                }
              if (element.appointments.length > 0) {
                element.appointments.forEach(element1 => {
                  // let fromHour = element1.scheduledStartDate ? element1.scheduledStartDate.split(':') : null
                  // let toHour = element1.scheduledEndDate ? element1.scheduledEndDate.split(':') : null
                  let existAppoitmnt = {
                    id: element1.customerInspectionAppointmentId,
                    lable: element1.customerInspectionNumber,
                    name: null,
                    // title: element1.customerInspectionNumber,
                    title: ' <span title="' + element1.hoverText + '">' + element1.customerInspectionNumber + ' </span> ',
                    start: element1.scheduledStartDate ? new Date(element1.scheduledStartDate) : null,
                    end: element1.scheduledEndDate ? new Date(element1.scheduledEndDate) : null,
                    // start: new Date(this.momentService.addHoursMinits(element1.appointmentDate, "10", "0")),
                    // end:  new Date(this.momentService.addHoursMinits(element1.appointmentDate, '10', "30")),
                    // color: element1.isSpecialAppointment ? this.colors.blue : this.colors.red,
                    color: element1.cssClass === 'status-label-blue' ? this.colors.blue : element1.cssClass === 'status-label-red' ? this.colors.red : element1.cssClass === 'status-label-orange' ? this.colors.orange : null,
                    meta: {
                      user: {
                        id: element.inspectorId,
                        name: element.inspectorName
                      }
                    }
                  }
                  this.events.push(existAppoitmnt)
                })
              }
            });
            this.users = response.resources.rows
            // this.events = response.resources.rows
          }

          setTimeout(() => {
            if ($(".cal-time-events-wrapper")?.length == 0) {
              $('.cal-time-events').wrapAll('<div class="cal-time-events-wrapper"></div>');
            }
            // if (this.validationData.appointmentStartTime == '09:00:00') {
            // if (!this.validationData.preferredTimeSlot || this.validationData.preferredTimeSlot.toLowerCase().includes('am')) {
            $('.cal-time-events-wrapper').animate({
              scrollTop: this.momentService.scrollCalenderByTime(this.validationData['appointmentStartTime'])
            }, 1000, function () {
            });

            // }

            // }
          }, 1000);
        }
      });
  }

  onDaySelect() {
    this.scheduleForm.get('appointmentDate').setValue(this.viewDate)
  }



  onPostScheduleSubmit(): void {  // post schedule

    if (!this.selectedUser) {
      return;
    }
    let formValue = {
      // appointmentLogId: this.appointmentLogId ? this.appointmentLogId : this.customerInspectionAppointmentId,
      customerInspectionAppointmentId: this.customerInspectionAppointmentId,
      modifiedUserId: this.loggedUser.userId,
      inspectorId: this.selectedUser.inspectorId
    }
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.INSPECTION_APPOINTMETNT_TECHNICIAN_ALLOCATION, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        // this.router.navigateByUrl('/out-of-office/manager-request?tab=0');
        // let obj = this.scheduleForm.value
        // Object.keys(obj).forEach(key => {
        //   if (obj[key] === "") {
        //     delete obj[key]
        //   }
        // });
        // let otherParams = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
        // return of(this.getInspectorList(otherParams));
        return of(this.reLoadData())

      }
    })
  }

  getReasonDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_APPOINTMENT_RESCHEDULE_REASON, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.commentsDropDown = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  // filter option 

  getDivisionDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.UX_DIVISIONS, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.divisionList = response.resources
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getBranchByDivisionId(divisionId): void {
    // this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, BillingModuleApiSuffixModels.UX_DISTRICTS, prepareRequiredHttpParams({
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_BRANCHES, prepareRequiredHttpParams({
      divisionId: divisionId
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.branchList = response.resources;
          if (this.validationData.branchId) {
            let exist = this.branchList.find(x => x.branchId == this.validationData.branchId)
            this.filterForm.get('branchIds').setValue(exist ? [{ branchId: this.validationData.branchId, branchName: this.validationData.branchName }] : [])
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  getTechAreaByBranchId(branchIds): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_AREAS, prepareRequiredHttpParams({
      branchIds: branchIds,
      TechnicianCallTypeId: this.validationData.technicianCallTypeId
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.techAreaList = response.resources;
          if (this.validationData.techAreaId) {
            let exist = this.techAreaList.find(x => x.id == this.validationData.techAreaId)
            this.filterForm.get('techAreaIds').setValue(exist ? [{ id: this.validationData.techAreaId, displayName: this.validationData.techAreaName }] : [])
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getInspectorByTechAreaId(techAreaIds): void {
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_INSPECTOR, prepareRequiredHttpParams({
      techAreaIds: techAreaIds
    }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.technitianList = response.resources;
          // if (this.validationData.inspectorId) {
          //   let exist = this.technitianList.find(x => x.id == this.validationData.inspectorId)
          //   this.filterForm.get('inspectorId').setValue(exist ? { id: this.validationData.inspectorId, displayName: this.validationData.inspectorName } : null)
          // }
          // if (this.initialLoad) {
          //   this.reLoadData()
          // }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  navigateToPage() {
    if (this.isNew == 'inspectionlist') {
      this.router.navigate(['/technical-management', 'inspection-call', 'add-edit'], {
        queryParams: {
          // customerId: this.customerInspectionId
          customerId: this.customerId,
          customerInspectionId: this.customerInspectionId,
          callType: this.callType, // 2- service call, 1- installation call
          isNew: this.isNew
        }
      });
    } else {
      this.router.navigate(['/customer', 'manage-customers', 'inspection-call'], {
        queryParams: {
          // customerId: this.customerInspectionId
          customerId: this.customerId,
          customerInspectionId: this.customerInspectionId,
          callType: this.callType, // 2- service call, 1- installation call
          isNew: this.isNew
        }
      });
    }
  }

  onOpenRecurringSchedulePopup() {
    this.showRecurringScheduleDialog = !this.showRecurringScheduleDialog
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RECURRING_APPOINTMENT, null, false,
      prepareGetRequestHttpParams(null, null, { customerInspectionId: this.customerInspectionId }))

      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          if (response.resources.recurringAppointmentFrequencyId == 0) {
            return
          }
          response.resources.startDate = new Date(response.resources.startDate)
          response.resources.endDate = new Date(response.resources.endDate)
          this.recurringScheduleFom.patchValue(response.resources)
          if (!response.resources.recurringAppointmentFrequencyName) {
            this.recurringScheduleFom.get('recurringAppointmentFrequencyName').setValue(FrequencyType.WEEKLY)
          }
          if (response.resources.recurringAppointmentFrequencyName == FrequencyType.MONTHLY || response.resources.recurringAppointmentFrequencyName == FrequencyType.YEARLY) {
            this.recurringScheduleFom.get('requisitionWeekPeriodId').setValue(response.resources.requisitionWeekPeriodId == 0 ? null : response.resources.requisitionWeekPeriodId)
            if (response.resources.requisitionWeekPeriodId != 0) {
              this.recurringScheduleFom.get('recurType').setValue('2')
              this.recurringScheduleFom.get('details').setValue(response.resources.details[0])
            } else {
              this.recurringScheduleFom.get('recurType').setValue('1')
            }
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  public checkValidity(): void {
    Object.keys(this.recurringScheduleFom.controls).forEach((key) => {
      this.recurringScheduleFom.controls[key].markAsDirty();
    });
  }

  onRecuringSchedule() {
    if (this.recurringScheduleFom.invalid) {
      this.checkValidity()
      return
    }
    let formValue = this.recurringScheduleFom.value
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (formValue.recurringAppointmentFrequencyName == FrequencyType.WEEKLY) {
      let formValue1 = {
        "recurringAppointmentId": formValue.recurringAppointmentId,
        "customerInspectionId": formValue.customerInspectionId,
        "recurringAppointmentFrequencyId": formValue.recurringAppointmentFrequencyId,
        "startDate": formValue.startDate,
        "endDate": formValue.endDate,
        "isNoEndDate": formValue.isNoEndDate,
        "recurDay": formValue.recurDay,
        "details": formValue.details,
        "createdUserId": formValue.createdUserId,
      }
      let crudService: Observable<IApplicationResponse> = !formValue.recurringAppointmentId ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RECURRING_APPOINTMENT, formValue1) : this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RECURRING_APPOINTMENT, formValue1)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.showRecurringScheduleDialog = false
        }
      })
    } else if (formValue.recurringAppointmentFrequencyName == FrequencyType.MONTHLY) {
      let formValue1 = formValue.recurType == '1' ? {
        "recurringAppointmentId": formValue.recurringAppointmentId,
        "customerInspectionId": formValue.customerInspectionId,
        "recurringAppointmentFrequencyId": formValue.recurringAppointmentFrequencyId,
        "startDate": formValue.startDate,
        "endDate": formValue.endDate,
        "isNoEndDate": formValue.isNoEndDate,
        "recurDay": formValue.recurDay,
        "recurMonthNumber": formValue.recurMonthNumber,
        "requisitionWeekPeriodId": 0,
        "createdUserId": formValue.createdUserId,
      } :
        {
          "recurringAppointmentId": formValue.recurringAppointmentId,
          "customerInspectionId": formValue.customerInspectionId,
          "recurringAppointmentFrequencyId": formValue.recurringAppointmentFrequencyId,
          "startDate": formValue.startDate,
          "endDate": formValue.endDate,
          "isNoEndDate": formValue.isNoEndDate,
          "recurMonthNumber": formValue.recurMonthNumber,
          "details": [formValue.details],
          "requisitionWeekPeriodId": formValue.requisitionWeekPeriodId,
          "createdUserId": formValue.createdUserId,
        }
      let crudService: Observable<IApplicationResponse> = !formValue.recurringAppointmentId ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RECURRING_APPOINTMENT, formValue1) : this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RECURRING_APPOINTMENT, formValue1)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.showRecurringScheduleDialog = false
        }
      })
    } else if (formValue.recurringAppointmentFrequencyName == FrequencyType.YEARLY) {
      let formValue1 = formValue.recurType == '1' ? {
        "recurringAppointmentId": formValue.recurringAppointmentId,
        "customerInspectionId": formValue.customerInspectionId,
        "recurringAppointmentFrequencyId": formValue.recurringAppointmentFrequencyId,
        "startDate": formValue.startDate,
        "endDate": formValue.endDate,
        "isNoEndDate": formValue.isNoEndDate,
        "recurDay": formValue.recurDay,
        "recurMonthNumber": formValue.recurMonthNumber,
        "requisitionWeekPeriodId": 0,
        "createdUserId": formValue.createdUserId,
      } :
        {
          "recurringAppointmentId": formValue.recurringAppointmentId,
          "customerInspectionId": formValue.customerInspectionId,
          "recurringAppointmentFrequencyId": formValue.recurringAppointmentFrequencyId,
          "startDate": formValue.startDate,
          "endDate": formValue.endDate,
          "isNoEndDate": formValue.isNoEndDate,
          "recurMonthNumber": formValue.recurMonthNumber,
          "details": [formValue.details],
          "requisitionWeekPeriodId": formValue.requisitionWeekPeriodId,
          "createdUserId": formValue.createdUserId,
        }

      let crudService: Observable<IApplicationResponse> = !formValue.recurringAppointmentId ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RECURRING_APPOINTMENT, formValue1) : this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.RECURRING_APPOINTMENT, formValue1)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.showRecurringScheduleDialog = false
        }
      })
    }


  }

  reLoadData() {

    this.showResult = false
    let branchIds = []
    let techAreaIds = []

    if (this.filterForm.get('branchIds').value && this.filterForm.get('branchIds').value.length > 0) {
      this.filterForm.get('branchIds').value.forEach(element => {
        branchIds.push(element.branchId)
      });
    }
    if (this.filterForm.get('techAreaIds').value && this.filterForm.get('techAreaIds').value.length > 0) {
      this.filterForm.get('techAreaIds').value.forEach(element => {
        techAreaIds.push(element.id)
      });
    }
    let filteredData = Object.assign({},
      { divisionId: this.filterForm.get('divisionId').value ? this.filterForm.get('divisionId').value.id : '' },
      { branchIds: this.filterForm.get('branchIds').value ? branchIds.toString() : '' },
      { techAreaIds: this.filterForm.get('techAreaIds').value ? techAreaIds.toString() : '' },
      { inspectorId: this.filterForm.get('inspectorId').value ? this.filterForm.get('inspectorId').value.id : '' },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    // this.scheduleForm.get('appointmentDate').setValue(this.viewDate)
    let scheduledData = this.scheduleForm.value
    scheduledData.appointmentDate = this.viewDate
    Object.keys(scheduledData).forEach(key => {
      if (scheduledData[key] === "") {
        delete scheduledData[key]
      }
    });
    let searchData = Object.assign({}, filterdNewData, scheduledData)
    let filterdNewData1 = Object.entries(searchData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    return of(this.getInspectorList(filterdNewData1));
  }


  confirmSchedule() {
    this.confirmationService.confirm({
      // header: this.callType == '1' ? 'Installation Call' : this.callType == '2' ? 'Service Call' :
      //   this.callType == '3' ? 'Special Projects' : '---',
      header: 'Inspection Call',
      message: 'Are you sure that you want to post schedule for <b>' + this.momentService.toFormateType(this.appointmentFom.get('scheduledStartDate').value, 'MMMM Do YYYY, h:mm a') + '</b>?',
      accept: () => {
        //Actual logic to perform a confirmation
        this.doAppintment()
      },
      reject: () => {
        this.reLoadData()
      }
    });
  }

  getYearRange() {
    return CURRENT_YEAR_TO_NEXT_FIFTY_YEARS;
  }

  getDateFormat() {
    return adjustDateFormatAsPerPCalendar();
  }

  doAppintment() {

    let formValue = this.appointmentFom.value
    formValue.appointmentDate = this.appointmentDate
    formValue.customerInspectionAppointmentId = this.customerInspectionAppointmentId ? this.customerInspectionAppointmentId : null
    // formValue.AppointmentLogId = this.appointmentLogId ? this.appointmentLogId : null
    formValue.scheduledStartDate = this.convertData(formValue.scheduledStartDate)
    formValue.scheduledEndDate = this.convertData(formValue.scheduledEndDate)
    formValue.inspectorId = this.scheduleForm.get('scheduleType').value == '1' ? this.filterForm.get('inspectorId').value?.id : '',
      formValue.isFixedAppointment = (formValue.inspectorId && this.isTechnicianManualSelect) ? true : false,
      //   formValue.scheduledStartDate = formValue.scheduledStartDate.toISOString()
      // formValue.scheduledEndDate = formValue.scheduledEndDate.toISOString()
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    let crudService: Observable<IApplicationResponse> = (!this.customerInspectionAppointmentId) ? this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.INSPECTION_APPOINTMETNTS, formValue) :
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN, OutOfOfficeModuleApiSuffixModels.INSPECTION_APPOINTMETNTS, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        // if (this.callType == '2') {
        if (response.resources) {
          // let jsonStri = JSON.parse(response.resources)
          let jsonStri = response.resources
          // this.customerInspectionAppointmentId = jsonStri
          this.customerInspectionAppointmentId = jsonStri?.customerInspectionAppointmentId
          // this.appointmentLogId = jsonStri.AppointmentLogId
        }
        // }
        this.reLoadData()
      }
    })

  }


}

