import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material';
import { SharedModule } from '@app/shared';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { MultiSelectModule } from 'primeng/multiselect';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DayViewSchedulerInspectAllocationComponent } from './day-view-scheduler-inspect-allocation/day-view-scheduler-inspect-allocation.component';
import { InspectorAllocationCalenderViewRoutingModule } from './inspector-allocation-calender-view-routing.module';
import { InspectorAllocationCalenderViewComponent } from './inspector-allocation-calender-view.component';
@NgModule({
  declarations: [InspectorAllocationCalenderViewComponent,DayViewSchedulerInspectAllocationComponent],
  imports: [
    CommonModule,
    InspectorAllocationCalenderViewRoutingModule,    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    MultiSelectModule,
    MatCheckboxModule,
    RadioButtonModule,
    ConfirmDialogModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),

  ]
})
export class InspectorAllocationCalenderViewModule { }
