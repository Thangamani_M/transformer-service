import { Component, OnInit } from '@angular/core';
import { CrudService } from '@app/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-special-project-waring-dialog',
  templateUrl: './special-project-waring-dialog.component.html'
})
export class SpecialProjectWaringDialogComponent implements OnInit {


  showDialogSpinner: boolean = false;
  data: any = [];
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, private crudService: CrudService) { }

  ngOnInit(): void {
  }

  close(type?:any) {
    this.ref.close(type);
  }

}
