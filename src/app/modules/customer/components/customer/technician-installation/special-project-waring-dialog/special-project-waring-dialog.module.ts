import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SpecialProjectWaringDialogComponent } from './special-project-waring-dialog.component';

@NgModule({
  declarations: [ SpecialProjectWaringDialogComponent ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    SharedModule
  ],
  exports: [SpecialProjectWaringDialogComponent],
  entryComponents: [SpecialProjectWaringDialogComponent],
})
export class SpecialProjectWaringDialogModule { }
