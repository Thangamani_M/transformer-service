import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { LayoutModule, SharedModule } from "@app/shared";
import { MaterialModule } from "@app/shared/material.module";
import { QuotationItemsComponent } from "./quotation-items.component";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';



@NgModule({
    declarations: [QuotationItemsComponent ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    LayoutModule,
    RouterModule.forChild([
        {
            path: '', component: QuotationItemsComponent, data: { title: 'Quotation Items' }, canActivate: [AuthGuard]
        },
    ])
  ],
  entryComponents: [],
})
export class QuotationItemsModule  { }