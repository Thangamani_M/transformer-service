import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudService, CrudType, CustomDirectiveConfig, debounceTimeForSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
@Component({
  selector: 'app-quotation-items',
  templateUrl: './quotation-items.component.html',
  styleUrls: ['./quotation-items.component.scss']
})
export class QuotationItemsComponent implements OnInit {
  customerId: string;
  quotationVersionId: string;
  searchForm: FormGroup;
  observableResponse;
  quote;
  columnFilterForm: FormGroup;
  primengTableConfigProperties: any;
  searchColumns: any;
  row: any = {};
  selectedColumns: any[];
  selectedRows: string[] = [];
  selectedTabIndex: any = 0;
  loggedInUserData: LoggedInUserModel;
  dataList: any;
  totalRecords: any;
  loading: boolean;
  searchKeyword: FormControl;
  componentProperties = new ComponentProperties();
  quotationListForm: FormGroup;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  customerAddressId: string;
  initiationId: string;
  callType: number;
  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private tableFilterFormService: TableFilterFormService,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder) {
      this.customerId = this.activatedRoute.snapshot.queryParams.customerId;
      this.quotationVersionId = this.activatedRoute.snapshot.queryParams.quotationVersionId;
      this.customerAddressId = this.activatedRoute.snapshot.queryParams?.customerAddressId ? this.activatedRoute.snapshot.queryParams?.customerAddressId : '';
      this.initiationId = this.activatedRoute.snapshot.queryParams?.initiationId ? this.activatedRoute.snapshot.queryParams?.initiationId: '';
      this.callType = this.activatedRoute.snapshot.queryParams?.callType ? +this.activatedRoute.snapshot.queryParams.callType : null;
      this.primengTableConfigProperties = {
        tableCaption: "Quote",
        selectedTabIndex: 0,
        breadCrumbItems: [{ displayName: 'Customer Management', relativeRouterUrl: '/customer/manage-customers/'+this.customerId, queryParams: {addressId: this.customerAddressId} }, { displayName: 'Customer Management: '+(this.callType == 1 ? 'Installation' : this.callType == 2 ? 'Service' : ''),
        relativeRouterUrl: '/customer/manage-customers/call-initiation', queryParams: {customerId: this.customerId, customerAddressId: this.customerAddressId, initiationId: this.initiationId, callType: this.callType} }, { displayName: 'Quote' }],
        tableComponentConfigs: {
          tabsList: [
            {
              caption: 'Quote',
              dataKey: 'structureTypeId',
              enableBreadCrumb: true,
              enableExportCSV: false,
              enableExportExcel: false,
              enableExportCSVSelected: false,
              enableReset: false,
              enableGlobalSearch: false,
              reorderableColumns: false,
              resizableColumns: false,
              enableScrollable: true,
              checkBox: false,
              enableRowDelete: false,
              enableStatusActiveAction: false,
              enableFieldsSearch: true,
              rowExpantable: false,
              rowExpantableIndex: 0,
              enableHyperLink: true,
              cursorLinkIndex: 0,
              columns: [
                { field: 'stockCode', header: 'Stock Code', width: '100px' },
                 { field: 'unitPrice', header: 'Unit Price', width: '100px' },
                { field: 'qty', header: 'Qty', width: '100px' },
                { field: 'subTotal', header: 'Sub Total', width: '100px' },
                { field: 'vat', header: 'Vat', width: '100px' },
                { field: 'total', header: 'Total', width: '100px' },
                ],
              apiSuffixModel: SalesModuleApiSuffixModels.QUOTATION_ITEMS,
              moduleName: ModulesBasedApiSuffix.SALES,
              enableMultiDeleteActionBtn: false,
              ebableAddActionBtn: false,
              ebableFilterActionBtn: true
            },
          ]
        }
      }
      this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    }


  ngOnInit(): void {
    this.createForm();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest()
    this.combineLatestNgrxStoreData()
    this.getquotationitems();

  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
        { queryParams: breadCrumbItem['queryParams']} )
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  createForm() {
    this.quotationListForm = this._fb.group({
      structureTypeName: [''],
      roofTypeName: [''],
      systemTypeName:['']
    });

  }

  getquotationitems() {
      let quotationVersionId= this.quotationVersionId
        this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.QUOTATION_ITEMS, quotationVersionId)
          .subscribe((data: IApplicationResponse) =>
           {
            this.loading = false;
            this.dataList = []
            this.totalRecords = 0;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
              this.quote= data.resources
              this.quotationListForm.patchValue(data.resources);
              this.observableResponse = data.resources.systemTypes;
              for (const obj of this.observableResponse) {
                let totalObj:any = {
                  subTotal:0,
                  total:0,
                  vatTotal:0
                }
              for (const services of obj.quotationItems) {
                totalObj.subTotal += Number(services.subTotal)
                totalObj.total += Number(services.total)
                totalObj.vatTotal += Number(services.vat)
              }
              totalObj.subTotal = totalObj.subTotal.toFixed(2)
              totalObj.total = totalObj.total.toFixed(2)
              totalObj.vatTotal = totalObj.vatTotal.toFixed(2)

              obj['totalObj'] =totalObj
              }
              this.dataList = this.observableResponse;
              this.totalRecords = data.totalCount;
           }
            else {
              this.observableResponse = null;
              this.dataList = this.observableResponse
              this.totalRecords = 0;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
           })
        }


  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }
  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }
  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair
          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTC(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        this.getquotationitems()
        break;
      case CrudType.EDIT:
        break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete()
          }
        } else {
          this.onOneOrManyRowsDelete(row)
        }
        break;
      case CrudType.EXPORT:
        break;
      default:
    }
  }
  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
  }
  onTabChange(event) {
    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.getquotationitems()

  }
}
