import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CounterSalesAcceptQuoteComponent } from './counter-sales-accept-quote.component';
import { CounterSalesInvoiceQuoteComponent } from './counter-sales-invoice-quote.component';
import { CounterSalesItemInfoComponent } from './counter-sales-item-info.component';
import { CounterSalesListComponent } from './counter-sales-list.component';
import { CounterSalesNewComponent } from './counter-sales-new.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const auditCycleModuleRoutes: Routes = [
  { path: '', component: CounterSalesListComponent, canActivate: [AuthGuard], data: { title: 'Counter Sales' } },
  { path: 'add-edit', component: CounterSalesNewComponent, canActivate: [AuthGuard], data: { title: 'Counter Sales' } },
  { path: 'item-info', component: CounterSalesItemInfoComponent, canActivate: [AuthGuard], data: { title: 'Counter Sales' } },
  { path: 'quote-info', component: CounterSalesAcceptQuoteComponent, canActivate: [AuthGuard], data: { title: 'Counter Sales' } },
  { path: 'invoice', component: CounterSalesInvoiceQuoteComponent, canActivate: [AuthGuard], data: { title: 'Counter Sales' } },
];

@NgModule({
  imports: [RouterModule.forChild(auditCycleModuleRoutes)],

})

export class CounterSalesRoutingModule { }
