import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AdvanceSearchModule } from '../../advanced-search/advanced-search.module';
import { CounterSalesAcceptQuoteComponent } from './counter-sales-accept-quote.component';
import { CounterSalesExistingComponent } from './counter-sales-existing.component';
import { CounterSalesInvoiceQuoteComponent } from './counter-sales-invoice-quote.component';
import { CounterSalesItemInfoComponent } from './counter-sales-item-info.component';
import { CounterSalesListComponent } from './counter-sales-list.component';
import { CounterSalesRoutingModule } from './counter-sales-new-routing.module';
import { CounterSalesNewComponent } from './counter-sales-new.component';
import { DocumentUploadComponent } from './document-upload.component';
import { QuoteDeclinePopUpCounterComponent } from './quote-decline/quote-decline-pop-up-counter.component';
import { UpdateStockCounterSalesModalComponent } from './update-stock-counter-sales.component';



@NgModule({
  declarations: [CounterSalesNewComponent, CounterSalesItemInfoComponent, CounterSalesExistingComponent, CounterSalesListComponent,
    CounterSalesAcceptQuoteComponent, QuoteDeclinePopUpCounterComponent,
    DocumentUploadComponent, CounterSalesInvoiceQuoteComponent,UpdateStockCounterSalesModalComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    CounterSalesRoutingModule,
    MaterialModule,
    AdvanceSearchModule
  ],
  entryComponents: [QuoteDeclinePopUpCounterComponent, DocumentUploadComponent,UpdateStockCounterSalesModalComponent]
})
export class CounterSalesModule { }
