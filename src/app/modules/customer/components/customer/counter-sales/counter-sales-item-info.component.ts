import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerUpgradeModalComponent, InventoryModuleApiSuffixModels, SalesModuleApiSuffixModels, selectDynamicEagerLoadingLeadCatgoriesState$, selectDynamicEagerLoadingSourcesState$, selectLeadCreationUserDataState$, selectLeadHeaderDataState$, selectStaticEagerLoadingCustomerTypesState$, selectStaticEagerLoadingSiteTypesState$, selectStaticEagerLoadingTitlesState$ } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  ConfirmDialogModel,
  ConfirmDialogPopupComponent, countryCodes, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService,
  IApplicationResponse, landLineNumberMaskPattern, LoggedInUserModel, mobileNumberMaskPattern, ModulesBasedApiSuffix,
  prepareAutoCompleteListFormatForMultiSelection,
  prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { LeadCreationUserDataModel, LeadHeaderData } from '@modules/sales/models';
import { LeadOutcomeStatusNames } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { UpdateStockCounterSalesModalComponent } from './update-stock-counter-sales.component';

declare var $;

@Component({
  selector: 'app-counter-sales-item-info',
  templateUrl: './counter-sales-item-info.component.html'
})

export class CounterSalesItemInfoComponent {
  formConfigs = formConfigs;
  selectedTabIndex = 0;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  alphaNumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  leadForm: FormGroup;
  rawLeadId: string;
  leadId: string;
  customerId: string;
  addressList = [];
  today: any = new Date()
  searchColumns: any
  filteredSchemes = [];
  selectedAddressOption = {};
  selectedLocationOption = {};
  selectedCityOption = {};
  selectedSuburbOption = {};
  selectedCommercialProducts: any;
  loggedInUserData: LoggedInUserModel;
  isSchemeMandatory: boolean;
  todayDate: Date = new Date();
  siteType: string;
  pageTitle: string = "Create";
  btnName = "Next";
  fromPage: string;
  tmpCurrentURL: string;
  tmpListName: string;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  lssSchemes = [];
  customerTypes = [];
  leadCategories = [];
  titles = [];
  siteTypes = [];
  latLongList = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  countryCodes = countryCodes;
  customerTypeName: string;
  commercialCustomerType = 'Commercial';             // edit here
  residentialCustomerType = 'Residential';            // edit here
  siteTypeName: string;
  mobileNumberMaskPattern = mobileNumberMaskPattern;
  landLineNumberMaskPattern = landLineNumberMaskPattern;
  existingCustomers = [];
  selectedExistingCustomer;
  objectKeys = Object.keys;
  subTotalSummary = {
    unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0
  };
  mobileNumberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  valueChangesCount = 0;
  addressModalMessage = "";
  existingAddresses = [];
  installedRentedItems = [];
  existingCustomerAddress;
  selectedInstalledRentedItems = [];
  sourceTypes = [];
  isFormSubmitted = false;
  isEnableQuoteGenerateButton = false;

  dataList = [];
  totalCount = 0;
  loading: boolean;
  pageLimit: any = [10, 25, 50, 75, 100];
  existingLeadsObservableResponse: IApplicationResponse;
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  primengTableConfigProperties = {
    tableCaption: "Template",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Customers', relativeRouterUrl: '' }, { displayName: 'Ticket List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: "Found Existing Customer Profile",
          dataKey: 'customerId',
          reorderableColumns: false,
          resizableColumns: false,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          columns: [{ field: 'isChecked', header: 'Select' },
          { field: 'customerRefNo', header: 'Customer ID' },
          { field: 'fullName', header: 'Customer Name' },
          { field: 'email', header: 'Email' },
          { field: 'mobile1', header: 'Mobile No' },
          { field: 'customerTypeName', header: 'Customer Type' },
          { field: 'fullAddress', header: 'Address' },
          { field: 'isOpenLeads', header: 'Open Lead' }]
        }]
    }
  }
  leadHeaderData: LeadHeaderData;
  fromUrl = 'Leads List';
  isLeadGroupUpgrade = false;
  reconnectionMessage = '';
  selectedExistingCustomerProfile = { isOpenLeads: false };
  isDataFetchedFromSelectedCustomerProfile = false;
  countryId = formConfigs.countryId;

  @ViewChild('existing_address_modal_installed_items', { static: false }) existing_address_modal_installed_items: ElementRef<any>;
  @ViewChild('existing_lead_modal', { static: false }) existing_lead_modal: ElementRef<any>;
  @ViewChild('existing_address_footprint_modal', { static: false }) existing_address_footprint_modal: ElementRef<any>;
  @ViewChild('confirm_reconnection_modal', { static: false }) confirm_reconnection_modal: ElementRef<any>;
  @ViewChild('input', { static: false }) row;
  provinceList: any;
  cities: any;
  suburbs: any;
  id: any;
  details: any;
  systemTypes = [];
  systemTypesData: any = [];
  selectedSystemId: any;
  itemData: any;
  saleId: any;
  isUpsell = false;
  addressId: any = '';
  customerRequestDetails: any = [];
  contactNoCountryCode: any;
  contactNo: any;
  constructor(private crudService: CrudService, private httpCancelService: HttpCancelService, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private dialog: MatDialog,
    private rxjsService: RxjsService, private router: Router, private store: Store<AppState>) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.id = params['params']['id'];
      this.addressId = params['params']['addressId'];
      this.leadId = params['params']['leadId'];
      this.saleId = params['params']['saleId'];
      this.fromPage = params['params']['pageFrom'];
    });

    this.combineLatestNgrxStoreData();
  }

  isAddUpsell = false;
  addUpsellQuote() {
    this.isAddUpsell = true;
  }

  openUpgrade() {
    this.isAddUpsell = false;
    let data = 'upsell';
    $('.quick-action-sidebar').addClass('hide-quick-action-menu');
    const dialogReff = this.dialog.open(CustomerUpgradeModalComponent, { width: '850px', data, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {

    });
  }
  isCancelUpgrade = false;
  referenceId = null;
  cancelUpgrade() {
    this.isCancelUpgrade = true;
    this.isAddUpsell = false;
    this.crudService
      .get(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.UPSEL_CUSTOMER_REQUESTED_BY, undefined, false, prepareRequiredHttpParams({
          customerId: this.id
        })
      ).subscribe(res => {
        this.rxjsService.setGlobalLoaderProperty(false);

        this.customerRequestDetails = res.resources;

      })
  }

  onChangeRequestedBy() {
    let found = this.customerRequestDetails.find(e => e.id == this.referenceId);
    if (found) {
      this.contactNoCountryCode = found.contactNoCountryCode;
      this.contactNo = this.contactNoCountryCode + ' ' + found.contactNo;

    }
  }

  getSystemTypes() {
    this.crudService
      .dropdown(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.UX_SYSTEM_TYPES
      ).subscribe(res => {
        this.rxjsService.setGlobalLoaderProperty(true);

        this.systemTypes = res.resources;
        this.systemTypes.forEach(element => {
          element.isDisabled = false;
        });

        this.getDetailsById();


      })
  }

  getDetailsById(): void {
    if (!this.id) return;
    this.rxjsService.setGlobalLoaderProperty(true);

    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_HEADER_DETAILS, undefined, false, prepareRequiredHttpParams({
      customerId: this.id
    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.details = response.resources;
        if (this.details?.directSaleQuotationVersionId) {
          this.getItemData(this.details?.directSaleQuotationVersionId);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getCustomerDetailsById(): void {
    if (!this.id) return;
    this.rxjsService.setGlobalLoaderProperty(true);

    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UPSELL_QUOTE_HEADER_DETAILS, undefined, false, prepareRequiredHttpParams({
      customerId: this.id,
      addressId: this.addressId ? this.addressId : ""
    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.details = response.resources;
        // if(this.details.directSaleQuotationVersionId){
        //   this.getItemData(this.details.directSaleQuotationVersionId);
        // }
        if (this.details && this.details?.directSaleQuotationVersionId) {
          this.getItemData(this.details?.directSaleQuotationVersionId);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(selectLeadCreationUserDataState$),
    this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingTitlesState$), this.store.select(selectStaticEagerLoadingSiteTypesState$),
    this.store.select(selectStaticEagerLoadingCustomerTypesState$), this.store.select(selectDynamicEagerLoadingLeadCatgoriesState$),
    this.store.select(selectLeadHeaderDataState$),
    this.store.select(selectDynamicEagerLoadingSourcesState$),
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.leadCreationUserDataModel = new LeadCreationUserDataModel(response[0]);
        this.loggedInUserData = new LoggedInUserModel(response[1]);
        this.titles = response[2];
        this.siteTypes = response[3];
        this.customerTypes = response[4];
        this.sourceTypes = response[7];
        this.rxjsService.setGlobalLoaderProperty(false);
        this.leadHeaderData = response[6];
        this.leadCategories = prepareAutoCompleteListFormatForMultiSelection(response[5]);


      });
  }

  ngOnInit(): void {

    this.getSystemTypes();
  }

  addOption() {
    let obj = {
      'directSaleQuotationItems': [],
      'systemTypeId': null,
      'systemTypes': this.systemTypes.filter(e => !e.isDisabled),
      'noOfPartitions': 0
    }
    this.isEnableQuoteGenerateButton = false;

    if (this.systemTypesData.length > 0) {
      if (this.systemTypesData[0].directSaleQuotationItems.length > 0) {
        this.systemTypesData.splice(0, 0, obj);
      }
    } else {
      this.systemTypesData.push(obj);
    }
  }

  onArrowClick(str) {
    if (str == 'forward') {
      this.router.navigate(['/customer/counter-sales/quote-info'], { queryParams: { id: this.id } })

    } else {
      this.router.navigate(['/customer/counter-sales/add-edit'], { queryParams: { id: this.saleId, customerId: this.id } })

    }
  }

  onCancelClick() {
    this.router.navigate(['/customer/counter-sales']);
  }

  openUpdateStock(dt, i) {
    if (!dt.systemTypeId) {
      this.snackbarService.openSnackbar("System Type is required", ResponseMessageTypes.WARNING);
      return;
    }
    this.selectedSystemId = dt.systemTypeId;
    dt.customerId = this.id;
    const details = dt;
    const dialogReff = this.dialog.open(UpdateStockCounterSalesModalComponent, { width: '950px', data: { data: details, index: i, leadData: this.leadHeaderData }, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {

    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      this.systemTypes.forEach(e => {
        if (e.id == this.selectedSystemId) {
          e.isDisabled = true;
        }
      })
      this.isEnableQuoteGenerateButton = false;
    
      let arr1 = this.systemTypesData.find(e => parseInt(e.systemTypeId) == this.selectedSystemId).directSaleQuotationItems;
      let arr2 = ele;
      let finalArr;
      if(arr1.length>0){
        finalArr =  arr1.concat(arr2);
      }
      else 
          finalArr = ele;
      
      finalArr  = finalArr.filter(
        (thing, i, arr) => arr.findIndex(t => t.stockCode === thing.stockCode) === i
      );
        
      this.systemTypesData.find(e => parseInt(e.systemTypeId) == this.selectedSystemId).directSaleQuotationItems = finalArr;
      this.systemTypesData.forEach(element => {
        element.directSaleQuotationItems.forEach((item, index) => {
          let sorted = [];

          if (item.techDiscountingProcessName && (element.systemTypeId == this.selectedSystemId)) {
            item.isFixed = "";
            item.subTotal = 0;
            item.vat = 0;
            item.total = 0;
            sorted.push(item);
            element.directSaleQuotationItems.splice(index, 1);
            element.directSaleQuotationItems = [...element.directSaleQuotationItems, ...sorted]

          }

        });
      });
      this.calculateTotalAmountSummary();
    });
  }


  onQuantityChange(type, serviceSubItem: object): void {
    this.isEnableQuoteGenerateButton = false;
    serviceSubItem["qty"] =
      type === "plus"
        ? serviceSubItem["qty"] + 1
        : serviceSubItem["qty"] === 1
          ? serviceSubItem["qty"]
          : serviceSubItem["qty"] - 1;
    serviceSubItem['subTotal'] = serviceSubItem['qty'] * serviceSubItem['unitPrice']
    serviceSubItem['vat'] = (serviceSubItem['qty'] * serviceSubItem['unitTaxPrice']);
    serviceSubItem['total'] = serviceSubItem['subTotal'] + serviceSubItem['vat'];
    serviceSubItem['total'] = serviceSubItem['total'];
    serviceSubItem['subTotal'] = serviceSubItem['subTotal']
    serviceSubItem['vat'] = serviceSubItem['vat']
    serviceSubItem['total'] = serviceSubItem['total']
    this.calculateTotalAmountSummary();
  }

  calculateTotalAmountSummary(): void {
    this.subTotalSummary = { unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0 };
    this.objectKeys(this.systemTypesData).forEach((systemTypeId, ix: number) => {
      this.systemTypesData[systemTypeId]["totalAmount"] = { unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0 };
      this.subTotalSummary = { unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0 };
      this.systemTypesData[systemTypeId]["directSaleQuotationItems"].forEach((obj) => {

        if (obj.techDiscountingProcessName) {
          if (obj.subTotal > 0) {
            if (obj.isFixed == "true" || obj.isFixed == true) {
              this.subTotalSummary.subTotal -= this.subTotalSummary.subTotal * (obj.subTotal / 100);
            } else {
              this.subTotalSummary.subTotal -= obj.subTotal;

            }
          }
        } else {
          this.subTotalSummary.subTotal += obj.subTotal;
        }
        this.subTotalSummary.vat += obj.vat;
        this.subTotalSummary.unitPrice += obj.unitPrice;
        this.subTotalSummary.totalPrice += obj.total;
        this.subTotalSummary.totalPrice = this.subTotalSummary.totalPrice;

        this.systemTypesData[systemTypeId]["totalAmount"] = this.subTotalSummary;
      });
    });

  }

  generateQuote() {
    const dialogData = new ConfirmDialogModel("Confirm Action", `Are you sure you want to Generate quote?`);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.isConfirmQuotation = true;
        this.submit();
      }
    });
  }

  isLeadItems = false;
  isOnlyOneItem = false;

  isConfirmQuotation = false;
  submit() {
    let dt = new Date()
    dt.setDate(dt.getDate() + 21);
    let postObj = {
      "directSaleId": this.details?.directSaleId,
      "addressId": this.details?.addressId,
      "customerId": this.details?.customerId,
      "referenceId": this.referenceId ? this.referenceId : null,
      "contactNo": this.contactNo ? this.contactNo : null,
      "contactNoCountryCode": this.contactNoCountryCode ? this.contactNoCountryCode : null,
      "directSaleQuotationId": this.itemData ? this.itemData.directSaleQuotationId : null,
      "directSaleQuotationVersionId": this.itemData ? this.itemData.directSaleQuotationVersionId : null,
      "expiryDate": dt,
      "createdUserId": this.loggedInUserData?.userId,
      "isQuoteGenerated": this.details?.isQuoteGenerated ? true : false,
      "isConfirmQuotation": this.isConfirmQuotation,
      "isCounterSale": true,
      "directSaleQuotationSystemTypes": this.systemTypesData
    }

    this.rxjsService.setFormChangeDetectionProperty(true);

    if (this.systemTypesData.length > 0) {
      this.systemTypesData.forEach(element => {
        if (element.directSaleQuotationItems.length == 0) {
          this.isLeadItems = true;
        }

        if (element.directSaleQuotationItems.length == 1) {
          element.directSaleQuotationItems.forEach(item => {
            if (item.techDiscountingProcessName) {
              this.isOnlyOneItem = true;
            } else {
              this.isOnlyOneItem = false;
            }
          });


        }
      });
    } else {
      this.isLeadItems = true;
    }

    if (this.isOnlyOneItem) {
      this.snackbarService.openSnackbar("Add one more item in option", ResponseMessageTypes.WARNING);
      return;
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    if (!this.isLeadItems) {
      let crudService: Observable<IApplicationResponse> = this.crudService.create(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.DIRECT_SALES_ITEMS,
        postObj, 1
      );
      crudService
        .pipe(
          tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          })
        )
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.isConfirmQuotation = false;

            this.getDetailsById();
          }
        });
    } else {
      this.snackbarService.openSnackbar("Option can not be empty", ResponseMessageTypes.WARNING);

    }
  }

  onDelete(index, obj) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe((dialogResult) => {
      if (dialogResult) {

        if (this.itemData?.directSaleQuotationVersionId) {
          this.crudService
            .delete(
              ModulesBasedApiSuffix.SALES,
              SalesModuleApiSuffixModels.DIRECT_SALES_DELETE_SYSTEM_TYPE,
              undefined,
              prepareRequiredHttpParams({
                directSaleQuotationVersionId: this.itemData.directSaleQuotationVersionId,
                systemTypeId: obj.systemTypeId,
                createdUserId: this.loggedInUserData.userId,
                isCounterSale: true
              }), 1
            )
            .subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                this.isEnableQuoteGenerateButton = false;

                this.systemTypesData.splice(index, 1);

                if (obj.systemTypeId) {
                  let fitr = this.systemTypes.find(e => e.id == obj.systemTypeId);
                  fitr.isDisabled = false;
                }
              }
            });
        } else {
          this.systemTypesData.splice(index, 1);

          if (obj.systemTypeId) {
            let fitr = this.systemTypes.find(e => e.id == obj.systemTypeId);
            fitr.isDisabled = false;
          }
        }
      }
    });
  }

  onDeleteItem(item, stock) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe((dialogResult) => {
      if (dialogResult) {

        if (stock?.directSaleQuotationItemId) {
          this.crudService
            .delete(
              ModulesBasedApiSuffix.SALES,
              SalesModuleApiSuffixModels.DIRECT_SALES_ITEMS,
              undefined,
              prepareRequiredHttpParams({
                DirectSaleQuotationVersionId: this.itemData?.directSaleQuotationVersionId,
                directSaleQuotationItemId: stock["directSaleQuotationItemId"],
                createdUserId: this.loggedInUserData.userId,
                isCounterSale: true
              }), 1
            )
            .subscribe((response: IApplicationResponse) => {
              this.isEnableQuoteGenerateButton = false;

              if (response.isSuccess && response.statusCode === 200) {

                this.systemTypesData.forEach(element => {
                  if (element.systemTypeId == item.systemTypeId) {
                    element.directSaleQuotationItems.forEach((stockItem, index) => {
                      if (stockItem.stockCode == stock.stockCode) {
                        element.directSaleQuotationItems.splice(index, 1)
                      }
                    });
                  }
                });
                this.calculateTotalAmountSummary();

              }
            });
        } else {
          this.isEnableQuoteGenerateButton = false;

          this.systemTypesData.forEach(element => {
            if (element.systemTypeId == item.systemTypeId) {
              element.directSaleQuotationItems.forEach((stockItem, index) => {
                if (stockItem.stockCode == stock.stockCode) {
                  element.directSaleQuotationItems.splice(index, 1)
                }
              });
            }
          });
          this.calculateTotalAmountSummary();
        }

      }
    });

  }

  changeDropdown(product: object) {
    product['subTotal'] = 0;
    product['vat'] = 0
    product['total'] = 0
    product['total'] = 0
    this.calculateTotalAmountSummary();
  }

  onChange(product: object) {
    this.isEnableQuoteGenerateButton = false;

    if (product['isFixed']) {
      if (product['subTotal'] > 100) {
        this.snackbarService.openSnackbar("Sub Total Can not be greater than 100", ResponseMessageTypes.WARNING);
      }
    }
    product['subTotal'] = product['subTotal'] ? parseFloat(product['subTotal']) : 0;
    product['vat'] = (product['subTotal'] * product['taxPercentage']) / 100;
    product['total'] = product['subTotal'] + product['vat'];
    product['total'] = product['total'];
    this.calculateTotalAmountSummary();
  }

  getItemData(id) {
    let crudService: Observable<IApplicationResponse> = this.crudService.get(
      ModulesBasedApiSuffix.SALES,
      SalesModuleApiSuffixModels.DIRECT_SALES_ITEMS_GET_DETAILS,
      undefined, false, prepareRequiredHttpParams({
        customerId: this.id,
        directSaleQuotationVersionId: id
      })
    );
    crudService.subscribe((response: IApplicationResponse) => {
      this.itemData = response.resources;
      if (response.isSuccess && response.statusCode === 200) {
         console.log('itemData',this.itemData.isQuoteGenerated)
        this.isEnableQuoteGenerateButton = this.itemData.isQuoteGenerated
        if (this.itemData && this.itemData.directSaleQuotationSystemTypes) {
          this.systemTypesData = this.itemData.directSaleQuotationSystemTypes;
          let totalValue = 0;
          this.systemTypesData.forEach((element, index) => {
            element.index = index + 1;
            totalValue = 0;
            element.directSaleQuotationItems.forEach((e, index) => {
              e.tax = e.vat;
              e.value = 0;

              if (e.isMiscellaneous) {
                element.directSaleQuotationItems.splice(index, 1)
              }

              if (e.techDiscountingProcessName) {
              } else {
                totalValue = totalValue + e.subTotal;

              }
            });
            element.totalValue = JSON.parse(JSON.stringify(totalValue));
            this.systemTypes.forEach(e => {
              if (e.id == element.systemTypeId) {
                e.isDisabled = true;
              }
            })
            element.systemTypes = this.systemTypes.filter(e => e.id == element.systemTypeId);
          });
          this.systemTypesData.forEach((element, index) => {
            let sorted = [];
            element.directSaleQuotationItems.forEach((e, index) => {
              if (e.techDiscountingProcessName && e.isFixed) {
                e.subTotal = (e.subTotal * 100) / element.totalValue;
                e.subTotal = parseFloat(e.subTotal).toFixed(2);
              }
            });

          });

          this.systemTypesData.forEach((element, index) => {
            let sorted = [];
            element.directSaleQuotationItems.forEach((e, index) => {

              if (e.techDiscountingProcessName) {
                sorted.push(e);
                element.directSaleQuotationItems.splice(index, 1);
                element.directSaleQuotationItems = [...element.directSaleQuotationItems, ...sorted]

              }
            });

          });

          // this.systemTypesData =this.systemTypesData.filter(e=> e.isMiscellaneous == false)
          this.calculateTotalAmountSummary();

        } else {
          this.details = {};
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

}
