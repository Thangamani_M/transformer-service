import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import {
  LeadCreationUserDataCreateAction, SalesModuleApiSuffixModels, selectDynamicEagerLoadingLeadCatgoriesState$, selectDynamicEagerLoadingSourcesState$, selectLeadCreationUserDataState$, selectLeadHeaderDataState$, selectStaticEagerLoadingCustomerTypesState$, selectStaticEagerLoadingSiteTypesState$, selectStaticEagerLoadingTitlesState$, UserModuleApiSuffixModels
} from '@app/modules';
import { AppState } from '@app/reducers';
import {
  addFormControls, countryCodes, CrudService,
  CrudType,
  CustomDirectiveConfig,
  debounceTimeForSearchkeyword, destructureAfrigisObjectAddressComponents, FindDuplicatePipe, formConfigs, HttpCancelService,
  IApplicationResponse, landLineNumberMaskPattern, LeadGroupTypes,
  LoggedInUserModel, mobileNumberMaskPattern, ModulesBasedApiSuffix,
  prepareAutoCompleteListFormatForMultiSelection, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControlError, removeFormControls, ResponseMessageTypes, RxjsService,
  setRequiredValidator,
  SnackbarService,
  validateLatLongFormat
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { AddressModel, BasicInfoModel, ContactInfoModel, LeadCreationUserDataModel, LeadHeaderData, LeadInfoModel } from '@modules/sales/models';
import { LeadOutcomeStatusNames } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
import { isUndefined } from 'util';
declare var $;
@Component({
  selector: 'app-counter-sales-new',
  templateUrl: './counter-sales-new.component.html'
})

export class CounterSalesNewComponent {
  formConfigs = formConfigs;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  alphaNumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  leadForm: FormGroup;
  rawLeadId: string;
  leadId: string;
  customerId: string;
  addressList = [];
  today: any = new Date()
  searchColumns: any
  filteredSchemes = [];
  selectedAddressOption = {};
  selectedLocationOption = {};
  selectedCityOption = {};
  selectedSuburbOption = {};
  selectedCommercialProducts: any;
  loggedInUserData: LoggedInUserModel;
  isSchemeMandatory: boolean;
  todayDate: Date = new Date();
  siteType: string;
  pageTitle: string = "Create";
  btnName = "Next";
  fromPage: string;
  tmpCurrentURL: string;
  tmpListName: string;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  lssSchemes = [];
  customerTypes = [];
  leadCategories = [];
  titles = [];
  siteTypes = [];
  latLongList = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  countryCodes = countryCodes;
  customerTypeName: string;
  commercialCustomerType = 'Commercial';             // edit here
  residentialCustomerType = 'Residential';            // edit here
  siteTypeName: string;
  mobileNumberMaskPattern = mobileNumberMaskPattern;
  landLineNumberMaskPattern = landLineNumberMaskPattern;
  existingCustomers = [];
  selectedExistingCustomer;
  mobileNumberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  valueChangesCount = 0;
  addressModalMessage = "";
  existingAddresses = [];
  installedRentedItems = [];
  existingCustomerAddress;
  selectedInstalledRentedItems = [];
  sourceTypes = [];
  isFormSubmitted = false;
  dataList = [];
  totalRecords: any = 0;
  loading: boolean;
  pageLimit: any = [10, 25, 50, 75, 100];
  existingLeadsObservableResponse: IApplicationResponse;
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  pageSize: number = 10;
  confLevelList:any=[];
  primengTableConfigProperties = {
    tableCaption: "Template",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Customers', relativeRouterUrl: '' }, { displayName: 'Ticket List' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: "Found Existing Customer Profile",
          dataKey: 'customerId',
          reorderableColumns: false,
          resizableColumns: false,
          enableHyperLink: false,
          cursorLinkIndex: 0,
          columns: [{ field: 'isChecked', header: 'Select' },
          { field: 'customerRefNo', header: 'Customer ID' },
          { field: 'fullName', header: 'Customer Name' },
          { field: 'email', header: 'Email' },
          { field: 'mobile1', header: 'Mobile No' },
          { field: 'customerTypeName', header: 'Customer Type' },
          { field: 'fullAddress', header: 'Address' },
          { field: 'isOpenLeads', header: 'Open Lead' }]
        }]
    }
  }
  leadHeaderData: LeadHeaderData;
  fromUrl = 'Leads List';
  isLeadGroupUpgrade = false;
  reconnectionMessage = '';
  selectedExistingCustomerProfile = { isOpenLeads: false };
  isDataFetchedFromSelectedCustomerProfile = false;
  countryId = formConfigs.countryId;
  customerIds;
  @ViewChild('existing_address_modal_installed_items', { static: false }) existing_address_modal_installed_items: ElementRef<any>;
  @ViewChild('existing_lead_modal', { static: false }) existing_lead_modal: ElementRef<any>;
  @ViewChild('existing_address_footprint_modal', { static: false }) existing_address_footprint_modal: ElementRef<any>;
  @ViewChild('confirm_reconnection_modal', { static: false }) confirm_reconnection_modal: ElementRef<any>;
  @ViewChild('input', { static: false }) row;
  provinceList: any;
  cities: any;
  suburbs: any;
  id: any;

  constructor(private crudService: CrudService, private httpCancelService: HttpCancelService, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private dialog: MatDialog,
    private rxjsService: RxjsService, private router: Router, private store: Store<AppState>) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.id = params['params']['id'];
      this.customerIds = params['params']['customerId'];
    });
    this.combineLatestNgrxStoreData();
  }

  getDetailsById(): void {
    if (!this.id) return;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.COUNTER_SALE, undefined, false, prepareRequiredHttpParams({
      directSaleId: this.id
    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        let leadInfoModel = new LeadInfoModel(response.resources);
        leadInfoModel.addressInfo.latLong = leadInfoModel.addressInfo.latitude + "," + leadInfoModel.addressInfo.longitude;
        leadInfoModel.contactInfo.officeNoCountryCode = leadInfoModel.contactInfo.officeNoCountryCode ? leadInfoModel.contactInfo.officeNoCountryCode : '+27';
        leadInfoModel.contactInfo.premisesNoCountryCode = leadInfoModel.contactInfo.premisesNoCountryCode ? leadInfoModel.contactInfo.premisesNoCountryCode : '+27';

        this.addressInfoFormGroupControls.patchValue({
          isNewAddress: response.resources?.isNewAddress,
          isAddressComplex: response.resources?.addressInfo.isAddressComplex,
          isAddressExtra: response.resources?.addressInfo.isAddressExtra
        })

        this.leadForm.patchValue(leadInfoModel, { emitEvent: false, onlySelf: true });

      }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(selectLeadCreationUserDataState$),
    this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingTitlesState$), this.store.select(selectStaticEagerLoadingSiteTypesState$),
    this.store.select(selectStaticEagerLoadingCustomerTypesState$), this.store.select(selectDynamicEagerLoadingLeadCatgoriesState$),
    this.store.select(selectLeadHeaderDataState$),
    this.store.select(selectDynamicEagerLoadingSourcesState$),
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.leadCreationUserDataModel = new LeadCreationUserDataModel(response[0]);
        this.loggedInUserData = new LoggedInUserModel(response[1]);
        this.titles = response[2];
        this.siteTypes = response[3];
        this.customerTypes = response[4];
        this.sourceTypes = response[7];
        this.rxjsService.setGlobalLoaderProperty(false);
        this.getRawLeadOrLeadOrCustomerById('ngOnInit');
        this.leadHeaderData = response[6];
        this.leadCategories = prepareAutoCompleteListFormatForMultiSelection(response[5]);

      });
  }

  ngOnInit(): void {
    this.rxjsService.setUpsellingQuoteProperty(true);

    this.createLeadInfoForm();
    this.onBootstrapModalEventChanges();
    this.onFormControlChanges();
    this.getDetailsById();
    this.getProvinceList();
    this.getConfLevelList();
    this.tmpCurrentURL = this.router.url.split('?')[0];
    if (this.tmpCurrentURL === '/sales/task-management/lead-creation') {
      this.tmpListName = 'Lead List';
      this.tmpCurrentURL = '/sales/task-management/tasks';
    }
    else {
      this.tmpListName = 'Lead List';
      this.tmpCurrentURL = '/sales/task-management/';
    }

    if (this.leadId) {
      this.pageTitle = "Update";
    }
    this.rxjsService.getFromUrl().subscribe((fromUrl: string) => {
      this.fromUrl = fromUrl ? fromUrl : 'My Leads List';
    });
    this.rxjsService.setGlobalLoaderProperty(false);

  }

  getProvinceList() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_PROVINCES,
      undefined, false, prepareRequiredHttpParams({
        countryId: this.countryId
      }))
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode === 200) {
          this.provinceList = resp.resources;
        }
      });
  }
  getConfLevelList() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.UX_ADDRESS_CONFIDENT_LEVEL,
      undefined, false, null)
      .subscribe(resp => {
        if (resp.isSuccess && resp.statusCode === 200) {
          this.confLevelList = resp.resources;
        }
      });
  }
  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object): void {
    switch (type) {
      case CrudType.EDIT:
        window.open(`/sales/task-management/lead-creation?leadId=${row['leadId']}`, "_blank")
        break;
    }
  }

  onBootstrapModalEventChanges(): void {
    $("#existing_lead_modal").on("shown.bs.modal", (e) => {
      this.dataList.map((obj) => obj['isChecked'] = false);
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#existing_lead_modal").on("hidden.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(false);
    });
    $("#existing_address_footprint_modal").on("shown.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#existing_address_footprint_modal").on("hidden.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(false);
    });
    $("#confirm_reconnection_modal").on("shown.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(true);
    });
    $("#confirm_reconnection_modal").on("hidden.bs.modal", (e) => {
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  createLeadInfoForm(): void {
    let leadInfoModel = new LeadInfoModel();
    this.leadForm = this.formBuilder.group({});
    Object.keys(leadInfoModel).forEach((key) => {
      switch (key) {
        case "basicInfo":
          this.leadForm.addControl(key, this.createBasicInfoForm());
          break;
        case "contactInfo":
          this.leadForm.addControl(key, this.createContactInfoForm());
          break;
        case "addressInfo":
          this.leadForm.addControl(key, this.createAndGetAddressForm());
          break;
        default:
          this.leadForm.addControl(key, new FormControl(leadInfoModel[key]));
          break;
      }
    });
    this.leadForm = setRequiredValidator(this.leadForm, ["siteTypeId"]);
    this.leadForm.controls['basicInfo'] = setRequiredValidator(this.leadForm.controls['basicInfo'] as FormGroup, ["customerTypeId", 'titleId', 'firstName', 'lastName']);
    this.leadForm.controls['contactInfo'] = setRequiredValidator(this.leadForm.controls['contactInfo'] as FormGroup, ['email', 'mobile1']);
    this.leadForm.controls['addressInfo'] = setRequiredValidator(this.leadForm.controls['addressInfo'] as FormGroup, ['formatedAddress']);
  }

  createBasicInfoForm(basicInfo?: BasicInfoModel): FormGroup {
    let basicInfoModel = new BasicInfoModel(basicInfo);
    let formControls = {};
    Object.keys(basicInfoModel).forEach((key) => {
      formControls[key] = new FormControl(basicInfoModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createContactInfoForm(contactInfo?: ContactInfoModel): FormGroup {
    let contactInfoModel = new ContactInfoModel(contactInfo);
    let formControls = {};
    Object.keys(contactInfoModel).forEach((key) => {
      formControls[key] = new FormControl(contactInfoModel[key]);
    });
    return this.formBuilder.group(formControls);
  }

  createAndGetAddressForm(address?: AddressModel): FormGroup {
    let addressModel = new AddressModel(address);
    let formControls = {};
    Object.keys(addressModel).forEach((key) => {
      if (key === 'streetName' || key === 'suburbName' || key === 'cityName' || key === 'provinceName' || key === 'postalCode' ||
        key === 'estateStreetNo' || key === 'estateStreetName' || key === 'estateName' || key === 'addressConfidentLevel') {
        formControls[key] = new FormControl({ value: addressModel[key], disabled: true });
      }
      else if(key === 'streetNo' || key === 'buildingNo' || key === 'buildingName'){
        formControls[key] = new FormControl({ value: addressModel[key], disabled: false});
      }
      else {
        formControls[key] = new FormControl(addressModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  onFormControlChanges(): void {
    // this.getAddressListFromLatLongSearch();
    this.onCustomerTypeChanges();
    this.onCityNameChanges();
    this.onSuburbNameChanges();

    this.contactInfoFormGroupControls.get('mobile2CountryCode').valueChanges.subscribe((mobile2CountryCode: string) => {
      this.setPhoneNumberLengthByCountryCode(mobile2CountryCode);
      setTimeout(() => {
        this.row.nativeElement.focus();
        this.row.nativeElement.blur();
      });
    });


    this.contactInfoFormGroupControls.get('mobile2').valueChanges.subscribe((mobileNumber2: string) => {
      this.setPhoneNumberLengthByCountryCode(this.contactInfoFormGroupControls.get('mobile2CountryCode').value);
    });

    this.addressInfoFormGroupControls.get('isAddressComplex').valueChanges.subscribe((isAddressComplex: boolean) => {
      if (isAddressComplex) {
        this.addressInfoFormGroupControls.get('buildingNo').enable();
        this.addressInfoFormGroupControls.get('buildingName').enable();
        this.leadForm.controls['addressInfo'] = setRequiredValidator(this.leadForm.controls['addressInfo'] as FormGroup, ["buildingNo", "buildingName"]);
      }
      else {
        if (!this.addressInfoFormGroupControls.value.isNewAddress) {
          this.addressInfoFormGroupControls.get('buildingNo').disable();
          this.addressInfoFormGroupControls.get('buildingName').disable();
        }

      }
    });

    this.addressInfoFormGroupControls.get('isAfrigisSearch').valueChanges.subscribe((isAfrigisSearch: boolean) => {
      if (isAfrigisSearch) {
        this.addressInfoFormGroupControls.get('isNewAddress').setValue(false);
      }
      else {

      }
    });

    this.addressInfoFormGroupControls.get('isNewAddress').valueChanges.subscribe((isNewAddress: boolean) => {
      if (isNewAddress) {
        this.addressInfoFormGroupControls.get('isAfrigisSearch').setValue(false);
        this.addressInfoFormGroupControls.get('buildingNo').enable();
        this.addressInfoFormGroupControls.get('formatedAddress').disable();
        this.addressInfoFormGroupControls.get('formatedAddress').setValue(null);

        this.addressInfoFormGroupControls.get('buildingName').enable();
        this.addressInfoFormGroupControls.get('suburbName').enable();
        this.addressInfoFormGroupControls.get('cityName').enable();
        this.addressInfoFormGroupControls.get('formatedAddress').setValue(null);
        this.addressInfoFormGroupControls.get('latLong').setValue(null);
        this.addressInfoFormGroupControls.get('suburbName').setValue(null);
        this.addressInfoFormGroupControls.get('cityName').setValue(null);
        this.addressInfoFormGroupControls.get('provinceName').setValue(null);
        this.addressInfoFormGroupControls.get('provinceName').enable();
        this.addressInfoFormGroupControls.get('provinceId').setValue('');

        this.addressInfoFormGroupControls.get('streetNo').enable();
        this.addressInfoFormGroupControls.get('streetName').enable();
        this.addressInfoFormGroupControls.get('postalCode').enable();
        this.addressInfoFormGroupControls.get('buildingNo').setValue(null);
        this.addressInfoFormGroupControls.get('buildingName').setValue(null);
        this.addressInfoFormGroupControls.get('streetNo').setValue(null);
        this.addressInfoFormGroupControls.get('streetName').setValue(null);

        this.addressInfoFormGroupControls.get('postalCode').setValue(null);

        this.leadForm.controls['addressInfo'] = setRequiredValidator(this.leadForm.controls['addressInfo'] as FormGroup, ["suburbName", "provinceId", "cityName"]);
      }
      else {
       // this.addressInfoFormGroupControls.get('buildingNo').disable();
        //this.addressInfoFormGroupControls.get('buildingName').disable();
        this.addressInfoFormGroupControls.get('formatedAddress').enable();
        this.addressInfoFormGroupControls.get('formatedAddress').setValue(null);
       // this.addressInfoFormGroupControls.get('streetNo').disable();
        this.addressInfoFormGroupControls.get('streetName').disable();
        this.addressInfoFormGroupControls.get('postalCode').disable();
        this.addressInfoFormGroupControls.get('provinceName').disable();
        this.addressInfoFormGroupControls.get('provinceName').setValue(null);
        this.addressInfoFormGroupControls.get('suburbName').disable();
        this.addressInfoFormGroupControls.get('suburbName').setValue(null);
        this.addressInfoFormGroupControls.get('cityName').disable();
        this.addressInfoFormGroupControls.get('cityName').setValue(null);
        this.addressInfoFormGroupControls.get('buildingNo').setValue(null);
        this.addressInfoFormGroupControls.get('buildingName').setValue(null);
        this.addressInfoFormGroupControls.get('streetNo').setValue(null);
        this.addressInfoFormGroupControls.get('streetName').setValue(null);
        this.addressInfoFormGroupControls.get('postalCode').setValue(null);
      }
    });

    this.addressInfoFormGroupControls.get('isAddressExtra').valueChanges.subscribe((isAddressExtra: boolean) => {
      if (isAddressExtra) {
        this.addressInfoFormGroupControls.get('estateStreetNo').enable();
        this.addressInfoFormGroupControls.get('estateStreetName').enable();
        this.addressInfoFormGroupControls.get('estateName').enable();
      }
      else {
        this.addressInfoFormGroupControls.get('estateStreetNo').disable();
        this.addressInfoFormGroupControls.get('estateStreetName').disable();
        this.addressInfoFormGroupControls.get('estateName').disable();
        this.addressInfoFormGroupControls.get('estateStreetNo').setValue(null);
        this.addressInfoFormGroupControls.get('estateStreetName').setValue(null);
        this.addressInfoFormGroupControls.get('estateName').setValue(null);
      }
    });
    this.getAddressListFromAfrigis();

  }


  onCityNameChanges(): void {
    var searchText: string;
    this.addressInfoFormGroupControls.get('cityName').valueChanges.pipe(debounceTime(debounceTimeForSearchkeyword),
      distinctUntilChanged(), switchMap(searchKeyword => {
        if (!searchKeyword) {
          return of();
        }
        if (searchKeyword === "") {
          this.addressInfoFormGroupControls.get('cityName').setErrors({ 'invalid': false });
          this.addressInfoFormGroupControls.get('cityName').setErrors({ 'required': true });
        }
        searchText = searchKeyword;
        if (!searchText) {
          return this.cities;
        } else if (typeof searchText === 'object') {
          return this.cities = [];
        } else {
          return this.filterDataByKeywordSearch(searchText, 'city')
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.cities = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);

          if (isUndefined(this.selectedCityOption) && searchText !== '') {
            this.addressInfoFormGroupControls.get('cityName').setErrors({ 'invalid': true });
          }
        }
      })
  }

  onSuburbNameChanges(): void {
    var searchText: string;
    this.addressInfoFormGroupControls.get('suburbName').valueChanges.pipe(debounceTime(debounceTimeForSearchkeyword),
      distinctUntilChanged(), switchMap(searchKeyword => {
        if (!searchKeyword) {
          return of();
        }
        if (searchKeyword === "") {
          this.addressInfoFormGroupControls.get('suburbName').setErrors({ 'invalid': false });
          this.addressInfoFormGroupControls.get('suburbName').setErrors({ 'required': true });
        }
        else if (isUndefined(this.selectedSuburbOption)) {
          this.addressInfoFormGroupControls.get('suburbName').setErrors({ 'invalid': true });
        }
        searchText = searchKeyword;
        if (!searchText) {
          return this.suburbs;
        } else if (typeof searchText === 'object') {
          return this.suburbs = [];
        } else {
          return this.filterDataByKeywordSearch(searchText, 'suburb')
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.suburbs = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);

          if (isUndefined(this.selectedSuburbOption) && searchText !== '') {
            this.addressInfoFormGroupControls.get('suburbName').setErrors({ 'invalid': true });
          }
        }
      })
  }

  filterDataByKeywordSearch(searchtext: string, type: string): Observable<IApplicationResponse> {
    switch (type) {

      case "city":
        return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_CITIES, null, true, prepareGetRequestHttpParams(null, null, {
          searchtext
        }));
      case "suburb":
        return this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, SalesModuleApiSuffixModels.SALES_API_SUBURBS, null, true, prepareGetRequestHttpParams(null, null, {
          searchtext
        }));
    }
  }

  onSelectedItem(isSelected: boolean, type: string, selectedItem: object): void {
    this.rxjsService.setGlobalLoaderProperty(false);

    if (isSelected) {
      setTimeout(() => {
        switch (type) {

          case "city":
            const cities = this.cities.filter(ct => ct['id'] === selectedItem['id']);
            if (cities.length > 0) {
              this.selectedCityOption = cities[0];
              this.addressInfoFormGroupControls.get('cityId').setValue(this.selectedCityOption['id'])
            }
            break;
          case "suburb":
            const suburbs = this.suburbs.filter(bt => bt['id'] === selectedItem['id']);
            if (suburbs.length > 0) {
              this.selectedSuburbOption = suburbs[0];
              this.addressInfoFormGroupControls.get('suburbId').setValue(this.selectedSuburbOption['id'])

            }
            break;
        }
      }, 200);
    }
  }

  setPhoneNumberLengthByCountryCode(countryCode: string) {
    switch (countryCode) {
      case "+27":
        this.contactInfoFormGroupControls.get('mobile2').setValidators([Validators.minLength(formConfigs.southAfricanContactNumberMaxLength),
        Validators.maxLength(formConfigs.southAfricanContactNumberMaxLength)]);
        break;
      default:
        this.contactInfoFormGroupControls.get('mobile2').setValidators([Validators.minLength(formConfigs.indianContactNumberMaxLength),
        Validators.maxLength(formConfigs.indianContactNumberMaxLength)]);
        break;
    }
  }

  onCustomerTypeChanges(): void {
    this.basicInfoFormGroupControls.get('customerTypeId').valueChanges.subscribe((customerTypeId: string) => {
      if (!customerTypeId) return;
      this.customerTypeName = this.customerTypes.find(s => s['id'] == customerTypeId).displayName;
      if (this.customerTypeName === this.commercialCustomerType) {
        this.leadForm.controls['basicInfo'] = addFormControls(this.leadForm.controls['basicInfo'] as FormGroup, ["companyRegNo", "companyName"]);
        this.leadForm.controls['basicInfo'] = setRequiredValidator(this.leadForm.controls['basicInfo'] as FormGroup, ["companyName"]);
        this.leadForm.controls['basicInfo'] = removeFormControls(this.leadForm.controls['basicInfo'] as FormGroup, ["said"]);
      }
      else {
        this.leadForm.controls['basicInfo'] = addFormControls(this.leadForm.controls['basicInfo'] as FormGroup, ["said"]);
        this.leadForm.controls['basicInfo'] = removeFormControls(this.leadForm.controls['basicInfo'] as FormGroup, ["companyRegNo", "companyName"]);
      }
    });
  }

  get addressInfoFormGroupControls(): FormGroup {
    if (!this.leadForm) return;
    return this.leadForm.get('addressInfo') as FormGroup;
  }

  get basicInfoFormGroupControls(): FormGroup {
    if (!this.leadForm) return;
    return this.leadForm.get('basicInfo') as FormGroup;
  }

  get contactInfoFormGroupControls(): FormGroup {
    if (!this.leadForm) return;
    return this.leadForm.get('contactInfo') as FormGroup;
  }

  getAddressListFromAfrigis(): void {
    var searchText: string;
    this.addressInfoFormGroupControls.get('formatedAddress').valueChanges.pipe(debounceTime(debounceTimeForSearchkeyword),
      distinctUntilChanged(), switchMap(searchKeyword => {
        if (!searchKeyword) {
          return of();
        }
        if (searchKeyword === "") {
          this.addressInfoFormGroupControls.get('formatedAddress').setErrors({ 'invalid': false });
          this.addressInfoFormGroupControls.get('formatedAddress').setErrors({ 'required': true });
        }
        else if (searchKeyword.length < 3) {
          this.addressInfoFormGroupControls.get('formatedAddress').setErrors({
            minlength: {
              actualLength: searchKeyword.length,
              requiredLength: 3
            }
          });
        }
        else if (isUndefined(this.selectedAddressOption)) {
          this.addressInfoFormGroupControls.get('formatedAddress').setErrors({ 'invalid': true });
        }
        else if (this.selectedAddressOption['description'] != searchKeyword && this.addressInfoFormGroupControls.get('isAfrigisSearch').value) {
          this.addressInfoFormGroupControls.get('formatedAddress').setErrors({ 'invalid': true });
          this.clearAddressFormGroupValues();
        }
        else if (this.selectedAddressOption['fullAddress'] != searchKeyword && !this.addressInfoFormGroupControls.get('isAfrigisSearch').value) {
          this.addressInfoFormGroupControls.get('formatedAddress').setErrors({ 'invalid': true });
          this.clearAddressFormGroupValues();
        }
        searchText = searchKeyword;
        if (!searchText) {
          return this.addressList;
        } else if (typeof searchText === 'object') {
          return this.addressList = [];
        } else {
          return this.filterAddressByKeywordSearch(searchText, 'address');
        }
      })).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.addressList = response.resources;
          if (isUndefined(this.selectedAddressOption) && searchText !== '') {
            this.addressInfoFormGroupControls.get('formatedAddress').setErrors({ 'invalid': true });
          }
        }

      })
  }

  getAddressListFromLatLongSearch(): void {
    var searchText: string;
    this.addressInfoFormGroupControls.get('latLong').valueChanges.pipe(debounceTime(debounceTimeForSearchkeyword),
      distinctUntilChanged(), switchMap(searchKeyword => {
        if (!searchKeyword || searchKeyword == '-') {
          return of();
        }
        if (searchKeyword === "") {
          this.addressInfoFormGroupControls.get('latLong').setErrors({ 'invalid': false });
          this.addressInfoFormGroupControls.get('latLong').setErrors({ 'required': true });
        }
        else if (isUndefined(this.selectedLocationOption) || validateLatLongFormat(searchKeyword)) {
          this.addressInfoFormGroupControls.get('latLong').setErrors({ 'invalid': true });
        }
        // else if (this.selectedLocationOption['description'] !== searchKeyword) {
        //   this.addressInfoFormGroupControls.get('latLong').setErrors({ 'invalid': true });
        //   this.clearAddressFormGroupValues();
        // }
        searchText = searchKeyword;
        if (!searchText) {
          return this.addressList;
        } else if (typeof searchText === 'object') {
          return this.addressList = [];
        } else {
          return this.filterAddressByKeywordSearch(searchText, 'location');
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.addressList = response.resources;
          if (isUndefined(this.selectedLocationOption) && searchText !== '') {
            this.addressInfoFormGroupControls.get('latLong').setErrors({ 'invalid': true });
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  clearAddressFormGroupValues(): void {
    this.addressInfoFormGroupControls.patchValue({
      latitude: "",
      longitude: "",
      suburbName: "", cityName: "", provinceName: "", postalCode: "", streetName: "",
      streetNo: "",
      buildingNo: "", buildingName: "", estateName: "", estateStreetName: "", estateStreetNo: ""
    });
  }

  onChangeCustomerSelection(rowData) {
    this.selectedExistingCustomerProfile = rowData;
    this.dataList.forEach((dataObj) => {
      if (dataObj['fullAddress'] !== rowData['fullAddress']) {
        dataObj['isChecked'] = false;
      }
      else if (dataObj['fullAddress'] == rowData['fullAddress']) {
        rowData['isChecked'] = true;
      }
    })
  }

  onExistingCustomerPopupBtnClick(type) {
    let selectedExistingCustomerProfileCopy = JSON.parse(JSON.stringify(this.selectedExistingCustomerProfile));
    switch (type) {
      case "exising customer":
        delete selectedExistingCustomerProfileCopy.isOpenLeads;
        this.contactInfoFormGroupControls.patchValue(new ContactInfoModel(selectedExistingCustomerProfileCopy));
        this.basicInfoFormGroupControls.patchValue(this.selectedExistingCustomerProfile);
        this.leadForm.get('customerId').setValue(this.selectedExistingCustomerProfile['customerId']);
        this.isDataFetchedFromSelectedCustomerProfile = true;
        this.rxjsService.setFormChangeDetectionProperty(true);
        break;
      case "existing lead":
        this.router.navigate(['/sales/task-management/leads/lead-info/view'], { queryParams: { leadId: this.selectedExistingCustomerProfile['leadId'] } });
        break;
      case "customer detail":
        this.rxjsService.setViewCustomerData({
          customerId: this.selectedExistingCustomerProfile['customerId'],
          addressId: this.selectedExistingCustomerProfile['addressId'],
          customerTab: 0,
          monitoringTab: null,
        })
        this.rxjsService.navigateToViewCustomerPage();
        break;
    }
  }

  actionAgainstExistingLead(): void {
    $(this.existing_lead_modal.nativeElement).modal('hide');
  }

  onSelectExistingCustomerAddress(existingCustomerAddressObj): void {
    this.existingCustomerAddress = existingCustomerAddressObj;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.ADDRESS_ITEMS, this.existingCustomerAddress['addressId'])
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.installedRentedItems = response.resources;
          this.installedRentedItems.forEach((installedRentedItem) => {
            installedRentedItem.isChecked = false;
          })
        }
      });
  }

  onChangeInstalledItems(installedItemObj): void {
    installedItemObj.isChecked = !installedItemObj.isChecked;
    if (installedItemObj.isChecked) {
      this.selectedInstalledRentedItems.push(installedItemObj);
    }
    else {
      this.selectedInstalledRentedItems.splice(this.selectedInstalledRentedItems.indexOf(installedItemObj.itemId), 1);
    }
  }

  onLinkExistingAddress(): void {
    $(this.existing_address_footprint_modal.nativeElement).modal('hide');
    if (this.installedRentedItems.length > 0) {
      $(this.existing_address_modal_installed_items.nativeElement).modal('show');
    }
  }

  onSelectedInstalledItems(): void {
    $(this.existing_address_modal_installed_items.nativeElement).modal('hide');
  }

  getAddressFullDetails(seoid: string): void {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_ADDRESS_DETAILS, undefined, false,
      prepareRequiredHttpParams({ seoid }), 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          this.leadForm.controls['addressInfo'].get('jsonObject').setValue(JSON.stringify(response.resources));
          this.addressInfoFormGroupControls.get('addressConfidentLevel').setValue(response.resources.addressConfidenceLevel);
          response.resources.addressDetails.forEach((addressObj) => {
            this.addressInfoFormGroupControls.get('addressConfidentLevelId').setValue(addressObj.confidence);
            this.patchAddressFormGroupValues('address', addressObj, addressObj['address_components']);
          });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  patchAddressFormGroupValues(type: string, addressObj, addressComponents: Object[]) {
    let { suburbName, cityName, provinceName, postalCode, streetName, streetNo } = destructureAfrigisObjectAddressComponents(addressComponents);
    if (type == 'address') {
      this.addressInfoFormGroupControls.patchValue({
        latitude: addressObj.geometry.location.lat,
        longitude: addressObj.geometry.location.lng,
        latLong: `${addressObj.geometry.location.lat}, ${addressObj.geometry.location.lng}`,
        suburbName, cityName, provinceName, postalCode, streetName, streetNo
      }, { emitEvent: false, onlySelf: true });
    }
  }

  onSelectedItemOption(isSelected: boolean, type: string, selectedObject: object): void {
    if (isSelected) {
      if (type == 'address') {
        if (selectedObject['seoid']) {
          this.leadForm.controls['addressInfo'].get('seoid').setValue(selectedObject['seoid']);
          this.getAddressFullDetails(selectedObject['seoid']);
          setTimeout(() => {
            const addressList = this.addressList.filter(bt => bt['seoid'] === selectedObject['seoid']);
            if (addressList.length > 0) {
              this.selectedAddressOption = addressList[0];
            }
          }, 200);
        }
        else {
          this.addressInfoFormGroupControls.patchValue({
            seoid: "",
            jsonObject: "",
            latitude: selectedObject['latitude'],
            longitude: selectedObject['longitude'],
            latLong: `${selectedObject['latitude']}, ${selectedObject['longitude']}`,
            suburbName: selectedObject['suburbName'], cityName: selectedObject['cityName'],
            provinceName: selectedObject['provinceName'], postalCode: selectedObject['postalCode'],
            streetName: selectedObject['streetName'], streetNo: selectedObject['streetNo'],
            estateName: selectedObject['estateName'], estateStreetName: selectedObject['estateStreetName'],
            estateStreetNo: selectedObject['estateStreetNo'], buildingName: selectedObject['buildingName'],
            buildingNo: selectedObject['buildingNo'],
            addressConfidentLevel: selectedObject['addressConfidentLevelName'], addressConfidentLevelId: selectedObject['addressConfidentLevelId'],
          }, { emitEvent: false, onlySelf: true });
          setTimeout(() => {
            const addressList = this.addressList.filter(bt => bt['addressId'] === selectedObject['addressId']);
            if (addressList.length > 0) {
              this.selectedAddressOption = addressList[0];
            }
          }, 200);
        }
      }
      else {
        setTimeout(() => {
          const latLongList = this.latLongList.filter(ll => ll['seoid'] === selectedObject['seoid']);
          if (latLongList.length > 0) {
            this.selectedLocationOption = latLongList[0];
          }
        }, 200);
      }
    }
  }

  filterAddressByKeywordSearch(searchtext: string, type: string): Observable<IApplicationResponse> {
    if (type == 'address') {
      if (searchtext.length < 3) {
        return of();
      }
      return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS, null, true,
        prepareRequiredHttpParams({
          searchtext,
          isAfrigisSearch: this.addressInfoFormGroupControls.get('isAfrigisSearch').value
        }), 1)
    }
    else {
      return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_ADDRESS, null, true,
        prepareRequiredHttpParams({
          searchtext,
          isAfrigisSearch: this.addressInfoFormGroupControls.get('isAfrigisSearch').value
        }), 1)
    }
  }

  getForkJoinRequests(): void {
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_UX_SOURCE_TYPES, undefined, true, null, 1)]
    )
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess) {
            switch (ix) {
              case 0:
                this.sourceTypes = respObj.resources;
                break;
            }
          }
          if (ix === response.length - 1) {
            this.rxjsService.setGlobalLoaderProperty(false);
            this.getRawLeadOrLeadOrCustomerById('ngOnInit');
          }
        });
      });
  }

  getRawLeadOrLeadOrCustomerById(type: string): void {
    if (!this.rawLeadId && !this.leadId && !this.customerId) return;
    let salesModuleApiSuffixModels = this.leadId ? SalesModuleApiSuffixModels.SALES_API_LEADS :
      this.rawLeadId ? SalesModuleApiSuffixModels.SALES_API_RAW_LEADS_CONVERT_LEAD :
        SalesModuleApiSuffixModels.GET_LEAD_BY_CUSTOMER_ID;
    this.crudService.get(ModulesBasedApiSuffix.SALES, salesModuleApiSuffixModels,
      this.leadId ? this.leadId : this.rawLeadId ? this.rawLeadId : this.customerId, false, undefined, 1)
      .subscribe((resp: IApplicationResponse) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          if (this.customerId) {
            this.isDataFetchedFromSelectedCustomerProfile = true;
          }
          let leadInfoModel = new LeadInfoModel(resp.resources);
          this.isLeadGroupUpgrade = +leadInfoModel.leadGroupId == LeadGroupTypes.UPGRADE ? true : false;
          if (!this.customerId) {
            leadInfoModel.addressInfo.latLong = leadInfoModel.addressInfo.latitude + "," + leadInfoModel.addressInfo.longitude;
          }
          else {
            leadInfoModel.addressInfo = new AddressModel(leadInfoModel.addressInfo);
          }
          this.leadForm.patchValue(leadInfoModel, { emitEvent: false, onlySelf: true });
          this.basicInfoFormGroupControls.get('customerTypeId').setValue(leadInfoModel.basicInfo.customerTypeId);
          let contactInfo = new ContactInfoModel(leadInfoModel.contactInfo);
          if (this.rawLeadId) {
            if (resp.resources.addressInfo.buildingNo) {
              resp.resources.addressInfo.isAddressComplex = true;
            }
            if (resp.resources.addressInfo.estateStreetNo) {
              resp.resources.addressInfo.isAddressExtra = true;
            }
          }
          this.contactInfoFormGroupControls.patchValue(contactInfo);
          if (!this.customerId) {
            this.addressInfoFormGroupControls.patchValue(resp.resources.addressInfo, { emitEvent: false, onlySelf: true });
            this.addressInfoFormGroupControls.get('isAddressComplex').setValue(resp.resources.addressInfo.isAddressComplex);
            this.addressInfoFormGroupControls.get('isAddressExtra').setValue(resp.resources.addressInfo.isAddressExtra);
          }
          // // to remove min length errors when masking the phone numbers
          // this.leadForm.controls['contactInfo'] = removeFormControlError(this.leadForm.controls['contactInfo'] as FormGroup, 'minlength');
          this.leadCreationUserDataModel = new LeadCreationUserDataModel(resp.resources);
          if (!this.customerId) {
            this.selectedAddressOption['description'] = leadInfoModel.addressInfo.formatedAddress;
          }
          this.leadCreationUserDataModel.suburbId = this.leadForm.value.suburbId;
          this.leadCreationUserDataModel.districtId = this.leadForm.value.districtId;
          this.leadCreationUserDataModel.leadId = leadInfoModel.leadId ? leadInfoModel.leadId : resp.resources;
          this.leadCreationUserDataModel.leadStatusName = this.leadCreationUserDataModel.leadStatusName ? this.leadCreationUserDataModel.leadStatusName :
            this.leadHeaderData ? this.leadHeaderData.leadStatusName :
              LeadOutcomeStatusNames.ACCEPT;
          this.store.dispatch(new LeadCreationUserDataCreateAction({ leadCreationUserDataModel: this.leadCreationUserDataModel }));
          if (type === 'submit') {
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  validateAtleastOneFieldToBeMandatory(type: string): void {
    if (type === 'contactInfo') {
      if (!this.contactInfoFormGroupControls.get('mobile2').value && !this.contactInfoFormGroupControls.get('officeNo').value &&
        !this.contactInfoFormGroupControls.get('premisesNo').value) {
        // this.setAtleastOneFieldRequiredError(type);
      }
      else {
        this.leadForm.controls['contactInfo'] = removeFormControlError(this.leadForm.controls['contactInfo'] as FormGroup, 'atleastOneOfTheFieldsIsRequired');
      }
    }
    else {
      if (this.addressInfoFormGroupControls.get('isAddressExtra').value && !this.addressInfoFormGroupControls.get('estateName').value && !this.addressInfoFormGroupControls.get('estateStreetName').value &&
        !this.addressInfoFormGroupControls.get('estateStreetNo').value) {
        this.setAtleastOneFieldRequiredError(type);
      }
      else {
        this.leadForm.controls['addressInfo'] = removeFormControlError(this.leadForm.controls['addressInfo'] as FormGroup, 'atleastOneOfTheFieldsIsRequired');
      }
    }
  }

  setAtleastOneFieldRequiredError(type: string): void {
    if (type === 'contactInfo') {
      this.contactInfoFormGroupControls.get('mobile2').setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.contactInfoFormGroupControls.get('officeNo').setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.contactInfoFormGroupControls.get('premisesNo').setErrors({ atleastOneOfTheFieldsIsRequired: true });
    }
    else {
      this.addressInfoFormGroupControls.get('estateName').setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.addressInfoFormGroupControls.get('estateStreetNo').setErrors({ atleastOneOfTheFieldsIsRequired: true });
      this.addressInfoFormGroupControls.get('estateStreetName').setErrors({ atleastOneOfTheFieldsIsRequired: true });
    }
  }

  changeProvince() {

  }

  onSubmit(): void {
    this.isFormSubmitted = true;
    this.leadForm = removeFormControls(this.leadForm, ["leadGroupId", "leadCategoryId", "modifiedUserId", "rawLeadId", "leadId"]);
    if (this.leadForm.invalid) {
      this.leadForm.markAllAsTouched();
      return;
    }
    if(this.addressInfoFormGroupControls.get('addressConfidentLevelId').value){
      let filter = this.confLevelList.filter(x=> x.id == this.addressInfoFormGroupControls.get('addressConfidentLevelId').value)
      if(filter && filter.length>0)
        this.addressInfoFormGroupControls.get('addressConfidentLevel').setValue(filter[0].displayName);
    }


    this.validateAtleastOneFieldToBeMandatory('contactInfo');
    this.validateAtleastOneFieldToBeMandatory('addressInfo');
    const isMobile1Duplicate = new FindDuplicatePipe().transform(this.contactInfoFormGroupControls.get('mobile1').value,
      this.contactInfoFormGroupControls, 'mobile1');
    const isMobile2Duplicate = new FindDuplicatePipe().transform(this.contactInfoFormGroupControls.get('mobile2').value,
      this.contactInfoFormGroupControls, 'mobile2', this.contactInfoFormGroupControls.get('mobile2CountryCode').value);
    const isOfficeNoDuplicate = new FindDuplicatePipe().transform(this.contactInfoFormGroupControls.get('officeNo').value,
      this.contactInfoFormGroupControls, 'officeNo');
    const isPremisesNoDuplicate = new FindDuplicatePipe().transform(this.contactInfoFormGroupControls.get('premisesNo').value,
      this.contactInfoFormGroupControls, 'premisesNo');

    let addressFormGroupRawValues = this.addressInfoFormGroupControls.getRawValue();
    if (!this.addressInfoFormGroupControls.value.isNewAddress) {
      if (!addressFormGroupRawValues.streetName || !addressFormGroupRawValues.provinceName || !addressFormGroupRawValues.suburbName ||
        !addressFormGroupRawValues.cityName || !addressFormGroupRawValues.postalCode) {
        this.snackbarService.openSnackbar("Street Name / Province Name / Suburb Name / City Name / Postal Code is required..!!", ResponseMessageTypes.ERROR);
        return;
      }
    }

    // else{
    //   if (!addressFormGroupRawValues.streetName && !addressFormGroupRawValues.streetNo && !addressFormGroupRawValues.buildingName &&
    //     !addressFormGroupRawValues.buildingNo) {
    //     this.snackbarService.openSnackbar("Street Name / Street No / Building Name / Building No  is required..!!", ResponseMessageTypes.ERROR);
    //     return;
    //   }else if(addressFormGroupRawValues.buildingNo || addressFormGroupRawValues.buildingName){
    //     if(!addressFormGroupRawValues.buildingNo){
    //       this.snackbarService.openSnackbar("Building No  is required..!!", ResponseMessageTypes.ERROR);
    //       return;
    //     }else if(!addressFormGroupRawValues.buildingName){
    //       this.snackbarService.openSnackbar("Building Name  is required..!!", ResponseMessageTypes.ERROR);
    //       return;
    //     }
    //   }

    //   else if(addressFormGroupRawValues.streetName || addressFormGroupRawValues.streetNo){
    //     if(!addressFormGroupRawValues.streetNo){
    //       this.snackbarService.openSnackbar("Street No  is required..!!", ResponseMessageTypes.ERROR);
    //       return;

    //     }else if(!addressFormGroupRawValues.streetName){
    //       this.snackbarService.openSnackbar("Street Name  is required..!!", ResponseMessageTypes.ERROR);
    //       return;

    //     }

    //   }
    // }

    if (this.addressInfoFormGroupControls.value.provinceId) {
      if (this.provinceList.length > 0) {
        let province = this.provinceList.find(e => e.id == this.addressInfoFormGroupControls.value.provinceId)
        this.addressInfoFormGroupControls.get("provinceName").setValue(province.displayName)
      }
    }


    if (!this.contactInfoFormGroupControls.get('mobile2').value) {
      delete this.contactInfoFormGroupControls.value.mobile2CountryCode;
    }
    if (!this.contactInfoFormGroupControls.get('premisesNo').value) {
      delete this.contactInfoFormGroupControls.value.premisesNoCountryCode;
    }
    if (!this.contactInfoFormGroupControls.get('officeNo').value) {
      delete this.contactInfoFormGroupControls.value.officeNoCountryCode;
    }
    this.leadForm.value.createdUserId = this.loggedInUserData.userId;
    this.leadForm.value.modifiedUserId = this.loggedInUserData.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.leadForm.value.addressInfo = this.addressInfoFormGroupControls.getRawValue();
    this.leadForm.value.contactInfo = this.contactInfoFormGroupControls.getRawValue();
    this.leadForm.value.basicInfo = this.basicInfoFormGroupControls.getRawValue();
    this.leadForm.value.contactInfo = this.contactInfoFormGroupControls.value;
    this.leadForm.value.contactInfo.mobile1 = this.leadForm.value.contactInfo.mobile1.toString().replace(/\s/g, "");
    if (this.leadForm.value.contactInfo.mobile2) {
      this.leadForm.value.contactInfo.mobile2 = this.leadForm.value.contactInfo.mobile2.toString().replace(/\s/g, "");
    }
    if (this.leadForm.value.contactInfo.officeNo) {
      this.leadForm.value.contactInfo.officeNo = this.leadForm.value.contactInfo.officeNo.toString().replace(/\s/g, "");
    }
    if (this.leadForm.value.contactInfo.premisesNo) {
      this.leadForm.value.contactInfo.premisesNo = this.leadForm.value.contactInfo.premisesNo.toString().replace(/\s/g, "");
    }

    let obj = this.leadForm.value
    obj.isNewAddress = this.leadForm.value.addressInfo.isNewAddress
    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.COUNTER_SALE, this.leadForm.value, 1);
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.router.navigate(['/customer/counter-sales/item-info'], { queryParams: { id:response.resources.directSaleId ,saleId:response.resources.customerId } })
       // this.router.navigate(['/customer/counter-sales/item-info'], { queryParams: { id: response.resources,saleId:this.id } })

      }

    })
  }

  nextOrPrevStepper(type?: string): void {
    //   if (type === btnActionTypes.SUBMIT) {
    //     this.router.navigate(['/sales/task-management/leads/lead-info/view'], { queryParams: { leadId: this.leadId } });
    //   }
    //   else if (type === btnActionTypes.NEXT) {

    //   }
    //   else if (type === btnActionTypes.PREVIOUS) {
    //     if (this.leadId) {
    //       this.router.navigate(['/sales/task-management/leads/lead-info/view'], { queryParams: { selectedIndex: 0, leadId: this.leadId }, skipLocationChange: true })
    //     }
    //     else if (this.fromUrl == 'Leads List') {
    //       this.router.navigate(['/sales/task-management/tasks']);
    //     }
    //     else if (this.fromUrl == 'My Leads List') {
    //       this.router.navigate(['/sales/task-management/leads']);
    //     }
    //   }
  }

  onCancelClick() {
    this.router.navigate(['/customer/counter-sales']);
  }
  onArrowClick(type){
    this.router.navigate(['/customer/counter-sales/item-info'], { queryParams: { id: this.customerIds,saleId:this.id } })
  }
  navigateToList(e): void {
    if (e === 'Leads List') {
      this.router.navigate(['/sales/task-management/leads']);
    } else if (e === 'My Leads List') {
      this.router.navigate(['sales/task-management/tasks'], { queryParams: { leadId: this.leadId, setindex: 1 }, skipLocationChange: false })
    }

  }

}
