import { DatePipe } from '@angular/common';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels, selectDynamicEagerLoadingLeadCatgoriesState$, selectDynamicEagerLoadingSourcesState$, selectLeadCreationUserDataState$, selectLeadHeaderDataState$, selectStaticEagerLoadingCustomerTypesState$, selectStaticEagerLoadingSiteTypesState$, selectStaticEagerLoadingTitlesState$ } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  countryCodes, CrudService,
  CrudType,
  CustomDirectiveConfig,
  debounceTimeForSearchkeyword, formConfigs, HttpCancelService,
  IApplicationResponse, landLineNumberMaskPattern, LoggedInUserModel, mobileNumberMaskPattern, ModulesBasedApiSuffix,
  prepareAutoCompleteListFormatForMultiSelection,
  prepareRequiredHttpParams, ResponseMessageTypes, RxjsService,
  setRequiredValidator,
  SnackbarService
} from '@app/shared';
import { PrimengDeleteConfirmDialogComponent } from '@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { LeadCreationUserDataModel, LeadHeaderData } from '@modules/sales/models';
import { LeadOutcomeStatusNames } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, take, tap } from 'rxjs/operators';
import { DocumentUploadComponent } from './document-upload.component';
import { QuoteDeclinePopUpCounterComponent } from './quote-decline/quote-decline-pop-up-counter.component';

declare var $;

// QuoteDeclinePopUpComponent
@Component({
  selector: 'app-counter-sales-accept-quote',
  templateUrl: './counter-sales-accept-quote.component.html'
})

export class CounterSalesAcceptQuoteComponent {
  formConfigs = formConfigs;
  selectedTabIndex = 0;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  alphaNumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  leadForm: FormGroup;
  rawLeadId: string;
  leadId: string;
  customerId: string;
  addressList = [];
  status = [];
  today: any = new Date()
  searchColumns: any
  filteredSchemes = [];
  selectedAddressOption = {};
  selectedLocationOption = {};
  selectedCityOption = {};
  selectedSuburbOption = {};
  selectedCommercialProducts: any;
  loggedInUserData: LoggedInUserModel;
  isSchemeMandatory: boolean;
  todayDate: Date = new Date();
  siteType: string;
  pageTitle: string = "Create";
  btnName = "Next";
  fromPage: string;
  tmpCurrentURL: string;
  tmpListName: string;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  lssSchemes = [];
  customerTypes = [];
  leadCategories = [];
  titles = [];
  siteTypes = [];
  latLongList = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  countryCodes = countryCodes;
  customerTypeName: string;
  commercialCustomerType = 'Commercial';             // edit here
  residentialCustomerType = 'Residential';            // edit here
  siteTypeName: string;
  mobileNumberMaskPattern = mobileNumberMaskPattern;
  landLineNumberMaskPattern = landLineNumberMaskPattern;
  existingCustomers = [];
  selectedExistingCustomer;
  objectKeys = Object.keys;
  subTotalSummary = {
    unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0
  };
  mobileNumberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  valueChangesCount = 0;
  addressModalMessage = "";
  existingAddresses = [];
  installedRentedItems = [];
  existingCustomerAddress;
  selectedInstalledRentedItems = [];
  sourceTypes = [];
  isFormSubmitted = false;
  dataList = [];
  totalCount = 0;
  loading: boolean;
  pageLimit: any = [10, 25, 50, 75, 100];
  existingLeadsObservableResponse: IApplicationResponse;
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  leadHeaderData: LeadHeaderData;
  fromUrl = 'Leads List';
  paymentInvoiceForm: FormGroup;
  isLeadGroupUpgrade = false;
  reconnectionMessage = '';
  selectedExistingCustomerProfile = { isOpenLeads: false };
  isDataFetchedFromSelectedCustomerProfile = false;
  countryId = formConfigs.countryId;
  primengTableConfigProperties: any;
  pageSize: number = 10;
  @ViewChild('existing_address_modal_installed_items', { static: false }) existing_address_modal_installed_items: ElementRef<any>;
  @ViewChild('existing_lead_modal', { static: false }) existing_lead_modal: ElementRef<any>;
  @ViewChild('existing_address_footprint_modal', { static: false }) existing_address_footprint_modal: ElementRef<any>;
  @ViewChild('confirm_reconnection_modal', { static: false }) confirm_reconnection_modal: ElementRef<any>;
  // @ViewChild('input', { static: false }) row;
  provinceList: any;
  cities: any;
  suburbs: any;
  id: any;
  details: any;
  systemTypes = [];
  systemTypesData: any = [];
  selectedSystemId: any;
  itemData: any;
  selectedQuote: any;
  headerDetails: any;
  searchKeyword: FormControl;
  searchForm: FormGroup;
  totalObj = {
    subTotal: 0,
    total: 0
  }

  columnFilterForm: FormGroup;
  totalRecords: number;
  row: any = {}
  invoiceDetails: any;
  debitMinDate: any = new Date();
  constructor(private crudService: CrudService, private momentService: MomentService,
    private tableFilterFormService: TableFilterFormService,
    private datePipe: DatePipe,
    private dialogService: DialogService,
    private httpCancelService: HttpCancelService, private _fb: FormBuilder, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private dialog: MatDialog,
    private rxjsService: RxjsService, private router: Router, private store: Store<AppState>) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.id = params['params']['id'];
      this.leadId = params['params']['leadId'];
      this.customerId = params['params']['customerId'];
      this.fromPage = params['params']['pageFrom'];
    });
    this.primengTableConfigProperties = {
      tableCaption: "Counter Sales",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Customer Management', relativeRouterUrl: '' }, { displayName: 'Counter Sales', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Counter Sales',
            dataKey: 'documentId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'description', header: 'Description' },
            { field: 'documnetType', header: 'Document Type' },
            { field: 'createdDate', header: 'Created Date' },
            { field: 'uploadedBy', header: 'Uploaded By' },
            ],
            apiSuffixModel: SalesModuleApiSuffixModels.DIRECT_SALES_DOCUMENT_UPLOAD,
            moduleName: ModulesBasedApiSuffix.SALES,
            enableMultiDeleteActionBtn: true,
            ebableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.combineLatestNgrxStoreData();
  }

  getInvoiceDetails() {
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.INVOICE_PAYMENT_UPSELL_QUOTE, undefined, false, prepareRequiredHttpParams({
      customerId: this.id,
      directSaleQuotationId: this.details[0].directSaleQuotationId
    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.resources && response.statusCode === 200) {
        this.invoiceDetails = response.resources
        this.createpaymentInvoiceForm();
        this.openPayment = true;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  acceptedTotal: any = 0
  openPayment = false;
  proceed() {
    //  this.process = true;
    this.getInvoiceDetails();
    this.acceptedTotal = 0;

    this.details.forEach(element => {
      if (element.outcomeId == 1) {
        this.acceptedTotal = this.acceptedTotal + element.total;
      }
    });


  }
  enableProceedButton = false;

  gotoInvoice() {
    // this.router.navigate(['/inventory/counter-sales/invoice'], { queryParams: {id: this.id } })

  }

  createpaymentInvoiceForm() {
    let model = {
      "totalPaymentAmount": this.acceptedTotal,
      "paymentDate": "",
      "isPurchaseOrder": false,
      "bankName": null,
      "noOfPayments": "",
      "bankBranch": null,
      "purchaseOrderDate": "",
      "purchaseOrderNo": "",
      "paymentMethodId": 1,
      "mobile1": "",
      "mobile1CountryCode": "",
      "file": '',
      "debtorAccountDetailId": null,
      "createdUserId": this.loggedInUserData.userId,
      "quotationVersionPayment": this.invoiceDetails.quotationVersionPayment,
      "debtorDetails": this.invoiceDetails.debtorDetails
    }
    this.paymentInvoiceForm = this.formBuilder.group({
      paymentInstallments: this.formBuilder.array([])
    });
    Object.keys(model).forEach((key) => {
      this.paymentInvoiceForm.addControl(key, new FormControl(model[key]));
    });
    // this.initiatonCallCreationForm = setRequiredValidator(this.initiatonCallCreationForm, ["contact","serviceInfo.systemTypeId","serviceInfo.systemTypeSubCategoryId","serviceInfo.jobTypeId"]);
    this.paymentInvoiceForm = setRequiredValidator(this.paymentInvoiceForm, ["paymentMethodId", "paymentDate"]);
    // this.paymentInvoiceForm.patchValue(this.invoiceDetails);

    this.changePurchaseOrder();
  }

  changeBankAccount() {
    let id = this.paymentInvoiceForm.value.debtorAccountDetailId;
    let found = this.invoiceDetails.debtorDetails.find(e => e.debtorAccountDetailId == id);
    this.paymentInvoiceForm.get("bankName").setValue(found.bankName)
    this.paymentInvoiceForm.get("bankBranch").setValue(found.bankBranchCode)

  }

  changePurchaseOrder() {
    let check = this.paymentInvoiceForm.value.isPurchaseOrder;
    if (check) {
      this.paymentInvoiceForm.get("purchaseOrderDate").enable();
      this.paymentInvoiceForm.get("purchaseOrderNo").enable();
      this.paymentInvoiceForm = setRequiredValidator(this.paymentInvoiceForm, ["purchaseOrderDate", "purchaseOrderNo"]);

    } else {

      this.paymentInvoiceForm.get("purchaseOrderDate").disable();
      this.paymentInvoiceForm.get("purchaseOrderNo").disable();
      this.paymentInvoiceForm.get("purchaseOrderDate").setValue(null);
      this.paymentInvoiceForm.get("purchaseOrderNo").setValue(null);
    }

  }


  getListById(): void {
    if (!this.id) return;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_QUOTATION_LIST, undefined, false, prepareRequiredHttpParams({
      customerId: this.id
    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.details = response.resources;
        let subTotal = 0;

        this.details.forEach(element => {
          if (element.outcomeId == 1) {
            this.selectedQuoteString = this.selectedQuoteString + ' ' + element.directSaleQuotationVersionNumber
            this.enableProceedButton = true;
          }
          this.totalObj.subTotal += element.subTotal;
          this.totalObj.total += element.total;

          // element.doaStatus = 'Approved' ;
          element.expiryDate = this.datePipe.transform(element.expiryDate, "d MMM y");
        });
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getDocumentById(pageIndex?: string, pageSize?: string, otherParams?: object): void {
    if (!this.id) return;
    let obj1 = {};
    obj1 = {
      customerId: this.id
    }
    if (otherParams) {
      otherParams = { ...otherParams, ...obj1 };
    } else {
      otherParams = obj1;
    }
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DOCUMENT_LIST, undefined, false, prepareRequiredHttpParams(otherParams), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response.resources;
        this.totalRecords = response.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  addDocumnent = false;
  addDocument(data?: any) {
    this.addDocumnent = true;
    data = data ? data : {};
    data['customerId'] = this.id;
    data['directSaleId'] = this.headerDetails.directSaleId;
    data['createdUserId'] = this.loggedInUserData.userId;
    const dialogReff1 = this.dialog.open(DocumentUploadComponent, { width: '700px', data, disableClose: false });
    dialogReff1.componentInstance['outputData'].subscribe(ele => {
      if (ele) {
        this.getDocumentById();
      } else {

      }
    });
  }

  formData = new FormData();
  submitpayment() {

    if (this.paymentInvoiceForm.invalid) {
      return;
    }
    this.formData = new FormData();
    let obj = this.paymentInvoiceForm.value;
    obj.isCounterSale = true;
    delete obj.paymentInstallments;
    delete obj.noOfPayments
    // this.formData.append("file", null, null);
    this.formData.append("json", JSON.stringify(obj));
    this.rxjsService.setDialogOpenProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    this.crudService.create(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.INVOICE_PAYMENT_COUNTER_SALE, this.formData, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {

      }
      this.rxjsService.setDialogOpenProperty(false);
    })


  }

  deleteDocument() {

  }

  uploadFiles() {

  }

  getDetailsById(): void {
    if (!this.id) return;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_HEADER_DETAILS, undefined, false, prepareRequiredHttpParams({
      customerId: this.id
    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.headerDetails = response.resources;

        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  confirmationAlert = false;
  changeOutcome(dt) {
    this.selectedQuote = dt;
    if (dt.outcomeId == 2) {
      this.enableProceedButton = false;
      let data = {};
      data = dt;
      const dialogReff1 = this.dialog.open(QuoteDeclinePopUpCounterComponent, { width: '700px', data, disableClose: false });
      dialogReff1.componentInstance['outputData'].subscribe(ele => {
        if (ele) {
          this.getListById();
        } else {

        }
      });
      dialogReff1.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;

      })
    } else {
      this.confirmationAlert = true;
    }

  }

  onArrowClick(str) {
    if (str == 'forward') {
      this.router.navigate(['/customer/counter-sales/quote-info'], { queryParams: { id: this.id } })

    } else {
      this.router.navigate(['/customer/counter-sales/item-info'], { queryParams: { id: this.id } })

    }
  }
  process = false;
  selectedQuoteString = "";

  acceptQuote() {
    let obj = {};
    obj['modifiedUserId'] = this.loggedInUserData.userId;
    obj['directSaleQuotationVersionId'] = this.selectedQuote.directSaleQuotationVersionId;
    obj['outcomeId'] = this.selectedQuote.outcomeId;
    obj['isCounterSale'] = true;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_QUOTATION_ACCEPT, obj, 1).subscribe((response) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.confirmationAlert = false;
        this.selectedQuoteString = '';
        this.details.forEach(element => {
          if (element.outcomeId == 1) {
            this.selectedQuoteString = this.selectedQuoteString + ',' + element.directSaleQuotationVersionNumber
            this.enableProceedButton = true;
          }

        });
      }
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(selectLeadCreationUserDataState$),
    this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingTitlesState$), this.store.select(selectStaticEagerLoadingSiteTypesState$),
    this.store.select(selectStaticEagerLoadingCustomerTypesState$), this.store.select(selectDynamicEagerLoadingLeadCatgoriesState$),
    this.store.select(selectLeadHeaderDataState$),
    this.store.select(selectDynamicEagerLoadingSourcesState$),
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.leadCreationUserDataModel = new LeadCreationUserDataModel(response[0]);
        this.loggedInUserData = new LoggedInUserModel(response[1]);
        this.titles = response[2];
        this.siteTypes = response[3];
        this.customerTypes = response[4];
        this.sourceTypes = response[7];
        this.rxjsService.setGlobalLoaderProperty(false);
        this.leadHeaderData = response[6];
        this.leadCategories = prepareAutoCompleteListFormatForMultiSelection(response[5]);


      });
  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.getDetailsById();
    this.getListById();
    this.getDocumentById();
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.row = row;

    let otherParams = {};
    if (this.searchForm.value.searchKeyword) {
      otherParams["search"] = this.searchForm.value.searchKeyword;
    }
    if (Object.keys(this.row).length > 0) {
      // logic for split columns and its values to key value pair

      if (this.row['searchColumns']) {
        Object.keys(this.row['searchColumns']).forEach((key) => {
          if (key.toLowerCase().includes('date')) {
            otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
          } else {
            otherParams[key] = this.row['searchColumns'][key];
          }
        });
      }
      if (this.row['sortOrderColumn']) {
        otherParams['sortOrder'] = this.row['sortOrder'];
        otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
      }
    }
    this.getDocumentById(this.row["pageIndex"], this.row["pageSize"], otherParams)

    // this.getDocumentById();

  }


  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.getDocumentById());
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.getDocumentById());
        })
      )
      .subscribe();
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        isdeleted: false,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getDocumentById();
      }
    });
  }


  isLeadItems = false;
  isConfirmQuotation = false;
  submit() {
    let dt = new Date()
    dt.setDate(dt.getDate() + 21);
    let postObj = {
      "directSaleId": this.details.directSaleId,
      "addressId": this.details.addressId,
      "customerId": this.details.customerId,
      "directSaleQuotationId": this.itemData ? this.itemData.directSaleQuotationId : null,
      "directSaleQuotationVersionId": this.itemData ? this.itemData.directSaleQuotationVersionId : null,
      "expiryDate": dt,
      "createdUserId": this.loggedInUserData.userId,
      "isQuoteGenerated": this.details.isQuoteGenerated ? true : false,
      "isConfirmQuotation": this.isConfirmQuotation,
      "isCounterSale": true,
      "directSaleQuotationSystemTypes": this.systemTypesData
    }

    this.rxjsService.setFormChangeDetectionProperty(true);

    if (this.systemTypesData.length > 0) {
      this.systemTypesData.forEach(element => {
        if (element.directSaleQuotationItems.length == 0) {
          this.isLeadItems = true;
        }
      });
    } else {
      this.isLeadItems = true;
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    if (!this.isLeadItems) {
      let crudService: Observable<IApplicationResponse> = this.crudService.create(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.DIRECT_SALES_ITEMS,
        postObj, 1
      );
      crudService
        .pipe(
          tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          })
        )
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            // this.getItemData(response.resources);
          }
        });
    } else {
      this.snackbarService.openSnackbar("Option can not be empty", ResponseMessageTypes.WARNING);

    }
  }

  exportExcel() { }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void { }

  changePaymentMethod() { }

}
