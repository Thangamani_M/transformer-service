import { DatePipe } from '@angular/common';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels, selectDynamicEagerLoadingLeadCatgoriesState$, selectDynamicEagerLoadingSourcesState$, selectLeadCreationUserDataState$, selectLeadHeaderDataState$, selectStaticEagerLoadingCustomerTypesState$, selectStaticEagerLoadingSiteTypesState$, selectStaticEagerLoadingTitlesState$ } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  countryCodes, CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService,
  IApplicationResponse, landLineNumberMaskPattern, LoggedInUserModel, mobileNumberMaskPattern, ModulesBasedApiSuffix,
  prepareAutoCompleteListFormatForMultiSelection,
  prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { loggedInUserData } from '@modules/others';
import { LeadCreationUserDataModel, LeadHeaderData } from '@modules/sales/models';
import { LeadOutcomeStatusNames } from '@modules/sales/shared';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { QuoteDeclinePopUpCounterComponent } from './quote-decline/quote-decline-pop-up-counter.component';

declare var $;

// QuoteDeclinePopUpComponent
@Component({
  selector: 'app-counter-sales-invoice',
  templateUrl: './counter-sales-invoice-quote.component.html'
})

export class CounterSalesInvoiceQuoteComponent {
  formConfigs = formConfigs;
  selectedTabIndex = 0;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  alphaNumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAlphaNumericSomeSpecialCharterOnly = new CustomDirectiveConfig({ isAlphaNumericSomeSpecialCharterOnly: true });
  leadForm: FormGroup;
  rawLeadId: string;
  leadId: string;
  customerId: string;
  addressList = [];
  today: any = new Date()
  searchColumns: any
  filteredSchemes = [];
  selectedAddressOption = {};
  selectedLocationOption = {};
  selectedCityOption = {};
  selectedSuburbOption = {};
  selectedCommercialProducts: any;
  loggedInUserData: LoggedInUserModel;
  isSchemeMandatory: boolean;
  todayDate: Date = new Date();
  siteType: string;
  pageTitle: string = "Create";
  btnName = "Next";
  fromPage: string;
  tmpCurrentURL: string;
  tmpListName: string;
  leadCreationUserDataModel: LeadCreationUserDataModel;
  lssSchemes = [];
  customerTypes = [];
  leadCategories = [];
  titles = [];
  siteTypes = [];
  latLongList = [];
  selectedColumns: any[];
  selectedRows: string[] = [];
  countryCodes = countryCodes;
  customerTypeName: string;
  commercialCustomerType = 'Commercial';             // edit here
  residentialCustomerType = 'Residential';            // edit here
  siteTypeName: string;
  mobileNumberMaskPattern = mobileNumberMaskPattern;
  landLineNumberMaskPattern = landLineNumberMaskPattern;
  existingCustomers = [];
  selectedExistingCustomer;
  objectKeys = Object.keys;
  subTotalSummary = {
    unitPrice: 0, subTotal: 0, vat: 0, totalPrice: 0
  };
  mobileNumberConfig = new CustomDirectiveConfig({ isAValidPhoneNumberOnly: true });
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  valueChangesCount = 0;
  addressModalMessage = "";
  existingAddresses = [];
  installedRentedItems = [];
  existingCustomerAddress;
  selectedInstalledRentedItems = [];
  sourceTypes = [];
  isFormSubmitted = false;
  dataList = [];
  totalCount = 0;
  loading: boolean;
  pageLimit: any = [10, 25, 50, 75, 100];
  existingLeadsObservableResponse: IApplicationResponse;
  leadOutcomeStatusNames = LeadOutcomeStatusNames;
  
  leadHeaderData: LeadHeaderData;
  fromUrl = 'Leads List';
  isLeadGroupUpgrade = false;
  reconnectionMessage = '';
  selectedExistingCustomerProfile = { isOpenLeads: false };
  isDataFetchedFromSelectedCustomerProfile = false;
  countryId = formConfigs.countryId;
  primengTableConfigProperties: any;

  @ViewChild('existing_address_modal_installed_items', { static: false }) existing_address_modal_installed_items: ElementRef<any>;
  @ViewChild('existing_lead_modal', { static: false }) existing_lead_modal: ElementRef<any>;
  @ViewChild('existing_address_footprint_modal', { static: false }) existing_address_footprint_modal: ElementRef<any>;
  @ViewChild('confirm_reconnection_modal', { static: false }) confirm_reconnection_modal: ElementRef<any>;
  @ViewChild('input', { static: false }) row;
  provinceList: any;
  cities: any;
  suburbs: any;
  id: any;
  details: any;
  systemTypes = [];
  systemTypesData: any = [];
  selectedSystemId: any;
  itemData: any;
  selectedQuote: any;
  headerDetails: any;
  searchKeyword: FormControl;
  searchForm: FormGroup;
  totalObj = {
    subTotal : 0,
    total :0
  }

  columnFilterForm: FormGroup;
  totalRecords: number;
  constructor(private crudService: CrudService,private momentService: MomentService,
    private tableFilterFormService: TableFilterFormService,
    private datePipe: DatePipe,
    private dialogService:DialogService,
     private httpCancelService: HttpCancelService,private _fb: FormBuilder, private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private dialog: MatDialog,
    private rxjsService: RxjsService, private router: Router, private store: Store<AppState>) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.id = params['params']['id'];
      this.leadId = params['params']['leadId'];
      this.customerId = params['params']['customerId'];
      this.fromPage = params['params']['pageFrom'];
    });
    this.primengTableConfigProperties = {
      tableCaption: "Counter Sales",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Customer Management', relativeRouterUrl: '' },{ displayName: 'Counter Sales', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Counter Sales',
            dataKey: 'documentId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'description', header: 'Description' }, 
            { field: 'documnetType', header: 'Document Type' },
            { field: 'createdDate', header: 'Created Date' },
            { field: 'uploadedBy', header: 'Uploaded By' },
            ],
            apiSuffixModel:  SalesModuleApiSuffixModels.DIRECT_SALES_DOCUMENT_UPLOAD,
            moduleName: ModulesBasedApiSuffix.SALES,
            enableMultiDeleteActionBtn: true,
            ebableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.combineLatestNgrxStoreData();
  }

  expandQuotation(dt){
    this.details.directSaleQuotationVersionList.forEach(element => {
      if(dt.directSaleQuotationVersionNumber == element.directSaleQuotationVersionNumber){
        element.isExpand = !element.isExpand;

      }else{
        element.isExpand = false;

      }
    }); 
  }

  
  getListById(): void {
    if (!this.id) return;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_QUOTATION_DETAILS, undefined, false, prepareRequiredHttpParams({
      customerId:this.id
    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.details = response.resources;
        let subTotal = 0;
        let total = 0;
        let vat = 0;

        this.details.directSaleQuotationVersionList.forEach(element => {
          element.isExpand = false;
          subTotal = 0;
          total = 0;
          vat = 0;
             element.directSaleQuotationSystemTypes.forEach(st => {
              st.directSaleQuotationItems.forEach(item => {
                subTotal += item.subTotal;
                vat += item.vat;
                total += item.total;
              });
              st.subTotal = subTotal;
              st.vat = vat;
              st.total = total;

             });
          
        });
      
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }


  getDetailsById(): void {
    if (!this.id) return;
    this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_HEADER_DETAILS, undefined, false, prepareRequiredHttpParams({
      customerId:this.id
    }), 1).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.headerDetails = response.resources;
       
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  confirmationAlert = false;
  changeOutcome(dt){
    this.selectedQuote = dt;
    if(dt.outcomeId == 2){
      let data = {};
      data = dt;
      const dialogReff1 = this.dialog.open(QuoteDeclinePopUpCounterComponent, { width: '700px', data, disableClose: false });
      dialogReff1.componentInstance['outputData'].subscribe(ele => {
        if (ele) {
         this.getListById();
        } else {
         
        }
      });
      dialogReff1.afterClosed().subscribe(dialogResult => {
        if (!dialogResult) return;
       
      })
    }else{
     this.confirmationAlert = true;
    }
   
  }

  acceptQuote(){
    let obj : any;
    obj.modifiedUserId = this.loggedInUserData.userId;
    obj.directSaleQuotationVersionId = this.selectedQuote.directSaleQuotationVersionId;
    obj.outcomeId = this.selectedQuote.outcomeId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.crudService.update(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.DIRECT_SALES_QUOTATION_ACCEPT, obj, 1).subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
              this.confirmationAlert = false;
            
        }
    })
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(selectLeadCreationUserDataState$),
    this.store.select(loggedInUserData), this.store.select(selectStaticEagerLoadingTitlesState$), this.store.select(selectStaticEagerLoadingSiteTypesState$),
    this.store.select(selectStaticEagerLoadingCustomerTypesState$), this.store.select(selectDynamicEagerLoadingLeadCatgoriesState$),
    this.store.select(selectLeadHeaderDataState$),
    this.store.select(selectDynamicEagerLoadingSourcesState$),
    ])
      .pipe(take(1))
      .subscribe((response) => {
        this.leadCreationUserDataModel = new LeadCreationUserDataModel(response[0]);
        this.loggedInUserData = new LoggedInUserModel(response[1]);
        this.titles = response[2];
        this.siteTypes = response[3];
        this.customerTypes = response[4];
        this.sourceTypes = response[7];
        this.rxjsService.setGlobalLoaderProperty(false);
        this.leadHeaderData = response[6];
        this.leadCategories = prepareAutoCompleteListFormatForMultiSelection(response[5]);
       

      });
  }

  ngOnInit(): void {
  
   this.getDetailsById();
   this.getListById();
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.row = row;

    let otherParams = {};
    if (this.searchForm.value.searchKeyword) {
      otherParams["search"] = this.searchForm.value.searchKeyword;
    }
    if (Object.keys(this.row).length > 0) {
      // logic for split columns and its values to key value pair

      if (this.row['searchColumns']) {
        Object.keys(this.row['searchColumns']).forEach((key) => {
          if (key.toLowerCase().includes('date')) {
            otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
          } else {
            otherParams[key] = this.row['searchColumns'][key];
          }
        });
      }
      if (this.row['sortOrderColumn']) {
        otherParams['sortOrder'] = this.row['sortOrder'];
        otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
      }
    }
    
    // this.getDocumentById();

  }

  


  isLeadItems = false;
  isConfirmQuotation = false;
  submit() {
    let dt = new Date()
    dt.setDate(dt.getDate() + 21);
    let postObj = {
      "directSaleId": this.details.directSaleId,
      "addressId": this.details.addressId,
      "customerId": this.details.customerId,
      "directSaleQuotationId": this.itemData ? this.itemData.directSaleQuotationId : null,
      "directSaleQuotationVersionId": this.itemData ? this.itemData.directSaleQuotationVersionId : null,
      "expiryDate": dt,
      "createdUserId": this.loggedInUserData.userId,
      "isQuoteGenerated":this.details.isQuoteGenerated ? true : false,
      "isConfirmQuotation": true,
      "isCounterSale": true,
      "directSaleQuotationSystemTypes":this.systemTypesData
    }
  
    this.rxjsService.setFormChangeDetectionProperty(true);
   
    if (this.systemTypesData.length > 0) {
      this.systemTypesData.forEach(element => {
        if (element.directSaleQuotationItems.length == 0) {
          this.isLeadItems = true;
        }
      });
    } else {
      this.isLeadItems = true;
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();

    if (!this.isLeadItems) {
      let crudService: Observable<IApplicationResponse> = this.crudService.create(
        ModulesBasedApiSuffix.SALES,
        SalesModuleApiSuffixModels.DIRECT_SALES_ITEMS,
        postObj, 1
      );
      crudService
        .pipe(
          tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          })
        )
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            // this.getItemData(response.resources);
          }
        });
    } else {
      this.snackbarService.openSnackbar("Option can not be empty", ResponseMessageTypes.WARNING);

    }
  }

  

}
