import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Industry } from '@app/modules/customer/models';
import { AlertService, CrudService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { CustomerModuleApiSuffixModels } from '@modules/customer/shared';



@Component({
  selector: 'app-industry-add-edit',
  templateUrl: './industry-add-edit.component.html'
})
export class IndustryAddEditComponent implements OnInit {

  @Input() id: string;
  industryAddEditForm: FormGroup;
  applicationResponse: IApplicationResponse;
  errorMessage: string;
  btnName: string;
  IndustryCategoryList: any;
  ddlFilter: any = '';

  constructor(@Inject(MAT_DIALOG_DATA) public data: Industry,

    private dialog: MatDialog,
    private httpServices: CrudService,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private httpService: CrudService) { }

  ngOnInit(): void {
    if (this.data.industryName) {
      this.btnName = 'Update';
      this.ddlFilter = this.data.industryCategoryId
    } else {
      this.btnName = 'Create';
    }
    this.getIndustryCategories();
    this.industryAddEditForm = this.formBuilder.group({
      industryName: this.data.industryName || '',
      industryCategoryId: this.data.industryCategoryId || '',
      description: this.data.description || ''
    });

  }

  save(): void {
    const Industry = { ...this.data, ...this.industryAddEditForm.value }
    if (Industry.industryId != null) {
      this.httpServices.update(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.Industry, Industry)
        .subscribe({
          next: response => {
            this.onSaveComplete(response);
          },
          error: err => this.errorMessage = err
        });

    }
    else {

      this.httpServices.create(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.Industry, Industry)
        .subscribe({
          next: response => {
            this.onSaveComplete(response);
          },
          error: err => this.errorMessage = err
        });


    }
  }

  public getIndustryCategories() {

    this.httpService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.Industry_CATEGORY_DROPDOWN,
      undefined,
      false,
    ).subscribe(resp => {
      this.applicationResponse = resp;
      this.IndustryCategoryList = this.applicationResponse.resources;
    });
  }

  onSaveComplete(response): void {
    if (response.statusCode == 200) {
      this.dialog.closeAll();
      //this.toastr.responseMessage(response);
      // Reset the form to clear the flags
      this.industryAddEditForm.reset();
    } else {
      //Display Alert message
      this.alertService.processAlert(response);
    }
  }

}
