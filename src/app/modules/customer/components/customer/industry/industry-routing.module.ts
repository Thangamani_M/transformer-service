import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IndustryListComponent } from './industry-list';

const industry: Routes = [
    {
        path: '', component: IndustryListComponent, data: { title: 'Industry List' }
    },
];

@NgModule({
  imports: [RouterModule.forChild(industry)],
  
})

export class IndustryRoutingModule { }