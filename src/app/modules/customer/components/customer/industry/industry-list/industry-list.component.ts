import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ConfirmDialogModel, ConfirmDialogPopupComponent } from '@app/shared/components';

import { IndustryAddEditComponent } from '@modules/customer/components';

import {EnableDisable} from  '@app/shared/models';
import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';
import {RxjsService,CrudService} from '@app/shared/services';
import { Industry } from '@app/modules/customer/models';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared/utils';
import {CustomerModuleApiSuffixModels} from '@modules/customer/shared';


@Component({
  selector: 'app-industry-list',
  templateUrl: './industry-list.component.html'
})
export class IndustryListComponent implements OnInit {

  displayedColumns: string[] = ['select', 'IndustryName','IndustryTypeName' ,'description', 'createdDate', 'isActive'];
  dataSource = new MatTableDataSource<Industry>();
  selection = new SelectionModel<Industry>(true, []);
  enableDisable = new EnableDisable();
  applicationResponse: IApplicationResponse;
  industry: Industry[];
  errorMessage: string;
  limit: number = 25;
  skip: number = 0;
  totalLength: number = 0;
  pageIndex: number = 0;
  pageLimit: number[] = [25, 50, 75, 100]
  ids: string[] = [];
  ddlFilter: any = '';
  IndustryCategoryList: any;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private httpService: CrudService,
    private dialog: MatDialog,
     ) { }

  ngOnInit(): void {
    this.getIndustries();
    this.getIndustryCategories();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  changePage(event) {
    this.pageIndex = event.pageIndex;
    this.limit = event.pageSize;
    this.getIndustries();
  }

  public getIndustries()
  {
    this.httpService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.Industry,
      '?IndustryCategoryId='+this.ddlFilter,
      false, prepareGetRequestHttpParams(this.pageIndex.toString(), this.limit.toString(),)
    ).subscribe(resp => {
      this.applicationResponse = resp;
      this.industry = this.applicationResponse.resources;
      this.dataSource.data = this.industry;
      this.totalLength = this.applicationResponse.totalCount;
    });
  }

  private getIndustryCategories()
  {
    this.httpService.get(
      ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT,
      CustomerModuleApiSuffixModels.Industry_CATEGORY_DROPDOWN,
      undefined,
      false,
    ).subscribe(resp => {
      this.applicationResponse = resp;
      this.IndustryCategoryList = this.applicationResponse.resources;
    });
  }




  /**To check Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  openAddDialog(industryId): void {

    var industry = new Industry();
    if (industryId) {
      this.httpService.get(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.Industry, industryId).subscribe(
        {
          next: response => {
            this.applicationResponse = response,
            industry = this.applicationResponse.resources;
            this.OpenDialogWindow(industry);
          }
        });
    }
    else {
      this.OpenDialogWindow(industry);
    }
  }

  private OpenDialogWindow(industry: Industry) {
    const dialogReff = this.dialog.open(IndustryAddEditComponent, { width: '450px', data: industry });
    dialogReff.afterClosed().subscribe(result => {
      this.selection.clear();
      this.getIndustries();
    });
  }
  enableDisableSelected(id, status: any) {
    this.enableDisable.ids = id;
    this.enableDisable.isActive = status.currentTarget.checked;
    this.httpService
      .enableDisable(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.Industry, this.enableDisable).subscribe({
        next: response => { //this.toastr.responseMessage(response);
         },
        error: err => this.errorMessage = err,
      });
  }

  removeSelected() {
    this.selection.selected.forEach(item => {
      this.ids.push(item.industryId);
    });

    if (this.ids.length > 0) {
      const message = `Are you sure you want to delete this?`;

      const dialogData = new ConfirmDialogModel("Confirm Action", message);

      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData
      });

      dialogRef.afterClosed().subscribe(dialogResult => {
        if (dialogResult) {
          //convert selected id to array
          this.deleteIndustry();
        }
      });
    }
    else {
      this.applicationResponse.statusCode = 204;
      this.applicationResponse.message = "Please select industry";
      //this.toastr.responseMessage(this.applicationResponse);
    }

  }

  deleteIndustry(): void {
    this.httpService.delete(ModulesBasedApiSuffix.CUSTOMER_MANAGEMENT, CustomerModuleApiSuffixModels.Industry, this.ids.join(','))
      .subscribe({
        next: response => {
          //this.toastr.responseMessage(response);
          this.ids = [];
          //clear check box
          this.selection = new SelectionModel<Industry>(true, []);
          this.getIndustries();
        },
        error: err => this.errorMessage = err
      });
  }

}
