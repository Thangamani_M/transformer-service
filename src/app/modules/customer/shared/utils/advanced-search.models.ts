import { defaultCountryCode } from "@app/shared";

class AdvancedSearchModel {
    constructor(advancedSearchModel?: AdvancedSearchModel) {
        this.customerTypeId = advancedSearchModel == undefined ? [] : advancedSearchModel.customerTypeId == undefined ? [] : advancedSearchModel.customerTypeId;
        this.customerStatusId = advancedSearchModel == undefined ? [] : advancedSearchModel.customerStatusId == undefined ? [] : advancedSearchModel.customerStatusId;
        this.divisionId = advancedSearchModel == undefined ? "" : advancedSearchModel.divisionId == undefined ? '' : advancedSearchModel.divisionId;
        this.branchId = advancedSearchModel == undefined ? "" : advancedSearchModel.branchId == undefined ? '' : advancedSearchModel.branchId;
        this.districtId = advancedSearchModel == undefined ? "" : advancedSearchModel.districtId == undefined ? '' : advancedSearchModel.districtId;
        this.regionId = advancedSearchModel == undefined ? "" : advancedSearchModel.regionId == undefined ? '' : advancedSearchModel.regionId;
        this.userId = advancedSearchModel == undefined ? "" : advancedSearchModel.userId == undefined ? '' : advancedSearchModel.userId;
        this.clientSearchModel = advancedSearchModel == undefined ? null : advancedSearchModel.clientSearchModel == undefined ? null : advancedSearchModel.clientSearchModel;
        this.addressSearchModel = advancedSearchModel == undefined ? null : advancedSearchModel.addressSearchModel == undefined ? null : advancedSearchModel.addressSearchModel;
        this.debtorSearchModel = advancedSearchModel == undefined ? null : advancedSearchModel.debtorSearchModel == undefined ? null : advancedSearchModel.debtorSearchModel;
        this.technicianSearchModel = advancedSearchModel == undefined ? null : advancedSearchModel.technicianSearchModel == undefined ? null : advancedSearchModel.technicianSearchModel;
        this.radioSearchModel = advancedSearchModel == undefined ? null : advancedSearchModel.radioSearchModel == undefined ? null : advancedSearchModel.radioSearchModel;
        this.optionsSearchModel = advancedSearchModel == undefined ? null : advancedSearchModel.optionsSearchModel == undefined ? null : advancedSearchModel.optionsSearchModel;
    }
    customerTypeId: [];
    customerStatusId: [];
    divisionId: string;
    districtId: string;
    regionId: string;
    branchId: string;
    clientSearchModel: ClientSearchModel;
    addressSearchModel: AddressSearchModel;
    debtorSearchModel: DebtorSearchModel;
    technicianSearchModel: TechnicianSearchModel;
    radioSearchModel:RadioSearchModel;
    optionsSearchModel:OptionsSearchModel;
    userId:string

}
class ClientSearchModel {
    constructor(clientSearchModel?: ClientSearchModel) {
        this.customerRefNo = clientSearchModel == undefined ? "" : clientSearchModel.customerRefNo == undefined ? "" : clientSearchModel.customerRefNo;
        this.firstName = clientSearchModel == undefined ? "" : clientSearchModel.firstName == undefined ? "" : clientSearchModel.firstName;
        this.lastName = clientSearchModel == undefined ? "" : clientSearchModel.lastName == undefined ? "" : clientSearchModel.lastName;
        this.SAIDNo = clientSearchModel == undefined ? "" : clientSearchModel.SAIDNo == undefined ? "" : clientSearchModel.SAIDNo;
        this.email = clientSearchModel == undefined ? "" : clientSearchModel.email == undefined ? "" : clientSearchModel.email;
        this.companyRegNo = clientSearchModel == undefined ? "" : clientSearchModel.companyRegNo == undefined ? '' : clientSearchModel.companyRegNo;
        this.mobile1 = clientSearchModel == undefined ? "" : clientSearchModel.mobile1 == undefined ? '' : clientSearchModel.mobile1;
        this.premisesNo = clientSearchModel == undefined ? "" : clientSearchModel.premisesNo == undefined ? '' : clientSearchModel.premisesNo;
        this.contactNo = clientSearchModel == undefined ? "" : clientSearchModel.contactNo == undefined ? '' : clientSearchModel.contactNo;
        this.customerCode = clientSearchModel == undefined ? "" : clientSearchModel.customerCode == undefined ? '' : clientSearchModel.customerCode;
        this.quoteNo = clientSearchModel == undefined ? "" : clientSearchModel.quoteNo == undefined ? '' : clientSearchModel.quoteNo;
        this.mobile1CountryCode = clientSearchModel == undefined ? defaultCountryCode : clientSearchModel.mobile1CountryCode == undefined ? defaultCountryCode : clientSearchModel.mobile1CountryCode;
        this.premisesNoCountryCode = clientSearchModel == undefined ? defaultCountryCode : clientSearchModel.premisesNoCountryCode == undefined ? defaultCountryCode : clientSearchModel.premisesNoCountryCode;
        this.contactNoCountryCode = clientSearchModel == undefined ? defaultCountryCode : clientSearchModel.contactNoCountryCode == undefined ? defaultCountryCode : clientSearchModel.contactNoCountryCode;
    }
    customerRefNo: string;
    firstName: string;
    lastName: string;
    SAIDNo: string;
    email: string;
    companyRegNo: string;
    premisesNo: string;
    premisesNoCountryCode: string;
    mobile1: string;
    mobile1CountryCode: string;
    contactNo:string;
    contactNoCountryCode :string;
    customerCode:string;
    quoteNo:string;


}
class AddressSearchModel {
    constructor(addressSearchModel?: AddressSearchModel) {
        this.addressCode = addressSearchModel == undefined ? "" : addressSearchModel.addressCode == undefined ? "" : addressSearchModel.addressCode;
        this.postalCode = addressSearchModel == undefined ? "" : addressSearchModel.postalCode == undefined ? "" : addressSearchModel.postalCode;
        this.buildingNo = addressSearchModel == undefined ? "" : addressSearchModel.buildingNo == undefined ? "" : addressSearchModel.buildingNo;
        this.buildingName = addressSearchModel == undefined ? "" : addressSearchModel.buildingName == undefined ? "" : addressSearchModel.buildingName;
        this.streetName = addressSearchModel == undefined ? "" : addressSearchModel.streetName == undefined ? "" : addressSearchModel.streetName;
        this.streetNo = addressSearchModel == undefined ? "" : addressSearchModel.streetNo == undefined ? "" : addressSearchModel.streetNo;
        this.suburbName = addressSearchModel == undefined ? "" : addressSearchModel.suburbName == undefined ? "" : addressSearchModel.suburbName;
        this.tempMainAreaName = addressSearchModel == undefined ? "" : addressSearchModel.tempMainAreaName == undefined ? "" : addressSearchModel.tempMainAreaName;
        this.mainAreaIds = addressSearchModel == undefined ? "" : addressSearchModel.mainAreaIds == undefined ? "" : addressSearchModel.mainAreaIds;
        this.subAreaId = addressSearchModel == undefined ? "" : addressSearchModel.subAreaId == undefined ? "" : addressSearchModel.subAreaId;
        this.cityName = addressSearchModel == undefined ? "" : addressSearchModel.cityName == undefined ? "" : addressSearchModel.cityName;
    }
    postalCode?: string;
    addressCode?: string;
    buildingNo?: string;
    buildingName?: string;
    streetNo?: string;
    streetName?: string;
    suburbName?: string;
    tempMainAreaName?: string;
    mainAreaIds?: string;
    subAreaId?: string;
    cityName : string;
}
class DebtorSearchModel {
    constructor(debtorSearchModel?: DebtorSearchModel) {
        this.debtorEmail = debtorSearchModel == undefined ? "" : debtorSearchModel.debtorEmail == undefined ? "" : debtorSearchModel.debtorEmail;
        this.customerRefNo = debtorSearchModel == undefined ? "" : debtorSearchModel.customerRefNo == undefined ? "" : debtorSearchModel.customerRefNo;
        this.debtor = debtorSearchModel == undefined ? "" : debtorSearchModel.debtor == undefined ? "" : debtorSearchModel.debtor;
        this.debtorSAID = debtorSearchModel == undefined ? "" : debtorSearchModel.debtorSAID == undefined ? "" : debtorSearchModel.debtorSAID;
        this.debtorStatus = debtorSearchModel == undefined ? [] : debtorSearchModel.debtorStatus == undefined ? [] : debtorSearchModel.debtorStatus;
        this.debtorSiteTypeId = debtorSearchModel == undefined ? [] : debtorSearchModel.debtorSiteTypeId == undefined ? [] : debtorSearchModel.debtorSiteTypeId;
        this.debtorGroup = debtorSearchModel == undefined ? [] : debtorSearchModel.debtorGroup == undefined ? [] : debtorSearchModel.debtorGroup;
        // this.bankAccountNo = debtorSearchModel == undefined ? "" : debtorSearchModel.bankAccountNo == undefined ? "" : debtorSearchModel.bankAccountNo;
        this.debtorRefNo = debtorSearchModel == undefined ? "" : debtorSearchModel.debtorRefNo == undefined ? "" : debtorSearchModel.debtorRefNo;
        this.paymentRefNo = debtorSearchModel == undefined ? "" : debtorSearchModel.paymentRefNo == undefined ? "" : debtorSearchModel.paymentRefNo;
        this.ProformaInvoiceNo = debtorSearchModel == undefined ? "" : debtorSearchModel.ProformaInvoiceNo == undefined ? "" : debtorSearchModel.ProformaInvoiceNo;
        this.debtorBDINumber = debtorSearchModel == undefined ? "" : debtorSearchModel.debtorBDINumber == undefined ? "" : debtorSearchModel.debtorBDINumber;
        this.debtorVatRegNo = debtorSearchModel == undefined ? "" : debtorSearchModel.debtorVatRegNo == undefined ? "" : debtorSearchModel.debtorVatRegNo;
        // this.crossRefNo = debtorSearchModel == undefined ? "" : debtorSearchModel.crossRefNo == undefined ? "" : debtorSearchModel.crossRefNo;
        // this.mobile1 = debtorSearchModel == undefined ? "" : debtorSearchModel.mobile1 == undefined ? "" : debtorSearchModel.mobile1;
        // this.mobile1CountryCode = debtorSearchModel == undefined ? defaultCountryCode : debtorSearchModel.mobile1CountryCode == undefined ? defaultCountryCode : debtorSearchModel.mobile1CountryCode;
        this.invoiceNo = debtorSearchModel == undefined ? "" : debtorSearchModel.invoiceNo == undefined ? "" : debtorSearchModel.invoiceNo;
        this.debtorCreditControllerId = debtorSearchModel == undefined ? "" : debtorSearchModel.debtorCreditControllerId == undefined ? "" : debtorSearchModel.debtorCreditControllerId;
        this.debtorCompayRegNo = debtorSearchModel == undefined ? "" : debtorSearchModel.debtorCompayRegNo == undefined ? "" : debtorSearchModel.debtorCompayRegNo;
        this.debtorContractNo = debtorSearchModel == undefined ? "" : debtorSearchModel.debtorContractNo == undefined ? "" : debtorSearchModel.debtorContractNo;
        this.debtorPaymentMethodId = debtorSearchModel == undefined ? "" : debtorSearchModel.debtorPaymentMethodId == undefined ? "" : debtorSearchModel.debtorPaymentMethodId;
    }
    debtorEmail?: string;
    customerRefNo?: string;
    debtor?: string;
    debtorSAID?: string;
    debtorStatus?: [];
    debtorSiteTypeId?: [];
    debtorGroup?: [];
    // bankAccountNo?: string;
    debtorRefNo?: string;
    paymentRefNo?: string;
    ProformaInvoiceNo?: string;
    debtorBDINumber?: string;
    debtorVatRegNo?: string;
    crossRefNo?: string;
    // mobile1?: string;
    // mobile1CountryCode?: string;
    invoiceNo?: string;
    debtorCreditControllerId?: string;
    debtorCompayRegNo?: string;
    debtorContractNo?: string;
    debtorPaymentMethodId?:string;
}

class TechnicianSearchModel {
  serviceCallNo?: string;
  technicalInvoiceNo?: string;
  OBNumber?: string;
  technicalQuoteNo?: string;
  technicalSiteId?: string;
  technicalCustomerId?: string;

  constructor(technicianSearchModel?: TechnicianSearchModel){
    this.serviceCallNo = technicianSearchModel == undefined ? "" : technicianSearchModel.serviceCallNo == undefined ? "" : technicianSearchModel.serviceCallNo;
    this.technicalInvoiceNo = technicianSearchModel == undefined ? "" : technicianSearchModel.technicalInvoiceNo == undefined ? "" : technicianSearchModel.technicalInvoiceNo;
    this.OBNumber = technicianSearchModel == undefined ? "" : technicianSearchModel.OBNumber == undefined ? "" : technicianSearchModel.OBNumber;
    this.technicalQuoteNo = technicianSearchModel == undefined ? "" : technicianSearchModel.technicalQuoteNo == undefined ? "" : technicianSearchModel.technicalQuoteNo;
    this.technicalSiteId = technicianSearchModel == undefined ? "" : technicianSearchModel.technicalSiteId == undefined ? "" : technicianSearchModel.technicalSiteId;
    this.technicalCustomerId = technicianSearchModel == undefined ? "" : technicianSearchModel.technicalCustomerId == undefined ? "" : technicianSearchModel.technicalCustomerId;
  }

}

class RadioSearchModel {
  transmitterNo? :string
  decoder? :string
  setName? :string
  cellPanicNo? :string
  cellPanicNoCountryCode? :string

  constructor(radioSearchModel?: RadioSearchModel){
    this.transmitterNo = radioSearchModel == undefined ? "" : radioSearchModel.transmitterNo == undefined ? "" : radioSearchModel.transmitterNo;
    this.decoder = radioSearchModel == undefined ? "" : radioSearchModel.decoder == undefined ? "" : radioSearchModel.decoder;
    this.setName = radioSearchModel == undefined ? "" : radioSearchModel.setName == undefined ? "" : radioSearchModel.setName;
    this.cellPanicNo = radioSearchModel == undefined ? "" : radioSearchModel.cellPanicNo == undefined ? "" : radioSearchModel.cellPanicNo;
    this.cellPanicNoCountryCode = radioSearchModel == undefined ? defaultCountryCode : radioSearchModel.cellPanicNoCountryCode == undefined ? defaultCountryCode : radioSearchModel.cellPanicNoCountryCode;
  }
}
class OptionsSearchModel {
  gridType? :string
  options? :string
  isAll:boolean;
  sortOrder:string;
  sortOrderColumn:string;
  useExactMatch:boolean;
  useAllSearch:boolean;
  useSoundexSearch:boolean;


  constructor(optionsSearchModel?: OptionsSearchModel){
    this.sortOrder = optionsSearchModel == undefined ? "" : optionsSearchModel.sortOrder == undefined ? "" : optionsSearchModel.sortOrder;
    this.sortOrderColumn = optionsSearchModel == undefined ? "" : optionsSearchModel.sortOrderColumn == undefined ? "" : optionsSearchModel.sortOrderColumn;
    this.useExactMatch = optionsSearchModel == undefined ? false : optionsSearchModel.useExactMatch == undefined ? false : optionsSearchModel.useExactMatch;
    this.useAllSearch = optionsSearchModel == undefined ? true : optionsSearchModel.useAllSearch == undefined ? true : optionsSearchModel.useAllSearch;
    this.useSoundexSearch = optionsSearchModel == undefined ? false: optionsSearchModel.useSoundexSearch == undefined ? false : optionsSearchModel.useSoundexSearch;
    this.isAll = optionsSearchModel == undefined ? false : optionsSearchModel.isAll == undefined ? false : optionsSearchModel.isAll;
  }
}

export { AddressSearchModel, AdvancedSearchModel, ClientSearchModel, DebtorSearchModel, TechnicianSearchModel ,RadioSearchModel,OptionsSearchModel};

