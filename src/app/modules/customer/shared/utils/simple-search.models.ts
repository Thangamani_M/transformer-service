import { defaultCountryCode } from "@app/shared";
import { RadioSearchModel } from "./advanced-search.models";

class SimpleSearchModel {
    constructor(simpleSearchModel?: SimpleSearchModel) {
        this.customerTypeId = simpleSearchModel == undefined ? [] : simpleSearchModel.customerTypeId == undefined ? [] : simpleSearchModel.customerTypeId;
        this.customerStatusId = simpleSearchModel == undefined ? [] : simpleSearchModel.customerStatusId == undefined ? [] : simpleSearchModel.customerStatusId;
        this.divisionId = simpleSearchModel == undefined ? "" : simpleSearchModel.divisionId == undefined ? '' : simpleSearchModel.divisionId;
        this.branchId = simpleSearchModel == undefined ? "" : simpleSearchModel.branchId == undefined ? '' : simpleSearchModel.branchId;
        this.districtId = simpleSearchModel == undefined ? "" : simpleSearchModel.districtId == undefined ? '' : simpleSearchModel.districtId;
        this.regionId = simpleSearchModel == undefined ? "" : simpleSearchModel.regionId == undefined ? '' : simpleSearchModel.regionId;
        this.clientSearchModel = simpleSearchModel == undefined ? null : simpleSearchModel.clientSearchModel == undefined ? null : simpleSearchModel.clientSearchModel;
        this.addressSearchModel = simpleSearchModel == undefined ? null : simpleSearchModel.addressSearchModel == undefined ? null : simpleSearchModel.addressSearchModel;
        this.debtorSearchModel = simpleSearchModel == undefined ? null : simpleSearchModel.debtorSearchModel == undefined ? null : simpleSearchModel.debtorSearchModel;
        this.technicianSearchModel = simpleSearchModel == undefined ? null : simpleSearchModel.technicianSearchModel == undefined ? null : simpleSearchModel.technicianSearchModel;
        this.radioSearchModel = simpleSearchModel == undefined ? null : simpleSearchModel.radioSearchModel == undefined ? null : simpleSearchModel.radioSearchModel;
    }
    customerTypeId: [];
    customerStatusId: [];
    divisionId: string;
    districtId: string;
    regionId: string;
    branchId: string;
    clientSearchModel: ClientSearchModel;
    addressSearchModel: AddressSearchModel;
    debtorSearchModel: DebtorSearchModel;
    technicianSearchModel: TechnicianSearchModel;
    radioSearchModel: RadioSearchModel;
}
class ClientSearchModel {
    constructor(clientSearchModel?: ClientSearchModel) {
        this.customerRefNo = clientSearchModel == undefined ? "" : clientSearchModel.customerRefNo == undefined ? "" : clientSearchModel.customerRefNo;
        this.firstName = clientSearchModel == undefined ? "" : clientSearchModel.firstName == undefined ? "" : clientSearchModel.firstName;
        this.lastName = clientSearchModel == undefined ? "" : clientSearchModel.lastName == undefined ? "" : clientSearchModel.lastName;
        this.SAID = clientSearchModel == undefined ? "" : clientSearchModel.SAID == undefined ? "" : clientSearchModel.SAID;
        this.email = clientSearchModel == undefined ? "" : clientSearchModel.email == undefined ? "" : clientSearchModel.email;
        this.companyRegNo = clientSearchModel == undefined ? "" : clientSearchModel.companyRegNo == undefined ? '' : clientSearchModel.companyRegNo;
        this.mobile1 = clientSearchModel == undefined ? "" : clientSearchModel.mobile1 == undefined ? '' : clientSearchModel.mobile1;
        this.premisesNo = clientSearchModel == undefined ? "" : clientSearchModel.premisesNo == undefined ? '' : clientSearchModel.premisesNo;
        this.mobile1CountryCode = clientSearchModel == undefined ? defaultCountryCode : clientSearchModel.mobile1CountryCode == undefined ? defaultCountryCode : clientSearchModel.mobile1CountryCode;
        this.premisesNoCountryCode = clientSearchModel == undefined ? defaultCountryCode : clientSearchModel.premisesNoCountryCode == undefined ? defaultCountryCode : clientSearchModel.premisesNoCountryCode;
    }
    customerRefNo: string;
    firstName: string;
    lastName: string;
    SAID: string;
    email: string;
    companyRegNo: string;
    premisesNo: string;
    premisesNoCountryCode: string;
    mobile1: string;
    mobile1CountryCode: string;
}
class AddressSearchModel {
    constructor(addressSearchModel?: AddressSearchModel) {
        this.postalCode = addressSearchModel == undefined ? "" : addressSearchModel.postalCode == undefined ? "" : addressSearchModel.postalCode;
        this.buildingNo = addressSearchModel == undefined ? "" : addressSearchModel.buildingNo == undefined ? "" : addressSearchModel.buildingNo;
        this.buildingName = addressSearchModel == undefined ? "" : addressSearchModel.buildingName == undefined ? "" : addressSearchModel.buildingName;
        this.streetName = addressSearchModel == undefined ? "" : addressSearchModel.streetName == undefined ? "" : addressSearchModel.streetName;
        this.streetNo = addressSearchModel == undefined ? "" : addressSearchModel.streetNo == undefined ? "" : addressSearchModel.streetNo;
        this.suburbName = addressSearchModel == undefined ? "" : addressSearchModel.suburbName == undefined ? "" : addressSearchModel.suburbName;
        this.tempMainAreaName = addressSearchModel == undefined ? "" : addressSearchModel.tempMainAreaName == undefined ? "" : addressSearchModel.tempMainAreaName;
        this.mainAreaName = addressSearchModel == undefined ? "" : addressSearchModel.mainAreaName == undefined ? "" : addressSearchModel.mainAreaName;
        this.subAreaName = addressSearchModel == undefined ? "" : addressSearchModel.subAreaName == undefined ? "" : addressSearchModel.subAreaName;
        this.cityName = addressSearchModel == undefined ? "" : addressSearchModel.cityName == undefined ? "" : addressSearchModel.cityName;
    }
    postalCode?: string;
    buildingNo?: string;
    buildingName?: string;
    streetNo?: string;
    streetName?: string;
    suburbName?: string;
    tempMainAreaName?: string;
    mainAreaName?: string;
    subAreaName?: string;
    cityName : string;
}
class DebtorSearchModel {
    constructor(debtorSearchModel?: DebtorSearchModel) {
        this.email = debtorSearchModel == undefined ? "" : debtorSearchModel.email == undefined ? "" : debtorSearchModel.email;
        this.customerRefNo = debtorSearchModel == undefined ? "" : debtorSearchModel.customerRefNo == undefined ? "" : debtorSearchModel.customerRefNo;
        this.debtor = debtorSearchModel == undefined ? "" : debtorSearchModel.debtor == undefined ? "" : debtorSearchModel.debtor;
        this.SAID = debtorSearchModel == undefined ? "" : debtorSearchModel.SAID == undefined ? "" : debtorSearchModel.SAID;
        this.debtorStatusId = debtorSearchModel == undefined ? [] : debtorSearchModel.debtorStatusId == undefined ? [] : debtorSearchModel.debtorStatusId;
        this.siteTypeId = debtorSearchModel == undefined ? [] : debtorSearchModel.siteTypeId == undefined ? [] : debtorSearchModel.siteTypeId;
        this.group = debtorSearchModel == undefined ? [] : debtorSearchModel.group == undefined ? [] : debtorSearchModel.group;
        this.bankAccountNo = debtorSearchModel == undefined ? "" : debtorSearchModel.bankAccountNo == undefined ? "" : debtorSearchModel.bankAccountNo;
        this.debtorRefNo = debtorSearchModel == undefined ? "" : debtorSearchModel.debtorRefNo == undefined ? "" : debtorSearchModel.debtorRefNo;
        this.paymentRefNo = debtorSearchModel == undefined ? "" : debtorSearchModel.paymentRefNo == undefined ? "" : debtorSearchModel.paymentRefNo;
        this.ProformaInvoiceNo = debtorSearchModel == undefined ? "" : debtorSearchModel.ProformaInvoiceNo == undefined ? "" : debtorSearchModel.ProformaInvoiceNo;
        this.bdiNo = debtorSearchModel == undefined ? "" : debtorSearchModel.bdiNo == undefined ? "" : debtorSearchModel.bdiNo;
        this.vatRegNo = debtorSearchModel == undefined ? "" : debtorSearchModel.vatRegNo == undefined ? "" : debtorSearchModel.vatRegNo;
        this.crossRefNo = debtorSearchModel == undefined ? "" : debtorSearchModel.crossRefNo == undefined ? "" : debtorSearchModel.crossRefNo;
        this.mobile1 = debtorSearchModel == undefined ? "" : debtorSearchModel.mobile1 == undefined ? "" : debtorSearchModel.mobile1;
        this.mobile1CountryCode = debtorSearchModel == undefined ? defaultCountryCode : debtorSearchModel.mobile1CountryCode == undefined ? defaultCountryCode : debtorSearchModel.mobile1CountryCode;
        this.invoiceNo = debtorSearchModel == undefined ? "" : debtorSearchModel.invoiceNo == undefined ? "" : debtorSearchModel.invoiceNo;
        this.creditController = debtorSearchModel == undefined ? "" : debtorSearchModel.creditController == undefined ? "" : debtorSearchModel.creditController;
        this.companyRegNo = debtorSearchModel == undefined ? "" : debtorSearchModel.companyRegNo == undefined ? "" : debtorSearchModel.companyRegNo;
        this.contractRefNo = debtorSearchModel == undefined ? "" : debtorSearchModel.contractRefNo == undefined ? "" : debtorSearchModel.contractRefNo;
        this.paymentMethodId = debtorSearchModel == undefined ? "" : debtorSearchModel.paymentMethodId == undefined ? "" : debtorSearchModel.paymentMethodId;
    }
    email?: string;
    customerRefNo?: string;
    debtor?: string;
    SAID?: string;
    debtorStatusId?: [];
    siteTypeId?: [];
    group?: [];
    bankAccountNo?: string;
    debtorRefNo?: string;
    paymentRefNo?: string;
    ProformaInvoiceNo?: string;
    bdiNo?: string;
    vatRegNo?: string;
    crossRefNo?: string;
    mobile1?: string;
    mobile1CountryCode?: string;
    invoiceNo?: string;
    creditController?: string;
    companyRegNo?: string;
    contractRefNo?: string;
    paymentMethodId?:string;
}

class TechnicianSearchModel {
  serviceCallNo?: string;
  invoiceNo?: string;
  OBNumber?: string;
  quoteNo?: string;
  siteId?: string;
  customerRefNo?: string;

  constructor(technicianSearchModel?: TechnicianSearchModel){
    this.serviceCallNo = technicianSearchModel == undefined ? "" : technicianSearchModel.serviceCallNo == undefined ? "" : technicianSearchModel.serviceCallNo;
    this.invoiceNo = technicianSearchModel == undefined ? "" : technicianSearchModel.invoiceNo == undefined ? "" : technicianSearchModel.invoiceNo;
    this.OBNumber = technicianSearchModel == undefined ? "" : technicianSearchModel.OBNumber == undefined ? "" : technicianSearchModel.OBNumber;
    this.quoteNo = technicianSearchModel == undefined ? "" : technicianSearchModel.quoteNo == undefined ? "" : technicianSearchModel.quoteNo;
    this.siteId = technicianSearchModel == undefined ? "" : technicianSearchModel.siteId == undefined ? "" : technicianSearchModel.siteId;
    this.customerRefNo = technicianSearchModel == undefined ? "" : technicianSearchModel.customerRefNo == undefined ? "" : technicianSearchModel.customerRefNo;
  }

}

export { AddressSearchModel, SimpleSearchModel, ClientSearchModel, DebtorSearchModel, TechnicianSearchModel };

