export enum CancellationType {
    CANCELLATION = "Cancellation",
    SUSPENDED = "Suspended",
}