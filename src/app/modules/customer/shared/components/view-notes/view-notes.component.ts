import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { RxjsService } from '@app/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
@Component({
  templateUrl: './view-notes.component.html',
  // styleUrls: ['./view-notes.component.scss'],
})

export class SharedCustomerViewNotesComponent implements OnInit {
  viewNotesObject: any
  @Output() outputData = new EventEmitter<any>();
  constructor(
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private rxjsService: RxjsService) {
  }
  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    this.viewNotesObject = this.config.data
  }

  dialogClose(): void {
    this.ref.close(false);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }
}
