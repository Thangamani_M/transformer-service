import { Component, ElementRef, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';

@Component({
  selector: 'app-page-quick-action',
  templateUrl: './page-quick-action.component.html',
  styleUrls: ['./page-quick-action.component.scss']
})
export class PageQuickActionComponent implements OnInit {

  isnavExpand: boolean;
  @ViewChild('quickaction_sidebar', {static: false}) quickActionSidebar: ElementRef;
  @ViewChild('quickaction_menu', {static: false}) quickActionMenu: ElementRef;
  @Input() actionTabList: any;
  @Input() height: any = '40vh';
  @Output() actionClicked = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }
  
  ngOnChanges(changes: SimpleChanges) {
    if (changes?.actionTabList?.currentValue != changes?.actionTabList?.previousValue) {
      this.actionTabList = this.actionTabList;
    }
  }

  openNav() {
    if (this.isnavExpand) {
      this.quickActionSidebar.nativeElement.style.width = "250px";
    } else {
      this.quickActionSidebar.nativeElement.style.width = "0px";
    }
    this.quickActionMenu.nativeElement.style.right = "12px";
    this.quickActionMenu.nativeElement.style.visibility = "visible";
    this.isnavExpand = !this.isnavExpand
  }

  closeNav() {
    this.quickActionSidebar.nativeElement.style.width = "0";
    this.quickActionMenu.nativeElement.style.right = "0";
    this.quickActionMenu.nativeElement.style.visibility = "hidden";
  }

  onActionTabClicked(action,i) {
    this.actionTabList?.columns?.map(el => {
      if(el?.id == i) {
        el.isSelected = true;
      } else {
        el.isSelected = false;
      }
      return el;
    });
    this.actionClicked.emit({type: action, index: i})
  }
}
