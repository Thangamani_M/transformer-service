import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatIconModule } from "@angular/material";
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ScrollPanelModule } from "primeng/scrollpanel";
import { PageQuickActionComponent, SharedCustomerAddNotesComponent, SharedCustomerLinkCustomerComponent, SharedCustomerViewCallVerificationComponent, SharedCustomerViewNotesComponent } from "./components";

@NgModule({
    declarations: [PageQuickActionComponent, 
      SharedCustomerViewNotesComponent,SharedCustomerAddNotesComponent,SharedCustomerViewCallVerificationComponent,SharedCustomerLinkCustomerComponent],
    imports: [ CommonModule, MatIconModule, SharedModule,MaterialModule,
      ReactiveFormsModule, FormsModule, ScrollPanelModule],
    providers: [],
    exports: [PageQuickActionComponent, SharedCustomerViewNotesComponent,SharedCustomerAddNotesComponent,SharedCustomerViewCallVerificationComponent,SharedCustomerLinkCustomerComponent],
    entryComponents: [SharedCustomerViewNotesComponent,SharedCustomerAddNotesComponent,SharedCustomerViewCallVerificationComponent,SharedCustomerLinkCustomerComponent]
  })
  export class CustomerSharedModule { }
