export class CustomerStatus{
    customerStatusId:any;
    customerStatusName:string;
    description:string;
    cssClass:string;
    isDeleted:boolean;
    isActive:boolean;
}