class SafeEntryRequestAddEditModal {
    constructor(safeEntryRequestAddEditModal?: SafeEntryRequestAddEditModal) {
        this.customerId=safeEntryRequestAddEditModal?safeEntryRequestAddEditModal.customerId==undefined?'':safeEntryRequestAddEditModal.customerId:'';
        this.customerAddressId = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.customerAddressId == undefined ? '' :safeEntryRequestAddEditModal.customerAddressId : '';
        this.requestDate = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.requestDate == undefined ? '' : safeEntryRequestAddEditModal.requestDate : '';
        this.requestTime = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.requestTime == undefined ? '' : safeEntryRequestAddEditModal.requestTime : '';
        this.safeEntryRequestPremisesTypeId = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.safeEntryRequestPremisesTypeId == undefined ? '' : safeEntryRequestAddEditModal.safeEntryRequestPremisesTypeId : '';
        this.vehicleRegistrationNumber = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.vehicleRegistrationNumber == undefined ? '' : safeEntryRequestAddEditModal.vehicleRegistrationNumber : '';
        this.vehicleMakeId = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.vehicleMakeId == undefined ? null : safeEntryRequestAddEditModal.vehicleMakeId : null;
        this.vehicleModelId = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.vehicleModelId == undefined ?null : safeEntryRequestAddEditModal.vehicleModelId : null;
        this.vehicleColorId = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.vehicleColorId == undefined ? null : safeEntryRequestAddEditModal.vehicleColorId : null;
        this.vehicleMake = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.vehicleMake == undefined ? '' : safeEntryRequestAddEditModal.vehicleMake : '';
        this.vehicleColor = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.vehicleColor == undefined ? '' : safeEntryRequestAddEditModal.vehicleColor : '';
        this.vehicleModel = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.vehicleModel == undefined ? '' : safeEntryRequestAddEditModal.vehicleModel : '';
        this.notes = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.notes == undefined ? '' : safeEntryRequestAddEditModal.notes : '';
        this.safeEntryRequestModeTypeId = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.safeEntryRequestModeTypeId == undefined ? 1 : safeEntryRequestAddEditModal.safeEntryRequestModeTypeId : 1;
        this.contactList = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.contactList == undefined ? [] : safeEntryRequestAddEditModal.contactList : [];
        this.isActive = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.isActive == undefined ? true : safeEntryRequestAddEditModal.isActive : true;
        this.createdUserId = safeEntryRequestAddEditModal ? safeEntryRequestAddEditModal.createdUserId = undefined? '': safeEntryRequestAddEditModal.createdUserId: ''
    }
    customerId: string;
    vehicleMake: string;
    vehicleColor:string;
    vehicleModel: string;
    customerAddressId: string;
    requestDate: string;
    requestTime: string;
    safeEntryRequestPremisesTypeId: string;
    vehicleRegistrationNumber: string;
    vehicleMakeId: number;
    vehicleModelId: number;
    vehicleColorId: number
    notes: string;
    safeEntryRequestModeTypeId: number;
    contactList: ContactListModel[];
    isActive: boolean;
    createdUserId: string

}
class ContactListModel {
    constructor(contactListModel?: ContactListModel) {
        this.keyholderId = contactListModel ? contactListModel.keyholderId == undefined? null: contactListModel.keyholderId:null;
        this.contactName = contactListModel ? contactListModel.contactName == undefined? '': contactListModel.contactName:''
        this.contactNoCountryCode = contactListModel ? contactListModel.contactNoCountryCode == undefined? '+27': contactListModel.contactNoCountryCode:'+27'
        this.contactNumber = contactListModel ? contactListModel.contactNumber == undefined? '' : contactListModel.contactNumber:''
        this.customerNumber = contactListModel ? contactListModel.customerNumber == undefined? '' : contactListModel.customerNumber:''
    }

    keyholderId?: string;
    contactName?: string;
    contactNoCountryCode?: string;
    contactNumber?: string;
    customerNumber?:string
}

export { SafeEntryRequestAddEditModal , ContactListModel}
