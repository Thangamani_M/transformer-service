class PaymentTransferModel {
    InvoicePaymentAndOverrideId?: string;
    creditNoteDocumentDate?: string;
    invoiceTransactionDescriptionId?: string;
    transactionNotes?: string;
  
    constructor(transactionProcessModel: PaymentTransferModel) {
        this.InvoicePaymentAndOverrideId = transactionProcessModel?.InvoicePaymentAndOverrideId != undefined ? transactionProcessModel?.InvoicePaymentAndOverrideId : '';
        this.creditNoteDocumentDate = transactionProcessModel?.creditNoteDocumentDate != undefined ? transactionProcessModel?.creditNoteDocumentDate : '';
        this.invoiceTransactionDescriptionId = transactionProcessModel?.invoiceTransactionDescriptionId != undefined ? transactionProcessModel?.invoiceTransactionDescriptionId : '';
        this.transactionNotes = transactionProcessModel?.transactionNotes != undefined ? transactionProcessModel?.transactionNotes : '';
    }
}
export { PaymentTransferModel };

