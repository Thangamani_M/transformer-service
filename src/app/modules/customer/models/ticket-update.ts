class TicketManualUpdateModel{
    constructor(ticketManualUpdateModel?:TicketManualUpdateModel){
        this.ticketId=ticketManualUpdateModel?ticketManualUpdateModel.ticketId==undefined?'':ticketManualUpdateModel.ticketId:'';
        this.ticketStatusId=ticketManualUpdateModel?ticketManualUpdateModel.ticketStatusId==undefined?'':ticketManualUpdateModel.ticketStatusId:'';
         this.assignedTo=ticketManualUpdateModel?ticketManualUpdateModel.assignedTo==undefined?'':ticketManualUpdateModel.assignedTo:'';
         this.assignedBy=ticketManualUpdateModel?ticketManualUpdateModel.assignedBy==undefined?'':ticketManualUpdateModel.assignedBy:'';
         this.userGroupId=ticketManualUpdateModel?ticketManualUpdateModel.userGroupId==undefined?'':ticketManualUpdateModel.userGroupId:'';
         this.description=ticketManualUpdateModel?ticketManualUpdateModel.description==undefined?'':ticketManualUpdateModel.description:'';
    }
    ticketId: any;
    ticketStatusId: string;
    assignedTo: string;
    assignedBy: string;
    userGroupId: string;
    description: string;
}
export{TicketManualUpdateModel}