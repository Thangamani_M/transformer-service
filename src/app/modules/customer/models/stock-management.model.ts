class BinLocation{
    constructor(binLocation?:BinLocation){
        this.BinLocationCode=binLocation?binLocation.BinLocationCode==undefined?'':binLocation.BinLocationCode:'';
        this.binLocationDetailList=binLocation?binLocation.binLocationDetailList==undefined?null:binLocation.binLocationDetailList:null;
       
    }
    BinLocationCode?: string 
    binLocationDetailList:BinLocationDetailList[];
}

class BinLocationDetailList{
    constructor(item?:BinLocationDetailList){    
        this.BinLocationCode=item?item.BinLocationCode==undefined?'':item.BinLocationCode:'';
        this.SerialNumber=item?item.SerialNumber==undefined?'':item.SerialNumber:'';
        this.Barcode=item?item.Barcode==undefined?'':item.Barcode:'';
        this.SupplierWarrentyExpiryDate=item?item.SupplierWarrentyExpiryDate==undefined?'':item.SupplierWarrentyExpiryDate:'';
        this.AgingDays=item?item.AgingDays==undefined?'':item.AgingDays:'';
    }
    BinLocationCode:string;
    SerialNumber:string;
    Barcode:string;
    SupplierWarrentyExpiryDate:string;
    AgingDays:string;
}
export   {BinLocation,BinLocationDetailList}