
class CustomerCategoriesModel {
    constructor(customerCategoriesAddEditModel?: CustomerCategoriesModel) {
        this.createdUserId = customerCategoriesAddEditModel ? customerCategoriesAddEditModel.createdUserId == undefined ? '' : customerCategoriesAddEditModel.createdUserId : '';
        this.modifiedUserId = customerCategoriesAddEditModel ? customerCategoriesAddEditModel.modifiedUserId == undefined ? '' : customerCategoriesAddEditModel.modifiedUserId : '';
        this.customerId = customerCategoriesAddEditModel ? customerCategoriesAddEditModel.customerId == undefined ? null : customerCategoriesAddEditModel.customerId : null;
        this.customerAddressId = customerCategoriesAddEditModel ? customerCategoriesAddEditModel.customerAddressId == undefined ? null : customerCategoriesAddEditModel.customerAddressId : null;
        this.customCategoryConfigId = customerCategoriesAddEditModel ? customerCategoriesAddEditModel.customCategoryConfigId == undefined ? null : customerCategoriesAddEditModel.customCategoryConfigId : null;
        this.customerCategoryValueId = customerCategoriesAddEditModel ? customerCategoriesAddEditModel.customerCategoryValueId == undefined ? null : customerCategoriesAddEditModel.customerCategoryValueId : null;
        this.value = customerCategoriesAddEditModel ? customerCategoriesAddEditModel.value == undefined ? null : customerCategoriesAddEditModel.value : null;
    }
    createdUserId?: string
    modifiedUserId?: string
    customerId?: string;
    customerAddressId?: Date;    
    customCategoryConfigId: string;
    customerCategoryValueId: string;
    value: string;
}
export { CustomerCategoriesModel }