class BalanceOfContractDocumentCaptureModel {
    constructor(balanceOfContractDocumentCaptureModel?: BalanceOfContractDocumentCaptureModel) {
        this.notes = balanceOfContractDocumentCaptureModel == undefined ? '' : balanceOfContractDocumentCaptureModel.notes == undefined ? '' : balanceOfContractDocumentCaptureModel.notes;
        this.documentConfigId = balanceOfContractDocumentCaptureModel == undefined ? '' : balanceOfContractDocumentCaptureModel.documentConfigId == undefined ? '' : balanceOfContractDocumentCaptureModel.documentConfigId;
        this.documentName = balanceOfContractDocumentCaptureModel == undefined ? '' : balanceOfContractDocumentCaptureModel.documentName == undefined ? '' : balanceOfContractDocumentCaptureModel.documentName;
    }
    notes: string;
    documentConfigId?:string;
    documentName?:string;
}

export { BalanceOfContractDocumentCaptureModel }