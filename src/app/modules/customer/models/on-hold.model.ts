
class OnHoldModel {
    constructor(onHoldModel?: OnHoldModel) {
        this.OnHoldStatusConfigId = onHoldModel ? onHoldModel.OnHoldStatusConfigId == undefined ? '' : onHoldModel.OnHoldStatusConfigId : '';
        this.noticeDays = onHoldModel ? onHoldModel.noticeDays == undefined ? '' : onHoldModel.noticeDays : '';
        this.toVoidDays = onHoldModel ? onHoldModel.toVoidDays == undefined ? '' : onHoldModel.toVoidDays : '';
        this.description = onHoldModel ? onHoldModel.description == undefined ? '' : onHoldModel.description : '';
        this.isActive = onHoldModel ? onHoldModel.isActive == undefined ? false : onHoldModel.isActive : false;
        this.modifiedUserId = onHoldModel ? onHoldModel.modifiedUserId == undefined ? '' : onHoldModel.modifiedUserId : '';
        
    }
    
    OnHoldStatusConfigId?:string;
    noticeDays ?: string;
    toVoidDays?:string;
    description?: string;
    isActive?:boolean;
    modifiedUserId?:string;

}
export { OnHoldModel }


