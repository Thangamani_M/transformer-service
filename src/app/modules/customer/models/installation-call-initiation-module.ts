
class InstallationCallinitiationAddEditModel {
    constructor(installationCallinitiationAddEditModel?: InstallationCallinitiationAddEditModel) {
        // this.address = billOfMaterialModel ? billOfMaterialModel.address == undefined ? '-' : billOfMaterialModel.address : '-';
        // if(installationCallinitiationAddEditModel != undefined){
        this.testRunConfigId = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.testRunConfigId == undefined ? '' : installationCallinitiationAddEditModel.testRunConfigId : '';
        this.testRunPeriodId = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.testRunPeriodId == undefined ? '' : installationCallinitiationAddEditModel.testRunPeriodId : '';
        this.executionTime = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.executionTime == undefined ? '' : installationCallinitiationAddEditModel.executionTime : '';
        this.overallExecutionTime = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.overallExecutionTime == undefined ? '' : installationCallinitiationAddEditModel.overallExecutionTime : '';
        this.testRunTime = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.testRunTime == undefined ? '' : installationCallinitiationAddEditModel.testRunTime : '';
        this.testRunPeriodName = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.testRunPeriodName == undefined ? '' : installationCallinitiationAddEditModel.testRunPeriodName : '';
        this.divisionIds = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.divisionIds == undefined ? '' : installationCallinitiationAddEditModel.divisionIds : '';
        this.division = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.division == undefined ? '' : installationCallinitiationAddEditModel.division : '';
        this.debtor = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.debtor == undefined ? '' : installationCallinitiationAddEditModel.debtor : '';
        this.debtorId = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.debtorId == undefined ? '' : installationCallinitiationAddEditModel.debtorId : '';
        this.debtorAdditionalInfoId = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.debtorAdditionalInfoId == undefined ? '' : installationCallinitiationAddEditModel.debtorAdditionalInfoId : '';

        this.isNew = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isNew == undefined ? false : installationCallinitiationAddEditModel.isNew : false;
        this.isScheduled = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isScheduled == undefined ? false : installationCallinitiationAddEditModel.isScheduled : false;
        this.isTempDone = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isTempDone == undefined ? false : installationCallinitiationAddEditModel.isTempDone : false;
        this.isInvoiced = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isInvoiced == undefined ? false : installationCallinitiationAddEditModel.isInvoiced : false;
        this.isOnHold = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isOnHold == undefined ? false : installationCallinitiationAddEditModel.isOnHold : false;
        this.isVoid = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isVoid == undefined ? false : installationCallinitiationAddEditModel.isVoid : false;
        this.isReferToSales = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isReferToSales == undefined ? false : installationCallinitiationAddEditModel.isReferToSales : false;
        this.isInstallPreCheck = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isInstallPreCheck == undefined ? false : installationCallinitiationAddEditModel.isInstallPreCheck : false;
        this.isInWarrenty = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isInWarrenty == undefined ? false : installationCallinitiationAddEditModel.isInWarrenty : false;
        this.isRecall = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isRecall == undefined ? false : installationCallinitiationAddEditModel.isRecall : false;
        this.isCode50 = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isCode50 == undefined ? false : installationCallinitiationAddEditModel.isCode50 : false;
        this.isPRCall = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isPRCall == undefined ? false : installationCallinitiationAddEditModel.isPRCall : false;
        this.isSaveOffer = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isSaveOffer == undefined ? false : installationCallinitiationAddEditModel.isSaveOffer : false;
        this.isCon = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isCon == undefined ? false : installationCallinitiationAddEditModel.isCon : false;
        this.isAddressVerified = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.isAddressVerified == undefined ? null : installationCallinitiationAddEditModel.isAddressVerified : null;
        this.financialYearId = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.financialYearId == undefined ? '' : installationCallinitiationAddEditModel.financialYearId : '';
        this.contactNumber = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.contactNumber == undefined ? '' : installationCallinitiationAddEditModel.contactNumber : '';
        this.contact = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.contact == undefined ? '' : installationCallinitiationAddEditModel.contact : '';
        this.contactId = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.contactId == undefined ? '' : installationCallinitiationAddEditModel.contactId : '';
        this.alternateContact = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.alternateContact == undefined ? '' : installationCallinitiationAddEditModel.alternateContact : '';
        this.alternateContactId = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.alternateContactId == undefined ? '' : installationCallinitiationAddEditModel.alternateContactId : '';
        this.alternateContactNumber = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.alternateContactNumber == undefined ? '' : installationCallinitiationAddEditModel.alternateContactNumber : '';
        this.priorityid = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.priorityid == undefined ? '' : installationCallinitiationAddEditModel.priorityid : '';
        this.panelTypeConfigId = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.panelTypeConfigId == undefined ? '' : installationCallinitiationAddEditModel.panelTypeConfigId : '';
        this.specialProjectId = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.specialProjectId == undefined ? '' : installationCallinitiationAddEditModel.specialProjectId : '';
        this.projectEndDate = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.projectEndDate == undefined ? '' : installationCallinitiationAddEditModel.projectEndDate : '';
        this.addressId = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.addressId == undefined ? '' : installationCallinitiationAddEditModel.addressId : '';
        this.installationPartitions = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.installationPartitions == undefined ? [] : installationCallinitiationAddEditModel.installationPartitions : [];
        this.ismainPartation = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.ismainPartation == undefined ? false : installationCallinitiationAddEditModel.ismainPartation : false;
        this.mainPartationCustomerId = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.mainPartationCustomerId == undefined ? '' : installationCallinitiationAddEditModel.mainPartationCustomerId : '';
        this.notes = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.notes == undefined ? '' : installationCallinitiationAddEditModel.notes : '';
        this.techInstruction = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.techInstruction == undefined ? '' : installationCallinitiationAddEditModel.techInstruction : '';
        this.serviceInfo = {
            callInitiationServiceInfoId: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['callInitiationServiceInfoId'] == undefined ? '' : installationCallinitiationAddEditModel['callInitiationServiceInfoId'] : '',
            systemTypeId: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['systemTypeId'] == undefined ? '' : installationCallinitiationAddEditModel['systemTypeId'] : '',
            systemTypeSubCategoryId: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['systemTypeSubCategoryId'] == undefined ? '' : installationCallinitiationAddEditModel['systemTypeSubCategoryId'] : '',
            serviceSubTypeId: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['serviceSubTypeId'] == undefined ? '' : installationCallinitiationAddEditModel['serviceSubTypeId'] : '',
            faultDescriptionId: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['faultDescriptionId'] == undefined ? '' : installationCallinitiationAddEditModel['faultDescriptionId'] : '',
            jobTypeId: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['jobTypeId'] == undefined ? '' : installationCallinitiationAddEditModel['jobTypeId'] : '',
            description: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['description'] == undefined ? '' : installationCallinitiationAddEditModel['description'] : '',
        }
        this.appointmentInfo = {
            appointmentId: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['appointmentId'] == undefined ? '' : installationCallinitiationAddEditModel['appointmentId'] : '',
            nextAvailableAppointment: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['nextAvailableAppointment'] == undefined ? '' : installationCallinitiationAddEditModel['nextAvailableAppointment'] : '',
            nextAvailableAppointmentCheckbox: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['nextAvailableAppointmentCheckbox'] == undefined ? false : installationCallinitiationAddEditModel['nextAvailableAppointmentCheckbox'] : false,
            scheduledStartDate: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['scheduledStartDate'] == undefined ? '' : installationCallinitiationAddEditModel['scheduledStartDate'] : '',
            scheduledEndDate: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['scheduledEndDate'] == undefined ? '' : installationCallinitiationAddEditModel['scheduledEndDate'] : '',
            diagnosisId: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['diagnosisId'] == undefined ? '' : installationCallinitiationAddEditModel['diagnosisId'] : '',
            wireman: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['wireman'] == undefined ? '' : installationCallinitiationAddEditModel['wireman'] : '',
            estimatedTime: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['estimatedTime'] == undefined ? '' : installationCallinitiationAddEditModel['estimatedTime'] : '',
            actualTime: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['actualTime'] == undefined ? '' : installationCallinitiationAddEditModel['actualTime'] : '',
        }
        this.nkaServiceCall = {
            isPOAccepted: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['isPOAccepted'] == undefined ? false : installationCallinitiationAddEditModel['isPOAccepted'] : false,
            isPONotAccepted: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['isPONotAccepted'] == undefined ? false : installationCallinitiationAddEditModel['isPONotAccepted'] : false,
            poNumber: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['poNumber'] == undefined ? '' : installationCallinitiationAddEditModel['poNumber'] : '',
            poAttachments: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['poAttachments'] == undefined ? '' : installationCallinitiationAddEditModel['poAttachments'] : '',
            poAmount: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['poAmount'] == undefined ? '' : installationCallinitiationAddEditModel['poAmount'] : '',
            poComment: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['poComment'] == undefined ? '' : installationCallinitiationAddEditModel['poComment'] : '',
            path: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['path'] == undefined ? undefined : installationCallinitiationAddEditModel['path'] : undefined,
            attachmentUrl: installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel['attachmentUrl'] == undefined ? '' : installationCallinitiationAddEditModel['attachmentUrl'] : '',
        }
        // }

        // installationCallinitiationAddEditModel?installationCallinitiationAddEditModel.serviceInfo==undefined?{}:installationCallinitiationAddEditModel.financialYearId:null;
        // this.TestRunConfigDetails = installationCallinitiationAddEditModel ? installationCallinitiationAddEditModel.TestRunConfigDetails == undefined ? [] : installationCallinitiationAddEditModel.TestRunConfigDetails : [];

    }
    testRunConfigId?: string
    testRunPeriodId?: string;
    executionTime?: string;
    overallExecutionTime?: string;
    testRunTime: string;
    testRunPeriodName?: string;
    divisionIds?: string;
    division?: string;
    contact?: string;
    contactId?: string;
    debtor?:string;
    debtorId?:string;
    debtorAdditionalInfoId?:string;
    isNew?: boolean;
    isScheduled?: boolean;
    isTempDone?: boolean;
    isInvoiced?: boolean;
    isOnHold?: boolean;
    isVoid?: boolean;
    isReferToSales?: boolean;
    isInstallPreCheck?: boolean;
    isInWarrenty?: boolean;
    isRecall?: boolean;
    isCode50?: boolean;
    isPRCall?: boolean;
    isSaveOffer?: boolean;
    isCon?: boolean;
    isAddressVerified?: boolean;
    financialYearId?: string;
    contactNumber: string;
    alternateContact: string;
    alternateContactId: string
    alternateContactNumber: string;
    priorityid: string
    panelTypeConfigId: string;
    specialProjectId?: string;
    projectEndDate?: string;
    addressId?: string;
    installationPartitions?: PartationsModel[];
    ismainPartation?: boolean;
    mainPartationCustomerId?: string;
    serviceInfo?: ServiceInfoModel = {};
    appointmentInfo?: AppointmentInfoModel = {};
    nkaServiceCall?: nkaServiceCallModel = {};
    techInstruction: string;
    notes: string

}

class PartationsModel {
    constructor(partationsModel: PartationsModel) {
        this.callInitiationPartitionId = partationsModel ? partationsModel.callInitiationPartitionId == undefined ? '' : partationsModel.callInitiationPartitionId : '';
        this.partitionId = partationsModel ? partationsModel.partitionId == undefined ? '' : partationsModel.partitionId : '';
        this.partitionCode = partationsModel ? partationsModel.partitionCode == undefined ? '' : partationsModel.partitionCode : '';
        this.partitionName = partationsModel ? partationsModel.partitionName == undefined ? '' : partationsModel.partitionName : '';
        this.mainPartition = partationsModel ? partationsModel.mainPartition == undefined ? false : partationsModel.mainPartition : false;

    }
    callInitiationPartitionId: string;
    partitionId: string;
    partitionCode: string;
    partitionName: string;
    mainPartition: boolean
}

interface ServiceInfoModel {
    // constructor(serviceInfoModel :ServiceInfoModel){
    //     this.callInitiationServiceInfoId=serviceInfoModel?serviceInfoModel.callInitiationServiceInfoId==undefined?'':serviceInfoModel.callInitiationServiceInfoId:'';
    //     this.systemTypeId=serviceInfoModel?serviceInfoModel.systemTypeId==undefined?'':serviceInfoModel.systemTypeId:'';
    //     this.systemTypeSubCategoryId=serviceInfoModel?serviceInfoModel.systemTypeSubCategoryId==undefined?'':serviceInfoModel.systemTypeSubCategoryId:'';
    //     this.serviceSubTypeId=serviceInfoModel?serviceInfoModel.serviceSubTypeId==undefined?'':serviceInfoModel.serviceSubTypeId:'';
    //     this.faultDescriptionId=serviceInfoModel?serviceInfoModel.faultDescriptionId==undefined?'':serviceInfoModel.faultDescriptionId:'';
    //     this.jobTypeId=serviceInfoModel?serviceInfoModel.jobTypeId==undefined?'':serviceInfoModel.jobTypeId:'';
    //     this.description=serviceInfoModel?serviceInfoModel.description==undefined?'':serviceInfoModel.description:'';
    // }
    callInitiationServiceInfoId?: string;
    systemTypeId?: string;
    systemTypeSubCategoryId?: string;
    serviceSubTypeId?: string;
    faultDescriptionId?: string;
    jobTypeId?: string;
    description?: string;
}


interface AppointmentInfoModel {
    // constructor(serviceInfoModel :ServiceInfoModel){
    //     this.callInitiationServiceInfoId=serviceInfoModel?serviceInfoModel.callInitiationServiceInfoId==undefined?'':serviceInfoModel.callInitiationServiceInfoId:'';
    //     this.systemTypeId=serviceInfoModel?serviceInfoModel.systemTypeId==undefined?'':serviceInfoModel.systemTypeId:'';
    //     this.systemTypeSubCategoryId=serviceInfoModel?serviceInfoModel.systemTypeSubCategoryId==undefined?'':serviceInfoModel.systemTypeSubCategoryId:'';
    //     this.serviceSubTypeId=serviceInfoModel?serviceInfoModel.serviceSubTypeId==undefined?'':serviceInfoModel.serviceSubTypeId:'';
    //     this.faultDescriptionId=serviceInfoModel?serviceInfoModel.faultDescriptionId==undefined?'':serviceInfoModel.faultDescriptionId:'';
    //     this.jobTypeId=serviceInfoModel?serviceInfoModel.jobTypeId==undefined?'':serviceInfoModel.jobTypeId:'';
    //     this.description=serviceInfoModel?serviceInfoModel.description==undefined?'':serviceInfoModel.description:'';
    // }
    appointmentId?: string
    nextAvailableAppointment?: string;
    nextAvailableAppointmentCheckbox?: boolean
    scheduledStartDate?: string;
    scheduledEndDate?: string;
    wireman?: string;
    diagnosisId?: string;
    estimatedTime?: string;
    actualTime?: string;
}

interface nkaServiceCallModel {
    isPOAccepted?: boolean
    isPONotAccepted?: boolean
    poNumber?: string;
    poAttachments?: string;
    poAmount?: string;
    poComment?: string;
    path?: File;
    attachmentUrl?: string;
}

class OnHoldModel {
    constructor(onHoldModel: OnHoldModel) {
        this.CallInitiationId = onHoldModel ? onHoldModel.CallInitiationId == undefined ? '' : onHoldModel.CallInitiationId : '';
        this.OnHoldReason = onHoldModel ? onHoldModel.OnHoldReason == undefined ? '' : onHoldModel.OnHoldReason : '';
        this.OnHoldFollowUpDate = onHoldModel ? onHoldModel.OnHoldFollowUpDate == undefined ? '' : onHoldModel.OnHoldFollowUpDate : '';
        this.ModifiedUserId = onHoldModel ? onHoldModel.ModifiedUserId == undefined ? '' : onHoldModel.ModifiedUserId : '';

    }
    CallInitiationId: string;
    OnHoldReason: string;
    OnHoldFollowUpDate: string;
    ModifiedUserId: string;
}

class VoidModel {
    constructor(voidModel: VoidModel) {
        this.CallInitiationId = voidModel ? voidModel.CallInitiationId == undefined ? '' : voidModel.CallInitiationId : '';
        this.VoidReason = voidModel ? voidModel.VoidReason == undefined ? '' : voidModel.VoidReason : '';
        this.ModifiedUserId = voidModel ? voidModel.ModifiedUserId == undefined ? '' : voidModel.ModifiedUserId : '';

    }
    CallInitiationId: string;
    VoidReason: string;
    ModifiedUserId: string;
}

export { InstallationCallinitiationAddEditModel, OnHoldModel, VoidModel };

