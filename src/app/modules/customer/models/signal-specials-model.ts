class SignalSpecialsModel {
    constructor(signalSpecialsModel?: SignalSpecialsModel) {
        this.signalSpecialId = signalSpecialsModel ? signalSpecialsModel.signalSpecialId == undefined ? null : signalSpecialsModel.signalSpecialId : null;
        this.createdUserId = signalSpecialsModel ? signalSpecialsModel.createdUserId == undefined ? '' : signalSpecialsModel.createdUserId : '';
        this.modifiedUserId = signalSpecialsModel ? signalSpecialsModel.modifiedUserId == undefined ? '' : signalSpecialsModel.modifiedUserId : '';
        this.customerId = signalSpecialsModel ? signalSpecialsModel.customerId == undefined ? '' : signalSpecialsModel.customerId : '';
        this.customerAddressId = signalSpecialsModel ? signalSpecialsModel.customerAddressId == undefined ? '' : signalSpecialsModel.customerAddressId : '';
        this.alarmTypeId = signalSpecialsModel ? signalSpecialsModel.alarmTypeId == undefined ? '' : signalSpecialsModel.alarmTypeId : '';
        this.decoder = signalSpecialsModel ? signalSpecialsModel.decoder == undefined ? '' : signalSpecialsModel.decoder : '';
        this.accountCode = signalSpecialsModel ? signalSpecialsModel.accountCode == undefined ? '' : signalSpecialsModel.accountCode : '';
        this.setName = signalSpecialsModel ? signalSpecialsModel.setName == undefined ? '' : signalSpecialsModel.setName : '';
        this.zoneCode = signalSpecialsModel ? signalSpecialsModel.zoneCode == undefined ? '' : signalSpecialsModel.zoneCode : '';
        this.originalAlarm = signalSpecialsModel ? signalSpecialsModel.originalAlarm == undefined ? '' : signalSpecialsModel.originalAlarm : '';
        this.eventCode = signalSpecialsModel ? signalSpecialsModel.eventCode == undefined ? '' : signalSpecialsModel.eventCode : '';
        this.partitionId = signalSpecialsModel ? signalSpecialsModel.partitionId == undefined ? '' : signalSpecialsModel.partitionId : '';

    }
    signalSpecialId: any;
    createdUserId: string;
    modifiedUserId: string;
    customerId: string;
    customerAddressId: string;
    alarmTypeId: string;
    decoder: string;
    accountCode: string;
    setName: string;
    zoneCode: string;
    originalAlarm: string;
    eventCode: string;
    partitionId:string
}

export { SignalSpecialsModel }