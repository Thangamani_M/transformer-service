
class StockSwapApprovalModel {
    constructor(stockSwapRequestModel?: StockSwapApprovalModel) {
        this.stockSwapId = stockSwapRequestModel ? stockSwapRequestModel.stockSwapId == undefined ? '' : stockSwapRequestModel.stockSwapId : '';
        this.stockSwapApprovalId = stockSwapRequestModel ? stockSwapRequestModel.stockSwapApprovalId == undefined ? '' : stockSwapRequestModel.stockSwapApprovalId : '';
        this.roleId = stockSwapRequestModel ? stockSwapRequestModel.roleId == undefined ? '' : stockSwapRequestModel.roleId : '';
        this.approvedById = stockSwapRequestModel ? stockSwapRequestModel.approvedById == undefined ? '' : stockSwapRequestModel.approvedById : '';
        this.comments = stockSwapRequestModel ? stockSwapRequestModel.comments == undefined ? '' : stockSwapRequestModel.comments : '';
        this.stockSwapApprovalStatusId = stockSwapRequestModel ? stockSwapRequestModel.stockSwapApprovalStatusId == undefined ? '' : stockSwapRequestModel.stockSwapApprovalStatusId : '';
        this.createdUserId = stockSwapRequestModel ? stockSwapRequestModel.createdUserId == undefined ? '' : stockSwapRequestModel.createdUserId : '';
        this.modifiedUserId = stockSwapRequestModel ? stockSwapRequestModel.modifiedUserId == undefined ? '' : stockSwapRequestModel.modifiedUserId : '';

    }
    stockSwapId?: string
    stockSwapApprovalId: string
    roleId: string
    approvedById: string
    comments: string
    stockSwapApprovalStatusId: string
    createdUserId: string
    modifiedUserId: string
}

export { StockSwapApprovalModel };

