export class CustomerAddress{
    customerAddressId:any;
    addressTypeId:any;
    customerId:any;
    contactPerson:string;
    addressCode:string;
    streetNumber:string;
    streetName:string;
    streetTypeId:any;
    suburbId:any;
    regionId:any;
    cityId:any;
    provinceId:any;
    countryId:any;
    postalCode:string;
    unitType:string;
    floor:string;
    buildingNumber:string;
    buildingName:string;
    phone:string;
    mobile:string;
    primaryEmail:string;
    secondaryEmail:string;
    complexName:string;
    cadastralReference:string;
    landmarkIdentifier:string;
    erfNumber:string;
    registrationDivision:string;
    roadName:string;
    routeName:string;
    usedFarmName:string;
    unitNumber:string;
    unitName:string;
    landParcelKey:string;
    longitude:string;
    latitude:string;
    description:string;
    addressStatusId:any;
    isVerified:boolean;
    isDeleted:boolean;
    isActive:boolean;
    isDefault:boolean;

    //importing for province;city,region,suburb,tier name...
    countryName:string;
    provinceName:string;
    cityName:string;
    regionName:string;
    suburbName:string;
    tierName:string;
    addressTypeName:string;
    tierId:any;
    streetTypeName:string;

}

export class CustomerAddressFormModel{
    constructor(customerAddressFormModel?: CustomerAddressFormModel) {
        this.customerAddressId = customerAddressFormModel ? customerAddressFormModel.customerAddressId == undefined ? '' : customerAddressFormModel.customerAddressId : '';
        this.addressTypeId = customerAddressFormModel ? customerAddressFormModel.addressTypeId == undefined ? "" : customerAddressFormModel.addressTypeId : "";
        this.countryId = customerAddressFormModel ? customerAddressFormModel.countryId == undefined ? "" : customerAddressFormModel.countryId : '';
        this.customerId = customerAddressFormModel ? customerAddressFormModel.customerId == undefined ? null : customerAddressFormModel.customerId : null;
        this.streetNumber = customerAddressFormModel ? customerAddressFormModel.streetNumber == undefined ? "" : customerAddressFormModel.streetNumber : '';
        this.streetTypeId = customerAddressFormModel ? customerAddressFormModel.streetTypeId == undefined ? "" : customerAddressFormModel.streetTypeId : "";
        this.suburbId = customerAddressFormModel ? customerAddressFormModel.suburbId == undefined ? "" : customerAddressFormModel.suburbId : "";
        this.cityId = customerAddressFormModel ? customerAddressFormModel.cityId == undefined ? "" : customerAddressFormModel.cityId : "";
        this.regionId = customerAddressFormModel ? customerAddressFormModel.regionId == undefined ? "" : customerAddressFormModel.regionId : '';
        this.provinceId = customerAddressFormModel ? customerAddressFormModel.provinceId == undefined ? "" : customerAddressFormModel.provinceId : "";
        this.tierId = customerAddressFormModel ? customerAddressFormModel.tierId == undefined ? "" : customerAddressFormModel.tierId : "";
        this.postalCode = customerAddressFormModel ? customerAddressFormModel.postalCode == undefined ? "" : customerAddressFormModel.postalCode : "";
        this.buildingNumber = customerAddressFormModel ? customerAddressFormModel.buildingNumber == undefined ? "" : customerAddressFormModel.buildingNumber : "";
        this.buildingName = customerAddressFormModel ? customerAddressFormModel.buildingName == undefined ? "" : customerAddressFormModel.buildingName : "";
        this.roadName = customerAddressFormModel ? customerAddressFormModel.roadName == undefined ? "" : customerAddressFormModel.roadName : "";
        this.suburbName = customerAddressFormModel ? customerAddressFormModel.suburbName == undefined ? "" : customerAddressFormModel.suburbName : "";
        this.countryName = customerAddressFormModel ? customerAddressFormModel.countryName == undefined ? "" : customerAddressFormModel.countryName : "";
        this.cityName = customerAddressFormModel ? customerAddressFormModel.cityName == undefined ? "" : customerAddressFormModel.cityName : "";
        this.regionName = customerAddressFormModel ? customerAddressFormModel.regionName == undefined ? "" : customerAddressFormModel.regionName : "";
        this.provinceName = customerAddressFormModel ? customerAddressFormModel.provinceName == undefined ? "" : customerAddressFormModel.provinceName : "";
        this.addressStatusId = customerAddressFormModel ? customerAddressFormModel.addressStatusId == undefined ? "" : customerAddressFormModel.addressStatusId : "";
        this.isDefault = customerAddressFormModel ? customerAddressFormModel.isDefault == undefined ? false : customerAddressFormModel.isDefault : false;
    }
    customerAddressId: string;
    addressTypeId: string;
    countryId: string;
    customerId: string;
    streetNumber: string;
    suburbId: string;
    cityId: string;
    regionId: string;
    provinceId: string;
    tierId: string;
    postalCode: string;
    buildingNumber: string;
    buildingName: string;
    roadName: string;
    suburbName: string;
    countryName: string;
    cityName: string;
    regionName: string;
    provinceName: string;
    addressStatusId: string;
    streetTypeId: string;
    isDefault:boolean
}

export class DebtoreditUpdateFormModel{
    constructor(debtoreditUpdateFormModel?: DebtoreditUpdateFormModel) {
        this.debtorId = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.debtorId == undefined ? "" : debtoreditUpdateFormModel.debtorId : "";
        this.debtorTypeId = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.debtorId == undefined ? "" : debtoreditUpdateFormModel.debtorId : "";
        this.titleId = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.titleId == undefined ? "" : debtoreditUpdateFormModel.titleId : "";
        this.firstName = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.firstName == undefined ? "" : debtoreditUpdateFormModel.firstName : "";
        this.lastName = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.lastName == undefined ? "" : debtoreditUpdateFormModel.lastName : "";
        this.said = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.said == undefined ? "" : debtoreditUpdateFormModel.said : "";
        this.companyName = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.companyName == undefined ? "" : debtoreditUpdateFormModel.companyName : "";
        this.companyRegNo = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.companyRegNo == undefined ? "" : debtoreditUpdateFormModel.companyRegNo : "";
        this.companyVATNo = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.companyVATNo == undefined ? "" : debtoreditUpdateFormModel.companyRegNo : "";
        this.contactId = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.contactId == undefined ? "" : debtoreditUpdateFormModel.contactId : "";
        this.mobile1CountryCode = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.mobile1CountryCode == undefined ? "+27" : debtoreditUpdateFormModel.mobile1CountryCode : "+27";
        this.mobile1 = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.mobile1 == undefined ? "" : debtoreditUpdateFormModel.mobile1 : "";
        this.mobile2CountryCode = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.mobile2CountryCode == undefined ? "+27" : debtoreditUpdateFormModel.mobile2CountryCode : "+27";
        this.mobile2 = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.mobile2 == undefined ? "" : debtoreditUpdateFormModel.mobile2 : "";
        this.email = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.email == undefined ? "" : debtoreditUpdateFormModel.email : "";
        this.alternateEmail = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.alternateEmail == undefined ? "" : debtoreditUpdateFormModel.alternateEmail : "";
        this.billingAddressId = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.billingAddressId == undefined ? "" : debtoreditUpdateFormModel.billingAddressId : "";
        this.addressLine1 = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.addressLine1 == undefined ? "" : debtoreditUpdateFormModel.addressLine1 : "";
        this.addressLine2 = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.addressLine2 == undefined ? "" : debtoreditUpdateFormModel.addressLine2 : "";
        this.addressLine3 = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.addressLine3 == undefined ? "" : debtoreditUpdateFormModel.addressLine3 : "";
        this.addressLine4 = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.addressLine4 == undefined ? "" : debtoreditUpdateFormModel.addressLine4 : "";
        this.postalCode = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.postalCode == undefined ? "" : debtoreditUpdateFormModel.postalCode : "";
        this.debtorAdditionalInfoId = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.debtorAdditionalInfoId == undefined ? "" : debtoreditUpdateFormModel.debtorAdditionalInfoId : "";
        this.isBillingSameAsSiteAddress = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.isBillingSameAsSiteAddress == undefined ? false : debtoreditUpdateFormModel.isBillingSameAsSiteAddress : false;
        this.isExcludeSmallBalanceAdjustment = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.isExcludeSmallBalanceAdjustment == undefined ? false : debtoreditUpdateFormModel.isExcludeSmallBalanceAdjustment : false;
        this.isEmailCommunication = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.isEmailCommunication == undefined ? false : debtoreditUpdateFormModel.isEmailCommunication : false;
        this.isSMSCommunication = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.isSMSCommunication == undefined ? false : debtoreditUpdateFormModel.isSMSCommunication : false;
        this.isPhoneCommunication = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.isPhoneCommunication == undefined ? false : debtoreditUpdateFormModel.isPhoneCommunication : false;
        this.isPostCommunication = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.isPostCommunication == undefined ? false : debtoreditUpdateFormModel.isPostCommunication : false;
        this.isGroupInvoice = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.isGroupInvoice == undefined ? false : debtoreditUpdateFormModel.isGroupInvoice : false;

        this.modifiedUserId = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.modifiedUserId == undefined ? "" : debtoreditUpdateFormModel.modifiedUserId : "";
        this.currentBalance = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.currentBalance == undefined ? "" : debtoreditUpdateFormModel.currentBalance : "";
        this.d30 = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.d30 == undefined ? "" : debtoreditUpdateFormModel.d30 : "";
        this.d60 = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.d60 == undefined ? "" : debtoreditUpdateFormModel.d60 : "";
        this.d90 = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.d90 == undefined ? "" : debtoreditUpdateFormModel.d90 : "";
        this.d120 = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.d120 == undefined ? "" : debtoreditUpdateFormModel.d120 : "";
        this.totalBalance = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.totalBalance == undefined ? "" : debtoreditUpdateFormModel.totalBalance : "";

        this.debtorRefNo = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.debtorRefNo == undefined ? "" : debtoreditUpdateFormModel.debtorRefNo : "";
        this.creditControllerCode = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.creditControllerCode == undefined ? "" : debtoreditUpdateFormModel.creditControllerCode : "";
        this.creditControllerName = debtoreditUpdateFormModel ? debtoreditUpdateFormModel.creditControllerName == undefined ? "" : debtoreditUpdateFormModel.creditControllerName : "";

    }
    debtorId: string;
    debtorTypeId:string;
    titleId:string;
    firstName:string;
    lastName:string;
    said:string;
    companyName:string;
    companyRegNo:string;
    companyVATNo:string;
    contactId:string;
    mobile1CountryCode:string;
    mobile1:string;
    mobile2:string;
    mobile2CountryCode:string;
    email:string;
    alternateEmail:string;
    billingAddressId:string;
    addressLine1:string;
    addressLine2:string;
    addressLine3:string;
    addressLine4:string;
    postalCode:string;
    debtorAdditionalInfoId:string;

    isBillingSameAsSiteAddress:boolean;
    isExcludeSmallBalanceAdjustment: boolean;
    isEmailCommunication:boolean;
    isSMSCommunication:boolean;
    isPhoneCommunication:boolean;
    isPostCommunication:boolean;
    isGroupInvoice: boolean;

    modifiedUserId:string;
    currentBalance:string;
    d30:string;
    d60:string;
    d90:string;
    d120:string;
    totalBalance:string;
    debtorRefNo:string;
    creditControllerName:string;
    creditControllerCode:string;

}

export class PaymentUnderrideFormModel{
    constructor(paymentUnderrideFormModel?: PaymentUnderrideFormModel) {
        this.customerId = paymentUnderrideFormModel ? paymentUnderrideFormModel.customerId == undefined ? "" : paymentUnderrideFormModel.customerId : "";
        this.invoiceId = paymentUnderrideFormModel ? paymentUnderrideFormModel.invoiceId == undefined ? "" : paymentUnderrideFormModel.invoiceId : "";
        this.amount = paymentUnderrideFormModel ? paymentUnderrideFormModel.amount == undefined ? "" : paymentUnderrideFormModel.amount : "";
        this.motivation = paymentUnderrideFormModel ? paymentUnderrideFormModel.motivation == undefined ? "" : paymentUnderrideFormModel.motivation : "";
        this.createdUserId = paymentUnderrideFormModel ? paymentUnderrideFormModel.createdUserId == undefined ? "" : paymentUnderrideFormModel.createdUserId : "";

    }
    customerId:string;
    invoiceId:string;
    amount:string;
    motivation:string;
    createdUserId:string;
}

export class PaymentInvoiceFormModel{
    constructor(paymentInvoiceFormModel?: PaymentInvoiceFormModel) {
        this.customerId = paymentInvoiceFormModel ? paymentInvoiceFormModel.customerId == undefined ? "" : paymentInvoiceFormModel.customerId : "";
        this.invoicePaymentId = paymentInvoiceFormModel ? paymentInvoiceFormModel.invoicePaymentId == undefined ? "" : paymentInvoiceFormModel.invoicePaymentId : "";
        this.debtorAccountDetailId = paymentInvoiceFormModel ? paymentInvoiceFormModel.debtorAccountDetailId == undefined ? "" : paymentInvoiceFormModel.debtorAccountDetailId : "";
        this.alternateEmail = paymentInvoiceFormModel ? paymentInvoiceFormModel.alternateEmail == undefined ? "" : paymentInvoiceFormModel.alternateEmail : "";
        this.createdUserId = paymentInvoiceFormModel ? paymentInvoiceFormModel.createdUserId == undefined ? "" : paymentInvoiceFormModel.createdUserId : "";
        this.isAlternateEmail = paymentInvoiceFormModel ? paymentInvoiceFormModel.isAlternateEmail == undefined ? false : paymentInvoiceFormModel.isAlternateEmail : false;
        this.referenceId = paymentInvoiceFormModel ? paymentInvoiceFormModel.referenceId == undefined ? "" : paymentInvoiceFormModel.referenceId : "";
        this.referenceType = paymentInvoiceFormModel ? paymentInvoiceFormModel.referenceType == undefined ? "" : paymentInvoiceFormModel.referenceType : "";
        this.templateId = paymentInvoiceFormModel ? paymentInvoiceFormModel.templateId == undefined ? "" : paymentInvoiceFormModel.templateId : "";

        this.customerEmail = paymentInvoiceFormModel ? paymentInvoiceFormModel.customerEmail == undefined ? "" : paymentInvoiceFormModel.customerEmail : "";
        this.otherEmail = paymentInvoiceFormModel ? paymentInvoiceFormModel.otherEmail == undefined ? "" : paymentInvoiceFormModel.otherEmail : "";
        this.debtorEmail = paymentInvoiceFormModel ? paymentInvoiceFormModel.debtorEmail == undefined ? "" : paymentInvoiceFormModel.debtorEmail : "";
        this.isCustomer = paymentInvoiceFormModel ? paymentInvoiceFormModel.isCustomer == undefined ? false : paymentInvoiceFormModel.isCustomer : false;
        this.isDebtor = paymentInvoiceFormModel ? paymentInvoiceFormModel.isDebtor == undefined ? false : paymentInvoiceFormModel.isDebtor : false;
        this.isOther = paymentInvoiceFormModel ? paymentInvoiceFormModel.isOther == undefined ? false : paymentInvoiceFormModel.isOther : false;

    }
    customerId:string;
    invoicePaymentId:string;
    debtorAccountDetailId:string;
    alternateEmail:string;
    createdUserId:string;
    isAlternateEmail:boolean;
    referenceId:string;
    referenceType:string;
    isCustomer:boolean;
    isOther:boolean;
    isDebtor:boolean;
    customerEmail:string;
    otherEmail:string;
    debtorEmail:string;
    templateId:string;
}

export class PaymentOverrideFormModel{
    constructor(PaymentOverrideFormModel?: PaymentOverrideFormModel) {
        this.isPaymentTypeSingle = PaymentOverrideFormModel ? PaymentOverrideFormModel.isPaymentTypeSingle == undefined ? true : PaymentOverrideFormModel.isPaymentTypeSingle : true;
        this.debitOrderRunCodeId = PaymentOverrideFormModel ? PaymentOverrideFormModel.debitOrderRunCodeId == undefined ? "" : PaymentOverrideFormModel.debitOrderRunCodeId : "";
        this.smsNotificationNoCountryCode = PaymentOverrideFormModel ? PaymentOverrideFormModel.smsNotificationNoCountryCode == undefined ? "+27" : PaymentOverrideFormModel.smsNotificationNoCountryCode : "+27";
        this.smsNotificationNo = PaymentOverrideFormModel ? PaymentOverrideFormModel.smsNotificationNo == undefined ? "" : PaymentOverrideFormModel.smsNotificationNo : "";
        this.transactionDescription = PaymentOverrideFormModel ? PaymentOverrideFormModel.transactionDescription == undefined ? "" : PaymentOverrideFormModel.transactionDescription : "";
        this.notes = PaymentOverrideFormModel ? PaymentOverrideFormModel.notes == undefined ? "" : PaymentOverrideFormModel.notes : "";
        this.noOfPayments = PaymentOverrideFormModel ? PaymentOverrideFormModel.noOfPayments == undefined ? "" : PaymentOverrideFormModel.noOfPayments : "";
        this.createdUserId = PaymentOverrideFormModel ? PaymentOverrideFormModel.createdUserId == undefined ? "" : PaymentOverrideFormModel.createdUserId : "";
        this.isSMSNotification = PaymentOverrideFormModel ? PaymentOverrideFormModel.isSMSNotification == undefined ? false : PaymentOverrideFormModel.isSMSNotification : false;
        this.otpId = PaymentOverrideFormModel ? PaymentOverrideFormModel.otpId == undefined ? "" : PaymentOverrideFormModel.otpId : "";

    }
    isPaymentTypeSingle:boolean;
    otpId:string;
    debitOrderRunCodeId:string;
    smsNotificationNoCountryCode:string;
    smsNotificationNo:string;
    transactionDescription:string;
    notes:string;
    noOfPayments:string;
    createdUserId:string;
    transactions:Transactions[];
    isSMSNotification:boolean;
}



export class Transactions{
    constructor(item?:Transactions){
        this.invoicePaymentAndOverrideId=item?item.invoicePaymentAndOverrideId==undefined?'':item.invoicePaymentAndOverrideId:'';
        this.debitDate=item?item.debitDate==undefined?null:item.debitDate:null;
        this.amount=item?item.amount==undefined?0:item.amount:0;
        this.invoiceRefNo=item?item.invoiceRefNo==undefined?'':item.invoiceRefNo:'';
        this.description=item?item.description==undefined?'':item.description:'';
    }
    invoicePaymentAndOverrideId:string;
    debitDate:Date;
    amount:Number;
    invoiceRefNo:string;
    description:string;
}
