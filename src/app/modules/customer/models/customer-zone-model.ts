class CustomerZoneModel {
    constructor(customerZoneModel?: CustomerZoneModel) {
        this.zoneEquipmentMappingId=customerZoneModel?customerZoneModel.zoneEquipmentMappingId==undefined? null:customerZoneModel.zoneEquipmentMappingId:null;
        this.createdUserId=customerZoneModel?customerZoneModel.createdUserId==undefined?'':customerZoneModel.createdUserId:'';
        this.createdDate=customerZoneModel?customerZoneModel.createdDate==undefined?'':customerZoneModel.createdDate:'';
        this.customerId=customerZoneModel?customerZoneModel.customerId==undefined?'':customerZoneModel.customerId:'';
        this.addressId=customerZoneModel?customerZoneModel.addressId==undefined?'':customerZoneModel.addressId:'';
        this.zoneId=customerZoneModel?customerZoneModel.zoneId==undefined?'':customerZoneModel.zoneId:'';
        this.itemId=customerZoneModel?customerZoneModel.itemId==undefined?'':customerZoneModel.itemId:'';
        this.stockDescription=customerZoneModel?customerZoneModel.stockDescription==undefined?'':customerZoneModel.stockDescription:'';
        this.serialNo=customerZoneModel?customerZoneModel.serialNo==undefined?'':customerZoneModel.serialNo:'';
        this.installDate=customerZoneModel?customerZoneModel.installDate==undefined?'':customerZoneModel.installDate:'';
        this.itemOwnershipTypeId=customerZoneModel?customerZoneModel.itemOwnershipTypeId==undefined?'':customerZoneModel.itemOwnershipTypeId:'';
        this.partitionId=customerZoneModel?customerZoneModel.partitionId==undefined?'':customerZoneModel.partitionId:'';
        this.zoneName=customerZoneModel?customerZoneModel.zoneName==undefined?'':customerZoneModel.zoneName:'';
        this.isActive=customerZoneModel?customerZoneModel.isActive==undefined?true:customerZoneModel.isActive:true;
        this.callInitiationId=customerZoneModel?customerZoneModel.callInitiationId==undefined?'':customerZoneModel.callInitiationId:'';
    }
   zoneEquipmentMappingId: any;
   createdUserId:string;
   createdDate:string;
   customerId:string;
   addressId:string;
   zoneId: string;
   itemId: string;
   serialNo: string;
   stockDescription: string;
   installDate:string;
   itemOwnershipTypeId:string;
   partitionId:string;
   zoneName:string;
   isActive:boolean;
   callInitiationId:string;
}

export { CustomerZoneModel }