class CustomerClassificationConfigModel{
    constructor(customerClassificationConfigModel?:CustomerClassificationConfigModel){
        this.customerClassificationMappingId=customerClassificationConfigModel?customerClassificationConfigModel.customerClassificationMappingId==undefined?'':customerClassificationConfigModel.customerClassificationMappingId:'';
        this.modifiedUserId=customerClassificationConfigModel?customerClassificationConfigModel.modifiedUserId==undefined?'':customerClassificationConfigModel.modifiedUserId:'';
        this.categoryId=customerClassificationConfigModel?customerClassificationConfigModel.categoryId==undefined? null :customerClassificationConfigModel.categoryId: null;
        this.originId=customerClassificationConfigModel?customerClassificationConfigModel.originId==undefined?null:customerClassificationConfigModel.originId:null;
        this.installOriginId=customerClassificationConfigModel?customerClassificationConfigModel.installOriginId==undefined?null:customerClassificationConfigModel.installOriginId:null;
        this.debtorGroupId=customerClassificationConfigModel?customerClassificationConfigModel.debtorGroupId==undefined?null:customerClassificationConfigModel.debtorGroupId:null;
        this.itemOwnershipTypeId=customerClassificationConfigModel?customerClassificationConfigModel.itemOwnershipTypeId==undefined?'':customerClassificationConfigModel.itemOwnershipTypeId:'';
        this.isManualCategory=customerClassificationConfigModel?customerClassificationConfigModel.isManualCategory==undefined?false:customerClassificationConfigModel.isManualCategory:false;
        this.isManualOrigin=customerClassificationConfigModel?customerClassificationConfigModel.isManualOrigin==undefined?false:customerClassificationConfigModel.isManualOrigin:false;
        this.isManualInstallOrigin=customerClassificationConfigModel?customerClassificationConfigModel.isManualInstallOrigin==undefined?false:customerClassificationConfigModel.isManualInstallOrigin:false;
        this.isManualDebtorGroup=customerClassificationConfigModel?customerClassificationConfigModel.isManualDebtorGroup==undefined?false:customerClassificationConfigModel.isManualDebtorGroup:false;
        this.isManualItemOwnerShipType=customerClassificationConfigModel?customerClassificationConfigModel.isManualItemOwnerShipType==undefined?false:customerClassificationConfigModel.isManualItemOwnerShipType:false;

    }
   
    customerClassificationMappingId: string
    modifiedUserId: string;
    categoryId: number;
    originId: number;
    installOriginId: number;
    debtorGroupId: number;
    itemOwnershipTypeId: string;
    isManualCategory: false;
    isManualOrigin: false;
    isManualInstallOrigin: false;
    isManualDebtorGroup: false;
    isManualItemOwnerShipType: false;

}

class CustomerSubClassificationConfigModel{
    constructor(customerSubClassificationConfigModel?:CustomerSubClassificationConfigModel){
        this.customerClassificationSubCategoryMappingId=customerSubClassificationConfigModel?customerSubClassificationConfigModel.customerClassificationSubCategoryMappingId==undefined?'':customerSubClassificationConfigModel.customerClassificationSubCategoryMappingId:'';
        this.isManual=customerSubClassificationConfigModel?customerSubClassificationConfigModel.isManual==undefined?false:customerSubClassificationConfigModel.isManual:false;
        this.isAvailable=customerSubClassificationConfigModel?customerSubClassificationConfigModel.isAvailable==undefined?false:customerSubClassificationConfigModel.isAvailable:false;
        this.modifiedUserId=customerSubClassificationConfigModel?customerSubClassificationConfigModel.modifiedUserId==undefined?'':customerSubClassificationConfigModel.modifiedUserId:'';
        this.subCategoryName=customerSubClassificationConfigModel?customerSubClassificationConfigModel.subCategoryName==undefined?'':customerSubClassificationConfigModel.subCategoryName:'';

    }
   
    customerClassificationSubCategoryMappingId: string
    isManual: boolean;
    isAvailable: boolean;
    modifiedUserId: string;
    subCategoryName: string;

}

export{CustomerClassificationConfigModel, CustomerSubClassificationConfigModel}