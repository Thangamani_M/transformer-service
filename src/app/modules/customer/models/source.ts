export class Source {
    sourceId: string;
    sourceName: string;
    description: string;
    cssClass: string;
    createdDate: Date;
  }
  