class CustomerSaveOfferDetailsModel {
    doaApprovalLevel?: string | number;
    contractRefNo?: string;
    customerId?: string;
    addressId?: string;
    contractId?: string;
    isComplaint?: boolean;
    isCancellation?: boolean;
    complaintNotes?: string;
    ticketCancellationReasonId?: string;
    ticketCancellationSubReasonId?: string;
    isServiceCallRequired?: boolean;
    isFreeMonths?: boolean;
    numberOfFreeMonthId?: string | number;
    numberOfFreeMonthStartDate?: string;
    numberOfFreeMonthEndDate?: string;
    saveOfferDiscountSummary?: SaveOfferDiscountSummaryDetailsModel;
    saveOfferDiscountServices?: SaveOfferDiscountServicesDetailsModel[];
    isDiscountServiceMonthlyFee?: boolean;
    isNoCostSaveOffer?: boolean;
    noCostSaveOfferReasonConfigId?: string | number;
    departmentId?: string;
    noCostSaveOfferNotes?: string;
    isFreeAccess?: boolean;
    freeAccessTypeId?: string | number;
    createdUserId?: string;
    serviceCallTicketTypeId?: string;
    creditControlTicketTypeId?: string;
    serviceCallTicketId?: string;
    serviceCallTicketRefNo?: string;
    creditControlTicketId?: string;
    creditControlTicketRefNo?: string;
    freeAccessTicketId?: string;
    freeAccessTicketRefNo?: string;
    constructor(customerSaveOfferDetailsModel: CustomerSaveOfferDetailsModel) {
        this.doaApprovalLevel = customerSaveOfferDetailsModel?.doaApprovalLevel == undefined ? '' : customerSaveOfferDetailsModel?.doaApprovalLevel;
        this.contractRefNo = customerSaveOfferDetailsModel?.contractRefNo == undefined ? '' : customerSaveOfferDetailsModel?.contractRefNo;
        this.customerId = customerSaveOfferDetailsModel?.customerId == undefined ? '' : customerSaveOfferDetailsModel?.customerId;
        this.addressId = customerSaveOfferDetailsModel?.addressId == undefined ? '' : customerSaveOfferDetailsModel?.addressId;
        this.contractId = customerSaveOfferDetailsModel?.contractId == undefined ? '' : customerSaveOfferDetailsModel?.contractId;
        this.isComplaint = customerSaveOfferDetailsModel?.isComplaint ? customerSaveOfferDetailsModel?.isComplaint : undefined;
        this.isCancellation = customerSaveOfferDetailsModel?.isCancellation ? customerSaveOfferDetailsModel?.isCancellation : undefined;
        this.complaintNotes = customerSaveOfferDetailsModel?.complaintNotes == undefined ? '' : customerSaveOfferDetailsModel?.complaintNotes;
        this.ticketCancellationReasonId = customerSaveOfferDetailsModel?.ticketCancellationReasonId == undefined ? '' : customerSaveOfferDetailsModel?.ticketCancellationReasonId;
        this.ticketCancellationSubReasonId = customerSaveOfferDetailsModel?.ticketCancellationSubReasonId == undefined ? '' : customerSaveOfferDetailsModel?.ticketCancellationSubReasonId;
        this.isServiceCallRequired = customerSaveOfferDetailsModel?.isServiceCallRequired == undefined ? false : customerSaveOfferDetailsModel?.isServiceCallRequired;
        this.isFreeMonths = customerSaveOfferDetailsModel?.isFreeMonths == undefined ? false : customerSaveOfferDetailsModel?.isFreeMonths;
        this.numberOfFreeMonthId = customerSaveOfferDetailsModel?.numberOfFreeMonthId == undefined ? '' : customerSaveOfferDetailsModel?.numberOfFreeMonthId;
        this.numberOfFreeMonthStartDate = customerSaveOfferDetailsModel?.numberOfFreeMonthStartDate == undefined ? '' : customerSaveOfferDetailsModel?.numberOfFreeMonthStartDate;
        this.numberOfFreeMonthEndDate = customerSaveOfferDetailsModel?.numberOfFreeMonthEndDate == undefined ? '' : customerSaveOfferDetailsModel?.numberOfFreeMonthEndDate;
        this.saveOfferDiscountSummary = customerSaveOfferDetailsModel?.saveOfferDiscountSummary == undefined ? {} : customerSaveOfferDetailsModel?.saveOfferDiscountSummary;
        this.saveOfferDiscountServices = customerSaveOfferDetailsModel?.saveOfferDiscountServices == undefined ? [] : customerSaveOfferDetailsModel?.saveOfferDiscountServices;
        this.isDiscountServiceMonthlyFee = customerSaveOfferDetailsModel?.isDiscountServiceMonthlyFee == undefined ? false : customerSaveOfferDetailsModel?.isDiscountServiceMonthlyFee;
        this.isNoCostSaveOffer = customerSaveOfferDetailsModel?.isNoCostSaveOffer == undefined ? false : customerSaveOfferDetailsModel?.isNoCostSaveOffer;
        this.noCostSaveOfferReasonConfigId = customerSaveOfferDetailsModel?.noCostSaveOfferReasonConfigId == undefined ? '' : customerSaveOfferDetailsModel?.noCostSaveOfferReasonConfigId;
        this.departmentId = customerSaveOfferDetailsModel?.departmentId == undefined ? '' : customerSaveOfferDetailsModel?.departmentId;
        this.noCostSaveOfferNotes = customerSaveOfferDetailsModel?.noCostSaveOfferNotes == undefined ? '' : customerSaveOfferDetailsModel?.noCostSaveOfferNotes;
        this.isFreeAccess = customerSaveOfferDetailsModel?.isFreeAccess == undefined ? false : customerSaveOfferDetailsModel?.isFreeAccess;
        this.freeAccessTypeId = customerSaveOfferDetailsModel?.freeAccessTypeId == undefined ? '' : customerSaveOfferDetailsModel?.freeAccessTypeId;
        this.createdUserId = customerSaveOfferDetailsModel?.createdUserId == undefined ? '' : customerSaveOfferDetailsModel?.createdUserId;
        this.serviceCallTicketTypeId = customerSaveOfferDetailsModel?.serviceCallTicketTypeId == undefined ? '' : customerSaveOfferDetailsModel?.serviceCallTicketTypeId;
        this.creditControlTicketTypeId = customerSaveOfferDetailsModel?.creditControlTicketTypeId == undefined ? '' : customerSaveOfferDetailsModel?.creditControlTicketTypeId;
        this.serviceCallTicketId = customerSaveOfferDetailsModel?.serviceCallTicketId == undefined ? '' : customerSaveOfferDetailsModel?.serviceCallTicketId;
        this.serviceCallTicketRefNo = customerSaveOfferDetailsModel?.serviceCallTicketRefNo == undefined ? '' : customerSaveOfferDetailsModel?.serviceCallTicketRefNo;
        this.creditControlTicketId = customerSaveOfferDetailsModel?.creditControlTicketId == undefined ? '' : customerSaveOfferDetailsModel?.creditControlTicketId;
        this.creditControlTicketRefNo = customerSaveOfferDetailsModel?.creditControlTicketRefNo == undefined ? '' : customerSaveOfferDetailsModel?.creditControlTicketRefNo;
        this.freeAccessTicketId = customerSaveOfferDetailsModel?.freeAccessTicketId == undefined ? '' : customerSaveOfferDetailsModel?.freeAccessTicketId;
        this.freeAccessTicketRefNo = customerSaveOfferDetailsModel?.freeAccessTicketRefNo == undefined ? '' : customerSaveOfferDetailsModel?.freeAccessTicketRefNo;
    }
}

class SaveOfferDiscountSummaryDetailsModel {
    currentMonthlyInst?: string | number;
    percentageOfDeduction?: string | number;
    deductionAmount?: string | number;
    futureMonthlyInst?: string | number;
    freeServices?: string;
    constructor(saveOfferDiscountSummaryDetailsModel: SaveOfferDiscountSummaryDetailsModel) {
        this.currentMonthlyInst = saveOfferDiscountSummaryDetailsModel?.currentMonthlyInst == undefined ? '' : saveOfferDiscountSummaryDetailsModel?.currentMonthlyInst;
        this.percentageOfDeduction = saveOfferDiscountSummaryDetailsModel?.percentageOfDeduction == undefined ? '' : saveOfferDiscountSummaryDetailsModel?.percentageOfDeduction;
        this.deductionAmount = saveOfferDiscountSummaryDetailsModel?.deductionAmount == undefined ? '' : saveOfferDiscountSummaryDetailsModel?.deductionAmount;
        this.futureMonthlyInst = saveOfferDiscountSummaryDetailsModel?.futureMonthlyInst == undefined ? '' : saveOfferDiscountSummaryDetailsModel?.futureMonthlyInst;
        this.freeServices = saveOfferDiscountSummaryDetailsModel?.freeServices == undefined ? '' : saveOfferDiscountSummaryDetailsModel?.freeServices;
    }
}

class SaveOfferDiscountServicesDetailsModel {
    subscriptionServiceId?: string;
    serviceName?: string;
    isDiscountInRand?: boolean;
    isDiscountInPercentage?: boolean;
    valueInRand?: string | any;
    valueInPercentage?: string | any;
    subtotal?: string | any;
    discountValue?: string | any;
    adjustedTotal?: string | any;
    vat?: string | any;
    totalValue?: string | any;
    constructor(saveOfferDiscountServicesDetailsModel: SaveOfferDiscountServicesDetailsModel) {
        this.subscriptionServiceId = saveOfferDiscountServicesDetailsModel?.subscriptionServiceId == undefined ? '' : saveOfferDiscountServicesDetailsModel?.subscriptionServiceId;
        this.serviceName = saveOfferDiscountServicesDetailsModel?.serviceName == undefined ? '' : saveOfferDiscountServicesDetailsModel?.serviceName;
        this.isDiscountInRand = saveOfferDiscountServicesDetailsModel?.isDiscountInRand == undefined ? undefined : saveOfferDiscountServicesDetailsModel?.isDiscountInRand;
        this.isDiscountInPercentage = saveOfferDiscountServicesDetailsModel?.isDiscountInPercentage == undefined ? undefined : saveOfferDiscountServicesDetailsModel?.isDiscountInPercentage;
        this.valueInRand = saveOfferDiscountServicesDetailsModel?.valueInRand == undefined ? '' : saveOfferDiscountServicesDetailsModel?.valueInRand;
        this.valueInPercentage = saveOfferDiscountServicesDetailsModel?.valueInPercentage == undefined ? '' : saveOfferDiscountServicesDetailsModel?.valueInPercentage;
        this.subtotal = saveOfferDiscountServicesDetailsModel?.subtotal == undefined ? '' : saveOfferDiscountServicesDetailsModel?.subtotal;
        this.discountValue = saveOfferDiscountServicesDetailsModel?.discountValue == undefined ? '' : saveOfferDiscountServicesDetailsModel?.discountValue;
        this.adjustedTotal = saveOfferDiscountServicesDetailsModel?.adjustedTotal == undefined ? '' : saveOfferDiscountServicesDetailsModel?.adjustedTotal;
        this.vat = saveOfferDiscountServicesDetailsModel?.vat == undefined ? '' : saveOfferDiscountServicesDetailsModel?.vat;
        this.totalValue = saveOfferDiscountServicesDetailsModel?.totalValue == undefined ? '' : saveOfferDiscountServicesDetailsModel?.totalValue;
    }
}

export { CustomerSaveOfferDetailsModel, SaveOfferDiscountServicesDetailsModel, SaveOfferDiscountSummaryDetailsModel }