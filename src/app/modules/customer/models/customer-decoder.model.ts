class partitionListModel {
    constructor(partitionModel?: partitionListModel) {
        this.partitionName = partitionModel ? partitionModel.partitionName == undefined ? "" : partitionModel.partitionName : "";
        this.partitionNo = partitionModel ? partitionModel.partitionNo == undefined ? "" : partitionModel.partitionNo : "";
        this.partitionId = partitionModel ? partitionModel.partitionId == undefined ? "" : partitionModel.partitionId : "";
    }
    partitionName: string;
    partitionNo:string;
    partitionId:string;
}
export { partitionListModel }