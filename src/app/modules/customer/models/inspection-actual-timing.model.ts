
class InspectionActualTimingModel {
    constructor(ActualTimingModel?: InspectionActualTimingModel) {
        this.actualTimingList = ActualTimingModel ? ActualTimingModel?.actualTimingList == undefined ? [] : ActualTimingModel?.actualTimingList : [];
    }
    actualTimingList: InspectionActualTimingListModel[];
}

class InspectionActualTimingListModel {
    constructor(callCreationTemplateModel: InspectionActualTimingListModel) {
        this.customerInsAppointmentActualTimingId = callCreationTemplateModel ? callCreationTemplateModel.customerInsAppointmentActualTimingId == undefined ? '' : callCreationTemplateModel.customerInsAppointmentActualTimingId : '';
        this.customerInspectionAppointmentId = callCreationTemplateModel ? callCreationTemplateModel.customerInspectionAppointmentId == undefined ? '' : callCreationTemplateModel.customerInspectionAppointmentId : '';
        this.appointmentDate = callCreationTemplateModel ? callCreationTemplateModel.appointmentDate == undefined ? '' : callCreationTemplateModel.appointmentDate : '';
        this.onRouteDate = callCreationTemplateModel ? callCreationTemplateModel.onRouteDate == undefined ? '' : callCreationTemplateModel.onRouteDate : '';
        this.coordinates = callCreationTemplateModel ? callCreationTemplateModel.coordinates == undefined ? '' : callCreationTemplateModel.coordinates : '';
        this.onRouteTime = callCreationTemplateModel ? callCreationTemplateModel.onRouteTime == undefined ? '' : callCreationTemplateModel.onRouteTime : '';
        this.startTime = callCreationTemplateModel ? callCreationTemplateModel.startTime == undefined ? '' : callCreationTemplateModel.startTime : '';
        this.endTime = callCreationTemplateModel ? callCreationTemplateModel.endTime == undefined ? '' : callCreationTemplateModel.endTime : '';
        this.actualTime = callCreationTemplateModel ? callCreationTemplateModel.actualTime == undefined ? '' : callCreationTemplateModel.actualTime : '';
        this.createdUserId = callCreationTemplateModel ? callCreationTemplateModel.createdUserId == undefined ? '' : callCreationTemplateModel.createdUserId : '';
        this.modifiedUserId = callCreationTemplateModel ? callCreationTemplateModel.modifiedUserId == undefined ? '' : callCreationTemplateModel.modifiedUserId : '';
        
        this.startTimeMinDate = callCreationTemplateModel ? callCreationTemplateModel.startTimeMinDate == undefined ? '' : callCreationTemplateModel.startTimeMinDate : '';
        this.endTimeMinDate = callCreationTemplateModel ? callCreationTemplateModel.endTimeMinDate == undefined ? '' : callCreationTemplateModel.endTimeMinDate : '';
    }
    customerInsAppointmentActualTimingId: string;
    customerInspectionAppointmentId: string;
    appointmentDate: string;
    onRouteDate: string;
    coordinates: string;
    onRouteTime: any
    startTime: any;
    endTime: any;
    actualTime: any;
    createdUserId: string;
    modifiedUserId: string;
    startTimeMinDate: any;
    endTimeMinDate: any;
}

export { InspectionActualTimingModel, InspectionActualTimingListModel };

