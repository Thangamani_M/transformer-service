class ERBExclusionRequestModel {
    constructor(eRBExclusionRequestModel?: ERBExclusionRequestModel) {
        this.erbExclusionRequestId=eRBExclusionRequestModel?eRBExclusionRequestModel.erbExclusionRequestId==undefined? null:eRBExclusionRequestModel.erbExclusionRequestId:null;
        this.createdUserId=eRBExclusionRequestModel?eRBExclusionRequestModel.createdUserId==undefined?'':eRBExclusionRequestModel.createdUserId:'';
        this.customerId=eRBExclusionRequestModel?eRBExclusionRequestModel.customerId==undefined?'':eRBExclusionRequestModel.customerId:'';
        this.customerAddressId=eRBExclusionRequestModel?eRBExclusionRequestModel.customerAddressId==undefined?'':eRBExclusionRequestModel.customerAddressId:'';
        this.erbExclusionReasonConfigId=eRBExclusionRequestModel?eRBExclusionRequestModel.erbExclusionReasonConfigId==undefined?'':eRBExclusionRequestModel.erbExclusionReasonConfigId:'';
        this.comments=eRBExclusionRequestModel?eRBExclusionRequestModel.comments==undefined?'':eRBExclusionRequestModel.comments:'';
        this.modifiedUserId=eRBExclusionRequestModel?eRBExclusionRequestModel.modifiedUserId==undefined?'':eRBExclusionRequestModel.modifiedUserId:'';
        this.erbExclusionTillDate=eRBExclusionRequestModel?eRBExclusionRequestModel.erbExclusionTillDate==undefined?'':eRBExclusionRequestModel.erbExclusionTillDate:'';
        this.isParmanent=eRBExclusionRequestModel?eRBExclusionRequestModel.isParmanent==undefined?false:eRBExclusionRequestModel.isParmanent:false;

        
    }
   erbExclusionRequestId: any;
   createdUserId:string;
   customerId:string;
   customerAddressId:string;
   erbExclusionReasonConfigId: string;
   comments: string;
   serialNo: string;
   modifiedUserId: string;
   erbExclusionTillDate: string;
   isParmanent: boolean;
}

export { ERBExclusionRequestModel }