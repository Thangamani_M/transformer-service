class TicketSuspendServiceDetailsModel {
    serviceSuspendId?: string;
    contractId?: string;
    contractRefNo?: string;
    suspendServiceDate?: string;
    reinstateServiceDate?: string;
    billStartDate?: string;
    contractLastBillDate?: string;
    serviceSuspendReasonId?: string;
    serviceSuspendSubReasonId?: string;
    isEquipmentRemovalRequired?: boolean;
    isSiteVisitRequired?: boolean;
    leadId?: string;
    leadRefNo?: string;
    notes?: string;
    services?: TicketSuspendServicesDetailsModel[];
    customerId?: string;
    siteAddressId?: string;
    createdUserId?: string;
    status?: string;
    ticketId?: string;
    ticketRefNo?: string;
    actionTypeID?: string;
    constructor(ticketSuspendServiceDetailsModel: TicketSuspendServiceDetailsModel) {
        this.serviceSuspendId = ticketSuspendServiceDetailsModel?.serviceSuspendId == undefined ? '' : ticketSuspendServiceDetailsModel?.serviceSuspendId;
        this.contractId = ticketSuspendServiceDetailsModel?.contractId == undefined ? '' : ticketSuspendServiceDetailsModel?.contractId;
        this.contractRefNo = ticketSuspendServiceDetailsModel?.contractRefNo == undefined ? '' : ticketSuspendServiceDetailsModel?.contractRefNo;
        this.suspendServiceDate = ticketSuspendServiceDetailsModel?.suspendServiceDate == undefined ? '' : ticketSuspendServiceDetailsModel?.suspendServiceDate;
        this.reinstateServiceDate = ticketSuspendServiceDetailsModel?.reinstateServiceDate == undefined ? '' : ticketSuspendServiceDetailsModel?.reinstateServiceDate;
        this.billStartDate = ticketSuspendServiceDetailsModel?.billStartDate == undefined ? '' : ticketSuspendServiceDetailsModel?.billStartDate;
        this.contractLastBillDate = ticketSuspendServiceDetailsModel?.contractLastBillDate == undefined ? '' : ticketSuspendServiceDetailsModel?.contractLastBillDate;
        this.serviceSuspendReasonId = ticketSuspendServiceDetailsModel?.serviceSuspendReasonId == undefined ? '' : ticketSuspendServiceDetailsModel?.serviceSuspendReasonId;
        this.serviceSuspendSubReasonId = ticketSuspendServiceDetailsModel?.serviceSuspendSubReasonId == undefined ? '' : ticketSuspendServiceDetailsModel?.serviceSuspendSubReasonId;
        this.isEquipmentRemovalRequired = ticketSuspendServiceDetailsModel?.isEquipmentRemovalRequired == undefined ? undefined : ticketSuspendServiceDetailsModel?.isEquipmentRemovalRequired;
        this.isSiteVisitRequired = ticketSuspendServiceDetailsModel?.isSiteVisitRequired == undefined ? undefined : ticketSuspendServiceDetailsModel?.isSiteVisitRequired;
        this.leadId = ticketSuspendServiceDetailsModel?.leadId == undefined ? '' : ticketSuspendServiceDetailsModel?.leadId;
        this.leadRefNo = ticketSuspendServiceDetailsModel?.leadRefNo == undefined ? '' : ticketSuspendServiceDetailsModel?.leadRefNo;
        this.notes = ticketSuspendServiceDetailsModel?.notes == undefined ? '' : ticketSuspendServiceDetailsModel?.notes;
        this.services = ticketSuspendServiceDetailsModel?.services == undefined ? [] : ticketSuspendServiceDetailsModel?.services;
        this.customerId = ticketSuspendServiceDetailsModel?.customerId == undefined ? '' : ticketSuspendServiceDetailsModel?.customerId;
        this.siteAddressId = ticketSuspendServiceDetailsModel?.siteAddressId == undefined ? '' : ticketSuspendServiceDetailsModel?.siteAddressId;
        this.createdUserId = ticketSuspendServiceDetailsModel?.createdUserId == undefined ? '' : ticketSuspendServiceDetailsModel?.createdUserId;
        this.status = ticketSuspendServiceDetailsModel?.status == undefined ? '' : ticketSuspendServiceDetailsModel?.status;
        this.ticketId = ticketSuspendServiceDetailsModel?.ticketId == undefined ? '' : ticketSuspendServiceDetailsModel?.ticketId;
        this.ticketRefNo = ticketSuspendServiceDetailsModel?.ticketRefNo == undefined ? '' : ticketSuspendServiceDetailsModel?.ticketRefNo;
        this.actionTypeID = ticketSuspendServiceDetailsModel?.actionTypeID == undefined ? '' : ticketSuspendServiceDetailsModel?.actionTypeID;
    }
}

class TicketSuspendServicesDetailsModel {
    serviceSuspendDetailId?: string;
    serviceId?: string;
    constructor(ticketSuspendServicesDetailsModel: TicketSuspendServicesDetailsModel) {
        this.serviceSuspendDetailId = ticketSuspendServicesDetailsModel?.serviceSuspendDetailId == undefined ? '' : ticketSuspendServicesDetailsModel?.serviceSuspendDetailId;
        this.serviceId = ticketSuspendServicesDetailsModel?.serviceId == undefined ? '' : ticketSuspendServicesDetailsModel?.serviceId;
    }
}

export {TicketSuspendServiceDetailsModel, TicketSuspendServicesDetailsModel}