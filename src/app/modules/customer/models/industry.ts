export class Industry {
    industryId: string;
    industryName: string;
    industryCategoryId:string;
    description: string;
    cssClass: string;
    createdDate: Date;
  }
  