class CustomerDispatchFlowModel {
    constructor(siteDispatchFlowModel?: CustomerDispatchFlowModel) {
        this.createdUserId=siteDispatchFlowModel?siteDispatchFlowModel.createdUserId==undefined? '':siteDispatchFlowModel.createdUserId:'';
        this.customerPreferenceCustomerDispatchFlowId=siteDispatchFlowModel?siteDispatchFlowModel.customerPreferenceCustomerDispatchFlowId==undefined?'':siteDispatchFlowModel.customerPreferenceCustomerDispatchFlowId:'';
        this.customerAddressId=siteDispatchFlowModel?siteDispatchFlowModel.customerAddressId==undefined?'':siteDispatchFlowModel.customerAddressId:'';
        this.customerId=siteDispatchFlowModel?siteDispatchFlowModel.customerId==undefined?'':siteDispatchFlowModel.customerId:'';

        this.isSignals=siteDispatchFlowModel?siteDispatchFlowModel.isSignals==undefined?false:siteDispatchFlowModel.isSignals:false;
        this.isDays=siteDispatchFlowModel?siteDispatchFlowModel.isDays==undefined?false:siteDispatchFlowModel.isDays:false;

        this.isTimeRange=siteDispatchFlowModel?siteDispatchFlowModel.isTimeRange==undefined?false:siteDispatchFlowModel.isTimeRange:false;
        this.description=siteDispatchFlowModel?siteDispatchFlowModel.description==undefined?'':siteDispatchFlowModel.description:'';
        this.timeFrom=siteDispatchFlowModel?siteDispatchFlowModel.timeFrom==undefined?null:siteDispatchFlowModel.timeFrom:null;
        this.timeTo=siteDispatchFlowModel?siteDispatchFlowModel.timeTo==undefined?null:siteDispatchFlowModel.timeTo:null;

        this.isActive=siteDispatchFlowModel?siteDispatchFlowModel.isActive==undefined?true:siteDispatchFlowModel.isActive:true;
        this.dayList=siteDispatchFlowModel?siteDispatchFlowModel.dayList==undefined?null:siteDispatchFlowModel.dayList:null;
        this.alarmTypeList=siteDispatchFlowModel?siteDispatchFlowModel.alarmTypeList==undefined?null:siteDispatchFlowModel.alarmTypeList:null;

    }
   createdUserId: any;
   customerPreferenceCustomerDispatchFlowId: string;
   customerAddressId: string;
   customerId: string;
   isSignals: boolean;
   isDays: boolean;
   isTimeRange: boolean;
   description:string;
   timeFrom?:string;
   timeTo?:string;
   isActive:boolean;
   dayList: CustomerPreferenceCustomerDispatchFlowDayListModel[];
   alarmTypeList: CustomerPreferenceCustomerDispatchFlowSignalListModel[];
}

class CustomerPreferenceCustomerDispatchFlowDayListModel {
  constructor(customerPreferenceCustomerDispatchFlowDayListModel?: CustomerPreferenceCustomerDispatchFlowDayListModel) {
    this.dayId=customerPreferenceCustomerDispatchFlowDayListModel?customerPreferenceCustomerDispatchFlowDayListModel.dayId==undefined? null:customerPreferenceCustomerDispatchFlowDayListModel.dayId:null;
}
 dayId:string;
}

class CustomerPreferenceCustomerDispatchFlowSignalListModel {
  constructor(customerPreferenceCustomerDispatchFlowSignalListModel?: CustomerPreferenceCustomerDispatchFlowSignalListModel) {
    this.alarmTypeId=customerPreferenceCustomerDispatchFlowSignalListModel?customerPreferenceCustomerDispatchFlowSignalListModel.alarmTypeId==undefined? null:customerPreferenceCustomerDispatchFlowSignalListModel.alarmTypeId:null;
}
 alarmTypeId:string;
}



export { CustomerDispatchFlowModel}
