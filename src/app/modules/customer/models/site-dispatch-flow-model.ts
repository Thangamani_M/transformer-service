class SiteDispatchFlowModel {
    constructor(siteDispatchFlowModel?: SiteDispatchFlowModel) {
        this.createdUserId=siteDispatchFlowModel?siteDispatchFlowModel.createdUserId==undefined? '':siteDispatchFlowModel.createdUserId:'';
        this.customerPreferenceSiteDispatchFlowId=siteDispatchFlowModel?siteDispatchFlowModel.customerPreferenceSiteDispatchFlowId==undefined?'':siteDispatchFlowModel.customerPreferenceSiteDispatchFlowId:'';
        this.customerAddressId=siteDispatchFlowModel?siteDispatchFlowModel.customerAddressId==undefined?'':siteDispatchFlowModel.customerId:'';
        this.customerId=siteDispatchFlowModel?siteDispatchFlowModel.customerId==undefined?'':siteDispatchFlowModel.customerAddressId:'';

        this.isSignals=siteDispatchFlowModel?siteDispatchFlowModel.isSignals==undefined?false:siteDispatchFlowModel.isSignals:false;
        this.isDays=siteDispatchFlowModel?siteDispatchFlowModel.isDays==undefined?false:siteDispatchFlowModel.isDays:false;

        this.isTimeRange=siteDispatchFlowModel?siteDispatchFlowModel.isTimeRange==undefined?false:siteDispatchFlowModel.isTimeRange:false;
        this.description=siteDispatchFlowModel?siteDispatchFlowModel.description==undefined?'':siteDispatchFlowModel.description:'';
        this.timeFrom=siteDispatchFlowModel?siteDispatchFlowModel.timeFrom==undefined?'':siteDispatchFlowModel.timeFrom:'';
        this.timeTo=siteDispatchFlowModel?siteDispatchFlowModel.timeTo==undefined?'':siteDispatchFlowModel.timeTo:'';
        this.partitionId=siteDispatchFlowModel?siteDispatchFlowModel.partitionId==undefined?'':siteDispatchFlowModel.partitionId:'';

        this.isActive=siteDispatchFlowModel?siteDispatchFlowModel.isActive==undefined?true:siteDispatchFlowModel.isActive:true;
        this.dayList=siteDispatchFlowModel?siteDispatchFlowModel.dayList==undefined?null:siteDispatchFlowModel.dayList:null;
        this.alarmTypeList=siteDispatchFlowModel?siteDispatchFlowModel.alarmTypeList==undefined?null:siteDispatchFlowModel.alarmTypeList:null;

    }
   createdUserId: any;
   customerPreferenceSiteDispatchFlowId: string;
   customerAddressId: string;
   customerId: string;
   isSignals: boolean;
   isDays: boolean;
   isTimeRange: boolean;
   description:string;
   timeFrom:string;
   timeTo:string;
   isActive:boolean;
   partitionId: string;
   dayList: CustomerPreferenceSiteDispatchFlowDaysListModel[];
   alarmTypeList: CustomerPreferenceSiteDispatchFlowSignalsListModel[];
}

class CustomerPreferenceSiteDispatchFlowDaysListModel {
  constructor(customerPreferenceSiteDispatchFlowDaysListModel?: CustomerPreferenceSiteDispatchFlowDaysListModel) {
    this.dayId=customerPreferenceSiteDispatchFlowDaysListModel?customerPreferenceSiteDispatchFlowDaysListModel.dayId==undefined? null:customerPreferenceSiteDispatchFlowDaysListModel.dayId:null;
}
 dayId:string;
}

class CustomerPreferenceSiteDispatchFlowSignalsListModel {
  constructor(customerPreferenceSiteDispatchFlowSignalsListModel?: CustomerPreferenceSiteDispatchFlowSignalsListModel) {
    this.alarmTypeId=customerPreferenceSiteDispatchFlowSignalsListModel?customerPreferenceSiteDispatchFlowSignalsListModel.alarmTypeId==undefined? null:customerPreferenceSiteDispatchFlowSignalsListModel.alarmTypeId:null;
}
 alarmTypeId:string;
}



export { SiteDispatchFlowModel}
