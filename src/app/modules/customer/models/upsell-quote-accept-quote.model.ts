
class UpsellQuotePaymentModel {
    constructor(upsellQuotePaymentModel?: UpsellQuotePaymentModel) {
        this.shouldDisable = upsellQuotePaymentModel == undefined ? true : upsellQuotePaymentModel.shouldDisable == undefined ? true : upsellQuotePaymentModel.shouldDisable;
        this.paymentMethodId = upsellQuotePaymentModel == undefined ? '' : upsellQuotePaymentModel.paymentMethodId == undefined ? '' : upsellQuotePaymentModel.paymentMethodId;
        this.paymentPercentageId = upsellQuotePaymentModel == undefined ? '' : upsellQuotePaymentModel.paymentPercentageId == undefined ? '' : upsellQuotePaymentModel.paymentPercentageId;
        this.paymentDate = upsellQuotePaymentModel == undefined ? null : upsellQuotePaymentModel.paymentDate == undefined ? null : upsellQuotePaymentModel.paymentDate;
        this.createdUserId = upsellQuotePaymentModel == undefined ? '' : upsellQuotePaymentModel.createdUserId == undefined ? '' : upsellQuotePaymentModel.createdUserId;
        this.amount = upsellQuotePaymentModel == undefined ? '' : upsellQuotePaymentModel.amount == undefined ? '' : upsellQuotePaymentModel.amount;
    }
    paymentPercentageId: string;
    amount: string;
    paymentMethodId: string;
    createdUserId: string;
    paymentDate;
    shouldDisable:boolean;
}


export {
    UpsellQuotePaymentModel
};

