class AddServices {
    constructor(addServices?: AddServices) {
        this.serviceCode = addServices == undefined ? null : addServices.serviceCode == undefined ? null : addServices.serviceCode;
        this.serviceDescription = addServices == undefined ? null : addServices.serviceDescription == undefined ? null : addServices.serviceDescription;
        this.qty = addServices == undefined ? null : addServices.qty == undefined ? null : addServices.qty;
        this.isQtyAvailable = addServices == undefined ? false : addServices.isQtyAvailable == undefined ? false : addServices.isQtyAvailable;
        this.serviceMappingId= addServices == undefined ? '' : addServices.serviceMappingId == undefined ? '' : addServices.serviceMappingId;
    }
    serviceCode: string;
    serviceDescription?: string;
    isQtyAvailable: boolean;
    qty?: string;
    serviceMappingId?: string;
 
}
export { AddServices };

