class CustomCallWorkflowModel {
  constructor(customCallWorkflowModel?: CustomCallWorkflowModel) {
    this.createdUserId = customCallWorkflowModel ? customCallWorkflowModel.createdUserId == undefined ? '' : customCallWorkflowModel.createdUserId : '';
    this.customerPreferenceCustomerCustomCallFlowId = customCallWorkflowModel ? customCallWorkflowModel.customerPreferenceCustomerCustomCallFlowId == undefined ? '' : customCallWorkflowModel.customerPreferenceCustomerCustomCallFlowId : '';
    this.customerAddressId = customCallWorkflowModel ? customCallWorkflowModel.customerAddressId == undefined ? '' : customCallWorkflowModel.customerAddressId : '';
    this.customerId = customCallWorkflowModel ? customCallWorkflowModel.customerId == undefined ? '' : customCallWorkflowModel.customerId : '';
    this.isSignals = customCallWorkflowModel ? customCallWorkflowModel.isSignals == undefined ? false : customCallWorkflowModel.isSignals : false;
    this.isDays = customCallWorkflowModel ? customCallWorkflowModel.isDays == undefined ? false : customCallWorkflowModel.isDays : false;

    this.isTimeRange = customCallWorkflowModel ? customCallWorkflowModel.isTimeRange == undefined ? false : customCallWorkflowModel.isTimeRange : false;
    this.description = customCallWorkflowModel ? customCallWorkflowModel.description == undefined ? '' : customCallWorkflowModel.description : '';
    this.timeFrom = customCallWorkflowModel ? customCallWorkflowModel.timeFrom == undefined ? null : customCallWorkflowModel.timeFrom : null;
    this.timeTo = customCallWorkflowModel ? customCallWorkflowModel.timeTo == undefined ? null : customCallWorkflowModel.timeTo : null;

    this.isActive = customCallWorkflowModel ? customCallWorkflowModel.isActive == undefined ? true : customCallWorkflowModel.isActive : true;
    this.dayList = customCallWorkflowModel ? customCallWorkflowModel.dayList == undefined ? null : customCallWorkflowModel.dayList : null;
    this.alarmTypeList = customCallWorkflowModel ? customCallWorkflowModel.alarmTypeList == undefined ? null : customCallWorkflowModel.alarmTypeList : null;
    this.keyHolderList = customCallWorkflowModel ? customCallWorkflowModel.keyHolderList == undefined ? null : customCallWorkflowModel.keyHolderList : null;
    this.partitionId = customCallWorkflowModel ? customCallWorkflowModel.partitionId == undefined ? '' : customCallWorkflowModel.partitionId : '';
  }
  createdUserId: any;
  customerPreferenceCustomerCustomCallFlowId: string;
  customerAddressId: string;
  customerId: string;
  isSignals: boolean;
  isDays: boolean;
  isTimeRange: boolean;
  description: string;
  timeFrom?: string;
  timeTo?: string;
  isActive: boolean;
  dayList: CustomerPreferenceCustomerCustomFlowDayListModel[];
  alarmTypeList: CustomerPreferenceCustomerCustomFlowSignalListModel[];
  keyHolderList: CustomerPreferenceCustomerCustomFlowKeyHolderListModel[];
  partitionId:any;
}

class CustomerPreferenceCustomerCustomFlowDayListModel {
  constructor(customerPreferenceCustomerCustomFlowDayListModel?: CustomerPreferenceCustomerCustomFlowDayListModel) {
    this.dayId = customerPreferenceCustomerCustomFlowDayListModel ? customerPreferenceCustomerCustomFlowDayListModel.dayId == undefined ? null : customerPreferenceCustomerCustomFlowDayListModel.dayId : null;
  }
  dayId: string;
}

class CustomerPreferenceCustomerCustomFlowSignalListModel {
  constructor(customerPreferenceCustomerCustomFlowSignalListModel?: CustomerPreferenceCustomerCustomFlowSignalListModel) {
    this.alarmTypeId = customerPreferenceCustomerCustomFlowSignalListModel ? customerPreferenceCustomerCustomFlowSignalListModel.alarmTypeId == undefined ? null : customerPreferenceCustomerCustomFlowSignalListModel.alarmTypeId : null;
  }
  alarmTypeId: string;
}

class CustomerPreferenceCustomerCustomFlowKeyHolderListModel {
  constructor(customerPreferenceCustomerCustomFlowKeyHolderListModel?: CustomerPreferenceCustomerCustomFlowKeyHolderListModel) {
    this.keyHolderId = customerPreferenceCustomerCustomFlowKeyHolderListModel ? customerPreferenceCustomerCustomFlowKeyHolderListModel.keyHolderId == undefined ? null : customerPreferenceCustomerCustomFlowKeyHolderListModel.keyHolderId : null;
    this.orderNumber = customerPreferenceCustomerCustomFlowKeyHolderListModel ? customerPreferenceCustomerCustomFlowKeyHolderListModel.orderNumber == undefined ? null : customerPreferenceCustomerCustomFlowKeyHolderListModel.orderNumber : null;
    this.IsCall = customerPreferenceCustomerCustomFlowKeyHolderListModel ? customerPreferenceCustomerCustomFlowKeyHolderListModel.IsCall == undefined ? false : customerPreferenceCustomerCustomFlowKeyHolderListModel.IsCall : false;

  }
  keyHolderId: string;
  orderNumber: string;
  IsCall: boolean;
}


class CustomerPreferenceModel {

  constructor(customerPreferenceModel?: CustomerPreferenceModel) {
    this.createdUserId = customerPreferenceModel ? customerPreferenceModel.createdUserId == undefined ? null : customerPreferenceModel.createdUserId : null;
    this.customerAddressId = customerPreferenceModel ? customerPreferenceModel.customerAddressId == undefined ? null : customerPreferenceModel.customerAddressId : null;
    this.isAutoFeedback = customerPreferenceModel ? customerPreferenceModel.isAutoFeedback == undefined ? false : customerPreferenceModel.isAutoFeedback : false;
    this.keyholderId = customerPreferenceModel ? customerPreferenceModel.keyholderId == undefined ? null : customerPreferenceModel.keyholderId : null;
    this.isAutoDispatch = customerPreferenceModel ? customerPreferenceModel.isAutoDispatch == undefined ? false : customerPreferenceModel.isAutoDispatch : false;
    this.namedStackConfigIdList = customerPreferenceModel ? customerPreferenceModel.namedStackConfigIdList == undefined ? [] : customerPreferenceModel.namedStackConfigIdList : [];
    this.isVirtualAgentService = customerPreferenceModel ? customerPreferenceModel.isVirtualAgentService == undefined ? false : customerPreferenceModel.isVirtualAgentService : false;
    this.virtualAgentServiceType = customerPreferenceModel ? customerPreferenceModel.virtualAgentServiceType == undefined ? 1 : customerPreferenceModel.virtualAgentServiceType : 1;
    this.virtualAgentServicePin = customerPreferenceModel ? customerPreferenceModel.virtualAgentServicePin == undefined ? null : customerPreferenceModel.virtualAgentServicePin : null;
    this.isActive = customerPreferenceModel ? customerPreferenceModel.isActive == undefined ? true : customerPreferenceModel.isActive : true;
    this.isSlipRequired = customerPreferenceModel ? customerPreferenceModel.isSlipRequired == undefined ? false : customerPreferenceModel.isSlipRequired : false;
    this.isCustomerAppNotification = customerPreferenceModel ? customerPreferenceModel.isCustomerAppNotification == undefined ? true : customerPreferenceModel.isCustomerAppNotification : true;
    this.isSMSNotification = customerPreferenceModel ? customerPreferenceModel.isSMSNotification == undefined ? true : customerPreferenceModel.isSMSNotification : true;
    this.isThirdPartyResponse = customerPreferenceModel ? customerPreferenceModel.isThirdPartyResponse == undefined ? false : customerPreferenceModel.isThirdPartyResponse : false;
    this.thirdPartyId = customerPreferenceModel ? customerPreferenceModel.thirdPartyId == undefined ? '' : customerPreferenceModel.thirdPartyId : '';
    this.thirdPartyCustomerCode = customerPreferenceModel ? customerPreferenceModel.thirdPartyCustomerCode == undefined ? '' : customerPreferenceModel.thirdPartyCustomerCode : '';
    this.partitionId = customerPreferenceModel ? customerPreferenceModel.partitionId == undefined ? '' : customerPreferenceModel.partitionId : '';

  }

  createdUserId: string;
  partitionId:string;
  customerAddressId: string;
  isAutoFeedback: boolean;
  keyholderId?: string;
  isAutoDispatch: boolean;
  namedStackConfigIdList?:number [];
  isVirtualAgentService: boolean;
  virtualAgentServiceType: number;
  virtualAgentServicePin?: string;
  isActive: boolean;
  isSlipRequired: boolean;
  isCustomerAppNotification: boolean;
  isSMSNotification: boolean;
  isThirdPartyResponse: boolean;
  thirdPartyId?: string;
  thirdPartyCustomerCode?: string;
}

export { CustomCallWorkflowModel, CustomerPreferenceModel }
