class PurchaseOfAlarmSystemAddEditModel {
    constructor(purchaseOfAlarmSystemAddEditModel?: PurchaseOfAlarmSystemAddEditModel) {
        this.date = purchaseOfAlarmSystemAddEditModel == undefined ? '' : purchaseOfAlarmSystemAddEditModel.date == undefined ? '' : purchaseOfAlarmSystemAddEditModel.date;
        this.inputValue = purchaseOfAlarmSystemAddEditModel == undefined ? '' : purchaseOfAlarmSystemAddEditModel.inputValue == undefined ? '' : purchaseOfAlarmSystemAddEditModel.inputValue;
        this.radioValue = purchaseOfAlarmSystemAddEditModel == undefined ? null : purchaseOfAlarmSystemAddEditModel.radioValue == undefined ? null : purchaseOfAlarmSystemAddEditModel.radioValue;
    }
    date: string;
    inputValue: string;
    radioValue: boolean;
}

class PurchaseOfAlarmSystemDocumentCaptureModel {
    constructor(purchaseOfAlarmSystemDocumentCaptureModel?: PurchaseOfAlarmSystemDocumentCaptureModel) {
        this.writeOffDocumentation = purchaseOfAlarmSystemDocumentCaptureModel == undefined ? '' : purchaseOfAlarmSystemDocumentCaptureModel.writeOffDocumentation == undefined ? '' : 
        purchaseOfAlarmSystemDocumentCaptureModel.writeOffDocumentation;
    }
    writeOffDocumentation: string;
}

export { PurchaseOfAlarmSystemAddEditModel, PurchaseOfAlarmSystemDocumentCaptureModel }