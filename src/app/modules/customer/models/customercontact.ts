export class CustomerContact{
    customerContactId:any;
    title:string;
    firstName:string;
    lastName:string;
    displayName:string;
    contactTypeId:any;
    customerId:any;
    description:string;
    designation:string;
    languageId:any;
    email:string;
    phone:string;
    mobile:string;
    fax:string;
    skype:string;
    ssnNumber:string;
    countryId:any;
    regionId:any;
    provinceId:any;
    cityId:any;
    suburbId:any;
    contactImage:string;
    thumbnailImage:string;
    largeImage:string;
    signatureImage:string;
    isDeleted:boolean;
    isActive:boolean;

 //importing for province,city,region,suburb name...
    countryName:string;
    provinceName:string;
    cityName:string;
    regionName:string;
    suburbName:string;
    languageName:string;
    contactTypeName:string;
}