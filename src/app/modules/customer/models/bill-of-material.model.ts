class BillOfMaterialModel {

    address?: string;
    billOfMaterialId?: string;
    billOfMaterialTypeId?: string;
    customer?: string;
    technicianName?: string;
    debtorRefNo?: string;
    referenceId?: string;
    customerId?: string;
    targetOrderAmount?: number; //string;
    differenceAmount?: number; //string;
    isStock?: number;
    isDiscount?: number;
    isLabour?: number;
    totalAmount?: number; //string;
    billOfMaterialStatusId?: string;
    createdUserId?: string;
    modifiedUserId?: string;
    jobCategory?: string;
    jobType?: string;
    quoteNumber?: string;
    quoteAmount?: number; //string;
    paymentMethod?: string;
    dealerDebitorCode?: string;
    status?: string;
    warehouseName?: string;
    stockCodeSerialNumber?: string;
    priceListCode?: string;
    billOfMaterialItems?: BillOfMaterialItems[] = [];
    bomInvoice?: BillOfMaterialInvoice = {};

    constructor(billOfMaterialModel?: BillOfMaterialModel, loogedInUserId?) {

        this.address = billOfMaterialModel ? billOfMaterialModel.address == undefined ? '-' : billOfMaterialModel.address : '-';

        this.billOfMaterialId = billOfMaterialModel ? billOfMaterialModel.billOfMaterialId == undefined ? '' : billOfMaterialModel.billOfMaterialId : '';

        this.billOfMaterialTypeId = billOfMaterialModel ? billOfMaterialModel.billOfMaterialTypeId == undefined ? '' : billOfMaterialModel.billOfMaterialTypeId : '';

        this.customer = billOfMaterialModel ? billOfMaterialModel.customer == undefined ? '-' : billOfMaterialModel.customer : '-';

        this.technicianName = billOfMaterialModel ? billOfMaterialModel.technicianName == undefined ? '-' : billOfMaterialModel.technicianName : '-';

        this.debtorRefNo = billOfMaterialModel ? billOfMaterialModel.debtorRefNo == undefined ? '-' : billOfMaterialModel.debtorRefNo : '-';

        this.referenceId = billOfMaterialModel ? billOfMaterialModel.referenceId == undefined ? '' : billOfMaterialModel.referenceId : '';

        this.customerId = billOfMaterialModel ? billOfMaterialModel.customerId == undefined ? '' : billOfMaterialModel.customerId : '';

        // this.targetOrderAmount = billOfMaterialModel ? billOfMaterialModel.totalAmount == undefined ? '' : billOfMaterialModel.totalAmount : '';

        this.targetOrderAmount = billOfMaterialModel ? billOfMaterialModel.targetOrderAmount == undefined ? 0 : billOfMaterialModel.targetOrderAmount : 0;

        this.differenceAmount = billOfMaterialModel ? billOfMaterialModel.differenceAmount == undefined ? 0 : billOfMaterialModel.differenceAmount : 0;

        this.isStock = billOfMaterialModel ? billOfMaterialModel.isStock == undefined ? 0 : billOfMaterialModel.isStock : 0;

        this.isDiscount = billOfMaterialModel ? billOfMaterialModel.isDiscount == undefined ? 0 : billOfMaterialModel.isDiscount : 0;

        this.isLabour = billOfMaterialModel ? billOfMaterialModel.isLabour == undefined ? 0 : billOfMaterialModel.isLabour : 0;

        this.totalAmount = billOfMaterialModel ? billOfMaterialModel.totalAmount == undefined ? 0 : billOfMaterialModel.totalAmount : 0;

        this.billOfMaterialStatusId = billOfMaterialModel ? billOfMaterialModel.billOfMaterialStatusId == undefined ? '' : billOfMaterialModel.billOfMaterialStatusId : '';

        this.createdUserId = billOfMaterialModel ? billOfMaterialModel.createdUserId == undefined ? '' : billOfMaterialModel.createdUserId : '';

        this.modifiedUserId = billOfMaterialModel ? billOfMaterialModel.modifiedUserId == undefined ? '' : billOfMaterialModel.modifiedUserId : '';

        this.jobCategory = billOfMaterialModel ? billOfMaterialModel.jobCategory == undefined ? '-' : billOfMaterialModel.jobCategory : '-';

        this.jobType = billOfMaterialModel ? billOfMaterialModel.jobType == undefined ? '-' : billOfMaterialModel.jobType : '-';

        this.quoteNumber = billOfMaterialModel ? billOfMaterialModel.quoteNumber == undefined ? '-' : billOfMaterialModel.quoteNumber : '-';

        this.quoteAmount = billOfMaterialModel ? billOfMaterialModel.quoteAmount == undefined ? 0 : billOfMaterialModel.quoteAmount : 0;

        this.paymentMethod = billOfMaterialModel ? billOfMaterialModel.paymentMethod == undefined ? '-' : billOfMaterialModel.paymentMethod : '-';

        this.dealerDebitorCode = billOfMaterialModel ? billOfMaterialModel.dealerDebitorCode == undefined ? '-' : billOfMaterialModel.dealerDebitorCode : '-';

        this.status = billOfMaterialModel ? billOfMaterialModel.status == undefined ? '-' : billOfMaterialModel.status : '-';

        this.warehouseName = billOfMaterialModel ? billOfMaterialModel.warehouseName == undefined ? '-' : billOfMaterialModel.warehouseName : '-';

        this.stockCodeSerialNumber = billOfMaterialModel ? billOfMaterialModel.stockCodeSerialNumber == undefined ? '' : billOfMaterialModel.stockCodeSerialNumber : '';

        this.priceListCode = billOfMaterialModel ? billOfMaterialModel.priceListCode == undefined ? '-' : billOfMaterialModel.priceListCode : '-';

        if (billOfMaterialModel != undefined) {
            if (billOfMaterialModel['bomQuotationItems'] && billOfMaterialModel['bomQuotationItems'].length > 0) {
                billOfMaterialModel['bomQuotationItems'].forEach((element, index) => {
                    let billOfMaterialItemDetails = [];
                    if(element['billOfMaterialItemDetails'] && element['billOfMaterialItemDetails'].length > 0) {
                        element['billOfMaterialItemDetails'].forEach(item => {
                            billOfMaterialItemDetails.push({
                                billOfMaterialItemDetailId: item['billOfMaterialItemDetailId'],
                                billOfMaterialItemId: item['billOfMaterialItemId'],
                                serialNumber: item['serialNumber'],
                                createdUserId: item['createdUserId'],
                                modifiedUserId: item['modifiedUserId']
                            }); 
                        });
                    }
                    this.billOfMaterialItems.push({
                        checkBillOfMaterial: element['checkBillOfMaterial'] ? true : false,
                        billOfMaterialItemId: element['billOfMaterialItemId'],
                        billOfMaterialId: element['billOfMaterialId'],
                        stockTypeId: element['stockTypeId'],
                        stockType: element['stockType'],
                        itemId: element['itemId'],
                        itemCode: element['itemCode'],
                        itemName: element['itemName'],
                        kitConfigId: element['kitConfigId'],
                        kitTypeName: element['kitTypeName'],
                        invoice: element['invoice'],
                        transfer: element['transfer'],
                        used: element['used'],
                        reserved: element['reserved'],
                        request: element['request'],
                        unitPrice: element['unitPrice']?.toString() ? element['unitPrice'] : '0.00',
                        subTotal: element['subTotal']?element['subTotal'].toFixed(2):'0.00',
                        vATAmount: element['vat']?element['vat'].toFixed(2):'0.00',
                        totalAmount: element['total']?element['total'].toFixed(2):'0.00',
                        IsDeleted: false,
                        createdUserId: loogedInUserId,
                        modifiedUserId: loogedInUserId,
                        billOfMaterialItemDetails: billOfMaterialItemDetails,
                        IsWarranty: element['isWarranty'],
                        WarrantyQty: element['warrantyQty'],
                        systemTypeId: element['systemTypeId'],
                        itemPricingConfigId: element['itemPricingConfigId'],
                        isNotSerialized: element['isNotSerialized'],
                    });                    
                });
            }            
        }

        if (billOfMaterialModel != undefined && billOfMaterialModel['bomInvoice'] && Object.keys(billOfMaterialModel['bomInvoice']).length > 0) {
            this.bomInvoice = {
                exclVAT: billOfMaterialModel['bomInvoice']['exclVAT']?billOfMaterialModel['bomInvoice']['exclVAT'].toFixed(2):'0.00',
                subTotal: billOfMaterialModel['bomInvoice']['subTotal']?billOfMaterialModel['bomInvoice']['subTotal'].toFixed(2):'0.00',
                totalAmount: billOfMaterialModel['bomInvoice']['totalAmount']?billOfMaterialModel['bomInvoice']['totalAmount'].toFixed(2):'0.00',
                vatAmount: billOfMaterialModel['bomInvoice']['vatAmount']?billOfMaterialModel['bomInvoice']['vatAmount'].toFixed(2):'0.00'
            }
        } else {
            this.bomInvoice = {
                exclVAT: '0.00',
                subTotal: '0.00',
                totalAmount: '0.00',
                vatAmount: '0.00'
            }
        }
    }
}

interface BillOfMaterialItems {
    checkBillOfMaterial?: boolean;
    billOfMaterialItemId?: string;
    billOfMaterialId?: string;
    stockTypeId?: string;
    stockType?: string;
    itemId?: string;
    itemCode?: string;
    itemName?: string;
    kitConfigId?: string;
    kitTypeName?: string;
    invoice?: string;
    transfer?: string;
    used?: string;
    reserved?: string;
    request?: number; //string;
    unitPrice?: number; //string;
    subTotal?: number; //string;
    vATAmount?: number; //string;
    totalAmount?: number; //string;
    createdUserId?: string;
    modifiedUserId?: string;
    billOfMaterialItemDetails?: BillOfMaterialItemDetails[];
    IsDeleted?: boolean;
    IsWarranty?: boolean, // used when pop up is shown and to show warranty items >> row 
    WarrantyQty?: any // used to display changed qty on pop up >> only qty
    systemTypeId?: string;
    itemPricingConfigId?: string;
    isNotSerialized?: boolean;
}

interface BillOfMaterialItemDetails {
    billOfMaterialItemDetailId?: string;
    billOfMaterialItemId?: string;
    serialNumber?: string;
    createdUserId?: string;
    modifiedUserId?: string;
}

interface BillOfMaterialInvoice {
    exclVAT?: any; //number; //string;
    subTotal?: any; //number; //string;
    totalAmount?: any; //number; //string;
    vatAmount?: any; //number; //string;
}

class StockCodeDescriptionModel {

    stockType?: string;
    stockCode?: string;
    stockDescription?: string;
    stockUnitPriceExcl?: string;
    stockRequestedQty?: string;
    stockTypeId?: string;
    itemId?: string;
    kitConfigId?: string;
    kitTypeName?: string;
    systemTypeId?: string;
    itemPricingConfigId?: string;
    isNotSerialized?: string;
    stockAddItemFormarry?: StockCodeDescriptionFormArrayModel[];

    constructor(stockCodeDescriptionModel?: StockCodeDescriptionModel) {

        this.stockType = stockCodeDescriptionModel ? stockCodeDescriptionModel.stockType == undefined ? '' : stockCodeDescriptionModel.stockType : '';

        this.stockCode = stockCodeDescriptionModel ? stockCodeDescriptionModel.stockCode == undefined ? '' : stockCodeDescriptionModel.stockCode : '';

        this.stockDescription = stockCodeDescriptionModel ? stockCodeDescriptionModel.stockDescription == undefined ? '' : stockCodeDescriptionModel.stockDescription : '';

        this.stockUnitPriceExcl = stockCodeDescriptionModel ? stockCodeDescriptionModel.stockUnitPriceExcl == undefined ? '' : stockCodeDescriptionModel.stockUnitPriceExcl : '';

        this.stockRequestedQty = stockCodeDescriptionModel ? stockCodeDescriptionModel.stockRequestedQty == undefined ? '' : stockCodeDescriptionModel.stockRequestedQty : '';

        this.stockTypeId = stockCodeDescriptionModel ? stockCodeDescriptionModel.stockTypeId == undefined ? '' : stockCodeDescriptionModel.stockTypeId : '';

        this.itemId = stockCodeDescriptionModel ? stockCodeDescriptionModel.itemId == undefined ? '' : stockCodeDescriptionModel.itemId : '';

        this.kitConfigId = stockCodeDescriptionModel ? stockCodeDescriptionModel.kitConfigId == undefined ? '' : stockCodeDescriptionModel.kitConfigId : '';

        this.kitTypeName = stockCodeDescriptionModel ? stockCodeDescriptionModel.kitTypeName == undefined ? '' : stockCodeDescriptionModel.kitTypeName : '';

        this.systemTypeId = stockCodeDescriptionModel ? stockCodeDescriptionModel.systemTypeId == undefined ? '' : stockCodeDescriptionModel.systemTypeId : '';

        this.itemPricingConfigId = stockCodeDescriptionModel ? stockCodeDescriptionModel.itemPricingConfigId == undefined ? '' : stockCodeDescriptionModel.itemPricingConfigId : '';

        this.isNotSerialized = stockCodeDescriptionModel ? stockCodeDescriptionModel.isNotSerialized == undefined ? '' : stockCodeDescriptionModel.isNotSerialized : '';
        this.stockAddItemFormarry = stockCodeDescriptionModel ? stockCodeDescriptionModel.stockAddItemFormarry == undefined ? [] : stockCodeDescriptionModel.stockAddItemFormarry : [];

    }

}

class StockCodeDescriptionFormArrayModel {

    id?: string;
    stockType?: string;
    stockCode?: string;
    stockDescription?: string;
    stockUnitPriceExcl?: string;
    stockRequestedQty?: string;
    stockTypeId?: string;
    itemId?: string;
    kitConfigId?: string;
    kitTypeName?: string;
    systemTypeId?: string;
    itemPricingConfigId?: string;
    isNotSerialized?: boolean;
    costPrice?: string;
    displayName?: string;
    isDecoder?: boolean;

    constructor(stockCodeDescriptionFormArrayModel?: StockCodeDescriptionFormArrayModel) {

        this.id = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.id == undefined ? '' : stockCodeDescriptionFormArrayModel.id : '';
        
        this.stockType = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.stockType == undefined ? '' : stockCodeDescriptionFormArrayModel.stockType : '';

        this.stockCode = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.stockCode == undefined ? '' : stockCodeDescriptionFormArrayModel.stockCode : '';

        this.stockDescription = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.stockDescription == undefined ? '' : stockCodeDescriptionFormArrayModel.stockDescription : '';

        this.stockUnitPriceExcl = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.stockUnitPriceExcl == undefined ? '' : stockCodeDescriptionFormArrayModel.stockUnitPriceExcl : '';

        this.stockRequestedQty = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.stockRequestedQty == undefined ? '' : stockCodeDescriptionFormArrayModel.stockRequestedQty : '';

        this.stockTypeId = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.stockTypeId == undefined ? '' : stockCodeDescriptionFormArrayModel.stockTypeId : '';

        this.itemId = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.itemId == undefined ? '' : stockCodeDescriptionFormArrayModel.itemId : '';

        this.kitConfigId = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.kitConfigId == undefined ? '' : stockCodeDescriptionFormArrayModel.kitConfigId : '';

        this.kitTypeName = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.kitTypeName == undefined ? '' : stockCodeDescriptionFormArrayModel.kitTypeName : '';

        this.systemTypeId = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.systemTypeId == undefined ? '' : stockCodeDescriptionFormArrayModel.systemTypeId : '';

        this.itemPricingConfigId = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.itemPricingConfigId == undefined ? '' : stockCodeDescriptionFormArrayModel.itemPricingConfigId : '';

        this.isNotSerialized = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.isNotSerialized == undefined ? false : stockCodeDescriptionFormArrayModel.isNotSerialized : false;

        this.costPrice = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.costPrice == undefined ? '' : stockCodeDescriptionFormArrayModel.costPrice : '';

        this.displayName = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.displayName == undefined ? '' : stockCodeDescriptionFormArrayModel.displayName : '';

        this.isDecoder = stockCodeDescriptionFormArrayModel ? stockCodeDescriptionFormArrayModel.isDecoder == undefined ? false : stockCodeDescriptionFormArrayModel.isDecoder : false;

    }

}
class PrCallStockItemModel{
    comments ?:string;
    Equipment ?: string;
    totalStockItems ?:string;
    stockItems ?:PrCallStockItemArray[] =[]
    

    constructor(prCallStockItemModel?: PrCallStockItemModel) {

        this.comments = prCallStockItemModel ? prCallStockItemModel.comments == undefined ? '' : prCallStockItemModel.comments : '';
        this.Equipment = prCallStockItemModel ? prCallStockItemModel.Equipment == undefined ? '' : prCallStockItemModel.Equipment : '';
        this.totalStockItems = prCallStockItemModel ? prCallStockItemModel.totalStockItems == undefined ? '' : prCallStockItemModel.totalStockItems : '';

        this.stockItems = prCallStockItemModel ? prCallStockItemModel.stockItems == undefined ? [] : prCallStockItemModel.stockItems : [];

        // this.stockDescription = prCallStockItemModel ? prCallStockItemModel.stockDescription == undefined ? '' : prCallStockItemModel.stockDescription : '';

        // this.stockUnitPriceExcl = prCallStockItemModel ? prCallStockItemModel.stockUnitPriceExcl == undefined ? '' : prCallStockItemModel.stockUnitPriceExcl : '';

        // this.stockRequestedQty = prCallStockItemModel ? prCallStockItemModel.stockRequestedQty == undefined ? '' : prCallStockItemModel.stockRequestedQty : '';

        // this.stockTypeId = prCallStockItemModel ? prCallStockItemModel.stockTypeId == undefined ? '' : prCallStockItemModel.stockTypeId : '';

        // this.itemId = prCallStockItemModel ? prCallStockItemModel.itemId == undefined ? '' : prCallStockItemModel.itemId : '';
    }

}

class PrCallStockItemArray{
    
    stockCode?: string;
    itemId?: string;
    stockDesc?: string;
    stockQty?: string;
    stockUnitPriceExVat?: string;
    stockSubTotalExVat?: string;
    stockSubTotalIncVat?: string;
    itemPricingConfigId?:  string;

    constructor(prCallStockItemArray?: PrCallStockItemArray) {

        this.itemId = prCallStockItemArray ? prCallStockItemArray.itemId == undefined ? '' : prCallStockItemArray.itemId : '';

        this.stockCode = prCallStockItemArray ? prCallStockItemArray.stockCode == undefined ? '' : prCallStockItemArray.stockCode : '';

        this.stockDesc = prCallStockItemArray ? prCallStockItemArray.stockDesc == undefined ? '' : prCallStockItemArray.stockDesc : '';

        this.stockQty = prCallStockItemArray ? prCallStockItemArray.stockQty == undefined ? '' : prCallStockItemArray.stockQty : '';

        this.stockUnitPriceExVat = prCallStockItemArray ? prCallStockItemArray.stockUnitPriceExVat == undefined ? '' : prCallStockItemArray.stockUnitPriceExVat : '';

        this.stockSubTotalExVat = prCallStockItemArray ? prCallStockItemArray.stockSubTotalExVat == undefined ? '' : prCallStockItemArray.stockSubTotalExVat : '';

        this.stockSubTotalIncVat = prCallStockItemArray ? prCallStockItemArray.stockSubTotalIncVat == undefined ? '' : prCallStockItemArray.stockSubTotalIncVat : '';
        this.itemPricingConfigId =  prCallStockItemArray ? prCallStockItemArray.itemPricingConfigId == undefined ? '' : prCallStockItemArray.itemPricingConfigId : '';
    }


}

class ProcessPayment {
    totalAmount?: string;
    billOfMaterialId?: string;
    customerName?: string;
    designation?: string;
    paymentMethodId?: string;
    isSinglePayment?: boolean
    notes?: string;
    debtorAccountDetailId?: string;
    isMultiplePayament?: boolean;
    debitOrderRunCodeId?: string;
    smsVerificationCountryCode?: string;
    smsVerificationNumber?: string;
    isIncludePONumber?: boolean;
    createdUserId?: string;
    bankName?: string;
    bankBranch?: string;
    poDate?: string;
    poNumber?: string;
    paymentDate?: string;
    noOfPayments?: string;
    multipleTotalPayment?: string;
    multipleStartDate?: string;
    transactionDescription?: string;
    purchaseOrder?: boolean;
    poAttachment?: any;
    techJobCardPaymentDetails?: TechJobCardPaymentDetails[] = [];

    constructor(processPayment?: ProcessPayment) {
        this.totalAmount = processPayment ? processPayment.totalAmount == undefined ? '' : processPayment.totalAmount : '';

        this.billOfMaterialId = processPayment ? processPayment.billOfMaterialId == undefined ? '' : processPayment.billOfMaterialId : '';

        this.customerName = processPayment ? processPayment.customerName == undefined ? '' : processPayment.customerName : '';

        this.designation = processPayment ? processPayment.designation == undefined ? '' : processPayment.designation : '';

        this.paymentMethodId = processPayment ? processPayment.paymentMethodId == undefined ? '' : processPayment.paymentMethodId : '';

        this.isSinglePayment = processPayment ? processPayment.isSinglePayment == undefined ? false : processPayment.isSinglePayment : false;

        this.notes = processPayment ? processPayment.notes == undefined ? '' : processPayment.notes : '';

        this.debtorAccountDetailId = processPayment ? processPayment.debtorAccountDetailId == undefined ? '' : processPayment.debtorAccountDetailId : '';

        this.isMultiplePayament = processPayment ? processPayment.isMultiplePayament == undefined ? false : processPayment.isMultiplePayament : false;

        this.debitOrderRunCodeId = processPayment ? processPayment.debitOrderRunCodeId == undefined ? '' : processPayment.debitOrderRunCodeId : '';

        this.smsVerificationCountryCode = processPayment ? processPayment?.smsVerificationCountryCode == undefined ? '' : processPayment?.smsVerificationCountryCode : '';
    
        this.smsVerificationNumber = processPayment ? processPayment.smsVerificationNumber == undefined ? '' : processPayment.smsVerificationNumber : '';

        this.isIncludePONumber = processPayment ? processPayment.isIncludePONumber == undefined ? false : processPayment.isIncludePONumber : false;

        this.createdUserId = processPayment ? processPayment.createdUserId == undefined ? '' : processPayment.createdUserId : '';

        this.bankName = processPayment ? processPayment.bankName == undefined ? '' : processPayment.bankName : '';

        this.bankBranch = processPayment ? processPayment.bankBranch == undefined ? '' : processPayment.bankBranch : '';

        this.poDate = processPayment ? processPayment.poDate == undefined ? '' : processPayment.poDate : '';

        this.poNumber = processPayment ? processPayment.poNumber == undefined ? '' : processPayment.poNumber : '';

        this.paymentDate = processPayment ? processPayment.paymentDate == undefined ? '' : processPayment.paymentDate : '';

        this.noOfPayments = processPayment ? processPayment.noOfPayments == undefined ? '' : processPayment.noOfPayments : '';

        this.multipleTotalPayment = processPayment ? processPayment.multipleTotalPayment == undefined ? '' : processPayment.multipleTotalPayment : '';

        this.multipleStartDate = processPayment ? processPayment.multipleStartDate == undefined ? '' : processPayment.multipleStartDate : '';

        this.transactionDescription = processPayment ? processPayment.transactionDescription == undefined ? '' : processPayment.transactionDescription : '';

        this.purchaseOrder = false;

        this.poAttachment = processPayment ? processPayment.poAttachment == undefined ? '' : processPayment.poAttachment : '';

        if (processPayment != undefined) {
            if (processPayment['bomQuotationItems'] && processPayment['bomQuotationItems'].length > 0) {
                processPayment['bomQuotationItems'].forEach((element, index) => {                    
                    this.techJobCardPaymentDetails.push({
                        paymentDate: element['checkBillOfMaterial'],
                        paymentAmount: element['billOfMaterialItemId'],
                    });                    
                });
            }            
        }
    }
}
interface TechJobCardPaymentDetails {
    paymentDate?: string;
    paymentAmount?: string;
}

class NKADoaRequestDialog {

    NKAPurchaseOrderRequestId: string;
    callInitiationId: string;
    comments: string;
    equipment: string;
    createdUserId: string;
    totalStockItems: string;
    itemRequestDetailsArray: Array<NKADoaRequestItemArray>;

    constructor(nkaDoaRequestDialog?: NKADoaRequestDialog) {
        this.NKAPurchaseOrderRequestId = nkaDoaRequestDialog ? nkaDoaRequestDialog.NKAPurchaseOrderRequestId == undefined ? '' : nkaDoaRequestDialog.NKAPurchaseOrderRequestId : '';
        this.callInitiationId = nkaDoaRequestDialog ? nkaDoaRequestDialog.callInitiationId == undefined ? '' : nkaDoaRequestDialog.callInitiationId : '';
        this.comments = nkaDoaRequestDialog ? nkaDoaRequestDialog.comments == undefined ? '' : nkaDoaRequestDialog.comments : '';
        this.equipment = nkaDoaRequestDialog ? nkaDoaRequestDialog.equipment == undefined ? '' : nkaDoaRequestDialog.equipment : '';
        this.createdUserId = nkaDoaRequestDialog ? nkaDoaRequestDialog.createdUserId == undefined ? '' : nkaDoaRequestDialog.createdUserId : '';
        this.totalStockItems = nkaDoaRequestDialog ? nkaDoaRequestDialog.totalStockItems == undefined ? '' : nkaDoaRequestDialog.totalStockItems : '';
        this.itemRequestDetailsArray = nkaDoaRequestDialog ? nkaDoaRequestDialog.itemRequestDetailsArray == undefined ? [] : nkaDoaRequestDialog.itemRequestDetailsArray : [];
    }
}

class NKADoaRequestItemArray {
    
    NKAPurchaseOrderRequestItemId?: string;
    NKAPurchaseOrderRequestId?: string;
    itemId?: string;
    itemCode?: string;
    itemName?: string;
    quantity?: string;
    unitPriceExclVat?: string;
    subTotalExclVat?: string;
    subTotalInclVat?:  string;
    itemPricingConfigId?:  string;

    constructor(nkaDoaRequestItemArray?: NKADoaRequestItemArray) {
        this.NKAPurchaseOrderRequestItemId = nkaDoaRequestItemArray ? nkaDoaRequestItemArray?.NKAPurchaseOrderRequestItemId == undefined ? '' : nkaDoaRequestItemArray?.NKAPurchaseOrderRequestItemId : '';
        this.NKAPurchaseOrderRequestId = nkaDoaRequestItemArray ? nkaDoaRequestItemArray.NKAPurchaseOrderRequestId == undefined ? '' : nkaDoaRequestItemArray.NKAPurchaseOrderRequestId : '';
        this.itemId = nkaDoaRequestItemArray ? nkaDoaRequestItemArray.itemId == undefined ? '' : nkaDoaRequestItemArray.itemId : '';
        this.itemCode = nkaDoaRequestItemArray ? nkaDoaRequestItemArray.itemCode == undefined ? '' : nkaDoaRequestItemArray.itemCode : '';
        this.itemName = nkaDoaRequestItemArray ? nkaDoaRequestItemArray.itemName == undefined ? '' : nkaDoaRequestItemArray.itemName : '';
        this.quantity = nkaDoaRequestItemArray ? nkaDoaRequestItemArray.quantity == undefined ? '' : nkaDoaRequestItemArray.quantity : '';
        this.unitPriceExclVat = nkaDoaRequestItemArray ? nkaDoaRequestItemArray.unitPriceExclVat == undefined ? '' : nkaDoaRequestItemArray.unitPriceExclVat : '';
        this.subTotalExclVat = nkaDoaRequestItemArray ? nkaDoaRequestItemArray.subTotalExclVat == undefined ? '' : nkaDoaRequestItemArray.subTotalExclVat : '';
        this.subTotalInclVat =  nkaDoaRequestItemArray ? nkaDoaRequestItemArray.subTotalInclVat == undefined ? '' : nkaDoaRequestItemArray.subTotalInclVat : '';
        this.itemPricingConfigId =  nkaDoaRequestItemArray ? nkaDoaRequestItemArray.itemPricingConfigId == undefined ? '' : nkaDoaRequestItemArray.itemPricingConfigId : '';
    }
}

export { BillOfMaterialModel, StockCodeDescriptionModel, PrCallStockItemModel, ProcessPayment, StockCodeDescriptionFormArrayModel, NKADoaRequestDialog, NKADoaRequestItemArray };

