class CustomerHolidayInstructionAddEditModel{
    constructor(customerHolidayInstructionAddEditModel?:CustomerHolidayInstructionAddEditModel){
        this.customerId=customerHolidayInstructionAddEditModel?customerHolidayInstructionAddEditModel.customerId==undefined?'':customerHolidayInstructionAddEditModel.customerId:'';
        this.addressId=customerHolidayInstructionAddEditModel?customerHolidayInstructionAddEditModel.addressId==undefined?'':customerHolidayInstructionAddEditModel.addressId:'';
        this.isAdditionalPaidPatrols=customerHolidayInstructionAddEditModel?customerHolidayInstructionAddEditModel.isAdditionalPaidPatrols==undefined?false:customerHolidayInstructionAddEditModel.isAdditionalPaidPatrols:false;
        this.holidayInstructionFromDate=customerHolidayInstructionAddEditModel?customerHolidayInstructionAddEditModel.holidayInstructionFromDate==undefined?'':customerHolidayInstructionAddEditModel.holidayInstructionFromDate:'';
        this.holidayInstructionToDate=customerHolidayInstructionAddEditModel?customerHolidayInstructionAddEditModel.holidayInstructionToDate==undefined?'':customerHolidayInstructionAddEditModel.holidayInstructionToDate:'';
        this.comments=customerHolidayInstructionAddEditModel?customerHolidayInstructionAddEditModel.comments==undefined?'':customerHolidayInstructionAddEditModel.comments:'';
        this.numberOfDailyPaidPatrols=customerHolidayInstructionAddEditModel?customerHolidayInstructionAddEditModel.numberOfDailyPaidPatrols==undefined?null:customerHolidayInstructionAddEditModel.numberOfDailyPaidPatrols:null;
        this.paidPatrolStartDate=customerHolidayInstructionAddEditModel?customerHolidayInstructionAddEditModel.paidPatrolStartDate==undefined?'':customerHolidayInstructionAddEditModel.paidPatrolStartDate:'';
        this.paidPatrolEndDate=customerHolidayInstructionAddEditModel?customerHolidayInstructionAddEditModel.paidPatrolEndDate==undefined?'':customerHolidayInstructionAddEditModel.paidPatrolEndDate:'';
        this.holidayInstructionId=customerHolidayInstructionAddEditModel?customerHolidayInstructionAddEditModel.holidayInstructionId==undefined?'':customerHolidayInstructionAddEditModel.holidayInstructionId:'';
        this.createdUserId=customerHolidayInstructionAddEditModel?customerHolidayInstructionAddEditModel.createdUserId==undefined?'':customerHolidayInstructionAddEditModel.createdUserId:'';
        this.holidayInstructionContacts = customerHolidayInstructionAddEditModel ? customerHolidayInstructionAddEditModel.holidayInstructionContacts == undefined ? [] : customerHolidayInstructionAddEditModel.holidayInstructionContacts : [];
    }
    customerId: string;
    addressId: string;
    createdUserId: string;
    holidayInstructionId: string;
    holidayInstructionFromDate: string;
    holidayInstructionToDate: string;
    comments: string;
    isAdditionalPaidPatrols: boolean;
    numberOfDailyPaidPatrols: number;
    paidPatrolStartDate: string;
    paidPatrolEndDate: string;
    holidayInstructionContacts: HolidayInstructionContactsModel[];
    holidayInstructionEmergencyContacts: HolidayInstructionEmergencyContactsModel[];
}
class HolidayInstructionContactsModel {
    constructor(holidayInstructionContactsModel?: HolidayInstructionContactsModel) {
        this.contactPersonName = holidayInstructionContactsModel ? holidayInstructionContactsModel.contactPersonName == undefined ? '' : holidayInstructionContactsModel.contactPersonName : '';
        this.contactNumberCountryCode = holidayInstructionContactsModel ? holidayInstructionContactsModel.contactNumberCountryCode == undefined ? '+27' : holidayInstructionContactsModel.contactNumberCountryCode : '+27';
        this.contactNumber = holidayInstructionContactsModel ? holidayInstructionContactsModel.contactNumber == undefined ? '' : holidayInstructionContactsModel.contactNumber : '';
        this.holidayInstructionContactId = holidayInstructionContactsModel ? holidayInstructionContactsModel.holidayInstructionContactId == undefined ? '' : holidayInstructionContactsModel.holidayInstructionContactId : '';
    }
    contactPersonName?: string;
    contactNumberCountryCode?: string;
    contactNumber?: string;
    holidayInstructionContactId? : string
}
class HolidayInstructionEmergencyContactsModel {
    constructor(holidayInstructionEmergencyContacts?: HolidayInstructionEmergencyContactsModel) {
        this.emergencyContactPersonName = holidayInstructionEmergencyContacts ? holidayInstructionEmergencyContacts.emergencyContactPersonName == undefined ? '' : holidayInstructionEmergencyContacts.emergencyContactPersonName : '';
        this.emergencyContactNumberCountryCode = holidayInstructionEmergencyContacts ? holidayInstructionEmergencyContacts.emergencyContactNumberCountryCode == undefined ? '+27' : holidayInstructionEmergencyContacts.emergencyContactNumberCountryCode : '+27';
        this.emergencyContactNumber = holidayInstructionEmergencyContacts ? holidayInstructionEmergencyContacts.emergencyContactNumber == undefined ? '' : holidayInstructionEmergencyContacts.emergencyContactNumber : '';
        this.holidayInstructionEmergencyContactId = holidayInstructionEmergencyContacts ? holidayInstructionEmergencyContacts.holidayInstructionEmergencyContactId == undefined ? '' : holidayInstructionEmergencyContacts.holidayInstructionEmergencyContactId : '';

    }
    emergencyContactPersonName?: string;
    emergencyContactNumberCountryCode?: string;
    emergencyContactNumber?: string;
    holidayInstructionEmergencyContactId?: string;
}

export { CustomerHolidayInstructionAddEditModel, HolidayInstructionContactsModel, HolidayInstructionEmergencyContactsModel }
