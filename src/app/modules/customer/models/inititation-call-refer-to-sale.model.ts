 class CallInitiationReferToSaleModel {
    constructor(callInitiationReferToSaleModel: CallInitiationReferToSaleModel) {
        this.appointmentId = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.appointmentId == undefined ? '' : callInitiationReferToSaleModel.appointmentId : '';
        this.creatorsContactNoCountryCode = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.creatorsContactNoCountryCode == undefined ? '' : callInitiationReferToSaleModel.creatorsContactNoCountryCode : '';
        this.creatorsContactNo = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.creatorsContactNo == undefined ? '' : callInitiationReferToSaleModel.creatorsContactNo : '';
        this.isCode50 = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.isCode50 == undefined ? false : callInitiationReferToSaleModel.isCode50 : false;
        this.description = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.description == undefined ? '' : callInitiationReferToSaleModel.description : '';
        this.description = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.description == undefined ? '' : callInitiationReferToSaleModel.description : '';
        this.isAcknowledge = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.isAcknowledge == undefined ? false : callInitiationReferToSaleModel.isAcknowledge : false;
        this.acknowledgeById = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.acknowledgeById == undefined ? '' : callInitiationReferToSaleModel.acknowledgeById : '';
        this.acknowledgeDate = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.acknowledgeDate == undefined ? '' : callInitiationReferToSaleModel.acknowledgeDate : '';
        this.acknowledgeRefertoSalesFeedbackId = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.acknowledgeRefertoSalesFeedbackId == undefined ? '' : callInitiationReferToSaleModel.acknowledgeRefertoSalesFeedbackId : '';
        this.acknowledgeFeedbackDescription = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.acknowledgeFeedbackDescription == undefined ? '' : callInitiationReferToSaleModel.acknowledgeFeedbackDescription : '';
        this.acknowledgeBy = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.acknowledgeBy == undefined ? '' : callInitiationReferToSaleModel.acknowledgeBy : '';
        this.createdUserId = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.createdUserId == undefined ? '' : callInitiationReferToSaleModel.createdUserId : '';
        this.referToSaleFlagType = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.referToSaleFlagType == undefined ? '' : callInitiationReferToSaleModel.referToSaleFlagType : '';
        this.referToSalesFlagList = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.referToSalesFlagList == undefined ? [] : callInitiationReferToSaleModel.referToSalesFlagList : [];
        this.callCreatedDate = callInitiationReferToSaleModel ? callInitiationReferToSaleModel.callCreatedDate == undefined ? '' : callInitiationReferToSaleModel.callCreatedDate : '';
    }
    appointmentId:string;
    creatorsContactNoCountryCode:string;
    creatorsContactNo:string;
    isCode50:boolean;
    description:string;
    createdUserId:string;
    isAcknowledge: boolean;
    acknowledgeById:string;
    acknowledgeBy : string;
    acknowledgeDate:string;
    acknowledgeRefertoSalesFeedbackId:string;
    acknowledgeFeedbackDescription:string;
    referToSaleFlagType:string;
    referToSalesFlagList:ReferToSalesFlagModel[];
    callCreatedDate:string;
//  [
// {
// "referToSalesStatusId": 1,
// "refertoSalesFlagTypeId": 10
// },
// {
// "referToSalesStatusId": 1,
// "refertoSalesFlagTypeId": 11
// }
// ]
}
class ReferToSalesFlagModel {
    constructor(referToSalesFlagModel: ReferToSalesFlagModel) {
        this.referToSalesStatusName = referToSalesFlagModel ? referToSalesFlagModel.referToSalesStatusName == undefined ? '' : referToSalesFlagModel.referToSalesStatusName : '';
        this.referToSalesFlagTypeId = referToSalesFlagModel ? referToSalesFlagModel.referToSalesFlagTypeId == undefined ? '' : referToSalesFlagModel.referToSalesFlagTypeId : '';
        this.referToSalesFlagTypeName = referToSalesFlagModel ? referToSalesFlagModel.referToSalesFlagTypeName == undefined ? '' : referToSalesFlagModel.referToSalesFlagTypeName : '';
        this.createdBy = referToSalesFlagModel ? referToSalesFlagModel.createdBy == undefined ? '' : referToSalesFlagModel.createdBy : '';
        this.createdDate = referToSalesFlagModel ? referToSalesFlagModel.createdDate == undefined ? '' : referToSalesFlagModel.createdDate : '';
        this.referToSalesRefNo = referToSalesFlagModel ? referToSalesFlagModel.referToSalesRefNo == undefined ? '' : referToSalesFlagModel.referToSalesRefNo : '';
        this.referToSalesId = referToSalesFlagModel ? referToSalesFlagModel.referToSalesId == undefined ? '' : referToSalesFlagModel.referToSalesId : '';     
        this.description = referToSalesFlagModel ? referToSalesFlagModel.description == undefined ? '' : referToSalesFlagModel.description : '';
        this.callCreatedDate = referToSalesFlagModel ? referToSalesFlagModel.callCreatedDate == undefined ? '' : referToSalesFlagModel.callCreatedDate : '';
        this.isAcknowledge = referToSalesFlagModel ? referToSalesFlagModel.isAcknowledge == undefined ? false : referToSalesFlagModel.isAcknowledge : false;
    }
    referToSalesId: string;
    referToSalesRefNo: string;
    createdDate: string;
    createdBy: string;
    referToSalesFlagTypeId: string;
    isAcknowledge:boolean;
    referToSalesFlagTypeName: string;
    referToSalesStatusName: string;
    description:string;
    callCreatedDate:string;
    // referToSalesStatusId: string;
    // refertoSalesFlagTypeId: string;
    
}

export { CallInitiationReferToSaleModel };

