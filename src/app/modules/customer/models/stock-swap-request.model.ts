
class StockSwapRequestModel {
    constructor(stockSwapRequestModel?: StockSwapRequestModel) {
        this.stockSwapId = stockSwapRequestModel ? stockSwapRequestModel.stockSwapId == undefined ? '' : stockSwapRequestModel.stockSwapId : '';
        this.callInitiationId = stockSwapRequestModel ? stockSwapRequestModel.callInitiationId == undefined ? '' : stockSwapRequestModel.callInitiationId : '';
        this.swapStockCodeId = stockSwapRequestModel ? stockSwapRequestModel.swapStockCodeId == undefined ? '' : stockSwapRequestModel.swapStockCodeId : '';
        this.replaceStockCodeId = stockSwapRequestModel ? stockSwapRequestModel.replaceStockCodeId == undefined ? '' : stockSwapRequestModel.replaceStockCodeId : '';
        this.replaceStockCodeItemDescription = stockSwapRequestModel ? stockSwapRequestModel.replaceStockCodeItemDescription == undefined ? '' : stockSwapRequestModel.replaceStockCodeItemDescription : '';
        this.swapOutReason = stockSwapRequestModel ? stockSwapRequestModel.swapOutReason == undefined ? '' : stockSwapRequestModel.swapOutReason : '';
        this.contactPersonId = stockSwapRequestModel ? stockSwapRequestModel.contactPersonId == undefined ? '' : stockSwapRequestModel.contactPersonId : '';
        this.stockSwapStatusId = stockSwapRequestModel ? stockSwapRequestModel.stockSwapStatusId == undefined ? '' : stockSwapRequestModel.stockSwapStatusId : '';
        this.isApproval = stockSwapRequestModel ? stockSwapRequestModel.isApproval == undefined ? '' : stockSwapRequestModel.isApproval : '';
        this.isNotApproval = stockSwapRequestModel ? stockSwapRequestModel.isNotApproval == undefined ? '' : stockSwapRequestModel.isNotApproval : '';
        this.isCustomerAgreed = stockSwapRequestModel ? stockSwapRequestModel.isCustomerAgreed == undefined ? false : stockSwapRequestModel.isCustomerAgreed : false;
        this.isInternalApproval = stockSwapRequestModel ? stockSwapRequestModel.isInternalApproval == undefined ? false : stockSwapRequestModel.isInternalApproval : false;
        this.billofmaterialid = stockSwapRequestModel ? stockSwapRequestModel.billofmaterialid == undefined ? '' : stockSwapRequestModel.billofmaterialid : '';
        this.stockSwapStatus = stockSwapRequestModel ? stockSwapRequestModel.stockSwapStatus == undefined ? '' : stockSwapRequestModel.stockSwapStatus : '';
        this.contactPersonNumber = stockSwapRequestModel ? stockSwapRequestModel.contactPersonNumber == undefined ? '' : stockSwapRequestModel.contactPersonNumber : '';
        this.totalDiff = stockSwapRequestModel ? stockSwapRequestModel.totalDiff == undefined ? '' : stockSwapRequestModel.totalDiff : '';
        this.createdUserId = stockSwapRequestModel ? stockSwapRequestModel.createdUserId == undefined ? '' : stockSwapRequestModel.createdUserId : '';
        this.modifiedUserId = stockSwapRequestModel ? stockSwapRequestModel.modifiedUserId == undefined ? '' : stockSwapRequestModel.modifiedUserId : '';
        this.stockSwapItems = stockSwapRequestModel ? stockSwapRequestModel.stockSwapItems == undefined ? [] : stockSwapRequestModel.stockSwapItems : [];

    }
    stockSwapId?: string
    callInitiationId: string
    swapStockCodeId: string
    replaceStockCodeId: string
    replaceStockCodeItemDescription: string
    swapOutReason: string
    contactPersonId: string
    stockSwapStatusId: string
    isCustomerAgreed: boolean
    isInternalApproval: boolean
    isApproval: string;
    isNotApproval: string;
    billofmaterialid: string;
    stockSwapStatus: string;
    contactPersonNumber: string
    totalDiff: string
    createdUserId: string
    modifiedUserId: string
    stockSwapItems: StockSwapItemsListModel[];
}

class StockSwapItemsListModel {
    constructor(stockSwapItemsListModel: StockSwapItemsListModel) {
        this.stockSwapItemId = stockSwapItemsListModel ? stockSwapItemsListModel.stockSwapItemId == undefined ? '' : stockSwapItemsListModel.stockSwapItemId : '';
        this.stockSwapId = stockSwapItemsListModel ? stockSwapItemsListModel.stockSwapId == undefined ? '' : stockSwapItemsListModel.stockSwapId : '';
        this.actualItemId = stockSwapItemsListModel ? stockSwapItemsListModel.actualItemId == undefined ? '' : stockSwapItemsListModel.actualItemId : '';
        this.swapItemId = stockSwapItemsListModel ? stockSwapItemsListModel.swapItemId == undefined ? '' : stockSwapItemsListModel.swapItemId : '';
        this.differenceAmount = stockSwapItemsListModel ? stockSwapItemsListModel.differenceAmount == undefined ? null : stockSwapItemsListModel.differenceAmount : null;
        this.differencePercentage = stockSwapItemsListModel ? stockSwapItemsListModel.differencePercentage == undefined ? '' : stockSwapItemsListModel.differencePercentage : '';
        this.swapStockCode = stockSwapItemsListModel ? stockSwapItemsListModel.swapStockCode == undefined ? '' : stockSwapItemsListModel.swapStockCode : '';
        this.swapSellingPrice = stockSwapItemsListModel ? stockSwapItemsListModel.swapSellingPrice == undefined ? '' : stockSwapItemsListModel.swapSellingPrice : '';
        this.swapItemPrice = stockSwapItemsListModel ? stockSwapItemsListModel.swapItemPrice == undefined ? '' : stockSwapItemsListModel.swapItemPrice : '';
        this.actualStockCode = stockSwapItemsListModel ? stockSwapItemsListModel.actualStockCode == undefined ? '' : stockSwapItemsListModel.actualStockCode : '';
        this.actualSellingPrice = stockSwapItemsListModel ? stockSwapItemsListModel.actualSellingPrice == undefined ? '' : stockSwapItemsListModel.actualSellingPrice : '';
        this.actualItemPrice = stockSwapItemsListModel ? stockSwapItemsListModel.actualItemPrice == undefined ? '' : stockSwapItemsListModel.actualItemPrice : '';
        this.createdUserId = stockSwapItemsListModel ? stockSwapItemsListModel.createdUserId == undefined ? '' : stockSwapItemsListModel.createdUserId : '';
        this.modifiedUserId = stockSwapItemsListModel ? stockSwapItemsListModel.modifiedUserId == undefined ? '' : stockSwapItemsListModel.modifiedUserId : '';
    }
    stockSwapItemId: string;
    stockSwapId: string;
    actualItemId: string;
    swapItemId: string;
    differenceAmount: number;
    differencePercentage: string;
    swapStockCode: string;
    swapSellingPrice: string;
    swapItemPrice:string
    actualStockCode: string;
    actualSellingPrice: string;
    actualItemPrice:string
    createdUserId: string
    modifiedUserId: string
}

export { StockSwapRequestModel, StockSwapItemsListModel };

