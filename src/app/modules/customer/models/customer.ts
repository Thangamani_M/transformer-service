export class Customer {
  customerNumber: string;
  title: string;
  Name:string;
  customerType: string;
  industry: string;
  contactNumber: string;
  emailAddress: string;
  customerStatus:string;
  createdDate: Date;
  fullName?: string;
  status?: string;
}

export class StockManagement {
  StockCode:string;
  StockDescription:string;
  Warehouse:string;
  StorageLocation:string;
  ValuationClass:string;
  QtyOnHand:string;
  MovingAveragePrice:string;
  TotalValue:string;
}




