
class SplitPaymentsDebtors {
  constructor(splitPaymentsDebtors?: SplitPaymentsDebtors) {
    this.debtorToId = splitPaymentsDebtors ? splitPaymentsDebtors.debtorToId == undefined ? '' : splitPaymentsDebtors.debtorToId : '';
    this.splitPaymentAmount = splitPaymentsDebtors ? splitPaymentsDebtors.splitPaymentAmount == undefined ? 0 : splitPaymentsDebtors.splitPaymentAmount : 0;
    this.debtorFromId = splitPaymentsDebtors ? splitPaymentsDebtors.debtorFromId == undefined ? '' : splitPaymentsDebtors.debtorFromId : '';
  }

  debtorToId?: string;
  splitPaymentAmount?: number;
  debtorFromId?: string;
}


class SplitPaymentDocumentsPostDTO {
  constructor(splitPaymentDocumentsPostDTO?: SplitPaymentDocumentsPostDTO) {
    this.invoiceTransactionDescriptionId = splitPaymentDocumentsPostDTO ? splitPaymentDocumentsPostDTO.invoiceTransactionDescriptionId == undefined ? '' : splitPaymentDocumentsPostDTO.invoiceTransactionDescriptionId : '';
    this.documentName = splitPaymentDocumentsPostDTO ? splitPaymentDocumentsPostDTO.documentName == undefined ? '' : splitPaymentDocumentsPostDTO.documentName : '';
    this.documentPath = splitPaymentDocumentsPostDTO ? splitPaymentDocumentsPostDTO.documentPath == undefined ? '' : splitPaymentDocumentsPostDTO.documentPath : '';
    this.fileName = splitPaymentDocumentsPostDTO ? splitPaymentDocumentsPostDTO.fileName == undefined ? '' : splitPaymentDocumentsPostDTO.fileName : '';
    this.isRequired = splitPaymentDocumentsPostDTO ? splitPaymentDocumentsPostDTO.isRequired == undefined ? false : splitPaymentDocumentsPostDTO.isRequired : false;
  }

  invoiceTransactionDescriptionId?: string;
  documentName?: string;
  documentPath?: string;
  fileName?: string;
  isRequired?: boolean;
}

class SplitPaymentAddEditModel {
  constructor(splitPaymentAddEditModel?: SplitPaymentAddEditModel) {
    this.customerId = splitPaymentAddEditModel ? splitPaymentAddEditModel.customerId == undefined ? '' : splitPaymentAddEditModel.customerId : '';
    this.notes = splitPaymentAddEditModel ? splitPaymentAddEditModel.notes == undefined ? '' : splitPaymentAddEditModel.notes : '';
    this.debtorFromId = splitPaymentAddEditModel ? splitPaymentAddEditModel.debtorFromId == undefined ? '' : splitPaymentAddEditModel.debtorFromId : '';
    this.createdUserId = splitPaymentAddEditModel ? splitPaymentAddEditModel.createdUserId == undefined ? '' : splitPaymentAddEditModel.createdUserId : '';
    this.referenceId = splitPaymentAddEditModel ? splitPaymentAddEditModel.referenceId == undefined ? '' : splitPaymentAddEditModel.referenceId : '';
    this.invoiceTransactionDescriptionId = splitPaymentAddEditModel ? splitPaymentAddEditModel.invoiceTransactionDescriptionId == undefined ? '' : splitPaymentAddEditModel.invoiceTransactionDescriptionId : '';
    this.isSingle = splitPaymentAddEditModel ? splitPaymentAddEditModel.isSingle == undefined ? true : splitPaymentAddEditModel.isSingle : true;
    this.splitPaymentsDebtors = splitPaymentAddEditModel ? splitPaymentAddEditModel.splitPaymentsDebtors == undefined ? [] : splitPaymentAddEditModel.splitPaymentsDebtors : [];
    this.splitPaymentDocumentsPostDTO = splitPaymentAddEditModel ? splitPaymentAddEditModel.splitPaymentDocumentsPostDTO == undefined ? [] : splitPaymentAddEditModel.splitPaymentDocumentsPostDTO : [];
  }

  customerId?: string;
  notes?: string;
  referenceId?: string;
  createdUserId?: string;
  debtorFromId?: string;
  isSingle?: boolean;
  invoiceTransactionDescriptionId?: string;
  splitPaymentsDebtors?: SplitPaymentsDebtors[]
  splitPaymentDocumentsPostDTO?: SplitPaymentDocumentsPostDTO[]
}
export { SplitPaymentsDebtors, SplitPaymentDocumentsPostDTO, SplitPaymentAddEditModel }


