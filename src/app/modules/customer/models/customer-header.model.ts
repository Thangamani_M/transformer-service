class CustomerHeaderModel {
    constructor(customerHeaderModel?: CustomerHeaderModel) {
        this.customerId = customerHeaderModel ? customerHeaderModel.customerId == undefined ? "" : customerHeaderModel.customerId : "";
        this.customerName = customerHeaderModel ? customerHeaderModel.customerName == undefined ? "" : customerHeaderModel.customerName : "";
        this.customerRefNo = customerHeaderModel ? customerHeaderModel.customerRefNo == undefined ? "" : customerHeaderModel.customerRefNo : "";
        this.customerTypeName = customerHeaderModel ? customerHeaderModel.customerTypeName == undefined ? "" : customerHeaderModel.customerTypeName : "";
        this.divisionName = customerHeaderModel ? customerHeaderModel.divisionName == undefined ? "" : customerHeaderModel.divisionName : "";
        this.siteId = customerHeaderModel ? customerHeaderModel.siteId == undefined ? "" : customerHeaderModel.siteId : "";
        this.siteType = customerHeaderModel ? customerHeaderModel.siteType == undefined ? "" : customerHeaderModel.siteType : "";
        this.status = customerHeaderModel ? customerHeaderModel.status == undefined ? "" : customerHeaderModel.status : "";
        this.mobileNumber1 = customerHeaderModel ? customerHeaderModel.mobileNumber1 == undefined ? "" : customerHeaderModel.mobileNumber1 : "";
        this.addressId = customerHeaderModel ? customerHeaderModel.addressId == undefined ? "" : customerHeaderModel.addressId : "";
        this.fullAddress = customerHeaderModel ? customerHeaderModel.fullAddress == undefined ? "" : customerHeaderModel.fullAddress : "";
        this.siteTypeName = customerHeaderModel ? customerHeaderModel.siteTypeName == undefined ? "" : customerHeaderModel.siteTypeName : "";
        this.contractId = customerHeaderModel ? customerHeaderModel.contractId == undefined ? "" : customerHeaderModel.contractId : "";
        this.contractName = customerHeaderModel ? customerHeaderModel.contractName == undefined ? "" : customerHeaderModel.contractName : "";
        this.email = customerHeaderModel ? customerHeaderModel.email == undefined ? "" : customerHeaderModel.email : "";
        this.officeNo = customerHeaderModel ? customerHeaderModel.officeNo == undefined ? "" : customerHeaderModel.officeNo : "";
        this.premisesNo = customerHeaderModel ? customerHeaderModel.premisesNo == undefined ? "" : customerHeaderModel.premisesNo : "";
        this.taxPrice = customerHeaderModel ? customerHeaderModel.taxPrice == undefined ? 0 : customerHeaderModel.taxPrice : 0;
        this.allowEdit = customerHeaderModel ? customerHeaderModel.allowEdit == undefined ? true : customerHeaderModel.allowEdit : true;
    }
    customerId?: string;
    customerName?: string;
    customerRefNo?: string;
    customerTypeName?: string;
    divisionName?: string;
    siteId?: string;
    siteType?: string;
    status?: string;
    mobileNumber1?: string;
    mobileNumber2?: string;
    addressId?: string;
    fullAddress?: string;
    contractId?: string;
    siteTypeName?: string;
    contractName?: string;
    email?: string;
    officeNo?: string;
    premisesNo?: string;
    taxPrice?: number;
    allowEdit?: boolean;
}
export { CustomerHeaderModel }
