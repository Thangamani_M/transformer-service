class CustomerClassificationModel{
    constructor(customerClassificationModel?:CustomerClassificationModel){
        this.customerClassificationReferenceTableId=customerClassificationModel?customerClassificationModel.customerClassificationReferenceTableId==undefined?'':customerClassificationModel.customerClassificationReferenceTableId:'';
        this.customerClassificationReferenceData=customerClassificationModel?customerClassificationModel.customerClassificationReferenceData==undefined?'':customerClassificationModel.customerClassificationReferenceData:'';
        this.precedingCategoryId=customerClassificationModel?customerClassificationModel.precedingCategoryId==undefined?0:customerClassificationModel.precedingCategoryId:0;
        this.dataType=customerClassificationModel?customerClassificationModel.dataType==undefined?'':customerClassificationModel.dataType:'';
        this.categoryName=customerClassificationModel?customerClassificationModel.categoryName==undefined?'':customerClassificationModel.categoryName:'';
        this.description=customerClassificationModel?customerClassificationModel.description==undefined?'':customerClassificationModel.description:'';
        this.createdUserId=customerClassificationModel?customerClassificationModel.createdUserId==undefined?'':customerClassificationModel.createdUserId:'';
        this.isManualClassification=customerClassificationModel?customerClassificationModel.isManualClassification==undefined?false:customerClassificationModel.isManualClassification:false;
        this.categoryId=customerClassificationModel?customerClassificationModel.categoryId==undefined?'':customerClassificationModel.categoryId:'';
        this.isDealerSpecific =customerClassificationModel?customerClassificationModel.isDealerSpecific==undefined?false:customerClassificationModel.isDealerSpecific:false;
    }
    customerClassificationReferenceTableId: string;
    customerClassificationReferenceData: string;
    precedingCategoryId: number;
    dataType: string;
    categoryName: string;
    description: string;
    isDealerSpecific:boolean;
    isManualClassification: boolean;
    createdUserId: string;
    categoryId: string;
}

class CustomerSubCategoryClassificationModel{
    constructor(customerSubCategoryClassificationModel?:CustomerSubCategoryClassificationModel){
        this.customerClassificationReferenceTableId=customerSubCategoryClassificationModel?customerSubCategoryClassificationModel.customerClassificationReferenceTableId==undefined?'':customerSubCategoryClassificationModel.customerClassificationReferenceTableId:'';
        this.customerClassificationReferenceData=customerSubCategoryClassificationModel?customerSubCategoryClassificationModel.customerClassificationReferenceData==undefined?'':customerSubCategoryClassificationModel.customerClassificationReferenceData:'';
        this.subCategoryName=customerSubCategoryClassificationModel?customerSubCategoryClassificationModel.subCategoryName==undefined?'':customerSubCategoryClassificationModel.subCategoryName:'';
        this.description=customerSubCategoryClassificationModel?customerSubCategoryClassificationModel.description==undefined?'':customerSubCategoryClassificationModel.description:'';
        this.createdUserId=customerSubCategoryClassificationModel?customerSubCategoryClassificationModel.createdUserId==undefined?'':customerSubCategoryClassificationModel.createdUserId:'';
        this.modifiedUserId=customerSubCategoryClassificationModel?customerSubCategoryClassificationModel.modifiedUserId==undefined?'':customerSubCategoryClassificationModel.modifiedUserId:'';
        this.isManualClassification=customerSubCategoryClassificationModel?customerSubCategoryClassificationModel.isManualClassification==undefined?false:customerSubCategoryClassificationModel.isManualClassification:false;
        this.subCategoryId=customerSubCategoryClassificationModel?customerSubCategoryClassificationModel.subCategoryId==undefined?'':customerSubCategoryClassificationModel.subCategoryId:'';
        this.isDealerSpecific =customerSubCategoryClassificationModel?customerSubCategoryClassificationModel.isDealerSpecific==undefined?false:customerSubCategoryClassificationModel.isDealerSpecific:false;
    }
    customerClassificationReferenceTableId: string;
    customerClassificationReferenceData: string;
    subCategoryName: string;
    description: string;
    isDealerSpecific:boolean;
    isManualClassification: boolean;
    createdUserId: string;
    modifiedUserId: string;
    subCategoryId: string;
}

    class CustomerOriginClassificationModel{
        constructor(customerOriginClassificationModel?:CustomerOriginClassificationModel){
            this.customerClassificationReferenceTableId=customerOriginClassificationModel?customerOriginClassificationModel.customerClassificationReferenceTableId==undefined?'':customerOriginClassificationModel.customerClassificationReferenceTableId:'';
            this.customerClassificationReferenceData=customerOriginClassificationModel?customerOriginClassificationModel.customerClassificationReferenceData==undefined?'':customerOriginClassificationModel.customerClassificationReferenceData:'';
            this.originName=customerOriginClassificationModel?customerOriginClassificationModel.originName==undefined?'':customerOriginClassificationModel.originName:'';
            this.description=customerOriginClassificationModel?customerOriginClassificationModel.description==undefined?'':customerOriginClassificationModel.description:'';
            this.createdUserId=customerOriginClassificationModel?customerOriginClassificationModel.createdUserId==undefined?'':customerOriginClassificationModel.createdUserId:'';
            this.modifiedUserId=customerOriginClassificationModel?customerOriginClassificationModel.modifiedUserId==undefined?'':customerOriginClassificationModel.modifiedUserId:'';
            this.isManualClassification=customerOriginClassificationModel?customerOriginClassificationModel.isManualClassification==undefined?false:customerOriginClassificationModel.isManualClassification:false;
            this.originId=customerOriginClassificationModel?customerOriginClassificationModel.originId==undefined?'':customerOriginClassificationModel.originId:'';
            this.precedingOriginId=customerOriginClassificationModel?customerOriginClassificationModel.precedingOriginId==undefined?0:customerOriginClassificationModel.precedingOriginId:0;
            this.dealType=customerOriginClassificationModel?customerOriginClassificationModel.dealType==undefined?'':customerOriginClassificationModel.dealType:'';
            this.isDealerSpecific =customerOriginClassificationModel?customerOriginClassificationModel.isDealerSpecific==undefined?false:customerOriginClassificationModel.isDealerSpecific:false;
        }
        customerClassificationReferenceTableId: string;
        customerClassificationReferenceData: string;
        precedingOriginId: number;
        originName: string;
        description: string;
        isDealerSpecific:boolean;
        isManualClassification: boolean;
        createdUserId: string;
        modifiedUserId: string;
        originId: string;
        dealType: string;

}
class CustomerDebtorGroupClassificationModel{
    constructor(customerDebtorGroupClassificationModel?:CustomerDebtorGroupClassificationModel){
        this.customerClassificationReferenceTableId=customerDebtorGroupClassificationModel?customerDebtorGroupClassificationModel.customerClassificationReferenceTableId==undefined?'':customerDebtorGroupClassificationModel.customerClassificationReferenceTableId:'';
        this.customerClassificationReferenceData=customerDebtorGroupClassificationModel?customerDebtorGroupClassificationModel.customerClassificationReferenceData==undefined?'':customerDebtorGroupClassificationModel.customerClassificationReferenceData:'';
        this.debtorGroupName=customerDebtorGroupClassificationModel?customerDebtorGroupClassificationModel.debtorGroupName==undefined?'':customerDebtorGroupClassificationModel.debtorGroupName:'';
        this.description=customerDebtorGroupClassificationModel?customerDebtorGroupClassificationModel.description==undefined?'':customerDebtorGroupClassificationModel.description:'';
        this.createdUserId=customerDebtorGroupClassificationModel?customerDebtorGroupClassificationModel.createdUserId==undefined?'':customerDebtorGroupClassificationModel.createdUserId:'';
        this.modifiedUserId=customerDebtorGroupClassificationModel?customerDebtorGroupClassificationModel.modifiedUserId==undefined?'':customerDebtorGroupClassificationModel.modifiedUserId:'';
        this.isManualClassification=customerDebtorGroupClassificationModel?customerDebtorGroupClassificationModel.isManualClassification==undefined?false:customerDebtorGroupClassificationModel.isManualClassification:false;
        this.debtorGroupId=customerDebtorGroupClassificationModel?customerDebtorGroupClassificationModel.debtorGroupId==undefined?'':customerDebtorGroupClassificationModel.debtorGroupId:'';
        this.precedingOriginId=customerDebtorGroupClassificationModel?customerDebtorGroupClassificationModel.precedingOriginId==undefined?'':customerDebtorGroupClassificationModel.precedingOriginId:'';
        this.isDealerSpecific =customerDebtorGroupClassificationModel?customerDebtorGroupClassificationModel.isDealerSpecific==undefined?false:customerDebtorGroupClassificationModel.isDealerSpecific:false;
    }
    customerClassificationReferenceTableId: string;
    customerClassificationReferenceData: string;
    precedingOriginId: string;
    debtorGroupName: string;
    description: string;
    isDealerSpecific:boolean;
    isManualClassification: boolean;
    createdUserId: string;
    modifiedUserId: string;
    debtorGroupId: string;
}
export { CustomerClassificationModel, CustomerSubCategoryClassificationModel, CustomerOriginClassificationModel, CustomerDebtorGroupClassificationModel };

