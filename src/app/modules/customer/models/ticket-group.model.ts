
class TicketGroupAddEditModel {
    constructor(billingFinancialYearConfigurationAddEditModel?: TicketGroupAddEditModel) {
        this.ticketGroupId = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.ticketGroupId == undefined ? '' : billingFinancialYearConfigurationAddEditModel.ticketGroupId : '';
        this.ticketGroupName = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.ticketGroupName == undefined ? null : billingFinancialYearConfigurationAddEditModel.ticketGroupName : null;
        this.description = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.description == undefined ? null : billingFinancialYearConfigurationAddEditModel.description : null;
        this.isActive = billingFinancialYearConfigurationAddEditModel ? billingFinancialYearConfigurationAddEditModel.isActive == undefined ? null : billingFinancialYearConfigurationAddEditModel.isActive : null;
    }
    ticketGroupId?: string
    ticketGroupName?: string;
    description?: Date;    
    isActive: string;
}
export { TicketGroupAddEditModel }