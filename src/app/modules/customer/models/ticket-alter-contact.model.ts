class TicketAlterContactDetailModel {
    email?: string;
    contactNumber?: string;
    contactNumberCountryCode?: string;
    emails?: TicketEmailArrayDetailsModel[];
    contacts?: TicketContactArrayDetailsModel[];
    constructor(ticketAlterContactDetailModel: TicketAlterContactDetailModel) {
        this.email = ticketAlterContactDetailModel?.email == undefined ? '' : ticketAlterContactDetailModel?.email;
        this.contactNumber = ticketAlterContactDetailModel?.contactNumber == undefined ? '' : ticketAlterContactDetailModel?.contactNumber;
        this.contactNumberCountryCode = ticketAlterContactDetailModel?.contactNumberCountryCode == undefined ? '+27' : ticketAlterContactDetailModel?.contactNumberCountryCode;
        this.emails = ticketAlterContactDetailModel?.emails == undefined ? [] : ticketAlterContactDetailModel?.emails;
        this.contacts = ticketAlterContactDetailModel?.contacts == undefined ? [] : ticketAlterContactDetailModel?.contacts;
    }
}

class TicketContactArrayDetailsModel {
    customerAlternateContactId?: string;
    contactNumber?: string;
    contactNumberCountryCode?: string;
    constructor(ticketContactArrayDetailsModel: TicketContactArrayDetailsModel) {
        this.customerAlternateContactId = ticketContactArrayDetailsModel?.customerAlternateContactId == undefined ? '' : ticketContactArrayDetailsModel?.customerAlternateContactId;
        this.contactNumber = ticketContactArrayDetailsModel?.contactNumber == undefined ? '' : ticketContactArrayDetailsModel?.contactNumber;
        this.contactNumberCountryCode = ticketContactArrayDetailsModel?.contactNumberCountryCode == undefined ? '' : ticketContactArrayDetailsModel?.contactNumberCountryCode;
    }
}

class TicketEmailArrayDetailsModel {
    customerAlternateContactId?: string;
    email?: string;
    constructor(ticketEmailArrayDetailsModel: TicketEmailArrayDetailsModel) {
        this.customerAlternateContactId = ticketEmailArrayDetailsModel?.customerAlternateContactId == undefined ? '' : ticketEmailArrayDetailsModel?.customerAlternateContactId;
        this.email = ticketEmailArrayDetailsModel?.email == undefined ? '' : ticketEmailArrayDetailsModel?.email;
    }
}

export { TicketAlterContactDetailModel, TicketContactArrayDetailsModel, TicketEmailArrayDetailsModel };
