import { RateServiceQuestionsModel } from "@modules/technical-management/models/no-contacts.model";

class PrCallApprovalModel {
    constructor(prCallApprovalModel?: PrCallApprovalModel) {
        this.serviceCallRequestId = prCallApprovalModel ? prCallApprovalModel.serviceCallRequestId == undefined ? '' : prCallApprovalModel.serviceCallRequestId : '';
        this.serviceCallApprovalId = prCallApprovalModel ? prCallApprovalModel.serviceCallApprovalId == undefined ? '' : prCallApprovalModel.serviceCallApprovalId : '';
        this.approvedById = prCallApprovalModel ? prCallApprovalModel.approvedById == undefined ? '' : prCallApprovalModel.approvedById : '';
        this.approvalComments = prCallApprovalModel ? prCallApprovalModel.approvalComments == undefined ? '' : prCallApprovalModel.approvalComments : '';
        this.serviceCallApprovalStatusId = prCallApprovalModel ? prCallApprovalModel.serviceCallApprovalStatusId == undefined ? '' : prCallApprovalModel.serviceCallApprovalStatusId : '';
        this.isOfficeHours = prCallApprovalModel ? prCallApprovalModel.isOfficeHours == undefined ? false : prCallApprovalModel.isOfficeHours : false;
        this.isAfterHours = prCallApprovalModel ? prCallApprovalModel.isAfterHours == undefined ? false : prCallApprovalModel.isAfterHours : false;
        this.callOutFee = prCallApprovalModel ? prCallApprovalModel.callOutFee == undefined ? '' : prCallApprovalModel.callOutFee : '';
        this.createdUserId = prCallApprovalModel ? prCallApprovalModel.createdUserId == undefined ? '' : prCallApprovalModel.createdUserId : '';
        this.modifiedUserId = prCallApprovalModel ? prCallApprovalModel.modifiedUserId == undefined ? '' : prCallApprovalModel.modifiedUserId : '';
    }
    serviceCallRequestId?: string
    serviceCallApprovalId: string
    // roleId: string
    callOutFee: string;
    isOfficeHours: boolean
    isAfterHours: boolean
    approvedById: string
    approvalComments: string
    serviceCallApprovalStatusId: string
    createdUserId: string
    modifiedUserId: string
}

class EquipmentModel {
    constructor(prCallApprovalModel?: EquipmentModel) {
        this.stockCodeSearch = prCallApprovalModel ? prCallApprovalModel.stockCodeSearch == undefined ? '' : prCallApprovalModel.stockCodeSearch : '';
        this.equipmentList = prCallApprovalModel ? prCallApprovalModel.equipmentList == undefined ? [] : prCallApprovalModel.equipmentList : [];
        this.totalDiff = prCallApprovalModel ? prCallApprovalModel.totalDiff == undefined ? null : prCallApprovalModel.totalDiff : null;
    }
    stockCodeSearch: string
    equipmentList: EquipmentListModel[]
    totalDiff: number
}


class EquipmentListModel {
    constructor(prCallApprovalModel?: EquipmentListModel) {
        this.serviceCallRequestItemId = prCallApprovalModel ? prCallApprovalModel.serviceCallRequestItemId == undefined ? '' : prCallApprovalModel.serviceCallRequestItemId : '';
        this.serviceCallRequestId = prCallApprovalModel ? prCallApprovalModel.serviceCallRequestId == undefined ? '' : prCallApprovalModel.serviceCallRequestId : '';
        this.itemPricingConfigId = prCallApprovalModel ? prCallApprovalModel.itemPricingConfigId == undefined ? '' : prCallApprovalModel.itemPricingConfigId : '';
        this.itemId = prCallApprovalModel ? prCallApprovalModel.itemId == undefined ? '' : prCallApprovalModel.itemId : '';
        this.exclVAT = prCallApprovalModel ? prCallApprovalModel.exclVAT == undefined ? '' : prCallApprovalModel.exclVAT : '';
        this.exclVATFormat = prCallApprovalModel ? prCallApprovalModel.exclVATFormat == undefined ? '' : prCallApprovalModel.exclVATFormat : '';
        this.exclVATStatic = prCallApprovalModel ? prCallApprovalModel.exclVATStatic == undefined ? '' : prCallApprovalModel.exclVATStatic : '';
        this.inclVAT = prCallApprovalModel ? prCallApprovalModel.inclVAT == undefined ? '' : prCallApprovalModel.inclVAT : '';
        this.inclVATFormat = prCallApprovalModel ? prCallApprovalModel.inclVATFormat == undefined ? '' : prCallApprovalModel.inclVATFormat : '';
        this.qtyRequired = prCallApprovalModel ? prCallApprovalModel.qtyRequired == undefined ? '' : prCallApprovalModel.qtyRequired : '';
        this.itemCode = prCallApprovalModel ? prCallApprovalModel.itemCode == undefined ? '' : prCallApprovalModel.itemCode : '';
        this.itemName = prCallApprovalModel ? prCallApprovalModel.itemName == undefined ? '' : prCallApprovalModel.itemName : '';
        this.stockItem = prCallApprovalModel ? prCallApprovalModel.stockItem == undefined ? '' : prCallApprovalModel.stockItem : '';
        this.stockType = prCallApprovalModel ? prCallApprovalModel.stockType == undefined ? '' : prCallApprovalModel.stockType : '';
        this.uomName = prCallApprovalModel ? prCallApprovalModel.uomName == undefined ? '' : prCallApprovalModel.uomName : '';
        this.qtyRequired = prCallApprovalModel ? prCallApprovalModel.qtyRequired == undefined ? '' : prCallApprovalModel.qtyRequired : '';
        this.createdUserId = prCallApprovalModel ? prCallApprovalModel.createdUserId == undefined ? '' : prCallApprovalModel.createdUserId : '';
        this.modifiedUserId = prCallApprovalModel ? prCallApprovalModel.modifiedUserId == undefined ? '' : prCallApprovalModel.modifiedUserId : '';
    }
    serviceCallRequestItemId?: string
    serviceCallRequestId: string
    itemPricingConfigId?: string
    itemId: string
    exclVAT: string
    exclVATFormat: string
    exclVATStatic: string
    inclVAT: string
    inclVATFormat: string
    qtyRequired: string
    itemCode: string
    itemName: string
    stockItem: string
    stockType: string
    uomName: string
    createdUserId: string
    modifiedUserId: string
}

class RateOurServiceDialogFormModel {
    constructor(rateOurServiceDialogFormModel?: RateOurServiceDialogFormModel) {
        this.ContactId = rateOurServiceDialogFormModel ? rateOurServiceDialogFormModel.ContactId == undefined ? '' : rateOurServiceDialogFormModel.ContactId : '';
        this.ContactNo = rateOurServiceDialogFormModel ? rateOurServiceDialogFormModel.ContactNo == undefined ? '' : rateOurServiceDialogFormModel.ContactNo : '';
        this.FollowUpDate = rateOurServiceDialogFormModel ? rateOurServiceDialogFormModel.FollowUpDate == undefined ? '' : rateOurServiceDialogFormModel.FollowUpDate : '';
        this.RateOurServiceFollowUpStatusId = rateOurServiceDialogFormModel ? rateOurServiceDialogFormModel.RateOurServiceFollowUpStatusId == undefined ? '' : rateOurServiceDialogFormModel.RateOurServiceFollowUpStatusId : '';
        this.Comments = rateOurServiceDialogFormModel ? rateOurServiceDialogFormModel.Comments == undefined ? '' : rateOurServiceDialogFormModel.Comments : '';
    }
    ContactId?: string;
    ContactNo: string;
    FollowUpDate: string;
    RateOurServiceFollowUpStatusId: string;
    Comments: string;
}

class ViewRatingListModel {
    rateServiceQuestionsArray: Array<RateServiceQuestionsModel>;
    constructor(viewRatingListModel?: ViewRatingListModel) {
        this.rateServiceQuestionsArray = viewRatingListModel ? viewRatingListModel.rateServiceQuestionsArray == undefined ? [] : viewRatingListModel.rateServiceQuestionsArray : [];
    }
}

class VideoConfigDialogFormModel {
    constructor(videoConfigDialogFormModel?: VideoConfigDialogFormModel) {
        this.videofiedSimStatusId = videoConfigDialogFormModel ? videoConfigDialogFormModel.videofiedSimStatusId == undefined ? '' : videoConfigDialogFormModel.videofiedSimStatusId : '';
    }
    videofiedSimStatusId: string;
}

class VideoFollowUpDialogFormModel {
    constructor(videoFollowUpDialogFormModel?: VideoFollowUpDialogFormModel) {
        this.VideofiedSimFollowUpStatusId = videoFollowUpDialogFormModel ? videoFollowUpDialogFormModel.VideofiedSimFollowUpStatusId == undefined ? '' : videoFollowUpDialogFormModel.VideofiedSimFollowUpStatusId : '';
    }
    VideofiedSimFollowUpStatusId: string;
}

class PaymentMethodDialogFormModel {
    constructor(paymentMethodDialogFormModel?: PaymentMethodDialogFormModel) {
        this.invoiceId = paymentMethodDialogFormModel ? paymentMethodDialogFormModel.invoiceId == undefined ? '' : paymentMethodDialogFormModel.invoiceId : '';
        this.paymentMethodId = paymentMethodDialogFormModel ? paymentMethodDialogFormModel.paymentMethodId == undefined ? '' : paymentMethodDialogFormModel.paymentMethodId : '';
        this.debitDate = paymentMethodDialogFormModel ? paymentMethodDialogFormModel.debitDate == undefined ? '' : paymentMethodDialogFormModel.debitDate : '';
        this.modifiedUserId = paymentMethodDialogFormModel ? paymentMethodDialogFormModel.modifiedUserId == undefined ? '' : paymentMethodDialogFormModel.modifiedUserId : '';
    }
    invoiceId: string;
    paymentMethodId: string;
    debitDate: string;
    modifiedUserId: string;
}

class TamAppointmentFormModel {
    constructor(tamAppointmentFormModel?: TamAppointmentFormModel) {
        this.saleOrderTAMCommentId = tamAppointmentFormModel ? tamAppointmentFormModel.saleOrderTAMCommentId == undefined ? '' : tamAppointmentFormModel.saleOrderTAMCommentId : '';
        this.saleOrderId = tamAppointmentFormModel ? tamAppointmentFormModel.saleOrderId == undefined ? '' : tamAppointmentFormModel.saleOrderId : '';
        this.isUrgent = tamAppointmentFormModel ? tamAppointmentFormModel.isUrgent == undefined ? undefined : tamAppointmentFormModel.isUrgent : undefined;
        this.isCode50 = tamAppointmentFormModel ? tamAppointmentFormModel.isCode50 == undefined ? undefined : tamAppointmentFormModel.isCode50 : undefined;
        this.saleOrderTAMCommentNumber = tamAppointmentFormModel ? tamAppointmentFormModel.saleOrderTAMCommentNumber == undefined ? '' : tamAppointmentFormModel.saleOrderTAMCommentNumber : '';
        this.sellerComments = tamAppointmentFormModel ? tamAppointmentFormModel.sellerComments == undefined ? '' : tamAppointmentFormModel.sellerComments : '';
        this.feedBack = tamAppointmentFormModel ? tamAppointmentFormModel.feedBack == undefined ? '' : tamAppointmentFormModel.feedBack : '';
        this.appointmentDate = tamAppointmentFormModel ? tamAppointmentFormModel.appointmentDate == undefined ? '' : tamAppointmentFormModel.appointmentDate : '';
        this.preferredDate = tamAppointmentFormModel ? tamAppointmentFormModel.preferredDate == undefined ? '' : tamAppointmentFormModel.preferredDate : '';
        this.technicalArea = tamAppointmentFormModel ? tamAppointmentFormModel.technicalArea == undefined ? '' : tamAppointmentFormModel.technicalArea : '';
        this.isSpecialAppointment = tamAppointmentFormModel ? tamAppointmentFormModel.isSpecialAppointment == undefined ? undefined : tamAppointmentFormModel.isSpecialAppointment : undefined;
        this.quotationNumber = tamAppointmentFormModel ? tamAppointmentFormModel.quotationNumber == undefined ? '' : tamAppointmentFormModel.quotationNumber : '';
        this.techinicianAreaManager = tamAppointmentFormModel ? tamAppointmentFormModel.techinicianAreaManager == undefined ? '' : tamAppointmentFormModel.techinicianAreaManager : '';
        this.quotationVersionId = tamAppointmentFormModel ? tamAppointmentFormModel.quotationVersionId == undefined ? '' : tamAppointmentFormModel.quotationVersionId : '';
        this.userId = tamAppointmentFormModel ? tamAppointmentFormModel.userId == undefined ? '' : tamAppointmentFormModel.userId : '';
        this.appointmentNos = tamAppointmentFormModel ? tamAppointmentFormModel.appointmentNos == undefined ? '' : tamAppointmentFormModel.appointmentNos : '';
        this.serviceCallNos = tamAppointmentFormModel ? tamAppointmentFormModel.serviceCallNos == undefined ? '' : tamAppointmentFormModel.serviceCallNos : '';
        this.noofhours = tamAppointmentFormModel ? tamAppointmentFormModel.noofhours == undefined ? '' : tamAppointmentFormModel.noofhours : '';
    }
    saleOrderTAMCommentId?: string;
    saleOrderId: string;
    isUrgent: boolean;
    isCode50: boolean;
    saleOrderTAMCommentNumber: string;
    sellerComments: string;
    feedBack: string;
    appointmentDate: string;
    preferredDate: string;
    technicalArea: string;
    quotationNumber: string;
    isSpecialAppointment: boolean;
    techinicianAreaManager: string;
    quotationVersionId: string;
    userId: string;
    appointmentNos: string;
    serviceCallNos: string;
    noofhours?: string;
}

class PsiraExpiryAlertDialogFormModel {
    constructor(psiraExpiryAlertDialogFormModel?: PsiraExpiryAlertDialogFormModel) {
        this.psiraRequestApprovalId = psiraExpiryAlertDialogFormModel ? psiraExpiryAlertDialogFormModel.psiraRequestApprovalId == undefined ? '' : psiraExpiryAlertDialogFormModel.psiraRequestApprovalId : '';
        this.employeeId = psiraExpiryAlertDialogFormModel ? psiraExpiryAlertDialogFormModel.employeeId == undefined ? '' : psiraExpiryAlertDialogFormModel.employeeId : '';
        this.psiraExpiryDate = psiraExpiryAlertDialogFormModel ? psiraExpiryAlertDialogFormModel.psiraExpiryDate == undefined ? '' : psiraExpiryAlertDialogFormModel.psiraExpiryDate : '';
        this.psiraDocument = psiraExpiryAlertDialogFormModel ? psiraExpiryAlertDialogFormModel.psiraDocument == undefined ? '' : psiraExpiryAlertDialogFormModel.psiraDocument : '';
        this.modifiedUserId = psiraExpiryAlertDialogFormModel ? psiraExpiryAlertDialogFormModel.modifiedUserId == undefined ? '' : psiraExpiryAlertDialogFormModel.modifiedUserId : '';
    }
    psiraRequestApprovalId: string;
    employeeId: string;
    psiraExpiryDate: string;
    psiraDocument: string;
    modifiedUserId: string;
}

class CallOnHoldDialogFormModel {
    constructor(callOnHoldDialogFormModel?: CallOnHoldDialogFormModel) {
        this.taskListId = callOnHoldDialogFormModel ? callOnHoldDialogFormModel.taskListId == undefined ? '' : callOnHoldDialogFormModel.taskListId : '';
        this.callInitiationOnholdRequestApprovalId = callOnHoldDialogFormModel ? callOnHoldDialogFormModel.callInitiationOnholdRequestApprovalId == undefined ? '' : callOnHoldDialogFormModel.callInitiationOnholdRequestApprovalId : '';
        this.onHoldReason = callOnHoldDialogFormModel ? callOnHoldDialogFormModel.onHoldReason == undefined ? '' : callOnHoldDialogFormModel.onHoldReason : '';
        this.callInitiationId = callOnHoldDialogFormModel ? callOnHoldDialogFormModel.callInitiationId == undefined ? '' : callOnHoldDialogFormModel.callInitiationId : '';
        this.onHoldFollowUpDate = callOnHoldDialogFormModel ? callOnHoldDialogFormModel.onHoldFollowUpDate == undefined ? '' : callOnHoldDialogFormModel.onHoldFollowUpDate : '';
        this.comments = callOnHoldDialogFormModel ? callOnHoldDialogFormModel.comments == undefined ? '' : callOnHoldDialogFormModel.comments : '';
        this.createdUserId = callOnHoldDialogFormModel ? callOnHoldDialogFormModel.createdUserId == undefined ? '' : callOnHoldDialogFormModel.createdUserId : '';
    }
    taskListId: string;
    callInitiationOnholdRequestApprovalId: string;
    onHoldReason: string;
    callInitiationId: string;
    onHoldFollowUpDate: string;
    comments: string;
    createdUserId: string;
}

export {
    PrCallApprovalModel, EquipmentModel, EquipmentListModel, RateOurServiceDialogFormModel, ViewRatingListModel,
    VideoConfigDialogFormModel, VideoFollowUpDialogFormModel, TamAppointmentFormModel, PaymentMethodDialogFormModel, 
    PsiraExpiryAlertDialogFormModel, CallOnHoldDialogFormModel,
};

