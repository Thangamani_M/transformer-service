class SiteCustomCallFlowModel {
    constructor(siteCustomCallFlowModel?: SiteCustomCallFlowModel) {
        this.createdUserId=siteCustomCallFlowModel?siteCustomCallFlowModel.createdUserId==undefined? '':siteCustomCallFlowModel.createdUserId:'';
        this.customerPreferenceSiteCustomCallFlowId=siteCustomCallFlowModel?siteCustomCallFlowModel.customerPreferenceSiteCustomCallFlowId==undefined?'':siteCustomCallFlowModel.customerPreferenceSiteCustomCallFlowId:'';
        this.customerAddressId=siteCustomCallFlowModel?siteCustomCallFlowModel.customerAddressId==undefined?'':siteCustomCallFlowModel.customerAddressId:'';
        this.customerId=siteCustomCallFlowModel?siteCustomCallFlowModel.customerId==undefined?'':siteCustomCallFlowModel.customerId:'';

        this.isSignals=siteCustomCallFlowModel?siteCustomCallFlowModel.isSignals==undefined?false:siteCustomCallFlowModel.isSignals:false;
        this.isDays=siteCustomCallFlowModel?siteCustomCallFlowModel.isDays==undefined?false:siteCustomCallFlowModel.isDays:false;

        this.isTimeRange=siteCustomCallFlowModel?siteCustomCallFlowModel.isTimeRange==undefined?false:siteCustomCallFlowModel.isTimeRange:false;
        this.description=siteCustomCallFlowModel?siteCustomCallFlowModel.description==undefined?'':siteCustomCallFlowModel.description:'';
        this.timeFrom=siteCustomCallFlowModel?siteCustomCallFlowModel.timeFrom==undefined?'':siteCustomCallFlowModel.timeFrom:'';
        this.timeTo=siteCustomCallFlowModel?siteCustomCallFlowModel.timeTo==undefined?'':siteCustomCallFlowModel.timeTo:'';

        this.isActive=siteCustomCallFlowModel?siteCustomCallFlowModel.isActive==undefined?true:siteCustomCallFlowModel.isActive:true;
        this.dayList=siteCustomCallFlowModel?siteCustomCallFlowModel.dayList==undefined?null:siteCustomCallFlowModel.dayList:null;
        this.alarmTypeList=siteCustomCallFlowModel?siteCustomCallFlowModel.alarmTypeList==undefined?null:siteCustomCallFlowModel.alarmTypeList:null;
        this.keyHolderList=siteCustomCallFlowModel?siteCustomCallFlowModel.keyHolderList==undefined?null:siteCustomCallFlowModel.keyHolderList:null;

    }
   createdUserId: any;
   customerPreferenceSiteCustomCallFlowId: string;
   customerAddressId: string;
   customerId: string;
   isSignals: boolean;
   isDays: boolean;
   isTimeRange: boolean;
   description:string;
   timeFrom:string;
   timeTo:string;
   isActive:boolean;
   dayList: CustomerPreferenceSiteCustomFlowDayListModel[];
   alarmTypeList: CustomerPreferenceSiteCustomFlowSignalListModel[];
   keyHolderList:CustomerPreferenceSiteCustomFlowKeyHolderListModel[];
}

class CustomerPreferenceSiteCustomFlowDayListModel {
  constructor(customerPreferenceSiteCustomFlowDayListModel?: CustomerPreferenceSiteCustomFlowDayListModel) {
    this.dayId=customerPreferenceSiteCustomFlowDayListModel?customerPreferenceSiteCustomFlowDayListModel.dayId==undefined? null:customerPreferenceSiteCustomFlowDayListModel.dayId:null;
}
 dayId:string;
}

class CustomerPreferenceSiteCustomFlowSignalListModel {
  constructor(customerPreferenceSiteCustomFlowSignalListModel?: CustomerPreferenceSiteCustomFlowSignalListModel) {
    this.alarmTypeId=customerPreferenceSiteCustomFlowSignalListModel?customerPreferenceSiteCustomFlowSignalListModel.alarmTypeId==undefined? null:customerPreferenceSiteCustomFlowSignalListModel.alarmTypeId:null;
}
 alarmTypeId:string;
}

class CustomerPreferenceSiteCustomFlowKeyHolderListModel{
  constructor(customerPreferenceSiteCustomFlowKeyHolderListModel?: CustomerPreferenceSiteCustomFlowKeyHolderListModel) {
    this.keyHolderId=customerPreferenceSiteCustomFlowKeyHolderListModel?customerPreferenceSiteCustomFlowKeyHolderListModel.keyHolderId==undefined? null:customerPreferenceSiteCustomFlowKeyHolderListModel.keyHolderId:null;
    this.orderNumber=customerPreferenceSiteCustomFlowKeyHolderListModel?customerPreferenceSiteCustomFlowKeyHolderListModel.orderNumber==undefined? null:customerPreferenceSiteCustomFlowKeyHolderListModel.orderNumber:null;
    this.IsCall=customerPreferenceSiteCustomFlowKeyHolderListModel?customerPreferenceSiteCustomFlowKeyHolderListModel.IsCall==undefined? false:customerPreferenceSiteCustomFlowKeyHolderListModel.IsCall:false;

  }
keyHolderId:string;
orderNumber:string;
IsCall: boolean;
}



export { SiteCustomCallFlowModel}
