class TicketManualAddModel{
    constructor(ticketManualAddModel?:TicketManualAddModel){
        this.ticketId=ticketManualAddModel?ticketManualAddModel.ticketId==undefined?'':ticketManualAddModel.ticketId:'';
        this.ticketTypeId=ticketManualAddModel?ticketManualAddModel.ticketTypeId==undefined?'':ticketManualAddModel.ticketTypeId:'';
         this.customerId=ticketManualAddModel?ticketManualAddModel.customerId==undefined?'':ticketManualAddModel.customerId:'';
         this.description=ticketManualAddModel?ticketManualAddModel.description==undefined?'':ticketManualAddModel.description:'';
         this.ticketOutcomeId=ticketManualAddModel?ticketManualAddModel.ticketOutcomeId==undefined?'':ticketManualAddModel.ticketOutcomeId: null;
         this.ticketStatusId=ticketManualAddModel?ticketManualAddModel.ticketStatusId==undefined?'':ticketManualAddModel.ticketStatusId:'';
         this.userGroupId=ticketManualAddModel?ticketManualAddModel.userGroupId==undefined?'':ticketManualAddModel.userGroupId:'';
         this.cancellationDate = ticketManualAddModel ? ticketManualAddModel.cancellationDate == undefined ? null : ticketManualAddModel.cancellationDate : null;
        this.suspensionFromDate = ticketManualAddModel ? ticketManualAddModel.suspensionFromDate == undefined ? null : ticketManualAddModel.suspensionFromDate : null;
          this.suspensionToDate = ticketManualAddModel ? ticketManualAddModel.suspensionToDate == undefined ? null : ticketManualAddModel.suspensionToDate : null;
         this.saveOfferPercentage=ticketManualAddModel?ticketManualAddModel.saveOfferPercentage==undefined?'':ticketManualAddModel.saveOfferPercentage:'';
         this.subscriptionIds=ticketManualAddModel?ticketManualAddModel.subscriptionIds==undefined?[]:ticketManualAddModel.subscriptionIds:[];
         this.ticketCancellationReasonId=ticketManualAddModel?ticketManualAddModel.ticketCancellationReasonId==undefined?null:ticketManualAddModel.ticketCancellationReasonId:null;
         this.ticketCancellationSubReasonId=ticketManualAddModel?ticketManualAddModel.ticketCancellationSubReasonId==undefined?null:ticketManualAddModel.ticketCancellationSubReasonId:null;
         this.isRequestTypeWritten=ticketManualAddModel?ticketManualAddModel.isRequestTypeWritten==undefined?null:ticketManualAddModel.isRequestTypeWritten:null;
         this.isCancellationLetterReceived=ticketManualAddModel?ticketManualAddModel.isCancellationLetterReceived==undefined?null:ticketManualAddModel.isCancellationLetterReceived:null;
        this.assignedBy=ticketManualAddModel?ticketManualAddModel.assignedBy==undefined?'':ticketManualAddModel.assignedBy:'';
         this.assignedTo=ticketManualAddModel?ticketManualAddModel.assignedTo==undefined?'':ticketManualAddModel.assignedTo:'';
         this.priorityId=ticketManualAddModel?ticketManualAddModel.priorityId==undefined?'':ticketManualAddModel.priorityId:'';
         this.flagColorId=ticketManualAddModel?ticketManualAddModel.flagColorId==undefined?'':ticketManualAddModel.flagColorId:'';
         this.lapsedStartTime=ticketManualAddModel?ticketManualAddModel.lapsedStartTime==undefined?null:ticketManualAddModel.lapsedStartTime:null;
         this.lapsedEndTime=ticketManualAddModel?ticketManualAddModel.lapsedEndTime ==undefined?null:ticketManualAddModel.lapsedEndTime:null;
         this.createdStartDate=ticketManualAddModel?ticketManualAddModel.createdStartDate ==undefined?null:ticketManualAddModel.createdStartDate: null
         this.createdEndDate=ticketManualAddModel?ticketManualAddModel.createdEndDate ==undefined? null :ticketManualAddModel.createdEndDate: null
         this.closedStartDate=ticketManualAddModel?ticketManualAddModel.closedStartDate ==undefined? null :ticketManualAddModel.closedStartDate: null
         this.closedEndDate=ticketManualAddModel?ticketManualAddModel.closedEndDate ==undefined? null :ticketManualAddModel.closedEndDate: null
         this.ticketStatusNameMultiple=ticketManualAddModel?ticketManualAddModel.ticketStatusNameMultiple ==undefined?'':ticketManualAddModel.ticketStatusNameMultiple:'';
         this.ticketTypeMultiple=ticketManualAddModel?ticketManualAddModel.ticketTypeMultiple ==undefined?'':ticketManualAddModel.ticketTypeMultiple:'';
         this.assignedToMultiple=ticketManualAddModel?ticketManualAddModel.assignedToMultiple ==undefined?'':ticketManualAddModel.assignedToMultiple:'';
         this.userGroupMultiple=ticketManualAddModel?ticketManualAddModel.userGroupMultiple ==undefined?'':ticketManualAddModel.userGroupMultiple:'';
         this.createdUserId=ticketManualAddModel?ticketManualAddModel.createdUserId==undefined?'':ticketManualAddModel.createdUserId:'';
         this.siteAddressId=ticketManualAddModel?ticketManualAddModel.siteAddressId ==undefined?'':ticketManualAddModel.siteAddressId:'';
         this.isRadio=ticketManualAddModel?ticketManualAddModel.isRadio==undefined?null:ticketManualAddModel.isRadio:null;
         this.isSystem=ticketManualAddModel?ticketManualAddModel.isSystem==undefined?null:ticketManualAddModel.isSystem:null;
         this.ticketGroupId=ticketManualAddModel?ticketManualAddModel.ticketGroupId==undefined?null:ticketManualAddModel.ticketGroupId:null;
         this.fileName=ticketManualAddModel?ticketManualAddModel.fileName==undefined?null:ticketManualAddModel.fileName:null;
        }
    ticketId?:string;
    ticketTypeId?: string ;
    customerId?: string ;
    description?: string;
    ticketOutcomeId?: string;
    ticketStatusId?: string;
    userGroupId?: string ;
    cancellationDate?: Date ;
    suspensionFromDate?: Date ;
    suspensionToDate?: Date ;
    saveOfferPercentage: string ;
    subscriptionIds?: string[] ;
    ticketCancellationReasonId: string;
    ticketCancellationSubReasonId: string;
    isRequestTypeWritten: boolean;
    isCancellationLetterReceived: boolean;
    assignedBy?: string ;
    assignedTo?: string ;
    priorityId?:string;
    flagColorId?:string;
    lapsedStartTime?:string;
    lapsedEndTime?:string;
    createdStartDate?:Date;
    createdEndDate?:Date;
    closedStartDate?:Date;
    closedEndDate?:Date;
    ticketStatusNameMultiple?:string;
    ticketTypeMultiple?:any;
    userGroupMultiple?:any;
    assignedToMultiple?:any;
    createdUserId?:string;
    siteAddressId?:string;
    isRadio: boolean;
    isSystem: boolean;
    ticketGroupId?:string;
    fileName?:string;
}

class SupportDocumentUploadModel{
    constructor(supportDocumentUploadModel?:SupportDocumentUploadModel){
        this.fileName=supportDocumentUploadModel?supportDocumentUploadModel.fileName==undefined?'':supportDocumentUploadModel.fileName:'';
        this.documentTypeId=supportDocumentUploadModel?supportDocumentUploadModel.documentTypeId==undefined? null:supportDocumentUploadModel.documentTypeId:null;
         this.customerId=supportDocumentUploadModel?supportDocumentUploadModel.customerId==undefined?'':supportDocumentUploadModel.customerId:'';
         this.description=supportDocumentUploadModel?supportDocumentUploadModel.description==undefined?'':supportDocumentUploadModel.description:'';
         this.departmentId=supportDocumentUploadModel?supportDocumentUploadModel.departmentId==undefined? '':supportDocumentUploadModel.departmentId:'';
         this.createdUserId=supportDocumentUploadModel?supportDocumentUploadModel.createdUserId==undefined?'':supportDocumentUploadModel.createdUserId:'';
         this.fileUpload=supportDocumentUploadModel?supportDocumentUploadModel.fileUpload==undefined?'':supportDocumentUploadModel.fileUpload:'';
         this.referenceId=supportDocumentUploadModel?supportDocumentUploadModel.referenceId==undefined?'':supportDocumentUploadModel.referenceId:'';
         this.documentTypeName=supportDocumentUploadModel?supportDocumentUploadModel.documentTypeName==undefined?'':supportDocumentUploadModel.documentTypeName:'';
         this.documentId=supportDocumentUploadModel?supportDocumentUploadModel.documentId==undefined?'':supportDocumentUploadModel.documentId:'';

        }
    fileName?:string;
    documentTypeId?: string ;
    customerId?: string ;
    description?: string;
    departmentId?: string;
    createdUserId?:string;
    fileUpload?:string;
    documentTypeName?:string;
    referenceId? : string;
    documentId?: string;
}

export { TicketManualAddModel, SupportDocumentUploadModel };


export class ticket {

    ticketId: any;
    ticketNumber: string;
    ticketTypeName: string;
    description: any;
    createdBy: any;
    assignedToName: string;
    createdDate: Date;
    targetDate: Date;
    ticketStatusName: string;
    subscriptionnumber: string;
    usergroupName: any;
    lapsedStartTime?:string;
    lapsedEndTime?:string;
    createdStartDate?:Date;
    createdEndDate?:Date;
    closedStartDate?:Date;
    closedEndDate?:Date;
    ticketStatusNameMultiple?:string;
    ticketTypeMultiple?:any;
    userGroupMultiple?:any;
    assignedToMultiple?:any;
}
