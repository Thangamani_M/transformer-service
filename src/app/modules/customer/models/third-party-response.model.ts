class ThirdPartyResponseFormModel {
  constructor(thirdPartyResponseFormModel?: ThirdPartyResponseFormModel) {
      this.operatorId = thirdPartyResponseFormModel ? thirdPartyResponseFormModel.operatorId == undefined ? null : thirdPartyResponseFormModel.operatorId : null;
      this.createdUserId = thirdPartyResponseFormModel ? thirdPartyResponseFormModel.createdUserId == undefined ? '' : thirdPartyResponseFormModel.createdUserId : '';
      this.occurrenceBookId = thirdPartyResponseFormModel ? thirdPartyResponseFormModel.occurrenceBookId == undefined ? '' : thirdPartyResponseFormModel.occurrenceBookId : '';
      this.callLogId = thirdPartyResponseFormModel ? thirdPartyResponseFormModel.callLogId == undefined ? '' : thirdPartyResponseFormModel.callLogId : '';
      this.thirdPartyId = thirdPartyResponseFormModel ? thirdPartyResponseFormModel.thirdPartyId == undefined ? '' : thirdPartyResponseFormModel.thirdPartyId : '';
      this.controllerName = thirdPartyResponseFormModel ? thirdPartyResponseFormModel.controllerName == undefined ? '' : thirdPartyResponseFormModel.controllerName : '';
      this.thirdPartyOBNumber = thirdPartyResponseFormModel ? thirdPartyResponseFormModel.thirdPartyOBNumber == undefined ? '' : thirdPartyResponseFormModel.thirdPartyOBNumber : '';
      this.eta = thirdPartyResponseFormModel ? thirdPartyResponseFormModel.eta == undefined ? '' : thirdPartyResponseFormModel.eta : '';
      this.isActive = thirdPartyResponseFormModel ? thirdPartyResponseFormModel.isActive == undefined ? true : thirdPartyResponseFormModel.isActive : true;
      this.operatorInstructionList = thirdPartyResponseFormModel ? thirdPartyResponseFormModel.operatorInstructionList == undefined ? [] : thirdPartyResponseFormModel.operatorInstructionList : [];
  }
  operatorId: any;
  createdUserId: string;
  occurrenceBookId: string;
  callLogId: string;
  thirdPartyId: string;
  controllerName: string;
  thirdPartyOBNumber: string;
  eta: string;
  isActive: boolean;
  operatorInstructionList: any[];

}

export { ThirdPartyResponseFormModel }
