class TicketProcessCancelDetailsModel {
    serviceCancellationId?: string;
    contractId?: string;
    ticketId?: string;
    suspendServiceDate?: string;
    contractLastBillDate?: string;
    ticketCancellationReasonId?: string;
    ticketCancellationSubReasonId?: string;
    notes?: string;
    isRadioRemovalRequired?: boolean;
    isRadio?: boolean;
    isSystem?: boolean;
    serviceCancellationStatusId?: string | number;
    cancellationDocumentationDate?: string;
    services?: TicketProcessServicesDetailsModel[];
    customerId?: string;
    siteAddressId?: string;
    oppositionId?: string;
    oppositionReasonId?: string;
    dateOfRemoval?: string;
    createdUserId?: string;
    status?: string;
    constructor(ticketProcessCancelDetailsModel: TicketProcessCancelDetailsModel) {
        this.serviceCancellationId = ticketProcessCancelDetailsModel?.serviceCancellationId == undefined ? '' : ticketProcessCancelDetailsModel?.serviceCancellationId;
        this.contractId = ticketProcessCancelDetailsModel?.contractId == undefined ? '' : ticketProcessCancelDetailsModel?.contractId;
        this.ticketId = ticketProcessCancelDetailsModel?.ticketId == undefined ? '' : ticketProcessCancelDetailsModel?.ticketId;
        this.suspendServiceDate = ticketProcessCancelDetailsModel?.suspendServiceDate == undefined ? '' : ticketProcessCancelDetailsModel?.suspendServiceDate;
        this.contractLastBillDate = ticketProcessCancelDetailsModel?.contractLastBillDate == undefined ? '' : ticketProcessCancelDetailsModel?.contractLastBillDate;
        this.ticketCancellationReasonId = ticketProcessCancelDetailsModel?.ticketCancellationReasonId == undefined ? '' : ticketProcessCancelDetailsModel?.ticketCancellationReasonId;
        this.ticketCancellationSubReasonId = ticketProcessCancelDetailsModel?.ticketCancellationSubReasonId == undefined ? '' : ticketProcessCancelDetailsModel?.ticketCancellationSubReasonId;
        this.notes = ticketProcessCancelDetailsModel?.notes == undefined ? '' : ticketProcessCancelDetailsModel?.notes;
        this.isRadioRemovalRequired = ticketProcessCancelDetailsModel?.isRadioRemovalRequired == undefined ? undefined : ticketProcessCancelDetailsModel?.isRadioRemovalRequired;
        this.isRadio = ticketProcessCancelDetailsModel?.isRadio == undefined ? undefined : ticketProcessCancelDetailsModel?.isRadio;
        this.isSystem = ticketProcessCancelDetailsModel?.isSystem == undefined ? undefined : ticketProcessCancelDetailsModel?.isSystem;
        this.serviceCancellationStatusId = ticketProcessCancelDetailsModel?.serviceCancellationStatusId == undefined ? 1 : ticketProcessCancelDetailsModel?.serviceCancellationStatusId;
        this.cancellationDocumentationDate = ticketProcessCancelDetailsModel?.cancellationDocumentationDate == undefined ? '' : ticketProcessCancelDetailsModel?.cancellationDocumentationDate;
        this.services = ticketProcessCancelDetailsModel?.services == undefined ? [] : ticketProcessCancelDetailsModel?.services;
        this.customerId = ticketProcessCancelDetailsModel?.customerId == undefined ? '' : ticketProcessCancelDetailsModel?.customerId;
        this.siteAddressId = ticketProcessCancelDetailsModel?.siteAddressId == undefined ? '' : ticketProcessCancelDetailsModel?.siteAddressId;
        this.oppositionId = ticketProcessCancelDetailsModel?.oppositionId == undefined ? '' : ticketProcessCancelDetailsModel?.oppositionId;
        this.oppositionReasonId = ticketProcessCancelDetailsModel?.oppositionReasonId == undefined ? '' : ticketProcessCancelDetailsModel?.oppositionReasonId;
        this.dateOfRemoval = ticketProcessCancelDetailsModel?.dateOfRemoval == undefined ? '' : ticketProcessCancelDetailsModel?.dateOfRemoval;
        this.createdUserId = ticketProcessCancelDetailsModel?.createdUserId == undefined ? '' : ticketProcessCancelDetailsModel?.createdUserId;
        this.status = ticketProcessCancelDetailsModel?.status == undefined ? '' : ticketProcessCancelDetailsModel?.status;
    }
}

class TicketProcessServicesDetailsModel {
    serviceCancellationDetailId?: string;
    serviceId?: string;
    constructor(ticketProcessServicesDetailsModel: TicketProcessServicesDetailsModel) {
        this.serviceCancellationDetailId = ticketProcessServicesDetailsModel?.serviceCancellationDetailId == undefined ? '' : ticketProcessServicesDetailsModel?.serviceCancellationDetailId;
        this.serviceId = ticketProcessServicesDetailsModel?.serviceId == undefined ? '' : ticketProcessServicesDetailsModel?.serviceId;
    }
}

class TicketProcessCancelDialogDetailsModel {
    contractId?: string;
    suspendServiceDate?: string;
    contractLastBillDate?: string;
    billValue?: string;
    bocBillDate?: string;
    bocValue?: string;
    noticeBillDate?: string;
    noticeValue?: string;
    prorataAmount?: string;
    flag?: boolean;
    constructor(ticketProcessCancelDialogDetailsModel: TicketProcessCancelDialogDetailsModel) {
        this.contractId = ticketProcessCancelDialogDetailsModel?.contractId == undefined ? '' : ticketProcessCancelDialogDetailsModel?.contractId;
        this.suspendServiceDate = ticketProcessCancelDialogDetailsModel?.suspendServiceDate == undefined ? '' : ticketProcessCancelDialogDetailsModel?.suspendServiceDate;
        this.contractLastBillDate = ticketProcessCancelDialogDetailsModel?.contractLastBillDate == undefined ? '' : ticketProcessCancelDialogDetailsModel?.contractLastBillDate;
        this.billValue = ticketProcessCancelDialogDetailsModel?.billValue == undefined ? '' : ticketProcessCancelDialogDetailsModel?.billValue;
        this.bocBillDate = ticketProcessCancelDialogDetailsModel?.bocBillDate == undefined ? '' : ticketProcessCancelDialogDetailsModel?.bocBillDate;
        this.bocValue = ticketProcessCancelDialogDetailsModel?.bocValue == undefined ? '' : ticketProcessCancelDialogDetailsModel?.bocValue;
        this.noticeBillDate = ticketProcessCancelDialogDetailsModel?.noticeBillDate == undefined ? '' : ticketProcessCancelDialogDetailsModel?.noticeBillDate;
        this.prorataAmount = ticketProcessCancelDialogDetailsModel?.prorataAmount == undefined ? '' : ticketProcessCancelDialogDetailsModel?.prorataAmount;
        this.noticeValue = ticketProcessCancelDialogDetailsModel?.noticeValue == undefined ? '' : ticketProcessCancelDialogDetailsModel?.noticeValue;
        this.flag = ticketProcessCancelDialogDetailsModel?.flag == undefined ? false : ticketProcessCancelDialogDetailsModel?.flag;
    }
}

export { TicketProcessCancelDetailsModel, TicketProcessServicesDetailsModel, TicketProcessCancelDialogDetailsModel };

