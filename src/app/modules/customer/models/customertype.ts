export class CustomerType{
    customerTypeId:any;
    customerTypeName:string;
    description:string;
    cssClass:string;
    isDeleted:boolean;
    isActive:boolean;
}