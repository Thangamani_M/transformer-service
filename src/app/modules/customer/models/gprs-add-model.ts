class GprsSignalModel {
  constructor(gprsSignalModel?: GprsSignalModel) {
      this.callInitiationId = gprsSignalModel ? gprsSignalModel.callInitiationId == undefined ? null : gprsSignalModel.callInitiationId : null;
      this.createdUserId = gprsSignalModel ? gprsSignalModel.createdUserId == undefined ? '' : gprsSignalModel.createdUserId : '';
      this.contractId = gprsSignalModel ? gprsSignalModel.contractId == undefined ? '' : gprsSignalModel.contractId : '';
      this.customerId = gprsSignalModel ? gprsSignalModel.customerId == undefined ? '' : gprsSignalModel.customerId : '';
      this.addressId = gprsSignalModel ? gprsSignalModel.addressId == undefined ? '' : gprsSignalModel.addressId : '';
      this.debtorId = gprsSignalModel ? gprsSignalModel.debtorId == undefined ? '' : gprsSignalModel.debtorId : '';
      this.stockId = gprsSignalModel ? gprsSignalModel.stockId == undefined ? null : gprsSignalModel.stockId : null;
      this.serviceCategoryId = gprsSignalModel ? gprsSignalModel.serviceCategoryId == undefined ? '' : gprsSignalModel.serviceCategoryId : '';
      this.monthlyChargeInclVAT = gprsSignalModel ? gprsSignalModel.monthlyChargeInclVAT == undefined ? 0 : gprsSignalModel.monthlyChargeInclVAT : 0;
      this.radioRecurringFeeId = gprsSignalModel ? gprsSignalModel.radioRecurringFeeId == undefined ? '' : gprsSignalModel.radioRecurringFeeId : '';
      this.debtorRefNO = gprsSignalModel ? gprsSignalModel.debtorRefNO == undefined ? '' : gprsSignalModel.debtorRefNO : '';
      this.debtorName = gprsSignalModel ? gprsSignalModel.debtorName == undefined ? '' : gprsSignalModel.debtorName : '';

  }
  callInitiationId: any;
  createdUserId: string;
  contractId: string;
  customerId: string;
  addressId: string;
  debtorId: string;
  stockId: any;
  serviceCategoryId: string;
  monthlyChargeInclVAT: number;
  radioRecurringFeeId: string;
  debtorName: string;
  debtorRefNO: string;
}


class AnfModel {
  constructor(anflModel?: AnfModel) {
      this.radioDecoderId = anflModel ? anflModel.radioDecoderId == undefined ? null : anflModel.radioDecoderId : null;
      this.createdUserId = anflModel ? anflModel.createdUserId == undefined ? '' : anflModel.createdUserId : '';
      this.subscriptionId = anflModel ? anflModel.subscriptionId == undefined ? '' : anflModel.subscriptionId : '';
      this.statusId = anflModel ? anflModel.statusId == undefined ? '' : anflModel.statusId : '';
      this.stockId = anflModel ? anflModel.stockId == undefined ? null : anflModel.stockId : null;
      this.customerAvailabilityId = anflModel ? anflModel.customerAvailabilityId == undefined ? '' : anflModel.customerAvailabilityId : '';
      this.subscriptionValue = anflModel ? anflModel.subscriptionValue == undefined ? 0 : anflModel.subscriptionValue : 0;
      this.stockValue = anflModel ? anflModel.stockValue == undefined ? 0 : anflModel.stockValue : 0;
      this.customerName = anflModel ? anflModel.customerName == undefined ? '' : anflModel.customerName : '';
      this.customerComments = anflModel ? anflModel.customerComments == undefined ? '' : anflModel.customerComments : '';
      this.customerSignaturePath = anflModel ? anflModel.customerSignaturePath == undefined ? '' : anflModel.customerSignaturePath : '';
      this.customerSignatureFileName = anflModel ? anflModel.customerSignatureFileName == undefined ? '' : anflModel.customerSignatureFileName : '';
      this.smsNotificationNumber = anflModel ? anflModel.smsNotificationNumber == undefined ? 0 : anflModel.smsNotificationNumber : 0;
      this.smsNotificationNumberCountryCode = anflModel ? anflModel.smsNotificationNumberCountryCode == undefined ? '' : anflModel.smsNotificationNumberCountryCode : '';
      this.anfDiscountMotivation = anflModel ? anflModel.anfDiscountMotivation == undefined ? '' : anflModel.anfDiscountMotivation : '';
      this.isCustomerAgreedCharges = anflModel ? anflModel.isCustomerAgreedCharges == undefined ? true : anflModel.isCustomerAgreedCharges : true;

  }
  radioDecoderId: any;
  createdUserId: string;
  subscriptionId: string;
  statusId: any;
  customerAvailabilityId: string;
  stockId: string;
  subscriptionValue?:number;
  stockValue:number;
  smsNotificationNumber:number;
  customerComments: string;
  customerName: string;
  customerSignatureFileName: string;
  customerSignaturePath: string;
  smsNotificationNumberCountryCode: string;
  anfDiscountMotivation: string;
  isCustomerAgreedCharges:boolean
}
class ApproveANFModel {
  constructor(approveANFModel?: ApproveANFModel) {
      this.radioDecoderANFRequestId = approveANFModel ? approveANFModel.radioDecoderANFRequestId == undefined ? null : approveANFModel.radioDecoderANFRequestId : null;
      this.createdUserId = approveANFModel ? approveANFModel.createdUserId == undefined ? '' : approveANFModel.createdUserId : '';
      this.approvedBy = approveANFModel ? approveANFModel.approvedBy == undefined ? '' : approveANFModel.approvedBy : '';
      this.isApproved = approveANFModel ? approveANFModel.isApproved == undefined ? true : approveANFModel.isApproved :true;
      this.comments = approveANFModel ? approveANFModel.comments == undefined ? '' : approveANFModel.comments : '';
      this.discountAmount = approveANFModel ? approveANFModel.discountAmount == undefined ? '' : approveANFModel.discountAmount : '';
  }
  radioDecoderANFRequestId: any;
  createdUserId: string;
  approvedBy: string;
  isApproved: boolean;
  comments: string;
  discountAmount: string;
}
class SharedOTPModel {
  constructor(sharedOTPModel?: SharedOTPModel) {
      this.otpId = sharedOTPModel ? sharedOTPModel.otpId == undefined ? null : sharedOTPModel.otpId : null;
      this.createdUserId = sharedOTPModel ? sharedOTPModel.createdUserId == undefined ? '' : sharedOTPModel.createdUserId : '';
      this.otpReferenceTypeId = sharedOTPModel ? sharedOTPModel.otpReferenceTypeId == undefined ? '' : sharedOTPModel.otpReferenceTypeId : '';
      this.createdDate = sharedOTPModel ? sharedOTPModel.createdDate == undefined ? '' : sharedOTPModel.createdDate : '';
      this.userId = sharedOTPModel ? sharedOTPModel.userId == undefined ? '' : sharedOTPModel.userId :'';
      this.mobileNoCountryCode = sharedOTPModel ? sharedOTPModel.mobileNoCountryCode == undefined ? '' : sharedOTPModel.mobileNoCountryCode : '';
      this.mobileNo = sharedOTPModel ? sharedOTPModel.mobileNo == undefined ? '' : sharedOTPModel.mobileNo : '';
  }
  otpId: any;
  createdUserId: string;
  otpReferenceTypeId: string;
  createdDate: string;
  userId: string;
  mobileNoCountryCode: string;
  mobileNo: string;
}



export { GprsSignalModel,AnfModel,ApproveANFModel ,SharedOTPModel}
