
class ActualTimingModel {
    constructor(ActualTimingModel?: ActualTimingModel) {
        this.actualTimingList = ActualTimingModel ? ActualTimingModel.actualTimingList == undefined ? [] : ActualTimingModel.actualTimingList : [];
    }
    actualTimingList: ActualTimingListModel[];
}

class ActualTimingListModel {
    constructor(callCreationTemplateModel: ActualTimingListModel) {
        this.appointmentActualTimingId = callCreationTemplateModel ? callCreationTemplateModel.appointmentActualTimingId == undefined ? '' : callCreationTemplateModel.appointmentActualTimingId : '';
        this.appointmentId = callCreationTemplateModel ? callCreationTemplateModel.appointmentId == undefined ? '' : callCreationTemplateModel.appointmentId : '';
        this.appointmentDate = callCreationTemplateModel ? callCreationTemplateModel.appointmentDate == undefined ? '' : callCreationTemplateModel.appointmentDate : '';
        this.onRouteDate = callCreationTemplateModel ? callCreationTemplateModel.onRouteDate == undefined ? '' : callCreationTemplateModel.onRouteDate : '';
        this.coordinates = callCreationTemplateModel ? callCreationTemplateModel.coordinates == undefined ? '' : callCreationTemplateModel.coordinates : '';
        this.onRouteTime = callCreationTemplateModel ? callCreationTemplateModel.onRouteTime == undefined ? '' : callCreationTemplateModel.onRouteTime : '';
        this.startTime = callCreationTemplateModel ? callCreationTemplateModel.startTime == undefined ? '' : callCreationTemplateModel.startTime : '';
        this.endTime = callCreationTemplateModel ? callCreationTemplateModel.endTime == undefined ? '' : callCreationTemplateModel.endTime : '';
        this.actualTime = callCreationTemplateModel ? callCreationTemplateModel.actualTime == undefined ? '' : callCreationTemplateModel.actualTime : '';
        this.createdUserId = callCreationTemplateModel ? callCreationTemplateModel.createdUserId == undefined ? '' : callCreationTemplateModel.createdUserId : '';
        this.modifiedUserId = callCreationTemplateModel ? callCreationTemplateModel.modifiedUserId == undefined ? '' : callCreationTemplateModel.modifiedUserId : '';
        this.appointmentLogId = callCreationTemplateModel ? callCreationTemplateModel.appointmentLogId == undefined ? '' : callCreationTemplateModel.appointmentLogId : '';
        this.startTimeMinDate = callCreationTemplateModel ? callCreationTemplateModel.startTimeMinDate == undefined ? '' : callCreationTemplateModel.startTimeMinDate : '';
        this.endTimeMinDate = callCreationTemplateModel ? callCreationTemplateModel.endTimeMinDate == undefined ? '' : callCreationTemplateModel.endTimeMinDate : '';
    }
    appointmentActualTimingId: string;
    appointmentId: string;
    appointmentDate: string;
    onRouteDate: any;
    coordinates: string;
    onRouteTime: any
    startTime: any;
    startTimeMinDate: any;
    endTime: any;
    endTimeMinDate: any;
    actualTime: any;
    createdUserId: string
    modifiedUserId: string
    appointmentLogId: string;
}

export { ActualTimingModel, ActualTimingListModel }

