
class CreditControllerSearchModel {
  constructor(creditControllerSearchModel?: CreditControllerSearchModel) {
    this.creditControllerCode = creditControllerSearchModel ? creditControllerSearchModel.creditControllerCode == undefined ? [] : creditControllerSearchModel.creditControllerCode : [];
    this.creditControllerTypeName = creditControllerSearchModel ? creditControllerSearchModel.creditControllerTypeName == undefined ? '' : creditControllerSearchModel.creditControllerTypeName : '';
    this.employeeName = creditControllerSearchModel ? creditControllerSearchModel.employeeName == undefined ? '' : creditControllerSearchModel.employeeName : '';
    this.requestDivisionId = creditControllerSearchModel ? creditControllerSearchModel.requestDivisionId == undefined ? '' : creditControllerSearchModel.requestDivisionId : '';
    this.currentDivisionId = creditControllerSearchModel ? creditControllerSearchModel.currentDivisionId == undefined ? '' : creditControllerSearchModel.currentDivisionId : '';
    this.assignAnotherDivision = creditControllerSearchModel ? creditControllerSearchModel.assignAnotherDivision == undefined ? false : creditControllerSearchModel.assignAnotherDivision : false;
    this.customerId = creditControllerSearchModel ? creditControllerSearchModel.customerId == undefined ? '' : creditControllerSearchModel.customerId : '';
    this.addressId = creditControllerSearchModel ? creditControllerSearchModel.addressId == undefined ? '' : creditControllerSearchModel.addressId : '';

  }

  creditControllerCode?: any[];
  creditControllerTypeName?: string;
  employeeName?: string;
  requestDivisionId?: string;
  currentDivisionId?: string;
  assignAnotherDivision?: boolean;
  customerId?: string;
  addressId?: string;

}
class CreditControllerAssignModel {
  constructor(creditControllerAssignModel?: CreditControllerAssignModel) {
    this.creditControllerAssignToDebtorId = creditControllerAssignModel ? creditControllerAssignModel.creditControllerAssignToDebtorId == undefined ? '' : creditControllerAssignModel.creditControllerAssignToDebtorId : '';
    this.creditControllerEmployeeId = creditControllerAssignModel ? creditControllerAssignModel.creditControllerEmployeeId == undefined ? '' : creditControllerAssignModel.creditControllerEmployeeId : '';
    this.contractId = creditControllerAssignModel ? creditControllerAssignModel.contractId == undefined ? '' : creditControllerAssignModel.contractId : '';
    this.requestDivisionId = creditControllerAssignModel ? creditControllerAssignModel.requestDivisionId == undefined ? '' : creditControllerAssignModel.requestDivisionId : '';
    this.debtorId = creditControllerAssignModel ? creditControllerAssignModel.debtorId == undefined ? '' : creditControllerAssignModel.debtorId : '';
    this.isActive = creditControllerAssignModel ? creditControllerAssignModel.isActive == undefined ? true : creditControllerAssignModel.isActive : true;
    this.customerId = creditControllerAssignModel ? creditControllerAssignModel.customerId == undefined ? '' : creditControllerAssignModel.customerId : '';
    this.userId = creditControllerAssignModel ? creditControllerAssignModel.userId == undefined ? '' : creditControllerAssignModel.userId : '';

  }

  creditControllerAssignToDebtorId?: string;
  creditControllerEmployeeId?: string;
  contractId?: string;
  requestDivisionId?: string;
  debtorId?: string;
  isActive?: boolean;
  customerId?: string;
  userId?: string;

}


export { CreditControllerSearchModel, CreditControllerAssignModel }

