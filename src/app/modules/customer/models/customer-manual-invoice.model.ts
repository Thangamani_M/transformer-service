
class CustomerManualInvoiceModel{
    constructor(customerManualInvoiceModel?:CustomerManualInvoiceModel){
        this.customerId=customerManualInvoiceModel?customerManualInvoiceModel.customerId==undefined?'':customerManualInvoiceModel.customerId:'';
        this.contractId=customerManualInvoiceModel?customerManualInvoiceModel.contractId==undefined?'':customerManualInvoiceModel.contractId:'';
        this.siteAddressId=customerManualInvoiceModel?customerManualInvoiceModel.siteAddressId==undefined?'':customerManualInvoiceModel.siteAddressId:'';
        this.invoiceTransactionTypeId=customerManualInvoiceModel?customerManualInvoiceModel.invoiceTransactionTypeId==undefined?'':customerManualInvoiceModel.invoiceTransactionTypeId:'';
        this.invoiceTransactionDescriptionId=customerManualInvoiceModel?customerManualInvoiceModel.invoiceTransactionDescriptionId==undefined?'':customerManualInvoiceModel.invoiceTransactionDescriptionId:'';
        this.departmentId=customerManualInvoiceModel?customerManualInvoiceModel.departmentId==undefined?'':customerManualInvoiceModel.departmentId:'';
        this.documentDate=customerManualInvoiceModel?customerManualInvoiceModel.documentDate==undefined?'':customerManualInvoiceModel.documentDate:'';
        this.comments=customerManualInvoiceModel?customerManualInvoiceModel.comments==undefined?'':customerManualInvoiceModel.comments:'';
        this.isIncludePONumber=customerManualInvoiceModel?customerManualInvoiceModel.isIncludePONumber==undefined?false:customerManualInvoiceModel.isIncludePONumber:false;
        this.isSendCustomerCopy=customerManualInvoiceModel?customerManualInvoiceModel.isSendCustomerCopy==undefined?false:customerManualInvoiceModel.isSendCustomerCopy:false;
        this.regionName=customerManualInvoiceModel?customerManualInvoiceModel.regionName==undefined?'':customerManualInvoiceModel.regionName:'';
        this.divisionName=customerManualInvoiceModel?customerManualInvoiceModel.divisionName==undefined?'':customerManualInvoiceModel.divisionName:'';
        this.branchName=customerManualInvoiceModel?customerManualInvoiceModel.branchName==undefined?'':customerManualInvoiceModel.branchName:'';
        this.purchaseOrderNo=customerManualInvoiceModel?customerManualInvoiceModel.purchaseOrderNo==undefined?'':customerManualInvoiceModel.purchaseOrderNo:'';
        this.createdUserId=customerManualInvoiceModel?customerManualInvoiceModel.createdUserId==undefined?'':customerManualInvoiceModel.createdUserId:'';
        this.expiryDate=customerManualInvoiceModel?customerManualInvoiceModel.expiryDate==undefined?'':customerManualInvoiceModel.expiryDate:'';
        this.requireDocId=customerManualInvoiceModel?customerManualInvoiceModel.requireDocId==undefined?'':customerManualInvoiceModel.requireDocId:'';
        this.optionalDocId=customerManualInvoiceModel?customerManualInvoiceModel.optionalDocId==undefined?'':customerManualInvoiceModel.optionalDocId:'';
        this.telephonicDocId=customerManualInvoiceModel?customerManualInvoiceModel.telephonicDocId==undefined?'':customerManualInvoiceModel.telephonicDocId:'';
        this.stockQty=customerManualInvoiceModel?customerManualInvoiceModel.stockQty==undefined?'':customerManualInvoiceModel.stockQty:'';
        this.stockIdDescription=customerManualInvoiceModel?customerManualInvoiceModel.stockIdDescription==undefined?'':customerManualInvoiceModel.stockIdDescription:'';
        this.stockId=customerManualInvoiceModel?customerManualInvoiceModel.stockId==undefined?'':customerManualInvoiceModel.stockId:'';

    }
    customerId?: string;
    contractId?: string;
    siteAddressId?: string;
    invoiceTransactionTypeId?: string;
    invoiceTransactionDescriptionId?: string;
    departmentId?: string;
    documentDate?: string;
    comments?: string;
    isIncludePONumber?: boolean;
    isSendCustomerCopy: boolean;
    regionName: string;
    divisionName: string;
    branchName: string;
    purchaseOrderNo: string;
    createdUserId: string;
    expiryDate: string;
    requireDocId: string;
    optionalDocId: string;
    telephonicDocId: string;
    stockQty: string;
    stockIdDescription: string;
    stockId: string;

}

export { CustomerManualInvoiceModel}
