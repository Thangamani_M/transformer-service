class TransactionProcessModel {
    // customerId?: string;
    // customerName?: string;
    // customerType?: string;
    // siteAddress?: string;
    // contactNumber?: string;
    // emailAddress?: string;    
    // debtorName?: string;
    // debtorCode?: string;
    // debtorBDINumber?: string;
    // debtorContact?: string;
    // debtorEmail?: string;

    invoicePaymentAndOverrideId?: string;
    creditNoteDocumentDate?: string;
    invoiceTransactionTypeId?: string;
    invoiceTransactionDescriptionId?: string;
    invoiceTransactionDescriptionSubTypeId?: string;
    transactionNotes?: string;
    sendCustomerCopy?: boolean;
    isSendToDOAApproval?: boolean;

  
    constructor(transactionProcessModel: TransactionProcessModel) {
        // this.invoicePaymentAndOverrideId = transactionProcessModel?.invoicePaymentAndOverrideId == undefined ? '' : transactionProcessModel?.invoicePaymentAndOverrideId;
        // this.creditNoteDocumentDate = transactionProcessModel?.creditNoteDocumentDate == undefined ? '' : transactionProcessModel?.creditNoteDocumentDate;
        // this.invoiceTransactionTypeId = transactionProcessModel?.invoiceTransactionTypeId == undefined ? '' : transactionProcessModel?.invoiceTransactionTypeId;
        // this.invoiceTransactionDescriptionId = transactionProcessModel?.invoiceTransactionDescriptionId == undefined ? '' : transactionProcessModel?.invoiceTransactionDescriptionId;
        // this.invoiceTransactionDescriptionSubTypeId = transactionProcessModel?.invoiceTransactionDescriptionSubTypeId == undefined ? '' : transactionProcessModel?.invoiceTransactionDescriptionSubTypeId;
        // this.transactionNotes = transactionProcessModel?.transactionNotes == undefined ? '' : transactionProcessModel?.transactionNotes;
        // this.isSendCustomerCopy = transactionProcessModel?.isSendCustomerCopy == undefined ? false : transactionProcessModel?.isSendCustomerCopy;
        // this.isSendToDOAApproval = transactionProcessModel?.isSendToDOAApproval == undefined ? false : transactionProcessModel?.isSendToDOAApproval;
        this.invoicePaymentAndOverrideId = transactionProcessModel?.invoicePaymentAndOverrideId != undefined ? transactionProcessModel?.invoicePaymentAndOverrideId : '';
        this.creditNoteDocumentDate = transactionProcessModel?.creditNoteDocumentDate != undefined ? transactionProcessModel?.creditNoteDocumentDate : '';
        this.invoiceTransactionTypeId = transactionProcessModel?.invoiceTransactionTypeId != undefined ? transactionProcessModel?.invoiceTransactionTypeId : '';
        this.invoiceTransactionDescriptionId = transactionProcessModel?.invoiceTransactionDescriptionId != undefined ? transactionProcessModel?.invoiceTransactionDescriptionId : '';
        this.invoiceTransactionDescriptionSubTypeId = transactionProcessModel?.invoiceTransactionDescriptionSubTypeId != undefined ? transactionProcessModel?.invoiceTransactionDescriptionSubTypeId : '';
        this.transactionNotes = transactionProcessModel?.transactionNotes != undefined ? transactionProcessModel?.transactionNotes : '';
        this.sendCustomerCopy = transactionProcessModel?.sendCustomerCopy != undefined ? transactionProcessModel?.sendCustomerCopy : false;
        this.isSendToDOAApproval = transactionProcessModel?.isSendToDOAApproval != undefined ? transactionProcessModel?.isSendToDOAApproval : false;
    }
}
export { TransactionProcessModel };

