
class SafeEntryModel {
    constructor(safeEntryModel?: SafeEntryModel) {
        this.customerId = safeEntryModel ? safeEntryModel.customerId == undefined ? '' : safeEntryModel.customerId : '';
        this.contactPersonName = safeEntryModel ? safeEntryModel.contactPersonName == undefined ? null : safeEntryModel.contactPersonName : null;
        this.contactNumber = safeEntryModel ? safeEntryModel.contactNumber == undefined ? null : safeEntryModel.contactNumber : null;
        this.safeEntryDate = safeEntryModel ? safeEntryModel.safeEntryDate == undefined ? null : safeEntryModel.safeEntryDate : null;
        this.startTime = safeEntryModel ? safeEntryModel.startTime == undefined ? null : safeEntryModel.startTime : null;
        this.endTime = safeEntryModel ? safeEntryModel.endTime == undefined ? null : safeEntryModel.endTime : null;
        this.vehicleMake = safeEntryModel ? safeEntryModel.vehicleMake == undefined ? null : safeEntryModel.vehicleMake : null;
        this.vehicleModel = safeEntryModel ? safeEntryModel.vehicleModel == undefined ? null : safeEntryModel.vehicleModel : null;
        this.vehicleRegistrationNumber = safeEntryModel ? safeEntryModel.vehicleRegistrationNumber == undefined ? null : safeEntryModel.vehicleRegistrationNumber : null;
        this.reason = safeEntryModel ? safeEntryModel.reason == undefined ? null : safeEntryModel.reason : null;
        this.comments = safeEntryModel ? safeEntryModel.comments == undefined ? null : safeEntryModel.comments : null;
        this.createdUserId = safeEntryModel ? safeEntryModel.createdUserId == undefined ? null : safeEntryModel.createdUserId : null;
        this.modifiedUserId = safeEntryModel ? safeEntryModel.modifiedUserId == undefined ? null : safeEntryModel.modifiedUserId : null;
        this.vehicleColor = safeEntryModel ? safeEntryModel.vehicleColor == undefined ? null : safeEntryModel.vehicleColor : null;
        this.contactCountryCode = safeEntryModel ? safeEntryModel.contactCountryCode == undefined ? null : safeEntryModel.contactCountryCode : null;
        this.contactNumberValue = safeEntryModel ? safeEntryModel.contactNumberValue == undefined ? null : safeEntryModel.contactNumberValue : null;
        this.agreementId = safeEntryModel ? safeEntryModel.agreementId == undefined ? null : safeEntryModel.agreementId : null;
        this.agreementNumber = safeEntryModel ? safeEntryModel.agreementNumber == undefined ? null : safeEntryModel.agreementNumber : null;
        this.subscriptionServiceId = safeEntryModel ? safeEntryModel.subscriptionServiceId == undefined ? null : safeEntryModel.subscriptionServiceId : null;
        this.subscriptionNumber = safeEntryModel ? safeEntryModel.subscriptionNumber == undefined ? null : safeEntryModel.subscriptionNumber : null;
        this.groupId = safeEntryModel ? safeEntryModel.groupId == undefined ? null : safeEntryModel.groupId : null;
        this.userGroup = safeEntryModel ? safeEntryModel.userGroup == undefined ? null : safeEntryModel.userGroup : null;
        this.balanceSafeEntryCount = safeEntryModel ? safeEntryModel.balanceSafeEntryCount == undefined ? null : safeEntryModel.balanceSafeEntryCount : null;

    }
    customerId?: string
    contactPersonName?: string;
    contactNumber?: string;    
    safeEntryDate?: string;
    startTime?:string;
    endTime?:string;
    vehicleMake?:string;
    vehicleModel?:string;
    vehicleRegistrationNumber?:string;
    vehicleColor?:string;
    reason?:string;
    comments?:string;
    createdUserId?:string;
    modifiedUserId?:string;
    contactCountryCode?:string;
    contactNumberValue?:string;
    agreementId?:string;
    agreementNumber?:string;
    subscriptionServiceId?:string;
    subscriptionNumber?:string;
    groupId?:string;
    userGroup?:string;
    balanceSafeEntryCount?:string;
}
export { SafeEntryModel }