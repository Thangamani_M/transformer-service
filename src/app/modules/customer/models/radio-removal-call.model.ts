class RadioRemovalCallAddEditModel {
    constructor(radioRemovalCallAddEditModel?: RadioRemovalCallAddEditModel) {
        this.createdUserId = radioRemovalCallAddEditModel?.createdUserId ? radioRemovalCallAddEditModel?.createdUserId : '';
        this.notes = radioRemovalCallAddEditModel?.notes ? radioRemovalCallAddEditModel?.notes : '';
        this.techInstruction = radioRemovalCallAddEditModel?.techInstruction ? radioRemovalCallAddEditModel?.techInstruction : '';
        this.basicInfo = radioRemovalCallAddEditModel?.basicInfo ? radioRemovalCallAddEditModel?.basicInfo : {};
        this.serviceInfo = radioRemovalCallAddEditModel?.serviceInfo ? radioRemovalCallAddEditModel?.serviceInfo : {};
        this.nkaServiceCall = radioRemovalCallAddEditModel?.nkaServiceCall ? radioRemovalCallAddEditModel?.nkaServiceCall : {};
        this.appointmentInfo = radioRemovalCallAddEditModel?.appointmentInfo ? radioRemovalCallAddEditModel?.appointmentInfo : {};
        this.callInitiationContacts = radioRemovalCallAddEditModel?.callInitiationContacts ? radioRemovalCallAddEditModel?.callInitiationContacts : [];
        
    }
    createdUserId: string;
    notes: string;
    techInstruction: string;
    basicInfo: RadioRemovalBasicInfoModel | any;
    serviceInfo: RadioRemovalServiceInfoModel | any;
    nkaServiceCall :NkaServiceCallModel | any;
    appointmentInfo: AppointmentInfoModel | any;
    callInitiationContacts: CallInitiationContactsFormArrayModel[];
}

class RadioRemovalBasicInfoModel {
    constructor(basicInfoModel?: RadioRemovalBasicInfoModel) {
        this.callInitiationId = basicInfoModel?.callInitiationId ? basicInfoModel?.callInitiationId : '';
        this.customerId = basicInfoModel?.customerId ? basicInfoModel?.customerId : '';
        this.addressId = basicInfoModel?.addressId ? basicInfoModel?.addressId : '';
        this.isCon = basicInfoModel?.isCon ? basicInfoModel?.isCon : undefined;
        this.isNew = basicInfoModel?.isNew ? basicInfoModel?.isNew : undefined;
        this.isTempDone = basicInfoModel?.isTempDone ? basicInfoModel?.isTempDone : undefined;
        this.isScheduled = basicInfoModel?.isScheduled ? basicInfoModel?.isScheduled : undefined;
        this.isCompleted = basicInfoModel?.isCompleted ? basicInfoModel?.isCompleted : undefined;
        this.isInvoiced = basicInfoModel?.isInvoiced ? basicInfoModel?.isInvoiced : undefined;
        this.isOnHold = basicInfoModel?.isOnHold ? basicInfoModel?.isOnHold : undefined;
        this.isVoid = basicInfoModel?.isVoid ? basicInfoModel?.isVoid : undefined;
        this.isReferToSales = basicInfoModel?.isReferToSales ? basicInfoModel?.isReferToSales : undefined;
        this.isRecall = basicInfoModel?.isRecall ? basicInfoModel?.isRecall : undefined;
        this.callInitiationStatusId = basicInfoModel?.callInitiationStatusId ? basicInfoModel?.callInitiationStatusId : '';
        this.technicianCallTypeId = basicInfoModel?.technicianCallTypeId ? basicInfoModel?.technicianCallTypeId : '';
        this.contactId = basicInfoModel?.contactId ? basicInfoModel?.contactId : '';
        this.contactNumber = basicInfoModel?.contactNumber ? basicInfoModel?.contactNumber : '';
        this.debtorId = basicInfoModel?.debtorId ? basicInfoModel?.debtorId : '';
        this.isAddressVerified = basicInfoModel?.isAddressVerified ? basicInfoModel?.isAddressVerified : undefined;
        this.ismainPartation = basicInfoModel?.ismainPartation ? basicInfoModel?.ismainPartation : undefined;
        this.mainPartationCustomerId = basicInfoModel?.mainPartationCustomerId ? basicInfoModel?.mainPartationCustomerId : '';
        this.alternateContactId = basicInfoModel?.alternateContactId ? basicInfoModel?.alternateContactId : '';
        this.alternateContactNumber = basicInfoModel?.alternateContactNumber ? basicInfoModel?.alternateContactNumber : '';
        this.priorityid = basicInfoModel?.priorityid ? basicInfoModel?.priorityid : '';
        this.panelTypeConfigId = basicInfoModel?.panelTypeConfigId ? basicInfoModel?.panelTypeConfigId : '';
        this.feedbackId = basicInfoModel?.feedbackId ? basicInfoModel?.feedbackId : '';
        this.isPRCall = basicInfoModel?.isPRCall ? basicInfoModel?.isPRCall : undefined;
        this.isSaveOffer = basicInfoModel?.isSaveOffer ? basicInfoModel?.isSaveOffer : undefined;
        this.isInstallPreCheck = basicInfoModel?.isInstallPreCheck ? basicInfoModel?.isInstallPreCheck : undefined;
        this.isInWarrenty = basicInfoModel?.isInWarrenty ? basicInfoModel?.isInWarrenty : undefined;
        this.isRecall = basicInfoModel?.isRecall ? basicInfoModel?.isRecall : undefined;
        this.isCode50 = basicInfoModel?.isCode50 ? basicInfoModel?.isCode50 : undefined;
        this.specialProjectId = basicInfoModel?.specialProjectId ? basicInfoModel?.specialProjectId : '';
        this.partitions = basicInfoModel?.partitions ? basicInfoModel?.partitions : [];
    }
    callInitiationId: string;
    customerId: string;
    addressId: string;
    isCon: boolean;
    isNew: boolean;
    isScheduled: boolean;
    isTempDone: boolean;
    isCompleted: boolean;
    isInvoiced: boolean;
    isOnHold: boolean;
    isVoid: boolean;
    isReferToSales: boolean;
    isRecall: boolean;
    callInitiationStatusId: string;
    technicianCallTypeId: string;
    contactId: string;
    contactNumber: string;
    debtorId: string;
    isAddressVerified: boolean;
    ismainPartation: boolean;
    mainPartationCustomerId: string;
    alternateContactId: string;
    alternateContactNumber: string;
    priorityid: string;
    panelTypeConfigId: string;
    feedbackId: string;
    isPRCall: boolean;
    isSaveOffer: boolean;
    isInstallPreCheck: boolean;
    isInWarrenty: boolean;
    isCode50: boolean;
    specialProjectId: string;
    partitions: PartitionModelFormArrayModel[];
}

class PartitionModelFormArrayModel {
    constructor(partitionModelFormArrayModel?: PartitionModelFormArrayModel) {
        this.partitionId = partitionModelFormArrayModel?.partitionId ? partitionModelFormArrayModel?.partitionId : '';
        this.partitionCode = partitionModelFormArrayModel?.partitionCode ? partitionModelFormArrayModel?.partitionCode : '';
        this.partitionName = partitionModelFormArrayModel?.partitionName ? partitionModelFormArrayModel?.partitionName : '';
        this.mainPartition = partitionModelFormArrayModel?.mainPartition ? partitionModelFormArrayModel?.mainPartition : '';
    }
    partitionId: string;
    partitionCode: string;
    partitionName: string;
    mainPartition: string;
}

class RadioRemovalServiceInfoModel {
    constructor(serviceInfoModel?: RadioRemovalServiceInfoModel) {
        this.callInitiationServiceInfoId = serviceInfoModel?.callInitiationServiceInfoId ? serviceInfoModel?.callInitiationServiceInfoId : '';
        this.systemTypeId = serviceInfoModel?.systemTypeId ? serviceInfoModel?.systemTypeId : '';
        this.systemTypeSubCategoryId = serviceInfoModel?.systemTypeSubCategoryId ? serviceInfoModel?.systemTypeSubCategoryId : '';
        this.serviceSubTypeId = serviceInfoModel?.serviceSubTypeId ? serviceInfoModel?.serviceSubTypeId : '';
        this.faultDescriptionId = serviceInfoModel?.faultDescriptionId ? serviceInfoModel?.faultDescriptionId : '';
        this.jobTypeId = serviceInfoModel?.jobTypeId ? serviceInfoModel?.jobTypeId : '';
        this.description = serviceInfoModel?.description ? serviceInfoModel?.description : '';
    }
    callInitiationServiceInfoId: string;
    systemTypeId: string;
    systemTypeSubCategoryId: string;
    serviceSubTypeId: string;
    faultDescriptionId: string;
    jobTypeId: string;
    description: string;
}

class NkaServiceCallModel {
    constructor(nkaServiceCallModel?: NkaServiceCallModel) {
        this.poNumber = nkaServiceCallModel?.poNumber ? nkaServiceCallModel?.poNumber : '';
        this.poAttachments = nkaServiceCallModel?.poAttachments ? nkaServiceCallModel?.poAttachments : '';
        this.path = nkaServiceCallModel?.path ? nkaServiceCallModel?.path : '';
        this.poAmount = nkaServiceCallModel?.poAmount ? nkaServiceCallModel?.poAmount : '';
        this.isPOAccepted = nkaServiceCallModel?.isPOAccepted ? nkaServiceCallModel?.isPOAccepted : undefined;
        this.isPONotAccepted = nkaServiceCallModel?.isPONotAccepted ? nkaServiceCallModel?.isPONotAccepted : undefined;
        this.attachmentUrl = nkaServiceCallModel?.attachmentUrl ? nkaServiceCallModel?.attachmentUrl : '';
        this.poComment = nkaServiceCallModel?.poComment ? nkaServiceCallModel?.poComment : '';
    }
    poNumber: string;
    poAttachments: string;
    path: string;
    poAmount: string;
    isPOAccepted: boolean;
    isPONotAccepted: boolean;
    attachmentUrl: string;
    poComment: string;
}

class AppointmentInfoModel {
    constructor(appointmentInfoModel?: AppointmentInfoModel) {
        this.nextAvailableAppointment = appointmentInfoModel?.nextAvailableAppointment ? appointmentInfoModel?.nextAvailableAppointment : '';
        this.scheduledStartDate = appointmentInfoModel?.scheduledStartDate ? appointmentInfoModel?.scheduledStartDate : '';
        this.scheduledEndDate = appointmentInfoModel?.scheduledEndDate ? appointmentInfoModel?.scheduledEndDate : '';
        this.estimatedTime = appointmentInfoModel?.estimatedTime ? appointmentInfoModel?.estimatedTime : '';
        this.estimatedTimeStatic = appointmentInfoModel?.estimatedTimeStatic ? appointmentInfoModel?.estimatedTimeStatic : '';
        this.actualTime = appointmentInfoModel?.actualTime ? appointmentInfoModel?.actualTime : '';
        this.technician = appointmentInfoModel?.technician ? appointmentInfoModel?.technician : '';
        this.nextAvailableAppointmentCheckbox = appointmentInfoModel?.nextAvailableAppointmentCheckbox ? appointmentInfoModel?.nextAvailableAppointmentCheckbox : undefined;
        this.isAppointmentChangable = appointmentInfoModel?.isAppointmentChangable ? appointmentInfoModel?.isAppointmentChangable : undefined;
        this.techArea = appointmentInfoModel?.techArea ? appointmentInfoModel?.techArea : '';
        this.wireman = appointmentInfoModel?.wireman ? appointmentInfoModel?.wireman : '';
        this.diagnosisId = appointmentInfoModel?.diagnosisId ? appointmentInfoModel?.diagnosisId : '';
        this.appointmentId = appointmentInfoModel?.appointmentId ? appointmentInfoModel?.appointmentId : '';
        this.appointmentLogId = appointmentInfoModel?.appointmentLogId ? appointmentInfoModel?.appointmentLogId : '';
    }
    nextAvailableAppointment: string;
    scheduledStartDate: string;
    scheduledEndDate: string;
    estimatedTime: string;
    estimatedTimeStatic: string;
    actualTime: string;
    technician: string;
    nextAvailableAppointmentCheckbox: boolean;
    isAppointmentChangable: boolean;
    techArea: string;
    wireman: string;
    diagnosisId: string;
    appointmentId:string;
    appointmentLogId:string;
}

class CallInitiationContactsFormArrayModel {
    constructor(callInitiationContactsFormArrayModel?: CallInitiationContactsFormArrayModel) {
        this.callInitiationId = callInitiationContactsFormArrayModel?.callInitiationId ? callInitiationContactsFormArrayModel?.callInitiationId : null;
        this.callInitiationContactId = callInitiationContactsFormArrayModel?.callInitiationContactId ? callInitiationContactsFormArrayModel?.callInitiationContactId : null;
        this.keyholderId = callInitiationContactsFormArrayModel?.keyholderId ? callInitiationContactsFormArrayModel?.keyholderId : '';
        this.contactName = callInitiationContactsFormArrayModel?.contactName ? callInitiationContactsFormArrayModel?.contactName : '';
        this.contactNo = callInitiationContactsFormArrayModel?.contactNo ? callInitiationContactsFormArrayModel?.contactNo : '';
        this.contactNoCountryCode = callInitiationContactsFormArrayModel?.contactNoCountryCode ? callInitiationContactsFormArrayModel?.contactNoCountryCode : '';
        this.isPrimary = callInitiationContactsFormArrayModel?.isPrimary ? callInitiationContactsFormArrayModel?.isPrimary : undefined;
    }
    callInitiationId: string;
    callInitiationContactId: string;
    keyholderId: string;
    contactName: string;
    contactNo: string;
    contactNoCountryCode: string;
    isPrimary: boolean;
}

export { RadioRemovalCallAddEditModel, RadioRemovalBasicInfoModel, PartitionModelFormArrayModel, RadioRemovalServiceInfoModel, AppointmentInfoModel, NkaServiceCallModel, CallInitiationContactsFormArrayModel };
