class UpgradeLeadDetailModel {
    constructor(upgradeLeadDetailModel: UpgradeLeadDetailModel) {
        this.titleId = upgradeLeadDetailModel?.titleId == undefined ? '' : upgradeLeadDetailModel?.titleId;
        this.firstName = upgradeLeadDetailModel?.firstName == undefined ? '' : upgradeLeadDetailModel?.firstName;
        this.lastName = upgradeLeadDetailModel?.lastName == undefined ? '' : upgradeLeadDetailModel?.lastName;
        this.sourceId = upgradeLeadDetailModel?.sourceId == undefined ? '' : upgradeLeadDetailModel?.sourceId;
        this.addressId = upgradeLeadDetailModel?.addressId == undefined ? '' : upgradeLeadDetailModel?.addressId;
        this.addressName = upgradeLeadDetailModel?.addressName == undefined ? '' : upgradeLeadDetailModel?.addressId;
        this.customerId = upgradeLeadDetailModel?.customerId == undefined ? '' : upgradeLeadDetailModel?.customerId;
        this.createdUserId = upgradeLeadDetailModel?.createdUserId == undefined ? '' : upgradeLeadDetailModel?.createdUserId;
        this.leadCategoryId = upgradeLeadDetailModel?.leadCategoryId == undefined ? '' : upgradeLeadDetailModel?.leadCategoryId;
        this.notes = upgradeLeadDetailModel?.notes == undefined ? '' : upgradeLeadDetailModel?.notes;
    }
    titleId?: string;
    firstName?: string;
    lastName?: string;
    sourceId?: string;
    addressId?: string;
    addressName?: string;
    customerId?: string;
    createdUserId?: string;
    leadCategoryId?: string;
    notes?: string;
}

export {UpgradeLeadDetailModel}