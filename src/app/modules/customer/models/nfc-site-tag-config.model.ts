class NFCSiteTagConfigModel {
    constructor(nFCSiteTagConfigModel?: NFCSiteTagConfigModel) {
        this.customerNFCSiteTagConfigId=nFCSiteTagConfigModel?nFCSiteTagConfigModel.customerNFCSiteTagConfigId==undefined? null:nFCSiteTagConfigModel.customerNFCSiteTagConfigId:null;
        this.createdUserId=nFCSiteTagConfigModel?nFCSiteTagConfigModel.createdUserId==undefined?'':nFCSiteTagConfigModel.createdUserId:'';
        this.customerId=nFCSiteTagConfigModel?nFCSiteTagConfigModel.customerId==undefined?'':nFCSiteTagConfigModel.customerId:'';
        this.customerAddressId=nFCSiteTagConfigModel?nFCSiteTagConfigModel.customerAddressId==undefined?'':nFCSiteTagConfigModel.customerAddressId:'';
        this.tagId=nFCSiteTagConfigModel?nFCSiteTagConfigModel.tagId==undefined?'':nFCSiteTagConfigModel.tagId:'';
        this.description=nFCSiteTagConfigModel?nFCSiteTagConfigModel.description==undefined?'':nFCSiteTagConfigModel.description:'';
        this.modifiedUserId=nFCSiteTagConfigModel?nFCSiteTagConfigModel.modifiedUserId==undefined?'':nFCSiteTagConfigModel.modifiedUserId:'';
        this.location=nFCSiteTagConfigModel?nFCSiteTagConfigModel.location==undefined?'':nFCSiteTagConfigModel.location:'';
        this.longitude=nFCSiteTagConfigModel?nFCSiteTagConfigModel.longitude==undefined?'':nFCSiteTagConfigModel.longitude:'';
        this.latitude=nFCSiteTagConfigModel?nFCSiteTagConfigModel.latitude==undefined?'':nFCSiteTagConfigModel.latitude:'';
        
    }
   customerNFCSiteTagConfigId: any;
   createdUserId:string;
   customerId:string;
   customerAddressId:string;
   tagId:string;
   description: string;
   modifiedUserId: string;
   location: string;
   longitude: string;
   latitude: string
}

export { NFCSiteTagConfigModel }