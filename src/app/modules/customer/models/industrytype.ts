export class IndustryType {
    industryCategoryId: string;
    industryCategoryName: string;
    description: string;
    cssClass: string;
    createdDate: Date;
  }
  