class SiteHazardModel {
    constructor(SiteHazardConfigModel?: SiteHazardModel) {
        this.isViciousDogs = SiteHazardConfigModel ? SiteHazardConfigModel.isViciousDogs == undefined ? false : SiteHazardConfigModel.isViciousDogs : false;
        this.isConfinedSpace = SiteHazardConfigModel ? SiteHazardConfigModel.isConfinedSpace == undefined ? false : SiteHazardConfigModel.isConfinedSpace : false;
        this.isExtendedLadders = SiteHazardConfigModel ? SiteHazardConfigModel.isExtendedLadders == undefined ? false : SiteHazardConfigModel.isExtendedLadders : false;
        this.isConstructionSite = SiteHazardConfigModel ? SiteHazardConfigModel.isConstructionSite == undefined ? false : SiteHazardConfigModel.isConstructionSite : false;
        this.isUnsafe = SiteHazardConfigModel ? SiteHazardConfigModel.isUnsafe == undefined ? false : SiteHazardConfigModel.isUnsafe : false;
        this.isConfinedSpaceInstalled = SiteHazardConfigModel ? SiteHazardConfigModel.isConfinedSpaceInstalled == undefined ? false : SiteHazardConfigModel.isConfinedSpaceInstalled : false;
        this.isHarmfulChemical = SiteHazardConfigModel ? SiteHazardConfigModel.isHarmfulChemical == undefined ? false : SiteHazardConfigModel.isHarmfulChemical : false;
        this.isLoudNoiseArea = SiteHazardConfigModel ? SiteHazardConfigModel.isLoudNoiseArea == undefined ? false : SiteHazardConfigModel.isLoudNoiseArea : false;
        this.isAsbestos = SiteHazardConfigModel ? SiteHazardConfigModel.isAsbestos == undefined ? false : SiteHazardConfigModel.isAsbestos : false;
        this.confinedSpaceDescription = SiteHazardConfigModel ? SiteHazardConfigModel.confinedSpaceDescription == undefined ? '' : SiteHazardConfigModel.confinedSpaceDescription : '';
        this.extendedLadderDescription = SiteHazardConfigModel ? SiteHazardConfigModel.extendedLadderDescription == undefined ? '' : SiteHazardConfigModel.extendedLadderDescription : '';
        this.constructionSiteDescription = SiteHazardConfigModel ? SiteHazardConfigModel.constructionSiteDescription == undefined ? '' : SiteHazardConfigModel.constructionSiteDescription : '';
        this.unsafeDescription = SiteHazardConfigModel ? SiteHazardConfigModel.unsafeDescription == undefined ? '' : SiteHazardConfigModel.unsafeDescription : '';
        this.confinedSpaceInstalledDescription = SiteHazardConfigModel ? SiteHazardConfigModel.confinedSpaceInstalledDescription == undefined ? '' : SiteHazardConfigModel.confinedSpaceInstalledDescription : '';
        this.harmfulChemicalDescription = SiteHazardConfigModel ? SiteHazardConfigModel.harmfulChemicalDescription == undefined ? '' : SiteHazardConfigModel.harmfulChemicalDescription : '';
        this.loudNoiseAreaDescription = SiteHazardConfigModel ? SiteHazardConfigModel.loudNoiseAreaDescription == undefined ? '' : SiteHazardConfigModel.loudNoiseAreaDescription : '';
        this.asbestosDescription = SiteHazardConfigModel ? SiteHazardConfigModel.asbestosDescription == undefined ? '' : SiteHazardConfigModel.asbestosDescription : '';
        this.noOfDogs = SiteHazardConfigModel ? SiteHazardConfigModel.noOfDogs == undefined ? '' : SiteHazardConfigModel.noOfDogs : '';
        this.dogType = SiteHazardConfigModel ? SiteHazardConfigModel.dogType == undefined ? '' : SiteHazardConfigModel.dogType : '';
        this.viciousDogsDescription = SiteHazardConfigModel ? SiteHazardConfigModel.viciousDogsDescription == undefined ? '' : SiteHazardConfigModel.viciousDogsDescription : '';
    
    
    }
    viciousDogsDescription:string;
    dogType:string;
    noOfDogs:string;
    isViciousDogs?:boolean;
    isConfinedSpace?:boolean;
    confinedSpaceDescription?:string;
    isExtendedLadders?:boolean;
    extendedLadderDescription?:string;
    isConstructionSite?:boolean;
    constructionSiteDescription?:string;
    isUnsafe?:boolean;
    unsafeDescription?:string;
    isConfinedSpaceInstalled?:boolean;
    confinedSpaceInstalledDescription?:string;
    isHarmfulChemical?:boolean;
    harmfulChemicalDescription?:string;
    isLoudNoiseArea?:boolean;
    loudNoiseAreaDescription?:string;
    isAsbestos?:boolean;
    asbestosDescription?:string;


}
export { SiteHazardModel };

