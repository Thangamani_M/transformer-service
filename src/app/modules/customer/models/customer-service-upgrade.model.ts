
class upgradeServiceRequestModel {
  constructor(upgradeServiceRequest?: upgradeServiceRequestModel) {
    this.upgradeServiceRequestId = upgradeServiceRequest ? upgradeServiceRequest.upgradeServiceRequestId == undefined ? "" : upgradeServiceRequest.upgradeServiceRequestId : "";
    this.contractId = upgradeServiceRequest ? upgradeServiceRequest.contractId == undefined ? "" : upgradeServiceRequest.contractId : "";
    this.addressId = upgradeServiceRequest ? upgradeServiceRequest.addressId == undefined ? "" : upgradeServiceRequest.addressId : "";
    this.customerId = upgradeServiceRequest ? upgradeServiceRequest.customerId == undefined ? "" : upgradeServiceRequest.customerId : "";
    this.lssId = upgradeServiceRequest ? upgradeServiceRequest.lssId == undefined ? "" : upgradeServiceRequest.lssId : "";
    this.voluntaryContributionAmount = upgradeServiceRequest ? upgradeServiceRequest.voluntaryContributionAmount == undefined ? 0 : upgradeServiceRequest.voluntaryContributionAmount : 0;

  }
  upgradeServiceRequestId: string
  contractId: string
  customerId: string
  addressId: string
  lssId: string
  voluntaryContributionAmount: number

}
class DowngradeServiceRequestModel {
  constructor(downgradeServiceRequestModel?: DowngradeServiceRequestModel) {
    this.downgradeServiceRequestId = downgradeServiceRequestModel ? downgradeServiceRequestModel.downgradeServiceRequestId == undefined ? "" : downgradeServiceRequestModel.downgradeServiceRequestId : "";
    this.contractId = downgradeServiceRequestModel ? downgradeServiceRequestModel.contractId == undefined ? "" : downgradeServiceRequestModel.contractId : "";
    this.addressId = downgradeServiceRequestModel ? downgradeServiceRequestModel.addressId == undefined ? "" : downgradeServiceRequestModel.addressId : "";
    this.customerId = downgradeServiceRequestModel ? downgradeServiceRequestModel.customerId == undefined ? "" : downgradeServiceRequestModel.customerId : "";
    this.lssId = downgradeServiceRequestModel ? downgradeServiceRequestModel.lssId == undefined ? "" : downgradeServiceRequestModel.lssId : "";
    this.voluntaryContributionAmount = downgradeServiceRequestModel ? downgradeServiceRequestModel.voluntaryContributionAmount == undefined ? 0 : downgradeServiceRequestModel.voluntaryContributionAmount : 0;

  }
  downgradeServiceRequestId: string
  contractId: string
  customerId: string
  addressId: string
  lssId: string
  voluntaryContributionAmount: number

}



class UpgradeServicesList {
  constructor(upgradeServiceList?: UpgradeServicesList) {
    this.upgradeServiceId = upgradeServiceList ? upgradeServiceList.upgradeServiceId == undefined ? "" : upgradeServiceList.upgradeServiceId : "";
    this.servicePriceId = upgradeServiceList ? upgradeServiceList.servicePriceId == undefined ? "" : upgradeServiceList.servicePriceId : "";
    this.qty = upgradeServiceList ? upgradeServiceList.qty == undefined ? 0 : upgradeServiceList.qty : 0;
    this.servicePrice = upgradeServiceList ? upgradeServiceList.servicePrice == undefined ? 0 : upgradeServiceList.servicePrice : 0;
    this.taxPrice = upgradeServiceList ? upgradeServiceList.taxPrice == undefined ? 0 : upgradeServiceList.taxPrice : 0;
    this.totalPrice = upgradeServiceList ? upgradeServiceList.totalPrice == undefined ? 0 : upgradeServiceList.totalPrice : 0;
    this.unitPrice = upgradeServiceList ? upgradeServiceList.unitPrice == undefined ? 0 : upgradeServiceList.unitPrice : 0;

  }
  upgradeServiceId: string
  servicePriceId: string
  qty: number
  servicePrice: number
  taxPrice: number
  totalPrice: number
  unitPrice: number

}

class DowngradeServicesList {
  constructor(downgradeServicesList?: DowngradeServicesList) {
    this.downgradeServiceId = downgradeServicesList ? downgradeServicesList.downgradeServiceId == undefined ? "" : downgradeServicesList.downgradeServiceId : "";
    this.servicePriceId = downgradeServicesList ? downgradeServicesList.servicePriceId == undefined ? "" : downgradeServicesList.servicePriceId : "";
    this.qty = downgradeServicesList ? downgradeServicesList.qty == undefined ? 0 : downgradeServicesList.qty : 0;
    this.servicePrice = downgradeServicesList ? downgradeServicesList.servicePrice == undefined ? 0 : downgradeServicesList.servicePrice : 0;
    this.taxPrice = downgradeServicesList ? downgradeServicesList.taxPrice == undefined ? 0 : downgradeServicesList.taxPrice : 0;
    this.totalPrice = downgradeServicesList ? downgradeServicesList.totalPrice == undefined ? 0 : downgradeServicesList.totalPrice : 0;
    this.unitPrice = downgradeServicesList ? downgradeServicesList.unitPrice == undefined ? 0 : downgradeServicesList.unitPrice : 0;

  }
  downgradeServiceId: string
  servicePriceId: string
  qty: number
  servicePrice: number
  taxPrice: number
  totalPrice: number
  unitPrice: number

}


class UpgradeCustomerServiceModel {
  constructor(upgradeServiceList?: UpgradeCustomerServiceModel) {
    this.createdUserId = upgradeServiceList ? upgradeServiceList.createdUserId == undefined ? "" : upgradeServiceList.createdUserId : "";
    this.upgradeServiceRequestDTO = upgradeServiceList ? upgradeServiceList.upgradeServiceRequestDTO == undefined ? '' : upgradeServiceList.upgradeServiceRequestDTO : {};
    this.upgradeServiceDTOs = upgradeServiceList ? upgradeServiceList.upgradeServiceDTOs == undefined ? [] : upgradeServiceList.upgradeServiceDTOs : [];
  }
  createdUserId: string
  upgradeServiceRequestDTO: upgradeServiceRequestModel | any
  upgradeServiceDTOs: UpgradeServicesList[]


}

class CustomerServicePriceAdjustments {
  constructor(customerServicePriceAdjustments?: CustomerServicePriceAdjustments) {
    this.upgradeServiceAdjustCostingId = customerServicePriceAdjustments == undefined ? null : customerServicePriceAdjustments.upgradeServiceAdjustCostingId == undefined ? null : customerServicePriceAdjustments.upgradeServiceAdjustCostingId;
    this.serviceCategoryId = customerServicePriceAdjustments == undefined ? null : customerServicePriceAdjustments.serviceCategoryId == undefined ? null : customerServicePriceAdjustments.serviceCategoryId;
    this.serviceCategoryName = customerServicePriceAdjustments == undefined ? null : customerServicePriceAdjustments.serviceCategoryName == undefined ? null : customerServicePriceAdjustments.serviceCategoryName;
    this.isDiscountAmount = customerServicePriceAdjustments == undefined ? null : customerServicePriceAdjustments.isDiscountAmount == undefined ? null : customerServicePriceAdjustments.isDiscountAmount;
    this.isDiscountPercentage = customerServicePriceAdjustments == undefined ? true : customerServicePriceAdjustments.isDiscountPercentage == undefined ? true : customerServicePriceAdjustments.isDiscountPercentage;
    this.amount = customerServicePriceAdjustments == undefined ? 0 : customerServicePriceAdjustments.amount == undefined ? 0 : customerServicePriceAdjustments.amount;
    this.subTotal = customerServicePriceAdjustments == undefined ? 0 : customerServicePriceAdjustments.subTotal == undefined ? 0 : customerServicePriceAdjustments.subTotal;
    this.discountAmount = customerServicePriceAdjustments == undefined ? 0 : customerServicePriceAdjustments.discountAmount == undefined ? 0 : customerServicePriceAdjustments.discountAmount;
    this.adjustedTotal = customerServicePriceAdjustments == undefined ? 0 : customerServicePriceAdjustments.adjustedTotal == undefined ? 0 : customerServicePriceAdjustments.adjustedTotal;
    this.vat = customerServicePriceAdjustments == undefined ? 0 : customerServicePriceAdjustments.vat == undefined ? 0 : customerServicePriceAdjustments.vat;
    this.totalAmount = customerServicePriceAdjustments == undefined ? 0 : customerServicePriceAdjustments.totalAmount == undefined ? 0 : customerServicePriceAdjustments.totalAmount;
    this.servicePercentage = customerServicePriceAdjustments == undefined ? 0 : customerServicePriceAdjustments.servicePercentage == undefined ? 0 : customerServicePriceAdjustments.servicePercentage;
    this.taxPercentage = customerServicePriceAdjustments == undefined ? 0 : customerServicePriceAdjustments.taxPercentage == undefined ? 0 : customerServicePriceAdjustments.taxPercentage;
    this.discountPercentage = customerServicePriceAdjustments == undefined ? 0 : customerServicePriceAdjustments.discountPercentage == undefined ? 0 : customerServicePriceAdjustments.discountPercentage;
  }
  upgradeServiceAdjustCostingId: string;
  serviceCategoryId?: string;
  serviceCategoryName: string;
  isDiscountAmount?: boolean;
  isDiscountPercentage?: boolean;
  discountPercentage?: number;
  amount?: number;
  subTotal?: number;
  discountAmount: number;
  adjustedTotal: number;
  vat: number;
  totalAmount: number;
  servicePercentage: number;
  taxPercentage: number;
}


class DowngradeServicePriceAdjustments {
  constructor(downgradeServicePriceAdjustments?: DowngradeServicePriceAdjustments) {
    this.downgradeServiceAdjustCostingId = downgradeServicePriceAdjustments == undefined ? null : downgradeServicePriceAdjustments.downgradeServiceAdjustCostingId == undefined ? null : downgradeServicePriceAdjustments.downgradeServiceAdjustCostingId;
    this.serviceCategoryId = downgradeServicePriceAdjustments == undefined ? null : downgradeServicePriceAdjustments.serviceCategoryId == undefined ? null : downgradeServicePriceAdjustments.serviceCategoryId;
    this.serviceCategoryName = downgradeServicePriceAdjustments == undefined ? null : downgradeServicePriceAdjustments.serviceCategoryName == undefined ? null : downgradeServicePriceAdjustments.serviceCategoryName;
    this.isDiscountAmount = downgradeServicePriceAdjustments == undefined ? null : downgradeServicePriceAdjustments.isDiscountAmount == undefined ? null : downgradeServicePriceAdjustments.isDiscountAmount;
    this.isDiscountPercentage = downgradeServicePriceAdjustments == undefined ? true : downgradeServicePriceAdjustments.isDiscountPercentage == undefined ? true : downgradeServicePriceAdjustments.isDiscountPercentage;
    this.discountPercentage = downgradeServicePriceAdjustments == undefined ? 0 : downgradeServicePriceAdjustments.discountPercentage == undefined ? 0 : downgradeServicePriceAdjustments.discountPercentage;
    this.amount = downgradeServicePriceAdjustments == undefined ? 0 : downgradeServicePriceAdjustments.amount == undefined ? 0 : downgradeServicePriceAdjustments.amount;
    this.subTotal = downgradeServicePriceAdjustments == undefined ? 0 : downgradeServicePriceAdjustments.subTotal == undefined ? 0 : downgradeServicePriceAdjustments.subTotal;
    this.discountAmount = downgradeServicePriceAdjustments == undefined ? 0 : downgradeServicePriceAdjustments.discountAmount == undefined ? 0 : downgradeServicePriceAdjustments.discountAmount;
    this.adjustedTotal = downgradeServicePriceAdjustments == undefined ? 0 : downgradeServicePriceAdjustments.adjustedTotal == undefined ? 0 : downgradeServicePriceAdjustments.adjustedTotal;
    this.vat = downgradeServicePriceAdjustments == undefined ? 0 : downgradeServicePriceAdjustments.vat == undefined ? 0 : downgradeServicePriceAdjustments.vat;
    this.totalAmount = downgradeServicePriceAdjustments == undefined ? 0 : downgradeServicePriceAdjustments.totalAmount == undefined ? 0 : downgradeServicePriceAdjustments.totalAmount;
    this.servicePercentage = downgradeServicePriceAdjustments == undefined ? 0 : downgradeServicePriceAdjustments.servicePercentage == undefined ? 0 : downgradeServicePriceAdjustments.servicePercentage;
    this.taxPercentage = downgradeServicePriceAdjustments == undefined ? 0 : downgradeServicePriceAdjustments.taxPercentage == undefined ? 0 : downgradeServicePriceAdjustments.taxPercentage;
  }
  downgradeServiceAdjustCostingId: string;
  serviceCategoryId?: string;
  serviceCategoryName: string;
  isDiscountAmount?: boolean;
  isDiscountPercentage?: boolean;
  discountPercentage?: number;
  amount?: number;
  subTotal?: number;
  discountAmount: number;
  adjustedTotal: number;
  vat: number;
  totalAmount: number;
  servicePercentage: number;
  taxPercentage: number;
}



class CustomerServicePriceAdjustModel {
  constructor(customerServicePriceAdjustModel?: CustomerServicePriceAdjustModel) {
    this.upgradeServiceAdjustCostingPostDTOs = customerServicePriceAdjustModel == undefined ? [] : customerServicePriceAdjustModel.upgradeServiceAdjustCostingPostDTOs == undefined ? [] : customerServicePriceAdjustModel.upgradeServiceAdjustCostingPostDTOs;
    this.createdUserId = customerServicePriceAdjustModel == undefined ? null : customerServicePriceAdjustModel.createdUserId == undefined ? null : customerServicePriceAdjustModel.createdUserId;
    this.doaConfigId = customerServicePriceAdjustModel == undefined ? null : customerServicePriceAdjustModel.doaConfigId == undefined ? null : customerServicePriceAdjustModel.doaConfigId;
    this.upgradeServiceRequestId = customerServicePriceAdjustModel == undefined ? null : customerServicePriceAdjustModel.upgradeServiceRequestId == undefined ? null : customerServicePriceAdjustModel.upgradeServiceRequestId;
    this.serviceDiscountAmount = customerServicePriceAdjustModel == undefined ? 0 : customerServicePriceAdjustModel.serviceDiscountAmount == undefined ? 0 : customerServicePriceAdjustModel.serviceDiscountAmount;
    this.serviceDiscountPercentage = customerServicePriceAdjustModel == undefined ? 0 : customerServicePriceAdjustModel.serviceDiscountPercentage == undefined ? 0 : customerServicePriceAdjustModel.serviceDiscountPercentage;
    this.customerId = customerServicePriceAdjustModel == undefined ? "" : customerServicePriceAdjustModel.customerId == undefined ? "" : customerServicePriceAdjustModel.customerId;
    this.addressId = customerServicePriceAdjustModel == undefined ? "" : customerServicePriceAdjustModel.addressId == undefined ? "" : customerServicePriceAdjustModel.addressId;
    this.contractId = customerServicePriceAdjustModel == undefined ? "" : customerServicePriceAdjustModel.contractId == undefined ? "" : customerServicePriceAdjustModel.contractId;

  }
  upgradeServiceAdjustCostingPostDTOs: CustomerServicePriceAdjustments[];
  createdUserId: string;
  doaConfigId: string;
  upgradeServiceRequestId: string;
  customerId: string;
  contractId: string;
  addressId: string;
  serviceDiscountAmount: number;
  serviceDiscountPercentage: number;
}

class DowngradeServicePriceAdjustModel {
  constructor(downgradeServicePriceAdjustModel?: DowngradeServicePriceAdjustModel) {
    this.downgradeServiceAdjustCostingPostDTOs = downgradeServicePriceAdjustModel == undefined ? [] : downgradeServicePriceAdjustModel.downgradeServiceAdjustCostingPostDTOs == undefined ? [] : downgradeServicePriceAdjustModel.downgradeServiceAdjustCostingPostDTOs;
    this.createdUserId = downgradeServicePriceAdjustModel == undefined ? null : downgradeServicePriceAdjustModel.createdUserId == undefined ? null : downgradeServicePriceAdjustModel.createdUserId;
    this.doaConfigId = downgradeServicePriceAdjustModel == undefined ? null : downgradeServicePriceAdjustModel.doaConfigId == undefined ? null : downgradeServicePriceAdjustModel.doaConfigId;
    this.downgradeServiceRequestId = downgradeServicePriceAdjustModel == undefined ? null : downgradeServicePriceAdjustModel.downgradeServiceRequestId == undefined ? null : downgradeServicePriceAdjustModel.downgradeServiceRequestId;
    this.serviceDiscountAmount = downgradeServicePriceAdjustModel == undefined ? 0 : downgradeServicePriceAdjustModel.serviceDiscountAmount == undefined ? 0 : downgradeServicePriceAdjustModel.serviceDiscountAmount;
    this.serviceDiscountPercentage = downgradeServicePriceAdjustModel == undefined ? 0 : downgradeServicePriceAdjustModel.serviceDiscountPercentage == undefined ? 0 : downgradeServicePriceAdjustModel.serviceDiscountPercentage;
    this.customerId = downgradeServicePriceAdjustModel == undefined ? "" : downgradeServicePriceAdjustModel.customerId == undefined ? "" : downgradeServicePriceAdjustModel.customerId;
    this.addressId = downgradeServicePriceAdjustModel == undefined ? "" : downgradeServicePriceAdjustModel.addressId == undefined ? "" : downgradeServicePriceAdjustModel.addressId;
    this.contractId = downgradeServicePriceAdjustModel == undefined ? "" : downgradeServicePriceAdjustModel.contractId == undefined ? "" : downgradeServicePriceAdjustModel.contractId;

  }
  downgradeServiceAdjustCostingPostDTOs: DowngradeServicePriceAdjustments[];
  createdUserId: string;
  doaConfigId: string;
  downgradeServiceRequestId: string;
  serviceDiscountAmount: number;
  serviceDiscountPercentage: number;
  customerId: string;
  contractId: string;
  addressId: string;
}






class DowngradeCustomerServiceModel {
  constructor(downgradeCustomerServiceModel?: DowngradeCustomerServiceModel) {
    this.createdUserId = downgradeCustomerServiceModel ? downgradeCustomerServiceModel.createdUserId == undefined ? "" : downgradeCustomerServiceModel.createdUserId : "";
    this.downgradeServiceRequestDTO = downgradeCustomerServiceModel ? downgradeCustomerServiceModel.downgradeServiceRequestDTO == undefined ? '' : downgradeCustomerServiceModel.downgradeServiceRequestDTO : {};
    this.newServices = downgradeCustomerServiceModel ? downgradeCustomerServiceModel.newServices == undefined ? [] : downgradeCustomerServiceModel.newServices : [];
    this.droppedServices = downgradeCustomerServiceModel ? downgradeCustomerServiceModel.droppedServices == undefined ? [] : downgradeCustomerServiceModel.droppedServices : [];
  }
  createdUserId: string
  downgradeServiceRequestDTO: DowngradeServiceRequestModel | any
  newServices: DowngradeServicesList[]
  droppedServices: DowngradeServicesList[]


}



export { UpgradeCustomerServiceModel, UpgradeServicesList, CustomerServicePriceAdjustments,
  CustomerServicePriceAdjustModel,DowngradeCustomerServiceModel ,DowngradeServicesList,
  DowngradeServicePriceAdjustModel,DowngradeServicePriceAdjustments}
