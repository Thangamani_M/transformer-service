
class CallCreationTemplateModel {
    constructor(CallCreationModel?: CallCreationTemplateModel) {
        this.callCreationList = CallCreationModel ? CallCreationModel.callCreationList == undefined ? [] : CallCreationModel.callCreationList : [];
    }
    callCreationList: CallCreationTemplateListModel[];
}

class CallCreationTemplateListModel {
    constructor(callCreationTemplateModel: CallCreationTemplateListModel) {
        this.callCreationConfigId = callCreationTemplateModel ? callCreationTemplateModel.callCreationConfigId == undefined ? '' : callCreationTemplateModel.callCreationConfigId : '';
        this.questionName = callCreationTemplateModel ? callCreationTemplateModel.questionName == undefined ? '' : callCreationTemplateModel.questionName : '';
        this.isTextBox = callCreationTemplateModel ? callCreationTemplateModel.isTextBox == undefined ? false : callCreationTemplateModel.isTextBox : false;
        this.isYesNo = callCreationTemplateModel ? callCreationTemplateModel.isYesNo == undefined ? false : callCreationTemplateModel.isYesNo : false;
        this.isDropdown = callCreationTemplateModel ? callCreationTemplateModel.isDropdown == undefined ? false : callCreationTemplateModel.isDropdown : false;
        this.isCallCreation = callCreationTemplateModel ? callCreationTemplateModel.isCallCreation == undefined ? false : callCreationTemplateModel.isCallCreation : false;
        this.isOverActive = callCreationTemplateModel ? callCreationTemplateModel.isOverActive == undefined ? false : callCreationTemplateModel.isTextBox : false;
        this.tableName = callCreationTemplateModel ? callCreationTemplateModel.tableName == undefined ? '' : callCreationTemplateModel.tableName : '';
        this.callInitiationQuestionId = callCreationTemplateModel ? callCreationTemplateModel.callInitiationQuestionId == undefined ? '' : callCreationTemplateModel.callInitiationQuestionId : '';
        this.callInitiationId = callCreationTemplateModel ? callCreationTemplateModel.callInitiationId == undefined ? '' : callCreationTemplateModel.callInitiationId : '';
        this.isTogglevalue = callCreationTemplateModel ? callCreationTemplateModel.isTogglevalue == undefined ? false : callCreationTemplateModel.isTogglevalue : false;
        this.value = callCreationTemplateModel ? callCreationTemplateModel.value == undefined ? '' : callCreationTemplateModel.value : '';
        this.customerId = callCreationTemplateModel ? callCreationTemplateModel.customerId == undefined ? '' : callCreationTemplateModel.customerId : '';
        this.createdUserId = callCreationTemplateModel ? callCreationTemplateModel.createdUserId == undefined ? '' : callCreationTemplateModel.createdUserId : '';
    }
    callCreationConfigId: string;
    questionName: string;
    isTextBox: boolean;
    isYesNo: boolean;
    isDropdown: boolean;
    isCallCreation: boolean;
    isOverActive: boolean;
    tableName: string;
    callInitiationQuestionId: string;
    callInitiationId: string;
    value: string;
    isTogglevalue: boolean
    customerId: string;
    createdUserId: string
}

export { CallCreationTemplateModel, CallCreationTemplateListModel}

