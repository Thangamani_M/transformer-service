class ProcessPaymentInstallationModel {
    constructor(processPaymentInstallationModel?: ProcessPaymentInstallationModel) {
        this.paymentTransactionId = processPaymentInstallationModel?.paymentTransactionId ? processPaymentInstallationModel?.paymentTransactionId : '';
        this.quotationVersionId = processPaymentInstallationModel?.quotationVersionId ? processPaymentInstallationModel?.quotationVersionId : '';
        this.balanceAmount = processPaymentInstallationModel?.balanceAmount ? processPaymentInstallationModel?.balanceAmount : '';
        this.isAllowedtoChangePaymentMethod = processPaymentInstallationModel?.isAllowedtoChangePaymentMethod ? processPaymentInstallationModel?.isAllowedtoChangePaymentMethod : false;
        this.paymentMethodName = processPaymentInstallationModel?.paymentMethodName ? processPaymentInstallationModel?.paymentMethodName : '';
        this.debtorName = processPaymentInstallationModel?.debtorName ? processPaymentInstallationModel?.debtorName : '';
        this.paymentOptionId = processPaymentInstallationModel?.paymentOptionId ? processPaymentInstallationModel?.paymentOptionId : '';
        this.bdiNumber = processPaymentInstallationModel?.bdiNumber ? processPaymentInstallationModel?.bdiNumber : '';
        this.accountHolderName = processPaymentInstallationModel?.accountHolderName ? processPaymentInstallationModel?.accountHolderName : '';
        this.accountNo = processPaymentInstallationModel?.accountNo ? processPaymentInstallationModel?.accountNo : '';
        this.bankName = processPaymentInstallationModel?.bankName ? processPaymentInstallationModel?.bankName : '';
        this.bankBranchCode = processPaymentInstallationModel?.bankBranchCode ? processPaymentInstallationModel?.bankBranchCode : '';
        this.isPurchaseOrderExist = processPaymentInstallationModel?.isPurchaseOrderExist?.toString() ? processPaymentInstallationModel?.isPurchaseOrderExist : null;
        this.purchaseOrderNo = processPaymentInstallationModel?.purchaseOrderNo ? processPaymentInstallationModel?.purchaseOrderNo : '';
        this.purchaseOrderDate = processPaymentInstallationModel?.purchaseOrderDate ? processPaymentInstallationModel?.purchaseOrderDate : null;
        this.paymentDetailsArray = processPaymentInstallationModel?.paymentDetailsArray ? processPaymentInstallationModel?.paymentDetailsArray : [];
    }
    paymentTransactionId: string;
    quotationVersionId: string;
    balanceAmount: string;
    isAllowedtoChangePaymentMethod: boolean;
    paymentMethodName: string;
    debtorName: string;
    paymentOptionId: string;
    bdiNumber: string;
    accountHolderName: string;
    accountNo: string;
    bankName: string;
    bankBranchCode: string;
    isPurchaseOrderExist: boolean;
    purchaseOrderNo: string;
    purchaseOrderDate: string | Date;
    paymentDetailsArray: Array<ProcessPaymentDetailsModel>;
}

class ProcessPaymentDetailsModel {
    constructor(processPaymentDetailsModel?: ProcessPaymentDetailsModel) {
        this.paidAmount = processPaymentDetailsModel?.paidAmount ? processPaymentDetailsModel?.paidAmount : '';
        this.paymentMethodId = processPaymentDetailsModel?.paymentMethodId ? processPaymentDetailsModel?.paymentMethodId : '';
        this.debitDate = processPaymentDetailsModel?.debitDate ? processPaymentDetailsModel?.debitDate : null;
    }
    paidAmount: number | string;
    paymentMethodId: number | string;
    debitDate: string | Date;
}

export { ProcessPaymentInstallationModel, ProcessPaymentDetailsModel }