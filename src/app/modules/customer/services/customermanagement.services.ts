import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';
import {environment} from '@environments/environment';
import {CustomerAddress,CustomerContact} from '@modules/customer/models';

@Injectable()
export class CustomerManagementService{
    constructor(private http:HttpClient){

    }    
   
    getCustomer(customerid): Observable<IApplicationResponse> {
        return this.http.get<IApplicationResponse>(environment.customerManagementApi + 'customers/' + customerid)
        .pipe(catchError(this.handleError<IApplicationResponse>('GetCustomers', null)));    
    }
    getCustomerAddressByCustomer(custId): Observable<IApplicationResponse>{
      return this.http.get<IApplicationResponse>(environment.customerManagementApi+'customeraddress?customerId='+custId)
      .pipe(catchError(this.handleError<IApplicationResponse>('GetCustomerAddress', null)));
    }
    getCustomerAddress(custAddid): Observable<IApplicationResponse>{
      return this.http.get<IApplicationResponse>(environment.customerManagementApi+'customeraddress/'+custAddid)
      .pipe(catchError(this.handleError<IApplicationResponse>('GetCustomerAddress', null)));
    }
    getCustomerContact(customerid): Observable<IApplicationResponse> {
      // return this.http.get<IApplicationResponse>(environment.customerManagementApi + 'customercontacts?customerId=' + customerid)
      // .pipe(catchError(this.handleError<IApplicationResponse>('GetCustomersContacts', null)));    

      return this.http.get<IApplicationResponse>(environment.salesApi + 'key-holders/customer/' + customerid)
      .pipe(catchError(this.handleError<IApplicationResponse>('GetCustomersContacts', null)));    

    }
    SaveCustomerAddress(customerAddress: CustomerAddress): Observable<IApplicationResponse>{
        const headers = new HttpHeaders({ 'content-type': 'application/json' });
        customerAddress.customerAddressId = null;
        return this.http.post<IApplicationResponse>(environment.customerManagementApi + 'customeraddress', customerAddress, { headers })
          .pipe(
                catchError(this.handleError<IApplicationResponse>('Create Customer Address', null)));
    }
    UpdateCustomerAddress(customerAddress: CustomerAddress){
        const headers = new HttpHeaders({ 'content-type': 'application/json' });
        return this.http.put<IApplicationResponse>(environment.customerManagementApi + 'customeraddress', customerAddress, { headers })
          .pipe(
                catchError(this.handleError<IApplicationResponse>('Update Customer Address', null)));
    }
    DeleteCustomerAddress(custId){
      const headers = new HttpHeaders({ 'content-type': 'application/json' });
      return this.http.delete<IApplicationResponse>(environment.customerManagementApi + 'customeraddress?Ids='+custId+'&IsDeleted=False', { headers })
        .pipe(
              catchError(this.handleError<IApplicationResponse>('Delete Customer Address', null)));
    }
    //getting CityName..
    getCityName(cityId)
    {
       return this.http.get<IApplicationResponse>(environment.apiUrl + 'cities/' + cityId)
       .pipe(catchError(this.handleError<IApplicationResponse>('GetCities', null)));
    }
    //get ProvinceName...
    getProvinceName(provinceId)
    {
       return this.http.get<IApplicationResponse>(environment.apiUrl + 'provinces/' + provinceId)
       .pipe(catchError(this.handleError<IApplicationResponse>('Getprovinces', null)));
    }
    //getting ProvinceName..
    getSuburbName(suburbId)
    {
      return this.http.get<IApplicationResponse>(environment.apiUrl + 'suburbs/' + suburbId)
      .pipe(catchError(this.handleError<IApplicationResponse>('GetSuburbs', null)));
    }
    getAddressTypeName(addrTypeId){
      return this.http.get<IApplicationResponse>(environment.apiUrl + 'addresstypes/' + addrTypeId)
      .pipe(catchError(this.handleError<IApplicationResponse>('GetAddressTypes', null)));
    }
    getAddressTypesDropdown(){
      return this.http.get<IApplicationResponse>(environment.apiUrl + 'ux/addresstypes/')
      .pipe(catchError(this.handleError<IApplicationResponse>('GetAddressTypesUX', null)));
    }
    getSuburbDropdown(cityId): Observable<IApplicationResponse> {
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      return this.http.get<IApplicationResponse>(environment.apiUrl + 'ux/suburbs/?CityId='+cityId)
        .pipe(catchError(this.handleError<IApplicationResponse>('GetSuburbs', null)))
    }
    getCityDropdown(provinceId): Observable<IApplicationResponse> {
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      return this.http.get<IApplicationResponse>(environment.apiUrl + 'ux/cities/?ProvinceId='+provinceId)
        .pipe(catchError(this.handleError<IApplicationResponse>('GetCities', null)))
    }
    getProvincesDropdown(countryId): Observable<IApplicationResponse> {
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      return this.http.get<IApplicationResponse>(environment.apiUrl + 'ux/provinces/?CountryId='+countryId+'&RegionId=')
        .pipe(catchError(this.handleError<IApplicationResponse>('GetStates', null)))
    }  
    getCountryDropdown(): Observable<IApplicationResponse> {
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      return this.http.get<IApplicationResponse>(environment.apiUrl + 'ux/countries')
        .pipe(catchError(this.handleError<IApplicationResponse>('GetCountries', null)))
    }
    getRegionDropdown(cntryId):Observable<IApplicationResponse>{
      const headers = new HttpHeaders({ 'Content-Type':'application/json '});
       return this.http.get<IApplicationResponse>(environment.apiUrl + 'ux/regions?countryId='+ cntryId)
       .pipe(catchError(this.handleError<IApplicationResponse>('GetRegions',null)))
    }
    getStreetTypeDropdown():Observable<IApplicationResponse>{
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      return this.http.get<IApplicationResponse>(environment.apiUrl + 'ux/streettypes')
        .pipe(catchError(this.handleError<IApplicationResponse>('GetStreetTypes', null)))
    }
  
    getCountryIdfromStateList(stId): Observable<IApplicationResponse> {
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      return this.http.get<IApplicationResponse>(environment.apiUrl + 'provinces/' + stId)
        .pipe(catchError(this.handleError<IApplicationResponse>('GetCountries', null)))
    }
    getContactTypesDropdown(){
      return this.http.get<IApplicationResponse>(environment.apiUrl + 'ux/contacttypes/')
      .pipe(catchError(this.handleError<IApplicationResponse>('GetContactTypesUX', null)));
    }
    getLanguageDropdown(){
      return this.http.get<IApplicationResponse>(environment.apiUrl + 'ux/languages/')
      .pipe(catchError(this.handleError<IApplicationResponse>('GetlanguagesUX', null)));
    }
    getAllCustomerContacts(customerid): Observable<IApplicationResponse> {
      return this.http.get<IApplicationResponse>(environment.customerManagementApi + 'customercontacts/' + customerid)
      .pipe(catchError(this.handleError<IApplicationResponse>('GetCustomers', null)));    
    }
    getCustomerContactByCustomer(custContctId): Observable<IApplicationResponse>{
      return this.http.get<IApplicationResponse>(environment.customerManagementApi+'customercontacts/'+custContctId)
      .pipe(catchError(this.handleError<IApplicationResponse>('GetCustomerAddress', null)));
    }
    SaveCustomerContact(customerContact: CustomerContact): Observable<IApplicationResponse>{
      const headers = new HttpHeaders({ 'content-type': 'application/json' });
      customerContact.customerContactId = null;
      return this.http.post<IApplicationResponse>(environment.customerManagementApi + 'customercontacts', customerContact, { headers })
        .pipe(
              catchError(this.handleError<IApplicationResponse>('Create Customer Address', null)));
    }
    UpdateCustomerContact(customerContact: CustomerContact){
        const headers = new HttpHeaders({ 'content-type': 'application/json' });
        return this.http.put<IApplicationResponse>(environment.customerManagementApi + 'customercontacts', customerContact, { headers })
          .pipe(
                catchError(this.handleError<IApplicationResponse>('Update Customer Address', null)));
    }
    DeleteCustomerContact(custCntctId){
      const headers = new HttpHeaders({ 'content-type': 'application/json' });
      return this.http.delete<IApplicationResponse>(environment.customerManagementApi + 'customercontacts?Ids='+custCntctId+'&IsDeleted=False', { headers })
        .pipe(
              catchError(this.handleError<IApplicationResponse>('Delete Customer Address', null)));
    }
    private handleError<T>(operation = 'operation', result?: T) {
       return (error: any): Observable<T> => {
          return of(result)
        }
     }
}