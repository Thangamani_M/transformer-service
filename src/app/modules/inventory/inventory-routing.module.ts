import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InventoryDashboardComponent } from '@inventory/components/dashboard';

const inventoryModuleRoutes: Routes = [
    {
        path: 'dashboard', component: InventoryDashboardComponent, data: { title: 'Inventory Dashboard' }
    },
    // {
    //     path: 'credit-note', loadChildren: () => import('../inventory/components/credit-note/credit-note.module').then(m => m.CreditNoteModule)
    // },
    // {
    //     path: 'dealer', loadChildren: () => import('../inventory/components/dealer/dealer.module').then(m => m.DealerModule)
    // },
    {
        path: 'inter-branch', loadChildren: () => import('../inventory/components/inter-branch/inter-branch.module').then(m => m.InterBranchModule)
    },
    {
        path: 'inter-branch-transfer-order', loadChildren: () => import('../inventory/components/inter-branch/inter-branch-transfer-order.module').then(m => m.InterBranchTransferOrderModule)
    },
    // {
    //     path: 'inter-technician', loadChildren: () => import('../inventory/components/inter-technician/inter-technician.module').then(m => m.InterTechnicianModule)
    // },
    // {
    //     path: 'items', loadChildren: () => import('../inventory/components/items/item.module').then(m => m.ItemModule)
    // },
    // {
    //     path: 'pricing', loadChildren: () => import('../inventory/components/pricing/pricing.module').then(m => m.PricingModule)
    // },
    // {
    //     path: 'job-order', loadChildren: () => import('../inventory/components/job-order/job-order.module').then(m => m.JobOrderModule)
    // },
    {
        path: 'kit-config', loadChildren: () => import('../inventory/components/kit-config/kit-config.module').then(m => m.KitConfigModule)
    },
    {
        path: 'order-receipts', loadChildren: () => import('../inventory/components/order-receipts/order-receipts.module').then(m => m.OrderReceiptsModule)
    },
    {
        path: 'warehouse', loadChildren: () => import('../inventory/components/plant-management/plant-management.module').then(m => m.PlantManagementModule)
    },
    // {
    //     path: 'products', loadChildren: () => import('../inventory/components/products/products.module').then(m => m.ProductsModule)
    // },
    //{
        // path: 'repair-request', loadChildren: () => import('../inventory/components/repair-request/repair-request.module').then(m => m.RepairRequestModule)
   // },
    {
        path: 'purchase-order', loadChildren: () => import('../inventory/components/purchase-order/purchase-order.module').then(m => m.PurchaseOrderModule)
    },
    {
        path: 'requisitions', loadChildren: () => import('../inventory/components/stock-order/stock-order.module').then(m => m.StockOrderModule)
    },
    {
        path: 'suppliers', loadChildren: () => import('../inventory/components/supplier/supplier.module').then(m => m.SupplierModule)
    },
    {
        path: 'variance', loadChildren: () => import('../inventory/components/variance/variance.module').then(m => m.VarianceModule)
    },
    {
        path: 'picker-jobs', loadChildren: () => import('../inventory/components/picker-jobs/picker-jobs.module').then(m => m.PickerJobsModule)
    },
    {
        path: 'warehouse-stock-approval', loadChildren: () => import('../inventory/components/WarehouseStockApproval/warehouse-stock-approval.module').then(m => m.WarehouseStockApprovalModule)
    },
    {
        path: 'warehouse-stock-take-manager', loadChildren: () => import('../inventory/components/warehouse-stock-take-manager/warehouse-stock-take-manager.module').then(m => m.WarehouseStockTakeManagerModule)
    },
    {
        path: 'stock', loadChildren: () => import('../inventory/components/stock-initiation/stock-initiation.module').then(m => m.StockInitiationModule)
    },
    {
        path: 'stock-maintenance', loadChildren: () => import('../inventory/components/stock-maintenance/stock-maintenance.module').then(m => m.StockMaintenanceModule)
    },
    {
        path: "bin", loadChildren: () => import('../inventory/components/bin/components/bin-management/bin-management.module').then(m => m.BinManagementModule)
    },
    {
        path: "audit-cycle-configuration", loadChildren: () => import('../inventory/components/audit-cycle-configuration/audit-cycle.module').then(m => m.AuditCycleModule)
    },
    {
        path: 'waste-disposal', loadChildren: () => import('../inventory/components/waste-disposal/waste-disposal.module').then(m => m.WasteDisposalModule)
    },
    {
        path: "cycle-count-configuration", loadChildren: () => import('../inventory/components/cycle-count/cycle-count.module').then(m => m.CycleCountModule)
    },
    {
        path: "cycle-count-enable-configuration", loadChildren: () => import('../inventory/components/cycle-count-enable-configuration/cycle-count-enable-configuration.module').then(m => m.CycleCountEnableConfigurationModule)
    },
    {
        path: "cycle-count-staff", loadChildren: () => import('../inventory/components/cycle-count/cycle-count-staff/cycle-count-staff.module').then(m => m.CycleCountStaffModule)
    },
    {
        path: "cycle-count-manager", loadChildren: () => import('../inventory/components/cycle-count/cycle-count-manager/cycle-count-manager.module').then(m => m.CycleCountManagerModule)
    },
    // {
    //     path: 'werehouse-staff', loadChildren: () => import('../inventory/components/werehouse-staff/werehouse-staff.module').then(m => m.WerehouseStaffModule)
    // },
    // {
    //     path: 'doa-config', loadChildren: () => import('../inventory/components/doa-config/doa-config.module').then(m => m.DoaConfigModule)
    // },
    {
        path: 'stock-selling-price', loadChildren: () => import('../inventory/components/stock-selling-price/stock-selling-price.module').then(m => m.StockSellingPriceModule)
    },
    {
        path: 'stock-scheduling', loadChildren: () => import('../inventory/components/stock-scheduling/stock-scheduling.module').then(m => m.StockSchedulingModule)
    },
    {
        path: 'stock-management', loadChildren: () => import('../inventory/components/stock-management/stock-management.module').then(m => m.StockManagementModule)
    },
    {
        path: 'daily-staging-bay', loadChildren: () => import('../inventory/components/daily-staging-pay/daily-staging-pay.module').then(m => m.DailyStagingPayModule)
    },
    {
        path: 'picking-dashboard-orders', loadChildren: () => import('../inventory/components/picking-dashboard-orders/picking-dashboard-orders.module').then(m => m.PickingDashboardOrdersModule)
    },
    {
        path: 'packing-dashboard', loadChildren: () => import('../inventory/components/packing-dashboard/packing-dashboard.module').then(m => m.PackingDashboardModule)
    },
    {
        path: 'job-selection', loadChildren: () => import('../inventory/components/instaff-job-selection/instaff-job-selection.module').then(m => m.InStaffJobSelectionModule)
    },
    // {
    //     path: 'variance-reports', loadChildren: () => import('../inventory/components/cycle-count-variance-reports/cycle-count-variance-reports.module').then(m => m.CycleCountVarianceReportsModule)
    // },
    {
        path: 'cycle-count-bmdmfm-reports', loadChildren: () => import('../inventory/components/cycle-count-bmdmfm-variance-reports/cycle-count-bmdmfm-variance-reports.module').then(m => m.CycleCountBMDMFMVarianceReportsModule)
    },
    {
        path: 'audit-cycle-count', loadChildren: () => import('../inventory/components/im-audit-cycle-count/im-audit-cycle-count.module').then(m => m.ImAuditCycleCountModule)
    },
    {
        path: 'grv-creation', loadChildren: () => import('../inventory/components/automated-grv/automated-grv-creation.module').then(m => m.AutomatedGrvCreationModule)
    },
    {
        path: 'stock-adjustment', loadChildren: () => import('../inventory/components/stock-adjustments/stock-adjustments.module').then(m => m.StockAdjustmentsModule)
    },
    {
        path: 'audit-cycle-count-manager', loadChildren: () => import('../inventory/components/audit-cycle-count-manager/audit-cycle-count-manager.module').then(m => m.AuditCycleCountManagerModule)
    },
    {
        path: 'stock-movement-report', loadChildren: () => import('../inventory/components/stock-movement-report/stock-movement-report.module').then(m => m.StockMovementReportModule)
    },
    {
        path: 'warehouse-assistant', loadChildren: () => import('../inventory/components/faulty-process/faulty-process.module').then(m => m.FaultyProcessModule)
    },
    {
        path: 'stock-discrepancy', loadChildren: () => import('../inventory/components/stock-discrepancy/stock-discrepancy.module').then(m => m.StockDiscrepancyModule)
    },
    {
        path: 'radio-removal', loadChildren: () => import('../inventory/components/radio-removal/radio-removal.module').then(m => m.RadioRemovalModule)
    },
    {
        path: 'radio-removals-worklist', loadChildren: () => import('../inventory/components/radio-removals-work-list/radio-removals-work-list.module').then(m => m.RadioRemovalsWorkListModule)
    },
    {
        path: 'radio-removal-admin', loadChildren: () => import('../inventory/components/radio-removal-admin-worklist/radio-removal-admin-worklist.module').then(m => m.RadioRemovalAdminWorklistModule)
    },
    {
        path: 'task-list', loadChildren: () => import('../inventory/components/inventory-task-list/inventory-task-list.module').then(m => m.InventoryTaskListModule)
    },
    {
        path: 'requisition-initiator-configuration', loadChildren: () => import('../inventory/components/requisition-initiator-configuration/requisition-initiator-configuration.module').then(m => m.RequisitionInitiatorConfigurationModule)
    },
    // {
    //     path: 'item-scrapping-configuration', loadChildren: () => import('./components/item-scrapping-configuration.component/item-scrapping-configuration.module').then(m => m.ItemScrappingConfigurationModule)
    // },
    {
        path: 'delar-stock-order', loadChildren: () => import('../inventory/components/delar-stock-order-collection/delar-stock-order-collection.module').then(m => m.DelarStockOrderCollectionModule)
    },
    {
        path: 'return-for-repair-3rd-process', loadChildren: () => import('../inventory/components/return-for-repair-process/inv-return-for-repair-process.module').then(m => m.InvReturnForRepairProcessModule)
    },
    {
        path: 'dealer-stock-order-collection', loadChildren: () => import('../inventory/components/delar-stock-order-collection/delar-stock-order-collection.module').then(m => m.DelarStockOrderCollectionModule)
    },
    {
        path: 'transfer-request', loadChildren: () => import('../inventory/components/transfer-request/transfer-request.module').then(m => m.TransferRequestModule)
    },
    {
        path: 'return-for-repair-request', loadChildren: () => import('../inventory/components/return-for-repair-request/return-for-repair-request.module').then(m => m.ReturnForRepairRequestModule)
    },
    {
        path: 'radio-system-checker', loadChildren: () => import('../inventory/components/radio-system-checker/radio-system-checker.module').then(m => m.RadioSystemCheckerModule)
    },
];



@NgModule({
    imports: [RouterModule.forChild(inventoryModuleRoutes)],

})

export class InventoryRoutingModule { }
