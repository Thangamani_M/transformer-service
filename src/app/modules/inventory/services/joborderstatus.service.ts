import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';
import {environment} from '@environments/environment';
import { catchError } from 'rxjs/operators';
import { Joborderstatus } from '../models/joborderstatus';
import {EnableDisable} from  '@app/shared/models';
import { Guid } from 'guid-typescript';

const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
@Injectable()
export class JoborderstatusService {

  constructor(private http: HttpClient) { }

  joborderstatusDropDown(id?: string): Observable<IApplicationResponse> {
    if (id == undefined) {
      id = "";
    } else {
      id = "?Id=" + id;
    }

    return this.http.get<IApplicationResponse>(environment.apiUrl + 'joborder/status/dropdown' + id);
  }
  getJoborderstatus(pageindex, maximumrows): Observable<IApplicationResponse> {

    return this.http.get<IApplicationResponse>(environment.apiUrl + 'joborder/status?active=false&pageindex=' + pageindex + '&maximumrows=' + maximumrows).pipe(catchError(this.handleError<IApplicationResponse>('GetModules', null)))
  }

  //Get joboredrstatus details by joborderstatusid
  getJobOrderStatus(jobOrderstatusid): Observable<IApplicationResponse> {

    return this.http.get<IApplicationResponse>(environment.apiUrl + 'joborder/status/' + jobOrderstatusid).pipe(catchError(this.handleError<IApplicationResponse>('GetJobOrderStatus', null)));
  }
  //Update joboredrstatus details
  updateJoborderstatus(joboredrstatus: Joborderstatus): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.apiUrl + 'joborder/status/', joboredrstatus, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Update Joborderstatus', null)));
  }
  //Create joboredrstatus
  createJoborderstatus(JoborderstatusDTO: Joborderstatus): Observable<IApplicationResponse> {

    JoborderstatusDTO.jobOrderStatusId = Guid.createEmpty().toString();;
    return this.http.post<IApplicationResponse>(environment.apiUrl + 'joborder/status/', JoborderstatusDTO, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Create Joborderstatus', null)));
  }
  //To enable or disable Joborderstatus
  enableDisableJoborderstatus(enableDisable: EnableDisable): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.apiUrl + 'joborder/status/enabledisable', enableDisable, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Enable or Disable Joborderstatus', null)));
  }
  deleteJoborderstatus(joboredrstatusIds): Observable<IApplicationResponse> {
    return this.http.delete<IApplicationResponse>(environment.apiUrl + 'joborder/status/', {
      params: {
        'ids': joboredrstatusIds
      }
    })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Update Joborderstatus', null)));
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result)
    }
  }
}
