import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';
import {environment} from '@environments/environment';
import {EnableDisable} from  '@app/shared/models';
import { catchError } from 'rxjs/operators';
import { TechnicianOrderStatus } from '../models/TechnicianOrderStatus';
import { Guid } from 'guid-typescript';


const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
const apiModuleName = "technician/order/status";

@Injectable()

export class TechnicianorderstatusService {

  constructor(private http: HttpClient) { }

  getTechnicianorderStatus(pageindex, maximumrows): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.apiUrl + apiModuleName + '?active=false&pageindex=' + pageindex + '&maximumrows=' + maximumrows).pipe(catchError(this.handleError<IApplicationResponse>('GetTechnicianorderStatus', null)))
  }

  //Get technicianorderStatus details by technicianorderStatus id
  getTechnicianorderStatusDetails(technicianorderStatusid): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.apiUrl + apiModuleName + '/' + technicianorderStatusid).pipe(catchError(this.handleError<IApplicationResponse>('GetTechnicianorderStatus', null)));
  }

  //Create technicianorderStatus
  createTechnicianorderStatus(technicianorderStatus: TechnicianOrderStatus): Observable<IApplicationResponse> {
    technicianorderStatus.technicianOrderStatusId = Guid.createEmpty().toString();
    return this.http.post<IApplicationResponse>(environment.apiUrl + apiModuleName, technicianorderStatus, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Create TechnicianorderStatus', null)));
  }

  //Update technicianorderStatus details
  updateTechnicianorderStatus(technicianorderStatus: TechnicianOrderStatus): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.apiUrl + apiModuleName, technicianorderStatus, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Update TechnicianorderStatus', null)));
  }

  //To enable or disable technicianorderStatus
  enableDisableTechnicianorderStatus(enableDisable: EnableDisable): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.apiUrl + apiModuleName + '/' + 'enabledisable', enableDisable, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Enable or Disable TechnicianorderStatus', null)));
  }

  //handle single delete or multiple delete
  deleteTechnicianorderStatus(skillSetIds): Observable<IApplicationResponse> {
    return this.http.delete<IApplicationResponse>(environment.apiUrl + apiModuleName, {
      params: {
        'ids': skillSetIds
      }
    })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Delete technicianorderStatus', null)));
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result)
    }
  }
}
