import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';
import {environment} from '@environments/environment';
import {EnableDisable} from  '@app/shared/models';
import { catchError } from 'rxjs/operators';
import { Guid } from 'guid-typescript';
import { WarrantyStatus } from '../models/WarrantyStatus';

const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
const apiModuleName = "warranty/status";

@Injectable()
export class WarrantyStatusService {

  constructor(private http: HttpClient) { }

  getWarrantyStatus(pageindex, maximumrows): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.apiUrl + apiModuleName + '?active=false&pageindex=' + pageindex + '&maximumrows=' + maximumrows).pipe(catchError(this.handleError<IApplicationResponse>('GetWarrantyStatus', null)))
  }

  //Get warrantyStatus details by warrantyStatus id
  getWarrantyStatusDetails(warrantyStatusid): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.apiUrl + apiModuleName + '/' + warrantyStatusid).pipe(catchError(this.handleError<IApplicationResponse>('GetWarrantyStatus', null)));
  }

  //Create warrantyStatus
  createWarrantyStatus(warrantyStatus: WarrantyStatus): Observable<IApplicationResponse> {
    warrantyStatus.warrentyStatusId = null;
    return this.http.post<IApplicationResponse>(environment.apiUrl + apiModuleName, warrantyStatus, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Create WarrantyStatus', null)));
  }

  //Update warrantyStatus details
  updateWarrantyStatus(warrantyStatus: WarrantyStatus): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.apiUrl + apiModuleName, warrantyStatus, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Update WarrantyStatus', null)));
  }

  //To enable or disable warrantyStatus
  enableDisableWarrantyStatus(enableDisable: EnableDisable): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.apiUrl + apiModuleName + '/' + 'enabledisable', enableDisable, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Enable or Disable WarrantyStatus', null)));
  }

  //handle single delete or multiple delete
  deleteWarrantyStatus(skillSetIds): Observable<IApplicationResponse> {
    return this.http.delete<IApplicationResponse>(environment.apiUrl + apiModuleName, {
      params: {
        'ids': skillSetIds
      }
    })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Delete warrantyStatus', null)));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result)
    }
  }
}
