import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';
import { Observable, of } from 'rxjs';
import {environment} from '@environments/environment';
import { catchError } from 'rxjs/operators';
import { VarianceInvestigationStatus } from '../models/VarianceInvestigationStatus';
import {EnableDisable} from  '@app/shared/models';
import { Guid } from 'guid-typescript';

const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
const apiModuleName = "variance/investigation/status";
@Injectable()
export class VarianceInvestigationStatusService {

  constructor(private http: HttpClient) { }

  getVarianceInvestigationStatus(pageindex, maximumrows): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.apiUrl + apiModuleName + '/?active=false&pageindex=' + pageindex + '&maximumrows=' + maximumrows).pipe(catchError(this.handleError<IApplicationResponse>('GetVarianceInvestigationService', null)))  }

  //Get varianceInvestigationStatus details by varianceInvestigationStatus id
  getVarianceInvestigationStatusDetails(varianceInvestigationStatusid): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.apiUrl + apiModuleName +'/'+ varianceInvestigationStatusid).pipe(catchError(this.handleError<IApplicationResponse>('GetVarianceInvestigationStatus', null)));
  }

  //Create varianceInvestigationStatus
  createVarianceInvestigationStatus(varianceInvestigationStatus: VarianceInvestigationStatus): Observable<IApplicationResponse> {
    varianceInvestigationStatus.varianceInvestigationStatusId = Guid.createEmpty().toString();
    return this.http.post<IApplicationResponse>(environment.apiUrl + apiModuleName, varianceInvestigationStatus, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Create VarianceInvestigationStatus', null)));
  }

  //Update varianceInvestigationStatus details
  updateVarianceInvestigationStatus(varianceInvestigationStatus: VarianceInvestigationStatus): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.apiUrl + apiModuleName, varianceInvestigationStatus, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Update VarianceInvestigationStatus', null)));
  }

  //To enable or disable varianceInvestigationStatus
  enableDisableVarianceInvestigationStatus(enableDisable: EnableDisable): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.apiUrl + apiModuleName + '/enabledisable', enableDisable, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Enable or Disable VarianceInvestigationStatus', null)));
  }

  //handle single delete or multiple delete
  deleteVarianceInvestigationStatus(varianceInvestigationStatusIds): Observable<IApplicationResponse> {
    return this.http.delete<IApplicationResponse>(environment.apiUrl + apiModuleName, {
      params: {
        'ids': varianceInvestigationStatusIds
      }
    })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Delete varianceInvestigationStatus', null)));
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result)
    }
  }
}