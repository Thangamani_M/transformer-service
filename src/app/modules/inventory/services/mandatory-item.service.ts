import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';
import { Observable, of } from 'rxjs';
import {environment} from '@environments/environment';
import { catchError } from 'rxjs/operators';
import { MandatoryItem } from '../models/MandatoryItem';
import {EnableDisable} from  '@app/shared/models';

const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
const apiModuleName = "mandatory/item";

@Injectable()
export class MandatoryItemService {

  constructor(private http: HttpClient) { }

  getMandatoryItem(pageindex, maximumrows): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.apiUrl + apiModuleName + '/?active=false&pageindex=' + pageindex + '&maximumrows=' + maximumrows).pipe(catchError(this.handleError<IApplicationResponse>('GetMandatoryItemService', null)))  }

  //Get mandatoryItem details by mandatoryItem id
  getMandatoryItemDetails(mandatoryItemid): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.apiUrl + apiModuleName +'/'+ mandatoryItemid).pipe(catchError(this.handleError<IApplicationResponse>('GetMandatoryItem', null)));
  }

  //Create mandatoryItem
  createMandatoryItem(mandatoryItem: MandatoryItem): Observable<IApplicationResponse> {
    mandatoryItem.mandatoryItemId = null;
    return this.http.post<IApplicationResponse>(environment.apiUrl + apiModuleName, mandatoryItem, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Create MandatoryItem', null)));
  }

  //Update mandatoryItem details
  updateMandatoryItem(mandatoryItem: MandatoryItem): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.apiUrl + apiModuleName, mandatoryItem, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Update MandatoryItem', null)));
  }

  //To enable or disable mandatoryItem
  enableDisableMandatoryItem(enableDisable: EnableDisable): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.apiUrl + apiModuleName + '/enabledisable', enableDisable, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Enable or Disable MandatoryItem', null)));
  }

  //handle single delete or multiple delete
  deleteMandatoryItem(mandatoryItemIds): Observable<IApplicationResponse> {
    return this.http.request<IApplicationResponse>('delete', environment.apiUrl + apiModuleName, {
      body: {
        'ids': mandatoryItemIds
      }
    })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Delete mandatoryItem', null)));
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result)
    }
  }
}