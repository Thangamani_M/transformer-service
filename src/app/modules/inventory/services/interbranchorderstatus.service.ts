import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';
import {environment} from '@environments/environment';
import {EnableDisable} from  '@app/shared/models';
import { catchError } from 'rxjs/operators';
import { InterBranchOrderStatus } from '../models/InterBranchOrderStatus';

const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
const apiModuleName = "interbranch/order/status/";
@Injectable()

export class InterbranchorderstatusService {

  constructor(private http: HttpClient) { }

  getInterBranchOrderStatus(pageindex, maximumrows): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.apiUrl + apiModuleName +'?active=false&pageindex=' + pageindex + '&maximumrows=' + maximumrows).pipe(catchError(this.handleError<IApplicationResponse>('GetInterBranchStatusOrder', null)))
  }
  
  //Get interBranchOrderStatus details by interBranchOrderStatus id
  getInterBranchOrderStatusDetails(interBranchOrderStatusid): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.apiUrl + apiModuleName + interBranchOrderStatusid).pipe(catchError(this.handleError<IApplicationResponse>('GetInterBranchOrderStatus', null)));
  }

  //Create interBranchOrderStatus
  createInterBranchOrderStatus(interBranchOrderStatus: InterBranchOrderStatus): Observable<IApplicationResponse> {
    interBranchOrderStatus.interBranchOrderStatusId = null;
    return this.http.post<IApplicationResponse>(environment.apiUrl + apiModuleName, interBranchOrderStatus, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Create InterBranchOrderStatus', null)));
  }

  //Update interBranchOrderStatus details
  updateInterBranchOrderStatus(interBranchOrderStatus: InterBranchOrderStatus): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.apiUrl + apiModuleName, interBranchOrderStatus, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Update InterBranchOrderStatus', null)));
  }

  //To enable or disable interBranchOrderStatus
  enableDisableInterBranchOrderStatus(enableDisable: EnableDisable): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.apiUrl + apiModuleName + 'enabledisable', enableDisable, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Enable or Disable InterBranchOrderStatus', null)));
  }

  //handle single delete or multiple delete
  deleteInterBranchOrderStatus(skillSetIds): Observable<IApplicationResponse> {
    return this.http.delete<IApplicationResponse>(environment.apiUrl + apiModuleName, {
      params: {
        'ids': skillSetIds
      }
    })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Delete interBranchOrderStatus', null)));
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result)
    }
  }
}
