import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';
import {environment} from '@environments/environment';
import {EnableDisable} from  '@app/shared/models';
import { catchError } from 'rxjs/operators';
import { ItemTypes } from '../models/ItemTypes';
import {Guid} from 'guid-typescript';

const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
const apiModuleName = "item/types/";

@Injectable()
export class ItemTypesService {

  constructor(private http: HttpClient) { }

  getItemTypes(pageindex, maximumrows): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.apiUrl + apiModuleName +'?active=false&pageindex=' + pageindex + '&maximumrows=' + maximumrows).pipe(catchError(this.handleError<IApplicationResponse>('GetitemTypes', null)))
  }
  
  //Get itemTypes details by itemTypes id
  getItemTypesDetails(itemTypesid): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.apiUrl + apiModuleName + itemTypesid).pipe(catchError(this.handleError<IApplicationResponse>('GetItemTypes', null)));
  }

  //Create itemTypes
  createItemTypes(itemTypes: ItemTypes): Observable<IApplicationResponse> {
    itemTypes.itemPriceTypeId = Guid.createEmpty().toString();
    itemTypes.itemPriceTypeName = itemTypes.displayName;
    return this.http.post<IApplicationResponse>(environment.apiUrl + apiModuleName, itemTypes, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Create ItemTypes', null)));
  }

  //Update itemTypes details
  updateItemTypes(itemTypes: ItemTypes): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.apiUrl + apiModuleName, itemTypes, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Update ItemTypes', null)));
  }

  //To enable or disable itemTypes
  enableDisableItemTypes(enableDisable: EnableDisable): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.apiUrl + apiModuleName + 'enabledisable', enableDisable, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Enable or Disable ItemTypes', null)));
  }

  //handle single delete or multiple delete
  deleteItemTypes(skillSetIds): Observable<IApplicationResponse> {
    return this.http.delete<IApplicationResponse>(environment.apiUrl + apiModuleName, {
      params: {
        'ids': skillSetIds
      }
    })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Delete itemTypes', null)));
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result)
    }
  }
}
