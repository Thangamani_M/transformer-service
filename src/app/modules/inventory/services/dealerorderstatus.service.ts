import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';
import {environment} from '@environments/environment';
import { catchError } from 'rxjs/operators';
import { DealerOrderStatus } from '../models/DealerOrderStatus';
import {EnableDisable} from  '@app/shared/models';
import { Guid } from 'guid-typescript';

const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
const apiModuleName = "dealerorderstatus";
@Injectable()
export class DealerorderstatusService {

  constructor(private http: HttpClient) { }
  //DealerOrderStatus List
  getDealerOrderStatusList(pageindex, maximumrows): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.inventoryApi + apiModuleName + '?IsAll=true&pageindex=' + pageindex + '&maximumrows=' + maximumrows).pipe(catchError(this.handleError<IApplicationResponse>('GetDealerOrderStatus', null)))
  }
  //DealerOrderStatus Create
  createDealerOrderStatus(dealerOrderStatus: DealerOrderStatus): Observable<IApplicationResponse> {
    dealerOrderStatus.dealerOrderStatusId = Guid.EMPTY;
    return this.http.post<IApplicationResponse>(environment.inventoryApi + apiModuleName, dealerOrderStatus, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Create DealerOrderStatus', null)));
  }
  //DealerOrderStatus Update
  updateDealerOrderStatus(dealerOrderStatus: DealerOrderStatus): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.inventoryApi + apiModuleName, dealerOrderStatus, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Update DealerOrderStatus', null)));
  }
  //DealerOrderStatus EnableDisable
  enableDisableDealerOrderStatus(enableDisable: EnableDisable): Observable<IApplicationResponse> {
    return this.http.put<IApplicationResponse>(environment.inventoryApi + apiModuleName + 'enabledisable', enableDisable, { headers })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Enable or Disable DealerOrderStatus', null)));
  }
  //DealerOrderStatus Delete
  deleteDealerOrderStatus(dealerOrderStatusIds): Observable<IApplicationResponse> {
    return this.http.delete<IApplicationResponse>(environment.inventoryApi + apiModuleName, {
      params: {
        'ids': dealerOrderStatusIds
      }
    })
      .pipe(
        catchError(this.handleError<IApplicationResponse>('Delete interBranchOrderStatus', null)));
  }
  //DealerOrderStatus List Using Id
  getDealerOrderStatus(dealerOrderStatusId): Observable<IApplicationResponse> {
    return this.http.get<IApplicationResponse>(environment.inventoryApi + apiModuleName + dealerOrderStatusId).pipe(catchError(this.handleError<IApplicationResponse>('GetDealerOrderStatusById', null)));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result)
    }
  }
}
