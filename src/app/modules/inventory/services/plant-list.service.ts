import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {IApplicationResponse} from '@app/shared/utils/interfaces.utils';
import {environment} from '@environments/environment';

@Injectable()

export class PlantService {
    constructor(private http: HttpClient) {
    }

    getPlantManagementList(pageindex, maximumrows, wharehouseId): Observable<IApplicationResponse>{
        return this.http.get<IApplicationResponse>(environment.inventoryApi + 'warehouses?IsAll=false&pageindex=' + pageindex +
      '&maximumrows=' + maximumrows + "&WarehouseId="+wharehouseId).pipe(catchError(this.handleError<IApplicationResponse>('GetWhareHouses', null)))

    }
    //getting CityName...
    getPlantMgmtCityName(cityId)
    {
      return this.http.get<IApplicationResponse>(environment.apiUrl + 'cities/' + cityId)
      .pipe(catchError(this.handleError<IApplicationResponse>('GetCities', null)));
    }
    //getting SuburbName...
    getPlantMgmtSuburbName(suburbId)
    {
      return this.http.get<IApplicationResponse>(environment.apiUrl + 'suburbs/' + suburbId)
      .pipe(catchError(this.handleError<IApplicationResponse>('GetSuburbs', null)));
    }
    //get ProvinceName...
    getPlantMgmtProvinceName(provId)
    {
      return this.http.get<IApplicationResponse>(environment.apiUrl + 'provinces/' + provId)
      .pipe(catchError(this.handleError<IApplicationResponse>('GetProvinces', null)));
    }
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
          return of(result)
      }
    }
}