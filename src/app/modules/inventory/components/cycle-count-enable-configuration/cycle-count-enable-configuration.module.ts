import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { CycleCountEnableConfigAddEditComponent } from './cycle-count-enable-config-add-edit/cycle-count-enable-config-add-edit.component';
import { CycleCountEnableConfigListComponent } from './cycle-count-enable-config-list/cycle-count-enable-config-list.component';
import { CycleCountEnableConfigViewComponent } from './cycle-count-enable-config-view/cycle-count-enable-config-view.component';
import { CycleCountEnableConfigurationRoutingModule } from './cycle-count-enable-configuration-routing.module';
@NgModule({
    declarations: [
        CycleCountEnableConfigListComponent,
        CycleCountEnableConfigViewComponent,
        CycleCountEnableConfigAddEditComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule, FormsModule,
        CycleCountEnableConfigurationRoutingModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
    ],
    entryComponents: []

})

export class CycleCountEnableConfigurationModule { }
