import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CycleCountEnableConfigAddEditComponent } from './cycle-count-enable-config-add-edit/cycle-count-enable-config-add-edit.component';
import { CycleCountEnableConfigListComponent } from './cycle-count-enable-config-list/cycle-count-enable-config-list.component';
import { CycleCountEnableConfigViewComponent } from './cycle-count-enable-config-view/cycle-count-enable-config-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
    { path: '', component: CycleCountEnableConfigListComponent, data: { title: 'Cycle Count Starting Date List' },canActivate:[AuthGuard] },
    { path: 'view', component: CycleCountEnableConfigViewComponent, data: { title: 'Cycle Count Starting Date View' } ,canActivate:[AuthGuard]},
    { path: 'add-edit', component: CycleCountEnableConfigAddEditComponent, data: { title: 'Cycle Count Starting Date Add Edit' },canActivate:[AuthGuard] },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
})

export class CycleCountEnableConfigurationRoutingModule { }