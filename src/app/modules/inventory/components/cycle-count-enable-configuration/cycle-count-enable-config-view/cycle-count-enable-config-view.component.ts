import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-cycle-count-enable-config-view',
  templateUrl: './cycle-count-enable-config-view.component.html'
})
export class CycleCountEnableConfigViewComponent  extends PrimeNgTableVariablesModel  implements OnInit {
  cycleCountScheduleId: string;
  CycleCountEnableConfigView: any;
  primengTableConfigProperties: any;
  cyclecountenableconfigviewdata: any;
  constructor(private store: Store<AppState>,private snackbarService:SnackbarService,
    private httpService: CrudService, private router: Router,private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
    super();
    this.cycleCountScheduleId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "Cycle Count Starting Date",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Cycle Count Starting Date', relativeRouterUrl: '/inventory/cycle-count-enable-configuration' },
      { displayName: 'View Cycle Count Starting Date', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.CycleCountEnableConfigView = [
      { name: 'Warehouse', value: '' },
      { name: 'Scheduled By', value: '' },
      { name: 'Scheduled Date', value: '' },
      { name: 'Status', value: '' },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.cycleCountScheduleId) {
      this.httpService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.CYCLE_COUNT_SCHEDULE, this.cycleCountScheduleId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.cyclecountenableconfigviewdata = response.resources;
            this.CycleCountEnableConfigView = [
              { name: 'Warehouse', value: response.resources?.warehouseName },
              { name: 'Scheduled By', value: response.resources?.scheduledBy },
              { name: 'Scheduled Date', value: response.resources?.scheduledDate },
              { name: 'Status', value: response?.resources?.status, statusClass: response?.resources?.status?.toLowerCase() == 'active' ? 'status-label-green' : response?.resources?.status?.toLowerCase() == 'inactive' ? 'status-label-pink' : '' },
            ]
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.CYCLE_COUNT_STARTING_DATE];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEditPage();
        break;
    }
  }

  navigateToEditPage() {
    this.router.navigate(['/inventory', 'cycle-count-enable-configuration', 'add-edit'], {
      queryParams: {
        id: this.cycleCountScheduleId
      }, skipLocationChange: true
    });
  }
}
