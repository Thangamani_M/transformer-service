import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, setRequiredValidator, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { CycleCountEnableConfigurationModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-cycle-count-enable-config-add-edit',
  templateUrl: './cycle-count-enable-config-add-edit.component.html'
})
export class CycleCountEnableConfigAddEditComponent implements OnInit {
  cycleCountScheduleId: string;
  today: any = new Date()
  cycleCountScheduleDetails: any = null;
  cycleCountEnableConfigForm: FormGroup;
  warehouseList: any = [];
  userData: UserLogin;
  startTodayDate = new Date();
  constructor(
    private httpService: CrudService, private router: Router, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private datePipe: DatePipe,
    private formBuilder: FormBuilder, private store: Store<AppState>,
  ) {
    this.cycleCountScheduleId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createCycleCountScheduleForm();
    this.getWarehouseDropDown();
    if (this.cycleCountScheduleId) {
      this.getCycleCountScheduleDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.cycleCountScheduleDetails = response.resources;
          let purchaseOrdeModels = new CycleCountEnableConfigurationModel(response.resources);
          response.resources.scheduledDate = response.resources.scheduledDate ? new Date(response.resources.scheduledDate) : ''
          this.cycleCountEnableConfigForm.patchValue(response.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  getCycleCountScheduleDetails(): Observable<IApplicationResponse> {
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_SCHEDULE, this.cycleCountScheduleId);
  }

  createCycleCountScheduleForm(): void {
    let productMaintenanceAddEditModel = new CycleCountEnableConfigurationModel();
    // create form controls dynamically from model class
    this.cycleCountEnableConfigForm = this.formBuilder.group({});
    Object.keys(productMaintenanceAddEditModel).forEach((key) => {
      this.cycleCountEnableConfigForm.addControl(key, new FormControl(productMaintenanceAddEditModel[key]));
    });
    if (this.cycleCountScheduleId) {
      this.cycleCountEnableConfigForm = setRequiredValidator(this.cycleCountEnableConfigForm, ['scheduledDate']);
    }
    else {
      this.cycleCountEnableConfigForm = setRequiredValidator(this.cycleCountEnableConfigForm, ['warehouseIds', 'scheduledDate']);
    }
  }

  getWarehouseDropDown() {
    this.warehouseList = [];
    this.httpService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.userData?.userId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let warehouseList = response.resources;
          for (var i = 0; i < warehouseList.length; i++) {
            let tmp = {};
            tmp['value'] = warehouseList[i].id;
            tmp['display'] = warehouseList[i].displayName;
            this.warehouseList.push(tmp)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onSubmit() {
    if (this.cycleCountEnableConfigForm.invalid) {
      return;
    }
    let formData = []
    let updateObject = {}
    let warehouseIds = this.cycleCountEnableConfigForm.get('warehouseIds').value
    if (this.cycleCountScheduleId) {
      updateObject = {
        cycleCountScheduleId: this.cycleCountScheduleId,
        WarehouseId: this.cycleCountScheduleDetails.warehouseId,
        ScheduledDate: this.datePipe.transform(this.cycleCountEnableConfigForm.get('scheduledDate').value, 'yyyy-MM-dd'),
        CreatedUserId: this.userData.userId,
        isActive: this.cycleCountEnableConfigForm.get('isActive').value
      }
    }
    else {
      warehouseIds.forEach(value => {
        let tmp = {};
        tmp['WarehouseId'] = value;
        tmp['ScheduledDate'] = this.datePipe.transform(this.cycleCountEnableConfigForm.get('scheduledDate').value, 'yyyy-MM-dd');
        tmp['CreatedUserId'] = this.userData.userId;
        formData.push(tmp)
      });
    }
    let crudService: Observable<IApplicationResponse> =
      this.cycleCountScheduleId ? this.httpService.update(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.CYCLE_COUNT_SCHEDULE, updateObject) :
        this.httpService.create(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.CYCLE_COUNT_SCHEDULE, formData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.navigateToList();
      }
    });
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'cycle-count-enable-configuration']);
  }

  navigateToEditPage() {
    this.router.navigate(['/inventory', 'cycle-count-enable-configuration', 'view'], {
      queryParams: {
        id: this.cycleCountScheduleId
      }, skipLocationChange: true
    });
  }
}
