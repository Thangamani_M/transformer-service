import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { DealerRoutingModule } from '@inventory/components/dealer';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,FormsModule,
    LayoutModule,
    DealerRoutingModule,
    MaterialModule,
    SharedModule
  ]
})
export class DealerModule { }
