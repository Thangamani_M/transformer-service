import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealerorderstatusListComponent } from '@inventory/components/dealer';

const dealerModuleRoutes: Routes = [
    {path:'',component:DealerorderstatusListComponent,data:{title:'Dealer List'}}
];
@NgModule({
    imports: [RouterModule.forChild(dealerModuleRoutes)],
})

export class DealerRoutingModule { }
