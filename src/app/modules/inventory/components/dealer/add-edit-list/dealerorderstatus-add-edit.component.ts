import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AlertService, CrudService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { DealerOrderStatus } from '@inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
@Component({
  selector: 'app-dealerorderstatus-add-edit',
  templateUrl: './dealerorderstatus-add-edit.component.html',
})
export class DealerorderstatusAddEditComponent implements OnInit {
  @Input() id: string;
  dealerOrderStatusAddEditForm: FormGroup;
  applicationResponse: IApplicationResponse;
  errorMessage: string;
  btnName: string;
  public alphaNumericWithSpacePattern = { '0': { pattern: new RegExp('^[A-Za-z0-9 ]*$') } };
  isDialogLoaderLoading = false;
  isFormSubmitted = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data: DealerOrderStatus,
    private dialog: MatDialog,private formBuilder: FormBuilder,private alertService: AlertService,private httpServices: CrudService) {
  }

  ngOnInit(): void {
    if (this.data.dealerOrderStatusId) {
      this.btnName = "Update";
    } else {
      this.btnName = "Create";
    }
    this.dealerOrderStatusAddEditForm = this.formBuilder.group({
      displayName: this.data.displayName || '',
      dealerOrderStatusCode: this.data.dealerOrderStatusCode || '',
      description: this.data.description || ''
    });

  }
  /* Save And Update Function */
  save(): void {
    const dealerorderstatus = { ...this.data, ...this.dealerOrderStatusAddEditForm.value }
    this.isDialogLoaderLoading = true;
    this.isFormSubmitted = true;
    if (dealerorderstatus.dealerOrderStatusId != null) {
      this.httpServices.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.DEALER_ORDER_STATUS, dealerorderstatus)
        .subscribe({
          next: response => {
            this.onSaveComplete(response);
          },
          error: err => this.errorMessage = err
        });
    }
    else {
      this.httpServices.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.DEALER_ORDER_STATUS, dealerorderstatus)
        .subscribe({
          next: response => {
            this.onSaveComplete(response);
          },
          error: err => this.errorMessage = err
        });

    }
  }
  onSaveComplete(response): void {

    if (response.statusCode == 200) {
      this.dialog.closeAll();
      this.dealerOrderStatusAddEditForm.reset();
    }
    else {
      this.alertService.processAlert(response);
    }
    this.isDialogLoaderLoading = false;
    this.isFormSubmitted = false;
  }

}
