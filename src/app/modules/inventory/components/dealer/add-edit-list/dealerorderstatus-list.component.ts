import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, prepareRequiredHttpParams } from '@app/shared';
import { EnableDisable } from '@app/shared/models';
import { CrudService, RxjsService } from '@app/shared/services';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { DealerOrderStatus } from '@inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DealerorderstatusAddEditComponent } from './';
@Component({
  selector: 'app-dealerorderstatus-list',
  templateUrl: './dealerorderstatus-list.component.html'
})
export class DealerorderstatusListComponent implements OnInit {
  displayedColumns: string[] = ['select', 'displayName', 'dealerOrderStatusCode', 'description', 'createdDate', 'isActive']
  dataSource = new MatTableDataSource<DealerOrderStatus>();
  applicationResponse: IApplicationResponse;
  dealerOrderStatus: DealerOrderStatus[];
  selection = new SelectionModel<DealerOrderStatus>(true, []);
  enableDisable = new EnableDisable();
  pageIndex: number = 0;
  pageLimit: number[] = [1, 50, 75, 100];
  limit: number = 25;
  totalLength: number = 0;
  errorMessage: string;
  checkedDealerOrderStatusIds: string[] = [];
  SearchText: any = { Search: '' };
  userData: UserLogin;
  ModifiedUserId: any;
  constructor(private httpService: CrudService,
    private dialog: MatDialog, private store: Store<AppState>,
    private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  ngOnInit() {
    this.getDealerOrderStatus(this.SearchText);
  }
  applyFilter(filterValue: string) {
    this.SearchText.Search = filterValue;
    this.getDealerOrderStatus(this.SearchText);
  }
  changePage(event) {
    this.pageIndex = event.pageIndex;
    this.limit = event.pageSize;
    this.getDealerOrderStatus(this.SearchText);
  }
  //* Load Data From Server *//
  private getDealerOrderStatus(params) {
    this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.DEALER_ORDER_STATUS, undefined, false,
      prepareGetRequestHttpParams(this.pageIndex.toString(), this.limit.toString(), params))
      .subscribe(response => {
        this.applicationResponse = response;
        this.dealerOrderStatus = this.applicationResponse.resources;
        this.dataSource.data = this.dealerOrderStatus;
        this.totalLength = this.applicationResponse.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  /* AllCheckBox Checked Event */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  private resetSelectedValues() {
    this.checkedDealerOrderStatusIds = [];
    //clear check box
    this.selection = new SelectionModel<DealerOrderStatus>(true, []);
  }
  /* Open Dialog While Insert and Update Data */
  openAddDealerOrderStatusDialog(dealerOrderStatusId?): void {
    this.rxjsService.setDialogOpenProperty(true);
    var dealerOrderStatusAdd = new DealerOrderStatus();
    if (dealerOrderStatusId) {
      this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.DEALER_ORDER_STATUS, dealerOrderStatusId).subscribe(
        {
          next: response => {
            this.applicationResponse = response,
              dealerOrderStatusAdd = this.applicationResponse.resources;
            this.OpenDialogWindow(dealerOrderStatusAdd);
          }
        });
    } else {
      this.OpenDialogWindow(dealerOrderStatusAdd);
    }
  }
  private OpenDialogWindow(dealerOrderStatusAdd: DealerOrderStatus) {
    const dialogReff = this.dialog.open(DealerorderstatusAddEditComponent, { width: '450px', data: dealerOrderStatusAdd });
    dialogReff.afterClosed().subscribe(result => {
      this.selection.clear();
      this.rxjsService.setDialogOpenProperty(false);
      this.getDealerOrderStatus(this.SearchText);
    });
  }
  //* Enable and Disable DealerOrderStatus
  enableDisableDealerOrderStatus(id, status: any) {
    this.enableDisable.ids = id;
    this.enableDisable.isActive = status.currentTarget.checked;

    this.httpService
      .enableDisable(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.DEALER_ORDER_STATUS, this.enableDisable).subscribe({
        next: response => { //this.toastr.responseMessage(response);
        },
        error: err => this.errorMessage = err,
      });
  }
  /* Delete Function */
  removeSelectedDealerOrderStatus() {
    //get selected department ids
    this.selection.selected.forEach(item => {
      this.checkedDealerOrderStatusIds.push(item.dealerOrderStatusId);
    });
    if (this.checkedDealerOrderStatusIds.length > 0) {
      const message = `Are you sure you want to delete this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData
      });
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (dialogResult) {
          //convert selected id to array
          this.deleteDealerOrderStatus();
          this.resetSelectedValues();
        } else {
          this.resetSelectedValues();
        }
      });
    } else {
      this.applicationResponse.statusCode = 204;
      this.applicationResponse.message = "Please select Dealer Order Status";
      //this.toastr.responseMessage(this.applicationResponse);
    }
  }

  //Delete selected DealerOrderStatus from database
  private deleteDealerOrderStatus() {
    this.ModifiedUserId = this.userData.userId;
    this.httpService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.DEALER_ORDER_STATUS, this.checkedDealerOrderStatusIds.join(','), prepareRequiredHttpParams({ ModifiedUserId: this.ModifiedUserId }))
      .subscribe({
        next: response => {
          //this.toastr.responseMessage(response);
          this.checkedDealerOrderStatusIds = [];
          //clear check box
          this.selection = new SelectionModel<DealerOrderStatus>(true, []);
          this.getDealerOrderStatus(this.SearchText);
        },
        error: err => this.errorMessage = err
      });
  }
}
