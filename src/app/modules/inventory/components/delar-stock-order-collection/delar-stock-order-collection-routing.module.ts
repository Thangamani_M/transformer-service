import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DelarStockOrderCollectionAddEditComponent } from './delar-stock-order-collection-add-edit.component';
import { DelarStockOrderCollectionViewComponent } from './delar-stock-order-collection-view.component';
import { DelarStockOrderCollectionComponent } from './delar-stock-order-collection.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
{ path: '', component: DelarStockOrderCollectionComponent, data: { title: 'Dealar Stock OrderList' },canActivate:[AuthGuard] },
{ path: 'view', component: DelarStockOrderCollectionViewComponent, data: { title: 'Dealar Stock Order View' } ,canActivate:[AuthGuard]},
{ path: 'add-edit', component: DelarStockOrderCollectionAddEditComponent, data: { title: 'Dealar Stock Order Add/Edit' },canActivate:[AuthGuard] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})

export class DelarStockOrderCollectionRoutingModule { }
