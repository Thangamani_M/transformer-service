import { HttpParams } from '@angular/common/http';
import { Component, OnInit, } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { LoggedInUserModel, CrudService, RxjsService, ModulesBasedApiSuffix, currentComponentPageBasedPermissionsSelector$, CrudType, prepareGetRequestHttpParams, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../.../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-delar-stock-order-collection',
  templateUrl: './delar-stock-order-collection.component.html'
})
export class DelarStockOrderCollectionComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  stockManagementFilterForm: FormGroup;
  userData: UserLogin;
  otherParams;
  constructor(private crudService: CrudService, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, public dialogService: DialogService, private router: Router, private store: Store<AppState>, private rxjsService: RxjsService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Dealer Stock Order Collection",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory management', relativeRouterUrl: '' }, { displayName: 'Stock Order List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Dealer Stock Order Collection',
            dataKey: 'stockOrderId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAction: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Stock Management',
            printSection: 'print-section0',
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ header: 'Stack Order Number', field: 'stockOrderNumber', width: '200px' },
            { header: 'Status', field: 'status', width: '200px' },
            { header: 'Dealer Code', field: 'dealerCode', width: '200px' },
            { header: 'Dealer Name', field: 'dealerName', width: '200px' },
            { header: 'Branch BarCode', field: 'batchBarcode', width: '200px' },
            { header: 'Created By', field: 'createdBy', width: '200px' },
            { header: 'Creation Date', field: 'createdDate', width: '200px' },
            { header: 'Priority', field: 'priorityName', width: '200px' },
            { header: 'Issuing Warehouse', field: 'warehouse', width: '200px' },
            ],
            apiSuffixModel: DealerModuleApiSuffixModels.DEALER_STOCK_ORDER_COLLECTION,
            moduleName: ModulesBasedApiSuffix.DEALER,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
    this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {

      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.DEALER_STOCK_ORDER_COLLECTION];

      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams = { ...otherParams, userId: this.loggedInUserData?.userId }
    this.otherParams = otherParams;
    let dealerModuleApiSuffixModels: DealerModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.DEALER,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      this.dataList = data.resources
      this.totalRecords = data.totalCount;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.row = row;
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let pageIndex = this.row && this.row["pageIndex"] ? this.row["pageIndex"] : 0;
        let pageSize = this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
        this.exportList(pageIndex, pageSize);
        // this.exportExcel()
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any, otherParams?: object) {
    let queryParams;
    queryParams = this.generateQueryParams(otherParams, pageIndex, pageSize);
    let exportapi = DealerModuleApiSuffixModels.DEALER_STOCK_ORDER_COLLECTION_EXPORT
    let label = 'Dealer Stock Order Collection';
    this.crudService.downloadFile(ModulesBasedApiSuffix.DEALER,
      exportapi, null, null, queryParams).subscribe((response: any) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response) {
          this.saveAsExcelFile(response, label);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }
  generateQueryParams(otherParams, pageIndex, pageSize) {
    pageIndex = 0
    let queryParamsString;
    queryParamsString = new HttpParams({ fromObject: this.otherParams })
      .set('pageIndex', pageIndex)
      .set('pageSize', pageSize);

    queryParamsString.toString();
    return '?' + queryParamsString;
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFileOld(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFileOld(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/inventory', 'dealer-stock-order-collection', 'view'], {
          queryParams: {
            id: editableObject['stockOrderId'],
          }, skipLocationChange: true
        });
        break;
    }
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
