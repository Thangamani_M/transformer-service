import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { DealerStockCollectionAddEditModal, dealerStockCollectionFormArrayModel } from '@modules/technical-management/models/standby-roster-technician.module';
import { select, Store } from '@ngrx/store';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-delar-stock-order-collection-add-edit',
  templateUrl: './delar-stock-order-collection-add-edit.component.html',
  styleUrls: ['./delar-stock-order-collection-add-edit.component.scss']
})
export class DelarStockOrderCollectionAddEditComponent implements OnInit {

  stockOrderId: any;
  stockOrderCollectionDetails: any;
  stockOrderCollectionBatchDetails: any;
  loggedInUserData: LoggedInUserModel;
  totalRecords = 0;
  pageIndex: number = 0;
  pageLimit: number;
  userData: UserLogin;
  dealerStockCollectionInfoForm: FormGroup;
  dealerStockCollectionSerialInfoForm: FormGroup;
  selectedTabIndex: number = 0;
  serialCollectionItems: FormArray;
  stockGetDetails: any;
  panelOpenState: boolean = false;
  varianceSerialNumbers: any = [];
  technicianStockOrderCollectionId: string;
  technicianStockOrderCollectionItemId: string;
  itemId: string;
  pageSize: number = 10;
  varianceEscalationModal: boolean = false;
  varianceModalIndex: number = 0;
  varianceEscalationForm: FormGroup;
  commentsDropDown: any = [];
  selectedSerialNumber: string = '';
  selectedSerialNumbers=[];
  varianceEscalationNo: string = '';

  cols = [
    { field: 'stockCode', header: 'Stock Code' },
    { field: 'stockDescription', header: 'Stock Description' },
    { field: 'isConsumable', header: 'Consumable' },
    { field: 'issuedQty', header: 'Issued Qty' },
    { field: 'collectedQty', header: 'Collected Qty' },
    { field: 'varianceQty', header: 'Variance Qty' },
    { field: 'unitPrice', header: 'Unit Price(R)' },
    { field: 'totalPrice', header: 'Total Price(R)' }
  ];

  @ViewChild(SignaturePad, { static: false }) signaturePad: any;

  public signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 370,
    'canvasHeight': 80
  };

  constructor(private router: Router, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>) {
    this.stockOrderId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.getDealarViewDetails();
    this.createSerialInfoform();

    this.dealerStockCollectionInfoForm = this.formBuilder.group({
      comments: ['', Validators.required]
    });
    this.varianceEscalationForm = this.formBuilder.group({
      comments: ['', Validators.required]
    });
  }

  getDealarViewDetails() {
    if (this.stockOrderId) {
      this.crudService.get(ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.DEALER_STOCK_ORDER_COLLECTION, this.stockOrderId)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources) {
            this.stockOrderCollectionDetails = response.resources.sealerStockOrderCollectionsDetails;
            this.stockOrderCollectionBatchDetails = response.resources.dealerStockOrderCollectionsBatch;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  onSubmit(bins) {
    if (this.dealerStockCollectionInfoForm.invalid) {
      return;
    }

    if (this.signaturePad.signaturePad._data.length == 0) {
      this.snackbarService.openSnackbar('Dealer Signature is required', ResponseMessageTypes.WARNING);
      return;
    }

    let formData = new FormData();
    const data = {
      technicianStockOrderCollectionId: bins?.technicianStockOrderCollectionId,
      technicianStockOrderCollectionBatchId: bins?.technicianStockOrderCollectionBatchId,
      signaturePath: null,
      comments: this.dealerStockCollectionInfoForm.get("comments").value,
      createdUserId: this.userData.userId

    }

    if (this.signaturePad.signaturePad._data.length > 0) {
      let imageName = this.stockOrderCollectionDetails.stockOrderNumber + "-signature.jpeg";
      const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
      const imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' });
      formData.append("document_files", imageFile);
    }

    formData.append("documentDetails", JSON.stringify(data));

    this.rxjsService.setFormChangeDetectionProperty(true);
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.DEALER_STOCK_ORDER_COLLECTION, formData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.panelOpenState = false;
        this.getDealarViewDetails();
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  drawComplete() {

  }

  clearPad() {
    this.signaturePad.clear();
  }

  ngAfterViewInit() {
    if (this.signaturePad) {
      this.signaturePad.set('minWidth', 1);
      this.signaturePad.resizeCanvas();
      this.signaturePad.clear();
    }
  }

  /* Serial Number Scan Process*/

  /* Create Form Controls */
  createSerialInfoform(): void {
    let stockOrderModel = new DealerStockCollectionAddEditModal();
    // create form controls dynamically from model class
    this.dealerStockCollectionSerialInfoForm = this.formBuilder.group({
      serialCollectionItems: this.formBuilder.array([]),
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.dealerStockCollectionSerialInfoForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
  }

  /* Create FormArray controls */
  createSerialNumberItemsModel(interBranchModel?: dealerStockCollectionFormArrayModel): FormGroup {
    let interBranchModelData = new dealerStockCollectionFormArrayModel(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      formControls[key] = [interBranchModelData[key], (key === 'serialNumbers') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }


  /* Create FormArray */
  get getNewSerialInfoItemsArray(): FormArray {
    if (!this.dealerStockCollectionSerialInfoForm) return;
    return this.dealerStockCollectionSerialInfoForm.get("serialCollectionItems") as FormArray;
  }

  scanSerialNumbers(obj: any) {
    this.technicianStockOrderCollectionId = obj?.technicianStockOrderCollectionId;
    this.technicianStockOrderCollectionItemId = obj?.technicianStockOrderCollectionItemId;
    this.itemId = obj.itemId;
    this.getSerialNumberDetails();
  }

  getSerialNumberDetails() {
    this.stockGetDetails = [];
    this.varianceEscalationModal = false;
    let params = new HttpParams().set('TechnicianStockOrderCollectionId', this.technicianStockOrderCollectionId)
      .set('TechnicianStockOrderCollectionItemId', this.technicianStockOrderCollectionItemId)
      .set('ItemId', this.itemId).set('IsConsumable', 'false')
    this.crudService.get(ModulesBasedApiSuffix.DEALER,
      DealerModuleApiSuffixModels.DEALER_STOCK_ORDER_COLLECTION_STOCK_DETAILS, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode == 200) {
          this.stockGetDetails = response.resources;
          this.getNewSerialInfoItemsArray.clear();
          this.selectedTabIndex = 1;
          let serialNumber = response.resources.serialNumber == null ? [] :
            response.resources.serialNumber.split(',');
          let collectedSerialNumber; collectedSerialNumber= response.resources.collectedSerialNumber == null ? [] :
            response.resources.collectedSerialNumber.split(',');
          let varianceSerialNumbers; varianceSerialNumbers = response.resources.varianceSerialNumber == null ? [] :
            response.resources.varianceSerialNumber.split(',');

          collectedSerialNumber =  collectedSerialNumber?.concat(varianceSerialNumbers);
          this.rxjsService.setGlobalLoaderProperty(false);
          this.serialCollectionItems = this.getNewSerialInfoItemsArray;
          if (serialNumber.length > 0) {
            serialNumber.forEach((serials) => {
              let stockOrder = {}
              stockOrder['serialNumber'] = serials; stockOrder['isCollected'] = false;
              stockOrder['isCollectedTemp'] = false;
              stockOrder['isScanned'] = false; stockOrder['isVariance'] = false;
              this.serialCollectionItems.push(this.createSerialNumberItemsModel(stockOrder));
            });
          }

          if (this.serialCollectionItems.value.length > 0 && collectedSerialNumber.length > 0) {
            this.getNewSerialInfoItemsArray.controls.forEach(control => {
              collectedSerialNumber.forEach(data => {
                if (control.get('serialNumber').value == data) {
                  control.get('isCollected').setValue(true);
                  control.get('isCollectedTemp').setValue(true);                  
                }
              });
            });
          }

          if (this.serialCollectionItems.value.length > 0 && varianceSerialNumbers.length > 0) {
            this.getNewSerialInfoItemsArray.controls.forEach(control => {
              varianceSerialNumbers.forEach(items => {
                if (control.get('serialNumber').value == items) {
                  control.get('isVariance').setValue(true);
                }
              });
            });
          }
        }else {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  saveVarianceQty(index: number) {
    let collected = this.getNewSerialInfoItemsArray.controls[index].get('isCollected').value;
    let variance = this.getNewSerialInfoItemsArray.controls[index].get('isVariance').value;
    if (variance) return;
    if (!collected) {
      this.getNewSerialInfoItemsArray.controls[index].get('isScanned').patchValue(true);
      this.varianceEscalationModal = true;
      this.varianceModalIndex = 0;
      this.selectedSerialNumber = this.getNewSerialInfoItemsArray.controls[index].get('serialNumber').value;
    }
    else {
      this.snackbarService.openSnackbar('This item is already collected', ResponseMessageTypes.WARNING);
      return;
    }
  }

  saveCollectedQty(index: number) {

     if(!this.getNewSerialInfoItemsArray.controls[index].value.isCollectedTemp){
      let serialNumber = this.getNewSerialInfoItemsArray.controls[index].value.serialNumber;
      let collected = this.getNewSerialInfoItemsArray.controls[index].value.isCollected; 
 
      if(!collected) {
        this.selectedSerialNumbers.push(serialNumber);
      }else {
        let index = this.selectedSerialNumbers.findIndex(x=>x==serialNumber);
        if(index >= 0)
          this.selectedSerialNumbers.splice(index,1);
      }
      this.getNewSerialInfoItemsArray.controls[index].get('isCollected').setValue(!collected);
     }else {
      if(this.getNewSerialInfoItemsArray.controls[index].value.isVariance)
        this.snackbarService.openSnackbar('The Serial Number is in variance.', ResponseMessageTypes.WARNING);
      else
        this.snackbarService.openSnackbar('The Serial Number is collected already.', ResponseMessageTypes.WARNING);
     }

    // let variance = this.getNewSerialInfoItemsArray.controls[index].get('isVariance').value;
    // this.getNewSerialInfoItemsArray.controls[index].get('isCollectedTemp').setValue(!collected);
    // if (variance) return;
    // if (!collected) {
    //   this.selectedSerialNumber = this.getNewSerialInfoItemsArray.controls[index].get('serialNumber').value;
    //   this.onSave('qty');
    // }
    // else {
    //   this.snackbarService.openSnackbar('This item is already collected', ResponseMessageTypes.WARNING);
    //   return;
    // }
  }
  showVarianceComments() {
    this.varianceEscalationModal = true;
    this.varianceModalIndex = 1;
  }
  collect(type){
    this.varianceEscalationModal = false;
    let postData = {
      "technicianStockOrderCollectionId": this.stockGetDetails?.technicianStockOrderCollectionId,
      "technicianStockOrderCollectionItemId": this.stockGetDetails?.technicianStockOrderCollectionItemId,
      "itemId": this.stockGetDetails?.itemId,
      "quantity": this.stockGetDetails?.isConsumable ? this.dealerStockCollectionSerialInfoForm.get('quantity').value : this.selectedSerialNumbers.length,
      "serialNumber": this.selectedSerialNumbers.toString(),
      "isConsumable": this.stockGetDetails?.isConsumable,
      "isVarianceEscalation": type == 'qty' ? false : true,
      "createdUserId": this.userData.userId,
      "varianceEscalationComments": this.varianceEscalationForm.get('comments').value
    }

    this.rxjsService.setFormChangeDetectionProperty(true);
    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.DEALER_STOCK_ORDER_COLLECTION, postData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateView();
      }
    });
  }
  navigateView(){
    this.router.navigate(['/inventory', 'dealer-stock-order-collection', 'view'], {
      queryParams: {
        id: this.stockOrderId,
      }, skipLocationChange: true
    });
  }
  onSave(type: any) {

    if (type == 'flag' && this.varianceEscalationForm.invalid) {
      return;
    }
    this.varianceEscalationModal = false;
    let postData = {
      "technicianStockOrderCollectionId": this.stockGetDetails?.technicianStockOrderCollectionId,
      "technicianStockOrderCollectionItemId": this.stockGetDetails?.technicianStockOrderCollectionItemId,
      "itemId": this.stockGetDetails?.itemId,
      "quantity": this.stockGetDetails?.isConsumable ?
        this.dealerStockCollectionSerialInfoForm.get('quantity').value : 1,
      "serialNumber": this.selectedSerialNumber,
      "isConsumable": this.stockGetDetails?.isConsumable,
      "isVarianceEscalation": type == 'qty' ? false : true,
      "createdUserId": this.userData.userId,
      "varianceEscalationComments": this.varianceEscalationForm.get('comments').value
    }

    this.rxjsService.setFormChangeDetectionProperty(true);
    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.DEALER,
        DealerModuleApiSuffixModels.DEALER_STOCK_ORDER_COLLECTION, postData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.varianceEscalationNo = response.resources;
        this.varianceEscalationModal = true;
        this.varianceModalIndex = 2;
      }
    });
  }

  /* Navigation */
  navigateToList() {
    this.router.navigate(['/inventory', 'dealer-stock-order-collection'], {
      queryParams: {
        tab: 0,
      }, skipLocationChange: true
    });
  }

  navigateToViewPage() {
    this.router.navigate(['/inventory', 'dealer-stock-order-collection', 'view'], {
      queryParams: {
        id: this.stockOrderId,
      }, skipLocationChange: true
    });
  }

  navigateToEditPage() {
    this.selectedTabIndex = 0;
    this.getDealarViewDetails();
  }

  onCrudRequst(row) {
    this.router.navigate(["inventory/delar-stock-order/stackview"], {
      queryParams: {
        technicianStockOrderCollectionId: this.technicianStockOrderCollectionId,
        technicianStockOrderCollectionItemId: this.technicianStockOrderCollectionItemId, ItemId: this.itemId, IsConsumable: row['isConsumable']
      }
    });
  }

  loadPaginationLazy(event) {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
  }

}

