import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService,CrudType, IApplicationResponse,currentComponentPageBasedPermissionsSelector$,  LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { DealerModuleApiSuffixModels } from '@modules/dealer';
import { INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../.../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-delar-stock-order-collection-view',
  templateUrl: './delar-stock-order-collection-view.component.html',
  styleUrls: ['./delar-stock-order-collection-view.component.scss']
})

export class DelarStockOrderCollectionViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  stockOrderId: any;
  stockOrderCollectionDetails: any;
  dealerStockOrderCollectionDetail:any;
  stockOrderCollectionBatchDetails: any;
  userData: UserLogin;
  cols: any[];
  pageSize: number = 10;
  panelOpenState: boolean = false;
  primengTableConfigProperties: any
  constructor(private snackbarService:SnackbarService,private router: Router,private activatedRoute: ActivatedRoute,private crudService: CrudService,
    private rxjsService: RxjsService,private store: Store<AppState>){
    super();
    this.stockOrderId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption:'Dealer Stock Order Collection',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Dealer Stock Order Collection', relativeRouterUrl: '/inventory/dealer-stock-order-collection' }, { displayName: ' View Dealer Stock Order Collection', relativeRouterUrl: '', }],
      tableComponentConfigs: {
          tabsList: [
              {
                  enableAction: true,
                  enableEditActionBtn: true,
                  enableBreadCrumb: true,
              }
          ]
      }
  }
  this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.getDealarViewDetails()
    this.cols = [
      { field: 'stockCode', header: 'Stock Code' },
      { field: 'stockDescription', header: 'Stock Description' },
      { field: 'isConsumable', header: 'Consumable' },
      { field: 'issuedQty', header: 'Issued Qty' },
      { field: 'collectedQty', header: 'Collected Qty' },
      { field: 'varianceQty', header: 'Variance Qty' },
      { field: 'unitPrice', header: 'Unit Price(R)' },
      { field: 'totalPrice', header: 'Total Price(R)' }
    ];
  }
  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.DEALER_STOCK_ORDER_COLLECTION];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onShowValue(response?: any) {
    this.dealerStockOrderCollectionDetail = [
      {
        name: 'Basic Info', columns: [
          { name: 'Stock Order Number', value: response?.stockOrderNumber},
          { name: 'Dealer Code', value: response?.dealerCode},
          { name: 'Dealer Name', value: response?.dealerName},
          { name: 'Dealer Stock Location', value: response?.dealerLocation},
          { name: 'Reporter Name', value: response?.pickerName},
          { name: 'Issuing Warehouse', value: response?.warehouse},
          { name: 'Created Date And Time', value: response?.createdDate},
          { name: 'Priority', value: response?.priorityName},
          { name: 'Status', value: response?.status, statusClass: response ? response?.cssClass : '' },
        ]
      }
    ]
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getDealarViewDetails() {
    if(this.stockOrderId){
      this.crudService.get(ModulesBasedApiSuffix.DEALER, 
      DealerModuleApiSuffixModels.DEALER_STOCK_ORDER_COLLECTION, this.stockOrderId)
      .subscribe((response: IApplicationResponse) => {
          if(response.resources && response.statusCode == 200){
            this.stockOrderCollectionDetails = response.resources.sealerStockOrderCollectionsDetails;
            this.onShowValue(response.resources.sealerStockOrderCollectionsDetails);
            this.stockOrderCollectionBatchDetails = response.resources.dealerStockOrderCollectionsBatch;
          }
          else {
            this.rxjsService.setGlobalLoaderProperty(false);
          }
      });
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.EDIT:

        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/inventory','delar-stock-order', 'add-edit'], {
          queryParams: {
            id: this.stockOrderId,
          }, skipLocationChange:true
        });  
        break;
    }
  }

  loadPaginationLazy(event){
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
  }
}
