import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxPrintModule } from 'ngx-print';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { DelarStockOrderCollectionAddEditComponent } from './delar-stock-order-collection-add-edit.component';
import { DelarStockOrderCollectionRoutingModule } from './delar-stock-order-collection-routing.module';
import { DelarStockOrderCollectionViewComponent } from './delar-stock-order-collection-view.component';
import { DelarStockOrderCollectionComponent } from './delar-stock-order-collection.component';

@NgModule({
  declarations: [
    DelarStockOrderCollectionComponent,
    DelarStockOrderCollectionViewComponent,
    DelarStockOrderCollectionAddEditComponent
  ],
  imports: [
    CommonModule,
    DelarStockOrderCollectionRoutingModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    NgxPrintModule,
    TableModule,
    InputTextModule,
    DropdownModule,
    ButtonModule,
    DialogModule,
    TabViewModule,
    MultiSelectModule
  ]
})
export class DelarStockOrderCollectionModule { }
