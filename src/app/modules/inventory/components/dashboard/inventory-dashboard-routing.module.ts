import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InventoryDashboardComponent } from '@inventory/components';

const inventoryDashboardModuleRoutes: Routes = [
    { path: '', component: InventoryDashboardComponent, data: { title: 'Inventory Dashboard' } }
];
@NgModule({
    imports: [RouterModule.forChild(inventoryDashboardModuleRoutes)],
})
export class InventoryDashboardRoutingModule { }
