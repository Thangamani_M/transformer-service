import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { InventoryDashboardRoutingModule } from '@inventory/components/dashboard';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    InventoryDashboardRoutingModule,
    MaterialModule,
    SharedModule,
    LayoutModule
  ]
})
export class InventoryDashboardModule { }
