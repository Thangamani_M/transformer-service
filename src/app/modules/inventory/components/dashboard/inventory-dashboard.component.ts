import { Component } from '@angular/core';
import { RxjsService } from '@app/shared/services';
@Component({
    selector: 'inventory-dashboard',
    templateUrl: './inventory-dashboard.component.html',
    styleUrls: ['./inventory-dashboard.component.css']

})

export class InventoryDashboardComponent {
    constructor(
        private rxjsService: RxjsService) {

    }
    ngOnInit() {
        this.rxjsService.setGlobalLoaderProperty(false);
    }

}

