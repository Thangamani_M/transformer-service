import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockMaintenanceComponent } from '@inventory/components/stock-maintenance';
import { StockMaintenanceAddEditComponent } from './stock-maintenance-add-edit.component';
import { StockMaintenanceViewComponent } from './stock-maintenance-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: StockMaintenanceComponent, data: { title: 'Stock Maintenance List' }, canActivate: [AuthGuard] },
    { path: 'view', component: StockMaintenanceViewComponent, data: { title: 'Stock Maintenance View' }, canActivate: [AuthGuard] },
    { path: 'add-edit', component: StockMaintenanceAddEditComponent, data: { title: 'Stock Maintenance Add Edit' }, canActivate: [AuthGuard] },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class StockMaintenanceRoutingModule { }