import { Component, OnInit } from '@angular/core';
import { CrudService, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-manual-respository-dialog',
  templateUrl: './manual-respository-dialog.component.html',
  styleUrls: ['./manual-respository-dialog.component.scss'],
})
export class ManualRespositoryDialogComponent implements OnInit {
  todayDate: any;
  isSubmitDialog: boolean;
  showError: boolean;
  manualRepository: any[] = [];
  selectedManualRepository: any[] = [];
  selectedRepository: string[] = [];
  findTrueArr: any = [];

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig,
    public ref: DynamicDialogRef, private crudService: CrudService) {
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnInit(): void {
    if (this.config?.data?.resources) {
      // this.config?.data?.resources.forEach((el, i) => {
      //   this.onLoadValue(el);
      // });
      this.manualRepository = this.ptreeRecursion(this.config?.data?.resources);
    }
    // this.onSetValue();
  }

  onLoadValue(el) {
    const obj = {
      label: el?.repositoryName,
      data: el?.repositoryId,
      expandedIcon: el?.isFolder ? "pi pi-folder-open" : '',
      collapsedIcon: el?.isFolder ? "pi pi-folder" : '',
      selected: el?.isselected,
    }
    if (el?.isselected) {
      this.selectedRepository.push(el?.repositoryId);
    }
    el?.children.forEach((el1, j) => {
      obj['children'] = (j == 0) ? [] : obj['children'];
      obj['children'].push({
        label: el1?.repositoryName,
        data: el1?.repositoryId,
        expandedIcon: el1?.isFolder ? "pi pi-folder-open" : '',
        collapsedIcon: el1?.isFolder ? "pi pi-folder" : '',
        selected: el1?.isselected,
      });
      if (el1?.isselected) {
        this.selectedRepository.push(el1?.repositoryId);
      }
      el1?.children.forEach((el2, k) => {
        obj['children']['children'] = (k == 0) ? [] : obj['children']['children'];;
        obj['children']['children'].push({
          label: el2?.repositoryName,
          data: el2?.repositoryId,
          expandedIcon: el2?.isFolder ? "pi pi-folder-open" : '',
          collapsedIcon: el2?.isFolder ? "pi pi-folder" : '',
          selected: el2?.isselected,
        });
        if (el2?.isselected) {
          this.selectedRepository.push(el2?.repositoryId);
        }
      });
    });
    this.manualRepository.push(obj);
  }
  ptreeRecursion(res):any{
    let c=0;
    if(res && res.length>0){
      res.forEach((element, i) => {
        element.label = element.repositoryName;
        element.data = element.repositoryId;
        element.selected =  element?.isselected,
        element.expandedIcon = element?.isFolder ? "pi pi-folder-open" : '';
        element.collapsedIcon =  element?.isFolder ? "pi pi-folder" : '';
        if(element?.selected ){
          this.selectedRepository.push(element?.repositoryId);
        }
        // check /uncheck start
        let allTrueFilter =[];let allFalseFilter =[];
        allTrueFilter = element?.children.filter(x=>x.isselected==true);
        allFalseFilter = element?.children.filter(x=>x.isselected==false);
        if (element?.isFolder && element?.children.length>0  && ( element?.children.length != allTrueFilter.length && element?.children.length != allFalseFilter.length &&  element?.children.some(el => el.isselected === true) ) ) {
          this.selectedManualRepository.push(element); 
          element['partialSelected'] = true;
        }
        else{
          let temp = {
            collapsedIcon:  element?.isFolder ? "pi pi-folder" : '',
            data: element.repositoryId,
            expandedIcon: element?.isFolder ? "pi pi-folder-open" : '',
            label: element.repositoryName,
            selected: element?.isselected
          }
          if(element?.selected )
            this.selectedManualRepository.push(element); 
        }
        // check /uncheck end
        if(element.children && element.children.length>0){
          element.children = this.ptreeRecursion(element.children);
        }
      });
      return res;
    }     
  }
  onSetValue() {
    this.manualRepository.forEach((el, i) => {
      this.findTrueArr = [];
      if (this.selectedRepository.indexOf(el?.data) != -1 && el?.selected) {
        this.selectedManualRepository.push(el);
        if (el?.children?.length && el?.selected) {
          this.findTrueArr = [];
          if (this.isSomeChildSelected(el?.children) && !this.isAllChildSelected(el?.children)) {
            el['partialSelected'] = true;
          }
          this.isAddChildren(el?.children);
        }
      } else if (this.isSomeChildSelected(el?.children) && !this.isAllChildSelected(el?.children)) {
        this.isAddChildren(el?.children);
        el['partialSelected'] = true;
      }
    });
  }

  isAddChildren(data) {
    data?.forEach((el) => {
      if (this.selectedRepository.indexOf(el?.data) !== -1 && el?.selected) {
        this.selectedManualRepository.push(el);
      }
      if (el?.children) {
        this.isAddChildren(el?.children);
      }
    });
  }

  isAllChildSelected(data) {
    data?.forEach(el => {
      this.findTrueArr.push(el?.selected);
      if (el?.children) {
        this.isAllChildSelected(data);
      }
    });
    return this.findTrueArr.every(el => el === true);
  }

  isSomeChildSelected(data) {
    data?.forEach(el => {
      this.findTrueArr.push(el?.selected);
      if (el?.children) {
        this.isSomeChildSelected(data);
      }
    });
    return this.findTrueArr.some(el => el === true);
  }

  onSelectCheckbox(event) {
    if (this.selectedRepository.indexOf(event?.node?.data) == -1) {
      this.selectedRepository.push(event?.node?.data);
      this.findTrueArr = [];
      if (event?.node?.parent && this.selectedRepository.indexOf(event?.node?.parent?.data) == -1 && this.isAllChildSelected(event?.node?.children)) {
        this.selectedRepository.push(event?.node?.parent?.data);
      }
    }
    if (!this.onAlreadyExist(event?.node?.children) && event?.node?.children) {
      this.selectedManualRepository.push(event?.node);
    } else if (!this.selectedManualRepository.find(el1 => event?.node?.data === el1?.data)) {
      this.selectedManualRepository.push(event?.node);
    }
    if (event?.node?.children) {
      this.addChildren(event?.node?.children);
    }
  }

  onAlreadyExist(data) {
    let res;
    if (data?.length) {
      data?.forEach(el => {
        res = this.selectedManualRepository.find(el1 => el?.data === el1?.data);
      });
    }
    return res;
  }

  addChildren(data) {
    data?.forEach((el) => {
      if (this.selectedRepository.indexOf(el?.data) == -1) {
        this.selectedRepository.push(el?.data);
      }
      if (!this.onAlreadyExist(el?.children) && el?.children) {
        this.selectedManualRepository.push(el);
      } else if (!this.selectedManualRepository.find(el1 => el?.data === el1?.data)) {
        this.selectedManualRepository.push(el);
      }
      if (el?.children) {
        this.addChildren(el?.children);
      }
    });
  }

  onUnselectCheckbox(event) {
    if (this.selectedRepository.indexOf(event?.node?.data) !== -1) {
      this.selectedRepository.splice(this.selectedRepository.indexOf(event?.node?.data), 1);
      if (event?.node?.parent && this.selectedRepository.indexOf(event?.node?.parent?.data) !== -1) {
        this.selectedRepository.splice(this.selectedRepository.indexOf(event?.node?.parent?.data), 1);
      }
    }
    if (this.onAlreadyExist(event?.node) && event?.node?.children) {
      this.selectedManualRepository.splice(this.selectedManualRepository.findIndex(el => event?.node?.data == el?.data), 1);
    } else if (this.selectedManualRepository.find(el1 => event?.node?.data === el1?.data)) {
      this.selectedManualRepository.splice(this.selectedManualRepository.findIndex(el => event?.node?.data == el?.data), 1);
    }
    if (event?.node?.children) {
      this.removeChildren(event?.node?.children);
    }
  }

  removeChildren(data) {
    data?.forEach((el) => {
      if (this.selectedRepository.indexOf(el?.data) != -1) {
        this.selectedRepository.splice(this.selectedRepository.indexOf(el?.data), 1);
      }
      if (this.selectedManualRepository.find(el1 => el?.data === el1?.data)) {
        this.selectedManualRepository.splice(this.selectedManualRepository.findIndex(el1 => el?.data == el1?.data), 1);
      }
      if (el?.children) {
        this.removeChildren(el?.children);
      }
    });
  }

  btnCloseClick() {
    this.ref.close(false);
  }

  onSubmitDialog() {
    this.showError = false;
    if (this.selectedRepository?.length == 0) {
      this.showError = true;
      return;
    }
    const manualObject = {
      itemId: this.config?.data?.itemId,
      repositoryId: this.selectedRepository.toString(),
      createdUserId: this.config?.data?.createdUserId,
    }
    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
    if (this.selectedRepository?.length) {
      this.isSubmitDialog = true;
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE_MANUALS, manualObject).subscribe((res: any) => {
        if (res?.isSuccess == true && res?.statusCode == 200) {

        }
        this.ref.close(res);
        this.rxjsService.setDialogOpenProperty(false);
        this.isSubmitDialog = false;
      })
    }
  }
}
