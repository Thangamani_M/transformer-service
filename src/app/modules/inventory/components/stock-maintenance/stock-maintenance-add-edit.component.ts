import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatCheckboxChange, MatDialog } from '@angular/material';
import { DateAdapter } from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, CustomDirectiveConfig, formConfigs, getMatMultiData, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService, currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR } from '@app/shared';
import { ManualRespositoryDialogComponent } from '@modules/inventory';
import { productDetailsArrayModal, ProductSkillMappingAddEditModel, StockMaintenanceProductAddEditModal, StockMaintenanceProductMaintenanceModel, StockMaintenanceProductThresholdModel, stockProductWarehouseModal } from '@modules/inventory/models/stock-maintenances.model';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { forkJoin, Observable } from 'rxjs';
import { pairwise, tap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { BarcodePrintComponent } from '../order-receipts/barcode-print';

@Component({
  selector: 'app-stock-maintenance-add-edit',
  templateUrl: './stock-maintenance-add-edit.component.html',
  styleUrls: ['./stock-maintenance-add-edit.component.scss']
})
export class StockMaintenanceAddEditComponent implements OnInit {

  productMaintenanceForm: FormGroup;
  stockMaintenancesDetail: any = {};
  stockId: any;
  costCourseList: any;
  warrantyPeriodsList: any;
  ownershipList: any;
  systemTypeList: any;
  componentGroupList: any;
  componentList: any;
  brandList: any;
  conditionList: any;
  stockCodesList: any = [];
  locationList: any = [];
  selectedOptions: any = [];
  thresholdLocationList: any;
  userData: any;
  isSaveMaintenance: boolean = true;
  locationWarehouseSelectedOption: any = [];
  productWarehouseFrom: FormGroup;
  productThresholdDetailForm: FormGroup;
  constNewStockCode: any;
  errorMessage: string;
  updateTechnicalSkillLevel = true;
  isButtondisabled = false;
  productSkillMappingForm: FormGroup;
  skillMappingGetDetails: any;
  getSubCategoryDropdown = [];
  breadCrumbTitle = "product - maintenance";
  selectedThresholdLocations: any = [];
  formConfigs = formConfigs;
  validateInputConfig = new CustomDirectiveConfig();
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  alphaNumericConfig = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  selectedTabIndex = 0;
  isMandetory: boolean = false;
  isinvalid: boolean = false;
  selectedAttachment: any = '';
  startTodayDate = 0;
  isChange: boolean = true;
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  isMaximum: boolean = false;
  isSubmit: boolean = false;
  selectedFiles = new Array();
  totalFileSize = 0;
  maxFileSize = 104857600; //in Bytes 100MB
  stockMaintainanceHeaderImage: any;
  isSkillMapping = false;
  manualRepositorySubscritption: any;
  showTechnicalSkillLevel: boolean = false;
  productWarehouseDetail;
  productThresholdDetail;
  tempProductWarehouseList = [];
  isPatchProductWarehouseId = false;
  isPatchProductThresholdWarehouseId = false;

  districtDropdown: any = [];
  itemSellableTypes: any = [];
  stockProductWarehouseFormArray: FormArray;
  productDetailsArray: FormArray;
  sellableList = [{ value: true, display: 'Yes' }, { value: false, display: 'No' }];
  minDate = new Date();
  selectAllProcurable: boolean = false;
  selectProcureIndex: number;
  buttonDisabled: boolean = false;
  invalidDates: boolean;
  previousConditionId;
  showConditionPopup: boolean = false;
  @Input() myAttribute: string;//This should be your ngModel attribute
  districtsDetail = [];
  barcodes: any = [];
  isShowRepairCode: boolean = false;
  productThresholdWarehouseItems = [];
  isThresholdFlag = true;
  roleName: any;
  isCondition = null;
  isEanble = false;
  isDisctountSkillMapping: boolean = true;
  stockList;
  condition;
  sellableAll = null;
  procurableAll = null;
  editPermissions: any = {
    "Product Maintenance": { edit: false, view: false },
    "Product - Warehouse": { edit: false, view: false },
    "Product - Threshold": { edit: false, view: false },
    "Product - Skill Mapping": { edit: false, view: false }
  }
  constructor(private crudService: CrudService, private dateAdapter: DateAdapter<Date>, private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    private snackbarService: SnackbarService, private dialogService: DialogService, private router: Router, private store: Store<AppState>, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private dialog: MatDialog) {
    this.stockId = this.activatedRoute.snapshot.queryParams.id;
    this.dateAdapter.setLocale('en-GB'); //dd/MM/yyyy
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    });

    this.roleName = this.userData.roleName;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.createProductMaintenanceForm();
    this.getProductWarehouseDropdown();

    this.getDropdownDetailStockMaintenance().subscribe((response: IApplicationResponse[]) => {

      this.costCourseList = response[0].resources;
      this.warrantyPeriodsList = response[1].resources;
      this.ownershipList = response[2].resources;
      this.systemTypeList = response[3].resources;
      this.componentGroupList = response[4].resources;
      this.componentList = response[5].resources;
      this.brandList = response[6].resources;
      this.conditionList = response[7].resources;
      let stockList = response[8].resources.filter(x => x.id != this.stockId);
      this.getProductMaintenanceDetails();
      let conditionId = this.productMaintenanceForm.get('conditionId').value;
      if (this.conditionList.find(x => x.id == conditionId)?.displayName == "Refurbished") {
        this.productMaintenanceForm.controls['newStockCode'].enable();
      }
      else
        this.productMaintenanceForm.controls['newStockCode'].setValue('');

      for (let i = 0; i < stockList.length; i++) {
        let temp = {};
        temp['display'] = stockList[i].displayName;
        temp['value'] = stockList[i].id;
        this.stockCodesList.push(temp);

      }
      this.stockList = stockList;

      if (this.conditionList.find(x => x.id == conditionId)?.displayName == "RepairCode") {
        //  this.isShowRepairCode = true;
        //  this.isSkillMapping= true;
      }
      // if (response[9].isSuccess) {
      //   this.stockMaintenancesDetail = response[9].resources;
      //   let productmaintenanceDetail = {
      //     stockId: response[9].resources.stockId,
      //     costPrice: response[9].resources.costPrice,
      //     costSourceId: response[9].resources.costSourceId,
      //     warrentyPeriodId: response[9].resources.warrentyPeriodId,
      //     ownershipId: response[9].resources.ownershipId,
      //     systemTypeId: response[9].resources.systemTypeId,
      //     componentGroupId: response[9].resources.componentGroupId,
      //     componentId: response[9].resources.componentId,
      //     brandId: response[9].resources.brandId,
      //     labourComponent: response[9].resources.labourComponent,
      //     conditionId: response[9].resources.conditionId,
      //     newStockCode: response[9].resources.newStockCodeid == null ? '' : response[9].resources.newStockCodeid,
      //     fullStockDescription: response[9].resources.fullStockDescription,
      //     isNotSerialized: response[9].resources.isNotSerialized,
      //     createdUserId: this.userData.userId,
      //     stockCodeBarcode: response[9].resources.stockCodeBarcode,
      //   }
      //   this.constNewStockCode = response[9].resources.conditionId == null ? '' : response[9].resources.conditionId;
      //   this.isSaveMaintenance = response[9].resources.conditionId == null ? true : (this.conditionList.find(x => x.id == response[9].resources.conditionId).displayName === 'Refurbished' ? (response[9].resources.newStockCodeid == null ? true : false) : false)
      //   this.productMaintenanceForm.patchValue(productmaintenanceDetail);
      //   if (this.stockMaintenancesDetail.materialType != null && this.stockMaintenancesDetail.materialType == 'ERSA' && this.stockMaintenancesDetail.valuationClass != null && this.stockMaintenancesDetail.valuationClass == '3060' && this.stockMaintenancesDetail.condition == 'New' && this.stockMaintenancesDetail.condition != null) {
      //     this.productMaintenanceForm.get('warrentyPeriodId').setValidators([Validators.required]);
      //     this.isMandetory = true;
      //   }
      //   else {
      //     this.isMandetory = false;
      //     this.productMaintenanceForm.get('warrentyPeriodId').clearValidators();
      //     this.productMaintenanceForm.get('warrentyPeriodId').updateValueAndValidity();
      //   }
      //   this.productMaintenanceForm.get('costSourceId').patchValue(response[9].resources.costSourceId);
      //   // this.productMaintenanceForm.get('costSourceId').patchValue((response[9].resources.contractCost != null) ?
      //   // (this.costCourseList.find(x => x.displayName == 'Supplier Price').id) :
      //   // (((response[9].resources.contractCost == null) && (parseInt(response[9].resources.costPrice == null ? '' :
      //   // response[9].resources.costPrice) == parseInt(response[9].resources.maxAverageCost == null ? '' :
      //   // response[9].resources.maxAverageCost))) ? (this.costCourseList.find(x => x.displayName == 'Validate').id) :
      //   // (((response[9].resources.contractCost == null) && (parseInt(response[9].resources.costPrice == null ? '' :
      //   // response[9].resources.costPrice) != parseInt(response[9].resources.maxAverageCost == null ? '' :
      //   // response[9].resources.maxAverageCost))) ? (this.costCourseList.find(x => x.displayName == 'Manual Override').id) : null)));

      // }
      this.rxjsService.setGlobalLoaderProperty(false);

    });
    this.productMaintenanceForm.get('conditionId')
      .valueChanges
      .pipe(pairwise())
      .subscribe(([prev, next]: [any, any]) => {

        this.previousConditionId = prev;
      });
    // this.productMaintenanceForm.get('conditionId').valueChanges.subscribe(val => {


    //   if (this.constNewStockCode.toString() != val) {
    //     this.productMaintenanceForm.controls['newStockCode'].setValue('');
    //   }
    //   if (val != null) {
    //     if (this.stockMaintenancesDetail.materialType != null && this.stockMaintenancesDetail.materialType == 'ERSA' && this.stockMaintenancesDetail.valuationClass != null && this.stockMaintenancesDetail.valuationClass == '3060' && this.conditionList.find(x => x.id == val).displayName == 'New' && this.stockMaintenancesDetail.condition != null) {
    //       this.productMaintenanceForm.get('warrentyPeriodId').setValidators([Validators.required]);
    //       this.productMaintenanceForm.get('warrentyPeriodId').updateValueAndValidity();

    //       this.isMandetory = true;
    //     }
    //     else {
    //       this.productMaintenanceForm.get('warrentyPeriodId').clearValidators();
    //       this.productMaintenanceForm.get('warrentyPeriodId').updateValueAndValidity();
    //       this.isMandetory = false;
    //     }
    //     if (this.conditionList.find(x => x.id == val).displayName === 'Refurbished') {
    //       this.productMaintenanceForm.controls['newStockCode'].enable();
    //       this.productMaintenanceForm = setRequiredValidator(this.productMaintenanceForm,
    //         ["newStockCode"]);
    //       this.productMaintenanceForm.get('newStockCode').markAllAsTouched();
    //     }
    //     else {
    //       this.productMaintenanceForm.controls['newStockCode'].disable();
    //       this.productMaintenanceForm.controls['newStockCode'].clearValidators();
    //       this.productMaintenanceForm.controls['newStockCode'].updateValueAndValidity();
    //     }
    //   }
    //   else {
    //     this.productMaintenanceForm.controls['newStockCode'].disable();
    //     this.productMaintenanceForm.controls['newStockCode'].clearValidators();
    //     this.productMaintenanceForm.controls['newStockCode'].updateValueAndValidity();
    //   }

    // });
    this.createProductWarehouseAddEditForm();

    // this.productWarehouseFrom.get('productWarehouseId').valueChanges.subscribe((response)=>{

    //   if(this.isPatchProductWarehouseId == false){
    //     // if(this.productWarehouseFormArray.value.length > 0){
    //     let isAddWarehouse = response.filter((x)=> !this.productWarehouseFormArray.value.some(({warehouseId:warId})=>x == warId));
    //     let isRemoveWarehouse = this.productWarehouseFormArray.value.filter(({warehouseId:warId})=> !response.some((x)=>warId == x))


    //     if(isAddWarehouse.length > 0){
    //       // this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_BALANCE_COUNT,undefined,false,prepareRequiredHttpParams({StockId:this.stockId,WarehouseId:isAddWarehouse[0]})).subscribe((response) => {
    //       //  if(response.resources == null){
    //          //add warehouse details
    //         if(this.productWarehouseFrom.value.productWarehouseList.find(x=>x.warehouseId == isAddWarehouse[0]) == null){

    //           if(this.productWarehouseDetail[0].resources.find(x=>x.warehouseId == isAddWarehouse[0]).selected){
    //              let productDetailId = this.productWarehouseDetail[1].resources.find(x=>x.warehouseId == isAddWarehouse[0]);

    //             this.productWarehouseFormArray.push(this.formBuilder.group({
    //               'warehouseItemId':productDetailId['warehouseItemId'],
    //               'warehouseId': productDetailId['warehouseId'],
    //               'warehouse': productDetailId['warehouse'],
    //               'stockId': productDetailId['stockId'],
    //               'procurable':productDetailId['procurable'],
    //               'sellable':productDetailId['sellable'],
    //               'obsolete':productDetailId['obsolete'],
    //               // 'sellableFrom': new FormControl(response.resources[i]['sellableFrom'],Validators.required),
    //               'sellableFrom': new FormControl({value : productDetailId['sellableFrom'],disabled : (productDetailId['sellable'] == true ? false : true)},productDetailId['sellable'] == true ? Validators.required : null),
    //               'lastMovementDate':productDetailId['lastMovementDate'],
    //               'ageingDays':productDetailId['ageingDays'],
    //               'overridenBy':productDetailId['overridenBy'],
    //               'overridenUserId':productDetailId['overridenUserId'],
    //               'warehouseCode': productDetailId['warehouseCode'],
    //               'stockCode': productDetailId['stockCode']
    //             }));
    //           }
    //           else {
    //              let productWarehouseData={
    //                warehouseId :isAddWarehouse[0],
    //                warehouse:this.locationList.find(x=>x.value == isAddWarehouse[0]).display,
    //                stockId :this.stockId,
    //                procurable :false,
    //                sellable :false,
    //                obsolete : false,
    //                sellableFrom :'',
    //                lastMovementDate:'',
    //                ageingDays :'',
    //                overridenBy: '',
    //                warehouseCode: this.locationList.find(x => x.value == isAddWarehouse[0]).code,
    //                stockCode: this.stockMaintenancesDetail.stockCode,
    //                modifiedUserId: this.userData.userId,
    //              }

    //              this.productWarehouseFormArray.push(this.formBuilder.group({
    //                'warehouseId': productWarehouseData['warehouseId'],
    //                'warehouse': productWarehouseData['warehouse'],
    //                'stockId': productWarehouseData['stockId'],
    //                'procurable':productWarehouseData['procurable'],
    //                'sellable': productWarehouseData['sellable'],
    //                'obsolete':productWarehouseData['obsolete'],
    //                'sellableFrom': new FormControl({value : productWarehouseData['sellableFrom'],disabled : (productWarehouseData['sellable'] == true ? false : true)},productWarehouseData['sellable'] == true ? Validators.required : null),
    //                'lastMovementDate':productWarehouseData['lastMovementDate'],
    //                'ageingDays':productWarehouseData['ageingDays'],
    //                'overridenBy': productWarehouseData['overridenBy'],
    //                'warehouseCode': productWarehouseData['warehouseCode'],
    //                'stockCode': productWarehouseData['stockCode'],
    //                'modifiedUserId': this.userData.userId
    //                }));
    //            }
    //         }
    //     }
    //     else if (isRemoveWarehouse.length >0){
    //         this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_BALANCE_COUNT,undefined,false,prepareRequiredHttpParams({StockId:this.stockId,WarehouseId:isRemoveWarehouse[0].warehouseId})).subscribe((response) => {

    //           this.rxjsService.setGlobalLoaderProperty(false);

    //           if(response.resources == null){
    //             // remove selected warehouse
    //             for(let i=0;i<this.productWarehouseFrom.value.productWarehouseList.length;i++){
    //             if(isRemoveWarehouse.find(x =>x.warehouseId == this.productWarehouseFrom.value.productWarehouseList[i].warehouseId) == null){
    //               this.productWarehouseFormArray.removeAt(i);
    //             }
    //           }
    //         }
    //         else if(response.resources.totalStockCount == 0){
    //           // remove selected warehouse
    //           for(let i=0;i<this.productWarehouseFrom.value.productWarehouseList.length;i++){
    //             if(isRemoveWarehouse.find(x =>x.warehouseId == this.productWarehouseFrom.value.productWarehouseList[i].warehouseId) != null){
    //               this.productWarehouseFormArray.removeAt(i);
    //             }
    //           }
    //         }
    //         else{
    //           // dont remove selected warehouse

    //           this.snackbarService.openSnackbar('Warehouse is assined to multiple stock item not able to remove ', ResponseMessageTypes.WARNING);

    //           let warehouseId = this.productWarehouseFrom.get('productWarehouseId').value;
    //           warehouseId.push(isRemoveWarehouse[0].warehouseId);
    //           this.productWarehouseFrom.get('productWarehouseId').patchValue(warehouseId,{eventEmit:false})
    //           //  this.locationWarehouseSelectedOption.push(isRemoveWarehouse[0].warehouseId);
    //         }
    //         })

    //     }
    //     }
    //     else{
    //       this.isPatchProductWarehouseId = false;
    //     }
    // })

    // this.getProductWarehouseDetails();
    this.createThresholdForm();
    this.productThresholdDetailForm.get('productThresholdWarehouseId').valueChanges.subscribe((response) => {

      // console.log('productThresholdDetail',this.productThresholdDetail)

      //  if(this.productThresholdDetail[1].resources?.length > 0) {
      //          let productThresholdDetailId = this.productThresholdDetail[1].resources.find(x=>x.WarehouseId == response.value);
      //           this.productThresholdFormArray.push(this.formBuilder.group({
      //             'warehouseId': productThresholdDetailId['WarehouseId'],
      //             'warehouseName': productThresholdDetailId['WarehouseName'],
      //             'stockId': this.stockId,
      //             'minimumThreshold': new FormControl(productThresholdDetailId['MinimumThreshold'] == null ? 0 :productThresholdDetailId['MinimumThreshold'],Validators.required),
      //             'maximumThreshold': new FormControl(productThresholdDetailId['MaximumThreshold'] == null ? 0 :productThresholdDetailId['MaximumThreshold'],Validators.required),
      //             'thresholdLastUpdated': productThresholdDetailId['ThresholdLastUpdated'],
      //             'modifiedUserId': this.userData.userId,
      //             'isNew':true,
      //             'isChange':true
      //             }));

      //             this.checkList();
      //  }



      // if(this.isPatchProductThresholdWarehouseId == false){
      //   let isAddWarehouse = response?.filter((x)=> !this.productThresholdFormArray.value.some(({warehouseId:warId})=>x == warId));
      // let isRemoveWarehouse = this.productThresholdFormArray.value.filter(({warehouseId:warId})=> !response.some((x)=>warId == x))

      //   if(isAddWarehouse.length > 0){

      //        if (this.productThresholdFormArray.value.find(x => x.warehouseId == isAddWarehouse[0]) == null) {

      //         if(this.productThresholdDetail[0].resources.find(x=>x.warehouseId == isAddWarehouse[0]).selected){
      //           let productThresholdDetailId = this.productThresholdDetail[1].resources.find(x=>x.WarehouseId == isAddWarehouse[0]);

      //           this.productThresholdFormArray.push(this.formBuilder.group({
      //             'warehouseId': productThresholdDetailId['WarehouseId'],
      //             'warehouseName': productThresholdDetailId['WarehouseName'],
      //             'stockId': this.stockId,
      //             'minimumThreshold': new FormControl(productThresholdDetailId['MinimumThreshold'] == null ? 0 :productThresholdDetailId['MinimumThreshold'],Validators.required),
      //             'maximumThreshold': new FormControl(productThresholdDetailId['MaximumThreshold'] == null ? 0 :productThresholdDetailId['MaximumThreshold'],Validators.required),
      //             'thresholdLastUpdated': productThresholdDetailId['ThresholdLastUpdated'],
      //             'modifiedUserId': this.userData.userId,
      //             'isNew':true,
      //             'isChange':true
      //             }));

      //             this.checkList();
      //         }
      //         else{

      //           let productWarehouseData = {
      //             'warehouseId': isAddWarehouse[0],
      //             'warehouseName': this.locationList.find(x => x.value == isAddWarehouse[0]).display,
      //             'stockId': this.stockId,
      //             'minimumThreshold': 0,
      //             'maximumThreshold': 0,
      //             'thresholdLastUpdated': '',
      //             'modifiedUserId': this.userData.userId,
      //             'isNew':true,
      //             'isChange':true
      //             }
      //             this.productThresholdFormArray.push(this.formBuilder.group(productWarehouseData));
      //             this.checkList();
      //         }

      //       }


      // }
      // else if (isRemoveWarehouse.length >0){
      //     this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_BALANCE_COUNT,undefined,false,prepareRequiredHttpParams({StockId:this.stockId,WarehouseId:isRemoveWarehouse[0].warehouseId})).subscribe((response) => {

      //       this.rxjsService.setGlobalLoaderProperty(false);

      //       if(response.resources == null){
      //       // remove selected warehouse
      //       for (let i = 0; i < this.productThresholdDetailForm.value.productThresholdDetails.length; i++) {
      //         if (isRemoveWarehouse.find(x => x.warehouseId == this.productThresholdDetailForm.value.productThresholdDetails[i].warehouseId) == null) {
      //         this.productThresholdFormArray.removeAt(i);
      //         this.checkList();
      //         }
      //       }
      //     }
      //     else if(response.resources.totalStockCount == 0){
      //       // remove selected warehouse
      //       for (let i = 0; i < this.productThresholdDetailForm.value.productThresholdDetails.length; i++) {
      //         if (isRemoveWarehouse.find(x => x.warehouseId == this.productThresholdDetailForm.value.productThresholdDetails[i].warehouseId) != null) {
      //         this.productThresholdFormArray.removeAt(i);
      //         this.checkList();

      //         }
      //       }
      //     }
      //     else{
      //        // dont remove selected warehouse

      //        this.snackbarService.openSnackbar('Warehouse is assined to multiple stock item not able to remove ', ResponseMessageTypes.WARNING);

      //        let warehouseId = this.productThresholdDetailForm.get('productThresholdWarehouseId').value;
      //        warehouseId.push(isRemoveWarehouse[0].warehouseId);
      //        console.log('warehouseId---',warehouseId)
      //        this.productThresholdDetailForm.get('productThresholdWarehouseId').patchValue(warehouseId,{eventEmit:false})
      //       //  this.locationWarehouseSelectedOption.push(isRemoveWarehouse[0].warehouseId);
      //       this.checkList();
      //     }
      //   })
      // }
      // }
      // else{
      //   this.isPatchProductThresholdWarehouseId = false;
      // }

    })
    this.createProductSkillMappingForm();

    this.productSkillMappingForm.controls['SystemTypeSubCategory'].valueChanges.subscribe((val) => {
      this.productSkillMappingForm.controls['TechnicalSkillLevel1'].patchValue(false);
      this.productSkillMappingForm.controls['TechnicalSkillLevel2'].patchValue(false);
      this.productSkillMappingForm.controls['TechnicalSkillLevel3'].patchValue(false);
      this.productSkillMappingForm.controls['TechnicalSkillLevel4'].patchValue(false);
    })

    this.productMaintenanceForm.controls['componentGroupId'].valueChanges.subscribe((val) => {
      if ((val != null && val != undefined && val != '') && (this.productMaintenanceForm.controls['systemTypeId'].value != null &&
        this.productMaintenanceForm.controls['systemTypeId'].value != '')) {
        let systemTypeName = this.systemTypeList.filter(x => x.id == this.productMaintenanceForm.controls['systemTypeId'].value);
        let componentGroupName = this.componentGroupList.filter(x => x.id == val);
        if (systemTypeName.length > 0 && systemTypeName[0]?.displayName == 'Miscellaneous' &&
          componentGroupName.length > 0 && componentGroupName[0]?.displayName == 'Labour and Discounts') {
          this.isSkillMapping = true;
        }
        else {
          this.isSkillMapping = false;
        }
      }
    })

    this.productMaintenanceForm.controls['systemTypeId'].valueChanges.subscribe((val) => {
      if ((val != null && val != undefined && val != '') && (this.productMaintenanceForm.controls['componentGroupId'].value != '' &&
        this.productMaintenanceForm.controls['componentGroupId'].value != null)) {
        let systemTypeName = this.systemTypeList.filter(x => x.id == val);
        let componentGroupName = this.componentGroupList.filter(x => x.id == this.productMaintenanceForm.controls['componentGroupId'].value);
        if (systemTypeName.length > 0 && systemTypeName[0]?.displayName == 'Miscellaneous' &&
          componentGroupName.length > 0 && componentGroupName[0]?.displayName == 'Labour and Discounts') {
          this.isSkillMapping = true;
        }
        else {
          this.isSkillMapping = false;
        }
      }
    })

    this.getStockMaintenanceDetails().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.stockMaintainanceHeaderImage = response.resources?.imagePath;
      }
    });

    this.productMaintenanceForm.get('costPrice').valueChanges.subscribe(element => {
      if (element != '') {
        if (this.stockMaintenancesDetail.maxAverageCost == element) {
          let name = this.costCourseList.find(x => x.displayName == 'Validate');
          if (name != undefined) {
            this.productMaintenanceForm.get('costSourceId').patchValue(name.id);
          }
        }
        if (this.stockMaintenancesDetail.maxAverageCost != element) {
          let manual = this.costCourseList.find(x => x.displayName == 'Manual Override');
          if (manual != undefined) {
            this.productMaintenanceForm.get('costSourceId').patchValue(manual.id);
          }
        }
      }
    });

  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][INVENTORY_COMPONENT.STOCK_MAINTENANCE]
      if (permission) {
        let edit = permission.find(item => item.menuName == "Edit");
        for (const iterator of edit['subMenu']) {
          let perm = { edit: false, view: false };
          let permissionss = iterator['subMenu'].length == 0 ? { edit: false, view: false } : iterator['subMenu'].forEach(item => {
            if (item.menuName == "Edit") {
              perm.edit = true
            }
            if (item.menuName == "View") {
              perm.view = true
            }
          })
          this.editPermissions[iterator.menuName] = perm;
        }
      }
    });
  }
  getDropdownDetailStockMaintenance(): Observable<any> {
    return forkJoin(
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_COST_SOURCE),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WARRANTY_PERIODS),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, SalesModuleApiSuffixModels.UX_ITEM_OWNER_SHIP_TYPE),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SYSTEM_TYPE),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_COMPONENT_GROUP),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_COMPONENT),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEMBRANDS),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_CONDITION),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_NEW_ITEMS_CODES, prepareRequiredHttpParams({
        id: this.stockId
      })),
      // this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE, this.stockId, false),
    )
  }

  getProductWarehouseDetail(): Observable<any> {
    return forkJoin(

      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE_UX_PRODUCT_WAREHOUSE, undefined, false, prepareGetRequestHttpParams(null, null, {
        UserId: this.userData.userId,
        itemid: this.stockId
      })),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE_PRODUCT_WAREHOUSE, undefined, false, prepareRequiredHttpParams({
        UserId: this.userData.userId,
        ItemId: this.stockId
      }))
    )
  }

  getProductThresholdDetail(): Observable<any> {
    return forkJoin(
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE_UX_PRODUCT_WAREHOUSE, undefined, false, prepareGetRequestHttpParams(null, null, {
        UserId: this.userData.userId,
        itemid: this.stockId
      })),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE_PRODUCT_THRESHOLD, undefined, false, prepareRequiredHttpParams({
        UserId: this.userData.userId,
        ItemId: this.stockId
      }))
    )
  }

  getSubCategory() {
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_SUB_CATEGORIES).subscribe((response) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode === 200) {
        this.getSubCategoryDropdown = response.resources;
      }
    });
  }

  showRepairCodeChecked: boolean = false;

  getProductMaintenanceDetails() {

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_MAINTENANCE, this.stockId, false).subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          if (response.isSuccess) {
            this.stockMaintenancesDetail = response.resources;
            // this.isCondition = response.resources;
            if (response.resources.condition == 'Refurbished')
              this.productMaintenanceForm.controls['newStockCode'].enable();

            if (response.resources.condition == 'Refurbished' && this.roleName != 'Inventory Manager') {
              this.isCondition = '';
              this.isEanble = true;
            } else {
              this.isCondition = null;
              this.isEanble = false;
            }
            //(this.condition =='Refurbished' && roleName != 'Inventory Manager') || this.condition !='Refurbished'? '' : null
            let productmaintenanceDetail = {
              stockId: response.resources?.stockId,
              costPrice: response.resources?.costPrice,
              costSourceId: response.resources?.costSourceId,
              warrentyPeriodId: response.resources?.warrentyPeriodId,
              ownershipId: response.resources?.ownershipId,
              systemTypeId: response.resources?.systemTypeId,
              componentGroupId: response.resources?.componentGroupId,
              componentId: response.resources?.componentId,
              brandId: response.resources?.brandId,
              labourComponent: response.resources?.labourComponent,
              conditionId: response.resources?.conditionId,
              isobsolete: response.resources?.isobsolete,
              newStockCode: response.resources?.newStockCodeid == null ? '' : response.resources?.newStockCodeid,
              fullStockDescription: response.resources?.fullStockDescription,
              isNotSerialized: response.resources?.isNotSerialized,
              createdUserId: this.userData.userId,
              stockCodeBarcode: response.resources?.stockCodeBarcode,
            }

            this.constNewStockCode = response.resources?.conditionId == null ? '' : response.resources?.conditionId;
            this.isSaveMaintenance = response.resources?.conditionId == null ? true : (this.conditionList.find(x => x.id == response.resources?.conditionId).displayName === 'Refurbished' ? (response.resources?.newStockCodeid == null ? true : false) : false)
            this.isShowRepairCode = response.resources?.isRepairCode;
            this.condition = response.resources?.condition;

            this.productMaintenanceForm.patchValue(productmaintenanceDetail);

            if (this.stockCodesList.length > 0) {
              let find = this.stockCodesList.find((x) => (x.value == response.resources?.newStockCodeid));
              if (find?.display)
                this.productMaintenanceForm.get('newStockCode').setValue({ displayName: find?.display, id: response.resources?.newStockCodeid })
            }

            this.barcodes = new Array(this.productMaintenanceForm?.get('stockCodeBarcode')?.value);
            if (this.stockMaintenancesDetail?.materialType != null && this.stockMaintenancesDetail?.materialType == 'ERSA' && this.stockMaintenancesDetail.valuationClass != null && this.stockMaintenancesDetail.valuationClass == '3060' && this.stockMaintenancesDetail.condition == 'New' && this.stockMaintenancesDetail.condition != null) {
              this.productMaintenanceForm.get('warrentyPeriodId').setValidators([Validators.required]);
              this.productMaintenanceForm.controls['newStockCode'].disable();
              this.isMandetory = true;
              this.showRepairCodeChecked = false;
            }
            else {
              this.isMandetory = false;
              this.showRepairCodeChecked = false;
              this.productMaintenanceForm.get('warrentyPeriodId').clearValidators();
              this.productMaintenanceForm.get('warrentyPeriodId').updateValueAndValidity();
              //   this.productMaintenanceForm.controls['newStockCode'].disable();
            }
            if (this.stockMaintenancesDetail.condition == 'RepairCode') {
              this.isShowRepairCode = true;
              this.isSkillMapping = true;
              let systemType = this.systemTypeList.find(sys => sys.displayName == 'Faulty/RepairStock');
              let componentGroup = this.componentGroupList.find(group => group.displayName == 'Faulty/RepairStock');
              let component = this.componentList.find(com => com.displayName == 'Faulty/RepairStock');
              let brand = this.brandList.find(sys => sys.displayName == 'Faulty/RepairStock');

              this.productMaintenanceForm.get('systemTypeId').patchValue(systemType ? systemType.id : this.productMaintenanceForm.get('systemTypeId').value);
              this.productMaintenanceForm.get('componentGroupId').patchValue(componentGroup ? componentGroup.id : this.productMaintenanceForm.get('systemTypeId').value);
              this.productMaintenanceForm.get('componentId').patchValue(component ? component.id : this.productMaintenanceForm.get('componentId').value);
              this.productMaintenanceForm.get('brandId').patchValue(brand ? brand.id : this.productMaintenanceForm.get('brandId').value);
              this.productMaintenanceForm.controls['newStockCode'].disable();
              this.showRepairCodeChecked = true;
            }
            this.productMaintenanceForm.get('costSourceId').patchValue(response.resources.costSourceId);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        }
        setTimeout(() => {
          this.rxjsService.setGlobalLoaderProperty(false);
        }, 1500)
      });
  }
  onChangeSellable(value) {
    value = value.checked;

    if (this.stockProductWarehouseFormArray.length > 0) {
      this.stockProductWarehouseFormArray.value.forEach((element, index) => {
        this.stockProductWarehouseFormArray.at(index).get("isSellable").setValue(value);
      });
    }

  }
  onChangeProcurable(value) {
    value = value.checked;

    if (this.stockProductWarehouseFormArray.length > 0) {
      this.stockProductWarehouseFormArray.value.forEach((element, index) => {
        this.stockProductWarehouseFormArray.at(index).get("isAllProcurable").setValue(value);

        // console.log('this.checm.--',this.stockProductWarehouseFormArray.at(index).get("productDetailsArray"))
        if (this.stockProductWarehouseFormArray.at(index).get("productDetailsArray").value.length > 0) {
          this.stockProductWarehouseFormArray.at(index).get("productDetailsArray").value.forEach((ele, i) => {
            let tt; tt = this.stockProductWarehouseFormArray.at(index).get("productDetailsArray");
            tt.controls[i]?.get('procurable').setValue(value);

          });
        }
      });
    }

  }
  checkedProcurableEvent(event: MatCheckboxChange) {
    if (event.checked) {
      for (let i = 0; i < this.productWarehouseFormArray.value.length; i++) {
        this.productWarehouseFormArray.controls[i].get('procurable').patchValue(true);
      }
    }
    else {
      for (let i = 0; i < this.productWarehouseFormArray.value.length; i++) {
        this.productWarehouseFormArray.controls[i].get('procurable').patchValue(false);
      }
    }
  }

  isShowConditionModal: boolean = false;

  onChangeCondition(value, value2) {
    this.isShowConditionModal = true;
    this.showConditionPopup = true;
  }

  cancelPopup() {
    if (this.previousConditionId)
      this.productMaintenanceForm.get('conditionId').setValue(this.previousConditionId);
    this.showConditionPopup = false;
  }

  approvePopup() {

    let val = this.productMaintenanceForm.get('conditionId').value;
    if (this.constNewStockCode.toString() != val) {
      this.productMaintenanceForm.controls['newStockCode'].setValue('');
    }
    this.condition = this.conditionList.find(x => x.id == val).displayName;
    this.isShowRepairCode = this.conditionList.find(x => x.id == val).displayName === 'RepairCode' ? true : false;
    if (val != null) {
      if (this.stockMaintenancesDetail?.materialType != null && this.stockMaintenancesDetail?.materialType == 'ERSA' && this.stockMaintenancesDetail.valuationClass != null && this.stockMaintenancesDetail.valuationClass == '3060' &&
        this.conditionList.find(x => x.id == val).displayName == 'New' && this.stockMaintenancesDetail.condition != null) {
        this.productMaintenanceForm.get('warrentyPeriodId').setValidators([Validators.required]);
        this.productMaintenanceForm.get('warrentyPeriodId').updateValueAndValidity();
        this.isMandetory = true;
        this.showRepairCodeChecked = false;
        //  this.productMaintenanceForm.controls['newStockCode'].disable();
      }
      else {
        this.productMaintenanceForm.get('warrentyPeriodId').clearValidators();
        this.productMaintenanceForm.get('warrentyPeriodId').updateValueAndValidity();
        this.isMandetory = false;
        this.showRepairCodeChecked = false;
        //  this.productMaintenanceForm.controls['newStockCode'].disable();
      }
      if (this.conditionList.find(x => x.id == val).displayName === 'Refurbished') {
        this.productMaintenanceForm.controls['newStockCode'].enable();
        this.productMaintenanceForm = setRequiredValidator(this.productMaintenanceForm,
          ["newStockCode"]);
        this.productMaintenanceForm.get('newStockCode').markAllAsTouched();
        this.showRepairCodeChecked = false;
      } else {
        this.productMaintenanceForm.controls['newStockCode'].disable();
        this.productMaintenanceForm.controls['newStockCode'].setValue('');
      }

      if (this.conditionList.find(x => x.id == val).displayName === 'RepairCode') {
        let systemType = this.systemTypeList.find(sys => sys.displayName == 'Faulty/RepairStock');
        let componentGroup = this.componentGroupList.find(group => group.displayName == 'Faulty/RepairStock');
        let component = this.componentList.find(com => com.displayName == 'Faulty/RepairStock');
        let brand = this.brandList.find(sys => sys.displayName == 'Faulty/RepairStock');

        this.productMaintenanceForm.get('systemTypeId').patchValue(systemType ? systemType.id : this.productMaintenanceForm.get('systemTypeId').value);
        this.productMaintenanceForm.get('componentGroupId').patchValue(componentGroup ? componentGroup.id : this.productMaintenanceForm.get('systemTypeId').value);
        this.productMaintenanceForm.get('componentId').patchValue(component ? component.id : this.productMaintenanceForm.get('componentId').value);
        this.productMaintenanceForm.get('brandId').patchValue(brand ? brand.id : this.productMaintenanceForm.get('brandId').value);
        this.productMaintenanceForm.controls['newStockCode'].disable();
        this.showRepairCodeChecked = true;
      }

    }
    else {
      //  this.productMaintenanceForm.controls['newStockCode'].disable();
      this.productMaintenanceForm.controls['newStockCode'].clearValidators();
      this.productMaintenanceForm.controls['newStockCode'].updateValueAndValidity();
    }
    this.showConditionPopup = false;
  }

  checkedSellableEvent(event: MatCheckboxChange) {
    if (event.checked) {
      for (let i = 0; i < this.productWarehouseFormArray.value.length; i++) {
        this.productWarehouseFormArray.controls[i].get('sellable').patchValue(true);
        this.productWarehouseFormArray.controls[i].get('sellableFrom').enable();
        this.productWarehouseFormArray.controls[i].get('sellableFrom').setValidators([Validators.required])
        this.productWarehouseFormArray.controls[i].get('sellableFrom').updateValueAndValidity()
      }
    }
    else {
      for (let i = 0; i < this.productWarehouseFormArray.value.length; i++) {
        this.productWarehouseFormArray.controls[i].get('sellable').patchValue(false);
        this.productWarehouseFormArray.controls[i].get('sellableFrom').disable()
        this.productWarehouseFormArray.controls[i].get('sellableFrom').setValue(null);
        this.productWarehouseFormArray.controls[i].get('sellableFrom').clearValidators();
        this.productWarehouseFormArray.controls[i].get('sellableFrom').updateValueAndValidity()
      }
    }
  }

  changeStockCodes() {
    this.isShowConditionModal = false;
    this.showConditionPopup = true;
  }

  approveStockCodePopup() {
    this.showConditionPopup = false;
  }

  cancelStockCodePopup() {

    if (this.productMaintenanceForm.get('isNotSerialized').value) {
      this.productMaintenanceForm.get('isNotSerialized').patchValue(false);
    }
    else {
      this.productMaintenanceForm.get('isNotSerialized').patchValue(true);
    }
    this.showConditionPopup = false;
  }

  onTabClicked(tabIndex) {

    this.breadCrumbTitle = tabIndex?.tab?.textLabel;
    // if(tabIndex.index === 0){
    // }
    if (tabIndex.index === 1) {
      this.getWarehouseProductConsolidateDetails('get');
      this.createProductWarehouseAddEditForm();

      // this.stockProductWarehouseFormArray.clear();

      // this.getProductWarehouseDetail().subscribe((response: IApplicationResponse[]) => {

      //   if (response[0].isSuccess) {
      //     this.locationList = [];
      //     for (let i = 0; i < response[0].resources.length; i++) {
      //       // this.locationList= response[0].resources;
      //       let temp = {}
      //       temp['display'] = response[0].resources[i].warehouseName;
      //       temp['value'] = response[0].resources[i].warehouseId;
      //       temp['code'] = response[0].resources[i].warehouseCode;
      //       this.locationList.push(temp);
      //     }
      //     // this.thresholdLocationList= response.resources;
      //     this.locationWarehouseSelectedOption=[];
      //     // this.selectedThresholdLocations = [];
      //     // setTimeout(() => {
      //     this.isPatchProductWarehouseId = true;
      //     // this.productWarehouseFrom.get('productWarehouseId').patchValue(warehouseIds);
      //     let warhouseId= response[0].resources.map( (item) => item.selected == true ? item.warehouseId:null);
      //     // ["04b8b10f-f1e4-4fc9-95f4-0bf200292698", "19f0e076-287c-4d43-827f-5c6f3b1c45c6"]

      //     warhouseId.forEach((element,index)=>{
      //       if(element== null) warhouseId.splice(index,1);
      //     });

      //     this.productWarehouseFrom.get('productWarehouseId').patchValue(warhouseId);
      //     // });
      //     // this.productWarehouseFrom.get('productWarehouseId').updateValueAndValidity({emitEvent: false, onlySelf: true})

      //   }
      //   if (response[1].isSuccess) {
      //     this.productWarehouseFormArray.clear();
      //     // for(const item of response.resources){
      //     for (let i = 0; i < response[1].resources.length; i++) {
      //       this.productWarehouseFormArray.push(this.formBuilder.group({
      //         'warehouseItemId': response[1].resources[i]['warehouseItemId'],
      //         'warehouseId': response[1].resources[i]['warehouseId'],
      //         'warehouse': response[1].resources[i]['warehouse'],
      //         'stockId': response[1].resources[i]['stockId'],
      //         'procurable': response[1].resources[i]['procurable'],
      //         'sellable': response[1].resources[i]['sellable'],
      //         'obsolete': response[1].resources[i]['obsolete'],
      //         // 'sellableFrom': new FormControl(response.resources[i]['sellableFrom'],Validators.required),
      //         'sellableFrom': new FormControl({ value: response[1].resources[i]['sellableFrom'], disabled: (response[1].resources[i]['sellable'] == true ? false : true) }, response[1].resources[i]['sellable'] == true ? Validators.required : null),
      //         'lastMovementDate': response[1].resources[i]['lastMovementDate'],
      //         'ageingDays': response[1].resources[i]['ageingDays'],
      //         'overridenBy': response[1].resources[i]['overridenBy'],
      //         'overridenUserId': response[1].resources[i]['overridenUserId'],
      //         'warehouseCode': response[1].resources[i]['warehouseCode'],
      //         'stockCode': response[1].resources[i]['stockCode']
      //       }));
      //     }
      //   }
      //   this.productWarehouseDetail = response;

      //   this.rxjsService.setGlobalLoaderProperty(false);
      // });
    }
    else if (tabIndex.index === 2) {
      this.getProductThresholdDetail().subscribe((response: IApplicationResponse[]) => {
        if (response[1].isSuccess) {
          this.productThresholdDetail = response;

          // const productThreshold = response[1].resources;
          this.productThresholdFormArray.clear();

          for (let i = 0; i < response[1].resources.length; i++) {
            let productThresholdFormObj = this.formBuilder.group({
              'warehouseItemId': response[1].resources[i]['warehouseItemId'],
              'warehouseId': response[1].resources[i]['WarehouseId'],
              'warehouseName': response[1].resources[i]['WarehouseName'],
              'stockId': this.stockId,
              'minimumThreshold': new FormControl((this.isEanble || response[1].resources[i]['MinimumThreshold'] == null) ? 0 : response[1].resources[i]['MinimumThreshold'], Validators.required),
              'maximumThreshold': new FormControl((this.isEanble || response[1].resources[i]['MaximumThreshold'] == null) ? 0 : response[1].resources[i]['MaximumThreshold'], Validators.required),
              'thresholdLastUpdated': response[1].resources[i]['ThresholdLastUpdated'],
              'modifiedUserId': this.userData.userId,
              'canUpdate': response[1].resources[i]['canUpdate'],
              'isChange': false
            });
            this.productThresholdFormArray.push(productThresholdFormObj);
          }
        }
        if (response[0].isSuccess) {

          this.locationList = [];
          // this.locationList= response[0].resources;

          for (let i = 0; i < response[0].resources.length; i++) {
            let temp = {}
            temp['display'] = response[0].resources[i].warehouseName;
            temp['value'] = response[0].resources[i].warehouseId;
            this.locationList.push(temp);
          }

          this.selectedThresholdLocations = [];
          let warehouseIds = []
          for (const warehouseId of response[0].resources) {
            if (warehouseId.selected) {

              warehouseIds.push(warehouseId.warehouseId);
              // this.selectedThresholdLocations.push(warehouseId.warehouseId);
            }
          }

          // setTimeout(() => {
          this.isPatchProductThresholdWarehouseId = true;
          // this.productWarehouseFrom.get('productWarehouseId').patchValue(warehouseIds);

          let warhouseId = response[0].resources.map((item) => item.selected == true ? item : null);

          // ["04b8b10f-f1e4-4fc9-95f4-0bf200292698", "19f0e076-287c-4d43-827f-5c6f3b1c45c6"]

          let arr = [];
          response[0].resources?.forEach((element, index) => {
            if (element.selected && element.warehouseId) {
              arr.push({ value: element.warehouseId, display: element.warehouseName });
            }
          });
          this.productThresholdDetailForm.get('productThresholdWarehouseId').patchValue(arr);


          // },1000 );
          // this.productThresholdDetailForm.get('productThresholdWarehouseId').patchValue(warehouseIds);

        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });


    }
    else if (tabIndex.index === 3) {
      this.getSubCategory();
      this.getSkillMappingDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.skillMappingGetDetails = response.resources;
          let mappingres = response.resources;
          let purchaseOrdeModels = new ProductSkillMappingAddEditModel(response.resources);
          this.productSkillMappingForm.patchValue(purchaseOrdeModels);
          if (mappingres.systemTypeName == 'Discounts') {
            this.isDisctountSkillMapping = false;
            this.productSkillMappingForm.get('SystemTypeSubCategory').disable();
            this.productSkillMappingForm = clearFormControlValidators(this.productSkillMappingForm, ["SystemTypeSubCategory"]);

            this.showTechnicalSkillLevel = true;
            this.productSkillMappingForm.controls['jobTypeName'].patchValue('-');
            this.productSkillMappingForm.controls['SystemTypeSubCategory'].patchValue('');
            this.productSkillMappingForm.controls['TechnicalSkillLevel1'].patchValue(false);
            this.productSkillMappingForm.controls['TechnicalSkillLevel2'].patchValue(false);
            this.productSkillMappingForm.controls['TechnicalSkillLevel3'].patchValue(false);
            this.productSkillMappingForm.controls['TechnicalSkillLevel4'].patchValue(false);
          }
          else if (mappingres.systemTypeName == 'Labour') {
            this.productSkillMappingForm.get('SystemTypeSubCategory').enable();
            this.isDisctountSkillMapping = true;
            this.productSkillMappingForm = setRequiredValidator(this.productSkillMappingForm, ["SystemTypeSubCategory"]);
            let category = this.getSubCategoryDropdown.find(x => x.displayName == 'Advanced');
            if (category != undefined && category.displayName == 'Advanced') {
              this.productSkillMappingForm.controls['SystemTypeSubCategory'].patchValue(category.id);
              this.productSkillMappingForm.controls['jobTypeName'].patchValue('-');
              this.productSkillMappingForm.controls['TechnicalSkillLevel1'].patchValue(true);
              this.productSkillMappingForm.controls['TechnicalSkillLevel2'].patchValue(true);
              this.productSkillMappingForm.controls['TechnicalSkillLevel3'].patchValue(true);
              this.productSkillMappingForm.controls['TechnicalSkillLevel4'].patchValue(true);
            }
          }
          else if (mappingres.systemTypeName != 'Discounts' || mappingres.systemTypeName != 'Labour') {
            this.productSkillMappingForm.get('SystemTypeSubCategory').enable();
            this.isDisctountSkillMapping = true;
            this.productSkillMappingForm = setRequiredValidator(this.productSkillMappingForm, ["SystemTypeSubCategory"]);
            let jobType = mappingres.systemTypeName && !mappingres.systemTypeName ? mappingres.systemTypeName :
              mappingres.systemTypeSubCategoryName && !mappingres.systemTypeName ? mappingres.systemTypeSubCategoryName :
                (mappingres.systemTypeName && mappingres.systemTypeSubCategoryName) ?
                  mappingres.systemTypeName + '-' + mappingres.systemTypeSubCategoryName : '-';
            this.productSkillMappingForm.controls['jobTypeName'].patchValue(jobType);
            this.productSkillMappingForm.controls['SystemTypeSubCategory'].patchValue(mappingres.SystemTypeSubCategory);
            this.productSkillMappingForm.controls['TechnicalSkillLevel1'].patchValue(mappingres.TechnicalSkillLevel1);
            this.productSkillMappingForm.controls['TechnicalSkillLevel2'].patchValue(mappingres.TechnicalSkillLevel2);
            this.productSkillMappingForm.controls['TechnicalSkillLevel3'].patchValue(mappingres.TechnicalSkillLevel3);
            this.productSkillMappingForm.controls['TechnicalSkillLevel4'].patchValue(mappingres.TechnicalSkillLevel4);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    }
  }
  checkList() {
    let flag = true;
    for (let i = 0; i < this.productThresholdFormArray?.length; i++) {
      if (this.productThresholdFormArray.controls[i].get('maximumThreshold').enabled) {
        flag = false;
        break;
      }
    }
    this.isThresholdFlag = flag;

  }
  //Disable form control inside form array

  disableFormControl(i) {
    return this.productThresholdFormArray?.controls[i]?.get('canUpdate')?.value == false
  }

  //product-maintenance tab method
  createProductMaintenanceForm(): void {
    let productMaintenanceAddEditModel = new StockMaintenanceProductMaintenanceModel();
    // create form controls dynamically from model class
    this.productMaintenanceForm = this.formBuilder.group({});
    Object.keys(productMaintenanceAddEditModel).forEach((key) => {
      this.productMaintenanceForm.addControl(key, new FormControl(productMaintenanceAddEditModel[key]));
    });
    this.productMaintenanceForm = setRequiredValidator(this.productMaintenanceForm,
      ["costPrice", 'costSourceId', 'ownershipId', 'systemTypeId', 'componentGroupId', 'componentId', 'brandId', 'labourComponent', 'conditionId', 'fullStockDescription']);
    // 'warrentyPeriodId'
    if (this.stockId) {
      this.productMaintenanceForm.get('supportingDocuments').disable();
    }
  }

  get productWarehouseFormArray(): FormArray {
    if (this.productWarehouseFrom !== undefined) {
      return (<FormArray>this.productWarehouseFrom.get('productWarehouseList'));
    }
  }

  getStockMaintenanceDetails(): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE, this.stockId, false);
  }

  saveProductMaintenance() {
    if (!this.editPermissions['Product Maintenance']?.edit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.productMaintenanceForm.invalid) {
      return;
    }
    let productMaintenanceData = this.productMaintenanceForm.value;
    // productMaintenanceData.newStockCode = this.stockCodesList.find(x=>x.id == this.productMaintenanceForm.value.newStockCode).displayName;
    if (this.productMaintenanceForm.value.newStockCode)
      productMaintenanceData.newStockCode = this.productMaintenanceForm.value.newStockCode.id;

    this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE,
      this.productMaintenanceForm.value).subscribe({
        next: response => {
          if (response.isSuccess && response.statusCode === 200) {
            this.isSaveMaintenance = false;
            this.getProductMaintenanceDetails();
            this.rxjsService.setGlobalLoaderProperty(false);

            // this.router.navigate(['/inventory/stock-maintenance']);
          }
        },
        error: err => this.errorMessage = err
      })
  }
  //end of product-maintenance tab method


  //start of product-warehouse tab method

  createProductWarehouseAddEditForm() {
    let stockOrderModel = new StockMaintenanceProductAddEditModal();
    this.productWarehouseFrom = this.formBuilder.group({
      stockProductWarehouseFormArray: this.formBuilder.array([]),
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.productWarehouseFrom.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.valueChangeProductWarehouse();
    // this.productWarehouseFrom = setRequiredValidator(this.productWarehouseFrom, ['districtId']);
  }
  valueChangeProductWarehouse() {
    this.productWarehouseFrom.get('districtId').valueChanges.subscribe((val) => {
    })
  }
  enableChange() {
    this.isChange = false;
  }
  procurableChange(obj) {
    if (!obj?.get('procurable').value)
      obj?.get('itemSellableTypeId').setValue(null);
    else {
      if (obj?.get('itemSellableTypeId').value)
        obj?.get('itemSellableTypeId').setValue(obj?.get('itemSellableTypeId').value);
      else if (this.itemSellableTypes && this.itemSellableTypes.length > 0)
        obj?.get('itemSellableTypeId').setValue(this.itemSellableTypes[0].id);
    }
  }
  procurableTypeChange(index1, index2, value) {

    this.getProductDetailsArray(index1)?.controls[index2]?.get('procurableTo').setValidators(Validators.required);

  }
  getWarehouseProductConsolidateDetails(type: string) {

    let params = Object.assign({},
      this.productWarehouseFrom.get('districtId').value == '' ? null : { DistrictIds: this.productWarehouseFrom.get('districtId').value },
      { ItemId: this.stockId }, { userId: this.userData.userId }
    );

    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_MAINTENANCE_PRODUCT_WAREHOUSE_CONSOLIDATE,
      undefined, false, prepareGetRequestHttpParams(null, null, params)
    ).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200 && response.resources.length > 0) {
        if (this.stockProductWarehouseFormArray != undefined && this.stockProductWarehouseFormArray.value.length > 0) {
          this.stockProductWarehouseFormArray.clear();
        }
        this.stockProductWarehouseFormArray = this.getstockProductWarehouseFormArray;
        this.buttonDisabled = false;
        this.rxjsService.setGlobalLoaderProperty(false);

        let checkProcurable: any = [];
        let productDetailsArr: number;
        this.districtsDetail = [];

        response.resources.forEach((element: any, index: number) => {
          this.districtsDetail.push({ value: element.districtId, display: element.districtName });
          element.sellableFrom = element.sellableFrom != null ? new Date(element.sellableFrom) : new Date();
          element.sellableFrom = element.sellableFrom;

          checkProcurable = element.productDetails.filter(x => x.procurable);
          productDetailsArr = element.productDetails.length;


          checkProcurable.length == productDetailsArr ? element.isAllProcurable = true : false;
          this.stockProductWarehouseFormArray.push(this.createStockProductWarehouseGroupForm(element));

          element.productDetails.forEach((discrepancyDocumentsList: productDetailsArrayModal, i: number) => {

            let filter = this.itemSellableTypes.filter(x => x.id == discrepancyDocumentsList.itemSellableTypeId);

            if (filter && filter.length > 0 && filter[0].displayName == 'Temporary') {
              discrepancyDocumentsList.procurableTo = discrepancyDocumentsList.procurableTo != null ?
                new Date(discrepancyDocumentsList.procurableTo) : new Date();
              discrepancyDocumentsList.procurableTo = discrepancyDocumentsList.procurableTo;
              discrepancyDocumentsList.itemSellableTypeId = (discrepancyDocumentsList.procurable && !discrepancyDocumentsList.itemSellableTypeId) ? this.itemSellableTypes[0].id : discrepancyDocumentsList.itemSellableTypeId;
            } else if (filter && filter.length > 0 && filter[0].displayName == 'Permanent') {
              discrepancyDocumentsList.procurableTo = null;
              discrepancyDocumentsList.itemSellableTypeId = (discrepancyDocumentsList.procurable && !discrepancyDocumentsList.itemSellableTypeId) ? this.itemSellableTypes[0].id : discrepancyDocumentsList.itemSellableTypeId;
            }
            else {
              discrepancyDocumentsList.procurableTo = null;
            }
            this.productDetailsArray = this.getProductDetailsArray(index);
            this.productDetailsArray.insert(index, this.createProductDetailsGroupForm(discrepancyDocumentsList));
          });

        });

        if (this.districtsDetail && this.districtsDetail.length > 0) {
          this.productWarehouseFrom.get('districtId').setValue(this.districtsDetail);
        }

      }
      else {
        this.stockProductWarehouseFormArray = this.getstockProductWarehouseFormArray;
        this.stockProductWarehouseFormArray.push(this.createStockProductWarehouseGroupForm());
        this.buttonDisabled = true;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getDistrictSelectedValue(event: any) {
    let arr = [];
    let value = this.productWarehouseFrom?.get('districtId').value;
    value?.forEach(element => {
      arr.push(element.value);
    });
    // if(this.stockProductWarehouseFormArray != undefined && event.length == 0){

    //   this.stockProductWarehouseFormArray.clear();
    // }

    if (arr.length > 0) {
      let params = Object.assign({},
        { DistrictIds: arr.toString() },
        { ItemId: this.stockId },
        { UserId: this.userData.userId }
      );

      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STOCK_MAINTENANCE_PRODUCT_WAREHOUSE,
        undefined, false, prepareGetRequestHttpParams(null, null, params)
      ).subscribe(response => {
        if (response.isSuccess && response.statusCode === 200 && response.resources.length > 0) {
          // this.stockProductWarehouseFormArray != undefined ? this.stockProductWarehouseFormArray.clear() :
          if (this.stockProductWarehouseFormArray != undefined && this.stockProductWarehouseFormArray.value.length > 0) {
            this.stockProductWarehouseFormArray.clear();
          }
          this.stockProductWarehouseFormArray = this.getstockProductWarehouseFormArray;
          this.buttonDisabled = false;
          this.rxjsService.setGlobalLoaderProperty(false);

          response.resources.forEach((element: any, index: number) => {
            element.sellableFrom = element.sellableFrom != null ? new Date(element.sellableFrom) : new Date();
            element.sellableFrom = element.sellableFrom;
            this.stockProductWarehouseFormArray.push(this.createStockProductWarehouseGroupForm(element));

            element.productDetails.forEach((discrepancyDocumentsList: productDetailsArrayModal, i: number) => {
              discrepancyDocumentsList.procurableTo = discrepancyDocumentsList.procurableTo != null ?
                new Date(discrepancyDocumentsList.procurableTo) : new Date();
              discrepancyDocumentsList.procurableTo = discrepancyDocumentsList.procurableTo;
              this.productDetailsArray = this.getProductDetailsArray(index);
              this.productDetailsArray.insert(index, this.createProductDetailsGroupForm(discrepancyDocumentsList));
            });

          });
        }
        else {
          this.stockProductWarehouseFormArray = this.getstockProductWarehouseFormArray;
          this.stockProductWarehouseFormArray.push(this.createStockProductWarehouseGroupForm());
          this.buttonDisabled = true;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

    }
    else {
      let length = this.stockProductWarehouseFormArray?.length;
      for (let i = 0; i < length; i++) {
        this.stockProductWarehouseFormArray?.removeAt(this.stockProductWarehouseFormArray?.length - 1);
      }

    }
  }

  get getstockProductWarehouseFormArray(): FormArray {
    if (this.productWarehouseFrom !== undefined) {
      return this.productWarehouseFrom.get("stockProductWarehouseFormArray") as FormArray;
    }
  }

  //Create FormArray
  getProductDetailsArray(index: number): FormArray {
    if (!this.productWarehouseFrom) return;
    return this.stockProductWarehouseFormArray.at(index).get("productDetailsArray") as FormArray
  }



  //Create FormArray controls
  createStockProductWarehouseGroupForm(serviceCategoryListModel?: stockProductWarehouseModal): FormGroup {
    let ListModelControl = new stockProductWarehouseModal(serviceCategoryListModel);
    let formControls = {};
    Object.keys(ListModelControl).forEach((key) => {
      if (key === 'productDetailsArray') {
        formControls[key] = this.formBuilder.array([])
      }
      else {
        formControls[key] = [{ value: ListModelControl[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  /* Create FormArray controls */
  createProductDetailsGroupForm(products?: productDetailsArrayModal): FormGroup {
    let productData = new productDetailsArrayModal(products ? products : undefined);
    let formControls = {};
    Object.keys(productData).forEach((key) => {
      formControls[key] = [{ value: productData[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  getProductWarehouseDropdown() {
    //UX_DISTRICTS //IT_MANAGEMENT
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_DISTRICTS_BY_USER, undefined, false, prepareRequiredHttpParams({ UserId: this.userData.userId })
    ).subscribe((response: IApplicationResponse) => {
      if (response.statusCode == 200 && response.isSuccess) {
        let districts = response.resources;
        this.districtDropdown = getMatMultiData(response.resources);
        // for (var i = 0; i < districts.length; i++) {
        //   let tmp = {};
        //   tmp['value'] = districts[i].id;
        //   tmp['display'] = districts[i].displayName;
        //   this.districtDropdown.push(tmp);
        // }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      TechnicalMgntModuleApiSuffixModels.UX_STOCK_MAINTENANCE_ITEM_SELLALE_TYPES)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.itemSellableTypes = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

  }

  checkAllProcurableEvent(checked: any, productDetails: any, index) {

    productDetails.forEach(element => {
      element.controls['procurable'].patchValue(checked);
    });
    this.isChange = false;
  }

  checkAllObsoleteEvent(checked: any, productDetails: any, index) {
    productDetails.forEach(element => {
      element.controls['obsolete'].patchValue(checked);
    });
    this.isChange = false;
  }

  productWarehouseSubmit(): void {
    this.isSubmit = true;

    if (!this.editPermissions['Product - Warehouse']?.edit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let formValue = this.stockProductWarehouseFormArray.value;
    if (this.productWarehouseFrom.invalid) {
      return;
    }
    if (this.stockProductWarehouseFormArray.invalid) {
      return;
    }
    if (formValue.length <= 0) {
      this.snackbarService.openSnackbar('Atleast one warehouse is required', ResponseMessageTypes.WARNING);
      return;
    }
    let arr = [];
    if (formValue.length > 0) {
      formValue?.forEach((prods, i) => {
        prods.sellableFrom = this.datePipe.transform(prods.sellableFrom, 'yyyy-MM-dd');
        prods.createdUserId = this.userData.userId;
        this.buttonDisabled = false;
        prods.productDetailsArray?.forEach((det, j) => {

          if ((det.itemSellableTypeId == "2" && !det.procurableTo) || (det.itemSellableTypeId == 2 && !det.procurableTo) || (det.itemSellableTypeId == "2" && det.procurableTo == null) || (det.itemSellableTypeId == 2 && det.procurableTo == null)) {
            arr.push(true);
          }

          det.procurableTo = det.procurableTo ? this.datePipe.transform(det.procurableTo, 'yyyy-MM-dd') : null;
        });

        prods.warehouseProduct = prods.productDetailsArray;
        //delete prods.productDetailsArray;
      });
    }

    if (arr && arr.length > 0) {
      let tempErr = arr.filter(x => x == true);
      if (tempErr && tempErr.length > 0) {
        this.rxjsService.setGlobalLoaderProperty(false);
        return;
      }
    }

    this.rxjsService.setFormChangeDetectionProperty(true);
    const submit$ = this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.SAVE_STOCK_MAINTENANCE_PRODUCT_WAREHOUSE,
      formValue
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getWarehouseProductConsolidateDetails('get');
        this.isSubmit = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getProductWarehouseDetails() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE_PRODUCT_WAREHOUSE, this.stockId, false).subscribe((response) => {

      // this.productWarehouseDetail = response.resources;
      this.productWarehouseFormArray.clear();

      for (let i = 0; i < response.resources.length; i++) {

        this.productWarehouseFormArray.push(this.formBuilder.group(
          {
            'warehouseItemId': response.resources[i]['warehouseItemId'],
            'warehouseId': response.resources[i]['warehouseId'],
            'warehouse': response.resources[i]['warehouse'],
            'stockId': response.resources[i]['stockId'],
            'procurable': response.resources[i]['procurable'],
            'sellable': response.resources[i]['sellable'],
            'obsolete': response.resources[i]['obsolete'],
            'sellableFrom': new FormControl({ value: response.resources[i]['sellableFrom'], disabled: (response.resources[i]['sellable'] == true ? false : true) }, response.resources[i]['sellable'] == true ? Validators.required : null),
            'lastMovementDate': response.resources[i]['lastMovementDate'],
            'ageingDays': response.resources[i]['ageingDays'],
            'overridenBy': response.resources[i]['overridenBy'],
            'overridenUserId': response.resources[i]['overridenUserId'],
            'warehouseCode': response.resources[i]['warehouseCode'],
            'stockCode': response.resources[i]['stockCode'],
            'modifiedUserId': response.resources[i]['modifiedUserId'],
            'isChange': response.resources[i]['isChange'],
          }
        ));
      }
      this.rxjsService.setGlobalLoaderProperty(false);

    });
  }

  getSelectedLocation(location) {

    let isAddWarehouse = location.filter((x) => !this.productWarehouseFormArray.value.some(({ warehouseId: warId }) => x == warId));
    let isRemoveWarehouse = this.productWarehouseFormArray.value.filter(({ warehouseId: warId }) => !location.some((x) => warId == x))

    //    if(isAddWarehouse.length > 0){
    //        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_BALANCE_COUNT,undefined,false,prepareRequiredHttpParams({StockId:this.stockId,WarehouseId:isAddWarehouse[0]})).subscribe((response) => {

    //         if(response.resources == null){
    //           //add warehouse details
    //           if(this.productWarehouseFrom.value.productWarehouseList.find(x=>x.warehouseId == isAddWarehouse[0]) == null){

    //             if(this.productWarehouseDetail[0].resources.find(x=>x.warehouseId == isAddWarehouse[0]).selected){
    //               let productDetailId = this.productWarehouseDetail[1].resources.find(x=>x.warehouseId == isAddWarehouse[0]);

    //   this.productWarehouseFormArray.push(this.formBuilder.group({
    //               'warehouseItemId':productDetailId['warehouseItemId'],
    //               'warehouseId': productDetailId['warehouseId'],
    //               'warehouse': productDetailId['warehouse'],
    //               'stockId': productDetailId['stockId'],
    //               'procurable':productDetailId['procurable'],
    //               'sellable':productDetailId['sellable'],
    //               'obsolete':productDetailId['obsolete'],
    //               // 'sellableFrom': new FormControl(response.resources[i]['sellableFrom'],Validators.required),
    //               'sellableFrom': new FormControl({value : productDetailId['sellableFrom'],disabled : (productDetailId['sellable'] == true ? false : true)},productDetailId['sellable'] == true ? Validators.required : null),
    //               'lastMovementDate':productDetailId['lastMovementDate'],
    //               'ageingDays':productDetailId['ageingDays'],
    //               'overridenBy':productDetailId['overridenBy'],
    //               'overridenUserId':productDetailId['overridenUserId'],
    //               'warehouseCode': productDetailId['warehouseCode'],
    //               'stockCode': productDetailId['stockCode']
    //           }));
    //             }
    //             else{
    //               let productWarehouseData={
    //                 warehouseId :isAddWarehouse[0],
    //                 warehouse:this.locationList.find(x=>x.value == isAddWarehouse[0]).display,
    //                 stockId :this.stockId,
    //                 procurable :false,
    //                 sellable :false,
    //                 obsolete : false,
    //                 sellableFrom :'',
    //                 lastMovementDate:'',
    //                 ageingDays :'',
    //                 overridenBy: '',
    //                 warehouseCode: this.locationList.find(x => x.value == isAddWarehouse[0]).code,
    //                 stockCode: this.stockMaintenancesDetail.stockCode,
    //                 modifiedUserId: this.userData.userId,
    //               }

    //               this.productWarehouseFormArray.push(this.formBuilder.group({
    //                 'warehouseId': productWarehouseData['warehouseId'],
    //                 'warehouse': productWarehouseData['warehouse'],
    //                 'stockId': productWarehouseData['stockId'],
    //                 'procurable':productWarehouseData['procurable'],
    //                 'sellable': productWarehouseData['sellable'],
    //                 'obsolete':productWarehouseData['obsolete'],
    //                 'sellableFrom': new FormControl({value : productWarehouseData['sellableFrom'],disabled : (productWarehouseData['sellable'] == true ? false : true)},productWarehouseData['sellable'] == true ? Validators.required : null),
    //                 'lastMovementDate':productWarehouseData['lastMovementDate'],
    //                 'ageingDays':productWarehouseData['ageingDays'],
    //                 'overridenBy': productWarehouseData['overridenBy'],
    //                 'warehouseCode': productWarehouseData['warehouseCode'],
    //                 'stockCode': productWarehouseData['stockCode'],
    //                 'modifiedUserId': this.userData.userId
    //                 }));
    //             }


    //           }
    //         }
    //         else if(response.resources.totalStockCount == 0){
    //           //add warehouse detail
    //           if(this.productWarehouseFrom.value.productWarehouseList.find(x=>x.warehouseId == isAddWarehouse[0]) == null){

    //             if(this.productWarehouseDetail[0].resources.find(x=>x.warehouseId == isAddWarehouse[0]).selected){
    //               let productDetailId = this.productWarehouseDetail[1].resources.find(x=>x.warehouseId == isAddWarehouse[0]);

    //   this.productWarehouseFormArray.push(this.formBuilder.group({
    //               'warehouseItemId':productDetailId['warehouseItemId'],
    //               'warehouseId': productDetailId['warehouseId'],
    //               'warehouse': productDetailId['warehouse'],
    //               'stockId': productDetailId['stockId'],
    //               'procurable':productDetailId['procurable'],
    //               'sellable':productDetailId['sellable'],
    //               'obsolete':productDetailId['obsolete'],
    //               // 'sellableFrom': new FormControl(response.resources[i]['sellableFrom'],Validators.required),
    //               'sellableFrom': new FormControl({value : productDetailId['sellableFrom'],disabled : (productDetailId['sellable'] == true ? false : true)},productDetailId['sellable'] == true ? Validators.required : null),
    //               'lastMovementDate':productDetailId['lastMovementDate'],
    //               'ageingDays':productDetailId['ageingDays'],
    //               'overridenBy':productDetailId['overridenBy'],
    //               'overridenUserId':productDetailId['overridenUserId'],
    //               'warehouseCode': productDetailId['warehouseCode'],
    //               'stockCode': productDetailId['stockCode']
    //           }));
    //             }
    //             else{
    //               let productWarehouseData={
    //                 warehouseId :isAddWarehouse[0],
    //                 warehouse:this.locationList.find(x=>x.value == isAddWarehouse[0]).display,
    //                 stockId :this.stockId,
    //                 procurable :false,
    //                 sellable :false,
    //                 obsolete : false,
    //                 sellableFrom :'',
    //                 lastMovementDate:'',
    //                 ageingDays :'',
    //                 overridenBy: '',
    //                 warehouseCode: this.locationList.find(x => x.value == isAddWarehouse[0]).code,
    //                 stockCode: this.stockMaintenancesDetail.stockCode,
    //                 modifiedUserId: this.userData.userId,
    //               }

    //               this.productWarehouseFormArray.push(this.formBuilder.group({
    //                 'warehouseId': productWarehouseData['warehouseId'],
    //                 'warehouse': productWarehouseData['warehouse'],
    //                 'stockId': productWarehouseData['stockId'],
    //                 'procurable':productWarehouseData['procurable'],
    //                 'sellable': productWarehouseData['sellable'],
    //                 'obsolete':productWarehouseData['obsolete'],
    //                 'sellableFrom': new FormControl({value : productWarehouseData['sellableFrom'],disabled : (productWarehouseData['sellable'] == true ? false : true)},productWarehouseData['sellable'] == true ? Validators.required : null),
    //                 'lastMovementDate':productWarehouseData['lastMovementDate'],
    //                 'ageingDays':productWarehouseData['ageingDays'],
    //                 'overridenBy': productWarehouseData['overridenBy'],
    //                 'warehouseCode': productWarehouseData['warehouseCode'],
    //                 'stockCode': productWarehouseData['stockCode'],
    //                 'modifiedUserId': this.userData.userId
    //                 }));
    //             }


    //           }
    //         }
    //         else{
    //           // removes selected warehouse
    //           this.locationWarehouseSelectedOption =this.locationWarehouseSelectedOption.filter(x=>x != isAddWarehouse[0]);

    //           // for(let i=0;i<this.productWarehouseFrom.value.productWarehouseList.length;i++){
    //           //   if(isAddWarehouse.find(x =>x == this.productWarehouseFrom.value.productWarehouseList[i].warehouseId) == null){
    //           //     this.productWarehouseFormArray.removeAt(i);
    //           //   }
    //           // }
    //         }

    //        })

    //    }
    //    else if (isRemoveWarehouse.length >0){
    //        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_BALANCE_COUNT,undefined,false,prepareRequiredHttpParams({StockId:this.stockId,WarehouseId:isRemoveWarehouse[0].warehouseId})).subscribe((response) => {

    //          if(response.resources == null){
    //  // remove selected warehouse
    //  for(let i=0;i<this.productWarehouseFrom.value.productWarehouseList.length;i++){
    //   if(isRemoveWarehouse.find(x =>x.warehouseId == this.productWarehouseFrom.value.productWarehouseList[i].warehouseId) == null){
    //     this.productWarehouseFormArray.removeAt(i);
    //   }
    // }
    //         }
    //         else if(response.resources.totalStockCount == 0){
    //           // remove selected warehouse
    //           for(let i=0;i<this.productWarehouseFrom.value.productWarehouseList.length;i++){
    //             if(isRemoveWarehouse.find(x =>x.warehouseId == this.productWarehouseFrom.value.productWarehouseList[i].warehouseId) == null){
    //               this.productWarehouseFormArray.removeAt(i);
    //             }
    //           }
    //         }
    //         else{
    //           // dont remove selected warehouse
    //           this.locationWarehouseSelectedOption.push(isRemoveWarehouse[0].warehouseId);
    //         }
    //        })

    //    }
    if (location.length > this.productWarehouseFrom.value.productWarehouseList.length) {
      if (location.length > 0) {
        for (let i = 0; i < location.length; i++) {
          if (this.productWarehouseFrom.value.productWarehouseList.find(x => x.warehouseId == location[i]) == null) {

            if (this.productWarehouseDetail[0].resources.find(x => x.warehouseId == location[i]).selected) {
              let productDetailId = this.productWarehouseDetail[1].resources.find(x => x.warehouseId == location[i]);

              this.productWarehouseFormArray.push(this.formBuilder.group({
                'warehouseItemId': productDetailId['warehouseItemId'],
                'warehouseId': productDetailId['warehouseId'],
                'warehouse': productDetailId['warehouse'],
                'stockId': productDetailId['stockId'],
                'procurable': productDetailId['procurable'],
                'sellable': productDetailId['sellable'],
                'obsolete': productDetailId['obsolete'],
                // 'sellableFrom': new FormControl(response.resources[i]['sellableFrom'],Validators.required),
                'sellableFrom': new FormControl({ value: productDetailId['sellableFrom'], disabled: (productDetailId['sellable'] == true ? false : true) }, productDetailId['sellable'] == true ? Validators.required : null),
                'lastMovementDate': productDetailId['lastMovementDate'],
                'ageingDays': productDetailId['ageingDays'],
                'overridenBy': productDetailId['overridenBy'],
                'overridenUserId': productDetailId['overridenUserId'],
                'warehouseCode': productDetailId['warehouseCode'],
                'stockCode': productDetailId['stockCode']
              }));
            }
            else {
              let productWarehouseData = {
                warehouseId: location[i],
                warehouse: this.locationList.find(x => x.value == location[i]).display,
                stockId: this.stockId,
                procurable: false,
                sellable: false,
                obsolete: false,
                sellableFrom: '',
                lastMovementDate: '',
                ageingDays: '',
                overridenBy: '',
                warehouseCode: this.locationList.find(x => x.value == location[i]).code,
                stockCode: this.stockMaintenancesDetail.stockCode,
                modifiedUserId: this.userData.userId,
              }

              this.productWarehouseFormArray.push(this.formBuilder.group({
                'warehouseId': productWarehouseData['warehouseId'],
                'warehouse': productWarehouseData['warehouse'],
                'stockId': productWarehouseData['stockId'],
                'procurable': productWarehouseData['procurable'],
                'sellable': productWarehouseData['sellable'],
                'obsolete': productWarehouseData['obsolete'],
                'sellableFrom': new FormControl({ value: productWarehouseData['sellableFrom'], disabled: (productWarehouseData['sellable'] == true ? false : true) }, productWarehouseData['sellable'] == true ? Validators.required : null),
                'lastMovementDate': productWarehouseData['lastMovementDate'],
                'ageingDays': productWarehouseData['ageingDays'],
                'overridenBy': productWarehouseData['overridenBy'],
                'warehouseCode': productWarehouseData['warehouseCode'],
                'stockCode': productWarehouseData['stockCode'],
                'modifiedUserId': this.userData.userId
              }));
            }


          }
        }


      }
    }
    else {
      if (location.length == 0) {
        this.productWarehouseFormArray.clear();
      }
      else {
        for (let i = 0; i < this.productWarehouseFrom.value.productWarehouseList.length; i++) {
          if (location.find(x => x == this.productWarehouseFrom.value.productWarehouseList[i].warehouseId) == null) {
            this.productWarehouseFormArray.removeAt(i);
          }
        }
      }

      // index = a.findIndex(x => x.LastName === "Skeet");
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  onSellableChecked(item, event, index) {

    if (event.checked) {
      this.productWarehouseFormArray.controls[index].get('sellableFrom').enable();
      this.productWarehouseFormArray.controls[index].get('sellableFrom').setValidators([Validators.required])
      this.productWarehouseFormArray.controls[index].get('sellableFrom').updateValueAndValidity()
    }
    else {
      this.productWarehouseFormArray.controls[index].get('sellableFrom').disable()
      this.productWarehouseFormArray.controls[index].get('sellableFrom').setValue(null);
      this.productWarehouseFormArray.controls[index].get('sellableFrom').clearValidators();
      this.productWarehouseFormArray.controls[index].get('sellableFrom').updateValueAndValidity()
    }

  }

  onObsoleteChecked(item, event, index) {
    if (!event.checked) {
      this.productWarehouseFormArray.controls[index].get('overridenUserId').setValue(this.userData.userId);
    }
    else {
      this.productWarehouseFormArray.controls[index].get('overridenUserId').setValue(null);
    }
  }

  warehouseSubmit() {
    if (this.productWarehouseFrom.invalid) {
      return
    }

    let productWarehouseItem = [];
    // response.filter((x)=> !this.productWarehouseFormArray.value.some(({warehouseId:warId})=>x == warId));
    let isAddWarehouse = this.productWarehouseDetail[1].resources.filter(({ warehouseId: x }) => !this.productWarehouseFormArray.value.some(({ warehouseId: warId }) => x == warId));

    for (let i = 0; i < this.productWarehouseFormArray.value.length; i++) {

      // this.interBranchTransferForm.value.createdDate.toLocaleString();
      // == Object ? 'abc' : 'def'
      if (this.productWarehouseFormArray.value[i].warehouseItemId != null) {
        productWarehouseItem.push(
          {
            warehouseItemId: this.productWarehouseFormArray.value[i].warehouseItemId,
            warehouseId: this.productWarehouseFormArray.value[i].warehouseId,
            stockId: this.productWarehouseFormArray.value[i]?.stockId,
            procurable: this.productWarehouseFormArray.value[i].procurable,
            sellable: this.productWarehouseFormArray.value[i].sellable,
            obsolete: this.productWarehouseFormArray.value[i].obsolete,
            sellableFrom: this.datePipe.transform(this.productWarehouseFormArray.value[i].sellableFrom, 'yyyy-MM-dd'),
            overridenUserId: this.productWarehouseFormArray.value[i].overridenUserId,
            warehouseCode: this.productWarehouseFormArray.value[i].warehouseCode,
            stockCode: this.productWarehouseFormArray.value[i].stockCode,
            modifiedUserId: this.userData.userId,
            isActive: true
          }
        )
      }
      // this.ticketForm.controls.cancellationDate.value.toLocaleString();
      else {
        productWarehouseItem.push(
          {

            warehouseId: this.productWarehouseFormArray.value[i].warehouseId,
            stockId: this.productWarehouseFormArray.value[i]?.stockId,
            procurable: this.productWarehouseFormArray.value[i].procurable,
            sellable: this.productWarehouseFormArray.value[i].sellable,
            obsolete: this.productWarehouseFormArray.value[i].obsolete,
            sellableFrom: this.datePipe.transform(this.productWarehouseFormArray.value[i].sellableFrom, 'yyyy-MM-dd'),
            warehouseCode: this.productWarehouseFormArray.value[i].warehouseCode,
            stockCode: this.productWarehouseFormArray.value[i].stockCode,
            modifiedUserId: this.userData.userId,
            isActive: true
          }
        )
      }
      if (isAddWarehouse.length > 0) {
        isAddWarehouse.forEach((item) => {
          item.isActive = false;
          item.modifiedUserId = this.userData.userId;

          productWarehouseItem.push(item);
        })
      }
    }
    if (productWarehouseItem.length > 0) {
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SAVE_STOCK_MAINTENANCE_PRODUCT_WAREHOUSE, productWarehouseItem).subscribe({
        next: response => {
          if (response.isSuccess && response.statusCode === 200) {
            // this.router.navigate(['/inventory/stock-maintenance']);
          }
          else if (response.statusCode == 304) {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.WARNING);
          }
        },
        error: err => this.errorMessage = err
      })
    }
    else {
      this.snackbarService.openSnackbar('Please add atleast one warehouse', ResponseMessageTypes.WARNING);
    }

  }

  //end of product-warehouse tab method


  // Product threshold starts
  createThresholdForm(stockMaintenanceProductThresholdModel?: StockMaintenanceProductThresholdModel) {
    let productThresholdModel = new StockMaintenanceProductThresholdModel(stockMaintenanceProductThresholdModel);
    this.productThresholdDetailForm = this.formBuilder.group({});
    let productThresholdFormArray = this.formBuilder.array([]);
    Object.keys(productThresholdModel).forEach((key) => {
      // if (typeof productThresholdModel[key] === 'string') {
      // this.productThresholdDetailForm.addControl(key, new FormControl(productThresholdModel[key]));
      // }
      // else
      if (key == 'productThresholdWarehouseId') {
        this.productThresholdDetailForm.addControl(key, new FormControl(productThresholdModel[key]));
      }
      else {
        this.productThresholdDetailForm.addControl(key, productThresholdFormArray);
      }
    });

    //this.valueChange();
  }
  valueChange(value, index, type) {
    let minimumThreshold = this.productThresholdFormArray?.controls[index]?.get('minimumThreshold')?.value;
    let maximumThreshold = this.productThresholdFormArray?.controls[index]?.get('maximumThreshold')?.value;

    if (type == 'minimumThreshold') {
      minimumThreshold = value;
    } else if (type == 'maximumThreshold') {
      maximumThreshold = value;
    }

    if ((minimumThreshold && maximumThreshold) || (parseInt(maximumThreshold) == 0) || (parseInt(minimumThreshold) == 0)) {
      if (parseInt(minimumThreshold) > parseInt(maximumThreshold)) {

        this.isMaximum = true;
        this.snackbarService.openSnackbar('Minimum Threshold should be less than Maximum Threshold', ResponseMessageTypes.WARNING);
        return;
      } else if ((maximumThreshold == 0) || (minimumThreshold == 0)) {
        this.productThresholdFormArray?.controls[index]?.get('isChange')?.setValue(true);
        this.isMaximum = false;
      } else {
        this.productThresholdFormArray?.controls[index]?.get('isChange')?.setValue(true);
        this.isMaximum = false;
      }
    } else {
      this.productThresholdFormArray?.controls[index]?.get('isChange')?.setValue(true);
      this.isMaximum = false;
    }

  }
  get productThresholdFormArray(): FormArray {
    if (this.productThresholdDetailForm !== undefined) {
      return (<FormArray>this.productThresholdDetailForm.get('productThresholdDetails'));
    }
  }

  getSelectedNotifications(locationsData) {

    this.selectedThresholdLocations = locationsData;
    if (locationsData?.value.length > this.productThresholdFormArray.controls.length) {
      for (let location of locationsData.value) {
        location = location.value

        if (this.productThresholdFormArray.value.find(x => x.warehouseId == location) == null) {

          if (this.productThresholdDetail[0].resources.find(x => x.warehouseId == location).selected) {
            let productThresholdDetailId = this.productThresholdDetail[1].resources.find(x => x.WarehouseId == location);

            this.productThresholdFormArray.push(this.formBuilder.group({
              'warehouseId': productThresholdDetailId['WarehouseId'],
              'warehouseName': productThresholdDetailId['WarehouseName'],
              'stockId': this.stockId,
              'minimumThreshold': new FormControl(productThresholdDetailId['MinimumThreshold'] == null ? 0 : productThresholdDetailId['MinimumThreshold'], Validators.required),
              'maximumThreshold': new FormControl(productThresholdDetailId['MaximumThreshold'] == null ? 0 : productThresholdDetailId['MaximumThreshold'], Validators.required),
              'thresholdLastUpdated': productThresholdDetailId['ThresholdLastUpdated'],
              'modifiedUserId': this.userData.userId
            }));
          }
          else {
            let productWarehouseData = {
              'warehouseId': location,
              'warehouseName': this.locationList.find(x => x.value == location).display,
              'stockId': this.stockId,
              'minimumThreshold': 0,
              'maximumThreshold': 0,
              'thresholdLastUpdated': '',
              'modifiedUserId': this.userData.userId
            }
            this.productThresholdFormArray.push(this.formBuilder.group(productWarehouseData));
          }

        }
      }
    } else {
      if (locationsData?.value.length == 0) {
        this.productThresholdFormArray.clear();
      }
      else {
        for (let i = 0; i < this.productThresholdDetailForm.value.productThresholdDetails.length; i++) {
          if (locationsData.value.find(x => x == this.productThresholdDetailForm.value.productThresholdDetails[i].warehouseId) == null) {
            this.productThresholdFormArray.removeAt(i);
          }
        }
      }

    }
    this.rxjsService.setFormChangeDetectionProperty(true);
  }

  saveProductThreshold() {

    if (!this.editPermissions['Product - Threshold']?.edit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.productWarehouseFrom.invalid) {
      return;
    }
    if (this.productThresholdDetailForm.invalid) {
      return;
    }

    if (this.isMaximum) {
      this.snackbarService.openSnackbar('Minimum Threshold should be less than Maximum Threshold', ResponseMessageTypes.WARNING);
      return;
    }

    let productThresholdDetails = this.productThresholdDetailForm.value.productThresholdDetails;
    productThresholdDetails?.forEach((ele) => ele.isActive = true);

    let productThresholdWarehouseItem = [];
    let isAddThresholdWarehouse = this.productThresholdDetail[1].resources.filter(({ WarehouseId: x }) => !this.productThresholdDetailForm.value['productThresholdDetails'].some(({ warehouseId: warId }) => x == warId));

    for (let i = 0; i < this.productThresholdDetailForm.value['productThresholdDetails'].length; i++) {

      // this.interBranchTransferForm.value.createdDate.toLocaleString();
      // == Object ? 'abc' : 'def'
      if (this.productThresholdDetailForm.value['productThresholdDetails'][i].warehouseItemId != null) {

        productThresholdWarehouseItem.push(
          {
            warehouseItemId: this.productThresholdDetailForm.value['productThresholdDetails'][i].warehouseItemId,
            warehouseId: this.productThresholdDetailForm.value['productThresholdDetails'][i].warehouseId,
            warehouseName: this.productThresholdDetailForm.value['productThresholdDetails'][i].warehouseName,
            stockId: this.productThresholdDetailForm.value['productThresholdDetails'][i]?.stockId,
            minimumThreshold: this.productThresholdDetailForm.value['productThresholdDetails'][i].minimumThreshold,
            maximumThreshold: this.productThresholdDetailForm.value['productThresholdDetails'][i].maximumThreshold,
            thresholdLastUpdated: this.productThresholdDetailForm.value['productThresholdDetails'][i].thresholdLastUpdated,
            modifiedUserId: this.userData.userId,
            isActive: true,
            isChange: this.productThresholdDetailForm.value['productThresholdDetails'][i].isChange
          }
        )

      }
      // this.ticketForm.controls.cancellationDate.value.toLocaleString();
      else {
        productThresholdWarehouseItem.push(
          {

            warehouseItemId: this.productThresholdDetailForm.value['productThresholdDetails'][i].warehouseItemId == undefined ? null : this.productThresholdDetailForm.value['productThresholdDetails'][i].warehouseItemId,
            warehouseId: this.productThresholdDetailForm.value['productThresholdDetails'][i].warehouseId,
            warehouseName: this.productThresholdDetailForm.value['productThresholdDetails'][i].warehouseName,
            stockId: this.productThresholdDetailForm.value['productThresholdDetails'][i]?.stockId,
            minimumThreshold: this.productThresholdDetailForm.value['productThresholdDetails'][i].minimumThreshold,
            maximumThreshold: this.productThresholdDetailForm.value['productThresholdDetails'][i].maximumThreshold,
            thresholdLastUpdated: this.productThresholdDetailForm.value['productThresholdDetails'][i].thresholdLastUpdated,
            modifiedUserId: this.userData.userId,
            isActive: true,
            isChange: this.productThresholdDetailForm.value['productThresholdDetails'][i].isChange
          }
        )

      }

      if (isAddThresholdWarehouse.length > 0) {
        isAddThresholdWarehouse.forEach((item) => {

          item.isActive = false;
          item.modifiedUserId = this.userData.userId;
          productThresholdWarehouseItem.push(item);
        });

      }

    }

    if (productThresholdWarehouseItem.length > 0) {
      let arr = [];
      productThresholdWarehouseItem.forEach((element) => {

        //  if(!element.warehouseItemId)
        arr.push(element)
      });
      productThresholdWarehouseItem = arr;
    }

    if (productThresholdDetails.length > 0) {
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SAVE_STOCK_MAINTENANCE_PRODUCT_THRESHOLD, productThresholdDetails).subscribe({
        next: response => {
          if (response.isSuccess && response.statusCode === 200) {
            let tabIndex = { index: 2 };
            this.onTabClicked(tabIndex);
          }
          else if (response.statusCode == 304) {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.WARNING);
          }
        },
        error: err => this.errorMessage = err
      })
    }
    else {
      this.snackbarService.openSnackbar('Please add atleast one threshold', ResponseMessageTypes.WARNING);
    }

  }
  // Product threshold ends


  /* Product Skill Mapping Form Start */
  createProductSkillMappingForm() {
    let skillMappingAddEditModel = new ProductSkillMappingAddEditModel();
    this.productSkillMappingForm = this.formBuilder.group({});
    Object.keys(skillMappingAddEditModel).forEach((key) => {
      this.productSkillMappingForm.addControl(key, new FormControl(skillMappingAddEditModel[key]));
    });
    this.productSkillMappingForm = setRequiredValidator(this.productSkillMappingForm, ["SystemTypeSubCategory"]);
  }

  getSkillMappingDetails(): Observable<any> {
    let params = new HttpParams().set('stockId', this.stockId);
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE_SKILL_MAPPING, null, null, params);
  }

  selectSubcategory(id: any) {
    let category = this.getSubCategoryDropdown.find(x => x.id == id);
    if (category.displayName == 'Advanced' && this.skillMappingGetDetails.systemTypeName == 'Labour') {
      this.productSkillMappingForm.controls['SystemTypeSubCategory'].patchValue(category.id);
      this.productSkillMappingForm.controls['TechnicalSkillLevel1'].patchValue(true);
      this.productSkillMappingForm.controls['TechnicalSkillLevel2'].patchValue(true);
      this.productSkillMappingForm.controls['TechnicalSkillLevel3'].patchValue(true);
      this.productSkillMappingForm.controls['TechnicalSkillLevel4'].patchValue(true);
    }
    if (this.skillMappingGetDetails.systemTypeName != 'Discounts' || this.skillMappingGetDetails.systemTypeName != 'Labour') {
      let jobType = (this.skillMappingGetDetails.systemTypeName && !category.displayName) ? this.skillMappingGetDetails.systemTypeName :
        (!this.skillMappingGetDetails.systemTypeName && category.displayName) ? category.displayName :
          (this.skillMappingGetDetails.systemTypeName && category.displayName) ?
            this.skillMappingGetDetails.systemTypeName + '-' + category.displayName : '-';
      this.productSkillMappingForm.controls['jobTypeName'].patchValue(jobType);
    }
    if (this.skillMappingGetDetails.systemTypeName == 'Discounts') {
      this.productSkillMappingForm.controls['TechnicalSkillLevel1'].patchValue(false);
      this.productSkillMappingForm.controls['TechnicalSkillLevel2'].patchValue(false);
      this.productSkillMappingForm.controls['TechnicalSkillLevel3'].patchValue(false);
      this.productSkillMappingForm.controls['TechnicalSkillLevel4'].patchValue(false);
      this.productSkillMappingForm.controls['TechnicalSkillLevel1'].disable();
      this.productSkillMappingForm.controls['TechnicalSkillLevel2'].disable();
      this.productSkillMappingForm.controls['TechnicalSkillLevel3'].disable();
      this.productSkillMappingForm.controls['TechnicalSkillLevel4'].disable();
      this.productSkillMappingForm.controls['SystemTypeSubCategory'].patchValue('');
      this.productSkillMappingForm.controls['jobTypeName'].patchValue('-');
    }
  }

  setAll(checked, value) {

    if (checked) {

      if (value == 4) {
        this.productSkillMappingForm.controls['TechnicalSkillLevel1'].patchValue(true);
        this.productSkillMappingForm.controls['TechnicalSkillLevel2'].patchValue(true);
        this.productSkillMappingForm.controls['TechnicalSkillLevel3'].patchValue(true);
      }

      if (value == 3) {
        this.productSkillMappingForm.controls['TechnicalSkillLevel1'].patchValue(true);
        this.productSkillMappingForm.controls['TechnicalSkillLevel2'].patchValue(true);
      }

      if (value == 2) {
        this.productSkillMappingForm.controls['TechnicalSkillLevel1'].patchValue(true);
      }

      if (value == 1) {
        this.productSkillMappingForm.controls['TechnicalSkillLevel1'].patchValue(true);
      }

    }
    else {
      value = 0;
      // if (value == 1)
      this.productSkillMappingForm.controls['TechnicalSkillLevel1'].patchValue(false);
      // if (value == 2)
      this.productSkillMappingForm.controls['TechnicalSkillLevel2'].patchValue(false);
      // if (value == 3)
      this.productSkillMappingForm.controls['TechnicalSkillLevel3'].patchValue(false);
      // if (value == 4)
      this.productSkillMappingForm.controls['TechnicalSkillLevel4'].patchValue(false);
    }
  }

  saveProductMapping() {

    if (!this.editPermissions['Product - Skill Mapping']?.edit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.isDisctountSkillMapping && (this.productSkillMappingForm.invalid || !this.productSkillMappingForm.value.TechnicalSkillLevel1 && !this.productSkillMappingForm.value.TechnicalSkillLevel2
      && !this.productSkillMappingForm.value.TechnicalSkillLevel3 && !this.productSkillMappingForm.value.TechnicalSkillLevel4)) {
      this.updateTechnicalSkillLevel = false;
      return;
    }
    else {
      this.updateTechnicalSkillLevel = true;
    }

    this.productSkillMappingForm.value.modifiedUserId = this.userData.userId;
    this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE_SKILL_MAPPING, this.productSkillMappingForm.value)
      .subscribe({
        next: response => {
          if (response.isSuccess && response.statusCode === 200) {
            this.router.navigate(['/inventory/stock-maintenance'], { skipLocationChange: true });
            // this.router.navigate(['/inventory/stock-maintenance'],{ queryParams: { tab: 2} ,skipLocationChange: true });
          }
          else {
            this.isButtondisabled = false;
          }
        },
        error: err => this.errorMessage = err
      });
  }
  /* Product Skill Mapping Form End */

  // onFileChange uploadFiles
  uploadFiles(event) {

    this.selectedFiles = [];
    // 'jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'
    let fileExtension: boolean = false;
    const supportedExtensions = ['jpeg', 'jpg', 'png', 'PNG', 'JPG', 'JPEG'];
    for (var i = 0; i < event.target.files.length; i++) {
      let selectedFile = event.target.files[i];
      const path = selectedFile?.name?.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension)) {
        this.selectedFiles.push(event.target.files[i]);
        this.totalFileSize += event.target.files[i].size;
      }
      else {
        fileExtension = true;
      }
    }
    if (this.selectedFiles.length >= 0 && !fileExtension) {
      this.Save();
    }
    else {
      this.snackbarService.openSnackbar("Allow to upload this file format only - jpeg, jpg, png", ResponseMessageTypes.WARNING);
      return;
    }
  }

  removeItem(event) {


    let sendData = new FormData();
    let obj = {
      ItemId: this.stockId,
      CreatedUserId: this.userData.userId
    }
    sendData.append("imageDetails", JSON.stringify(obj));

    if (this.selectedFiles.length > 0) {
      for (const file of this.selectedFiles) {
        sendData.append('File', file);
      }
    }
    else {
      this.stockMaintainanceHeaderImage = "";
      return;
    }



    this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_IMAGES, sendData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.snackbarService.openSnackbar("Image has been removed", ResponseMessageTypes.SUCCESS);
          this.stockMaintainanceHeaderImage = "";
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  openManalRepository() {
    this.rxjsService.setGlobalLoaderProperty(true);
    if (this.manualRepositorySubscritption && !this.manualRepositorySubscritption.closed) {
      this.manualRepositorySubscritption.unsubscribe();
    }
    this.manualRepositorySubscritption = this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_MAINTENANCE_MANUALS, null, false, prepareRequiredHttpParams({ ItemId: this.stockId })).subscribe((res: any) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (res?.isSuccess == true && res?.statusCode == 200 && res?.resources) {
          const ref = this.dialogService.open(ManualRespositoryDialogComponent, {
            header: 'Supporting Documents',
            baseZIndex: 1000,
            width: '400px',
            closable: false,
            showHeader: false,
            data: {
              itemId: this.stockId,
              ...res,
              createdUserId: this.userData?.userId,
            },
          });
          ref.onClose.subscribe((res) => {
            if (res) {

            }
          });
        }
      });
  }

  Save(): void {

    if (this.totalFileSize > this.maxFileSize)
      return;

    let formData = new FormData();
    let obj = {
      ItemId: this.stockId,
      CreatedUserId: this.userData.userId
    }
    formData.append("imageDetails", JSON.stringify(obj));

    if (this.selectedFiles.length > 0) {
      for (const file of this.selectedFiles) {
        formData.append('File', file);
      }
    }

    this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_IMAGES, formData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.stockMaintainanceHeaderImage = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.selectedFiles = [];
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

  }

  openBarcodeModal() {
    const dialogReff = this.dialog.open(BarcodePrintComponent,
      {
        width: '400px', height: '400px', disableClose: true,
        data: { serialNumbers: new Array(this.productMaintenanceForm?.get('stockCodeBarcode')?.value) }
      });
  }

  navigateToView() {
    this.router.navigate(['inventory/stock-maintenance/view'], { queryParams: { id: this.stockId }, skipLocationChange: true });
  }

}
