import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService,currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService, CrudType } from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { combineLatest } from 'rxjs';
import { BarcodePrintComponent } from '../order-receipts/barcode-print';
@Component({
  selector: 'app-stock-maintenance-view',
  templateUrl: './stock-maintenance-view.component.html',
  styleUrls: ['./stock-maintenance-view.component.scss']
})
export class StockMaintenanceViewComponent implements OnInit {

  stockId;
  userData: any;
  selectedTabIndex=0;
  stockMaintenancesDetail:any={ };
  stockMaintenancesWarehouseDetail;
  stockMaintenancesThresholdDetail;
  stockMaintenancesSkillMappingDetail = {
    systemTypeName: null,
    systemTypeSubCategoryName: null,
    jobTypeName: null
  };
  breadCrumbTitle = "product - maintenance";
  productSkillArray: any;
  stockMaintainanceHeaderImage: any;
  productWarehouseDetails: any = {};
  sellableList = [{ value: true, display: 'Yes' },{ value: false, display: 'No' }];
  costSource: string = '';
  barcodes: any = [];
  documents = [];
  isSaveMaintenance: boolean = true;
  isShowRepairCode: boolean = false;
  primengTableConfigProperties: any;
  loggedInUserData;
  condition;
  constructor(
    private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private cdr: ChangeDetectorRef,private dialog: MatDialog, private snackbarService: SnackbarService,
    private router: Router, private rxjsService: RxjsService,private store: Store<AppState>) {
      this.stockId = this.activatedRoute.snapshot.queryParams.id;
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        this.userData = userData;
      });
      this.primengTableConfigProperties = {
        tableCaption: "Stock Maintenance",
        selectedTabIndex: 0,
        breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Stock Maintenance List', relativeRouterUrl: '/inventory/stock-maintenance' }, { displayName: 'Stock Maintenance View :'+ this.breadCrumbTitle.toLocaleUpperCase(), relativeRouterUrl: '' }],
        tableComponentConfigs: {
          tabsList: [
            {
              selectedTabIndex:0,
              enableBreadCrumb: true,
              enableAction: true,
              enableEditActionBtn: true
            }
          ]
        }
      }
    }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getStockMaintenanceDetails().subscribe((response: IApplicationResponse) => {
      this.stockMaintenancesDetail = response.resources;
      this.isSaveMaintenance = response.resources?.productMaintenance?.condition == null ? true : (response.resources?.productMaintenance?.condition === 'Refurbished' ? (response.resources?.productMaintenance?.newStockCodeid == null ? true : false) : false)
      // this.isShowRepairCode = response.resources?.isRepairCode;
      // this.productWarehouseDetails = response.resources.productWarehouse;
      if (this.stockMaintenancesDetail.productSkillMapping.technicalSkillLeavel) {
        let skillDetails = this.stockMaintenancesDetail.productSkillMapping.technicalSkillLeavel
        this.productSkillArray = skillDetails?.split(',');
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    this.getWarehouseDocuments();
    this.getStockMaintenanceHeaderDetails().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.stockMaintainanceHeaderImage = response.resources;
        this.isShowRepairCode = response.resources?.isRepairCode;
        this.condition =  response.resources?.condition;
        this.barcodes = new Array(this.stockMaintainanceHeaderImage?.stockCodeBarcode);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

    this.getProductWarehouseDetails().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.productWarehouseDetails = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }
  getWarehouseDocuments() {
     this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE_DOCUMENTS,undefined,false,prepareRequiredHttpParams({
      ItemId: this.stockId
    })).subscribe((response: IApplicationResponse) => {
         if(response.isSuccess && response.statusCode==200) {
          this.documents = response.resources;
         }
    });
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][INVENTORY_COMPONENT.STOCK_MAINTENANCE];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  ngAfterViewChecked(){
    this.cdr.detectChanges();
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {  
      case CrudType.EDIT:
        this.navigateToEdit();
        break;    
      default:
    }
  }
  navigateToEdit() {
     if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
     }
    this.router.navigate(['inventory/stock-maintenance/add-edit'], { queryParams: { id: this.stockId }, skipLocationChange: true });
  }

  getStockMaintenanceDetails(): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE_CONSOLIDATE,undefined,false,prepareRequiredHttpParams({
      UserId:this.userData.userId,
      ItemId: this.stockId
    }));
  }

  getProductWarehouseDetails(): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE_PRODUCT_WAREHOUSE_CONSOLIDATE,undefined,false,prepareRequiredHttpParams({
      ItemId: this.stockId
    }, { userId: this.userData.userId }));
  }

  getStockMaintenanceHeaderDetails(): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MAINTENANCE, this.stockId, false);
  }

  onTabClicked(tab): void {
    this.selectedTabIndex = tab.index;
    this.breadCrumbTitle = tab.tab.textLabel;
    this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Stock Maintenance List', relativeRouterUrl: '/inventory/stock-maintenance' }, { displayName: 'Stock Maintenance View :'+ this.breadCrumbTitle?.toLocaleUpperCase(), relativeRouterUrl: '' }];
  }

  openBarcodeModal(){
    const dialogReff = this.dialog.open(BarcodePrintComponent,
    { width: '400px', height: '400px', disableClose: true,
      data: { serialNumbers: new Array(this.stockMaintainanceHeaderImage?.stockCodeBarcode) }
    });
  }

}
