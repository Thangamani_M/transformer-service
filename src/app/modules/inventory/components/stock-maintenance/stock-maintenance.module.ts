import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { StockMaintenanceComponent, StockMaintenanceRoutingModule } from '@inventory/components/stock-maintenance';
import { BarcodePrinterModule } from '../barcode-printer/barcode-printer/barcode-printer.module';
import { StockMaintenanceAddEditComponent } from './stock-maintenance-add-edit.component';
import { StockMaintenanceViewComponent } from './stock-maintenance-view.component';

@NgModule({
  declarations: [StockMaintenanceComponent, StockMaintenanceAddEditComponent, StockMaintenanceViewComponent],
  imports: [
    CommonModule,
    StockMaintenanceRoutingModule,
    ReactiveFormsModule,
    FormsModule, LayoutModule,
    MaterialModule,
    BarcodePrinterModule,
    SharedModule,

  ]
})
export class StockMaintenanceModule { }
