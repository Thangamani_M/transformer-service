import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import {
    DetailedAutomatedListComponent, StockOrderAddEditComponent, StockOrderApprovalComponent,
    StockOrderApprovalViewComponent, StockOrderEditViewComponent,
    StockOrderInterbranchModalComponent, StockOrderListComponent, StockOrderModelComponent, StockOrderRoutingModule
} from '@inventory/components/stock-order';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NgxPrintModule } from 'ngx-print';
import { FullListInterBranchReqisitionRegComponent } from './add-edit-list/fullList-interBranch-requisitionReg.component';
import { RequisitionCreationModalComponent } from './add-edit-list/requisition-creation-modal.component';
import { StockInfoModalComponent } from './add-edit-list/stock-info-modal.component';
import { StockOrderViewComponent } from './add-edit-list/stock-order-view.component';
import { ExclutionListComponent } from './exclusion/exclution-list.component';
import { ExclutionUpdateComponent } from './exclusion/exclution-update.component';
import { ExclutionViewComponent } from './exclusion/exclution-view.component';



@NgModule({
  declarations: [StockOrderAddEditComponent, StockOrderApprovalComponent, StockOrderApprovalViewComponent,
    StockOrderListComponent, StockOrderEditViewComponent, StockOrderInterbranchModalComponent, FullListInterBranchReqisitionRegComponent,
    StockOrderViewComponent, RequisitionCreationModalComponent, StockOrderModelComponent, StockInfoModalComponent, DetailedAutomatedListComponent, ExclutionListComponent, ExclutionViewComponent, ExclutionUpdateComponent],

  imports: [
    CommonModule,
    StockOrderRoutingModule,
    ReactiveFormsModule,
    FormsModule, LayoutModule,
    MaterialModule,
    SharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule, NgxPrintModule
  ],

  entryComponents: [StockOrderAddEditComponent, StockOrderInterbranchModalComponent, StockOrderModelComponent, RequisitionCreationModalComponent, StockInfoModalComponent]
})
export class StockOrderModule { }