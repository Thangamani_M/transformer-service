import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, CustomDirectiveConfig, debounceTimeForDeepNestedObjectSearchkeyword,
  HttpCancelService,
  IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  ResponseMessageTypes,
  RxjsService,
  SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-exclution-update',
  templateUrl: './exclution-update.component.html',
  styleUrls: ['./exclution-update.component.scss']
})
export class ExclutionUpdateComponent implements OnInit {

  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  userData: UserLogin;
  searchForm: FormGroup;
  observableResponse: any;
  dataList: any
  status: any = [];
  selectedRows: string[] = [];
  selectedRow: any;
  selectedTabIndex: any = 0;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties: any;
  pageSize: number = 10;
  columnFilterForm: FormGroup;
  searchColumns: any
  row: any = {}
  loading: boolean;
  today: any = new Date();
  exclusionId: string = '';
  exclusionDetails: any = {};
  alteredItems: any = []

  constructor(
    private commonService: CrudService, private tableFilterFormService: TableFilterFormService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder, private momentService: MomentService,
    private rxjsService: RxjsService, private httpCancelService: HttpCancelService,
    private store: Store<AppState>, private snackbarService: SnackbarService
  ) {

    this.exclusionId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Update Exclusion Report",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Ordering', relativeRouterUrl: '' }, { displayName: 'Exclusion Report', relativeRouterUrl: '/inventory/requisitions/exclusion' }, { displayName: 'View Exclusion Report', relativeRouterUrl: '/inventory/requisitions/exclusion/view', queryParams: { id: this.exclusionId } },{ displayName: 'Update Exclusion Report', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Update Exclusion Report',
            dataKey: 'exclusionId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'stockCode', header: 'Stock Code', width: '110px' },
            { field: 'stockDescription', header: 'Stock Description', width: '200px' },
            { field: 'fixedPricing', header: 'Fixed Pricing', width: '120px' },
            { field: 'condition', header: 'Condition', width: '150px' },
            { field: 'obsolete', header: 'Obsolete', width: '100px' },
            { field: 'procurable', header: 'Procurable', width: '100px' },
            { field: 'quantityOnHand', header: 'Quantity On Hand', width: '105px' },
            { field: 'minimumThreshold', header: 'Min Threshold', width: '100px' },
            { field: 'maximumThreshold', header: 'Max Threshold', width: '100px' },
            { field: 'pendingRequisitionPOQuantity', header: 'Pending Requisition/POs', width: '200px' },
            { field: 'pickingOrderQuantity', header: 'Picking Order Qty', width: '200px' },
            { field: 'maxThresholdRefillQuantity', header: 'Max Threshold Refill Qty', width: '200px' },
            { field: 'orderQuantityMaxThreshold', header: 'Order Qty > Max Threshold', width: '210px' },
            { field: 'requestedQuantity', header: 'Requsted Qty', width: '150px' },
            { field: 'comments', header: 'Comments', width: '350px' }],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            shouldShowFilterActionBtn: false,
            apiSuffixModel: InventoryModuleApiSuffixModels.EXCLUSION_LIST_DETAILS,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }

    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? params['params']['tab'] ? +params['params']['tab'] : 0 : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
     // this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });



  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }


  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {

    otherParams = { ...otherParams, ...{ exclusionListId: this.exclusionId } }
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources.items;
        this.dataList = this.observableResponse;
        this.totalRecords = data.resources.totalCount;
        this.exclusionDetails = data.resources;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = data.resources.totalCount;
      }
    })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        // this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair

          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        otherParams['UserId'] = this.userData.userId;
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        // this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  onTabChange(event) {
    this.row = {}
    this.columnFilterForm = this.formBuilder.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/inventory/exlusion-report'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  onUpdate() {

    const formValue = this.dataList;
    this.alteredItems = []
    formValue.forEach(element => {
      let data = {
        exclusionListItemId: element.exclusionListItemId,
        previousComment: element.previousComment,
        comments: element.comments,
        modifiedUserId: this.userData.userId
      }
      this.alteredItems.push(data)
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> = this.commonService.update(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.EXCLUSION_LIST, this.alteredItems)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.router.navigateByUrl('/inventory/requisitions/exclusion');
      }
      else if (response.isSuccess == false) {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.WARNING);
      }
    })
  }

}


