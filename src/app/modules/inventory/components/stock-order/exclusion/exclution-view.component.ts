import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, LoggedInUserModel, prepareGetRequestHttpParams,currentComponentPageBasedPermissionsSelector$, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions } from '@app/shared/utils';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-exclution-view',
  templateUrl: './exclution-view.component.html',
  styleUrls: ['./exclution-view.component.scss']
})
export class ExclutionViewComponent extends PrimeNgTableVariablesModel implements OnInit {

  exclusionId: string;
  exclusionDetails: any = [];
  // editStatus: any;
  userData: UserLogin;
  searchForm: FormGroup;
  observableResponse: any;
  dataList: any
  status: any = [];
  selectedRows: string[] = [];
  selectedTabIndex: any = 0;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel; 
  primengTableConfigProperties:any;
  columnFilterForm: FormGroup;
  loading: boolean;
  row: any = {};
  first: any = 0;
  reset: boolean;
  namedStackMappingViewDetails;
  constructor(private router: Router, private store: Store<AppState>,private snackbarService:SnackbarService,
    private crudService: CrudService, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private activatedRoute: ActivatedRoute) {
    super();

    this.exclusionId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });  

    // this.primengTableConfigProperties = {
    //   tableCaption: "",
    //   breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' },{ displayName: '', relativeRouterUrl: '' }],
    //   selectedTabIndex: 0,
    //   tableComponentConfigs: {
    //     tabsList: [
    //       {
    //         caption: '',
    //         dataKey: 'cycleCountId',
    //         enableBreadCrumb: true,
    //         enableAction: true,
    //         enableReset: false,
    //         enableGlobalSearch: false,
    //         reorderableColumns: false,
    //         resizableColumns: false,
    //         enableScrollable: true,
    //         checkBox: false,
    //         enableRowDelete: false,
    //         enableStatusActiveAction: false,
    //         enableFieldsSearch: true,
    //         rowExpantable: false,
    //         rowExpantableIndex: 0,
    //         enableHyperLink: true,
    //         cursorLinkIndex: 0,
    //         columns: [{ field: 'stockCode', header: 'Stock Code', width: '180px' },
    //         { field: 'stockDescription', header: 'Stock Description', width: '200px' },
    //         { field: 'fixedPricing', header: 'Fixed Pricing', width: '180px' },
    //         { field: 'condition', header: 'Condition', width: '150px' },
    //         { field: 'obsolete', header: 'Obsolete', width: '150px' },
    //         { field: 'procurable', header: 'Procurable', width: '150px' },
    //         { field: 'quantityOnHand', header: 'Quantity On Hand', width: '105px' },
    //         { field: 'minimumThreshold', header: 'Min Threshold', width: '180px' },
    //         { field: 'maximumThreshold', header: 'Max Threshold', width: '180px' },
    //         { field: 'pendingRequisitionPOQuantity', header: 'Pending Requisition/POs', width: '200px' },
    //         { field: 'pickingOrderQuantity', header: 'Picking Order Qty', width: '200px' },
    //         { field: 'maxThresholdRefillQuantity', header: 'Max Threshold Refill Qty', width: '200px' },
    //         { field: 'orderQuantityMaxThreshold', header: 'Order Qty > Max Threshold', width: '210px' },
    //         { field: 'requestedQuantity', header: 'Requsted Qty', width: '150px' },
    //         { field: 'comments', header: 'Comments', width: '150px',isparagraph:true }], 
    //         shouldShowDeleteActionBtn: false,
    //         enableAddActionBtn: false,
    //         areCheckboxesRequired: false,
    //         isDateWithTimeRequired: true,
    //         shouldShowFilterActionBtn: false,
    //         apiSuffixModel: InventoryModuleApiSuffixModels.EXCLUSION_LIST_DETAILS,
    //         moduleName: ModulesBasedApiSuffix.INVENTORY,
    //       }
    //     ]
    //   }
    // }
 
    this.primengTableConfigProperties = {
      tableCaption: "View Exclusion Report",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Ordering', relativeRouterUrl: '' } ,{ displayName: 'Exclusion Report', relativeRouterUrl: '/inventory/requisitions/exclusion' },{ displayName: 'View Exclusion Report', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'View Exclusion Report',
            dataKey: 'cycleCountId',
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'stockCode', header: 'Stock Code', width: '180px' },
            { field: 'stockDescription', header: 'Stock Description', width: '200px' },
            { field: 'fixedPricing', header: 'Fixed Pricing', width: '180px' },
            { field: 'condition', header: 'Condition', width: '150px' },
            { field: 'obsolete', header: 'Obsolete', width: '150px' },
            { field: 'procurable', header: 'Procurable', width: '150px' },
            { field: 'quantityOnHand', header: 'Quantity On Hand', width: '105px' },
            { field: 'minimumThreshold', header: 'Min Threshold', width: '180px' },
            { field: 'maximumThreshold', header: 'Max Threshold', width: '180px' },
            { field: 'pendingRequisitionPOQuantity', header: 'Pending Requisition/POs', width: '200px' },
            { field: 'pickingOrderQuantity', header: 'Picking Order Qty', width: '200px' },
            { field: 'maxThresholdRefillQuantity', header: 'Max Threshold Refill Qty', width: '200px' },
            { field: 'orderQuantityMaxThreshold', header: 'Order Qty > Max Threshold', width: '210px' },
            { field: 'requestedQuantity', header: 'Requsted Qty', width: '150px' },
            { field: 'comments', header: 'Comments', width: '150px',isparagraph:true }], 
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            shouldShowFilterActionBtn: false,
            apiSuffixModel: InventoryModuleApiSuffixModels.EXCLUSION_LIST_DETAILS,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
      }
        ]
      }
    }
    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? params['params']['tab'] ? +params['params']['tab'] : 0 : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    //  this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });

  
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }


  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.EXCLUSION_LIST]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = {...otherParams,...{exclusionListId: this.exclusionId}}
    this.loading = true;      
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels,
      undefined, false, 
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources.items;
        this.dataList = this.observableResponse;
        this.totalRecords = data.resources.totalCount;
        this.exclusionDetails = data.resources;
        this.onShowValue();
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = data.resources.totalCount;
      }
    });
  }
  onShowValue() {
    this.namedStackMappingViewDetails = [
      { name: 'Exclusion ID', value: this.exclusionDetails?.exclusionListNumber },
      { name: 'Generated Date', value: this.exclusionDetails?.createdDate },
      { name: 'Warehouse', value: this.exclusionDetails?.warehouseName },
    ]
  }
  navigateToEdit(){
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
     }
    this.router.navigate(['/inventory/requisitions/exclusion/update'], {
      queryParams: {
        id: this.exclusionId,
      }, skipLocationChange: true
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.EDIT:
        // this.openAddEditPage(CrudType.EDIT, row);
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
        this.navigateToEdit();
        break;
        case CrudType.GET:
          unknownVar = {...unknownVar,...{UserId:this.userData.userId}};
          this.getRequiredListData(row["pageIndex"],row["pageSize"], unknownVar)
          break;
      default:
    }
  }
}
