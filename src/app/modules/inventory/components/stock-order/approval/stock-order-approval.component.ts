import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@app/modules';
import { AppState } from '@app/reducers';
import {ComponentProperties, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  prepareDynamicTableTabsFromPermissions,currentComponentPageBasedPermissionsSelector$,
  prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { RequisitionListFilterModel } from '@modules/inventory/models/RequisitionListFilter.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin,} from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-stock-order-approval',
  templateUrl: './stock-order-approval.component.html'
})
export class StockOrderApprovalComponent extends PrimeNgTableVariablesModel {
  observableResponse;
  componentProperties = new ComponentProperties();
  primengTableConfigProperties: any;
  status: any = [];
  row: any = {}
  dateFormat = 'MMM dd, yyyy';
  pageSize: number = 10;
  showFilterForm = false;
  warehouseList = [];
  requisitionFilterForm: FormGroup;
  stockOrderList = [];
  stockCodeList = [];
  statusList = [];
  startTodayDate = 0
  userData: UserLogin;
  otherParams;
  filterParams;
  constructor(private crudService: CrudService, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private datePipe: DatePipe,private snackbarService:SnackbarService,
    public dialogService: DialogService, private router: Router,
    private store: Store<AppState>,private rxjsService: RxjsService,
    private changeDetectorRef: ChangeDetectorRef)
    {
     super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Requisition Approval",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Ordering', relativeRouterUrl: '' }, { displayName: 'Requisition Approval List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Requisition Approval List',
            dataKey: 'requisitionId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            enableAction: true,
            enableEditActionBtn: false,
            shouldShowFilterActionBtn: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Daily Staging Bay Report',
            printSection: 'print-section0',
            columns: [{ field: 'requisitionNumber', header: 'Requisition ID', width: '130px' },
            { field: 'rqnStatus', header: 'Status', width: '110px' },
            { field: 'requisitionRequestNumber', header: 'Requisition Request Number', width: '150px' },
            { field: 'requisitionDate', header: 'Requisition Date', width: '180px' },
            { field: 'warehouse', header: 'Warehouse', width: '200px' },
            { field: 'orderType', header: 'Order Type', width: '120px' },
            { field: 'requisitioner', header: 'Requisitioner', width: '150px' },
            { field: 'approval', header: 'Approval', width: '150px' },
            { field: 'approvalDate', header: 'Approval Date', width: '150px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.REQUISITION_GET_DETAILS,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.REQUISITION_APPROVAL]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams =  {...otherParams,...{ UserId: this.userData.userId}};
    this.otherParams = otherParams;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams )
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.requisitionDate = this.datePipe.transform(val?.requisitionDate, 'yyyy-MM-dd HH:mm:ss');
          val.approvalDate = this.datePipe.transform(val?.approvalDate, 'yyyy-MM-dd HH:mm:ss');          
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
    })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        if(this.filterParams){
          unknownVar = {...unknownVar,...this.filterParams}
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        this.displayAndLoadFilterData();
        break;
      case CrudType.EXPORT:
         if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
          let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
          this.exportList(pageIndex,pageSize );
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any, otherParams?: object) {
    let queryParams;
    queryParams = this.generateQueryParams(otherParams,pageIndex,pageSize);
    this.crudService.downloadFile(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.REQUISITION_APPROVAL_EXPORT,null, null, queryParams).subscribe((response: any) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response) {
          this.saveAsExcelFile(response, 'REQUISITION APPROVAL');
          this.rxjsService.setGlobalLoaderProperty(false);
        }
    });
  }
  generateQueryParams(queryParams,pageIndex,pageSize) {
    pageIndex=0;
    let queryParamsString;
    queryParamsString = new HttpParams({ fromObject: this.otherParams })
    .set('pageIndex',pageIndex)
    .set('pageSize',pageSize);

    queryParamsString.toString();
    return '?' + queryParamsString;
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/inventory/requisitions/approvals/view/'], {
          queryParams: {
            id: editableObject['requisitionId'],
            type: 'requisition-list'
          }
        });
        break;
    }
  }

  getAllDataList(): void {
    forkJoin([this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId })),
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCKORDERS_DROPDOWN),
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCKCODES_DROPDOWN),
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RQNSTATUS),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.warehouseList = resp.resources;

              break;
            case 1:
              this.stockOrderList = resp.resources;

              break;
            case 2:
              this.stockCodeList = resp.resources;

              break;
            case 3:
              this.statusList = resp.resources;
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  displayAndLoadFilterData() {
    this.createRequisitionFilterForm();
    this.showFilterForm = !this.showFilterForm;
    this.getAllDataList();
  }

  createRequisitionFilterForm(requisitionListModel?: RequisitionListFilterModel) {
    let requisitionListFilterModel = new RequisitionListFilterModel(requisitionListModel);
    this.requisitionFilterForm = this.formBuilder.group({});
    Object.keys(requisitionListFilterModel).forEach((key) => {
      if (typeof requisitionListFilterModel[key] === 'string') {
        this.requisitionFilterForm.addControl(key, new FormControl(requisitionListFilterModel[key]));
      }
    });
  }

  ngAfterViewChecked() {
    this.changeDetectorRef.detectChanges();
  }

  getStockOrderNumber(event) {
    let warehouse = [];
    event.value.forEach(element => {
      warehouse.push(element.id);
    });
    if (event.value.length > 0) {
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STOCKORDERS_DROPDOWN,
        undefined,
        false,
        prepareRequiredHttpParams({
          WarehouseId: warehouse
        })
      )
        .subscribe(response => {
          this.stockOrderList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);

        });
    }
  }

  getSelectedStockCode(stockOrderId) {
    let stockOrderIds = [];
    stockOrderId.value.forEach(element => {
      stockOrderIds.push(element.id);
    });
    if (stockOrderId.value.length > 0) {
      this.stockCodeList = [];
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STOCKCODES_DROPDOWN,
        undefined,
        false,
        prepareRequiredHttpParams({
          stockOrderId: stockOrderIds
        })
      )
        .subscribe(response => {
          this.stockCodeList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  submitFilter() {
    let WarehouseIds = [];
    if (this.requisitionFilterForm.get('warehouseId').value.length > 0) {
      this.requisitionFilterForm.get('warehouseId').value.forEach(element => {
        WarehouseIds.push(element.id);
      });
    }
    let rqnStatusIds = [];
    if (this.requisitionFilterForm.get('rqnStatusId').value.length > 0) {
      this.requisitionFilterForm.get('rqnStatusId').value.forEach(status => {
        rqnStatusIds.push(status.id);
      });
    }
    let stockOrderIds = [];
    if (this.requisitionFilterForm.get('stockOrderId').value.length > 0) {
      this.requisitionFilterForm.get('stockOrderId').value.forEach(stocks => {
        stockOrderIds.push(stocks.id);
      });
    }
    let stockCodeIds = [];
    if (this.requisitionFilterForm.get('stockCodeId').value.length > 0) {
      this.requisitionFilterForm.get('stockCodeId').value.forEach(codes => {
        stockCodeIds.push(codes.id);
      });
    }
    let data = Object.assign({},
      this.requisitionFilterForm.get('warehouseId').value.length == 0 ? null : { warehouseId: WarehouseIds },
      this.requisitionFilterForm.get('fromDate').value == '' ? null : { fromDate: this.datePipe.transform(this.requisitionFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
      this.requisitionFilterForm.get('toDate').value == '' ? null : { toDate: this.datePipe.transform(this.requisitionFilterForm.get('toDate').value, 'yyyy-MM-dd') },
      this.requisitionFilterForm.get('rqnStatusId').value.length == 0 ? null : { rqnStatusId: rqnStatusIds },
      this.requisitionFilterForm.get('stockOrderId').value.length == 0 ? null : { stockOrderId: stockOrderIds },
      this.requisitionFilterForm.get('stockCodeId').value.length == 0 ? null : { stockCode: stockCodeIds }, { UserId: this.userData.userId }
    );
    this.filterParams = data;
    this.observableResponse = this.getRequiredListData('', '', data);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.requisitionFilterForm.reset();
    this.filterParams=null;
    this.observableResponse = this.getRequiredListData('', '', null);
    this.showFilterForm = !this.showFilterForm;
  }
}
