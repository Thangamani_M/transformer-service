import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatTabGroup } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { RequisitionAddEdtModel, requisitionRequestEditModel, RequisitionStockModel } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import {
  CrudService, CustomDirectiveConfig, formConfigs, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControlError, ResponseMessageTypes, RxjsService,
  setRequiredValidator, SnackbarService
} from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { RequisitionCreationModalComponent } from './../add-edit-list/requisition-creation-modal.component';
import { StockOrderInterbranchModalComponent } from './../add-edit-list/stock-order-interbranch-modal.component';
import { StockOrderModelComponent } from './stock-order-model.component';

@Component({
  selector: 'app-stock-order-edit-view',
  templateUrl: './stock-order-edit-view.component.html',
  styleUrls: ['./stock-order-edit-view.component.scss']
})
export class StockOrderEditViewComponent implements OnInit {

  approvalId: string;
  requisitionApprovalForm: FormGroup;
  requisitionRequestForm: FormGroup;
  requisitionApprovalFullListForm: FormGroup
  requisitionApprovalDetails: any = {};
  priorityList = [];
  fulldataList = [];
  statusList = [];
  status: any = [];
  stockOrderList = [];
  requisitionId: any;
  isLoading: boolean;
  selectedTabIndex: any = 0;
  selectStatus: any;
  statusName = false;
  requiredLength = false;
  stockCodeEmpty = true;
  stockDescriptionEmpty = true;
  userData: UserLogin;

  filteredStockCodes = [];
  selectedStock = [];
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });

  isStockCodeBlank: boolean = false;
  isStockDescriptionSelected: boolean = false;
  isStockOrderClear: boolean = false;
  stockCodeErrorMessage: any = '';
  showStockCodeError: boolean = false;
  isStockError: boolean = false;
  StockDescErrorMessage: any = '';
  showStockDescError: boolean = false;
  isStockDescError: boolean = false;
  filteredStockDescription: any = [];
  approvalStatusName: any
  selectedIndex: any;
  fullList: FormArray;
  formConfigs = formConfigs;

  @ViewChild('tabGroup', { static: false }) tabGroup: MatTabGroup;
  listOfFiles: any = []

  constructor(
    private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private crudService: CrudService, private httpCancelService: HttpCancelService,
    private snackbarService: SnackbarService, private store: Store<AppState>,
    private dialog: MatDialog,
  ) {
    this.approvalId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }

  // fullListAddEdtModel
  ngOnInit() {
    this.getDropdownList();
    this.createRequisitionApprovalUpdateForm();
    this.createRequisitionApprovalFullUpdateForm();
    this.createrequisitionRequestForm();
    this.requisitionRequestForm.get('rqnStatusId').valueChanges.subscribe(id => {
      let obj = this.statusList.filter(data => {
        if (data.id == id) {
          return true
        }
      })

      if (obj.length > 0) {
        this.approvalStatusName = obj[0].displayName;
        if (obj[0].displayName == 'Cancelled' || obj[0].displayName == 'Rejected') {
          this.requisitionRequestForm.get('reason').setValidators([Validators.required]);
          this.requisitionRequestForm.get('reason').updateValueAndValidity();
          // this.requisitionRequestForm = addFormControls(this.requisitionRequestForm, [ "reason"]);
          // this.requisitionRequestForm = setRequiredValidator(this.requisitionRequestForm, ["reason"]);
        }
        if (obj[0].displayName == 'Approved') {
          this.requisitionRequestForm.get('reason').setErrors(null);
          this.requisitionRequestForm.get('reason').setValue(null);
          this.requisitionRequestForm.get('reason').clearValidators();
          //this.requisitionRequestForm.get('reason').removeFormconrol();
          this.requisitionRequestForm.markAllAsTouched();

          this.requisitionRequestForm.get('reason').updateValueAndValidity();
          this.requisitionRequestForm = removeFormControlError(this.requisitionRequestForm, 'reason');
          //this.requisitionRequestForm = removeFormControls(this.requisitionRequestForm, ["reason"]);
        }
      }
    })

    if (this.approvalId) {
      this.getRequisitionApprovalDetails();


    }

    // To catch changes from interbranch pop up submit
    this.rxjsService.getRequisitionData().subscribe(data => {

      if (this.requisitionApprovalDetails['fullList']) {
        for (const stock of this.requisitionApprovalDetails['fullList']) {
          if (stock.stockId === data['itemId']) {
            stock['ibtOrderQty'] = data['quantity'];
            stock['reasonForDiscrepancy'] = data['discrepancyReason'];
          }
        }
      }


    });
    // description
    this.requisitionApprovalForm.get('stockDescription').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        if ((searchText != null)) {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;

          if (this.isStockDescError == false) {
            this.StockDescErrorMessage = '';
            this.showStockDescError = false;
          }
          else {
            this.showStockDescError = false;
          }
          if (this.isStockDescriptionSelected == false) {
            this.requisitionApprovalForm.controls.stockId.patchValue(null);
            this.requisitionApprovalForm.controls.requestedQty.patchValue(null);
          }
          else {
            this.isStockDescriptionSelected = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.StockDescErrorMessage = '';
              this.showStockDescError = false;
              this.isStockDescError = false;
            }
            else {
              this.StockDescErrorMessage = 'Stock description is not available in the system';
              this.showStockDescError = true;
              this.isStockDescError = true;
            }
            // this.snackbarService.openSnackbar('Stock description is not available in the system', ResponseMessageTypes.ERROR);
            return this.filteredStockDescription = [];
          } else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_NAME, null, true,
              prepareGetRequestHttpParams(null, null, { searchText, isAll: false }))
          }
        } else {
          return this.filteredStockDescription = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockDescription = response.resources;
          this.isStockDescriptionSelected = false;
          this.StockDescErrorMessage = '';
          this.showStockDescError = false;
          this.isStockDescError = false;
        } else {
          this.filteredStockDescription = [];
          this.isStockDescriptionSelected = false;
          this.StockDescErrorMessage = 'Stock description is not available in the system';
          this.showStockDescError = true;
          this.isStockDescError = true;
          // this.snackbarService.openSnackbar('Stock description is not available in the system', ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    // Stock code
    this.requisitionApprovalForm.get('stockId').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {

        this.isStockDescError = false
        this.StockDescErrorMessage = '';
        this.showStockDescError = false;

        if (this.isStockError == false) {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
        }
        else {
          this.isStockError = false;
        }

        if (searchText != null) {
          if (this.isStockCodeBlank == false) {
            this.requisitionApprovalForm.controls.requestedQty.patchValue(null);
            this.requisitionApprovalForm.controls.stockDescription.patchValue(null);
          }
          else {
            this.isStockCodeBlank = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.stockCodeErrorMessage = '';
              this.showStockCodeError = false;
              this.isStockError = false;
            }
            else {
              this.stockCodeErrorMessage = 'Stock code is not available in the system';
              this.showStockCodeError = true;
              this.isStockError = true;
            }
            // this.snackbarService.openSnackbar('Stock code is not available in the system', ResponseMessageTypes.ERROR);
            return this.filteredStockCodes = [];
          } else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, true,
              prepareGetRequestHttpParams(null, null, { searchText, isAll: false }))
          }
        }
        else {
          return this.filteredStockCodes = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockCodes = response.resources;
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;

        } else {

          this.filteredStockCodes = [];
          this.stockCodeErrorMessage = 'Stock code is not available in the system';
          this.showStockCodeError = true;
          this.isStockError = true;
          // this.snackbarService.openSnackbar('Stock code is not available in the system', ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  inputChangeStockFilter(): void {

    // To catch changes inside stock code
    this.requisitionApprovalForm.get('stockId').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      // switchMap(searchText => {
      //   if (!searchText) {
      //     this.stockCodeEmpty = true;
      //     return this.filteredStockCodes = [];
      //   }
      //   else {
      //     this.stockCodeEmpty = false;
      //     return this.crudService.get(
      //       ModulesBasedApiSuffix.INVENTORY,
      //       InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, true,
      //       prepareGetRequestHttpParams(null, null, { searchText, isAll: false }))
      //   }
      // })).subscribe( (response: IApplicationResponse) =>{
      //   if (response.isSuccess && response.resources.length > 0) {
      //     this.filteredStockCodes = response.resources;
      //     this.stockCodeEmpty = false;
      //   }
      //   else {
      //     this.filteredStockCodes = [];
      //     this.stockCodeEmpty = true;
      //   }
      //   this.rxjsService.setGlobalLoaderProperty(false);
      // });
      switchMap(searchText => {

        this.isStockDescError = false
        this.StockDescErrorMessage = '';
        this.showStockDescError = false;

        if (this.isStockError == false) {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
        }
        else {
          this.isStockError = false;
        }

        if (searchText != null) {
          if (this.isStockCodeBlank == false) {
            this.requisitionApprovalForm.controls.requestedQty.patchValue(null);
            this.requisitionApprovalForm.controls.stockDescription.patchValue(null);
          }
          else {
            this.isStockCodeBlank = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.stockCodeErrorMessage = '';
              this.showStockCodeError = false;
              this.isStockError = false;
            }
            else {
              this.stockCodeErrorMessage = 'Stock code is not available in the system';
              this.showStockCodeError = true;
              this.isStockError = true;
            }
            // this.snackbarService.openSnackbar('Stock code is not available in the system', ResponseMessageTypes.ERROR);
            return this.filteredStockCodes = [];
          } else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, true,
              prepareGetRequestHttpParams(null, null, { searchText, isAll: false }))
          }
        }
        else {
          return this.filteredStockCodes = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockCodes = response.resources;
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;

        } else {

          this.filteredStockCodes = [];
          this.stockCodeErrorMessage = 'Stock code is not available in the system';
          this.showStockCodeError = true;
          this.isStockError = true;
          // this.snackbarService.openSnackbar('Stock code is not available in the system', ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

  }

  inputChangeDescpFilter(value: string) {
    // To catch changes inside stock description
    this.requisitionApprovalForm.get('stockDescription').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),


      switchMap(searchText => {
        if ((searchText != null)) {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;

          if (this.isStockDescError == false) {
            this.StockDescErrorMessage = '';
            this.showStockDescError = false;
          }
          else {
            this.showStockDescError = false;
          }
          if (this.isStockDescriptionSelected == false) {
            this.requisitionApprovalForm.controls.stockId.patchValue(null);
            this.requisitionApprovalForm.controls.requestedQty.patchValue(null);
          }
          else {
            this.isStockDescriptionSelected = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.StockDescErrorMessage = '';
              this.showStockDescError = false;
              this.isStockDescError = false;
            }
            else {
              this.StockDescErrorMessage = 'Stock description is not available in the system';
              this.showStockDescError = true;
              this.isStockDescError = true;
            }
            // this.snackbarService.openSnackbar('Stock description is not available in the system', ResponseMessageTypes.ERROR);
            return this.filteredStockDescription = [];
          } else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_NAME, null, true,
              prepareGetRequestHttpParams(null, null, { searchText, isAll: false }))
          }
        } else {
          return this.filteredStockDescription = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockDescription = response.resources;
          this.isStockDescriptionSelected = false;
          this.StockDescErrorMessage = '';
          this.showStockDescError = false;
          this.isStockDescError = false;
        } else {
          this.filteredStockDescription = [];
          this.isStockDescriptionSelected = false;
          this.StockDescErrorMessage = 'Stock description is not available in the system';
          this.showStockDescError = true;
          this.isStockDescError = true;
          // this.snackbarService.openSnackbar('Stock description is not available in the system', ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getDropdownList() {

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PRIORITY, null, null).subscribe((response: IApplicationResponse) => {
      this.priorityList = response.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    });

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RQNSTATUS, null, null).subscribe((response: IApplicationResponse) => {
      let status = response.resources;
      this.statusList = status.filter(x => x.displayName === 'Approved' || x.displayName === 'Cancelled' ||
        x.displayName === 'Pending' || x.displayName === 'Rejected');
      this.rxjsService.setGlobalLoaderProperty(false);
    });

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCKORDERS_DROPDOWN, null, null).subscribe((response: IApplicationResponse) => {
      //this.stockOrderList = response.resources;
      let respObj = response.resources;
      for (let i = 0; i < respObj.length; i++) {
        let temp = {};
        temp['display'] = respObj[i].displayName;
        temp['value'] = respObj[i].id;
        this.stockOrderList.push(temp);
      }
      if (this.stockOrderList.length) {
        if (this.approvalId && this.requisitionApprovalDetails['stockOrderIds']) {
          this.selectedStock = this.requisitionApprovalDetails['stockOrderIds'].toLowerCase().split(',');

        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });

  }

  createRequisitionApprovalFullUpdateForm(): void {
    this.requisitionApprovalFullListForm = this.formBuilder.group({
      fullList: this.formBuilder.array([])
    });
  }

  createRequisitionApprovalUpdateForm(): void {
    let requisitionAddEdtModel = new RequisitionAddEdtModel();
    this.requisitionApprovalForm = this.formBuilder.group({});
    Object.keys(requisitionAddEdtModel).forEach((key) => {
      this.requisitionApprovalForm.addControl(key, new FormControl(requisitionAddEdtModel[key]));
    });
    this.requisitionApprovalForm = setRequiredValidator(this.requisitionApprovalForm, ["priorityId"]);
    //this.requisitionApprovalForm.get('stockOrderIds').disable();
    this.requisitionApprovalForm.controls['stockOrderIds'].disable();
  }


  get getFullListGroupFormArray(): FormArray {
    if (this.requisitionApprovalFullListForm !== undefined) {
      return this.requisitionApprovalFullListForm.get("fullList") as FormArray;
    }
  }

  /* Create FormArray controls */
  createFullListtGroupForm(fullListDetails?: RequisitionStockModel): FormGroup {
    let structureTypeData = new RequisitionStockModel(fullListDetails ? fullListDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: fullListDetails && (key == '') && structureTypeData[key] !== '' ? true : false },
      (key === 'requiredQty' ? [Validators.min(1), Validators.required] : [])]
    });
    let formContorlBuilder = this.formBuilder.group(formControls)
    formContorlBuilder.get('requiredQty').valueChanges.subscribe(requiredQuantity => {
      formContorlBuilder.get('requisitionQty').setValue((+requiredQuantity) - (+formContorlBuilder.get('ibtOrderQty').value));
      formContorlBuilder.get('subTotal').setValue((+requiredQuantity) * (+formContorlBuilder.get('costPrice').value));
    });

    return this.formBuilder.group(formControls);
  }

  getRequisitionApprovalDetails(): void {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.REQUISITION_GET_DETAILS, this.approvalId)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.requisitionApprovalDetails = response.resources;
          let dataResult = response.resources;
          this.fulldataList = response.resources.fullList;
          this.requisitionId = response.resources.requisitionId;

          this.listOfFiles = []
          if (this.requisitionApprovalDetails['supplierDocuments'].length > 0) {
            this.requisitionApprovalDetails['supplierDocuments'].forEach(element => {
              this.listOfFiles.push({ id: element.requisitionSupplierDocumentId, name: element.docName })
            });
          }
          this.rxjsService.setGlobalLoaderProperty(false);

          // this.fullList = response.resources.fullList;
          this.fullList = this.getFullListGroupFormArray;
          if (response.resources.fullList.length >= 0) {
            response.resources.fullList.forEach(element => {
              this.fullList.push(this.createFullListtGroupForm(element));
            });
          }

          if (response.resources.stockOrderIds) {
            this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCKORDERS_DROPDOWN, null, null).subscribe((response: IApplicationResponse) => {
              //this.stockOrderList = response.resources;
              let respObj = response.resources;
              for (let i = 0; i < respObj.length; i++) {
                let temp = {};
                temp['display'] = respObj[i].displayName;
                temp['value'] = respObj[i].id;
                this.stockOrderList.push(temp);
              }
              if (this.stockOrderList.length) {
                if (this.approvalId && this.requisitionApprovalDetails['stockOrderIds']) {
                  this.selectedStock = this.requisitionApprovalDetails['stockOrderIds'].toLowerCase().split(',');

                }
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            });
          }

          /* requisitionApprovalForm */
          let requisitionModels = new RequisitionAddEdtModel(response.resources);
          this.requisitionApprovalForm.patchValue(requisitionModels);
          this.requisitionApprovalForm.patchValue(response.resources);
          this.stockCodeEmpty = false;
          this.stockDescriptionEmpty = false;

          /*requisitionRequestForm*/
          let requestModels = new requisitionRequestEditModel(response.resources);
          this.requisitionRequestForm.patchValue(requestModels);
          this.requisitionRequestForm.patchValue(response.resources);
          if (this.statusList.length) {
            this.requisitionRequestForm.get('rqnStatusId').setValue(this.statusList[1].id);
          }
        }
      });
  }

  createrequisitionRequestForm(): void {
    let purchase = new requisitionRequestEditModel();
    this.requisitionRequestForm = this.formBuilder.group({});
    Object.keys(purchase).forEach((key) => {
      this.requisitionRequestForm.addControl(key, new FormControl(purchase[key]));
    });
    this.requisitionRequestForm = setRequiredValidator(this.requisitionRequestForm, ['rqnStatusId']);
  }

  // ngAfterViewInit(){    
  //   if (!this.approvalId){
  //     this.tabGroup._tabs['_results'][1].disabled = true;
  //     this.tabGroup._tabs['_results'][2].disabled = true;
  //   }
  // }
  uploadFiles(files) {

  }
  onTabClicked(tabIndex) {
    this.selectedTabIndex = tabIndex.index;
  }

  openPopupStockDetails(data) {
    this.dialog.open(StockOrderModelComponent, {
      data: {
        stockCode: data
      }
    });
  }

  navigateToIBTRequestView(stockOrderId, interbranchTransfer) {
    if (interbranchTransfer['ibtrqNumber'] != '-') {
      this.router.navigate(['/inventory', 'inter-branch', 'inter-branch-transfer', 'manager-view'], {
        queryParams: {
          interBranchStockOrderId: stockOrderId,
          isCurrentBranchRequest: false
        }
      });
    }
  }

  openInterBranchModal(ItemId, RequiredQty) {
    const dialogReff = this.dialog.open(StockOrderInterbranchModalComponent, {
      width: '600px',
      data: {
        ItemId,
        WarehouseId: this.requisitionApprovalForm.get('warehouseId').value,
        RequisitionId: this.requisitionApprovalForm.get('requisitionId').value,
        loggedInWarehouseId: this.userData.warehouseId,
        StockOrderTypeId: this.requisitionApprovalForm.get('stockOrderTypeId').value,
        PriorityId: this.requisitionApprovalForm.get('priorityId').value,
        // OrderPriority: this.priorityTypeRef.nativeElement.selectedOptions[0].text.trim().toLowerCase(),
        RQNStatusId: '92BCB72A-BCA5-4A55-B673-5AFF5C38B2AE',
        InterBranchOrderStatus: 'New',
        PONumber: '',
        CreatedUserId: this.userData.userId,
        RequiredQty,
      }, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      ;
      if (result) return;
      this.dialog.closeAll();
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  deleteStockNumbers(stockId, stockOrderIds, stockCode, index?, isActiveNew?) {
    // Remove stock codes from table array when inserted at very 1st time on create page
    if (Object.keys(this.requisitionApprovalDetails).length < 1) {
      // this.fulldataList[index].removeAt(index);
      this.getFullListGroupFormArray.value(index).removeAt(index);
      return;
    }

    // Delete pop up
    const dialogRemoveStockCode = this.dialog.open(RequisitionCreationModalComponent, {
      width: '450px',
      data: {
        message: `Are you sure you want to<br />remove the stock code: ${stockCode} ?`,
        requisitionId: this.requisitionId,
        buttons: {
          cancel: 'No',
          create: 'Yes'
        }
      }, disableClose: true
    });

    dialogRemoveStockCode.afterClosed().subscribe(result => {
      if (!result) return;
      let data = Object.assign({},
        this.requisitionApprovalForm.get('requisitionId').value == null ? null : { requisitionId: this.requisitionApprovalForm.get('requisitionId').value },
        stockId == null ? null : { stockId: stockId },
        stockOrderIds == null ? null : { stockOrderIds: stockOrderIds },
        this.userData.userId == null ? null : { modifiedUserId: this.userData.userId }
      );
      const options = {
        body: data
      };
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.crudService.deleteByParams(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ADD_REQUISITION_ITEM,
        options).subscribe((response) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.requisitionApprovalDetails['fullList'].splice(index, 1);
            // this.getFullListGroupFormArray.controls.splice(index, 1);
            this.getFullListGroupFormArray.removeAt(index);
            if (!isActiveNew) {
              this.requisitionApprovalDetails.requisitionRequestDetails.splice(this.requisitionApprovalDetails['requisitionRequestDetails'].
                findIndex(el => el.stockCode == stockCode), 1);
            }
            // this.getRequisitionApprovalDetails();
          }
        })
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  onStockCodeSelected(items, type) {
    let stockItemData;
    if (type === 'stockCode') {
      stockItemData = this.getFullListGroupFormArray.value.find(stock => stock.stockCode === items.displayName);
      if (!stockItemData) {
        this.isStockDescriptionSelected = true;
        this.crudService.get(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.ITEM,
          items.id,
          false,
          prepareRequiredHttpParams({})
        ).subscribe((response) => {
          if (response.statusCode == 200) {
            this.requisitionApprovalForm.controls.stockDescription.patchValue(response.resources.itemName);
          }
        });
      }
      else {
        this.requisitionApprovalForm.controls.stockId.patchValue(null);
        this.requisitionApprovalForm.controls.stockDescription.patchValue(null);
        this.isStockError = true;
        this.stockCodeErrorMessage = 'Stock code is already present';
        this.showStockCodeError = true;
      }
    }
    else if (type === 'stockDescription') {
      stockItemData = this.getFullListGroupFormArray.value.find(stock => stock.stockDescription === items.displayName);
      if (!stockItemData) {
        this.isStockCodeBlank = (this.requisitionApprovalForm.controls.stockId.value == null || this.requisitionApprovalForm.controls.stockId.value == '') ? true : false;
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM,
          items.id).subscribe((response) => {
            if (response.statusCode == 200) {
              this.filteredStockCodes = [{
                id: response.resources.itemId,
                displayName: response.resources.itemCode
              }];
              this.requisitionApprovalForm.controls.stockId.patchValue(response.resources.itemCode);
            }
          })
      }
      else {
        this.requisitionApprovalForm.controls.stockId.patchValue(null);
        this.requisitionApprovalForm.controls.stockDescription.patchValue(null);
        this.isStockDescError = true;
        this.StockDescErrorMessage = 'Stock Description is already present';
        this.showStockDescError = true;
      }
    }
  }

  //Validate Requested quantity
  ValidateReqQty(ReqQty) {

    if (parseInt(ReqQty) == 0) {
      this.snackbarService.openSnackbar("Please enter valid Request Qty", ResponseMessageTypes.WARNING)
      // this.isButtondisabled = true;
    } else {
      // this.isButtondisabled = false;

    }
  }

  //Numeric only function
  numericOnly(event): boolean {
    let patt = /^([0-9])$/;
    let result = patt.test(event.key);
    return result;
  }


  addStockCodeTable() {

    let stockID = this.filteredStockCodes[0].id;
    let warehouseID = this.requisitionApprovalForm.value.warehouseId;
    let getData;

    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.REQUISITION_SEARCH_ITEM,
      undefined,
      false,
      prepareRequiredHttpParams({
        warehouseId: warehouseID,
        stockId: stockID
      })
    )
      .subscribe(response => {
        if (response.resources != null) {
          response.resources.requestedQty = this.requisitionApprovalForm.value.requestedQty ? this.requisitionApprovalForm.value.requestedQty : response.resources.requestedQty;
          response.resources.requiredQty = response.resources.requestedQty;
          response.resources.subTotal = response.resources.requiredQty * response.resources.costPrice;
          response.resources.isActiveNew = true;
          getData = response.resources;


          this.fullList = this.getFullListGroupFormArray;
          this.fullList.insert(0, this.createFullListtGroupForm(getData));

          this.requisitionApprovalForm.controls.stockId.patchValue('');
          this.requisitionApprovalForm.controls.stockDescription.patchValue('');
          this.requisitionApprovalForm.controls.requestedQty.patchValue('');
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.requisitionApprovalForm.controls.stockId.patchValue('');
          this.requisitionApprovalForm.controls.stockDescription.patchValue('');
          this.requisitionApprovalForm.controls.requestedQty.patchValue('');
          this.rxjsService.setGlobalLoaderProperty(false);
          return;
        }

      });

  }

  changeRequestFilter(index, reqvalue) {

    this.getFullListGroupFormArray.controls[index].get('requisitionQty').patchValue(reqvalue);
  }

  onUpdateRequest(): void {

    // Form submitt No changes snackbar prevent
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.requisitionRequestForm.invalid) { return }
    let status = this.statusList.find(sta => sta.id == this.requisitionRequestForm.get('rqnStatusId').value)
    if (status.displayName == 'Cancelled') {
      const dialogRemoveStockCode = this.dialog.open(RequisitionCreationModalComponent, {
        width: '450px',
        data: {
          message: `Are you sure you want to<br />cancel the requisition id: 
          ${this.requisitionApprovalForm.get('requisitionNumber').value} ?`,
          buttons: {
            create: 'Ok',
            cancel: 'Cancel',
          }
        }, disableClose: true
      });
      dialogRemoveStockCode.afterClosed().subscribe(result => {
        if (result) {
          this.onSubmitfullList();
        }
      });
    }
    else { this.onSubmitfullList(); }
  }

  onSubmitfullList(): void {

    if (this.requisitionApprovalForm.invalid || this.getFullListGroupFormArray.invalid) {
      this.requisitionApprovalForm.get('stockOrderIds').markAllAsTouched();
      return;
    };

    this.getFullListGroupFormArray.value.forEach((value, index) => {
      value.requestedQty = Number(value.requestedQty);
      value.requiredQty = Number(value.requiredQty);
      value.RequestedQuantity = value.requestedQty;
      value.RequiredQuantity = value.requiredQty;
      value.IsApproved = true;
      // value.RQNStatusId = this.requisitionRequestForm.get('rqnStatusId').value;
      // value.Reason = this.requisitionRequestForm.get('reason').value;
    });

    const listData = {
      ...this.requisitionApprovalForm.value, Items: this.getFullListGroupFormArray.value,
      createdUserId: this.userData.userId,
      stockOrderIds: this.requisitionApprovalForm.value.stockOrderId,
      locationId: this.requisitionApprovalDetails.locationId,
      supplierId: this.requisitionApprovalDetails.supplierId,
      motivation: this.requisitionApprovalDetails.motivation,
    }

    var formData = new FormData();
    formData.append('json', JSON.stringify(listData));
    // this.fileList.forEach(file => {
    //   formData.append('file', file);
    // });

    // return;
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.REQUISITION_LIST, formData)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources != null) {
        // this.selectedTabIndex = 1;
        // // this.requisitionApprovalFullListForm.reset();
        // // this.getFullListGroupFormArray.reset();
        // this.fullList.clear();
        // this.getRequisitionApprovalDetails();
        this.listOfFiles = []

        this.rxjsService.setGlobalLoaderProperty(false);
        this.onSubmitRequisitionRequest();
      }
      // if (response.isSuccess && response.statusCode === 200 && response.resources != null) {
      //   this.router.navigate(['/inventory/requisitions/approvals']);
      // }
      // else {
      //   this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
      //   return;
      // }
    });

  }

  onSubmitInterBranchTransfer(): void {
    this.selectedTabIndex = 2;
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onSubmitRequisitionRequest(): void {

    this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent

    if (this.requisitionRequestForm.invalid) { return }

    let status = this.statusList.find(sta => sta.id == this.requisitionRequestForm.get('rqnStatusId').value)

    const requestData = {
      ...this.requisitionRequestForm.value, requisitionId: this.requisitionId,
      approvalId: this.userData.userId, modifiedUserId: this.userData.userId,
      action: status.displayName,
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.REQUISITION_GET_DETAILS, requestData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.router.navigate(['/inventory/requisitions/approvals']);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        return;
      }
    });
  }

  onUpdatefullList(): void {
    if (this.requisitionApprovalForm.invalid || this.getFullListGroupFormArray.invalid) {
      this.requisitionApprovalForm.get('stockOrderIds').markAllAsTouched();
      return;
    };
    this.selectedTabIndex = 1;
  }

  navigateToList() {
    this.router.navigate(["/inventory/requisitions/approvals"], { skipLocationChange: true });
  }

  navigateToEdit() {
    // if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
    //   return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    //  }
    // this.router.navigate(['/inventory/requisitions/approvals/update']);
    this.router.navigate(['/inventory/requisitions/approvals/view'], {
      queryParams: {
        id: this.approvalId
      }, skipLocationChange: true
    });
  }

}
