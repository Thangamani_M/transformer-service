import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { stockOrderViewModel } from '@app/modules/inventory/models';
import { RxjsService } from '@app/shared';

@Component({
  selector: 'app-stock-order-model',
  templateUrl: './stock-order-model.component.html',
  styleUrls: ['./stock-order-model.component.scss']
})

export class StockOrderModelComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: stockOrderViewModel,
    private rxjsService: RxjsService) { }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);

  }
  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
