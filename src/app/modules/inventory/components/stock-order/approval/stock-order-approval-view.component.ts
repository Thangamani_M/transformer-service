import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudType } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { fileUrlDownload, ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { map } from 'rxjs/operators';
import { StockOrderModelComponent } from './stock-order-model.component';


@Component({
  selector: 'app-stock-order-approval-view',
  templateUrl: './stock-order-approval-view.component.html',
  styleUrls: ['./stock-order-approval-view.component.scss']
})

export class StockOrderApprovalViewComponent implements OnInit {

  approvalId: string;
  requisitionApprovalDetails: any = {};
  selectedTabIndex = 0;
  primengTableConfigProperties: any;
  StockOrderapprovalview: any;
  supplierDocuments: any = []
  fullListSubTotal: number = 0;
  requestType;
  constructor(
    private router: Router, private dialog: MatDialog, private datePipe: DatePipe,
    private crudService: CrudService, private rxjsService: RxjsService, private activatedRoute: ActivatedRoute,
  ) {
    this.approvalId = this.activatedRoute.snapshot.queryParams.id;
    this.requestType = this.activatedRoute.snapshot.queryParams.type;
    this.primengTableConfigProperties = {
      tableCaption: "View Requisition Approval",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Ordering', relativeRouterUrl: '' }, { displayName: 'Requisition Approval List', relativeRouterUrl: '/inventory/requisitions/approvals' },
      { displayName: 'View Requisition Approval', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            // enableAction: true,
            enableViewBtn: false,
            enableClearfix: true,
          }
        ]
      }
    }
    this.StockOrderapprovalview = [
      { name: 'Requisition ID', value: '' },
      { name: 'Order Type', value: '' },
      { name: 'Requisition Date', value: '' },
      { name: 'Stock Order No', value: '' },
      { name: 'Warehouse', value: '' },
      { name: 'Priority', value: '' },
      { name: 'Actioned By', value: '' },
      { name: 'Actioned Date', value: '' },
    ]
  }

  ngOnInit() {
    if (this.approvalId) {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.REQUISITION_GET_DETAILS, this.approvalId, false, null)
        .pipe(map((res: IApplicationResponse) => {
          if (res?.isSuccess == true && res?.resources && res?.statusCode === 200) {
            if (res?.resources?.length > 0) {
              res?.resources?.forEach(val => {
                val.contractCost = val.contractCost ? val.contractCost.toFixed() : '0.00';
                val.costPrice = val.costPrice ? val.costPrice.toFixed() : '0.00';
                val.subTotal = val.subTotal ? val.subTotal.toFixed() : '0.00';
                // val.createdDate = val.createdDate ? this.datePipe.transform(val.createdDate, 'dd-MM-yyyy h:mm a') : '';
                return val;
              })
            }
          }
          return res;
        }))
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.requisitionApprovalDetails = response.resources;
            this.supplierDocuments = response?.resources?.supplierDocuments ? response?.resources?.supplierDocuments : []
            this.StockOrderapprovalview = [
              { name: 'Requisition ID', value: response ? response?.resources?.requisitionNumber : '', order: 1 },
              { name: 'Order Type', value: response ? response?.resources?.orderType : '', order: 2 },
              { name: 'Requisition Date', value: response ? response?.resources.requisitionDate : '', order: 3 },
              { name: 'Stock Order No', value: response ? response?.resources?.stockOrderNumbers : '', order: 4 },
              { name: 'Warehouse', value: response ? response?.resources?.warehouseName : '', order: 5 },
              { name: 'Storage Location', value: response ? response?.resources?.storageLocationName : '', order: 6 },
              { name: 'Priority', value: response ? response?.resources?.priority : '', order: 7 },
              { name: 'Actioned By', value: response ? response?.resources?.actionedBy : '', order: 8 },
              { name: 'Motivation', value: response ? response?.resources?.motivation : '', order: 9 },
              { name: 'Actioned Date', value: response ? response?.resources?.actionedDate : '', order: 10 },
              // { name: 'Supplier', value: response ? response?.resources?.supplierName : '', order: 11 },
              // { name: 'Supporting Document', value: response?.resources ? supplierDocumentsStings : '', order: 12 }
            ]
            if (response.resources?.status != 'Approved' && response.resources?.status != 'Save as Draft' && response.resources?.status != 'New' && response.resources?.status != 'Saved' && response.resources?.status != 'Cancelled') {
              this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
            }
            if (response.resources.fullList.length > 0) {
              this.fullListSubTotal = 0;
              response.resources.fullList.forEach(datas => {
                this.fullListSubTotal += datas.subTotal;
              });
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEditPage();
        break;
    }
  }

  navigateToIBTRequestView(stockOrderId, interbranchTransfer) {
    if (interbranchTransfer['ibtrqNumber'] != '-') {
      this.router.navigate(['/inventory', 'inter-branch', 'inter-branch-transfer', 'manager-view'], {
        queryParams: {
          interBranchStockOrderId: stockOrderId,
          isCurrentBranchRequest: false
        }
      });
    }
  }

  navigateToPurchaseOrder(purchaseOrderId) {
    if (purchaseOrderId == null) return;
    this.router.navigate(["inventory/purchase-order/view"], {
      queryParams: {
        id: purchaseOrderId,
      }, skipLocationChange: true
    });
  }

  navigateToEditPage() {
    this.router.navigate(['/inventory/requisitions/approvals/update'], {
      queryParams: {
        id: this.approvalId
      }, skipLocationChange: true
    });
  }

  onTabClicked(tab): void {

  }

  addRequisitionQty(event) {

  }
  navigateToDetailedList() {
    this.router.navigate(['/inventory', 'requisitions', 'stock-orders', 'detailed-list'], {
      queryParams: {
        requisitionId: this.approvalId, view: false, type: this.requestType
      }
    });
  }
  openPopupStockDetails(data) {
    this.dialog.open(StockOrderModelComponent, {
      data: {
        stockCode: data
      }
    });
  }
  downloadFile(url) {
    fileUrlDownload(url, true)
  }
}
