import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-requisition-creation-modal',
  templateUrl: './requisition-creation-modal.component.html',
  styleUrls: ['./requisition-modal.component.scss']
})
export class RequisitionCreationModalComponent implements OnInit {

  interbranchRequestForm: FormGroup;
  isDiscrepancy = false;
  totalSelectedQty = 0;

  constructor(@Inject(MAT_DIALOG_DATA) public popupData: any) { }

  ngOnInit(): void {
    let messageSpan = document.createElement('span');
    messageSpan.innerHTML = this.popupData['message'];
    document.getElementById('parentContainer').appendChild(messageSpan);
  }
  close() {

    // this.popupData.close(true);
  }
}
