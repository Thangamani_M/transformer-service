import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, ResponseMessageTypes, SnackbarService, currentComponentPageBasedPermissionsSelector$ } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { fileUrlDownload, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { StockInfoModalComponent } from './stock-info-modal.component';
import { combineLatest, forkJoin } from 'rxjs';
@Component({
    selector: 'app-stock-order-view',
    templateUrl: './stock-order-view.component.html',
    styleUrls: ['./stock-order-view.component.scss'],
})
export class StockOrderViewComponent implements OnInit {
    loggedUser: UserLogin;
    requisitionId: string;
    stockOrderRequisitionViewModel: any = {};
    selectedTabIndex = 0;
    primengTableConfigProperties: any;
    stockOrderView: any;
    fromValue: Number = 0;
    toValue: Number = 0;
    requestType: string = '';
    userId: string = '';
    referenceId: string = '';
    fullListSubTotal: number = 0;
    requestListSubTotal: number = 0;
    supplierDocuments: any = [];
    sapDetails?: any = {};
    loggedInUserData;
    constructor(
        private router: Router, private activatedRoute: ActivatedRoute, private crudService: CrudService,
        private rxjsService: RxjsService, private store: Store<AppState>, private dialog: MatDialog,
        private snackbarService: SnackbarService
    ) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });

        this.activatedRoute.queryParams.subscribe(params => {

            this.requisitionId = params.requisitionId;
            this.fromValue = Number(params.fromValue);
            this.toValue = Number(params.toValue);

            this.requestType = params.type;
            this.userId = params.userId;
            this.referenceId = params.referenceId;

            // this.requisitionId = this.activatedRoute.snapshot.queryParams.requisitionId;
            // this.fromValue = Number(this.activatedRoute.snapshot.queryParams.fromValue);
            // this.toValue = Number(this.activatedRoute.snapshot.queryParams.toValue);
            // this.requestType = this.activatedRoute.snapshot.queryParams.type;
            // this.userId = this.activatedRoute.snapshot.queryParams.userId;
            // this.referenceId = this.activatedRoute.snapshot.queryParams.referenceId;

            let requestUrl = this.requestType == 'requisition-list' ? '/inventory/requisitions/stock-orders' : '/my-tasks/inventory-task-list';
            let requestUrlName = this.requestType == 'requisition-list' ? 'Requisition' : 'Inventory Task List';

            this.primengTableConfigProperties = {
                tableCaption: "View Requisition Request",
                breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Ordering', relativeRouterUrl: '' },
                { displayName: requestUrlName, relativeRouterUrl: requestUrl },
                { displayName: 'View Requisition', relativeRouterUrl: '' }],
                selectedTabIndex: 0,
                tableComponentConfigs: {
                    tabsList: [{
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableViewBtn: false,
                        enableClearfix: true,
                    }]
                }
            }
            this.onViewPage();
            this.getDetailsByRequisitionsId();
        });
    }

    ngOnInit() {
        // this.getDetailsByRequisitionsId();
        this.combineLatestNgrxStoreData();
    }
    combineLatestNgrxStoreData() {
        combineLatest([
            this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {

            this.loggedInUserData = new LoggedInUserModel(response[0]);
            let permission = response[1][INVENTORY_COMPONENT.REQUISITION];
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
            }
        });
    }
    getDetailsByRequisitionsId() {
        if (this.requisitionId) {
            this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.REQUISITION_EDIT, this.requisitionId, false, null)
                .pipe(map((res: IApplicationResponse) => {
                    if (res?.resources) {
                        res?.resources?.items?.forEach(val => {
                            val.contractCost = (val.contractCost) ? val.contractCost.toFixed(2) : '0.00';
                            val.costPrice = (val.costPrice) ? val.costPrice.toFixed(2) : '0.00';
                            val.subTotal = (val.subTotal) ? val.subTotal.toFixed(2) : '0.00';
                            return val;
                        });

                        res?.resources?.requisitionRequests?.forEach(val => {
                            val.contractCost = (val.contractCost) ? val.contractCost.toFixed(2) : '0.00';
                            val.costPrice = (val.costPrice) ? val.costPrice.toFixed(2) : '0.00';
                            val.subTotal = (val.subTotal) ? val.subTotal.toFixed(2) : '0.00';
                            return val;
                        });
                    }
                    return res;
                }))
                .subscribe((response: IApplicationResponse) => {
                    if (response.resources && response.statusCode === 200) {

                        this.stockOrderRequisitionViewModel = response.resources;
                        this.sapDetails = response?.resources['requisitionRequests'][0];
                        let requisition = response.resources.requisitionApproverRanges;
                        if (requisition != null && requisition.length > 0 && response.resources?.rqnStatusName != 'Rejected') {
                            let requisitionApprover = requisition.filter(x => x.userId?.toLowerCase() == this.userId?.toLowerCase());
                            if (requisitionApprover.length > 0) {
                                this.fromValue = requisitionApprover[0].fromRange;
                                this.toValue = requisitionApprover[0].toRange;
                            }
                        }
                        else {
                            if (response.resources?.rqnStatusName == 'Save as Draft' || response.resources?.rqnStatusName == 'New' || response.resources?.rqnStatusName == 'Pending Approval') {
                            } else {
                                this.fromValue = 0;
                                this.toValue = 0;
                            }


                        }

                        this.onViewPage(this.stockOrderRequisitionViewModel);
                        if (response.resources?.canApprove || response.resources?.canReject || response.resources?.canCancel || response.resources?.canEdit || response.resources?.canRework) {
                            this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
                        }
                        // else {
                        //     this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;  
                        // }
                        // if (this.requestType == 'inventory-task-list' && response.resources?.rqnStatusName != 'Approved' && response.resources?.rqnStatusName != 'Cancelled') {
                        //     this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
                        // }
                        // else if (this.requestType == 'requisition-list' && (response.resources?.rqnStatusName == 'Pending Approval' || response.resources?.rqnStatusName == 'New' ||
                        //     response.resources?.rqnStatusName == 'Save as Draft' || response.resources?.rqnStatusName == 'Rejected')) {
                        //     this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
                        // }

                        // else {
                        // this.tabGroup._tabs['_results'][1].disabled = true;
                        //  }
                        if (response.resources.items.length > 0) {
                            this.fullListSubTotal = 0;
                            response.resources.items.forEach(datas => {
                                this.fullListSubTotal += parseFloat(datas.subTotal.toString());
                            });
                        }

                        if (response.resources.requisitionRequests.length > 0) {
                            this.requestListSubTotal = 0;
                            response.resources.requisitionRequests.forEach(datas => {
                                this.requestListSubTotal += parseFloat(datas.subTotal.toString());
                            });
                        }
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                    else {
                        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                });
        }
    }

    onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
        switch (type) {
            case CrudType.EDIT:
                if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0]?.canEdit) {
                    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                }
                this.navigateToEdit();
                break;
        }
    }
    onTabClicked(tabIndex) {
        this.selectedTabIndex = tabIndex.index;
    }
    navigateToEdit() {
        this.router.navigate(['/inventory', 'requisitions', 'stock-orders', 'add-edit'], {
            queryParams: {
                requisitionId: this.requisitionId,
                type: this.requestType,
                userId: this.userId,
                referenceId: this.referenceId,
                fromValue: this.fromValue,
                toValue: this.toValue,
            }, skipLocationChange: true
        });
    }

    navigateToIBTRequestView(interBranchStockOrderId, interbranchTransfer) {
        if (interbranchTransfer['ibtrqNumber'] && interbranchTransfer['ibtrqNumber'] != '-') {
            this.router.navigate(['/inventory', 'inter-branch', 'inter-branch-transfer', 'manager-view'], { queryParams: { interBranchStockOrderId: interBranchStockOrderId, isCurrentBranchRequest: false } });
        }
    }

    navigateToPurchaseOrder(purchaseOrderId) {
        if (purchaseOrderId == null) return;
        this.router.navigate(["inventory/purchase-order/view"], {
            queryParams: {
                id: purchaseOrderId,
            }, skipLocationChange: true
        });
    }

    stockInfoModal(interBranchStockDetails) {

        const dialogReff = this.dialog.open(StockInfoModalComponent, {
            width: '600px',

            data: {
                interBranchStockDetails: interBranchStockDetails
            },
            disableClose: true
        });
        dialogReff.afterClosed().subscribe(result => {
            if (result) return;
            this.rxjsService.setDialogOpenProperty(false);
        });
    }

    navigateToDetailedList() {
        this.router.navigate(['/inventory', 'requisitions', 'stock-orders', 'detailed-list'], {
            queryParams:
                { requisitionId: this.requisitionId, view: true, type: this.requestType }
        });
    }

    addRequisitionQty(e) { }

    onViewPage(response?) {
        this.supplierDocuments = response?.supplierDocuments ? response?.supplierDocuments : []
        this.stockOrderView = [
            { name: 'Requisition ID', value: response ? response?.requisitionNumber : '', order: 1 },
            { name: 'Order Type', value: response ? response?.orderType : '', order: 2 },
            { name: 'Requisition Date', value: response ? response.requisitionDate : '', order: 3 },
            { name: 'Stock Order No', value: response ? response?.stockOrderNumbers : '', order: 4 },
            { name: 'Warehouse', value: response ? response?.warehouseName : '', order: 5 },
            { name: 'Storage Location', value: response ? response?.storageLocationName : '', order: 6 },
            { name: 'Priority', value: response ? response?.priority : '', order: 7 },
            { name: 'Actioned By', value: response ? response?.approvalBy : '', order: 8 },
            { name: 'Motivation', value: response ? response?.motivation : '', order: 9 },
            { name: 'Actioned Date', value: response ? response?.approvalDate : '', order: 10 },
            { name: 'Status', value: this.stockOrderRequisitionViewModel ? this.stockOrderRequisitionViewModel?.rqnStatusName : '', statusClass: this.stockOrderRequisitionViewModel?.rqnStatusName != null ? this.stockOrderRequisitionViewModel?.cssClass : '', order: 11 },
            { name: 'Reason', value: this.stockOrderRequisitionViewModel ? this.stockOrderRequisitionViewModel?.reason : '', order: 12 }
        ]
    }
    downloadFile(url) {
        fileUrlDownload(url, true)
    }
}
