import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-stock-info-modal',
  templateUrl: './stock-info-modal.component.html',
  styleUrls: ['./requisition-modal.component.scss']
})
export class StockInfoModalComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public interBranchData) { }

  ngOnInit(): void {
  }

}
