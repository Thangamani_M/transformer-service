import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setMinMaxValidator, setRequiredValidator } from '@app/shared';
import { StockCodeInterBranchRequestModel } from '@modules/inventory/models/RequisitionListCreation.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-stock-order-interbranch-modal',
  templateUrl: './stock-order-interbranch-modal.component.html'
})
export class StockOrderInterbranchModalComponent implements OnInit {

  interbranchRequestForm: FormGroup;
  isDiscrepancy = false;
  isSelectedQtyGreater = false;
  isTotalQtyGreaterThanZero = false;
  selectedQtyGreaterThanRequired = false;
  totalSelectedQty = 0;
  interBranchStockOrderId = null;
  InterBranchStockOrderItemId = null;

  constructor(@Inject(MAT_DIALOG_DATA) public interBranchData: string,
    private dialog: MatDialog,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createInterbranchrequestForm();
    let data = Object.assign({},
      this.interBranchData['ItemId'] == '' ? null : { ItemId: this.interBranchData['ItemId'] },
      this.interBranchData['WarehouseId'] == '' ? null : { WarehouseId: this.interBranchData['WarehouseId'] },
      this.interBranchData['RequisitionId'] == '' ? null : { RequisitionId: this.interBranchData['RequisitionId'] }
    );

    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCKORDERS_OTHERBRANCH,
      undefined,
      false,
      prepareGetRequestHttpParams('', '', {
        search: '',
        ...data
      })
    ).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200) {
        let stockOrderInterbranchModel = response.resources;
        this.interBranchStockOrderId = null;
        this.interbranchRequestForm.get('stockCode').setValue(stockOrderInterbranchModel['itemCode']);
        this.interbranchRequestForm.get('stockDescription').setValue(stockOrderInterbranchModel['stockDescription']);
        this.interbranchRequestForm.get('requiredQty').setValue(this.interBranchData['RequiredQty']);
        this.interbranchRequestForm.get('CreatedUserId').setValue(this.interBranchData['CreatedUserId']);
        this.interbranchRequestForm.get('FromWarehouseId').setValue(this.interBranchData['WarehouseId']);
        this.interbranchRequestForm.get('InterBranchOrderStatus').setValue(this.interBranchData['InterBranchOrderStatus']);
        this.interbranchRequestForm.get('PONumber').setValue(this.interBranchData['PONumber']);
        this.interbranchRequestForm.get('PriorityId').setValue(this.interBranchData['PriorityId']);
        this.interbranchRequestForm.get('OrderPriority').setValue(this.interBranchData['OrderPriority']);
        this.interbranchRequestForm.get('RQNStatusId').setValue(this.interBranchData['RQNStatusId']);
        this.interbranchRequestForm.get('StockOrderTypeId').setValue(this.interBranchData['StockOrderTypeId']);
        this.interbranchRequestForm.get('WarehouseId').setValue(this.interBranchData['loggedInWarehouseId']);
        this.interbranchRequestForm.get('requiredQty').setValue(this.interBranchData['RequiredQty']);
        this.interbranchRequestForm.get('requisitionId').setValue(this.interBranchData['RequisitionId']);
        this.interbranchRequestForm.get('ItemId').setValue(this.interBranchData['ItemId']);
        if (stockOrderInterbranchModel['reasonforDiscrepancy']) {
          this.interbranchRequestForm.get('reasonForDiscrepancy').setValue(stockOrderInterbranchModel['reasonforDiscrepancy']);
          this.isDiscrepancy = true;
        } else {
          this.isDiscrepancy = false;
        }
        let warehouseDetailsArray = this.warehouseDetailsFormArray;
        for (const warehouse of stockOrderInterbranchModel['interBranchWarehouses']) { // warehouses
          if (this.interBranchStockOrderId == null) {
            this.interBranchStockOrderId = warehouse['interBranchStockOrderId'];
          }
          if (this.InterBranchStockOrderItemId == null) {
            this.InterBranchStockOrderItemId = warehouse['interBranchStockOrderItemId'];
          }
          let data = Object.assign({},
            warehouse['interBranchStockOrderId'] == null ? null : { InterBranchStockOrderId: warehouse['interBranchStockOrderId'] },
            warehouse['interBranchStockOrderItemId'] == null ? null : { InterBranchStockOrderItemId: warehouse['interBranchStockOrderItemId'] },
            this.interBranchData['WarehouseId'] == '' ? null : { FromWarehouseId: this.interBranchData['WarehouseId'] },
            warehouse['warehouseId'] == '' ? null : { ToWarehouseId: warehouse['warehouseId'] },
            { Quantity: warehouse['selectedQty'] },
            warehouse['itemId'] == '' ? null : { ItemId: warehouse['itemId'] },
            warehouse['warehouseName'] == '' ? null : { warehouse: warehouse['warehouseName'] },
            { availableQty: warehouse['availableQty'] }
          );
          let warehouseDetailsFormGroup = this.formBuilder.group(data);
          warehouseDetailsFormGroup = setRequiredValidator(warehouseDetailsFormGroup, ['Quantity']);
          warehouseDetailsArray.push(warehouseDetailsFormGroup);
          warehouseDetailsFormGroup = setMinMaxValidator(warehouseDetailsFormGroup, [
            { formControlName: 'Quantity', compareWith: 'availableQty', type: 'minEqual' }
          ]);

        }
        // Code to change selected qty
        this.warehouseDetailsFormArray.controls.forEach((control: FormGroup) => {
          control.get('Quantity').valueChanges.subscribe(selectedQty => {
            this.totalSelectedQty = 0;
            this.warehouseDetailsFormArray.controls.forEach((control, index) => {
              this.totalSelectedQty += (+control.get('Quantity').value);
            })
            if (this.totalSelectedQty != 0 && this.interBranchData['RequiredQty'] > this.totalSelectedQty) {
              this.isDiscrepancy = true;
              this.isTotalQtyGreaterThanZero = false;
              this.interbranchRequestForm.get('reasonForDiscrepancy').setValidators(Validators.required);
              this.isSelectedQtyGreater = false;
            } else if (this.totalSelectedQty != 0 && this.interBranchData['RequiredQty'] < this.totalSelectedQty) {
              this.isDiscrepancy = false;
              this.isTotalQtyGreaterThanZero = false;
              this.interbranchRequestForm.get('reasonForDiscrepancy').clearValidators();
              this.isSelectedQtyGreater = true;
            }
            else if (this.totalSelectedQty != 0) {
              this.isTotalQtyGreaterThanZero = false;
              this.isSelectedQtyGreater = false;
              this.isDiscrepancy = false;
              this.interbranchRequestForm.get('reasonForDiscrepancy').clearValidators();
            } else if (this.totalSelectedQty == 0) {
              this.isTotalQtyGreaterThanZero = true;
              this.isSelectedQtyGreater = false;
              this.isDiscrepancy = false;
              this.interbranchRequestForm.get('reasonForDiscrepancy').clearValidators();
            }
            this.interbranchRequestForm.get('reasonForDiscrepancy').updateValueAndValidity();
          });
        });
        this.rxjsService.setGlobalLoaderProperty(false);
      };
    });
  }

  createInterbranchrequestForm(stockCodeInterBranchRequestModel?: StockCodeInterBranchRequestModel) {
    let interbranchRequestModel = new StockCodeInterBranchRequestModel(stockCodeInterBranchRequestModel);
    this.interbranchRequestForm = this.formBuilder.group({});
    let stockItemsFormArray = this.formBuilder.array([]);
    Object.keys(interbranchRequestModel).forEach((key) => {
      if (typeof interbranchRequestModel[key] !== 'object') {
        this.interbranchRequestForm.addControl(key, new FormControl(interbranchRequestModel[key]));
      } else {
        this.interbranchRequestForm.addControl(key, stockItemsFormArray);
      }
    });
  }

  get warehouseDetailsFormArray(): FormArray {
    if (this.interbranchRequestForm !== undefined) {
      return (<FormArray>this.interbranchRequestForm.get('interBranchWarehouses'));
    }
  }

  createIBTorder() {
    if (this.interbranchRequestForm.invalid) {
      this.interbranchRequestForm.get('reasonForDiscrepancy').markAsTouched();
      return;
    }
    if (this.isSelectedQtyGreater) {
      return;
    }

    if (this.totalSelectedQty == 0) {
      this.isTotalQtyGreaterThanZero = true;
      return;
    } else {
      this.isTotalQtyGreaterThanZero = false;
    }

   

    if (!this.isDiscrepancy) {
      this.interbranchRequestForm.get('reasonForDiscrepancy').setValue('');
    }


    this.rxjsService.setFormChangeDetectionProperty(true);
    const ibtSubmit$ = (this.interBranchStockOrderId != null && this.InterBranchStockOrderItemId != null) ? this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCKORDERS_OTHERBRANCH,
      this.interbranchRequestForm.value
    ) : this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCKORDERS_OTHERBRANCH,
      this.interbranchRequestForm.value
    );

    ibtSubmit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setRequisitionData(response['resources']);
        this.dialog.closeAll();
      }
    })
  }
}
