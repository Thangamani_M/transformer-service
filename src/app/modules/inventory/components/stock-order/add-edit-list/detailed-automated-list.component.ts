import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  ComponentProperties, CrudService, CrudType, debounceTimeForDeepNestedObjectSearchkeyword, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService
} from '@app/shared';
import { PrimengStatusConfirmDialogComponent } from '@app/shared/components/primeng-status-confirm-dialog/primeng-status-confirm-dialog.component';
import { MomentService } from '@app/shared/services/moment.service';
import { RequisitionReOrderingListFilter } from '@modules/inventory';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest, of ,forkJoin} from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-detailed-automated-list',
  templateUrl: './detailed-automated-list.component.html',
  styleUrls: ['./detailed-automated-list.component.scss']
})
export class DetailedAutomatedListComponent implements OnInit {

  @ViewChildren(Table) tables: QueryList<Table>;

  componentProperties = new ComponentProperties();
  observableResponse;
  selectedRows: string[] = [];
  selectedTabIndex = 0;
  requisitionId: string;
  requestType: string;
  redirectTo: boolean;
  dataList: any;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loading = false;
  loggedInUserData: LoggedInUserModel;
  detailedAutomatedListModel = {};
  searchForm: FormGroup;
  primengTableConfigProperties: any;
  requisitionReOrderListFilterForm: FormGroup;
  showReOrderingFilterForm: boolean = false;
  stockCodeDropdown: any = [];
  warehouseDropdown: any = [];
  itemConditionDropdown: any = [];
  stockDescDropdown: any = [];

  status: any = [
    { label:'Yes', value: 'Yes'},
    { label:'No', value: 'No'}
  ];

  constructor(private crudService: CrudService, private dialogService: DialogService,
    private rxjsService: RxjsService, private store: Store<AppState>,
    private router: Router, private formBuilder: FormBuilder,private momentService: MomentService,
    private activatedRoute: ActivatedRoute) {
    this.requisitionId = this.activatedRoute.snapshot.queryParams.requisitionId;
    this.redirectTo = this.activatedRoute.snapshot.queryParams.view;
    this.requestType = this.activatedRoute.snapshot.queryParams.type;

    this.primengTableConfigProperties = {
      tableCaption: "Detailed List",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Requisition', relativeRouterUrl: '/inventory/requisitions/stock-orders' },
      {
        displayName: this.redirectTo ? 'View Requisition' : 'Update Requisition',
        relativeRouterUrl: this.redirectTo ? '/inventory/requisitions/stock-orders/requisition-view' : '/inventory/requisitions/stock-orders/add-edit',
        queryParams: { requisitionId: this.requisitionId }
      },
      { displayName: 'Detailed List' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'RE-ORDERING LIST',
            dataKey: 'fullListId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'stockCode', header: 'Stock Code', width: '120px' },
              { field: 'stockDescription', header: 'Stock Description', width: '200px' },
              { field: 'warehouseName', header: 'Warehouse', width: '200px' },
              { field: 'fixedPricing', header: 'Fixed Pricing', width: '120px' },
              { field: 'itemCondition', header: 'Condition', width: '100px' },
              { field: 'obsolete', header: 'Obsolete', width: '100px' },
              { field: 'procurable', header: 'Procurable', width: '120px' },
              { field: 'quantityOnHand', header: 'Quantity On Hand', width: '150px' },
              { field: 'pendingRQN_POQty', header: "Pending Requisitions/PO's", width: '200px' },
              { field: 'orderQty', header: 'Picking Order Qty', width: '150px' },
              { field: 'refillQty', header: 'Maximum Threshold Refill Qty', width: '250px' },
              { field: 'orderQtyMaxThreshold', header: 'Order Qty > Max Threshold', width: '250px' },
              { field: 'requestedQty', header: 'Requested Qty', width: '150px' }
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            shouldShowFilter: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.REQUISITION_DETAILED_ORDERING_LIST,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          },
          {
            caption: 'EXCLUSION LIST',
            dataKey: 'exclusionListId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'stockCode', header: 'Stock Code', width: '120px' },
              { field: 'stockDescription', header: 'Stock Description', width: '280px' },
              { field: 'storageLocationName', header: 'Storage Location', width: '200px' },
              { field: 'warehouseName', header: 'Warehouse', width: '200px' },
              { field: 'fixedPricing', header: 'Fixed Pricing', width: '200px' },
              { field: 'itemCondition', header: 'Condition', width: '200px' },
              { field: 'obsolete', header: 'Obsolete', width: '200px' },
              { field: 'procurable', header: 'Procurable', width: '200px' },
              { field: 'quantityOnHand', header: 'Quantity On Hand', width: '200px' },
              { field: 'pendingRQN_POQty', header: "Pending Requisitions/PO's", width: '200px' },
              { field: 'orderQty', header: 'Order Quantity', width: '200px' },
              { field: 'refillQty', header: 'Refill Qty', width: '200px' },
              { field: 'orderQtyMaxThreshold', header: 'Order Qty > Max Threshold', width: '250px' },
              { field: 'requestedQty', header: 'Requested Qty', width: '150px' }
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            shouldShowFilter: true,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.REQUISITION_DETAILED_LIST,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }

    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
  }

  ngOnInit() {
    this.searchKeywordRequest();
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.getForkJoinRequests();
    this.createReorderingFilterForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      ).subscribe();
  }
  handleInput(event: KeyboardEvent): void{
    event.stopPropagation();
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {

    this.loading = true;
    let inventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    inventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      inventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
      otherParams ? otherParams: {
        requisitionId: this.requisitionId,
        isAll: true
      })
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      this.observableResponse = data.resources;
      this.dataList = this.observableResponse
      this.totalRecords = data.totalCount;
    });

  }

  onBreadCrumbClick(breadCrumbItem: object): void {
    if (breadCrumbItem.hasOwnProperty('queryParams')) {
      this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
      { queryParams: breadCrumbItem['queryParams'] })
    }
    else {
      this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
    }
  }

  onTabChange(event) {
    this.tables.forEach(table => {
      table.filters = {}
    })
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/inventory/requisitions/stock-orders/detailed-list'], { queryParams: {
        tab: this.selectedTabIndex,
        requisitionId: this.requisitionId,
        type: this.requestType
      }
    })
    this.getRequiredListData();
  }

  loadPaginationLazy(event): void {
    let row = {};
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'asc' : 'desc';
    row['searchColumns'] = event.filters;
    this.onCRUDRequested(CrudType.GET, row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(row).length > 0) {
          // logic for split columns and its values to key value pair

          Object.keys(row['searchColumns']).forEach((key) => {
            if (key.toLowerCase().includes('date')) {
              otherParams[key] = this.momentService.localToUTC(row['searchColumns'][key]['value']);
            } else {
              otherParams[key] = row['searchColumns'][key]['value'];
            }
          });
          otherParams['sortOrder'] = row['sortOrder'];
          if (row['sortOrderColumn']) {
            otherParams['sortOrderColumn'] = row['sortOrderColumn'];
          }
        }
        otherParams['requisitionId'] = this.requisitionId;
        otherParams['isAll'] = true;
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.FILTER:

        this.showReOrderingFilterForm = !this.showReOrderingFilterForm;
        break;

      default:
    }
  }

  navigateToView() {
    if (this.redirectTo) {
      this.router.navigate(['/inventory', 'requisitions', 'stock-orders', 'requisition-view'], {
        queryParams: {
          requisitionId: this.requisitionId,
          type: this.requestType,
        }
      });
    } else if (!this.redirectTo) {
      this.router.navigate(['/inventory', 'requisitions', 'stock-orders', 'add-edit'], {
        queryParams: {
          requisitionId: this.requisitionId,
          type: this.requestType,
        }
      });
    }
  }

  onChangeStatus(rowData, index) {
    const ref = this.dialogService.open(PrimengStatusConfirmDialogComponent, {
      // header: 'Choose a Car',
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        index: index,
        ids: rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey],
        isActive: rowData.isActive,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      if (!result) {
        this.dataList[index].isActive = this.dataList[index].isActive ? false : true;
      }
    });
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  createReorderingFilterForm(requisitionListModel?: RequisitionReOrderingListFilter) {
    let stockListFilterModel = new RequisitionReOrderingListFilter(requisitionListModel);
    this.requisitionReOrderListFilterForm = this.formBuilder.group({});
    Object.keys(stockListFilterModel).forEach((key) => {
      if (typeof stockListFilterModel[key] === 'string') {
        this.requisitionReOrderListFilterForm.addControl(key, new FormControl(stockListFilterModel[key]));
      }
    });
  }
  getForkJoinRequests(): void {
    forkJoin([this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_DESCRIPTION, null, null),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId })),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_CONDITION, null, null)
    ])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((response: IApplicationResponse, ix: number) => {
          if (response.isSuccess) {
            switch (ix) {
              case 0:
                if (response.isSuccess && response.statusCode === 200 && response.resources) {
                  let codeFilter = response.resources.filter(x=>x.itemCode != null);
                  let descFilter = response.resources.filter(x=>x.displayName != null);
                  //this.stockCodeDropdown = codeFilter;
                  // this.stockDescDropdown = descFilter;
                  this.stockDescDropdown=[];
                  for (var i = 0; i < response.resources.length; i++) {
                    if(response.resources[i].displayName != null){
                      let tmp = {};
                      tmp['value'] = response.resources[i].id;
                      tmp['display'] = response.resources[i].displayName;
                      this.stockCodeDropdown.push(tmp);
                    }
                    // if(response.resources[i].displayName != null){
                    //   let stock = {};
                    //   stock['id'] = response.resources[i].id;
                    //   stock['displayName'] = response.resources[i].displayName;
                    //   this.stockDescDropdown.push(stock);
                    // }
                  }

                  this.rxjsService.setGlobalLoaderProperty(false);
                }
                break;
                case 1:
                  if (response.isSuccess && response.statusCode === 200) {
                    this.warehouseDropdown = [];
                    for (var i = 0; i < response.resources.length; i++) {
                      let tmp = {};
                      tmp['value'] = response.resources[i].id;
                      tmp['display'] = response.resources[i].displayName;
                      this.warehouseDropdown.push(tmp);
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                  }
                break;
                case 2:
                  if (response.isSuccess && response.statusCode === 200) {
                    this.itemConditionDropdown = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                  }
                break;
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
      });
  }
  displayAndLoadFilterData() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_ITEM_DESCRIPTION, null, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200 && response.resources) {
          let codeFilter = response.resources.filter(x=>x.itemCode != null);
          let descFilter = response.resources.filter(x=>x.displayName != null);
          //this.stockCodeDropdown = codeFilter;
          // this.stockDescDropdown = descFilter;
          this.stockDescDropdown=[];
          for (var i = 0; i < response.resources.length; i++) {
            if(response.resources[i].displayName != null){
              let tmp = {};
              tmp['value'] = response.resources[i].id;
              tmp['display'] = response.resources[i].displayName;
              this.stockCodeDropdown.push(tmp);
            }
            // if(response.resources[i].displayName != null){
            //   let stock = {};
            //   stock['id'] = response.resources[i].id;
            //   stock['displayName'] = response.resources[i].displayName;
            //   this.stockDescDropdown.push(stock);
            // }
          }

          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.warehouseDropdown = [];
          for (var i = 0; i < response.resources.length; i++) {
            let tmp = {};
            tmp['value'] = response.resources[i].id;
            tmp['display'] = response.resources[i].displayName;
            this.warehouseDropdown.push(tmp);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_ITEM_CONDITION, null, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.itemConditionDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  submitFilter(){

    // let  stockDescription;
    // stockDescription = this.requisitionReOrderListFilterForm.get('stockDescription').value;
    let stockCodeList=[];  let warehouseList=[]; let fixedPriceList=[];
    let obsoleteList=[];  let conditionList=[];

    let stockCode =this.requisitionReOrderListFilterForm.get('stockCode').value;
    if(stockCode && stockCode.length > 0) {
      stockCode?.forEach(element => {
        stockCodeList.push(element.value);
      });
    }
    let warehouse =this.requisitionReOrderListFilterForm.get('warehouseName').value;
    if(warehouse && warehouse.length > 0) {
      warehouse?.forEach(element => {
        warehouseList.push(element.value);
      });
    }
    let fixedPrice =this.requisitionReOrderListFilterForm.get('fixedPricing').value;
    if(fixedPrice && fixedPrice.length > 0) {
      fixedPrice.forEach(element => {
        fixedPriceList.push(element.value);
      });
    }
    let obsolete =this.requisitionReOrderListFilterForm.get('obsolete').value;
    if(obsolete && obsolete.length > 0) {
      obsolete.forEach(element => {
        obsoleteList.push(element.value);
      });
    }
    let condition =this.requisitionReOrderListFilterForm.get('itemCondition').value;
    if(condition && condition.length > 0) {
      condition.forEach(element => {
        conditionList.push(element.id);
      });
    }
    let input = Object.assign({},
      this.requisitionReOrderListFilterForm.get('stockCode').value == '' || this.requisitionReOrderListFilterForm.get('stockCode').value == null ? null :
      { StockCodeIds: stockCodeList.toString() },
      this.requisitionReOrderListFilterForm.get('stockDescription').value == '' || this.requisitionReOrderListFilterForm.get('stockDescription').value == null  ? null :
      // { stockDescriptionIds: stockDescription ? stockDescription.id:null },
      this.requisitionReOrderListFilterForm.get('warehouseName').value == '' || this.requisitionReOrderListFilterForm.get('warehouseName').value == null  ? null :
      { WarehouseIds: warehouseList.toString() },
      this.requisitionReOrderListFilterForm.get('fixedPricing').value == '' || this.requisitionReOrderListFilterForm.get('fixedPricing').value == null  ? null :
      { FixedPricingIds: fixedPriceList.toString() },
      this.requisitionReOrderListFilterForm.get('obsolete').value == '' || this.requisitionReOrderListFilterForm.get('obsolete').value == null  ? null :
      { ObsoleteIds: obsoleteList.toString() },
      this.requisitionReOrderListFilterForm.get('itemCondition').value == '' || this.requisitionReOrderListFilterForm.get('itemCondition').value == null  ? null :
      { ItemConditionIds: conditionList.toString() },
      { ProcurableIds: this.requisitionReOrderListFilterForm.get('procurableId').value },
      { requisitionId: this.requisitionId }, { isAll: true }
    );
    this.observableResponse = this.getRequiredListData('', '', input);
    this.showReOrderingFilterForm = !this.showReOrderingFilterForm;
  }

  reset(){
    this.showReOrderingFilterForm = !this.showReOrderingFilterForm;
    this.getRequiredListData('','', null);
    this.requisitionReOrderListFilterForm.reset();
  }

}
