import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatExpansionPanel, MatTab, MatTabGroup } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CustomDirectiveConfig, removeFormControlError } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { fileUrlDownload, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { RequisitionListCreationModel, StockCodeDescriptionModel } from '@modules/inventory/models/RequisitionListCreation.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { RequisitionCreationModalComponent } from './requisition-creation-modal.component';
import { StockInfoModalComponent } from './stock-info-modal.component';
import { StockOrderInterbranchModalComponent } from './stock-order-interbranch-modal.component';

@Component({
  selector: 'app-stock-order-add-edit',
  templateUrl: './stock-order-add-edit.component.html',
  styleUrls: ['./stock-order-add-edit.component.scss'],
})
export class StockOrderAddEditComponent implements OnInit {

  requisitionCreationForm: FormGroup;
  stockCodeDescriptionForm: FormGroup;
  selectedOptions: any = [];
  selectSupplierOptions: any = [];
  warehouseList = [];
  stockOrderTypeList = [];
  stockOrderNumbersList = [];
  stockCodeList = [];
  priorityList = [];
  filteredStockCodes = [];
  stockCodeAndDescriptionList = [];
  newStockOrderTypesStockCodeList = [];
  loggedUser: UserLogin;
  requisitionId: string;
  dropdownsAndData = [];
  selectedTabIndex = 0;
  breadCrumbTitle: any = '';
  fullListInterbranchRequisitionModel: any = {};
  filteredStockDescription: any = [];
  isStockCodeBlank: boolean = false;
  isStockDescriptionSelected: boolean = false;
  isStockOrderClear: boolean = false;
  stockCodeErrorMessage: any = '';
  showStockCodeError: boolean = false;
  isStockError: boolean = false;
  StockDescErrorMessage: any = '';
  showStockDescError: boolean = false;
  isStockDescError: boolean = false;
  rqnStatusName: any = '';
  isNewStock: boolean = true;
  isArrayEmpty = false;
  startTodayDate = new Date();
  decimalPattern = "/^\d+(\.\d{0,2})?$/";
  @ViewChild('matExpansionPanel', { static: false }) matExpansionPanelElement: MatExpansionPanel;
  @ViewChildren('matTab') matTab: MatTab;
  @ViewChild('stockOrderTypeRef', { static: false }) stockOrderTypeRef: ElementRef<HTMLSelectElement>;
  @ViewChild('priorityTypeRef', { static: false }) priorityTypeRef: ElementRef<HTMLSelectElement>;
  @ViewChild('tabGroup', { static: false }) tabGroup: MatTabGroup;
  isLoading: boolean;
  minDate: Date = new Date();
  numberConfig = new CustomDirectiveConfig({ isADecimalOnly: true });
  isADecimalWithConfig = new CustomDirectiveConfig({ isADecimalWithConfig: { trailingDotDigitsCount: 2 } });
  storageLocationDropDown: any;
  suppliersDropDown: any = []
  public formData = new FormData();
  fileList: File[] = [];
  listOfFiles: any[] = [];
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  maxFilesUpload: Number = 5;
  supplierDocuments: any = []
  fromValue: Number = 0;
  toValue: Number = 0;
  requestType: string = '';
  userId: string = '';
  referenceId: string = '';
  approvalStatusName: any
  statusList: any = [];
  fullListSubTotal: number = 0;
  requestListSubTotal: number = 0;
  rqnStatusError: boolean;

  isToggle = true;
  isDeletePopup: boolean = false;
  deleteIndex;
  stockId;
  stockOrderIds;
  control;
  isAdd: boolean = false;
  isRepairStock: boolean = true;
  loggedInUserData;
  enableStockOrder: boolean = true;
  constructor(
    private router: Router,
    private datePipe: DatePipe,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private crudService: CrudService,
    private httpService: CrudService,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private dialog: MatDialog, private changeDetectorRef: ChangeDetectorRef
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.requisitionId = this.activatedRoute.snapshot.queryParams.requisitionId;
    this.fromValue = Number(this.activatedRoute.snapshot.queryParams.fromValue);
    this.toValue = Number(this.activatedRoute.snapshot.queryParams.toValue);
    this.requestType = this.activatedRoute.snapshot.queryParams.type;
    this.userId = this.activatedRoute.snapshot.queryParams.userId;
    this.referenceId = this.activatedRoute.snapshot.queryParams.referenceId;
  }

  ngOnInit() {

    this.rxjsService.setFormChangeDetectionProperty(true);
    this.createRequisitionCRUDForm();
    this.createStockCodeDescriptionForm();
    this.getSuppliersDropdown()
    this.getStatusDropDown();
    this.combineLatestNgrxStoreData();
    if (this.requisitionId) {
      this.breadCrumbTitle = 'Update';
      this.dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({
            userId: this.loggedUser.userId
          })),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.STOCK_ORDER_TYPES),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.UX_PRIORITY),
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_EDIT, this.requisitionId, true, null)
      ];
      this.requisitionCreationForm.get('warehouseId').disable({ emitEvent: false });
      this.requisitionCreationForm.get('requisitionDate').disable({ emitEvent: false });
      this.requisitionCreationForm.get('stockOrderTypeId').disable({ emitEvent: false });
    }
    else {
      this.breadCrumbTitle = 'Create';
      this.dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({
            userId: this.loggedUser.userId
          })),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.STOCK_ORDER_TYPES),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.UX_PRIORITY)
      ];
      this.requisitionCreationForm.get('requisitionDate').setValue(new Date());
    }

    this.loadActionTypes(this.dropdownsAndData);
     // To catch changes from interbranch pop up submit
     this.rxjsService.getRequisitionData().subscribe(data => {
      this.stockItemsFormArray.value.forEach((stock, index) => {
        if (stock.stockId === data['itemId']) {
          this.stockItemsFormArray.controls[index].get('ibtOrderQty').setValue(data['quantity']);
          this.requisitionCreationForm.get('requisitionId').setValue(data['requisitionId']);
          this.stockItemsFormArray.controls[index].get('discrepancyReason').setValue(data['discrepancyReason']);
        }
      });
    });
    this.stockCodeDescriptionForm.get('stockId').disable();
    this.stockCodeDescriptionForm.get('stockDescription').disable();
    this.stockCodeDescriptionForm.get('requestedOty').disable();
    this.valueChange();
  }
  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }
  createRequisitionCRUDForm(reqCreationListModel?: RequisitionListCreationModel) {
    let requisitionCreationModel = new RequisitionListCreationModel(reqCreationListModel);
    this.requisitionCreationForm = this.formBuilder.group({});
    let stockItemsFormArray = this.formBuilder.array([]); // [], Validators.required
    Object.keys(requisitionCreationModel).forEach((key) => {
      if (typeof requisitionCreationModel[key] !== 'object') {
        this.requisitionCreationForm.addControl(key,
          new FormControl(requisitionCreationModel[key]));
      } else {
        this.requisitionCreationForm.addControl(key, stockItemsFormArray);
      }
    });
    this.requisitionCreationForm = setRequiredValidator(this.requisitionCreationForm, ['warehouseId', 'requisitionDate', 'stockOrderTypeId', 'priorityId', 'LocationId', 'Motivation']);
    //this.requisitionCreationForm.get('requisitionDate').setValue(new Date());
    this.isNewStock = false;
  }
  valueChange(){
    this.requisitionCreationForm.get('warehouseId').valueChanges.subscribe(data => {
      this.stockCodeAndDescriptionList = [];
      if (data) {
        this.getStockOrderNumbersByWarehouseId(data);
        this.getStorageLocationByWarehouseId(data);
      } else {
        this.stockOrderNumbersList = [];
        this.storageLocationDropDown = [];
      }
    });
    this.requisitionCreationForm.get('stockOrderTypeId').valueChanges.subscribe(data => {
      this.isArrayEmpty = false;
      this.isRepairStock = true;

      if(data){
        this.stockCodeDescriptionForm.get('stockId').enable();
        this.stockCodeDescriptionForm.get('stockDescription').enable();
        this.stockCodeDescriptionForm.get('requestedOty').enable();
      }
      if (this.stockOrderTypeList.find(x => x.id == data?.toLowerCase())?.displayName?.toLowerCase() === 'new stock') {
        this.enableStockOrder=false;
        this.stockCodeDescriptionForm.get('stockId').enable();
        this.stockCodeDescriptionForm.get('stockDescription').enable();
        this.stockCodeDescriptionForm.get('requestedOty').enable();

        this.requisitionCreationForm.get('stockOrderIds').setValue('');
        this.requisitionCreationForm = clearFormControlValidators(this.requisitionCreationForm, ["stockOrderIds"]);
        this.isNewStock = false;
        let priorityData = this.priorityList.find(priority => priority.displayName.toLowerCase() === 'standard');
        this.requisitionCreationForm.get('priorityId').setValue(priorityData.id);
      }
      else if (this.stockOrderTypeList.find(x => x.id == data?.toLowerCase())?.displayName?.toLowerCase() === 'job specific' || this.stockOrderTypeList.find(x => x.id == data?.toLowerCase())?.displayName?.toLowerCase() === 'once off') {
       this.enableStockOrder=true;
        this.requisitionCreationForm.get('stockOrderIds').enable({ emitEvent: false });
        this.isNewStock = true;
        // Make stock order number required
        this.requisitionCreationForm.get('stockOrderIds').setValidators([Validators.required]);
        let priorityData = this.priorityList.find(priority => priority.displayName.toLowerCase() === 'medium');
        this.requisitionCreationForm.get('priorityId').setValue(priorityData.id);
        // this.newStockOrderTypesStockCodeList = []; // For stock order types other than new stock we will never et new stock codes for the selected warehouse so we should make it empty
        if (!this.requisitionCreationForm.get('stockOrderIds').value) {
          this.stockItemsFormArray.clear(); // To clear form array table
        }
        // if (this.stockCodeAndDescriptionList.length > 0) {
        //   this.createRequisitionDataTable(this.stockCodeAndDescriptionList);
        // }
      }
      else if (this.stockOrderTypeList.find(x => x.id == data?.toLowerCase())?.displayName?.toLowerCase() === 'urgent') {
       // this.stockCodeDescriptionForm.get('stockId').disable();
      //  this.stockCodeDescriptionForm.get('stockDescription').disable();
       // this.stockCodeDescriptionForm.get('requestedOty').disable();
       this.enableStockOrder=true;
        this.requisitionCreationForm.get('stockOrderIds').enable({ emitEvent: false });
        this.isNewStock = true;
        this.requisitionCreationForm.get('stockOrderIds').setValidators([Validators.required]);
        let priorityData = this.priorityList.find(priority => priority.displayName.toLowerCase() === 'high');
        this.requisitionCreationForm.get('priorityId').setValue(priorityData.id);
        if (!this.requisitionCreationForm.get('stockOrderIds').value) {
          this.stockItemsFormArray.clear(); // To clear form array table
        }
      }
      else if(this.stockOrderTypeList.find(x => x.id == data?.toLowerCase())?.displayName?.toLowerCase() === 'repair stock'){
        this.stockCodeDescriptionForm.get('stockId').enable();
        this.stockCodeDescriptionForm.get('stockDescription').enable();
        this.stockCodeDescriptionForm.get('requestedOty').enable();
        this.enableStockOrder=true;
        this.requisitionCreationForm.get('stockOrderIds').setValue('');
        this.requisitionCreationForm.get('stockOrderIds').setValidators([Validators.required]);
        this.isRepairStock = false;
      }
      else {
        this.enableStockOrder=true;
        // this.newStockOrderTypesStockCodeList = []; // For stock order types other than new stock we will never et new stock codes for the selected warehouse so we should make it empty
        if (!this.requisitionCreationForm.get('stockOrderIds').value) {
          this.stockItemsFormArray.clear(); // To clear form array table
        }

        this.requisitionCreationForm.get('stockOrderIds').enable({ emitEvent: false });
        this.isNewStock = true;
        this.requisitionCreationForm.get('stockOrderIds').setValidators([Validators.required]);

      }

    });

    // To catch changes inside stock code
    this.stockCodeDescriptionForm.get('stockId').valueChanges.pipe(debounceTime(50), distinctUntilChanged(),
      switchMap(searchText => {

          if(this.stockCodeDescriptionForm.get('stockId').value?.displayName)
              searchText=this.stockCodeDescriptionForm.get('stockId').value?.displayName;

        this.isStockDescError = false
        this.StockDescErrorMessage = '';
        this.showStockDescError = false;

        if (this.isStockError == false) {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
        }
        else {
          this.isStockError = false;
        }

        if (searchText != null) {
          searchText =  searchText?.trim();
          if (this.isStockCodeBlank == false) {
            this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
            this.stockCodeDescriptionForm.controls.requestedOty.patchValue(null);
          }
          else {
            this.isStockCodeBlank = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.stockCodeErrorMessage = '';
              this.showStockCodeError = false;
              this.isStockError = false;
            }
            else {
              this.stockCodeErrorMessage = 'Stock code is not available in the system';
              this.showStockCodeError = true;
              this.isStockError = true;
            }
            // this.snackbarService.openSnackbar('Stock code is not available in the system', ResponseMessageTypes.ERROR);
            return this.filteredStockCodes = [];
          } else {
             let flag=true;let params;
             flag = this.stockOrderTypeList.find(x => x.id ==  this.requisitionCreationForm.get('stockOrderTypeId').value.toLowerCase()).displayName.toLowerCase() === 'repair stock' ?  false : true;
             if(!flag){
              params = { searchText, isAll: false, materialtype: flag? 'Y':null, IsRequisition:  true,IsRepairCode: true };
             }
             else
               params ={ searchText, isAll: false, warehouseId: this.requisitionCreationForm.controls['warehouseId'].value, materialtype: flag? 'Y':null, IsRequisition:  true };

            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, true,
              prepareGetRequestHttpParams(null, null, params))
          }
        }
        else {
          return this.filteredStockCodes = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockCodes = [];
          response.resources.forEach(element => {
            if (element.displayName == null && element.message) {
              this.snackbarService.openSnackbar(element.message, ResponseMessageTypes.ERROR);
              return;
            }
            else {
              this.filteredStockCodes.push(element);
            }
          });
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.filteredStockCodes = [];
          this.stockCodeErrorMessage = 'Stock code is not available in the system';
          this.showStockCodeError = true;
          this.isStockError = true;
          this.rxjsService.setGlobalLoaderProperty(false);
          // this.snackbarService.openSnackbar('Stock code is not available in the system', ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    // To catch changes inside stock description
    this.stockCodeDescriptionForm.get('stockDescription').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        if ((searchText != null)) {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;

          if (this.isStockDescError == false) {
            this.StockDescErrorMessage = '';
            this.showStockDescError = false;
          }
          else {
            this.showStockDescError = false;
          }
          if (this.isStockDescriptionSelected == false) {
            this.stockCodeDescriptionForm.controls.stockId.patchValue(null);
            this.stockCodeDescriptionForm.controls.requestedOty.patchValue(null);
          }
          else {
            this.isStockDescriptionSelected = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.StockDescErrorMessage = '';
              this.showStockDescError = false;
              this.isStockDescError = false;
            }
            else {
              if(!this.stockCodeDescriptionForm.get('stockId').value){
                // this.StockDescErrorMessage = 'Stock description is not available in the system';
                // this.showStockDescError = true;
                // this.isStockDescError = true;
              }
            }
            // this.snackbarService.openSnackbar('Stock description is not available in the system', ResponseMessageTypes.ERROR);
            return this.filteredStockDescription = [];
          } else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_NAME, null, true,
              prepareGetRequestHttpParams(null, null, { searchText, isAll: false, warehouseId: this.requisitionCreationForm.controls['warehouseId'].value, materialtype: 'Y' }))
          }
        } else {
          return this.filteredStockDescription = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockDescription = response.resources;
          this.isStockDescriptionSelected = false;
          this.StockDescErrorMessage = '';
          this.showStockDescError = false;
          this.isStockDescError = false;
        } else {
          this.filteredStockDescription = [];
          this.isStockDescriptionSelected = false;
          // this.StockDescErrorMessage = 'Stock description is not available in the system';
          // this.showStockDescError = true;
          // this.isStockDescError = true;
          // this.snackbarService.openSnackbar('Stock description is not available in the system', ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    this.requisitionCreationForm.get('rqnStatusId').valueChanges.subscribe(id => {

      let obj = this.statusList.filter(data => {
        if (data.id == id) {
          return true
        }
      });
    //  this.requisitionCreationForm.get('stockOrderIds').clearValidators();
      this.requisitionCreationForm = clearFormControlValidators(this.requisitionCreationForm, ['stockOrderIds']);
      if (obj.length > 0) {
        this.approvalStatusName = obj[0].displayName;
        if (obj[0].displayName == 'Cancelled' || obj[0].displayName == 'Rejected') {
          this.rqnStatusError = false;
        //  this.requisitionCreationForm.get('reason').setValidators([Validators.required]);
          // this.requisitionCreationForm = addFormControls(this.requisitionCreationForm, [ "reason"]);
        }
        if (obj[0].displayName == 'Approved') {
          this.rqnStatusError = false;
          this.requisitionCreationForm.get('reason').setErrors(null);
          this.requisitionCreationForm.get('reason').setValue(null);
          this.requisitionCreationForm.get('reason').clearValidators();
          this.requisitionCreationForm = clearFormControlValidators(this.requisitionCreationForm, ['reason']);
        //  this.requisitionCreationForm.get('reason').updateValueAndValidity();
         // this.requisitionCreationForm = removeFormControlError(this.requisitionCreationForm, 'reason');
          //this.requisitionCreationForm = removeFormControls(this.requisitionCreationForm, ["reason"]);
        }
      }
     // this.requisitionCreationForm.get('stockOrderIds').clearValidators();
      this.requisitionCreationForm = clearFormControlValidators(this.requisitionCreationForm, ['stockOrderIds']);
    });

  }
  ngAfterViewInit() {
    if (!this.requisitionId) {
      this.tabGroup._tabs['_results'][1].disabled = true;
      this.tabGroup._tabs['_results'][2].disabled = true;
    }
    // if(this.requestType == 'inventory-task-list'){
    //   this.tabGroup._tabs['_results'][0].disabled = true;
    //   this.tabGroup._tabs['_results'][1].disabled = true;
    //   this.tabGroup._tabs['_results'][2].disabled = false;
    //   this.selectedTabIndex = 2;
    // }
  }

  ngAfterViewChecked() {
    this.changeDetectorRef.detectChanges();
  }
  validateNumber(event: any, index) {
    // event.returnValue = true;
    // const reg = /^-?\d*(\.\d{0,2})?$/;
    // let input = event.target.value + String.fromCharCode(event.charCode);
    // if (reg.test(input) == true) {
    // }else if (reg.test(input) == false) {
    //   event.preventDefault();

    // }
   // this.stockItemsFormArray.controls[index].get('costPrice').setValue(parseFloat(event.target.value).toFixed(2));
  }
  onTabClicked(tabData) {
    this.selectedTabIndex = tabData.index;
    if (this.selectedTabIndex != 0) {
      this.requisitionCreationForm.get('warehouseId').disable({ emitEvent: false });
      this.requisitionCreationForm.get('requisitionDate').disable({ emitEvent: false });
      this.requisitionCreationForm.get('stockOrderTypeId').disable({ emitEvent: false });
     // this.requisitionCreationForm.get('stockOrderIds').disable({ emitEvent: false });
      this.requisitionCreationForm.get('priorityId').disable({ emitEvent: false });
    } else { // 0
      if (this.requisitionId) {
        this.requisitionCreationForm.get('warehouseId').disable({ emitEvent: false });
        this.requisitionCreationForm.get('requisitionDate').disable({ emitEvent: false });
        this.requisitionCreationForm.get('stockOrderTypeId').disable({ emitEvent: false });
        this.requisitionCreationForm.get('stockOrderIds').enable({ emitEvent: false });
        this.requisitionCreationForm.get('priorityId').enable({ emitEvent: false });
      } else {
        this.requisitionCreationForm.get('warehouseId').enable({ emitEvent: false });
        this.requisitionCreationForm.get('requisitionDate').enable({ emitEvent: false });
        this.requisitionCreationForm.get('stockOrderTypeId').enable({ emitEvent: false });
        this.requisitionCreationForm.get('stockOrderIds').enable({ emitEvent: false });
        this.requisitionCreationForm.get('priorityId').enable({ emitEvent: false });
      }
    }
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.warehouseList = resp.resources;
              this.requisitionCreationForm.get('warehouseId').setValue(this.loggedUser.warehouseId);
              break;
            case 1:
              this.stockOrderTypeList = resp.resources;
              //this.requisitionCreationForm.get('stockOrderTypeId').setValue(this.stockOrderTypeList[0].id);
              break;
            case 2:
              this.priorityList = resp.resources;
              break;
            case 3:
              this.isRepairStock = resp.resources.orderType?.toLowerCase()== "repair stock" ? false:true;
              this.stockItemsFormArray.clear();
              this.fullListInterbranchRequisitionModel = {};
              resp?.resources?.requisitionRequests?.forEach(val => {
                val.contractCost = (val.contractCost) ? val.contractCost.toFixed(2) : '0.00';
                val.costPrice = (val.costPrice) ? val.costPrice.toFixed(2) : '0.00';
                val.subTotal = (val.subTotal) ? val.subTotal.toFixed(2) : '0.00';
                return val;
              });

              this.fullListInterbranchRequisitionModel = resp.resources;
              this.requisitionCreationForm.get('stockOrderTypeId').setValue(resp.resources?.orderTypeId);
   
              if( resp.resources && resp.resources.requisitionApproverRanges && resp.resources.requisitionApproverRanges.length > 0){
                let requisitionApproverRanges=   resp.resources.requisitionApproverRanges;
                let filterData =  requisitionApproverRanges.filter(x=>x.userId == this.loggedUser.userId);
                if(filterData && filterData.length > 0) {
                 this.fromValue =   filterData[0].fromRange;
                 this.toValue =   filterData[0].toRange;

                }
               }
              if(resp.resources.rqnStatusName == "New" || resp.resources.rqnStatusName == "Save as Draft") {
                //rqnStatusName
                this.getRequistionCreation();
                this.requisitionCreationForm = clearFormControlValidators(this.requisitionCreationForm, ['stockOrderIds']);
              }
              //this.requisitionCreationForm.patchValue(this.fullListInterbranchRequisitionModel);
              this.requisitionCreationForm.get('rqnStatusId').patchValue(resp.resources.rqnStatusId);
              this.requisitionCreationForm.get('requisitionId').setValue(this.fullListInterbranchRequisitionModel['requisitionId']);
              this.requisitionCreationForm.get('warehouseId').setValue(this.fullListInterbranchRequisitionModel['warehouseId']);
              // this.requisitionCreationForm.get('supplierId').setValue(this.fullListInterbranchRequisitionModel['supplierId']);
      
              this.supplierDocuments = this.fullListInterbranchRequisitionModel['supplierDocuments']
              this.listOfFiles = []
              if (this.fullListInterbranchRequisitionModel['supplierDocuments'].length > 0) {
                this.fullListInterbranchRequisitionModel['supplierDocuments'].forEach(element => {
                  this.listOfFiles.push({ id: element.requisitionSupplierDocumentId, name: element.docName, path: element.path })
                });
              }
              this.requisitionCreationForm.get('requisitionDate').setValue(new Date(this.fullListInterbranchRequisitionModel['requisitionDate']));
              // if(!Guid.EMPTY) {
              //this.requisitionCreationForm.get('stockOrderTypeId').setValue(this.fullListInterbranchRequisitionModel['orderTypeId']);

              if (this.fullListInterbranchRequisitionModel['orderType'].toLowerCase() == 'new stock') {
                this.requisitionCreationForm.get('stockOrderIds').setValue('');
                this.requisitionCreationForm = clearFormControlValidators(this.requisitionCreationForm, ["stockOrderIds"]);
            //    this.requisitionCreationForm.get('stockOrderIds').disable({ emitEvent: false });
                this.isNewStock = false;
              }
        
              this.fullListInterbranchRequisitionModel['items'] = resp.resources.items;
              if (resp.resources.items.length > 0) {
                this.fullListSubTotal = 0;
                resp.resources.items.forEach(datas => {
                  this.fullListSubTotal += datas.subTotal;
                });
              }

              if (resp.resources.requisitionRequests.length > 0) {
                this.requestListSubTotal = 0;
                resp.resources.requisitionRequests.forEach(requests => {
                  this.requestListSubTotal += requests.subTotal;
                });
              }

             // this.requisitionCreationForm.patchValue(this.fullListInterbranchRequisitionModel);
              this.rqnStatusName = resp.resources.rqnStatusName;
              if (this.fullListInterbranchRequisitionModel['stockOrderIds'] && this.fullListInterbranchRequisitionModel['stockOrderIds'].indexOf(',') > 0) {
                this.requisitionCreationForm.get('stockOrderIds').setValue(this.fullListInterbranchRequisitionModel['stockOrderIds'].split(','));
                this.selectedOptions = this.fullListInterbranchRequisitionModel['stockOrderIds'].split(',');
              } else if (this.fullListInterbranchRequisitionModel['stockOrderIds']) {
                 this.requisitionCreationForm.get('stockOrderIds').setValue([this.fullListInterbranchRequisitionModel['stockOrderIds']]);
                this.selectedOptions = [this.fullListInterbranchRequisitionModel['stockOrderIds']];
              }

             // this.requisitionCreationForm.get('stockOrderIds').setValue(this.selectedOptions);
              if (this.fullListInterbranchRequisitionModel['supplierId'] && this.fullListInterbranchRequisitionModel['supplierId'].indexOf(',') > 0) {
                this.selectSupplierOptions = this.fullListInterbranchRequisitionModel['supplierId'].split(',');
              } else if (this.fullListInterbranchRequisitionModel['supplierId']) {
                this.selectSupplierOptions = this.fullListInterbranchRequisitionModel['supplierId'];
              }
              this.requisitionCreationForm.get('warehouseId').setValue(resp.resources.warehouseId);
              this.requisitionCreationForm.get('priorityId').setValue(resp.resources.priorityId);
              this.requisitionCreationForm.get('Motivation').setValue(resp.resources.motivation);

              this.createRequisitionDataTable(this.fullListInterbranchRequisitionModel['items'], true);
              if (Object.keys(this.fullListInterbranchRequisitionModel['interBranchTransfers']).length > 0) {
                // this.selectedTabIndex = 1;
                this.tabGroup._tabs['_results'][1].disabled = false;
              } else {
                this.tabGroup._tabs['_results'][1].disabled = true;
              }

              if (Object.keys(this.fullListInterbranchRequisitionModel['requisitionRequests']).length > 0) {
                this.tabGroup._tabs['_results'][2].disabled = false;
              } else {
                this.tabGroup._tabs['_results'][2].disabled = true;
              }
              // if(this.requestType == 'inventory-task-list'){
              //   this.tabGroup._tabs['_results'][0].disabled = true;
              //   this.tabGroup._tabs['_results'][1].disabled = true;
              //   this.tabGroup._tabs['_results'][2].disabled = false;
              //   this.selectedTabIndex = 2;
              // }
              this.requisitionCreationForm.get('reason').patchValue(resp.resources.reason);
              this.requisitionCreationForm.get('warehouseId').patchValue(resp.resources.warehouseId);
              this.requisitionCreationForm.get('requisitionDate').patchValue(new Date(resp.resources.requisitionDate));
              this.requisitionCreationForm.get('priorityId').patchValue(resp.resources.priorityId);
              this.requisitionCreationForm.get('LocationId').patchValue(resp.resources.locationId);
              this.requisitionCreationForm.get('Motivation').patchValue(resp.resources.motivation);

           //   this.requisitionCreationForm.get('stockOrderTypeId').patchValue(resp.resources.stockOrderIds);
              // this.requisitionCreationForm.get('rqnStatusId').setValue(this.statusList[1].id);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  onStatucCodeSelected(value, type) {

    if(value?.displayName)
      value = value?.displayName

    let stockItemData;
    if (value && type === 'stockCode') {
      stockItemData = this.stockItemsFormArray.value.find(stock => stock.stockCode === value);
      if (!stockItemData) {
        this.isStockDescriptionSelected = true;

        this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM, this.filteredStockCodes.find(x => x.displayName === value).id).subscribe((response) => {
          if (response.statusCode == 200) {
            this.stockCodeDescriptionForm.controls.stockDescription.patchValue(response.resources.itemName);
          }
        })
      }
      else {
        this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
        this.stockCodeDescriptionForm.controls.stockId.patchValue(null);
        this.isStockError = true;
        this.stockCodeErrorMessage = 'Stock code is already present';
        this.showStockCodeError = true;
        // this.snackbarService.openSnackbar('Stock code is already present', ResponseMessageTypes.ERROR);
      }
    } else if (value &&  type === 'stockDescription') {
      stockItemData = this.stockItemsFormArray.value.find(stock => stock.stockDescription === value);
      if (!stockItemData) {
        this.isStockCodeBlank = (this.stockCodeDescriptionForm.get('stockId').value == null || this.stockCodeDescriptionForm.get('stockId').value == '') ? true : false;
        this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM, this.filteredStockDescription.find(x => x.displayName === value).id).subscribe((response) => {
          if (response.statusCode == 200) {
            this.filteredStockCodes = [{
              id: response.resources.itemId,
              displayName: response.resources.itemCode
            }];
            this.stockCodeDescriptionForm.controls.stockId.patchValue(response.resources.itemCode);
          }
        })
      }
      else {
        this.stockCodeDescriptionForm.controls.stockDescription.patchValue(null);
        this.stockCodeDescriptionForm.controls.stockId.patchValue(null);
        this.isStockDescError = true;
        this.StockDescErrorMessage = 'Stock Description is already present';
        this.showStockDescError = true;
        // this.snackbarService.openSnackbar('Stock code is already present', ResponseMessageTypes.ERROR);
      }

    }

  }
  getRequistionCreation(){
    //https://fidelity-inventory-staging.azurewebsites.net/api/requisition-creation-validation/d4ecbcef-6759-4785-9bb7-4009e77c156c
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.REQUISITION_CREATION_VALIDATION,
      this.loggedUser.userId,
      false,
      null
    ).subscribe((response:any) => {
      this.fromValue = response.resources.fromValue;
      this.toValue = response.resources.toValue;

    });
  }
  getStockOrderNumbersByWarehouseId(warehouseId) {
    let data = {};
    if (Object.keys(this.fullListInterbranchRequisitionModel).length > 0) {
      data['WarehouseId'] = warehouseId;
      data['requisitionId'] = this.requisitionCreationForm.get('requisitionId').value;
    } else {
      data['WarehouseId'] = warehouseId;
    }
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_STOCK_ORDERS,
      undefined,
      false,
      prepareRequiredHttpParams(data)
    )
      .subscribe(response => {
        let respObj = response.resources;
        if (response.resources && response.resources.length > 0 && Object.keys(respObj).length > 0) {
          for (let i = 0; i < respObj.length; i++) {
            let temp = {};
            temp['display'] = respObj[i].displayName;
            temp['value'] = respObj[i].id;
            this.stockOrderNumbersList.push(temp);
          }
        } else {
          this.stockOrderNumbersList = [];
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getStorageLocationByWarehouseId(warehouseId) {
    let data = {};
    data['WarehouseId'] = warehouseId;
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION, undefined,
      false, prepareRequiredHttpParams(data))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.storageLocationDropDown = [];
          this.storageLocationDropDown = response.resources;
          if (this.fullListInterbranchRequisitionModel['locationId'] && this.fullListInterbranchRequisitionModel['locationId'] != "00000000-0000-0000-0000-000000000000") {
            this.requisitionCreationForm.get('LocationId').setValue(this.fullListInterbranchRequisitionModel['locationId']);
          } else {
            if (this.storageLocationDropDown.length > 0) {
              let filteredObj = this.storageLocationDropDown.filter((item: any) => {
                if (JSON.stringify(item).includes('Main')) {
                  return item;
                }
              });
              this.requisitionCreationForm.get('LocationId').setValue(filteredObj?.length > 0 ? filteredObj[0].id : '');
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getSuppliersDropdown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIERS)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.suppliersDropDown = [];
          this.suppliersDropDown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getStatusDropDown() {
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RQNSTATUS)
      .subscribe((response: IApplicationResponse) => {
        let status = response.resources;
        if (status.length > 0) {
          this.statusList = status.filter(x => x.displayName === 'Approved' || x.displayName === 'Cancelled' ||
            x.displayName === 'Pending' || x.displayName === 'Rejected');
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  stockInfoModal(interBranchStockDetails) {

    const dialogReff = this.dialog.open(StockInfoModalComponent, {
      width: '600px',
      data: {
        interBranchStockDetails: interBranchStockDetails
      },
      disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  createStockCodeDescriptionForm(stockCodeDescriptionModel?: StockCodeDescriptionModel) {
    let stockCodeModel = new StockCodeDescriptionModel(stockCodeDescriptionModel);
    this.stockCodeDescriptionForm = this.formBuilder.group({});
    Object.keys(stockCodeModel).forEach((key) => {
      if (typeof stockCodeModel[key] !== 'object') {
        this.stockCodeDescriptionForm.addControl(key, new FormControl(stockCodeModel[key]));
      }
    });
  }


  getStockCodesByStockNumber(stockNumbers) {
    // this.isArrayEmpty = false;
    if (stockNumbers.length > 0) {
      this.isStockOrderClear = false;
      this.isArrayEmpty = false;
    }
    if (stockNumbers.length >= 0 && this.isStockOrderClear == false) {
      if (stockNumbers.length > 0) {
        this.isArrayEmpty = false;
        this.crudService.get(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.STOCKORDER_ITEMS,
          undefined, false,
          prepareRequiredHttpParams({
            Ids: stockNumbers.join()
          })
        )
          .subscribe(response => {
            // set stock numbers in the table
            let stockCodesModel = response.resources;
            this.fullListSubTotal = 0;
            if (stockCodesModel.length > 0) {
              for (var i = 0; i < stockCodesModel.length; i++) {
                stockCodesModel[i].requiredQty = stockCodesModel[i].requestedQty;
                stockCodesModel[i].subTotal = stockCodesModel[i].subtotal;
                this.fullListSubTotal += stockCodesModel[i].subTotal;
              }
              this.stockItemsFormArray.clear();
              if (this.stockCodeAndDescriptionList.length > 0) {
                stockCodesModel = [...stockCodesModel, ...this.stockCodeAndDescriptionList];
              }
              this.createRequisitionDataTable(stockCodesModel);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
            this.isStockOrderClear = false;
          });
      }
      else {
        // This will be used when we dont have stock order numbers selected.
        this.stockItemsFormArray.clear();
        this.isStockOrderClear = true;
        if (this.stockCodeAndDescriptionList.length > 0) {
          this.createRequisitionDataTable(this.stockCodeAndDescriptionList);
        }
      }
    }
  }

  createRequisitionDataTable(stockCodesModel, updateMode = false) {
    // Create variable to store list of stock orders for comparison
    this.isArrayEmpty = false;
    let stockCodesDetailsArray = this.stockItemsFormArray;
    for (const stockCode of stockCodesModel) {
      let stockCodesDetailsFormGroup = this.formBuilder.group({
        'stockCode': stockCode.stockCode,
        'stockDescription': stockCode.stockDescription,
        'requestedQuantity': stockCode.requestedQty,
        'requiredQuantity': stockCode.requiredQty, // [stockCode.requiredQty, [Validators.min(1)]]
        'stockOrderNo': stockCode.stockOrderNo,
        'minimumThreshold': stockCode.minimumThreshold,
        'maximumThreshold': stockCode.maximumThreshold,
        'stockOnHand': stockCode.stockOnHand,
        'ibtAvailableQty': stockCode.ibtAvailableQty,
        'ibtOrderQty': stockCode.ibtOrderQty,
        'requisitionQty': stockCode.requiredQty - stockCode.ibtOrderQty,
        // stockCode.requisitionQty,
        'stockId': stockCode.stockId,
        'requisitionIdForDelete': [{ value: updateMode ? this.fullListInterbranchRequisitionModel['requisitionId'] : null, disabled: true }], // check for this line as we have to pass requisition id
        'stockOrderId': stockCode.stockOrderId ? stockCode.stockOrderId : null,
        'discrepancyReason': stockCode.reasonForDiscrepancy ? stockCode.reasonForDiscrepancy : null,
        'contractCost': stockCode.contractCost ? stockCode.contractCost?.toFixed(2) : '0.00',
        'costPrice': stockCode.costPrice ? stockCode.costPrice?.toFixed(2) : '0.00',
        'subTotal': stockCode.subTotal ? stockCode.subTotal?.toFixed(2): '0.00',
      });

      // stockCodesDetailsFormGroup.get('subTotal').setValue((+stockCodesDetailsFormGroup.get('requisitionQty').value) * (+stockCodesDetailsFormGroup.get('costPrice').value));

      let subtotal = (+stockCodesDetailsFormGroup.get('requisitionQty').value) * (+stockCodesDetailsFormGroup.get('costPrice').value);
      let subtotals = subtotal.toFixed(2);
      stockCodesDetailsFormGroup.get('subTotal').setValue(subtotals);

      if (stockCodesDetailsFormGroup.get('contractCost').value == null ||
        stockCodesDetailsFormGroup.get('contractCost').value == 0.00 ||
        stockCodesDetailsFormGroup.get('contractCost').value < 0.00) {
        stockCodesDetailsFormGroup.get('costPrice').enable();
      }
      else {
        stockCodesDetailsFormGroup.get('costPrice').disable();
      }

      // stockCodesDetailsFormGroup = setMinMaxValidator(stockCodesDetailsFormGroup, [
      //   { formControlName: 'requiredQuantity', compareWith: 'requestedQuantity', type: 'minEqual' }
      // ]);

      stockCodesDetailsFormGroup.get('requiredQuantity').valueChanges.subscribe(requiredQuantity => {
        stockCodesDetailsFormGroup.get('requisitionQty').setValue((+requiredQuantity) - (+stockCodesDetailsFormGroup.get('ibtOrderQty').value));
        // stockCodesDetailsFormGroup.get('subTotal').setValue((+stockCodesDetailsFormGroup.get('requisitionQty').value) * (+stockCodesDetailsFormGroup.get('costPrice').value));

        // let requisitionQty = (+requiredQuantity) - (+stockCodesDetailsFormGroup.get('ibtOrderQty').value);
        // let requisitionQtys = requisitionQty.toFixed(2);
        // stockCodesDetailsFormGroup.get('requisitionQty').setValue(requisitionQtys);

        let subtotal = (+stockCodesDetailsFormGroup.get('requisitionQty').value) * (+stockCodesDetailsFormGroup.get('costPrice').value);
        let subtotals = subtotal.toFixed(2);
        stockCodesDetailsFormGroup.get('subTotal').setValue(subtotals);

        this.fullListSubTotal = 0;
        stockCodesDetailsArray.value.forEach(element => {
          let subtotal = parseFloat(element.subTotal);
          this.fullListSubTotal += subtotal;
        });

      });
      stockCodesDetailsFormGroup.get('requiredQuantity').setValidators([Validators.required]);
      stockCodesDetailsFormGroup.controls['requiredQuantity'].setValidators(Validators.compose([Validators.min(1), Validators.required]));
    
      stockCodesDetailsFormGroup.get('ibtOrderQty').valueChanges.subscribe(ibtOrderQty => {
        stockCodesDetailsFormGroup.get('requisitionQty').setValue((+stockCodesDetailsFormGroup.get('requiredQuantity').value) - (+ibtOrderQty));
        // stockCodesDetailsFormGroup.get('subTotal').setValue((+stockCodesDetailsFormGroup.get('requisitionQty').value) * (+stockCodesDetailsFormGroup.get('costPrice').value));

        // let requisitionQty = (+stockCodesDetailsFormGroup.get('requiredQuantity').value) - (+ibtOrderQty);
        // let requisitionQtys = requisitionQty.toFixed(2)
        // stockCodesDetailsFormGroup.get('requisitionQty').setValue(requisitionQtys);

        let subtotal = (+stockCodesDetailsFormGroup.get('requisitionQty').value) * (+stockCodesDetailsFormGroup.get('costPrice').value);
        let subtotals = subtotal.toFixed(2);
        stockCodesDetailsFormGroup.get('subTotal').setValue(subtotals);

        this.fullListSubTotal = 0;
        stockCodesDetailsArray.value.forEach(element => {
          let subtotal = parseFloat(element.subTotal);
          this.fullListSubTotal += subtotal;
        });

      });

      stockCodesDetailsFormGroup.get('costPrice').valueChanges.subscribe(costPrice => {
        // stockCodesDetailsFormGroup.get('subTotal').setValue((+stockCodesDetailsFormGroup.get('requisitionQty').value) * (+costPrice));

        let subtotal = (+stockCodesDetailsFormGroup.get('requisitionQty').value) * (+costPrice);
        let subtotals = subtotal.toFixed(2);
        stockCodesDetailsFormGroup.get('subTotal').setValue(subtotals);

        this.fullListSubTotal = 0;
        stockCodesDetailsArray.value.forEach(element => {
          let subtotal = parseFloat(element.subTotal);
          this.fullListSubTotal += subtotal;
        });

      });

      stockCodesDetailsArray.push(stockCodesDetailsFormGroup);
    }

  }

  addStockNumberToTable() {

     if(!this.stockCodeDescriptionForm?.get('stockId')?.value ||
     !this.stockCodeDescriptionForm?.get('stockDescription')?.value ||
     !this.stockCodeDescriptionForm?.get('requestedOty')?.value || this.stockCodeDescriptionForm?.get('requestedOty')?.value == 0){
        this.isAdd = true;
        return;
     }else{
      this.isAdd = false;
     }
     let flag = true;let params;
     flag = this.stockOrderTypeList.find(x => x.id ==  this.requisitionCreationForm.get('stockOrderTypeId').value.toLowerCase()).displayName.toLowerCase() === 'repair stock' ?  false : true;

     if(flag){
        params = {
          stockId: this.filteredStockCodes.find(x => x.displayName == this.stockCodeDescriptionForm.get('stockId').value).id,
       // stockId :this.stockCodeDescriptionForm.get('stockId').value,
        warehouseId: flag ? this.requisitionCreationForm.get('warehouseId').value : null
        }
     }else {
      params = {
         stockId: this.filteredStockCodes.find(x => x.displayName == this.stockCodeDescriptionForm.get('stockDescription').value).id,
        // stockId :this.stockCodeDescriptionForm.get('stockDescription').value,  
         warehouseId: flag ? this.requisitionCreationForm.get('warehouseId').value : null
       }
     }

    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ADD_REQUISITION_ITEM,
      undefined,
      false,
      prepareRequiredHttpParams(params)
    )
      .subscribe(response => {
    //  this.stockCodeDescriptionForm = clearFormControlValidators(this.stockCodeDescriptionForm, ["stockId","stockDescription","requestedOty"]);
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources) {
            response.resources.requestedQty = this.stockCodeDescriptionForm.get('requestedOty').value;
            response.resources.requiredQty = response.resources.requestedQty;
            response.resources.requisitionQty = response.resources.requiredQty - response.resources.ibtOrderQty;
            response.resources.subTotal = response.resources.requisitionQty * response.resources.costPrice;

            // this.fullListSubTotal = 0;
            // this.stockItemsFormArray.value.forEach(element => {
            //   this.fullListSubTotal += element.subTotal;
            // });

            this.fullListSubTotal += response.resources.subTotal;

            this.stockCodeAndDescriptionList.push(response.resources);
            this.stockCodeDescriptionForm.reset();
            this.createRequisitionDataTable([response.resources]);
            this.snackbarService.openSnackbar('Stock code added manually', ResponseMessageTypes.SUCCESS);
            this.rxjsService.setGlobalLoaderProperty(false);
          } else {
            this.rxjsService.setGlobalLoaderProperty(false);
            this.snackbarService.openSnackbar('Stock not added with this warehouse. Kindly add in stock maintenance', ResponseMessageTypes.WARNING);
          }

        } else {
          // this.stockItemsFormArray.clear();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  openInterBranchModal(ItemId, RequiredQty) {

    if (this.requisitionCreationForm.get('priorityId').value == '' && this.requisitionCreationForm.get('stockOrderTypeId').value == '') {
      this.snackbarService.openSnackbar('Select Stock Order Type and Priority', ResponseMessageTypes.WARNING)
      return;
    } else if (this.requisitionCreationForm.get('priorityId').value == '') {
      this.snackbarService.openSnackbar('Select Stock Priority', ResponseMessageTypes.WARNING)
      return;
    } else if (this.requisitionCreationForm.get('stockOrderTypeId').value == '') {
      this.snackbarService.openSnackbar('Select Stock Order Type', ResponseMessageTypes.WARNING)
      return;
    }

    const dialogReff = this.dialog.open(StockOrderInterbranchModalComponent, {
      width: '600px',
      data: {
        ItemId,
        WarehouseId: this.requisitionCreationForm.get('warehouseId').value,
        RequisitionId: this.requisitionCreationForm.get('requisitionId').value,
        loggedInWarehouseId: this.loggedUser.warehouseId,
        StockOrderTypeId: this.requisitionCreationForm.get('stockOrderTypeId').value,
        PriorityId: this.requisitionCreationForm.get('priorityId').value,
        OrderPriority: this.priorityTypeRef.nativeElement.selectedOptions[0].text.trim().toLowerCase(),
        RQNStatusId: '92BCB72A-BCA5-4A55-B673-5AFF5C38B2AE',
        InterBranchOrderStatus: 'New',
        PONumber: '',
        CreatedUserId: this.loggedUser.userId,
        RequiredQty,
      }, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.dialog.closeAll();
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  get stockItemsFormArray(): FormArray {
    if (this.requisitionCreationForm !== undefined) {
      return (<FormArray>this.requisitionCreationForm.get('Items'));
    }
  }

  async saveAsDraft(type) {
    // let checkValid = await this.checkValidation()
    // if (!checkValid) {
    //   return
    // }
    if(this.requisitionCreationForm.invalid){
      return;
    }
    this.requisitionCreationForm.get('createdUserId').setValue(this.requisitionId ? this.loggedUser.userId : this.loggedUser.userId);
    this.requisitionCreationForm.get('modifiedUserId').setValue(this.requisitionId ? this.loggedUser.userId : this.loggedUser.userId);
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.createPostRequisitionApi(type)?.subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  checkValidation() {

    return new Promise((resolve, reject) => {
      var totalSubTotal = 0;
      this.requisitionCreationForm.value.Items.forEach(element => {
        let subtotal = parseFloat(element.subTotal);
        totalSubTotal += subtotal;
      });

      if (this.fromValue <= totalSubTotal && this.toValue >= totalSubTotal) {
        resolve(true)
      }
      else {
        // let message = "You don't have access range for " + this.fromValue + ' to ' + this.toValue + '.Please contact administrator for further access';
        this.snackbarService.openSnackbar('The Total value for the requisition exceeds the limit setup for your user account.', ResponseMessageTypes.WARNING);
        resolve(false)
      }
    });
  }

  async createRequisition(type) {

    if (this.requisitionId && (this.selectedTabIndex == 2 ) && !this.requisitionCreationForm.dirty) {
       this.rxjsService.setFormChangeDetectionProperty(true);
       return this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
    }
    if(this.enableStockOrder)
        this.requisitionCreationForm.get('stockOrderIds').setValidators([Validators.required])

     
    if(this.requisitionId && this.selectedTabIndex == 2 &&  this.fullListInterbranchRequisitionModel?.createdUserId !=this.loggedUser.userId && this.requestType == 'inventory-task-list'){
      if(!this.approvalStatusName){
        this.snackbarService.openSnackbar("Please select the status.",ResponseMessageTypes.ERROR);
        return;
      }
    }
    if(this.requestType == 'inventory-task-list'){ 
      //['warehouseId', 'requisitionDate', 'stockOrderTypeId', 'priorityId', 'LocationId', 'Motivation'
      this.requisitionCreationForm = clearFormControlValidators(this.requisitionCreationForm, ['warehouseId', 'requisitionDate', 'stockOrderTypeId']);
      this.requisitionCreationForm.get('stockOrderIds')?.clearValidators();
      this.requisitionCreationForm.get('supplierId')?.clearValidators();
      this.requisitionCreationForm.get('requiredQuantity')?.clearValidators();
      this.requisitionCreationForm.get('reason')?.clearValidators();
    }
    if (this.requestType != 'inventory-task-list' && this.enableStockOrder && this.requisitionCreationForm.invalid) {
      this.requisitionCreationForm.get('stockOrderIds').markAsTouched();
      return;
    }

    this.rqnStatusError = false;

    if (this.selectedTabIndex == 2 && this.requestType == 'inventory-task-list' &&
      !this.requisitionCreationForm.get('rqnStatusId').value) {
      this.rqnStatusError = true;
      return;
    }

    let filter = this.statusList.filter(x=> x.id == this.requisitionCreationForm.get('rqnStatusId').value)
   
    if (this.requisitionId 
      &&  this.fullListInterbranchRequisitionModel?.createdUserId !=this.loggedUser.userId
      &&  this.selectedTabIndex == 2 && this.requestType == 'inventory-task-list'
      &&  this.requisitionCreationForm.get('rqnStatusId').value
      && (filter?.length>0 && (filter[0].displayName=='Rejected' || filter[0].displayName=='Cancelled' ) && !this.requisitionCreationForm.get('reason').value)) {
      //!this.requisitionCreationForm.get('reason').value
      this.snackbarService.openSnackbar('Reason is required',ResponseMessageTypes.WARNING)
      return;
    }
    if (this.stockItemsFormArray.controls.length == 0) {
      this.isArrayEmpty = true;
      return;
    }

    if(this.requestType != 'inventory-task-list' && this.rqnStatusName != 'Rejected'){
      let checkValid = await this.checkValidation()
      if (!checkValid) {
        return;
      }
    }

    let subTotalMsg: boolean = false;
    let contractValidation: boolean = false;
    this.requisitionCreationForm.value.Items.forEach(x => {
      if (x.subTotal == null || x.subTotal <= 0) {
        subTotalMsg = true;
      }
      if (x.contractCost == null || x.contractCost <= 0) {
        contractValidation = true;
      }
    });

    // if (subTotalMsg) {
    //   this.snackbarService.openSnackbar('Sub Total cannot be Zero', ResponseMessageTypes.WARNING)
    //   return;
    // }
    if (contractValidation && (this.requisitionCreationForm.get('supplierId').value == '' ||
      this.requisitionCreationForm.get('supplierId').value == null)) {
      this.requisitionCreationForm.get('supplierId').setValidators([Validators.required]);
      this.requisitionCreationForm.get('supplierId').updateValueAndValidity();
      this.requisitionCreationForm.get('supplierId').markAsUntouched();
      return;
    }
    else {
      this.requisitionCreationForm.get('supplierId').setErrors(null);
      this.requisitionCreationForm.get('supplierId').clearValidators();
      this.requisitionCreationForm.get('supplierId').updateValueAndValidity();
    }

    if (contractValidation && this.listOfFiles.length == 0) {
      this.snackbarService.openSnackbar('Supporting documents is required', ResponseMessageTypes.WARNING)
      return;
    }

    this.requisitionCreationForm.get('createdUserId').setValue(this.requisitionId ? this.loggedUser.userId : this.loggedUser.userId);
    this.requisitionCreationForm.get('modifiedUserId').setValue(this.requisitionId ? this.loggedUser.userId : this.loggedUser.userId);

    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.selectedTabIndex == 0) { // Proceed : after clicking redirect to Interbranch tab
      this.createPostRequisitionApi(type)?.subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          // Used for getting complete data including all 3 tabs
          this.getFullListInterBranchRequisitionData(response.resources ? response.resources : this.requisitionId);
        }
      });
    }
    else if (this.selectedTabIndex == 1) { // Next : after clicking redirect to Requisition tab
      this.selectedTabIndex = 2;
    }
    else if (this.selectedTabIndex == 2) { // Create : after clicking redirect to list page
      // 1st Pop up

      let creationMsg: string = '';
      let statusName: string = '';

      if (this.requestType == 'inventory-task-list') {
        let status = this.statusList.find(sta => sta.id == this.requisitionCreationForm.get('rqnStatusId').value)
        statusName = status?.displayName == 'Rejected' ? 'reject' : status?.displayName == 'Cancelled' ? 'cancel' : 'approve'
      }

      creationMsg = this.requestType == 'inventory-task-list' ? `Are you sure you want to ${statusName} the requisition?` :
        'Would you like to proceed for Approval?'

      const dialogReff = this.dialog.open(RequisitionCreationModalComponent, {
        width: '450px',
        data: {
          message: creationMsg,
          requisitionId: this.requisitionId,
          buttons: {
            cancel: 'No',
            create: 'Yes'
          }
        }, disableClose: true
      });
      dialogReff.afterClosed().subscribe(result => {
        if (!result) return;
        this.requisitionCreationForm.get('isSaved').setValue(false);
        this.createPostRequisitionApi(type)?.subscribe((response) => {
          if (response.isSuccess && response.statusCode == 200) {
            // 2nd Pop up
            const dialogRequisitionId = this.dialog.open(RequisitionCreationModalComponent, {
              width: '450px',
              data: {
                message: this.requisitionId ? `Requisition ID ${this.fullListInterbranchRequisitionModel['requisitionNumber']} was updated.` : `Requisition ID ${this.fullListInterbranchRequisitionModel['requisitionNumber']} is generated.`, // <br />Would you like to proceed?
                requisitionId: this.requisitionId,
                buttons: {
                  create: 'Ok'
                }
              }, disableClose: true
            });
            dialogRequisitionId.afterClosed().subscribe(result => {
              if (!result) return;
              this.navigateToList();
              this.rxjsService.setDialogOpenProperty(false);
            });
          }
        })
        this.rxjsService.setDialogOpenProperty(false);
      });
    }
  }

  createPostRequisitionApi(type) {
    // To check whether to call PUT or POST api
    // If we are on edit component, we call EDIT/GET api ie we have fullListInterbranchRequisitionModel data so call
    // PUT api.
    // If we are on create component, and we click PROCEED btn ie (we are on Requisition Tab)/ (We have already called //
    // POST api) so we have to call PUT api.
  //  if( this.requestType != 'inventory-task-list' && this.requisitionCreationForm.invalid){
  
    let status = this.statusList.find(sta => sta.id == this.requisitionCreationForm.get('rqnStatusId').value)

    let RequisitionBody = this.requisitionCreationForm.getRawValue();
    if(RequisitionBody.Items && RequisitionBody.Items.filter(x=>x.costPrice <=0 || x.costPrice==null)?.length >0){
      this.snackbarService.openSnackbar("Cost Price (R) value should be greater than 0.",ResponseMessageTypes.WARNING);
      return;
    }

    RequisitionBody.supplierDocuments = this.supplierDocuments;
    RequisitionBody.RequisitionApprovalId = this.requestType == 'inventory-task-list' ? this.referenceId : '';
    RequisitionBody.action = this.requisitionId && status != undefined && this.selectedTabIndex == 2 ? status.displayName : '';
    // RequisitionBody.Requisitioncreation = false;

    RequisitionBody.Requisitioncreation = !this.requisitionId && this.selectedTabIndex == 2 ? true :
      this.requisitionId && this.selectedTabIndex == 2 && this.requestType == 'requisition-list' && (this.rqnStatusName == 'New' || this.rqnStatusName == 'Save as Draft') ? true : false;
    RequisitionBody.requisitionDate = this.datePipe.transform(RequisitionBody.requisitionDate, 'yyyy-MM-dd hh:mm:ss a');

    RequisitionBody.Items.forEach(element => {
      element.costPrice = element.costPrice == null ? 0.00 : element.costPrice;
      element.subTotal = element.subTotal == null ? 0.00 : element.subTotal;
    });
    if(type == 'save-as-draft'){
      RequisitionBody.IsUpdate =  false;
      RequisitionBody.IsDraft =  true;
      RequisitionBody.IsProceed =  false;
    }
    else if(type == 'submit'){
      if(this.selectedTabIndex == 0){
        RequisitionBody.IsUpdate =  false;
        RequisitionBody.IsDraft =  false;
        RequisitionBody.IsProceed =  true;
      }
      else if(this.selectedTabIndex == 2 && this.requisitionId){
        RequisitionBody.IsUpdate =  true;
        RequisitionBody.IsDraft =  false;
        RequisitionBody.IsProceed =  false;
        RequisitionBody.IsCancelled  =  status?.displayName == 'Cancelled' ? true:false;
        RequisitionBody.IsRejected  =   status?.displayName == 'Rejected' ? true:false;
        RequisitionBody.IsApproved  =   status?.displayName == 'Approved' ? true:false;
      }
      else if(this.selectedTabIndex == 2) {
        RequisitionBody.IsUpdate =  false;
        RequisitionBody.IsDraft =  false;
        RequisitionBody.IsProceed =  false;
        RequisitionBody.IsCancelled  =  status?.displayName == 'Cancelled' ? true:false;
        RequisitionBody.IsRejected   =   status?.displayName == 'Rejected' ? true:false;
        RequisitionBody.IsApproved   =   status?.displayName == 'Approved' ? true:false;
      }
      // {{ selectedTabIndex == 0 ? 'Proceed' : selectedTabIndex == 1? 'Next' : ( selectedTabIndex == 2
      //   && requisitionId? 'Update':'Create')}}
    }


    RequisitionBody.supplierId = RequisitionBody.supplierId ? RequisitionBody.supplierId?.toString() : '';
    var formData = new FormData();
    formData.append('json', JSON.stringify(RequisitionBody));
    this.fileList.forEach(file => {
      formData.append('file', file);
    });

    let sendParams = {
      "requisitionId": this.requisitionId,
      "approvalId": this.loggedUser.userId,
      "rqnStatusId": this.requisitionCreationForm.get('rqnStatusId').value,
      "approvalDate": new Date(),
      "requisitionApprovalId": this.referenceId,
      "reason": this.requisitionCreationForm.get('reason').value,
      "modifiedDate": new Date(),
      "modifiedUserId": this.loggedUser.userId,
      "IsUpdate" :   false,
      "IsDraft" :    false,
      "IsProceed" :  false,
      "IsCancelled" :  status?.displayName == 'Cancelled' ? true:false,
      "IsRejected"  :  status?.displayName == 'Rejected' ? true:false,
      "IsApproved"  :  status?.displayName == 'Approved' ? true:false
    }
    if(this.approvalStatusName){
      sendParams['action']= this.approvalStatusName;
    }

    if (Object.keys(this.fullListInterbranchRequisitionModel).length > 0) {
      // We have already called POST Api so thats why we have fullListInterbranchRequisitionModel data now call PUT
      return this.crudService.update(
        ModulesBasedApiSuffix.INVENTORY, this.selectedTabIndex == 2 && this.requestType == 'inventory-task-list' ?
        InventoryModuleApiSuffixModels.REQUISITION_GET_DETAILS : InventoryModuleApiSuffixModels.REQUISITION_LIST,
        this.selectedTabIndex == 2 && this.requestType == 'inventory-task-list' ? sendParams : formData
      ).pipe(tap(() => {
        // this.supplierDocuments = []
        // this.fileList = []
        // this.listOfFiles = []
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
    } else {
      // If we dont have fullListInterbranchRequisitionModel data means we dont have called POST api now call POST api
      return this.crudService.create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.REQUISITION_LIST,
        formData
        // this.requisitionCreationForm.getRawValue()
      ).pipe(tap(() => {
        // this.supplierDocuments = []
        // this.fileList = []
        // this.listOfFiles = []
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
    }
  }

  getFullListInterBranchRequisitionData(requisitionId) {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_EDIT, requisitionId, true, null).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200) {

        this.supplierDocuments = []
        this.fileList = []
        this.stockItemsFormArray.clear();
        this.fullListInterbranchRequisitionModel = {};
        response?.resources?.requisitionRequests?.forEach(val => {
          val.contractCost = (val.contractCost) ? val.contractCost.toFixed(2) : '0.00';
          val.costPrice = (val.costPrice) ? val.costPrice.toFixed(2) : '0.00';
          val.subTotal = (val.subTotal) ? val.subTotal.toFixed(2) : '0.00';
          return val;
        });
        this.fullListInterbranchRequisitionModel = response.resources;

        this.requisitionCreationForm.get('requisitionId').setValue(this.fullListInterbranchRequisitionModel['requisitionId']);
        this.requisitionCreationForm.get('warehouseId').setValue(this.fullListInterbranchRequisitionModel['warehouseId'], { emitEvent: false });
        this.requisitionCreationForm.get('requisitionDate').setValue(new Date(this.fullListInterbranchRequisitionModel['requisitionDate']));
        this.requisitionCreationForm.get('stockOrderTypeId').setValue(this.fullListInterbranchRequisitionModel['orderTypeId']);

        this.supplierDocuments = this.fullListInterbranchRequisitionModel['supplierDocuments']
        this.listOfFiles = []

        if (this.fullListInterbranchRequisitionModel['supplierDocuments'].length > 0) {
          this.fullListInterbranchRequisitionModel['supplierDocuments'].forEach(element => {
            this.listOfFiles.push({ id: element.requisitionSupplierDocumentId, name: element.docName, path: element.path })
          });
        }

        this.fullListInterbranchRequisitionModel['items'] = response.resources.items;
        if (response.resources.items.length > 0) {
          this.fullListSubTotal = 0;
          response.resources.items.forEach(datas => {
            this.fullListSubTotal += datas.subTotal;
          });
        }

        if (response.resources.requisitionRequests.length > 0) {
          this.requestListSubTotal = 0;
          response.resources.requisitionRequests.forEach(requests => {
            this.requestListSubTotal += requests.subTotal;
          });
        }
        if (this.fullListInterbranchRequisitionModel['stockOrderIds'] && this.fullListInterbranchRequisitionModel['stockOrderIds'].indexOf(',') > 0) {
          this.selectedOptions = this.fullListInterbranchRequisitionModel['stockOrderIds'].split(',');
        } else if (this.fullListInterbranchRequisitionModel['stockOrderIds']) {
          this.selectedOptions = [this.fullListInterbranchRequisitionModel['stockOrderIds']];
        }

        if (this.fullListInterbranchRequisitionModel['supplierId'] && this.fullListInterbranchRequisitionModel['supplierId'].indexOf(',') > 0) {
          this.selectSupplierOptions = this.fullListInterbranchRequisitionModel['supplierId'].split(',');
        } else if (this.fullListInterbranchRequisitionModel['supplierId']) {
          this.selectSupplierOptions = this.fullListInterbranchRequisitionModel['supplierId'];
        }

        this.requisitionCreationForm.get('stockOrderIds').setValue(this.selectedOptions);
        this.requisitionCreationForm.get('priorityId').setValue(this.fullListInterbranchRequisitionModel['priorityId']);
        this.createRequisitionDataTable(this.fullListInterbranchRequisitionModel['items'], true);
        if (Object.keys(this.fullListInterbranchRequisitionModel['interBranchTransfers']).length > 0) {
          this.selectedTabIndex = 1;
          this.tabGroup._tabs['_results'][1].disabled = false;
        } else {
          this.selectedTabIndex = 2;
          // this.tabGroup._tabs[1].disable = true;
          this.tabGroup._tabs['_results'][1].disabled = true;
        }

        if (Object.keys(this.fullListInterbranchRequisitionModel['requisitionRequests']).length > 0) {
          this.tabGroup._tabs['_results'][2].disabled = false;
        } else {
          this.tabGroup._tabs['_results'][2].disabled = true;
        }
        // if(this.requestType == 'inventory-task-list'){
        //   this.tabGroup._tabs['_results'][0].disabled = true;
        //   this.tabGroup._tabs['_results'][1].disabled = true;
        //   this.tabGroup._tabs['_results'][2].disabled = false;
        //   this.selectedTabIndex = 2;
        // }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  deleteRequisition() {

    const dialogRemoveStockCode = this.dialog.open(RequisitionCreationModalComponent, {
      width: '450px',
      data: {
        message: `Are you sure you want to<br />remove the requisition id: ${this.fullListInterbranchRequisitionModel['requisitionNumber']} ?`,
        buttons: {
          cancel: 'No',
          create: 'Yes'
        }
      }, disableClose: true
    });
    dialogRemoveStockCode.afterClosed().subscribe(result => {
      if (!result) return;
      this.rxjsService.setFormChangeDetectionProperty(true);
      const options = {
        body: {
          requisitionId: this.requisitionCreationForm.get('requisitionId').value,
          modifiedUserId: this.loggedUser.userId
        }
      };
      this.crudService.deleteByParams(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_LIST,
        options).subscribe((response) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.navigateToList();
          }
        })
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  deleteStockNumbersBackup(stockId, stockOrderIds, control, index?) {
    // Delete pop up
    const dialogRemoveStockCode = this.dialog.open(RequisitionCreationModalComponent, {
      width: '450px',
      data: {
        message: `Are you sure you want to<br />remove the stock code: ${control.get('stockCode').value} ?`,
        buttons: {
          cancel: 'No',
          create: 'Yes'
        }
      }, disableClose: true
    });

    dialogRemoveStockCode.afterClosed().subscribe(result => {

      if (!result) return;
      //
      if (!(control.get('requisitionIdForDelete').value)) {
        // Object.keys(this.fullListInterbranchRequisitionModel).length < 1 &&
        let subtotal = this.stockItemsFormArray.controls[index].get('subTotal').value;
        this.fullListSubTotal = this.fullListSubTotal - subtotal;
        this.stockItemsFormArray.removeAt(index);
        return;
      }
      //
      let data = Object.assign({},
        this.requisitionCreationForm.get('requisitionId').value == null ? null : { requisitionId: this.requisitionCreationForm.get('requisitionId').value },
        stockId == null ? null : { stockId: stockId },
        stockOrderIds == null ? null : { stockOrderIds: stockOrderIds },
        this.loggedUser.userId == null ? null : { modifiedUserId: this.loggedUser.userId }
      );
      const options = {
        body: data
      };
      this.rxjsService.setFormChangeDetectionProperty(true);
      this.crudService.deleteByParams(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ADD_REQUISITION_ITEM,
        options).subscribe((response) => {
          if (response.isSuccess && response.statusCode === 200) {
            let subtotal = this.stockItemsFormArray.controls[index].get('subTotal').value;
            this.fullListSubTotal = this.fullListSubTotal - subtotal;
            this.stockItemsFormArray.removeAt(index);
          }
        })
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
  deleteStockNumbers(stockId, stockOrderIds, control, index?) {
    this.isDeletePopup = true;
    this.stockId = stockId;
    this.deleteIndex = index;
    this.control = control
    this.stockOrderIds = stockOrderIds;

  }
  deleteClose() {
    this.isDeletePopup = false;
  }
  deleteStockCode() {

    if (!(this.control.get('requisitionIdForDelete').value)) {
      // Object.keys(this.fullListInterbranchRequisitionModel).length < 1 &&
      let subtotal = this.stockItemsFormArray.controls[this.deleteIndex].get('subTotal').value;
      this.fullListSubTotal = this.fullListSubTotal - subtotal;
      this.stockItemsFormArray.removeAt(this.deleteIndex);
      this.isDeletePopup = false;
      return;
    }
    //
    let data = Object.assign({},
      this.requisitionCreationForm.get('requisitionId').value == null ? null : { requisitionId: this.requisitionCreationForm.get('requisitionId').value },
      this.stockId == null ? null : { stockId: this.stockId },
      this.stockOrderIds == null ? null : { stockOrderIds: this.stockOrderIds },
      this.loggedUser.userId == null ? null : { modifiedUserId: this.loggedUser.userId }
    );
    const options = {
      body: data
    };
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.crudService.deleteByParams(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ADD_REQUISITION_ITEM,
      options).subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.isDeletePopup = false;
          let subtotal = this.stockItemsFormArray.controls[this.deleteIndex].get('subTotal').value;
          this.fullListSubTotal = this.fullListSubTotal - subtotal;
          this.stockItemsFormArray.removeAt(this.deleteIndex);
        }
      })
    this.rxjsService.setDialogOpenProperty(false);
  }
  navigateToList() {
    if (this.requestType == 'inventory-task-list') {
      this.router.navigate(['my-tasks/inventory-task-list']);
    }
    else {
      this.router.navigate(['/inventory', 'requisitions', 'stock-orders']);
    }
  }
  cancelList(){
    this.router.navigate(['/inventory', 'requisitions', 'stock-orders']);
  }
  navigateToViewPage() {
    this.router.navigate(['/inventory', 'requisitions', 'stock-orders', 'requisition-view'], {
      queryParams: {
        requisitionId: this.requisitionId,
        userId: this.loggedUser.userId,
        type: 'requisition-list',
        fromValue: this.fromValue ? this.fromValue : 0,
        toValue: this.toValue ? this.toValue : 0,
        referenceId: this.referenceId
      }, skipLocationChange: true
    });
  }

  showCancelRequisition: boolean = false;
  showReasonRequest: boolean = false;
  requisitionReasons = '';
  requisitionReasonsMsg: boolean = false;

  CancelRequisition() {

    this.showCancelRequisition = true;
    this.showReasonRequest = false;

    // // this.navigateToList();
    // const dialogRemoveStockCode = this.dialog.open(RequisitionCreationModalComponent, {
    //   width: '450px',
    //   data: {
    //     message: `Are you sure you want to<br />cancel the Requisition: ${this.fullListInterbranchRequisitionModel['requisitionNumber']} ?`,
    //     buttons: {
    //       cancel: 'No',
    //       create: 'Yes'
    //     }
    //   }, disableClose: true
    // });
    // dialogRemoveStockCode.afterClosed().subscribe(result => {
    //   if (!result) return;
    //   this.rxjsService.setFormChangeDetectionProperty(true);
    //   const options = {
    //     requisitionId: this.requisitionCreationForm.get('requisitionId').value,
    //     modifiedUserId: this.loggedUser.userId,
    //   };
    //   this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_CANCEL,
    //     options).subscribe((response) => {
    //       if (response.isSuccess && response.statusCode === 200) {
    //         this.navigateToList();
    //       }
    //     })
    //   this.rxjsService.setDialogOpenProperty(false);
    // });
  }

  submitCancelOrder(){
    this.requisitionReasonsMsg = false;
    if(this.requisitionReasons == ''){
      this.requisitionReasonsMsg = true;
      return;
    }

    const options = {
      requisitionId: this.requisitionCreationForm.get('requisitionId').value,
      modifiedUserId: this.loggedUser.userId,
      reason: this.requisitionReasons
    };
    this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_CANCEL,
      options).subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.navigateToList();
        }
        else {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        }
      });
  }

  navigateToDetailedList() {
    this.router.navigate(['/inventory', 'requisitions', 'stock-orders', 'detailed-list'], {
      queryParams: {
        requisitionId: this.requisitionId, view: false, type: this.requestType
      }
    });
  }

  uploadFiles(file) {
    if (file && file.length == 0)
      return;

    var numberOfFilesUploaded = this.listOfFiles.length;
    if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
      this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
      return;
    }

    const supportedExtensions = [
      'jpeg',
      'jpg',
      'png',
      'gif',
      'pdf',
      'doc',
      'docx',
      'xls',
      'xlsx',
    ];

    for (let i = 0; i < file.length; i++) {
      let selectedFile = file[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension)) {
        let filename = this.fileList.find(x => x.name === selectedFile.name);
        if (filename == undefined) {
          this.fileList.push(selectedFile);
          this.listOfFiles.push({ id: null, name: selectedFile.name });
          this.myFileInputField.nativeElement.value = null;
        }
      } else {
        this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
        return;
      }
    }
  }

  removeSelectedFile(index, id) {
    // Delete the item from fileNames list
    this.listOfFiles.splice(index, 1);
    // delete file from FileList
    this.fileList.splice(index, 1);
    // let id = this.listOfFiles[index]
    if (id) {
      let indexRemove = this.supplierDocuments.map(function (item) {
        return item.requisitionSupplierDocumentId
      }).indexOf(id);
      this.supplierDocuments.splice(indexRemove, 1);
    }
  }

  download(path) {
    if (path) return fileUrlDownload(path, true);
  }

  toggle() {
    this.isToggle = !this.isToggle;
  }


}

