import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { RequisitionListCreationModel } from '@modules/inventory/models/RequisitionListCreation.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';


@Component({
  selector: 'app-fullList-interbranch-requisition',
  templateUrl: './fullList-interBranch-requisitionReg.component.html',
  styleUrls: ['./stock-order-add-edit.component.scss'],
})
export class FullListInterBranchReqisitionRegComponent implements OnInit {

  requisitionCreationForm: FormGroup;
  warehouseList = [];
  stockOrderTypeList = [];
  stockOrderNumbersList = [];
  selectedTabIndex: any = 0;
  status: any = [];
  stockCodeList = [];
  priorityList = [];
  filteredStockCodes = [];
  loggedUser: UserLogin;
  fullListInterbranchRequisitionModel = {};
  constructor(

    private formBuilder: FormBuilder,
    private crudService: CrudService,
    private rxjsService: RxjsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.createRequisitionCRUDForm();
    this.getFullListInterBranchRequisitionData('');
  }

  getFullListInterBranchRequisitionData(doaApprovalId) {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_EDIT, '8e3a7d9e-0188-4160-b379-76f05d3ba9b6', true, null).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200) {
        this.fullListInterbranchRequisitionModel = response.resources;
        this.createRequisitionDataTable(this.fullListInterbranchRequisitionModel['items']);
        //
        this.requisitionCreationForm.get('warehouseId').setValue(this.fullListInterbranchRequisitionModel['warehouseId']);
        this.requisitionCreationForm.get('requisitionDate').setValue(this.fullListInterbranchRequisitionModel['requisitionDate']);
        this.requisitionCreationForm.get('stockOrderTypeId').setValue(this.fullListInterbranchRequisitionModel['stockOrderTypeId']);
        this.requisitionCreationForm.get('stockOrderIds').setValue(this.fullListInterbranchRequisitionModel['stockOrderNumbers']);
        this.requisitionCreationForm.get('priorityId').setValue(this.fullListInterbranchRequisitionModel['priorityId']);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  createRequisitionCRUDForm(reqCreationListModel?: RequisitionListCreationModel) {
    let requisitionCreationModel = new RequisitionListCreationModel(reqCreationListModel);
    this.requisitionCreationForm = this.formBuilder.group({});
    let stockItemsFormArray = this.formBuilder.array([]); // [], Validators.required
    Object.keys(requisitionCreationModel).forEach((key) => {
      if (typeof requisitionCreationModel[key] !== 'object') {
        this.requisitionCreationForm.addControl(key, new FormControl(requisitionCreationModel[key]));
      } else {
        this.requisitionCreationForm.addControl(key, stockItemsFormArray);
      }
    });
  }

  get stockItemsFormArray(): FormArray {
    if (this.requisitionCreationForm !== undefined) {
      return (<FormArray>this.requisitionCreationForm.get('Items'));
    }
  }

  createRequisitionDataTable(stockCodesModel) {
    // Create variable to store list of stock orders for comparison
    let stockCodesDetailsArray = this.stockItemsFormArray;
    for (const stockCode of stockCodesModel) {
      stockCodesDetailsArray.push(this.formBuilder.group({
        'stockCode': stockCode.stockCode,
        'stockDescription': stockCode.stockDescription,
        'requestedQuantity': stockCode.requestedQty,
        'requiredQuantity': stockCode.requiredQty,
        'stockOrderNo': stockCode.stockOrderNo,
        'minimumThreshold': stockCode.minimumThreshold,
        'maximumThreshold': stockCode.maximumThreshold,
        'stockOnHand': stockCode.stockOnHand,
        'ibtAvailableQty': stockCode.ibtAvailableQty,
        'ibtOrderQty': stockCode.ibtOrderQty,
        'requisitionQty': stockCode.requisitionQty,
        'stockId': stockCode.stockId,
        'stockOrderId': stockCode.stockOrderId ? stockCode.stockOrderId : null
      }));
    }
  }

  createRequisition() {
    this.requisitionCreationForm.get('createdUserId').setValue(this.loggedUser.userId);
    this.requisitionCreationForm.get('modifiedUserId').setValue(this.loggedUser.userId);
    this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.REQUISITION_LIST,
      this.requisitionCreationForm.value
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {

      }
    })
  }

}
