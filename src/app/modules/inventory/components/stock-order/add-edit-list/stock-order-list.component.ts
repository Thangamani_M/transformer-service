import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@app/modules';
import { AppState } from '@app/reducers';
import {
  ComponentProperties, CrudType, IApplicationResponse, LoggedInUserModel,currentComponentPageBasedPermissionsSelector$,
  ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService, PERMISSION_RESTRICTION_ERROR
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { RequisitionListFilterModel } from '@modules/inventory/models/RequisitionListFilter.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: 'app-stock-order-list',
  templateUrl: './stock-order-list.component.html',
  styleUrls: ['./stock-order-list.component.scss'],
})
export class StockOrderListComponent extends PrimeNgTableVariablesModel{
  componentProperties = new ComponentProperties();
  primengTableConfigProperties: any;
  row: any = {};
  dateFormat = 'MMM dd, yyyy';
  showFilterForm = false;
  warehouseId: any;
  warehouseList = [];
  requisitionFilterForm: FormGroup;
  stockOrderList = [];
  stockCodeList = [];
  statusList = [];
  startTodayDate = 0;
  userData: UserLogin;
  filteredData: any={};
  otherParams;
  filterdNewData;
  constructor(
    private snackbarService: SnackbarService,private crudService: CrudService, private formBuilder: FormBuilder, private snakbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    public dialogService: DialogService, private router: Router,
    private store: Store<AppState>,private rxjsService: RxjsService) {
     super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Requisition",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Ordering', relativeRouterUrl: '' },{ displayName: 'Requisition List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Requisition List',
            dataKey: 'stockId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            enableAddActionBtn: true,
            enableAction: true,
            enableEditActionBtn: false,
           shouldShowFilterActionBtn: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Requisition-List',
            printSection: 'print-section0',
            columns: [
              { field: 'requisitionNumber', header: 'Requisition ID', width: '130px' },
              { field: 'rqnStatus', header: 'Status', width: '160px' },
              { field: 'requisitionRequestNumber', header: 'Requisition Request Number', width: '150px' },
              { field: 'requisitionDate', header: 'Requisition Date', width: '160px' },
              { field: 'warehouse', header: 'Warehouse', width: '200px' },
              { field: 'storageLocationName', header: 'Storage Location Name', width: '200px' },
              { field: 'orderType', header: 'Order Type', width: '130px' },
              { field: 'requisitioner', header: 'Requisitioner', width: '200px' },
              { field: 'approval', header: 'Approval', width: '200px' },
              { field: 'priority', header: 'Priority', width: '200px' },
              { field: 'approvalDate', header: 'Approval Date', width: '160px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.REQUISITION,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            exportApiSuffixModel: InventoryModuleApiSuffixModels.REQUISITION_EXPORT,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.createRequisitionFilterForm();

  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.REQUISITION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getRequiredListData(pageIndex?: string, pageSize?: string,otherParams?: any) {

    this.row['pageIndex']= pageIndex?pageIndex:0;
    this.row['pageSize']= pageSize?pageSize:10;
    this.loading = true;
    otherParams =  {...otherParams,...{ UserId: this.userData.userId}};
    this.otherParams = otherParams;
    delete otherParams?.pageIndex;

    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.requisitionDate = val.requisitionDate ? this.datePipe.transform(val.requisitionDate, 'yyyy-MM-dd HH:mm:ss') : '';
          val.approvalDate = val.approvalDate ? this.datePipe.transform(val.approvalDate, 'yyyy-MM-dd HH:mm:ss') : '';
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row['UserId'] = this.userData.userId;
        this.row['pageIndex']= row["pageIndex"];
        this.row['pageSize']= row["pageSize"];
        this.row = row;
        unknownVar = {...unknownVar,...this.filterdNewData};
        delete unknownVar?.pageIndex;
        this.getRequiredListData(row["pageIndex"],row["pageSize"], unknownVar)
        break;
      case CrudType.FILTER:
        this.displayAndLoadFilterData();
        this.getAllDataList();
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
        let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
        this.exportList(pageIndex,pageSize );
         // this.exportListTest();
         //  this.exportExcel();
          break;
      default:
    }
  }

  exportList(pageIndex?: any, pageSize?: any, otherParams?: object) {
    let queryParams;
    queryParams = this.generateQueryParams(otherParams,pageIndex,pageSize);
    this.crudService.downloadFile(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.REQUISITION_EXPORT,null, null, queryParams).subscribe((response: any) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response) {
          this.saveAsExcelFile(response, 'REQUISITION');
          this.rxjsService.setGlobalLoaderProperty(false);
        }
    });
  }
  generateQueryParams(queryParams,pageIndex,pageSize) {
    pageIndex=0;
    let queryParamsString;
    queryParamsString = new HttpParams({ fromObject: this.otherParams })
    .set('pageIndex',pageIndex)
    .set('pageSize',pageSize);

    queryParamsString.toString();
    return '?' + queryParamsString;
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.getRequisitionValidation('create', editableObject);
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.getRequisitionValidation('view', editableObject);
            break;
        }
        break;
    }
  }

  getAllDataList(): void {
    this.warehouseList = [];
    this.stockOrderList = [];
    this.stockCodeList = [];
    this.statusList = [];
    forkJoin([this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId })),
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCKORDERS_DROPDOWN),
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCKCODES_DROPDOWN),
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RQNSTATUS),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              let warehouseList = resp.resources;
              this.warehouseList = [];
              for (var i = 0; i < warehouseList.length; i++) {
                let tmp = {};
                tmp['value'] = warehouseList[i].id;
                tmp['display'] = warehouseList[i].displayName;
                this.warehouseList.push(tmp)
              }
              break;
            case 1:
              let stockOrderList = resp.resources;
              this.stockOrderList = [];
              for (var i = 0; i < stockOrderList.length; i++) {
                let tmp = {};
                tmp['value'] = stockOrderList[i].id;
                tmp['display'] = stockOrderList[i].displayName;
                if (stockOrderList[i].id != null || stockOrderList[i].displayName != null) {
                  this.stockOrderList.push(tmp)
                }
              }
              break;
            case 2:
              let stockCodeList = resp.resources;
              this.stockCodeList = [];
              for (var i = 0; i < stockCodeList.length; i++) {
                let tmp = {};
                tmp['value'] = stockCodeList[i].id;
                tmp['display'] = stockCodeList[i].displayName;
                this.stockCodeList.push(tmp)
              }
              break;
            case 3:
              let statusList = resp.resources;
              this.statusList = [];
              for (var i = 0; i < statusList.length; i++) {
                let tmp = {};
                tmp['value'] = statusList[i].id;
                tmp['display'] = statusList[i].displayName;
                this.statusList.push(tmp)
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
  }

  createRequisitionFilterForm(requisitionListModel?: RequisitionListFilterModel) {
    let requisitionListFilterModel = new RequisitionListFilterModel(requisitionListModel);
    this.requisitionFilterForm = this.formBuilder.group({});
    Object.keys(requisitionListFilterModel).forEach((key) => {
      if (typeof requisitionListFilterModel[key] === 'string') {
        this.requisitionFilterForm.addControl(key, new FormControl(requisitionListFilterModel[key]));
      }
    });
  }

  getRequisitionValidation(type, editableObject) {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.REQUISITION_CREATION_VALIDATION,
      this.userData.userId)
      .subscribe(response => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess && response.statusCode == 200) {
          if (!response.resources.isSuccess) {
            this.snakbarService.openSnackbar(response.resources.message, ResponseMessageTypes.WARNING);
          }
          else {
            if(type == 'create'){
              this.router.navigate(['/inventory', 'requisitions', 'stock-orders', 'add-edit'], {
                queryParams: {
                  type: 'requisition-list',
                  fromValue: response.resources.fromValue ? response.resources.fromValue : 0,
                  toValue: response.resources.toValue ? response.resources.toValue : 0
                }, skipLocationChange: true
              });
            }
            else {
              this.router.navigate(['/inventory', 'requisitions', 'stock-orders', 'requisition-view'], {
                queryParams: {
                  requisitionId: editableObject['requisitionId'],
                  userId: this.userData.userId,
                  type: 'requisition-list',
                  fromValue: response.resources.fromValue ? response.resources.fromValue : 0,
                  toValue: response.resources.toValue ? response.resources.toValue : 0,
                  referenceId: editableObject['referenceId']
                }, skipLocationChange: true
              });
            }
          }
        }
      });
  }

  getStockOrderNumber(warehouseId) {
    if (warehouseId != '') {
      this.stockOrderList = [];
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STOCKORDERS_DROPDOWN,
        undefined,
        false,
        prepareRequiredHttpParams({
          WarehouseId: warehouseId
        })
      )
      .subscribe(response => {
        let stockOrderList = response.resources;
        for (var i = 0; i < stockOrderList.length; i++) {
          let tmp = {};
          tmp['value'] = stockOrderList[i].id;
          tmp['display'] = stockOrderList[i].displayName;
          this.stockOrderList.push(tmp)
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  getSelectedStockCode(StockOrderId) {
    if (StockOrderId != '') {
      this.stockCodeList = [];
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STOCKCODES_DROPDOWN,
        undefined,
        false,
        prepareRequiredHttpParams({
          StockOrderId: StockOrderId
        })
      )
      .subscribe(response => {
        let stockCodeList = response.resources;
        for (var i = 0; i < stockCodeList.length; i++) {
          let tmp = {};
          tmp['value'] = stockCodeList[i].id;
          tmp['display'] = stockCodeList[i].displayName;
          this.stockCodeList.push(tmp)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  submitFilter() {
    this.filteredData = Object.assign({},
      this.requisitionFilterForm.get('warehouseId').value == '' || this.requisitionFilterForm.get('warehouseId').value == null ? null :
      { warehouseId: this.requisitionFilterForm.get('warehouseId').value },
      this.requisitionFilterForm.get('fromDate').value == '' || this.requisitionFilterForm.get('fromDate').value == null ? null :
      { fromDate: this.datePipe.transform(this.requisitionFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
      this.requisitionFilterForm.get('toDate').value == '' || this.requisitionFilterForm.get('toDate').value == null ? null :
      { toDate: this.datePipe.transform(this.requisitionFilterForm.get('toDate').value, 'yyyy-MM-dd') },
      this.requisitionFilterForm.get('rqnStatusId').value == '' || this.requisitionFilterForm.get('rqnStatusId').value == null ? null :
      { rqnStatusId: this.requisitionFilterForm.get('rqnStatusId').value.join() },
      this.requisitionFilterForm.get('stockOrderId').value == '' || this.requisitionFilterForm.get('stockOrderId').value == null ? null :
      { stockOrderId: this.requisitionFilterForm.get('stockOrderId').value },
      this.requisitionFilterForm.get('stockCodeId').value == '' || this.requisitionFilterForm.get('stockCodeId').value == null ? null :
      { stockCode: this.requisitionFilterForm.get('stockCodeId').value },
      { UserId: this.userData.userId }
    );
    Object.keys(this.filteredData).forEach(key => {
      if (this.filteredData[key] === "" || this.filteredData[key].length == 0) {
        delete this.filteredData[key]
      }
    });
    this.filterdNewData = Object.entries(this.filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.onCRUDRequested('get', this.filterdNewData);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.requisitionFilterForm.reset();
    this.filterdNewData = null;
    this.showFilterForm = !this.showFilterForm;
    this.getRequiredListData('','',null);
  }
}
