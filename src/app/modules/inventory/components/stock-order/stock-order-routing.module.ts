import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailedAutomatedListComponent, StockOrderAddEditComponent, StockOrderApprovalComponent, StockOrderApprovalViewComponent, StockOrderEditViewComponent, StockOrderListComponent } from '@inventory/components/stock-order';
import { FullListInterBranchReqisitionRegComponent } from './add-edit-list/fullList-interBranch-requisitionReg.component';
import { StockOrderViewComponent } from './add-edit-list/stock-order-view.component';
import { ExclutionListComponent } from './exclusion/exclution-list.component';
import { ExclutionUpdateComponent } from './exclusion/exclution-update.component';
import { ExclutionViewComponent } from './exclusion/exclution-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    {
        path: 'stock-orders', children: [
            { path: '', component: StockOrderListComponent, data: { title: 'Requisitions List' } ,canActivate:[AuthGuard] },
            { path: 'view', component: StockOrderEditViewComponent, data: { title: 'Requisitions View' },canActivate:[AuthGuard] },
            { path: 'requisition-view', component: StockOrderViewComponent, data: { title: 'Requisitions View' } ,canActivate:[AuthGuard]},
            { path: 'add-edit', component: StockOrderAddEditComponent, data: { title: 'Requisitions Add/Edit' }  ,canActivate:[AuthGuard]},
            { path: 'preview-requisition', component: FullListInterBranchReqisitionRegComponent, data: { title: 'Requisitions Add/Edit' },canActivate:[AuthGuard] },
            { path: 'detailed-list', component: DetailedAutomatedListComponent, data: { title: 'Detailed List' } ,canActivate:[AuthGuard]}
        ]
    },
    { path: 'approvals', component: StockOrderApprovalComponent, data: { title: 'Requisition Approval List' },canActivate:[AuthGuard] },
    { path: 'approvals/view', component: StockOrderApprovalViewComponent, data: { title: 'View Requisition Info' } ,canActivate:[AuthGuard]},
    { path: 'approvals/update', component: StockOrderEditViewComponent, data: { title: 'Update Requisition Info' },canActivate:[AuthGuard] },
    { path: 'exclusion', component: ExclutionListComponent, data: { title: 'Requisition Exclusion List' },canActivate:[AuthGuard] },
    { path: 'exclusion/view', component: ExclutionViewComponent, data: { title: 'View Requisition Exclusion' } ,canActivate:[AuthGuard]},
    { path: 'exclusion/update', component: ExclutionUpdateComponent, data: { title: 'Update Requisition Exclusion' },canActivate:[AuthGuard] },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class StockOrderRoutingModule { }