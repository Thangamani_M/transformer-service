import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { GenerateReportCreationFilter } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, getPDropdownData, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
// import { setTimeout } from 'timers';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
    selector: 'app-daily-staging-pay-add-edit',
    templateUrl: './daily-staging-pay-add-edit.component.html',
    styleUrls: ['./daily-staging-pay-add-edit.component.scss']
})
export class DailyStagingPayAddEditComponent extends PrimeNgTableVariablesModel implements OnInit{
    stagingBayReportId: string;
    stagingPayReportDetails: any = null;
    generateReportForm: FormGroup;
    warehouseList = [];
    referenceNumberList = [];
    receivingBarCodeList = [];
    stockCodeList = [];
    stockInfoDetails = [];
    stockFullInfoDetails = [];
    systemTypePostDTOs: FormArray;
    userData: UserLogin;
    dateFormat = 'MMM dd, yyyy';
    tabIndex: any;
    todayDate = new Date();
    selectedOptions = [];
    primengTableConfigProperties: any;
    @ViewChild('divClick',null) divClick: ElementRef;
    otherParams;
    constructor(
        private httpService: CrudService, private router: Router, private snackbarService: SnackbarService,
        private rxjsService: RxjsService, private formBuilder: FormBuilder, private datePipe: DatePipe, private store: Store<AppState>,
    ) {
        super();
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: "Generate Report",
            shouldShowBreadCrumb: false,
            selectedTabIndex: 0,
            breadCrumbItems:  [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Daily Staging Bay Report', relativeRouterUrl: '/inventory/daily-staging-bay' }, { displayName: 'Generate Report', relativeRouterUrl: '' }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Generate Report',
                        dataKey: 'itemCode',
                        enableAction: true,
                        enableBreadCrumb: true,
                        enableExportCSV: false,
                        enableExportExcel: false,
                        enableExportCSVSelected: false,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableStatusActiveAction: false,
                        enableFieldsSearch: true,
                        enableHyperLink: false,
                        enableSecondHyperLink: false,
                        enablePrintBtn: true,
                        enableExportBtn: true,
                        printTitle: 'Daily Staging Bay Report',
                        printSection: 'print-section0',
                        columns: [
                            { field: 'itemCode', header: 'Stock Code', width: '150px' },
                            { field: 'description', header: 'Stock Description', width: '200px' },
                            { field: 'quantity', header: 'Qty', width: '200px' }
                        ],
                        apiSuffixModel: InventoryModuleApiSuffixModels.STAGING_STOCK_INFO
                    },
                    {
                        caption: 'DETAILED LIST',
                        dataKey: 'itemCode',
                        enableAction: true,
                        enableBreadCrumb: true,
                        enableExportCSV: false,
                        enableExportExcel: false,
                        enableExportCSVSelected: false,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableStatusActiveAction: false,
                        enableFieldsSearch: true,
                        enableHyperLink: false,
                        enableSecondHyperLink: false,
                        enablePrintBtn: true,
                        enableExportBtn: true,
                        printTitle: 'Daily Staging Bay Report',
                        printSection: 'print-section1',
                        columns: [
                            { field: 'itemCode', header: 'Stock Code', width: '200px' },
                            { field: 'description', header: 'Stock Description', width: '200px' },
                            { field: 'serialNumber', header: 'Serial Number', width: '200px' },
                            { field: 'quantity', header: 'Qty', width: '150px' },
                            { field: 'referenceNumber', header: 'Reference Number', width: '150px' },
                            { field: 'receivingBarcode', header: 'Receiving Barcode', width: '150px' },
                        { field: 'receivingDate', header: 'Receiving Date', width: '150px', isDateTime: true }
                        ],
                        apiSuffixModel: InventoryModuleApiSuffixModels.STAGING_STOCK_INFO_DETAILS
                    }
                ]
            }
        }
    }

    ngOnInit() {
        this.combineLatestNgrxStoreData()
        this.getDropdownList();
        this.createstagingReportFilterForm();
        this.getSelectedReferenceNumber();
        this.getRequiredListData();
        this.getStockList();
    }
    combineLatestNgrxStoreData() {
        combineLatest([
          this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
          // this.store.select(loggedInUserData),
         
        ).subscribe((response) => {
          this.loggedInUserData= new LoggedInUserModel(response[0]);
          let permission = response[1][INVENTORY_COMPONENT.DAILY_STAGING_BAY_REPORT]
          if (permission) {
            let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
            this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
          }
        });
      }
    getDropdownList() {
        this.warehouseList = [];
        this.referenceNumberList = [];
        forkJoin([
            this.httpService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({userId:this.userData.userId})),
            this.httpService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PURCHASEORDER_SEARCH)
        ]).subscribe((response: IApplicationResponse[]) => {
          
            response.forEach((resp: IApplicationResponse, ix: number) => {
                if (resp.isSuccess && resp.statusCode === 200) {
                    switch (ix) {
                        case 0:
                            this.warehouseList = resp.resources;
                            // for (var i = 0; i < warehouseList?.length; i++) {
                            //     let tmp = {};
                            //     tmp['value'] = warehouseList[i].id;
                            //     tmp['display'] = warehouseList[i].displayName;
                            //     this.warehouseList.push(tmp)
                            // }
                            break;
                        case 1:
                            this.referenceNumberList = getPDropdownData(resp.resources, 'displayName', 'id')
                            break;
                    }
                }
            });
           this.rxjsService.setGlobalLoaderProperty(false);
        })
    }

    getSelectedReferenceNumber() {
        this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RECEIVING_BARCODE_LIST)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.receivingBarCodeList = [];
                    // this.receivingBarCodeList= response.resources;
                    let receivingBarCodeList = response.resources;
                    for (var i = 0; i < receivingBarCodeList?.length; i++) {
                        let tmp = {};
                        tmp['value'] = receivingBarCodeList[i].id;
                        tmp['display'] = receivingBarCodeList[i].displayName;
                        this.receivingBarCodeList.push(tmp)
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    getStockList(){
        this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEMS, undefined, true, null)
        .subscribe((response: IApplicationResponse) => {
            if (response.resources && response.statusCode === 200) {
                this.stockCodeList = getPDropdownData(response.resources, 'displayName', 'id');
                // let stockCodeList = response.resources;

                // for (var i = 0; i < stockCodeList?.length; i++) {
                //     let tmp = {};
                //     tmp['value'] = stockCodeList[i].id;
                //     tmp['display'] = stockCodeList[i].itemCode + '-' + stockCodeList[i].displayName ;
                //     this.stockCodeList.push(tmp)
                // }
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    getSelectedRecevingBarcode(stockCode): void {
        if (stockCode != '') {
            let params = new HttpParams().set('OrderReceiptBatchIds', stockCode);
            this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STAGING_STOCK_CODE_LIST, undefined, true, params)
                .subscribe((response: IApplicationResponse) => {
                    if (response.resources && response.statusCode === 200) {
                        this.stockCodeList = getPDropdownData(response.resources, 'displayName', 'id')
                        // let stockCodeList = response.resources;
                        // for (var i = 0; i < stockCodeList?.length; i++) {
                        //     let tmp = {};
                        //     tmp['value'] = stockCodeList[i].id;
                        //     tmp['display'] = stockCodeList[i].displayName;
                        //     this.stockCodeList.push(tmp)
                        // }
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                });
        }
    }

    createstagingReportFilterForm(stagingPayListModel?: GenerateReportCreationFilter) {
        let DailyStagingPayListModel = new GenerateReportCreationFilter(stagingPayListModel);
        this.generateReportForm = this.formBuilder.group({});
        Object.keys(DailyStagingPayListModel).forEach((key) => {
            if (typeof DailyStagingPayListModel[key] === 'string') {
                this.generateReportForm.addControl(key, new FormControl(DailyStagingPayListModel[key]));
            }
        });
    }

    generateReport(pageIndex?: string, pageSize?: string, otherParams?: object) {
      //  this.resetFilterOnHide =false;
        this.getRequiredListData();
    }
    generateStockReport(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.rxjsService.setFormChangeDetectionProperty(true);
        let data = Object.assign({},
            this.generateReportForm.get('WarehouseIds').value == '' || this.generateReportForm.get('WarehouseIds').value == null ? null :
                { WarehouseIds: this.generateReportForm.get('WarehouseIds').value },
            this.generateReportForm.get('PurchaseOrderIds').value == '' || this.generateReportForm.get('PurchaseOrderIds').value == null ? null :
                { PurchaseOrderIds: this.generateReportForm.get('PurchaseOrderIds').value },
            this.generateReportForm.get('OrderReceiptIds').value == '' || this.generateReportForm.get('OrderReceiptIds').value == null ? null :
                { OrderReceiptBatchIds: this.generateReportForm.get('OrderReceiptIds').value },
            this.generateReportForm.get('FromDate').value == '' || this.generateReportForm.get('FromDate').value == null ? '' :
                { FromDate: this.datePipe.transform(this.generateReportForm.get('FromDate').value, 'yyyy-MM-dd') },
            this.generateReportForm.get('ToDate').value == '' || this.generateReportForm.get('ToDate').value == null ? '' :
                { ToDate: this.datePipe.transform(this.generateReportForm.get('ToDate').value, 'yyyy-MM-dd') },
            this.generateReportForm.get('ItemIds').value == '' || this.generateReportForm.get('ItemIds').value == null ? null :
                { ItemId: this.generateReportForm.get('ItemIds').value },
        );
        Object.keys(data).forEach(key => {
            if (data[key] == "" || data[key] == null) {
                delete data[key]
            }
        });
        this.otherParams = data;
        this.httpService.get(ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.STAGING_STOCK_INFO, undefined, true,
            prepareGetRequestHttpParams(pageIndex, pageSize, data))
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.stockInfoDetails = response.resources;
                    //  this.stockFullInfoDetails = response.resources?.stagingBayReportStockInfoDetailsDTO;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    generateDetailReport(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.rxjsService.setFormChangeDetectionProperty(true);
        let data = Object.assign({},
            this.generateReportForm.get('WarehouseIds').value == '' || this.generateReportForm.get('WarehouseIds').value == null ? null :
                { WarehouseIds: this.generateReportForm.get('WarehouseIds').value },
            this.generateReportForm.get('PurchaseOrderIds').value == '' || this.generateReportForm.get('PurchaseOrderIds').value == null ? null :
                { PurchaseOrderIds: this.generateReportForm.get('PurchaseOrderIds').value },
            this.generateReportForm.get('OrderReceiptIds').value == '' || this.generateReportForm.get('OrderReceiptIds').value == null ? null :
                { OrderReceiptBatchIds: this.generateReportForm.get('OrderReceiptIds').value },
            this.generateReportForm.get('FromDate').value == '' || this.generateReportForm.get('FromDate').value == null ? '' :
                { FromDate: this.datePipe.transform(this.generateReportForm.get('FromDate').value, 'yyyy-MM-dd') },
            this.generateReportForm.get('ToDate').value == '' || this.generateReportForm.get('ToDate').value == null ? '' :
                { ToDate: this.datePipe.transform(this.generateReportForm.get('ToDate').value, 'yyyy-MM-dd') },
            this.generateReportForm.get('ItemIds').value == '' || this.generateReportForm.get('ItemIds').value == null ? null :
                { ItemId: this.generateReportForm.get('ItemIds').value },
        );
        Object.keys(data).forEach(key => {
            if (data[key] == "" || data[key] == null) {
                delete data[key]
            }
        });
        this.otherParams = data;
        this.httpService.get(ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.STAGING_STOCK_INFO_DETAILS, undefined, true,
            prepareGetRequestHttpParams(pageIndex, pageSize, data))
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    // this.stockInfoDetails = response.resources;
                    this.stockFullInfoDetails = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {

        let OrderReceiptIds =[]; let OrderReceiptIdsTemp =[];
        OrderReceiptIds = this.generateReportForm.get('OrderReceiptIds').value;
        if(OrderReceiptIds && OrderReceiptIds.length > 0){
            OrderReceiptIds.forEach(element => {
                OrderReceiptIdsTemp.push(element.value);  
            });
        }
        let WarehouseIds =[]; let WarehouseIdsTemp =[];
        WarehouseIds = this.generateReportForm.get('WarehouseIds').value;
        if(WarehouseIds && WarehouseIds.length > 0){
            WarehouseIds.forEach(element => {
                WarehouseIdsTemp.push(element.id);  
            });
        }
        this.rxjsService.setFormChangeDetectionProperty(true);
        let data = Object.assign({},
            this.generateReportForm.get('WarehouseIds').value == '' || this.generateReportForm.get('WarehouseIds').value == null ? null :
                { WarehouseIds: WarehouseIdsTemp.toString() },
            this.generateReportForm.get('PurchaseOrderIds').value == '' || this.generateReportForm.get('PurchaseOrderIds').value == null ? null :
                { PurchaseOrderIds: this.generateReportForm.get('PurchaseOrderIds').value },
            this.generateReportForm.get('OrderReceiptIds').value == '' || this.generateReportForm.get('OrderReceiptIds').value == null ? null :
                { OrderReceiptBatchIds: OrderReceiptIdsTemp.toString() },
            this.generateReportForm.get('FromDate').value == '' || this.generateReportForm.get('FromDate').value == null ? '' :
                { FromDate: this.datePipe.transform(this.generateReportForm.get('FromDate').value, 'yyyy-MM-dd') },
            this.generateReportForm.get('ToDate').value == '' || this.generateReportForm.get('ToDate').value == null ? '' :
                { ToDate: this.datePipe.transform(this.generateReportForm.get('ToDate').value, 'yyyy-MM-dd') },
            this.generateReportForm.get('ItemIds').value == '' || this.generateReportForm.get('ItemIds').value == null ? null :
                { ItemId: this.generateReportForm.get('ItemIds').value },
        );

        Object.keys(data).forEach(key => {
            if (data[key] == "" || data[key] == null) {
                delete data[key]
            }
        });
        otherParams = { ...otherParams, ...data,...{userId:this.userData.userId} };
        this.otherParams = otherParams;
        let inventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
        inventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
        this.httpService.get(ModulesBasedApiSuffix.INVENTORY, inventoryModuleApiSuffixModels,
            undefined, false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).subscribe(data => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess) {
                this.dataList = data.resources;
                this.totalRecords = data.totalCount;
            } else {
                this.dataList = null;
                this.totalRecords = 0;
            }
        })
    }
    resetForm(){
        this.generateReportForm.reset();     
        this.warehouseList=[];
        this.generateReportForm.get('WarehouseIds').setValue('');
        this.generateReportForm.get('PurchaseOrderIds').setValue('');
        this.generateReportForm.get('OrderReceiptIds').setValue('');
        this.generateReportForm.get('ItemIds').setValue('');
        this.generateReportForm.get('FromDate').setValue(null);
        this.generateReportForm.get('ToDate').setValue(null);   
        this.getDropdownList();     
        this.getRequiredListData();    
    }
    clerReset(){
      //  this.resetFilterOnHide =false;
    }
    submitFilter() {

        this.stockInfoDetails.forEach(value => {
            value.createdDate = new Date().toISOString();
            value.createdUserId = this.userData.userId;
        });

        let crudService: Observable<IApplicationResponse> = this.httpService.create(
            ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.DAILY_STAGING_PAY_REPORT, this.stockInfoDetails)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.navigateToList();
            }
            else {
                this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    onTabClicked(tabIndex) {
        this.tabIndex = tabIndex.index;
        this.selectedTabIndex = tabIndex.index;
        if (this.selectedTabIndex == 0) {
            this.generateReport();
        } else {
            this.generateDetailReport();
        }
    }
    onTabChange(e) {
        this.dataList = [];
        this.totalRecords = null;
        this.selectedTabIndex = e.index;
        this.getRequiredListData();
    }
    exportList(pageIndex?: any, pageSize?: any, otherParams?: object) {
        let queryParams; 
        otherParams = {...otherParams,...{userId:this.userData.userId},...this.otherParams}
        queryParams =  this.selectedTabIndex ==0 ? this.generateQueryParams1(otherParams,pageIndex,pageSize):this.generateQueryParams2(otherParams,pageIndex,pageSize);
        let exportapi =  this.selectedTabIndex ==0 ? InventoryModuleApiSuffixModels.DAILY_STAGING_PAY_REPORT_INFO_EXPORT : InventoryModuleApiSuffixModels.DAILY_STAGING_PAY_REPORT_DETAILS_EXPORT;
        let label = this.selectedTabIndex ==0 ? 'Daily Staging Bay Report - Generate Report' : 'Daily Staging Bay Report - Detail List';
        this.httpService.downloadFile(ModulesBasedApiSuffix.INVENTORY,
          exportapi,null, null, queryParams).subscribe((response: any) => {
            this.rxjsService.setGlobalLoaderProperty(false);
            if (response) {
              this.saveAsExcelFile(response, label);
              this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
      }
      generateQueryParams1(otherParams,pageIndex,pageSize) {
    
        let queryParamsString;
        queryParamsString = new HttpParams({ fromObject: otherParams ? otherParams : this.otherParams })
        .set('pageIndex',pageIndex)
        .set('pageSize',pageSize);
          queryParamsString.toString();
    
           return '?' + queryParamsString;
      }
      generateQueryParams2(otherParams,pageIndex,pageSize) {
    
        let queryParamsString;
        queryParamsString = new HttpParams({ fromObject: otherParams? otherParams:this.otherParams })
        .set('pageIndex',pageIndex)
        .set('pageSize',pageSize);
    
          queryParamsString.toString();
    
           return '?' + queryParamsString;
      }
    
      saveAsExcelFile(buffer: any, fileName: string): void {
        import("file-saver").then(FileSaver => {
          let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
          let EXCEL_EXTENSION = '.xlsx';
          const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
          });
          FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
        });
      }
    exportListOld() {
        if (this.dataList && this.dataList?.length != 0 && this.selectedTabIndex != 1) {
            let fileName = 'Stock List' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
            let columnNames = ["Stock Code", "Stock Description", "Quantity"];
            let header = columnNames.join(',');
            let csv = header;
            csv += '\r\n';
            this.dataList.map(c => {
                csv += [c['itemCode'], c['description'], c['quantity']].join(',');
                csv += '\r\n';
            })
            var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
            var link = document.createElement("a");
            if (link.download !== undefined) {
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", fileName);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
        else if (this.dataList?.length != 0 && this.selectedTabIndex != 0) {
            let fileName = 'Detailed List' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
            let columnNames = ["Stock Code", "Stock Description", "Serial Number", "Quantity", "Reference Number", "Receiving Barcode", "Receiving Date"];
            let header = columnNames.join(',');
            let csv = header;
            csv += '\r\n';
            this.dataList.map(c => {
                csv += [c['itemCode'], c['description'], c['serialNumber'], c['quantity'], c['referenceNumber'], c['receivingBarcode'], c['receivingDate']].join(',');
                csv += '\r\n';
            })
            var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
            var link = document.createElement("a");
            if (link.download !== undefined) {
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", fileName);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
        else {
            this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
        }
    }
    printClick(type,refernce){
        if(type == 'first'){
            this.dataList = this.dataList;
        //   this.printClickFlag=true;

        }else{
            this.dataList = this.dataList;
        //   this.printClickFlag=false;
        }
        setTimeout(() => {
            this.divClick.nativeElement.click();
           // document.getElementById('printSectionID').click();
            }, 500);
    } 
    onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
        switch (type) {
            case CrudType.GET:
                this.row = row;
                this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
                break;
            case CrudType.Export:
                if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
                    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                }
                    let pageIndex =  row ? row["pageIndex"]:0;
                    let pageSize  =  row ? row["pageSize"] : 10;
                this.exportList(pageIndex,pageSize,unknownVar)
                break;
                
        }
    }

    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
             this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }
    navigateToList() {
        this.router.navigate(['/inventory', 'daily-staging-bay']);
    }
}
