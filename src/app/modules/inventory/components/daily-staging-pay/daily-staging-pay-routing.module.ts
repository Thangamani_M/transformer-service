import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DailyStagingPayAddEditComponent, DailyStagingPayListComponent, DailyStagingPayViewComponent } from '@inventory/components/daily-staging-pay';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
    { path: '', component: DailyStagingPayListComponent, data: { title: 'Daily Staging Bay List' },canActivate:[AuthGuard] },
    { path: 'view', component: DailyStagingPayViewComponent, data: { title: 'Daily Staging Bay View' } ,canActivate:[AuthGuard]},
    { path: 'add-edit', component: DailyStagingPayAddEditComponent, data: { title: 'Daily Staging Bay Add Edit' } ,canActivate:[AuthGuard]},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class DailyStagingPayRoutingModule { }