import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DailyStagingPayListFilter } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import {CrudService, CrudType,  currentComponentPageBasedPermissionsSelector$,IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,prepareRequiredHttpParams,ResponseMessageTypes, RxjsService, SnackbarService} from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-daily-staging-pay-list',
  templateUrl: './daily-staging-pay-list.component.html',
  styleUrls: ['./daily-staging-pay-list.component.scss']
})
export class DailyStagingPayListComponent extends PrimeNgTableVariablesModel {
  userData: UserLogin;
  stagingReportFilterForm: FormGroup;
  showFilterForm = false;
  warehouseList = [];
  referenceNumberList = [];
  receivingBarCodeList = [];
  stockCodeList = [];
  dateFormat = 'MMM dd, yyyy';
  primengTableConfigProperties:any;
  row: any = {}
  startTodayDate = 0;
  otherParams;
  constructor(private commonService: CrudService,private snackbarService:SnackbarService,private router: Router,private formBuilder: FormBuilder,private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private datePipe: DatePipe,private store: Store<AppState>) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Daily Staging Bay Report",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Daily Staging Bay Report' },{ displayName: 'Automated' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Automated',
            dataKey: 'stagingBayReportId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableAction: true,
            enableEditActionBtn: false,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Daily Staging Bay Report',
            printSection: 'print-section0',
            columns: [{ field: 'reportId', header: 'Report ID' }, { field: 'warehouse', header: 'Warehouse' },
            { field: 'quantity', header: 'Quantity' }, { field: 'reportDate', header: 'Report date',isDate:true }],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: InventoryModuleApiSuffixModels.DAILY_STAGING_PAY_REPORT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          },
          {
            caption: 'Manual',
            dataKey: 'stagingBayReportId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableAction: true,
            enableEditActionBtn: false,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Daily Staging Bay Report',
            printSection: 'print-section',
            cursorLinkIndex: 0,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
          }
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.createstagingReportFilterForm();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.DAILY_STAGING_BAY_REPORT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams = {...otherParams,...{UserId: this.userData.userId}}
    this.otherParams = otherParams;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,otherParams )
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.reportDate = this.datePipe.transform(val?.reportDate, 'yyyy-MM-dd HH:mm:ss');
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList =  data.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList =  data.resources
        this.totalRecords = 0;
      }
    })
  }

  onTabChange(event) {
    if (event.index == 1) {
      this.router.navigate(['/inventory', 'daily-staging-bay', 'add-edit'], {});
      return false;
    }
    this.row = {}
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/inventory/daily-staging-bay'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData();
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row;
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        this.displayAndLoadFilterData();
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
        let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
        let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
        this.exportList(pageIndex,pageSize );
        break;
      default:
    }
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.VIEW:
        this.router.navigate(['/inventory', 'daily-staging-bay', 'view'], {
          queryParams: {
            id: editableObject['stagingBayReportId']
          }, skipLocationChange: true
        });
        break;
    }
  }

  displayAndLoadFilterData() {
    this.showFilterForm = !this.showFilterForm;
    this.warehouseList = [];
    this.referenceNumberList = [];
    forkJoin([
      this.commonService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.userData?.userId })),
      this.commonService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PURCHASEORDER_SEARCH),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              let warehouseList = resp.resources;
              for (var i = 0; i < warehouseList.length; i++) {
                let tmp = {};
                tmp['value'] = warehouseList[i].id;
                tmp['display'] = warehouseList[i].displayName;
                this.warehouseList.push(tmp)
              }
              break;
            case 1:
              let referenceNumberList = resp.resources;
              for (var i = 0; i < referenceNumberList.length; i++) {
                let tmp = {};
                tmp['value'] = referenceNumberList[i].id;
                tmp['display'] = referenceNumberList[i].displayName;
                this.referenceNumberList.push(tmp)
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })

  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
}

  createstagingReportFilterForm(stagingPayListModel?: DailyStagingPayListFilter) {
    let DailyStagingPayListModel = new DailyStagingPayListFilter(stagingPayListModel);
    this.stagingReportFilterForm = this.formBuilder.group({});
    Object.keys(DailyStagingPayListModel).forEach((key) => {
      if (typeof DailyStagingPayListModel[key] === 'string') {
        this.stagingReportFilterForm.addControl(key, new FormControl(DailyStagingPayListModel[key]));
      }
    });
  }

  getSelectedReferenceNumber(reference): void {
    this.receivingBarCodeList = [];
    if (reference != '') {
      let params = new HttpParams().set('PurchaseOrderIds', reference);
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RECEIVING_BARCODE_LIST, undefined, true, params).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let receivingBarCodeList = response.resources;
          for (var i = 0; i < receivingBarCodeList.length; i++) {
            let tmp = {};
            tmp['value'] = receivingBarCodeList[i].id;
            tmp['display'] = receivingBarCodeList[i].displayName;
            this.receivingBarCodeList.push(tmp)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  getSelectedRecevingBarcode(stockCode): void {
    this.stockCodeList = [];
    if (stockCode != '') {
      let params = new HttpParams().set('OrderReceiptBatchIds', stockCode);
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STAGING_STOCK_CODE_LIST, undefined, true, params).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let stockCodeList = response.resources;
          for (var i = 0; i < stockCodeList.length; i++) {
            let tmp = {};
            tmp['value'] = stockCodeList[i].id;
            tmp['display'] = stockCodeList[i].displayName;
            this.stockCodeList.push(tmp)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  submitFilter() {
    let data = Object.assign({},
      this.stagingReportFilterForm.get('warehouseId').value == '' ? null : { WarehouseIds: this.stagingReportFilterForm.get('warehouseId').value },
      this.stagingReportFilterForm.get('referenceNumberId').value == '' ? null : { PurchaseOrderIds: this.stagingReportFilterForm.get('referenceNumberId').value },
      this.stagingReportFilterForm.get('receivingBarcodeId').value == '' ? null : { OrderReceiptBatchIds: this.stagingReportFilterForm.get('receivingBarcodeId').value },
      this.stagingReportFilterForm.get('fromDate').value == '' ? null : { FromDate: this.datePipe.transform(this.stagingReportFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
      this.stagingReportFilterForm.get('toDate').value == '' ? null : { ToDate: this.datePipe.transform(this.stagingReportFilterForm.get('toDate').value, 'yyyy-MM-dd') },
      this.stagingReportFilterForm.get('stockCodeId').value == '' ? null : { ItemIds: this.stagingReportFilterForm.get('stockCodeId').value }, { UserId: this.userData.userId }
    );
    Object.keys(data).forEach(key => {
      if (data[key] == "" || data[key] == null) {
        delete data[key]
      }
  });
    this.getRequiredListData('', '', data);
    this.showFilterForm = !this.showFilterForm;   
  }

  resetForm() {
    this.stagingReportFilterForm.reset();
    this.getRequiredListData('', '', null);
    this.showFilterForm = !this.showFilterForm;
  }
  exportList(pageIndex?: any, pageSize?: any, otherParams?: object) {
    let queryParams;
    queryParams = this.generateQueryParams(otherParams,pageIndex,pageSize);
    let exportapi =  InventoryModuleApiSuffixModels.DAILY_STAGING_PAY_EXPORT
    let label = 'Daily Staging Bay Automated';
    this.commonService.downloadFile(ModulesBasedApiSuffix.INVENTORY,
      exportapi,null, null, queryParams).subscribe((response: any) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response) {
          this.saveAsExcelFile(response, label);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
    });
  }
  generateQueryParams(otherParams,pageIndex,pageSize) {

    let queryParamsString;
    queryParamsString = new HttpParams({ fromObject:this.otherParams })
    .set('pageIndex',pageIndex)
    .set('pageSize',pageSize);

     queryParamsString.toString();
     return '?' + queryParamsString;
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  exportListOld() {
    if (this.dataList.length != 0) {
      let fileName = 'Daily Staging Bay Report' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
      let columnNames = ["Report ID", "Warehouse", "Quantity", "Report Date"];
      let header = columnNames.join(',');
      let csv = header;
      csv += '\r\n';
      this.dataList.map(c => {
        csv += [c['reportId'], c['warehouse'], c['quantity'], c['reportDate']].join(',');
        csv += '\r\n';
      })
      var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
      var link = document.createElement("a");
      if (link.download !== undefined) {
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
    else {
      this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
    }
  }
}
