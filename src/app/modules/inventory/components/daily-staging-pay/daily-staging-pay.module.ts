import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTooltipModule } from '@angular/material';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import {
    DailyStagingPayAddEditComponent, DailyStagingPayListComponent, DailyStagingPayRoutingModule, DailyStagingPayViewComponent
} from "@inventory/components/daily-staging-pay";
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { BarcodePrinterModule } from '../barcode-printer/barcode-printer/barcode-printer.module';
@NgModule({
    declarations: [
        DailyStagingPayListComponent,DailyStagingPayViewComponent,DailyStagingPayAddEditComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule, FormsModule,
        DailyStagingPayRoutingModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
        BarcodePrinterModule,
        MatTooltipModule
    ],
    entryComponents: [
        // StockCodeInfoPopupComponent, PurchaseOrderBarcodeModalComponent, PurchaseOrderTechnicianDeletePopupComponent
    ]
})

export class DailyStagingPayModule { }
