import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { Observable } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
    selector: 'app-daily-staging-pay-view',
    templateUrl: './daily-staging-pay-view.component.html',
    styleUrls: ['./daily-staging-pay-view.component.scss']
})
export class DailyStagingPayViewComponent extends PrimeNgTableVariablesModel implements OnInit {
    DailyStagingPayviewDetail:any;
    stagingBayReportId: string;
    stagingPayReportDetails: any = null;
    stockInfoDetails = [];
    stockFullInfoDetails = [];
    dateFormat = 'MMM dd, yyyy';
    tabIndex: any;
    primengTableConfigProperties: any
    constructor(
        private httpService: CrudService, private snackbarService: SnackbarService,
        private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private datePipe: DatePipe) {
            super();
        this.stagingBayReportId = this.activatedRoute.snapshot.queryParams.id;
        this.primengTableConfigProperties = {
            tableCaption:'Daily Staging Bay Report View',
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Daily Staging Bay Report List', relativeRouterUrl: '/inventory/daily-staging-bay' }, { displayName: 'Report View', relativeRouterUrl: '', }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        enableAction: true,
                        enableEditActionBtn: false,
                        enableBreadCrumb: true,
                        enableExportBtn: true,
                        enablePrintBtn: true,
                        printTitle: 'Daily Staging Bay Report',
                        printSection: 'print-section',
                    }
                ]
            }
        }
        this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    }

    ngOnInit() {
        if (this.stagingBayReportId) {
            this.getStagingPayReportDetails().subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                    this.stagingPayReportDetails = response.resources;
                    this.onShowValue(response.resources);
                    this.stockInfoDetails = response.resources.stagingBayReportItemDTO;
                    this.stockFullInfoDetails = response.resources.stagingBayReportItemDetailsDTO;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
        }
    }

    onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
        switch (type) {
          case CrudType.GET:
            this.getStagingPayReportDetails()
            break;
          case CrudType.EXPORT:
            this.exportList();
            break;
        }
      }

    onShowValue(response?: any) {
        this.DailyStagingPayviewDetail = [
          {
            name: 'Basic Info', columns: [
              { name: 'Report ID', value: response?.reportId},
              { name: 'Warehouse', value: response?.warehouse},
              { name: 'Report Date', value: response?.reportDate},
              
            ]
          }
        ]
        
      }

    onTabClicked(tabIndex) {
        this.tabIndex = tabIndex.index;
    }

    exportList() {
        if (this.stockInfoDetails.length != 0 && this.tabIndex != 1) {
            let fileName = 'Stock List' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
            let columnNames = ["Stock Code", "Stock Description", "Quantity"];
            let header = columnNames.join(',');
            let csv = header;
            csv += '\r\n';
            this.stockInfoDetails.map(c => {
                csv += [c['itemCode'], c['description'], c['quantity']].join(',');
                csv += '\r\n';
            })
            var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
            var link = document.createElement("a");
            if (link.download !== undefined) {
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", fileName);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
        else if (this.stockFullInfoDetails.length != 0 && this.tabIndex != 0) {
            let fileName = 'Detailed List' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
            let columnNames = ["Stock Code", "Stock Description", "Serial Number", "Quantity", "Reference Number", "Receiving Barcode", "Receiving Date"];
            let header = columnNames.join(',');
            let csv = header;
            csv += '\r\n';
            this.stockFullInfoDetails.map(c => {
                csv += [c['itemCode'], c['description'], c['serialNumber'], c['quantity'], c['referenceNumber'], c['receivingBarcode'], c['receivingDate']].join(',');
                csv += '\r\n';
            })
            var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
            var link = document.createElement("a");
            if (link.download !== undefined) {
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", fileName);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
        else {
            this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
        }
    }

    getStagingPayReportDetails(): Observable<IApplicationResponse> {
        return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.DAILY_STAGING_PAY_REPORT, this.stagingBayReportId);
    }

}