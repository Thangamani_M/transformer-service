import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InventoryTaskListComponent } from './inventory-task-list/inventory-task-list.component';

const routes: Routes = [
    { path: '', component: InventoryTaskListComponent, data: { title: 'Task List' } },
]

@NgModule({
     imports: [RouterModule.forChild(routes)],
    
})

export class InventoryTaskListRoutingModule { }