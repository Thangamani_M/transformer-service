import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareDynamicTableTabsFromPermissions,
  prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { RequestType } from '@modules/technical-management/components/technical-area-manager-worklist/request-type.enum';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-inventory-task-list',
  templateUrl: './inventory-task-list.component.html',
  styleUrls: ['./inventory-task-list.component.scss']
})
export class InventoryTaskListComponent extends PrimeNgTableVariablesModel implements OnInit {

  userData: UserLogin;
  primengTableConfigProperties: any;
  row: any = {}
  stockBinMovementDialog: boolean = false;
  stockCodeDetails: string;
  binLocationDetails: string;
  inventoryTaskListId: string;
  constructor(private commonService: CrudService, private router: Router, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private store: Store<AppState>,
    private datePipe: DatePipe
  ) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Task List",
      breadCrumbItems: [{ displayName: 'My Task', relativeRouterUrl: '' },
      { displayName: 'Task List', relativeRouterUrl: '' }, { displayName: 'Task List', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Inventory',
            dataKey: 'stockDiscrepancyId',
            enableAction: true,
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableExportBtn: true,
            columns: [
              { field: 'requestNumber', header: 'Request Number', width: '150px' },
              { field: 'createrRole', header: 'Role', width: '150px' },
              { field: 'customerId', header: 'Customer ID', width: '150px' },
              { field: 'debtors', header: 'Debtors Code', width: '150px' },
              { field: 'customerDescription', header: 'Customer Description', width: '150px' },  
              { field: 'reference', header: 'Reference', width: '150px' },
              { field: 'requestType', header: 'Request Type', width: '150px' },
              { field: 'requestMotivation', header: 'Request Motivation', width: '150px' },
              { field: 'value', header: 'Value', width: '100px' },                 
              { field: 'dueDate', header: 'Due Date', width: '100px', type: 'date' },
              { field: 'createdUser', header: 'Created By', width: '150px' },
              { field: 'createrRole', header: 'Creator Role', width: '150px' },
              { field: 'status', header: 'Status', width: '150px' },
              { field: 'division', header: 'Division', width: '150px' },
              { field: 'districtName', header: 'District', width: '150px' },
              { field: 'branch', header: 'Branch', width: '150px' },
              { field: 'subArea', header: 'Sub Area', width: '150px' },
              { field: 'techArea', header: 'Tech Area', width: '150px' },
              { field: 'salesArea', header: 'Sales Area', width: '150px' },
              { field: 'address', header: 'Address', width: '150px' },      
              { field: 'createdDate', header: 'Created Date', width: '150px', type: 'date' },
              { field: 'currentLevel', header: 'Current Approval Level', width: '150px' },
              { field: 'lastApprovalDoneBy', header: 'Last Approval Done By', width: '150px' },
              { field: 'lastApprovalDoneDate', header: 'Last Approval Date/Time', width: '150px', type: 'date' },
              { field: 'escalated', header: 'Escalated', width: '150px', type: 'dropdown', options: [{ label: 'Yes', value: true }, { label: 'No', value: false }] },
              { field: 'actionDate', header: 'Actioned Date', width: '150px', type: 'date' },
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.TASK_LIST,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  // combineLatestNgrxStoreData() {
  //   combineLatest(
  //     this.store.select(loggedInUserData)
  //   ).subscribe((response) => {
  //     this.loggedInUserData = new LoggedInUserModel(response[0]);
  //   });
  // }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.INVENTORY]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {

    if (this.userData.warehouseId) {
      otherParams = {
        ...otherParams, ...{
          IsAll: true,
          CreatedUserId: this.userData.userId,
          WarehouseId: this.userData.warehouseId
        }
      }
    } else {
      otherParams = {
        ...otherParams, ...{
          IsAll: true,
          CreatedUserId: this.userData.userId
        }
      }
    }

    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.dueDate = this.datePipe.transform(val?.dueDate, 'yyyy-MM-dd HH:mm:ss');
          val.actionDate = this.datePipe.transform(val?.actionDate, 'yyyy-MM-dd HH:mm:ss');
          val.createdDate = this.datePipe.transform(val?.createdDate, 'yyyy-MM-dd HH:mm:ss');
          val.lastApprovalDoneDate = this.datePipe.transform(val?.lastApprovalDoneDate, 'yyyy-MM-dd HH:mm:ss');
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        unknownVar['IsAll'] = true;
        unknownVar['CreatedUserId'] = this.userData.userId;
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canExport) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
           }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
          let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
          this.exportList(pageIndex,pageSize );
        break;
      default:
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    if(editableObject['url']){
      this.navigateByUrl(editableObject);
    }else {
      switch (type) {
        case CrudType.VIEW:
          if (editableObject['type']) {
            if (RequestType.STOCK_BIN_MOVEMOVEMENT == editableObject['type']?.toLowerCase()) {
              this.stockBinMovementDialog = true;
              this.stockCodeDetails = editableObject['requestNumber'];
              this.binLocationDetails = editableObject['BinDetails'];
              this.inventoryTaskListId = editableObject['inventoryTaskListId'];
            }
            else if (RequestType.REQUISITION_APPROVAL == editableObject['type']?.toLowerCase() && editableObject['requestType'] != 'Accepted Inter Branch Transfer' && editableObject['requestType'] != 'Inter Branch Transfer Accepted' && editableObject['requestType'] != 'Manual Receive/Receipt/Issue Stock') {
              this.navigateToRequisiton(editableObject);
            }
            else if (RequestType.FAULTY == editableObject['type']?.toLowerCase()) {
              this.router.navigate(['/inventory/return-for-repair-request/update'], {
                queryParams: { type: 'task-list', id: editableObject['referenceId'] }
              });
            }
            else if (RequestType.RADIO_REMOVAL_TRANSFER_REQUEST == editableObject['type']?.toLowerCase()) {
              this.router.navigate(['/inventory', 'transfer-request', 'add-edit'], {
                queryParams: {
                  id: editableObject['referenceId'],
                  type: editableObject['currentLevel'] == 1 || editableObject['currentLevel'] == 2 ? 'task-list' : null,
                  currentLevel: editableObject['currentLevel'],
                }, skipLocationChange: false
              });
            }
            else if (RequestType.INTER_BRANCH_STOCKORDER == editableObject['type']?.toLowerCase() || editableObject['requestType'] == 'Accepted Inter Branch Transfer' || editableObject['requestType'] == 'Inter Branch Transfer Accepted') {
              let flag = editableObject['status'] == "Approved" ? false : true;
              if (editableObject['status'] == "Approved") {
                flag = false;
              } else if (editableObject['status'] == "Accepted") {
                flag = false;
              } else {
                flag = true;
              }
              //'/inventory/inter-branch/inter-branch-transfer/manager-view'
              this.router.navigate(['/inventory/inter-branch/inter-branch-transfer/view'], {
                queryParams: { interBranchStockOrderId: editableObject['referenceId'], isCurrentBranchRequest: flag }
              });
            }
            else if (RequestType.MANUAL_RECEIVE_RECEIPT_STOCK == editableObject['requestType']?.toLowerCase()) {
              if (editableObject['url']) {
                this.router.navigateByUrl(editableObject['url']);
              }
            }
            else if (RequestType.STOCK_ORDER_NOT_COLLECTED == editableObject['requestType']?.toLowerCase()) {
              if (editableObject['url']) {
                this.router.navigateByUrl(editableObject['url']);
              }
            }
          }
          break;
      }
    }
    
  }
 navigateByUrl(editableObject) {
  this.router.navigateByUrl(editableObject['url']);
 }
  unassignStockCodes() {
    let sendData = {
      "inventoryTaskListId": this.inventoryTaskListId,
      "isAccept": true,
      "createdUserId": this.userData.userId
    }
    let crudService: Observable<IApplicationResponse> =
      this.commonService.update(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STOCK_BIN_MOVEMOVEMENT, sendData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.stockBinMovementDialog = false;
        this.getRequiredListData();
      }
      else {
        this.stockBinMovementDialog = false;
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigateToRequisiton(editableObject) {
    this.router.navigate(['/inventory', 'requisitions', 'stock-orders', 'requisition-view'], {
      queryParams: {
        requisitionId: editableObject['requisitionId'],
        type: 'inventory-task-list',
        userId: editableObject['userId'],
        referenceId: editableObject['referenceId']
      }, skipLocationChange: true
    });
  }
  exportList(pageIndex?: any, pageSize?: any, otherParams?: object) {
    let queryParams;
    queryParams = this.generateQueryParams(otherParams,pageIndex,pageSize);
    let exportapi =  InventoryModuleApiSuffixModels.TASK_LIST_EXPORT
    let label = 'Task List';
    this.commonService.downloadFile(ModulesBasedApiSuffix.INVENTORY,
      exportapi,null, null, queryParams).subscribe((response: any) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response) {
          this.saveAsExcelFile(response, label);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
    });
  }
  generateQueryParams(otherParams,pageIndex,pageSize) {

    let queryParamsString;
    queryParamsString = new HttpParams({ fromObject:otherParams })
    .set('pageIndex',pageIndex)
    .set('pageSize',pageSize);

     queryParamsString.toString();
     return '?' + queryParamsString;
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
}
