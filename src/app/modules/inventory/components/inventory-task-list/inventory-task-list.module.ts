import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { InventoryTaskListRoutingModule } from './inventory-task-list-routing.module';
import { InventoryTaskListComponent } from './inventory-task-list/inventory-task-list.component';


@NgModule({
    declarations: [
        InventoryTaskListComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule, FormsModule,
        InventoryTaskListRoutingModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
    ],
    entryComponents: [
    ]

})

export class InventoryTaskListModule { }
