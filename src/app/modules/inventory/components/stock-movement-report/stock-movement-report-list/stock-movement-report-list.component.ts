import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, debounceTimeForDeepNestedObjectSearchkeyword,exportList, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, SnackbarService,currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, getMatMultiData } from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory';
import { StockMovementListFilter, StockMovementSummaryListFilter, StockOnHandListFilter } from '@modules/inventory/models';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { Table } from 'primeng/table';
import { forkJoin, of,combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-stock-movement-report-list',
  templateUrl: './stock-movement-report-list.component.html',
  styleUrls: ['./stock-movement-report-list.component.scss']
})
export class StockMovementReportListComponent extends PrimeNgTableVariablesModel implements OnInit {

  searchKeyword: FormControl;
  searchForm: FormGroup;
  columnFilterForm: FormGroup;
  today: any = new Date()
  searchColumns: any
  dateFormat = 'MMM dd, yyyy';
  minDate: any;
  pageSize: number = 10;
  userData: UserLogin;
  financialYearList: any = [];
  //stock code
  isStockCodeLoading: boolean;
  stockCodeErrorMessage: any;
  showStockCodeError: boolean = false;
  isStockCodeBlank: boolean = false;
  filteredStockCodes: any = [];

  startTodayDate = 0;
  otherParams;
  isSerialNumberPopup;
  serialNumbers = [];
  //filter form
  stockonHandFilterForm: FormGroup;
  stockMovementSummaryFilterForm: FormGroup;
  stockMovementReportFilterForm: FormGroup;
  stockAgeingFilterForm: FormGroup;
  showStockonHandFilter: boolean;
  showStockMovementReportFilterForm: boolean = false;
  showStockAgeingFilterForm: boolean = false;
  stockMovementFilterList: any = {};
  multipleSubscription: any;
  filterData: any = [];
  stockCodeList: any = [];
  listSubscription: any;
  @ViewChild('dt', { static: false }) table: Table;



  constructor(private crudService: CrudService,private tableFilterFormService: TableFilterFormService,
    private activatedRoute: ActivatedRoute,private datePipe: DatePipe,private snackbarService:SnackbarService,
    private router: Router,private formBuilder: FormBuilder,
    private store: Store<AppState>, private momentService: MomentService,
    private rxjsService: RxjsService, private _fb: FormBuilder) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Stock Movement",
      breadCrumbItems: [{ displayName: 'Stock Movement Report', relativeRouterUrl: '' },
      { displayName: 'Stock Movement List', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Stock On Hand',
            dataKey: 'stockDiscrepancyId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Stock On Hand',
            printSection: 'print-section0',
            cursorLinkIndex: 0,
            columns: [{ field: 'stockCode', header: 'Stock Code', width: '100px', total: 'Grand Total' },
            { field: 'stockDescription', header: 'Stock Discreption', width: '130px', total: null },
            { field: 'techArea', header: 'Tech Area', width: '100px', total: null },
            { field: 'stockHoldingWarehouse', header: 'Stock Holding Warehouse', width: '150px', total: null },
            { field: 'stockLocation', header: 'Storage Location', width: '130px', total: null },
            { field: 'techStockLocation', header: 'Tech Stock Location', width: '130px', total: null },
            { field: 'techStockLocationName', header: 'Tech Stock Location Name', width: '150px', total: null },
            { field: 'consumable', header: 'Consumable', width: '100px', total: null },
            { field: 'serialized', header: 'Serialized', width: '100px', total: null },
            { field: 'custOrderOnHand', header: 'Customer Order On Hand', width: '150px', total: 0 },
            { field: 'custReserved', header: 'Reserved', width: '100px', total: 0 },
            { field: 'technicianOrderOnHand', header: 'Technician Order On Hand', width: '150px', total: 0 },
            { field: 'techReserved', header: 'Reserved', width: '100px', total: 0 },
            { field: 'grv', header: 'GRV', width: '100px', total: 0 },
            { field: 'stockRepairs', header: 'Stock Repairs', width: '100px', total: 0 },
            { field: 'stockOnHand', header: 'Stock On Hand', width: '100px', total: 0 },
            { field: 'unitPrice', header: 'Unit Price(R)', width: '100px', total: 0 },
            { field: 'totalValue', header: 'Total Value(R)', width: '100px', total: 0 },
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            shouldShowFilterActionBtn: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_MOVEMENT_STOCKONHAND,
            filterApiSuffixModel: InventoryModuleApiSuffixModels.STOCK_MOVEMENT_STOCKONHAND_FILTER,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          },
          {
            caption: 'Stock Movement Summary',
            dataKey: 'itemId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            shouldShowFilterActionBtn: true,
            printTitle: 'Stock Movement Summary',
            printSection: 'print-section1',
            cursorLinkIndex: 0,
            columns: [{ field: 'stockCode', header: 'Stock Code', width: '200px' },
            { field: 'stockDescription', header: 'Stock Description', width: '200px' },
            { field: 'grandTotal', header: 'Grand Total' , width: '200px'},
            { field: 'customerOrderTrf', header: 'Customer Order Trf', width: '200px' },
            { field: 'customerOrderInvoice', header: 'Customer Order Invoice' , width: '200px'},
            { field: 'techOrderTrf', header: 'Tech Order Trf', width: '200px' },
            { field: 'techOrderInvoice', header: 'Tech Order Invoice' , width: '200px'},
            { field: 'stockReturn', header: 'Stock Return', width: '200px' },
            { field: 'stockWriteOff', header: 'Stock Write Off', width: '200px' },
            { field: 'consumableExpensed', header: 'Consumable Expensed' , width: '200px'},
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            rowExpantable: true,
            rowExpantableIndex: 0,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_MOVEMENT_SUMMARY,
            filterApiSuffixModel: InventoryModuleApiSuffixModels.STOCK_MOVEMENT_SUMMARY_FILTER,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          },
          {
            caption: 'Stock Movement Report',
            dataKey: 'stockDiscrepancyId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: false,
            enableExportBtn: true,
            enablePrintBtn: true,
            shouldShowFilterActionBtn: true,
            printTitle: 'Stock Movement Report',
            printSection: 'print-section2',
            cursorLinkIndex: 0,
            columns: [{ field: 'issuingWarehouse', header: 'Issuing Warehouse', width: '200px' },
            { field: 'issuingLocation', header: 'Issuing Location', width: '200px' },
            { field: 'issuingLocationName', header: 'Issuing Location Name', width: '200px' },
            { field: 'receivingWarehouse', header: 'Receiving Warehouse', width: '200px' },
            { field: 'receivingLocation', header: 'Receiving Location', width: '200px' },
            { field: 'receivingLocationName', header: 'Receiving Location Name', width: '200px' },
            { field: 'vendorNumber', header: 'Vendor Number', width: '200px' },
            { field: 'stockCode', header: 'Stock Code', width: '80px' },
            { field: 'stockDescription', header: 'Stock Description', width: '200px' },
            { field: 'movementMode', header: 'SAP/CRM', width: '200px', type: 'dropdown', options: [{ label: 'SAP', value: 'sap' }, { label: 'CRM', value: 'crm' }] },
            { field: 'movementType', header: 'Movement Type', width: '200px' },
            { field: 'movementDescription', header: 'Movement Description', width: '200px' },
            { field: 'postingDate', header: 'Posting Date', width: '140px' },
            { field: 'processedBy', header: 'Processed By', width: '200px' },
            { field: 'referenceDocument', header: 'Reference Document', width: '200px' },
            { field: 'specialStock', header: 'Special Stock', width: '80px' },
            { field: 'unitOfMeasure', header: 'Unit of Measure', width: '80px' },
            { field: 'serialNumber', header: 'Serial Number', width: '80px' },
            { field: 'balance', header: 'Balance', width: '60px' },
            { field: 'quantity', header: 'Qty', width: '50px' },
            { field: 'amountInLC', header: 'Amount InLC', width: '200px' },
            { field: 'financialPeriod', header: 'Financial Period', width: '200px' },
            { field: 'sapBatchNumber', header: 'SAP Batch Number', width: '200px' },
            { field: 'consumable', header: 'Consumable', type: 'dropdown', options: [{ label: 'YES', value: true }, { label: 'NO', value: false }], width: '100px' },
            { field: 'reserved', header: 'Reserved', type: 'dropdown', options: [{ label: 'YES', value: true }, { label: 'NO', value: false }], width: '100px' },
            { field: 'faulty', header: 'Faulty Flag', type: 'dropdown', options: [{ label: 'YES', value: true }, { label: 'NO', value: false }], width: '100px' },
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_MOVEMENT,
            filterApiSuffixModel: InventoryModuleApiSuffixModels.STOCK_MOVEMENT_FILTER,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          },
          {
            caption: 'Stock Ageing Greater Than 5',
            dataKey: 'stockDiscrepancyId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            shouldShowFilterActionBtn: true,
            printTitle: 'Stock Ageing Greater Than 5',
            printSection: 'print-section3',
            cursorLinkIndex: 0,
            columns: [{ field: 'location', header: 'Location', width: '80px' },
            { field: 'locationName', header: 'Location Name', width: '80px' },
            { field: 'techArea', header: 'Tech Area', width: '80px' },
            { field: 'movementType', header: 'Movement Type', width: '100px' },
            { field: 'movementDescription', header: 'Movement Description', width: '100px' },
            { field: 'referenceDocument', header: 'Reference Document', width: '100px' },
            { field: 'stockCode', header: 'Stock Code', width: '80px' },
            { field: 'stockDescription', header: 'Stock Description', width: '150px' },
            { field: 'serialNumber', header: 'Serial Number', width: '80px' },
            { field: 'qty', header: 'Qty', width: '50px' },
            { field: 'postingDate', header: 'Posting Date', width: '140px' },
            { field: 'ageingInWorkingDays', header: 'Ageing In Working Days', width: '100px' },
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_MOVEMENT_AGEING,
            filterApiSuffixModel: InventoryModuleApiSuffixModels.STOCK_MOVEMENT_AGEING_FILTER,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          },
        ]
      }
    }
    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.getRequiredListData();
    this.initStockonHandFilterForm();
    if (this.selectedTabIndex == 1) {
      this.initStockMovementSummaryFilterForm();
      this.displayAndLoadFilterData();
    }
    if (this.selectedTabIndex == 2 || this.selectedTabIndex == 3) {
      this.stockFilterFunction();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.STOCK_MOVEMENT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams = { ...otherParams, ...{ isAll: true, UserId: this.userData.userId } }
    this.otherParams = otherParams;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.listSubscription && !this.listSubscription?.closed) {
      this.listSubscription.unsubscribe();
    }
    this.listSubscription = this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          if (this.selectedTabIndex == 2) {
            val.postingDate = this.datePipe.transform(val?.postingDate, 'yyyy-MM-dd');
          }
          if (this.selectedTabIndex == 3) {
            val.postingDate = this.datePipe.transform(val?.postingDate, 'yyyy-MM-dd HH:mm:ss');
          }
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        if (this.selectedTabIndex == 0) {
          var custOrderOnHand = 0;
          var custReserved = 0;
          var technicianOrderOnHand = 0;
          var techReserved = 0;
          var grv = 0;
          var stockRepairs = 0;
          var stockOnHand = 0;
          var unitPrice = 0;
          var totalValue = 0;
          this.dataList.forEach(element => {
            custOrderOnHand += element.custOrderOnHand;
            custReserved += element.custReserved;
            technicianOrderOnHand += element.technicianOrderOnHand;
            techReserved += element.techReserved;
            grv += element.grv;
            stockRepairs += element.stockRepairs;
            stockOnHand += element.stockOnHand;
            unitPrice += element.unitPrice;
            totalValue += element.totalValue;
          });
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[9].total = custOrderOnHand;
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[10].total = custReserved;
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[11].total = technicianOrderOnHand;
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[12].total = techReserved;
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[13].total = grv;
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[14].total = stockRepairs;
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[15].total = stockOnHand;
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[16].total = unitPrice?.toFixed(2);
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns[17].total = totalValue?.toFixed(2);
        }
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = []
        this.totalRecords = 0;
      }
    })
  }

  loadPaginationLazy(event) {
    let row = {};
    this.first = event.first;
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.onCRUDRequested(CrudType.GET, row, this.otherParams);
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row;
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (this.row) {
          if (Object.keys(this.row)?.length > 0) {
            // logic for split columns and its values to key value pair
            if (this.row['searchColumns']) {
              Object.keys(this.row['searchColumns']).forEach((key) => {
                if (key.toLowerCase().includes('date')) {
                  otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
                } else {
                  otherParams[key] = this.row['searchColumns'][key];
                }
              });
            }
            if (this.row['sortOrderColumn']) {
              otherParams['sortOrder'] = this.row['sortOrder'];
              otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
            }
          }
        }
        otherParams['UserId'] = this.userData.userId;
        if (otherParams['consumable'] && this.selectedTabIndex!=0) {
          otherParams['consumable'] = otherParams['consumable'].value ? 1 : 0;
        }
        if (otherParams['reserved']) {
          otherParams['reserved'] = otherParams['reserved'].value ? 'yes' : 'no';
        }
        if (otherParams['faulty']) {
          otherParams['faulty'] = otherParams['faulty'].value ? 'yes' : 'no';
        }
        //movementMode
        if (otherParams['movementMode']) {
          otherParams['movementMode'] = otherParams['movementMode'].value.toLowerCase();
        }
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        otherParams = { ...unknownVar, ...otherParams, ...this.filterData }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        switch (this.selectedTabIndex) {
          case 0:
            this.serialInfoList(row);
            break;
        }
        break;
      case CrudType.FILTER:
        this.openAddEditPage(CrudType.FILTER, row);
        break;
      case CrudType.EXPORT:
          this. export();
        break;        
      default:
    }
  }

  onTabChange(event) {
    this.row = {};
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.resetData();
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    this.router.navigate(['/inventory/stock-movement-report'], { queryParams: { tab: this.selectedTabIndex } });
    this.row = { pageIndex: 0, pageSize: this.pageSize };
    this.onCRUDRequested('get', this.row);
    if (this.selectedTabIndex == 1) {
      this.initStockMovementSummaryFilterForm();
      this.displayAndLoadFilterData();
    }
    if (this.selectedTabIndex == 2 || this.selectedTabIndex == 3) {
      this.stockFilterFunction();
    }
  }

  displayFn(val) {
    if (val) { return val?.displayName ? val?.displayName : val; } else { return val }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.FILTER:
        switch (this.selectedTabIndex) {
          case 0:
            this.showStockonHandFilter = !this.showStockonHandFilter;
            break;
          case 2:
            this.showStockMovementReportFilterForm = !this.showStockMovementReportFilterForm;
            break;
          case 3:
            this.showStockAgeingFilterForm = !this.showStockAgeingFilterForm;
            break;
        }
        if (this.showStockonHandFilter || this.showStockMovementReportFilterForm || this.showStockAgeingFilterForm) {
          this.displayAndLoadFilterData();
        }
        break;
    }
  }

  serialInfoList(editableObject) {
    let params = {
      ItemId: editableObject.itemId,
      warehouseId: editableObject.warehouseId,
      technicianId: editableObject.technicianId,
      techAreaId: editableObject.techAreaId
    }
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_MOVEMENT_STOCKONHAND_DETAILS,
      undefined,
      false, prepareRequiredHttpParams(params)
    ).subscribe((data: IApplicationResponse) => {
      if (data.isSuccess && data.statusCode == 200 && data.resources) {
        this.isSerialNumberPopup = true;
        this.serialNumbers = data.resources;
      } else {
        this.isSerialNumberPopup = false;
        this.serialNumbers = [];
      }
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    }, (err) => {
      this.isSerialNumberPopup = false;
      this.serialNumbers = [];
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  closeSerialPopup() {
    this.isSerialNumberPopup = false;
  }

  initStockonHandFilterForm(stockOnHandListFilter?: StockOnHandListFilter) {
    let stockOnHandListModel = new StockOnHandListFilter(stockOnHandListFilter);
    /*Picking dashboard*/
    this.stockonHandFilterForm = this.formBuilder.group({});
    Object.keys(stockOnHandListModel).forEach((key) => {
      if (typeof stockOnHandListModel[key] === 'string' || typeof stockOnHandListModel[key] == 'object') {
        this.stockonHandFilterForm.addControl(key, new FormControl(stockOnHandListModel[key]));
      }
    });
    this.inputChangeStockFilter();
  }

  initStockMovementSummaryFilterForm(stockMovementSummaryListFilter?: StockMovementSummaryListFilter) {
    let stockMovementSummaryListModel = new StockMovementSummaryListFilter(stockMovementSummaryListFilter);
    /*Picking dashboard*/
    this.stockMovementSummaryFilterForm = this.formBuilder.group({});
    Object.keys(stockMovementSummaryListModel).forEach((key) => {
      if (stockMovementSummaryListModel[key] == '' || stockMovementSummaryListModel[key] == null) {
        this.stockMovementSummaryFilterForm.addControl(key, new FormControl(stockMovementSummaryListModel[key]));
      }
    });
    this.onStockMovementSummaryValueChange();
  }

  stockFilterFunction(stockListModel?: StockMovementListFilter) {
    let stockMovementsListModel = new StockMovementListFilter(stockListModel);
    /*Picking dashboard*/
    this.stockMovementReportFilterForm = this.formBuilder.group({});
    Object.keys(stockMovementsListModel).forEach((key) => {
      if (typeof stockMovementsListModel[key] === 'string') {
        this.stockMovementReportFilterForm.addControl(key, new FormControl(stockMovementsListModel[key]));
      }
    });
    /*Picking orders*/
    this.stockAgeingFilterForm = this.formBuilder.group({});
    Object.keys(stockMovementsListModel).forEach((key) => {
      if (typeof stockMovementsListModel[key] === 'string') {
        this.stockAgeingFilterForm.addControl(key, new FormControl(stockMovementsListModel[key]));
      }
    });
    this.inputChangeStockFilter();
  }

  onBeforeStockCode(searchText) {
    this.filteredStockCodes = [];
    this.stockCodeErrorMessage = '';
    if (searchText) {
      this.isStockCodeLoading = true;
      this.rxjsService.setGlobalLoaderProperty(true);
      if (typeof searchText == 'object') {
        searchText = searchText?.displayName;
      }
      return this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.UX_ITEM_DESCRIPTION, null, true,
        prepareGetRequestHttpParams(null, null, { searchText }))
    }
    else {
      this.isStockCodeLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      return this.filteredStockCodes = [];
    }
  }

  onAfterStockCode(response) {
    this.rxjsService.setGlobalLoaderProperty(false);
    if (response.isSuccess && response.statusCode == 200 && response.resources.length > 0) {
      this.filteredStockCodes = response.resources;
      this.stockCodeErrorMessage = '';
      this.showStockCodeError = false;
    }
    else {
      this.filteredStockCodes = [];
      this.stockCodeErrorMessage = 'Stock code is not available in the system';
      this.showStockCodeError = true;
    }
    this.isStockCodeLoading = false;
  }

  inputChangeStockFilter() {
    // Stock code
    if (this.selectedTabIndex == 0) {
      this.stockonHandFilterForm.get('stockCode').valueChanges.pipe(debounceTime(500), distinctUntilChanged(),
        switchMap(searchText => {
          return this.onBeforeStockCode(searchText);
        })).subscribe((response: IApplicationResponse) => {
          this.onAfterStockCode(response);
        });
    }
    if (this.selectedTabIndex == 2) {
      this.stockMovementReportFilterForm.get('stockCode').valueChanges.pipe(debounceTime(500), distinctUntilChanged(),
        switchMap(searchText => {
          return this.onBeforeStockCode(searchText);
        })).subscribe((response: IApplicationResponse) => {
          this.onAfterStockCode(response);
        });
    }
    if (this.selectedTabIndex == 3) {
      this.stockAgeingFilterForm.get('stockCode').valueChanges.pipe(debounceTime(500), distinctUntilChanged(),
        switchMap(searchText => {
          return this.onBeforeStockCode(searchText);
        })).subscribe((response: IApplicationResponse) => {
          this.onAfterStockCode(response);
        });
    }
  }

  emptyStockCode() {
    switch (this.selectedTabIndex) {
      case 2:
        this.stockMovementReportFilterForm.get('stockCode').patchValue(null);
        this.filteredStockCodes = [];
        break;
      case 3:
        this.stockAgeingFilterForm.get('stockCode').patchValue(null);
        this.filteredStockCodes = [];
        break;
    }
  }

  onStockMovementSummaryValueChange() {
    this.table._rows = this.pageSize;
    this.stockMovementSummaryFilterForm?.valueChanges.pipe(
      debounceTime(100), distinctUntilChanged())?.subscribe((val => {
        if (val) {
          this.filterData = Object.assign({},
            this.stockMovementSummaryFilterForm.get('locationId').value == '' ? null : { locationId: this.stockMovementSummaryFilterForm.get('locationId').value },
            !this.stockMovementSummaryFilterForm.get('postingFromDate').value ? null : { postingFromDate: this.datePipe.transform(this.stockMovementSummaryFilterForm.get('postingFromDate').value, 'yyyy-MM-dd') },
            !this.stockMovementSummaryFilterForm.get('postingToDate').value ? null : { postingToDate: this.datePipe.transform(this.stockMovementSummaryFilterForm.get('postingToDate').value, 'yyyy-MM-dd') },
          );
          this.row = { pageIndex: 0, pageSize: this.pageSize };
          this.onCRUDRequested('get', this.row);
        }
      }))
  }


  displayAndLoadFilterData() {
    let api;
    if (this.selectedTabIndex == 0 || this.selectedTabIndex == 1 || this.selectedTabIndex == 2 || this.selectedTabIndex == 3) {
      api = [
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedTabIndex]?.filterApiSuffixModel,
          undefined, null, prepareRequiredHttpParams({ userId: this.userData?.userId })),
      ]
      // if (this.selectedTabIndex == 0 || this.selectedTabIndex == 2 || this.selectedTabIndex == 3) {
      //   this.stockCodeList = [];
      // api.push(this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCKCODES_DROPDOWN));
      // }
      switch (this.selectedTabIndex) {
        case 0:
          this.stockMovementFilterList = {
            techStockLocation: [],
            storageLocation: [],
            warehouse: [],
            techArea: [],
            goodsType: [],
          };
          break;
        case 2:
          this.stockMovementFilterList = {
            issuingWarehouse: [],
            issuingLocation: [],
            receivingWarehouse: [],
            receivingLocation: [],
            movementType: [],
          };
          this.financialYearList = [];
          api.push(this.crudService.dropdown(ModulesBasedApiSuffix.BILLING, BillingModuleApiSuffixModels.UX_BILLING_FINANCIAL_YEAR));
          break;
        case 3:
          this.stockMovementFilterList = {
            location: [],
            techArea: [],
            movementType: [],
          };
          break;
      }
      this.rxjsService.setGlobalLoaderProperty(true);
      if (this.multipleSubscription && !this.multipleSubscription?.closed) {
        this.multipleSubscription?.unsubscribe();
      }
      this.multipleSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
        response?.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp?.isSuccess && resp?.statusCode === 200) {
            switch (ix) {
              case 0:
                this.onSetMatDropdownValue(resp?.resources);
                break;
              case 1:
                //   this.stockCodeList = getMatMultiData(resp?.resources);
                //   break;
                // case 2:
                if (this.selectedTabIndex == 2) {
                  this.financialYearList = getMatMultiData(resp?.resources);
                }
                break;
            }
          }
        });
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  onSetMatDropdownValue(resources) {
    switch (this.selectedTabIndex) {
      case 0:
        this.stockMovementFilterList.techStockLocation = getMatMultiData(resources?.techStockLocation);
        this.stockMovementFilterList.storageLocation = getMatMultiData(resources?.storageLocation);
        this.stockMovementFilterList.warehouse = getMatMultiData(resources?.warehouse);
        this.stockMovementFilterList.techArea = getMatMultiData(resources?.techArea);
        this.stockMovementFilterList.goodsType = getMatMultiData(resources?.goodsType);
        break;
      case 1:
        this.stockMovementFilterList.location = resources?.location;
        break;
      case 2:
        this.stockMovementFilterList.issuingWarehouse = getMatMultiData(resources?.issuingWarehouse);
        this.stockMovementFilterList.issuingLocation = getMatMultiData(resources?.issuingLocation);
        this.stockMovementFilterList.receivingWarehouse = getMatMultiData(resources?.receivingWarehouse);
        this.stockMovementFilterList.receivingLocation = getMatMultiData(resources?.receivingLocation);
        this.stockMovementFilterList.movementType = getMatMultiData(resources?.movementType);
        break;
      case 3:
        this.stockMovementFilterList.location = getMatMultiData(resources?.location);
        this.stockMovementFilterList.techArea = getMatMultiData(resources?.techArea);
        this.stockMovementFilterList.movementType = getMatMultiData(resources?.movementType);
        break;
    }
  }

  submitFilter() {
    this.table._rows = this.pageSize;
    if (this.selectedTabIndex == 0) {
      this.filterData = Object.assign({},
        this.stockonHandFilterForm.get('techStockLocationIds').value == '' ? null : { techStockLocationIds: this.stockonHandFilterForm.get('techStockLocationIds').value },
        this.stockonHandFilterForm.get('storageLocationIds').value == '' ? null : { storageLocationIds: this.stockonHandFilterForm.get('storageLocationIds').value },
        this.stockonHandFilterForm.get('warehouseIds').value == '' ? null : { warehouseIds: this.stockonHandFilterForm.get('warehouseIds').value },
        this.stockonHandFilterForm.get('techAreaIds').value == '' ? null : { techAreaIds: this.stockonHandFilterForm.get('techAreaIds').value },
        !this.stockonHandFilterForm.get('stockCode').value ? null : { stockCodeIds: this.stockonHandFilterForm.get('stockCode').value?.id },
        this.stockonHandFilterForm.get('goodsType').value == '' ? null : { goodsType: this.stockonHandFilterForm.get('goodsType').value },
      );
      this.showStockonHandFilter = !this.showStockonHandFilter;
    }
    if (this.selectedTabIndex == 2) {
      this.filterData = Object.assign({},
        this.stockMovementReportFilterForm.get('issuingWarehouse').value == '' ? null : { IssuingWarehouseIds: this.stockMovementReportFilterForm.get('issuingWarehouse').value },
        this.stockMovementReportFilterForm.get('issuingLocation').value == '' ? null : { IssuingLocationCodeIds: this.stockMovementReportFilterForm.get('issuingLocation').value },
        this.stockMovementReportFilterForm.get('issuingLocationName').value == '' ? null : { IssuingLocationNameIds: this.stockMovementReportFilterForm.get('issuingLocationName').value },
        this.stockMovementReportFilterForm.get('receivingWarehouse').value == '' ? null : { ReceivingWarehouseIds: this.stockMovementReportFilterForm.get('receivingWarehouse').value },
        this.stockMovementReportFilterForm.get('receivingLocation').value == '' ? null : { ReceivingLocationCodeIds: this.stockMovementReportFilterForm.get('receivingLocation').value },
        this.stockMovementReportFilterForm.get('receivingLocationName').value == '' ? null : { ReceivingLocationNameIds: this.stockMovementReportFilterForm.get('receivingLocationName').value },
        !this.stockMovementReportFilterForm.get('stockCode').value ? null : { StockCodeIds: this.stockMovementReportFilterForm.get('stockCode').value?.id },
        this.stockMovementReportFilterForm.get('movementType').value == '' ? null : { MovementTypeIds: this.stockMovementReportFilterForm.get('movementType').value },
        this.stockMovementReportFilterForm.get('financialPeriod').value == '' ? null : { FinancialYear: this.stockMovementReportFilterForm.get('financialPeriod').value },
        !this.stockMovementReportFilterForm.get('balance').value ? null : { BalanceFilter: this.stockMovementReportFilterForm.get('balance').value },
        !this.stockMovementReportFilterForm.get('fromDate').value ? null : { PostingFromDate: this.datePipe.transform(this.stockMovementReportFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
        !this.stockMovementReportFilterForm.get('toDate').value ? null : { PostingToDate: this.datePipe.transform(this.stockMovementReportFilterForm.get('toDate').value, 'yyyy-MM-dd') }, { UserId: this.userData.userId }
      );
      this.showStockMovementReportFilterForm = !this.showStockMovementReportFilterForm;
    }
    else if (this.selectedTabIndex == 3) {
      this.filterData = Object.assign({},
        this.stockAgeingFilterForm.get('location').value == '' ? null : { LocationIds: this.stockAgeingFilterForm.get('location').value },
        this.stockAgeingFilterForm.get('locationName').value == '' ? null : { LocationNameIds: this.stockAgeingFilterForm.get('locationName').value },
        this.stockAgeingFilterForm.get('techArea').value == '' ? null : { TechAreaIds: this.stockAgeingFilterForm.get('techArea').value },
        !this.stockAgeingFilterForm.get('stockCode').value ? null : { StockCodeIds: this.stockAgeingFilterForm.get('stockCode').value?.id },
        !this.stockAgeingFilterForm.get('fromDate').value ? null : { PostingFromDate: this.datePipe.transform(this.stockAgeingFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
        !this.stockAgeingFilterForm.get('toDate').value ? null : { PostingToDate: this.datePipe.transform(this.stockAgeingFilterForm.get('toDate').value, 'yyyy-MM-dd') },
        this.stockAgeingFilterForm.get('movementType').value == '' ? null : { MovementTypeIds: this.stockAgeingFilterForm.get('movementType').value },
        this.stockAgeingFilterForm.get('referenceDocument').value == '' ? null : { ReferenceDocument: this.stockAgeingFilterForm.get('referenceDocument').value },
        this.stockAgeingFilterForm.get('quantity').value == '' ? null : { Qty: this.stockAgeingFilterForm.get('quantity').value },
        this.stockAgeingFilterForm.get('ageingInWorkingDays').value == '' ? null : { AgeingInWorkingDays: this.stockAgeingFilterForm.get('ageingInWorkingDays').value }, { UserId: this.userData.userId }
      );
      this.showStockAgeingFilterForm = !this.showStockAgeingFilterForm;
    }
    this.row = { pageIndex: 0, pageSize: this.pageSize };
    this.onCRUDRequested('get', this.row);
  }

  resetData() {
    switch (this.selectedTabIndex) {
      case 0:
        this.showStockonHandFilter = false;
        this.initStockonHandFilterForm();
        break;
      case 2:
        this.showStockMovementReportFilterForm = false;
        this.stockFilterFunction();
        break;
      case 3:
        this.showStockAgeingFilterForm = false;
        this.stockFilterFunction();
        break;
    }
    this.filterData = [];
    this.filteredStockCodes = [];
    this.table._rows = this.pageSize;
  }

  resetForm() {
    this.resetData();
    this.row = { pageIndex: 0, pageSize: this.pageSize };
    this.onCRUDRequested('get', this.row);
  }

  export() {
    let pageIndex = this.row && this.row["pageIndex"] ? this.row["pageIndex"] : 0;
    let pageSize = this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
    this.exportList(pageIndex, pageSize);
  }

  exportList(pageIndex?: any, pageSize?: any) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
     }
    let api;let label;
    if(this.selectedTabIndex==0){
      api = InventoryModuleApiSuffixModels.STOCK_MOVEMENT_STOCKONHAND_EXPORT;
      label = 'Stock Movement Stock On Hand';
    }
    else if (this.selectedTabIndex == 1) {
      api = InventoryModuleApiSuffixModels.STOCK_MOVEMENT_SUMMARY_EXPORT;
      label = 'Stock Movement Summary';
    }
    else if (this.selectedTabIndex == 2) {
      api = InventoryModuleApiSuffixModels.STOCK_MOVEMENT_EXPORT;
      label = 'Stock Movement Report';
    }
    else if (this.selectedTabIndex == 3) {
      api = InventoryModuleApiSuffixModels.STOCK_MOVEMENT_AGEING_EXPORT;
      label = 'Stock Movement Ageing';
    }
    exportList(this.otherParams, 0, pageSize, ModulesBasedApiSuffix.INVENTORY,
      api, this.crudService, this.rxjsService, label);
  }

  exportListOLd() {
    // if(this.dataList.length != 0){
    let fileName = 'Stock Movement Report ' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
    let columnNames = [];
    if (this.selectedTabIndex == 0) {
      columnNames = ["Stock Code", "Stock Discreption", "Tech Area", "Stock Holding Warehouse", "Storage Location", "Tech Stock Location", "Tech Stock Location Name", "Consumable", "Serialized", "Customer Order On Hand", "Reserved", "Technician Order On Hand", "Reserved", "GRV", "Stock Repairs", "Stock On Hand", "Unit Price(R)", "Total Value(R)"];
    }
    if (this.selectedTabIndex == 1) {
      columnNames = ["Stock Code", "Stock Description", "Grand Total", "Customer Order Trf", "Customer Order Invoice", "Tech Order Trf", "Tech Order Invoice", "Stock Return", "Stock Write Off", "Consumable Expensed"];
    }
    if (this.selectedTabIndex == 2) {
      columnNames = ["Issuing Warehouse", "Issuing Location", "Issuing Location Name", "Receiving Warehouse", "Receiving Location", "Receiving Location Name", "Stock Code", "Stock Description", "SAP Stock Movement", "Movement Type", "Posting Date", "Processed By", "Reference Document", "Special Stock", "Unit of Measure", "Serial Number", "Qty", "Amount in LC", "Financial Period", "SAP Batch Number", "Consumable", "Reserved", "Faulty Flag"];
    }
    if (this.selectedTabIndex == 3) {
      columnNames = ["Location", "Location Name", "Tech Area", "Movement Type", "Movement Description", "Reference Document", "Stock Code", "Stock Description", "Serial Number", "Qty", "Posting Date", "Ageing In Working Days"];
    }
    let header = columnNames.join(',')
    let csv = header;
    csv += '\r\n';
    if (this.selectedTabIndex == 0) {
      this.dataList.map(c => {
        csv += [c["stockCode"], c["stockDescription"], c["techArea"], c['stockHoldingWarehouse'], c['stockLocation'], c['techStockLocationName'], c['consumable'], c['serialized'], c['custOrderOnHand'], c['custReserved'], c['technicianOrderOnHand'], c['techReserved'], c['grv'], c['stockRepairs'], c['stockOnHand'], c['unitPrice'], c['unitPrice']].join(',');
        csv += '\r\n';
      });
    }
    if (this.selectedTabIndex == 1) {
      this.dataList.map(c => {
        csv += [c["stockCode"], c["stockDescription"], c["grandTotal"], c['customerOrderTrf'], c['customerOrderInvoice'], c['techOrderTrf'], c['techOrderInvoice'], c['stockReturn'], c['stockWriteOff'], c['consumableExpensed'], c['postingDate']].join(',');
        csv += '\r\n';
      });
    }
    if (this.selectedTabIndex == 2) {
      this.dataList.map(c => {
        csv += [c["issuingWarehouse"], c["issuingLocation"], c["issuingLocationName"], c['receivingWarehouse'], c['receivingLocation'], c['receivingLocationName'], c['stockCode'], c['stockDescription'], c['sapStockMovement'], c['movementType'], c['postingDate'], c['processedBy'], c['referenceDocument'], c['specialStock'], c['unitOfMeasure'], c['serialNumber'], c['qty'], c['amountInLC'], c['financialPeriod'], c['sapBatchNumber'], c['consumable'], c['reserved'], c['facultyFlag']].join(',');
        csv += '\r\n';
      });
    }
    if (this.selectedTabIndex == 3) {
      this.dataList.map(c => {
        csv += [c["location"], c["locationName"], c["techArea"], c['movementType'], c['movementDescription'], c['referenceDocument'], c['stockCode'], c['stockDescription'], c['serialNumber'], c['qty'], c['postingDate'], c['ageingInWorkingDays'], c['referenceDocument']].join(',');
        csv += '\r\n';
      });
    }
    var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
    var link = document.createElement("a");
    if (link.download !== undefined) {
      var url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", fileName);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }

  ngOnDestroy() {
    if (this.listSubscription) {
      this.listSubscription.unsubscribe();
    }
    if (this.multipleSubscription) {
      this.multipleSubscription.unsubscribe();
    }
  }

}
