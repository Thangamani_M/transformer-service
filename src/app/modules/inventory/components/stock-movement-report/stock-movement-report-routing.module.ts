import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StockMovementReportListComponent } from './stock-movement-report-list/stock-movement-report-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {path:'',component:StockMovementReportListComponent,data:{title:'Stock Movement Report List'},canActivate:[AuthGuard]},
  //  {path:'view',component:StockMaintenanceViewComponent,data:{title:'Stock Maintenance View'}},
  //  {path:'add-edit',component:StockMaintenanceAddEditComponent,data:{title:'Stock Maintenance Add Edit'}},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class StockMovementReportRoutingModule { }
