import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxPrintModule } from 'ngx-print';
import { StockMovementReportListComponent } from './stock-movement-report-list/stock-movement-report-list.component';
import { StockMovementReportRoutingModule } from './stock-movement-report-routing.module';

@NgModule({
  declarations: [StockMovementReportListComponent],
  imports: [
    CommonModule,
    StockMovementReportRoutingModule,
    ReactiveFormsModule,
    FormsModule, LayoutModule,
    MaterialModule,
    SharedModule,
    NgxPrintModule
  ]
})
export class StockMovementReportModule { }
