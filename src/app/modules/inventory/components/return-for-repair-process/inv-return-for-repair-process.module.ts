import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { InvReturnForRepairProcessRoutingModule } from './inv-return-for-repair-process-routing.module';
import { ReturnForRepairProcessListComponent } from './return-for-repair-process-list/return-for-repair-process-list.component';
import { ReturnForRepairProcessUpdateComponent } from './return-for-repair-process-update/return-for-repair-process-update.component';
import { ReturnForRepairProcessViewComponent } from './return-for-repair-process-view/return-for-repair-process-view.component';

@NgModule({
    declarations: [
        ReturnForRepairProcessListComponent,
        ReturnForRepairProcessViewComponent,
        ReturnForRepairProcessUpdateComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule, FormsModule,
        LayoutModule,
        InvReturnForRepairProcessRoutingModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
    ],
    entryComponents: []

})

export class InvReturnForRepairProcessModule { }
