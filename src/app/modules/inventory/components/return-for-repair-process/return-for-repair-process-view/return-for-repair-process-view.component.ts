import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-return-for-repair-process-view',
  templateUrl: './return-for-repair-process-view.component.html'
})
export class ReturnForRepairProcessViewComponent implements OnInit {
  orderReceiptId: string = '';
  returnForRepairGetDetails: any;
  returnForRepairDetail;
  loggedInUserData;
  primengTableConfigProperties: any;
  constructor(private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute, private router: Router, private store: Store<AppState>,
    private rxjsService: RxjsService, private crudService: CrudService) {
    this.orderReceiptId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Return For Repair",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Return For Repair Verification', relativeRouterUrl: '' },
      { displayName: 'Return For Repair', relativeRouterUrl: '/inventory/return-for-repair-3rd-process' },
      { displayName: 'View Return For Repair', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [{
          enableBreadCrumb: true,
          enableAction: true,
          enableEditActionBtn: true,
          enableClearfix: true,
        }]
      }
    }

    this.returnForRepairDetail = [
      { name: 'RFR Request No', value: '' },
      { name: 'Receiving ID', value: '' },
      { name: 'Receiving Barcode', value: '' },
      { name: 'QR Barcode', value: '' },
      { name: 'Created By', value: '' },
      { name: 'Created Date & Time', value: '' },
      { name: 'Tech Stock Location Name', value: '' },
      { name: 'Receiving Type', value: '' },
      { name: 'Service Call Number', value: '' },
      { name: 'Warehouse', value: '' },
      { name: 'Status', value: '' },
    ]

  }

  ngOnInit() {
    if (this.orderReceiptId) {
      let params = new HttpParams().set('OrderReceiptId', this.orderReceiptId)
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RFR_INVENTORY_DETAILS, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.returnForRepairGetDetails = response.resources;
            let getDetails = response.resources;
            this.returnForRepairDetail = [
              { name: 'RFR Request No', value: getDetails?.rtrRequestNumber },
              { name: 'Receiving ID', value: getDetails?.receivingId },
              { name: 'Receiving Barcode', value: getDetails?.receivingBarCode },
              { name: 'QR Barcode', value: getDetails?.qrCode },
              { name: 'Created By', value: getDetails?.createdBy },
              { name: 'Created Date & Time', value: response?.resources?.createdDate },
              { name: 'Tech Stock Location Name', value: getDetails?.technicianStockLocation },
              { name: 'Receiving Type', value: getDetails?.orderTypeName },
              { name: 'Service Call Number', value: getDetails?.serviceCallNumber },
              { name: 'Warehouse', value: getDetails?.warehouseName },
              { name: 'Status', value: getDetails?.status },
            ]
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
    this.combineLatestNgrxStoreData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.RETURN_FOR_REPAIR_VERIFICATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEditPage();
        break;
    }
  }

  navigateToEditPage() {
    this.router.navigate(['/inventory', 'return-for-repair-3rd-process', 'add-edit'], {
      queryParams: {
        id: this.orderReceiptId
      }, skipLocationChange: true,
    });
  }

  stockDetailListDialog: boolean = false;
  stockDetails: any = [];

  openSerialNumberPage(stockDetails: any) {
    this.stockDetails = {
      stockCode: stockDetails?.stockCode,
      stockDescription: stockDetails?.stockName,
      quantity: stockDetails?.returnedQuantity,
      serialNumbers: stockDetails?.rfrInventoryItemDetails
    }
    this.stockDetailListDialog = true;
  }

}
