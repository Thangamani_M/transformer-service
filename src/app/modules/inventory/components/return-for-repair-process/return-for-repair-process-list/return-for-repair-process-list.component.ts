import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, currentComponentPageBasedPermissionsSelector$, exportList, IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  ResponseMessageTypes,
  RxjsService,
  SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-return-for-repair-process-list',
  templateUrl: './return-for-repair-process-list.component.html'
})
export class ReturnForRepairProcessListComponent extends PrimeNgTableVariablesModel implements OnInit {
  userData: UserLogin;
  listSubscribtion: any;
  primengTableConfigProperties: any;
  row: any = {};
  first: any = 0;
  reset: boolean;
  otherParams;
  constructor(private snackbarService: SnackbarService, private commonService: CrudService, private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private datePipe: DatePipe,
    private store: Store<AppState>, private changeDetectorRef: ChangeDetectorRef) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Return For Repair List",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Return For Repair Verification', relativeRouterUrl: '' }, { displayName: '', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Return For Repair',
            dataKey: 'orderReceiptId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enablePrintBtn: true,
            printTitle: 'Return For Repair',
            printSection: 'print-section0',
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'rtrRequestNumber', header: 'RFR Request No' },
            { field: 'status', header: 'Status' },
            { field: 'serviceCallNumber', header: 'Service Call Number' },
            { field: 'qrCode', header: 'QR Code' },
            { field: 'warehouseName', header: 'Warehouse' },
            { field: 'techStockLocation', header: 'Tech Stock Location' },
            { field: 'techStockLocationName', header: 'Tech Stock Location Name' },
            { field: 'createdBy', header: 'Created By' },
            { field: 'createdDate', header: 'Created Date & Time' },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            shouldShowFilterActionBtn: false,
            enableExportBtn: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.RFR_INVENTORY,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  ngAfterViewInit(): void {
    this.changeDetectorRef.detectChanges();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.RETURN_FOR_REPAIR_VERIFICATION];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    otherParams = { ...otherParams, userId: this.userData?.userId }
    this.otherParams = otherParams;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.createdDate = this.datePipe.transform(val?.createdDate, 'dd-MM-yyyy, HH:mm:ss');
          return val;
        });
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data?.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data?.resources
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        unknownVar['UserId'] = this.userData.userId;
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        break;
      case CrudType.EXPORT:

        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let pageIndex = this.row && this.row["pageIndex"] ? this.row["pageIndex"] : 0;
        let pageSize = this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
        this.exportList(pageIndex, pageSize);
        break;
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams, 0, pageSize, ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RTR_INVENTORY_EXPORT, this.commonService, this.rxjsService, 'Return For Repair Request');
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['/inventory', 'return-for-repair-3rd-process'], {
          queryParams: {}, skipLocationChange: true
        });
        break;
      case CrudType.VIEW:
        this.router.navigate(['/inventory', 'return-for-repair-3rd-process', 'view'], {
          queryParams: {
            id: editableObject['orderReceiptId'],
          }, skipLocationChange: true
        });
        break;
    }
  }
  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }
}
