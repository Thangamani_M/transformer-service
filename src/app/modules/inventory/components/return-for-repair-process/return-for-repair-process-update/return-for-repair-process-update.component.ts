import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels, OrderReceiptItemDetailsModal, TechnicianReceivingFaultyModal } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { BarcodePrintComponent } from '../../order-receipts/barcode-print';

@Component({
  selector: 'app-return-for-repair-process-update',
  templateUrl: './return-for-repair-process-update.component.html'
})
export class ReturnForRepairProcessUpdateComponent implements OnInit {

  orderReceiptId: string = '';
  returnForRepairGetDetails: any = [];
  selectedTabIndex: number = 0;
  returnForRepairRequestScanForm: FormGroup;
  warrantyStatusDropDown: any = [];
  itemStatusDropdown: any = [];
  userData: UserLogin;
  returnForRepairStockInfoForm: FormGroup;
  stockCodeDropdown: any = [];
  confirmationPopup: boolean = false;
  confirmationIcons: boolean = false;
  popupHeader: string = '';
  stockInfoIcons: boolean = false;
  stockGetDetails: any = {};
  orderReceiptItemDetails: FormArray;
  filteredSuppliers: any = [];
  allSerialChecked: boolean = false;

  returnCustomers = [
    { value: true, name: 'Yes' },
    { value: false, name: 'No' },
  ];

  constructor(
    private activatedRoute: ActivatedRoute, private router: Router,
    private store: Store<AppState>, private snackbarService: SnackbarService, private dialog: MatDialog,
    private rxjsService: RxjsService, private crudService: CrudService, private formBuilder: FormBuilder) {
    this.orderReceiptId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createReturForRepairRequestForm();
    this.getAllDropDown();
    this.getDetails();
  }

  createReturForRepairRequestForm(): void {
    let stockOrderModel = new TechnicianReceivingFaultyModal();
    // create form controls dynamically from model class
    this.returnForRepairRequestScanForm = this.formBuilder.group({
      orderReceiptItemDetails: this.formBuilder.array([])
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.returnForRepairRequestScanForm.addControl(key, new FormControl(stockOrderModel[key]));
    });

    this.returnForRepairStockInfoForm = this.formBuilder.group({
      'stockCode': ['', Validators.required],
      'stockDescription': ['', Validators.required],
    })
  }

  //Create FormArray controls
  createReturnForRepairItemsModel(interBranchModel?: OrderReceiptItemDetailsModal): FormGroup {
    let modelData = new OrderReceiptItemDetailsModal(interBranchModel);
    let formControls = {};
    Object.keys(modelData).forEach((key) => {
      formControls[key] = [modelData[key], (key === '') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getOrderReceiptItemDetailsArray(): FormArray {
    if (!this.returnForRepairRequestScanForm) return;
    return this.returnForRepairRequestScanForm.get("orderReceiptItemDetails") as FormArray;
  }



  getDetails() {
    if (this.orderReceiptId) {
      let params = new HttpParams().set('OrderReceiptId', this.orderReceiptId)
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RFR_INVENTORY_DETAILS, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.returnForRepairGetDetails = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  getAllDropDown() {
    /* Warrantty Status dropdown */
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WARRANTY_STATUS)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.warrantyStatusDropDown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    /* Item Status dropdown */
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_REQUEST_ITEM_STATUS)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.itemStatusDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

    let params = new HttpParams().set('isConsumable', 'false').set('StockType', 'Y')
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
      TechnicalMgntModuleApiSuffixModels.BILL_OF_MATERIAL_SEARCH_BY_STOCK_CODE, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.stockCodeDropdown = [];
          let stockCodeDropdown = response.resources;
          for (var i = 0; i < stockCodeDropdown.length; i++) {
            let tmp = {};
            tmp['value'] = stockCodeDropdown[i].id;
            tmp['display'] = stockCodeDropdown[i].itemCode;
            tmp['displayName'] = stockCodeDropdown[i].displayName;
            this.stockCodeDropdown.push(tmp);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

  }

  openSerialNumberPage(getDetails: any) {
    this.selectedTabIndex = 1;
    this.stockGetDetails = getDetails;
    this.getOrderReceiptItemDetailsArray.clear();
    this.allSerialChecked = false;
    let orderReceipt = getDetails.rfrInventoryItemDetails;
    this.orderReceiptItemDetails = this.getOrderReceiptItemDetailsArray;
    if (orderReceipt.length > 0) {
      orderReceipt.forEach((receipt, index) => {

        let statusName
        statusName = receipt.supplierName;
        let suppliers = {
          displayName: typeof statusName == 'object' && statusName != null ?
            receipt.supplierName.displayName : receipt.supplierName,
          id: receipt.supplierId,
        }
        this.filteredSuppliers.push(suppliers);
        receipt['supplierName'] = suppliers;
        this.onSupplierAddressSelected(suppliers, index);
        this.orderReceiptItemDetails.push(this.createReturnForRepairItemsModel(receipt));
      });
    }
  }

  selectItemStatus(index: number, event): void {
    let statusDetail = this.itemStatusDropdown.find(x => x.id == event.target.value);
    this.getOrderReceiptItemDetailsArray.controls[index].get('itemStatusName').patchValue(statusDetail.displayName)
  }

  inputChangeSupplierFilter(text: any) {
    let otherParams = {}
    if (text.query == null || text.query == '') return;
    otherParams['searchText'] = text.query;
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_AUTOCOMPLETE, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredSuppliers = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  /* Supplier Address dropdown */
  onSupplierAddressSelected(obj: any, index: number) {
    if (obj) {
      if (obj.id == null) return;
      let params = new HttpParams().set('supplierId', obj.id);
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIER_ADDRESS, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.getOrderReceiptItemDetailsArray.controls[index].get('addressDropdown').patchValue(response.resources);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  selectedItemId: string = '';

  selectWarrantyStatus(event: any, index: number) {
    if (event.target.value == null) return;
    this.confirmationPopup = false;
    this.confirmationIcons = false;
    let status = this.warrantyStatusDropDown.filter(x => x.id == event.target.value);
    if (status.length > 0 && status[0].displayName == 'In Full warranty' &&
      this.stockGetDetails?.stockName == 'Repair Code') {
      this.confirmationPopup = true;
      this.popupHeader = 'Confirmation';
      this.confirmationIcons = true;
    }
    this.getOrderReceiptItemDetailsArray.controls[index].get('warrentyStatusName').patchValue(status[0].displayName);
    this.selectedItemId = this.getOrderReceiptItemDetailsArray.controls[index].get('itemId').value;
  }

  openSerialInfo() {
    this.confirmationPopup = true;
    this.popupHeader = 'Stock Info';
    this.confirmationIcons = false;
    this.stockInfoIcons = true;
  }

  selectAll(checked: boolean) {
    this.getOrderReceiptItemDetailsArray.controls.forEach(control => {
      checked ? control.get('scannedSerialNumber').setValue(true) : control.get('scannedSerialNumber').setValue(false);
    });
  }

  openBarcodeModal() {
    let isSelected: boolean = false;
    let printedValue = [];
    this.getOrderReceiptItemDetailsArray.value.forEach(element => {
      if (element.scannedSerialNumber) {
        isSelected = true;
        printedValue.push(element.serialNumber);
      }
    });

    if (!isSelected) {
      this.snackbarService.openSnackbar('Please select atleast one checkbox', ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.print(printedValue, 'multiple');
    }
  }

  print(printedValue: any, type: any) {
    if (printedValue.length == 0) {
      this.snackbarService.openSnackbar("Select at least one Serial Number to print", ResponseMessageTypes.WARNING);
      return;
    }
    const dialogReff = this.dialog.open(BarcodePrintComponent,
      {
        width: '400px', height: '400px', disableClose: true,
        data: { serialNumbers: type == 'multiple' ? printedValue : new Array(printedValue) }
      });
  }

  selectStockDescription(colName: any) {

    let colunms = this.returnForRepairStockInfoForm.get(colName).value;
    if (colunms == '' || colunms == null) return;
    let codes = this.stockCodeDropdown.filter(x => x.value == colunms);
    if (codes.length > 0) {
      this.returnForRepairStockInfoForm.get('stockCode').setValue(codes[0].value);
      this.returnForRepairStockInfoForm.get('stockDescription').patchValue(codes[0].value);
    }
  }

  /* Stock Info Popup Close */
  onClose() {
    this.confirmationPopup = false;
    this.returnForRepairStockInfoForm.get('stockCode').setValue('');
    this.returnForRepairStockInfoForm.get('stockDescription').patchValue('');
  }

  saveStockCode() {

    if (this.returnForRepairStockInfoForm.invalid) {
      this.returnForRepairStockInfoForm.get('stockCode').markAsTouched();
      this.returnForRepairStockInfoForm.get('stockDescription').markAsTouched();
      return;
    }

    let selectedValue = this.getOrderReceiptItemDetailsArray.value.filter(x => x.itemId == this.selectedItemId);
    if (selectedValue.length == 0) return;



    let sendParams = {
      "orderReceiptId": this.orderReceiptId,
      "rtrRequestId": this.stockGetDetails?.rtrRequestId,
      "orderReceiptItemId": selectedValue[0]?.orderReceiptItemId,
      "orderReceiptItemDetailId": selectedValue[0]?.orderReceiptItemDetailId,
      "rtrRequestItemId": selectedValue[0]?.rtrRequestItemId,
      "rtrRequestItemDetailId": selectedValue[0]?.rtrRequestItemDetailId,
      "oldItemId": selectedValue[0]?.itemId,
      "newItemId": this.returnForRepairStockInfoForm.get('stockCode').value,
      "supplierId": selectedValue[0]?.supplierId,
      "supplierAddressId": selectedValue[0]?.supplierAddressId,
      "serialNumber": selectedValue[0]?.serialNumber,
      "tempStockCode": selectedValue[0]?.tempStockCode,
      "warrentyStatusId": selectedValue[0]?.warrentyStatusId,
      "isReturnToCustomer": selectedValue[0]?.isReturnToCustomer,
      "modifiedUserId": this.userData.userId
    }

    this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RFR_INVENTORY_ITEM_WARRANTY_STATUS, sendParams)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.confirmationPopup = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

  }

  onSave() {

    if (this.returnForRepairRequestScanForm.invalid) {
      return
    }

    if (this.getOrderReceiptItemDetailsArray.value.length == 0) {
      this.snackbarService.openSnackbar('Atleast one records is required', ResponseMessageTypes.WARNING);
      return;
    }

    this.getOrderReceiptItemDetailsArray.value.forEach(element => {
      delete element.addressDropdown;
      element['warrantyStatusId'] = element.warrentyStatusId;
      element['warrantyStatusName'] = element.warrentyStatusName;
      delete element.warrentyStatusId;
      delete element.warrentyStatusName
      delete element.itemStatusName;
      delete element.type;
      if (element.supplierName) {
        element['supplierId'] = element.supplierName.id;
        element['supplierName'] = element.supplierName.displayName;
      }
    });

    const data = {
      "OrderReceiptId": this.orderReceiptId,
      "RTRRequestId": this.returnForRepairGetDetails?.rtrRequestId,
      "WarehouseId": this.returnForRepairGetDetails?.warehouseId,
      "LocationId": this.returnForRepairGetDetails?.locationId,
      "CreatedUserId": this.userData.userId,
      "rfrInventoryItems": [
        {
          "RTRRequestId": this.stockGetDetails?.rtrRequestId,
          "RTRRequestItemId": this.stockGetDetails?.rtrRequestItemId,
          "OrderReceiptId": this.stockGetDetails?.orderReceiptId,
          "OrderReceiptItemId": this.stockGetDetails?.orderReceiptItemId,
          "ItemId": this.stockGetDetails?.itemId,
          "Quantity": this.stockGetDetails?.returnedQuantity,
          "IsSerialized": this.stockGetDetails?.isNotSerialized ? true : false,
          "rfrInventoryItemsDetails": this.getOrderReceiptItemDetailsArray.value
        }
      ]
    }

    this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RFR_INVENTORY, data)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.getDetails();
          this.selectedTabIndex = 0;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

  }

  navigateToViewPage() {
    this.router.navigate(['/inventory', 'return-for-repair-3rd-process', 'view'], {
      queryParams: {
        id: this.orderReceiptId
      }, skipLocationChange: true,
    });
  }

  navigateToEditPage() {
    this.selectedTabIndex = 0;
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'return-for-repair-3rd-process'], {
      skipLocationChange: true,
    });
  }

}
