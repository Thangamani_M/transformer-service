import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReturnForRepairProcessListComponent } from './return-for-repair-process-list/return-for-repair-process-list.component';
import { ReturnForRepairProcessUpdateComponent } from './return-for-repair-process-update/return-for-repair-process-update.component';
import { ReturnForRepairProcessViewComponent } from './return-for-repair-process-view/return-for-repair-process-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
    { path: '', component: ReturnForRepairProcessListComponent, data: { title: 'Return For Repair List' }, canActivate: [AuthGuard] },
    { path: 'view', component: ReturnForRepairProcessViewComponent, data: { title: 'Return For Repair View' }, canActivate: [AuthGuard] },
    { path: 'add-edit', component: ReturnForRepairProcessUpdateComponent, data: { title: 'Return For Repair Update' }, canActivate: [AuthGuard] },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class InvReturnForRepairProcessRoutingModule { }