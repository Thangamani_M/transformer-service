import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PurchaseOrderListFilter } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import {
  CrudType,exportList, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService,currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, prepareRequiredHttpParams
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';

@Component({
  selector: "app-stock-management-list",
  templateUrl: "./stock-management-list.component.html",
  styleUrls: ["./stock-management-list.component.scss"],
})

export class StockManagementListComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties: any;
  status: any = [];
  selectedRows: string[] = [];
  searchKeyword: FormControl;
  searchForm: FormGroup
  today: any = new Date();
  stockManagementFilterForm: FormGroup;
  pageSize: number = 10;
  showFilterForm = false;
  warehouseId: any;
  warehouseIds:any;
  StorageLocationId: any;
  warehouseList: any = [];
  storageLocationList: any = [];
  dateFormat = 'MMM dd, yyyy';
  userData: UserLogin;
  viewable:boolean=false;
  otherParams;
  filterInputs;
  constructor(private snackbarService:SnackbarService,
    private crudService: CrudService, private momentService: MomentService, private datePipe: DatePipe,
    private activatedRoute: ActivatedRoute,public dialogService: DialogService,private router: Router, private store: Store<AppState>, private formBuilder: FormBuilder,private rxjsService: RxjsService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Stock Management",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Management List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Stock Management List',
            dataKey: 'stockId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            enableAction:true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Stock Management',
            printSection: 'print-section0',
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'StockCode', header: 'Stock Code', width: '200px' }, { field: 'StockDescription', header: 'Stock Description', width: '200px' },
            { field: 'Warehouse', header: 'Warehouse', width: '200px' }, { field: 'StorageLocation', header: 'Storage Location', width: '200px' },
            { field: 'ValuationClass', header: 'Valuation Class', width: '200px' }, { field: 'Condition', header: 'Condition', width: '200px' },
            { field: 'QtyOnHand', header: 'Qty On Hand', width: '200px' }, { field: 'MovingAveragePrice', header: 'Moving Average Price', width: '200px' },
            { field: 'TotalValue', header: 'Total Value', width: '200px' },],
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_LIST,
            apiExportSuffixModel: InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_EXPORT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            shouldShowFilterActionBtn: true,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.createpurchaseOrderFilterForm();
    this.displayAndLoadFilterData();
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }


  // onBreadCrumbClick(breadCrumbItem: object): void {
  //   if (breadCrumbItem.hasOwnProperty('queryParams')) {
  //     this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
  //       { queryParams: breadCrumbItem['queryParams'] })
  //   }

  //   else {
  //     this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
  //   }
  // }

  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.STOCK_MANAGEMENT];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  // searchKeywordRequest() {
  //   this.searchForm.valueChanges
  //     .pipe(
  //       debounceTime(debounceTimeForSearchkeyword),
  //       distinctUntilChanged(),
  //       switchMap(val => {
  //         return of(this.onCRUDRequested(CrudType.GET, {}));
  //       })
  //     ).subscribe();
  // }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = {...otherParams,...{  CreatedUserId: this.userData.userId}};
    let otherParamsTemp = {};
     otherParamsTemp = {...otherParams}
       otherParamsTemp['pageIndex'] = pageIndex ? pageIndex : 0;
       otherParamsTemp['maximumRows'] = pageSize  ? pageSize: 10;
    this.otherParams = otherParamsTemp;
    // this.otherParams['pageIndex'] = pageIndex ? pageIndex : 0;
    // this.otherParams['maximumRows'] = pageSize  ? pageSize: 0;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams )
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      this.dataList =  data.resources
      this.totalRecords = data.totalCount;
    })
  }

  dataExportList: any = {};

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        // unknownVar['sortOrder'] = row['sortOrder'];
        //   if (row['sortOrderColumn']) {
        //     unknownVar['sortOrderColumn'] = row['sortOrderColumn'];
        // }
        this.row=row;
        unknownVar['CreatedUserId'] = this.userData.userId;
        unknownVar = {...unknownVar,...this.filterInputs}
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
        case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      case CrudType.EXPORT:
       // this.exportExcel()
       if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
       }
       let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
       let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
       this.exportList(pageIndex,pageSize);
        // this.exportLisst();
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams,0,pageSize,ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_EXPORT,this.crudService,this.rxjsService,'Stock Management');
  }
  exportListOld() {
 //   let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_EXPORT;
   // InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiExportSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_EXPORT,
      undefined,
      false, prepareGetRequestHttpParams('0', '10', null)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        if (data.resources.length != 0) {
          let fileName = 'Stock Management ' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
          let columnNames =  ["Stock Code", "Stock Description", "Warehouse", "Storage Location","Valuation Class","Condition","QtyOnHand","MovingAveragePrice","TotalValue"];
          let header = columnNames.join(',');
          let csv = header;
          csv += '\r\n';
          data.resources.map(c => {
            csv += [c['StockCode'], c['StockDescription'], c['Warehouse'], c['StorageLocation'], c['ValuationClass'], c['QtyOnHand'],c['MovingAveragePrice'],c['QtyOnHand'], c['TotalValue']].join(',');
            csv += '\r\n';
          })
          var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
          var link = document.createElement("a");
          if (link.download !== undefined) {
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", fileName);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
          }
        }
        else {
          this.snackbarService.openSnackbar('No Records Found', ResponseMessageTypes.WARNING);
        }
      }
    })
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["inventory/stock-management/list/view"], {
              queryParams: {
                stockId: editableObject['StockId'],
                warehouseId: editableObject['WarehouseId'],
                storageLocationId: editableObject['StorageLocationId'],
                locationId: editableObject['locationId']
              }, skipLocationChange: true
            });
          //  this.router.navigate(['inventory/stock-maintenance/add-edit'], { queryParams: { id: editableObject['StockId'] }, skipLocationChange: true });
            break;
        }
    }
  }

  displayAndLoadFilterData() {
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.userData?.userId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.storageLocationList = [];
          for (var i = 0; i < response.resources.length; i++) {
            if(response.resources[i].displayName != null){
              let tmp = {};
              tmp['value'] = response.resources[i].id;
              tmp['display'] = response.resources[i].displayName;
              this.warehouseList.push(tmp);
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  createpurchaseOrderFilterForm(requisitionListModel?: PurchaseOrderListFilter) {
    let stockListFilterModel = new PurchaseOrderListFilter(requisitionListModel);
    this.stockManagementFilterForm = this.formBuilder.group({});
    Object.keys(stockListFilterModel).forEach((key) => {
      if (typeof stockListFilterModel[key] === 'string') {
        this.stockManagementFilterForm.addControl(key, new FormControl(stockListFilterModel[key]));
      }
    });
  }

  applyFilterWarehouse(filterValue: any) {
    if (filterValue != '' && filterValue != null) {
      let params = new HttpParams().set('warehouseId', this.stockManagementFilterForm.get('warehouseId').value);
      let api;
      if(this.stockManagementFilterForm.get('warehouseId').value)
        api = this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STORAGE_LOCATION, null, null, params)
      else
        api  = this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STORAGE_LOCATION, null, null, null)

      api.subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.storageLocationList = [];
            for (var i = 0; i < response.resources.length; i++) {
              if(response.resources[i].displayName != null){
                let tmp = {};
                tmp['value'] = response.resources[i].id;
                tmp['display'] = response.resources[i].displayName;
                this.storageLocationList.push(tmp);
              }
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
  }

  submitFilter(){
    let input = Object.assign({},
      this.stockManagementFilterForm.get('warehouseId').value == '' || this.stockManagementFilterForm.get('warehouseId').value == null ? null :
      { Warehouses: this.stockManagementFilterForm.get('warehouseId').value },
      this.stockManagementFilterForm.get('stockOrderId').value == '' || this.stockManagementFilterForm.get('stockOrderId').value == null  ? null :
      { StorageLocations: this.stockManagementFilterForm.get('stockOrderId').value },
      { CreatedUserId : this.userData.userId }
    );
    this.filterInputs = input;
   this.getRequiredListData('', '', input);
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm(){
    this.showFilterForm = !this.showFilterForm;
    this.getRequiredListData('','', null);
    this.stockManagementFilterForm.reset();
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }

  exportLisst() {
    const queryParams = this.generateQueryParams(this.otherParams);

    this.crudService.downloadFile(ModulesBasedApiSuffix.INVENTORY,
    InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_EXPORT, null, null, queryParams)
    .subscribe((response: IApplicationResponse) => {
      if (response) {
        this.saveAsExcelFile(response, 'stock management')
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  generateQueryParams(queryParams) {
    const queryParamsString = new HttpParams({ fromObject: queryParams }).toString();
    return '?' + queryParamsString;
  }

  exportToExcel(buffer, fileName) {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const blob: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(blob, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }


}
