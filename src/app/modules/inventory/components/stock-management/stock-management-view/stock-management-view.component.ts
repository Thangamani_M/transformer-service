import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes ,currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, SnackbarService, exportList} from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared/utils';
import { combineLatest, Observable } from 'rxjs';
import { BarcodePrintComponent } from '../../order-receipts/barcode-print';
import { select, Store } from '@ngrx/store';
import { loggedInUserData } from '@modules/others';
@Component({
  selector: 'app-stock-management-view',
  templateUrl: './stock-management-view.component.html',
  styleUrls: ['./stock-management-view.component.scss'],
})

export class StockManagementViewComponent implements OnInit {

  stockId: string;
  warehouseId: string;
  storageLocationId: string;
  locationId: string;
  stockManagementDetails: any = null;
  inputObject: any;
  stockManagementBinDetails: any;
  panelOpenState:boolean;
  primengTableConfigProperties;
  loggedInUserData;
  selectedTabIndex:any=0;
  pageLimit = [10, 25, 50, 75, 100];
  constructor(
    private httpService: CrudService, private router: Router,private snackbarService:SnackbarService,
    private dialog: MatDialog, private store: Store<AppState>,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService
  ) {
    this.stockId = this.activatedRoute.snapshot.queryParams.stockId;
    this.warehouseId = this.activatedRoute.snapshot.queryParams.warehouseId;
    this.storageLocationId = this.activatedRoute.snapshot.queryParams.storageLocationId;
    this.locationId = this.activatedRoute.snapshot.queryParams.locationId;

    this.primengTableConfigProperties = {
      tableCaption: "Stock Management",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Management List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Stock Management List',
            dataKey: 'stockId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            enableAction:true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Stock Management',
            printSection: 'print-section0',
            cursorSecondLinkIndex: 1,
            columns: []
          }
        ]
      }
    }
  }

  ngOnInit() {
    if (this.stockId) {
      this.getStockManagementDetail().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.stockManagementDetails = response.resources;
          // this.locationId = response.resources.locationId
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

      this.getStockManagementBinDetail().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.stockManagementBinDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

    }
    this.combineLatestNgrxStoreData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.STOCK_MANAGEMENT];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getStockManagementDetail(): Observable<IApplicationResponse> {
    let stockParams = new HttpParams().set('StockId', this.stockId).set('WarehouseId', this.warehouseId)
    .set('StorageLocationId', this.storageLocationId).set('LocationId', this.locationId);
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_DETAILS, null, null, stockParams);
  }

  getStockManagementBinDetail(): Observable<IApplicationResponse> {
    let binParams = new HttpParams().set('StockId', this.stockId).set('WarehouseId', this.warehouseId)
    .set('StorageLocationId', this.storageLocationId).set('LocationId', this.locationId);
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_LOCATIONBIN_STOCK_DETAILS, null, null, binParams);
  }

  openBarcodeModal(){
    const dialogReff = this.dialog.open(BarcodePrintComponent,
    { width: '400px', height: '400px', disableClose: true,
      data: { serialNumbers: new Array(this.stockManagementDetails?.stockCodeBarcode) }
    });
  }

  navigateToList() {
    this.router.navigate(["inventory/stock-management/list"], { skipLocationChange: true });
  }

  navigateToEdit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
     }
    this.router.navigate(['inventory/stock-maintenance/view'], {
      queryParams: { id: this.stockId }, skipLocationChange: true
    });
  }

  onButtonClick(){
    let otherParams = {
      locationId:this.locationId,
      itemId : this.stockId
    }
      exportList(otherParams,0,10,ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_SERIAL_NUMBER_EXPORT,this.httpService,this.rxjsService,'Stock Management');
  }

}
