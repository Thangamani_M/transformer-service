import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockManagementListComponent, StockManagementViewComponent } from '@inventory/components/stock-management';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: 'list', component: StockManagementListComponent, data: { title: 'Stock Management List' } ,canActivate:[AuthGuard]},
    { path: 'list/view', component: StockManagementViewComponent, data: { title: 'Stock Management View' } ,canActivate:[AuthGuard] },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class StockManagementRoutingModule { }