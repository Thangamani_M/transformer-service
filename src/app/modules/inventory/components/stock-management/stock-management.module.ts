import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { StockManagementListComponent, StockManagementRoutingModule, StockManagementViewComponent } from '@inventory/components/stock-management';
import { NgxPrintModule } from 'ngx-print';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { StockManagementBinViewListComponent } from './stock-management-bin-view/stock-management-bin-view-list/stock-management-bin-view-list.component';
@NgModule({
    declarations: [
        StockManagementListComponent,
        StockManagementViewComponent,
        StockManagementBinViewListComponent

    ],
    imports: [
        CommonModule,
        ReactiveFormsModule, FormsModule,
        StockManagementRoutingModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxPrintModule,
        TableModule,
        InputTextModule,
        DropdownModule,
        ButtonModule,
        DialogModule,
        TabViewModule,
        MultiSelectModule
    ],
    exports: [StockManagementListComponent]

})

export class StockManagementModule { }
