import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  RxjsService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-stock-management-bin-view-list',
  templateUrl: './stock-management-bin-view-list.component.html'
})
export class StockManagementBinViewListComponent implements OnInit {

  @Input() locationIds: string;
  @Input() stockIds: string;
  @Input() locationBinIds: string;
  @Input() pageLimit;
  userData: UserLogin;
  reset: boolean;
  searchForm: FormGroup;
  observableResponse: any;
  dataList: any
  status: any = [];
  selectedRows: string[] = [];
  selectedTabIndex: any = 0;
  totalRecords: any;
  listSubscribtion: any;
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties:any;
  columnFilterForm: FormGroup;
  loading: boolean;
  row: any = {};
  first: any = 0;

    constructor(
      private formBuilder: FormBuilder, private datePipe: DatePipe,
      private router: Router,private activatedRoute: ActivatedRoute,
      private rxjsService: RxjsService, private crudService: CrudService,
      private store: Store<AppState>, private changeDetectorRef: ChangeDetectorRef,
    ) {
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      });

      this.primengTableConfigProperties = {
        tableCaption: "",
        breadCrumbItems: [{ displayName: '', relativeRouterUrl: ''},
        { displayName: '', relativeRouterUrl: ''}],
        selectedTabIndex: 0,
        tableComponentConfigs: {
          tabsList: [
            {
              caption: '',
              dataKey: 'locationId',
              enableBreadCrumb: true,
              enableAction: true,
              enableReset: false,
              enableGlobalSearch: false,
              reorderableColumns: false,
              resizableColumns: false,
              enableScrollable: true,
              checkBox: false,
              enableRowDelete: false,
              enableFieldsSearch: false,
              enableHyperLink: false,
              cursorLinkIndex: 0,
              columns: [
                { field: 'serialNumber', header: 'Serial Number', width: '200px' },
                { field: 'supplierWarrentyExpiryDate', header: 'Supplier Warranty Expiry Date', width: '200px' },
                { field: 'agingDays', header: 'Ageing Days', width: '200px' }
              ],
              shouldShowDeleteActionBtn: false,
              enableAddActionBtn: false,
              areCheckboxesRequired: false,
              isDateWithTimeRequired: true,
              shouldShowFilterActionBtn: false,
              apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_MANAGEMENT_LOCATIONBIN_STOCK_LIST,
              moduleName: ModulesBasedApiSuffix.INVENTORY,
            }
          ]
        }
      }

      this.searchForm = this.formBuilder.group({ searchKeyword: "" });
      this.columnFilterForm = this.formBuilder.group({});

      this.activatedRoute.queryParamMap.subscribe((params) => {
        this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? params['params']['tab'] ? +params['params']['tab'] : 0 : 0;
        this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
        this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
      });

    }

    ngOnInit(): void {
      this.combineLatestNgrxStoreData()
       this.getRequiredListData();
    }

    ngOnChanges(){
      if(this.locationIds && this.stockIds)
      this.getRequiredListData();
    }

    ngAfterViewInit(): void {
      // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
      this.changeDetectorRef.detectChanges();
    }

    onActionSubmited(e: any) {
      if (e.data && !e.search && !e?.col?.toString()) {
          this.onCRUDRequested(e.type, e.data);
      } else if (e.data && e.search && !e?.col) {
          this.onCRUDRequested(e.type, e.data, e.search);
      } else if (e.type && !e.data && !e?.col) {
          this.onCRUDRequested(e.type, {});
      } else if (e.type && e.data && e?.col?.toString()) {
          this.onCRUDRequested(e.type, e.data, e?.col);
      }
    }

    combineLatestNgrxStoreData() {
      combineLatest(
        this.store.select(loggedInUserData)
      ).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      });
    }

    getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {

      this.loading = true;

      let params;
      if(this.locationBinIds){
        params = { StockId: this.stockIds, LocationId: this.locationIds, LocationBinId: this.locationBinIds }
      }
      else {
        params = { StockId: this.stockIds, LocationId: this.locationIds }
      }

      otherParams = { ...otherParams, ...params };

      let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
      InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
      this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels,
        undefined, false,
        prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
      ).pipe(map((res: IApplicationResponse) => {
        if (res?.resources) {
          res?.resources?.forEach(val => {
          val.supplierWarrentyExpiryDate =  this.datePipe.transform(val?.supplierWarrentyExpiryDate, 'yyyy-MM-dd HH:mm:ss');
            return val;
          });
        }
        return res;
      })).subscribe(data => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.observableResponse = data.resources;
          this.dataList = this.observableResponse;
          this.totalRecords = data.totalCount;
        } else {
          this.observableResponse = null;
          this.dataList = this.observableResponse
          this.totalRecords = data.totalCount;
        }
      });
    }

    onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
      switch (type) {
        case CrudType.CREATE:
          this.openAddEditPage(CrudType.CREATE, row);
          break;
        case CrudType.GET:
          this.row = row ? row : { pageIndex: 0, pageSize: 10 };
          this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
          this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
          break;
        case CrudType.EDIT:
          this.openAddEditPage(CrudType.VIEW, row);
          break;
        case CrudType.VIEW:
          this.openAddEditPage(CrudType.VIEW, row);
          break;
        default:
      }
    }

    onTabChange(event) {
      this.dataList = [];
      this.totalRecords = null;
      this.selectedTabIndex = event.index
      this.router.navigate(['/inventory/stock-management/list'], { queryParams: { tab: this.selectedTabIndex } })
      this.getRequiredListData()
    }

    openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
      switch (type) {

      }
    }

    ngOnDestroy() {
      if (this.listSubscribtion) {
        this.listSubscribtion.unsubscribe();
      }
    }

}
