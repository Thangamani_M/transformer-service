import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { RadioRemovalRoutingModule } from './radio-removal-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RadioRemovalRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    NgxBarcodeModule,
    NgxPrintModule,
    AutoCompleteModule,
  ],
  providers: [
    DatePipe
  ],
})
export class RadioRemovalModule { }
