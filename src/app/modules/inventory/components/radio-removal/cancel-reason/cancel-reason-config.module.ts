import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CancelReasonComponent } from './cancel-reason.component';

import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations: [CancelReasonComponent],
    imports: [
      CommonModule,
      ReactiveFormsModule,
      FormsModule,
      LayoutModule,
      MaterialModule,
      SharedModule,
      InputSwitchModule,
      NgxBarcodeModule,
      NgxPrintModule,
      AutoCompleteModule,
      RouterModule.forChild([{ path: '', component: CancelReasonComponent,canActivate:[AuthGuard], data: { title: 'Cancel Reason Config' },},])
    ],
    providers:[
      DatePipe
  ],
  })
  export class CancelReasonConfigModule { }
