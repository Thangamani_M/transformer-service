import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { CancelReasonConfigModel, CancelReasonConfigFormArrayModel } from '@modules/inventory/models/RadioRemoval.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-cancel-reason',
  templateUrl: './cancel-reason.component.html',
  styleUrls: ['./cancel-reason.component.scss']
})
export class CancelReasonComponent implements OnInit {
  loggedUser: any;
  primengTableConfigProperties: any;
  selectedIndex: number = 2;
  Getcancelreason: any;
  CancelReasonDetailForm: FormGroup;
  cancelreasonconfigArray = [];
  selectedrowINdex: any;
  IsEdit: boolean = false;
  isNoChange: boolean = false;
  constructor(private router: Router, private snackbarService: SnackbarService, private dialog: MatDialog, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'Radio Removals Appointment Cancellation Reasons',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radio Removal Config', relativeRouterUrl: '/inventory/radio-removal' }, { displayName: 'Radio Removals Appointment Cancellation Reasons', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Termination',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Decoders',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'APT Cancel Reason',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Non Recovery Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Rented Kit Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Reschedule Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Unrecoverable Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Sub Contractor Invoicing',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'ITEM TYPE',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'RADIO SYSTEM SETUP',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          }
        ]
      }
    }
  }
  tabClick(e) {
    if (e?.index == 0) {
      this.router.navigate(['./inventory/radio-removal']);
    }
    if (e?.index == 1) {
      this.router.navigate(['./radio-removal/radio-removal-config/decoders-radio-transmeter']);
    }
    if (e?.index == 2) {
      this.router.navigate(['./radio-removal/radio-removal-config/cancel-reason-config']);
    }
    if (e?.index == 3) {
      this.router.navigate(['./radio-removal/radio-removal-config/non-recovery-reason-config']);
    }
    if (e?.index == 4) {
      this.router.navigate(['./radio-removal/radio-removal-config/rented-kit-config']);
    }
    if (e?.index == 5) {
      this.router.navigate(['./radio-removal/radio-removal-config/reschedule-reason-config']);
    }
    if (e?.index == 6) {
      this.router.navigate(['./radio-removal/radio-removal-config/unrecoverable-reason-config']);
    }
    if (e?.index == 7) {
      this.router.navigate(['./radio-removal/radio-removal-config/sub-contractor-invoicing']);
    }
    if (e?.index == 8) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 8 } });
    }
    if (e?.index == 9) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 9 } });
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createCancelReasonConfigForm();
    this.loadDetails();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Config"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  loadDetails() {
    this.getcancelreasonDetailsById().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response?.isSuccess && response?.statusCode == 200) {
        response?.resources?.forEach(element => {
          const reqObj = {
            radioRemovalAppointmentCancelReasonConfigId: element?.radioRemovalAppointmentCancelReasonConfigId,
            appointmentCancelReason: element?.appointmentCancelReason,
            isActive: element?.isActive,
            createdUserId: this.loggedUser?.userId,
          }
          this.initFormArray(reqObj);
        });
        this.getCancelReasonConfigFormArray.disable();
      }
    })
  }
  // Form Array
  get getCancelReasonConfigFormArray(): FormArray {
    if (!this.CancelReasonDetailForm) return;
    return this.CancelReasonDetailForm.get("CancelReasonsFormArray") as FormArray;
  }

  initFormArray(cancelreasonconfigModel?: CancelReasonConfigFormArrayModel) {
    let cancelreasonConfigDetailsModel = new CancelReasonConfigFormArrayModel(cancelreasonconfigModel);
    let cancelreasonConfigDetailsFormArray = this.formBuilder.group({});
    Object.keys(cancelreasonConfigDetailsModel).forEach((key) => {
      cancelreasonConfigDetailsFormArray.addControl(key, new FormControl({ value: cancelreasonConfigDetailsModel[key], disabled: true }));
    });
    cancelreasonConfigDetailsFormArray = setRequiredValidator(cancelreasonConfigDetailsFormArray, ["appointmentCancelReason"]);
    if (cancelreasonConfigDetailsFormArray?.get('radioRemovalAppointmentCancelReasonConfigId').value) {
      this.getCancelReasonConfigFormArray?.push(cancelreasonConfigDetailsFormArray);
    } else {
      this.getCancelReasonConfigFormArray?.insert(0, cancelreasonConfigDetailsFormArray);
    }
  }
  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  //Get Details
  getcancelreasonDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIOREMOVAL_APPOINTMENT_CANCELREASON_DETAILS,
      undefined, null)
  };
  createCancelReasonConfigForm(cancelreasonconfigModel?: CancelReasonConfigModel) {
    let radioRemovalModel = new CancelReasonConfigModel(cancelreasonconfigModel);
    this.CancelReasonDetailForm = this.formBuilder.group({
    });
    Object.keys(radioRemovalModel).forEach((key) => {
      if (typeof radioRemovalModel[key] == 'object') {
        this.CancelReasonDetailForm.addControl(key, new FormArray(radioRemovalModel[key]));
      } else {
        this.CancelReasonDetailForm.addControl(key, new FormControl(radioRemovalModel[key]));
      }
    });
    this.CancelReasonDetailForm = setRequiredValidator(this.CancelReasonDetailForm, ["appointmentCancelReason"]);
  }

  Onsubmit() {
    if (this.getCancelReasonConfigFormArray?.invalid) {
      this.getCancelReasonConfigFormArray.markAllAsTouched();
      return;
    } else if (!this.CancelReasonDetailForm?.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    } else if (this.validateExistonSave()) {
      this.snackbarService.openSnackbar("Non Recovery Reason Config already exists", ResponseMessageTypes.WARNING);
      return;
    }
    this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIOREMOVAL_APPOINTMENT_CANCELREASON,
      this.getCancelReasonConfigFormArray.getRawValue()
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
      }
      this.CancelReasonDetailForm.get('appointmentCancelReason').reset('');
      this.clearFormArray(this.getCancelReasonConfigFormArray);
      this.loadDetails();
      this.isNoChange = false;
    });

  }
  validateExistonSave() {
    let findArr = [];
    this.getCancelReasonConfigFormArray.getRawValue()?.forEach((el, i) => {
      this.getCancelReasonConfigFormArray.getRawValue()?.forEach((el1, j) => {
        if (el.appointmentCancelReason?.toLowerCase() == el1.appointmentCancelReason?.toLowerCase() && i != j) {
          findArr.push(true);
        }
      });
    });
    return findArr?.length;
  }

  EditItem(item, i) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.getCancelReasonConfigFormArray.controls[i].enable();
  }

  OnAdd() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.CancelReasonDetailForm = setRequiredValidator(this.CancelReasonDetailForm, ["appointmentCancelReason"]);
    if (this.CancelReasonDetailForm.invalid) {
      this.CancelReasonDetailForm.markAllAsTouched();
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar("Cancel Reason already exists", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      let ratingItemsFormGroup = {
        appointmentCancelReason: this.CancelReasonDetailForm.get('appointmentCancelReason').value,
        isActive: this.CancelReasonDetailForm.get('isActive').value,
        radioRemovalAppointmentCancelReasonConfigId: null,
        createdUserId: this.loggedUser?.userId
      };
      this.initFormArray(ratingItemsFormGroup);
      this.CancelReasonDetailForm.get('appointmentCancelReason').setValue('', { emitEvent: false });
      this.CancelReasonDetailForm.get('isActive').setValue(true, { emitEvent: false });
      this.CancelReasonDetailForm.get('appointmentCancelReason').setErrors(null, { emitEvent: false });
      this.CancelReasonDetailForm.get('appointmentCancelReason').markAsUntouched();
    }
  }

  validateExist(e?: any) {
    const currItem = this.CancelReasonDetailForm.value.appointmentCancelReason;
    const findItem = this.getCancelReasonConfigFormArray.getRawValue().find(el => el.appointmentCancelReason == currItem);
    if (findItem) {
      return true;
    }
    return false;
  }
}
