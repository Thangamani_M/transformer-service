import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputSwitchModule } from 'primeng/inputswitch';
import { RentedKitConfigComponent } from './rented-kit-config.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [RentedKitConfigComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    InputSwitchModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    NgxBarcodeModule,
    NgxPrintModule,
    AutoCompleteModule,
    RouterModule.forChild([{ path: '', component: RentedKitConfigComponent, canActivate:[AuthGuard],data: { title: 'Rented Kit Config' },},])
  ],
  providers:[
    DatePipe
],
})
export class RentedKitConfigModule { }
