import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { RentedKitConfigModel } from '@modules/inventory/models/RadioRemoval.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-rented-kit-config',
  templateUrl: './rented-kit-config.component.html'
})
export class RentedKitConfigComponent implements OnInit {
  loggedUser: any;
  primengTableConfigProperties: any;
  selectedIndex: number = 4;
  rentedkitconfigForm: FormGroup;
  RentedKit: any;
  isNoChange: boolean = false;
  selectedrowINdex: any;
  StockCodes: any;
  IsEdit: boolean = false;
  rentedkitconfigArray = [];
  constructor(private router: Router, private snackbarService: SnackbarService, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'Rented Kit Components',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radio Removal Config', relativeRouterUrl: '/inventory/radio-removal' }, { displayName: 'Rented Kit Components', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Termination',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Decoders',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'APT Cancel Reason',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Non Recovery Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Rented Kit Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Reschedule Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Unrecoverable Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Sub Contractor Invoicing',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'ITEM TYPE',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'RADIO SYSTEM SETUP',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          }
        ]
      }
    }
  }
  tabClick(e) {
    if (e?.index == 0) {
      this.router.navigate(['./inventory/radio-removal']);
    }
    if (e?.index == 1) {
      this.router.navigate(['./radio-removal/radio-removal-config/decoders-radio-transmeter']);
    }
    if (e?.index == 2) {
      this.router.navigate(['./radio-removal/radio-removal-config/cancel-reason-config']);
    }
    if (e?.index == 3) {
      this.router.navigate(['./radio-removal/radio-removal-config/non-recovery-reason-config']);
    }
    if (e?.index == 4) {
      this.router.navigate(['./radio-removal/radio-removal-config/rented-kit-config']);
    }
    if (e?.index == 5) {
      this.router.navigate(['./radio-removal/radio-removal-config/reschedule-reason-config']);
    }
    if (e?.index == 6) {
      this.router.navigate(['./radio-removal/radio-removal-config/unrecoverable-reason-config']);
    }
    if (e?.index == 7) {
      this.router.navigate(['./radio-removal/radio-removal-config/sub-contractor-invoicing']);
    }
    if (e?.index == 8) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 8 } });
    }
    if (e?.index == 9) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 9 } });
    }
  }

  ngOnInit(): void {
    this.getStockCode();
    this.createrentedkitconfigFrom();
    this.combineLatestNgrxStoreData()
    this.getRentedKitById().subscribe((Response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      this.RentedKit = Response.resources;

    })
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Config"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  // Create Form
  createrentedkitconfigFrom(RentedKitModel?: RentedKitConfigModel) {
    let radioRemovalModel = new RentedKitConfigModel(RentedKitModel);
    this.rentedkitconfigForm = this.formBuilder.group({
      equipmentName: ['', Validators.required],
      description: ['', Validators.required],
      itemId: ['', Validators.required]
    });
    Object.keys(radioRemovalModel).forEach((key) => {
      this.rentedkitconfigForm.addControl(key, new FormControl(radioRemovalModel[key]));
    });
    this.rentedkitconfigForm = setRequiredValidator(this.rentedkitconfigForm, ["equipmentName", "description", "itemId"]);
  }
  //Get Details
  getRentedKitById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_RENTEDKIT_COMPONENT,
      undefined, null)
  };

  // Stock Code Dropdown
  getStockCode() {
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.StockCodes = response.resources;
        }
      });
  }

  EditItem(item, i) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.selectedrowINdex = i;
    this.IsEdit = true;
    this.isNoChange = true;
  }
  Onsubmit() {
    // if (this.rentedkitconfigForm.invalid) {
    //   this.rentedkitconfigForm.markAllAsTouched();
    //   return;
    // }
    if (!this.isNoChange) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    if (!this.IsEdit) {

    }
    else {
      this.rentedkitconfigArray = this.RentedKit;
    }
    this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_RENTEDKIT_COMPONENT,
      this.RentedKit
    ).subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess == true && response.statusCode == 200) {
        this.IsEdit = false;
        this.selectedrowINdex = -1;
      }
      this.isNoChange = false;
      this.rentedkitconfigForm.get('equipmentName').reset('');
      this.rentedkitconfigForm.get('description').reset('');
      this.rentedkitconfigForm.get('itemId').reset('');
    });

  }

  OnAdd() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.rentedkitconfigForm.invalid) {
      this.rentedkitconfigForm.markAllAsTouched();
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar("Rented Kit Config already exists", ResponseMessageTypes.WARNING);
      return;
    }
    this.isNoChange = true;
    if (!this.IsEdit) {
      let ratingItemsFormGroup = {
        equipmentName: this.rentedkitconfigForm.get('equipmentName').value,
        description: this.rentedkitconfigForm.get('description').value,
        itemId: this.rentedkitconfigForm.get('itemId').value,
      };
      this.RentedKit.push(ratingItemsFormGroup);
      let formValue = this.rentedkitconfigForm.value;
      formValue.createdUserId = this.loggedUser.userId;
      this.rentedkitconfigArray.push(formValue);
      this.rentedkitconfigForm.get('equipmentName').reset('');
      this.rentedkitconfigForm.get('description').reset('');
      this.rentedkitconfigForm.get('itemId').reset('');
    }

  }
  validateExist(e?: any) {
    const currItem = this.rentedkitconfigForm.value.equipmentName;
    const findItem = this.RentedKit.find(el => el.equipmentName == currItem);
    if (findItem) {
      return true;
    }
    return false;
  }
}
