import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { RadioRemovalConfigComponent } from './radio-removal-config.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [RadioRemovalConfigComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    NgxBarcodeModule,
    NgxPrintModule,
    AutoCompleteModule,
    RouterModule.forChild([{ path: '', component: RadioRemovalConfigComponent, canActivate:[AuthGuard],data: { title: 'Radio Removal' },},])
  ],
  providers:[
    DatePipe
],
})
export class RadioRemovalConfigModule { }
