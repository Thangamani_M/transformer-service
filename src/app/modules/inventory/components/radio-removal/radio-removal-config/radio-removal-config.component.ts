import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { RadioRemovalItemsFormArrayModel, radioRemovalItemsModel } from '@modules/inventory/models/RadioRemoval.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { BillingModuleApiSuffixModels } from '@modules/sales';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';

@Component({
  selector: 'app-radio-removal-config',
  templateUrl: './radio-removal-config.component.html'
})
export class RadioRemovalConfigComponent implements OnInit {

  loggedUser: any;
  cancelreasoncodes: any;
  selectedIndex: number = 0;
  radioremovalDetailForm: FormGroup;
  primengTableConfigProperties: any;
  TerminationDropvalue: boolean;
  onHoldCreationForm: FormArray;
  radioremoval: any;
  RadioArray = [];
  selectedrowINdex: any;
  isLoading: boolean;
  isSubmitted: boolean;

  constructor(private crudService: CrudService, private dialog: MatDialog, private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute, private httpCancelService: HttpCancelService, private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private store: Store<AppState>, private route: ActivatedRoute, private router: Router) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'Radio Removal Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radio Removal Config', relativeRouterUrl: '' }, { displayName: 'Termination', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Termination',
            dataKey: 'radioRemovalTerminationTimeLineCofigId',
            formArrayName: 'terminationFormArray',
            enableBreadCrumb: true,
            disabled: true,
            isShowTooltip: true,
            columns: [
              {
                field: 'ticketCancellationReasonId', displayName: 'Termination Type', type: 'input_select', className: 'col-3', defaultText: 'Select',
                isDefaultOpt: false, options: [], assignValue: 'id', displayValue: 'displayName', isTooltip: true
              },
              { field: 'delayPeriod', displayName: 'Delay Period', type: 'input_text', className: 'col-4', isTooltip: true },
            ],
            isEditFormArray: true,
            isRemoveFormArray: true,
          },
          {
            caption: 'Decoders',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'APT Cancel Reason',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Non Recovery Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Rented Kit Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Reschedule Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Unrecoverable Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Sub Contractor Invoicing',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'ITEM TYPE',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'RADIO SYSTEM SETUP',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createInstallationcallCreationFrom();
    this.callonHoldDetailsById();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Config"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  callonHoldDetailsById() {
    this.radioremoval = [];
    this.RadioArray = [];
    this.rxjsService.setGlobalLoaderProperty(true);
    this.isLoading = true;
    forkJoin([this.crudService.dropdown(ModulesBasedApiSuffix.SALES, BillingModuleApiSuffixModels.UX_CANCEL_REASON),
    this.getonHoldDetailsById()]).subscribe((Response: IApplicationResponse[]) => {
      Response?.forEach((resp: IApplicationResponse, ix: number) => {
        switch (ix) {
          case 0:
            this.cancelreasoncodes = resp?.resources;
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].columns[0].options = resp?.resources;
            break;
          case 1:
            this.radioremoval = resp?.resources;
            resp?.resources?.forEach(el => {
              this.initFormArray(el);
            });
            break;
        }
      })
      this.isLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);

    })
  }

  createInstallationcallCreationFrom(radiotModel?: radioRemovalItemsModel) {
    let radioRemovalModel = new radioRemovalItemsModel(radiotModel);
    this.radioremovalDetailForm = this.formBuilder.group({});
    Object.keys(radioRemovalModel).forEach((key) => {
      if (typeof radioRemovalModel[key] === 'object') {
        this.radioremovalDetailForm.addControl(key, new FormArray(radioRemovalModel[key]));
      } else {
        this.radioremovalDetailForm.addControl(key, new FormControl(radioRemovalModel[key]));
      }
    });
    this.radioremovalDetailForm = setRequiredValidator(this.radioremovalDetailForm, ["ticketCancellationReasonId", "delayPeriod"]);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  initFormArray(radioRemovalItemsFormArrayModel?: RadioRemovalItemsFormArrayModel, add = false) {
    let radioRemovalDetailsModel = new RadioRemovalItemsFormArrayModel(radioRemovalItemsFormArrayModel);
    let radioRemovalDetailsFormArray = this.formBuilder.group({});
    Object.keys(radioRemovalDetailsModel).forEach((key) => {
      radioRemovalDetailsFormArray.addControl(key, new FormControl({ value: radioRemovalDetailsModel[key], disabled: true }));
    });
    radioRemovalDetailsFormArray = setRequiredValidator(radioRemovalDetailsFormArray, ["delayPeriod"]);
    this.radioremovalDetailForm.get('ticketCancellationReasonId').reset('');
    this.radioremovalDetailForm.get('delayPeriod').reset('');
    this.getRadioRemovalFormArray.insert(0, radioRemovalDetailsFormArray);
    if (add) {
      this.getRadioRemovalFormArray?.controls[0]?.markAsDirty();
    }
  }
  //Form Array
  get getRadioRemovalFormArray(): FormArray {
    if (!this.radioremovalDetailForm) return;
    return this.radioremovalDetailForm.get("terminationFormArray") as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onTabChange() {
    this.selectedIndex = this.route.snapshot.data?.index;
    if (this.selectedIndex == 1) {
      this.primengTableConfigProperties.tableCaption = this.route.snapshot.data?.title;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = 'Radio Removal Config';
    } else if (this.selectedIndex == 0) {
      this.primengTableConfigProperties.tableCaption = this.route.snapshot.data?.title;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = 'Radio Removal Config';
    }
  }

  tabClick(e) {
    if (e?.index == 0) {
      this.router.navigate(['./inventory/radio-removal']);
    }
    if (e?.index == 1) {
      this.router.navigate(['./radio-removal/radio-removal-config/decoders-radio-transmeter']);
    }
    if (e?.index == 2) {
      this.router.navigate(['./radio-removal/radio-removal-config/cancel-reason-config']);
    }
    if (e?.index == 3) {
      this.router.navigate(['./radio-removal/radio-removal-config/non-recovery-reason-config']);
    }
    if (e?.index == 4) {
      this.router.navigate(['./radio-removal/radio-removal-config/rented-kit-config']);
    }
    if (e?.index == 5) {
      this.router.navigate(['./radio-removal/radio-removal-config/reschedule-reason-config']);
    }
    if (e?.index == 6) {
      this.router.navigate(['./radio-removal/radio-removal-config/unrecoverable-reason-config']);
    }
    if (e?.index == 7) {
      this.router.navigate(['./radio-removal/radio-removal-config/sub-contractor-invoicing']);
    }
    if (e?.index == 8) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 8 } });
    }
    if (e?.index == 9) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 9 } });
    }
  }

  // Only Numbers with Decimals
  keyPressNumbersDecimal(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    }
    return true;
  }

  //Get Details
  getonHoldDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_TERMINATION_TIMELINE_CONFIG,
      undefined, null)
  };
  OnAdd() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (this.radioremovalDetailForm.invalid) {
      this.radioremovalDetailForm.markAllAsTouched();
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar("Termination Type already exists", ResponseMessageTypes.WARNING);
      return;
    }
    let ratingItemsFormGroup = {
      ticketCancellationReasonId: this.radioremovalDetailForm.get('ticketCancellationReasonId').value,
      delayPeriod: this.radioremovalDetailForm.get('delayPeriod').value,
      isSystem: false,
      isDeleted: false,
      isActive: true,
      radioRemovalTerminationTimeLineCofigId: null
    };
    this.initFormArray(ratingItemsFormGroup, true);
  }

  Onsubmit() {
    this.RadioArray = [];
    if (this.getRadioRemovalFormArray?.invalid) {
      this.getRadioRemovalFormArray.markAllAsTouched();
      return;
    } else if (!this.getRadioRemovalFormArray?.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    } else if (this.validExistOnSave()) {
      this.snackbarService.openSnackbar("Termination already exists Can't Save", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.getRadioRemovalFormArray?.controls?.forEach((el: any) => {
        if (el?.dirty) {
          this.RadioArray.push(el?.getRawValue());
        }
      })
      this.isSubmitted = true;
      this.crudService.create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RADIO_REMOVAL_TERMINATION_TIMELINE_CONFIG,
        this.RadioArray
      ).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
          this.clearFormArray(this.getRadioRemovalFormArray);
          this.radioremovalDetailForm.reset();
          this.callonHoldDetailsById();
        }
        this.isSubmitted = false;
      });
    }
  }

  deleteItem(i) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canRowDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const item = (this.getRadioRemovalFormArray.controls[i] as FormGroup)?.getRawValue();
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "450px",
      data: dialogData
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        if (item?.radioRemovalTerminationTimeLineCofigId) {
          const options = {
            body: {
              ids: item?.radioRemovalTerminationTimeLineCofigId,
              isDeleted: true,
              modifiedUserId: this.loggedUser.userId
            }
          };
          this.rxjsService.setGlobalLoaderProperty(true);
          this.crudService.deleteByParams(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.RADIO_REMOVAL_TERMINATION_TIMELINE_CONFIG_DELETE, options
          ).subscribe((response: IApplicationResponse) => {
            if (response?.isSuccess && response?.statusCode == 200) {
              // this.radioremoval.splice(i, 1)
              this.clearFormArray(this.getRadioRemovalFormArray);
              this.callonHoldDetailsById();
            }
          });
        } else {
          this.getRadioRemovalFormArray.removeAt(i);
        }
      }
    });

  }
  EditItem(i) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    const item = (this.getRadioRemovalFormArray.controls[i] as FormGroup)?.getRawValue();
    (this.getRadioRemovalFormArray.controls[i] as FormGroup)?.enable();
  }

  validateExist(e?: any) {
    const currItem = this.radioremovalDetailForm.value.ticketCancellationReasonId;
    const findItem = this.getRadioRemovalFormArray.getRawValue()?.find(el => el.ticketCancellationReasonId == currItem);
    if (findItem) {
      return true;
    }
    return false;
  }

  validExistOnSave() {
    let arr = [];
    this.getRadioRemovalFormArray?.getRawValue()?.find((el, i) => {
      this.getRadioRemovalFormArray?.getRawValue()?.find((el1, j) => {
        if (el.ticketCancellationReasonId == el1.ticketCancellationReasonId && i != j) {
          arr.push(true);
          return;
        }
      });
    });
    return arr.length;
  }

}
