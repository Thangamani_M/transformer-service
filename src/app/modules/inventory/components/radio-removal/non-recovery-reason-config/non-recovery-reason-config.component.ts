import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { nonrecoveryReasonConfigFormArrayModel, nonrecoveryReasonConfigModel } from '@modules/inventory/models/RadioRemoval.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-non-recovery-reason-config',
  templateUrl: './non-recovery-reason-config.component.html'
})
export class NonRecoveryReasonConfigComponent implements OnInit {

  primengTableConfigProperties: any;
  loggedUser: any;
  selectedIndex: number = 3;
  nonrecoveryReasonDetailForm: FormGroup;
  nonrecovery: any;

  constructor(private router: Router, private snackbarService: SnackbarService, private dialog: MatDialog, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'Radio Removal Non Recovery Reason Cofig',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radio Removal Config', relativeRouterUrl: '/inventory/radio-removal' }, { displayName: 'Non Recovery Reason Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Termination',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Decoders',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'APT Cancel Reason',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Non Recovery Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Rented Kit Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Reschedule Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Unrecoverable Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Sub Contractor Invoicing',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'ITEM TYPE',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'RADIO SYSTEM SETUP',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          }
        ]
      }
    }
  }

  tabClick(e) {
    if (e?.index == 0) {
      this.router.navigate(['./inventory/radio-removal']);
    }
    if (e?.index == 1) {
      this.router.navigate(['./radio-removal/radio-removal-config/decoders-radio-transmeter']);
    }
    if (e?.index == 2) {
      this.router.navigate(['./radio-removal/radio-removal-config/cancel-reason-config']);
    }
    if (e?.index == 3) {
      this.router.navigate(['./radio-removal/radio-removal-config/non-recovery-reason-config']);
    }
    if (e?.index == 4) {
      this.router.navigate(['./radio-removal/radio-removal-config/rented-kit-config']);
    }
    if (e?.index == 5) {
      this.router.navigate(['./radio-removal/radio-removal-config/reschedule-reason-config']);
    }
    if (e?.index == 6) {
      this.router.navigate(['./radio-removal/radio-removal-config/unrecoverable-reason-config']);
    }
    if (e?.index == 7) {
      this.router.navigate(['./radio-removal/radio-removal-config/sub-contractor-invoicing']);
    }
    if (e?.index == 8) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 8 } });
    }
    if (e?.index == 9) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 9 } });
    }

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getnonrecoveryconfig();
    this.createnonrecoveryReasonConfigForm();
    this.loadDetails();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Config"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  //DropDown
  getnonrecoveryconfig() {
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RADIO_REMOVAL_NONRECOVERY_REASON_CONFIG,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.nonrecovery = response.resources;
        }
      });
  }

  //Get Details
  getnonrecoveryreasonById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_NONRECOVERY_REASON_CONFIG,
      undefined, null)
  };

  loadDetails() {
    this.getnonrecoveryreasonById().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response?.isSuccess && response?.statusCode == 200) {
        response?.resources?.forEach(element => {
          const reqObj = {
            radioRemovalNonRecoveryReasonCofigId: element?.radioRemovalNonRecoveryReasonCofigId,
            nonRecoveryReasons: element?.nonRecoveryReasons,
            isActive: element?.isActive,
            createdUserId: this.loggedUser?.userId,
          }
          this.initFormArray(reqObj);
        });
        this.getnonrecoveryReasonConfigFormArray.disable();
      }
    })
  }
  //create Form
  createnonrecoveryReasonConfigForm(cancelreasonconfigModel?: nonrecoveryReasonConfigModel) {
    let nonrecoveryReasonModel = new nonrecoveryReasonConfigModel(cancelreasonconfigModel);
    this.nonrecoveryReasonDetailForm = this.formBuilder.group({
      //  nonRecoveryReasons: ['', Validators.required],
    });
    Object.keys(nonrecoveryReasonModel).forEach((key) => {
      if (typeof nonrecoveryReasonModel[key] == 'object') {
        this.nonrecoveryReasonDetailForm.addControl(key, new FormArray(nonrecoveryReasonModel[key]));
      } else {
        this.nonrecoveryReasonDetailForm.addControl(key, new FormControl(nonrecoveryReasonModel[key]));
      }
    });
    this.nonrecoveryReasonDetailForm = setRequiredValidator(this.nonrecoveryReasonDetailForm, ["nonRecoveryReasons"]);
  }

  initFormArray(nonrecoveryReasonConfigModel?: nonrecoveryReasonConfigFormArrayModel) {
    let nonrecoveryReasonConfigDetailsModel = new nonrecoveryReasonConfigFormArrayModel(nonrecoveryReasonConfigModel);
    let nonrecoveryReasonConfigDetailsFormArray = this.formBuilder.group({});
    Object.keys(nonrecoveryReasonConfigDetailsModel).forEach((key) => {
      nonrecoveryReasonConfigDetailsFormArray.addControl(key, new FormControl({ value: nonrecoveryReasonConfigDetailsModel[key], disabled: true }));
    });
    nonrecoveryReasonConfigDetailsFormArray = setRequiredValidator(nonrecoveryReasonConfigDetailsFormArray, ["nonRecoveryReasons"]);
    if (nonrecoveryReasonConfigDetailsFormArray?.get('radioRemovalNonRecoveryReasonCofigId').value) {
      this.getnonrecoveryReasonConfigFormArray?.push(nonrecoveryReasonConfigDetailsFormArray);
    } else {
      this.getnonrecoveryReasonConfigFormArray?.insert(0, nonrecoveryReasonConfigDetailsFormArray);
    }
  }

  get getnonrecoveryReasonConfigFormArray(): FormArray {
    if (!this.nonrecoveryReasonDetailForm) return;
    return this.nonrecoveryReasonDetailForm.get("nonRecoveryReasonsFormArray") as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  Onsubmit() {
    if (this.getnonrecoveryReasonConfigFormArray?.invalid) {
      this.getnonrecoveryReasonConfigFormArray.markAllAsTouched();
      return;
    } else if (!this.nonrecoveryReasonDetailForm?.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    } else if (this.validateExistonSave()) {
      this.snackbarService.openSnackbar("Non Recovery Reason Config already exists", ResponseMessageTypes.WARNING);
      return;
    } else {
      this.crudService.create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RADIO_REMOVAL_NONRECOVERY_REASON_CONFIG,
        this.getnonrecoveryReasonConfigFormArray.getRawValue()
      ).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
        }
        this.nonrecoveryReasonDetailForm.get('nonRecoveryReasons').reset('');
        this.clearFormArray(this.getnonrecoveryReasonConfigFormArray);
        this.loadDetails();
      });
    }
  }

  //Edit
  EditItem(item, i) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.getnonrecoveryReasonConfigFormArray.controls[i].enable();
  }

  //Add
  OnAdd() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.nonrecoveryReasonDetailForm = setRequiredValidator(this.nonrecoveryReasonDetailForm, ["nonRecoveryReasons"]);
    if (this.nonrecoveryReasonDetailForm.invalid) {
      this.nonrecoveryReasonDetailForm.markAllAsTouched();
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar("Non Recovery Reason Config already exists", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      let ratingItemsFormGroup = {
        radioRemovalNonRecoveryReasonCofigId: null,
        nonRecoveryReasons: this.nonrecoveryReasonDetailForm.get('nonRecoveryReasons').value,
        isActive: this.nonrecoveryReasonDetailForm.get('isActive').value,
        createdUserId: this.loggedUser?.userId
      };
      this.initFormArray(ratingItemsFormGroup);
      this.nonrecoveryReasonDetailForm.get('nonRecoveryReasons').setValue('', { emitEvent: false });
      this.nonrecoveryReasonDetailForm.get('isActive').setValue(true, { emitEvent: false });
      this.nonrecoveryReasonDetailForm.get('nonRecoveryReasons').setErrors(null, { emitEvent: false });
      this.nonrecoveryReasonDetailForm.get('nonRecoveryReasons').markAsUntouched();
    }
  }
  validateExist(e?: any) {
    const currItem = this.nonrecoveryReasonDetailForm.value.nonRecoveryReasons;
    const findItem = this.getnonrecoveryReasonConfigFormArray.getRawValue().find(el => el.nonRecoveryReasons == currItem);
    if (findItem) {
      return true;
    }
    return false;
  }
  validateExistonSave() {
    let findArr = [];
    this.getnonrecoveryReasonConfigFormArray.getRawValue()?.forEach((el, i) => {
      this.getnonrecoveryReasonConfigFormArray.getRawValue()?.forEach((el1, j) => {
        if (el.nonRecoveryReasons?.toLowerCase() == el1.nonRecoveryReasons?.toLowerCase() && i != j) {
          findArr.push(true);
        }
      });
    });
    return findArr?.length;
  }
}
