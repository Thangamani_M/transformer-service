import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { DecoderRadioTransmeterComponent } from './decoder-radio-transmeter.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [DecoderRadioTransmeterComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: DecoderRadioTransmeterComponent, canActivate:[AuthGuard],data: { title: 'Decoders Radio Transmeter' } }]),
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    NgxBarcodeModule,
    NgxPrintModule,
    AutoCompleteModule,
  ],
  providers: [
    DatePipe
  ],
})
export class DecoderRadioTransmeterModule { }
