import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { DecoderConfigFormArrayModel, DecoderModel } from '@modules/inventory/models/RadioRemoval.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';

@Component({
  selector: 'app-decoder-radio-transmeter',
  templateUrl: './decoder-radio-transmeter.component.html'
})
export class DecoderRadioTransmeterComponent implements OnInit {
  item: any
  primengTableConfigProperties: any;
  selectedIndex: number = 1;
  DecoderCodes: any;
  selectedrowINdex: any;
  selectedval: string;
  decodercodes: any;
  currItem: any;
  decoderremoval: any;
  isNoChange: boolean = false;
  DecoderDetailForm: FormGroup;
  loggedUser: any;
  StockCodes: any;
  decoderArray = [];
  DecoderDropvalue: boolean;
  IsEdit: boolean = false;
  actionsDropDown: any;
  constructor(private router: Router, private snackbarService: SnackbarService, private dialog: MatDialog, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'Radio Removals Decoders Set Up',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radio Removal Config', relativeRouterUrl: '/inventory/radio-removal' }, { displayName: 'Decoders', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Termination',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Decoders',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'APT Cancel Reason',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Non Recovery Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Rented Kit Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Reschedule Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Unrecoverable Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Sub Contractor Invoicing',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'ITEM TYPE',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'RADIO SYSTEM SETUP',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          }

        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getForkJoinRequests();
    this.createDecoderRadioTransmitterFrom();
    this.loadDetails();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Config"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  //Navigate
  tabClick(e) {
    if (e?.index == 0) {
      this.router.navigate(['./radio-removal/radio-removal-config']);
    }
    if (e?.index == 1) {
      this.router.navigate(['./radio-removal/radio-removal-config/decoders-radio-transmeter']);
    }
    if (e?.index == 2) {
      this.router.navigate(['./radio-removal/radio-removal-config/cancel-reason-config']);
    }
    if (e?.index == 3) {
      this.router.navigate(['./radio-removal/radio-removal-config/non-recovery-reason-config']);
    }
    if (e?.index == 4) {
      this.router.navigate(['./radio-removal/radio-removal-config/rented-kit-config']);
    }
    if (e?.index == 5) {
      this.router.navigate(['./radio-removal/radio-removal-config/reschedule-reason-config']);
    }
    if (e?.index == 6) {
      this.router.navigate(['./radio-removal/radio-removal-config/unrecoverable-reason-config']);
    }
    if (e?.index == 7) {
      this.router.navigate(['./radio-removal/radio-removal-config/sub-contractor-invoicing']);
    }
    if (e?.index == 8) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 8 } });
    }
    if (e?.index == 9) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 9 } });
    }
  }

  getForkJoinRequests(): void {
    this.rxjsService.setGlobalLoaderProperty(true);
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RADIO_REMOVAL_ACTION),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RADIO_REMOVAL_DECODER),
    ])
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess && respObj.statusCode == 200) {
            switch (ix) {
              case 0:
                this.actionsDropDown = respObj.resources;
                break;
              case 1:
                this.StockCodes = respObj.resources;

                break;
              case 2:
                this.decodercodes = respObj.resources;

                break;
            }
          }
          setTimeout(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
          }, 50);
        });
      });
  }

  loadDetails() {
    this.getdecoderDetailsById().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response?.isSuccess && response?.statusCode == 200) {
        response?.resources?.forEach(element => {
          const reqObj = {
            radioRemovalDecoderSetUpConfigId: element?.radioRemovalDecoderSetUpConfigId,
            itemId: element?.itemId,
            radioRemovalDecoderActionStatusId: element?.radioRemovalDecoderActionStatusId,
            decoderName: element?.decoderName,
            itemName: element?.itemName,
            decoderId: element?.decoderId,
            radioRemovalDecoderActionStatusName: element?.radioRemovalDecoderActionStatusName,
            createdUserId: this.loggedUser?.userId,
          }
          this.initFormArray(reqObj);
        });
        this.getDecoderFormArray.disable();
      }
    })
  }

  initFormArray(nonrecoveryReasonConfigModel?: DecoderConfigFormArrayModel) {
    let nonrecoveryReasonConfigDetailsModel = new DecoderConfigFormArrayModel(nonrecoveryReasonConfigModel);
    let nonrecoveryReasonConfigDetailsFormArray = this.formBuilder.group({});
    Object.keys(nonrecoveryReasonConfigDetailsModel).forEach((key) => {
      nonrecoveryReasonConfigDetailsFormArray.addControl(key, new FormControl({ value: nonrecoveryReasonConfigDetailsModel[key], disabled: true }));
    });
    nonrecoveryReasonConfigDetailsFormArray = setRequiredValidator(nonrecoveryReasonConfigDetailsFormArray, ["itemId", "radioRemovalDecoderActionStatusId", "decoderId"]);
    if (nonrecoveryReasonConfigDetailsFormArray?.get('radioRemovalDecoderSetUpConfigId').value) {
      this.getDecoderFormArray?.push(nonrecoveryReasonConfigDetailsFormArray);
    } else {
      this.getDecoderFormArray?.insert(0, nonrecoveryReasonConfigDetailsFormArray);
    }
  }
  //Get Details
  getdecoderDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_DECODER,
      undefined, null)
  };

  //Create Form
  createDecoderRadioTransmitterFrom(decoderlistModel?: DecoderModel) {
    let radioRemovalModel = new DecoderModel(decoderlistModel);
    this.DecoderDetailForm = this.formBuilder.group({
    });
    Object.keys(radioRemovalModel).forEach((key) => {
      if (typeof radioRemovalModel[key] == 'object') {
        this.DecoderDetailForm.addControl(key, new FormArray(radioRemovalModel[key]));
      } else {
        this.DecoderDetailForm.addControl(key, new FormControl(radioRemovalModel[key]));
      }
    });
    this.DecoderDetailForm = setRequiredValidator(this.DecoderDetailForm, ["itemId", "radioRemovalDecoderActionStatusId", "decoderId"]);
  }

  get getDecoderFormArray(): FormArray {
    if (!this.DecoderDetailForm) return;
    return this.DecoderDetailForm.get("DecoderConfigsFormArray") as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  //Delete Item
  deleteItem(item, i) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[1].canRowDelete) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let deleteData = {
      ids: item.value.radioRemovalDecoderSetUpConfigId,
      modifiedUserId: this.loggedUser.userId
    };
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "450px",
      data: dialogData
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        if (item.value.radioRemovalDecoderSetUpConfigId) {

          this.crudService.delete(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.RADIO_REMOVAL_DECODER,
            '',
            prepareRequiredHttpParams(deleteData)
          ).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
              this.getDecoderFormArray.removeAt(i)
              this.getdecoderDetailsById();
            }
            this.rxjsService.setFormChangeDetectionProperty(true);
          });
        } else {
          this.getDecoderFormArray.removeAt(i);
            this.snackbarService.openSnackbar("Decoder Setup Config removed successfully", ResponseMessageTypes.SUCCESS);
        }
      }
    });


  }

  validateExistonSave() {
    let findArr = [];
    this.getDecoderFormArray.getRawValue()?.forEach((el, i) => {
      this.getDecoderFormArray.getRawValue()?.forEach((el1, j) => {
        if (el.itemId?.toLowerCase() == el1.itemId?.toLowerCase() && i != j) {
          findArr.push(true);
        }
      });
    });
    return findArr?.length;
  }

  Onsubmit() {
    if (this.getDecoderFormArray?.invalid) {
      this.getDecoderFormArray.markAllAsTouched();
      return;
    } else if (!this.DecoderDetailForm?.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    } else if (this.validateExistonSave()) {
      this.snackbarService.openSnackbar("Non Recovery Reason Config already exists", ResponseMessageTypes.WARNING);
      return;
    } else {

      let decoderFormArry = this.getDecoderFormArray.getRawValue();
      if (decoderFormArry && decoderFormArry.length) {
        decoderFormArry.forEach(decoderForm => {
          decoderForm['createdUserId'] = this.loggedUser.userId;
        })
      }

      this.crudService.create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RADIO_REMOVAL_DECODER,
        decoderFormArry
      ).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
        }
        this.DecoderDetailForm.get('decoderId').reset('');
        this.DecoderDetailForm.get('radioRemovalDecoderActionStatusId').reset('');
        this.DecoderDetailForm.get('itemId').reset('');
        this.clearFormArray(this.getDecoderFormArray);
        this.loadDetails();
      });
    }
  }
  EditItem(item, i) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[1].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.getDecoderFormArray.controls[i].enable();
  }


  DecoderDrop(event) {
    const currItem = event.target.options[event.target.options.selectedIndex].text;;
    this.decoderremoval.find(el => {
      if (el.decoderName == currItem) {
        this.snackbarService.openSnackbar("Decoder Type already exists", ResponseMessageTypes.WARNING);
        this.DecoderDropvalue = true;
        return;
      }
    }
    );
  }
  validateExist(e?: any) {
    const currItem = this.DecoderDetailForm.value.itemId;
    const findItem = this.getDecoderFormArray.getRawValue().find(el => el.itemId == currItem);
    if (findItem) {
      return true;
    }
    return false;
  }


  OnAdd() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.DecoderDetailForm = setRequiredValidator(this.DecoderDetailForm, ["itemId"]);
    if (this.DecoderDetailForm.invalid) {
      this.DecoderDetailForm.markAllAsTouched();
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar("Decoder Radio Transmeter already exists", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      let ratingItemsFormGroup = {
        radioRemovalDecoderSetUpConfigId: null,
        decoderId: this.DecoderDetailForm.get('decoderId').value,
        radioRemovalDecoderActionStatusId: this.DecoderDetailForm.get('radioRemovalDecoderActionStatusId').value,
        itemId: this.DecoderDetailForm.get('itemId').value,
        createdUserId: this.loggedUser?.userId
      };
      this.initFormArray(ratingItemsFormGroup);
      this.DecoderDetailForm.get('decoderId').setValue('', { emitEvent: false });
      this.DecoderDetailForm.get('decoderId').setErrors(null, { emitEvent: false });
      this.DecoderDetailForm.get('decoderId').markAsUntouched();
      this.DecoderDetailForm.get('itemId').setValue('', { emitEvent: false });
      this.DecoderDetailForm.get('itemId').setErrors(null, { emitEvent: false });
      this.DecoderDetailForm.get('itemId').markAsUntouched();
    }
  }

}
