import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { nonrecoveryReasonConfigFormArrayModel, unrecoverableReasonConfigFormArrayModel, UnRecoverableReasonConfigModel } from '@modules/inventory/models/RadioRemoval.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-unrecoverable-reason-config',
  templateUrl: './unrecoverable-reason-config.component.html'
})
export class UnrecoverableReasonConfigComponent implements OnInit {
  loggedUser: any;
  primengTableConfigProperties: any;
  selectedIndex: number = 6;
  unrecoverablereasonconfigForm: FormGroup;
  UnRecoverablereason: any;
  unrecoverableArray = [];
  selectedrowINdex: any;
  IsEdit: boolean = false;
  constructor(private crudService: CrudService, private snackbarService: SnackbarService, private dialog: MatDialog, private activatedRoute: ActivatedRoute, private httpCancelService: HttpCancelService, private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private store: Store<AppState>, private route: ActivatedRoute, private router: Router) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: 'UnRecoverable Reason Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radio Removal Config', relativeRouterUrl: '' }, { displayName: 'UnRecoverable Reason Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Termination',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Decoders',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'APT Cancel Reason',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Non Recovery Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Rented Kit Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Reschedule Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Unrecoverable Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Sub Contractor Invoicing',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'ITEM TYPE',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'RADIO SYSTEM SETUP',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          }
        ]
      }
    }
  }


  tabClick(e) {
    if (e?.index == 0) {
      this.router.navigate(['./inventory/radio-removal']);
    }
    if (e?.index == 1) {
      this.router.navigate(['./radio-removal/radio-removal-config/decoders-radio-transmeter']);
    }
    if (e?.index == 2) {
      this.router.navigate(['./radio-removal/radio-removal-config/cancel-reason-config']);
    }
    if (e?.index == 3) {
      this.router.navigate(['./radio-removal/radio-removal-config/non-recovery-reason-config']);
    }
    if (e?.index == 4) {
      this.router.navigate(['./radio-removal/radio-removal-config/rented-kit-config']);
    }
    if (e?.index == 5) {
      this.router.navigate(['./radio-removal/radio-removal-config/reschedule-reason-config']);
    }
    if (e?.index == 6) {
      this.router.navigate(['./radio-removal/radio-removal-config/unrecoverable-reason-config']);
    }
    if (e?.index == 7) {
      this.router.navigate(['./radio-removal/radio-removal-config/sub-contractor-invoicing']);
    }
    if (e?.index == 8) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 8 } });
    }
    if (e?.index == 9) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 9 } });
    }
  }


  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createunrecoverableconfigFrom();
    this.loadDetails();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Config"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  loadDetails() {
    this.getunrecoverableById().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response?.isSuccess && response?.statusCode == 200) {
        response?.resources?.forEach(element => {
          const reqObj = {
            radioRemovalUnRecoverableReasonConfigId: element?.radioRemovalUnRecoverableReasonConfigId,
            unRecoverableReason: element?.unRecoverableReason,
            isActive: element?.isActive,
            createdUserId: this.loggedUser?.userId,
          }
          this.initFormArray(reqObj);
        });
        this.getunrecoverableReasonConfigFormArray.disable();
      }
    })
  }

  initFormArray(unrecoveryReasonConfigModel?: unrecoverableReasonConfigFormArrayModel) {
    let unrecoveryReasonConfigDetailsModel = new unrecoverableReasonConfigFormArrayModel(unrecoveryReasonConfigModel);
    let nonrecoveryReasonConfigDetailsFormArray = this.formBuilder.group({});
    Object.keys(unrecoveryReasonConfigDetailsModel).forEach((key) => {
      nonrecoveryReasonConfigDetailsFormArray.addControl(key, new FormControl({ value: unrecoveryReasonConfigDetailsModel[key], disabled: true }));
    });
    nonrecoveryReasonConfigDetailsFormArray = setRequiredValidator(nonrecoveryReasonConfigDetailsFormArray, ["unRecoverableReason"]);
    if (nonrecoveryReasonConfigDetailsFormArray?.get('radioRemovalUnRecoverableReasonConfigId').value) {
      this.getunrecoverableReasonConfigFormArray?.push(nonrecoveryReasonConfigDetailsFormArray);
    } else {
      this.getunrecoverableReasonConfigFormArray?.insert(0, nonrecoveryReasonConfigDetailsFormArray);
    }
  }

  get getunrecoverableReasonConfigFormArray(): FormArray {
    if (!this.unrecoverablereasonconfigForm) return;
    return this.unrecoverablereasonconfigForm.get("unRecoverableReasonsFormArray") as FormArray;
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  createunrecoverableconfigFrom(RentedKitModel?: UnRecoverableReasonConfigModel) {
    let radioRemovalModel = new UnRecoverableReasonConfigModel(RentedKitModel);
    this.unrecoverablereasonconfigForm = this.formBuilder.group({
    });
    Object.keys(radioRemovalModel).forEach((key) => {
      if (typeof radioRemovalModel[key] == 'object') {
        this.unrecoverablereasonconfigForm.addControl(key, new FormArray(radioRemovalModel[key]));
      } else {
        this.unrecoverablereasonconfigForm.addControl(key, new FormControl(radioRemovalModel[key]));
      }
    });
    this.unrecoverablereasonconfigForm = setRequiredValidator(this.unrecoverablereasonconfigForm, ["unRecoverableReason"]);
  }
  //Get Details
  getunrecoverableById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_UNRECOVERABLE_REASON_CONFIG,
      undefined, null)
  };

  OnAdd() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.unrecoverablereasonconfigForm = setRequiredValidator(this.unrecoverablereasonconfigForm, ["unRecoverableReason"]);
    if (this.unrecoverablereasonconfigForm.invalid) {
      this.unrecoverablereasonconfigForm.markAllAsTouched();
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar("Non Recovery Reason Config already exists", ResponseMessageTypes.WARNING);
      return;
    }
    else {
    }
    let ratingItemsFormGroup = {
      radioRemovalUnRecoverableReasonConfigId: null,
      unRecoverableReason: this.unrecoverablereasonconfigForm.get('unRecoverableReason').value,
      isActive: this.unrecoverablereasonconfigForm.get('isActive').value,
      createdUserId: this.loggedUser?.userId
    };
    this.initFormArray(ratingItemsFormGroup);
    this.unrecoverablereasonconfigForm.get('unRecoverableReason').setValue('', { emitEvent: false });
    this.unrecoverablereasonconfigForm.get('isActive').setValue(true, { emitEvent: false });
    this.unrecoverablereasonconfigForm.get('unRecoverableReason').setErrors(null, { emitEvent: false });
    this.unrecoverablereasonconfigForm.get('unRecoverableReason').markAsUntouched();
  }
  validateExist(e?: any) {
    const currItem = this.unrecoverablereasonconfigForm.value.unRecoverableReason;
    const findItem = this.getunrecoverableReasonConfigFormArray.getRawValue().find(el => el.unRecoverableReason == currItem);
    if (findItem) {
      return true;
    }
    return false;
  }

  Onsubmit() {
    if (this.getunrecoverableReasonConfigFormArray?.invalid) {
      this.getunrecoverableReasonConfigFormArray.markAllAsTouched();
      return;
    } else if (!this.unrecoverablereasonconfigForm?.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    } else if (this.validateExistonSave()) {
      this.snackbarService.openSnackbar("Un Recoverable Reason Config already exists", ResponseMessageTypes.WARNING);
      return;
    } else {
      this.crudService.create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RADIO_REMOVAL_UNRECOVERABLE_REASON_CONFIG,
        this.getunrecoverableReasonConfigFormArray.getRawValue()
      ).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
        }
        this.unrecoverablereasonconfigForm.get('unRecoverableReason').reset('');
        this.clearFormArray(this.getunrecoverableReasonConfigFormArray);
        this.loadDetails();
      });
    }
  }
  EditItem(item, i) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.getunrecoverableReasonConfigFormArray.controls[i].enable();
  }

  validateExistonSave() {
    let findArr = [];
    this.getunrecoverableReasonConfigFormArray.getRawValue()?.forEach((el, i) => {
      this.getunrecoverableReasonConfigFormArray.getRawValue()?.forEach((el1, j) => {
        if (el.unRecoverableReason?.toLowerCase() == el1.unRecoverableReason?.toLowerCase() && i != j) {
          findArr.push(true);
        }
      });
    });
    return findArr?.length;
  }
}
