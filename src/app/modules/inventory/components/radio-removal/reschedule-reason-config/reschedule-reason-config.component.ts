import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { RescheduleReasonConfigModel, reshedulereasonConfigFormArrayModel } from '@modules/inventory/models/RadioRemoval.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';

@Component({
  selector: 'app-reschedule-reason-config',
  templateUrl: './reschedule-reason-config.component.html'
})
export class RescheduleReasonConfigComponent implements OnInit {
  primengTableConfigProperties: any;
  loggedUser: any;
  selectedIndex: number = 5;
  reschedulereason: any;
  reshedulereasonconfig: any;
  reschedulereasonconfigForm: FormGroup;
  selectedrowINdex: any;
  IsEdit: boolean = false;
  reschedulereasonconfigArray = [];
  constructor(private router: Router, private snackbarService: SnackbarService, private dialog: MatDialog, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'Reschedule Reason Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radio Removal Config', relativeRouterUrl: '/inventory/radio-removal' }, { displayName: 'Reschedule Reason Config', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Termination',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Decoders',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'APT Cancel Reason',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Non Recovery Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Rented Kit Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Reschedule Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Unrecoverable Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Sub Contractor Invoicing',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'ITEM TYPE',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'RADIO SYSTEM SETUP',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          }
        ]
      }
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Config"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  tabClick(e) {
    if (e?.index == 0) {
      this.router.navigate(['./inventory/radio-removal']);
    }
    if (e?.index == 1) {
      this.router.navigate(['./radio-removal/radio-removal-config/decoders-radio-transmeter']);
    }
    if (e?.index == 2) {
      this.router.navigate(['./radio-removal/radio-removal-config/cancel-reason-config']);
    }
    if (e?.index == 3) {
      this.router.navigate(['./radio-removal/radio-removal-config/non-recovery-reason-config']);
    }
    if (e?.index == 4) {
      this.router.navigate(['./radio-removal/radio-removal-config/rented-kit-config']);
    }
    if (e?.index == 5) {
      this.router.navigate(['./radio-removal/radio-removal-config/reschedule-reason-config']);
    }
    if (e?.index == 6) {
      this.router.navigate(['./radio-removal/radio-removal-config/unrecoverable-reason-config']);
    }
    if (e?.index == 7) {
      this.router.navigate(['./radio-removal/radio-removal-config/sub-contractor-invoicing']);
    }
    if (e?.index == 8) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 8 } });
    }
    if (e?.index == 9) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 9 } });
    }
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getreschedulereasonconfig();
    this.createreschedulereasonconfigFrom();
    this.loadDetails();
  }

  loadDetails() {
    this.getreshedulereasonconfigById().subscribe((response: IApplicationResponse) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response?.isSuccess && response?.statusCode == 200) {
        response?.resources?.forEach(element => {
          const reqObj = {
            radioRemovalRescheduleReasonConfigId: element?.radioRemovalRescheduleReasonConfigId,
            rescheduleReason: element?.rescheduleReason,
            isActive: element?.isActive,
            createdUserId: this.loggedUser?.userId,
          }
          this.initFormArray(reqObj);
        });
        this.getreshedulereasonConfigFormArray.disable();
      }
    })
  }

  initFormArray(resheduleReasonConfigModel?: reshedulereasonConfigFormArrayModel) {
    let resheduleReasonConfigDetailsModel = new reshedulereasonConfigFormArrayModel(resheduleReasonConfigModel);
    let resheduleReasonConfigDetailsFormArray = this.formBuilder.group({});
    Object.keys(resheduleReasonConfigDetailsModel).forEach((key) => {
      resheduleReasonConfigDetailsFormArray.addControl(key, new FormControl({ value: resheduleReasonConfigDetailsModel[key], disabled: true }));
    });
    resheduleReasonConfigDetailsFormArray = setRequiredValidator(resheduleReasonConfigDetailsFormArray, ["rescheduleReason"]);
    if (resheduleReasonConfigDetailsFormArray?.get('radioRemovalRescheduleReasonConfigId').value) {
      this.getreshedulereasonConfigFormArray?.push(resheduleReasonConfigDetailsFormArray);
    } else {
      this.getreshedulereasonConfigFormArray?.insert(0, resheduleReasonConfigDetailsFormArray);
    }
  }
  // Form Array
  get getreshedulereasonConfigFormArray(): FormArray {
    if (!this.reschedulereasonconfigForm) return;
    return this.reschedulereasonconfigForm.get("reschedulereasonFormArray") as FormArray;
  }

  //dropdown
  getreschedulereasonconfig() {
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RADIO_REMOVAL_RESCHEDULE_REASON_CONFIG,
      prepareGetRequestHttpParams(null, null, null)).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.reschedulereason = response.resources;
        }
      });
  }
  //Get Details
  getreshedulereasonconfigById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_RESCHEDULE_REASON_CONFIG,
      undefined, null)
  };
  //Create Form
  createreschedulereasonconfigFrom(RescheduleReasonconfigModel?: RescheduleReasonConfigModel) {
    let radioRemovalModel = new RescheduleReasonConfigModel(RescheduleReasonconfigModel);
    this.reschedulereasonconfigForm = this.formBuilder.group({
    });
    Object.keys(radioRemovalModel).forEach((key) => {
      if (typeof radioRemovalModel[key] == 'object') {
        this.reschedulereasonconfigForm.addControl(key, new FormArray(radioRemovalModel[key]));
      } else {
        this.reschedulereasonconfigForm.addControl(key, new FormControl(radioRemovalModel[key]));
      }
    });
    this.reschedulereasonconfigForm = setRequiredValidator(this.reschedulereasonconfigForm, ["rescheduleReason"]);

  }
  EditItem(item, i) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.getreshedulereasonConfigFormArray.controls[i].enable();
  }
  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  Onsubmit() {
    if (this.getreshedulereasonConfigFormArray?.invalid) {
      this.getreshedulereasonConfigFormArray.markAllAsTouched();
      return;
    } else if (!this.reschedulereasonconfigForm?.dirty) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    } else if (this.validateExistonSave()) {
      this.snackbarService.openSnackbar("Non Recovery Reason Config already exists", ResponseMessageTypes.WARNING);
      return;
    } else {
      this.crudService.create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RADIO_REMOVAL_RESCHEDULE_REASON_CONFIG,
        this.getreshedulereasonConfigFormArray.getRawValue()
      ).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess == true && response.statusCode == 200) {
        }
        this.reschedulereasonconfigForm.get('rescheduleReason').reset('');
        this.clearFormArray(this.getreshedulereasonConfigFormArray);
        this.loadDetails();
      });
    }
  }

  OnAdd() {
    this.reschedulereasonconfigForm = setRequiredValidator(this.reschedulereasonconfigForm, ["rescheduleReason"]);
    if (this.reschedulereasonconfigForm.invalid) {
      this.reschedulereasonconfigForm.markAllAsTouched();
      return;
    } else if (this.validateExist()) {
      this.snackbarService.openSnackbar("Reschedule Reason already exists", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      let ratingItemsFormGroup = {
        radioRemovalRescheduleReasonConfigId: null,
        rescheduleReason: this.reschedulereasonconfigForm.get('rescheduleReason').value,
        isActive: this.reschedulereasonconfigForm.get('isActive').value,
        createdUserId: this.loggedUser?.userId
      };
      this.initFormArray(ratingItemsFormGroup);
      this.reschedulereasonconfigForm.get('rescheduleReason').setValue('', { emitEvent: false });
      this.reschedulereasonconfigForm.get('isActive').setValue(true, { emitEvent: false });
      this.reschedulereasonconfigForm.get('rescheduleReason').setErrors(null, { emitEvent: false });
      this.reschedulereasonconfigForm.get('rescheduleReason').markAsUntouched();
    }
  }
  validateExist(e?: any) {
    const currItem = this.reschedulereasonconfigForm.value.rescheduleReason;
    const findItem = this.getreshedulereasonConfigFormArray.getRawValue().find(el => el.rescheduleReason == currItem);
    if (findItem) {
      return true;
    }
    return false;
  }

  validateExistonSave() {
    let findArr = [];
    this.getreshedulereasonConfigFormArray.getRawValue()?.forEach((el, i) => {
      this.getreshedulereasonConfigFormArray.getRawValue()?.forEach((el1, j) => {
        if (el.rescheduleReason?.toLowerCase() == el1.rescheduleReason?.toLowerCase() && i != j) {
          findArr.push(true);
        }
      });
    });
    return findArr?.length;
  }
}
