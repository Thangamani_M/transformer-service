import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputSwitchModule } from 'primeng/inputswitch';
import { RescheduleReasonConfigComponent } from './reschedule-reason-config.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
    declarations: [RescheduleReasonConfigComponent],
    imports: [
      CommonModule,
      RouterModule.forChild([{ path: '', component: RescheduleReasonConfigComponent, canActivate:[AuthGuard],data: { title: 'Reshedule Reason Config' } }]),
      ReactiveFormsModule,
      FormsModule,
      LayoutModule,
      MaterialModule,
      SharedModule,
      InputSwitchModule,
      NgxBarcodeModule,
      NgxPrintModule,
      AutoCompleteModule,
    ],
    providers: [
      DatePipe
    ],
  })
  export class ResheduleReasonConfigModule { }
