import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  { path: '', data: { index: 0, title: 'radio-removal' }, loadChildren: () => import('./radio-removal-config/radio-removal-config.module').then(m => m.RadioRemovalConfigModule) },
  { path: 'decoders-radio-transmeter', data: { index: 1, title: 'decoders-radio-transmeter' }, loadChildren: () => import('./decoder-radio-transmeter/decoder-radio-transmeter.module').then(m => m.DecoderRadioTransmeterModule) },
  { path: 'cancel-reason-config', data: { index: 2, title: 'cancel-reason-config' }, loadChildren: () => import('./cancel-reason/cancel-reason-config.module').then(m => m.CancelReasonConfigModule) },
  { path: 'non-recovery-reason-config', data: { index: 3, title: 'non-recovery-reason-config' }, loadChildren: () => import('./non-recovery-reason-config/non-recovery-reason.module').then(m => m.NonRecoveryReasonConfigModule) },
  { path: 'rented-kit-config', data: { index: 4, title: 'rented-kit-config' }, loadChildren: () => import('./rented-kit-config/rented-kit-config.module').then(m => m.RentedKitConfigModule) },
  { path: 'reschedule-reason-config', data: { index: 5, title: 'reschedule-reason-config' }, loadChildren: () => import('./reschedule-reason-config/reshedule-reason-config.module').then(m => m.ResheduleReasonConfigModule) },
  { path: 'unrecoverable-reason-config', data: { index: 6, title: 'unrecoverable-reason-config' }, loadChildren: () => import('./unrecoverable-reason-config/unrecoverable-reason-config.module').then(m => m.NonRecoverableReasonConfigModule) },
  { path: 'sub-contractor-invoicing', data: { index: 6, title: 'sub-contractor-invoicing' }, loadChildren: () => import('./sub-contractor-invoicing/sub-contractor-invoicing.module').then(m => m.SubContractorInvoicingModule) },
  { path: 'item-scrapping-configuration', data: { index: 7, title: 'item-scrapping-configuration' }, loadChildren: () => import('../item-scrapping-configuration.component/item-scrapping-configuration.module').then(m => m.ItemScrappingConfigurationModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class RadioRemovalRoutingModule { }
