import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { SubContractorInvoicingAddEditModel } from "@modules/inventory/models/sub-contractor-invoicing.model";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";

@Component({
  selector: 'app-sub-contractor-invoicing',
  templateUrl: './sub-contractor-invoicing.component.html',
  styleUrls: ['./sub-contractor-invoicing.component.scss']
})
export class SubContractorInvoicingComponent {
  primengTableConfigProperties: any;
  selectedIndex: number = 7;
  userData;
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  subContractorInvoicingAddEditForm: FormGroup; // form group
  isRadioSubmit: boolean = false;
  isSystemSubmit: boolean = false;
  isNoChange: boolean = false;
  constructor(private snackbarService: SnackbarService, private router: Router, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService,
    private crudService: CrudService) {
    this.primengTableConfigProperties = {
      tableCaption: 'Sub Contractor Invoicing',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radio Removal Config', relativeRouterUrl: '/inventory/radio-removal' }, { displayName: 'Sub Contractor Invoicing', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Termination',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Decoders',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'APT Cancel Reason',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Non Recovery Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Rented Kit Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Reschedule Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Unrecoverable Reason Config',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'Sub Contractor Invoicing',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'ITEM TYPE',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          },
          {
            caption: 'RADIO SYSTEM SETUP',
            isShowTooltip: true,
            enableBreadCrumb: true,
            disabled: true,
          }
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createSubContractorInvoicingAddEdit();
    this.getList();
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Config"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createSubContractorInvoicingAddEdit() {
    let CreateDealerTypeConfigAddEditModel = new SubContractorInvoicingAddEditModel();
    // create form controls dynamically from model class
    this.subContractorInvoicingAddEditForm = this.formBuilder.group({});
    Object.keys(CreateDealerTypeConfigAddEditModel).forEach((key) => {
      if (key == 'subContractorInvoicingAddEditCodeList') {
        let CreateDealerTypeConfigAddEditFormArray = this.formBuilder.array([]);
        this.subContractorInvoicingAddEditForm.addControl(key, CreateDealerTypeConfigAddEditFormArray);
      }
      else {
        this.subContractorInvoicingAddEditForm.addControl(key, new FormControl(CreateDealerTypeConfigAddEditModel[key]));
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);
    this.valueChanges();
  }
  valueChanges() {
    this.subContractorInvoicingAddEditCodesFormArray.valueChanges.forEach((element) => {
      if (element && element.length > 0) {
        if (element[0].endCount && (element[0].startCount)) {
          if (Number(element[0].endCount) < Number(element[0].startCount)) {
            this.snackbarService.openSnackbar("Radio Start Count should be less than End Count", ResponseMessageTypes.WARNING); return;
          }
        } else if (element[0].sysEndCount && element[0].sysStartCount) {
          if (Number(element[0].sysEndCount) < Number(element[0].sysStartCount)) {
            this.snackbarService.openSnackbar("System Start Count should be less than End Count", ResponseMessageTypes.WARNING); return;
          }
        }
      }
    });
  }

  tabClick(e) {
    if (e?.index == 0) {
      this.router.navigate(['./inventory/radio-removal']);
    }
    if (e?.index == 1) {
      this.router.navigate(['./radio-removal/radio-removal-config/decoders-radio-transmeter']);
    }
    if (e?.index == 2) {
      this.router.navigate(['./radio-removal/radio-removal-config/cancel-reason-config']);
    }
    if (e?.index == 3) {
      this.router.navigate(['./radio-removal/radio-removal-config/non-recovery-reason-config']);
    }
    if (e?.index == 4) {
      this.router.navigate(['./radio-removal/radio-removal-config/rented-kit-config']);
    }
    if (e?.index == 5) {
      this.router.navigate(['./radio-removal/radio-removal-config/reschedule-reason-config']);
    }
    if (e?.index == 6) {
      this.router.navigate(['./radio-removal/radio-removal-config/unrecoverable-reason-config']);
    }
    if (e?.index == 7) {
      this.router.navigate(['./radio-removal/radio-removal-config/sub-contractor-invoicing']);
    }
    if (e?.index == 8) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 8 } });
    }
    if (e?.index == 9) {
      this.router.navigate(['./radio-removal/radio-removal-config/item-scrapping-configuration'], { queryParams: { tab: 9 } });
    }
  }

  getList(): void {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.SUB_CONTRACTOR_INVOICING,
      undefined,
      false,
      null
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        this.setValue(response.resources);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });;
  }
  setValue(data) {
    this.subContractorInvoicingAddEditCodesFormArray.clear();
    data.forEach(response => {
      if (response.flag == 'Radio') {
        let dealerTypeItem = this.setExistingRadioValue(response);
        this.subContractorInvoicingAddEditCodesFormArray.push(dealerTypeItem);
      } else {
        let dealerTypeItem = this.setExistingSystemValue(response);
        this.subContractorInvoicingAddEditCodesFormArray.push(dealerTypeItem);
      }
    });
  }
  setExistingRadioValue(response) {
    let dealerTypeItem = this.formBuilder.group({
      startCount: new FormControl({ value: response.startCount, disabled: true }, Validators.required),
      endCount: new FormControl({ value: response.endCount, disabled: true }, Validators.required),
      value: new FormControl({ value: response.value, disabled: true }, Validators.required),
      sysStartCount: new FormControl({ value: null, disabled: true }),
      sysEndCount: new FormControl({ value: null, disabled: true }),
      sysValue: new FormControl({ value: null, disabled: true }),
      isRadio: new FormControl({ value: true, disabled: true }),
      isSystem: new FormControl({ value: false, disabled: true }),
      subcontractorInvoicingId: new FormControl({ value: response.subcontractorInvoicingId, disabled: true })
    });
    dealerTypeItem = setRequiredValidator(dealerTypeItem, ["startCount", "endCount", "value"]);
    return dealerTypeItem;
  }
  setExistingSystemValue(response) {
    let dealerTypeItem = this.formBuilder.group({
      startCount: new FormControl({ value: null, disabled: true }),
      endCount: new FormControl({ value: null, disabled: true }),
      value: new FormControl({ value: null, disabled: true }),
      sysStartCount: new FormControl({ value: response.startCount, disabled: true }, Validators.required),
      sysEndCount: new FormControl({ value: response.endCount, disabled: true }, Validators.required),
      sysValue: new FormControl({ value: response.value, disabled: true }, Validators.required),
      isRadio: new FormControl({ value: false, disabled: true }),
      isSystem: new FormControl({ value: true, disabled: true }),
      subcontractorInvoicingId: new FormControl({ value: response.subcontractorInvoicingId, disabled: true })
    });
    dealerTypeItem = setRequiredValidator(dealerTypeItem, ["sysStartCount", "sysEndCount", "sysValue"]);
    return dealerTypeItem;
  }
  addSubContractor(type) {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedIndex].canCreate) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    let isRadio = false; let isSystem = false;
    this.isNoChange = true;
    if (type == 'radio') {
      isRadio = true;
      this.isRadioSubmit = (!this.subContractorInvoicingAddEditForm.value.endCount || !this.subContractorInvoicingAddEditForm.value.startCount || !this.subContractorInvoicingAddEditForm.value.value) ? true : false;
      if (this.isRadioSubmit) return;
      if (Number(this.subContractorInvoicingAddEditForm.value.endCount) < Number(this.subContractorInvoicingAddEditForm.value.startCount)) {
        this.snackbarService.openSnackbar("Start Count should be less than End Count", ResponseMessageTypes.WARNING); return;
      }
      let dealerTypeItem = this.setNewRadioValue(isRadio, isSystem);
      this.subContractorInvoicingAddEditCodesFormArray.push(dealerTypeItem);
      this.subContractorInvoicingAddEditForm.controls['startCount'].reset();
      this.subContractorInvoicingAddEditForm.controls['endCount'].reset();
      this.subContractorInvoicingAddEditForm.controls['value'].reset();
    } else {
      isSystem = true;
      this.isSystemSubmit = (!this.subContractorInvoicingAddEditForm.value.sysEndCount || !this.subContractorInvoicingAddEditForm.value.sysStartCount || !this.subContractorInvoicingAddEditForm.value.sysValue) ? true : false;
      if (this.isSystemSubmit) return;
      if (Number(this.subContractorInvoicingAddEditForm.value.sysEndCount) < Number(this.subContractorInvoicingAddEditForm.value.sysStartCount)) {
        this.snackbarService.openSnackbar("Start Count should be less than End Count", ResponseMessageTypes.WARNING); return;
      }
      let dealerTypeItem = this.setNewSystemValue(isRadio, isSystem);
      this.subContractorInvoicingAddEditCodesFormArray.push(dealerTypeItem);
      this.subContractorInvoicingAddEditForm.controls['sysStartCount'].reset();
      this.subContractorInvoicingAddEditForm.controls['sysEndCount'].reset();
      this.subContractorInvoicingAddEditForm.controls['sysValue'].reset();
    }
  }
  setNewRadioValue(isRadio, isSystem) {
    let dealerTypeItem = this.formBuilder.group({
      startCount: new FormControl({ value: this.subContractorInvoicingAddEditForm.value.startCount, disabled: true }, Validators.required),
      endCount: new FormControl({ value: this.subContractorInvoicingAddEditForm.value.endCount, disabled: true }, Validators.required),
      value: new FormControl({ value: this.subContractorInvoicingAddEditForm.value.value, disabled: true }, Validators.required),
      sysStartCount: new FormControl({ value: null, disabled: true }),
      sysEndCount: new FormControl({ value: null, disabled: true }),
      sysValue: new FormControl({ value: null, disabled: true }),
      isRadio: new FormControl({ value: isRadio, disabled: true }),
      isSystem: new FormControl({ value: isSystem, disabled: true }),
    });
    return dealerTypeItem;
  }
  setNewSystemValue(isRadio, isSystem) {
    let dealerTypeItem = this.formBuilder.group({
      startCount: new FormControl({ value: null, disabled: true }),
      endCount: new FormControl({ value: null, disabled: true }),
      value: new FormControl({ value: null, disabled: true }),
      sysStartCount: new FormControl({ value: this.subContractorInvoicingAddEditForm.value.sysStartCount, disabled: true }, Validators.required),
      sysEndCount: new FormControl({ value: this.subContractorInvoicingAddEditForm.value.sysEndCount, disabled: true }, Validators.required),
      sysValue: new FormControl({ value: this.subContractorInvoicingAddEditForm.value.sysValue, disabled: true }, Validators.required),
      isRadio: new FormControl({ value: isRadio, disabled: true }),
      isSystem: new FormControl({ value: isSystem, disabled: true }),
    });
    return dealerTypeItem;
  }
  subContractorInvoicingEdit(index, type) {
    this.isNoChange = true;
    let subContractorInvoicingFormArray = this.subContractorInvoicingAddEditCodesFormArray;
    if (type == 'radio') {
      subContractorInvoicingFormArray.controls[index].get('startCount').enable();
      subContractorInvoicingFormArray.controls[index].get('endCount').enable();
      subContractorInvoicingFormArray.controls[index].get('value').enable();
    } else {
      subContractorInvoicingFormArray.controls[index].get('sysStartCount').enable();
      subContractorInvoicingFormArray.controls[index].get('sysEndCount').enable();
      subContractorInvoicingFormArray.controls[index].get('sysValue').enable();
    }
  }
  get subContractorInvoicingAddEditCodesFormArray(): FormArray {
    if (this.subContractorInvoicingAddEditForm !== undefined) {
      return (<FormArray>this.subContractorInvoicingAddEditForm.get('subContractorInvoicingAddEditCodeList'));
    }
  }
  onSubmit() {
    this.rxjsService.setFormChangeDetectionProperty(false);
    if (this.subContractorInvoicingAddEditCodesFormArray.invalid) {
      this.subContractorInvoicingAddEditCodesFormArray.markAllAsTouched();
      return;
    }
    let saveBody = [];
    if (!this.isNoChange) {
      this.snackbarService.openSnackbar("No Changes were detected", ResponseMessageTypes.WARNING);
      return;
    }
    if (this.check()) return;
    for (let i = 0; i < this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList.length; i++) {
      if (this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList[i].isRadio) {
        saveBody.push({
          subcontractorInvoicingId: this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList[i].subcontractorInvoicingId,
          startCount: this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList[i].startCount,
          endCount: this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList[i].endCount,
          value: this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList[i].value,
          isRadio: this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList[i].isRadio,
          isSystem: this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList[i].isSystem,
          createdUserId: this.userData.userId
        });
      } else {
        saveBody.push({
          subcontractorInvoicingId: this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList[i].subcontractorInvoicingId,
          startCount: this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList[i].sysStartCount,
          endCount: this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList[i].sysEndCount,
          value: this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList[i].sysValue,
          isRadio: this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList[i].isRadio,
          isSystem: this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList[i].isSystem,
          createdUserId: this.userData.userId
        });
      }
    }
    this.crudService
      .create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUB_CONTRACTOR_INVOICING, saveBody)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.getList();
        }
      });
  }
  check() {
    if ((this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList && this.subContractorInvoicingAddEditForm.getRawValue().subContractorInvoicingAddEditCodeList.length == 0)) {
      this.snackbarService.openSnackbar("Please add atleast one radio and system value", ResponseMessageTypes.ERROR);
      return true;
    }
  }
  onCRUDRequested(type: CrudType | string, row?: object): void {
    switch (type) {

    }
  }
}
