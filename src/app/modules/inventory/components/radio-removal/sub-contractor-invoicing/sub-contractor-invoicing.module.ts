import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { SubContractorInvoicingRoutingModule } from './sub-contractor-invoicing-routing.module';
import { SubContractorInvoicingComponent } from './sub-contractor-invoicing.component';


@NgModule({
  declarations: [SubContractorInvoicingComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    SubContractorInvoicingRoutingModule
  ]
})
export class SubContractorInvoicingModule { }
