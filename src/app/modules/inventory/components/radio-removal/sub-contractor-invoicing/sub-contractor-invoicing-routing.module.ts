import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubContractorInvoicingComponent } from './sub-contractor-invoicing.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
    { path: '', component: SubContractorInvoicingComponent, canActivate:[AuthGuard],data: { title: 'Sub Contractor Invoicing' } },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class SubContractorInvoicingRoutingModule{ }
