import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RequisitionInitiatorConfigurationAddEditComponent } from './requisition-initiator-configuration-add-edit.component';
import { RequisitionInitiatorConfigurationViewComponent } from './requisition-initiator-configuration-view.component';
import { RequisitionInitiatorConfigurationComponent } from './requisition-initiator-configuration.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
    { path:'', component: RequisitionInitiatorConfigurationComponent, data: { title: 'Requisition Initiator Configuration' },canActivate:[AuthGuard] },
    { path:'add-edit', component: RequisitionInitiatorConfigurationAddEditComponent, data: { title: 'View Requisition Initiator Configuration' },canActivate:[AuthGuard] },
    { path:'view', component: RequisitionInitiatorConfigurationViewComponent, data: { title: 'Requisition Initiator Configuration Add/Edit' },canActivate:[AuthGuard] }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
})

export class RequisitionInitiatorConfigurationRoutingModule { }
