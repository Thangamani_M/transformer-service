import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { RequisitionInitiatorConfigurationAddEditComponent } from './requisition-initiator-configuration-add-edit.component';
import { RequisitionInitiatorConfigurationRoutingModule } from './requisition-initiator-configuration-routing.module';
import { RequisitionInitiatorConfigurationViewComponent } from './requisition-initiator-configuration-view.component';
import { RequisitionInitiatorConfigurationComponent } from './requisition-initiator-configuration.component';
@NgModule({
    declarations:[
        RequisitionInitiatorConfigurationComponent,
        RequisitionInitiatorConfigurationAddEditComponent,
        RequisitionInitiatorConfigurationViewComponent
    ],
    imports:[
        CommonModule,
        ReactiveFormsModule, FormsModule,
        RequisitionInitiatorConfigurationRoutingModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
    ],
    entryComponents:[]

})

export class RequisitionInitiatorConfigurationModule { }
