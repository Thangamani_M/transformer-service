import { DatePipe } from "@angular/common";
import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
    selector: 'app-requisition-initiator-configuration-view',
    templateUrl: './requisition-initiator-configuration-view.component.html',
    styleUrls: ['./requisition-initiator-configuration.component.scss']
})
export class RequisitionInitiatorConfigurationViewComponent {
    RequistionInitioatorviewDetail: any;
    primengTableConfigProperties: any;
    loggedUser: any;
    selectedTabIndex: any = 0;
    requisitionInitiatorConfigId: string;
    requisitionInitiatorConfigDetails;
    loggedInUserData;

    constructor(private activatedRoute: ActivatedRoute, private datePipe: DatePipe, private snackbarService: SnackbarService,
        private rxjsService: RxjsService, private router: Router, private crudService: CrudService, private store: Store<AppState>) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: "View Requisition Initiator Configuration",
            breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Requisition Initiator Configuration', relativeRouterUrl: '/inventory/requisition-initiator-configuration' }, { displayName: 'View Requisition Initiator Configuration', relativeRouterUrl: '', }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Requisition Initiator Configuration',
                        dataKey: 'requisitionInitiatorConfigId',
                        captionFontSize: '21px',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: false,
                        enableHyperLink: false,
                        cursorLinkIndex: 0,
                        columns: [],
                        enableEditActionBtn: true,
                        enableExportBtn: false,
                        enableEmailBtn: false,
                        shouldShowFilterActionBtn: false,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false
                    }
                ]
            }
        }
        this.activatedRoute.queryParamMap.subscribe((params) => {
            this.requisitionInitiatorConfigId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
        });
        this.onShowValue();
    }
    ngOnInit(): void {
        this.combineLatestNgrxStoreData();
        this.getRequiredDetailData();
        this.rxjsService.setGlobalLoaderProperty(false);
    }
    combineLatestNgrxStoreData() {
        combineLatest([this.store.select(loggedInUserData),
        this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
            let permission = response[1][INVENTORY_COMPONENT.REQUISITION_INITIATOR_CONFIGURATION];
            if (permission) {
                let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
                this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
            }
        });
    }

    getRequiredDetailData() {
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_INITIATOR_CONFIGURATION, this.requisitionInitiatorConfigId, false, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.statusCode = 200 && response.isSuccess && response.resources) {
                    this.requisitionInitiatorConfigDetails = response.resources;
                    this.onShowValue(response.resources);
                    if (this.requisitionInitiatorConfigDetails.createdDate)
                        this.requisitionInitiatorConfigDetails.createdDate = this.datePipe.transform(this.requisitionInitiatorConfigDetails.createdDate, 'dd/MM/yyyy, hh:mm:ss a');
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }

    onShowValue(response?: any) {
        this.RequistionInitioatorviewDetail = [
            { name: 'Value(R)', value: response ? (response?.fromValue + ' to ' + response?.toValue) : '' },
            { name: 'Role', value: response ? response?.rolename : '' },
            { name: 'Created Date', value: response ? response?.createdDate : '' },
            { name: 'Description', value: response ? response?.description : '' },
            { name: 'Status', value: response ? (response.isActive == true ? 'Active' : 'In-Active') : '', statusClass: response ? (response.isActive == true ? "status-label-green" : 'status-label-pink') : '' },
        ]

    }
    onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
        switch (type) {
            case CrudType.EDIT:
                if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
                    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                }
                this.router.navigate(["inventory/requisition-initiator-configuration/add-edit"], { queryParams: { id: this.requisitionInitiatorConfigId } });
                break;
        }
    }
    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }
}
