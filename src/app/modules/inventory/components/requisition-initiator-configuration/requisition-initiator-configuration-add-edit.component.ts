import { Component } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { RequisitionInitiatorConfigurationModel } from "@modules/billing-management/models/requisition-initiator-configuration.model";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { UserModuleApiSuffixModels } from "@modules/user";
import { select, Store } from "@ngrx/store";

@Component({
  selector: 'app-requisition-initiator-configuration-add-edit',
  templateUrl: './requisition-initiator-configuration-add-edit.component.html',
  styleUrls: ['./requisition-initiator-configuration.component.scss']
})
export class RequisitionInitiatorConfigurationAddEditComponent {
  primengTableConfigProperties: any;
  userData: any;
  roleList = [];
  statusList = [{ id: true, displayName: 'Active' }, { id: false, displayName: 'InActive' }];
  selectedTabIndex = 0;
  startTodayDate = new Date();
  requisitionInitiatorConfigurationForm: FormGroup;
  requisitionInitiatorConfigId: string;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });

  constructor(private snackbarService: SnackbarService, private router: Router, private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.requisitionInitiatorConfigId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    });
    let title = this.requisitionInitiatorConfigId ? 'Edit Requisition Initiator Configuration' : 'Add Requisition Initiator Configuration';
    this.primengTableConfigProperties = {
      tableCaption: title,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Requisition Initiator Configuration List', relativeRouterUrl: '/inventory/requisition-initiator-configuration' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Requisition Initiator Configuration',
            dataKey: 'requisitionInitiatorConfigId',
            enableBreadCrumb: true,
            enableAction: false,
            url: '',
          },
        ]
      }
    }
    if (this.requisitionInitiatorConfigId)
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Requisition Initiator Configuration', relativeRouterUrl: '/inventory/requisition-initiator-configuration/view', queryParams: { id: this.requisitionInitiatorConfigId } });

    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title, relativeRouterUrl: '' });
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }
  ngOnInit(): void {
    this.createForm();
    this.getRequiredDetailData();
    this.getRoleDropdown();

  }
  createForm(requisitionInitiatorConfigurationModel?: RequisitionInitiatorConfigurationModel) {
    let requisitionInitiatorConfigurationModels = new RequisitionInitiatorConfigurationModel(requisitionInitiatorConfigurationModel);
    this.requisitionInitiatorConfigurationForm = this.formBuilder.group({});
    Object.keys(requisitionInitiatorConfigurationModels).forEach((key) => {
      this.requisitionInitiatorConfigurationForm.addControl(key, new FormControl());
    });
    this.requisitionInitiatorConfigurationForm = setRequiredValidator(this.requisitionInitiatorConfigurationForm, ["roleId", "fromValue", "toValue", "description", "status"]);
    this.onValueChanges();
  }
  onValueChanges() {
    this.requisitionInitiatorConfigurationForm.get("fromValue")
      .valueChanges.subscribe((fromValue: string) => {
        if ((this.requisitionInitiatorConfigurationForm.value.toValue != null) && Number(fromValue) > Number(this.requisitionInitiatorConfigurationForm.value.toValue))
          this.snackbarService.openSnackbar("From Value should be less than To Value", ResponseMessageTypes.WARNING);
      });
    this.requisitionInitiatorConfigurationForm.get("toValue")
      .valueChanges.subscribe((toValue: string) => {
        if ((this.requisitionInitiatorConfigurationForm.value.fromValue != null) && Number(this.requisitionInitiatorConfigurationForm.value.fromValue) > Number(toValue))
          this.snackbarService.openSnackbar("From Value should be less than To Value", ResponseMessageTypes.WARNING);
      });

  }
  getRoleDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_ROLES)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources) {
          this.roleList = response.resources;
        }
      });
  }
  getRequiredDetailData() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_INITIATOR_CONFIGURATION, this.requisitionInitiatorConfigId, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode = 200 && response.isSuccess && response.resources) {
          this.setValue(response.resources);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  setValue(response) {
    this.requisitionInitiatorConfigurationForm.get('fromValue').setValue(response.fromValue);
    this.requisitionInitiatorConfigurationForm.get('toValue').setValue(response.toValue);
    this.requisitionInitiatorConfigurationForm.get('roleId').setValue(response.roleId);
    this.requisitionInitiatorConfigurationForm.get('description').setValue(response.description);
    this.requisitionInitiatorConfigurationForm.get('status').setValue(response.isActive);
  }
  onSubmit() {
    if (this.requisitionInitiatorConfigurationForm.invalid) {
      this.requisitionInitiatorConfigurationForm.markAllAsTouched();
      return;
    }
    let fromValue = this.requisitionInitiatorConfigurationForm.get('fromValue').value;
    let toValue = this.requisitionInitiatorConfigurationForm.get('toValue').value;
    if (fromValue > toValue) {
      this.snackbarService.openSnackbar("From Value should be less than To Value", ResponseMessageTypes.WARNING);
      return;
    }
    let formValue = this.requisitionInitiatorConfigurationForm.value;
    let finalObject = {
      FromValue: Number(formValue.fromValue),
      ToValue: Number(formValue.toValue),
      RoleId: formValue.roleId,
      isActive: formValue.status,
      description: formValue.description,
      createdUserId: this.userData.userId,
      RequisitionInitiatorConfigId: this.requisitionInitiatorConfigId ? this.requisitionInitiatorConfigId : null
    }
    let api = this.requisitionInitiatorConfigId ? this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_INITIATOR_CONFIGURATION, finalObject, 1) : this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.REQUISITION_INITIATOR_CONFIGURATION, finalObject, 1);
    api.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        if (this.requisitionInitiatorConfigId)
          this.router.navigate(['inventory/requisition-initiator-configuration/view'], { queryParams: { id: this.requisitionInitiatorConfigId } })
        else
          this.router.navigate(['inventory/requisition-initiator-configuration']);
      }
      this.rxjsService.setDialogOpenProperty(false);
    });

  }
  navigate() {
    if (this.requisitionInitiatorConfigId)
      this.router.navigate(['inventory/requisition-initiator-configuration/view'], { queryParams: { id: this.requisitionInitiatorConfigId, } });
    else {
      this.router.navigate(['inventory/requisition-initiator-configuration']);
    }
  }
}
