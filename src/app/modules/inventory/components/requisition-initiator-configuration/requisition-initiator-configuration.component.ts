import { DatePipe } from "@angular/common";
import { HttpParams } from "@angular/common/http";
import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService,currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { CONFIGURATION_COMPONENT } from "@modules/others/configuration/utils/configuration-component.enum";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { map } from "rxjs/operators";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-requisition-initiator-configuration',
  templateUrl: './requisition-initiator-configuration.component.html',
  styleUrls: ['./requisition-initiator-configuration.component.scss']
})

export class RequisitionInitiatorConfigurationComponent  extends PrimeNgTableVariablesModel {

  userData: UserLogin;
  primengTableConfigProperties: any;
  otherParams;
  constructor(private snackbarService:SnackbarService,
    private commonService: CrudService,private router: Router, private activatedRoute: ActivatedRoute,private rxjsService: RxjsService,private store: Store<AppState>,private datePipe: DatePipe) {
    super()
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Requisition Initiator Configuration",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },
      { displayName: 'Requisition Initiator Configuration', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Requisition Initiator Configuration',
            dataKey: 'requisitionInitiatorConfigId',
            enableAction: true,
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enableFieldsSearch: true,
            checkBox: false,
            enablePrintBtn: true,
            printTitle: 'Requisition Initiator',
            printSection: 'print-section0',
            enableRowDelete: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableExportBtn: true,
            columns: [
              { field: 'value', header: 'Value' },
              { field: 'isActive', header: 'Status'},
              { field: 'role', header: 'Role' },
              { field: 'description', header: 'Description' },
              { field: 'createdDate', header: 'Created Date & Time' },

            ],
            enableAddActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.REQUISITION_INITIATOR_CONFIGURATION,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.REQUISITION_INITIATOR_CONFIGURATION];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = { ...otherParams, ...{ IsAll: true } }
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.createdDate = this.datePipe.transform(val?.createdDate, 'dd/MM/yyyy, hh:mm:ss a');
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;

        this.totalRecords = data.totalCount;
      } else {
        data.resources= null;
        this.dataList = data.resources
        this.totalRecords = 0;

      }
    })
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canExport) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
           }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
          let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
          let otherParams ={IsAll:true};
          this.exportList(pageIndex,pageSize,otherParams );
          break;
      default:
    }
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigateByUrl("inventory/requisition-initiator-configuration/add-edit");
        break;
      case CrudType.EDIT:
        this.router.navigate(["inventory/requisition-initiator-configuration/add-edit"], { queryParams: { id: editableObject['requisitionInitiatorConfigId'] } });
      case CrudType.VIEW:
        this.router.navigate(["inventory/requisition-initiator-configuration/view"], { queryParams: { id: editableObject['requisitionInitiatorConfigId'] } });
    }
  }
  exportList(pageIndex?: any, pageSize?: any, otherParams?: object) {
    let queryParams;
    queryParams = this.generateQueryParams(otherParams,pageIndex,pageSize);
    let exportapi =  InventoryModuleApiSuffixModels.REQUISITION_INITIATOR_CONFIGURATION_EXCEL
    let label = 'Requisition Initiator Configuration';
    this.commonService.downloadFile(ModulesBasedApiSuffix.INVENTORY,
      exportapi,null, null, queryParams).subscribe((response: any) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response) {
          this.saveAsExcelFile(response, label);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
    });
  }
  generateQueryParams(otherParams,pageIndex,pageSize) {

    let queryParamsString;
    queryParamsString = new HttpParams({ fromObject:otherParams })
    .set('pageIndex',pageIndex)
    .set('pageSize',pageSize);

     queryParamsString.toString();
     return '?' + queryParamsString;
  }
  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

}
