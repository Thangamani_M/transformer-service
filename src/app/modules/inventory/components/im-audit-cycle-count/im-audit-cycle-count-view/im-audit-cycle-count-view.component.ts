import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse,CrudType, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-im-audit-cycle-count-view',
  templateUrl: './im-audit-cycle-count-view.component.html',
  styleUrls: ['./im-audit-cycle-count-view.component.scss']
})
export class ImAuditCycleCountViewComponent extends PrimeNgTableVariablesModel  implements OnInit {
  auditCycleCountId: any = '';
  auditCycleCountDetail: any = {};
  IMAuditCycleCountviewDetail:any;
  userData: any;
  primengTableConfigProperties: any
  constructor(private crudService: CrudService,private snackbarService:SnackbarService,
    private activatedRoute: ActivatedRoute,private router: Router,private store: Store<AppState>,private rxjsService: RxjsService,) {
      super();
    this.auditCycleCountId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    })
    this.primengTableConfigProperties = {
      tableCaption:'Audit Cycle Count View',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Audit Cycle Count List', relativeRouterUrl: '/inventory/audit-cycle-count' }, { displayName: ' Audit Cycle Count View', relativeRouterUrl: '', }],
      tableComponentConfigs: {
          tabsList: [
              {
                 enableBreadCrumb: true,
                  enableAction: true,
                  enableEditActionBtn: true
              }
          ]
      }
  }
  this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  this.IMAuditCycleCountviewDetail = [
    { name: 'Audit Cycle Count ID', value: ''},
    { name: 'Warehouse', value: ''},
    { name: 'Actioned By', value: ''},
    { name: 'Actioned Date & Time', value: ''},
    { name: 'Status', value: ''},
]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getAuditCycleCountDetails().subscribe((response: IApplicationResponse) => {
      let auditCycleDetail = Object.assign({}, response.resources);
      this.auditCycleCountDetail = response.resources;
      this.auditCycleCountDetail.items = auditCycleDetail.items.filter(x => x.auditCycleCountRefTypeName == 'User Selection');
      this.auditCycleCountDetail.HighRiskItem = auditCycleDetail.items.filter(x => x.auditCycleCountRefTypeName == 'High Risk Items');
      this.onShowValue(this.auditCycleCountDetail);
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.AUDIT_CYCLE_COUNT];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onShowValue(response?: any) {
    this.IMAuditCycleCountviewDetail = [
          { name: 'Audit Cycle Count ID', value: response?.auditCycleCountNumber},
          { name: 'Warehouse', value: response?.warehouseName},
          { name: 'Actioned By', value: response?.assignedby},
          { name: 'Actioned Date & Time', value: response?.auditCycleCountDate},
          { name: 'Status', value: response?.cycleCountStatusName, statusClass: response.cssClass },
    ]
  }
  onCRUDRequested(type: CrudType | string): void {

    switch (type) {
        case CrudType.EDIT:
          // if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          //   return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          // }
          if(this.userData.roleName === 'Inventory Manager' && this.auditCycleCountDetail?.cycleCountStatusName == 'Created'){
            this.navigateToEdit();
          }else {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          break;
    }
  }

  navigateToEdit() {
    this.router.navigate(['inventory/audit-cycle-count/add-edit'], { queryParams: { id: this.auditCycleCountId }, skipLocationChange: true });
  }

  getAuditCycleCountDetails(): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT, this.auditCycleCountId, false);
  }

  getAuditCycleCountHighRiskItemDetails(): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNTHIGHRISKITEM, undefined, false, prepareRequiredHttpParams({
      WarehouseId: this.auditCycleCountDetail.warehouseId,
      AuditCycleCountId: this.auditCycleCountDetail.auditCycleCountId,
      CreatedUserId: this.userData.userId
    }));
  }

}
