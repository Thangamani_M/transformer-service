import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudService, CrudType,exportList, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService ,currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-im-audit-cycle-count-list',
  templateUrl: './im-audit-cycle-count-list.component.html'
})
export class ImAuditCycleCountListComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {};
  otherParams;
  constructor(private snackbarService:SnackbarService,private crudService: CrudService,private router: Router,private store: Store<AppState>,private rxjsService: RxjsService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Audit Cycle Count",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Audit Cycle Count', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Audit Cycle Count List',
            dataKey: 'stockId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            enableAction: true,
            enableAddActionBtn: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Packing-dashboard Report',
            printSection: 'print-section0',
            columns: [{ field: 'auditCycleCountNumber', header: 'Audit Cycle Count ID', width: '200px',  },
            { field: 'status', header: 'Status', width: '200px' },
            { field: 'warehouseName', header: 'Warehouse', width: '200px', },
            { field: 'actionedBy', header: 'Actioned By', width: '200px' },
            { field: 'actionedDate', header: 'Actioned Date & Time', width: '200px',isDateTime:true },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.AUDIT_CYCLE_COUNT];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = {
      userId: this.loggedInUserData?.userId,
      ...otherParams,
    }
    this.otherParams=otherParams;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {

      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = []
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
          if (this.row['sortOrderColumn']) {
            unknownVar['sortOrder'] = this.row['sortOrder'];
            unknownVar['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
          let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
          this.exportList(pageIndex,pageSize);
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams,0,pageSize,ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_EXPORT,this.crudService,this.rxjsService,'Audit Cycle Count');
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['inventory/audit-cycle-count/add-edit'], { skipLocationChange: true });
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(['inventory/audit-cycle-count/view'], { queryParams: { id: editableObject['auditCycleCountId'] }, skipLocationChange: true });
            break;
        }
        break;
    }
  }

}
