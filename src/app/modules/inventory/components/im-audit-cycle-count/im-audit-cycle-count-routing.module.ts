import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ImAuditCycleCountAddEditComponent } from './im-audit-cycle-count-add-edit/im-audit-cycle-count-add-edit.component';
import { ImAuditCycleCountListComponent } from './im-audit-cycle-count-list/im-audit-cycle-count-list.component';
import { ImAuditCycleCountViewComponent } from './im-audit-cycle-count-view/im-audit-cycle-count-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  { path: '', component: ImAuditCycleCountListComponent, data: { title: 'Audit Cycle Count List' } ,canActivate:[AuthGuard] },
  { path: 'view', component: ImAuditCycleCountViewComponent, data: { title: 'Audit Cycle Count View' }  ,canActivate:[AuthGuard]},
  { path: 'add-edit', component: ImAuditCycleCountAddEditComponent, data: { title: 'Audit Cycle Count Add Edit' }  ,canActivate:[AuthGuard]},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class ImAuditCycleCountRoutingModule { }
