import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ImAuditCycleCountAddEditComponent } from './im-audit-cycle-count-add-edit/im-audit-cycle-count-add-edit.component';
import { ImAuditCycleCountListComponent } from './im-audit-cycle-count-list/im-audit-cycle-count-list.component';
import { ImAuditCycleCountRoutingModule } from './im-audit-cycle-count-routing.module';
import { ImAuditCycleCountViewComponent } from './im-audit-cycle-count-view/im-audit-cycle-count-view.component';

@NgModule({
  declarations: [ImAuditCycleCountListComponent, ImAuditCycleCountViewComponent, ImAuditCycleCountAddEditComponent],
  imports: [
    CommonModule,
    ImAuditCycleCountRoutingModule,
    ReactiveFormsModule, LayoutModule,
    FormsModule,
    MaterialModule,
    SharedModule
  ]
})
export class ImAuditCycleCountModule { }
