import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { AuditCycleCountModel, AuditCycleCountUpdateModel, StockCodeModel } from '@modules/inventory/models/audit-cycle-count.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-im-audit-cycle-count-add-edit',
  templateUrl: './im-audit-cycle-count-add-edit.component.html',
  styleUrls: ['./im-audit-cycle-count-add-edit.component.scss']
})
export class ImAuditCycleCountAddEditComponent implements OnInit {

  warehouseList: any = [];
  auditCycleCountId: any = '';
  selectedTabIndex = 0;
  userData: UserLogin;
  auditCycleCountForm: FormGroup;
  randomUserSelectioStockCodeForm: FormGroup;
  highRiskStockCodeForm: FormGroup;
  randomStockDescription: any = [];
  randomStockCodes: any = [];
  highRiskStockCodes: any = [];
  highRiskStockDescription: any = [];
  isLimitConfigDetails: any = {};
  auditCycleCountDetail: any = {};
  isLoading: boolean;
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  showStockCodeError: boolean;
  stockCodeErrorMessage: any;
  showStockDescError: boolean;
  StockDescErrorMessage: any = '';
  actionCreate: any = '';
  actionUpdate: any = '';
  constructor(private crudService: CrudService, private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    private router: Router, private store: Store<AppState>, private rxjsService: RxjsService, private formBuilder: FormBuilder) {
    this.auditCycleCountId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;

    })
  }

  ngOnInit(): void {
    this.auditCycleCountId == null ? this.actionCreate = 'Initiation' : '';
    this.auditCycleCountId != null ? this.actionUpdate = 'Update' : '';
    this.createStockCodeForm();
    if (this.auditCycleCountId) {
      this.updateAuditCycleCountCreateForm();
      this.getAuditCycleCountDetails().subscribe((response: IApplicationResponse) => {

        let auditCycleDetail = Object.assign({}, response.resources);
        this.auditCycleCountDetail = response.resources;
        this.auditCycleCountDetail.items = auditCycleDetail.items.filter(x => x.auditCycleCountRefTypeName == 'User Selection');
        this.auditCycleCountDetail.HighRiskItem = auditCycleDetail.items.filter(x => x.auditCycleCountRefTypeName == 'High Risk Items');
        this.auditCycleCountForm.controls['Action'].patchValue(response.resources.cycleCountStatusName == 'Created' ? true : response.resources.cycleCountStatusName == "canceled" ? false : true);
        this.auditCycleCountForm.controls['Comment'].patchValue(response.resources.comment);
        this.auditCycleCountForm.controls['warehouseId'].patchValue(response.resources.warehouseId);
        if (this.auditCycleCountForm.get('Action').value == false) {
          this.auditCycleCountForm.get('Comment').setValidators([Validators.required]);
          this.auditCycleCountForm.get('Comment').updateValueAndValidity();
        }

        for (let i = 0; i < this.auditCycleCountDetail.items.length; i++) {
          this.auditCycleCountRandomStockItemFormArray.push(this.formBuilder.group(this.auditCycleCountDetail.items[i]))
        }
        for (let i = 0; i < this.auditCycleCountDetail.HighRiskItem.length; i++) {
          this.auditCycleCountHighRiskFormArray.push(this.formBuilder.group(this.auditCycleCountDetail.HighRiskItem[i]));
        }

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_STOCK_LIMIT_CONFIG, undefined, false, prepareRequiredHttpParams({
            warehouseId: this.auditCycleCountDetail.warehouseId
          })).subscribe((response: IApplicationResponse) => {

            // if(response.resources == null){
            response.resources == null ? this.randomUserSelectioStockCodeForm.get('stockCode').disable() : response.resources.randomStockLimit == 0 ? this.randomUserSelectioStockCodeForm.get('stockCode').disable() : this.randomUserSelectioStockCodeForm.get('stockCode').enable();
            response.resources == null ? this.randomUserSelectioStockCodeForm.get('stockName').disable() : response.resources.randomStockLimit == 0 ? this.randomUserSelectioStockCodeForm.get('stockName').disable() : this.randomUserSelectioStockCodeForm.get('stockName').enable();
            response.resources == null ? this.highRiskStockCodeForm.get('stockCode').disable() : response.resources.highRiskStockLimitPerItem == 0 ? this.highRiskStockCodeForm.get('stockCode').disable() : this.highRiskStockCodeForm.get('stockCode').enable();
            response.resources == null ? this.highRiskStockCodeForm.get('stockName').disable() : response.resources.highRiskStockLimitPerItem == 0 ? this.highRiskStockCodeForm.get('stockName').disable() : this.highRiskStockCodeForm.get('stockName').enable();
            // }
            this.isLimitConfigDetails = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);

          })

        this.rxjsService.setGlobalLoaderProperty(false);

      });
    }
    else {
      this.createAuditCycleCountCreateForm();
      this.getWarehouseList().subscribe((response: IApplicationResponse) => {
        this.warehouseList = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
        // for (let i = 0; i < response.resources.length; i++) {
        //   let temp = {};
        //   temp['display'] = response.resources[i].displayName;
        //   temp['value'] = response.resources[i].id;
        //   this.warehouseList.push(temp);
        //   this.rxjsService.setGlobalLoaderProperty(false);
        // }
      })
    }
    this.auditCycleCountForm.get('warehouseId').valueChanges.subscribe(val => {
      this.auditCycleCountRandomStockItemFormArray.clear();
      this.auditCycleCountHighRiskFormArray.clear();
      if (val != '' || val != null) {

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_STOCK_LIMIT_CONFIG, undefined, false, prepareRequiredHttpParams({
            warehouseId: val
          })).subscribe((response: IApplicationResponse) => {

            response.resources == null ? this.randomUserSelectioStockCodeForm.get('stockCode').disable() : response.resources.randomStockLimit == 0 ? this.randomUserSelectioStockCodeForm.get('stockCode').disable() : this.randomUserSelectioStockCodeForm.get('stockCode').enable();
            response.resources == null ? this.randomUserSelectioStockCodeForm.get('stockName').disable() : response.resources.randomStockLimit == 0 ? this.randomUserSelectioStockCodeForm.get('stockName').disable() : this.randomUserSelectioStockCodeForm.get('stockName').enable();
            response.resources == null ? this.highRiskStockCodeForm.get('stockCode').disable() : response.resources.highRiskStockLimitPerItem == 0 ? this.highRiskStockCodeForm.get('stockCode').disable() : this.highRiskStockCodeForm.get('stockCode').enable();
            response.resources == null ? this.highRiskStockCodeForm.get('stockName').disable() : response.resources.highRiskStockLimitPerItem == 0 ? this.highRiskStockCodeForm.get('stockName').disable() : this.highRiskStockCodeForm.get('stockName').enable();


            this.isLimitConfigDetails = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);

          })
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNTHIGHRISKITEM, undefined, false, prepareRequiredHttpParams({
            WarehouseId: val
          })).subscribe((response: IApplicationResponse) => {

            this.rxjsService.setGlobalLoaderProperty(false);

            for (let i = 0; i < response.resources.length; i++) {
              this.auditCycleCountHighRiskFormArray.push(
                this.formBuilder.group(
                  {
                    auditCycleCountItemId: null,
                    auditCycleCountId: null,
                    itemId: response.resources[i].itemId,
                    stockName: response.resources[i].displayName,
                    stockCode: response.resources[i].itemCode,
                    consumables: response.resources[i].isNotSerialized == true ? 'Yes' : 'No',
                  }));
            }
          });
      }

    });

    // this.auditCycleCountForm.get('Action').valueChanges.subscribe(actionVal => {


    this.randomUserSelectioStockCodeForm.get('stockName').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        if (this.auditCycleCountForm.invalid) {
          this.auditCycleCountForm.markAllAsTouched();
          return this.randomStockDescription = [];
        }
        if ((searchText != null)) {
          this.isLoading = true;
          if (!searchText) {
            if (searchText === '') {
            }
            this.isLoading = false;
            return this.randomStockDescription = [];
          } else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_STOCK, null, true,
              prepareGetRequestHttpParams(null, null, { ItemCode: searchText, IsHighRisk: 0, WarehouseId: this.auditCycleCountId == null ? this.auditCycleCountForm.get('warehouseId').value : this.auditCycleCountDetail.warehouseId }))
          }
        } else {
          this.isLoading = false;
          return this.randomStockDescription = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.randomStockDescription = response.resources;
          this.isLoading = false;
        } else {
          this.isLoading = false;
          this.randomStockDescription = [];
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    this.randomUserSelectioStockCodeForm.get('stockCode').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {

        if (this.auditCycleCountForm.invalid) {
          this.auditCycleCountForm.controls['warehouseId'].markAllAsTouched();
          return this.randomStockCodes = [];
        }
        this.isLoading = true;
        if (searchText != null) {

          if (!searchText) {
            this.isLoading = false;
            return this.randomStockCodes = [];
          } else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_STOCK, null, true,
              prepareGetRequestHttpParams(null, null, { ItemCode: searchText, IsHighRisk: 0, WarehouseId: this.auditCycleCountId == null ? this.auditCycleCountForm.get('warehouseId').value : this.auditCycleCountDetail.warehouseId }))
          }
        }
        else {
          this.isLoading = false;
          return this.randomStockCodes = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.randomStockCodes = response.resources;
          this.isLoading = false;
        } else {
          this.isLoading = false;
          this.randomStockCodes = [];
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    this.highRiskStockCodeForm.get('stockName').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        if ((searchText != null)) {
          this.isLoading = true;
          if (this.auditCycleCountForm.invalid) {
            this.auditCycleCountForm.markAllAsTouched();
            return;
          }
          if (!searchText) {
            this.isLoading = false;
            return this.highRiskStockDescription = [];
          } else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_STOCK, null, true,
              prepareGetRequestHttpParams(null, null, { ItemCode: searchText, IsHighRisk: 1, WarehouseId: this.auditCycleCountId == null ? this.auditCycleCountForm.get('warehouseId').value : this.auditCycleCountDetail.warehouseId }))
          }
        } else {
          this.isLoading = false;
          return this.highRiskStockDescription = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.highRiskStockDescription = response.resources;
          this.isLoading = false;
        } else {
          this.highRiskStockDescription = [];
          this.isLoading = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

    this.highRiskStockCodeForm.get('stockCode').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        this.isLoading = true;
        if (this.auditCycleCountForm.invalid) {
          this.auditCycleCountForm.markAllAsTouched();
          return;
        }

        if (searchText != null) {
          if (!searchText) {
            this.isLoading = false;
            return this.highRiskStockCodes = [];
          } else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_STOCK, null, true,
              prepareGetRequestHttpParams(null, null, { ItemCode: searchText, IsHighRisk: 1, WarehouseId: this.auditCycleCountId == null ? this.auditCycleCountForm.get('warehouseId').value : this.auditCycleCountDetail.warehouseId }))
          }
        }
        else {
          this.isLoading = false;
          return this.highRiskStockCodes = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.highRiskStockCodes = response.resources;
          this.isLoading = false;
        } else {
          this.highRiskStockCodes = [];
          this.isLoading = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

  }

  getWarehouseList(): Observable<any> {
    let params = new HttpParams().set('userId', this.userData.userId)
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_CONFIG_DETAILS, undefined, true, params)
  }
  getAuditCycleCountDetails(): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT, this.auditCycleCountId, false);
  }

  getAuditCycleCountHighRiskItemDetails(): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNTHIGHRISKITEM, undefined, false, prepareRequiredHttpParams({
      WarehouseId: this.auditCycleCountDetail.warehouseId,
      AuditCycleCountId: this.auditCycleCountDetail.auditCycleCountId,
      CreatedUserId: this.userData.userId
    }));
  }

  actionTypeChange() {

    if (this.auditCycleCountForm.get('Action').value == false) {
      this.auditCycleCountForm.get('Comment').setValidators([Validators.required]);
      this.auditCycleCountForm.get('Comment').updateValueAndValidity();
    }
    else {
      this.auditCycleCountForm.get('Comment').clearValidators();
      this.auditCycleCountForm.get('Comment').updateValueAndValidity();
    }

  }

  createStockCodeForm(): void {
    let auditCycleCountAddEditModel = new StockCodeModel();
    // create form controls dynamically from model class
    this.randomUserSelectioStockCodeForm = this.formBuilder.group({});
    this.highRiskStockCodeForm = this.formBuilder.group({});
    Object.keys(auditCycleCountAddEditModel).forEach((key) => {
      this.randomUserSelectioStockCodeForm.addControl(key, new FormControl(auditCycleCountAddEditModel[key]));
      this.highRiskStockCodeForm.addControl(key, new FormControl(auditCycleCountAddEditModel[key]));
    });

  }

  updateAuditCycleCountCreateForm(): void {
    let auditCycleCountAddEditModel = new AuditCycleCountUpdateModel();
    // create form controls dynamically from model class
    this.auditCycleCountForm = this.formBuilder.group({});
    Object.keys(auditCycleCountAddEditModel).forEach((key) => {
      if (key == 'randomUserSelectedStockCodesList') {
        let productWarehouseFormArray = this.formBuilder.array([
        ]);
        // Object.keys(productWarehouseAddEditModel).forEach((key) => {
        this.auditCycleCountForm.addControl(key, productWarehouseFormArray);
        // });
      }
      else if (key == 'highRiskItemList') {
        let productWarehouseFormArray = this.formBuilder.array([
        ]);
        // Object.keys(productWarehouseAddEditModel).forEach((key) => {
        this.auditCycleCountForm.addControl(key, productWarehouseFormArray);
        // });
      }
      else {
        this.auditCycleCountForm.addControl(key, new FormControl(auditCycleCountAddEditModel[key]));
      }
    });

    this.auditCycleCountForm = setRequiredValidator(this.auditCycleCountForm, ["warehouseId"]);
  }

  createAuditCycleCountCreateForm(): void {
    let auditCycleCountAddEditModel = new AuditCycleCountModel();
    // create form controls dynamically from model class
    this.auditCycleCountForm = this.formBuilder.group({});
    Object.keys(auditCycleCountAddEditModel).forEach((key) => {
      if (key == 'randomUserSelectedStockCodesList') {
        let productWarehouseFormArray = this.formBuilder.array([

        ]);
        // Object.keys(productWarehouseAddEditModel).forEach((key) => {
        this.auditCycleCountForm.addControl(key, productWarehouseFormArray);
        // });
      }
      else if (key == 'highRiskItemList') {
        let productWarehouseFormArray = this.formBuilder.array([

        ]);
        // Object.keys(productWarehouseAddEditModel).forEach((key) => {
        this.auditCycleCountForm.addControl(key, productWarehouseFormArray);
        // });
      }
      else {
        this.auditCycleCountForm.addControl(key, new FormControl(auditCycleCountAddEditModel[key]));
      }
    });
    this.auditCycleCountForm = setRequiredValidator(this.auditCycleCountForm, ["warehouseId"]);
  }

  get auditCycleCountHighRiskFormArray(): FormArray {
    if (this.auditCycleCountForm !== undefined) {
      return (<FormArray>this.auditCycleCountForm.get('highRiskItemList'));
    }
  }

  get auditCycleCountRandomStockItemFormArray(): FormArray {
    if (this.auditCycleCountForm !== undefined) {
      return (<FormArray>this.auditCycleCountForm.get('randomUserSelectedStockCodesList'));
    }
  }

  onRandomStatucCodeSelected() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.randomUserSelectioStockCodeForm.controls.stockName.patchValue(this.randomStockCodes.find(x => x.itemCode == this.randomUserSelectioStockCodeForm.get('stockCode').value).displayName);
    this.randomUserSelectioStockCodeForm.controls.consumables.patchValue(this.randomStockCodes.find(x => x.itemCode == this.randomUserSelectioStockCodeForm.get('stockCode').value).isConsumable == false ? 'No' : 'Yes');
  }

  onRandomStatucDescSelected() {
    this.randomUserSelectioStockCodeForm.controls.stockCode.patchValue(this.randomStockDescription.find(x => x.displayName === this.randomUserSelectioStockCodeForm.get('stockName').value).itemCode);
    this.randomUserSelectioStockCodeForm.controls.consumables.patchValue(this.randomStockDescription.find(x => x.displayName === this.randomUserSelectioStockCodeForm.get('stockName').value).isConsumable == false ? 'No' : 'Yes');
  }

  addRandomStockCode() {

    if (this.auditCycleCountRandomStockItemFormArray.value.length < this.isLimitConfigDetails.randomStockLimit) {
      if (this.auditCycleCountRandomStockItemFormArray.value.find(x => x.stockCode == this.randomUserSelectioStockCodeForm.get('stockCode').value) == undefined) {
        this.auditCycleCountRandomStockItemFormArray.push(this.formBuilder.group({
          auditCycleCountItemId: null,
          auditCycleCountId: null,
          itemId: this.randomStockCodes[0].itemId,
          stockName: this.randomUserSelectioStockCodeForm.get('stockName').value,
          stockCode: this.randomUserSelectioStockCodeForm.get('stockCode').value,
          consumables: this.randomUserSelectioStockCodeForm.get('consumables').value,
        }))
        this.randomUserSelectioStockCodeForm.reset();
      }
      else {
        this.snackbarService.openSnackbar('Stock code alreday added !', ResponseMessageTypes.WARNING);

      }
    }
    else {
      this.snackbarService.openSnackbar('Random stock limit cannot be higher than ' + this.isLimitConfigDetails.randomStockLimit, ResponseMessageTypes.WARNING);
    }
  }

  addHighRiskStockCode() {

    if (this.auditCycleCountHighRiskFormArray.value.length < this.isLimitConfigDetails?.highRiskStockLimitPerItem) {
      if (this.auditCycleCountHighRiskFormArray.value.find(x => x.stockCode == this.highRiskStockCodeForm.get('stockCode').value) == undefined) {
        this.auditCycleCountHighRiskFormArray.push(this.formBuilder.group({
          auditCycleCountItemId: null,
          auditCycleCountId: null,
          itemId: this.highRiskStockCodes.find(x => x.displayName == this.highRiskStockCodeForm.get('stockName').value).itemId,
          stockName: this.highRiskStockCodeForm.get('stockName').value,
          stockCode: this.highRiskStockCodeForm.get('stockCode').value,
          consumables: this.highRiskStockCodeForm.get('consumables').value,
        }))
        this.highRiskStockCodeForm.reset();
      }
      else {
        this.snackbarService.openSnackbar('Stock code alreday added !', ResponseMessageTypes.WARNING);
      }
    }
    else {
      this.snackbarService.openSnackbar('Random stock limit cannot be higher than ' + this.isLimitConfigDetails.randomStockLimit, ResponseMessageTypes.WARNING);
    }
  }

  onHighRiskCodeSelected() {
    this.highRiskStockCodeForm.controls.stockName.patchValue(this.highRiskStockCodes.find(x => x.itemCode == this.highRiskStockCodeForm.get('stockCode').value).displayName);
    this.highRiskStockCodeForm.controls.consumables.patchValue(this.highRiskStockCodes.find(x => x.itemCode == this.highRiskStockCodeForm.get('stockCode').value).isConsumable == true ? 'Yes' : 'No');
  }

  onHighRiskStockDescSelected() {
    this.highRiskStockCodeForm.controls.stockCode.patchValue(this.highRiskStockDescription.find(x => x.displayName == this.highRiskStockCodeForm.get('stockName').value).itemCode);
    this.highRiskStockCodeForm.controls.consumables.patchValue(this.highRiskStockDescription.find(x => x.displayName == this.highRiskStockCodeForm.get('stockName').value).isConsumable == true ? 'Yes' : 'No');
  }

  saveAuditCycleCount() {

    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.auditCycleCountForm.invalid) {
      this.auditCycleCountForm.markAllAsTouched();
      return;
    }

    if (this.auditCycleCountId) {
      // if(this.auditCycleCountForm.invalid){
      //   this.auditCycleCountForm.markAllAsTouched();
      //   return;
      // }
      let item = [];
      if (this.auditCycleCountForm.value.randomUserSelectedStockCodesList.length >= 1 || this.auditCycleCountForm.value.highRiskItemList.length >= 1) {
        for (let i = 0; i < this.auditCycleCountForm.value.randomUserSelectedStockCodesList.length; i++) {
          item.push({
            "auditCycleCountRefTypeId": 11,
            "itemId": this.auditCycleCountForm.value.randomUserSelectedStockCodesList[i].itemId,
          })
        }
        for (let i = 0; i < this.auditCycleCountForm.value.highRiskItemList.length; i++) {
          item.push({
            "auditCycleCountRefTypeId": 10,
            "itemId": this.auditCycleCountForm.value.highRiskItemList[i].itemId,
          });
        } 
      }

      let itemData = {
        "auditCycleCountId": this.auditCycleCountDetail.auditCycleCountId ? this.auditCycleCountDetail.auditCycleCountId : null,
        "createdUserId": this.userData.userId,
        "warehouseId" : this.auditCycleCountDetail.warehouseId,
        "iscancel": this.auditCycleCountForm.value.Action == true ? false : true,
        "isInitiated": this.auditCycleCountForm.value.Action == true ? true : false,
        "comment": this.auditCycleCountForm.value.Comment,
        "items":item
      };

      this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_ITEMS_ADD, itemData).subscribe((response) => {
        if (response.statusCode == 200) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.router.navigate(['/inventory/audit-cycle-count']);
        }
      })
    }
    else {

      let item = [];
      if (this.auditCycleCountForm.value.randomUserSelectedStockCodesList.length >= 1 || this.auditCycleCountForm.value.highRiskItemList.length >= 1) {
        for (let i = 0; i < this.auditCycleCountForm.value.randomUserSelectedStockCodesList.length; i++) {
          item.push({
            "auditCycleCountRefTypeId": 11,
            "itemId": this.auditCycleCountForm.value.randomUserSelectedStockCodesList[i].itemId
          })
        }
        for (let i = 0; i < this.auditCycleCountForm.value.highRiskItemList.length; i++) {
          item.push({
            "auditCycleCountRefTypeId": 10,
            "itemId": this.auditCycleCountForm.value.highRiskItemList[i].itemId
          });
        }
        let itemData = {
          "auditCycleCountId": this.auditCycleCountDetail.auditCycleCountId ? this.auditCycleCountDetail.auditCycleCountId : null,
          "createdUserId": this.userData.userId,
          "warehouseId":this.auditCycleCountForm.value.warehouseId,
          "iscancel": false,
          "isInitiated": false,
          "comment": this.auditCycleCountForm.value.Comment,
          "items":item
        };
        this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_ITEMS_ADD, itemData).subscribe((response) => {
          if (response.statusCode == 200) {
            this.rxjsService.setGlobalLoaderProperty(false);
            this.router.navigate(['/inventory/audit-cycle-count'])
          }
        });
      }
      else {
        this.snackbarService.openSnackbar('Add at least one stock item of random stock code or high risk item ', ResponseMessageTypes.ERROR);
      }
    }
  }

  deleteHightRiskStockNumbers(item, index) {
    // if (this.auditCycleCountId) {

    //   this.rxjsService.setFormChangeDetectionProperty(true);
    //   if (this.auditCycleCountHighRiskFormArray.value.length > 1) {
    //     this.crudService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_ITEM_DELETE, undefined, prepareRequiredHttpParams({
    //       ids: item.auditCycleCountItemId
    //     })).subscribe((response: IApplicationResponse) => {
    //       if (response.isSuccess) {
    //         this.auditCycleCountHighRiskFormArray.removeAt(index);
    //       }
    //       this.rxjsService.setGlobalLoaderProperty(false);
    //     });
    //   }
    //   else {
    //     // this.snackbarService.openSnackbar('At least one stock item ', ResponseMessageTypes.ERROR);
    //   }
    // }
    // else {
      this.auditCycleCountHighRiskFormArray.removeAt(index);
   // }
  }

  deleteRandomStockNumbers(item, index) {
    // if (this.auditCycleCountId) {

    //   this.rxjsService.setFormChangeDetectionProperty(true);
    //   if (this.auditCycleCountRandomStockItemFormArray.value.length > 1) {
    //     this.crudService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_ITEM_DELETE, undefined, prepareRequiredHttpParams({
    //       ids: item.auditCycleCountItemId
    //     })).subscribe((response: IApplicationResponse) => {
    //       if (response.isSuccess) {
    //         this.auditCycleCountRandomStockItemFormArray.removeAt(index);

    //       }
    //       this.rxjsService.setGlobalLoaderProperty(false);
    //     });
    //   }
    //   else {
    //     // this.snackbarService.openSnackbar('At least one stock item ', ResponseMessageTypes.ERROR);

    //   }

    // }
    // else {
      this.auditCycleCountRandomStockItemFormArray.removeAt(index);

   // }
  }

  getHeight() {
    if (window?.screen?.width > 1280) {
      return '40vh';
    } else if (window?.screen?.width <= 1280) {
      return '35vh';
    }
  }
}
