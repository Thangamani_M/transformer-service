import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { WarehouseStockApprovalListComponent, WarehouseStockApprovalRoutingModule, WarehouseStockApprovalUpdateComponent, WarehouseStockApprovalViewEditComponent, WarehouseStoclApprovalModalComponent } from '@inventory-components/WarehouseStockApproval';

@NgModule({
    declarations: [WarehouseStockApprovalListComponent, WarehouseStockApprovalViewEditComponent, WarehouseStockApprovalUpdateComponent, WarehouseStoclApprovalModalComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MaterialModule,
        SharedModule,
        LayoutModule,
        WarehouseStockApprovalRoutingModule
    ],
    entryComponents: [WarehouseStoclApprovalModalComponent]
})

export class WarehouseStockApprovalModule { }