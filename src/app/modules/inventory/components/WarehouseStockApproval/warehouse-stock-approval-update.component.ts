import { DatePipe } from '@angular/common';
import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryModuleApiSuffixModels, loggedInUserData } from '@app/modules';
import { AppState } from '@app/reducers';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { WarehouseStockTakeUpdateMappingModel } from '@modules/inventory/models/warehouse-stock-take';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { WarehouseStoclApprovalModalComponent } from './warehouse-stocl-approval-modal.component';
@Component({
  selector: 'app-warehouse-stock-approval-update',
  templateUrl: './warehouse-stock-approval-update.component.html'
})
export class WarehouseStockApprovalUpdateComponent implements OnInit, AfterViewChecked {
  warehouseStockTakeId: string;
  warehouseStockTakeStatus: string;
  warehouseStockTakeDetail: any = {};
  warehouseStockTakeStatusId: string;
  warehousStockForm: FormGroup;
  warehouseStockTakeStatusList = [];
  storagelocationsList = [];
  locations: any = [];
  userData: UserLogin;
  subLocationList: any = [];
  selectedlocations: any = [];
  existingIds: any = [];
  ids: string;
  removeids: string;
  newids: string;
  isActive: boolean;
  isbookFrozen: string;
  isbookFrozenStatus: boolean;
  selectedOptions = [];
  isreasonValidate: boolean = false;
  primengTableConfigProperties;
  selectedTabIndex=0;
  showLocationErrorMsg: boolean = false;
  constructor(
    private router: Router, private dialog: MatDialog, private rxjsService: RxjsService,
    private store: Store<AppState>,
    private formBuilder: FormBuilder, private crudService: CrudService,
    private activatedRoute: ActivatedRoute,
    private readonly changeDetectorRef: ChangeDetectorRef, private datePipe: DatePipe) {

    this.warehouseStockTakeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    let title = this.warehouseStockTakeId ? 'Update' :'Add';
    this.primengTableConfigProperties = {
      tableCaption: title+' Stock Take Initiation',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Stock Take Initiation List', relativeRouterUrl: '/inventory/warehouse-stock-approval/list' }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableEditActionBtn: false,
            enableBreadCrumb: true,
            enableExportBtn: false,
          }
        ]
      }
    }
    if(this.warehouseStockTakeId)
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Stock Take Initiation', relativeRouterUrl: '/inventory/warehouse-stock-approval/view' , queryParams: {id: this.warehouseStockTakeId}});
  
    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title+' Stock Take Initiation', relativeRouterUrl: '' });
  
  }

  ngOnInit(): void {
    this.isActive = true;
    this.createWarehouseStockTakeMappingForm();
    // this.getLocationList();
    this.getwarehousestocktake();

  }
  getLocationList(warehouseId) {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_WAREHOUSE_STOCK_TAKE_JOBS_LOCATIONS, warehouseId, false, null).subscribe((response: IApplicationResponse) => {
        response.resources.forEach((element) => {
          let temp = {};
          temp['display'] = element.displayName;
          temp['value'] = element.id;
          this.storageLocationList.push(temp)
        })

        if (this.warehouseStockTakeId && this.warehouseStockTakeDetail.locationIds) {
          this.warehousStockForm.controls.locations.setValue(this.warehouseStockTakeDetail.locationIds.split(','));
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }


  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  createWarehouseStockTakeMappingForm(): void {
    let warehouseStockTakeUpdateMappingModel = new WarehouseStockTakeUpdateMappingModel();
    // create form controls dynamically from model class
    this.warehousStockForm = this.formBuilder.group({});
    Object.keys(warehouseStockTakeUpdateMappingModel).forEach((key) => {
      this.warehousStockForm.addControl(key, new FormControl(warehouseStockTakeUpdateMappingModel[key]));
    });
    this.onValueChanges();
  }
  onValueChanges() {
    this.warehousStockForm.get("isInitiateOrCancel").valueChanges.subscribe((value: string) => {

         if(value == "Cancel"){
          this.showLocationErrorMsg=false;
        }else if(value =="Initiate"){
          this.warehousStockForm = setRequiredValidator(this.warehousStockForm, ["locations"]);
          this.showLocationErrorMsg=true;
        }
      }); 
      this.warehousStockForm.get("locations").valueChanges.subscribe((value: string) => {
        if(value?.length >0){
          this.showLocationErrorMsg=false;
        }
     }); 
  }
  storageLocationList: any = [];
  getwarehousestocktake() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_JOBS, this.warehouseStockTakeId, false, null).subscribe((response: IApplicationResponse) => {
        this.warehouseStockTakeDetail = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
        this.isbookFrozen = this.warehouseStockTakeDetail['isBookFrozen'];
        this.warehousStockForm.controls.warehouseStockTakeId.setValue(this.warehouseStockTakeDetail['warehouseStockTakeInitiationJobId']);
        this.warehousStockForm.controls.warehousestocktakeNumber.setValue(this.warehouseStockTakeDetail['warehouseStockTakeNumber']);
        this.warehousStockForm.controls.warehouseId.setValue(this.warehouseStockTakeDetail['warehouseId']);
        this.warehousStockForm.controls.warehouseName.setValue(this.warehouseStockTakeDetail['warehouseName']);
        this.getLocationList(this.warehouseStockTakeDetail['warehouseId']);
        this.warehousStockForm.controls.scheduledBy.setValue(this.warehouseStockTakeDetail['scheduledBy']);
        this.warehousStockForm.controls.scheduledDate.setValue(this.datePipe.transform(this.warehouseStockTakeDetail['scheduleDate'], 'dd-MM-yyyy'));
        this.warehousStockForm.controls.isInitiateOrCancel.setValue(this.warehouseStockTakeDetail['warehouseStockTakeStatusName']);
      });
  }


  save() {

    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.warehousStockForm.invalid || (this.showLocationErrorMsg && this.warehousStockForm.get('locations').value.length == 0)) {
      return;
    }

    let saveBody;
    if (this.warehousStockForm.controls['isInitiateOrCancel'].value == 'Cancel') {
      const dialogReff = this.dialog.open(WarehouseStoclApprovalModalComponent, {
        width: '450px',
        data: {
          message: `Staging bin and dispatch bin needs to be cleared`,
          buttons: {
            cancel: 'Cancel',
            create: 'Proceed',
            action: 'Cancel'
          }
        }, disableClose: true
      });
      dialogReff.afterClosed().subscribe(result => {

        if (!result) return;
        // saveBody.reason =result;
        let formValue = this.warehousStockForm.getRawValue();
        let updateBody = {
          "warehouseStockTakeInitiationJobId": formValue.warehouseStockTakeId,
          "reason": result,
          "isInitiated": false,
          "iscancel": true,
          "createdUserId": this.userData.userId
        }

        this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_JOBS_STATUS_UPDATE, updateBody)
          .subscribe((response: IApplicationResponse) => {
            this.rxjsService.setGlobalLoaderProperty(false);

            this.router.navigate(['inventory/warehouse-stock-approval']);

          })
      });
    }
    else {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_JOBS_STOCK_AVAILABLITY_IN_STAGING_DISPATCH_LOCATION, undefined, false,
        prepareRequiredHttpParams({ WarehouseId: this.warehouseStockTakeDetail['warehouseId'] })).subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          if (response.resources.isStockAvailable) {
            const dialogReff = this.dialog.open(WarehouseStoclApprovalModalComponent, {
              width: '450px',
              data: {
                message: `Staging bin and dispatch bin needs to be cleared`,
                buttons: {
                  cancel: 'Cancel',
                  create: 'Proceed',
                  action: 'Initiate'
                }
              }, disableClose: true
            });
            dialogReff.afterClosed().subscribe(result => {

              if (result && typeof result == 'boolean') {
                let saveObj = [];
                let formValue = this.warehousStockForm.getRawValue();

                let locationIds = formValue.locations.toString();
                formValue.locations.delete;
                formValue['locationId'] = locationIds;
                formValue['WarehouseStockTakeInitiationJobId'] = formValue.warehouseStockTakeId;
                formValue['createdUserId'] = this.userData.userId;

                let updateValue = this.warehousStockForm.getRawValue();
                let updateBodyStock = {
                  "warehouseStockTakeInitiationJobId": updateValue.warehouseStockTakeId,
                  "reason": "",
                  "isInitiated": true,
                  "iscancel": false,
                  "createdUserId": this.userData.userId
                }
                saveObj.push(formValue);
                this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_JOBS_DETAILS_INSERT, saveObj)
                  .subscribe((response: IApplicationResponse) => {
                    if (response.isSuccess && response.statusCode == 200) {
                      this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_JOBS_STATUS_UPDATE, updateBodyStock)
                        .subscribe((response: IApplicationResponse) => {
                          this.rxjsService.setGlobalLoaderProperty(false);
                          if (response.isSuccess && response.statusCode == 200) {
                            this.router.navigate(['inventory/warehouse-stock-approval']);
                          }
                        });
                    }
                  });

              }
            });
          }
          else {
            // WAREHOUSE_STOCK_TAKE_JOBS_STATUS_UPDATE
            let formValue = this.warehousStockForm.getRawValue();
            let updateBody = {
              "warehouseStockTakeInitiationJobId": formValue.warehouseStockTakeId,
              "reason": "",
              "isInitiated": true,
              "iscancel": false,
              "createdUserId": this.userData.userId
            }

            let saveObjUpdate = [];
            let formValueUpdate = this.warehousStockForm.getRawValue();

            let locationIds = formValueUpdate.locations.toString();
            formValueUpdate.locations.delete;
            formValueUpdate['locationId'] = locationIds;
            formValueUpdate['WarehouseStockTakeInitiationJobId'] = formValueUpdate.warehouseStockTakeId;
            formValueUpdate['createdUserId'] = this.userData.userId;
            saveObjUpdate.push(formValueUpdate);

            this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_JOBS_DETAILS_INSERT, saveObjUpdate)
              .subscribe((response: IApplicationResponse) => {
                this.rxjsService.setGlobalLoaderProperty(false);
                if (response.isSuccess && response.statusCode == 200) {
                  this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_JOBS_STATUS_UPDATE, updateBody)
                    .subscribe((response: IApplicationResponse) => {
                      this.rxjsService.setGlobalLoaderProperty(false);
                      this.router.navigate(['inventory/warehouse-stock-approval']);
                    });
                }
              })
          }
        });
    }
  }
  cancel(){
    this.router.navigate(['/inventory/warehouse-stock-approval/view'], { queryParams: { id: this.warehouseStockTakeId }, skipLocationChange: true });
  }
}
