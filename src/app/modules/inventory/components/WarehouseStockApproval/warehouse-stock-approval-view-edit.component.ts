import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { IApplicationResponse, ModulesBasedApiSuffix, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, SnackbarService, ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { CrudType } from '@app/shared';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-warehouse-stock-approval-view-edit',
  templateUrl: './warehouse-stock-approval-view-edit.component.html'
})
export class WarehouseStockApprovalViewEditComponent extends PrimeNgTableVariablesModel implements OnInit {
  @ViewChild('reason', null) reason: ElementRef;
  warehouseStockTakeId: string;
  warehouseStockTakeDetail: any = {};
  WarehouseStockAppviewDetail: any;
  userData: UserLogin;
  primengTableConfigProperties: any;
  constructor(private snackbarService: SnackbarService, private store: Store<AppState>, private crudService: CrudService, private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private router: Router) {
    super();
    this.warehouseStockTakeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.primengTableConfigProperties = {
      tableCaption: 'Stock Take Initiation',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Stock Take Initiation List', relativeRouterUrl: '/inventory/warehouse-stock-approval/list' }, { displayName: 'View Stock Initiation', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableEditActionBtn: true,
            enableBreadCrumb: true,
            enableExportBtn: false,
          }
        ]
      }
    }
    this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getwarehousestocktake();
  }
  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
    this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.STAKE_TAKE_INITIATION];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getwarehousestocktake() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_JOBS, this.warehouseStockTakeId, false, null).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.warehouseStockTakeDetail = response.resources;
          this.onShowValue(response.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onShowValue(response?: any) {
    this.WarehouseStockAppviewDetail = [
      { name: 'Warehouse Stock Take ID', value: response?.warehouseStockTakeNumber },
      { name: 'Scheduled Date', value: response?.scheduleDate },
      { name: 'Action Date & Time', value: response?.actionedDate },
      { name: 'Action', value: response?.action },
      { name: 'Warehouse', value: response?.warehouseName },
      { name: 'Scheduled By', value: response?.scheduledBy },
      { name: 'Action By', value: response?.displayName },
      { name: 'Status', value: response.warehouseStockTakeStatusName, statusClass: response.cssClass },
    ]

  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEdit(CrudType.EDIT, row);
        break;
      default:
    }
  }

  navigateToEdit(type: CrudType | string, editableObject?: object | string): void {
    this.router.navigate(['inventory/warehouse-stock-approval/update'], { queryParams: { id: this.warehouseStockTakeId } })
  }
}
