import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WarehouseStockApprovalListComponent, WarehouseStockApprovalUpdateComponent, WarehouseStockApprovalViewEditComponent } from '@inventory-components/WarehouseStockApproval';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const warehouseStockApprovalModuleRoutes: Routes = [
  { path: '', component: WarehouseStockApprovalListComponent, data: { title: 'Stock Take Initiation List' }, canActivate: [AuthGuard] },
  { path: 'list', component: WarehouseStockApprovalListComponent, data: { title: 'Stock Take Initiation List' }, canActivate: [AuthGuard] },
  { path: 'view', component: WarehouseStockApprovalViewEditComponent, data: { title: 'Stock Take Initiation View' }, canActivate: [AuthGuard] },
  { path: 'update', component: WarehouseStockApprovalUpdateComponent, data: { title: 'Stock Take Initiation Update' }, canActivate: [AuthGuard] }
];
@NgModule({
  imports: [RouterModule.forChild(warehouseStockApprovalModuleRoutes)],

})
export class WarehouseStockApprovalRoutingModule { }