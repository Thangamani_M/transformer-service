import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RxjsService } from '@app/shared';
@Component({
  selector: 'app-warehouse-stocl-approval-modal',
  templateUrl: './warehouse-stocl-approval-modal.component.html',
  styleUrls: ['./warehouse-stocl-approval-modal.component.scss']
})
export class WarehouseStoclApprovalModalComponent implements OnInit {

  reasonText = '';
  isProceed: boolean = false;

  constructor(private rxjsService: RxjsService, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    let messageSpan = document.createElement('span');
    messageSpan.innerHTML = this.data['message'];
    document.getElementById('parentContainer').appendChild(messageSpan);
    this.rxjsService.setDialogOpenProperty(true);
  }

  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
