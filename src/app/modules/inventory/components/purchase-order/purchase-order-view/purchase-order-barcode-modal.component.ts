import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-purchase-order-barcode-modal',
  templateUrl: './purchase-order-barcode-modal.component.html',
  styleUrls: ['./purchase-order-barcode-modal.component.scss']
})
export class PurchaseOrderBarcodeModalComponent implements OnInit {
  // Barcode generator configuration start
  elementType = 'svg';
  value = '';
  format = 'CODE128';
  lineColor = '#000000';
  width = 3;
  height = 50;
  displayValue = true;
  fontOptions = '';
  font = 'monospace';
  textAlign = 'center';
  textPosition = 'bottom';
  textMargin = 2;
  fontSize = 20;
  background = '#ffffff';
  margin = 10;
  marginTop = 10;
  marginBottom = 10;
  marginLeft = 10;
  marginRight = 10;
  // Barcode generator configuration end

  constructor(@Inject(MAT_DIALOG_DATA) public barcodeData: any
  ) {

    this.value = this.barcodeData.barcode.poBarcode
  }

  ngOnInit() {

  }

  get values(): string[] {
    return this.value.split('\n');
  }

}
