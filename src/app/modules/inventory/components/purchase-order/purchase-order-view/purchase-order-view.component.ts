import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService, currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR,DATE_FORMAT_TYPES } from '@app/shared';
import { ConfirmDialogModel, ConfirmDialogPopupComponent } from '@app/shared/components';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
@Component({
  selector: 'app-purchase-order-view',
  templateUrl: './purchase-order-view.component.html',
  styleUrls: ['./purchase-order-view.component.scss']
})
export class PurchaseOrderViewComponent implements OnInit {
  primengTableConfigProperties: any;
  purchaseOrderId: string;
  purchaseOrderDetails: any = null;
  documentsDetails = new Array();
  userData: UserLogin;
  listOfFiles: any[] = [];
  fileList: File[] = [];
  maxFilesUpload: Number = 5;
  FileEnabled = false;
  public formData = new FormData();
  @ViewChild('fileInput', null) myFileInputField: ElementRef;

  selectedFiles = new Array();
  totalFileSize = 0;
  maxFileSize = 104857600; //in Bytes 100MB
  selectedTabIndex = 0;
  stockOrderNumber: any;
  documents: any;
  loggedInUserData;
  stockDetailListDialog: boolean = false;
  stockDetails: any = {};

  constructor(
    private httpService: CrudService, private router: Router,
    private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private store: Store<AppState>,
    private dialog: MatDialog,
  ) {

    this.purchaseOrderId = this.activatedRoute.snapshot.queryParams.id;
    this.selectedTabIndex = this.activatedRoute.snapshot.queryParams.tab ? this.activatedRoute.snapshot.queryParams.tab : 0;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "View Purchase Order",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Ordering', relativeRouterUrl: '' }, { displayName: 'Purchase Order List', relativeRouterUrl: '/inventory/purchase-order' },
      { displayName: 'View Purchase Order', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableViewBtn: true,
            enableClearfix: true,
          }]
      }
    }
  }

  fullListTotalvalue: number = 0;
  receiptingTotal: number = 0;
  barcodes: any = [];
  DATE_FORMAT_TYPES = DATE_FORMAT_TYPES;
  ngOnInit() {
    this.combineLatestNgrxStoreData();

    if (this.purchaseOrderId) {
      this.getPurchaseOrderDetail().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.purchaseOrderDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
          let fullList = response?.resources?.purchaseOrderDetailsFullViewList;
          let receptingTotal = response?.resources?.purchaseOrderDetailsReceptingBatchesList;
          this.fullListTotalvalue = 0;
          this.receiptingTotal = 0;
          fullList.forEach(list => {
            this.fullListTotalvalue += list.totalPrice;
          });
          receptingTotal.forEach(receipt => {
            receipt.receptingBatchesList.forEach(batch => {
              this.receiptingTotal += batch.totalPrice;
            });
          });
          this.barcodes = new Array(response?.resources?.poBarcode);
          //this.rxjsService.setGlobalLoaderProperty(false);

        }
        else {
          this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.PURCHASE_ORDER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getPurchaseOrderDetail(): Observable<IApplicationResponse> {
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PURCHASEORDER_DETAILS, this.purchaseOrderId);
  }

  getPurchaseOrderDocumentsDetail() {
    let params = new HttpParams().set('PurchaseOrderId', this.purchaseOrderId).set('CreatedUserId', this.userData.userId)
      .set('OrderNumber', this.purchaseOrderDetails.orderNumber)
    this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PURCHASE_ORDERS_GET_FILE, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (response.message == "" || response.message == null) {
            this.documents = response.resources;
            this.FileEnabled = true;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.FileEnabled = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  uploadDocuments(type) {
    if (type == 'Edit') {
      this.FileEnabled = true;
    }
    else {
      this.FileEnabled = false;
    }
  }

  // onFileChange uploadFiles
  uploadFiles(event) {
    this.selectedFiles = [];
    const supportedExtensions = ['jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'];
    for (var i = 0; i < event.target.files.length; i++) {
      let selectedFile = event.target.files[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension)) {
        this.selectedFiles.push(event.target.files[i]);
        this.totalFileSize += event.target.files[i].size;
        let temp = {};
        temp['purchaseOrderDocumentId'] = '';
        temp['docName'] = event.target.files[i].name;
        temp['path'] = '';
        this.documentsDetails.push(temp);
      }
      else {
        this.snackbarService.openSnackbar("Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx", ResponseMessageTypes.WARNING);
      }
    }
    if (this.selectedFiles.length > 0) {
      this.Save();
    }
  }

  removeSelectedFile(id, index) {
    if (id) {

      const message = `Are you sure you want to delete this?`;

      const dialogData = new ConfirmDialogModel("Confirm Action", message);

      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData
      });

      dialogRef.afterClosed().subscribe(dialogResult => {
        if (dialogResult) {
          this.httpService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PURCHASEORDER_FILEUPLOAD, id)
            .subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                this.selectedFiles.splice(index, 1);
                this.documentsDetails.splice(index, 1);
              }
            });
        }
      });

    }
  }

  Save(): void {

    if (this.totalFileSize > this.maxFileSize)
      return;

    let obj = {
      PurchaseOrderId: this.purchaseOrderId,
      createdUserId: this.userData.userId
    }
    this.formData.append("creditNoteDetails", JSON.stringify(obj));

    if (this.selectedFiles.length > 0) {
      for (const file of this.selectedFiles) {
        this.formData.append('File', file);
      }
    }

    this.httpService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PURCHASEORDER_FILEUPLOAD, this.formData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.listOfFiles = response.resources;
          this.documentsDetails = response.resources;
          this.FileEnabled = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.selectedFiles = [];
        }
      });

  }
  onTabChange(e): void {
    this.selectedTabIndex = e.index;
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit();
        break;
    }
  }

  navigateToEdit(): void {
    if (!this.primengTableConfigProperties?.tableComponentConfigs?.tabsList[0]?.canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['inventory/purchase-order/add-edit'], { queryParams: { id: this.purchaseOrderId, tab: this.selectedTabIndex }, skipLocationChange: true })
  }

  stockDetail(data: any) {
    if (data == null) return;
    let itemDetails;
    itemDetails = data.serialNumberList != null ? data.serialNumberList?.serialNumber.split(',') : [];

    if (itemDetails?.length > 0) {
      itemDetails.sort();
    }
    this.stockDetails = {
      stockCode: data.itemCode,
      stockDescription: data.itemName,
      serialNumbers: itemDetails
    }
    this.stockDetailListDialog = true;
  }

  stockInfoModal(technicianObj) {

    if (technicianObj.technicianStockCollectionStatus == "Draft") {
      this.router.navigate(['inventory/purchase-order/add-edit'], { queryParams: { tcjId: technicianObj.technicianStockCollectionId, tcjNo: technicianObj.technicianStockCollectionJobNumber, id: this.purchaseOrderId }, skipLocationChange: true });
    }
    else {
      this.router.navigate(['inventory/purchase-order/technician-view'], { queryParams: { id: technicianObj.technicianStockCollectionId, purchaseOrderId: this.purchaseOrderId }, skipLocationChange: true });
    }
  }

}
