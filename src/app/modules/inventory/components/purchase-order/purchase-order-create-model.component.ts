import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-purchase-order-create-model',
  templateUrl: './purchase-order-create-model.component.html'
})
export class PurchaseOrderCreateModelComponent implements OnInit {

  totalSelectedQty = 0;

  constructor(@Inject(MAT_DIALOG_DATA) public popupData: any) { }

  ngOnInit(): void {
    let messageSpan = document.createElement('span');
    messageSpan.innerHTML = this.popupData['message'];
    document.getElementById('parentContainer').appendChild(messageSpan);
  }
}