import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { PurchaseOrderAddEdtModel } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setMinMaxValidator, setRequiredValidator, SnackbarService
} from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { debounceTime, finalize, switchMap, tap } from 'rxjs/operators';
import { PurchaseOrderCreateModelComponent } from '../purchase-order-create-model.component';
import { PurchaseOrderTechnicianDeletePopupComponent } from '@inventory/components/purchase-order';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-purchase-order-edit',
  templateUrl: './purchase-order-edit.component.html',
  styleUrls: ['./purchase-order-edit.component.scss']
})
export class PurchaseOrderEditComponent extends PrimeNgTableVariablesModel implements OnInit {
  PurchaseOrderEditDetail:any;
  purchaseOrderId: string;
  technicianJobId: string;
  purchaseOrderForm: FormGroup;
  purchaseOrderDetails: any = null;
  doc: any;
  userData: UserLogin;
  technicianList = [];
  minLengthValidation: boolean;
  technicianWarehouseList: any = [];
  technicianStockCollectionDocumentList: any = [];
  isLoading: boolean = true;
  collectionAddress: any = [];
  locationPoint: any = '';
  todayDate = new Date();
  actionPO: string = '';
  tcjNumber: string = ''
  searchAddress: any = '';
  selectedFile: any = [];
  tab;
  primengTableConfigProperties: any
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  isFirstTimeAddress: Boolean = true;
  isCollectionAddressSelected: Boolean = false;
  isTotalQtyZero = false;
  technicianJobNo;
  noMatchingAdress: boolean = false;
  startTodayDate=new Date();
  constructor(
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute,
    private snackbarService:SnackbarService,
    private httpService: CrudService, private router: Router,
    private rxjsService: RxjsService,private store: Store<AppState>,
    private dialog: MatDialog, private momentService: MomentService) {
      super();
    this.purchaseOrderId = this.activatedRoute.snapshot.queryParams.id;
    this.technicianJobId = this.activatedRoute.snapshot.queryParams.tcjId;
    this.technicianJobNo = this.activatedRoute.snapshot.queryParams.tcjNo;
    this.tab = this.activatedRoute.snapshot.queryParams.tab;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption:'Create Technician Collection Request ',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Purchase Order List', relativeRouterUrl: '/inventory/purchase-order' }, { displayName: ' View Purchase Order', relativeRouterUrl: '/inventory/purchase-order/view',  queryParams: { id: this.purchaseOrderId,tab:this.tab } }, { displayName: 'Create Technician Collection Request', relativeRouterUrl: '', }],
      tableComponentConfigs: {
          tabsList: [
              {
                  enableAction: true,
                  enableEditActionBtn: false,
                  enableBreadCrumb: true,
              }
          ]
      }
  }
  this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  ngOnInit(): void {
    this.purchaseOrderUpdateForm();
    this.getPurchaseOrderDetailsById();
    this.getTechnicianWarehouseDropdown();

    if (this.purchaseOrderId) {
      if(this.technicianJobId){
        this.getTechnicianJobDetails().subscribe((response: IApplicationResponse) => {
          if(response.resources && response.statusCode === 200){
          this.purchaseOrderDetails = response.resources;
          this.onShowValue(response.resources.purchaseOrderDetailsView);
          this.rxjsService.setGlobalLoaderProperty(false);
          this.purchaseOrderForm.get('technicianId').patchValue(response.resources.technicianId);
          this.purchaseOrderForm.get('collectionDate').patchValue(new Date(response.resources.collectionDate));
          this.purchaseOrderForm.get('description').setValue(response.resources.collectionAddress == null ? '' : response.resources.collectionAddress);
          this.locationPoint = response.resources.locationPoint;
          let locPoint = this.locationPoint.split("  ");
          this.isFirstTimeAddress = true;
          this.purchaseOrderForm.get('latitude').patchValue(locPoint[0])
          this.purchaseOrderForm.get('longitude').patchValue(locPoint[1])
          for (let i = 0; i < this.purchaseOrderDetails.technicianStockCollectionStockList.length; i++) {
            let stockCodesDetailsFormGroup = this.formBuilder.group(
              {
                'purchaseOrderId': this.purchaseOrderDetails.technicianStockCollectionStockList[i]['purchaseOrderId'],
                'stockOrderItemId': this.purchaseOrderDetails.technicianStockCollectionStockList[i]['stockOrderItemId'],
                'technicianStockCollectionItemId': this.purchaseOrderDetails.technicianStockCollectionStockList[i]['technicianStockCollectionItemId'],
                'itemCode': this.purchaseOrderDetails.technicianStockCollectionStockList[i]['itemCode'],
                'itemName': this.purchaseOrderDetails.technicianStockCollectionStockList[i]['itemName'],
                'poQty': this.purchaseOrderDetails.technicianStockCollectionStockList[i]['poQty'],
                'receivedQty': this.purchaseOrderDetails.technicianStockCollectionStockList[i]['receivedQty'],
                'varianceQty': this.purchaseOrderDetails.technicianStockCollectionStockList[i]['pendingCollectionJobQty'],
                'qtyToCollect': new FormControl({ value:  response.resources.technicianStockCollectionStockList[i]['technicianStockCollectionQtyToCollect'], disabled: ((this.purchaseOrderDetails.technicianStockCollectionStockList[i]['poQty'] - (this.purchaseOrderDetails.technicianStockCollectionStockList[i]['receivedQty'] + this.purchaseOrderDetails.technicianStockCollectionStockList[i]['pendingCollectionJobQty'])) != 0 ? false : true) }), // , Validators.required
                'PoQtyMinusPendingTechnicianCollectionJobQty': [{ value: (this.purchaseOrderDetails.technicianStockCollectionStockList[i]['poQty'] - (this.purchaseOrderDetails.technicianStockCollectionStockList[i]['receivedQty'] + this.purchaseOrderDetails.technicianStockCollectionStockList[i]['pendingCollectionJobQty'])), disabled: true }]
              }
            )
            stockCodesDetailsFormGroup.get('qtyToCollect').valueChanges.subscribe(qtyToCollect => {
              this.isTotalQtyZero = false;
            });
            this.purchaseOrderFormArray.push(stockCodesDetailsFormGroup);
            stockCodesDetailsFormGroup = setMinMaxValidator(stockCodesDetailsFormGroup, [
              { formControlName: 'qtyToCollect', compareWith: 'PoQtyMinusPendingTechnicianCollectionJobQty', type: 'minEqual', skipZero: true }
            ]);
  
          }
          if(response.resources.validateMessage){
            this.snackbarService.openSnackbar(response.resources.validateMessage, ResponseMessageTypes.ERROR);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        })
      }else {
        this.getPurchaseOrderDetailsById().subscribe((response: IApplicationResponse) => {
          if(response.resources && response.statusCode === 200){
          this.purchaseOrderDetails = response.resources;
          this.onShowValue(response.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
          this.purchaseOrderForm.get('description').setValue(response.resources.collectionAddress == null ? '' : response.resources.collectionAddress);
          this.locationPoint = response.resources.locationPoint;
          let locPoint = this.locationPoint.split("  ");
          this.isFirstTimeAddress = true;
          this.purchaseOrderForm.get('latitude').patchValue(locPoint[0])
          this.purchaseOrderForm.get('longitude').patchValue(locPoint[1])
          for (let i = 0; i < this.purchaseOrderDetails.purchaseOrderDetailsFullViewList.length; i++) {
            let stockCodesDetailsFormGroup = this.formBuilder.group(
              {
                'purchaseOrderId': this.purchaseOrderDetails.purchaseOrderDetailsFullViewList[i]['purchaseOrderId'],
                'stockOrderItemId': this.purchaseOrderDetails.purchaseOrderDetailsFullViewList[i]['stockOrderItemId'],
                'technicianStockCollectionItemId': this.purchaseOrderDetails.purchaseOrderDetailsFullViewList[i]['technicianStockCollectionItemId'],
                'itemCode': this.purchaseOrderDetails.purchaseOrderDetailsFullViewList[i]['itemCode'],
                'itemName': this.purchaseOrderDetails.purchaseOrderDetailsFullViewList[i]['itemName'],
                'poQty': this.purchaseOrderDetails.purchaseOrderDetailsFullViewList[i]['poQty'],
                'receivedQty': this.purchaseOrderDetails.purchaseOrderDetailsFullViewList[i]['receivedQty'],
                'varianceQty': this.purchaseOrderDetails.purchaseOrderDetailsFullViewList[i]['pendingCollectionJobQty'],
                'qtyToCollect': new FormControl({ value: '', disabled: ((this.purchaseOrderDetails.purchaseOrderDetailsFullViewList[i]['poQty'] - (this.purchaseOrderDetails.purchaseOrderDetailsFullViewList[i]['receivedQty'] + this.purchaseOrderDetails.purchaseOrderDetailsFullViewList[i]['pendingCollectionJobQty'])) != 0 ? false : true) }), // , Validators.required
                'PoQtyMinusPendingTechnicianCollectionJobQty': [{ value: (this.purchaseOrderDetails.purchaseOrderDetailsFullViewList[i]['poQty'] - (this.purchaseOrderDetails.purchaseOrderDetailsFullViewList[i]['receivedQty'] + this.purchaseOrderDetails.purchaseOrderDetailsFullViewList[i]['pendingCollectionJobQty'])), disabled: true }]
              }
            )
            stockCodesDetailsFormGroup.get('qtyToCollect').valueChanges.subscribe(qtyToCollect => {
              this.isTotalQtyZero = false;
            });
            this.purchaseOrderFormArray.push(stockCodesDetailsFormGroup);
            stockCodesDetailsFormGroup = setMinMaxValidator(stockCodesDetailsFormGroup, [
              { formControlName: 'qtyToCollect', compareWith: 'PoQtyMinusPendingTechnicianCollectionJobQty', type: 'minEqual', skipZero: true }
            ]);
  
          }
          if(response.resources.validateMessage){
            this.snackbarService.openSnackbar(response.resources.validateMessage, ResponseMessageTypes.ERROR);
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
        })
      }
     
    }
    else {
      this.actionPO = "- Draft";
      this.getTechnicianJobDetails().subscribe((response: IApplicationResponse) => {
        if(response.resources && response.statusCode === 200){
        this.tcjNumber = response.resources.technicianStockCollectionJobNumber;
        this.technicianStockCollectionDocumentList = response.resources.technicianStockCollectionDocumentList;
        this.purchaseOrderDetails = response.resources;
        this.onShowValue(response.resources.purchaseOrderDetailsView);
        this.purchaseOrderForm.get('technicianWarehouseId').patchValue(response.resources.technicianWarehouseId);
        this.purchaseOrderForm.get('technicianId').patchValue(response.resources.technicianId);
        this.purchaseOrderForm.get('collectionDate').patchValue(new Date(response.resources.collectionDate));
        this.todayDate = new Date(response.resources.collectionDate);
        this.purchaseOrderForm.get('description').patchValue(response.resources.collectionAddress == null ? '' : response.resources.collectionAddress);
        this.locationPoint = response.resources.locationPoint;
        this.isFirstTimeAddress = true;
        this.rxjsService.setGlobalLoaderProperty(false);
        for (let i = 0; i < response.resources.technicianStockCollectionStockList?.length; i++) {
          let stockCodesDetailsFormGroup = this.formBuilder.group(
            {
              'purchaseOrderId': response.resources.technicianStockCollectionStockList[i]['purchaseOrderId'],
              'stockOrderItemId': response.resources.technicianStockCollectionStockList[i]['stockOrderItemId'],
              'technicianStockCollectionItemId': response.resources.technicianStockCollectionStockList[i]['technicianStockCollectionItemId'],
              'itemCode': response.resources.technicianStockCollectionStockList[i]['itemCode'],
              'itemName': response.resources.technicianStockCollectionStockList[i]['itemName'],
              'poQty': response.resources.technicianStockCollectionStockList[i]['poQty'],
              'receivedQty': response.resources.technicianStockCollectionStockList[i]['receivedQty'],
              'varianceQty': response.resources.technicianStockCollectionStockList[i]['pendingCollectionJobQty'],
              'qtyToCollect': new FormControl({ value: response.resources.technicianStockCollectionStockList[i]['technicianStockCollectionQtyToCollect'], disabled: ((response.resources.technicianStockCollectionStockList[i]['poQty'] - (response.resources.technicianStockCollectionStockList[i]['receivedQty'] + response.resources.technicianStockCollectionStockList[i]['pendingCollectionJobQty'])) != 0 ? false : true) }), // , Validators.required
              'PoQtyMinusPendingTechnicianCollectionJobQty': [{ value: (response.resources.technicianStockCollectionStockList[i]['poQty'] - (response.resources.technicianStockCollectionStockList[i]['receivedQty'] + response.resources.technicianStockCollectionStockList[i]['pendingCollectionJobQty'])), disabled: true }]
            }
          )
          stockCodesDetailsFormGroup.get('qtyToCollect').valueChanges.subscribe(qtyToCollect => {
            this.isTotalQtyZero = false;
          });
          stockCodesDetailsFormGroup = setMinMaxValidator(stockCodesDetailsFormGroup, [
            { formControlName: 'qtyToCollect', compareWith: 'PoQtyMinusPendingTechnicianCollectionJobQty', type: 'minEqual', skipZero: true }
          ]);
          this.purchaseOrderFormArray.push(stockCodesDetailsFormGroup);
        }
      }
      this.rxjsService.setGlobalLoaderProperty(false);
      })
    }

    this.purchaseOrderForm.get('description').valueChanges.pipe(
      debounceTime(300),
      tap(() => (this.isLoading = true)),
      switchMap((value) => {
        this.searchAddress = value;
        if (!value) {
          this.isLoading = false;
          this.locationPoint = null;
          this.minLengthValidation = false;
          this.noMatchingAdress = false;
          return (this.collectionAddress = []);
        } else if (typeof value === 'object') {
          this.isLoading = false;
          this.locationPoint = null;
          this.minLengthValidation = false;
          this.noMatchingAdress = false;
          return (this.collectionAddress = []);
        } else {
          if (this.isCollectionAddressSelected == false && this.isFirstTimeAddress == false) {
            this.locationPoint = null;
          }
          return this.httpService
            .get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_SUPPLIERADDRESS_SEARCH,
              null,
              undefined,
              prepareGetRequestHttpParams(null, null, {
                Search: value,
                SupplierId: this.purchaseOrderDetails?.purchaseOrderDetailsView?.supplierId ? this.purchaseOrderDetails?.purchaseOrderDetailsView?.supplierId:this.purchaseOrderDetails?.supplierId,
              })
            )
            .pipe(finalize(() => (this.isLoading = false)));
        }
      })
    )
      .subscribe((results: IApplicationResponse) => {
        if (results.resources != undefined) {
          this.isFirstTimeAddress = false;
          this.isCollectionAddressSelected = false;
          if (results.resources.length > 0) {
            this.collectionAddress = results.resources;
            if(this.collectionAddress[0].description.includes('No matching address found')){
              this.noMatchingAdress=true;
              this.collectionAddress =[];
            }else{
              this.noMatchingAdress=false;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          } else {
            let filterSearch$ = this.filterServiceTypeDetailsBySearchOptions(this.searchAddress);
            if (filterSearch$) {
              filterSearch$.subscribe(results => {
                if (results.isSuccess && results.resources.length > 0) {
                  this.collectionAddress = results.resources;
                  if(this.collectionAddress[0].description.includes('No matching address found')){
                    this.noMatchingAdress=true;
                    this.collectionAddress =[];
                  }else{
                    this.noMatchingAdress=false;
                  }
                } else {
                  this.collectionAddress = [];
                  this.noMatchingAdress=false;
                }
              });
            } else {
              this.collectionAddress = [];
              this.noMatchingAdress=false;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        }
        else {
          this.isLoading = false;
          this.noMatchingAdress=false;
          return this.collectionAddress = [];
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    this.purchaseOrderForm.get('technicianWarehouseId').valueChanges.subscribe(data => {
      let warehouseId = this.purchaseOrderForm.get('technicianWarehouseId').value;
      if(warehouseId)
        this.getTechnicianDropdown(warehouseId);
    })
  }

  onShowValue(response?: any) {
    this.PurchaseOrderEditDetail = [
          { name: 'PO Number', value: response?.orderNumber},
          { name: 'PO Barcode', value: response?.poBarcode},
          { name: 'Document Date', value: response?.orderDate},
          { name: 'Warehouse', value: response?.warehouseName},
          { name: 'Older Type', value: response?.purchaseOrderTypeName},
          { name: 'Priority', value: response?.purchaseOrderPriorityName},
          { name: 'Supplier', value: response?.supplierName},
          { name: 'Stock Order No', value: response?.stockOrderNumber},
          { name: 'Status', value: response?.purchaseOrderStatusName, statusClass: response ? response?.cssClass : '' },
    ]
  }

  filterServiceTypeDetailsBySearchOptions(searchValue?: string): Observable<IApplicationResponse> { //
    this.minLengthValidation = false;
    if (searchValue.length < 3) {
      this.minLengthValidation = true;
      return;
    } else {
      return this.httpService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_GET_ADDRESS_FROM_AFRIGIS, null, true, prepareGetRequestHttpParams(null, null, {
        searchtext: searchValue,
        IsAfrigisSearch :true
      }), 1);
    }
  }

  getTechnicianJobDetails() {
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.TECHNICIAN_JOB_DETAILS, this.technicianJobId)
  }

  getTechnicianWarehouseDropdown() {
    let httpParams = new HttpParams();
    this.httpService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSES, prepareRequiredHttpParams({ userId: this.userData?.userId }))
      .subscribe((response) => {
        if (response.statusCode == 200) {
          this.purchaseOrderForm.get('technicianWarehouseId').setValue(this.userData.warehouseId)
          this.rxjsService.setGlobalLoaderProperty(false);
          this.technicianWarehouseList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getTechnicianDropdown(warehouseId) {    
    let params=null;
    if(warehouseId)
     params =  { WarehouseId: warehouseId ,userId:this.userData.userId};
    else {
      params =  { userId:this.userData.userId};
    }
    let httpParams = new HttpParams();
    this.httpService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_TECHNICIAN_SEARCH, prepareGetRequestHttpParams(null, null, params)).subscribe((response) => {
      if (response.statusCode == 200) {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.technicianList = response.resources;
      }
    });
  }

  purchaseOrderUpdateForm(): void {
    let purchase = new PurchaseOrderAddEdtModel();
    this.purchaseOrderForm = this.formBuilder.group({});
    let purchaseOrderStockFormArray = this.formBuilder.array([]);

    Object.keys(purchase).forEach((key) => {
      if (key === 'technicianStockCollectionItemList') {
        this.purchaseOrderForm.addControl(key, purchaseOrderStockFormArray);
      }
      else {
        this.purchaseOrderForm.addControl(key, new FormControl(purchase[key]));
      }
    });
    this.purchaseOrderForm = setRequiredValidator(this.purchaseOrderForm, ['technicianWarehouseId', 'technicianId', 'collectionDate','description']);
  }

  get purchaseOrderFormArray(): FormArray {
    if (this.purchaseOrderForm !== undefined) {
      return (<FormArray>this.purchaseOrderForm.get('technicianStockCollectionItemList'));
    }
  }

  //Get Details
  getPurchaseOrderDetailsById(): Observable<IApplicationResponse> {
    return this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.PURCHASEORDER_DETAILS,
      this.purchaseOrderId
    );
  }

  saveAsDraft() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.purchaseOrderForm.invalid || this.locationPoint == null) {
      this.purchaseOrderForm.get('description').markAsTouched();
      return;
    }
    const formData = new FormData();
    if (this.selectedFile.length > 0) {
      for (const file of this.selectedFile) {
        formData.append('document_files[]', file);
      }
    }
    let purchaseOrderDetail = this.purchaseOrderForm.getRawValue();
    let purchaseOrderSaveAsDraft = {
      "technicianStockCollectionDetail": {
        "technicianStockCollectionId": this.technicianJobId == undefined ? null : this.technicianJobId,
        "purchaseOrderId": this.purchaseOrderDetails.purchaseOrderId,
        "collectionDate": purchaseOrderDetail.collectionDate,
        "technicianId": purchaseOrderDetail.technicianId,
        "technicianWarehouseId": purchaseOrderDetail.technicianWarehouseId,
        "createdUserId": this.userData.userId,
        "isDraft": true,
        "latitude": this.purchaseOrderForm.get('latitude').value,
        "longitude": this.purchaseOrderForm.get('longitude').value,
        "description": purchaseOrderDetail.description
      },
      "technicianStockCollectionItemList": []
    }
    for (let i = 0; i < purchaseOrderDetail.technicianStockCollectionItemList.length; i++) {
      purchaseOrderSaveAsDraft.technicianStockCollectionItemList.push({
        "technicianStockCollectionId": this.technicianJobId == undefined ? null : this.technicianJobId,
        "technicianStockCollectionItemId": this.technicianJobId == undefined ? null : purchaseOrderDetail.technicianStockCollectionItemList[i].technicianStockCollectionItemId,
        "itemId": purchaseOrderDetail.technicianStockCollectionItemList[i].stockOrderItemId,
        "qtyToCollect": purchaseOrderDetail.technicianStockCollectionItemList[i].qtyToCollect,
        "createdUserId": this.userData.userId
      })
    }
    formData.append("purchaseOrderSaveAsDraft", JSON.stringify(purchaseOrderSaveAsDraft));
    this.httpService.create(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_COLLECTION_CREATE, formData).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.router.navigate(["inventory/purchase-order/view"], { queryParams: { id: this.purchaseOrderDetails.purchaseOrderId } });
      })
  }

  poCreate() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.purchaseOrderForm.invalid || this.locationPoint == null) {
      this.purchaseOrderForm.get('description').markAsTouched();
      return;
    }
    let purchaseOrderDetail = this.purchaseOrderForm.getRawValue();
    if (purchaseOrderDetail.technicianStockCollectionItemList.find(v => v['qtyToCollect'] != '' && v['qtyToCollect'] != 0) == undefined) {
      this.isTotalQtyZero = true;
      return;
    } else {
      this.isTotalQtyZero = false;
    }
    let technicianName = this.technicianList.find(x => x.id == this.purchaseOrderForm.get('technicianId').value).displayName;
    const dialogReff = this.dialog.open(PurchaseOrderCreateModelComponent, {
      width: '450px',
      data: {
        message: `Are you sure you want to create the Technician Collection Job for <b>${technicianName}</b>`,
        buttons: {
          cancel: 'No',
          create: 'Yes'
        },
        action: 'Create'
      }, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (!result) return;

      const formData = new FormData();
      if (this.selectedFile.length > 0) {
        for (const file of this.selectedFile) {
          formData.append('document_files[]', file);
        }
      }
      purchaseOrderDetail.collectionDate = this.momentService.toMoment(purchaseOrderDetail.collectionDate).format('YYYY-MM-DDThh:mm:ss[Z]');
      let purchaseOrderSaveAsDraft = {
        "technicianStockCollectionDetail": {
          "technicianStockCollectionId": this.technicianJobId == undefined ? null : this.technicianJobId,
          "purchaseOrderId": this.purchaseOrderDetails.purchaseOrderId,
          "collectionDate": purchaseOrderDetail.collectionDate,
          "technicianId": purchaseOrderDetail.technicianId,
          "technicianWarehouseId": purchaseOrderDetail.technicianWarehouseId,
          "createdUserId": this.userData.userId,
          "isDraft": false,
          "latitude": this.purchaseOrderForm.get('latitude').value,
          "longitude": this.purchaseOrderForm.get('longitude').value,
          "description": this.purchaseOrderForm.get('description').value
        },
        "technicianStockCollectionItemList": []
      }
      for (let i = 0; i < purchaseOrderDetail.technicianStockCollectionItemList.length; i++) {
        if (purchaseOrderDetail.technicianStockCollectionItemList[i].qtyToCollect > 0) {
        purchaseOrderSaveAsDraft.technicianStockCollectionItemList.push({
          "technicianStockCollectionId": this.technicianJobId == undefined ? null : this.technicianJobId,
          "technicianStockCollectionItemId": this.technicianJobId == undefined ? null : purchaseOrderDetail.technicianStockCollectionItemList[i].technicianStockCollectionItemId,
          "itemId": purchaseOrderDetail.technicianStockCollectionItemList[i].stockOrderItemId,
          "qtyToCollect": purchaseOrderDetail.technicianStockCollectionItemList[i].qtyToCollect,
          "createdUserId": this.userData.userId
        })
      }
      }
      formData.append("purchaseOrderSaveAsDraft", JSON.stringify(purchaseOrderSaveAsDraft));
      this.httpService.create(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_COLLECTION_CREATE, formData).subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.router.navigate(["inventory/purchase-order/view"], { queryParams: { id: this.purchaseOrderDetails.purchaseOrderId } });
        })

    });
  }

  poDelete() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    const dialogReff = this.dialog.open(PurchaseOrderCreateModelComponent, {
      width: '450px',
      data: {
        message: `Are you sure you want to delete this Draft : <b>${this.tcjNumber}</b>`,
        buttons: {
          cancel: 'No',
          create: 'Yes'
        },
        action: 'Delete'
      }, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (!result) return;
      this.httpService.delete(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_COLLECTION_DELETE, null, prepareGetRequestHttpParams(null, null, {
          ids: this.technicianJobId,
          isDeleted: false
        })).subscribe((response: IApplicationResponse) => {
          this.router.navigate(["inventory/purchase-order/view"], { queryParams: { id: this.purchaseOrderDetails.purchaseOrderId } });
        })
    })
  }

  uploadFiles(event) {
    let fileUploadObj = {
      "PurchaseOrderId": this.purchaseOrderId,
      "createdUserId": this.userData.userId
    }
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedFile.push(event.target.files[i]);
    }
  }

  removeSelectedFile(index) {
    this.selectedFile.splice(index, 1);
  }

  removeDocFile(docObj, index) {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.httpService.delete(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_COLLECTION_DOCUMENT, null, prepareGetRequestHttpParams(null, null, {
        ids: docObj.technicianStockCollectionDocumentId,
        isDeleted: false
      })).subscribe((response: IApplicationResponse) => {
        this.technicianStockCollectionDocumentList.splice(index, 1);
      })
  }

  onCollectionAddressSelected(selected, address) {
    if (address.seoid == undefined) {
      this.locationPoint = address.latitude + " " + address.longitude;
      this.purchaseOrderForm.get('latitude').patchValue(address.latitude)
      this.purchaseOrderForm.get('longitude').patchValue(address.longitude)
      this.isCollectionAddressSelected = true;
    }
    else {
      this.httpService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.POST_SELECTED_ADDRESS, null, true, prepareGetRequestHttpParams(null, null, {
        seoid: address.seoid
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.purchaseOrderForm.get('latitude').patchValue(res.resources.addressDetails[0].geometry.location.lat)
          this.purchaseOrderForm.get('longitude').patchValue(res.resources.addressDetails[0].geometry.location.lng)
          this.locationPoint = this.purchaseOrderForm.get('latitude').value + " " + this.purchaseOrderForm.get('longitude').value
          this.isCollectionAddressSelected = true;
        }
      })
    }
  }
  redirectToViewPo() {
    this.router.navigate(["inventory/purchase-order/view"], { queryParams: { id: this.purchaseOrderId } });
  }
  cancelJob() {
    const dialogReff = this.dialog.open(PurchaseOrderTechnicianDeletePopupComponent, {
      width: '450px',
      data: {
        message: `Are you sure you want to cancel the<br />Technician Collection Job: <span class="font-weight-bold">${this.technicianJobNo} ?</span>`,
        buttons: {
          cancel: 'Cancel',
          create: 'Yes'
        }
      }, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (!result) return;
      let data;
      data = Object.assign({},
        //this.technicianStockCollectionFormGroup.get('technicianStockCollectionId').value == '' ? null : { technicianStockCollectionId: this.technicianStockCollectionFormGroup.get('technicianStockCollectionId').value },
        this.userData.userId == '' ? null : { modifiedUserId: this.userData.userId },
        result == '' ? null : { reason: result }
      );
      data.technicianStockCollectionId = this.technicianJobId,
      this.httpService.update(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_COLLECTION_CANCEL,
        data
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.navigateToTCJView();
        }
      })
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
  navigateToTCJView() {
    this.router.navigate(["inventory/purchase-order/view"], {
      queryParams:
      {
        id: this.purchaseOrderId,
      }, skipLocationChange: true
    });
  }
}
