import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import {
    PurchaseOrderBarcodeModalComponent, PurchaseOrderEditComponent, PurchaseOrderListComponent, PurchaseOrderRoutingModule, PurchaseOrderTechinicianViewComponent, PurchaseOrderTechnicianDeletePopupComponent, PurchaseOrderTechnicianEditComponent, PurchaseOrderViewComponent, StockCodeInfoPopupComponent
} from '@inventory/components/purchase-order';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { BarcodePrinterModule } from '../barcode-printer/barcode-printer/barcode-printer.module';
import { PurchaseOrderCreateModelComponent } from './purchase-order-create-model.component';

@NgModule({
    declarations: [
        PurchaseOrderListComponent,
        PurchaseOrderViewComponent,
        PurchaseOrderEditComponent,
        PurchaseOrderTechinicianViewComponent,
        PurchaseOrderTechnicianEditComponent,
        PurchaseOrderCreateModelComponent,
        StockCodeInfoPopupComponent,
        PurchaseOrderBarcodeModalComponent,
        PurchaseOrderTechnicianDeletePopupComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule, FormsModule,
        PurchaseOrderRoutingModule,
        LayoutModule,
        MaterialModule,
        BarcodePrinterModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
    ],
    entryComponents: [StockCodeInfoPopupComponent, PurchaseOrderBarcodeModalComponent, PurchaseOrderTechnicianDeletePopupComponent, PurchaseOrderCreateModelComponent]

})

export class PurchaseOrderModule { }
