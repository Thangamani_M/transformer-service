import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@app/modules';
import { PurchaseOrderListFilter } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import {
  CrudType, exportList, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix,
  prepareDynamicTableTabsFromPermissions,currentComponentPageBasedPermissionsSelector$,
  prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService, PERMISSION_RESTRICTION_ERROR
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-purchase-order-list',
  templateUrl: './purchase-order-list.component.html',
  styleUrls: ['./purchase-order-list.component.scss']
})
export class PurchaseOrderListComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties: any;
  row: any = {}
  dateFormat = 'MMM dd, yyyy';
  showFilterForm = false;
  warehouseList = [];
  stockOrderList = [];
  stockCodeList = [];
  purchaseOrderFilterForm: FormGroup;
  purchaseOrderList = [];
  suppliersList = [];
  userData: UserLogin;
  otherParams:any={};
  startTodayDate=0;
  constructor(private crudService: CrudService, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>,private rxjsService: RxjsService){
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Purchase Order",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Ordering', relativeRouterUrl: '' }, 
      { displayName: 'Purchase Order List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Purchase Order List',
            dataKey: 'stockId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
             shouldShowFilterActionBtn: true,
            enableHyperLink: true,
            enableExportBtn: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            enableAction: true,
            enablePrintBtn: true,
            printTitle: 'Stock Management',
            printSection: 'print-section0',
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'poNumber', header: 'PO Number', width: '200px' }, { field: 'status', header: 'Status', width: '200px' }, { field: 'orderDate', header: 'Documented Date', width: '200px' },
            { field: 'warehouseName', header: 'Warehouse', width: '200px' }, { field: 'requisitionNumber', header: 'Requisition Request Number', width: '250px' },
            { field: 'stockOrderNumber', header: 'Stock Order Number', width: '200px' }, { field: 'poBarcode', header: 'PO Bar Code', width: '200px' },
            { field: 'supplierName', header: 'Supplier', width: '200px' }],
            apiSuffixModel: InventoryModuleApiSuffixModels.PURCHASE_ORDER,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.createpurchaseOrderFilterForm();
    this.displayAndLoadFilterData();
    this.getWarehouseOptions();
    this.getStockCodeOptions();
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.PURCHASE_ORDER]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = {...otherParams, ...{ UserId: this.userData.userId}}
    this.otherParams = otherParams;
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,  otherParams )
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources && res?.statusCode == 200) {
        res?.resources?.forEach(val => {
          val.orderDate = this.datePipe.transform(val?.orderDate, 'yyyy-MM-dd HH:mm:ss');
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      //orderDate
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess && data?.statusCode == 200) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } 
      else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = data.totalCount;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row;
        this.getRequiredListData(row["pageIndex"],row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
        case CrudType.EXPORT:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
           }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
          let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
          this.exportList(pageIndex,pageSize );
          break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(["inventory/purchase-order/view"], {
          queryParams:
          {
            id: editableObject['purchaseOrderId'],
          }, skipLocationChange: true
        });
        break;
    }
  }
  exportList(pageIndex?: any, pageSize?: any, otherParams?: object) {
    exportList(this.otherParams,0,pageSize,ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.PURCHASE_ORDER_EXPORT,this.crudService,this.rxjsService,'Purchase Order');
  }

  exportListOld() {
    if (this.dataList.length != 0) {
      let fileName = 'Purchase Order ' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
      let columnNames = ["PO Number", "Documented Date", "Warehouse", "Requisition Request Number", "Stock Order Number", "PO Bar Code", "Supplier", "Status"];
      let header = columnNames.join(',');
      let csv = header;
      csv += '\r\n';
      this.dataList.map(c => {
        csv += [c["poNumber"], c["orderDate"], c["warehouseName"], c['requisitionNumber'], c['stockOrderNumber'], c['poBarcode'], c['supplierName'], c['status']].join(',');
        csv += '\r\n';
      })
      var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
      var link = document.createElement("a");
      if (link.download !== undefined) {
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
    else {
      this.snackbarService.openSnackbar('Please select atleast one checkbox', ResponseMessageTypes.WARNING);
    }
  }

  displayAndLoadFilterData() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PURCHASEORDER_SEARCH).subscribe((response: IApplicationResponse) => {
      if (response.resources && response.statusCode === 200) {
        this.purchaseOrderList = [];
        let purchaseOrderList = response.resources;
        for (var i = 0; i < purchaseOrderList.length; i++) {
          if(purchaseOrderList[i].displayName != null){
            let tmp = {};
            tmp['value'] = purchaseOrderList[i].id;
            tmp['display'] = purchaseOrderList[i].displayName;
            this.purchaseOrderList.push(tmp)  
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UI_WAREHOUSES, null, false, prepareRequiredHttpParams({userId: this.loggedInUserData?.userId})).subscribe((response: IApplicationResponse) => {
      if (response.resources && response.statusCode === 200) {
        this.warehouseList = [];
        let warehouseList = response.resources;
        for (var i = 0; i < warehouseList.length; i++) {
          if(warehouseList[i].displayName != null){
            let tmp = {};
            tmp['value'] = warehouseList[i].id;
            tmp['display'] = warehouseList[i].displayName;
            this.warehouseList.push(tmp)  
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  createpurchaseOrderFilterForm(requisitionListModel?: PurchaseOrderListFilter) {
    let purchaseListFilterModel = new PurchaseOrderListFilter(requisitionListModel);
    this.purchaseOrderFilterForm = this.formBuilder.group({});
    Object.keys(purchaseListFilterModel).forEach((key) => {
      if (typeof purchaseListFilterModel[key] === 'string') {
        this.purchaseOrderFilterForm.addControl(key, new FormControl(purchaseListFilterModel[key]));
      }
    });
  }

  getWarehouseOptions(): void {
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIERS),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STOCK_ORDER),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              let suppliersList = resp.resources;
              this.suppliersList = [];
              for (var i = 0; i < suppliersList.length; i++) {
                if(suppliersList[i].displayName != null){
                  let tmp = {};
                  tmp['value'] = suppliersList[i].id;
                  tmp['display'] = suppliersList[i].displayName;
                  this.suppliersList.push(tmp)  
                }
              }
              break;
            case 1:
              let stockOrderList = resp.resources;
              this.stockOrderList = [];
              for (var i = 0; i < stockOrderList.length; i++) {
                if(stockOrderList[i].displayName != null){
                  let tmp = {};
                  tmp['value'] = stockOrderList[i].id;
                  tmp['display'] = stockOrderList[i].displayName;
                  this.stockOrderList.push(tmp)  
                }
              }
              break;
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getStockCodeOptions(): void {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM).subscribe((response: IApplicationResponse) => {
      if (response.resources && response.statusCode === 200) {
        this.stockCodeList = [];
        let stockCodeList = response.resources;
        for (var i = 0; i < stockCodeList.length; i++) {
          if(stockCodeList[i].itemCode != null){
            let tmp = {};
            tmp['value'] = stockCodeList[i].id;
            tmp['display'] = stockCodeList[i].itemCode;
            this.stockCodeList.push(tmp)  
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  submitFilter() {
    let data = Object.assign({},
      this.purchaseOrderFilterForm.get('poNumber').value == '' || this.purchaseOrderFilterForm.get('poNumber').value == null ? null : 
      { poNumbers: this.purchaseOrderFilterForm.get('poNumber').value.join() },

      this.purchaseOrderFilterForm.get('warehouseId').value == '' || this.purchaseOrderFilterForm.get('warehouseId').value == null ? null : 
      { warehouseId: this.purchaseOrderFilterForm.get('warehouseId').value },

      this.purchaseOrderFilterForm.get('supplierId').value == '' || this.purchaseOrderFilterForm.get('supplierId').value == null ? null : 
      { supplierId: this.purchaseOrderFilterForm.get('supplierId').value },

      this.purchaseOrderFilterForm.get('fromDate').value == '' || this.purchaseOrderFilterForm.get('fromDate').value == null ? null : 
      { fromDate: this.datePipe.transform(this.purchaseOrderFilterForm.get('fromDate').value, 'yyyy-MM-dd') },

      this.purchaseOrderFilterForm.get('toDate').value == '' || this.purchaseOrderFilterForm.get('toDate').value == null ? null : 
      { toDate: this.datePipe.transform(this.purchaseOrderFilterForm.get('toDate').value, 'yyyy-MM-dd') },

      this.purchaseOrderFilterForm.get('stockOrderId').value == '' || this.purchaseOrderFilterForm.get('stockOrderId').value == null ? null : 
      { stockOrderId: this.purchaseOrderFilterForm.get('stockOrderId').value },

      this.purchaseOrderFilterForm.get('stockCodeId').value == '' || this.purchaseOrderFilterForm.get('stockCodeId').value == null ? null : 
      { itemId: this.purchaseOrderFilterForm.get('stockCodeId').value },

      this.purchaseOrderFilterForm.get('scanBarcode').value == '' || this.purchaseOrderFilterForm.get('scanBarcode').value == null ? null : 
      { scanBarcode: this.purchaseOrderFilterForm.get('scanBarcode').value }, { UserId: this.userData.userId }
    );
   this.getRequiredListData('', '', data);
    this.showFilterForm = !this.showFilterForm;
  }

  searchInputField() {
    if (this.purchaseOrderFilterForm.get('scanBarcode').value) {
      let input = Object.assign({},
        this.purchaseOrderFilterForm.get('scanBarcode').value == '' ? null : { POBarcode: this.purchaseOrderFilterForm.get('scanBarcode').value }
      );
      this.getRequiredListData('', '', input);
      this.showFilterForm = !this.showFilterForm;
    }
  }

  resetForm() {
    this.purchaseOrderFilterForm.reset();
    this.getRequiredListData('', '', null);
    this.showFilterForm = !this.showFilterForm;
  }
}
