import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { stockOrderViewModel } from '@app/modules/inventory/models';
import { RxjsService } from '@app/shared';

@Component({
  selector: 'app-purchase-order-technician-delete-popup',
  templateUrl: './purchase-order-technician-delete-popup.component.html',
  styleUrls: ['./purchase-order-technician-delete-popup.component.scss']
})

export class PurchaseOrderTechnicianDeletePopupComponent implements OnInit {

  reasonText = '';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: stockOrderViewModel,
    private rxjsService: RxjsService) { }

  ngOnInit(): void {
    let messageSpan = document.createElement('span');
    messageSpan.innerHTML = this.data['message'];
    document.getElementById('parentContainer').appendChild(messageSpan);
    this.rxjsService.setDialogOpenProperty(true);
  }
  ngOnDestroy(): void {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
