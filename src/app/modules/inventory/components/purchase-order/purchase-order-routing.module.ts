import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PurchaseOrderEditComponent, PurchaseOrderListComponent, PurchaseOrderTechinicianViewComponent, PurchaseOrderTechnicianEditComponent, PurchaseOrderViewComponent } from '@inventory/components/purchase-order';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
    { path: '', component: PurchaseOrderListComponent, data: { title: 'Purchase Order List' } ,canActivate:[AuthGuard]},
    { path: 'view', component: PurchaseOrderViewComponent, data: { title: 'View Purchase Order' } ,canActivate:[AuthGuard]},
    { path: 'add-edit', component: PurchaseOrderEditComponent, data: { title: 'Create Technician Job' } ,canActivate:[AuthGuard]},
    { path: 'technician-view', component: PurchaseOrderTechinicianViewComponent, data: { title: 'View Technician Job' },canActivate:[AuthGuard] },
    { path: 'technician-edit', component: PurchaseOrderTechnicianEditComponent, data: { title: 'Update Technician Job' },canActivate:[AuthGuard] }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class PurchaseOrderRoutingModule { }