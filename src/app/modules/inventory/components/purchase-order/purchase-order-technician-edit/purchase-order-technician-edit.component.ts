import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { SalesModuleApiSuffixModels } from '@app/modules';
import { PurchaseOrderTechnicianUpdateModel } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import {
  CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { PurchaseOrderTechnicianDeletePopupComponent } from '@inventory/components/purchase-order';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable } from 'rxjs';
import { debounceTime, finalize, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-purchase-order-technician-edit',
  templateUrl: './purchase-order-technician-edit.component.html',
  styleUrls: ['./purchase-order-technician-edit.component.scss']
})
export class PurchaseOrderTechnicianEditComponent implements OnInit {

  technicianJobId: string;
  purchaseOrderTechnicianForm: FormGroup;
  purchaseOrderDetails: any = null;
  userData: UserLogin;
  selectedFiles = new Array();
  totalFileSize = 0;
  technicianList = [];
  collectionList = [];
  stockItemsDetails = [];
  stockCollectionItemsDetails = [];
  warehouseList = [];
  technicianCollectionJobDetailsModel: any = {};
  collectionAddress: any = [];
  isLoading: boolean = true;
  locationPoint: any = '';
  isFormSubmitted = false;
  deliveryMethodStatusName: string;
  technicianName: string;
  searchAddress: string;
  minLengthValidation: boolean;
  isFirstTimeAddress: Boolean = true;
  startTodayDate = new Date();
  noMatchingAdress: boolean= false;
  purchaseOrderId;
  constructor(
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private router: Router,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>, private datePipe: DatePipe,
    private dialog: MatDialog, private momentService: MomentService) {
    this.technicianJobId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

  }

  ngOnInit(): void {
    this.purchaseOrderTCJUpdateForm();
    this.loadActionTypes();
    this.technicianStockCollectionFormGroup.get('technicianWarehouseId').valueChanges.subscribe(technicianWarehouseId => {
      if (technicianWarehouseId) {
        this.getTechnicianDropdown(technicianWarehouseId);
      } else {
        this.technicianList = [];
      }
    });
  }

  getTechnicianDropdown(technicianWarehouseId) {
    let params=null;
    if(technicianWarehouseId)
     params =  { WarehouseId: technicianWarehouseId ,userId:this.userData.userId};
    else {
      params =  { userId:this.userData.userId};
    }

    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_TECHNICIAN_SEARCH, prepareGetRequestHttpParams(null, null, params)).subscribe((response) => {
      if (response.statusCode == 200) {
        this.technicianList = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      } else {
        this.technicianList = [];
      }
    });
  }
  loadActionTypes() {
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.userData?.userId })),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.TECHNICIAN_JOB_DETAILS, this.technicianJobId)
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          this.rxjsService.setGlobalLoaderProperty(false);

          switch (ix) {
            case 0:
              this.warehouseList = resp.resources;
              break;
            case 1:
              this.purchaseOrderId  = resp.resources.purchaseOrderId;
              this.technicianCollectionJobDetailsModel = resp.resources;      
              this.technicianCollectionJobDetailsModel['createdUserId'] = this.userData.userId;
    
              let technicianCollJobModel = new PurchaseOrderTechnicianUpdateModel(this.technicianCollectionJobDetailsModel);
              technicianCollJobModel['technicianStockCollectionDetail']['createdDate'] = this.datePipe.transform(technicianCollJobModel['technicianStockCollectionDetail']['createdDate'], 'dd-MM-yyyy hh:mm a');
              //technicianCollJobModel['technicianStockCollectionDetail']['collectionDate'] = this.datePipe.transform(technicianCollJobModel['technicianStockCollectionDetail']['collectionDate'], 'dd-MM-yyyy hh:mm a');
              technicianCollJobModel['technicianStockCollectionDetail']['collectionDate'] = new Date(technicianCollJobModel['technicianStockCollectionDetail']['collectionDate']);
              // this.purchaseOrderTechnicianForm.get('collectionDate').setValue(new Date(this.technicianCollectionJobDetailsModel.collectionDate));
              //code to insert data in nested formgroup
              this.technicianStockCollectionFormGroup.patchValue(technicianCollJobModel['technicianStockCollectionDetail']);
              this.isFirstTimeAddress = true;
              let stockCodesDetailsArray = this.stockCollectionFormArray;
              technicianCollJobModel['technicianStockCollectionItemList'].forEach((element) => {
                let stockCodesDetailsFormGroup = this.formBuilder.group({
                  'technicianStockCollectionId': element['technicianStockCollectionId'],
                  'technicianStockCollectionItemId': element['technicianStockCollectionItemId'],
                  'itemId': element['itemId'],
                  'qtyToCollect': [{ value: element['qtyToCollect'], disabled: true }],
                  // 'isSerialized': element['isSerialized'],
                  'createdUserId': element['createdUserId'],
                  'itemCode': [{ value: element['itemCode'], disabled: true }],
                  'itemName': [{ value: element['itemName'], disabled: true }],
                  'poQty': [{ value: element['poQty'], disabled: true }],
                  'receivedQty': [{ value: element['receivedQty'], disabled: true }]
                });
                stockCodesDetailsArray.push(stockCodesDetailsFormGroup);
              });
              // value changes for Location address
              this.purchaseOrderTechnicianForm.get('technicianStockCollectionDetail.description').valueChanges.pipe(
                debounceTime(500),
                tap(() => (this.isLoading = true)),
                switchMap((value) => {
                  // this.locationPoint = '';
                  this.searchAddress = value;
                  if (!value) {
                    this.isLoading = false;
                    this.minLengthValidation = false;
                    this.noMatchingAdress= false;
                    this.technicianStockCollectionFormGroup.get('locationPoint').setValue(null);
                    return this.collectionAddress = [];
                  } else if (typeof value === 'object') {
                    this.isLoading = false;
                    this.minLengthValidation = false;
                    this.noMatchingAdress= false;
                    this.technicianStockCollectionFormGroup.get('locationPoint').setValue(null);
                    return this.collectionAddress = [];
                  } else {
                    // this.searchAddress = value;
                    if (this.isFirstTimeAddress == false) {
                      this.locationPoint = null;
                    }
                    return this.crudService
                      .get(
                        ModulesBasedApiSuffix.INVENTORY,
                        InventoryModuleApiSuffixModels.UX_SUPPLIERADDRESS_SEARCH,
                        null,
                        undefined,
                        prepareGetRequestHttpParams(null, null, {
                          Search: value,
                          SupplierId: this.technicianCollectionJobDetailsModel['purchaseOrderDetailsView']['supplierId']
                        })
                      )
                      .pipe(finalize(() => (this.isLoading = false)));
                  }
                })
              )
                .subscribe((results: any) => {
                  if (results.resources != undefined) {
                    this.isFirstTimeAddress = false;
                    if (results.resources.length > 0) {
                      this.collectionAddress = results.resources;
                      if(this.collectionAddress[0].description.includes('No matching address found')){
                        this.noMatchingAdress=true;
                        this.collectionAddress =[];
                      }else{
                        this.noMatchingAdress=false;
                      }
                      this.rxjsService.setGlobalLoaderProperty(false);
                    } else {
                      let filterSearch$ = this.filterServiceTypeDetailsBySearchOptions(this.searchAddress);
                      if (filterSearch$) {
                        filterSearch$.subscribe(results => {
                          if (results.isSuccess && results.resources.length > 0) {
                            this.collectionAddress = results.resources;
                            if(this.collectionAddress[0].description.includes('No matching address found')){
                              this.noMatchingAdress=true;
                              this.collectionAddress =[];
                            }else{
                              this.noMatchingAdress=false;
                            }
                          } else {
                            this.collectionAddress = [];
                            this.noMatchingAdress=false;
                          }
                        });
                      } else {
                        this.noMatchingAdress=false;
                        this.collectionAddress = [];
                      }
                      this.rxjsService.setGlobalLoaderProperty(false);
                    }
                  }
                  else {
                    this.isLoading = false;
                    this.noMatchingAdress=false;
                    return this.collectionAddress = [];
                  }
                  this.rxjsService.setGlobalLoaderProperty(false);
                });
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  filterServiceTypeDetailsBySearchOptions(searchValue?: string): Observable<IApplicationResponse> { //
    this.minLengthValidation = false;
    if (searchValue.length < 3) {
      this.minLengthValidation = true;
      return;
    } else {

      return this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.SALES_API_GET_ADDRESS_FROM_AFRIGIS, null, true, prepareGetRequestHttpParams(null, null, {
        searchtext: searchValue,
        IsAfrigisSearch: true
      }), 1);
    }
  }

  purchaseOrderTCJUpdateForm(reqCreationListModel?: PurchaseOrderTechnicianUpdateModel) {
    let technicianCollJobModel = new PurchaseOrderTechnicianUpdateModel(reqCreationListModel);
    this.purchaseOrderTechnicianForm = this.formBuilder.group({});
    Object.keys(technicianCollJobModel).forEach((key) => {
      if (typeof technicianCollJobModel[key] !== 'object') {
        this.purchaseOrderTechnicianForm.addControl(key, new FormControl(technicianCollJobModel[key]));
      } else {
        if (key == 'technicianStockCollectionDetail') {
          let tcjFormGroup = this.formBuilder.group(
            {
              'tcjNumber': [{ value: '', disabled: true }],
              'createdDate': [{ value: '', disabled: true }],
              'poNumber': [{ value: '', disabled: true }],
              'collectionWarehouse': [{ value: '', disabled: true }],
              'supplier': [{ value: '', disabled: true }],
              'locationPoint': [{ value: '', disabled: true }],
              'tcjStatus': [''],
              'technicianStockCollectionId': [''],
              'purchaseOrderId': [''],
              'collectionDate': [''],
              'technicianId': [''],
              'technicianWarehouseId': [''],
              'createdUserId': [''],
              'isDraft': [''],
              'latitude': [''],
              'longitude': [''],
              'description': ['']
            });
          tcjFormGroup = setRequiredValidator(tcjFormGroup, ['collectionDate', 'technicianWarehouseId', 'technicianId', 'description']);
          this.purchaseOrderTechnicianForm.addControl(key, tcjFormGroup); //technicianCollJobModel[key]
        } else if (key == 'technicianStockCollectionItemList') {
          let stockCodesDetailsArray = this.formBuilder.array([]);
          this.purchaseOrderTechnicianForm.addControl(key, stockCodesDetailsArray);
        }
      }
    });
  }

  get technicianStockCollectionFormGroup(): FormGroup {
    if (this.purchaseOrderTechnicianForm !== undefined) {
      return (<FormGroup>this.purchaseOrderTechnicianForm.get('technicianStockCollectionDetail'));
    }
  }

  get stockCollectionFormArray(): FormArray {
    if (this.purchaseOrderTechnicianForm !== undefined) {
      return (<FormArray>this.purchaseOrderTechnicianForm.get('technicianStockCollectionItemList'));
    }
  }





  onCollectionAddressSelected(selected, address) {
    if (address.seoid == undefined) {
      this.locationPoint = address.latitude + " " + address.longitude;
      this.technicianStockCollectionFormGroup.get('latitude').setValue(address.latitude);
      this.technicianStockCollectionFormGroup.get('longitude').setValue(address.longitude);
      this.technicianStockCollectionFormGroup.get('locationPoint').setValue(this.locationPoint);
    }
    else {
      this.crudService.get(ModulesBasedApiSuffix.SALES, SalesModuleApiSuffixModels.POST_SELECTED_ADDRESS, null, true, prepareGetRequestHttpParams(null, null, {
        seoid: address.seoid
      }), 1).subscribe((res) => {
        if (res.resources) {
          this.technicianStockCollectionFormGroup.get('latitude').setValue(res.resources.addressDetails[0].geometry.location.lat)
          this.technicianStockCollectionFormGroup.get('longitude').setValue(res.resources.addressDetails[0].geometry.location.lng)
          this.locationPoint = this.technicianStockCollectionFormGroup.get('latitude').value + " " + this.technicianStockCollectionFormGroup.get('longitude').value;
          this.technicianStockCollectionFormGroup.get('locationPoint').setValue(this.locationPoint);
        }
      })
    }
  }



  onFileChange(event) {
    const supportedExtensions = ['jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'];
    for (var i = 0; i < event.target.files.length; i++) {
      let selectedFile = event.target.files[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension)) {
        this.selectedFiles.push(event.target.files[i]);
        this.totalFileSize += event.target.files[i].size;
      }
      else {

      }
    }
  }

  removeDocument(type: string, index: number, docId?) {
    if (type == 'new') {
      this.selectedFiles.splice(index, 1);
    } else {
      // call delete api for document
      this.crudService.delete(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.PURCHASEORDERS_FILEDELETE, null, prepareGetRequestHttpParams(null, null, {
          ids: docId,
          isDeleted: false
        })).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.technicianCollectionJobDetailsModel['technicianStockCollectionDocumentList'].splice(index, 1);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
  }


  cancelJob() {
    const dialogReff = this.dialog.open(PurchaseOrderTechnicianDeletePopupComponent, {
      width: '450px',
      data: {
        message: `Are you sure you want to cancel the<br />Technician Collection Job: <span class="font-weight-bold">${this.technicianStockCollectionFormGroup.get('tcjNumber').value} ?</span>`,
        buttons: {
          cancel: 'Cancel',
          create: 'Yes'
        }
      }, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (!result) return;
      let data = Object.assign({},
        this.technicianStockCollectionFormGroup.get('technicianStockCollectionId').value == '' ? null : { technicianStockCollectionId: this.technicianStockCollectionFormGroup.get('technicianStockCollectionId').value },
        this.userData.userId == '' ? null : { modifiedUserId: this.userData.userId },
        result == '' ? null : { reason: result }
      );
      this.crudService.update(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_COLLECTION_CANCEL,
        data
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.navigateToTCJViews();
        }
      })
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  updateTCJ() {
    if (this.purchaseOrderTechnicianForm.invalid || this.technicianStockCollectionFormGroup.get('locationPoint').value == null) {
      this.technicianStockCollectionFormGroup.get('description').markAsTouched();
      return;
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    const formData = new FormData();
    if (this.selectedFiles.length > 0) {
      for (const file of this.selectedFiles) {
        formData.append('document_files[]', file);
      }
    }
    // else {
    //   this.snackbarService.openSnackbar("Supporting document is required", ResponseMessageTypes.ERROR);
    //   return;
    // }

    //this.technicianStockCollectionFormGroup.get('collectionDate').setValue(this.momentService.toMoment(this.technicianStockCollectionFormGroup.get('collectionDate').value).format('YYYY-MM-DDThh:mm:ss[Z]'));
    // this.technicianStockCollectionFormGroup.get('collectionDate').setValue(new Date(this.technicianStockCollectionFormGroup.get('collectionDate').value));
  
    let newValue ;
    newValue =this.purchaseOrderTechnicianForm.getRawValue();
    newValue.technicianStockCollectionDetail.collectionDate = ( newValue.technicianStockCollectionDetail.collectionDate &&  newValue.technicianStockCollectionDetail.collectionDate != null) ? this.momentService.toMoment( newValue.technicianStockCollectionDetail.collectionDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
 
    formData.append("purchaseOrderSaveAsDraft", JSON.stringify(newValue));
    this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_COLLECTION_CREATE,// this.requisitionCreationForm.getRawValue()
      formData

    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToTCJView();
      }
    })
  }

  navigateToTCJView() {
    this.router.navigate(['inventory/purchase-order/technician-view'], { queryParams: { id: this.technicianCollectionJobDetailsModel['technicianStockCollectionId'] } });
  }
  navigateToTCJViews(){
    if(this.purchaseOrderId) {
      this.router.navigate(["inventory/purchase-order/view"], {
        queryParams:
        {
          id: this.purchaseOrderId,
        }
      });
    } 
  }

}
