import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { Observable } from 'rxjs';
import { StockCodeInfoPopupComponent } from '../stock-code-info/stock-code-info-popup.component';


@Component({
  selector: 'app-purchase-order-technician-view',
  templateUrl: './purchase-order-technician-view.component.html',
  styleUrls: ['./purchase-order-technician-view.component.scss']
})
export class PurchaseOrderTechinicianViewComponent implements OnInit {
  technicianJobId: string;
  technicianCollectionJobDetailsModel: any;

  constructor(
    private httpService: CrudService, private dialog: MatDialog,
    private router: Router, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    
  ) {
    this.technicianJobId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit() {

    if (this.technicianJobId) {
      this.getTechnicianJobDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.technicianCollectionJobDetailsModel = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getTechnicianJobDetails(): Observable<IApplicationResponse> {
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.TECHNICIAN_JOB_DETAILS, this.technicianJobId);
  }

  stockDetailListDialog: boolean = false;
  stockDetails: any = {};

  stockInfoModal(StockId) {

    this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_COLLECTION,
      undefined,
      false,
      prepareGetRequestHttpParams('', '', {
        TechnicianStockCollectionId: this.technicianCollectionJobDetailsModel['technicianStockCollectionId'],
        StockId: StockId //'fa673724-2f46-411c-a62c-c69acb9bc174'
      })
    ).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200) {
        this.stockDetails = response.resources;
        this.stockDetailListDialog = true;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

    // const dialogReff = this.dialog.open(StockCodeInfoPopupComponent, {
    //   width: '700px',
    //   data: {
    //     TechnicianStockCollectionId: this.technicianCollectionJobDetailsModel['technicianStockCollectionId'],
    //     StockId: StockId //'fa673724-2f46-411c-a62c-c69acb9bc174'
    //   },
    //   disableClose: true
    // });
    // dialogReff.afterClosed().subscribe(result => {
    //   if (result) return;
    //   this.rxjsService.setDialogOpenProperty(false);
    // });

  }

  navigateToEdit(): void {
    this.router.navigate(['inventory/purchase-order/technician-edit'], { queryParams: { id: this.technicianJobId } })
  }
  checkStatus(status: string) {
    return status != 'Fully Collected' && status != 'Partially Collected' &&  status != 'Cancelled' &&  status != 'Rejected';
  }

}
