import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { CrudService, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';

@Component({
  selector: 'app-stock-code-info-popup',
  templateUrl: './stock-code-info-popup.component.html',
  styleUrls: ['./stock-code-info-popup.component.scss']
})
export class StockCodeInfoPopupComponent implements OnInit {

  stockCodeInfoModel: any;

  constructor(@Inject(MAT_DIALOG_DATA) public stockCodeInfo,
    private rxjsService: RxjsService,
    private crudService: CrudService) { }

  ngOnInit(): void {

    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_COLLECTION,
      undefined,
      false,
      prepareGetRequestHttpParams('', '', {
        ...this.stockCodeInfo
      })
    ).subscribe(response => {
      if (response.isSuccess && response.statusCode === 200) {
        this.stockCodeInfoModel = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }


}
