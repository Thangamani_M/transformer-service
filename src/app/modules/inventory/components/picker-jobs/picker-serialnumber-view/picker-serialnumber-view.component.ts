import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-picker-serialnumber-view',
  templateUrl: './picker-serialnumber-view.component.html',
  styleUrls: ['./picker-serialnumber-view.component.scss']
})
export class PickerSerialnumberViewComponent implements OnInit {
  barcode: any;
  barcodes: any;
  count: any;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.barcode = this.data.barcode;
    if (this.barcode != null) {
      this.count = this.data.count;
      this.barcodes = this.barcode.split(',');
    }

  }

}
