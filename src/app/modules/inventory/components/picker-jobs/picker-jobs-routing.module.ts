import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PickerJobAddEditComponent, PickerJobListComponent, PickerJobScanComponent, PickerJobViewComponent } from '@inventory/components/picker-jobs';

const routes: Routes = [
  { path: '', component: PickerJobListComponent, data: { title: 'Picker Job List' } },
  { path: 'add', component: PickerJobAddEditComponent, data: { title: 'Add Picker Jobs' } },
  { path: 'scan', component: PickerJobScanComponent, data: { title: 'Picker Job Scan' } },
  { path: 'view', component: PickerJobViewComponent, data: { title: 'Picker Job View' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class PickerJobsRoutingModule { }