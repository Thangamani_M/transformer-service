import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { KeyValuePair, OrderReceipt, pickerJobBarcodeDetails, pickerJobItemsDTO, PickerJobs } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { PickerSerialnumberViewComponent } from '..';

@Component({
  selector: 'app-picker-job-add-edit',
  templateUrl: './picker-job-add-edit.component.html',
  styleUrls: ['./picker-job-add-edit.component.scss']
})
export class PickerJobAddEditComponent implements OnInit {
  myControl = new FormControl();
  applicationResponse: IApplicationResponse;
  uxOderTypes: any;
  uxOrderStatus: any;
  isLoading = false;
  filteredPurchaseOrders: any[] = [];
  orderReceipt = new OrderReceipt();
  pickerJobForm: FormGroup;
  Items: any[] = [];
  pickerJobs: PickerJobs;
  pickerJobsCopy: PickerJobs;
  pickerJobItemsDTO: pickerJobItemsDTO;
  pickerJobBarcodeDetails: pickerJobBarcodeDetails;
  barcode: any;
  barCodeDetails: any;
  barCodeList: Array<any> = [];
  orderReceiptId: any;
  pickerJobId: string;
  itemExistFlag = false;
  returnDataFromScanPage: PickerJobs;
  updatedOrderItemDetails: any[];
  lastUpdatedOrderReceiptItemId: any;
  ItemCopy: any;
  errorMessage: any;
  IsSubmitDisabled = true;
  loggedUser: UserLogin;
  isVarianceFlag = false;
  orderReceiptBatchId: any;
  showFilterOption: boolean = false;

  constructor(private dialog: MatDialog, private httpService: CrudService,
    private rxjsService: RxjsService,
    private router: Router,
    private formBuilder: FormBuilder,
    private snackBar: SnackbarService,
    private route: ActivatedRoute, private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.orderReceiptBatchId = this.route.snapshot.queryParams.orderReceiptBatchId;
    this.orderReceiptId = this.route.snapshot.queryParams.orderReceiptId;
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.returnDataFromScanPage = this.router.getCurrentNavigation().extras.state.data.OrderInfo;
      this.updatedOrderItemDetails = this.router.getCurrentNavigation().extras.state.data.Details;
      this.lastUpdatedOrderReceiptItemId = this.router.getCurrentNavigation().extras.state.data.OrderReceiptItemId;
    }
    else {
      if (this.route.snapshot.queryParams != null) {
        this.orderReceiptId = this.route.snapshot.queryParams.orderReceiptId;
        this.orderReceiptBatchId = this.route.snapshot.queryParams.orderReceiptBatchId;

      }
      else {
        this.router.navigate(['/inventory/picker-jobs/add']);
      }

    }

  }

  ngOnInit(): void {
    this.getUXOrderType();
    this.getUXJobOrderStatus();
    this.pickerJobForm = this.formBuilder.group({
      orderNumber: [''],
      jobOrderTypeName: [''],
      storageLocation: [''],
      batchNumber: [''],
      barCode: ['']
    });
    //after back from scan page
    if (this.returnDataFromScanPage != undefined) {

      this.UpdateItemDetailsFromScanPage();
    }
    else {
      this.pickerJobs = new PickerJobs();
      this.pickerJobItemsDTO = new pickerJobItemsDTO();
      this.pickerJobBarcodeDetails = new pickerJobBarcodeDetails();
    }
    if (this.orderReceiptId) {
      this.myControl.setValue({ displayName: this.orderReceiptBatchId == null ? '' : this.orderReceiptBatchId });
      this.getPickerJobDetails(this.orderReceiptId, this.orderReceiptBatchId);
    }
    else {
      this.pickerJobId = "New";
    }
    this.EnableDisableSubmit();



  }

  getUXOrderType(): void {
    this.httpService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_TYPES)
      .subscribe(resp => {
        this.applicationResponse = resp;
        this.uxOderTypes = this.applicationResponse.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getUXJobOrderStatus(): void {
    this.httpService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_JOB_ORDER_STATUS)
      .subscribe(resp => {
        this.applicationResponse = resp;
        this.uxOrderStatus = this.applicationResponse.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  getUXOrderReceiptSearch(value: string): Observable<any> {
    return this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_PICKER_JOBS, null, true, prepareGetRequestHttpParams(null, null, { displayName: value }))
      .pipe(
        finalize(() => this.isLoading = false),
      );
  }

  //Search picker job Id
  getPickerJobDetails(orderReceiptId, BatchId) {
    if (orderReceiptId && orderReceiptId != null) {
      let orderNumber = (typeof orderReceiptId === 'object') ? orderReceiptId.id : orderReceiptId;

      let params = new HttpParams().set('OrderReceiptId', this.orderReceiptId)
        .set('UserId', this.loggedUser.userId).set('OrderReceiptBatchId', this.orderReceiptBatchId);
      return this.httpService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.PICKER_JOBS_DETAILS, null, null, params).subscribe({
          next: response => {
            if (response.statusCode == 200 && response.resources != null) {
              this.showFilterOption = true;
              this.pickerJobForm.patchValue(response.resources);
              this.pickerJobs = response.resources;
              this.Items = this.pickerJobs.pickerJobItemsDTO;
              this.ItemCopy = this.pickerJobs.pickerJobItemsDTO;
              this.rxjsService.setGlobalLoaderProperty(false);
            }
            else {
              this.pickerJobs = new PickerJobs();
              this.Items = new Array();
            }

          }
        });
      this.rxjsService.setGlobalLoaderProperty(false);
    }

  }
  //verify and Scanning in Items
  getpickerBarcode() {
    this.barcode = this.pickerJobForm.get('barCode').value;
    if (this.barcode === '' || this.barcode == null)
      return;

    this.itemExistFlag = false;
    let itemFoundFlag = false;
    this.Items.forEach((item: pickerJobItemsDTO) => {
      item.pickerJobBarcodeDetails.forEach((itemDetail: pickerJobBarcodeDetails) => {
        if (itemDetail.barcode == this.barcode || itemDetail.serialNumber == this.barcode) {
          if (item.pickedBarcode != undefined && item.pickedBarcode.split(',').indexOf(itemDetail.barcode) > -1) {
            this.snackBar.openSnackbar(this.applicationResponse.message = "Barcode already verified!", ResponseMessageTypes.WARNING);
            this.itemExistFlag = true;
            return;
          }
        }
      });
    });

    if (this.itemExistFlag) {
      return;
    }

    this.Items.forEach((item: pickerJobItemsDTO) => {
      item.pickerJobBarcodeDetails.forEach((itemDetail: pickerJobBarcodeDetails) => {

        if (itemDetail.barcode == this.barcode || itemDetail.serialNumber == this.barcode) {
          item.pickingQty = item.pickingQty == undefined ? 1 : item.pickingQty += 1;
          if (item.pickedBarcode == undefined)
            item.pickedBarcode = itemDetail.barcode;
          else
            item.pickedBarcode += ',' + itemDetail.barcode;

          //remove the comma at beginning
          item.pickedBarcode = item.pickedBarcode.replace(/^,|,$/g, '');

          itemDetail.pickedBarcode = itemDetail.barcode;
          itemFoundFlag = true;
        }
      });
    });
    this.pickerJobForm.get('barCode').setValue('');
    if (!itemFoundFlag)
      this.snackBar.openSnackbar(this.applicationResponse.message = "Barcode not maching with current picker job!", ResponseMessageTypes.WARNING);
    this.EnableDisableSubmit();
  }
  //Selected value to assign auto fill textbox
  displayPurchaseOrderNo(keyValuePair: KeyValuePair) {
    if (keyValuePair) { return keyValuePair.displayName; }
  }
  UpdateItemDetailsFromScanPage() {

    this.pickerJobs = this.returnDataFromScanPage;
    this.Items = this.pickerJobs.pickerJobItemsDTO;
    if (this.pickerJobs.orderNumber != null)
      this.showFilterOption = true;

    //this.myControl.setValue({ displayName: this.pickerJobs.orderNumber == null ? '' : this.pickerJobs.orderNumber });
    this.pickerJobForm.setValue({
      storageLocation: this.pickerJobs.storageLocation, jobOrderTypeId: this.pickerJobs.jobOrderTypeId,
      batchNumber: this.pickerJobs.batchNumber, barCode: '', //jobOrderStatusId: this.pickerJobs.jobOrderStatusId,
      orderNumber: this.pickerJobs.orderNumber
    });    //this.getPickerJobDetails(this.pickerJobs.orderReceiptId);

    if (this.updatedOrderItemDetails == undefined || this.updatedOrderItemDetails.length < 1)
      return;

    // apply the changes from scan page
    let orderReceiptItemId = this.updatedOrderItemDetails[0].orderReceiptItemId;
    this.Items.forEach(element => {
      if (element.orderReceiptItemId == orderReceiptItemId) {
        element.pickerJobBarcodeDetails = this.updatedOrderItemDetails;
        element.pickingQty = this.updatedOrderItemDetails.filter(function (x) {
          return x.pickedBarcode != undefined
        }).length;

        //update the barcode in list of items
        element.pickedBarcode = element.pickerJobBarcodeDetails.map(function (detail) {
          if (detail.pickedBarcode != undefined)
            return detail.pickedBarcode;
        }).join(',');

        element.pickedBarcode = element.pickedBarcode.replace(/^,|,|,,$/g, '');
      }

      //when all items removed
      if (element.returnQuantity == 0)
        element.pickedBarcode = '';
    })

    this.EnableDisableSubmit();
    //this.getPickerJobDetails(this.pickerJobs.orderReceiptId);
  }
  getserialnumber(event, data) {
    const dialogReff = this.dialog.open(PickerSerialnumberViewComponent, { width: '700px', disableClose: true, data: { barcode: event, count: data.length } });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.rxjsService.setDialogOpenProperty(false);
      //this.ticketObservableResponse = this.getTicketList();
    });
  }
  OpenBarcodesList(item: any) {
    this.router.navigate(['/inventory/picker-jobs/scan'], { state: { data: { pickJobs: { ...this.pickerJobs, ...this.pickerJobForm.value }, Detail: item } } });
  }
  savePickerJob() {
    this.IsSubmitDisabled = true;
    this.pickerJobs.pickerId = this.loggedUser.userId;
    this.pickerJobs.warehouseId = this.loggedUser.warehouseId;
    this.pickerJobs.orderReceiptBatchId = this.orderReceiptBatchId;
    this.pickerJobs.jobDate = new Date();
    this.pickerJobs.acceptedDate = new Date();
    this.pickerJobForm.get('barCode').clearValidators();
    if (this.pickerJobForm.invalid) {
      this.pickerJobForm.markAllAsTouched();
      return;
    }
    this.pickerJobsCopy = JSON.parse(JSON.stringify(this.pickerJobs));
    this.pickerJobsCopy.pickerJobItemsDTO.forEach(item => {
      if (item.availableQty != item.pickingQty) {
        this.isVarianceFlag = true;
      }
      item.pickerJobBarcodeDetails = item.pickerJobBarcodeDetails.filter(function (detail) {
        return detail.pickedBarcode != undefined;
      });
    });
    this.pickerJobsCopy.pickerJobItemsDTO = this.pickerJobsCopy.pickerJobItemsDTO.filter(function (item) {
      return item.pickerJobBarcodeDetails.length != 0;
    });

    //Temporary fix. Need to remove this code in future development
    if (this.isVarianceFlag == true)
      this.pickerJobsCopy.jobOrderStatusId = "A5A5EFBE-459E-4EFC-B37A-64E42F5DEF1E";
    else
      this.pickerJobsCopy.jobOrderStatusId = "2A119790-070F-4A2D-AAB7-8B428992A6C7";

    const plantdata = { ...this.pickerJobsCopy, ...this.pickerJobForm.value };

    this.httpService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PICKER_JOBS, plantdata).subscribe({
      next: response => this.AfterSubmit(response),
      error: err => this.errorMessage = err
    });
  }

  EnableDisableSubmit() {
    if (this.pickerJobs != undefined && this.pickerJobs.pickerJobItemsDTO != undefined) {
      this.pickerJobs.pickerJobItemsDTO.forEach(item =>
        item.pickerJobBarcodeDetails.forEach(element => {
          if (element.pickedBarcode != undefined)
            this.IsSubmitDisabled = false;
        })
      )
    }
  }
  AfterSubmit(response: IApplicationResponse): void {
    if (response.statusCode == 200) {
      if (this.isVarianceFlag == false)
        this.router.navigate(['/inventory/picker-jobs']);
      else {
        // commented temporarly. needs to address this issue
        if (this.isVarianceFlag == true)
          this.router.navigate(['/inventory/variance/variance-esclation/add-edit'], { queryParams: { OrderReceiptId: this.pickerJobs.orderReceiptId, OrderReceiptBatchId: this.pickerJobs.orderReceiptBatchId } });
        else
          this.router.navigate(['/inventory/picker-jobs']);
      }


    }
    else {
      this.IsSubmitDisabled = false;
      this.snackBar.openSnackbar(this.applicationResponse.message = 'something went wrong', ResponseMessageTypes.WARNING);
    }
  }
  loadBarcodes() {
    this.itemExistFlag = false;
    let itemFoundFlag = false;
    this.Items.forEach((item: pickerJobItemsDTO) => {
      item.pickerJobBarcodeDetails.forEach((itemDetail: pickerJobBarcodeDetails) => {
        if (itemDetail.itemId == item.itemId) {
          item.pickingQty = item.pickingQty == undefined ? 1 : item.pickingQty += 1;
          if (item.pickedBarcode == undefined)
            item.pickedBarcode = itemDetail.barcode;
          else
            item.pickedBarcode += ',' + itemDetail.barcode;

          //remove the comma at beginning
          item.pickedBarcode = item.pickedBarcode.replace(/^,|,$/g, '');

          itemDetail.pickedBarcode = itemDetail.barcode;
          itemFoundFlag = true;
        }
      });
    });
    this.EnableDisableSubmit();
  }

}
