import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { PickerJobAddEditComponent, PickerJobListComponent, PickerJobScanComponent, PickerJobsRoutingModule, PickerJobViewComponent } from '@inventory/components/picker-jobs';
import { PickerSerialnumberViewComponent } from './picker-serialnumber-view/picker-serialnumber-view.component';



@NgModule({
    declarations: [PickerSerialnumberViewComponent, PickerJobListComponent, PickerJobAddEditComponent, PickerJobScanComponent, PickerJobViewComponent, PickerSerialnumberViewComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule, FormsModule,
        PickerJobsRoutingModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
    ],
    entryComponents: [
        PickerSerialnumberViewComponent],
})

export class PickerJobsModule { }