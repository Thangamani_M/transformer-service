import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { pickerJobBarcodeDetails, pickerJobItemsDTO } from '@modules/inventory/models';

@Component({
  selector: 'app-picker-job-scan',
  templateUrl: './picker-job-scan.component.html',
  styleUrls: ['./picker-job-scan.component.scss']
})
export class PickerJobScanComponent implements OnInit {
  pickerJobItemsDTO: pickerJobItemsDTO;
  pickerJobBarcodeDetail = new Array();
  pickerJobBarcodeDetailFiltered = new Array();
  itemCount: Number;
  pickerJobs: any[];
  pickerJobsCopy: any[];

  constructor(private router: Router) {
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.pickerJobItemsDTO = this.router.getCurrentNavigation().extras.state.data.Detail;
      this.pickerJobs = this.router.getCurrentNavigation().extras.state.data.pickJobs;
      this.pickerJobsCopy = JSON.parse(JSON.stringify(this.pickerJobs));
      this.pickerJobBarcodeDetail = this.pickerJobItemsDTO.pickerJobBarcodeDetails;
      this.pickerJobBarcodeDetailFiltered = this.pickerJobItemsDTO.pickerJobBarcodeDetails.filter(function (row) { return row.pickedBarcode != undefined; });
      this.itemCount = this.pickerJobBarcodeDetail.length;
    }
    else {
      this.router.navigate(['/inventory/picker-jobs/add']);
    }
  }

  ngOnInit(): void {
    this.pickerJobBarcodeDetailFiltered = this.pickerJobItemsDTO.pickerJobBarcodeDetails.filter(function (row) {
      return row.pickedBarcode != undefined;
    });
  }
  onAllItemsRemoved() {
    this.pickerJobBarcodeDetail.forEach((element: pickerJobBarcodeDetails, index) => {
      element.pickedBarcode = undefined;
    });

    this.pickerJobBarcodeDetailFiltered = new Array();
    this.filter();
  }
  filter() {
    this.pickerJobBarcodeDetailFiltered = this.pickerJobItemsDTO.pickerJobBarcodeDetails.filter(function (row) {
      return row.pickedBarcode != undefined;
    });
  }
  OnItemRemoved(item: pickerJobBarcodeDetails) {
    this.pickerJobBarcodeDetail.forEach((row, index) => {
      if (row.orderReceiptItemDetailId == item.orderReceiptItemDetailId) {
        row.pickedBarcode = undefined;
        this.pickerJobBarcodeDetailFiltered.splice(index, 1);
      }
    });
    this.filter();
  }

  BackToCreatePage() {
    this.router.navigate(['/inventory/picker-jobs/add'], { state: { data: { OrderInfo: this.pickerJobsCopy } } });
  }

  save() {
    this.router.navigate(['/inventory/picker-jobs/add'], { state: { data: { OrderInfo: this.pickerJobs, Details: this.pickerJobBarcodeDetail, OrderReceiptItemId: this.pickerJobItemsDTO.orderReceiptItemId } } });
  }

}
