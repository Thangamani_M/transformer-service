import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { PickerSerialnumberViewComponent } from '..';

@Component({
  selector: 'app-picker-job-view',
  templateUrl: './picker-job-view.component.html',
  styleUrls: ['./picker-job-view.component.scss']
})

export class PickerJobViewComponent implements OnInit {
  orderReceiptId: string;
  pickerJobDetails: any;
  ReceiptStatus: string = "";
  orderReceiptBatchId: string = "";
  IsLoaded: boolean = false;
  barcodes: any;
  barcode: any;
  userData: UserLogin;
  // IsPurchaseOrder: boolean = false;

  constructor(
    private dialog: MatDialog, private httpService: CrudService, 
    private router: Router, private store: Store<AppState>,
    private rxjsService: RxjsService) {
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      });
      if (this.router && this.router.getCurrentNavigation().extras.state) {
        this.orderReceiptId = this.router.getCurrentNavigation().extras.state.orderReceiptId;
        this.orderReceiptBatchId = this.router.getCurrentNavigation().extras.state.orderReceiptBatchId;
      }
  }

  ngOnInit(): void {

    let params = new HttpParams().set('OrderReceiptId', this.orderReceiptId)
    .set('OrderReceiptBatchId', this.orderReceiptBatchId).set('UserId', this.userData.userId);
    this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.PICKER_JOBS_DETAILS, null, null, params).subscribe({
        next: response => {
          if (response.statusCode == 200 && response.resources != null) {
            this.pickerJobDetails = response.resources;
            if (this.pickerJobDetails) {
              this.IsLoaded = true;
              this.pickerJobDetails.pickerJobItemsDTO.forEach(element => {
                if (element.availableQty > element.pickingQty) {
                  return;
                }
              });
            }
          }
          else {
          }

        }
      });
    this.rxjsService.setGlobalLoaderProperty(false);

  }
  openAddEditPickerJobs() {
    this.router.navigate(['/inventory/picker-jobs/add/'], { queryParams: { orderReceiptBatchId: this.orderReceiptBatchId, orderReceiptId: this.orderReceiptId } });

  }
  getserialnumber(event, data) {
    this.barcodes = event.pickerJobBarcodeDetails;
    this.barcode = undefined;
    if (this.barcodes.length != 0) {
      this.barcode = Array.prototype.map.call(this.barcodes, function (item) { return item.barcode; }).join(",");
    }
    const dialogReff = this.dialog.open(PickerSerialnumberViewComponent, { width: '700px', disableClose: true, data: { barcode: this.barcode, count: this.barcodes.length } });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

}
