import { HttpParams } from '@angular/common/http';
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels, JobSelectionTechStockLookupAddEditModel } from '@modules/inventory';
import { BarcodePrintComponent } from '@modules/inventory/components/order-receipts/barcode-print';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-job-selection-stock-lookup-list',
  templateUrl: './job-selection-stock-lookup-list.component.html'
})

export class JobSelectionStockLookupListComponent implements OnInit {

  jobSelectionStockLookUpForm: FormGroup;
  userData: UserLogin;
  showItemCodeError: boolean = false;
  itemCodeErrorMsg: string = '';
  warehouseDropdown: any = [];
  storageLocationList: any = [];
  binLocationList: any = [];
  selectedIndex: number = 0;
  panelOpenState:boolean;
  getallStockbinsDetails: any = [];
  stockCodeValidateDetails: any = {};
  binLocationHide: boolean = false;
  printStockCode: any = [];
  printClickFlag:boolean=false;
  commonCode:any = [];
  serialBarcodes: any = [];
  @ViewChild('divClick',null) divClick: ElementRef;
  stockBarCode;
  refernce:any
  constructor(private snackbarService: SnackbarService, private changeDetectorRef: ChangeDetectorRef,
    private store: Store<AppState>, private router: Router, private dialog: MatDialog,
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService,
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createpickingStockOrderScanForm();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createpickingStockOrderScanForm(): void {
    let stockOrderModel = new JobSelectionTechStockLookupAddEditModel();
    // create form controls dynamically from model class
    this.jobSelectionStockLookUpForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.jobSelectionStockLookUpForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
  }

  getStockLookupDetails(index:number){
    if(index === 0){ this.getStockCodeScanData(index) }
    else if(index === 1) { this.getStorageLocationDropdown(index) }
    else if(index === 2) { this.getBinLocationDropdown(index) }
    else { this.getAllStockbinDetails(index, true) }
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  getStockCodeScanData(index:number){

    this.showItemCodeError = false;
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.jobSelectionStockLookUpForm.get('itemcode').value == '' || this.jobSelectionStockLookUpForm.get('itemcode').value == null) {
      this.showItemCodeError = true;
      this.itemCodeErrorMsg = "Stock code is required";
      return;
    }

    let stockCodeParams = new HttpParams().set('stockCode',
    this.jobSelectionStockLookUpForm.get('itemcode').value).set('UserID', this.userData.userId)

    let crudService: Observable<IApplicationResponse> =
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_CODE_VALIDATE_WAREHOUSELIST, undefined, true, stockCodeParams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        if(response.resources.swList == null){
          this.snackbarService.openSnackbar(response.resources.message, ResponseMessageTypes.WARNING);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.stockBarCode = response.resources.stockBarCode;
          this.warehouseDropdown = [];
          this.stockCodeValidateDetails = response.resources.swList[0];
          this.jobSelectionStockLookUpForm.get('itemId').patchValue(response.resources.swList[0].stockId);
          this.jobSelectionStockLookUpForm.get('isNotSerialized').patchValue(response.resources.swList[0].isNotSerialized);
          for (let i = 0; i < response.resources.swList.length; i++) {
            let tmp = {};
            tmp['value'] = response.resources.swList[i].warehouseId;
            tmp['display'] = response.resources.swList[i].warehouseName;
            this.warehouseDropdown.push(tmp);
          }
          this.jobSelectionStockLookUpForm.get('warehouseId').patchValue(null);
          this.selectedIndex = index + 1;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getStorageLocationDropdown(index:number) {

    this.showItemCodeError = false;
    this.jobSelectionStockLookUpForm.get('warehouseId').setErrors(null);
    this.jobSelectionStockLookUpForm.get('warehouseId').clearValidators();
    this.jobSelectionStockLookUpForm.get('warehouseId').updateValueAndValidity();

    if(this.jobSelectionStockLookUpForm.get('warehouseId').value == null ||
      this.jobSelectionStockLookUpForm.get('warehouseId').value.length <= 0){
      this.jobSelectionStockLookUpForm.get('warehouseId').setValidators([Validators.required]);
      this.jobSelectionStockLookUpForm.get('warehouseId').updateValueAndValidity();
      return;
    }

    let params = new HttpParams().set('UserId', this.userData.userId)
    .set('warehouseid', this.jobSelectionStockLookUpForm.get('warehouseId').value)
    .set('ItemId', this.jobSelectionStockLookUpForm.get('itemId').value);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STOCK_LOOKUP_STORAGE_LOCATION,
      undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          let getdetails = response.resources;
          if(getdetails.length == 0){
            this.snackbarService.openSnackbar('No stock on hand for this stock code in the selected warehouse(s).', ResponseMessageTypes.ERROR);
          }
          this.storageLocationList = [];
          for (let i = 0; i < getdetails.length; i++) {
            let tmp = {};
            tmp['value'] = getdetails[i].id;
            tmp['display'] = getdetails[i].displayName;
            this.storageLocationList.push(tmp);
          }
          this.jobSelectionStockLookUpForm.get('storageLocationId').patchValue(null);
          this.selectedIndex = index + 1;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }
  printClick(type,refernce){
    if(type == 'first'){
      let arr=[];
      arr.push(this.stockBarCode);
      this.commonCode = arr;
      this.printClickFlag=true;
    }else{
      this.commonCode = this.serialBarcodes;
      this.printClickFlag=false;
    }

    setTimeout(() => {
      this.divClick.nativeElement.click();
     // document.getElementById('printSectionID').click();
      }, 500);
    //this.myFileInputField?.nativeElement?.click();
  }
  printClickCommon(){

  }
  getBinLocationDropdown(index:number) {

    this.jobSelectionStockLookUpForm.get('storageLocationId').setErrors(null);
    this.jobSelectionStockLookUpForm.get('storageLocationId').clearValidators();
    this.jobSelectionStockLookUpForm.get('storageLocationId').updateValueAndValidity();

    if(this.jobSelectionStockLookUpForm.get('storageLocationId').value == null ||
      this.jobSelectionStockLookUpForm.get('storageLocationId').value.length <= 0){
      this.jobSelectionStockLookUpForm.get('storageLocationId').setValidators([Validators.required]);
      this.jobSelectionStockLookUpForm.get('storageLocationId').updateValueAndValidity();
      return;
    }

    let params = new HttpParams().set('UserId', this.userData.userId)
    .set('warehouseId', this.jobSelectionStockLookUpForm.get('warehouseId').value)
    .set('StorageLocationId', this.jobSelectionStockLookUpForm.get('storageLocationId').value)
    .set('ItemId', this.jobSelectionStockLookUpForm.get('itemId').value);

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_BIN_LOCATION,
      undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          let bins = response.resources;
          this.binLocationList = [];
          this.jobSelectionStockLookUpForm.get('stockId').patchValue(null);
          this.rxjsService.setGlobalLoaderProperty(false);
          if(bins.length > 0) {
            for (let i = 0; i < bins.length; i++) {
              let tmp = {};
              tmp['value'] = bins[i].id;
              tmp['display'] = bins[i].displayName;
              this.binLocationList.push(tmp);
            }
            this.selectedIndex = index + 1;
          }
          else {
            this.getAllStockbinDetails(3, false);
            this.selectedIndex = index + 2;
          }
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  printedValue: any = [];
  // showSingleBarcodes: boolean = false;

  getAllStockbinDetails(index:number, required: boolean){

    this.jobSelectionStockLookUpForm.get('stockId').setErrors(null);
    this.jobSelectionStockLookUpForm.get('stockId').clearValidators();
    this.jobSelectionStockLookUpForm.get('stockId').updateValueAndValidity();

    if(required && (this.jobSelectionStockLookUpForm.get('stockId').value == null ||
      this.jobSelectionStockLookUpForm.get('stockId').value.length <= 0)){
      this.jobSelectionStockLookUpForm.get('stockId').setValidators([Validators.required]);
      this.jobSelectionStockLookUpForm.get('stockId').updateValueAndValidity();
      return;
    }

    // if(required && (this.jobSelectionStockLookUpForm.get('stockId').value != null &&
    //   this.jobSelectionStockLookUpForm.get('stockId').value.length > 65)){
    //   this.snackbarService.openSnackbar('Bin cannot be selected greater than 65', ResponseMessageTypes.WARNING);
    //   return;
    // }

    let params;
    if(required){
      // params = new HttpParams().set('UserId', '60cccd59-dca4-4c41-a1e2-a5d891f1bc21')
      // .set('ItemId', 'ed10d865-e5b8-437f-af33-000d84fb2cc4')
      // .set('warehouseId', '3b2d8d18-f2ea-4e16-ad65-8396afe692a5')
      // .set('StorageLocationId', '4c47877a-c8d9-4b82-8607-09f6d4113e75')
      // .set('LocationBinId', 'a191d390-83f5-4bac-a800-1c9fd8fd7237')
      // .set('IsNotSerialized', 'false');

      params = new HttpParams().set('UserId', this.userData.userId)
      .set('ItemId', this.jobSelectionStockLookUpForm.get('itemId').value)
      .set('warehouseId', this.jobSelectionStockLookUpForm.get('warehouseId').value)
      .set('StorageLocationId', this.jobSelectionStockLookUpForm.get('storageLocationId').value)
      .set('LocationBinId', this.jobSelectionStockLookUpForm.get('stockId').value)
      .set('IsNotSerialized', this.jobSelectionStockLookUpForm.get('isNotSerialized').value);

    }
    else {
      params = new HttpParams().set('UserId', this.userData.userId)
      .set('ItemId', this.jobSelectionStockLookUpForm.get('itemId').value)
      .set('warehouseId', this.jobSelectionStockLookUpForm.get('warehouseId').value)
      .set('StorageLocationId', this.jobSelectionStockLookUpForm.get('storageLocationId').value)
      .set('IsNotSerialized', this.jobSelectionStockLookUpForm.get('isNotSerialized').value);
    }

    this.rxjsService.setFormChangeDetectionProperty(true);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_LOOKUP, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.getallStockbinsDetails = response.resources;
          this.printStockCode.push(this.getallStockbinsDetails[0]?.storageLocationList[0]?.binList[0]?.serialNumber);
          this.commonCode =  this.printStockCode;
          this.printedValue = new Array(this.getallStockbinsDetails[0]?.storageLocationList[0]?.binList[0]?.serialNumber);
          // this.showSingleBarcodes = true;
          this.selectedIndex = index + 1;
          let binslistarr = [];
          let bincodearr = []
          this.binLocationHide = false;
          // this.getallStockbinsDetails = response.resources;
          response.resources.forEach(were => {
            were.storageLocationList.forEach(store => {
              store['allBinChecked'] = false;
              store['allSerialChecked'] = false;
              store.binList.forEach(bins => {
                bins['binChecked'] = false;
                bins['serialChecked'] = false;
                binslistarr.push(bins.serialNumber);
                if(bins.bincode == '--'){
                  bincodearr.push(bins.bincode);
                }
              });
            });
          });
          if(binslistarr.length == bincodearr.length){
            this.binLocationHide = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  checkAllCheckboxEvent(checked:any, findex:number, sindex:number, type:string){

    // if(!checked){
    //   this.serialBarcodes = [];
    //   this.printedValue = new Array(this.getallStockbinsDetails[0]?.storageLocationList[0]?.binList[0]?.serialNumber);
    // }

    this.getallStockbinsDetails.forEach((were, i) => {
      were.storageLocationList.forEach((store, j) => {
        store.binList.forEach((bins) => {
          // if(i===findex && j===sindex && checked) {
          //   if(type == 'bins'){
          //     bins['binChecked'] = true;
          //     store['allSerialChecked'] = false;
          //     bins['serialChecked'] = false
          //   }
          //   else{
          //     bins['binChecked'] = false;
          //     store['allBinChecked'] = false
          //     bins['serialChecked'] = true;
          //   }
          // }
          // else {
          //   type == 'bins' ? bins['binChecked'] = false : bins['serialChecked'] = false;
          // }
           if(i===findex && j===sindex && checked) {
              if(type == 'bins'){
                 bins['binChecked'] = true;
              //  store['allSerialChecked'] = false;
                // bins['serialChecked'] = false
             }
             else{
                // bins['binChecked'] = false;
               // store['allBinChecked'] = false
                 bins['serialChecked'] = true;
             }
           }
           else {
            type == 'bins' ? bins['binChecked'] = false : bins['serialChecked'] = false;
          }
        });
      });
    });
    this.stockLookupPrintAction('checked');
  }



  stockLookupPrintAction(type: string){

    let isSelected: boolean = false;
    let printedValue = [];
    this.getallStockbinsDetails.forEach(stocks => {
      stocks.storageLocationList.forEach(store => {
        store.binList.forEach(bins => {
          if(bins.serialChecked){
            isSelected = true;
            printedValue.push(bins.serialNumber);
          }
          if(bins.binChecked){
            isSelected = true;
            printedValue.push(bins.bincode);
          }
        });
      });
    });

    this.commonCode =  printedValue;
    if(!isSelected && type == 'unchecked'){
      this.snackbarService.openSnackbar('Select at least one bin location or serial number to print a barcode.', ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.serialBarcodes = printedValue;
      // this.print(printedValue);
    }
  }

  print(printedValue:any) {
    if (printedValue.length == 0) {
      this.snackbarService.openSnackbar("Select at least one Serial Number to print", ResponseMessageTypes.WARNING);
      return;
    }
    const dialogReff = this.dialog.open(BarcodePrintComponent,
    { width: '400px', height: '400px', disableClose: true,
      data: { serialNumbers: printedValue }
    });
  }

  // openBarcodeModal(barcode) {

  //   this.serialBarcodes = [];
  //   this.printedValue = new Array(barcode);

  //   if(barcode == null) return;
  //   const dialogReff = this.dialog.open(StocklookupBarcodeModalComponent, { width: '450px',
  //   data: {barcode: barcode},
  //   disableClose: true });
  //   dialogReff.afterClosed().subscribe(result => {
  //     if (result) return;
  //   });
  // }

  navigateToPreviousPage(index:number){
    this.selectedIndex = index - 1;
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'job-selection'], {
      skipLocationChange: true
    });
  }

}
