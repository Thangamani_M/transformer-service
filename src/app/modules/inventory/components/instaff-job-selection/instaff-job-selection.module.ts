import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import {
    InStaffJobSelectionRoutingModule, JobSelectionCycleCountsListComponent, JobSelectionCycleCountsModalComponent, JobSelectionCycleCountsScanComponent, JobSelectionListComponent, JobSelectionManagerModalComponent,
    JobSelectionPackingScanComponent, JobSelectionPickedBatchesComponent, JobSelectionReceivingBatchesComponent, JobSelectionScanComponent, JobSelectionStockTakeListComponent, JobSelectionStockTakeModalComponent, JobSelectionStockTakeScanComponent
} from '@inventory/components/instaff-job-selection';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { SignaturePadModule } from 'ngx-signaturepad';
import { BarcodePrinterModule } from '../barcode-printer/barcode-printer/barcode-printer.module';
import { JobSelectionAssignBinLocationComponent } from './job-selection-assign-bin-location/job-selection-assign-bin-location/job-selection-assign-bin-location.component';
import { JobSelectionAuditCycleCountBinListComponent } from './job-selection-audit-cycle-count/job-selection-audit-cycle-count-bin-list.component';
import { JobSelectionAuditCycleCountListComponent } from './job-selection-audit-cycle-count/job-selection-audit-cycle-count-list.component';
import { JobSelectionAuditCycleCountScanComponent } from './job-selection-audit-cycle-count/job-selection-audit-cycle-count-scan.component';
import { JobSelectionAuditCycleCountComponent } from './job-selection-audit-cycle-count/job-selection-audit-cycle-count.component';
import { JobSelectionCycleCountListComponent, JobSelectionCycleCountScanComponent } from './job-selection-cycle-count';
import { JobSelectionCycleCountBinListComponent } from './job-selection-cycle-count/job-selection-cycle-count-bin-list/job-selection-cycle-count.component';
import { JobSelectionCycleCoutsBinListComponent } from './job-selection-cycle-counts/job-selection-cycle-counts-bin-list/job-selection-cycle-counts-bin-list.component';

import { JobSelectionMoveStockCodeComponent } from './job-selection-move-stock-code/job-selection-move-stock-code/job-selection-move-stock-code.component';
import { JobSelectionNewCountComponent } from './job-selection-new-count/job-selection-new-count.component';
import { JobSelectionRadioSystemRemovalsScanComponent } from './job-selection-radio-system-removals/job-selection-radio-system-removals-scan/job-selection-radio-system-removals-scan.component';
import { JobSelectionReceivingScanComponent } from './job-selection-receiving/job-selection-receiving-scan/job-selection-receiving-scan.component';
import { JobSelectionStockLookupListComponent } from './job-selection-stock-lookup/job-selection-stock-lookup-list/job-selection-stock-lookup-list.component';
import { StocklookupBarcodeModalComponent } from './job-selection-stock-lookup/job-selection-stock-lookup-list/stocklookup-barcode-modal/stocklookup-barcode-modal.component';
import { JobSelectionStockTakeBinListComponent } from './job-selection-stock-take/job-selection-stock-take-bin-list.component/job-selection-stock-take-bin-list.component';
import { JobSelectionTechAuditCycleCountListComponent } from './job-selection-tech-audit-cycle-count/job-selection-tech-audit-cycle-count-list/job-selection-tech-audit-cycle-count-list.component';
import { JobSelectionTechAuditCycleCountScanComponent } from './job-selection-tech-audit-cycle-count/job-selection-tech-audit-cycle-count-scan/job-selection-tech-audit-cycle-count-scan.component';
import { JobSelectionTechAuditCycleCountViewComponent } from './job-selection-tech-audit-cycle-count/job-selection-tech-audit-cycle-count-view/job-selection-tech-audit-cycle-count-view.component';
import { JobSelectionTechnicianStockTakeListComponent } from './job-selection-technician-stock-take/job-selection-technician-stock-take-list/job-selection-technician-stock-take-list.component';
import { JobSelectionTechnicianStockTakeScanComponent } from './job-selection-technician-stock-take/job-selection-technician-stock-take-scan/job-selection-technician-stock-take-scan.component';
import { JoSelectionTransferRequestScanComponent } from './job-selection-transfer-request/jo-selection-transfer-request-scan/jo-selection-transfer-request-scan.component';
@NgModule({
    declarations: [
        JobSelectionListComponent, JobSelectionScanComponent, JobSelectionManagerModalComponent,
        JobSelectionPackingScanComponent, JobSelectionCycleCountsListComponent,
        JobSelectionCycleCountsScanComponent, JobSelectionStockTakeListComponent,
        JobSelectionStockTakeScanComponent, JobSelectionStockTakeModalComponent,
        JobSelectionPickedBatchesComponent, JobSelectionAuditCycleCountComponent,
        JobSelectionAuditCycleCountListComponent, JobSelectionAuditCycleCountScanComponent,
        JobSelectionNewCountComponent,
        JobSelectionTechnicianStockTakeListComponent,
        JobSelectionTechnicianStockTakeScanComponent,
        JobSelectionStockLookupListComponent,
        StocklookupBarcodeModalComponent,
        JobSelectionAssignBinLocationComponent,
        JobSelectionMoveStockCodeComponent,
        JobSelectionReceivingScanComponent,
        JobSelectionTechAuditCycleCountListComponent,
        JobSelectionTechAuditCycleCountScanComponent,
        JobSelectionTechAuditCycleCountViewComponent,
        JobSelectionRadioSystemRemovalsScanComponent,
        JoSelectionTransferRequestScanComponent, JobSelectionReceivingBatchesComponent, JobSelectionStockTakeBinListComponent,
        JobSelectionAuditCycleCountBinListComponent,JobSelectionCycleCoutsBinListComponent,JobSelectionCycleCountsModalComponent,
        JobSelectionCycleCountListComponent,JobSelectionCycleCountScanComponent,JobSelectionCycleCountBinListComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule, FormsModule,
        InStaffJobSelectionRoutingModule,
        LayoutModule,
        MaterialModule,
        SignaturePadModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
        BarcodePrinterModule
    ],
    entryComponents: [
        JobSelectionManagerModalComponent,JobSelectionCycleCountsModalComponent, JobSelectionStockTakeModalComponent, StocklookupBarcodeModalComponent
    ],
})

export class InStaffJobSelectionModule { }
