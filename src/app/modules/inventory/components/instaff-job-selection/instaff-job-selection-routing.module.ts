import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    JobSelectionListComponent, JobSelectionPackingScanComponent, JobSelectionPickedBatchesComponent, JobSelectionScanComponent, JobSelectionStockTakeListComponent,
    JobSelectionStockTakeScanComponent, JobSelectionReceivingBatchesComponent,
} from '@inventory/components/instaff-job-selection';
import { JobSelectionAssignBinLocationComponent } from './job-selection-assign-bin-location/job-selection-assign-bin-location/job-selection-assign-bin-location.component';
import { JobSelectionAuditCycleCountListComponent } from './job-selection-audit-cycle-count/job-selection-audit-cycle-count-list.component';
import { JobSelectionAuditCycleCountScanComponent } from './job-selection-audit-cycle-count/job-selection-audit-cycle-count-scan.component';
import { JobSelectionAuditCycleCountComponent } from './job-selection-audit-cycle-count/job-selection-audit-cycle-count.component';
import { JobSelectionCycleCountsListComponent, JobSelectionCycleCountsScanComponent } from './job-selection-cycle-counts';
import { JobSelectionMoveStockCodeComponent } from './job-selection-move-stock-code/job-selection-move-stock-code/job-selection-move-stock-code.component';
import { JobSelectionNewCountComponent } from './job-selection-new-count/job-selection-new-count.component';
import { JobSelectionRadioSystemRemovalsScanComponent } from './job-selection-radio-system-removals/job-selection-radio-system-removals-scan/job-selection-radio-system-removals-scan.component';
import { JobSelectionReceivingScanComponent } from './job-selection-receiving/job-selection-receiving-scan/job-selection-receiving-scan.component';
import { JobSelectionStockLookupListComponent } from './job-selection-stock-lookup/job-selection-stock-lookup-list/job-selection-stock-lookup-list.component';
import { JobSelectionTechAuditCycleCountListComponent } from './job-selection-tech-audit-cycle-count/job-selection-tech-audit-cycle-count-list/job-selection-tech-audit-cycle-count-list.component';
import { JobSelectionTechAuditCycleCountScanComponent } from './job-selection-tech-audit-cycle-count/job-selection-tech-audit-cycle-count-scan/job-selection-tech-audit-cycle-count-scan.component';
import { JobSelectionTechAuditCycleCountViewComponent } from './job-selection-tech-audit-cycle-count/job-selection-tech-audit-cycle-count-view/job-selection-tech-audit-cycle-count-view.component';
import { JobSelectionTechnicianStockTakeListComponent } from './job-selection-technician-stock-take/job-selection-technician-stock-take-list/job-selection-technician-stock-take-list.component';
import { JobSelectionTechnicianStockTakeScanComponent } from './job-selection-technician-stock-take/job-selection-technician-stock-take-scan/job-selection-technician-stock-take-scan.component';
import { JoSelectionTransferRequestScanComponent } from './job-selection-transfer-request/jo-selection-transfer-request-scan/jo-selection-transfer-request-scan.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: JobSelectionListComponent, canActivate: [AuthGuard],data: { title: 'Job Selection' } },
    { path: 'scan', component: JobSelectionScanComponent, canActivate: [AuthGuard],data: { title: 'Job Selection Picking Scan' } },
    { path: 'packing-scan', component: JobSelectionPackingScanComponent,canActivate: [AuthGuard], data: { title: 'Job Selection Receiving Batches' } },
    { path: 'picked-batches', component: JobSelectionPickedBatchesComponent, canActivate: [AuthGuard],data: { title: 'Job Selection Picked Batches' } },
    { path: 'cycle-count-list', component: JobSelectionCycleCountsListComponent, canActivate: [AuthGuard],data: { title: 'Job Selection Cycle Count' } },
    { path: 'cycle-count-scan', component: JobSelectionCycleCountsScanComponent, canActivate: [AuthGuard],data: { title: 'Job Selection Cycle Count' } },
    { path: 'stock-take-list', component: JobSelectionStockTakeListComponent, canActivate: [AuthGuard],data: { title: 'Job Selection Stock Take' } },
    { path: 'stock-take-scan', component: JobSelectionStockTakeScanComponent, canActivate: [AuthGuard],data: { title: 'Job Selection Stock Take' } },
    { path: 'audit-cycle-count-list', component: JobSelectionAuditCycleCountListComponent,canActivate: [AuthGuard], data: { title: 'Job Selection Audit Cycle Count' } },
    { path: 'audit-cycle-count-view', component: JobSelectionAuditCycleCountComponent, canActivate: [AuthGuard],data: { title: 'Job Selection Audit Cycle Count' } },
    { path: 'audit-cycle-count-scan', component: JobSelectionAuditCycleCountScanComponent, canActivate: [AuthGuard],data: { title: 'Job Selection Audit Cycle Count' } },
    { path: 'new-count', component: JobSelectionNewCountComponent, canActivate: [AuthGuard],data: { title: 'Job Selection New Count' } },
    { path: 'tech-stock-take-list', component: JobSelectionTechnicianStockTakeListComponent, canActivate: [AuthGuard],data: { title: 'Job Selection Tech Stock Take' } },
    { path: 'tech-stock-take-scan', component: JobSelectionTechnicianStockTakeScanComponent, canActivate: [AuthGuard],data: { title: 'Job Selection Tech Stock Take' } },
    { path: 'stock-lookup-list', component: JobSelectionStockLookupListComponent, canActivate: [AuthGuard],data: { title: 'Job Selection Stock Lookup' } },
    { path: 'assign-bin-location', component: JobSelectionAssignBinLocationComponent,canActivate: [AuthGuard], data: { title: 'Job Selection Assign Bin Location' } },
    { path: 'move-stock-code', component: JobSelectionMoveStockCodeComponent,canActivate: [AuthGuard], data: { title: 'Job Selection Move Stock Code' } },
    { path: 'receiving-scan', component: JobSelectionReceivingScanComponent, canActivate: [AuthGuard],data: { title: 'Job Selection Receiving' } },

    { path: 'tech-audit-cycle-count-list', component: JobSelectionTechAuditCycleCountListComponent, data: { title: 'Job Selection Technician Audit Cycle Count' } },
    { path: 'tech-audit-cycle-count-view', component: JobSelectionTechAuditCycleCountViewComponent, data: { title: 'Job Selection Technician Audit Cycle Count' } },
    { path: 'tech-audit-cycle-count-scan', component: JobSelectionTechAuditCycleCountScanComponent, data: { title: 'Job Selection Technician Audit Cycle Count' } },
    { path: 'radio-system-removals-receiving', component: JobSelectionRadioSystemRemovalsScanComponent, data: { title: 'Job Selection Radio System Removal Receiving' } },
    { path: 'transfer-request-picking', component: JoSelectionTransferRequestScanComponent, data: { title: 'Job Selection Transfer Request Picking Job' } },

    { path: 'receiving-batches', component: JobSelectionReceivingBatchesComponent, data: { title: 'Job Selection Receiving Batches' } },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],

})

export class InStaffJobSelectionRoutingModule { }
