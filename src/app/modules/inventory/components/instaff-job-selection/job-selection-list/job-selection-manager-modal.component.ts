import { HttpParams } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import {
    AuthenticationModuleApiSuffixModels, CrudService,
    HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix,
    ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService, UserService
} from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
    selector: 'app-job-selection-manager-modal',
    templateUrl: './job-selection-manager-modal.component.html',
})

export class JobSelectionManagerModalComponent implements OnInit {

    reasonText = '';
    managerOverrideUpdateForm: FormGroup;
    availableBinList: any = [];
    isAvailableBin : boolean = false;
    overrideReasonDropDown: any = [];
    pickersList: any = [];
    userData: UserLogin;
    showAlternatePickers: boolean = false;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data,private router: Router,
        private dialogRef: MatDialogRef<JobSelectionManagerModalComponent>, private crudService: CrudService,
        private rxjsService: RxjsService, private formBuilder: FormBuilder,
        private snackbarService: SnackbarService, private httpCancelService: HttpCancelService,
        private userService: UserService, private store: Store<AppState>
    ) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
    }

    ngOnInit(): void {

        this.createEmployeeeRegistrationForm();
        this.getDropdown();

        if (this.data.type == 'Bins') {
            let params = new HttpParams().set('ItemId', this.data.ItemId).set('WarehouseId', this.data.WarehouseId).set('LocationId', this.data.WarehouseId);
            this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_AVAILABLE_BINS, undefined, true, params)
                .subscribe((response: IApplicationResponse) => {
                    this.rxjsService.setGlobalLoaderProperty(true);
                    if (response.isSuccess && response.statusCode == 200 && response.resources) {
                        this.isAvailableBin = true;
                        this.availableBinList = response.resources;
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                    else {
                        this.isAvailableBin = false;
                        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                });
        }
    }

    getDropdown() {

        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_OVERRIDEREASONS)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.overrideReasonDropDown = response.resources
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });

        let params = new HttpParams().set('RoleName', this.userData.roleName).set('warehouseid', this.userData.warehouseId);
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PICKER,
            undefined, true, params).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                    this.pickersList = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    createEmployeeeRegistrationForm() {
        this.managerOverrideUpdateForm = this.formBuilder.group({
            VarianceQuantity: [this.data.Quantity],
            UserName: ['', this.data.type == 'Picking' || this.data.type == 'Packing' || this.data.type == 'Transfer-request' ? Validators.required : ''],
            password: ['', this.data.type == 'Picking' || this.data.type == 'Packing' || this.data.type == 'Transfer-request' ? Validators.required : ''],
            Comments: ['', this.data.type == 'Picking' || this.data.type == 'Packing' || this.data.type == 'Transfer-request' ? Validators.required : ''],
            overrideReasonId: ['', this.data.type == 'Picking' || this.data.type == 'Packing' || this.data.type == 'Transfer-request' ? Validators.required : ''],
            locationBinIds: ['', this.data.type == 'Bins' ? Validators.required : ''],
            alternatePickerId: ['']
          });   
        //    if(this.data.type == 'Picking' || this.data.type == 'Packing' || this.data.type == 'Transfer-request')
        //        this.managerOverrideUpdateForm = setRequiredValidator(this.managerOverrideUpdateForm, ['UserName','Password','Comments',"overrideReasonId"]);
        //    if(this.data.type == 'Bins')
        //        this.managerOverrideUpdateForm = setRequiredValidator(this.managerOverrideUpdateForm, ['locationBinIds']);

        this.rxjsService.setFormChangeDetectionProperty(true);
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    onChange(reasons) {
        if (reasons != '' && reasons != null && this.overrideReasonDropDown.length > 0) {
            let getDetails = this.overrideReasonDropDown.filter(x => x.id == reasons);
            if (getDetails[0].displayName != undefined) {
                getDetails[0].displayName == 'Reassign' ? this.showAlternatePickers = true : this.showAlternatePickers = false;
            }

        }
    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }

    onSubmit(): void {

        if (this.showAlternatePickers) {
            this.managerOverrideUpdateForm.get('alternatePickerId').setValidators([Validators.required]);
            this.managerOverrideUpdateForm.get('alternatePickerId').updateValueAndValidity();
            this.managerOverrideUpdateForm.get('alternatePickerId').markAsUntouched();
        }
        else {
            this.managerOverrideUpdateForm.get('alternatePickerId').setErrors(null);
            this.managerOverrideUpdateForm.get('alternatePickerId').clearValidators();
            this.managerOverrideUpdateForm.get('alternatePickerId').markAllAsTouched();
            this.managerOverrideUpdateForm.get('alternatePickerId').updateValueAndValidity();
        }

        if (this.managerOverrideUpdateForm.invalid) {
            this.managerOverrideUpdateForm.markAllAsTouched();
            return;
        }

        if (this.data.type == 'Picking' || this.data.type == 'Packing' || this.data.type == 'Transfer-request') {

            let login = {
                username: this.managerOverrideUpdateForm.get('UserName').value,
                password: this.managerOverrideUpdateForm.get('password').value 
            }
            this.rxjsService.setFormChangeDetectionProperty(true);
            this.httpCancelService.cancelPendingRequestsOnFormSubmission();
            this.crudService.create(ModulesBasedApiSuffix.AUTHENTICATION, AuthenticationModuleApiSuffixModels.LOGIN_V2, login)
                .subscribe((response: IApplicationResponse) => {
                    if (typeof response.resources.accessToken === 'string' && response.isSuccess) {
                        const decodedUserData = this.userService.getDecodedLoggedInData(response.resources.accessToken);

                        if (decodedUserData) {
                            const parsedUserData = JSON.parse(decodedUserData['userData']);
                            let formData = {}

                            if (this.data.type == 'Picking') {
                                formData = {
                                    PackerJobId: this.data.PackerJobId,
                                    ItemId: this.data.ItemId,
                                    locationBinId: this.data.LocationBinId,
                                    createdUserId: parsedUserData.userId,
                                    itemCount: this.managerOverrideUpdateForm.get('VarianceQuantity').value,
                                    comment: this.managerOverrideUpdateForm.get('Comments').value,
                                    overrideReasonId: this.managerOverrideUpdateForm.get('overrideReasonId').value,
                                    isNotSerialized: this.data.isNotSerialized,
                                }
                            }
                            else if (this.data.type == 'Transfer-request') {
                                formData = {
                                    pickerJobId: this.data.PackerJobId,
                                    itemId: this.data.ItemId,
                                    orderType: this.data.OrderType,
                                    serialNumber: this.data.SerialNumber,
                                    qty: this.data.Quantity,
                                    overrideReasonId: this.managerOverrideUpdateForm.get('overrideReasonId').value,
                                    comment: this.managerOverrideUpdateForm.get('Comments').value,
                                    locationBinId: this.data.LocationBinId,
                                    locationId: this.data.LocationId,
                                    isNotSerialized: this.data.isNotSerialized,
                                    userId: parsedUserData.userId
                                }
                            }
                            else {
                                formData = {
                                    PickerJobId: this.data.PackerJobId,
                                    ItemId: this.data.ItemId,
                                    createdUserId: parsedUserData.userId,
                                    itemCount: this.managerOverrideUpdateForm.get('VarianceQuantity').value,
                                    comment: this.managerOverrideUpdateForm.get('Comments').value,
                                    overrideReasonId: this.managerOverrideUpdateForm.get('overrideReasonId').value,
                                    isNotSerialized: this.data.isNotSerialized,
                                }
                            }

                            let crudService: Observable<IApplicationResponse> =
                                this.data.type == 'Picking' ? this.crudService.create(
                                    ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PACKERS_JOBS_MANAGER_OVERRIDE, formData) :
                                    this.data.type == 'Transfer-request' ? this.crudService.create(
                                        ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.TRANSFER_REQUEST_PICKING_JOB_MANAGER_OVERRIDE, formData) :
                                        this.crudService.update(
                                            ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PICKERJOBS_MANAGER_OVERRIDE, formData)
                            crudService.subscribe((response: IApplicationResponse) => {
                                if (response.isSuccess && response.statusCode == 200) {                        
                                     let  reasons = this.managerOverrideUpdateForm.get('overrideReasonId').value;        
                                     let  getDetails = this.overrideReasonDropDown.filter(x => x.id == reasons);
                                     if(getDetails?.length >0 && getDetails[0].displayName == 'Reassign')
                                       this.navigateToJobSelection();

                                    this.dialogRef.close({ bool: true });
                                }
                                else {
                                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                                    this.rxjsService.setGlobalLoaderProperty(false);
                                }
                                this.rxjsService.setGlobalLoaderProperty(false);
                            });
                            this.rxjsService.setGlobalLoaderProperty(false);
                        }
                        else {
                            this.snackbarService.openSnackbar("Invalid credentials", ResponseMessageTypes.ERROR);
                            this.rxjsService.setGlobalLoaderProperty(false);
                        }
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                });
        }
        else {
            const formBinsData = {
                ItemId: this.data.ItemId,
                createdUserId: this.data.userId,
                locationBinIds: new Array(this.managerOverrideUpdateForm.get('locationBinIds').value)
            }

            let crudService: Observable<IApplicationResponse> = this.crudService.create(
                ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_IN_BINS_LINK,
                formBinsData
            )
            crudService.subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.dialogRef.close({ bool: true, locationBinIds: formBinsData.locationBinIds });
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
        }
    }
    navigateToJobSelection() {
        this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
    }
}