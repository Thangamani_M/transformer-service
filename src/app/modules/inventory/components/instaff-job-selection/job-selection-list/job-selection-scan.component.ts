import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { JobSelectionAddEditModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { BarcodePrintComponent } from '../../order-receipts/barcode-print';
import { JobSelectionManagerModalComponent } from './job-selection-manager-modal.component';
import { JobSelectionScanModalComponent } from './job-selection-scan-modal.component';

@Component({
    selector: 'app-job-selection-scan',
    templateUrl: './job-selection-scan.component.html',
    styleUrls: ['./job-selection-scan.component.scss']
})
export class JobSelectionScanComponent {

    pickerJobDetails: any;
    packerJobScanItemDetails: any;
    userId: any;
    packerJobId: any;
    warehouseId: any;
    type: any;
    pickingStockOrderScanForm: FormGroup;
    showManager: boolean;
    quantity: any;
    duplicateError: any;
    showDuplicateError: boolean = false;
    isPrintSuccessDialog: boolean = false;
    printMessages: any;
    barcodes: any = [];

    constructor(
        private store: Store<AppState>, private router: Router,
        private crudService: CrudService, private formBuilder: FormBuilder,
        private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute,
        private rxjsService: RxjsService, private dialog: MatDialog,
    ) {
        this.userId = this.activatedRoute.snapshot.queryParams.userId;
        this.packerJobId = this.activatedRoute.snapshot.queryParams.packerJobId;
        this.warehouseId = this.activatedRoute.snapshot.queryParams.warehouseId;
    }

    ngOnInit() {
        this.getSelectionJobsHeaderDetailsById();
        this.getSelectionJobsDetailsById();
        this.createpickingStockOrderScanForm();

        this.pickingStockOrderScanForm.get('pickedQty').valueChanges.subscribe(pickedQty => {
            this.quantity = this.pickingStockOrderScanForm.get('totalQty').value - this.pickingStockOrderScanForm.get('pickedQty').value

            if (this.quantity >= 1) {
                this.showManager = true;
            }
            else {
                this.showManager = false;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    getSelectionJobsHeaderDetailsById(): void {

        let params = new HttpParams().set('UserId', this.userId)
            .set('PackerJobId', this.packerJobId);

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.PACKERJOBS_DETAILS, undefined, true, params)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.pickerJobDetails = response.resources;
                    this.barcodes = new Array(this.pickerJobDetails?.batchBarcode);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    getSelectionJobsDetailsById() {

        let params = new HttpParams().set('UserId', this.userId)
            .set('PackerJobId', this.packerJobId);

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.PACKERJOBS_ITEM_DETAILS, undefined, true, params)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.receiveJobSelectionDetails(response);
                    if (response.resources.packerJobScanItem == null || !response.resources.packerJobScanItem) {
                        this.navigateToList();
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    createpickingStockOrderScanForm(): void {
        let stockOrderModel = new JobSelectionAddEditModel();
        // create form controls dynamically from model class
        this.pickingStockOrderScanForm = this.formBuilder.group({});
        Object.keys(stockOrderModel).forEach((key) => {
            this.pickingStockOrderScanForm.addControl(key, new FormControl(stockOrderModel[key]));
        });
    }

    checkScanItem() {

        this.showDuplicateError = false;
        if (!this.packerJobScanItemDetails?.isNotSerialized &&
            this.pickingStockOrderScanForm.get('scanSerialNumberInput').value == '') {
            this.showDuplicateError = true;
            this.duplicateError = "Serial number is required";
            return;
        }

        if (this.pickingStockOrderScanForm.invalid) {
            return
        }

        let serialNumber = this.pickingStockOrderScanForm.get('scanSerialNumberInput').value;
        let sendParams = {
            "packerJobId": this.packerJobId,
            "itemId": this.pickingStockOrderScanForm.get('itemId').value,
            "serialNumber": serialNumber,
            "qty": !this.packerJobScanItemDetails.isNotSerialized ? 1 : this.pickingStockOrderScanForm.get('pickedQty').value,
            "locationBinId": this.pickingStockOrderScanForm.get('locationBinId').value,
            "locationId": this.packerJobScanItemDetails.locationId,
            "OrderType": null,
            "userId": this.userId,
            "isNotSerialized": this.pickingStockOrderScanForm.get('isNotSerialized').value,
        }

        let crudService: Observable<IApplicationResponse> = this.crudService.create(
            ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PACKERJOBS_SCAN, sendParams)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.resources.isSuccess && response.statusCode == 200) {
                if (response.resources.isFullyPicked || response.resources.isPartialyPicked) {
                    this.receiveJobSelectionDetails(response);
                }
                else {
                    this.getSelectionJobsHeaderDetailsById();
                    this.getSelectionJobsDetailsById();
                }
            }
            else {
                this.snackbarService.openSnackbar(response.resources.exceptionMsg, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

    getSelectionNewItemsById() {

        let params = new HttpParams().set('WareHouseId', this.warehouseId)
            .set('CreatedUserId', this.userId).set('UserId', this.userId);

        let crudService: Observable<IApplicationResponse> =
            this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.PACKER_JOBS_ASSIGN, undefined, true, params)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode === 200) {
                this.rxjsService.setGlobalLoaderProperty(false);
                if (!response.resources.isSuccess) {
                    this.navigateToList();
                    this.snackbarService.openSnackbar('Picker Job is not Available', ResponseMessageTypes.WARNING);
                    return;
                }
                else {
                    this.packerJobId = response.resources.packerJobId;
                    this.getSelectionJobsDetailsById();
                    this.getSelectionJobsHeaderDetailsById();
                }
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

    receiveJobSelectionDetails(response: any) {

        if (response.resources.packerJobScanItem != null) {
            this.packerJobScanItemDetails = response.resources.packerJobScanItem;
            let stockOrderModel = new JobSelectionAddEditModel(response.resources.packerJobScanItem);
            this.pickingStockOrderScanForm.patchValue(stockOrderModel);
            this.pickingStockOrderScanForm.get('bincode').patchValue(response.resources.packerJobScanItem.binDetails);
            this.pickingStockOrderScanForm.get('totalQty').patchValue(response.resources.packerJobScanItem.orderQty);
            this.pickingStockOrderScanForm.get('scanSerialNumberInput').patchValue('');

            this.quantity = this.pickingStockOrderScanForm.get('totalQty').value - this.pickingStockOrderScanForm.get('pickedQty').value

            if (this.quantity >= 1) {
                this.showManager = true;
            }
            else {
                this.showManager = false;
            }

            if (response.resources.isNotSerialized) {
                this.pickingStockOrderScanForm = setRequiredValidator(this.pickingStockOrderScanForm, ["totalQty"]);
            }
        }

        if (response.resources.isError) {
            this.snackbarService.openSnackbar('Picker Job is not Available', ResponseMessageTypes.WARNING);
            this.navigateToList();
        }

        if (response.resources.isFullyPicked) {
            this.onModalPopupOpen('fully');
        }

        if (response.resources.isPartialyPicked) {
            this.onModalPopupOpen('partially');
        }

    }

    onModalPopupOpen(message: string) {
        const dialogRemoveStockCode = this.dialog.open(JobSelectionScanModalComponent, {
            width: '450px',
            data: {
                header: `Confirmation`,
                message: `Order <b>${this.pickerJobDetails?.stockOrderNumber}</b> for <b>${this.pickerJobDetails?.requestingLocation}</b> </br>
            ${this.pickerJobDetails?.requestingLocationName} ${message} picked`,
                buttons: {
                    create: 'Ok'
                }
            }, disableClose: true
        });
        dialogRemoveStockCode.afterClosed().subscribe(result => {
            if (!result) return;
            this.isPrintSuccessDialog = true;
            this.printMessages = 'Do you want to print Batch Barcode ' + this.pickerJobDetails?.batchBarcode + '?'
            this.rxjsService.setDialogOpenProperty(false);
        });
    }

    onModalPopupNextOpen() {

        this.isPrintSuccessDialog = false;
        const dialogRemoveStockCode = this.dialog.open(JobSelectionScanModalComponent, {
            width: '450px',
            data: {
                header: `Confirmation`,
                message: `Pick Next Stock Order`,
                buttons: {
                    cancel: 'No',
                    create: 'Yes'
                }
            }, disableClose: true
        });
        dialogRemoveStockCode.afterClosed().subscribe(result => {
            if (result && typeof result == 'boolean') {
                this.getSelectionNewItemsById();
                this.rxjsService.setDialogOpenProperty(false);
            }
            else if (!result && typeof result == 'boolean') {
                this.navigateToList();
            }
        });
    }

    reprintAction() {
        this.printMessages = 'Do you want to Reprint Batch Barcode ' + this.pickerJobDetails?.batchBarcode;
        this.isPrintSuccessDialog = true;
    }

    managerOverridePopup(value: string) {
        const dialogManagerOverride = this.dialog.open(JobSelectionManagerModalComponent, {
            width: '450px',
            data: {
                header: 'Manager Override',
                PackerJobId: this.pickerJobDetails['packerJobId'],
                ItemId: this.pickingStockOrderScanForm.get('itemId').value,
                Quantity: this.packerJobScanItemDetails.availableQty,
                LocationBinId: this.pickingStockOrderScanForm.get('locationBinId').value,
                isNotSerialized: this.pickingStockOrderScanForm.get('isNotSerialized').value,
                type: value
            }, disableClose: true
        });
        dialogManagerOverride.afterClosed().subscribe(result => {
            if (result && typeof result.bool == 'boolean') {
                this.getSelectionJobsDetailsById();
                this.getSelectionJobsHeaderDetailsById();
                this.rxjsService.setDialogOpenProperty(false);
            }
            else if (!result && typeof result.bool == 'boolean') {
                this.navigateToList();
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    navigateToList() {
        this.router.navigate(['/inventory', 'job-selection'], {
            queryParams: {}, skipLocationChange: true
        });
    }
}

