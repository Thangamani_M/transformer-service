import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { JobSelectionAddEditModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { JobSelectionManagerModalComponent } from './job-selection-manager-modal.component';
import { JobSelectionScanModalComponent } from './job-selection-scan-modal.component';

@Component({
    selector: 'app-job-selection-receiving-batches',
    templateUrl: './job-selection-receiving-batches.component.html',
    styleUrls: ['./job-selection-scan.component.scss']
})

export class JobSelectionReceivingBatchesComponent implements OnInit {

    userData: UserLogin;
    pickerJobDetails: any;
    packerJobScanItemDetails: any;
    type: any;
    pickingStockOrderScanForm: FormGroup;
    showManager: boolean;
    quantity: any;
    pickerId: string = '';
    orderType: string = '';
    showDuplicateBinError: boolean = false;
    duplicateBinError: any;
    showDuplicateSerialError: boolean = false;
    duplicateSerialError: any;
    receivingBarcodeItem: boolean = false;
    showDuplicateBarcodeError: boolean = false;
    duplicateBarcodeError: any;
    showDuplicatePickerError: boolean = false;
    duplicatePickerError: any;
    scannedBinDetails: any;
    switchBinGetDetails: any = [];
    warehouseId;
    constructor(
        private store: Store<AppState>, private router: Router,
        private crudService: CrudService, private formBuilder: FormBuilder,
        private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute,
        private rxjsService: RxjsService, private dialog: MatDialog
    ) {
        this.pickerId = this.activatedRoute.snapshot.queryParams.pickerJobId;
        this.orderType = this.activatedRoute.snapshot.queryParams.type;
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });


    }

    ngOnInit() {

        this.createpickingStockOrderScanForm();
        this.pickingStockOrderScanForm.get('itemId').valueChanges.subscribe(codeTrue => {
            if (codeTrue != '') {
                this.quantity = this.pickingStockOrderScanForm.get('totalQty').value -
                    this.pickingStockOrderScanForm.get('pickedQty').value
                if (this.quantity >= 1) {
                    this.showManager = true;
                }
                else {
                    this.showManager = false;
                }
            }
        });

        this.pickingStockOrderScanForm.get('pickedQty').valueChanges.subscribe(qty => {
            this.showDuplicatePickerError = false;
            if (this.pickingStockOrderScanForm.get('totalQty').value < qty) {
                this.showDuplicatePickerError = true;
                this.duplicatePickerError = "Picked qty cannot be greater than total qty";
                return;
            }

        });

        if (this.pickerId == null) {
            this.receivingBarcodeItem = true;
        }
        else {
            this.receivingBarcodeItem = false;
            this.getSelectionJobsDetailsById();
        }

        this.pickingStockOrderScanForm.get('itemId').valueChanges.subscribe(itemIdValue => {
            if (itemIdValue != '') {
                let params = new HttpParams().set('WarehouseId', this.userData.warehouseId).set('ItemId', itemIdValue)
                this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_IN_BINS, undefined, true, params)
                    .subscribe((response: IApplicationResponse) => {
                        if (response.isSuccess && response.statusCode == 200) {
                            if (response.resources.length > 0) {
                                this.switchBinGetDetails = response.resources[0];
                                this.pickingStockOrderScanForm.get('bincode').patchValue(this.switchBinGetDetails.displayName);
                                this.pickingStockOrderScanForm.get('locationBinId').patchValue(this.switchBinGetDetails.locationBinId);
                                this.pickingStockOrderScanForm.get('scanBinLocationInput').patchValue(this.switchBinGetDetails.displayName);
                                this.rxjsService.setGlobalLoaderProperty(false);
                            }
                        }
                        else {
                            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                            this.rxjsService.setGlobalLoaderProperty(false);
                        }
                    });
            }
        });

        this.rxjsService.setGlobalLoaderProperty(false);
    }

    getSelectionJobsDetailsById() {

        this.rxjsService.setGlobalLoaderProperty(true);
        let detailsParams = new HttpParams().set('CreatedUserId', this.userData.userId)
            .set('UserId', this.userData.userId).set('PickerJobId', this.pickerId);
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PICKER_JOBS_DETAILS, undefined, true, detailsParams)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.pickerJobDetails = response.resources;
                    this.warehouseId = this.pickerJobDetails.warehouseId;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });

        let scanParams = new HttpParams().set('CreatedUserId', this.userData.userId)
            .set('PickerJobId', this.pickerId).set('UserId', this.userData.userId);

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.PICKERJOBS_SCAN_ITEM_DETAILS, undefined, true, scanParams)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    if (!response.resources.isJobCompleted) {
                        this.packerJobScanItemDetails = response.resources;
                        let stockOrderModel = new JobSelectionAddEditModel(response.resources.pickerJobScanItem);
                        this.pickingStockOrderScanForm.patchValue(stockOrderModel);
                        this.pickingStockOrderScanForm.get('totalQty').patchValue(response.resources.pickerJobScanItem.orderedQty);
                        this.rxjsService.setGlobalLoaderProperty(false);

                        if (this.packerJobScanItemDetails != null) {
                            this.quantity = this.pickingStockOrderScanForm.get('totalQty').value - this.pickingStockOrderScanForm.get('pickedQty').value
                            if (this.quantity >= 1) {
                                this.showManager = true;
                            }
                            else {
                                this.showManager = false;
                            }
                        }
                    }

                    if (response.resources.isJobCompleted) {
                        this.pickingStockOrderScanForm.get('pickedQty').patchValue(
                            this.pickingStockOrderScanForm.get('pickedQty').value + 1);
                    }

                    if (response.resources.isJobCompleted && response.resources.isFullyPicked) {
                        this.onModalPopupOpen('fully');
                    }

                    if (response.resources.isJobCompleted && response.resources.isPartiallyPicked) {
                        this.onModalPopupOpen('partially');
                    }

                    if (response.resources.pickerJobScanItem.isNotSerialized) {
                        this.pickingStockOrderScanForm = setRequiredValidator(this.pickingStockOrderScanForm, ["pickedQty"]);
                    }

                    // this.snackbarService.openSnackbar(response.resources.responseMsg, ResponseMessageTypes.SUCCESS); 
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });

    }

    createpickingStockOrderScanForm(): void {
        let stockOrderModel = new JobSelectionAddEditModel();
        // create form controls dynamically from model class
        this.pickingStockOrderScanForm = this.formBuilder.group({});
        Object.keys(stockOrderModel).forEach((key) => {
            this.pickingStockOrderScanForm.addControl(key, new FormControl(stockOrderModel[key]));
        });
    }

    receivingBarcodeScanItem() {

        this.showDuplicateBarcodeError = false;
        if (this.pickingStockOrderScanForm.get('scanReceivingBarcodeInput').value == '') {
            this.showDuplicateBarcodeError = true;
            this.duplicateBarcodeError = "Serial number is required";
            return;
        }

        let sendParams = new HttpParams().set('WareHouseId', this.userData.warehouseId)
            .set('CreatedUserId', this.userData.userId).set('UserId', this.userData.userId)
            .set('BarcodeNumber', this.pickingStockOrderScanForm.get('scanReceivingBarcodeInput').value);
        this.rxjsService.setFormChangeDetectionProperty(true);

        let crudService: Observable<IApplicationResponse> =
            this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.PICKERJOBS_JOB_ASSIGN, undefined, true, sendParams)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                if (response.resources.pickerJobId != null) {
                    this.receivingBarcodeItem = false;
                    this.pickerId = response.resources.pickerJobId
                    this.getSelectionJobsDetailsById();
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.receivingBarcodeItem = true;
                    this.snackbarService.openSnackbar(response.resources.responseMsg, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });

    }

    checkScanItem() {

        this.showDuplicateBinError = false;
        if (this.pickingStockOrderScanForm.get('scanBinLocationInput').value == '') {
            this.showDuplicateBinError = true;
            this.duplicateBinError = "Bin location is required";
            return;
        }

        this.rxjsService.setFormChangeDetectionProperty(true);
        let binParams = new HttpParams().set('binLocation',
            this.pickingStockOrderScanForm.get('scanBinLocationInput').value)
            .set('warehouseId', this.userData.warehouseId);
        let crudService: Observable<IApplicationResponse> =
            this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_IN_BINS_SCAN, undefined, true, binParams)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.scannedBinDetails = response.resources;
                this.pickingStockOrderScanForm.get('locationBinId').patchValue(response.resources.locationBinId);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }


    checkSerialScanItem() {

        this.showDuplicateSerialError = false;
        this.showDuplicatePickerError = false;

        if (this.pickingStockOrderScanForm.invalid) {
            return;
        }

        if (this.pickingStockOrderScanForm.get('scanSerialNumberInput').value == '' &&
            !this.packerJobScanItemDetails.pickerJobScanItem.isNotSerialized) {
            this.showDuplicateSerialError = true;
            this.duplicateSerialError = "Serial number is required";
            return;
        }

        else if (this.scannedBinDetails == undefined) {
            this.snackbarService.openSnackbar('Bin location is not available', ResponseMessageTypes.ERROR);
            return;
        }

        let sendParams = {
            "pickerJobId": this.pickerId,
            "itemId": this.pickingStockOrderScanForm.get('itemId').value,
            "barcode": this.packerJobScanItemDetails.pickerJobScanItem.isNotSerialized ? null
                : this.pickingStockOrderScanForm.get('scanSerialNumberInput').value,
            "serialNumber": this.packerJobScanItemDetails.pickerJobScanItem.isNotSerialized ? null
                : this.pickingStockOrderScanForm.get('scanSerialNumberInput').value,
            "qrCode": null,
            "locationBinId": this.scannedBinDetails.locationBinId,
            "quantity": this.packerJobScanItemDetails.pickerJobScanItem.isNotSerialized ?
                this.pickingStockOrderScanForm.get('pickedQty').value : 1,
            "createdUserId": this.userData.userId

        }

        this.rxjsService.setFormChangeDetectionProperty(true);
        let crudService: Observable<IApplicationResponse> =
            this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PICKERJOBS_SCAN, sendParams)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.getSelectionJobsDetailsById();
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

    index = 1;
    switchBinsGetMethod() {
        this.rxjsService.setFormChangeDetectionProperty(true);
        if (this.index < this.switchBinGetDetails.length) {
            this.pickingStockOrderScanForm.get('bincode').patchValue(this.switchBinGetDetails[this.index].displayName);
            // this.pickingStockOrderScanForm.get('scanBinLocationInput').patchValue(this.switchBinGetDetails[this.index].displayName)
            this.index++;
        }
    }

    variancePopupModal() {
        const dialogVariance = this.dialog.open(JobSelectionScanModalComponent, {
            width: '450px',
            data: {
                header: `Variance Escalation`,
                message: `Do you want to create Variance Escalation?`,
                buttons: {
                    cancel: 'No',
                    create: 'Yes'
                }
            }, disableClose: true
        });
        dialogVariance.afterClosed().subscribe(result => {
            if (result && typeof result == 'boolean') {
                this.rxjsService.setDialogOpenProperty(false);

                let variance = {}
                variance = {
                    "orderNumber": this.pickerJobDetails.orderNumber,
                    "warehouseId": this.userData.warehouseId,
                    "locationId": null,
                    "varianceInvReqUserId": this.userData.userId,
                    "orderTypeId": null,
                    "technicianStockOrderCollectionBatchId": null,
                    "isExcessiveStock": false,
                    "isShortPickedStock": true,
                    "quantity": this.quantity,
                    "itemId": this.pickingStockOrderScanForm.get('itemId').value,
                    "serialNumber": null,
                    "referenceId": this.pickerJobDetails.pickerJobId
                }

                this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
                    InventoryModuleApiSuffixModels.VARIANCE_ESCLATION_LIST, variance)
                    .subscribe((response: IApplicationResponse) => {
                        if (response.resources && response.statusCode === 200) {
                            this.onVarianceModalPopupOpen(response.resources)
                        }
                        else {
                            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                            this.rxjsService.setGlobalLoaderProperty(false);
                        }
                    });
            }
        });
    }

    onVarianceModalPopupOpen(message: string) {
        const dialogModal = this.dialog.open(JobSelectionScanModalComponent, {
            width: '450px',
            data: {
                header: `Variance Escalation`,
                message: `${this.pickingStockOrderScanForm.get('itemCode').value} - ${this.pickerJobDetails.orderNumber}
            short picked stock item. <br> Variance Escalation ${message} has been created </br>`,
                buttons: {
                    create: 'Ok'
                }
            }, disableClose: true
        });
        dialogModal.afterClosed().subscribe(result => {
            if (!result) { return };
            if (result && typeof result == 'boolean') {
                this.callNextPickedBatches();
            }
            this.rxjsService.setDialogOpenProperty(false);
        });
    }

    callNextPickedBatches() {

        let picked = new HttpParams().set('wareHouseId', this.userData.warehouseId)
            .set('createdUserId', this.userData.userId).set('UserId', this.userData.userId);
        let crudService: Observable<IApplicationResponse> = this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.PICKEDJOBS_ASSIGN, undefined, true, picked)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.resources && response.statusCode === 200) {
                let pickerId = response.resources.pickerJobId;
                this.rxjsService.setGlobalLoaderProperty(false);
                if (pickerId == null) {
                    this.receivingBarcodeItem = true;
                }
                else {
                    this.receivingBarcodeItem = false;
                    this.getSelectionJobsDetailsById();
                }
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });

    }

    onModalPopupOpen(message: string) {
        let barcode;
        if (this.pickerJobDetails != undefined) {
            barcode = this.pickerJobDetails.receivingBarcode;
        }
        else {
            barcode = '';
        }
        const dialogRemoveStockCode = this.dialog.open(JobSelectionScanModalComponent, {
            width: '450px',
            data: {
                header: `Batch ${message} Packed`,
                message: `Receiving Barcode ${barcode} ${message} packed`,
                buttons: {
                    create: 'Ok'
                }
            }, disableClose: true
        });
        dialogRemoveStockCode.afterClosed().subscribe(result => {
            if (!result) return;
            this.onModalPopupNextOpen();
            this.rxjsService.setDialogOpenProperty(false);
        });
    }

    onModalPopupNextOpen() {
        const dialogRemoveStockCode = this.dialog.open(JobSelectionScanModalComponent, {
            width: '450px',
            data: {
                header: `Job Confirmation`,
                message: `Pack Next Received Batch`,
                buttons: {
                    cancel: 'No',
                    create: 'Yes'
                }
            }, disableClose: true
        });
        dialogRemoveStockCode.afterClosed().subscribe(result => {
            if (result && typeof result == 'boolean') {
                this.receivingBarcodeItem = true;
                this.rxjsService.setDialogOpenProperty(false);
            }
            else if (!result && typeof result == 'boolean') {
                this.navigateToList();
            }
        });
    }

    managerOverridePopup(value: string) {
        if (value == 'Bins') {
            var header = 'Available Bins'
        }
        else {
            header = 'Manager Override'
        }
        const dialogManagerOverride = this.dialog.open(JobSelectionManagerModalComponent, {
            width: '450px',
            data: {
                header: header,
                PackerJobId: this.pickerId,
                ItemId: this.pickingStockOrderScanForm.get('itemId').value,
                Quantity: this.quantity,
                WarehouseId: this.warehouseId,
                LocationBinId: this.pickingStockOrderScanForm.get('locationBinId').value,
                isNotSerialized: this.pickingStockOrderScanForm.get('isNotSerialized').value,
                userId: this.userData.userId,
                type: value
            }, disableClose: true
        });
        dialogManagerOverride.afterClosed().subscribe(result => {
            if (result && typeof result.bool == 'boolean') {
                this.getSelectionJobsDetailsById();
                this.pickingStockOrderScanForm.get('bincode').patchValue(this.switchBinGetDetails.displayName);
                this.rxjsService.setDialogOpenProperty(false);
            }
            else if (!result && typeof result.bool == 'boolean') {
                return;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }

    navigateToList() {
        this.router.navigate(['/inventory', 'job-selection'], {
            skipLocationChange: true
        });
    }
}