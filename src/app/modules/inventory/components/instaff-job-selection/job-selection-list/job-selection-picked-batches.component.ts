import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { JobSelectionAddEditModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { UserLogin } from '@modules/others/models/others-module-models';
import { Observable } from 'rxjs';
import { JobSelectionManagerModalComponent } from './job-selection-manager-modal.component';
import { JobSelectionScanModalComponent } from './job-selection-scan-modal.component';

@Component({
    selector: 'app-job-selection-picked-batches',
    templateUrl: './job-selection-picked-batches.component.html',
    styleUrls: ['./job-selection-scan.component.scss']
})

export class JobSelectionPickedBatchesComponent {

    userData: UserLogin;
    pickerJobDetails: any;
    packerJobScanItemDetails: any;
    userId: any;
    warehouseId: any;
    type: any;
    pickingStockOrderScanForm: FormGroup;
    showManager: boolean;
    quantity: any;
    pickerId: any;
    showDuplicateBinError: boolean = false;
    duplicateBinError: any;
    showDuplicateSerialError: boolean = false;
    duplicateSerialError: any;
    receivingBarcodeItem: boolean = false;
    showDuplicateBarcodeError: boolean = false;
    duplicateBarcodeError: any;
    showDuplicatePickerError: boolean = false;
    duplicatePickerError: any;
    scannedBinDetails: any;
    switchBinGetDetails: any = [];
    itemIdValue;

    constructor(
        private router: Router,
        private crudService: CrudService, private formBuilder: FormBuilder,
        private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute,
        private rxjsService: RxjsService, private dialog: MatDialog,
    ) {
        this.userId = this.activatedRoute.snapshot.queryParams.userId;
        // this.warehouseId = this.activatedRoute.snapshot.queryParams.warehouseId;
        this.pickerId = this.activatedRoute.snapshot.queryParams.pickerId;
    }

    ngOnInit() {

        this.createpickingStockOrderScanForm();

        if (this.pickerId == null) {
            this.receivingBarcodeItem = true;
        }
        else {
            this.receivingBarcodeItem = false;
            this.getSelectionJobsDetailsById();
        }

        this.pickingStockOrderScanForm.get('itemId').valueChanges.subscribe(codeTrue => {
            if (codeTrue != '') {
                this.itemIdValue = codeTrue;
                this.quantity = this.pickingStockOrderScanForm.get('totalQty').value -
                    this.pickingStockOrderScanForm.get('pickedQty').value
                if (this.quantity >= 1) {
                    this.showManager = true;
                }
                else {
                    this.showManager = false;
                }
            }
        });

        this.pickingStockOrderScanForm.get('pickedQty').valueChanges.subscribe(qty => {
            this.showDuplicatePickerError = false;
            if (this.pickingStockOrderScanForm.get('totalQty').value < qty) {
                this.showDuplicatePickerError = true;
                this.duplicatePickerError = "Picked qty cannot be greater than total qty";
                return;
            }
        });

        this.pickingStockOrderScanForm.get('itemId').valueChanges.subscribe(itemIdValue => {
            if (itemIdValue != '' && this.warehouseId) {
                let params = new HttpParams().set('WarehouseId', this.warehouseId).set('ItemId', itemIdValue)
                this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_IN_BINS, undefined, true, params)
                    .subscribe((response: IApplicationResponse) => {
                        if (response.isSuccess && response.statusCode == 200) {
                            if (response.resources.length > 0) {
                                this.switchBinGetDetails = response.resources[0];
                                this.pickingStockOrderScanForm.get('bincode').patchValue(this.switchBinGetDetails.displayName);
                                // this.pickingStockOrderScanForm.get('locationBinId').patchValue(this.switchBinGetDetails.locationBinId);
                                this.pickingStockOrderScanForm.get('scanBinLocationInput').patchValue(this.switchBinGetDetails.displayName);
                                this.rxjsService.setGlobalLoaderProperty(false);
                            }
                        }
                        else {
                            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                            this.rxjsService.setGlobalLoaderProperty(false);
                        }
                    });
            }
        });

        this.rxjsService.setGlobalLoaderProperty(false);
    }

    getSelectionJobsDetailsById() {

        this.rxjsService.setGlobalLoaderProperty(true);
        // let detailsParams = new HttpParams().set('WareHouseId', this.warehouseId)
        // .set('CreatedUserId', this.userId).set('UserId', this.userId).set('PickerJobId', this.pickerId);

        let detailsParams = new HttpParams().set('CreatedUserId', this.userId).set('UserId', this.userId).set('PickerJobId', this.pickerId);

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.PICKEDJOBS_DETAILS, undefined, true, detailsParams)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.pickerJobDetails = response.resources;
                    this.warehouseId = this.pickerJobDetails.warehouseId;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });

        let scanParams = new HttpParams().set('CreatedUserId', this.userId)
            .set('UserId', this.userId).set('PickerJobId', this.pickerId);
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.PICKEDJOBS_SCAN_ITEM_DETAILS, undefined, true, scanParams)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.receiveGetDetails(response);
                }
                else {
                    this.snackbarService.openSnackbar(response.resources.responseMessage, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }

                // if(!response.resources.isJobCompleted){
                //     this.packerJobScanItemDetails = response.resources;                 
                //     let stockOrderModel = new JobSelectionAddEditModel(response.resources.pickerJobScanItem);
                //     this.pickingStockOrderScanForm.patchValue(stockOrderModel);
                //     this.pickingStockOrderScanForm.get('totalQty').patchValue(response.resources.pickerJobScanItem.orderedQty);
                //     this.rxjsService.setGlobalLoaderProperty(false);  

                //     if(this.packerJobScanItemDetails != null){
                //         this.quantity = this.pickingStockOrderScanForm.get('totalQty').value - this.pickingStockOrderScanForm.get('pickedQty').value
                //         if(this.quantity >= 1){
                //             this.showManager = true;
                //         }
                //         else {
                //             this.showManager = false; 
                //         }
                //     } 
                // }  

                // if(response.resources.isJobCompleted){
                //     this.pickingStockOrderScanForm.get('pickedQty').patchValue(
                //     this.pickingStockOrderScanForm.get('pickedQty').value + 1);
                // }

                // if(response.resources.isJobCompleted && response.resources.isFullyPicked){
                //     this.onModalPopupOpen('fully');
                // }

                // if(response.resources.isJobCompleted && response.resources.isPartiallyPicked){
                //     this.onModalPopupOpen('partially');
                // }

                // if(response.resources.pickerJobScanItem.isNotSerialized) {
                //     this.pickingStockOrderScanForm = setRequiredValidator(this.pickingStockOrderScanForm,["pickedQty"]);  
                // }               
                // this.rxjsService.setGlobalLoaderProperty(false);   

            });

    }

    createpickingStockOrderScanForm(): void {
        let stockOrderModel = new JobSelectionAddEditModel();
        // create form controls dynamically from model class
        this.pickingStockOrderScanForm = this.formBuilder.group({});
        Object.keys(stockOrderModel).forEach((key) => {
            this.pickingStockOrderScanForm.addControl(key, new FormControl(stockOrderModel[key]));
        });
    }

    receivingBarcodeScanItem() {

        this.showDuplicateBarcodeError = false;
        if (this.pickingStockOrderScanForm.get('scanReceivingBarcodeInput').value == '') {
            this.showDuplicateBarcodeError = true;
            this.duplicateBarcodeError = "Serial number is required";
            return;
        }

        let sendParams = new HttpParams().set('CreatedUserId', this.userId).set('UserId', this.userId)
            .set('BarcodeNumber', this.pickingStockOrderScanForm.get('scanReceivingBarcodeInput').value);
        this.rxjsService.setFormChangeDetectionProperty(true);
        let crudService: Observable<IApplicationResponse> =
            this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.PICKEDJOBS_JOB_ASSIGN, undefined, true, sendParams)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                if (response.resources.pickerJobId != null) {
                    this.receivingBarcodeItem = false;
                    this.pickerId = response.resources.pickerJobId
                    this.getSelectionJobsDetailsById();
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.receivingBarcodeItem = true;
                    this.snackbarService.openSnackbar(response.resources.responseMsg, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });

    }

    checkScanItem() {

        this.showDuplicateBinError = false;
        if (this.pickingStockOrderScanForm.get('scanBinLocationInput').value == '') {
            this.showDuplicateBinError = true;
            this.duplicateBinError = "Bin location is required";
            return;
        }

        this.rxjsService.setFormChangeDetectionProperty(true);
        let binParams = new HttpParams().set('binLocation',
            this.pickingStockOrderScanForm.get('scanBinLocationInput').value)
            .set('warehouseId', this.warehouseId);
        let crudService: Observable<IApplicationResponse> =
            this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_IN_BINS_SCAN, undefined, true, binParams)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.scannedBinDetails = response.resources;
                // this.pickingStockOrderScanForm.get('locationBinId').patchValue(response.resources.locationBinId);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

    checkSerialScanItem() {

        this.showDuplicateSerialError = false;
        this.showDuplicatePickerError = false;

        if (this.pickingStockOrderScanForm.invalid) {
            return;
        }

        if (this.pickingStockOrderScanForm.get('scanSerialNumberInput').value == '' &&
            !this.packerJobScanItemDetails.pickerJobScanItem.isNotSerialized) {
            this.showDuplicateSerialError = true;
            this.duplicateSerialError = "Serial number is required";
            return;
        }

        else if (this.scannedBinDetails == undefined) {
            this.snackbarService.openSnackbar('Bin location is not available', ResponseMessageTypes.ERROR);
            return;
        }

        let sendParams = {
            "pickerJobId": this.pickerId,
            "itemId": this.pickingStockOrderScanForm.get('itemId').value,
            "barcode": this.packerJobScanItemDetails.pickerJobScanItem.isNotSerialized ? null
                : this.pickingStockOrderScanForm.get('scanSerialNumberInput').value,
            "serialNumber": this.packerJobScanItemDetails.pickerJobScanItem.isNotSerialized ? null
                : this.pickingStockOrderScanForm.get('scanSerialNumberInput').value,
            "qrCode": null,
            "locationBinId": this.scannedBinDetails.locationBinId,
            "quantity": this.packerJobScanItemDetails.pickerJobScanItem.isNotSerialized ?
                this.pickingStockOrderScanForm.get('pickedQty').value : 1,
            "userId": this.userId

        }

        this.rxjsService.setFormChangeDetectionProperty(true);
        let crudService: Observable<IApplicationResponse> =
            this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PICKEDJOBS_SCAN, sendParams)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200 && response.resources.responseMessage == null) {
                if (response.resources.isFullyPicked || response.resources.isPartialyPicked) {
                    this.receiveGetDetails(response);
                }
                else {
                    this.getSelectionJobsDetailsById();
                }
            }
            else {
                this.snackbarService.openSnackbar(response.resources.responseMessage, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

    receiveGetDetails(response: any) {
        if (response.resources.pickerJobScanItem != null) {
            this.packerJobScanItemDetails = response.resources;
            let stockOrderModel = new JobSelectionAddEditModel(response.resources.pickerJobScanItem);
            this.pickingStockOrderScanForm.patchValue(stockOrderModel);
            this.pickingStockOrderScanForm.get('totalQty').patchValue(response.resources.pickerJobScanItem.orderedQty);
            this.rxjsService.setGlobalLoaderProperty(false);

            if (this.packerJobScanItemDetails != null) {
                this.quantity = this.pickingStockOrderScanForm.get('totalQty').value - this.pickingStockOrderScanForm.get('pickedQty').value
                if (this.quantity >= 1) {
                    this.showManager = true;
                }
                else {
                    this.showManager = false;
                }
            }

            if (response.resources.pickerJobScanItem.isNotSerialized) {
                this.pickingStockOrderScanForm = setRequiredValidator(this.pickingStockOrderScanForm, ["pickedQty"]);
            }
        }

        if (response.resources.isFullyPicked) {
            this.onModalPopupOpen('fully');
        }

        if (response.resources.isPartialyPicked) {
            this.onModalPopupOpen('partially');
        }
    }

    index = 1;
    switchBinsGetMethod() {
        this.rxjsService.setFormChangeDetectionProperty(true);
        if (this.index < this.switchBinGetDetails.length) {
            this.pickingStockOrderScanForm.get('bincode').patchValue(this.switchBinGetDetails[this.index].displayName);
            this.index++;
        }
    }

    // variancePopupModal(){
    //     const dialogVariance = this.dialog.open(JobSelectionScanModalComponent, {
    //         width: '450px',
    //         data: {
    //           header: `Variance Escalation`,
    //           message: `Do you want to create Variance Escalation?`,
    //           buttons: {
    //             cancel: 'No',
    //             create: 'Yes'
    //           }
    //         }, disableClose: true
    //     });
    //     dialogVariance.afterClosed().subscribe(result => {
    //         if(result && typeof result == 'boolean' ) {
    //             this.rxjsService.setDialogOpenProperty(false); 

    //             let variance = {}
    //             variance = { 
    //                 "orderNumber": this.pickerJobDetails.orderNumber,
    //                 "warehouseId": this.warehouseId,
    //                 "locationId": null,
    //                 "varianceInvReqUserId": this.userId,
    //                 "orderTypeId": null,
    //                 "technicianStockOrderCollectionBatchId": null,
    //                 "isExcessiveStock": false,
    //                 "isShortPickedStock": true,
    //                 "quantity": this.quantity,
    //                 "itemId": this.pickingStockOrderScanForm.get('itemId').value,
    //                 "serialNumber": null,
    //                 "referenceId": this.pickerJobDetails.pickerJobId                
    //             } 

    //             this.crudService.create(ModulesBasedApiSuffix.INVENTORY, 
    //             InventoryModuleApiSuffixModels.VARIANCE_ESCLATION_LIST, variance)
    //             .subscribe((response: IApplicationResponse) => {
    //                 if (response.resources && response.statusCode === 200) { 
    //                     this.onVarianceModalPopupOpen(response.resources)
    //                 }
    //                 else {
    //                     this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
    //                     this.rxjsService.setGlobalLoaderProperty(false);     
    //                 }
    //             });  
    //         }                        
    //     });
    // }

    // onVarianceModalPopupOpen(message: string) {
    //     const dialogModal = this.dialog.open(JobSelectionScanModalComponent, {
    //       width: '450px',
    //       data: {
    //         header: `Variance Escalation`,
    //         message: `${this.pickingStockOrderScanForm.get('itemCode').value} - ${this.pickerJobDetails.orderNumber}
    //         short picked stock item. <br> Variance Escalation ${message} has been created </br>`,
    //         buttons: {
    //           create: 'Ok'
    //         }
    //       }, disableClose: true
    //     });
    //     dialogModal.afterClosed().subscribe(result => {
    //         if (!result) {return};
    //         if(result && typeof result == 'boolean'){
    //             this.callNextPickedBatches();
    //         }
    //         this.rxjsService.setDialogOpenProperty(false);
    //     });
    // }

    // callNextPickedBatches(){

    //     let picked = new HttpParams().set('wareHouseId', this.warehouseId)
    //     .set('createdUserId', this.userId);
    //     let crudService: Observable<IApplicationResponse> = 
    //     this.crudService.get(ModulesBasedApiSuffix.INVENTORY, 
    //         InventoryModuleApiSuffixModels.PICKEDJOBS_ASSIGN, undefined, true, picked)
    //     crudService.subscribe((response: IApplicationResponse) => {
    //         if (response.resources && response.statusCode === 200) { 
    //             let pickerId = response.resources.pickerJobId;
    //             this.rxjsService.setGlobalLoaderProperty(false);  
    //             if(pickerId == null){
    //                 this.receivingBarcodeItem = true;
    //             }
    //             else {
    //                 this.receivingBarcodeItem = false;
    //                 this.getSelectionJobsDetailsById();
    //             }
    //         }
    //         else {
    //             this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
    //             this.rxjsService.setGlobalLoaderProperty(false);     
    //         }
    //     });

    // }

    onModalPopupOpen(message: string) {

        let barcode;
        if (this.pickerJobDetails != undefined) {
            barcode = this.pickerJobDetails.receivingBarcode;
        }
        else {
            barcode = '';
        }
        const dialogRemoveStockCode = this.dialog.open(JobSelectionScanModalComponent, {
            width: '450px',
            data: {
                header: `Batch ${message} Packed`,
                message: `Picking Barcode ${barcode} ${message} packed`,
                buttons: {
                    create: 'Ok'
                }
            }, disableClose: true
        });
        dialogRemoveStockCode.afterClosed().subscribe(result => {
            if (!result) return;
            this.onModalPopupNextOpen();
            this.rxjsService.setDialogOpenProperty(false);
        });
    }

    onModalPopupNextOpen() {
        const dialogRemoveStockCode = this.dialog.open(JobSelectionScanModalComponent, {
            width: '450px',
            data: {
                header: `Job Confirmation`,
                message: `Pack Next Picked Batch`,
                buttons: {
                    cancel: 'No',
                    create: 'Yes'
                }
            }, disableClose: true
        });
        dialogRemoveStockCode.afterClosed().subscribe(result => {
            if (result && typeof result == 'boolean') {
                this.receivingBarcodeItem = true;
                this.rxjsService.setDialogOpenProperty(false);
            }
            else if (!result && typeof result == 'boolean') {
                this.navigateToList();
            }
        });
    }

    managerOverridePopup(value: string) {
        if (value == 'Bins') {
            var header = 'Available Bins'
        }
        else {
            header = 'Manager Override'
        }
        const dialogManagerOverride = this.dialog.open(JobSelectionManagerModalComponent, {
            width: '450px',
            data: {
                header: header,
                PackerJobId: this.pickerId,
                ItemId: this.pickingStockOrderScanForm.get('itemId').value,
                Quantity: this.quantity,
                WarehouseId: this.warehouseId,
                LocationBinId: this.pickingStockOrderScanForm.get('locationBinId').value,
                isNotSerialized: this.pickingStockOrderScanForm.get('isNotSerialized').value,
                userId: this.userId,
                type: value
            }, disableClose: true
        });
        dialogManagerOverride.afterClosed().subscribe(result => {
            if (result && typeof result.bool == 'boolean' && this.warehouseId) {

                let params = new HttpParams().set('WarehouseId', this.warehouseId).set('ItemId', this.itemIdValue)
                this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_IN_BINS, undefined, true, params)
                    .subscribe((response: IApplicationResponse) => {
                        if (response.isSuccess && response.statusCode == 200) {
                            if (response.resources.length > 0) {
                                this.switchBinGetDetails = response.resources[0];
                                this.pickingStockOrderScanForm.get('bincode').patchValue(this.switchBinGetDetails.displayName);
                                this.pickingStockOrderScanForm.get('locationBinId').patchValue(this.switchBinGetDetails.locationBinId);
                                this.pickingStockOrderScanForm.get('scanBinLocationInput').patchValue(this.switchBinGetDetails.displayName);
                                this.rxjsService.setGlobalLoaderProperty(false);
                            }
                        }
                        else {
                            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                            this.rxjsService.setGlobalLoaderProperty(false);
                        }
                        this.rxjsService.setGlobalLoaderProperty(false);
                    });
                // this.pickingStockOrderScanForm.get('bincode').patchValue(this.switchBinGetDetails.displayName);
                this.rxjsService.setDialogOpenProperty(false);


                this.getSelectionJobsDetailsById();
                this.pickingStockOrderScanForm.get('bincode').patchValue(this.switchBinGetDetails.displayName);
                this.rxjsService.setDialogOpenProperty(false);
            }
            else if (!result && typeof result.bool == 'boolean') {
                return;
            }
        });
    }

    navigateToList() {
        this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
    }
}