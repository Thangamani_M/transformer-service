import { Component, Inject, OnInit } from '@angular/core'
import { MAT_DIALOG_DATA } from '@angular/material';
import { RxjsService } from '@app/shared';

@Component({
    selector: 'app-job-selection-scan-modal',
    templateUrl: './job-selection-scan-modal.component.html',
})

export class JobSelectionScanModalComponent implements OnInit {

    reasonText = '';
    constructor(
        @Inject(MAT_DIALOG_DATA) public data,
        private rxjsService: RxjsService
    ) { }

    ngOnInit(): void {
        let messageSpan = document.createElement('span');
        messageSpan.innerHTML = this.data['message'];
        document.getElementById('parentContainer').appendChild(messageSpan);
        this.rxjsService.setDialogOpenProperty(true);
    }
    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }

}