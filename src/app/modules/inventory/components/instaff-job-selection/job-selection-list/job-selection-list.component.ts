import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-job-selection-list',
  templateUrl: './job-selection-list.component.html',
  styleUrls: ['./job-selection-list.component.scss']
})

export class JobSelectionListComponent extends PrimeNgTableVariablesModel implements OnInit {
  userData: UserLogin;
  primengTableConfigProperties: any
  permissions = []
  constructor(private store: Store<AppState>, private router: Router, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'Job Selection',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: ' Job selection', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableEditActionBtn: false,
            enableBreadCrumb: true,
            enableExportBtn: false,
            enablePrintBtn: false,
          }
        ]
      }
    }
    this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][INVENTORY_COMPONENT.JOB_SELECTION]
      if (permission) {
        this.permissions = permission
      }
    });
  }

  getSelectionJobsDetailsById(type: string) {

    let packingParams;
    if (type == 'Picking') {
      let isAccessDeined = this.getPermissionByType("Picking", 'Picking - Main Locations', "Edit");
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      packingParams = new HttpParams()
        .set('UserId', this.userData.userId);
    }
    else {
      let typeOfAction = ""
      if (type == "Packing") {
        typeOfAction = "Received Batches - Main Locations"
      } else {
        typeOfAction = "Picked Batches"
      }
      let isAccessDeined = this.getPermissionByType("Packing", typeOfAction, 'Edit');
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
      packingParams = new HttpParams()
        .set('CreatedUserId', this.userData.userId).set('UserId', this.userData.userId);
    }

    let crudService: Observable<IApplicationResponse> =
      type == 'Picking' ? this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.PACKER_JOBS_ASSIGN, undefined, true, packingParams) :
        type == 'Picked' ? this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.PICKEDJOBS_ASSIGN, undefined, true, packingParams) :
          this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.PICKERJOBS_ASSIGN, undefined, true, packingParams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.resources && response.statusCode === 200) {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (type == 'Picked' || type == 'Packing') {
          this.navigateToPackingScanPage(type, response.resources.pickerJobId);
        }
        else {
          if (!response.resources.isSuccess) {
            this.snackbarService.openSnackbar('Picker Job is not Available', ResponseMessageTypes.WARNING);
            return;
          }
          else {
            this.navigateToScanPage(response.resources.packerJobId);
          }
        }
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  getTransferRequestPickingJob(type: string, typeStr?, subType?) {
    let isAccessDeined = this.getPermissionByType(typeStr, subType, 'Edit');
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    // this.router.navigate(['/inventory', 'job-selection', 'transfer-request-picking'], {
    //   queryParams: {
    //     pickerJobId: null,
    //     type: "pack"
    //   }, skipLocationChange: true
    // });
    let params = new HttpParams().set('WareHouseId', this.userData.warehouseId)
      .set('UserId', this.userData.userId);

    let crudService: Observable<IApplicationResponse> =
      type == 'picking' ? this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.TRANSFER_REQUEST_PICKING_JOB_ASSIGN, undefined, true, params) :
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.TRANSFER_REQUEST_PACKING_JOB_ASSIGN, undefined, true, params)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.resources.isSuccess && response.statusCode == 200) {
        this.navigateToTransferRequest(response.resources, type);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.resources.responseMsg, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  cycleCountViewPage() {
    let isAccessDeined = this.getPermissionByType("Stock Count", 'Cycle Count', 'List');
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let isEdit = this.getPermissionByType("Stock Count", 'Cycle Count', 'Edit');
    let isExport = this.getPermissionByType("Stock Count", 'Cycle Count', 'Export');

    this.router.navigate(['/inventory', 'job-selection', 'cycle-count-list'], { queryParams: { isEdit: isEdit ? 0 : 1, isExport: isExport ? false : true }, skipLocationChange: true });
  }

  stockTakeViewPage() {
    let isAccessDeined = this.getPermissionByType("Stock Count", 'Stock Take', 'List');
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let isEdit = this.getPermissionByType("Stock Count", 'Stock Take', 'Edit');
    let isExport = this.getPermissionByType("Stock Count", 'Stock Take', 'Export');
    this.router.navigate(['/inventory', 'job-selection', 'stock-take-list'], { queryParams: { isEdit: isEdit ? 0 : 1, isExport: isExport ? false : true }, skipLocationChange: true });
  }

  auditCycleCountViewPage() {
    let isAccessDeined = this.getPermissionByType("Stock Count", 'Audit Cycle Count', 'List');
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let isEdit = this.getPermissionByType("Stock Count", 'Audit Cycle Count', 'Edit');
    let isExport = this.getPermissionByType("Stock Count", 'Audit Cycle Count', 'Export');
    this.router.navigate(['/inventory', 'job-selection', 'audit-cycle-count-list'], { queryParams: { isEdit: isEdit ? 0 : 1, isExport: isExport ? false : true }, skipLocationChange: true });
  }

  navigateToScanPage(id: any) {
    this.router.navigate(['/inventory', 'job-selection', 'scan'], {
      queryParams: {
        packerJobId: id,
        userId: this.userData.userId,
        warehouseId: this.userData.warehouseId,
      }, skipLocationChange: true
    });
  }

  technicianStockTakeViewPage() {
    let isAccessDeined = this.getPermissionByType("Stock Count", 'Technician Stock Take', 'List');
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let isEdit = this.getPermissionByType("Stock Count", 'Technician Stock Take', 'Edit');
    this.router.navigate(['/inventory', 'job-selection', 'tech-stock-take-list'], { queryParams: { isEdit: isEdit ? 0 : 1 }, skipLocationChange: true });
  }

  newCountViewPage() {
    let isAccessDeined = this.getPermissionByType("Stock Count", 'New Count', 'Edit');
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/inventory', 'job-selection', 'new-count'], { skipLocationChange: true });
  }

  navigateToPackingScanPage(type: string, pickerId: string) {
    if (type == 'Picked') {
      this.router.navigate(['/inventory', 'job-selection', 'picked-batches'], {
        queryParams: {
          pickerId: pickerId,
          userId: this.userData.userId,
          warehouseId: this.userData.warehouseId
        }, skipLocationChange: true
      });
    }
    else {
      this.router.navigate(['/inventory', 'job-selection', 'packing-scan'], {
        queryParams: {
          pickerId: pickerId,
          userId: this.userData.userId,
          warehouseId: this.userData.warehouseId
        }, skipLocationChange: true
      });
    }
  }

  navigateToStockLookup() {
    let isAccessDeined = this.getPermissionByType("Stock Lookup");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/inventory', 'job-selection', 'stock-lookup-list'], { skipLocationChange: true });
  }

  navigateToAssignStockCode() {
    let isAccessDeined = this.getPermissionByType("Assign Bin Location");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/inventory', 'job-selection', 'assign-bin-location'], { skipLocationChange: true });
  }

  navigateToMoveStockCode() {
    let isAccessDeined = this.getPermissionByType("Move");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/inventory', 'job-selection', 'move-stock-code'], { skipLocationChange: true });
  }

  receivingViewPage() {
    let isAccessDeined = this.getPermissionByType("Receiving");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/inventory', 'job-selection', 'receiving-scan'], { skipLocationChange: true });
  }

  techAuditCycleCountViewPage() {
    let isAccessDeined = this.getPermissionByType("Stock Count", 'Technician Stock Take', 'List');
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    let isEdit = this.getPermissionByType("Stock Count", 'Technician Stock Take', 'Edit');
    let isExport = this.getPermissionByType("Stock Count", 'Audit Cycle Count - Technician', 'Export');
    this.router.navigate(['/inventory', 'job-selection', 'tech-audit-cycle-count-list'], { queryParams: { isEdit: isEdit ? 0 : 1, isExport: isExport ? 0 : 1 }, skipLocationChange: true });
  }

  navigateToTransferRequest(res, type) {
    if (type == 'picking') {
      this.router.navigate(['/inventory', 'job-selection', 'transfer-request-picking'], {
        queryParams: {
          pickerJobId: res.pickerJobId,
          type: res.orderType
        }, skipLocationChange: true
      });
    }
    else {
      this.router.navigate(['/inventory', 'job-selection', 'receiving-batches'], {
        queryParams: {
          pickerJobId: res.packerJobId,
          type: res.orderType
        }, skipLocationChange: true
      });
    }

  }
  getPermissionByType(actionTypeMenuName?, feature?, actionName?): boolean {
    let foundObj = this.permissions.find(fSC => fSC.menuName == actionTypeMenuName);
    if (feature && foundObj) {
      let foundPermission = foundObj['subMenu'].find(fSC => fSC.menuName == feature);
      if (actionName && foundPermission) {
        let actionPermission = foundPermission['subMenu'].find(fSC => fSC.menuName == actionName);
        if (actionPermission) { return false; } else { return true; }
      }
      if (foundPermission) { return false; } else { return true; }
    }
    if (foundObj) { return false; } else { return true; }
  }

}
