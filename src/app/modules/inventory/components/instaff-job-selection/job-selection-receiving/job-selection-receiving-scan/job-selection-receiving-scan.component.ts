import { HttpParams } from '@angular/common/http';
import { AfterViewInit, ElementRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog';
import { InventoryModuleApiSuffixModels, OrderTypes } from '@modules/inventory';
import { BarcodePrintComponent } from '@modules/inventory/components/order-receipts/barcode-print';
import { receiptingNewItemAddEditModal, ReceivingNewItemsArrayModal } from '@modules/inventory/models/receipting-new-item.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';
import { DialogService } from 'primeng/api';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-job-selection-receiving-scan',
  templateUrl: './job-selection-receiving-scan.component.html',
  styleUrls: ['./job-selection-receiving-scan.component.scss']
})
export class JobSelectionReceivingScanComponent implements OnInit, AfterViewInit {

  jobSelectionReceivingForm: FormGroup;
  // receivingDropdown: any = [];
  userData: UserLogin;
  selectedIndex: number = 0;
  selectedSubIndex: number = 0;
  receptingItemsDetails: any = {};
  receiptingGetDetails: any = {};
  filteredPONumbers: any = [];
  showPONumberError: boolean = false;
  showReceptingItems: boolean = false;
  showPOBarCodeError: boolean = false;
  scannedBarcodesCount: number = 0;
  getallBarcodesCount: number = 0;
  showAddQuantity: boolean = false;
  showSerialNumberError: boolean = false;
  showStockCodeError: boolean = false;
  stockCodeNewDetails: any = {};
  showQuantityError: boolean = false;
  stockInfoDetailDialog: boolean = false;
  isConfirmationDialog: boolean = false;
  isBatchReceivedDialog: boolean = false;
  batchReceivedMsg: string;
  stockCodeNewItemsDetails: any = [];
  receiptingNewItemsarray: FormArray;
  receivedUserDropdown: any = [];
  isGeneratedSerialNumber: boolean = true;
  receivingType;
  isEntry: boolean = false;
  isNext: boolean = false;
  @ViewChild(SignaturePad, { static: false }) signaturePad: any;
  stockCodes: any = [];
  @ViewChild('divClick', null) divClick: ElementRef;
  public signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 320,
    'canvasHeight': 100
  };
  isDisable: boolean = false;
  receivingDropdown = [
    { id: 1, displayName: 'New' },
    { id: 2, displayName: 'GRV' },
    { id: 3, displayName: 'Faulty' },
    { id: 4, displayName: 'Inter Branch Transfer' }
    // { id: 5, displayName: 'Radio/System Removals' }
  ]

  constructor(
    private router: Router, private rxjsService: RxjsService, private dialogService: DialogService,
    private crudService: CrudService, private httpCancelService: HttpCancelService,
    private snackbarService: SnackbarService, private store: Store<AppState>,
    private formBuilder: FormBuilder, private dialog: MatDialog,
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createReceiptingNewItemForm();
    this.getDropdown();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.isEntry = true;
  }



  createReceiptingNewItemForm(): void {
    let stockOrderModel = new receiptingNewItemAddEditModal();
    // create form controls dynamically from model class
    this.jobSelectionReceivingForm = this.formBuilder.group({
      receiptingNewItemsarray: this.formBuilder.array([])
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.jobSelectionReceivingForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.jobSelectionReceivingForm = setRequiredValidator(this.jobSelectionReceivingForm, ["deliveryNote"]);
    this.valueChanges();
  }

  //Create FormArray controls
  createReceivingItemsarrayModel(interBranchModel?: ReceivingNewItemsArrayModal): FormGroup {
    let interBranchModelData = new ReceivingNewItemsArrayModal(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      formControls[key] = [interBranchModelData[key], (key === '') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getNewScanItemsArray(): FormArray {
    if (!this.jobSelectionReceivingForm) return;
    return this.jobSelectionReceivingForm.get("receiptingNewItemsarray") as FormArray;
  }

  valueChanges() {
    this.jobSelectionReceivingForm.get("receivedQty")
      .valueChanges.subscribe((receivedQty: any) => {
        let scannedBarcodesCount;
        if (this.scannedBarcodesCount)
          scannedBarcodesCount = parseInt(this.scannedBarcodesCount.toString()) + parseInt(receivedQty);

        if (!this.isEntry && parseInt(scannedBarcodesCount) > parseInt(this.getallBarcodesCount.toString()) || parseInt(this.scannedBarcodesCount.toString()) >  parseInt(this.getallBarcodesCount.toString()) || parseInt(receivedQty) > parseInt(this.getallBarcodesCount.toString())) {
          this.isDisable = true;
          this.jobSelectionReceivingForm.get("generateSerialNo").setValue(null);
          this.jobSelectionReceivingForm.get("generateSerialNo").disable();
          this.snackbarService.openSnackbar("All serial numbers scanned for this stock code.", ResponseMessageTypes.WARNING);
          return;
        } else {
          this.isDisable = false;
          this.jobSelectionReceivingForm.get("generateSerialNo").enable();
          // if(receivedQty>0){
          //   this.jobSelectionReceivingForm.get("generateSerialNo").enable();
          //   }else{
          //   this.jobSelectionReceivingForm.get("generateSerialNo").disable();
          //   }
        }
        this.isEntry =false;
      });
  }

  getDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.
      UX_RECEIVING_FROM_TYPES)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.receivedUserDropdown = response.resources;
        }
      });
  }

  /* first */
  receivingTypeSubmit(index: number) {
    let value = this.jobSelectionReceivingForm.get('receivingType').value;
    if(value==1){
      this.router.navigate(["inventory/order-receipts/receipting/new-item-add-edit"],{ queryParams:
        { type: 'create' ,screenType:"job-selection"},
        skipLocationChange: true });
    }else if(value==2 || value==3) {
      this.router.navigate(["inventory/order-receipts/receipting/technician-receiving-goods-return-edit"], { queryParams:
        { orderTypeId: OrderTypes.NEW,
          isActiveNew: true,screenType:"job-selection"
        }, skipLocationChange: true
      });
    }else if(value==4) {
      this.router.navigate(['inventory/order-receipts/receipting/intransit-add-edit'], { queryParams: { orderTypeId: OrderTypes.INTRANSIT,screenType:"job-selection" }, skipLocationChange: true });
    }
    // if (this.jobSelectionReceivingForm.get('receivingType').value == '' ||
    //   this.jobSelectionReceivingForm.get('receivingType').value == null) {
    //   this.jobSelectionReceivingForm.get('receivingType').setValidators([Validators.required]);
    //   this.jobSelectionReceivingForm.get('receivingType').updateValueAndValidity();
    // }
    // else {
    //   this.jobSelectionReceivingForm.get('receivingType').setErrors(null);
    //   this.jobSelectionReceivingForm.get('receivingType').clearValidators();
    //   this.jobSelectionReceivingForm.get('receivingType').markAllAsTouched();
    //   this.jobSelectionReceivingForm.get('receivingType').updateValueAndValidity();
    //   if (this.jobSelectionReceivingForm.get('receivingType').value == 1) {
    //     this.selectedIndex = index + 1;
    //   }
    //   else if (this.jobSelectionReceivingForm.get('receivingType').value == 5) {
    //     this.openRadioSystemRemovals();
    //   }
    // }
  }

  /* Second */
  inputChangePoNumberFilter(text: any) {

    let otherParams = {}
    this.showPONumberError = false;
    if (text.query == null || text.query == '') return;
    otherParams['displayName'] = text.query;
    otherParams['userId'] = this.userData.userId;
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.
      UX_PURCHASEORDER_BY_WEREHOUSE, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.filteredPONumbers = response.resources;
          if (response.resources.length == 0) {
            this.showPONumberError = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  isReceivedQuantity: boolean = false;

  getReceiptingNewOrderItems(obj: any, type: string) {

    let params;
    this.showPONumberError = false;
    this.showPOBarCodeError = false;
    this.rxjsService.setFormChangeDetectionProperty(true);

    if (type == 'poNumber') {
      if (obj.id == null) return;
      else {
        params = new HttpParams().set('PurchaseOrderId', obj.id).set('UserId', this.userData.userId);
      }
    }
    else if (type == 'poBarcode') {
      if (this.jobSelectionReceivingForm.get('poBarcode').value == '' || this.jobSelectionReceivingForm.get('poBarcode').value == null) {
        this.showPOBarCodeError = true;
        return;
      }
      else {
        params = new HttpParams().set('POBarcode', this.jobSelectionReceivingForm.get('poBarcode').value)
          .set('UserId', this.userData.userId);
      }
    }
    else {
      if (this.receiptingGetDetails?.orderReceiptBatchId != undefined) {
        params = new HttpParams().set('POBarcode', this.jobSelectionReceivingForm.get('poBarcode').value)
          .set('UserId', this.userData.userId).set('OrderReceiptBatchId', this.receiptingGetDetails?.orderReceiptBatchId);
      }
      else {
        params = new HttpParams().set('PurchaseOrderId', obj.id).set('UserId', this.userData.userId);
      }
    }

    let crudService: Observable<IApplicationResponse> =
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RECEIVING_NEW_ITEMS, undefined, true, params)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.showReceptingItems = true;
        this.receiptingGetDetails = response.resources;
        this.receptingItemsDetails = response.resources.items;
        this.jobSelectionReceivingForm.patchValue(response.resources);
        this.isReceivedQuantity = response.resources.items.some(x => x.receivedQuantity != 0);
        let poNumbers = {
          displayName: response.resources.poNumber,
          id: response.resources.orderReferenceId,
        }
        this.filteredPONumbers.push(poNumbers);
        this.jobSelectionReceivingForm.get('poNumber').patchValue(poNumbers);
        this.receiptingNewItemsarray = this.getNewScanItemsArray;
        this.receiptingNewItemsarray.push(this.createReceivingItemsarrayModel());
        this.isNext = (response.resources.warningMessage != null) ? true : false;
        if (response.resources.warningMessage != null || response.resources.items.length == 0) {
          response.resources.warningMessage = response.resources.warningMessage ? response.resources.warningMessage : 'All stock was received on the PO.';
          this.snackbarService.openSnackbar(response.resources.warningMessage, ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });

  }

  scanPoNumberSubmit(index: number) {
    if(!this.jobSelectionReceivingForm.value.poBarcode ){
      this.snackbarService.openSnackbar("Please do select PO Number from suggestion list.", ResponseMessageTypes.WARNING);
      return;
    }
    this.showPOBarCodeError = false;
    let poNumber = this.jobSelectionReceivingForm.get('poNumber').value;
    let poBarcode = this.jobSelectionReceivingForm.get('poBarcode').value;

    if (poNumber == '' || poNumber == null || this.showPONumberError) {
      this.showPONumberError = true;
      return;
    }
    // else if(this.jobSelectionReceivingForm.get('poBarcode').value == '' ||
    // this.jobSelectionReceivingForm.get('poBarcode').value == null){
    //   this.showPOBarCodeError = true;
    // }
    else {
      this.showPONumberError = false;
      this.selectedIndex = index + 1;
    }
  }

  /* Third */
  deliveryNoteSubmit(index: number) {
    if (this.jobSelectionReceivingForm.invalid) return;

    this.selectedIndex = index + 1;
    this.stockCodeNewDetails = {};
  }

  /* Four */

  scanStockCodeSubmit(selectedTab) {
    if (this.jobSelectionReceivingForm.invalid) return;
    this.selectedIndex = 5;
  }

  navigateToSerializedFlow(stockCodeval: any, index: number) {

    this.showAddQuantity = false;
    this.receiptingNewItemsarray?.clear();
    this.stockCodeNewDetails = stockCodeval;
    let itemDetails = stockCodeval.itemDetails;
    this.receiptingNewItemsarray = this.getNewScanItemsArray;
    if (itemDetails != null && itemDetails.length > 0) {
      itemDetails.forEach((items) => {
        this.receiptingNewItemsarray.push(this.createReceivingItemsarrayModel(items));
      });
    }
    this.getallBarcodesCount = stockCodeval.receivedQuantity + stockCodeval.outStandingQuantity;
    this.scannedBarcodesCount = stockCodeval.receivedQuantity;
    this.jobSelectionReceivingForm.get('receivedQty').patchValue(stockCodeval.receivedQuantity);
    this.jobSelectionReceivingForm.get('serializedQty').patchValue(stockCodeval.receivedQuantity);
    this.selectedIndex = index + 1;
  }

  /* Five */
  serialCheckedEvent(checked) {
    if (checked)
      this.isGeneratedSerialNumber = false;

    this.showAddQuantity = false;
    checked ? this.showAddQuantity = true : this.showAddQuantity = false;
  }
  changeReceivedQty(value) {
    // if(value>0){
    this.isGeneratedSerialNumber = (value > 0) ? false : true;
    if (this.scannedBarcodesCount >= this.getallBarcodesCount || parseInt(value) > this.getallBarcodesCount) {
      this.snackbarService.openSnackbar("All serial numbers scanned for this stock code.", ResponseMessageTypes.WARNING);
      return;
    }
    // }
    if (this.jobSelectionReceivingForm.get('generateSerialNo').value) {
      this.getStockCount(true);
    }
  }
  printstock(data) {
    let params = new HttpParams().set('ItemId', data.orderItemId);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCKCODEBARCODE_DETAILS, null, null, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.resources != null) {
          this.stockCodes = new Array(response.resources.stockCodeBarcode);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    // this.stockCodes =new Array(data.itemCode);
    setTimeout(() => {
      this.divClick.nativeElement.click();
    }, 800);
  }
  Print(data) {
    // let itemCode = Array.of(data.itemCode)
    let itemCode = new Array(data.itemCode);

    const dialogReff = this.dialog.open(BarcodePrintComponent,
      {
        width: '400px', height: '400px', disableClose: false,
        data: { serialNumbers: itemCode }
      });
  }
  scanSerialNumber() {

    this.showSerialNumberError = false;
    let scanvalue: boolean = false
    this.rxjsService.setFormChangeDetectionProperty(true);

    let scanInput = this.jobSelectionReceivingForm.get('scanSerialNumberInput').value;
    if (scanInput == '' || scanInput == null) {
      this.showSerialNumberError = true;
      return;
    }
    this.receiptingNewItemsarray.value.forEach(e => {
      if (e.serialNumber == scanInput) {
        scanvalue = true;
      }
    });
    this.jobSelectionReceivingForm.get('scanSerialNumberInput').patchValue(null);
    if (scanvalue) {
      this.snackbarService.openSnackbar('Serial number scanned already', ResponseMessageTypes.WARNING);
      return;
    }
    if (this.scannedBarcodesCount >= this.getallBarcodesCount) {
      this.snackbarService.openSnackbar("All serial numbers scanned for this stock code.", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.addNewSerialNumberFun(scanInput)
    }
  }

  isSerialNumberConfirmationDialog: boolean = false;

  getStockCount(isChecked: boolean) {

    if (!isChecked) return
    let qty = this.jobSelectionReceivingForm.get('receivedQty').value;
    if(qty<0){
      this.snackbarService.openSnackbar("Enter a valid quantity in the Received Qty field.", ResponseMessageTypes.WARNING);
      return;
    }
    if (qty == '' || qty == null) {
      this.showQuantityError = true;
      return;
    }
    let countValue = parseInt(qty);
    let scannedValue = this.scannedBarcodesCount + countValue;
    if (countValue == 0) {
      this.snackbarService.openSnackbar("Enter a valid quantity in the Received Qty field.", ResponseMessageTypes.WARNING);
      return;
    }
    if (scannedValue <= this.getallBarcodesCount) {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SERIAL_NUMBER_GENERATE, countValue)
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources != null) {
            this.isGeneratedSerialNumber = true;
            let resp = response.resources;
            for (var i = 0; i < resp.length; i++) {
              this.addNewSerialNumberFun(response.resources[i]);
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    else {
      this.snackbarService.openSnackbar("All serial numbers scanned for this stock code.", ResponseMessageTypes.WARNING);
      // this.isSerialNumberConfirmationDialog = true;
    }
  }

  addNewSerialNumberFun(value) {
    if (value == undefined) return;
    let duplicate = this.stockCodeNewItemsDetails.filter(x => x.serialNumber === value);
    if (duplicate.length > 0) {
      this.snackbarService.openSnackbar("Serial Number '" + value + "' was already added", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.scannedBarcodesCount += 1;
      let newarray = {
        "serialNumber": value,
        "serialNumberChecked": false,
        "orderReceiptItemDetailId": null,
        "orderReceiptItemId": null,
        "barcode": value,
        "isDeleted": false
      }
      this.receiptingNewItemsarray.push(this.createReceivingItemsarrayModel(newarray));
      this.jobSelectionReceivingForm.get('receivedQty').patchValue(this.stockCodeNewItemsDetails.length);
      this.jobSelectionReceivingForm.get('scanSerialNumberInput').patchValue(null);
      this.jobSelectionReceivingForm.get('generateSerialNo').patchValue(false);
    }
  }

  removeScanedBarcode(i: number, type: string) {

    let customText = "Are you sure you want to delete this?"
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: 'Confirmation',
      showHeader: false,
      closable: true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText },
    });
    confirm.onClose.subscribe((resp) => {
      if (resp === false) {
        if (type == 'new') {
          this.receiptingNewItemsarray.controls[i].get('isDeleted').patchValue(true);
          this.scannedBarcodesCount = this.scannedBarcodesCount - 1;
        }
        else {
          this.receiptingNewItemsarray.controls.forEach(control => {
            control.get('isDeleted').setValue(true);
          });
          this.scannedBarcodesCount = 0;
        }
      }
    });
    this.jobSelectionReceivingForm.get('generateSerialNo').setValue(false);
  }

  // onSelectAll(isChecked: boolean) {
  //   this.printSerialNumbers = [];
  //   this.receiptingNewItemsarray.controls.forEach(control => {
  //     control.get('serialNumberChecked').setValue(isChecked);
  //     if(control.get('serialNumberChecked').value && !control.get('isDeleted').value){
  //       this.printSerialNumbers.push(control.get('serialNumber').value);
  //     }
  //   });
  // }

  onSelectAll(isChecked: boolean, i: number) {

    this.printSerialNumbers = [];
    if (i != undefined || i != null) {
      this.receiptingNewItemsarray.controls[i].get('serialNumberChecked').patchValue(isChecked);
    }
    this.receiptingNewItemsarray.controls.forEach(control => {
      if (i == undefined || i == null) {
        control.get('serialNumberChecked').setValue(isChecked);
      }
      if (control.get('serialNumberChecked').value && !control.get('isDeleted').value) {
        this.printSerialNumbers.push(control.get('serialNumber').value);
      }
    });

  }

  onSubmit(index: number) {

    let getValue = this.receiptingNewItemsarray.value.filter(x => x.isDeleted);

    if (!this.stockCodeNewDetails?.isNotSerialized &&
      (this.receiptingNewItemsarray.value.length === getValue.length)) {
      this.snackbarService.openSnackbar('"Atleast one record is required"', ResponseMessageTypes.WARNING);
      return;
    }

    if (this.stockCodeNewDetails?.isNotSerialized) {
      if (this.jobSelectionReceivingForm.get('serializedQty').value <= 0) {
        this.snackbarService.openSnackbar('Invalid quantity', ResponseMessageTypes.WARNING);
        return;
      }
      if (this.jobSelectionReceivingForm.get('serializedQty').value > this.getallBarcodesCount) {
        this.snackbarService.openSnackbar("All serial numbers scanned for this stock code.", ResponseMessageTypes.WARNING);
        return;
      }
    }

    // if(this.stockCodeNewDetails?.isNotSerialized &&
    //   (this.jobSelectionReceivingForm.get('serializedQty').value > this.getallBarcodesCount)){
    //   this.snackbarService.openSnackbar("Enter a valid quantity in the Received quantity", ResponseMessageTypes.WARNING);
    //   return;
    // }

    let sendparams = {
      "OrderReceiptId": this.receiptingGetDetails.orderReceiptId,
      "OrderReferenceId": this.receiptingGetDetails.orderReferenceId,
      "OrderReceiptBatchId": this.receiptingGetDetails.orderReceiptBatchId,
      "DeliveryNote": this.jobSelectionReceivingForm.get('deliveryNote').value,
      "CreatedUserId": this.userData.userId,
      "Item": {
        "OrderReceiptItemId": this.stockCodeNewDetails.orderReceiptItemId,
        "OrderItemId": this.stockCodeNewDetails.orderItemId,
        "Quantity": this.scannedBarcodesCount,
        "ConsumableQuantity": this.stockCodeNewDetails?.isNotSerialized ?
          this.jobSelectionReceivingForm.get('serializedQty').value : 0,
        "IsSerialized": !this.stockCodeNewDetails?.isNotSerialized ? true : false,
        "IsConsumable": this.stockCodeNewDetails?.isNotSerialized ? true : false,
        "ItemDetails": this.receiptingNewItemsarray.value.length > 0 ? this.receiptingNewItemsarray.value : null
      }
    }


    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RECEIVING_NEW_ITEMS, sendparams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.selectedIndex = index - 1;
        this.getReceiptingNewOrderItems(this.jobSelectionReceivingForm.get('poNumber').value, 'getDetails');
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
      }
    });
  }

  openStockCodeModal() {
    this.stockInfoDetailDialog = true;
  }

  /* six */

  selectedPopupIndex: number = 0;

  finalSubmit() {

    if (this.jobSelectionReceivingForm.get('receivedFromTypeId').value == '' ||
      this.jobSelectionReceivingForm.get('receivedFromTypeId').value == null) {
      this.jobSelectionReceivingForm.get('receivedFromTypeId').setValidators([Validators.required]);
      this.jobSelectionReceivingForm.get('receivedFromTypeId').updateValueAndValidity();
      this.jobSelectionReceivingForm.get('receivedFromTypeId').markAsUntouched();
      return;
    }
    else {
      this.jobSelectionReceivingForm.get('receivedFromTypeId').setErrors(null);
      this.jobSelectionReceivingForm.get('receivedFromTypeId').clearValidators();
      this.jobSelectionReceivingForm.get('receivedFromTypeId').markAllAsTouched();
      this.jobSelectionReceivingForm.get('receivedFromTypeId').updateValueAndValidity();
    }

    if (this.jobSelectionReceivingForm.get('comments').value == '' ||
      this.jobSelectionReceivingForm.get('comments').value == null) {
      this.jobSelectionReceivingForm.get('comments').setValidators([Validators.required]);
      this.jobSelectionReceivingForm.get('comments').updateValueAndValidity();
      this.jobSelectionReceivingForm.get('comments').markAsUntouched();
      return;
    }
    else {
      this.jobSelectionReceivingForm.get('comments').setErrors(null);
      this.jobSelectionReceivingForm.get('comments').clearValidators();
      this.jobSelectionReceivingForm.get('comments').markAllAsTouched();
      this.jobSelectionReceivingForm.get('comments').updateValueAndValidity();
    }

    if (this.signaturePad.signaturePad._data.length == 0) {
      this.snackbarService.openSnackbar("Please Enter the signature", ResponseMessageTypes.WARNING);
      return;
    }

    let verifyDetails = {
      "PONumber": this.receiptingGetDetails?.poNumber,
      "OrderReceiptId": this.receiptingGetDetails.orderReceiptId,
      "OrderReceiptBatchId": this.receiptingGetDetails.orderReceiptBatchId,
      "DocumentType": "Supplier Invoice",
      "Comments": this.jobSelectionReceivingForm.get('comments').value,
      "OrderTypeId": 11,
      "ReceivedFromTypeId": this.jobSelectionReceivingForm.get('receivedFromTypeId').value,
      "deliveryNote": this.jobSelectionReceivingForm?.get('deliveryNote')?.value,
      "CreatedUserId": this.userData.userId
    }

    const formData = new FormData();
    formData.append("receivingDetails", JSON.stringify(verifyDetails));
    if (this.signaturePad.signaturePad._data.length > 0) {
      let imageName = this.receiptingGetDetails.receivingId + ".jpeg";
      const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
      const imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' });
      formData.append('document_files[]', imageFile);
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RECEIVING_NEW_SUBMIT, formData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.printSerialNumbers = [];
        this.printSerialNumbers = new Array(this.receiptingGetDetails?.receivingBarcode);
        this.batchReceivedMsg = response.message;
        this.isBatchReceivedDialog = true;
        this.selectedPopupIndex = 0;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
      }
    });

  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  openPrintSuccesDialog() {
    this.selectedPopupIndex = 1;
    this.batchReceivedMsg = `Do you want to print Batch Barcode ${this.receiptingGetDetails['receivingBarcode']}?`;
  }

  jobConfirmation() {
    this.isBatchReceivedDialog = false;
    this.isConfirmationDialog = true;
  }

  receiveNextOrder() {
    this.selectedIndex = 1;
    this.isConfirmationDialog = false;
    this.jobSelectionReceivingForm.reset();
  }

  printSerialNumbers: any = [];

  print() {
    this.printSerialNumbers = [];
    this.receiptingNewItemsarray.value.forEach(prints => {
      if (prints.serialNumberChecked && !prints.isDeleted) {
        this.printSerialNumbers.push(prints.serialNumber);
      }
    });
    if (this.printSerialNumbers.length == 0) {
      this.snackbarService.openSnackbar("Select at least one Serial Number to print", ResponseMessageTypes.WARNING);
      return;
    }

    // const dialogReff = this.dialog.open(BarcodePrintComponent,
    // { width: '400px', height: '400px', disableClose: true,
    //   data: { serialNumbers: this.printSerialNumbers }
    // });
  }

  dialogClose() { }

  drawComplete() { }

  clearPad() {
    this.signaturePad.clear();
  }

  ngAfterViewInit() {
    if (this.signaturePad == undefined) return;
    this.signaturePad.set('canvasWidth', 1);
    this.signaturePad.resizeCanvas();
    this.signaturePad.clear();
  }

  navigateToPreviousPage(index: number) {
    if (index == 5) {
      this.selectedIndex = 3;
    }
    else {
      this.selectedIndex = index - 1;
      this.receiptingNewItemsarray.clear();
    }
  }

  navigateToList() {
    this.isConfirmationDialog = false;
    this.router.navigate(['/inventory', 'job-selection'], {
      skipLocationChange: true
    });
  }

  openRadioSystemRemovals() {
    this.router.navigate(['/inventory', 'job-selection', 'radio-system-removals-receiving'], { skipLocationChange: true });
  }

}

