import { HttpParams } from '@angular/common/http';
import { ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { JobSelectionStockTakeAddEditModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { JobSelectionCycleCountsModalComponent } from './job-selection-cycle-counts-modal.component';

@Component({
    selector: 'app-job-selection-cycle-counts-scan',
    templateUrl: './job-selection-cycle-counts-scan.component.html',
    styleUrls: ['./job-selection-cycle-counts.component.scss']
})

export class JobSelectionCycleCountsScanComponent {

    cycleCountId: string = '';
    iterationId: string = '';
    cycleCountDetails: any = {};
    warehouseStockTakeBinLocationDetails: any = {};
    jobSelectionStockTakeScanForm: FormGroup;
    storageLocationDropDown: any = [];
    storageLocationId: string = '';
    selectedIndex: number = 0;
    userData: UserLogin;

    constructor(
        private rjxService: RxjsService, private rxjsService: RxjsService,
        private crudService: CrudService, private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder, private router: Router, private dialog: MatDialog,
        private snackbarService: SnackbarService,
        private changeDetectorRef: ChangeDetectorRef, private store: Store<AppState>,
    ) {
        this.cycleCountId = this.activatedRoute.snapshot.queryParams.id;
        this.iterationId = this.activatedRoute.snapshot.queryParams.iterationId;
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
    }

    ngOnInit(): void {
        this.getLocationDropdown();
        this.createJobSelectionStockTakeScanForm();
        if (this.cycleCountId && this.iterationId) {
            this.getDetailsById();
            // this.getStockTakeDetailsById();
        }
    }

    createJobSelectionStockTakeScanForm(): void {
        let stockOrderModel = new JobSelectionStockTakeAddEditModel();
        // create form controls dynamically from model class
        this.jobSelectionStockTakeScanForm = this.formBuilder.group({});
        Object.keys(stockOrderModel).forEach((key) => {
            this.jobSelectionStockTakeScanForm.addControl(key, new FormControl(stockOrderModel[key]));
        });
    }

    getLocationDropdown() {
        let dropParams = new HttpParams().set('CycleCountId', this.cycleCountId)
            .set('CycleCountIterationId', this.iterationId).set('CreatedUserId', this.userData.userId);
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_AVAILABLE_ROWS, undefined, true, dropParams)

            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.storageLocationDropDown = response.resources;
                    this.getStockTakeDetailsById();
                    this.rjxService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rjxService.setGlobalLoaderProperty(false);
                }
            });
    }

    getDetailsById() {

        let params = new HttpParams().set('CycleCountId', this.cycleCountId)
            .set('CycleCountIterationId', this.iterationId)
            .set('createdUserId', this.userData.userId)
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_DETAILS, undefined, true, params)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.cycleCountDetails = response.resources;
                    this.rjxService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rjxService.setGlobalLoaderProperty(false);
                }
            });
    }

    getStockTakeDetailsById() {

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_INPROGRESS_ITEMS,
            undefined, true, prepareRequiredHttpParams({
                CycleCountId: this.cycleCountId, CycleCountIterationId: this.iterationId, createdUserId: this.userData.userId, locationId: this.jobSelectionStockTakeScanForm.get('locationId').value
            })).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200 && response.resources != null) {
                    if (response.resources.locationId != null) {
                        if (response.resources.itemId != null) {
                            this.selectedIndex = 3;
                            this.warehouseStockTakeBinLocationDetails = response.resources;
                            this.jobSelectionStockTakeScanForm.patchValue(response.resources);
                            if (response.resources.storageLocationName.includes('Main Location')) {
                                this.mainLocationWarehouse = true;
                            }
                            else {
                                this.mainLocationWarehouse = false;
                            }
                        }
                        if (response.resources.itemId == null) {
                            this.selectedIndex = 1;
                            this.jobSelectionStockTakeScanForm.get('locationId').patchValue(response.resources.locationId);
                            this.onStorageLocation(response.resources.locationId);
                        }
                    }
                    if (response.resources.locationId == null) {
                        this.selectedIndex = 1;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.selectedIndex = 1;
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rjxService.setGlobalLoaderProperty(false);
                }
            });
    }

    mainLocationWarehouse: boolean = false;

    onStorageLocation(locationIds) {

        if (locationIds != '' && locationIds != null) {
            let storage = this.storageLocationDropDown.find(x => x.id == locationIds);
            if (storage.displayName.includes('Main Location')) {
                this.mainLocationWarehouse = true;
            }
            else {
                this.mainLocationWarehouse = false;
            }
            this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_ITEM_DETAIL,
                undefined, true, prepareRequiredHttpParams({
                    CycleCountId: this.cycleCountId, CycleCountIterationId: this.iterationId,
                    createdUserId: this.userData.userId, LocationRowId: locationIds
                })).subscribe((response: IApplicationResponse) => {
                    if (response.isSuccess && response.statusCode == 200 && response.resources != null) {
                        this.selectedIndex = 2;
                        this.storageLocationId = locationIds;
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                    else {
                        this.selectedIndex = 1;
                        this.jobSelectionStockTakeScanForm.get('binLocation').patchValue('');
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                });

        }
    }

    showBinLocationError: boolean = false;

    scanBinlocations(type: string) {

        if (type == 'scan' && this.jobSelectionStockTakeScanForm.get('binLocation').value == '') {
            this.showBinLocationError = true;
            return;
        }
        let params;
        if (this.mainLocationWarehouse) {
            params = {
                CycleCountId: this.cycleCountId,
                CycleCountIterationId: this.iterationId,
                locationRowId: this.jobSelectionStockTakeScanForm.get('locationId').value,
                createdUserId: this.userData.userId,
                binDetails: this.mainLocationWarehouse ? this.jobSelectionStockTakeScanForm.get('binLocation').value : null,

            }
        } else {
            params = {
                CycleCountId: this.cycleCountId,
                CycleCountIterationId: this.iterationId,
                locationRowId: this.jobSelectionStockTakeScanForm.get('locationId').value,
                createdUserId: this.userData.userId,
                BinDetails: !this.mainLocationWarehouse ? this.jobSelectionStockTakeScanForm.get('binLocation').value : null
            }
        }

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_ITEM,
            undefined, true, prepareRequiredHttpParams(params)).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    if (response.resources.isSuccess) {
                        this.selectedIndex = 3;
                        this.warehouseStockTakeBinLocationDetails = response.resources.itemDetail;
                        this.jobSelectionStockTakeScanForm.patchValue(response.resources.itemDetail);
                        this.rjxService.setGlobalLoaderProperty(false);
                    }
                    else {
                        this.snackbarService.openSnackbar(response.resources.responseMsg, ResponseMessageTypes.ERROR);
                        this.rjxService.setGlobalLoaderProperty(false);
                    }
                    this.rjxService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rjxService.setGlobalLoaderProperty(false);
                }
                this.rjxService.setGlobalLoaderProperty(false);
            });
    }

    showSerialNumbersError: boolean = false;
    showQuantityError: boolean = false;

    scanSerialNumber() {
        this.showQuantityError = false;
        if (!this.warehouseStockTakeBinLocationDetails?.isNotSerialized &&
            this.jobSelectionStockTakeScanForm.get('serialNumbers').value == '') {
            this.showSerialNumbersError = true;
            return;
        }

        if (this.warehouseStockTakeBinLocationDetails?.isNotSerialized && (this.jobSelectionStockTakeScanForm.get('countedQty').value == '' ||
            this.jobSelectionStockTakeScanForm.get('countedQty').value <= 0)) {
            this.showQuantityError = true;
            return;
        }
        let params = {
            cycleCountId: this.cycleCountId,
            cycleCounItemId: null,
            CycleCountIterationId: this.iterationId,
            locationBinId: this.warehouseStockTakeBinLocationDetails?.locationBinId,
            itemId: this.warehouseStockTakeBinLocationDetails?.itemId,
            serialNumber: !this.warehouseStockTakeBinLocationDetails?.isNotSerialized ?
                this.jobSelectionStockTakeScanForm.get('serialNumbers').value : null,
            availableQty: this.jobSelectionStockTakeScanForm.get('countedQty').value,
            isNotSerialized: this.warehouseStockTakeBinLocationDetails?.isNotSerialized,
            StockCodeBarcode: this.warehouseStockTakeBinLocationDetails?.itemCode,
            BinDetails: this.warehouseStockTakeBinLocationDetails?.binDetails,
            createdUserId: this.userData.userId
        }

        this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_SCAN_ITEMS, params)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                    this.scanBinlocations('get');
                    this.jobSelectionStockTakeScanForm.get('serialNumbers').patchValue('');
                    this.rjxService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rjxService.setGlobalLoaderProperty(false);
                }
            });
    }

    isShowMultipleBinLocations: boolean = false;
    getBinLocations: any = [];
    binLocationsPopupBtn: boolean = false;

    binLocationsPopup(type: string) {

        this.binLocationsPopupBtn = type == "submit" ? true : false;

        // if(this.mainLocationWarehouse && this.warehouseStockTakeBinLocationDetails?.itemId){
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_ITEMS_BIN_DETAILS,
            undefined, true, prepareRequiredHttpParams({
                cycleCountId: this.cycleCountId, CycleCountIterationId: this.iterationId,
                locationId: this.jobSelectionStockTakeScanForm.get('locationId').value, createdUserId: this.userData.userId,
                itemId: this.warehouseStockTakeBinLocationDetails?.itemId
            })).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    if (response.resources.length > 0) {
                        this.isShowMultipleBinLocations = true;
                        this.getBinLocations = response.resources
                    }
                    else {
                        this.saveMultipleLocations();
                    }
                    this.rjxService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rjxService.setGlobalLoaderProperty(false);
                }
            });
        // }
        // else{
        // this.saveMultipleLocations();
        // }
    }


    getNewBinlocations(obj: any) {

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_ITEM,
            undefined, true, prepareRequiredHttpParams({
                cycleCountId: this.cycleCountId, CycleCountIterationId: this.iterationId,
                locationId: this.jobSelectionStockTakeScanForm.get('locationId').value, createdUserId: this.userData.userId,
                binDetails: obj?.binDetails
            })).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    if (response.resources.isSuccess) {
                        this.selectedIndex = 3;
                        this.isShowMultipleBinLocations = false;
                        this.warehouseStockTakeBinLocationDetails = response.resources.itemDetail;
                        this.jobSelectionStockTakeScanForm.patchValue(response.resources.itemDetail);
                        this.rjxService.setGlobalLoaderProperty(false);
                    }
                    else {
                        this.snackbarService.openSnackbar(response.resources.responseMsg, ResponseMessageTypes.ERROR);
                        this.rjxService.setGlobalLoaderProperty(false);
                    }
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rjxService.setGlobalLoaderProperty(false);
                }
            });
    }

    showStockBarcodeError: boolean = false;

    openSerialInfoPopup() {

        let params = new HttpParams().set('CycleCountId', this.cycleCountId)
            .set('CycleCountIterationId', this.iterationId).set('locationId', this.jobSelectionStockTakeScanForm.get('locationId').value)
            .set('locationBinId', this.warehouseStockTakeBinLocationDetails?.locationBinId).set('itemid', this.warehouseStockTakeBinLocationDetails?.itemId)
            .set('createdUserId', this.userData.userId)
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_STOCK_SCANNED_SERIAL_NUMBER, undefined, true, params)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200 && response.resources) {
                    let serials = response.resources?.serialNumber != null ? response.resources.serialNumber.split(',') : [];
                    let modalDetails = {
                        stockCode: response.resources.itemCode,
                        stockDescription: response.resources.displayName,
                        serialNumbers: serials
                    }

                    this.rxjsService.setGlobalLoaderProperty(false);
                    const dialogRemoveStockCode = this.dialog.open(JobSelectionCycleCountsModalComponent, {
                        width: '500px',
                        data: {
                            serialNumberDetails: modalDetails
                        }, disableClose: true
                    });
                    dialogRemoveStockCode.afterClosed().subscribe(result => {
                        if (!result) return;
                        this.rxjsService.setDialogOpenProperty(false);
                    });
                    this.rxjsService.setDialogOpenProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    showApproveRequestModal: boolean = false;

    saveMultipleLocations() {

        let params = {
            CycleCountId: this.cycleCountId,
            CycleCountIterationId: this.iterationId,
            locationId: this.jobSelectionStockTakeScanForm.get('locationId').value,
            itemId: this.warehouseStockTakeBinLocationDetails?.itemId,
            createdUserId: this.userData.userId
        }

        this.isShowMultipleBinLocations = false;

        const submit$ = this.binLocationsPopupBtn ? this.crudService.create(
            ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_TAKES_ITERATION_PROCESS,
            params) : this.crudService.update(ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.CYCLE_COUNT_NEXT_ITEMS_DETAILS, params);
        submit$.pipe(tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
        })).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
                if (this.binLocationsPopupBtn) {
                    this.selectedIndex == 1;
                    this.showApproveRequestModal = true;
                }
                else {
                    this.selectedIndex = 2;
                    this.jobSelectionStockTakeScanForm.get('binLocation').patchValue('');
                    this.onStorageLocation(this.jobSelectionStockTakeScanForm.get('locationId').value);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

    cancelPopupModal() {
        this.isShowMultipleBinLocations = false;
        // this.selectedIndex = 0;
        // this.jobSelectionStockTakeScanForm.get('locationId').patchValue('');
    }

    ngAfterViewChecked() {
        this.changeDetectorRef.detectChanges();
    }

    navigateToList() {
        this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
    }

    navigateToStockTakeList() {
        // if(this.selectedIndex == 1){
        this.router.navigate(['/inventory', 'job-selection', 'cycle-count-list'], { skipLocationChange: true });
        // }
        // else {
        //     this.selectedIndex = 1;
        //     this.jobSelectionStockTakeScanForm.get('locationId').patchValue('');
        // }
    }
}
