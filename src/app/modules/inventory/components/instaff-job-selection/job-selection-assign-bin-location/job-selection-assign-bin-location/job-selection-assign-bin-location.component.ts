import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-job-selection-assign-bin-location',
  templateUrl: './job-selection-assign-bin-location.component.html'
})
export class JobSelectionAssignBinLocationComponent implements OnInit {

  jobSelectionAssignBinLocationForm: FormGroup;
  selectedIndex: number = 0;
  wareHouse: boolean =true;
  showItemCodeError: boolean = false;
  showStockCodeError: boolean = false;
  userData: UserLogin;
  assignStockCodeDetails: any = {};
  isConfirmationDialog: boolean = false;
  confirmationMsg: string;
  warehouseDropDownList=[];
  wareHouseId;
  constructor(private snackbarService: SnackbarService,
    private store: Store<AppState>, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService,
  ){
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createForm();
    this.getWarehouseDropdown();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createForm(){
    this.jobSelectionAssignBinLocationForm = new FormGroup({
      'binLocationCode': new FormControl(null),
      'stockCode': new FormControl(null),
    });
    // this.receiptingNewVerifyForm = setRequiredValidator(this.receiptingNewVerifyForm, ['receivedFromTypeId', 'comments']);
  }
  getWarehouseDropdown() {
    //IsAll=true
    this.crudService.dropdown(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_WAREHOUSE,
       prepareRequiredHttpParams({userId:this.userData.userId})
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
       this.warehouseDropDownList = data.resources;
      }
    });
  }
  warehouseChange(value){
    this.wareHouseId = value;
  }
  getAssignWareHouse(){
    this.wareHouse = false;
  }
  stockCodeValue: string;

  getAssignStockCode(index: number){

    this.showItemCodeError = false; let params;
    this.rxjsService.setFormChangeDetectionProperty(true);
    let binLocationCode = this.jobSelectionAssignBinLocationForm.get('binLocationCode').value;
    let stockCode = this.jobSelectionAssignBinLocationForm.get('stockCode').value;
    if(index == 0){
      if (binLocationCode == '' || binLocationCode == null) {
        this.showItemCodeError = true;
        return;
      }
      params = new HttpParams().set('BinLocation', binLocationCode)
      .set('WarehouseId', this.wareHouseId)
    }
    else {
      if (stockCode == '' || stockCode == null) {
        this.showStockCodeError = true;
        return;
      }
      if(this.stockCodeValue != null){
        this.snackbarService.openSnackbar('The selected Bin Location is already linked to another Stock Code', ResponseMessageTypes.WARNING);
        return;
      }
      params = {
        "itemId": this.assignStockCodeDetails.stockId,
        "itemCode": stockCode,
        "createdUserId": this.userData.userId,
        "locationBinIds": new Array(this.assignStockCodeDetails.binLocationId)
      }
    }

    let crudService: Observable<IApplicationResponse> =
    index == 0 ? this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.BIN_LOCATION_VALIDATE, undefined, true, params) :
    this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_IN_BINS_LINK, params)

    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setGlobalLoaderProperty(false);
        if(index == 0){
          this.assignStockCodeDetails = response.resources;
          let binLocationCodeTemp = response.resources.binLocationCode.replaceAll('-', '')
          if((response.resources.binLocationCode.toUpperCase() == binLocationCode.toUpperCase() ) || (binLocationCodeTemp.toUpperCase() == binLocationCode.toUpperCase())){
            this.selectedIndex = index + 1;
            this.stockCodeValue = response.resources.stockCode;
            this.jobSelectionAssignBinLocationForm.get('stockCode').patchValue(response.resources.stockCode);
          }
        }
        else {
          this.confirmationMsg = response.resources.message;
          this.isConfirmationDialog = true;
        }
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigateToPreviousPage(index){
    this.selectedIndex = index - 1;
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'job-selection'], {
      skipLocationChange: true
    });
  }

}
