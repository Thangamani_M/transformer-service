import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { JobSelectionAddEditModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { JobSelectionManagerModalComponent, JobSelectionScanModalComponent } from '../..';

@Component({
  selector: 'app-jo-selection-transfer-request-scan',
  templateUrl: './jo-selection-transfer-request-scan.component.html'
})
export class JoSelectionTransferRequestScanComponent implements OnInit {

  pickerJobDetails : any;
  packerJobScanItemDetails: any;
  pickerJobId: string = '';
  orderType: string = '';
  userData: UserLogin;
  pickingStockOrderScanForm : FormGroup;
  showManager:boolean;
  quantity: any;
  duplicateError:any;
  showDuplicateError : boolean = false;
  isPrintSuccessDialog: boolean = false;
  printMessages: any;
  printActionBtn: boolean = false;

  constructor(
      private store: Store<AppState>,private router: Router,
      private crudService: CrudService, private formBuilder: FormBuilder,
      private snackbarService: SnackbarService,private activatedRoute: ActivatedRoute,
      private rxjsService: RxjsService,private dialog: MatDialog,
  ) {
      this.pickerJobId = this.activatedRoute.snapshot.queryParams.pickerJobId;
      this.orderType = this.activatedRoute.snapshot.queryParams.type;
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      });
  }

  ngOnInit(){
    this.getSelectionJobsDetailsById();
    this.createpickingStockOrderScanForm();

    this.pickingStockOrderScanForm.get('pickedQty').valueChanges.subscribe( pickedQty =>{
      this.quantity = this.pickingStockOrderScanForm.get('totalQty').value - pickedQty
      if(this.quantity >= 1){
        this.showManager = true;
      }
      else{
        this.showManager = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getSelectionJobsDetailsById(){

    let params = new HttpParams().set('PickerJobId', this.pickerJobId)
    .set('OrderType', this.orderType).set('UserId', this.userData.userId);

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
    InventoryModuleApiSuffixModels.TRANSFER_REQUEST_PICKING_JOB_DETAILS, undefined, true, params)
    .subscribe((response: IApplicationResponse) => {
      if (response.resources && response.statusCode === 200) {
        this.pickerJobDetails = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
    InventoryModuleApiSuffixModels.TRANSFER_REQUEST_PICKING_JOB_ITEM_DETAILS, undefined, true, params)
    .subscribe((response: IApplicationResponse) => {
      if (response.resources && response.statusCode === 200) {
        if(response.resources.itemDetail != null) {
          this.packerJobScanItemDetails = response.resources.itemDetail;
          let stockOrderModel = new JobSelectionAddEditModel(response.resources.itemDetail);
          this.pickingStockOrderScanForm.patchValue(stockOrderModel);
          this.pickingStockOrderScanForm.get('bincode').patchValue(response.resources.itemDetail.binDetails);
          this.pickingStockOrderScanForm.get('totalQty').patchValue(response.resources.itemDetail.orderQty);
          this.pickingStockOrderScanForm.get('scanSerialNumberInput').patchValue('');

          this.quantity = this.pickingStockOrderScanForm.get('totalQty').value - this.pickingStockOrderScanForm.get('pickedQty').value

          if(this.quantity >= 1){
            this.showManager = true;
          }
          else {
            this.showManager = false;
          }
        }

        if(response.resources.headder.isFullyPicked){
          this.onModalPopupOpen('fully');
        }

        if(response.resources.headder.isPartialyPicked){
          this.onModalPopupOpen('partially');
        }

        if(response?.resources?.itemDetail?.isNotSerialized) {
          this.pickingStockOrderScanForm = setRequiredValidator(this.pickingStockOrderScanForm,["totalQty"]);
        }

        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  createpickingStockOrderScanForm(): void {
    let stockOrderModel = new JobSelectionAddEditModel();
    // create form controls dynamically from model class
    this.pickingStockOrderScanForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.pickingStockOrderScanForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
  }

  checkScanItem(){

    this.showDuplicateError = false;
    if(!this.packerJobScanItemDetails?.isNotSerialized &&
      this.pickingStockOrderScanForm.get('scanSerialNumberInput').value == ''){
      this.showDuplicateError = true;
      this.duplicateError = "Serial number is required";
      return;
    }

    if(this.pickingStockOrderScanForm.invalid){
      return;
    }

    let serialNumber = this.pickingStockOrderScanForm.get('scanSerialNumberInput').value;
    let sendParams = {
      "pickerJobId" : this.pickerJobId,
      "orderType": this.orderType,
      "itemId": this.pickingStockOrderScanForm.get('itemId').value,
      "serialNumber": serialNumber,
      "qty" : !this.packerJobScanItemDetails.isNotSerialized ? 1 :
      this.pickingStockOrderScanForm.get('totalQty').value,
      "userId": this.userData.userId,
      "locationBinId": this.pickingStockOrderScanForm.get('locationBinId').value,
      "locationId": this.pickingStockOrderScanForm.get('locationId').value,
      "isNotSerialized": this.pickingStockOrderScanForm.get('isNotSerialized').value,
    }

    let crudService: Observable<IApplicationResponse> = this.crudService.create(
    ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.TRANSFER_REQUEST_PICKING_JOB_SCAN, sendParams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        if(!response.resources.headder.isSuccess){
          this.snackbarService.openSnackbar(response.resources.headder.exceptionMsg, ResponseMessageTypes.WARNING);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.getSelectionJobsDetailsById();
        }
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  getSelectionNewItemsById(){

    let params = new HttpParams().set('WareHouseId', this.userData.warehouseId)
    .set('UserId', this.userData.userId);

    let crudService: Observable<IApplicationResponse> =
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
    InventoryModuleApiSuffixModels.TRANSFER_REQUEST_PICKING_JOB_ASSIGN, undefined, true, params)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.rxjsService.setGlobalLoaderProperty(false);
        if(!response.resources.isSuccess) {
          this.navigateToList();
          this.snackbarService.openSnackbar('Picker Job is not Available', ResponseMessageTypes.WARNING);
          return;
        }
        else {
          this.pickerJobId = response.resources.pickerJobId;
          this.orderType = response.resources.orderType;
          this.getSelectionJobsDetailsById();
        }
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  onModalPopupOpen(message: string) {

    const dialogRemoveStockCode = this.dialog.open(JobSelectionScanModalComponent, {
      width: '450px',
      data: {
        header: `Confirmation`,
        message: `Order <b>${this.pickerJobDetails['stockOrderNumber']}</b> for <b>${this.pickerJobDetails['receivingLocationCode']}</b> </br>
        ${this.pickerJobDetails['receivingLocationName']} ${message} picked`,
        buttons: {
          create: 'Ok'
        }
      }, disableClose: true
    });

    dialogRemoveStockCode.afterClosed().subscribe(result => {
      if (!result) return;
      this.onModalPopupNextOpen();
      // this.printActionBtn = true;
      // this.isPrintSuccessDialog = true;
      // this.printMessages = 'Batch Barcode 10804037 printed'
      this.rxjsService.setDialogOpenProperty(false);
    });

  }

  onModalPopupNextOpen() {

    // this.isPrintSuccessDialog = false;
    const dialogRemoveStockCode = this.dialog.open(JobSelectionScanModalComponent, {
      width: '450px',
      data: {
        header: `Confirmation`,
        message: `Pick Next Stock Order`,
        buttons: {
          cancel: 'No',
          create: 'Yes'
        }
      }, disableClose: true
    });

    dialogRemoveStockCode.afterClosed().subscribe(result => {
      if(result && typeof result == 'boolean' ) {
        this.getSelectionNewItemsById();
        this.rxjsService.setDialogOpenProperty(false);
      }
      else if(!result && typeof result == 'boolean') {
        this.navigateToList();
      }
    });
  }


  managerOverridePopup(){

    const dialogManagerOverride = this.dialog.open(JobSelectionManagerModalComponent, {
      width: '450px',
      data: {
        header:'Manager Override',
        PackerJobId: this.pickerJobId,
        OrderType: this.orderType,
        ItemId: this.pickingStockOrderScanForm.get('itemId').value,
        Quantity: this.pickingStockOrderScanForm.get('totalQty').value,
        SerialNumber: this.pickingStockOrderScanForm.get('scanSerialNumberInput').value,
        LocationBinId: this.pickingStockOrderScanForm.get('locationBinId').value,
        LocationId: this.pickingStockOrderScanForm.get('locationId').value,
        isNotSerialized: this.pickingStockOrderScanForm.get('isNotSerialized').value,
        type: 'Transfer-request'
      }, disableClose: true
    });

    dialogManagerOverride.afterClosed().subscribe(result => {
      if(result && typeof result.bool == 'boolean' ) {
        this.getSelectionJobsDetailsById();
        this.rxjsService.setDialogOpenProperty(false);
      }
      else if(!result && typeof result.bool == 'boolean') {
        this.navigateToList();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });

  }

  navigateToList(){
    this.router.navigate(['/inventory','job-selection'], {
      queryParams: {}, skipLocationChange:true
    });
  }

}
