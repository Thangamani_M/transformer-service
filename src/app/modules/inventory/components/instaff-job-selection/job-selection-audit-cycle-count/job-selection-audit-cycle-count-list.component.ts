import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType,
  exportList,
  LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams,
  ResponseMessageTypes,
  RxjsService,
  SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';

@Component({
  selector: 'app-job-selection-audit-cycle-count-list',
  templateUrl: './job-selection-audit-cycle-count-list.component.html',
  styleUrls: ['./job-selection-audit-cycle-count.component.scss']
})

export class JobSelectionAuditCycleCountListComponent implements OnInit {

  userData: UserLogin;
  searchForm: FormGroup;
  observableResponse: any;
  dataList: any
  status: any = [];
  selectedRows: string[] = [];
  selectedTabIndex: any = 0;
  totalRecords: any;
  listSubscribtion: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties: any;
  columnFilterForm: FormGroup;
  loading: boolean;
  row: any = {};
  first: any = 0;
  reset: boolean;
  otherParams;
  isEdit = 0;
  isExport;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private changeDetectorRef: ChangeDetectorRef, private snackbarService: SnackbarService
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Audit Cycle Count",
      breadCrumbItems: [{ displayName: 'Job Selection', relativeRouterUrl: '' },
      { displayName: 'Job Selection', relativeRouterUrl: '/inventory/job-selection' },
      { displayName: 'Audit Cycle Count', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Audit Cycle Count',
            dataKey: 'cycleCountId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'auditCycleCountNumber', header: 'Audit Cycle Count ID' },
            { field: 'warehouseName', header: 'Warehouse' },
            { field: 'numberofIterations', header: 'Count' },
            { field: 'actionedDate', header: 'Actioned Date & Time', isDateTime: true },
            { field: 'status', header: 'Status' }],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportBtn: true,
            shouldShowFilterActionBtn: false,
            apiSuffixModel: InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }

    // this.activatedRoute.queryParamMap.subscribe((params) => {
    //   this.isEdit = +params['params']['isEdit']
    // });
    this.isExport = this.activatedRoute.snapshot.queryParams.isExport;
    this.isEdit = this.activatedRoute.snapshot.queryParams.isEdit;
    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  ngOnChanges() {
    this.getRequiredListData();
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
    this.changeDetectorRef.detectChanges();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {

    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = { ...otherParams, UserId: this.userData.userId }
    this.otherParams = otherParams
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels,
      undefined, false,
      prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = data.totalCount;
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
        if (!this.isExport || this.isExport == 'false') {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let pageIndex = this.row && this.row["pageIndex"] ? this.row["pageIndex"] : 0;
        let pageSize = this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
        this.exportList(pageIndex, pageSize);
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams, 0, pageSize, ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_EXPORT, this.crudService, this.rxjsService, 'Audit Cycle Count');
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/inventory/job-selection'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }


  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        if (!this.isEdit) {
          return this.snackbarService.openSnackbar(
            PERMISSION_RESTRICTION_ERROR,
            ResponseMessageTypes.WARNING
          );
        }
        this.router.navigate(['/inventory', 'job-selection', 'audit-cycle-count-view'], {
          queryParams: {
            id: editableObject['auditCycleCountId'],
            iterationId: editableObject['auditCycleCountIterationId'],
            userId: this.userData.userId,
            isEdit: this.isEdit
          }, skipLocationChange: true
        });
        break;
    }
  }

  navigateToJobSelection() {
    this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }

}
