import { HttpParams } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
    HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { JobSelectionAuditCycleCountAddEditModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { JobSelectionStockTakeModalComponent } from '../job-selection-stock-take';

@Component({
    selector: 'app-job-selection-audit-cycle-count-scan',
    templateUrl: './job-selection-audit-cycle-count-scan.component.html',
    styleUrls: ['./job-selection-audit-cycle-count.component.scss']
})
export class JobSelectionAuditCycleCountScanComponent implements OnInit {

    auditCycleCountId: any;
    auditIterationId: any;
    auditType: any;
    auditRefTypeId: any;
    jobSelectionAuditCycleCountScanForm: FormGroup;
    auditCycleCountDetails: any = {};
    userData: UserLogin;
    auditCycleItemsDetails: any
    selectedIndex: number = 0;
    showDuplicateError: boolean = false;
    stockCodeEnable: boolean = false;
    isEdit = 0;
    constructor(
        private rxjsService: RxjsService,
        private crudService: CrudService, private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder, private router: Router, private dialog: MatDialog,
        private httpCancelService: HttpCancelService, private snackbarService: SnackbarService,
        private changeDetectorRef: ChangeDetectorRef, private store: Store<AppState>,
    ) {
        this.auditCycleCountId = this.activatedRoute.snapshot.queryParams.id;
        this.auditIterationId = this.activatedRoute.snapshot.queryParams.iterationId;
        this.auditType = this.activatedRoute.snapshot.queryParams.type;
        this.auditRefTypeId = this.activatedRoute.snapshot.queryParams.refTypeId;
        this.selectedIndex = this.activatedRoute.snapshot.queryParams.tabIndex;
        this.isEdit = this.activatedRoute.snapshot.queryParams.isEdit;

        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
    }

    ngOnInit() {

        this.createAuditCycleCountScanForm();
        this.getDetailsById();
        if (this.auditCycleCountId && this.auditIterationId && this.selectedIndex == 1) {
            this.getInprogressItemDetails();
        }
    }

    createAuditCycleCountScanForm(): void {
        let stockOrderModel = new JobSelectionAuditCycleCountAddEditModel();
        // create form controls dynamically from model class
        this.jobSelectionAuditCycleCountScanForm = this.formBuilder.group({});
        Object.keys(stockOrderModel).forEach((key) => {
            this.jobSelectionAuditCycleCountScanForm.addControl(key, new FormControl(stockOrderModel[key]));
        });
    }

    getInprogressItemDetails() {

        let params = new HttpParams().set('auditCycleCountId', this.auditCycleCountId)
            .set('auditCycleCountIterationId', this.auditIterationId).set('createdUserId', this.userData.userId);
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_INPROGRESS_ITEMS, undefined, true, params)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.auditCycleItemsDetails = response.resources;
                    this.selectedIndex = 1;
                    this.jobSelectionAuditCycleCountScanForm.patchValue(response.resources);
                    this.jobSelectionAuditCycleCountScanForm.get('bincode').patchValue(response.resources.binDetails);
                    if (response.resources.isNotSerialized) {
                        this.jobSelectionAuditCycleCountScanForm = setRequiredValidator(this.jobSelectionAuditCycleCountScanForm, ['countedQty']);
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    isShowStockCodeConfirmModal: boolean = false;

    getDetailsById() {

        let params = new HttpParams().set('auditCycleCountId', this.auditCycleCountId)
            .set('auditCycleCountIterationId', this.auditIterationId);
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_DETAILS, undefined, true, params)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.auditCycleCountDetails = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    getBinSerialResponse(bins: string) {

        this.isShowMultipleBinLocations = false;
        let params = new HttpParams().set('auditCycleCountId', this.auditCycleCountId)
            .set('auditCycleCountIterationId', this.auditIterationId).set('auditCycleCountRefTypeId', this.auditRefTypeId)
            .set('binDetails', bins).set('createdUserId', this.userData.userId);

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_ITEM, undefined, true, params)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    if (response.resources.isSuccess && response.resources.responsemsg == null) {
                        this.auditCycleItemsDetails = response.resources.auditCycleCountJobItemDetails;
                        let itemsDetails = response.resources.auditCycleCountJobItemDetails;
                        this.selectedIndex = 1;
                        this.jobSelectionAuditCycleCountScanForm.patchValue(itemsDetails);
                        this.jobSelectionAuditCycleCountScanForm.get('bincode').patchValue(itemsDetails.binDetails);
                        if (itemsDetails.isNotSerialized) {
                            this.jobSelectionAuditCycleCountScanForm = setRequiredValidator(this.jobSelectionAuditCycleCountScanForm, ['countedQty']);
                        }
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                    else {
                        this.snackbarService.openSnackbar(response.resources.responsemsg, ResponseMessageTypes.ERROR);
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    showBinlocationError: boolean = false;

    scanBinlocationInput() {
        let bins = this.jobSelectionAuditCycleCountScanForm.get('scanBinLocation').value;
        if (bins == null || bins == '') {
            this.showBinlocationError = true;
            return
        }
        else {
            this.getBinSerialResponse(bins);
        }
    }

    scanSerialNumber() {

        this.rxjsService.setFormChangeDetectionProperty(true);
        this.showDuplicateError = false;
        if (this.jobSelectionAuditCycleCountScanForm.get('scanSerialNumberInput').value == '') {
            this.showDuplicateError = true;
            return;
        }

        if (this.auditCycleItemsDetails.isNotSerialized && this.jobSelectionAuditCycleCountScanForm.invalid) {
            return;
        }

        let formData = {};
        formData = {
            "auditCycleCountId": this.auditCycleCountId,
            "auditCycleCountIterationId": this.auditIterationId,
            "auditCycleCountItemId": this.auditCycleItemsDetails.auditCycleCountItemId,
            "auditCycleCountRefTypeId": this.auditRefTypeId,
            "locationBinId": this.auditCycleItemsDetails.locationBinId,
            "itemId": this.auditCycleItemsDetails.itemId,
            "serialNumber": this.jobSelectionAuditCycleCountScanForm.get('scanSerialNumberInput').value,
            "availableQty": !this.auditCycleItemsDetails.isNotSerialized ? 1 :
                this.jobSelectionAuditCycleCountScanForm.get('countedQty').value,
            "isNotSerialized": this.auditCycleItemsDetails.isNotSerialized,
            "createdUserId": this.userData.userId
        }

        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let crudService: Observable<IApplicationResponse> =
            this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_SCAN, formData)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.getBinSerialResponse(this.jobSelectionAuditCycleCountScanForm.get('scanBinLocation').value);
                this.jobSelectionAuditCycleCountScanForm.get('scanSerialNumberInput').reset();
                this.rxjsService.setGlobalLoaderProperty(false);
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

    isShowMultipleBinLocations: boolean = false;
    getBinLocations: any = [];
    binLocationsPopupBtn: boolean = false;

    binLocationsPopup(type: string) {

        this.binLocationsPopupBtn = type == "submit" ? true : false;

        let params;
        if (this.auditCycleItemsDetails != undefined) {
            params = new HttpParams().set('auditCycleCountId', this.auditCycleCountId)
                .set('locationBinId', this.auditCycleItemsDetails.locationBinId)
                .set('auditCycleCountIterationId', this.auditIterationId).set('auditCycleCountRefTypeId', this.auditRefTypeId)
                .set('itemId', this.auditCycleItemsDetails.itemId).set('createdUserId', this.userData.userId);
        }
        else {
            params = new HttpParams().set('auditCycleCountId', this.auditCycleCountId)
                .set('locationBinId', this.auditCycleItemsDetails.locationBinId)
                .set('auditCycleCountIterationId', this.auditIterationId).set('auditCycleCountRefTypeId', this.auditRefTypeId)
                .set('createdUserId', this.userData.userId);
        }

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_ITEMS_BIN_DETAILS, undefined, true, params)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    if (response.resources.length > 0) {
                        this.isShowMultipleBinLocations = true;
                        this.getBinLocations = response.resources
                    }
                    else {
                        if (this.binLocationsPopupBtn) {
                            this.getSelectedCountDetails();
                        }
                        else {
                            this.saveMultipleLocations();
                        }
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }
    closeBinLocationsPopup() {
        this.isShowStockCodeConfirmModal = false;
    }
    randomStockCodeMsg: string = '';

    getSelectedCountDetails() {

        this.isShowStockCodeConfirmModal = false;
        let params = new HttpParams().set('auditCycleCountId', this.auditCycleCountId).set('createdUserId', this.userData.userId)
            .set('auditCycleCountIterationId', this.auditIterationId).set('auditCycleCountRefTypeId', this.auditRefTypeId);
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_STATUS_DETAILS, undefined, true, params)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.isShowStockCodeConfirmModal = true;
                    this.randomStockCodeMsg = response.resources.responsemsg;
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    saveMultipleLocations() {

        let params = {
            auditCycleCountId: this.auditCycleCountId,
            auditCycleCountIterationId: this.auditIterationId,
            auditCycleCountRefTypeId: this.auditRefTypeId,
            itemId: this.auditCycleItemsDetails.itemId,
            createdUserId: this.userData.userId
        }

        this.isShowMultipleBinLocations = false;

        const submit$ = this.binLocationsPopupBtn ? this.crudService.create(
            ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_ITERATION_PROCESS,
            params) : this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_COMPLETE_CURRENT_STOCK, params);
        submit$.pipe(tap(() => {
            this.rxjsService.setGlobalLoaderProperty(false);
        })).subscribe((response) => {
            if (response.isSuccess && response.statusCode == 200) {
                if (this.binLocationsPopupBtn) {
                    this.navigateToAuditCycleCountList();
                }
                else {
                    this.selectedIndex = 0;
                    this.jobSelectionAuditCycleCountScanForm.get('scanBinLocation').patchValue('');
                    this.getDetailsById();
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

    filteredStockCodes: any = [];
    showStockCodeMsg: boolean = false;
    newSerialNumberErroMsg: boolean = false;

    inputChangeStockCodesFilter(text: any, type: string) {

        this.showStockCodeMsg = false;
        this.newSerialNumberErroMsg = false;

        let otherParams = {}
        if (type == 'stockCode' && text == null || text == '') return;
        if (type == 'serial' && this.jobSelectionAuditCycleCountScanForm.get('scanNewSerialNumber').value == '') {
            this.newSerialNumberErroMsg = true;
            return;
        }

        otherParams['auditCycleCountId'] = this.auditCycleCountId;
        type == 'stockCode' ? otherParams['stockCodeBarcode'] = text : otherParams['serialNumber'] = text;
        otherParams['createdUserId'] = this.userData.userId;

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_NEW_ITEMS, null, false,
            prepareGetRequestHttpParams(null, null, otherParams))
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    if (type == 'stockCode') {
                        this.filteredStockCodes = response.resources;
                        if (response.resources.length < 0) {
                            this.showStockCodeMsg = true;
                        }
                    }
                    else {
                        this.addNewStockCode(response.resources.itemId)
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);

                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }


    addNewStockCode(value: any) {


        let otherParams = {}
        if (value == null || value == '') return;
        otherParams['auditCycleCountId'] = this.auditCycleCountId;
        otherParams['auditCycleCountRefTypeId'] = this.auditRefTypeId;
        otherParams['itemId'] = value;
        otherParams['createdUserId'] = this.userData.userId;

        this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_NEW_ITEM_POST, otherParams)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.selectedIndex = 0;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    saveConfirmation() {
        this.selectedIndex = 2;
        this.isShowStockCodeConfirmModal = false;
    }

    ngAfterViewChecked() {
        this.changeDetectorRef.detectChanges();
    }

    cancelPopupModal() {
        this.selectedIndex = 0;
        this.isShowMultipleBinLocations = false;
        this.jobSelectionAuditCycleCountScanForm.get('scanBinLocation').patchValue('');
    }

    isStockInfoDialogModal: boolean = false;
    modalDetails: any = [];

    openSerialInfoPopup() {

        let params = new HttpParams().set('auditCycleCountId', this.auditCycleCountId)
            .set('auditCycleCountIterationId', this.auditIterationId).set('auditCycleCountRefTypeId', this.auditRefTypeId)
            .set('itemId', this.auditCycleItemsDetails.itemId);

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_SERIALNUMBERS, undefined, true, params)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    let serials = response.resources.serialNumber != null ? response.resources.serialNumber.split(',') : [];
                    let modalDetails = {
                        stockCode: response.resources.itemCode,
                        stockDescription: response.resources.displayName,
                        serialNumbers: serials
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                    const dialogRemoveStockCode = this.dialog.open(JobSelectionStockTakeModalComponent, {
                        width: '500px',
                        data: {
                            serialNumberDetails: modalDetails
                        }, disableClose: true
                    });
                    dialogRemoveStockCode.afterClosed().subscribe(result => {
                        if (!result) return;
                        this.rxjsService.setDialogOpenProperty(false);
                    });
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    navigateToJobSelection() {
        this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
    }

    navigateToAuditCycleCountList() {
        this.router.navigate(['/inventory', 'job-selection', 'audit-cycle-count-list'], { queryParams: { isEdit: this.isEdit }, skipLocationChange: true });
    }

    navigateToAuditCycleCountView() {
        this.router.navigate(['/inventory', 'job-selection', 'audit-cycle-count-view'], {
            queryParams: {
                id: this.auditCycleCountId,
                iterationId: this.auditIterationId,
                userId: this.userData.userId,
                isEdit: this.isEdit,
            }, skipLocationChange: true
        });
    }

    navigateToBack() {

        if (this.selectedIndex == 1) {
            this.selectedIndex = 0;
        }
        else {
            this.navigateToAuditCycleCountView();
        }
    }

}