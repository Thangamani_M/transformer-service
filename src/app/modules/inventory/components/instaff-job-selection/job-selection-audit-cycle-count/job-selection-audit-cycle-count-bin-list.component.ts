import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType,  LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  RxjsService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-job-selection-audit-cycle-count-bin-list',
  templateUrl: './job-selection-audit-cycle-count-bin-list.component.html',
  styleUrls: ['./job-selection-audit-cycle-count.component.scss']
})
export class JobSelectionAuditCycleCountBinListComponent implements OnInit {

  @Input() auditCycleCountIds: string;
  @Input() auditCycleCountRefTypeIds: string;
  @Input() auditCycleCountIterationIds: string;

  userData: UserLogin;
  searchForm: FormGroup;
  observableResponse: any;
  dataList: any
  status: any = [];
  selectedRows: string[] = [];
  selectedTabIndex: any = 0;
  totalRecords: any;
  listSubscribtion: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties: any;
  columnFilterForm: FormGroup;
  loading: boolean;
  row: any = {};
  first: any = 0;
  reset: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private changeDetectorRef: ChangeDetectorRef,
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "",
      breadCrumbItems: [{ displayName: '', relativeRouterUrl: '' },
      { displayName: '', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: '',
            dataKey: 'warehouseStockTakeId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [{ field: 'binDetails', header: 'Bin Details', width: '150px' },
            { field: 'itemCode', header: 'Stock Code', width: '150px' },
            { field: 'itemDescription', header: 'Stock Description', width: '150px' },
            { field: 'isLocked', header: 'Status', width: '150px', type: 'toggle-btn' }],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            shouldShowFilterActionBtn: false,
            apiSuffixModel: InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_ITEM_DETAILS_LIST,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }

    this.searchForm = this.formBuilder.group({ searchKeyword: "" });
    this.columnFilterForm = this.formBuilder.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? params['params']['tab'] ? +params['params']['tab'] : 0 : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    // this.getRequiredListData();
  }

  ngOnChanges() {
    if (this.auditCycleCountIds && this.auditCycleCountRefTypeIds && this.auditCycleCountIterationIds)
      this.getRequiredListData();
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
    this.changeDetectorRef.detectChanges();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {

    this.loading = true;
    let params = {
      AuditCycleCountId: this.auditCycleCountIds,
      AuditCycleCountRefTypeId: this.auditCycleCountRefTypeIds,
      AuditCycleCountIterationId: this.auditCycleCountIterationIds, IsAll: true
    }
    otherParams = { ...otherParams, ...params };

    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels,
      undefined, false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = data.totalCount;
      }
    });
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        unknownVar['UserId'] = this.userData.userId;
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/inventory/job-selection'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      // case CrudType.CREATE:
      //   this.router.navigate(['/technical-management','standby-roster','add-edit'], {
      //     queryParams: {}, skipLocationChange:true
      //   });
      // break;
      // case CrudType.VIEW:
      //   this.router.navigate(['/inventory','job-selection','view'], {
      //     queryParams: {
      //       id: editableObject['standbyRosterId'],
      //     }, skipLocationChange:true
      //   });
      // break;
    }
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }

};