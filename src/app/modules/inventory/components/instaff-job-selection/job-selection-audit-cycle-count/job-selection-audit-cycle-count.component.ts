import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes,
  RxjsService, SnackbarService
} from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-job-selection-audit-cycle-count',
  templateUrl: './job-selection-audit-cycle-count.component.html',
  styleUrls: ['./job-selection-audit-cycle-count.component.scss']
})
export class JobSelectionAuditCycleCountComponent implements OnInit {

  auditCycleCountId: any;
  auditIterationId: any;
  auditCycleCountDetails: any = {};
  auditSystem: any;
  auditUserselection: any;
  auditHighRiskItems: any;
  randomSelection: any;
  createdUserId: string = '';
  isEdit = 0;
  constructor(
    private router: Router, private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService,
  ) {
    this.auditCycleCountId = this.activatedRoute.snapshot.queryParams.id;
    this.auditIterationId = this.activatedRoute.snapshot.queryParams.iterationId;
    this.createdUserId = this.activatedRoute.snapshot.queryParams.userId;
    this.isEdit = this.activatedRoute.snapshot.queryParams.isEdit;
  }

  ngOnInit() {

    if (this.auditCycleCountId && this.auditIterationId) {
      this.getInprogressItemDetails();
    }
  }

  getInprogressItemDetails() {

    let params = new HttpParams().set('auditCycleCountId', this.auditCycleCountId)
      .set('auditCycleCountIterationId', this.auditIterationId).set('createdUserId', this.createdUserId);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_INPROGRESS_ITEMS, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources != null) {
            let getDetails = response.resources;
            let value = getDetails.auditCycleCountRefTypeId == 12 ? 'system' : getDetails.auditCycleCountRefTypeId == 11 ? 'user' : 'high';
            this.navigateToScanPage(value, getDetails.auditCycleCountRefTypeId, 1);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.getDetailsById();
          }
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getDetailsById() {
    let params = new HttpParams().set('AuditCycleCountId', this.auditCycleCountId).set('AuditCycleCountIterationId', this.auditIterationId);
    forkJoin([
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_DETAILS, undefined, true, params),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_DETAILS_LIST, undefined, true, params),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.auditCycleCountDetails = resp.resources;
              break;
            case 1:
              if (resp.resources.length > 0) {
                resp.resources.forEach(x => {
                  if (x.auditCycleCountRefTypeId == 10) {
                    this.auditHighRiskItems = x.progresspercentage;
                  }
                  else if (x.auditCycleCountRefTypeId == 11) {
                    this.auditUserselection = x.progresspercentage;
                  }
                  else {
                    this.auditSystem = x.progresspercentage;
                  }
                });
              }
              if (resp.resources.length >= 3) {
                this.randomSelection = (this.auditUserselection + this.auditHighRiskItems) / 2;
              }
              this.rxjsService.setGlobalLoaderProperty(false);
              break;
          }
        }
        else {
          this.snackbarService.openSnackbar(resp.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    });
  }

  navigateToJobSelection() {
    this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
  }

  navigateToAuditCycleCountList() {
    this.router.navigate(['/inventory', 'job-selection', 'audit-cycle-count-list'], { queryParams: { isEdit: this.isEdit, }, skipLocationChange: true });
  }

  navigateToScanPage(value: string, id: number, index: number) {
    this.router.navigate(['/inventory', 'job-selection', 'audit-cycle-count-scan'], {
      queryParams: {
        id: this.auditCycleCountId,
        iterationId: this.auditIterationId,
        type: value,
        refTypeId: id,
        tabIndex: index,
        isEdit: this.isEdit,
      }, skipLocationChange: true
    });
  }

}
