import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { radioRemovalReceivingAddEditModal, RadioSystemRemovalItemsArrayModal, receiptingNewItemsScanModal } from '@modules/inventory/models/receipting-new-item.model';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { HttpParams } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { T } from '@angular/cdk/keycodes';
import { MatDialog, MatRadioChange } from '@angular/material';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog';
import { DialogService } from 'primeng/api';
import { BarcodePrintComponent } from '@modules/inventory/components/order-receipts/barcode-print';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';

@Component({
  selector: 'app-job-selection-radio-system-removals-scan',
  templateUrl: './job-selection-radio-system-removals-scan.component.html'

})
export class JobSelectionRadioSystemRemovalsScanComponent implements OnInit {

  radioRemovalReceivingAddEditForm: FormGroup;
  usersDropDown: any = [];
  userData: UserLogin;
  radioRemovalSystemDetails: any = {};
  radioRemovalReceiptId: string = '';
  radioSystemType: string = '';
  radioRemovalUserId: string = '';
  selectedTabIndex: number = 0;
  showTechLocationDetails: boolean = false;
  getReceivingScanItemDetails: any = {};
  showReceivigScanDetails: boolean = false;
  scannedBarcodesCount: number;
  getallBarcodesCount: number;
  showAddQuantity: boolean = false;
  radioRemovalReceivingItemsArray: FormArray;
  radioRemovalReceivingScanForm: FormGroup;
  showItemCodeError: boolean = false;
  printSerialNumbers: any = [];
  isButtonDisabled: boolean = false;

  @ViewChild(SignaturePad, { static: false }) signaturePad: any;

  public signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 500,
    'canvasHeight': 80
  };

  constructor(
    private crudService: CrudService, private rxjsService: RxjsService, private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder, private store: Store<AppState>, private router: Router,
    private snackbarService: SnackbarService, private dialogService: DialogService, private dialog: MatDialog,
  ) {

    this.radioRemovalReceiptId = this.activatedRoute.snapshot.queryParams.id;
    this.radioRemovalUserId = this.activatedRoute.snapshot.queryParams.radioRemovalUserId;
    this.radioSystemType = this.activatedRoute.snapshot.queryParams.type;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createRadioRemovalReceivingForm();
    this.createRadioRemovalNewItemForm();
    this.getUsersDropdown();
  }

  /* Create Form Controls */
  createRadioRemovalReceivingForm(): void {
    let stockOrderModel = new radioRemovalReceivingAddEditModal();
    // create form controls dynamically from model class
    this.radioRemovalReceivingAddEditForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.radioRemovalReceivingAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.radioRemovalReceivingAddEditForm = setRequiredValidator(this.radioRemovalReceivingAddEditForm, ["radioRemovalUserId"]);
  }

  getUsersDropdown() {
    let params = new HttpParams().set('UserId', this.userData.userId)
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_RADIO_REMOVAL_RECEIVING_USERS, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.usersDropDown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onUserChange(value: string) {
    if (value == '' || value == null) return;
    let params = new HttpParams().set('UserId', this.userData.userId).set('RadioRemovalUserId', value);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING_ITEMS, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.radioRemovalSystemDetails = response.resources;
          this.showTechLocationDetails = true;
          this.radioRemovalReceivingAddEditForm.patchValue(response.resources.basicInfo);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    let userName = this.usersDropDown.filter(x => x.id == this.radioRemovalReceivingAddEditForm.get('radioRemovalUserId').value);
    this.radioRemovalReceivingAddEditForm.get('radioRemovalUser').patchValue(userName[0].displayName)
  }

  // onSubmit(){

  //   let sendparams = {
  //     "RadioRemovalReceiptId": this.radioRemovalSystemDetails?.basicInfo?.radioRemovalReceiptId,
  //     "DeliveryNotes": this.radioRemovalReceivingAddEditForm.get('deliveryNote').value,
  //     "CreatedUserId": this.userData.userId,
  //   }

  //   let crudService: Observable<IApplicationResponse> =
  //   this.crudService.update(ModulesBasedApiSuffix.INVENTORY,
  //     InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING_SAVE_AS_DRAFT, sendparams)
  //   crudService.subscribe((response: IApplicationResponse) => {
  //     if (response.isSuccess && response.statusCode == 200) {
  //       this.radioRemovalReceiptId = response.resources;
  //       this.navigateToList();
  //     }
  //     else {
  //       this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
  //       this.rxjsService.setGlobalLoaderProperty(false);
  //     }
  //   });

  // }

  // delete(){

  //   this.crudService.delete(
  //     ModulesBasedApiSuffix.INVENTORY,
  //     InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING,
  //     '',
  //     prepareRequiredHttpParams({
  //       ids: this.radioRemovalSystemDetails?.basicInfo?.radioRemovalReceiptId,
  //       modifiedUserId: this.userData.userId,
  //     })
  //   ).subscribe((response: IApplicationResponse) => {
  //     if (response.isSuccess && response.statusCode === 200) {
  //       this.onUserChange(null);
  //     }
  //     this.rxjsService.setGlobalLoaderProperty(false);
  //   });
  // }
  /* ---------------------------------------- */

  /* Scan Details */

  createRadioRemovalNewItemForm(): void {
    let stockOrderModel = new receiptingNewItemsScanModal();
    // create form controls dynamically from model class
    this.radioRemovalReceivingScanForm = this.formBuilder.group({
      radioRemovalReceivingItemsArray: this.formBuilder.array([])
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.radioRemovalReceivingScanForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    if (this.getReceivingScanItemDetails?.items?.isNotSerialized) {
      this.radioRemovalReceivingScanForm = setRequiredValidator(this.radioRemovalReceivingScanForm, ["receivedQty"]);
    }
  }

  //Create FormArray controls
  createRadioItemsArrayModel(interBranchModel?: RadioSystemRemovalItemsArrayModal): FormGroup {
    let interBranchModelData = new RadioSystemRemovalItemsArrayModal(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      formControls[key] = [interBranchModelData[key], (key === '') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getNewScanItemsArray(): FormArray {
    if (!this.radioRemovalReceivingScanForm) return;
    return this.radioRemovalReceivingScanForm.get("radioRemovalReceivingItemsArray") as FormArray;
  }

  barcodeScan(itemsArray: any) {

    let receivingDetails = {};
    receivingDetails = {
      radioRemovalUserId: this.radioRemovalReceivingAddEditForm.get('radioRemovalUserId').value,
      type: this.radioSystemType,
      deliveryNotes: this.radioRemovalReceivingAddEditForm.get('deliveryNote').value,
      userId: this.userData.userId,
      basicInfo: this.radioRemovalSystemDetails.basicInfo,
      items: itemsArray,
    }

    this.showReceivigScanDetails = true;
    this.getReceivingScanItemDetails = receivingDetails;
    this.getallBarcodesCount = this.getReceivingScanItemDetails.items.receivedQty +
      this.getReceivingScanItemDetails.items.outStandingQty;
    this.scannedBarcodesCount = this.getReceivingScanItemDetails.items.receivedQty;
    this.serialCheckedEvent(this.getReceivingScanItemDetails?.items?.isSerialNumberAvailable);

    let itemDetails = (this.getReceivingScanItemDetails.items.itemDetails == null ||
      this.getReceivingScanItemDetails.items.itemDetails.length < 0) ? null : this.getReceivingScanItemDetails.items.itemDetails;
    this.radioRemovalReceivingItemsArray = this.getNewScanItemsArray;
    if (this.radioRemovalReceivingItemsArray.value.length > 0) {
      this.radioRemovalReceivingItemsArray.clear();
    }
    if (itemDetails != null && itemDetails.length > 0) {
      itemDetails.forEach((items) => {
        this.radioRemovalReceivingItemsArray.push(this.createRadioItemsArrayModel(items));
      });
    }

    if (this.getallBarcodesCount === this.scannedBarcodesCount) {

    }

  }

  serialCheckedEvent(value) {
    value == "true" ? this.showAddQuantity = true : this.showAddQuantity = false;
  }

  showQuantityError: boolean = false;

  getStockCount() {

    this.showQuantityError = false;
    let qty = this.radioRemovalReceivingScanForm.get('notReceivedQty').value;
    if (qty == '' || qty == null) {
      this.showQuantityError = true;
      return;
    }
    let countValue = parseInt(qty);
    let scannedValue = this.scannedBarcodesCount + countValue;
    if (countValue == 0) {
      this.snackbarService.openSnackbar("Invalid Stock count", ResponseMessageTypes.WARNING);
      return;
    }
    if (scannedValue <= this.getallBarcodesCount) {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SERIAL_NUMBER_GENERATE, countValue)
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources != null) {
            let resp = response.resources;
            for (var i = 0; i < resp.length; i++) {
              this.addNewSerialNumberFun(response.resources[i], null);
            }
          }
          else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
    else {
      this.snackbarService.openSnackbar("All serial numbers scanned for this stock code.", ResponseMessageTypes.WARNING);
    }

  }

  scanSerialNumber() {

    this.showItemCodeError = false;
    let scanvalue: boolean = false
    this.rxjsService.setFormChangeDetectionProperty(true);

    let scanInput = this.radioRemovalReceivingScanForm.get('scanSerialNumberInput').value;
    if (scanInput == '' || scanInput == null) {
      this.showItemCodeError = true;
      return;
    }
    this.radioRemovalReceivingItemsArray.value.forEach(e => {
      if (e.serialNumber == scanInput) {
        scanvalue = true;
      }
    });

    this.radioRemovalReceivingScanForm.get('scanSerialNumberInput').patchValue(null);
    if (scanvalue) {
      this.snackbarService.openSnackbar('Serial number scanned already', ResponseMessageTypes.WARNING);
      return;
    }
    if (this.scannedBarcodesCount >= this.getallBarcodesCount) {
      this.snackbarService.openSnackbar("Enter a valid quantity in the Serial Number field", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      let params = new HttpParams().set('SerialNumber', scanInput)
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING_CUSTOMER_DETAILS, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources != null) {
            this.addNewSerialNumberFun(scanInput, response.resources.customer);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.addNewSerialNumberFun(scanInput, null);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  addNewSerialNumberFun(value: string, customer: any) {

    if (value == undefined) return;
    let duplicate = this.radioRemovalReceivingItemsArray.value.filter(x => x.serialNumber === value);
    if (duplicate.length > 0) {
      this.snackbarService.openSnackbar("Serial Number '" + value + "' was already added", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.scannedBarcodesCount += 1;
      let newarray = {
        "serialNumber": value,
        "serialNumberChecked": false,
        "customer": customer != null ? customer : '-',
        "radioRemovalReceiptItemDetailId": null,
        "radioRemovalReceiptItemId": null,
        "barcode": value,
        "isDeleted": false
      }
      this.radioRemovalReceivingItemsArray.push(this.createRadioItemsArrayModel(newarray));
      this.radioRemovalReceivingScanForm.get('notReceivedQty').patchValue(null);
      this.radioRemovalReceivingScanForm.get('scanSerialNumberInput').patchValue(null);
    }
  }

  removeScanedBarcode(i: number, type: string) {

    let customText = "Are you sure you want to delete this?"
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: 'Confirmation',
      showHeader: false,
      closable: true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText },
    });
    confirm.onClose.subscribe((resp) => {
      if (resp === false) {
        if (type == 'new') {
          this.radioRemovalReceivingItemsArray.controls[i].get('isDeleted').patchValue(true);
          this.scannedBarcodesCount = this.scannedBarcodesCount - 1;
        }
        else {
          this.radioRemovalReceivingItemsArray.controls.forEach(control => {
            control.get('isDeleted').setValue(true);
          });
          this.scannedBarcodesCount = 0;
        }
      }
    });
  }

  onSelectAll(isChecked: boolean) {
    this.printSerialNumbers = [];
    this.radioRemovalReceivingItemsArray.controls.forEach(control => {
      control.get('serialNumberChecked').setValue(isChecked);
      if (control.get('serialNumberChecked').value) {
        this.printSerialNumbers.push(control.get('serialNumber').value);
      }
    });
  }

  print() {
    this.printSerialNumbers = [];
    this.radioRemovalReceivingItemsArray.value.forEach(prints => {
      if (prints.serialNumberChecked) {
        this.printSerialNumbers.push(prints.serialNumber);
      }
    });
    if (this.printSerialNumbers.length == 0) {
      this.snackbarService.openSnackbar("Select at least one Serial Number to print", ResponseMessageTypes.WARNING);
      return;
    }
    const dialogReff = this.dialog.open(BarcodePrintComponent,
      {
        width: '400px', height: '400px', disableClose: true,
        data: { serialNumbers: this.printSerialNumbers }
      });
  }

  onSave() {

    if (this.radioRemovalReceivingScanForm.invalid) {
      return;
    }

    this.rxjsService.setFormChangeDetectionProperty(true);
    let getValue = this.radioRemovalReceivingItemsArray.value.filter(x => x.isDeleted);

    if (!this.getReceivingScanItemDetails?.items?.isNotSerialized &&
      (this.radioRemovalReceivingItemsArray.value.length === 0 && getValue.length === 0)) {
      this.snackbarService.openSnackbar('"Atleast one record is required"', ResponseMessageTypes.WARNING);
      return;
    }

    let sendparams = {};
    sendparams = {
      "radioRemovalReceiptId": this.radioRemovalSystemDetails?.basicInfo?.radioRemovalReceiptId,
      "warehouseId": this.getReceivingScanItemDetails?.basicInfo.warehouseId,
      "radioRemovalUserId": this.getReceivingScanItemDetails?.basicInfo.radioRemovalUserId,
      "techStockLocationId": this.getReceivingScanItemDetails?.basicInfo.techStockLocationId,
      "DeliveryNotes": this.getReceivingScanItemDetails?.deliveryNotes,
      "createdUserId": this.getReceivingScanItemDetails?.userId,
      "item": {
        "radioRemovalReceiptItemId": this.getReceivingScanItemDetails?.items?.radioRemovalReceiptItemId,
        "radioRemovalReceiptId": this.radioRemovalSystemDetails?.basicInfo?.radioRemovalReceiptId,
        "itemId": this.getReceivingScanItemDetails?.items?.itemId,
        "onHandQuantity": this.getReceivingScanItemDetails?.items?.qtyOnHand,
        "receivedQty": this.getReceivingScanItemDetails?.items?.receivedQty,
        "outStandingQty": this.getReceivingScanItemDetails?.items?.outStandingQty,
        "unitCost": this.getReceivingScanItemDetails?.items?.unitCost,
        "isSerialNumberAvailable": this.showAddQuantity,
        "isToBeTested": this.getReceivingScanItemDetails?.items?.isToBeTested,
        "isToBeDisposed": this.getReceivingScanItemDetails?.items?.isToBeDisposed,
        "isNotSerialized": this.getReceivingScanItemDetails?.items?.isNotSerialized,
        "ItemDetails": this.radioRemovalReceivingItemsArray.value.length > 0 ? this.radioRemovalReceivingItemsArray.value : null
      }
    }

    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING, sendparams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.showReceivigScanDetails = false;
        this.selectedTabIndex = 2;
        this.onUserChange(this.radioRemovalSystemDetails?.basicInfo?.radioRemovalUserId);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  /* Verify */

  radioRemovalDetails(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('RadioRemovalReceiptId', this.radioRemovalSystemDetails?.basicInfo?.radioRemovalReceiptId)
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING_DETAILS,
      undefined, true, params);
  }

  drawComplete() {

  }

  clearPad() {
    this.signaturePad.clear();
  }

  ngAfterViewInit() {
    if (this.signaturePad == undefined) return;
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.resizeCanvas();
    this.signaturePad.clear();
  }

  onVerifySave() {

    if (this.radioRemovalReceivingAddEditForm.invalid) {
      return;
    }

    if (this.signaturePad.signaturePad._data.length === 0) {
      this.snackbarService.openSnackbar("Signature is required", ResponseMessageTypes.WARNING);
      return;
    }

    let formData = new FormData();
    let data = {
      "RadioRemovalReceiptId": this.radioRemovalSystemDetails?.basicInfo?.radioRemovalReceiptId,
      "Comments": this.radioRemovalReceivingAddEditForm.get('comments').value,
      "CreatedUserId": this.userData.userId,
      "DocumentType": this.radioRemovalSystemDetails?.basicInfo?.DocumentTypeName,
      "DeliveryNotes": this.radioRemovalReceivingAddEditForm.get('deliveryNote').value
    }

    this.isButtonDisabled = true;
    if (this.signaturePad.signaturePad._data.length > 0) {
      let imageName = this.radioRemovalSystemDetails?.basicInfo?.receivingId + "-signature.jpeg";
      const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
      const imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' });
      formData.append("document_files", imageFile);
    }

    formData.append("documentDetails", JSON.stringify(data));

    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING_SUBMIT, formData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
        this.isButtonDisabled = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.isButtonDisabled = false;
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  radioSystemVerifyDetails: any = {};

  navigateToNextPage(index: number) {
    if (index == 0) {
      if (this.radioRemovalReceivingAddEditForm.invalid) {
        return;
      }
      else {
        this.selectedTabIndex = index + 1;
      }
    }
    if (index == 2) {
      this.radioRemovalDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.radioSystemVerifyDetails = response.resources;
          this.selectedTabIndex = index + 1;
          this.radioRemovalReceivingAddEditForm.get('comments').setValidators(Validators.required);
          this.radioRemovalReceivingAddEditForm.get('comments').updateValueAndValidity();
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
    else {
      this.selectedTabIndex = index + 1;
    }
  }

  navigateToPrevious(index) {
    if (index == 0) {
      this.router.navigate(['/inventory', 'job-selection', 'receiving-scan'], { skipLocationChange: true });
    }
    else {
      this.selectedTabIndex = index - 1;
    }
  }

  navigateToScanPrevious() {
    this.showReceivigScanDetails = false;
    this.selectedTabIndex = 2;
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'job-selection'], {
      skipLocationChange: true
    });
  }

}
