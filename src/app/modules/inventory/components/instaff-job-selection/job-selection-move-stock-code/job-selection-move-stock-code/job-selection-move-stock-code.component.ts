import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-job-selection-move-stock-code',
  templateUrl: './job-selection-move-stock-code.component.html'
})
export class JobSelectionMoveStockCodeComponent implements OnInit {

  jobSelectionMoveStockCodeForm: FormGroup;
  selectedIndex: number = 0;
  userData: UserLogin;
  moveFirstStockCodeDetails: any = {};
  showBinLocationCodeError: boolean = false;
  showStockCodeError: boolean = false;
  showFirstSerialNumberError: boolean = false;
  showSecondSerialNumberError: boolean = false;
  showFirstStockMoveDetails: boolean = false;
  showSecondStockMoveDetails: boolean = false;
  serialNumbersArray: any = [];
  newSerialNumbersArray: any = [];
  serialNumberErrorMsg: string;
  moveSecondStockCodeDetails: any = {};
  unAssignConfirmationDialog: boolean = false;
  isConfirmationDialog: boolean = false;
  linkBinLocationConfirmationDialog: boolean = false;
  stockBinMovementId: string;
  warehouseDropDownList = [];
  wareHouseId;
  binDetails;
  printBarCode: boolean = false;
  itemId;
  barcodes = [];
  @ViewChild('divClick', null) divClick: ElementRef;
  constructor(
    private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private snackbarService: SnackbarService, private store: Store<AppState>,
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createForm();
    this.getWarehouseDropdown();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createForm() {
    this.jobSelectionMoveStockCodeForm = new FormGroup({
      'binDetails': new FormControl(null),
      'stockBarcode': new FormControl(null),
      'stockCode': new FormControl(null),
      'serialNumber': new FormControl(null),
      'itemName': new FormControl(null),
      'itemCode': new FormControl(null),
      'quantity': new FormControl(null),
      'newBinDetails': new FormControl(null),
      'newSerialNumbers': new FormControl(null),
      'newItemCode': new FormControl(null),
      'newItemName': new FormControl(null),
      'newQuantity': new FormControl(null),
    });
  }

  navigateToNextPage(index: number) {
    if (this.moveFirstStockCodeDetails.itemCode != null &&
      this.jobSelectionMoveStockCodeForm.get('binDetails').value != null) {
      this.selectedIndex = index + 1;
    }
    else {
      this.getBinDetails(index);
    }
  }
  getWarehouseDropdown() {
    //IsAll=true
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_WAREHOUSE,
      undefined,
      false, prepareRequiredHttpParams({ userId: this.userData.userId })
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.warehouseDropDownList = data.resources;
      }
    });
  }
  warehouseChange(value) {
    this.wareHouseId = value;
  }
  getBinDetails(index: number) {

    this.rxjsService.setFormChangeDetectionProperty(true);
    this.showBinLocationCodeError = false; let params;
    let binDetails = this.jobSelectionMoveStockCodeForm.get('binDetails').value;
    if (binDetails == '' || binDetails == null) {
      this.showBinLocationCodeError = true;
      return;
    }
    if (!this.wareHouseId) {
      this.snackbarService.openSnackbar("Warehouse is required.", ResponseMessageTypes.WARNING)
      return;
    }
    params = new HttpParams().set('BinDetails', binDetails).set('wareHouseId', this.wareHouseId)
      .set('CreatedUserId', this.userData.userId)
    let crudService: Observable<IApplicationResponse> =
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STOCKBIN_MOVEMENT_BINDETAILS, undefined, true, params)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.selectedIndex = index + 1;
        this.moveFirstStockCodeDetails = response.resources;
        this.binDetails = binDetails;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigateToFirstNextPage(index: number) {

    let stockCode = this.jobSelectionMoveStockCodeForm.get('stockCode').value;

    if (this.moveFirstStockCodeDetails.itemCode != stockCode) {
      this.checkedStockCode(index);
    }
    else if (!this.showFirstStockMoveDetails && this.moveFirstStockCodeDetails.itemCode == stockCode) {
      this.checkedStockCode(index);
    }
    else if (!this.moveFirstStockCodeDetails.isNotSerialized &&
      this.showFirstStockMoveDetails && this.serialNumbersArray.length > 0) {
      this.selectedIndex = index + 1;
    }
    else {
      this.scanFirstSerialNumber(index);
    }

  }

  checkedStockCode(index: number) {

    this.showStockCodeError = false;
    this.showFirstStockMoveDetails = false;
    let stockCode = this.jobSelectionMoveStockCodeForm.get('stockCode').value;

    if (stockCode == null) {
      this.showStockCodeError = true;
      return;
    }
    if (this.moveFirstStockCodeDetails.itemCode == stockCode) {
      this.jobSelectionMoveStockCodeForm.patchValue(this.moveFirstStockCodeDetails);
      this.showFirstStockMoveDetails = true;
    }
    else {
      this.showFirstStockMoveDetails = false;
      this.snackbarService.openSnackbar('Invalid bin', ResponseMessageTypes.WARNING);
    }
  }

  showFirstQuantityError: boolean = false;

  scanFirstSerialNumber(index: number) {

    this.showFirstSerialNumberError = false;
    this.serialNumberErrorMsg = '';
    this.showFirstQuantityError = false;

    let serialNumber = this.jobSelectionMoveStockCodeForm.get('serialNumber').value;
    let quantity = this.jobSelectionMoveStockCodeForm.get('quantity').value;

    if (!this.moveFirstStockCodeDetails.isNotSerialized && (serialNumber == null)) {
      this.showFirstSerialNumberError = true;
      this.serialNumberErrorMsg = 'Serial number is required';
      return;
    }

    if (this.moveFirstStockCodeDetails.isNotSerialized && (quantity == null)) {
      this.showFirstQuantityError = true;
      return;
    }

    let params = new HttpParams().set('LocationBinId', this.moveFirstStockCodeDetails.locationBinId)
      .set('ItemId', this.moveFirstStockCodeDetails.itemId)
      .set('IsNotSerialized', this.moveFirstStockCodeDetails.isNotSerialized)
      .set('SerialNumber', this.moveFirstStockCodeDetails.isNotSerialized ? null : serialNumber)
      .set('ScannedQty', this.moveFirstStockCodeDetails.isNotSerialized ?
        this.jobSelectionMoveStockCodeForm.get('quantity').value : this.serialNumbersArray.length)

    let crudService: Observable<IApplicationResponse> =
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STOCKBIN_MOVEMENT_SCAN_ITEMDETAILS_BINDETAILS, undefined, true, params)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        if (!this.moveFirstStockCodeDetails.isNotSerialized) {
          this.serialNumbersArray.push(serialNumber);
          let unique = this.serialNumbersArray.filter((v, i, a) => a.indexOf(v) === i);
          this.serialNumbersArray = unique;
          this.jobSelectionMoveStockCodeForm.get('serialNumber').patchValue(null);
          this.jobSelectionMoveStockCodeForm.get('quantity').patchValue(unique.length);
        }
        else {
          this.selectedIndex = index + 1
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigateToSecondNextPage() {

    let newBinDetails = this.jobSelectionMoveStockCodeForm.get('newBinDetails').value;
    if (this.moveSecondStockCodeDetails.binDetails != newBinDetails) {
      this.getDestinationBinDetails();
    }
    else if (!this.showSecondStockMoveDetails && this.moveSecondStockCodeDetails.binDetails == newBinDetails) {
      this.getDestinationBinDetails();
    }
    else if (!this.moveSecondStockCodeDetails.isNotSerialized &&
      this.showSecondStockMoveDetails && this.newSerialNumbersArray.length === 0) {
      this.scanSecondSerialNumber();
    }
    else {
      this.finalStockCode();
    }
  }

  getDestinationBinDetails() {

    this.showSecondStockMoveDetails = false;
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.showBinLocationCodeError = false; let params;
    let newBinDetails = this.jobSelectionMoveStockCodeForm.get('newBinDetails').value;
    if (newBinDetails == '' || newBinDetails == null) {
      this.showBinLocationCodeError = true;
      return;
    }
    if (!this.wareHouseId) {
      this.snackbarService.openSnackbar("Warehouse is required.", ResponseMessageTypes.WARNING)
      return;
    }
    params = new HttpParams().set('BinDetails', newBinDetails).set('wareHouseId', this.wareHouseId)
      .set('CreatedUserId', this.userData.userId)

    let crudService: Observable<IApplicationResponse> =
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STOCKBIN_MOVEMENT_BINDETAILS, undefined, true, params)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.moveSecondStockCodeDetails = response.resources;
        if (this.moveFirstStockCodeDetails.itemId != response.resources.itemId && response.resources.isItemMapped) {
          this.snackbarService.openSnackbar('Destination bin and move stock code does not match', ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else if (response.resources.binDetails == newBinDetails && response.resources.isItemMapped) {
          this.itemId = this.moveSecondStockCodeDetails.itemId;
          this.jobSelectionMoveStockCodeForm.get('newItemCode').patchValue(response.resources.itemCode);
          this.jobSelectionMoveStockCodeForm.get('newItemName').patchValue(response.resources.itemName);
          this.jobSelectionMoveStockCodeForm.get('newQuantity').patchValue(null);
          this.showSecondStockMoveDetails = true;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else if (!response.resources.isItemMapped) {
          //  this.linkBinLocationConfirmationDialog = true;
          this.linkBinLocations();
        }
        // else {
        //   this.snackbarService.openSnackbar('Destination bin and move stock code does not match', ResponseMessageTypes.ERROR);
        //   this.rxjsService.setGlobalLoaderProperty(false);
        // }
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  unassignLink() {
    this.unAssignConfirmationDialog = false
    this.printBarCode = true;
  }
  linkBinLocations() {
    let sendData = {
      "itemId": this.moveFirstStockCodeDetails.itemId,
      "createdUserId": this.userData.userId,
      "locationBinIds": new Array(this.moveSecondStockCodeDetails.locationBinId)
    }

    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STOCK_IN_BINS_LINK, sendData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.linkBinLocationConfirmationDialog = false;
        this.getDestinationBinDetails();
        // this.printBarCode = true;
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  getBarCode() {
    let params = new HttpParams().set('itemId', this.itemId);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCKCODEBARCODE_DETAILS, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.resources != null) {
          this.barcodes = new Array(response.resources.stockCodeBarcode);
          setTimeout(() => {
            this.divClick.nativeElement.click();
          }, 500);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

  }
  scanSecondSerialNumber() {

    this.showSecondSerialNumberError = false;
    let newSerialNumbers = this.jobSelectionMoveStockCodeForm.get('newSerialNumbers').value;
    if (newSerialNumbers == '' || newSerialNumbers == null) {
      this.showSecondSerialNumberError = true;
      this.serialNumberErrorMsg = 'Serial number is required'; return;
    }

    let newValue = this.serialNumbersArray.filter(serials => serials === newSerialNumbers);
    if (newValue.length > 0) {
      this.newSerialNumbersArray.push(newValue[0]);
      let uniqueValue = this.newSerialNumbersArray.filter((v, i, a) => a.indexOf(v) === i);
      this.newSerialNumbersArray = uniqueValue;
      this.jobSelectionMoveStockCodeForm.get('newSerialNumbers').patchValue(null);
      this.jobSelectionMoveStockCodeForm.get('newQuantity').patchValue(uniqueValue.length);
    }
    else {
      this.snackbarService.openSnackbar('Serial number does not match', ResponseMessageTypes.WARNING);
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  finalStockCode() {

    if (!this.moveSecondStockCodeDetails.isNotSerialized &&
      (this.serialNumbersArray.length != this.newSerialNumbersArray.length)) {
      this.snackbarService.openSnackbar('Scanned quatity does not match', ResponseMessageTypes.WARNING);
      return;
    }

    if (this.moveSecondStockCodeDetails.isNotSerialized &&
      (this.jobSelectionMoveStockCodeForm.get('quantity').value != this.jobSelectionMoveStockCodeForm.get('newQuantity').value)) {
      this.snackbarService.openSnackbar('Scanned quatity does not match --', ResponseMessageTypes.WARNING);
      return;
    }

    let sendData = {
      "fromLocationBinId": this.moveFirstStockCodeDetails.locationBinId,
      "toLocationBinId": this.moveSecondStockCodeDetails.locationBinId,
      "itemId": this.moveSecondStockCodeDetails.itemId,
      "isNotSerialized": this.moveSecondStockCodeDetails.isNotSerialized,
      "quantity": this.moveSecondStockCodeDetails.isNotSerialized ?
        this.jobSelectionMoveStockCodeForm.get('newQuantity').value : this.newSerialNumbersArray.length,
      "serialNumbers": this.moveSecondStockCodeDetails.isNotSerialized ? null : this.newSerialNumbersArray.toString(),
      "createdUserId": this.userData.userId
    }

    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STOCK_BIN_MOVEMOVEMENT, sendData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.stockBinMovementId = response.resources.stockBinMovementId;
        this.isConfirmationDialog = true;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  stockCodeUnassgin() {
    this.isConfirmationDialog = false;
    if (!this.moveSecondStockCodeDetails.isNotSerialized &&
      this.moveFirstStockCodeDetails.totalQty == this.newSerialNumbersArray.length) {
      this.unAssignConfirmationDialog = true;
    }
    else if (this.moveSecondStockCodeDetails.isNotSerialized &&
      this.moveFirstStockCodeDetails.totalQty == this.jobSelectionMoveStockCodeForm.get('newQuantity').value) {
      this.unAssignConfirmationDialog = true;
    }
    else {
      this.printBarCode = true;
      // this.navigateToList();
    }
  }

  unassignStockOrders() {

    let sendData = {
      "createdUserId": this.userData.userId,
      "stockBinMovementId": this.stockBinMovementId
    }

    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.STOCKBIN_MOVEMOVEMENT_TASKLIST_INSERT, sendData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        // this.navigateToList();
        this.unAssignConfirmationDialog = false;
        this.printBarCode = true;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.unAssignConfirmationDialog = false;
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigateToPreviousPage(index) {
    this.showFirstSerialNumberError = false;
    this.showSecondSerialNumberError = false;
    this.showBinLocationCodeError = false;
    this.showStockCodeError = false;
    this.serialNumberErrorMsg = '';
    this.showFirstStockMoveDetails = false;
    this.showSecondStockMoveDetails = false;
    this.showFirstQuantityError = false;
    this.serialNumbersArray = [];
    this.newSerialNumbersArray = [];
    this.selectedIndex = index - 1;
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'job-selection'], {
      skipLocationChange: true
    });
  }

}
