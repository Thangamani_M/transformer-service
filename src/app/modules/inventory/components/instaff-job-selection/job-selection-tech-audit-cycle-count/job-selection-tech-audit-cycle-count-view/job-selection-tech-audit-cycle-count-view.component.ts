import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes,
  RxjsService, SnackbarService
} from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { UserLogin } from '@modules/others/models/others-module-models';
import { forkJoin, Observable } from 'rxjs';

@Component({
  selector: 'app-job-selection-tech-audit-cycle-count-view',
  templateUrl: './job-selection-tech-audit-cycle-count-view.component.html'
})

export class JobSelectionTechAuditCycleCountViewComponent implements OnInit {

  userData: UserLogin;
  techAuditCycleCountId: any;
  auditIterationId: any;
  auditCycleCountHeaderDetails: any = {};
  auditCycleCountDetails: any = {};
  auditSystem: any;
  auditUserselection: any;
  auditHighRiskItems: any;
  randomSelection: any;
  isEdit=0;
  constructor(
    private router: Router, private activatedRoute: ActivatedRoute,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private crudService: CrudService,
  ) {
    this.techAuditCycleCountId = this.activatedRoute.snapshot.queryParams.id;
    this.isEdit = this.activatedRoute.snapshot.queryParams.isEdit;

  }

  ngOnInit() {
    if (this.techAuditCycleCountId) {
      this.getDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.auditCycleCountDetails = response.resources;
          this.auditIterationId = response.resources?.techAuditCycleCountIterationId;
          this.getAuditCycleCountDetails();
          this.rxjsService.setGlobalLoaderProperty(false); 
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);     
        }
      });     
    }
  }

  getDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_JOB, this.techAuditCycleCountId);
  }

  getAuditCycleCountDetails() {
    
    if(this.auditIterationId){
      let params = new HttpParams().set('TechAuditCycleCountId', this.techAuditCycleCountId).set('TechAuditCycleCountIterationId', this.auditIterationId)
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_JOB_DETAILS_LIST, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            if (response.resources.length > 0) {
              response.resources.forEach(x => {
                if (x.auditCycleCountRefTypeId == 10) {
                  this.auditHighRiskItems = x.progresspercentage;
                }
                else if (x.auditCycleCountRefTypeId == 11) {
                  this.auditUserselection = x.progresspercentage;
                }
                else {
                  this.auditSystem = x.progresspercentage;
                }
              });
            }
            if (response.resources.length >= 3) {
              this.randomSelection = (this.auditUserselection + this.auditHighRiskItems) / 2;
            }            
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
      });
    }
    
  }

  navigateToJobSelection() {
    this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
  }

  navigateToAuditCycleCountList() {
    this.router.navigate(['/inventory', 'job-selection', 'tech-audit-cycle-count-list'], {queryParams:{isEdit:this.isEdit}, skipLocationChange: true });
  }

  navigateToScanPage(value: string, id: number) {
    this.router.navigate(['/inventory', 'job-selection', 'tech-audit-cycle-count-scan'], {
      queryParams: {
        id: this.techAuditCycleCountId,
        iterationId: this.auditIterationId,
        type: value,
        refTypeId: id,
        isEdit:this.isEdit
      }, skipLocationChange: true
    });
  }

}
