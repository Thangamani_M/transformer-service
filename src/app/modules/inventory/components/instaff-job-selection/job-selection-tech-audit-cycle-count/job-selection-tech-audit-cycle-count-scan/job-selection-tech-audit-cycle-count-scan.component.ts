import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels, JobSelectionAuditCycleCountAddEditModel } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { JobSelectionScanModalComponent } from '../..';

@Component({
  selector: 'app-job-selection-tech-audit-cycle-count-scan',
  templateUrl: './job-selection-tech-audit-cycle-count-scan.component.html'
})
export class JobSelectionTechAuditCycleCountScanComponent implements OnInit {

    techAuditCycleCountId: any;
    auditIterationId: any;
    auditType: any;
    auditRefTypeId: any;
    jobSelectionAuditCycleCountScanForm: FormGroup;
    auditCycleCountDetails: any = {};
    userData: UserLogin;
    auditCycleItemsDetails: any
    auditCycleCountShow: boolean = false;
    duplicateError: string = '';
    showDuplicateError: boolean = false;
    stockCodeEnable: boolean = false;
    filteredStockCodes = [];
    auditTechItemId: string = '';
    showRandomStockCode: boolean = false;
    showStockCodeError: boolean = false;
    filteredRandomStockCodes: any = [];
    isEdit=0;
    constructor(
      private rxjsService: RxjsService, 
      private crudService: CrudService, private activatedRoute: ActivatedRoute,
      private formBuilder: FormBuilder, private router: Router, private dialog: MatDialog,
      private httpCancelService: HttpCancelService, private snackbarService: SnackbarService,
      private changeDetectorRef: ChangeDetectorRef, private store: Store<AppState>,
    ) {

      this.techAuditCycleCountId = this.activatedRoute.snapshot.queryParams.id;
      this.auditIterationId = this.activatedRoute.snapshot.queryParams.iterationId;
      this.auditType = this.activatedRoute.snapshot.queryParams.type;
      this.auditRefTypeId = this.activatedRoute.snapshot.queryParams.refTypeId;
      this.isEdit = this.activatedRoute.snapshot.queryParams.isEdit;
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      });
    }

    ngOnInit() {
      this.createAuditCycleCountScanForm();
      if (this.techAuditCycleCountId && this.auditIterationId) {
        this.getDetailsById();
        this.getAuditItemsDetailsById('get');
      }
    }

    createAuditCycleCountScanForm(): void {
      let stockOrderModel = new JobSelectionAuditCycleCountAddEditModel();
      // create form controls dynamically from model class
      this.jobSelectionAuditCycleCountScanForm = this.formBuilder.group({});
      Object.keys(stockOrderModel).forEach((key) => {
        this.jobSelectionAuditCycleCountScanForm.addControl(key, new FormControl(stockOrderModel[key]));
      });
    }

    getDetailsById() {
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_JOB, this.techAuditCycleCountId)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.auditCycleCountDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }

    getAuditItemsDetailsById(type: string) {

      let params = Object.assign({},
        this.techAuditCycleCountId == undefined ? null : { TechAuditCycleCountId: this.techAuditCycleCountId },
        this.auditIterationId == undefined ? null : { TechAuditCycleCountIterationId: this.auditIterationId },
        this.auditRefTypeId == undefined ? null : { AuditCycleCountRefTypeId: this.auditRefTypeId },
        { IsNext: type == 'get' ? false : true }, { CreatedUserId: this.userData.userId },
        type == 'get' ? null : { ItemId: this.auditTechItemId },
      );

      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_JOB_STOCK_DETAILS, undefined, true,
        prepareRequiredHttpParams(params)).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          if(response.resources.techAuditCycleCountJobItemDetails != null){
            this.auditCycleItemsDetails = response.resources.techAuditCycleCountJobItemDetails;
            this.auditTechItemId = this.auditCycleItemsDetails?.itemId;
            this.auditCycleItemsDetails.isNotSerialized ? this.stockCodeEnable = true : false;
            this.auditCycleCountShow = true;
            this.jobSelectionAuditCycleCountScanForm.get('countedQty').patchValue(this.auditCycleItemsDetails.scannedQty);
            this.jobSelectionAuditCycleCountScanForm.get('isNotSerialized').patchValue(this.auditCycleItemsDetails.isNotSerialized);
            this.jobSelectionAuditCycleCountScanForm.get('itemCode').patchValue(this.auditCycleItemsDetails.itemCode);
            this.jobSelectionAuditCycleCountScanForm.get('itemDescription').patchValue(this.auditCycleItemsDetails.itemDescription);
            if (this.stockCodeEnable) {
              this.jobSelectionAuditCycleCountScanForm = setRequiredValidator(this.jobSelectionAuditCycleCountScanForm, ['countedQty']);
            }
          }

          if(!response.resources.isAllStockScanned && response.resources.techAuditCycleCountJobItemDetails == null){
            this.navigateToAuditCycleCountView();
          }
          
          if(response.resources.isAllStockScanned && 
            (this.auditRefTypeId == '10' || this.auditRefTypeId == '11')){
            this.onModalPopupOpen(response.resources.responsemsg);
          }

          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }

    ngAfterViewChecked() {
      this.changeDetectorRef.detectChanges();
    }

    onScanSerialInput() {

      this.rxjsService.setFormChangeDetectionProperty(true);
      this.showDuplicateError = false;
      if (!this.stockCodeEnable && this.jobSelectionAuditCycleCountScanForm.get('scanSerialNumberInput').value == '') {
        this.showDuplicateError = true;
        this.duplicateError = "Serial number is required";
        return;
      }

      if (this.stockCodeEnable && this.jobSelectionAuditCycleCountScanForm.invalid) {
        return;
      }

      let formData = {};
      formData = {
        "techAuditCycleCountId": this.techAuditCycleCountId,
        "itemId": this.auditTechItemId,
        "techAuditCycleCountIterationId": this.auditIterationId,
        "techAuditCycleCountItemId": this.auditCycleItemsDetails.techAuditCycleCountItemId,
        "serialNumber": this.jobSelectionAuditCycleCountScanForm.get('scanSerialNumberInput').value,
        "availableQty": !this.stockCodeEnable ? 1 :
        this.jobSelectionAuditCycleCountScanForm.get('countedQty').value,
        "isNotSerialized": this.auditCycleItemsDetails.isNotSerialized,
        "createdUserId": this.userData.userId
      }

      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
      InventoryModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_JOB_SCANNED_ITEM_POST, formData)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.jobSelectionAuditCycleCountScanForm.get('scanSerialNumberInput').patchValue('');
          this.getAuditItemsDetailsById('get');
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }

    iterationUpdate() {

      let iteration = {};
      iteration = {
        "techAuditCycleCountId": this.techAuditCycleCountId,
        "techAuditCycleCountIterationId": this.auditIterationId,
        "createdUserId": this.userData.userId,
        "auditCycleCountRefTypeId": this.auditRefTypeId
      }

      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.TECHNICIAN,
      InventoryModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_JOB_ITERATION_UPDATE, iteration)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.onModalPopupOpenNext();
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
    
    inputChangeRandomStockCode(text:any){

      this.showStockCodeError = false 
      if(text.query == null || text.query == ''){
        this.showStockCodeError = true;
        return;
      }
  
      let otherParams = {}
      this.showStockCodeError = false 
      otherParams['StockCode'] = text.query;
      if(this.techAuditCycleCountId){
        otherParams['techAuditCycleCountId'] = this.techAuditCycleCountId ;
      }
      otherParams['IsUserSelection'] = this.auditRefTypeId == '11' ? true : false;
      otherParams['IsHighRisk'] = this.auditRefTypeId == '10' ? true : false;
  
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, 
        TechnicalMgntModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_STOCK_DETAILS, null, false,
        prepareGetRequestHttpParams(null, null, otherParams))
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.isSuccess) {
            this.filteredRandomStockCodes = [];
            this.filteredRandomStockCodes = response.resources;
            if(response.resources.length == 0){
              this.showStockCodeError = true;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }

    getNewStockCodes(text:any){
      
      if(text == '' || text == null) return;
      let stockCodes = {
        "techAuditCycleCountId": this.techAuditCycleCountId,
        "auditCycleCountRefTypeId": this.auditRefTypeId,
        "itemId": text.itemId,
        "createdUserId": this.userData.userId
      }

      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
      InventoryModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_JOB_NEW_ITEM_POST, stockCodes)
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.auditTechItemId = text.itemId;
          this.getAuditItemsDetailsById('get');
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

    }

    onModalPopupOpenNext() {
      const dialogRemoveStockCode = this.dialog.open(JobSelectionScanModalComponent, {
        width: '450px',
        data: {
          header: 'Success',
          message: `Audit Cycle Count ID <b>${this.auditCycleCountDetails['techAuditCycleCountNumber']}</b></br>
        completed successfully`,
          buttons: {
            create: 'Ok'
          }
        }, disableClose: true
      });
      dialogRemoveStockCode.afterClosed().subscribe(result => {
        if (result && typeof result == 'boolean') {
          this.navigateToAuditCycleCountList();
          this.rxjsService.setDialogOpenProperty(false);
        }
      });
    }

    onModalPopupOpen(confirmationMsg) {
      const dialogRemoveStockCode = this.dialog.open(JobSelectionScanModalComponent, {
        width: '450px',
        data: {
          header: 'Confirmation',
          message: confirmationMsg,
          buttons: {
            create: 'Yes',
            cancel: 'No'
          }
        }, disableClose: true
      });
      dialogRemoveStockCode.afterClosed().subscribe(result => {
        if (result && typeof result == 'boolean') {
          this.showRandomStockCode = true;
          this.auditCycleCountShow = false;
        }
        if (!result && typeof result == 'boolean') {
          this.navigateToAuditCycleCountView();
        }
        this.rxjsService.setDialogOpenProperty(false);
      });
    }

    navigateToJobSelection() {
      this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
    }

    navigateToAuditCycleCountList() {
      this.router.navigate(['/inventory', 'job-selection', 'tech-audit-cycle-count-list'],
       {queryParams: { isEdit: this.isEdit},skipLocationChange: true});
    }

    navigateToAuditCycleCountView() {
      this.router.navigate(['/inventory', 'job-selection', 'tech-audit-cycle-count-view'], {
        queryParams: {
          id: this.techAuditCycleCountId,
          iterationId: this.auditIterationId,
          isEdit: this.isEdit
        }, skipLocationChange: true
      });
    }


}
