import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, CustomDirectiveConfig,
  exportList,
  IApplicationResponse,
  LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareGetRequestHttpParams,
  ResponseMessageTypes,
  RxjsService,
  SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-job-selection-tech-audit-cycle-count-list',
  templateUrl: './job-selection-tech-audit-cycle-count-list.component.html',
  styleUrls: ['./job-selection-tech-audit-cycle-count-list.component.scss']
})
export class JobSelectionTechAuditCycleCountListComponent implements OnInit {

  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  userData: UserLogin;
  searchForm: FormGroup;
  observableResponse: any;
  dataList: any
  status: any = [];
  selectedRows: string[] = [];
  selectedTabIndex: any = 0;
  totalRecords: any;
  listSubscribtion: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties:any;
  columnFilterForm: FormGroup;
  searchColumns: any
  loading: boolean;
  today: any = new Date();
  row: any = {};
  first: any = 0;
  reset: boolean;
  otherParams;
  isEdit= 0;
  isExport;
    constructor(
      private commonService: CrudService,
      private router: Router,private activatedRoute: ActivatedRoute,
      private rxjsService: RxjsService, private datePipe: DatePipe,
      private store: Store<AppState>,private snackbarService: SnackbarService
    ) {
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      });
      this.primengTableConfigProperties = {
        tableCaption: "Audit Cycle Count - Technician",
        breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: ''},
        { displayName: 'Job Selection', relativeRouterUrl: '/inventory/job-selection'},
        { displayName: 'Audit Cycle Count - Technician', relativeRouterUrl: ''}],
        selectedTabIndex: 0,
        tableComponentConfigs: {
          tabsList: [
            {
              caption: 'Audit Cycle Count - Technician',
              dataKey: 'goodsReturnId',
              enableBreadCrumb: true,
              enableAction: true,
              enableReset: false,
              enableGlobalSearch: false,
              reorderableColumns: false,
              resizableColumns: false,
              enableScrollable: true,
              checkBox: false,
              enableRowDelete: false,
              enableFieldsSearch: true,
              enableHyperLink: true,
              cursorLinkIndex: 0,
              columns: [{ field: 'techAuditCycleCountNumber', header: 'Audit Cycle Count ID', width: '180px' },
              { field: 'techStockLocation', header: 'Tech Stock Location', width: '180px' },
              { field: 'techStockLocationName', header: 'Tech Stock Location Name', width: '200px' },
              { field: 'techArea', header: 'Tech Area', width: '200px' },
              { field: 'warehouse', header: 'Warehouse', width: '200px' },
              { field: 'storageLocationName', header: 'Storage Location', width: '150px' },
              { field: 'branchName', header: 'Branch', width: '150px' },
              { field: 'districtName', header: 'District', width: '150px' },
              { field: 'divisionName', header: 'Division', width: '150px' },
              { field: 'regionName', header: 'Region', width: '150px' },
              { field: 'actionedDate', header: 'Actioned Date & Time', width: '200px' },
              { field: 'actionedBy', header: 'Actioned By', width: '150px' },
              { field: 'status', header: 'Status', width: '150px' }],
              shouldShowDeleteActionBtn: false,
              enableAddActionBtn: false,
              areCheckboxesRequired: false,
              isDateWithTimeRequired: true,
              enableExportBtn: true,
              apiSuffixModel: InventoryModuleApiSuffixModels.TECH_AUDIT_CYCLE_COUNT_JOB,
              moduleName: ModulesBasedApiSuffix.TECHNICIAN,
            }
          ]
        }
      }
      this.activatedRoute.queryParamMap.subscribe((params) => {
        this.isEdit = + params['params']['isEdit'];
        this.isExport = + params['params']['isExport']
        });

    }

    ngOnInit(): void {
      this.combineLatestNgrxStoreData()
      this.getRequiredListData();
    }

    ngAfterViewInit(): void {
      // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
    }

    onActionSubmited(e: any) {
      if (e.data && !e.search && !e?.col?.toString()) {
          this.onCRUDRequested(e.type, e.data);
      } else if (e.data && e.search && !e?.col) {
          this.onCRUDRequested(e.type, e.data, e.search);
      } else if (e.type && !e.data && !e?.col) {
          this.onCRUDRequested(e.type, {});
      } else if (e.type && e.data && e?.col?.toString()) {
          this.onCRUDRequested(e.type, e.data, e?.col);
      }
    }

    combineLatestNgrxStoreData() {
      combineLatest(
        this.store.select(loggedInUserData)
      ).subscribe((response) => {
        this.loggedInUserData = new LoggedInUserModel(response[0]);
      });
    }

    getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
      this.loading = true;
      otherParams = {...otherParams, IsAll: true, userId: this.userData?.userId}
      this.otherParams = otherParams;
      let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
      InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
      if (this.listSubscribtion && !this.listSubscribtion.closed) {
        this.listSubscribtion.unsubscribe();
      }
      this.listSubscribtion = this.commonService.get(
        ModulesBasedApiSuffix.TECHNICIAN,
        InventoryModuleApiSuffixModels,
        undefined,
        false, prepareGetRequestHttpParams(pageIndex, pageSize,otherParams)
      ).pipe(map((res: IApplicationResponse) => {
          if (res?.resources) {
            res?.resources?.forEach(val => {
              val.actionedDate = this.datePipe.transform(val?.actionedDate, 'dd-MM-yyyy, HH:mm:ss');
              return val;
            });
          }
          return res;
        })).subscribe(data => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.observableResponse = data.resources;
          this.dataList = this.observableResponse;
          this.totalRecords = data.totalCount;
        } else {
          this.observableResponse = null;
          this.dataList = this.observableResponse
          this.totalRecords = 0;
        }
        this.reset = false;
      });
    }

    onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
      switch (type) {
        case CrudType.CREATE:
          this.openAddEditPage(CrudType.CREATE, row);
          break;
        case CrudType.GET:
          unknownVar['IsAll'] = true;
          this.row = row ? row : { pageIndex: 0, pageSize: 10 };
          this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
          this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar);
          break;
        case CrudType.EDIT:
          if (!this.isEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.openAddEditPage(CrudType.VIEW, row);
          break;
        case CrudType.VIEW:
          if (!this.isEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
          }
          this.openAddEditPage(CrudType.VIEW, row);
          break;
          case CrudType.EXPORT:
            if (!this.isExport) {
              return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
            let pageIndex = this.row && this.row["pageIndex"] ? this.row["pageIndex"] : 0;
            let pageSize = this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
            this.exportList(pageIndex, pageSize);
            break;

        default:
      }
    }

    exportList(pageIndex?: any, pageSize?: any) {
      exportList(this.otherParams, 0, pageSize, ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.CYCLE_COUNT_EXPORT, this.commonService, this.rxjsService, 'Audit Cycle Count - Technician');
    }
    onTabChange(event) {
      this.dataList = [];
      this.totalRecords = null;
      this.selectedTabIndex = event.index
      this.router.navigate(['/technical-management/technician-goods-return'], { queryParams: { tab: this.selectedTabIndex } })
      this.getRequiredListData();
    }

    openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
      switch (type) {
        case CrudType.VIEW:
          this.router.navigate(['/inventory', 'job-selection', 'tech-audit-cycle-count-view'], {
            queryParams: {
              id: editableObject['techAuditCycleCountId'],
            }, skipLocationChange:true
          });
        break;
      }
    }

    ngOnDestroy() {
      if (this.listSubscribtion) {
        this.listSubscribtion.unsubscribe();
      }
    }

}
