import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RxjsService } from '@app/shared';


@Component({
    selector: 'app-job-selection-stock-take-modal',
    templateUrl: './job-selection-stock-take-modal.component.html',
})

export class JobSelectionStockTakeModalComponent implements OnInit {

    modalDetails: any;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data,
        private rxjsService: RxjsService,
    ) {

        this.modalDetails = data.serialNumberDetails;
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
    }
}