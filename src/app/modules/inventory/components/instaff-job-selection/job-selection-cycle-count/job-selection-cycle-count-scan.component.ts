import { HttpParams } from '@angular/common/http';
import { ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { JobSelectionCycleCountAddEditModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { JobSelectionScanModalComponent } from '../job-selection-list';


@Component({
    selector: 'app-job-selection-cycle-count-scan',
    templateUrl: './job-selection-cycle-count-scan.component.html',
    styleUrls: ['./job-selection-cycle-count-scan.component.scss']
})

export class JobSelectionCycleCountScanComponent {

    cycleCountId: any;
    iterationId: any;
    userId: any;
    type: any;
    cycleCountDetails: any;
    cycleCountStockItemsDetails: any = [];
    jobSelectionCycleCountScanForm: FormGroup;
    availableRowDropDown: any = [];
    cycleCountShow: boolean = false;
    duplicateError: any;
    showDuplicateError: boolean = false;
    userData: UserLogin;
    selectedIndex: number = 0;
    cycleCountLocationDetails: any = {};
    storageLocationId: string = '';
    storageLocationDropDown: any = [];
    constructor(
        private rjxService: RxjsService, private changeDetectorRef: ChangeDetectorRef,
        private crudService: CrudService, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
        private formBuilder: FormBuilder, private router: Router, private dialog: MatDialog,
        private httpCancelService: HttpCancelService, private snackbarService: SnackbarService,
    ) {
        this.cycleCountId = this.activatedRoute.snapshot.queryParams.id;
        this.iterationId = this.activatedRoute.snapshot.queryParams.iterationId;
        this.userId = this.activatedRoute.snapshot.queryParams.userId;
        this.type = this.activatedRoute.snapshot.queryParams.type;
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
    }

    ngOnInit(): void {
        this.availableRowsDropDown();
        this.createCycleCountAddEditForm();
        if (this.cycleCountId) {
            this.getDetailsById();
        }
    }

    getDetailsById() {
        let params = new HttpParams().set('CycleCountId', this.cycleCountId).set('CycleCountIterationId', this.iterationId)
            .set('CreatedUserId', this.userId);
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_DETAILS, undefined, true, params)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.cycleCountShow = false;
                    this.cycleCountDetails = response.resources;
                    this.rjxService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rjxService.setGlobalLoaderProperty(false);
                }
            });
    }
    mainLocationWarehouse: boolean = false;
    getCycleCountDetailsById() {

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_INPROGRESS_ITEMS,
            undefined, true, prepareRequiredHttpParams({
                CycleCountId: this.cycleCountId, CycleCountIterationId: this.iterationId, createdUserId: this.userData.userId
            })).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200 && response.resources != null) {
                    this.jobSelectionCycleCountScanForm.get('availableRows').setValue(response.resources.rowId);
                    if (response.resources.locationId != null) {
                        if (response.resources.itemId != null) {
                            this.selectedIndex = 3;
                            this.cycleCountLocationDetails = response.resources;
                            this.jobSelectionCycleCountScanForm.patchValue(response.resources);
                            if (response.resources.storageLocationName.includes('Main Location')) {
                                this.mainLocationWarehouse = true;
                            }
                            else {
                                this.mainLocationWarehouse = false;
                            }
                        }
                        if (response.resources.itemId == null) {
                            this.selectedIndex = 1;
                            this.jobSelectionCycleCountScanForm.get('locationId').patchValue(response.resources.locationId);

                        }
                    }
                    if (response.resources.locationId == null) {
                        this.selectedIndex = 1;
                    }
                    this.rjxService.setGlobalLoaderProperty(false);
                }
                else {
                    // this.selectedIndex = 1;
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rjxService.setGlobalLoaderProperty(false);
                }
            });
    }

    onStorageLocation() {

        // if (locationIds != '' && locationIds != null) {
        // let storage = this.availableRowDropDown.find(x=> x.id == locationIds);
        // if(storage.displayName.includes('Main Location')){
        //     this.mainLocationWarehouse = true;
        // }
        // else {
        //     this.mainLocationWarehouse = false;
        // }

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_ITEM_DETAIL,
            undefined, true, prepareRequiredHttpParams({
                CycleCountIterationId: this.iterationId, CycleCountId: this.cycleCountId,
                createdUserId: this.userData.userId
            })).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200 && response.resources != null) {
                    this.selectedIndex = 2;
                    // this.storageLocationId = locationIds;
                    this.rjxService.setGlobalLoaderProperty(false);
                }
                else {
                    this.selectedIndex = 1;
                    this.jobSelectionCycleCountScanForm.get('binLocation').patchValue('');
                    this.rjxService.setGlobalLoaderProperty(false);
                }
            });

        //  }
    }

    availableRowsDropDown() {
        let dropParams = new HttpParams().set('CycleCountId', this.cycleCountId)
            .set('CycleCountIterationId', this.iterationId).set('CreatedUserId', this.userId);
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_AVAILABLE_ROWS, undefined, true, dropParams)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.availableRowDropDown = response.resources;
                    this.getCycleCountDetailsById();
                    this.rjxService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rjxService.setGlobalLoaderProperty(false);
                }
            });
    }

    getDetailsItemById(e?: any) {
        let dropParams = new HttpParams().set('CycleCountId', this.cycleCountId)
            .set('CycleCountIterationId', this.iterationId).set('CreatedUserId', this.userId)
            .set('LocationRowId', this.jobSelectionCycleCountScanForm.get('availableRows').value);
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_ITEM_TO_SCAN,
            undefined, true, dropParams)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.cycleCountStockItemsDetails = response.resources.cycleCountItemtoScandetail;
                    let itemScanResponse = response.resources.cycleCountItemtoScanResponse;
                    this.rjxService.setGlobalLoaderProperty(false);
                    if (itemScanResponse.isError) {
                        this.snackbarService.openSnackbar(itemScanResponse.errorMessage, ResponseMessageTypes.WARNING);
                        this.navigateToCycleCountList();
                    }
                    if (this.cycleCountStockItemsDetails != null) {
                        this.cycleCountShow = true;
                        this.jobSelectionCycleCountScanForm.get('cycleCountIterationCount').patchValue(this.cycleCountStockItemsDetails.scannedItemCount);
                        this.jobSelectionCycleCountScanForm.get('bincode').patchValue(this.cycleCountStockItemsDetails.bincode);
                        this.jobSelectionCycleCountScanForm.get('itemCode').patchValue(this.cycleCountStockItemsDetails.itemCode);
                        this.jobSelectionCycleCountScanForm.get('itemDescription').patchValue(this.cycleCountStockItemsDetails.itemDescription);
                        this.jobSelectionCycleCountScanForm.get('availableRows').patchValue(this.cycleCountStockItemsDetails.locationRowId);
                    }
                }
                else {
                    this.cycleCountShow = false;
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rjxService.setGlobalLoaderProperty(false);
                }
            });
    }

    ngAfterViewChecked() {
        this.changeDetectorRef.detectChanges();
    }

    createCycleCountAddEditForm(): void {
        let stockOrderModel = new JobSelectionCycleCountAddEditModel();
        // create form controls dynamically from model class
        this.jobSelectionCycleCountScanForm = this.formBuilder.group({});
        Object.keys(stockOrderModel).forEach((key) => {
            this.jobSelectionCycleCountScanForm.addControl(key, new FormControl(stockOrderModel[key]));
        });
    }

    onSubmit() {

        this.showDuplicateError = false;
        if (this.jobSelectionCycleCountScanForm.get('scanSerialNumberInput').value == '') {
            this.showDuplicateError = true;
            this.duplicateError = "Serial number is required";
            return;
        }
        const formData = {
            cycleCountId: this.cycleCountId,
            cycleCountIterationId: this.iterationId,
            cycleCountItemId: this.cycleCountStockItemsDetails.cycleCountItemId,
            serialNumber: this.jobSelectionCycleCountScanForm.get('scanSerialNumberInput').value,
            createdUserId: this.userId,
            itemId: this.cycleCountStockItemsDetails.cycleCountItemId,
        }

        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let crudService: Observable<IApplicationResponse> =
            this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.CYCLE_COUNT_SCAN_ITEMS, formData)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                if (response.resources.isItemCompleted || response.resources.isItemNotAvailable ||
                    response.resources.isduplicate) {
                    this.snackbarService.openSnackbar(response.resources.responsemsg, ResponseMessageTypes.ERROR);
                    return;
                }
                else {
                    this.jobSelectionCycleCountScanForm.get('scanSerialNumberInput').patchValue('');
                    this.callIterationAPIMethod();
                }
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rjxService.setGlobalLoaderProperty(false);
            }
        });
        //         this.cycleCountShow = true;
        // const formData = {
        //     cycleCountId: this.cycleCountId,
        //     cycleCountIterationId: this.iterationId,
        //     cycleCountItemId: this.cycleCountStockItemsDetails.cycleCountItemId,
        //     BinDetails:this.jobSelectionCycleCountScanForm.get('scanSerialNumberInput').value,
        //     createdUserId: this.userData.userId
        // }
        // this.crudService.get(
        //     ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_ITEM,
        //     undefined, false,
        //     prepareGetRequestHttpParams('0', '10', formData)
        //   ).subscribe(data => {
        //     if (data.isSuccess) {
        //         this.cycleCountShow = true;
        //     }
        //     this.rjxService.setGlobalLoaderProperty(false);
        //   });
    }

    callIterationAPIMethod() {
        let cancelPrams = {}
        cancelPrams = {
            "cycleCountId": this.cycleCountId,
            'cycleCountIterationId': this.iterationId,
            "createdUserId": this.userId
        }

        this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_ITERATION_PROCESS, cancelPrams)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.rjxService.setGlobalLoaderProperty(false);
                    if (response.resources.isCompleted) {
                        this.onModalPopupOpen();
                    }
                    else {
                        this.getDetailsItemById();
                    }
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rjxService.setGlobalLoaderProperty(false);
                }
            });
    }

    lockUnlockScanItems() {
        const lockUnlockItems = {
            cycleCountId: this.cycleCountId,
            cycleCountIterationId: this.iterationId,
            cycleCountItemId: this.cycleCountStockItemsDetails.cycleCountItemId,
            createdUserId: this.userId,
        }

        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let crudService: Observable<IApplicationResponse> =
            this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_ITEM_UNLOCK, lockUnlockItems)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.callIterationAPIMethod();
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rjxService.setGlobalLoaderProperty(false);
            }
        });
    }

    onModalPopupOpen() {
        const dialogRemoveStockCode = this.dialog.open(JobSelectionScanModalComponent, {
            width: '450px',
            data: {
                header: 'Success',
                message: `Count ${this.cycleCountDetails['iterationCount']} of Cycle Count ID <b>${this.cycleCountDetails['cycleCountNumber']}</b></br>
            completed successfully`,
                buttons: {
                    create: 'Ok'
                }
            }, disableClose: true
        });
        dialogRemoveStockCode.afterClosed().subscribe(result => {
            if (!result) return;
            this.navigateToCycleCountList();
            this.rjxService.setDialogOpenProperty(false);
        });
    }

    navigateToList() {
        this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
    }

    navigateToCycleCountList() {
        if (this.type == 'staff') {
            this.router.navigate(['/inventory', 'job-selection', 'cycle-count-list'], { skipLocationChange: true });
        }
        else {
            this.router.navigate(['/inventory', 'cycle-count-manager'], { skipLocationChange: true });
        }
    }
}
