import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import {
  CrudType,
  LoggedInUserModel,
  ModulesBasedApiSuffix,
  PERMISSION_RESTRICTION_ERROR,
  prepareGetRequestHttpParams,
  ResponseMessageTypes,
  RxjsService,
  SnackbarService,
} from "@app/shared";
import { CrudService } from "@app/shared/services";
import { InventoryModuleApiSuffixModels } from "@modules/inventory/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";

@Component({
  selector: "app-job-selection-cycle-count-list",
  templateUrl: "./job-selection-cycle-count-list.component.html",
  styleUrls: ["./job-selection-cycle-count.component.scss"],
})
export class JobSelectionCycleCountListComponent implements OnInit {
  userData: UserLogin;
  searchForm: FormGroup;
  observableResponse: any;
  dataList: any;
  status: any = [];
  selectedRows: string[] = [];
  selectedTabIndex: any = 0;
  totalRecords: any;
  listSubscribtion: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties: any;
  columnFilterForm: FormGroup;
  loading: boolean;
  row: any = {};
  first: any = 0;
  reset: boolean;
  isEdit = 0;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private crudService: CrudService,
    private store: Store<AppState>,
    private changeDetectorRef: ChangeDetectorRef,
    private snackbarService: SnackbarService
  ) {
    this.store
      .pipe(select(loggedInUserData))
      .subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      });

    this.primengTableConfigProperties = {
      tableCaption: "Cycle Count List",
      breadCrumbItems: [
        { displayName: "Job Selection", relativeRouterUrl: "" },
        { displayName: "Cycle Count List", relativeRouterUrl: "" },
      ],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Cycle Count List",
            dataKey: "cycleCountId",
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: "cycleCountNumber", header: "Cycle Count ID" },
              { field: "numberofIterations", header: "Count" },
              { field: "cycleCountDate", header: "Scheduled Date" },
              { field: "cycleCountDate", header: "Initiated Date & Time" },
              { field: "cycleCountStatusName", header: "Status" },
            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            shouldShowFilterActionBtn: false,
            apiSuffixModel: InventoryModuleApiSuffixModels.CYCLE_COUNT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          },
        ],
      },
    };
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.isEdit = +params["params"]["isEdit"];
    });
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    this.router.navigate(["/inventory/job-selection"], {
      queryParams: { tab: this.selectedTabIndex },
    });
    this.getRequiredListData();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
  }

  ngAfterViewInit(): void {
    // this.componentProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].cursorLinkIndex = 1
    this.changeDetectorRef.detectChanges();
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(this.store.select(loggedInUserData)).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  getRequiredListData(
    pageIndex?: string,
    pageSize?: string,
    otherParams?: object
  ) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels =
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[
        this.selectedTabIndex
      ].apiSuffixModel;
    this.crudService
      .get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels,
        undefined,
        false,
        prepareGetRequestHttpParams(
          pageIndex,
          pageSize,
          otherParams
            ? otherParams
            : {
              userId: this.userData.userId,
            }
        )
      )
      .subscribe((data) => {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (data.isSuccess) {
          this.observableResponse = data.resources;
          this.dataList = this.observableResponse;
          this.totalRecords = data.totalCount;
        } else {
          this.observableResponse = null;
          this.dataList = this.observableResponse;
          this.totalRecords = data.totalCount;
        }
      });
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        unknownVar["userId"] = this.userData.userId;
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first =
          row?.pageIndex && row?.pageSize
            ? row["pageIndex"] * row["pageSize"]
            : 0;
        this.getRequiredListData(
          this.row["pageIndex"],
          this.row["pageSize"],
          unknownVar
        );
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  openAddEditPage(
    type: CrudType | string,
    editableObject?: object | string,
    index?: number
  ): void {
    switch (type) {
      case CrudType.VIEW:
        if (!this.isEdit) {
          return this.snackbarService.openSnackbar(
            PERMISSION_RESTRICTION_ERROR,
            ResponseMessageTypes.WARNING
          );
        }
        this.router.navigate(
          ["/inventory", "job-selection", "cycle-count-scan"],
          {
            queryParams: {
              userId: this.userData.userId,
              id: editableObject["cycleCountId"],
              iterationId: editableObject["cycleCountIterationId"],
              type: "staff",
            },
            skipLocationChange: true,
          }
        );
        break;
    }
  }

  navigateToJobSelection() {
    this.router.navigate(["/inventory", "job-selection"], {
      skipLocationChange: true,
    });
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }
}
