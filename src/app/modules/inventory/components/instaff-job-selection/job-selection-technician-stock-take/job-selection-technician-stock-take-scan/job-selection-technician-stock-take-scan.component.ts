import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { JobSelectionTechStockTakeAddEditModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { JobSelectionScanModalComponent } from '../../job-selection-list';


@Component({
  selector: 'app-job-selection-technician-stock-take-scan',
  templateUrl: './job-selection-technician-stock-take-scan.component.html'
})
export class JobSelectionTechnicianStockTakeScanComponent implements OnInit {

  technicianStockTakeDetails: any;
  techStockTakeInfoDetails: any;
  userId: any;
  techStockTakeId: any;
  techStockIterationId: any;
  type: any;
  technicianStockTakeScanForm: FormGroup;
  showManager: boolean;
  quantity: any;
  duplicateError: any;
  showDuplicateError: boolean = false;
  showInitialScan: boolean = false;
  userData: UserLogin;
  packerJobScanItemDetails: any;

  constructor(
    private store: Store<AppState>, private router: Router,
    private crudService: CrudService, private formBuilder: FormBuilder,
    private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private dialog: MatDialog,
  ) {
    this.techStockTakeId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.createtechnicianStockTakeScanForm();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  checkInitialScanItem(): void {
    this.showDuplicateError = false;
    this.duplicateError = '';
    if (this.technicianStockTakeScanForm.get('scanSerialNumberInput').value == '') {
      this.showDuplicateError = true;
      this.duplicateError = "Serial number is required";
      return;
    }

    let params = new HttpParams().set('TechStockTakeId', this.techStockTakeId)
    .set('QRCode', this.technicianStockTakeScanForm.get('scanSerialNumberInput').value)
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE_VALIDATE_QRCODE, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.showInitialScan = true;
          this.getSelectionJobsHeaderDetailsById();
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getSelectionJobsHeaderDetailsById(): void {
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE, this.techStockTakeId)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.technicianStockTakeDetails = response.resources;
          this.techStockIterationId = response.resources.techStockTakeIterationId;
          this.getSelectionJobsDetailsById('GET');
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getSelectionJobsDetailsById(type: any) {

    let params;
    if (this.techStockIterationId == undefined) return;
    params = Object.assign({},
      this.techStockTakeId == '' ? null : { TechStockTakeId: this.techStockTakeId },
      this.techStockIterationId == '' ? null : { TechStockTakeIterationId: this.techStockIterationId },
      this.userData.userId == '' ? null : { CreatedUserId: this.userData.userId },
      this.technicianStockTakeScanForm.get('serialNumber').value == null || this.technicianStockTakeScanForm.get('serialNumber').value == '' ? null :
      { SerialNumber: this.technicianStockTakeScanForm.get('serialNumber').value },
      this.technicianStockTakeScanForm.get('itemId').value == null || this.technicianStockTakeScanForm.get('itemId').value == '' ? null :
      { ItemId: this.technicianStockTakeScanForm.get('itemId').value },
      { IsNext: type == 'GET' ? false : true },
    );
      
    this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE_STOCK_DETAILS, undefined, true, 
      prepareRequiredHttpParams(params)).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.techStockTakeInfoDetails = response.resources.techStockTakeStockDetails;
          let stockOrderModel = new JobSelectionTechStockTakeAddEditModel(response.resources.techStockTakeStockDetails);
          this.technicianStockTakeScanForm.patchValue(stockOrderModel);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  createtechnicianStockTakeScanForm(): void {
    let stockOrderModel = new JobSelectionTechStockTakeAddEditModel();
    // create form controls dynamically from model class
    this.technicianStockTakeScanForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.technicianStockTakeScanForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
  }

  checkScanItem() {
    this.showDuplicateError = false;
    if (this.technicianStockTakeScanForm.get('serialNumber').value == '') {
      this.showDuplicateError = true;
      this.duplicateError = "Serial number is required";
      return;
    }

    let sendParams = {
      "techStockTakeItemId": this.technicianStockTakeScanForm.get('techStockTakeItemId').value,
      "itemId": this.techStockTakeInfoDetails.itemId,
      "techStockTakeId": this.techStockTakeInfoDetails.techStockTakeId,
      "techStockTakeIterationId": this.techStockTakeInfoDetails.techStockTakeIterationId,
      "serialNumber": this.technicianStockTakeScanForm.get('serialNumber').value,
      "scannedItemCount": this.technicianStockTakeScanForm.get('scannedQty').value,
      "createdUserId": this.userData.userId,
    }

    let crudService: Observable<IApplicationResponse> = this.crudService.create(
      ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE_SCANNED_ITEM_POST, sendParams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.getSelectionJobsDetailsById('GET');
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  saveIterations() {
    let sendParams = {
      "techStockTakeId": this.techStockTakeId,
      "techStockTakeIterationId": this.techStockIterationId,
      "createdUserId": this.userData.userId,
    }

    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.TECHNICIAN,
        InventoryModuleApiSuffixModels.TECHNICIAN_STOCK_TAKE_ITERATION_UPDATE, sendParams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.onModalPopupOpen(response.resources.errormsg)
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  onModalPopupOpen(message: string) {
    const dialogRemoveStockCode = this.dialog.open(JobSelectionScanModalComponent, {
      width: '450px',
      data: {
        header: `Success`,
        message: message,
        buttons: {
          create: 'Ok'
        }
      }, disableClose: true
    });
    dialogRemoveStockCode.afterClosed().subscribe(result => {
      if (!result) return;
      this.navigateToTechStockTakeList();
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
  }

  navigateToTechStockTakeList() {
    this.router.navigate(['/inventory', 'job-selection', 'tech-stock-take-list'], { skipLocationChange: true });
  }

  managerOverridePopup(val) { }
}
