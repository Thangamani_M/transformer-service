import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { JobSelectionAuditCycleCountAddEditModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { JobSelectionScanModalComponent } from '../job-selection-list';

@Component({
    selector: 'app-job-selection-new-count',
    templateUrl: './job-selection-new-count.component.html',
    // styleUrls: ['./storage-location-add-edit.component.scss'],
})

export class JobSelectionNewCountComponent implements OnInit {

    jobSelectionNewCountScanForm: FormGroup;
    showVarianceReport: boolean = false;
    showVarianceDetails: boolean = false;
    varianceReportsDropdown: any = [];
    jobNewCountId: any;
    userData: UserLogin;
    newCountItemsDetails: any = {};
    filteredStockCodes: any = [];
    stockCodeNewCountItems: boolean = false;
    isNotSerialized: boolean = false;
    varianceReportNewCountItemId: any;
    showStockCodeError: boolean;
    showDuplicateError: boolean;
    stockCodeErrorMessage: any;
    isLoading: boolean;
    duplicateError: any;

    constructor(
        private rxjsService: RxjsService,
        private crudService: CrudService, private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder, private router: Router, private dialog: MatDialog,
        private httpCancelService: HttpCancelService, private snackbarService: SnackbarService,
        private store: Store<AppState>,
    ) {
        this.jobNewCountId = this.activatedRoute.snapshot.queryParams.id;
        this.showVarianceReport = true;
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
    }

    ngOnInit() {
        this.createJobselectionNewCountForm();
        this.getDropdown();
    }

    createJobselectionNewCountForm(): void {
        let stockOrderModel = new JobSelectionAuditCycleCountAddEditModel();
        // create form controls dynamically from model class
        this.jobSelectionNewCountScanForm = this.formBuilder.group({});
        Object.keys(stockOrderModel).forEach((key) => {
            this.jobSelectionNewCountScanForm.addControl(key, new FormControl(stockOrderModel[key]));
        });
    }

    getDropdown() {
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_VARIANCE_REPORT)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.varianceReportsDropdown = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    getNewCountDetailsById() {
        let params = new HttpParams().set('VarianceReportId', this.jobSelectionNewCountScanForm.get('varianceReportId').value)
            .set('CreatedUserId', this.userData.userId)
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_REPORT_NEW_COUNT_DETAILS, undefined, true, params)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.newCountItemsDetails = response.resources;
                    this.showVarianceDetails = true;
                    this.showVarianceReport = false;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    inputChangeStockCodeFilter() {
        this.jobSelectionNewCountScanForm.get('stockCode').valueChanges.subscribe(codes => {
            if (codes != '') {
                let codeParams = new HttpParams().set('VarianceReportId', this.newCountItemsDetails.varianceReportId)
                    .set('ItemCode', this.jobSelectionNewCountScanForm.get('stockCode').value);
                this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_VARIANCE_REPORT_NEW_COUNT, undefined, true, codeParams)
                    .subscribe((response: IApplicationResponse) => {
                        if (response.isSuccess && response.statusCode == 200) {
                            this.filteredStockCodes = response.resources;
                            this.rxjsService.setGlobalLoaderProperty(false);
                        }
                        else {
                            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                            this.rxjsService.setGlobalLoaderProperty(false);
                        }
                    });
            }
        });
    }

    onStockCodeSelected(obj: any) {
        this.varianceReportNewCountItemId = obj.id;
        this.jobSelectionNewCountScanForm.get('stockCode').patchValue(obj.displayName);
        let selectedParams = new HttpParams().set('VarianceReportNewCountItemId', obj.id)
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.VARIANCE_REPORT_NEW_COUNT_ITEMS_DETAILS, undefined, true, selectedParams)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    let responseData = response.resources;
                    responseData.isNotSerialized ? this.isNotSerialized = true : false;
                    this.stockCodeNewCountItems = true;
                    this.jobSelectionNewCountScanForm.get('countedQty').patchValue(responseData.scannedQty);
                    this.jobSelectionNewCountScanForm.get('itemCode').patchValue(responseData.itemCode);
                    this.jobSelectionNewCountScanForm.get('itemDescription').patchValue(responseData.displayName);
                    if (this.isNotSerialized) {
                        this.jobSelectionNewCountScanForm = setRequiredValidator(this.jobSelectionNewCountScanForm, ['countedQty']);
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    scanSerialNumber() {

        if (this.varianceReportNewCountItemId == undefined) {
            return;
        }
        let formData = {};
        formData = {
            "varianceReportNewCountItemId": this.varianceReportNewCountItemId,
            "serialNumber": this.jobSelectionNewCountScanForm.get('scanSerialNumberInput').value,
            "countedQty": !this.isNotSerialized ? 1 :
                this.jobSelectionNewCountScanForm.get('countedQty').value,
            "isNotSerialized": this.isNotSerialized,
            "createdUserId": this.userData.userId,
        }

        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let crudService: Observable<IApplicationResponse> =
            this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.VARIANCE_REPORT_NEW_COUNT_SCAN, formData)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                if (response.resources != null) {
                    let responseValue = response.resources[0];
                    let sendData = {
                        id: responseValue.VarianceReportNewCountItemId,
                        displayName: this.jobSelectionNewCountScanForm.get('stockCode').value
                    }
                    this.onStockCodeSelected(sendData);
                    if (responseValue.IsCompleted || responseValue.IsDuplicate || responseValue.IsError) {
                        this.snackbarService.openSnackbar(responseValue.Responsemsg, ResponseMessageTypes.ERROR);
                        return;
                    }
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

    nextStockCodeChangeFun() {
        this.showVarianceReport = false;
        this.showVarianceDetails = true;
        this.stockCodeNewCountItems = false;
        this.jobSelectionNewCountScanForm.reset();
    }

    onModalPopup() {
        const dialogRemoveStockCode = this.dialog.open(JobSelectionScanModalComponent, {
            width: '450px',
            data: {
                header: 'Success',
                message: `New Count For Stock Code <b>${this.jobSelectionNewCountScanForm.get('itemCode').value}</b> on Variance</br>
            Report <b>${this.newCountItemsDetails['varianceReportNumber'] }</b> completed successfully`,
                buttons: {
                    create: 'Ok'
                }
            }, disableClose: true
        });
        dialogRemoveStockCode.afterClosed().subscribe(result => {
            if (result && typeof result == 'boolean') {
                this.navigateToJobSelection();
                this.rxjsService.setDialogOpenProperty(false);
            }
        });
    }

    navigateToJobSelection() {
        this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
    }

}