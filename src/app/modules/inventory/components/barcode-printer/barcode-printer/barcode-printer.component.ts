import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-barcode-printer',
  templateUrl: './barcode-printer.component.html'
})
export class BarcodePrinterComponent implements OnInit {

  @Input()  printcodes: any[] = [];

  elementType = 'svg';
  value = 'someValue12340987';
  format = 'CODE128';
  lineColor = '#000000';
  width = 3;
  height = 50;
  displayValue = true;
  fontOptions = '';
  font = 'monospace';
  textAlign = 'center';
  textPosition = 'bottom';
  textMargin = 2;
  fontSize = 20;
  background = '#ffffff';
  margin = 10;
  marginTop = 10;
  marginBottom = 10;
  marginLeft = 10;
  marginRight = 10;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes:SimpleChanges){
    if (changes?.printcodes?.currentValue != changes?.printcodes?.previousValue) {
       this.printcodes = changes?.printcodes?.currentValue;
    }
  }

}
