import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgxBarcodeModule } from 'ngx-barcode';
import { BarcodePrinterComponent } from './barcode-printer.component';

@NgModule({
  declarations: [BarcodePrinterComponent],
  imports: [
    CommonModule,
    NgxBarcodeModule,
  ], 
  exports: [
    BarcodePrinterComponent
  ]
})

export class BarcodePrinterModule { }
