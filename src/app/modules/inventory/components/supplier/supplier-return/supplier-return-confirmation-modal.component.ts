import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RxjsService } from '@app/shared';


@Component({
    selector: 'app-supplier-return-confirmation-modal',
    templateUrl: './supplier-return-confirmation-modal.component.html',
})

export class SupplierReturnConfirmationModalComponent implements OnInit {

    reasonText = '';

    constructor(
        @Inject(MAT_DIALOG_DATA) public data,
        private rxjsService: RxjsService
    ) { }

    ngOnInit(): void {
        let messageSpan = document.createElement('span');
        messageSpan.innerHTML = this.data['message'];
        document.getElementById('parentContainer').appendChild(messageSpan);
        this.rxjsService.setDialogOpenProperty(true);
    }
    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }

}