import { HttpParams } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CustomDirectiveConfig, formConfigs, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, PrimengConfirmDialogPopupComponent, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { StaffMonitoringModule } from '@modules/event-management/components/event-supervisor-dashboard/staff-monitoring/staff-monitoring.module';
import { newAddressFormAddEditModal, supplierItemDetails, SupplierReturnCreateModel, supplierReturnStockItemsDetails } from '@modules/inventory/models/supplier-retrun.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { SalesModuleApiSuffixModels } from '@modules/sales';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-supplier-return',
  templateUrl: './supplier-return.component.html',
  styleUrls: ['./supplier-return.component.scss']
})
export class SupplierReturnComponent implements OnInit {
  supplierAddEditForm: FormGroup;
  items: FormArray;
  itemDetails: FormArray;
  userData: UserLogin;
  supplierReturnId: string = '';
  filteredPONumbers: any = [];
  stockInfoDetailDialog: boolean = false;
  supplierItemStockDetails: any = [];
  selectedPoNumbers = [];
  showBarCodeItemError: boolean = false;
  showSerialItemError: boolean = false;
  getSupplierReturnResponse: any = {};
  filteredSuppliers: any = [];
  showSupplierError: Boolean = false;
  suppType: string;
  newAddressForm: FormGroup;
  countryId = formConfigs.countryId;
  filteredCites: any = [];
  isLoading: boolean;
  filteredSuburbs: any = [];
  provinceList: any = [];
  numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
  primengTableConfigProperties;
  isEnableSubmit: boolean = false;
  selectedTabIndex = 0;
  constructor(private store: Store<AppState>, private crudService: CrudService, private dialog: MatDialog, private router: Router, private dialogService: DialogService, private changeDetectorRef: ChangeDetectorRef,
    private snackBarService: SnackbarService, private rxjsService: RxjsService,
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.supplierReturnId = this.activatedRoute.snapshot.queryParams.id;
    this.suppType = this.activatedRoute.snapshot.queryParams.type;
    let title = this.supplierReturnId ? 'Update' : 'Create';
    this.primengTableConfigProperties = {
      tableCaption: title + " Supplier Return Requests",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Stock Return', relativeRouterUrl: '' }, { displayName: 'Return to Supplier', relativeRouterUrl: '/inventory/suppliers/return' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: title + " Supplier Return Requests",
            dataKey: 'supplierId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAction: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            cursorLinkIndex: 0
          },
        ]
      }
    }
    if (this.supplierReturnId)
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: "View Supplier Return Requests", relativeRouterUrl: '/inventory/suppliers/return/return-view', queryParams: { id: this.supplierReturnId } });

    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title + " Supplier Return Requests", relativeRouterUrl: '' });

  }

  ngOnInit() {
    this.createSupplierRetrunForm();
    this.getPoNumberList();
    if (this.supplierReturnId) {
      this.getSelectedValue(this.supplierReturnId, 'get')
    }
    this.createNewAddressForm();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createSupplierRetrunForm() {
    let stockOrderModel = new SupplierReturnCreateModel();
    this.supplierAddEditForm = this.formBuilder.group({
      items: this.formBuilder.array([]),
    });
    Object.keys(stockOrderModel)?.forEach((key) => {
      this.supplierAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.supplierAddEditForm = setRequiredValidator(this.supplierAddEditForm, ['orderReceiptId', 'supplierAddress']);
  }

  /* Create FormArray controls */
  createItemsInfoGroupForm(stockItemsDetails?: supplierReturnStockItemsDetails): FormGroup {
    let serialInfoData = new supplierReturnStockItemsDetails(stockItemsDetails ? stockItemsDetails : undefined);
    let formControls = {};
    Object.keys(serialInfoData)?.forEach((key) => {
      if (key === 'itemDetails') {
        formControls[key] = this.formBuilder.array([])
      } else {
        formControls[key] = [{ value: serialInfoData[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  get getOrderReceiptItemsFormArray(): FormArray {
    if (this.supplierAddEditForm !== undefined) {
      return this.supplierAddEditForm.get("items") as FormArray;
    }
  }

  //Create FormArray
  getSupplierItemsList(index: number): FormArray {
    if (!this.supplierAddEditForm) return;
    return this.items.at(index).get("itemDetails") as FormArray;
  }

  /* Create FormArray controls */
  createItemsDetailsGroupForm(discrepancyDocuments?: supplierItemDetails): FormGroup {
    let serialInfoData = new supplierItemDetails(discrepancyDocuments ? discrepancyDocuments : undefined);
    let formControls = {};
    Object.keys(serialInfoData).forEach((key) => {
      formControls[key] = [{ value: serialInfoData[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  getPoNumberList() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_SUPPLIER_RETURN_PURCHASE_ORDER, undefined, false,
      prepareRequiredHttpParams({
        UserId: this.userData.userId,
      })).subscribe((response: any) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources?.length > 0) {
            let PoNumbers = response.resources;
            this.filteredPONumbers = [];
            for (var i = 0; i < PoNumbers?.length; i++) {
              let tmp = {};
              tmp['value'] = PoNumbers[i].id;
              tmp['display'] = PoNumbers[i].displayName;
              this.filteredPONumbers.push(tmp);
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackBarService.openSnackbar(response?.message, ResponseMessageTypes.WARNING);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });

    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_PROVINCES,
      undefined, false, prepareRequiredHttpParams({
        countryId: this.countryId
      })).subscribe(resp => {
        if (resp.isSuccess && resp.statusCode === 200) {
          this.provinceList = resp.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackBarService.openSnackbar(resp?.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  /* PO Numbers dropdown */
  onPONumbersSelected(obj: any) {

    if (obj[0].id == null) return;
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.SUPPLIER_RETURN_REQUEST_NEW_PO_DETAILS,
      undefined, false, prepareRequiredHttpParams({
        POBarCode: obj[0].id,
      }))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess && response.statusCode == 200 && response.resources != null) {
          this.getSupplierDetails(response.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackBarService.openSnackbar(response?.message, ResponseMessageTypes.WARNING);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getSelectedValue(reference: string, type: string, inputType?: string) {
    if (inputType == 'poNum') {//if po number is selcted clear all  sub list and set value
      if (this.getOrderReceiptItemsFormArray.value && this.getOrderReceiptItemsFormArray.value.length > 0) {
        this.getOrderReceiptItemsFormArray.value.forEach((ele, index) => {
          this.getOrderReceiptItemsFormArray.removeAt(index);
        });
      }
    }
    if (reference != '' && reference != null) {
      let apiSuffixModels = type == 'dropdown' ?
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.SUPPLIER_RETURN_REQUEST_NEW_PO_DETAILS,
          undefined, false, prepareRequiredHttpParams({ OrderReceiptId: reference })) :
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.SUPPLIER_RETURN_REQUEST_NEW, reference)
      apiSuffixModels.subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess && response.statusCode == 200 && response.resources != null) {

          if (inputType == 'poNum') { //if po number is selcted clear all and set value
            this.supplierAddEditForm.reset();
            this.supplierAddEditForm.get('isRequestType').setValue(true);
          }
          this.getSupplierDetails(response.resources)
        }
        else {
          this.snackBarService.openSnackbar(response?.message, ResponseMessageTypes.WARNING);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }
  searchPOBarCode() {
    this.showBarCodeItemError = false;
    if (this.supplierAddEditForm.get('poBarcode').value == '') {
      this.showBarCodeItemError = true;
      return;
    }
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.SUPPLIER_RETURN_REQUEST_NEW_PO_DETAILS,
      undefined, false, prepareRequiredHttpParams({
        POBarCode: this.supplierAddEditForm.get('poBarcode').value,
      }))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess && response.statusCode == 200) {

          if (this.getOrderReceiptItemsFormArray != undefined && this.getOrderReceiptItemsFormArray.value.length > 0) {
            this.getOrderReceiptItemsFormArray.clear();
          }
          this.getSupplierDetails(response.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackBarService.openSnackbar(response?.message, ResponseMessageTypes.WARNING);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }
  print() {

     let data = {
       SupplierReturnId : this.supplierReturnId,
       userId: this.userData.userId
     }
     this.crudService.create(ModulesBasedApiSuffix.PDF, InventoryModuleApiSuffixModels.SUPPLIER_RETURN_GENERATE_PDF_DETAIL, data)
     .subscribe((response: IApplicationResponse) => {
       if (response.isSuccess && response.statusCode === 200 && response.resources) {
         window.open(response.resources, '_blank');
       }
       this.rxjsService.setGlobalLoaderProperty(false);
       this.navigateToList();
     });
   }
  scanSerialItem() {
    this.showSerialItemError = false;
    if (this.supplierAddEditForm.get('scanBarCodeItem').value == '') {
      this.showSerialItemError = true;
      return;
    }
    let serialChecked: boolean = false;
    let quanChecked: boolean = false;
    if (this.getOrderReceiptItemsFormArray.value?.length > 0) {
      this.getOrderReceiptItemsFormArray.value.forEach(items => {
        if (items.receivedQuantity < items.returnQuantity) {
          quanChecked = true;
        }
        items?.itemDetails?.forEach(det => {
          if (det.serialNumber == this.supplierAddEditForm.get('scanBarCodeItem').value) {
            serialChecked = true;
          }
        });
      });
      if (serialChecked) {
        this.snackBarService.openSnackbar('Serial number already scanned', ResponseMessageTypes.WARNING);
        return;
      }
      if (quanChecked) {
        this.snackBarService.openSnackbar('Return quantity cannot be more than received quantity', ResponseMessageTypes.WARNING);
        return;
      }
    }
    let params;
    this.getOrderReceiptItemsFormArray.value?.length == 0 ?
      params = new HttpParams().set('SerialNumber', this.supplierAddEditForm.get('scanBarCodeItem').value) :
      params = new HttpParams().set('SerialNumber', this.supplierAddEditForm.get('scanBarCodeItem').value)
        .set('PurchaseOrderId', this.getSupplierReturnResponse?.purchaseOrderId);

    let apiSuffixModels = InventoryModuleApiSuffixModels.SUPPLIER_RETURN_REQUEST_NEW_PO_DETAILS;
    //this.getOrderReceiptItemsFormArray.value?.length == 0 ? 
    // InventoryModuleApiSuffixModels.SUPPLIER_RETURN_REQUEST_NEW_SERIAL_NUMBER_SCAN
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, apiSuffixModels, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {


        if (response.isSuccess && response.statusCode == 200) {
          if (response.resources) {
            this.snackBarService.openSnackbar('Scanned successfully', ResponseMessageTypes.SUCCESS);

            if (this.getOrderReceiptItemsFormArray.value?.length == 0) {
              this.getSupplierDetails(response.resources);
              this.getOrderReceiptItemsFormArray?.controls?.forEach(data => {
                if (data.get('itemId').value == response.resources.items[0].itemId) {
                  data.get('itemDetails').value.push(this.supplierAddEditForm.get('scanBarCodeItem').value);
                }
              });
            } else {
              this.scanSerialDetails(response.resources);
            }

          } else {
            this.snackBarService.openSnackbar(response?.message, ResponseMessageTypes.ERROR);
          }
          this.supplierAddEditForm.get('scanBarCodeItem').setValue(null);
          // if(this.getOrderReceiptItemsFormArray.value?.length)
          // this.scanSerialItem();
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackBarService.openSnackbar(response?.message, ResponseMessageTypes.WARNING);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  // showItemsArray: boolean = false;
  locationPoint: any;
  getSupplierDetails(response: any) {
    if (response != null) {
      this.getSupplierReturnResponse = response;
      this.supplierAddEditForm.patchValue(response);
      let lat = response.latitude == null ? '' : response.latitude;
      let long = response.longitude == null ? '' : response.longitude;
      this.locationPoint = lat + ',' + long;
      this.supplierAddEditForm.get('locationPoint').patchValue(this.locationPoint);
      this.showQuantityError = false;
      this.inputChangeSupplierFilter();
      this.supplierAddEditForm.get('isNew').patchValue(true);
      this.supplierAddEditForm.get('createdUserId').patchValue(this.userData.userId);
      if (this.supplierReturnId && !this.getSupplierReturnResponse.isDraft && this.getSupplierReturnResponse.status != 'For Supplier') {
        this.supplierAddEditForm.get('orderReceiptId').disable();
        this.supplierAddEditForm.get('poBarcode').disable();
        this.supplierAddEditForm.get('scanBarCodeItem').disable();
        this.supplierAddEditForm.get('supplierAddress').disable();
      }
      if (this.supplierReturnId && this.getSupplierReturnResponse.isDraft) {
        this.supplierAddEditForm.get('orderReceiptId').disable();
        this.supplierAddEditForm.get('poBarcode').disable();
      }
      this.items = this.getOrderReceiptItemsFormArray;
      if (this.getOrderReceiptItemsFormArray != undefined && this.getOrderReceiptItemsFormArray.value.length > 0) {
        this.getOrderReceiptItemsFormArray.clear();
      }

      if (response?.items.length > 0) {
        response?.items.forEach((element: any, index: number) => {

          this.items.push(this.createItemsInfoGroupForm(element));

          if (element.itemDetails?.length > 0) {
            element.itemDetails.push(this.supplierAddEditForm.get('scanBarCodeItem').value);
            // element.itemDetails.forEach((value: supplierItemDetails, i: number) => {
            //   this.itemDetails = this.getSupplierItemsList(index);                                                     
            //   this.itemDetails.insert(index, this.createItemsDetailsGroupForm(value));
            // });
          }
        });

        this.isEnableSubmit = false;
      }
      else {
        // this.showItemsArray = true;
        this.isEnableSubmit = true;
        this.rxjsService.setGlobalLoaderProperty(false);
        //warehouseName supplierName //supplierAddressId locationPoint
        this.supplierAddEditForm.get('warehouseName').setValue(null);
        this.supplierAddEditForm.get('supplierName').setValue(null);
        this.supplierAddEditForm.get('supplierAddressId').setValue('');
        this.supplierAddEditForm.get('locationPoint').setValue(null);
        this.supplierAddEditForm.get('poBarcode').setValue(null);
        this.snackBarService.openSnackbar('No stock received on the PO.', ResponseMessageTypes.WARNING);
        return;
      }
    }
    this.newAddressForm.get('supplierId').patchValue(response.supplierId);
  }

  inputChangeSupplierFilter() {
    let otherParams = {}
    this.showSupplierError = false;
    if (this.getSupplierReturnResponse.supplierId == null) return;
    otherParams['supplierId'] = this.getSupplierReturnResponse.supplierId;
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIER_RETURN_SEARCH_ADDRESS, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources.length > 0) {
          let suppliers = response.resources;
          this.filteredSuppliers = [];
          for (var i = 0; i < suppliers?.length; i++) {
            let tmp = {};
            tmp['value'] = suppliers[i].supplierAddressId;
            tmp['display'] = suppliers[i].description;
            tmp['latitude'] = suppliers[i].latitude;
            tmp['longitude'] = suppliers[i].longitude;
            this.filteredSuppliers.push(tmp);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.showSupplierError = true;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  /* Supplier Address dropdown */
  onSupplierAddressSelected(obj: any) {
    this.supplierAddEditForm.controls['latitude'].patchValue(''),
      this.supplierAddEditForm.controls['longitude'].patchValue(''),
      this.supplierAddEditForm.controls['supplierAddressId'].patchValue('')
    let locationData = this.filteredSuppliers.filter(function (data) {
      if (data.value == obj) {
        return true
      } else {
        return false
      }
    });
    if (locationData?.length > 0) {
      let locationPoint = locationData[0].latitude == null ? '' : locationData[0].latitude + ',' + locationData[0].longitude == null ? '' : locationData[0].longitude;
      this.supplierAddEditForm.get('locationPoint').setValue(locationPoint);
      this.supplierAddEditForm.get('latitude').setValue(locationData[0].latitude);
      this.supplierAddEditForm.get('longitude').setValue(locationData[0].longitude);
      this.supplierAddEditForm.get('supplierAddressId').setValue(locationData[0].value);
    }
  }

  ngAfterViewChecked() {
    this.changeDetectorRef.detectChanges();
  }

  scanSerialDetails(response: any) {
    // if(response.isSuccess){
    let getItems = this.getOrderReceiptItemsFormArray.value.filter(x => x.itemId == response.items[0].itemId);
    if (getItems?.length > 0) {
      let items: supplierItemDetails = {
        supReturnItemDetailId: getItems.supReturnItemDetailId,
        supReturnItemId: getItems.supReturnItemId,
        serialNumber: response.serialNumber,
        barcode: response.serialNumber,
      };
      let serialChecked: boolean = false;

      this.getOrderReceiptItemsFormArray?.controls?.forEach((data,index) => {

        if (data.get('itemId').value == response.itemId) {
        
          serialChecked = true;
 
          if(data.get('itemCode').value == response.itemCode) {
            data.get('itemDetails').value.push(this.supplierAddEditForm.get('scanBarCodeItem').value);  
          }

          if (parseInt(data.get('receivedQuantity').value) > parseInt(data.get('returnQuantity').value)) {
            data.get('returnQuantity').setValue(data.get('returnQuantity').value + 1);
          }
        }
      });
      if (serialChecked) {
        this.snackBarService.openSnackbar('Scanned successfully', ResponseMessageTypes.SUCCESS);
        this.supplierAddEditForm.get('scanBarCodeItem').patchValue('');
        return;
      }
    }
    else if (response?.items[0].itemId && (getItems?.length == 0 || !getItems)) {
      this.insertNewStock(response)
    }
    // }
    // else {
    //   this.snackBarService.openSnackbar(response?.message, ResponseMessageTypes.WARNING);
    //   this.rxjsService.setGlobalLoaderProperty(false); 
    // }
  }
  insertNewStock(response) {
    // this.items = this.getOrderReceiptItemsFormArray;
    if (response.items?.length > 0) {
      response.items.forEach((element: any, index: number) => {
        this.getOrderReceiptItemsFormArray.push(this.createItemsInfoGroupForm(element));
        this.getOrderReceiptItemsFormArray?.controls?.forEach(data => {
          if (data.get('itemId').value == response.items[0].itemId) {
            data.get('itemDetails').value.push(this.supplierAddEditForm.get('scanBarCodeItem').value);
          }
        });
        // element.itemDetails.push(this.supplierAddEditForm.get('scanBarCodeItem').value);
      });
      this.isEnableSubmit = false;
    }
  }
  selectedPopupIndex: number = 0;
  openStockModal(data: any, i: number) {
    this.supplierItemStockDetails = data.value.itemDetails;
    this.selectedPopupIndex = i;
    this.stockInfoDetailDialog = true;
  }

  removeSerialNumber(id: string, index: number) {
    if (index == undefined) return;
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialogService.open(PrimengConfirmDialogPopupComponent, {
      width: "400px",
      data: { ...dialogData, msgCenter: true },
      showHeader: false,
      baseZIndex: 1000
    });
    dialogRef.onClose.subscribe(dialogResult => {
      if (dialogResult) {
        if (id) {
          this.rxjsService.setFormChangeDetectionProperty(true);
          this.crudService.delete(ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.SUPPLIERRETURN_DELETE_SERIAL_NUMBER, undefined,
            prepareRequiredHttpParams({
              ids: id,
              modifiedUserId: this.userData.userId
            })).subscribe({
              next: response => {
                if (response.isSuccess && response.statusCode == 200) {
                  this.supplierItemStockDetails.splice(index, 1);
                  let returnQuantity = this.getOrderReceiptItemsFormArray.controls[this.selectedPopupIndex].get('returnQuantity').value;
                  this.getOrderReceiptItemsFormArray.controls[this.selectedPopupIndex].get('returnQuantity').setValue(returnQuantity - 1);
                  this.rxjsService.setGlobalLoaderProperty(false);
                }
              },
              error: err => this.errorMessage = err
            });
        }
        else {
          this.supplierItemStockDetails.splice(index, 1);
          let returnQuantity = this.getOrderReceiptItemsFormArray.controls[this.selectedPopupIndex].get('returnQuantity').value;
          this.getOrderReceiptItemsFormArray.controls[this.selectedPopupIndex].get('returnQuantity').setValue(returnQuantity - 1);
        }
      }
    });

  }

  showNewAddressPopupModal: boolean = false;
  /* Add New Address Start */

  openAddressPopup() {
    this.showNewAddressPopupModal = true;
  }

  createNewAddressForm(): void {
    let newAddressModel = new newAddressFormAddEditModal();
    this.newAddressForm = this.formBuilder.group({});
    Object.keys(newAddressModel)?.forEach((key) => {
      this.newAddressForm.addControl(key, new FormControl(newAddressModel[key]));
    });
    this.newAddressForm = setRequiredValidator(this.newAddressForm, ["streetNumber", "streetName", "provinceId", "cityArray", "suburbArray", "postalCode"]);
  }

  inputChangeCityFilter(text: any, type: string) {
    let otherParams = {}
    if (text == null || text == '') return;
    let provinceId = this.newAddressForm.controls.provinceId.value;
    if (provinceId == '' || provinceId == null) {
      this.newAddressForm.get('provinceId').setValidators([Validators.required]);
      this.newAddressForm.get('provinceId').updateValueAndValidity();
      this.newAddressForm.get('provinceId').markAllAsTouched();
    }
    else {
      this.newAddressForm.get('provinceId').setErrors(null);
      this.newAddressForm.get('provinceId').clearValidators();
      this.newAddressForm.get('provinceId').updateValueAndValidity();
    }

    otherParams['SearchText'] = text;
    otherParams['ProvinceId'] = provinceId;

    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT,
      type == 'city' ? SalesModuleApiSuffixModels.SALES_API_CITIES : SalesModuleApiSuffixModels.SALES_API_SUBURBS, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response?.statusCode == 200 && response?.isSuccess) {
          type == 'city' ? this.filteredCites = [] : this.filteredSuburbs = [];
          type == 'city' ? this.filteredCites = response.resources : this.filteredSuburbs = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackBarService.openSnackbar(response?.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  /* Add New Address End */

  selectedIndex: number = 0;
  showQuantityError: boolean = false;
  onChangeQuantity(obj, index) {
    this.showQuantityError = false;
    this.selectedIndex = index;
    if (obj?.value.receivedQuantity < obj?.value?.returnQuantity) {
      this.showQuantityError = true;
      this.selectedIndex = index;
    }
  }

  isNewSupplierAddress: boolean = false;

  onSave() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    if (this.newAddressForm.invalid) {
      return;
    }

    let city = this.newAddressForm.get('cityArray').value.displayName == undefined ?
      this.newAddressForm.get('cityArray').value : this.newAddressForm.get('cityArray').value.displayName;
    let suburb = this.newAddressForm.get('suburbArray').value.displayName == undefined ?
      this.newAddressForm.get('suburbArray').value : this.newAddressForm.get('suburbArray').value.displayName;
    this.newAddressForm.get('cityName').patchValue(city);
    this.newAddressForm.get('suburbName').patchValue(suburb);
    let province = this.provinceList.filter(x => x.id === this.newAddressForm.get('provinceId').value);
    if (province?.length > 0) {
      this.newAddressForm.get('provinceName').patchValue(province[0].displayName);
    }
    else {
      this.newAddressForm.get('provinceName').patchValue(this.newAddressForm.get('provinceId').value);
    }
    this.showNewAddressPopupModal = false;
    this.snackBarService.openSnackbar('New address added manually', ResponseMessageTypes.SUCCESS);
    this.isNewSupplierAddress = true;
    this.supplierAddEditForm.get('supplierAddress').setErrors(null);
    this.supplierAddEditForm.get('supplierAddress').clearValidators();
    this.supplierAddEditForm.get('supplierAddress').updateValueAndValidity();


    // let value={
    //   supplierId:"88b66d25-bd1a-40ef-878c-37021cb01604",
    //   buildingNo:"004",
    //   buildingName:"StationRoad",
    //   streetNumber:"005",
    //   streetName:"GandhiDham",
    //   provinceName:"EasternCape",
    //   cityName:"PRETORIA",
    //   suburbName:"ATHOLLHEIGHTS",
    //   postalCode:"3434",
    //   createdUserId:"d4ecbcef-6759-4785-9bb7-4009e77c156c"
    // }
    let value;
    value = this.newAddressForm.value;
    value.createdUserId = this.userData.userId;
    value.buildingNumber = value.buildingNo;
    this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.SUPPLIER_ADDRESS_NEW,
      value
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200 && response.resources) {
        this.inputChangeSupplierFilter();
        this.supplierAddEditForm.get('supplierAddressId').setValue(response.resources);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });

  }

  isPrintSuccessDialog: boolean = false;

  onSubmit(type: string) {
    if (this.supplierAddEditForm.invalid) {
      this.supplierAddEditForm.markAllAsTouched();
      return;
    }
    let itemsArray = this.getOrderReceiptItemsFormArray.value.filter(arr => arr.returnQuantity > 0);
    if (itemsArray?.length == 0) {
      this.snackBarService.openSnackbar('Return Qty not populated, select at least one item to return.', ResponseMessageTypes.WARNING);
      return;
    };
    this.showQuantityError = false;
    itemsArray.filter((quan, index) => {
      if (quan.receivedQuantity < quan.returnQuantity) {
        this.selectedIndex = index;
        this.showQuantityError = true;
      }
    });

    if (this.showQuantityError) {
      return;
    }
    let formValue = this.supplierAddEditForm.value;
    let orderReceiptId; let poBarcode;
    if (this.suppType == 'update') {
      orderReceiptId = this.getSupplierReturnResponse?.orderReceiptId;
      poBarcode = this.getSupplierReturnResponse?.poBarcode;
    }
    else {
      let orders = this.supplierAddEditForm.value.orderReceiptId;
      orderReceiptId = orders?.toString();
      poBarcode = this.supplierAddEditForm.value.poBarcode;
    }
    let newAddressForm = this.newAddressForm.value;
    newAddressForm.buildingNumber = newAddressForm.buildingNo;
    delete newAddressForm.cityArray; delete newAddressForm.suburbArray;
    delete newAddressForm.provinceId;
    delete formValue.supplierAddress; delete formValue.orderReceiptId;
    delete formValue.locationPoint; delete formValue.scanBarCodeItem;
    delete formValue.items; delete formValue.isRequestType; delete formValue.poBarcode;

    if (itemsArray && itemsArray.length > 0) {
      itemsArray.forEach((ele) => {
        if (ele.itemDetails && ele.itemDetails.length > 0) {
          ele.UserId = this.userData.userId;
          ele.isDraft = type == 'draft' ? true : false,
            ele.serialNumber = ele.itemDetails.toString();
          delete ele.itemDetails;
        }
      })

    }

    const sendValue = {
      ...formValue, UserId: this.userData.userId, orderReceiptId: orderReceiptId, poBarcode: poBarcode, itemDetails: itemsArray,
      isDraft: type == 'draft' ? true : false, isNewSupplierAddress: this.isNewSupplierAddress,
      newSupplierAddress: this.isNewSupplierAddress ? null : null
    }

    const submit$ = this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.SUPPLIER_RETURN_REQUEST_NEW_SAVE,
      sendValue
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        if (this.supplierReturnId) {
          this.getSelectedValue(this.supplierReturnId, 'get')
        } else {
          this.getUpdatedPo(response.resources)
        }
        // this.goodsMovementSapRequest();
        this.supplierReturnId = response.resources;
        this.isPrintSuccessDialog = true;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackBarService.openSnackbar(response?.message, ResponseMessageTypes.WARNING);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  goodsMovementSapRequest() {
    let detailsParams = new HttpParams().set('CreatedUserId', this.userData.userId)
      .set('GoodsMovementType', '122').set('GoodsMovementFilterId', this.supplierReturnId);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.GOODS_MOVEMENT_SAP_REQUEST, undefined, true, detailsParams)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getUpdatedPo(reference: any) {
    if (reference != '' && reference != null) {
      let apiSuffixModels = this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_RETURN_REQUEST_NEW_VIEW_DETAILS, reference);
      apiSuffixModels.subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
        if (response.isSuccess && response.statusCode == 200 && response.resources != null) {
          response.resources.items = response.resources.supplierReturnItems;
          response.resources?.items.forEach(element => {
            element.receivedQuantity = element.receivedQty;
            element.returnQuantity = element.quantity;
          });
          this.getSupplierDetails(response.resources)
        }
        else {
          this.snackBarService.openSnackbar(response?.message, ResponseMessageTypes.WARNING);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }
  errorMessage: boolean = false;

  deleteSupplier() {
    const message = `Are you sure you want to delete the return to supplier request: ${this.getSupplierReturnResponse['supReturnNumber']} ?`;
    const dialogData = new ConfirmDialogModel("Request Deletion", message);
    const dialogRef = this.dialogService.open(PrimengConfirmDialogPopupComponent, {
      width: "400px",
      data: { ...dialogData, msgCenter: true },
      showHeader: false,
      baseZIndex: 1000
    });
    dialogRef.onClose.subscribe(dialogResult => {
      if (dialogResult) {
        this.rxjsService.setFormChangeDetectionProperty(true);
        this.crudService.delete(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.SUPPLIER_RETURN, undefined,
          prepareRequiredHttpParams({
            ids: this.supplierReturnId,
            modifiedUserId: this.userData.userId
          })).subscribe({
            next: response => {
              if (response.isSuccess) {
                this.navigateToList();
                this.rxjsService.setGlobalLoaderProperty(false);
              }
            },
            error: err => this.errorMessage = err
          });
      }
    });

  }

  navigateToList() {
    this.router.navigate(['/inventory/suppliers/return']);
  }

  navigateToView() {
    this.router.navigate(['inventory/suppliers/return/return-view'], {
      queryParams: {
        id: this.supplierReturnId
      }, skipLocationChange: true
    });
  }

  onCRUDRequested(event) { }
}
