import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, RxjsService, currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService, exportList } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { SupplierReturnListFilterModel } from '@modules/inventory/models/SupplierReturnListFilter.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'supplier-list',
  templateUrl: './supplier-list.component.html',
  styleUrls: ['./supplier-list.component.scss']
})
export class SupplierListComponent extends PrimeNgTableVariablesModel implements OnInit {
  showFilterForm: boolean = false;
  supplierReturnFilterForm: FormGroup;
  supplierObservable;
  primengTableConfigProperties: any;
  supplierList = [];
  statusList = [];
  warehouseList = [];
  ReferenceList = [];
  row: any = {}
  userData: UserLogin;
  filterObject;
  otherParams;
  startTodayDate=0;
  constructor(private commonService: CrudService, private datePipe: DatePipe, private formBuilder: FormBuilder, private router: Router, private rxjsService: RxjsService,
    private activatedRoute: ActivatedRoute, private store: Store<AppState>, private snackbarService: SnackbarService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Return to Supplier",
      shouldShowBreadCrumb: false,
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Stock Return', relativeRouterUrl: '' }, { displayName: 'Return to Supplier' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Return to Supplier',
            dataKey: 'supplierId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableAddActionBtn: true,
            enablePrintBtn: true,
            printTitle: 'Packing-dashboard Report',
            printSection: 'print-section0',
            cursorLinkIndex: 0,
            columns: [{ field: 'supReturnNumber', header: 'Return to Supplier No' },
            { field: 'supplierReturnStatusName', header: 'Status' },
            { field: 'supplierName', header: 'Supplier Name' },
            { field: 'orderNumber', header: 'PO Number' },
            { field: 'barcode', header: 'PO Barcode' },
            { field: 'warehouseName', header: 'Warehouse' },
            { field: 'createdBy', header: 'Created By' },
            { field: 'createdDate', header: 'Created Date' },

            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            shouldShowFilterActionBtn: true,
            isDateWithTimeRequired: true,
            enableExportBtn: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.SUPPLIER_RETURN,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          },
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.displayAndLoadFilterData();
    this.getRequiredListData();
    this.getReferenceListArr();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.SUPPLIER_RETURN_REQUESTS];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }


  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = {
      ...otherParams,
      IsAll: true,
      UserId: this.userData.userId,
      ReturnType: this.selectedTabIndex == 0 ? 'new' : 'faulty'
    }
    this.otherParams = otherParams;


    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.createdDate = this.datePipe.transform(val?.createdDate, 'yyyy-MM-dd HH:mm:ss');
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        unknownVar['IsAll'] = true;
        unknownVar['UserId'] = this.userData.userId,
          unknownVar['ReturnType'] = this.selectedTabIndex == 0 ? 'new' : 'faulty';
        unknownVar = { ...unknownVar, ...this.filterObject }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        // this.displayAndLoadFilterData();
        this.showFilterForm = !this.showFilterForm;
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let pageIndex = this.row && this.row["pageIndex"] ? this.row["pageIndex"] : 0;
        let pageSize = this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
        this.exportList(pageIndex, pageSize);
        break;
      default:
    }
  }

  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams, 0, pageSize, ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.SUPPLIER_RETURN_TO_EXPORT, this.commonService, this.rxjsService, 'Return to Supplier');
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(['inventory/suppliers/return/create'], {
          queryParams: { type: 'create' }, skipLocationChange: true
        });
        break;
      case CrudType.VIEW:
        this.router.navigate(['inventory/suppliers/return/return-view'], {
          queryParams: {
            id: editableObject['supReturnId'],
            type: 'update'
          }, skipLocationChange: true
        });
        break;
    }
  }

  displayAndLoadFilterData(): void {
    this.createSupplierReturnFilterForm();
    this.warehouseList = [];
    forkJoin([
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSES, undefined, false, prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId })),
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIERRETURNSTATUS, undefined, false),
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIERS, undefined, false),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              let warehouseList = resp.resources;
              for (var i = 0; i < warehouseList.length; i++) {
                let tmp = {};
                tmp['value'] = warehouseList[i].id;
                tmp['display'] = warehouseList[i].displayName;
                this.warehouseList.push(tmp)
              }
              break;
            case 1:
              this.statusList = resp.resources;
              break;
            case 2:
              let supplierList = resp.resources;
              for (var i = 0; i < supplierList.length; i++) {
                let tmp = {};
                tmp['value'] = supplierList[i].id;
                tmp['display'] = supplierList[i].displayName;
                this.supplierList.push(tmp)
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    })
    this.supplierReturnFilterForm.get('warehouseId').valueChanges.subscribe(val => {
      if (val != null) {
        if (val.length > 0) {
          this.getReferenceList(Array.prototype.map.call(val, function (item) { return item; }).join(',')).subscribe((response: IApplicationResponse) => {
            this.ReferenceList = [];
            response.resources.forEach(x => this.ReferenceList.push({ value: x.id, display: x.displayName }))
            this.rxjsService.setGlobalLoaderProperty(false);
          })
        }
      }
      else {
        this.ReferenceList = [];
        this.supplierReturnFilterForm.get('referenceId').setValue(null);
        this.getReferenceListArr();
      }
    });
  }
  onWarehouseChage(val){
    if (val != null) {
      if (val.length > 0) {
        this.getReferenceList(Array.prototype.map.call(val, function (item) { return item; }).join(',')).subscribe((response: IApplicationResponse) => {
          this.ReferenceList = [];
          response.resources.forEach(x => this.ReferenceList.push({ value: x.id, display: x.displayName }))
          this.rxjsService.setGlobalLoaderProperty(false);
        })
      }
    }
    else {
      this.ReferenceList = [];
      this.supplierReturnFilterForm.get('referenceId').setValue(null);
      this.getReferenceListArr();
    }
  }
  createSupplierReturnFilterForm(supplierReturnListFilterModel?: SupplierReturnListFilterModel) {
    let supplierReturnFilterModel = new SupplierReturnListFilterModel(supplierReturnListFilterModel);
    this.supplierReturnFilterForm = this.formBuilder.group({});
    Object.keys(supplierReturnFilterModel).forEach((key) => {
      if (typeof supplierReturnFilterModel[key] === 'string') {
        this.supplierReturnFilterForm.addControl(key, new FormControl(supplierReturnFilterModel[key]));
      }
    });
  }

  getReferenceList(warehouseId) {
    return this.commonService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_PURCHASEORDERS_BY_WAREHOUSE,
      undefined,
      false,
      prepareGetRequestHttpParams(null, null, {
        WarehouseIds: warehouseId ? warehouseId : '',
        userId:this.loggedInUserData.userId
      })
    );
  }
  getReferenceListArr() {
    this.commonService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_PURCHASEORDERS_BY_WAREHOUSE,
      undefined,
      false,
      prepareGetRequestHttpParams(null, null, {userId:this.loggedInUserData.userId})
    ).subscribe((response: IApplicationResponse) => {
      this.ReferenceList = [];
      response.resources.forEach(x => this.ReferenceList.push({ value: x.id, display: x.displayName }))
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  resetFilter() {
    this.supplierReturnFilterForm.reset();
    this.filterObject = null;
    this.getRequiredListData();
    this.showFilterForm = !this.showFilterForm;
  }

  submitFilter() {
    let data = Object.assign({},
      (this.supplierReturnFilterForm.get('warehouseId').value === null || this.supplierReturnFilterForm.get('warehouseId').value.length == 0) ? null : { WarehouseId: Array.prototype.map.call(this.supplierReturnFilterForm.get('warehouseId').value, function (item) { return item; }).join(',') },
      (this.supplierReturnFilterForm.get('referenceId').value === null || this.supplierReturnFilterForm.get('referenceId').value.length == 0) ? null : { ReferenceId: Array.prototype.map.call(this.supplierReturnFilterForm.get('referenceId').value, function (item) { return item; }).join(',') },
      (this.supplierReturnFilterForm.get('statusId').value == '' || this.supplierReturnFilterForm.get('statusId').value == null) ? null : { StatusId: this.supplierReturnFilterForm.get('statusId').value },
      (this.supplierReturnFilterForm.get('fromDate').value == null || this.supplierReturnFilterForm.get('fromDate').value == '') ? null : { FromDate: this.datePipe.transform(this.supplierReturnFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
      (this.supplierReturnFilterForm.get('toDate').value == '' || this.supplierReturnFilterForm.get('toDate').value == null) ? null : { ToDate: this.datePipe.transform(this.supplierReturnFilterForm.get('toDate').value, 'yyyy-MM-dd') },
      (this.supplierReturnFilterForm.get('poBarCode').value == '' || this.supplierReturnFilterForm.get('poBarCode').value == null) ? null : { POBarcodes: this.supplierReturnFilterForm.get('poBarCode').value },
      (this.supplierReturnFilterForm.get('supplierId').value == '' || this.supplierReturnFilterForm.get('supplierId').value == null) ? null : { SupplierId: Array.prototype.map.call(this.supplierReturnFilterForm.get('supplierId').value, function (item) { return item; }).join(',') },
      { IsAll: true }, { ReturnType: this.selectedTabIndex == 0 ? 'new' : 'faulty' }, { UserId: this.userData.userId }
    );
    this.filterObject = data;
    this.getRequiredListData('', '', data);
    this.showFilterForm = !this.showFilterForm;
  }
}
