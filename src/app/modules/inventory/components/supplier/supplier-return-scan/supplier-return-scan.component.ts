import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { RxjsService } from '@app/shared/services';
import { OrderReceiptItem, OrderReceiptItemDetail } from '@modules/inventory/models';

@Component({
  selector: 'app-supplier-return-scan',
  templateUrl: './supplier-return-scan.component.html',
  styleUrls: ['./supplier-return-scan.component.scss']
})
export class SupplierReturnScanComponent implements OnInit {

  orderReceiptItem: OrderReceiptItem;
  orderReceiptItemDetails = new Array();
  orderReceiptItemDetailFiltered = new Array();
  itemCount: Number;
  orderReceiptInfo: any[];
  orderReceiptInfoCopy: any[];
  supplierList: any[];
  stockDetails: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private router: Router, private rxjsService: RxjsService) {

  }

  ngOnInit() {

    this.stockDetails = this.data;
    this.rxjsService.setGlobalLoaderProperty(false);
  }


  onAllItemsRemoved() {
    this.orderReceiptItemDetails.forEach((element: OrderReceiptItemDetail, index) => {
      element.returnBarcode = undefined;
    });

    this.orderReceiptItemDetailFiltered = new Array();
    this.orderReceiptItem.returnQuantity = 0;
    this.filter();
  }

  filter() {
    this.orderReceiptItemDetailFiltered = this.orderReceiptItem.orderReceiptItemDetails.filter(function (row) {
      return row.returnBarcode != undefined;
    });
  }

  OnItemRemoved(item: OrderReceiptItemDetail) {
    this.orderReceiptItemDetails.forEach((row, index) => {
      if (row.orderReceiptItemDetailId == item.orderReceiptItemDetailId) {
        row.returnBarcode = undefined;
        this.orderReceiptItemDetailFiltered.splice(index, 1);
        //this is to update the count after removing an item
        this.orderReceiptItem.returnQuantity = this.orderReceiptItem.returnQuantity - 1;
      }
    });
    this.filter();
  }

  BackToCreatePage() {
    this.router.navigate(['/inventory/suppliers/return/create'], { state: { data: { OrderInfo: this.orderReceiptInfoCopy } } });
  }

  save() {
    this.router.navigate(['/inventory/suppliers/return/create'], { state: { data: { OrderInfo: this.orderReceiptInfo, Details: this.orderReceiptItemDetails, OrderReceiptItemId: this.orderReceiptItem.orderReceiptItemId } } });
  }

}
