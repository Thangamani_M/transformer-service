import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SupplierListComponent } from '@inventory/components/supplier';
import { SupplierReturnComponent } from '@inventory/components/supplier/supplier-return';
import { SupplierReturnScanComponent } from './supplier-return-scan';
import { SupplierReturnViewComponent } from './supplier-return-view/supplier-return-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
    { path: 'return', component: SupplierListComponent, data: { title: 'Supplier Returns List' } ,canActivate:[AuthGuard] },
    { path: 'return/create', component: SupplierReturnComponent, data: { title: 'Create Supplier Returns' },canActivate:[AuthGuard]  },
    { path: 'return/update', component: SupplierReturnComponent, data: { title: 'Update Supplier Returns' },canActivate:[AuthGuard]  },
    { path: 'return-scan', component: SupplierReturnScanComponent, data: { title: 'Supplier Return Scans' },canActivate:[AuthGuard]  },
    { path: 'return/return-view', component: SupplierReturnViewComponent, data: { title: 'Supplier Return Info' },canActivate:[AuthGuard]  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class SupplierRoutingModule { }