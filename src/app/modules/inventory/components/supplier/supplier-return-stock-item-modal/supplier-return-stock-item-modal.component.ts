import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-supplier-return-stock-item-modal',
  templateUrl: './supplier-return-stock-item-modal.component.html'
})
export class SupplierReturnStockItemModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  constructor(@Inject(MAT_DIALOG_DATA) public popupData: any, public dialogRef: MatDialogRef<SupplierReturnStockItemModalComponent>,) { }

  ngOnInit(): void {
  }

  deleteSerialNumbers(serialObj: any) {
    this.outputData.emit(serialObj);
    this.popupData.list = this.popupData.list.filter(x => x.barcode != serialObj.barcode);
  }
}
