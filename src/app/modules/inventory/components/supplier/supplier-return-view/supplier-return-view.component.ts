import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService, currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR } from '@app/shared';
import { ConfirmDialogModel, ConfirmDialogPopupComponent } from '@app/shared/components';
import { CrudService, RxjsService } from '@app/shared/services';
import { SupplierReturn } from '@modules/inventory/models/SupplierReturns';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-supplier-return-view',
  templateUrl: './supplier-return-view.component.html',
  styleUrls: ['./supplier-return-view.component.scss']
})
export class SupplierReturnViewComponent implements OnInit {
  userData: UserLogin;
  supReturnId: any;
  suppReturn: any = {};
  primengTableConfigProperties: any;
  errorMessage: any;
  FileEnabled: boolean = false;
  // public formData = new FormData();
  @ViewChild('fileInput', null) myFileInputField: ElementRef;
  @ViewChild('iframe', null) iframe: ElementRef;
  selectedFiles = new Array();
  totalFileSize = 0;
  maxFileSize = 104857600; //in Bytes 100MB
  listOfFiles: any[] = [];
  fileList: File[] = [];
  maxFilesUpload: Number = 5;
  documentsDetails = [];
  suppType: string;
  loggedInUserData;
  fileIndex = 0;
  url;
  constructor(
    private activatedRoute: ActivatedRoute, private dialog: MatDialog,private sanitizer: DomSanitizer,
    private router: Router, private commonService: CrudService,
    private rxjsService: RxjsService, private snackbarService: SnackbarService,
    private store: Store<AppState>
  ) {

    this.supReturnId = this.activatedRoute.snapshot.queryParams.id;
    this.suppType = this.activatedRoute.snapshot.queryParams.type;
    this.primengTableConfigProperties = {
      tableCaption: "View Return to Supplier",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Stock Return', relativeRouterUrl: '' }, { displayName: 'Return to Supplier' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Return to Supplier',
            dataKey: 'stockAdjustmentId',
            enableAction: true,
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            enablePrintBtn: true,
            enableExportBtn: true,
            printTitle: 'View Stock Adjustment List',
            printSection: 'print-section0',
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
            ],
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
          }
        ]
      }
    }

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    this.suppReturn = new SupplierReturn();
    this.supplierReturnDetail();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
      // this.store.select(loggedInUserData),

    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.SUPPLIER_RETURN_REQUESTS]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  supplierReturnDetail() {
    this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_RETURN_REQUEST_NEW_VIEW_DETAILS, this.supReturnId).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.suppReturn = response.resources;
        let supportDocs = response.resources.supplierReturnSupportingDocument;
        this.documentsDetails = [];
        if (supportDocs.length > 0) {
          for (var i = 0; i < supportDocs.length; i++) {
            let temp = {};
            temp['docName'] = supportDocs[i].docName;
            temp['path'] = supportDocs[i].path;
            temp['docRepId'] = supportDocs[i].docRepId;
            this.documentsDetails.push(temp);
          }
      
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR)
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigateToEdit() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['inventory/suppliers/return/update'], {
      queryParams: {
        id: this.supReturnId,
        type: this.suppType
      }
    });
  }

  // uploadDocuments(flag) {
  //   if (flag == 'Edit') {
  //     this.FileEnabled = true;
  //   }
  //   else {
  //     this.FileEnabled = false;
  //   }
  // }

  //uploadFiles
  uploadFiles(event) {

    this.selectedFiles = [];
    const supportedExtensions = ['jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'];
    for (var i = 0; i < event.target.files.length; i++) {
      let selectedFile = event.target.files[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension) || supportedExtensions.includes(extension?.toLowerCase())) {
        this.selectedFiles.push(event.target.files[i]);
        this.totalFileSize += event.target.files[i].size;
        let temp = {};
        temp['docName'] = event.target.files[i].name;
        temp['path'] = '';
        this.documentsDetails.push(temp);
      }
      else {
        this.snackbarService.openSnackbar("Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx", ResponseMessageTypes.WARNING);
      }
    }
    if (this.selectedFiles.length > 0) {
      this.Save();
    }
  }

  Save(): void {

    if (this.totalFileSize > this.maxFileSize)
      return;

    let formData = new FormData();

    let obj = {
      supplierReturnId: this.supReturnId,
      createdUserId: this.userData.userId
    }

    formData.append("supplierDetails", JSON.stringify(obj));

    if (this.selectedFiles.length > 0) {
      for (const file of this.selectedFiles) {
        formData.append('File', file);
      }
    }

    this.commonService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_RETURN_FILE_UPLOAD, formData)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          // this.documentsDetails = response.resources;
          // this.FileEnabled = false;
          // this.supplierReturnDetail();
          this.documentsDetails[this.documentsDetails.length - 1].docRepId = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.selectedFiles = [];
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  removeSelectedFile(id, index) {
    //  this.supplierReturnDetail();

    if (id) {
      const message = `Are you sure you want to delete this?`;
      const dialogData = new ConfirmDialogModel("Confirm Action", message);
      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData
      });
      dialogRef.afterClosed().subscribe(dialogResult => {
        if (dialogResult) {
          let params = new HttpParams().set('ids', id).set('ModifiedUserId', this.userData.userId);
          this.commonService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_RETURN_NEW_DOCUMENT, null, params)
            .subscribe((response: IApplicationResponse) => {
              if (response.isSuccess && response.statusCode === 200) {
                this.selectedFiles.splice(index, 1);
                this.documentsDetails.splice(index, 1);
              }
            });
        }
      });
    } else {
      this.selectedFiles.splice(index, 1);
      this.documentsDetails.splice(index, 1);
    }
    this.myFileInputField.nativeElement.value = '';

  }
  print() {
    let data = {
      SupplierReturnId : this.supReturnId,
      userId: this.userData.userId
    }
    this.commonService.create(ModulesBasedApiSuffix.PDF, InventoryModuleApiSuffixModels.SUPPLIER_RETURN_GENERATE_PDF_DETAIL, data)
    .subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200 && response.resources) {
        window.open(response.resources, '_blank');
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  changeCreditNoteChnges(ev, item) {


    let reqObj = {};
    let status;
    if (ev.target.checked == true) {
      status = true;
    } else {
      status = false;
    }


    reqObj['supReturnItemDetailId'] = item.supReturnItemDetailId;
    reqObj['isCreditNote'] = status;
    reqObj['modifiedUserId'] = this.userData.userId

    this.commonService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_RETURN_SERIAL_NUMBER_UPDATE, reqObj)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {

        }
      });
  }

}


