import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { PrimengConfirmDialogPopupModule } from '@app/shared/components/primeng-confirm-dialog-popup/primeng-confirm-dialog-popup.module';
import { MaterialModule } from '@app/shared/material.module';
import { SupplierListComponent, SupplierRoutingModule } from '@inventory/components/supplier';
import { SupplierReturnComponent, SupplierReturnConfirmationModalComponent } from '@inventory/components/supplier/supplier-return';
import { NgxPrintModule } from 'ngx-print';
import { SupplierReturnScanComponent } from './supplier-return-scan/supplier-return-scan.component';
import { SupplierReturnStockItemModalComponent } from './supplier-return-stock-item-modal/supplier-return-stock-item-modal.component';
import { SupplierReturnViewComponent } from './supplier-return-view/supplier-return-view.component';

@NgModule({
  declarations: [SupplierListComponent, SupplierReturnComponent, SupplierReturnScanComponent, SupplierReturnViewComponent, SupplierReturnStockItemModalComponent, SupplierReturnConfirmationModalComponent],
  imports: [
    CommonModule,
    SupplierRoutingModule,
    ReactiveFormsModule,FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    NgxPrintModule,
    PrimengConfirmDialogPopupModule
  ],
  entryComponents:[SupplierReturnStockItemModalComponent, SupplierReturnConfirmationModalComponent]
})
export class SupplierModule { }