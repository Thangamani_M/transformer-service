import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { VarianceInvestigationStatus } from '@app/modules/inventory/models';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, EnableDisable } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { VarianceInvestigationStatusAddEditComponent } from './';

@Component({
  selector: 'app-variance-investigation-status-list',
  templateUrl: './variance-investigation-status-list.component.html'
})
export class                                                                                                                                                                                                                        VarianceInvestigationStatusListComponent implements OnInit {
  varianceInvestigationStatus: VarianceInvestigationStatus[];
  displayedColumns: string[] = ['select', 'displayName', 'description', 'createdDate', 'isActive'];
  dataSource = new MatTableDataSource<VarianceInvestigationStatus>();
  selection = new SelectionModel<VarianceInvestigationStatus>(true, []);
  checkedVarianceInvestigationStatusIds: string[] = [];
  enableDisable = new EnableDisable();
  applicationResponse: IApplicationResponse;

  errorMessage: string;
  limit: number = 25;
  skip: number = 0;
  totalLength;
  pageIndex: number = 0;
  pageLimit: number[] = [25, 50, 75, 100]
  searchText: any = { Search: '', IsAll: true }

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private httpService: CrudService,
    private dialog: MatDialog,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService) {


  }

  ngOnInit() {
    this.dataSource.sortingDataAccessor = (data: any, sortHeaderId: string): string => {
      if (typeof data[sortHeaderId] === 'string') {
        return data[sortHeaderId].toLocaleLowerCase();
      }
      return data[sortHeaderId];
    };
    this.getVarianceInvestigationStatus(this.searchText);
  }
  applyFilter(filterValue: string) {
    this.searchText.Search = filterValue;
    this.getVarianceInvestigationStatus(this.searchText);
  }
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
  changePage(event) {

    this.pageIndex = event.pageIndex;
    this.limit = event.pageSize;
    this.getVarianceInvestigationStatus(this.searchText);

  }
  private getVarianceInvestigationStatus(param?) {
    this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.VARIANCE_INVESTIGATION_STATUS, null, false,
      prepareGetRequestHttpParams(this.pageIndex.toString(), this.limit.toString(), param)).subscribe(resp => {
        this.applicationResponse = resp;
        this.varianceInvestigationStatus = this.applicationResponse.resources;
        this.dataSource.data = this.varianceInvestigationStatus;
        this.totalLength = this.applicationResponse.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  openAddVarianceInvestigationStatusDialog(varianceInvestigationStatusId): void {
    var varianceInvestigationStatusAdd = new VarianceInvestigationStatus();
    if (varianceInvestigationStatusId) {
      this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_INVESTIGATION_STATUS, varianceInvestigationStatusId).subscribe(
        {
          next: response => {
            this.applicationResponse = response,
              varianceInvestigationStatusAdd = this.applicationResponse.resources;
            this.OpenDialogWindow(varianceInvestigationStatusAdd);
          }
        });

    } else {
      this.OpenDialogWindow(varianceInvestigationStatusAdd);
    }

  }


  private OpenDialogWindow(varianceInvestigationStatusAdd: VarianceInvestigationStatus) {
    const dialogReff = this.dialog.open(VarianceInvestigationStatusAddEditComponent, { width: '30vw', data: varianceInvestigationStatusAdd, disableClose: true });
    dialogReff.afterClosed().subscribe(result => {
      this.selection.clear();
      if (result) { return; }
      this.getVarianceInvestigationStatus(this.searchText);
    });
  }

  enableDisableVarianceInvestigationStatus(id, status: any) {
    this.enableDisable.ids = id;
    this.enableDisable.isActive = status.currentTarget.checked;

    this.httpService
      .enableDisable(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_INVESTIGATION_STATUS, this.enableDisable).subscribe({
        next: response => { this.rxjsService.setGlobalLoaderProperty(false); },
        error: err => this.errorMessage = err
      });
  }

  private resetSelectedValues() {
    this.checkedVarianceInvestigationStatusIds = [];
    //clear check box
    this.selection = new SelectionModel<VarianceInvestigationStatus>(true, []);
  }

  removeSelectedVarianceInvestigationStatus() {
    //get selected VarianceInvestigationStatus ids
    this.selection.selected.forEach(item => {
      this.checkedVarianceInvestigationStatusIds.push(item.varianceInvestigationStatusId);
    });



    if (this.checkedVarianceInvestigationStatusIds.length > 0) {
      const message = `Are you sure you want to delete this?`;

      const dialogData = new ConfirmDialogModel("Confirm Action", message);

      const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
        maxWidth: "400px",
        data: dialogData
      });

      dialogRef.afterClosed().subscribe(dialogResult => {
        if (dialogResult) {
          //convert selected id to array
          this.deleteVarianceInvestigationStatus();
          this.resetSelectedValues();
        }
        else {
          this.resetSelectedValues();
        }
      });
    } else {
      // this.applicationResponse.statusCode = 204;
      this.applicationResponse.message = "Please Select Variance Investigation Status";
      this.snackbarService.openSnackbar(this.applicationResponse.message, ResponseMessageTypes.WARNING)
    }
  }

  //Delete selected varianceInvestigationStatus from database
  private deleteVarianceInvestigationStatus() {

    //call varianceInvestigationStatus services to delete
    this.httpService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_INVESTIGATION_STATUS, this.checkedVarianceInvestigationStatusIds.join(',')).subscribe({
      next: response => {
        //reload data
        this.getVarianceInvestigationStatus(this.searchText);
      },
      error: err => this.errorMessage = err
    });

  }

}
