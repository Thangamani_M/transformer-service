import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { VarianceInvestigationStatus } from '../../../models/VarianceInvestigationStatus';

@Component({
  selector: 'app-variance-investigation-status-add-edit',
  templateUrl: './variance-investigation-status-add-edit.component.html',
  styleUrls: ['./variance-investigation-status-add-edit.component.scss']
})
export class VarianceInvestigationStatusAddEditComponent implements OnInit {
  @Input() id: string;
  varianceInvestigationStatusAddEditForm: FormGroup;
  applicationResponse: IApplicationResponse;
  errorMessage: string;
  btnName: string;
  isDialogLoaderLoading = true;
  isADependentDropdownSelectionChanged = false;
  //mask validations
  public alphaNumericWithSpacePattern = { '0': { pattern: new RegExp('^[A-Za-z0-9 ]*$') } };

  constructor(@Inject(MAT_DIALOG_DATA) public data: VarianceInvestigationStatus,
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private httpServices: CrudService,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService) { }

  ngOnInit(): void {
    this.rxjsService.setDialogOpenProperty(true);
    if (this.data.varianceInvestigationStatusId) {
      this.btnName = 'Update';
    } else {
      this.btnName = 'Create';
    }
    this.varianceInvestigationStatusAddEditForm = this.formBuilder.group({
      displayName: this.data.displayName || '',
      description: this.data.description || ''
    });
    this.isDialogLoaderLoading = false;
  }
  save(): void {
    this.isDialogLoaderLoading = true;
    this.isADependentDropdownSelectionChanged = true;
    const varianceInvestigationStatus = { ...this.data, ...this.varianceInvestigationStatusAddEditForm.value }
    if (varianceInvestigationStatus.varianceInvestigationStatusId != null) {
      this.httpServices.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_INVESTIGATION_STATUS, varianceInvestigationStatus).subscribe({
        next: response => this.onSaveComplete(response),
        error: err => this.errorMessage = err
      });

    }
    else {
      this.httpServices.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_INVESTIGATION_STATUS, varianceInvestigationStatus).subscribe({
        next: response => this.onSaveComplete(response),
        error: err => this.errorMessage = err
      });

    }
  }
  onSaveComplete(response): void {
    if (response.statusCode == 200) {
      this.dialog.closeAll();
      // Reset the form to clear the flags
      this.varianceInvestigationStatusAddEditForm.reset();
    } else {
      //Display Alert message
      this.snackbarService.openSnackbar(response['message'], ResponseMessageTypes.WARNING)
    }
    this.isDialogLoaderLoading = false;
    this.isADependentDropdownSelectionChanged = false;
  }

  ngOnDestroy() {
    this.rxjsService.setDialogOpenProperty(false);
  }

}
