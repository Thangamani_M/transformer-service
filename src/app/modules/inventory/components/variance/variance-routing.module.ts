import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  VarianceEscalationNewViewComponent, VarianceEscalationUpdateComponent, VarianceEsclationAddEditComponent, VarianceEsclationListComponent, VarianceInvestigationStatusListComponent
} from '@inventory/components/variance';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { VarianceEsclationAddEditOldComponent } from './esclation/variance-esclation-add-edit.component';

const routes: Routes = [
  { path: 'status', component: VarianceInvestigationStatusListComponent, data: { title: 'Variance List' } ,canActivate:[AuthGuard]},
  { path: 'variance-esclation', component: VarianceEsclationListComponent, data: { title: 'Variance Esclation List' } ,canActivate:[AuthGuard]},
  { path: 'variance-esclation/add-edit', component: VarianceEsclationAddEditComponent, data: { title: 'Variance Esclation Add Edit' } ,canActivate:[AuthGuard]},
  { path: 'variance-esclation/view', component: VarianceEscalationNewViewComponent, data: { title: 'Variance Escalation View' } ,canActivate:[AuthGuard]},
  { path: 'variance-esclation/update', component: VarianceEscalationUpdateComponent, data: { title: 'Variance Escalation View' },canActivate:[AuthGuard] },
  { path: 'variance-esclation/old-add-edit', component: VarianceEsclationAddEditOldComponent, data: { title: 'Variance Escalation View' },canActivate:[AuthGuard] }, // want to remove after stabilazation over
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class VarianceRoutingModule { }
