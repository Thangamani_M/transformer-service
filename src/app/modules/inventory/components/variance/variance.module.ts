import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import {
    VarianceEscalationCommentsModalComponent, VarianceEscalationNewViewComponent, VarianceEscalationPopupModalComponent, VarianceEscalationUpdateComponent, VarianceEsclationAddEditComponent, VarianceEsclationListComponent, VarianceInvestigationStatusAddEditComponent, VarianceInvestigationStatusListComponent,
    VarianceRoutingModule
} from '@inventory/components/variance';
import { NgxPrintModule } from 'ngx-print';
import { VarianceEsclationAddEditOldComponent } from './esclation/variance-esclation-add-edit.component';


@NgModule({
  declarations: [
    VarianceInvestigationStatusAddEditComponent, VarianceInvestigationStatusListComponent,
    VarianceEsclationListComponent, VarianceEsclationAddEditComponent,
    VarianceEscalationNewViewComponent, VarianceEscalationUpdateComponent,
    VarianceEscalationPopupModalComponent, VarianceEscalationCommentsModalComponent,VarianceEsclationAddEditOldComponent // want to remove after stabilazation over
  ],
  imports: [
    CommonModule,
    VarianceRoutingModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    NgxPrintModule
  ],
  entryComponents: [VarianceInvestigationStatusAddEditComponent, VarianceEscalationPopupModalComponent, VarianceEscalationCommentsModalComponent]
})
export class VarianceModule { }
