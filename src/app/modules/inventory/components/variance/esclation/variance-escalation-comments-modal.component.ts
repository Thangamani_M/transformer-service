import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {
    CrudService, IApplicationResponse, ModulesBasedApiSuffix,
    ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { Observable } from 'rxjs';


@Component({
    selector: 'app-variance-escalation-comments-modal',
    templateUrl: './variance-escalation-comments-modal.component.html',
    styleUrls: ['./variance-esclation-add-edit.component.scss']

})

export class VarianceEscalationCommentsModalComponent implements OnInit {

    reasonText = '';
    pickingJobCommentsForm: FormGroup;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data,
        private dialogRef: MatDialogRef<VarianceEscalationCommentsModalComponent>,
        private crudService: CrudService,
        private rxjsService: RxjsService, private formBuilder: FormBuilder,
        private snackbarService: SnackbarService,
    ) {
  
    }

    ngOnInit(): void {
        this.createPickingJobModalForm();
        this.rxjsService.setDialogOpenProperty(true);
    }

    createPickingJobModalForm() {
        this.pickingJobCommentsForm = this.formBuilder.group({
            Comments: ['', Validators.required],
        });

    }

    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }

    onSubmit(): void {
        if (this.pickingJobCommentsForm.invalid) {
            this.pickingJobCommentsForm.markAllAsTouched();
            return;
        }

        const formData = {
            PackerJobId: this.data.reasonId,
            Comments: this.pickingJobCommentsForm.get('Comments').value,
        }

        let crudService: Observable<IApplicationResponse> = this.crudService.update(
            ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PACKERS_JOBS_MANAGER_OVERRIDE,
            formData
        )
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.dialogRef.close(true);
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

}