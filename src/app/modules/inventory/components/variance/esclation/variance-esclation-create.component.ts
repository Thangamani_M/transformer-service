import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog/primeng-custom-dialog.component';
import { ResponseMessageTypes, VarianceInvestigationStatus } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { VarianceEsclationAddEditModel } from '@modules/inventory/models';
import { VarianceEsclation, VarianceEsclationDetails, VarianceInvPostItemDTOs } from '@modules/inventory/models/VarianceEsclationDetails';
import { InventoryModuleApiSuffixModels, OrderTypes } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-variance-escalation-create',
  templateUrl: './variance-esclation-create.component.html',
  styleUrls: ['./variance-esclation-list.component.scss']
})
export class VarianceEsclationAddEditComponent implements OnInit {
  requestWarehouseList = []
  varianceEsclationForm: FormGroup;
  filteredPONumbers: any = [];
  showPONumberError = false
  isSubmitted = false
  showPOBarCodeError = false
  enableQty = false
  primengTableConfigProperties: any = {
    tableCaption: "Create Variance Escalation",
    shouldShowBreadCrumb: false,
    selectedTabIndex: 0,
    breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Variance Escalation', relativeRouterUrl: '/inventory/variance/variance-esclation' }, { displayName: 'Create Variance Escalation' }],
    tableComponentConfigs: {
      tabsList: [
        {
          caption: 'Create Variance Escalation',
          dataKey: '',
          enableBreadCrumb: true,
        }]
    }
  }

  userData: UserLogin;

  constructor(private crudService: CrudService, private route: ActivatedRoute, private formBuilder: FormBuilder,
    private store: Store<AppState>, private router: Router, private snackbarService: SnackbarService, private rxjsService: RxjsService,
    private dialogService: DialogService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

  }

  ngOnInit() {
    this.createVariaceEsclationForm()
    this.getWarehouses()
  }

  createVariaceEsclationForm() {
    this.varianceEsclationForm = this.formBuilder.group({});
    let varianceEsclationModal = new VarianceEsclationAddEditModel();

    Object.keys(varianceEsclationModal).forEach(key => {
      this.varianceEsclationForm.addControl(key, new FormControl())
    })
    this.varianceEsclationForm = setRequiredValidator(this.varianceEsclationForm, ["warehouseId"])
    if(this.userData?.warehouseId){
      this.varianceEsclationForm.get("warehouseId").setValue(this.userData?.warehouseId)
    }
  }

  getWarehouses() {
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSES, prepareRequiredHttpParams({ userId: this.userData?.userId })).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false)
      if (response.isSuccess && response.resources) {
        this.requestWarehouseList = response.resources;
      }
    })
  }

  onChangeStockCode(text: any) {
    this.isSubmitted = false
    let otherParams = {}
    this.showPONumberError = false;
    if (text.query == null || text.query == '') return;
    otherParams['itemCode'] = text.query;
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.
      UX_ITEM, null, false,
      prepareRequiredHttpParams(otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.filteredPONumbers = response.resources;
          if (response.resources.length == 0) {
            this.showPONumberError = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });

  }

  onSelectStockCode(event) {
    this.isSubmitted = false
    this.varianceEsclationForm.get("itemCode").setValue(event?.itemCode)
    this.varianceEsclationForm.get("itemName").setValue(event?.displayName)
    if (event?.isNotSerialized == false) {
      this.varianceEsclationForm.get("quantity").setValidators(null)
      this.varianceEsclationForm.get("quantity").setValue(null)
      this.openDialog();
      if(!this.varianceEsclationForm.get("itemSerialNumber").value)
         this.varianceEsclationForm.get("serialNumber").setValidators([Validators.required]);
      else
          this.varianceEsclationForm.get("serialNumber").clearValidators();

      this.enableQty = false
    } else {
      this.varianceEsclationForm.get("quantity").setValue(null)
      this.varianceEsclationForm.get("quantity").setValidators([Validators.required])
      this.varianceEsclationForm.get("serialNumber").setValidators(null)
      this.varianceEsclationForm.get("serialNumber").setValue(null)
      this.varianceEsclationForm.get("itemSerialNumber").setValue(null)
      this.enableQty = true
    }
    this.varianceEsclationForm.get("serialNumber").updateValueAndValidity()
    this.varianceEsclationForm.get("quantity").updateValueAndValidity()
    this.varianceEsclationForm.updateValueAndValidity()

  }

  onClickSerialNumber() {
    this.varianceEsclationForm.get("serialNumber").clearValidators();
    this.isSubmitted = false
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_BALANCE_SERIAL, null, false,
      prepareRequiredHttpParams({ serialNumber: this.varianceEsclationForm.get("serialNumber").value }))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.rxjsService.setGlobalLoaderProperty(false);
          this.varianceEsclationForm.get("itemSerialNumber").setValue(response.resources?.serialNumber);
          this.varianceEsclationForm.get("serialNumber").setValue(null);
          this.varianceEsclationForm.get("quantity").setValue(1);
          this.varianceEsclationForm.get("itemCode").setValue(response.resources?.itemCode)
          this.varianceEsclationForm.get("itemName").setValue(response.resources?.itemName)
        }
      })
  }
  openDialog() {
    let customText = "<b>This stock code has serial numbers associated with it. <br> Please add the item by scanning or entering the serial number.</b>"
    this.dialogService.open(PrimengCustomDialogComponent, {
      header: 'Warning Message',
      showHeader: false,
      closable: true,
      baseZIndex: 10000,
      width: '500px',
      data: { customText: customText, isShowNo: false },
    });
  }

  onSubmit() {
    this.isSubmitted = true;
    if(!this.varianceEsclationForm.get('itemCode').value) {
      this.snackbarService.openSnackbar("Please enter serial number or stock code.",ResponseMessageTypes.WARNING);
      return;
    }
    else
        this.varianceEsclationForm.get("serialNumber").clearValidators();

    if (this.varianceEsclationForm.invalid) {
      return;
    }
    let obj = {...this.varianceEsclationForm.value};
    obj.itemId =  obj.itemId?.id;
    obj.isExcessiveStock =  true
    obj.isShortPickedStock =  false;
    obj.varianceInvReqUserId =  this.userData?.userId
    obj.serialNumber =   obj.itemSerialNumber;

    this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_ESCLATION_LIST, obj).subscribe(response => {
      this.rxjsService.setGlobalLoaderProperty(false)
      if (response.isSuccess && response.statusCode) {
        this.router.navigate(['/inventory/variance/variance-esclation']);
      }
    })
  }
}
