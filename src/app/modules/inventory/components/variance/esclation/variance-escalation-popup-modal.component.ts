import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RxjsService } from '@app/shared';


@Component({
    selector: 'app-variance-escalation-popup-modal',
    templateUrl: './variance-escalation-popup-modal.component.html',
    styleUrls: ['./variance-esclation-add-edit.component.scss']

})

export class VarianceEscalationPopupModalComponent implements OnInit {

    reasonText = '';
    headerMessage: any;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data,
        private rxjsService: RxjsService
    ) { }

    ngOnInit(): void {
        let messageSpan = document.createElement('span');
        messageSpan.innerHTML = this.data['message'];
        document.getElementById('parentContainer').appendChild(messageSpan);
        this.headerMessage = this.data['header']
        this.rxjsService.setDialogOpenProperty(true);
    }
    ngOnDestroy(): void {
        this.rxjsService.setDialogOpenProperty(false);
    }

}