import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType,exportList,  IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { VarianceEscalationFilterModal } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { UserModuleApiSuffixModels } from '@modules/user';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { currentComponentPageBasedPermissionsSelector$ } from '@app/shared';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-variance-esclation-list',
  templateUrl: './variance-esclation-list.component.html',
  styleUrls: ['./variance-esclation-list.component.scss'],
  styles: [`
      .filter-dropdwon-con {
        width: 100%;
        background-color: #fff;
        padding: 15px;
        border-radius: 7px;
        box-shadow: 0px 0px 0px 1px #cccccc61;
        top: 65px;
        right: 0px;
        z-index: 5;
      }
  `]
})
export class VarianceEsclationListComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties: any;
  row: any = {}
  isLoading: boolean;
  varianceEscalationFilterForm: FormGroup;
  showFilterForm = false;
  varianceNumberList: any = [];
  varianceUsersList: any = []
  warehouseList: any = [];
  divisionsList: any = [];
  branchesList: any = [];
  techAreasList: any = [];
  dateFormat = 'MMM dd, yyyy';
  filterData;
  varianceTypeList = [
    { value: 'E', display: "Excessive" },
    { value: 'S', display: "Shortpicked" },
  ];
  isStockCodeBlank: boolean = false;
  stockCodeErrorMessage: any = '';
  showStockCodeError: boolean = false;
  isStockError: boolean = false;
  filteredStockCodes = [];
  userData: UserLogin;
  startTodayDate = 0;
  otherParams;
  pickingBatch;
  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    public dialogService: DialogService, private snackbarService: SnackbarService,
    private router: Router, private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
  ) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Variance Escalation",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Variance Escalation', relativeRouterUrl: '' }, { displayName: 'Variance Escalation List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Variance Escalation',
            dataKey: 'varianceInvReqId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableAction: true,
            enableEditActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Stock Management',
            printSection: 'print-section0',
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'varianceInvReqNumber', header: 'Variance Escalation Number', width: '200px' },
            { field: 'varianceInvestigationStatusName', header: 'Status', width: '200px', },
            { field: 'varianceInvReqUser', header: 'Created By', width: '200px' },
            { field: 'varianceInvReqDate', header: 'Creation Date & Time', width: '200px'},
            { field: 'varianceType', header: 'Variance Type', width: '200px' },
            { field: 'orderTypeName', header: 'Stock Order Type', width: '200px' },
            { field: 'orderNumber', header: 'Stock Order Number', width: '200px' },
            { field: 'pickingBatchBarcode', header: 'Picking Batch Barcode', width: '200px' },
            { field: 'location', header: 'Location Serial Linked To', width: '200px' },
            { field: 'stockOrderNumberAssociatedWithSerial', header: 'Stock Order Number Associate With Serial', width: '200px' },
            { field: 'stockCode', header: 'Stock Code', width: '200px' },
            { field: 'stockName', header: 'Stock Description', width: '200px' },
            { field: 'serialNumber', header: 'Serial Number', width: '200px' },
            { field: 'quantity', header: 'Variance Qty', width: '200px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.VARIANCE_ESCLATION_LIST,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
          }
        ]
      }
    }
    // this.activatedRoute.queryParamMap.subscribe((params) => {
    //   this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
    //   this.pickingBatch = params['params']['pickingBatch'];
    //   this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    //   this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    // });
    this.pickingBatch = this.activatedRoute.snapshot.queryParams.pickingBatch;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.createVarianceFilterForm();
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

    combineLatestNgrxStoreData() {
      combineLatest([
        this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
      ).subscribe((response) => {
        this.loggedInUserData= new LoggedInUserModel(response[0]);
        let permission = response[1][INVENTORY_COMPONENT.VARIANCE_ESCALATION]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
    }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    otherParams={...otherParams,...{
      TechAreaManager: this.userData.roleName == 'Technical Area Manager' ? true : false,
      UserId: this.userData.userId
    }};
    if(this.pickingBatch)
      otherParams = {...otherParams,...{PickingBatchBarcode:this.pickingBatch}};

    this.otherParams = otherParams;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.varianceInvReqDate = this.datePipe.transform(val?.varianceInvReqDate, 'yyyy-MM-dd HH:mm:ss');
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources= null;
        this.dataList = data.resources
        this.totalRecords = 0;
      }
    })
  }
  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams,0,pageSize,ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.VARIANCE_ESCALATION_EXPORT,this.crudService,this.rxjsService,'Variance Esclation');
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        if(unknownVar){
          unknownVar.TechAreaManager = this.userData.roleName == 'Technical Area Manager' ? true : false;
          unknownVar.UserId = this.userData.userId;
        }

        unknownVar = {...unknownVar,...this.filterData}
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        this.displayAndLoadFilterData();
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
          let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
          this.exportList(pageIndex,pageSize);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
      this.router.navigate(['/inventory/variance/variance-esclation/view'], {
        queryParams: {
          id: editableObject['varianceInvReqId']
        }, skipLocationChange: true
      });
      break;
      case CrudType.CREATE:
        this.router.navigate(['/inventory/variance/variance-esclation/add-edit'], {
          queryParams: {

          }, skipLocationChange: false
        });
        break;
    }
  }

  displayAndLoadFilterData() {

    this.showFilterForm = !this.showFilterForm;
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_VARIANCE_NUMBER),
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, UserModuleApiSuffixModels.UX_USERS),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId })),
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, TechnicalMgntModuleApiSuffixModels.UX_DIVISIONS),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              let varianceNumberList = resp.resources;
              for (var i = 0; i < varianceNumberList.length; i++) {
                let tmp = {};
                tmp['value'] = varianceNumberList[i].id;
                tmp['display'] = varianceNumberList[i].displayName;
                this.varianceNumberList.push(tmp)
              }
              break;
            case 1:
              let varianceUsersList = resp.resources;
              for (var i = 0; i < varianceUsersList.length; i++) {
                let tmp = {};
                tmp['value'] = varianceUsersList[i].id;
                tmp['display'] = varianceUsersList[i].displayName;
                this.varianceUsersList.push(tmp)
              }
              break;
            case 2:
              let warehouseList = resp.resources;
              for (var i = 0; i < warehouseList.length; i++) {
                let tmp = {};
                tmp['value'] = warehouseList[i].id;
                tmp['display'] = warehouseList[i].displayName;
                this.warehouseList.push(tmp)
              }
              break;
            case 3:
              let divisionsList = resp.resources;
              for (var i = 0; i < divisionsList.length; i++) {
                let tmp = {};
                tmp['value'] = divisionsList[i].id;
                tmp['display'] = divisionsList[i].displayName;
                this.divisionsList.push(tmp)
              }
              break;
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createVarianceFilterForm(requisitionListModel?: VarianceEscalationFilterModal) {
    let purchaseListFilterModel = new VarianceEscalationFilterModal(requisitionListModel);
    this.varianceEscalationFilterForm = this.formBuilder.group({});
    Object.keys(purchaseListFilterModel).forEach((key) => {
      if (typeof purchaseListFilterModel[key] === 'string') {
        this.varianceEscalationFilterForm.addControl(key, new FormControl(purchaseListFilterModel[key]));
      }
    });
  }

  getSelectedBranchOptions(stockCode): void {
    this.branchesList = [];
    if (stockCode != '') {
      let params = new HttpParams().set('DivisionIds', this.varianceEscalationFilterForm.get('divisions').value);
      this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.UX_BRANCHES, undefined, true, params).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let branchesList = response.resources;
          for (var i = 0; i < branchesList.length; i++) {
            let tmp = {};
            tmp['value'] = branchesList[i].branchId;
            tmp['display'] = branchesList[i].branchName;
            this.branchesList.push(tmp)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  inputChangeStockCodeFilter() {
    this.varianceEscalationFilterForm.get('stockCodes').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        if (this.isStockError == false) {
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
        }
        else {
          this.isStockError = false;
        }

        if (searchText != null) {
          if (this.isStockCodeBlank == false) {
          }
          else {
            this.isStockCodeBlank = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.stockCodeErrorMessage = '';
              this.showStockCodeError = false;
              this.isStockError = false;
            }
            else {
              this.stockCodeErrorMessage = 'Stock code is not available in the system';
              this.showStockCodeError = true;
              this.isStockError = true;
            }
            return this.filteredStockCodes = [];
          }
          else {
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, true,
              prepareGetRequestHttpParams(null, null, { searchText, isAll: false }))
          }
        }
        else {
          return this.filteredStockCodes = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.resources.length > 0) {
          this.filteredStockCodes = response.resources;
          this.stockCodeErrorMessage = '';
          this.showStockCodeError = false;
          this.isStockError = false;
        } else {
          this.filteredStockCodes = [];
          this.stockCodeErrorMessage = 'Stock code is not available in the system';
          this.showStockCodeError = true;
          this.isStockError = true;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  submitFilter() {
    let data = Object.assign({},
      this.varianceEscalationFilterForm.get('varianceInvReqIds').value == '' ? null : { VarianceInvReqIds: this.varianceEscalationFilterForm.get('varianceInvReqIds').value.join() },
      this.varianceEscalationFilterForm.get('varianceInvReqUserIds').value == '' ? null : { VarianceInvReqUserIds: this.varianceEscalationFilterForm.get('varianceInvReqUserIds').value },
      this.varianceEscalationFilterForm.get('warehouseId').value == '' ? null : { WarehouseId: this.varianceEscalationFilterForm.get('warehouseId').value },
      this.varianceEscalationFilterForm.get('stockCodes').value == '' ? null : { StockCodes: this.varianceEscalationFilterForm.get('stockCodes').value },
      this.varianceEscalationFilterForm.get('createdFromDate').value == '' ? null : { CreatedFromDate: this.datePipe.transform(this.varianceEscalationFilterForm.get('createdFromDate').value, 'yyyy-MM-dd') },
      this.varianceEscalationFilterForm.get('createdToDate').value == '' ? null : { CreatedToDate: this.datePipe.transform(this.varianceEscalationFilterForm.get('createdToDate').value, 'yyyy-MM-dd') },
      this.varianceEscalationFilterForm.get('varianceTypes').value == '' ? null : { VarianceTypes: this.varianceEscalationFilterForm.get('varianceTypes').value },
      this.varianceEscalationFilterForm.get('divisions').value == '' ? null : { Divisions: this.varianceEscalationFilterForm.get('divisions').value },
      this.varianceEscalationFilterForm.get('branches').value == '' ? null : { Branches: this.varianceEscalationFilterForm.get('branches').value },
      this.varianceEscalationFilterForm.get('techAreas').value == '' ? null : { TechAreas: this.varianceEscalationFilterForm.get('techAreas').value }, { UserId: this.userData.userId}
    );
    this.filterData =data;
     this.onCRUDRequested('get');
    this.showFilterForm = !this.showFilterForm;
  }

  resetForm() {
    this.varianceEscalationFilterForm.reset();
    this.filterData =null;
     this.getRequiredListData('', '', null);
    this.showFilterForm = !this.showFilterForm;
  }

  exportListOld() {
    if (this.dataList.length != 0) {
      let fileName = 'Variance Esclation ' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
      let columnNames = ["Variance Escalation Number", "Created By", "Creation Date & Time", "Variance Type", "Stock Order Type", "Stock Order Number",
        "Picking Batch Barcode", "Location Serial Linked To", "Stock Order Number Associate With Serial", "stockCode", "Stock Description", "Serial Number", "Variance Qty", "Status"];
      let header = columnNames.join(',');
      let csv = header;
      csv += '\r\n';
      this.dataList.map(c => {
        csv += [c["varianceInvReqNumber"], c["varianceInvReqUser"], c["varianceInvReqDate"], c['varianceType'], c['orderTypeName'], c['orderNumber'], c['pickingBatchBarcode'],
        c['location'], c['stockOrderNumberAssociatedWithSerial'], c['stockCode'], c['stockName'], c['serialNumber'], c['quantity'], c['varianceInvestigationStatusName']].join(',');
        csv += '\r\n';
      })
      var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
      var link = document.createElement("a");
      if (link.download !== undefined) {
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
    else {
      this.snackbarService.openSnackbar('Please select atleast one checkbox', ResponseMessageTypes.WARNING);
    }
  }
}
