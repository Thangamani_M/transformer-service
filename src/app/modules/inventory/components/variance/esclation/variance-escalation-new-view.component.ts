import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {CrudService, IApplicationResponse,ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService,currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR} from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { VarianceEscalationPopupModalComponent } from './variance-escalation-popup-modal.component';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
    selector: 'app-variance-escalation-new-view',
    templateUrl: './variance-escalation-new-view.component.html',
    styleUrls: ['./variance-esclation-add-edit.component.scss']
})
export class VarianceEscalationNewViewComponent extends PrimeNgTableVariablesModel implements OnInit {
    varianceInvReqId: any;
    varianceDetailsData: any;
    varianceType: any;
    userData: UserLogin;
    primengTableConfigProperties: any
    constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog,private router: Router,private store: Store<AppState>, private rxjsService: RxjsService,private crudService: CrudService, private snackbarService: SnackbarService,) {
        super();
        this.varianceInvReqId = this.activatedRoute.snapshot.queryParams.id;
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: 'View Variance Escalation',
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Variance Escalation List', relativeRouterUrl: '/inventory/variance/variance-esclation' }, { displayName: ' View Variance Escalation', relativeRouterUrl: '', }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        enableAction: true,
                        enableEditActionBtn: false,
                        enableBreadCrumb: true,
                        enableExportBtn: false,
                        enablePrintBtn: false,
                    }
                ]
            }
        }
        this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    }

    ngOnInit() {
        this.combineLatestNgrxStoreData();
        if (this.varianceInvReqId) {
            this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_ESCLATION_LIST, this.varianceInvReqId, false, null)
                .subscribe((response: IApplicationResponse) => {
                    if (response.resources && response.statusCode === 200) {
                        this.varianceDetailsData = response.resources;
                        this.varianceType = response.resources.varianceType;
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                });
        }
    }
    combineLatestNgrxStoreData() {
        combineLatest([
          this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]     
        ).subscribe((response) => {
          this.loggedInUserData= new LoggedInUserModel(response[0]);
          let permission = response[1][INVENTORY_COMPONENT.VARIANCE_ESCALATION]
          if (permission) {
            let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
            this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
          }
        });
      }
    //pickingJobPopupOpen
    pickingJobPopupOpen() {
        const dialogRemoveStockCode = this.dialog.open(VarianceEscalationPopupModalComponent, {
            width: '450px',
            data: {
                header: 'Picking Confirmation',
                message: 'Do you to create a picking job?',
                buttons: {
                    create: 'YES',
                    cancel: 'NO'
                }
            }, disableClose: true
        });
        dialogRemoveStockCode.afterClosed().subscribe(result => {
            if (!result) return;
            else {
                this.createPickingJob();
            }
            this.rxjsService.setDialogOpenProperty(false);
        });
    }

    createPickingJob() {
        let pickingParams = {
            'VarianceInvReqId': this.varianceInvReqId,
            'wareHouseId': this.userData.warehouseId,
            'StockOrderNumberAssociatedWithSerial': this.varianceDetailsData.stockOrderNumberAssociatedWithSerial,
            'ItemId': this.varianceDetailsData.itemId,
            'PickingQty': this.varianceDetailsData.quantity,
            'SerialNumber': this.varianceDetailsData.serialNumber,
            'Comments': this.varianceDetailsData.comments,
            'createdUserId': this.userData.userId
        }
        this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_PICKER_JOBS, pickingParams)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.SUCCESS);
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    return;
                }
            });
    }

    // inventory/variance/variance-esclation
    navigateToList() {
        this.router.navigate(['/inventory/variance/variance-esclation'], { skipLocationChange: true });
    }

    navigateToEditPage() {
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/inventory/variance/variance-esclation/update'], {
            queryParams: {
                id: this.varianceInvReqId
            }, skipLocationChange: true
        });
    }

}