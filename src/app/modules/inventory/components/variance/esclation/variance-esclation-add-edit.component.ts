import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { ResponseMessageTypes, VarianceInvestigationStatus } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { VarianceEsclation, VarianceEsclationDetails, VarianceInvPostItemDTOs } from '@modules/inventory/models/VarianceEsclationDetails';
import { InventoryModuleApiSuffixModels, OrderTypes } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';


@Component({
  selector: 'app-variance-esclation-add-edit',
  templateUrl: './variance-esclation-add-edit.component.html',
  styleUrls: ['./variance-esclation-add-edit.component.scss']
})
export class VarianceEsclationAddEditOldComponent implements OnInit {
  IsCreatePage: Boolean;
  varianceList: VarianceEsclationDetails;
  VarianceReqNo: any;
  OrderReceiptId: any;
  applicationResponse: IApplicationResponse;
  varianceEsclationForm: FormGroup;
  LocationList: any = '';
  varianceEsclationItem: Array<any> = [];
  documentdetails: Array<any> = [];
  dynamicdocumentdetails: Array<any> = [];
  userName: any;
  userData: UserLogin;
  fileList: File[] = [];
  listOfFiles: any[] = [];
  varianceEsclation: VarianceEsclation;
  varianceEsclationitemList: VarianceInvPostItemDTOs;
  errorMessage: any;
  IsbuttonDisabled: boolean;
  path: string;
  OrderReceiptBatchId: string;
  constructor(private httpService: CrudService, private route: ActivatedRoute, private formBuilder: FormBuilder,
    private store: Store<AppState>, private router: Router, private snackbarService: SnackbarService, private rxjsService: RxjsService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.OrderReceiptId = this.router.getCurrentNavigation().extras.state.OrderReceiptId;
      this.VarianceReqNo = this.router.getCurrentNavigation().extras.state.VarianceReqNo;
      this.OrderReceiptBatchId = this.router.getCurrentNavigation().extras.state.OrderReceiptBatchId;
    }
    if (this.OrderReceiptId == undefined) {
      this.OrderReceiptId = this.route.snapshot.queryParams.OrderReceiptId;
      this.OrderReceiptBatchId = this.route.snapshot.queryParams.OrderReceiptBatchId;
    }
  }

  ngOnInit() {
    if (this.VarianceReqNo != null && this.OrderReceiptId != null) {
      this.IsCreatePage = true;
      this.IsbuttonDisabled = true;
      this.GetVarianceEsclationDetails();

    } else {
      this.IsbuttonDisabled = false;
      this.GetVarianceEsclationDetails();

    }
    this.userName = this.userData.displayName;
    this.LoadLocationDropdown();

    this.varianceEsclationForm = this.formBuilder.group({

      varianceInvReqNumber: [this.varianceList.varianceInvReqNumber || 'null'],
      orderNumber: [this.varianceList.orderNumber || '', Validators.required],
      locationId: [this.varianceList.locationId || '', Validators.required],
      userName: [this.userName],
      varianceReason: [this.varianceList.varianceReason],
      customFileLang: ['']

    });

    if (this.VarianceReqNo != null && this.OrderReceiptId != null) {
      this.varianceEsclationForm.disable();
    }

  }
  //Load VarianceEsclation Details
  GetVarianceEsclationDetails() {
    this.varianceList = new VarianceEsclationDetails();
    let params = new HttpParams().set('OrderReceiptId', this.OrderReceiptId).set('OrderReceiptBatchId', this.OrderReceiptBatchId);
    this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_ESCLATION_DETAILS, null, null, params).subscribe(response => {
      this.varianceList = response.resources;
      this.varianceEsclationItem = this.varianceList["varianceInvReqDetailsDTOListItems"];
      this.documentdetails = this.varianceList['documentRepositoryDTO'];
      this.applicationResponse = response.resources;
      this.rxjsService.setGlobalLoaderProperty(false);

      this.varianceEsclationForm.patchValue(response.resources);
      if (this.varianceList.varianceInvReqNumber) {
        this.IsCreatePage = true;
        this.IsbuttonDisabled = true;
        this.varianceEsclationForm.disable();
      }
      if (this.varianceEsclationItem.length < 0) {
        this.IsbuttonDisabled = true;
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);

  }

  LoadLocationDropdown() {
    this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STORAGE_LOCATION,
      undefined, false).subscribe(response => {
        this.LocationList = response.resources;
        this.applicationResponse = response.resources;

      });

  }

  fileUpload(e) {

    for (let i = 0; i < e.target.files.length; i++) {
      let selectedFile = e.target.files[i];
      var fileName = this.listOfFiles.find(x => x == selectedFile.name);
      this.fileList.push(selectedFile);
      if (e.target.files[i].size > 100000000) {
        this.applicationResponse.statusCode = 204;
        this.applicationResponse.message = "Please Upload Less then 100Mb";
        this.snackbarService.openSnackbar(this.applicationResponse.message, ResponseMessageTypes.WARNING)
        return false;
      }
      else if (fileName) {
        this.applicationResponse.statusCode = 204;
        this.applicationResponse.message = "File Already Exist";
        this.snackbarService.openSnackbar(this.applicationResponse.message, ResponseMessageTypes.WARNING);
        return false;
      }
      else {
        this.listOfFiles.push(selectedFile.name);

        this.dynamicdocumentdetails = [{
          docName: selectedFile.name
        }]
        this.documentdetails.push(this.dynamicdocumentdetails[0]);

      }
    }
  }

  uploadFiles() {
    const frmData = new FormData();

    for (var i = 0; i < this.fileList.length; i++) {
      frmData.append("fileUpload", this.fileList[i]);
    }

  }
  removeSelectedFile(index) {
    this.documentdetails.splice(index, 1);
    this.fileList.splice(index, 1);
  }

  Save() {
    this.rxjsService.setGlobalLoaderProperty(true);

    var varianceEsclationDetails = new VarianceEsclation();
    if (this.VarianceReqNo != null) {

    } else {
      varianceEsclationDetails.varianceInvReqDate = new Date().toLocaleString();
      varianceEsclationDetails.orderNumber = this.varianceEsclationForm.controls.orderNumber.value;
      varianceEsclationDetails.locationId = this.varianceEsclationForm.controls.locationId.value;
      varianceEsclationDetails.varianceReason = this.varianceEsclationForm.controls.varianceReason.value;
      varianceEsclationDetails.varianceInvReqFlag = true;
      varianceEsclationDetails.warehouseId = this.userData.warehouseId;
      varianceEsclationDetails.varianceInvReqUserId = this.userData.userId;
      varianceEsclationDetails.varianceInvestigationStatus = VarianceInvestigationStatus.OPENED;
      varianceEsclationDetails.modifiedUserId = this.userData.userId;
      varianceEsclationDetails.createdUserId = this.varianceList.createdUserId;
      if (this.varianceList.createdUserId == null)
        varianceEsclationDetails.createdUserId = this.userData.userId;
      varianceEsclationDetails.orderTypeId = OrderTypes.NEW;
      varianceEsclationDetails.orderReceiptBatchId = this.OrderReceiptBatchId;
      for (let item of this.varianceEsclationItem) {
        this.varianceEsclationitemList = new VarianceInvPostItemDTOs();
        this.varianceEsclationitemList.itemId = item.itemId;
        this.varianceEsclationitemList.quantity = item.varianceQty;
        varianceEsclationDetails.VarianceInvPostItemDTOs.push(this.varianceEsclationitemList);
      }

    }
    this.IsbuttonDisabled = true;
    if (this.VarianceReqNo != null) {

    } else {
      const frmData = new FormData();
      for (var i = 0; i < this.fileList.length; i++) {
        frmData.append("fileUpload", this.fileList[i]);
      }
      frmData.append("varianceEsclation", JSON.stringify(varianceEsclationDetails));
      this.httpService.fileUpload(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_ESCLATION, frmData)
        .subscribe({
          next: response => {
            this.router.navigate(['/inventory/picker-jobs']);

          },
          error: err => this.errorMessage = err
        });
    }

    this.rxjsService.setGlobalLoaderProperty(false);

  }

}
