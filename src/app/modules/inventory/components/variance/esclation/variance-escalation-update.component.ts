import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
    clearFormControlValidators,
    ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, HttpCancelService, IApplicationResponse,
    ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService
} from '@app/shared';
import { VarianceEscalationUpdateModal } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { VarianceEscalationPopupModalComponent } from './variance-escalation-popup-modal.component';

@Component({
    selector: 'app-variance-escalation-update',
    templateUrl: './variance-escalation-update.component.html',
    styleUrls: ['./variance-esclation-add-edit.component.scss']
})

export class VarianceEscalationUpdateComponent implements OnInit {

    varianceInvReqId: any;
    varianceDetailsData: any;
    varianceEscalationUpdateForm: FormGroup;
    reasonDropDown: any = [];
    userData: UserLogin;

    stockAssociatedSerial: any;
    referenceNumber: any;
    varianceType: any;

    faultyPartyDropDown: any = [];
    selectedFiles = new Array();
    totalFileSize = 0;
    maxFileSize = 104857600; //in Bytes 100MB
    documentsDetails = new Array();

    public formData = new FormData();
    activeButtonClass: any;
    roleName: any;

    technicianDropDown: any = [];
    technicianShow: boolean = false;
    pickerPackerShow: boolean = false;
    showDuplicateError: boolean = false;
    duplicateError: any;
    varianceStatus: any;
    isCycleEnable = false;
    isConfirmSubmit: boolean = false;
    confirmContent:string;
    isCommentReq:boolean = false;
    constructor(
        private activatedRoute: ActivatedRoute, private dialog: MatDialog,
        private router: Router, private httpCancelService: HttpCancelService,
        private store: Store<AppState>, private rxjsService: RxjsService,
        private formBuilder: FormBuilder, private crudService: CrudService,
        private snackbarService: SnackbarService,) {
        this.varianceInvReqId = this.activatedRoute.snapshot.queryParams.id;
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
            this.roleName = this.userData.roleName;
        });
    }

    ngOnInit() {
        this.createVarianceUpdateForm();
        this.getDropDown();
        this.getDetailsById();
        this.varianceEscalationUpdateForm.get('faultyPartyId').valueChanges.subscribe(fromTechId => {
            this.technicianShow = false;
            this.varianceEscalationUpdateForm.get('assignCode').patchValue(null);
            if (fromTechId != '' && this.faultyPartyDropDown.length > 0) {
                let techShowValue = this.faultyPartyDropDown.filter(tech => fromTechId == tech['id']);
                if (techShowValue.length > 0) {
                    if (techShowValue[0].displayName == "Technician") {
                        this.technicianShow = true;
                        let params = new HttpParams().set('RoleName', 'Technician').set('WarehouseId', this.userData.warehouseId);
                        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_ROLE, undefined, true, params).subscribe((response: IApplicationResponse) => {
                            if (response.resources && response.statusCode == 200 && response.resources.length > 0) {
                                this.technicianDropDown = response.resources;
                                this.rxjsService.setGlobalLoaderProperty(false);
                            }
                            else {
                                this.snackbarService.openSnackbar("No data available", ResponseMessageTypes.WARNING);
                                this.rxjsService.setGlobalLoaderProperty(false);
                            }

                        });
                    }
                    if (techShowValue[0].displayName == "Picker/Packer") {
                        this.pickerPackerShow = true;
                    }
                }
            }
        });

    }

    getDetailsById() {
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_ESCLATION_LIST, this.varianceInvReqId, false, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    const varianceDetailsModel = new VarianceEscalationUpdateModal(response.resources);
                    this.varianceEscalationUpdateForm.patchValue(varianceDetailsModel);
                    this.varianceDetailsData = response.resources;

                    let documents = response.resources.documentRepositories == null ? [] : response.resources.documentRepositories;
                    for (var i = 0; i < documents.length; i++) {
                        let temp = {};
                        temp['docRepId'] = documents[i].docRepId;
                        temp['docName'] = documents[i].docName;
                        temp['path'] = documents[i].path;
                        this.documentsDetails.push(temp);
                    }

                    this.stockAssociatedSerial = response.resources.stockOrderNumberAssociatedWithSerial;
                    this.referenceNumber = response.resources.referenceVarianceEscalationNumber;
                    this.varianceType = response.resources.varianceType;
                    this.rxjsService.setGlobalLoaderProperty(false);

                    
                    if (this.varianceType == 'Excessive') {
                        if (this.stockAssociatedSerial != null && this.referenceNumber == null) {
                            this.activeButtonClass = 'Resolve';
                        }
                        else if (this.stockAssociatedSerial == null && this.referenceNumber == null &&
                            this.varianceDetailsData.cycleCountNumber == null) {
                            this.activeButtonClass = 'Cycle Count';
                            this.isCycleEnable = true;
                        }
                        else if (this.varianceDetailsData.cycleCountNumber != null ||
                            this.varianceDetailsData.stockWriteUpNumber != null) {
                            this.activeButtonClass = 'Save';
                        }
                        else if (this.stockAssociatedSerial != null && this.referenceNumber != null &&
                            this.varianceDetailsData.stockWriteUpNumber == null) {
                            this.activeButtonClass = 'Stock Write Up';
                        }

                    }
                    else if (this.varianceType == 'Short Picked') {
                        if (this.stockAssociatedSerial == null && this.referenceNumber == null &&
                            this.varianceDetailsData.cycleCountNumber == null) {
                            this.activeButtonClass = 'Cycle Count';
                            this.isCycleEnable = true;
                        }
                        else if (this.stockAssociatedSerial != null && this.referenceNumber != null &&
                            this.varianceDetailsData.cycleCountNumber == null) {
                            this.activeButtonClass = 'Resolve';
                        }
                        else if (this.varianceDetailsData.cycleCountNumber != null) {
                            this.activeButtonClass = 'Save';
                        }
                    }
                    if (this.varianceType == "Excessive" && !this.isCycleEnable) {
                        this.varianceEscalationUpdateForm = setRequiredValidator(this.varianceEscalationUpdateForm, ["varianceReasonId"]);
                    }
                    else {
                        this.varianceEscalationUpdateForm = setRequiredValidator(this.varianceEscalationUpdateForm, ["faultyPartyId"]);
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    getDropDown() {

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_REASONS, null, false, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.reasonDropDown = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_FAULTY_PARTY, null, false, null)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.faultyPartyDropDown = response.resources;
                    this.technicianShow = false;
                    this.varianceEscalationUpdateForm.get('assignCode').patchValue(null);
                    let techShowValue = this.faultyPartyDropDown.filter(tech => this.varianceEscalationUpdateForm.get('faultyPartyId').value == tech['id']);
                    if (techShowValue.length > 0) {
                        if (techShowValue[0].displayName == "Technician") {
                            this.technicianShow = true;
                            let params = new HttpParams().set('RoleName', 'Technician').set('WarehouseId', this.userData.warehouseId);
                            this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_ROLE, undefined, true, params).subscribe((response: IApplicationResponse) => {
                                if (response.resources && response.statusCode == 200 && response.resources.length > 0) {
                                    this.technicianDropDown = response.resources;
                                    this.rxjsService.setGlobalLoaderProperty(false);
                                }
                                else {
                                    this.snackbarService.openSnackbar("No data available", ResponseMessageTypes.WARNING);
                                    this.rxjsService.setGlobalLoaderProperty(false);
                                }

                            });
                        }
                        if (techShowValue[0].displayName == "Picker/Packer") {
                            this.pickerPackerShow = true;
                        }
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    createVarianceUpdateForm(esclatioListModel?: VarianceEscalationUpdateModal) {
        let varianceupdateModel = new VarianceEscalationUpdateModal(esclatioListModel);
        this.varianceEscalationUpdateForm = this.formBuilder.group({});
        Object.keys(varianceupdateModel).forEach((key) => {
            this.varianceEscalationUpdateForm.addControl(key, new FormControl(varianceupdateModel[key]));
        });
       // this.varianceEscalationUpdateForm = setRequiredValidator(this.varianceEscalationUpdateForm, ["comments"]);
    }

    // onFileChange uploadFiles
    uploadFiles(event) {
        this.selectedFiles = [];
        const supportedExtensions = ['jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'];
        for (var i = 0; i < event.target.files.length; i++) {
            let selectedFile = event.target.files[i];
            const path = selectedFile.name.split('.');
            const extension = path[path.length - 1];
            if (supportedExtensions.includes(extension)) {
                this.selectedFiles.push(event.target.files[i]);
                this.totalFileSize += event.target.files[i].size;

                let temp = {};
                temp['docRepId'] = '';
                temp['docName'] = event.target.files[i].name;
                temp['path'] = '';
                this.documentsDetails.push(temp);
            }
            else {
                this.snackbarService.openSnackbar("Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx", ResponseMessageTypes.WARNING);
            }
        }
    }

    removeSelectedFile(id, index) {
        if (id) {

            const message = `Are you sure you want to delete this?`;

            const dialogData = new ConfirmDialogModel("Confirm Action", message);

            const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
                maxWidth: "400px",
                data: dialogData
            });

            dialogRef.afterClosed().subscribe(dialogResult => {

                if (dialogResult) {
                    this.crudService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PURCHASEORDER_FILEUPLOAD, id)
                        .subscribe((response: IApplicationResponse) => {
                            if (response.isSuccess && response.statusCode === 200) {
                                this.selectedFiles.splice(index, 1);
                                this.documentsDetails.splice(index, 1);
                            }
                        });
                }

            });

        }
    }

    selectTechnician() {
        this.varianceStatus = 'Pending Investigation';
    }

    checkScanItem() {

        this.showDuplicateError = false;
        if (this.varianceEscalationUpdateForm.get('scanSerialNumberInput').value == '') {
            this.showDuplicateError = true;
            this.duplicateError = "Stock code is required";
            return;
        }

        // else if (this.varianceEscalationUpdateForm.get('scanSerialNumberInput').value != this.varianceDetailsData.serialNumber) {
        //     this.showDuplicateError = true;
        //     this.duplicateError = "Stock code is not available in the system";
        //     return;
        // }

        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        // this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_SERIAL_NUMBER, undefined, true, params).subscribe((response: IApplicationResponse) => {
         let params;
         params = {
                packerjobid:this.varianceEscalationUpdateForm.value.referenceId,
                itemId:   this.varianceEscalationUpdateForm.value.itemId,
                locationBinId :null,
                userId:this.userData.userId,
                quantity:this.varianceEscalationUpdateForm.value.quantity,
                varianceSerialNumber:this.varianceEscalationUpdateForm.value.serialNumber,
                serialNumber: this.varianceEscalationUpdateForm.get('scanSerialNumberInput').value
            }
        this.crudService.update(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.VARIANCE_ESCALATION_SCAN,
            params
          ).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                    this.snackbarService.openSnackbar("Scanned successfully", ResponseMessageTypes.SUCCESS);
                    this.getDetailsById();
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
        });
    }


    onSubmit(type: string) {
        this.rxjsService.setFormChangeDetectionProperty(true);
      //  this.varianceEscalationUpdateForm = setRequiredValidator(this.varianceEscalationUpdateForm, ["comments"]);
        if(type == 'Save' || type == 'Update') {
            this.isCommentReq = true;
            this.varianceEscalationUpdateForm = setRequiredValidator(this.varianceEscalationUpdateForm, ["comments"]);
        }else {
            this.isCommentReq = false;
            this.varianceEscalationUpdateForm = clearFormControlValidators(this.varianceEscalationUpdateForm, ["comments"]);
        }
        if (this.varianceEscalationUpdateForm.invalid) {
            this.varianceEscalationUpdateForm?.markAllAsTouched();
            return;
        }
        this.isConfirmSubmit = true;
        // if (type == 'Resolve' || type == 'Save') 
        //   this.confirmContent = type;
        // else if (type == 'Cycle Count') 
        //   this.confirmContent = type;
        // else if (type == 'Stock Write Up') 
        //   this.confirmContent = type;
        // else if (type == 'Stock Write Off') 
        //   this.confirmContent = type;
        // else if (type == 'Update') 
        //   this.confirmContent = type;
        this.confirmContent = type;
        
    }
    finalSubmit(type: string) {

       
        this.isConfirmSubmit = false;
        if (this.varianceEscalationUpdateForm.invalid) {
            return;
        }
        let formValueData = {};
        let header;
        let message;
        let button;
        let stockWriteup = {};
        let resolve = {}
        // let APIName: InventoryModuleApiSuffixModels;

        //     "VarianceInvReqId":"3A790BD8-FFA9-4572-80F4-7B6760DCF955",
        //     "VarianceReasonId":"10",
        //     "Comments":"Testing",
        //     "InvManagerId":"60CCCD59-DCA4-4C41-A1E2-A5D891F1BC21",
        //     "FaultyPartyId":"10",
        //     "TechnicianId":null,
        //    "VarianceInvestigationStatusName":"Pending Investigation"

        if (type == 'Save') {
            this.isCommentReq = true;
            this.varianceEscalationUpdateForm = setRequiredValidator(this.varianceEscalationUpdateForm, ["comments"]);
            // APIName = InventoryModuleApiSuffixModels.VARIANCE_ESCLATION_LIST;
            let resolve = {}
            resolve = {
                VarianceInvReqId: this.varianceInvReqId,
                VarianceReasonId: this.varianceEscalationUpdateForm.get('varianceReasonId').value,
                Comments: this.varianceEscalationUpdateForm.get('comments').value,
                faultyPartyId: this.varianceEscalationUpdateForm.get('faultyPartyId').value,
                InvManagerId: this.userData.userId,
                TechnicianId: this.varianceEscalationUpdateForm.get('assignCode').value,
                VarianceInvestigationStatusName: this.varianceStatus,
            }
            this.formData.append("varianceEsclationDetails", JSON.stringify(resolve));
        }
        else if(type == 'Resolve'){            
            this.isCommentReq = false;
            this.varianceEscalationUpdateForm = clearFormControlValidators(this.varianceEscalationUpdateForm, ["comments"]);
           resolve = {
                   VarianceInvReqId: this.varianceInvReqId,
                   VarianceReasonId: this.varianceEscalationUpdateForm.get('varianceReasonId').value,
                   Comments: this.varianceEscalationUpdateForm.get('comments').value,
                   faultyPartyId: this.varianceEscalationUpdateForm.get('faultyPartyId').value,
                   userId: this.userData.userId,
                //    TechnicianId: this.varianceEscalationUpdateForm.get('assignCode').value,
                //    VarianceInvestigationStatusName: this.varianceStatus,
               }
              // this.formData.append("varianceEsclationDetails", JSON.stringify(resolve));
        }
        else if (type == 'Cycle Count' || type =='GRV') {
            // APIName = InventoryModuleApiSuffixModels.CYCLE_COUNT_VARIANCE_ESCLATION;
            this.isCommentReq = false;
            this.varianceEscalationUpdateForm = clearFormControlValidators(this.varianceEscalationUpdateForm, ["comments"]);
            formValueData = {
                VarianceInvReqId: this.varianceInvReqId,
                CreatedUserId: this.userData.userId,
                WarehouseId: null,
                ItemId: this.varianceDetailsData.itemId,
                Comments: this.varianceEscalationUpdateForm.get('comments').value,
                TotalQty: this.varianceDetailsData.quantity
            }
        }

        else if (type == 'Stock Write Up') {
            // APIName = InventoryModuleApiSuffixModels.STOCK_WRITE_UP;
            this.isCommentReq = false;
            this.varianceEscalationUpdateForm = clearFormControlValidators(this.varianceEscalationUpdateForm, ["comments"]);
            stockWriteup = {
                VarianceInvReqId: this.varianceInvReqId,
                Comments: this.varianceEscalationUpdateForm.get('comments').value,
                // InvManagerId: this.userData.userId,
                userId: this.userData.userId,
                // IsStockWriteUp: true,
                // IsStockWriteOff: false,
                // VarianceInvestigationStatusName: 'Pending Investigation',
                // WarehouseId: null,
                // LocationId: this.varianceDetailsData.locationId
                // VarianceReasonId: this.varianceEscalationUpdateForm.get('varianceReasonId').value,
                // faultyPartyId: this.varianceEscalationUpdateForm.get('faultyPartyId').value,
            }
           // this.formData.append("varianceEsclationDetails", JSON.stringify(stockWriteup));

        }

        // STOCK_WRITE_OFF
        // "VarianceInvReqId":"806BD102-9C25-4568-94D1-D762E8DC3265",
        // "InvManagerId":"60CCCD59-DCA4-4C41-A1E2-A5D891F1BC21",
        // "CreatedUserId":"60CCCD59-DCA4-4C41-A1E2-A5D891F1BC21",
        // "Comments":"Testing stock write up"

        else if (type == 'Stock Write Off') {
            this.isCommentReq = false;
            this.varianceEscalationUpdateForm = clearFormControlValidators(this.varianceEscalationUpdateForm, ["comments"]);
            header = `Stock Write Off`;
            message = `Do you want to create a stock write off for <b>${this.varianceDetailsData.stockCode} - ${this.varianceDetailsData.serialNumber}</b>`;
            button = `yes/no`;
            this.onModalPopupOpen(button, header, message);
        }

        // "VarianceInvReqId":"4CCB4AB5-95E1-4266-BC4C-0419C33058E5",
        // "TAMComment":"Tech area manager comments",
        // "ModifiedUserId":"60CCCD59-DCA4-4C41-A1E2-A5D891F1BC21"

        else if (type == 'Update') {
            this.isCommentReq = true;
            this.varianceEscalationUpdateForm = setRequiredValidator(this.varianceEscalationUpdateForm, ["comments"]);
            let updateData = {}
            updateData = {
                VarianceInvReqId: this.varianceInvReqId,
                TAMComment: this.varianceEscalationUpdateForm.get('tamComment').value,
                ModifiedUserId: this.userData.userId,
            }
            this.formData.append("varianceEsclationDetails", JSON.stringify(updateData));
        }

        if (this.varianceType == 'Short Picked') {
            this.isCommentReq = false;
            this.varianceEscalationUpdateForm = clearFormControlValidators(this.varianceEscalationUpdateForm, ["comments"]);
            if (this.selectedFiles.length > 0) {
                for (const file of this.selectedFiles) {
                    this.formData.append('File', file);
                }
            }
        }
        //old code
        // let crudService: Observable<IApplicationResponse> = type == 'Resolve' || type == 'Save' ?
        //     this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_ESCLATION_LIST, this.formData) :
        //     type == 'Stock Write Up' ? this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_WRITE_UP, this.formData) :
        //         // type == 'Stock Write Off' ? this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_WRITE_OFF, formValueData) :
        //         type == 'Update' ? this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_TECH_AREA_MANAGER, this.formData) :
        //             this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_VARIANCE_ESCLATION, formValueData)
        // crudService
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let crudService;
                    //||'Stock Write Off'
        switch(type){
            case 'Save':
                crudService = this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_ESCLATION_LIST, this.formData);

            break;
            case 'Resolve':
                crudService = this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_ESCLATION_RESOLVE, resolve);
            break
            case 'Stock Write Up':
                crudService = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_WRITE_UP_VARIANCE_ESCALATION, stockWriteup)
            break;
            case 'Update':
                crudService = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_TECH_AREA_MANAGER, this.formData);
            break;
            case 'GRV':
                crudService =  this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.GRV_ESCALATION, formValueData);
            break;
            case 'Cycle Count':
                crudService =  this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CYCLE_COUNT_VARIANCE_ESCLATION, formValueData);
            break;
        }
  
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.confirmContent = type;
                if ((type == 'Resolve' || type == 'Save') && this.varianceDetailsData.cycleCountNumber == null) {
                    this.navigateToList();
                }
                else {
                    // if(type == 'Save' && this.varianceDetailsData.cycleCountNumber != null){
                    //     if(this.technicianShow || this.pickerPackerShow){
                    //         header = `Stock Write Off`;
                    //         message = `Do you want to create a stock write off for <b>${this.varianceDetailsData.stockCode} - ${this.varianceDetailsData.serialNumber}</b>`;
                    //         button = `yes/no`;
                    //     }
                    // }
                    // else {
                       if (type == 'Cycle Count' || type == 'Stock Write Up' || type == 'Stock Write Off' ) {
                        this.confirmContent = type;
                        header = `Request Confirmation`;
                        message = `${type}</b> Request <b>${response.resources}</b> </br> created successfully`
                        this.onModalPopupOpen(button, header, message);
    
                        this.rxjsService.setGlobalLoaderProperty(false);
                       }
                       else if(response.message) {
                        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.SUCCESS);
                        this.navigateToViewPage();
                        this.rxjsService.setGlobalLoaderProperty(false);
                       }                   
                    // }
              
                }
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });

    }
    finalCancel(){
        this.isConfirmSubmit = false;
    }
    onModalPopupOpen(button: string, header: string, message: string) {
        if (button == 'yes/no') {
            const dialogRemoveStockCode = this.dialog.open(VarianceEscalationPopupModalComponent, {
                width: '450px',
                data: {
                    header: header,
                    message: message,
                    buttons: {
                        create: 'YES',
                        cancel: 'NO'
                    }
                }, disableClose: true
            });
            dialogRemoveStockCode.afterClosed().subscribe(result => {
                if (!result) return;
                else {
                    this.stockWriteOffGetMethod();
                }
                this.rxjsService.setDialogOpenProperty(false);
            });
        }
        else {
            const dialogRemoveStockCode = this.dialog.open(VarianceEscalationPopupModalComponent, {
                width: '450px',
                data: {
                    header: header,
                    message: message,
                    buttons: {
                        create: 'OK'
                    }
                }, disableClose: true
            });
            dialogRemoveStockCode.afterClosed().subscribe(result => {
                if (!result) return;
                else {
                    this.navigateToList();
                }
                this.rxjsService.setDialogOpenProperty(false);
            });
        }
    }

    stockWriteOffGetMethod() {
        let formValueData = {
            VarianceInvReqId: this.varianceInvReqId,
            CreatedUserId: this.userData.userId,
            InvManagerId: this.userData.userId,
            IsStockWriteUp: false,
            IsStockWriteOff: true,
            TAMComment: this.varianceEscalationUpdateForm.get('tamComment').value,
        }

        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_WRITE_OFF, formValueData)
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.stockWriteOffPopup(response.resources)
                }
                else {
                    this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    stockWriteOffPopup(response) {
        const dialogRemoveStockCode = this.dialog.open(VarianceEscalationPopupModalComponent, {
            width: '450px',
            data: {
                header: `Stock Write Off`,
                message: `Stock write off <b>${this.varianceDetailsData.stockCode} - ${this.varianceDetailsData.serialNumber}</b> for <b>${response}</b> created successfully`,
                buttons: {
                    create: 'OK'
                }
            }, disableClose: true
        });
        dialogRemoveStockCode.afterClosed().subscribe(result => {
            if (!result) return;
            else {
                this.navigateToList();
            }
            this.rxjsService.setDialogOpenProperty(false);
        });
    }

    onModalPopupAckOpen() {
        const dialogRemoveStockCode = this.dialog.open(VarianceEscalationPopupModalComponent, {
            width: '450px',
            data: {
                header: `Picking Confirmation`,
                message: `Do you want to Create a picking job?`,
                buttons: {
                    create: 'YES',
                    cancel: 'NO'
                }
            }, disableClose: true
        });
        dialogRemoveStockCode.afterClosed().subscribe(result => {
            if (!result) return;
            else {
                this.navigateToList();
            }
            this.rxjsService.setDialogOpenProperty(false);
        });
    }

    navigateToList() {
        this.router.navigate(['/inventory/variance/variance-esclation'], { skipLocationChange: true });
    }

    navigateToViewPage() {
        this.router.navigate(['/inventory/variance/variance-esclation/view'], {
            queryParams: {
                id: this.varianceInvReqId
            }, skipLocationChange: true
        });
    }

}
