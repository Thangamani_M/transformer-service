import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { StockInitiationAddEditComponent, StockInitiationListComponent, StockInitiationViewComponent } from '@inventory-components/stock-initiation';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { StockInitiationRoutingModule } from './stock-initiation-routing.module';

@NgModule({
  declarations: [StockInitiationListComponent, StockInitiationAddEditComponent, StockInitiationViewComponent, StockInitiationViewComponent],
  imports: [
    CommonModule,
    StockInitiationRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule

  ],
})
export class StockInitiationModule { }
