import { DatePipe } from '@angular/common';
import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-stock-initiation-add-edit',
  templateUrl: './stock-initiation-add-edit.component.html'
})
export class StockInitiationAddEditComponent implements OnInit, AfterViewChecked {
  warehouseStockTakeInitiationId: any;
  stockInitiateForm: FormGroup;
  userData: UserLogin;
  dropdownData = [];
  warehouseList = [];
  scheduledDateList = [];
  stockInitiate: any;
  primengTableConfigProperties;

  constructor(private store: Store<AppState>, private datePipe: DatePipe,
    private activatedRoute: ActivatedRoute, private crudService: CrudService,
    private formBuilder: FormBuilder, private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private rxjsService: RxjsService,
  ) {
    this.warehouseStockTakeInitiationId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    let title = this.warehouseStockTakeInitiationId ? 'Update' :'Add';
    this.primengTableConfigProperties = {
      tableCaption: title+" Stock Take Scheduling",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Take', relativeRouterUrl: '' },
      { displayName: 'Stock Take Scheduling List', relativeRouterUrl: '/inventory/stock/stock-initiation' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: false,
            enableEditActionBtn: false,
            enableClearfix: true,
          }]
      }
    }
    if(this.warehouseStockTakeInitiationId)
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Stock Take Scheduling', relativeRouterUrl: '/inventory/stock/stock-initiation/view' , queryParams: {id: this.warehouseStockTakeInitiationId}});
    
    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title+' Stock Take Scheduling', relativeRouterUrl: '' });
    
  }

  ngOnInit(): void {
    this.initStockInitiateForm();
    if (this.warehouseStockTakeInitiationId) {
      this.dropdownData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.userData?.userId })),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_INITIATION_SCHEDULED_DATE),
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_INITIATION_LIST, this.warehouseStockTakeInitiationId)];
    } else {
      this.dropdownData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.userData?.userId })),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_INITIATION_SCHEDULED_DATE)];
    }
    this.loadActionTypes(this.dropdownData);
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  initStockInitiateForm() {
    this.stockInitiateForm = this.formBuilder.group({});
    this.stockInitiateForm.addControl('warehouseId', new FormControl(''));
    this.stockInitiateForm.addControl('warehouseScheduleDateId', new FormControl(''));
    this.stockInitiateForm.addControl('warehouseStockTakeInitiationId', new FormControl(''));
    this.stockInitiateForm = setRequiredValidator(this.stockInitiateForm, ['warehouseId', 'warehouseScheduleDateId']);
  }

  loadActionTypes(dropdownData) {
    forkJoin(dropdownData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.warehouseList = [];
              if (resp.resources.length) {
                for (let i = 0; i < resp.resources.length; i++) {
                  let temp = {};
                  temp['display'] = resp.resources[i].displayName;
                  temp['value'] = resp.resources[i].id;
                  this.warehouseList.push(temp);
                }
              }
              break;
            case 1:
              this.scheduledDateList = resp.resources;
              break;

            case 2:
              this.stockInitiate = resp.resources;
              this.stockInitiateForm.get('warehouseStockTakeInitiationId').setValue(this.stockInitiate['warehouseStockTakeInitiationId']);
              this.stockInitiateForm.get('warehouseId').setValue([this.stockInitiate['warehouseId']]);
              this.stockInitiateForm.get('warehouseScheduleDateId').setValue(this.stockInitiate['warehouseScheduleDateId']);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }



  submitStockInitiation() {
    if (this.stockInitiateForm.invalid) {
      this.stockInitiateForm.markAllAsTouched();
      return;
    }
    let submit$;
    let dataToSend: any;;

    if (this.warehouseStockTakeInitiationId) {
      dataToSend = {
        warehouseStockTakeInitiationId: this.stockInitiateForm.get('warehouseStockTakeInitiationId').value,
        warehouseId: this.stockInitiateForm.get('warehouseId').value.join(),
        warehouseScheduleDateId: this.stockInitiateForm.get('warehouseScheduleDateId').value,
        modifiedUserId: this.userData.userId
      };
      submit$ = this.crudService.update(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_INITIATION_LIST,
        dataToSend
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }));
    } else {
      dataToSend = [];
      this.stockInitiateForm.get('warehouseId').value.forEach(element => {
        const stockInitiationData = {
          warehouseId: element,
          warehouseScheduleDateId: this.stockInitiateForm.get('warehouseScheduleDateId').value,
          createdUserId: this.userData.userId
        };
        dataToSend.push(stockInitiationData);
      });
      submit$ = this.crudService.create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_INITIATION_LIST,
        dataToSend
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }));
    }
    submit$.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.redirectToList();
      }
    });
  }

  redirectToList() {
    this.router.navigate(['/inventory/stock/stock-initiation']);
  }

  redirectToView() {
    this.router.navigate(['/inventory/stock/stock-initiation/view'], { queryParams: { id: this.warehouseStockTakeInitiationId }, skipLocationChange: true });
  }

  onCRUDRequested(ev){}

}
