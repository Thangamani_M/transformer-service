import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockInitiationAddEditComponent, StockInitiationListComponent } from '@inventory-components/stock-initiation';
import { StockInitiationViewComponent } from './stock-initiation-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
    { path: 'stock-initiation', component: StockInitiationListComponent, data: { title: 'Stock Take Scheduling List' } ,canActivate:[AuthGuard] },
    { path: 'stock-initiation/add-edit', component: StockInitiationAddEditComponent, data: { title: 'Stock Take Scheduling Add Edit' } ,canActivate:[AuthGuard] },
    { path: 'stock-initiation/view', component: StockInitiationViewComponent, data: { title: 'Stock Take Scheduling View' }  ,canActivate:[AuthGuard]},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class StockInitiationRoutingModule { }
