import { Component, QueryList, ViewChildren } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, CustomDirectiveConfig, exportList,currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  RxjsService,
  prepareDynamicTableTabsFromPermissions,
  PERMISSION_RESTRICTION_ERROR,
  ResponseMessageTypes,
  SnackbarService,
  IApplicationResponse
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Table } from 'primeng/table';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-stock-initiation-list',
  templateUrl: './stock-initiation-list.component.html'
})
export class StockInitiationListComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties: any;
  row: any = {};
  otherParams;
  constructor(private snackbarService:SnackbarService,
    private crudService: CrudService,private activatedRoute: ActivatedRoute,public dialogService: DialogService,private router: Router, private store: Store<AppState>,private rxjsService: RxjsService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Stock Take Scheduling",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Stock Take Scheduling List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Stock Take Scheduling List',
            dataKey: 'warehouseStockTakeInitiationId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            enablePrintBtn: true,
            printTitle: 'Stock Take Scheduling List',
            printSection: 'print-section0',
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'warehouseStockTakeNumber', header: 'Warehouse Stock Take ID', width: '200px' },
              { field: 'warehouseName', header: 'Warehouse', width: '200px' },
              { field: 'scheduledBy', header: 'Scheduled By', width: '200px' },
              { field: 'scheduled', header: 'Scheduled', width: '200px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_INITIATION_LIST,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableExportBtn: true,
            shouldShowFilterActionBtn: false,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.STOCK_TAKE_SCHEDULING];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = {
      userId: this.loggedInUserData?.userId,
      ...otherParams,
    };
    this.otherParams = otherParams;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.scheduled = val?.scheduledDate;
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;

      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row=row;
        if(unknownVar['scheduled']){
          unknownVar['scheduledDate'] = unknownVar['scheduled'];
        }
        if(unknownVar['sortOrderColumn']=='scheduled'){
          unknownVar['sortOrderColumn'] = 'scheduledDate';
        }
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex]?.canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
          let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
          this.exportList(pageIndex,pageSize);
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams,0,pageSize,ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_EXPORT,this.crudService,this.rxjsService,'Stock Take Scheduling');
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(["inventory/stock/stock-initiation/add-edit"], { skipLocationChange: true });
        break;
      case CrudType.VIEW:
        this.router.navigate(['inventory/stock/stock-initiation/view'],
          {
            queryParams: {
              id: editableObject['warehouseStockTakeInitiationId']
            }, skipLocationChange: true
          });
        break;
    }
  }
}
