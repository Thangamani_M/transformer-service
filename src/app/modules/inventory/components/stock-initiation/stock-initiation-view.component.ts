import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, RxjsService,currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { combineLatest } from 'rxjs';
import { Store } from '@ngrx/store';
import { loggedInUserData } from '@modules/others';
import { AppState } from '@app/reducers';
@Component({
  selector: 'app-stock-initiation-view',
  templateUrl: './stock-initiation-view.component.html',
  styleUrls: ['./stock-initiation-view.component.scss']
})
export class StockInitiationViewComponent implements OnInit {
  StockInitiationView: any;
  Stockinitiationviewdata: any;
  primengTableConfigProperties: any;
  warehouseStockTakeInitiationId: any;
  loggedInUserData;
  selectedTabIndex=0
  constructor(private router: Router,private store: Store<AppState>,
    private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,private snackbarService:SnackbarService
  ) {
    this.warehouseStockTakeInitiationId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "View Stock Take Scheduling",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Take', relativeRouterUrl: '' },
      { displayName: 'Stock Take Scheduling List', relativeRouterUrl: '/inventory/stock/stock-initiation' }, { displayName: 'View Stock Take Scheduling', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.StockInitiationView = [
      { name: 'Warehouse', value: '' },
      { name: 'Scheduled By', value: '' },
      { name: 'Scheduled Date', value: '' },
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.warehouseStockTakeInitiationId) {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_TAKE_INITIATION_LIST, this.warehouseStockTakeInitiationId, false, null)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.Stockinitiationviewdata = response.resources;
            this.StockInitiationView = [
              { name: 'Warehouse', value: response.resources?.warehouseName },
              { name: 'Scheduled By', value: response.resources?.scheduledBy },
              {name: 'Scheduled Date', value: response.resources?.scheduledDate, isValue: true},
            ]
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.STOCK_TAKE_SCHEDULING];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEditPage();
        break;
    }
  }
  navigateToEditPage() {
    if (this.warehouseStockTakeInitiationId) {
      this.router.navigate(['inventory/stock/stock-initiation/add-edit'], { queryParams: { id: this.warehouseStockTakeInitiationId }, skipLocationChange: true });

    }
  }
}