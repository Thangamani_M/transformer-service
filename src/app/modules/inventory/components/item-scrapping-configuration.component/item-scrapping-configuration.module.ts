import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from '@angular/router';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ItemScrappingConfigurationComponent } from './item-scrapping-configuration.component';
import { ItemTypeAddEditComponent } from './item-type/item-type-add-edit.component';
import { ItemTypeViewComponent } from './item-type/item-type-view.component';
import { RadioSystemSetupAddEditComponent } from './radio-system-setup/radio-system-setup-add-edit.component';
import { RadioSystemSetupViewComponent } from './radio-system-setup/radio-system-setup-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: '', component: ItemScrappingConfigurationComponent, canActivate: [AuthGuard], data: { title: 'Item Scrapping Configuration' },
  },
  {
    path: 'item-type/add-edit', component: ItemTypeAddEditComponent, canActivate: [AuthGuard], data: { title: 'Item Type Add/Edit' },
  },
  {
    path: 'item-type/view', component: ItemTypeViewComponent, canActivate: [AuthGuard], data: { title: 'Item Type View' },
  },
  {
    path: 'radio-system-setup/add-edit', component: RadioSystemSetupAddEditComponent, canActivate: [AuthGuard], data: { title: 'Radio System Setup Add/Edit' },
  },
  {
    path: 'radio-system-setup/view', component: RadioSystemSetupViewComponent, canActivate: [AuthGuard], data: { title: 'Radio System Setup View' },
  }
]
@NgModule({
  declarations: [ItemScrappingConfigurationComponent, ItemTypeAddEditComponent, ItemTypeViewComponent, RadioSystemSetupAddEditComponent, RadioSystemSetupViewComponent],
  imports: [CommonModule, LayoutModule, MaterialModule, FormsModule, ReactiveFormsModule, SharedModule, RouterModule.forChild(routes)],

  entryComponents: [],
  providers: [DatePipe]
})

export class ItemScrappingConfigurationModule { }
