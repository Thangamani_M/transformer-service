import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, ModulesBasedApiSuffix, CrudType, RxjsService, currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";

@Component({
  selector: 'app-radio-system-setup-view',
  templateUrl: './radio-system-setup-view.component.html',
})
export class RadioSystemSetupViewComponent {
  primengTableConfigProperties: any;
  RadioSystemSetupviewDetail: any;
  loggedUser: any;
  id;
  radioSystemDetails;
  selectedTabIndex: any = 0;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}, {}, {}, {},{}]
    }
  }
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "View Radio System Setup",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Item Scrapping Configuration List', relativeRouterUrl: '/radio-removal/radio-removal-config/item-scrapping-configuration' }, { displayName: 'Item Type View', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "View Radio System Setup",
            dataKey: "itemTypeConfigId",
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
            ],
            enableEditActionBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
          }
        ]
      }
    }
    this.id = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getItemTypeDetail();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Config"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getItemTypeDetail() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_SCRAPPING, this.id, false,
      null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.radioSystemDetails = data.resources;
        this.onShowValue(data.resources);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  onShowValue(response?: any) {
    this.RadioSystemSetupviewDetail = [
      { name: 'Item Type', value: response?.itemTypeName },
      { name: 'Created By', value: response?.createdBy },
      { name: 'Stock Code', value: response?.itemCode },
      { name: 'Created Date', value: response?.createdDate },
      { name: 'Stock Description', value: response?.itemName },
      { name: 'Reasons', value: response?.reason },
      { name: 'Status', value: response.status == "Active" ? 'Active' : 'In-Active', statusClass: response.status == "Active" ? "status-label-green" : 'status-label-pink' },
    ]
  }

  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[9].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.edit()
        break;
    }
  }

  navigate() {
    this.router.navigate(["radio-removal/radio-removal-config/item-scrapping-configuration"], { queryParams: { tab: 9 } });
  }
  edit() {
    this.router.navigate(["radio-removal/radio-removal-config/item-scrapping-configuration/radio-system-setup/add-edit"], { queryParams: { id: this.id } });
  }
}
