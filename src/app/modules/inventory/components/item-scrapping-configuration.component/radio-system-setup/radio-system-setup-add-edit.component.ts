import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { clearFormControlValidators, CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { RadioSystemSetupModel } from "@modules/inventory/models/radio-system-setup.model";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Component({
    selector: 'app-radio-system-setup-add-edit',
    templateUrl: './radio-system-setup-add-edit.component.html',
    styleUrls: ['./radio-system-setup-add-edit.component.scss']
})
export class RadioSystemSetupAddEditComponent {
    primengTableConfigProperties: any;
    selectedTabIndex: any = 0;
    loading: boolean;
    itemTypeList: any = [];
    filteredStockCodes = [];
    loggedUser: any;
    filterData;
    radioSystemSetupAddForm: FormGroup;
    radioSystemSetupEditForm: FormGroup;
    id; radioSystemDetails;
    searchTerm: string;
    statusList;
    showPopup = false;
    stockCode:string;
    index;
    isLoading:boolean=false;
    constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
        private router: Router, private formBuilder: FormBuilder) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.id = this.activatedRoute.snapshot.queryParams.id;
        let title = this.id ? 'Edit Radio System Component' : 'Add Radio System Component';
        this.primengTableConfigProperties = {
            tableCaption: title,
            breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Item Scrapping Configuration List', relativeRouterUrl: '/radio-removal/radio-removal-config/item-scrapping-configuration', screen: 'list' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Add Radio System Component',
                        dataKey: 'itemTypeConfigId',
                        captionFontSize: '21px',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: false,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                        ],
                        apiSuffixModel: InventoryModuleApiSuffixModels.ITEM_SCRAPPING,
                        moduleName: ModulesBasedApiSuffix.INVENTORY,
                    }
                ]
            }
        }
        this.statusList = [
            { displayName: "Active", id: true },
            { displayName: "In-Active", id: false },
        ];
        if (this.id)
            this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'Radio/System Component View', relativeRouterUrl: 'radio-removal/radio-removal-config/item-scrapping-configuration/radio-system-setup/view', screen: 'view' })

        this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title, relativeRouterUrl: '' })
    }
    ngOnInit(): void {
        this.rxjsService.setGlobalLoaderProperty(false);
        this.createFilterForm();
        this.getItemDropDown();
        if (this.id) {
            this.getItemTypeDetail();
            this.editForm();
        }
    }
    createFilterForm(): void {
        let filterFormModel = new RadioSystemSetupModel();
        // create form controls dynamically from model class
        this.radioSystemSetupAddForm = this.formBuilder.group({
            radioSystemSetupAddEdit: this.formBuilder.array([])
        });
        Object.keys(filterFormModel).forEach((key) => {
            this.radioSystemSetupAddForm.addControl(key, new FormControl(filterFormModel[key]));
        });
       // this.radioSystemSetupAddForm = setRequiredValidator(this.radioSystemSetupAddForm, ["itemCode", "itemTypeConfigId"]);
        this.rxjsService.setGlobalLoaderProperty(false);
    }
    editForm() {
        this.radioSystemSetupEditForm = new FormGroup({
            'itemTypeConfigId': new FormControl(null),
            'itemId': new FormControl(null),
            'isActive': new FormControl(null),
            'reason': new FormControl(null),
            'itemScrappingId': new FormControl(null)
        });
        this.radioSystemSetupEditForm = setRequiredValidator(this.radioSystemSetupEditForm, ["itemTypeConfigId", "isActive"]);
    }
    getItemDropDown() {
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_TYPE_CONFIG, undefined, null, prepareRequiredHttpParams({ isAll: true }))
            .subscribe((response: IApplicationResponse) => {
              this.rxjsService.setGlobalLoaderProperty(false);
                if (response.statusCode == 200 && response.isSuccess == true) {
                    this.itemTypeList = response.resources;
                }

            });

    }
    getItemTypeDetail() {
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.ITEM_SCRAPPING, this.id, false,
            null
        ).subscribe(data => {
           this.rxjsService.setGlobalLoaderProperty(false);
            if (data.isSuccess && data.statusCode == 200) {
                this.radioSystemDetails = data.resources;
                this.setValue(data.resources);
            }
        });
    }
    setValue(response) {
        this.radioSystemSetupEditForm.get('itemTypeConfigId').setValue(response.itemTypeConfigId);
        this.radioSystemSetupEditForm.get('itemScrappingId').setValue(this.id);
        this.radioSystemSetupEditForm.get('itemId').setValue(response.itemId);
        this.radioSystemSetupEditForm.get('reason').setValue(response.reason);
        this.radioSystemSetupEditForm.get('isActive').setValue(response.status == 'Active' ? true : false);
    }
    //Add  Details
    addItem() {
        this.radioSystemSetupAddForm = setRequiredValidator(this.radioSystemSetupAddForm, ["itemCode", "itemTypeConfigId"]);

        if (this.radioSystemSetupAddForm.invalid) {
            this.radioSystemSetupAddForm.markAllAsTouched();
            return;
        }
       let itemTypeConfigValArr = this.itemTypeList.filter(x=>x.id==this.radioSystemSetupAddForm.get("itemTypeConfigId").value);
       let itemTypeConfigVal = itemTypeConfigValArr && itemTypeConfigValArr.length >0 ? itemTypeConfigValArr[0]?.displayName:null;
   
       let radioSystem = this.formBuilder.group({
            itemTypeConfigId: this.radioSystemSetupAddForm.get("itemTypeConfigId").value,
            itemTypeConfigVal:itemTypeConfigVal,
            itemId: this.radioSystemSetupAddForm.get("itemId").value,
            itemCode: this.radioSystemSetupAddForm.get("itemCode").value,
            itemName: this.radioSystemSetupAddForm.get("itemName").value,
            reason: this.radioSystemSetupAddForm.get("reason").value,
            isActive: true,
            createdDate: new Date(),
            createdUserId: this.loggedUser.userId
        })
        radioSystem = setRequiredValidator(radioSystem, ["itemCode", "itemTypeConfigId"]);
        this.getItemsArray.push(radioSystem);
        this.radioSystemSetupAddForm.get('itemTypeConfigVal').reset();
        this.radioSystemSetupAddForm.get('itemTypeConfigId').reset();
        this.radioSystemSetupAddForm.get('itemCode').reset();
        this.radioSystemSetupAddForm.get('itemName').reset();
        this.radioSystemSetupAddForm.get('reason').reset();
       this.radioSystemSetupAddForm = clearFormControlValidators(this.radioSystemSetupAddForm, ["itemCode", "itemTypeConfigId"]);
    }
    get getItemsArray(): FormArray {
        if (this.radioSystemSetupAddForm !== undefined) {
            return (<FormArray>this.radioSystemSetupAddForm.get('radioSystemSetupAddEdit'));
        }
    }
    removeStockOrderItems(index) {
        this.index = index;
        this.stockCode = this.getItemsArray.controls[index].get('itemCode').value;
        this.showPopup = !this.showPopup;
    }
    deletePopup(){
        this.getItemsArray.removeAt(this.index);
        this.showPopup = !this.showPopup;
    }
    cancelPopup(){
      this.showPopup = !this.showPopup;
    }
    onChangeStockCode(event, type, index?: any) {
        let params = { searchText: event.target.value,isAll:true };
        this.searchTerm = event.target.value;
        let api = this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, false, prepareRequiredHttpParams(params));
        api.subscribe((response: IApplicationResponse) => {
          this.rxjsService.setGlobalLoaderProperty(false);
            if (response.isSuccess && response.statusCode == 200 && response.resources) {
                this.filteredStockCodes = response.resources;
            }
        });
    }
    onStatucCodeSelected(value, type, index?: any) {
        if (value) {
            let filter = this.filteredStockCodes.filter(x => x.displayName == value);
            if (filter && filter.length > 0) {
                this.getStockDetails(filter[0].id).subscribe((res) => {
                  this.rxjsService.setGlobalLoaderProperty(false);
                    if (index) { }
                    else {
                        this.radioSystemSetupAddForm.get("itemId").setValue(res.resources.itemId);
                        this.radioSystemSetupAddForm.get("itemCode").setValue(res.resources.itemCode);
                        this.radioSystemSetupAddForm.get("itemName").setValue(res.resources.itemName);
                    }
                });

            }
        }

    }
    getStockDetails(id): Observable<any> {
        return this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.ITEM,
            id,
            false,
            prepareRequiredHttpParams({})
        ).pipe(
            map(result => {
                return result;
            })
        );
    }
    // BreadCrumb click -
    onBreadCrumbClick(breadCrumbItem: object, i): void {
        if (breadCrumbItem['screen'] == 'view') {
            this.router.navigate([`${breadCrumbItem["relativeRouterUrl"]}`], {
                queryParams: { id: this.id },
            });
        } else {
            this.router.navigate([`${breadCrumbItem["relativeRouterUrl"]}`], {
                queryParams: { tab: 9 },
            });
        }
    }
    onSubmit() {
        this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
        if (this.radioSystemSetupAddForm.invalid) {
            return;
        }
        let formValue = this.radioSystemSetupAddForm.value.radioSystemSetupAddEdit;
        let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_SCRAPPING, formValue);
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigate();
            }
        });
    }
    update() {
        this.rxjsService.setFormChangeDetectionProperty(true); // Form submitt No changes snackbar prevent
        if (this.radioSystemSetupEditForm.invalid) {
            return;
        }
        let formValue = [];
        formValue.push(this.radioSystemSetupEditForm.value);
        let crudService: Observable<IApplicationResponse> = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_SCRAPPING, formValue);
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigate();
            }
        });
    }
    navigate() {
        this.router.navigate(["radio-removal/radio-removal-config/item-scrapping-configuration"], { queryParams: { tab: 9 } });
    }
}
