import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";

@Component({
    selector: 'app-item-type-add-edit',
    templateUrl: './item-type-add-edit.component.html',
    styleUrls: ['./item-type-add-edit.component.scss']
})
export class ItemTypeAddEditComponent {
    primengTableConfigProperties: any;
    itemTypeAddEditForm: FormGroup;
    loggedUser;feature;id;
    statusList;
    selectedTabIndex=0;
    itemTypeDetails;
    constructor( private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private store: Store<AppState>,
        private crudService: CrudService,  private router: Router) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.id = this.activatedRoute.snapshot.queryParams.id;
        let title =this.id ? 'Edit Item Type':'Add Item Type';
        this.primengTableConfigProperties = {
            tableCaption: title,
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: ''}, { displayName: 'Item Scrapping Configuration List', relativeRouterUrl: '/radio-removal/radio-removal-config/item-scrapping-configuration',screen:'list' }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Add Item Type',
                        dataKey: 'itemTypeConfigId',
                        enableBreadCrumb: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        checkBox: false,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        enableAddActionBtn: true,
                        shouldShowDeleteActionBtn: false,
                        shouldShowCreateActionBtn: true,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        url: '',
                    },
                ]
            }
        }
        this.statusList = [
            { displayName: "Active", id: true },
            { displayName: "In-Active", id: false },
        ];

        if(this.id)
            this.primengTableConfigProperties.breadCrumbItems.push({displayName:'Item View',relativeRouterUrl:'radio-removal/radio-removal-config/item-scrapping-configuration/item-type/view',screen:'view'})

        this.primengTableConfigProperties.breadCrumbItems.push({displayName:title,relativeRouterUrl:''})

        setTimeout(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
        }, 500);

    }
    ngOnInit(): void {
        this.initForm();
        if (this.id)
            this.getItemTypeDetail();
    }
    initForm() {
        this.itemTypeAddEditForm = new FormGroup({
            'itemTypeName': new FormControl(null),
            'reasons': new FormControl(null),
            'isActive': new FormControl(null),
        });
        this.itemTypeAddEditForm = setRequiredValidator(this.itemTypeAddEditForm, ["itemTypeName","reasons","isActive"]);
    }
    getItemTypeDetail() {
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.ITEM_TYPE_CONFIG, this.id, false,
            null
        ).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200 && data.resources) {
                this.itemTypeDetails=data.resources;
                this.setValue(data.resources);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }
    setValue(data) {
       this.itemTypeAddEditForm.get('itemTypeName').setValue(data.itemTypeName);
       this.itemTypeAddEditForm.get('reasons').setValue(data.reasons);
       this.itemTypeAddEditForm.get('isActive').setValue(data.status=='Active'?true:false);
    }
     // BreadCrumb click -
    onBreadCrumbClick(breadCrumbItem: object,i): void {
        if (breadCrumbItem['screen']=='view'){
            this.router.navigate([`${breadCrumbItem["relativeRouterUrl"]}`], {
                queryParams:  { id:this.id},
            });
        }else {
            this.router.navigate([`${breadCrumbItem["relativeRouterUrl"]}`], {
                queryParams:  { tab:8},
            });
        }
    }
    onSubmit() {
        if (this.itemTypeAddEditForm.invalid) {
            this.itemTypeAddEditForm.markAllAsTouched();
            return;
        }
        let formValue = this.itemTypeAddEditForm.getRawValue();
        let finalObject;
        finalObject= {
            itemTypeName: formValue.itemTypeName,
            reasons:formValue.reasons,
            isActive: formValue.isActive,
            itemTypeConfigId: this.id? this.id :null
        }
        if(!this.id){
            finalObject.createdDate=new Date();
            finalObject.createdUserId=this.loggedUser.userId;
        }
        let api = this.id ?  this.crudService.update(ModulesBasedApiSuffix.INVENTORY,InventoryModuleApiSuffixModels.ITEM_TYPE_CONFIG, finalObject, 1): this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM_TYPE_CONFIG, finalObject, 1);
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigate();
            }
            this.rxjsService.setDialogOpenProperty(false);
        });

    }
    navigate() {
        this.router.navigate(["radio-removal/radio-removal-config/item-scrapping-configuration"], { queryParams: { tab: 8 } });
    }

}
