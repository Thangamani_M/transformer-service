import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";

@Component({
  selector: 'app-item-type-view',
  templateUrl: './item-type-view.component.html'
})
export class ItemTypeViewComponent {
  primengTableConfigProperties: any;
  loggedUser: any;
  id;
  itemTypeDetails;
  selectedTabIndex: any = 0;
  primengTableConfigPropertiesObj: any = {
    tableComponentConfigs: {
      tabsList: [{}, {}, {}, {}, {}, {}, {}, {}, {}]
    }
  }
  viewData:any = []
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
    private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "View Item type",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Item Scrapping Configuration List', relativeRouterUrl: '/radio-removal/radio-removal-config/item-scrapping-configuration' }, { displayName: 'Item Type View', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "View Item type",
            dataKey: "itemTypeConfigId",
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
            ],
            enableAddActionBtn: false,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableEditActionBtn: true,
          }
        ]
      }
    }
    this.id = this.activatedRoute.snapshot.queryParams.id;
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getItemTypeDetail();
    this.viewData = [
      { name: 'Item Type', value: '' },
      { name: 'Created Date', value: '' },
      { name: 'Created By', value: '' },
      { name: 'Reasons', value: '' },
      { name: 'Status', value: '' },
    ]
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Config"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigPropertiesObj, permission);
        this.primengTableConfigPropertiesObj = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getItemTypeDetail() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ITEM_TYPE_CONFIG, this.id, false,
      null
    ).subscribe(data => {
      if (data.isSuccess && data.statusCode == 200) {
        this.itemTypeDetails = data.resources;
        this.viewData = [
          { name: 'Item Type', value: data.resources?.itemTypeName },
          { name: 'Created Date', value: data.resources?.createdDate,isDateTime:true },
          { name: 'Created By', value: data.resources?.createdBy },
          { name: 'Reasons', value: data.resources?.reasons },
          { name: 'Status', value: data.resources?.status == "Active" ? 'Active' : 'In-Active', statusClass: data.resources.status == "Active" ? "status-label-green" : 'status-label-red' },
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }
  onBreadCrumbClick(breadCrumbItem: object): void {
    this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
      { queryParams: { tab: 8 } });
  }
  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.EDIT:
       this.edit()
        break;
    }
  }
  navigate() {
    this.router.navigate(["radio-removal/radio-removal-config/item-scrapping-configuration"], { queryParams: { tab: 8 } });
  }
  edit() {
    if (!this.primengTableConfigPropertiesObj.tableComponentConfigs.tabsList[8].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["radio-removal/radio-removal-config/item-scrapping-configuration/item-type/add-edit"], { queryParams: { id: this.id } });
  }
}
