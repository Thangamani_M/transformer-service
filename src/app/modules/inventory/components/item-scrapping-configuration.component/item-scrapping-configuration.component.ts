import { DatePipe } from "@angular/common";
import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { Store } from "@ngrx/store";
import { combineLatest, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
    selector: 'app-item-scrapping-configuration',
    templateUrl: './item-scrapping-configuration.component.html',
})
export class ItemScrappingConfigurationComponent extends PrimeNgTableVariablesModel {

    primengTableConfigProperties: any;
    row: any = {};
    isLoading: boolean;
    first: any = 0;
    filterData: any;
    obj: any;
    observableResponse;
    showFilterForm = false;
    filterForm: FormGroup;
    itemTypeList: any = [];
    filteredStockCodes = [];
    constructor(private crudService: CrudService, private rxjsService: RxjsService,  private datePipe: DatePipe,
        private store: Store<AppState>,private router: Router, private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService) {
        super();
        this.primengTableConfigProperties = {
            tableCaption: 'Item Scrapping Configuration',
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Radio Removal -Configuration', relativeRouterUrl: '' }, { displayName: 'Radio Removals', relativeRouterUrl: '' }, { displayName: 'Item Type', relativeRouterUrl: '' }],
            tableComponentConfigs: {
                tabsList: [
                {
                    caption: 'Termination',
                    isShowTooltip: true,
                    enableBreadCrumb: true,
                    disabled:true,
                },
                {
                    caption: 'Decoders',
                    isShowTooltip: true,
                    enableBreadCrumb: true,
                    disabled:true,
                },
                {
                    caption: 'APT Cancel Reason',
                    isShowTooltip: true,
                    enableBreadCrumb: true,
                    disabled:true,
                },
                {
                    caption: 'Non Recovery Reason Config',
                    isShowTooltip: true,
                    enableBreadCrumb: true,
                    disabled:true,
                },
                {
                    caption: 'Rented Kit Config',
                    isShowTooltip: true,
                    enableBreadCrumb: true,
                    disabled:true,
                },
                {
                    caption: 'Reschedule Reason Config',
                    isShowTooltip: true,
                    enableBreadCrumb: true,
                    disabled:true,
                },
                {
                    caption: 'Unrecoverable Reason Config',
                    isShowTooltip: true,
                    enableBreadCrumb: true,
                    disabled:true,
                },
                {
                    caption: 'Sub Contractor Invoicing',
                    isShowTooltip: true,
                    enableBreadCrumb: true,
                    disabled:true,
                },
                {
                    caption: 'Item Type',
                    dataKey: 'itemTypeConfigId',
                    enableBreadCrumb: true,
                    disabled:true,
                    enableAction: true,
                    enableGlobalSearch: false,
                    reorderableColumns: false,
                    resizableColumns: false,
                    enableScrollable: true,
                    checkBox: false,
                    enableRowDelete: false,
                    enableFieldsSearch: false,
                    enableHyperLink: true,
                    cursorLinkIndex: 0,
                    columns: [
                        { field: 'itemTypeName', header: 'Item Type', width: '200px' },
                        { field: 'status', header: 'Status', width: '200px' },
                        { field: 'createdBy', header: 'Created By', width: '200px' },
                        { field: 'createdDate', header: 'Created Date', width: '200px' },
                        { field: 'reasons', header: 'Reasons', width: '200px' },

                    ],
                    enableAddActionBtn: true,
                    shouldShowDeleteActionBtn: false,
                    shouldShowCreateActionBtn: true,
                    areCheckboxesRequired: false,
                    isDateWithTimeRequired: true,
                    apiSuffixModel: InventoryModuleApiSuffixModels.ITEM_TYPE_CONFIG,
                    moduleName: ModulesBasedApiSuffix.INVENTORY,
                },
                {
                    caption: 'Radio System Setup',
                    dataKey: 'itemScrappingId',
                    enableBreadCrumb: true,
                    disabled:true,
                    enableAction: true,
                    enableGlobalSearch: false,
                    reorderableColumns: false,
                    resizableColumns: false,
                    enableScrollable: true,
                    checkBox: false,
                    enableRowDelete: false,
                    enableFieldsSearch: false,
                    enableHyperLink: true,
                    cursorLinkIndex: 0,
                    columns: [
                        { field: 'itemTypeName', header: 'Item Type', width: '200px' },
                        { field: 'status', header: 'Status', width: '200px' },
                        { field: 'itemCode', header: 'Stock Code', width: '200px' },
                        { field: 'itemName', header: 'Stock Description', width: '200px' },
                        { field: 'createdBy', header: 'Created By', width: '200px' },
                        { field: 'createdDate', header: 'Created Date', width: '200px' },
                        { field: 'reason', header: 'Reasons', width: '200px' },

                    ],
                    enableAddActionBtn: true,
                    shouldShowFilterActionBtn: true,
                    shouldShowDeleteActionBtn: false,
                    shouldShowCreateActionBtn: true,
                    areCheckboxesRequired: false,
                    isDateWithTimeRequired: true,
                    apiSuffixModel: InventoryModuleApiSuffixModels.ITEM_SCRAPPING,
                    moduleName: ModulesBasedApiSuffix.INVENTORY,
                },

                ],
            },
        };

        this.combineLatestNgrxStoreData();

    }
    ngOnInit(): void {
      this.combineLatestNgrxStoreDataOne()
        this.getTab();
        this.getRequiredListData();
        this.createFilterForm();
        this.getItemDropDown();
    }
    combineLatestNgrxStoreDataOne() {
      combineLatest([
        this.store.select(currentComponentPageBasedPermissionsSelector$)]
      ).subscribe((response) => {
        let permission = response[0]["Radio Removal Config"]
        if (permission) {
          let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
          this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
        }
      });
    }

    getTab(){
        this.activatedRoute.queryParamMap.subscribe((params) => {
            this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
            this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
            this.primengTableConfigProperties.breadCrumbItems = [{ displayName: 'Radio Removal', relativeRouterUrl: '' },{ displayName: 'Radio Removal Config', relativeRouterUrl: '' },
            { displayName: this.selectedTabIndex == 8 ? 'Configuration Item Scrapping Configuration List' : this.selectedTabIndex == 9 ? 'Radio System Setup': 'Radio System Setup', relativeRouterUrl: '' }]
            if(this.selectedTabIndex == 8){
                this.primengTableConfigProperties.tableCaption = "Item Scrapping Configuration"
            }
            if(this.selectedTabIndex == 9){
                this.primengTableConfigProperties.tableCaption = "Radio System Setup"
            }


          });
    }
    createFilterForm() {
        this.filterForm = new FormGroup({
            'itemId': new FormControl(null),
            'itemCode': new FormControl(null),
            'itemTypeConfigId': new FormControl(null),
        });
      }
    combineLatestNgrxStoreData() {
        combineLatest(
            this.store.select(loggedInUserData)
        ).subscribe((response) => {
            this.loggedInUserData = new LoggedInUserModel(response[0]);
        });
    }
    getItemDropDown() {
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_TYPE_CONFIG, undefined, null, prepareRequiredHttpParams({ isAll: true }))
            .subscribe((response: IApplicationResponse) => {
                if (response.statusCode == 200 && response.isSuccess == true) {
                    this.itemTypeList = response.resources;
                }
            });

    }
    getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
        InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).pipe(map((res: IApplicationResponse) => {
            if (res?.resources) {
              res?.resources?.forEach(val => {
                val.createdDate = val.createdDate ? this.datePipe.transform(val.createdDate, 'dd-MM-yyyy h:mm:ss a') : '';
                return val;
              })
            }
            return res;
          })).subscribe((response: IApplicationResponse) => {
            this.loading = false;
            this.rxjsService.setGlobalLoaderProperty(false);
            if (response.isSuccess && response.statusCode === 200) {
                this.dataList = response.resources;
                this.totalRecords = response.totalCount;
            } else {
                this.dataList = null;
                this.totalRecords = 0;
            }
        })
    }

    onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
        switch (type) {
            case CrudType.CREATE:
              if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
                return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
            }
                this.openAddEditPage(CrudType.CREATE, row);
                break;
            case CrudType.GET:
                this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
                unknownVar = { ...this.filterData, ...unknownVar };
                this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
                break;
            case CrudType.VIEW:
                this.openAddEditPage(CrudType.VIEW, row);
                break;
            case CrudType.FILTER:
                 this.displayAndLoadFilterData();
                break;

            default:
        }
    }
    openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
        switch (type) {
            case CrudType.CREATE:
                switch (this.selectedTabIndex) {
                    case 8:
                        this.router.navigate(["radio-removal/radio-removal-config/item-scrapping-configuration/item-type/add-edit"]);
                        break;
                    case 9:
                         this.router.navigate(["radio-removal/radio-removal-config/item-scrapping-configuration/radio-system-setup/add-edit"]);
                        break;
                }
                break;
            case CrudType.VIEW:
                switch (this.selectedTabIndex) {
                    case 8:
                        this.router.navigate(["radio-removal/radio-removal-config/item-scrapping-configuration/item-type/view"], { queryParams: { id: editableObject['itemTypeConfigId'] } });
                        break;
                    case 9:
                        this.router.navigate(["radio-removal/radio-removal-config/item-scrapping-configuration/radio-system-setup/view"], { queryParams: { id: editableObject['itemScrappingId']  } });
                        break;
                }
                break;
        }
    }
    onActionSubmited(e: any) {
        if (e.data && !e.search && !e.col) {
            this.onCRUDRequested(e.type, e.data)
        } else if (e.data && e.search) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.data && e.col) {
            this.onCRUDRequested(e.type, e.data, e.col);
        } else if (e.type && !e.data) {
            this.onCRUDRequested(e.type, {})
        }
    }
    displayAndLoadFilterData() {
        this.showFilterForm = !this.showFilterForm;
      }
    submitFilter() {
        this.filterData = Object.assign({},
          this.filterForm.get('itemId').value == '' ? null : { itemId: this.filterForm.get('itemId').value },
          this.filterForm.get('itemTypeConfigId').value ? { itemTypeConfigId: this.filterForm.get('itemTypeConfigId').value } : '',
        );
        Object.keys(this.filterData).forEach(key => {
            if (this.filterData[key] == "" || this.filterData[key] == null) {
              delete this.filterData[key]
            }
          });
        this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
        this.observableResponse = this.onCRUDRequested('get', this.row);
        this.showFilterForm = !this.showFilterForm;
      }
    resetForm() {
        this.filterForm.reset();
        this.filterData = null;
        this.showFilterForm = !this.showFilterForm;
        if (!this.showFilterForm) {
          this.getRequiredListData();
        }
      }
      onChangeStockCode(event, type, index?: any) {
        let params = { searchText: event.target.value };
       //this.searchTerm = event.target.value;
        let api = this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, false, prepareRequiredHttpParams(params));
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200 && response.resources) {
                this.filteredStockCodes = response.resources;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    onStatucCodeSelected(value, type, index?: any) {
        if (value) {
            let filter = this.filteredStockCodes.filter(x => x.displayName == value);
            if (filter && filter.length > 0) {
                this.getStockDetails(filter[0].id).subscribe((res) => {
                    if (index) { }
                    else {
                        this.filterForm.get("itemId").setValue(res.resources.itemId);
                    }
                });

            }
        }

    }
    getStockDetails(id): Observable<any> {
        return this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.ITEM,
            id,
            false,
            prepareRequiredHttpParams({})
        ).pipe(
            map(result => {
                return result;
            })
        );
    }
    tabClick(e) {
        if (e?.index == 0) {
          this.router.navigate(['./inventory/radio-removal']);
        }
        if (e?.index == 1) {
          this.router.navigate(['./radio-removal/radio-removal-config/decoders-radio-transmeter']);
        }
        if (e?.index == 2) {
          this.router.navigate(['./radio-removal/radio-removal-config/cancel-reason-config']);
        }
        if (e?.index == 3) {
          this.router.navigate(['./radio-removal/radio-removal-config/non-recovery-reason-config']);
        }
        if (e?.index == 4) {
          this.router.navigate(['./radio-removal/radio-removal-config/rented-kit-config']);
        }
        if (e?.index == 5) {
          this.router.navigate(['./radio-removal/radio-removal-config/reschedule-reason-config']);
        }
        if (e?.index == 6) {
          this.router.navigate(['./radio-removal/radio-removal-config/unrecoverable-reason-config']);
        }
        if (e?.index == 7) {
          this.router.navigate(['./radio-removal/radio-removal-config/sub-contractor-invoicing']);
        }
        if (e?.index == 8) {
            this.selectedTabIndex = e?.index;
            this.dataList = [];
            let queryParams = {};
            queryParams['tab'] = this.selectedTabIndex;
            this.onAfterTabChange(queryParams);
            this.getRequiredListData();
        }
        if (e?.index == 9) {
            this.selectedTabIndex = e?.index;
            this.dataList = [];
            let queryParams = {};
            queryParams['tab'] = this.selectedTabIndex;
            this.onAfterTabChange(queryParams);
            this.getRequiredListData();
        }
      }
    onAfterTabChange(queryParams) {
        if (this.selectedTabIndex != 0) {
            this.router.navigate([`../`], { relativeTo: this.activatedRoute, queryParams: queryParams });
        }
    }
}
