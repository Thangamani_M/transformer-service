import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RadioRemovalsWorkListRoutingModule } from './radio-removals-work-list-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RadioRemovalsWorkListRoutingModule
  ]
})
export class RadioRemovalsWorkListModule { }
