import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from '@app/reducers';
import { CrudService, debounceTimeForSearchkeyword, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CustomEventTitleFormatter } from '@modules/customer/components/customer/technician-installation/technician-allocation-calender-view/custom-event-title-formatter.provider';
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from '@modules/others/';
import { UserLogin } from '@modules/others/models';
import { OutOfOfficeModuleApiSuffixModels } from '@modules/out-of-office/enums/out-of-office-employee-request.enum';
import { select, Store } from '@ngrx/store';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarEventTitleFormatter, CalendarView } from 'angular-calendar';
import { isSameDay, isSameMonth } from 'date-fns';
import { NgxCaptureService } from 'ngx-capture';
import { of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-radio-removals-calendar-print',
  templateUrl: './radio-removals-calendar-print.component.html',
  styleUrls: ['./radio-removals-calendar-print.component.scss'],
  providers: [NgxCaptureService,
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter,
    },]
})
export class RadioRemovalsCalendarPrintComponent implements OnInit {

  refresh = new Subject<void>();
  dealerDetail: any;
  viewable: boolean = true;
  primengTableConfigProperties: any;
  limit: number = 10;
  totalLength: number = 0;
  pageIndex: any = 0;
  pageLimit: number[] = [10, 50, 75, 100];
  clientDetails: any;
  dataDecodersList: any = [];
  dataContactList: any = [];
  dataEquipmentList: any = [];
  totalRecords: any = 0;
  totalContactRecords: any = 0;
  totalEquipmentRecords: any = 0;
  row: any = {};
  selectedRows: any[] = [];
  loading: Boolean = false;
  selectedTabIndex: any = 0;
  addressId: any;
  customerId: any;
  isShowNoContactRecords: any = true;
  isShowNoDecodersRecords: any = true;
  isShowNoEquipmentRecords: any = true;
  radioRemovalWorkListId: any;
  status: boolean = false;
  servicecancellationId: any;

  // scheduleForm: FormGroup
  searchTechForm: FormGroup
  activeDayIsOpen: boolean = false
  viewDate: any = new Date()
  loadingEvents: boolean = false
  loggedUser: any
  selectedUser: any
  subContractorId: any
  reasonDropdown: any = []
  equipmentTobeRemoved: any
  radioRemovalAppointmentId: any
  filterForm: FormGroup
  showFilterForm: boolean = false
  @ViewChild('screen', { static: true }) screen: any;
  capturedImagBase64: any
  capturedImg: any
  imageBlobUrl:any
  appointmentStartDate;
  constructor( private captureService: NgxCaptureService, private httpCancelService: HttpCancelService, private activatedRoute: ActivatedRoute, 
    private store: Store<AppState>, private momentService: MomentService, private _fb: FormBuilder, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.customerId = (Object.keys(params['params']).length > 0) ? params['params']['customerId'] : '';
      this.addressId = (Object.keys(params['params']).length > 0) ? params['params']['addressId'] : '';
      this.equipmentTobeRemoved = (Object.keys(params['params']).length > 0) ? params['params']['equipmentTobeRemoved'] : '';
      this.radioRemovalWorkListId = (Object.keys(params['params']).length > 0) ? params['params']['radioRemovalWorkListId'] : '';
      this.radioRemovalAppointmentId = (Object.keys(params['params']).length > 0) ? params['params']['radioRemovalAppointmentId'] : '';
      this.appointmentStartDate = (Object.keys(params['params']).length > 0) ? params['params']['appointmentStartDate'] : '';
    });
  }
  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.createFilterForm()
    this.createSearchForm()
    this.searchKeywordRequest()
    this.reLoadData()
  }

  createFilterForm(): void {
    this.filterForm = this._fb.group({
      subContractorId: [this.loggedUser.userId],
      appointmentStartDate: [new Date()],
      appointmentEndDate: [new Date()],
    });

  }



  createSearchForm() {
    this.searchTechForm = this._fb.group({
      search: [''],
    });
  }

  searchKeywordRequest() {
    this.searchTechForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          this.filterForm.get('search').setValue(val.search)

          return this.reLoadData()
        })
      )
      .subscribe();
  }

  // appinement booking calender view starts
  view: CalendarView = CalendarView.Week;

  CalendarView = CalendarView;
  yesterday = this.momentService.getYesterday()
  minDate: Date = new Date(this.yesterday);


  setView(view) {
    this.view = view;
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  onHoursSegmentClick(daytime) {
    return

  }


  handleEvent(action: string, event: any): void {
    return

  }


  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-pencil"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      },
    },
    // {
    //   label: '<i class="fa fa-trash"></i>',
    //   a11yLabel: 'Delete',
    //   onClick: ({ event }: { event: CalendarEvent }): void => {
    //     this.events = this.events.filter((iEvent) => iEvent !== event);
    //     this.handleEvent('Deleted', event);
    //   },
    // },
  ];

  colors: any = {
    red: {
      primary: '#ad2121',
      secondary: '#FAE3E3',
    },
    blue: {
      primary: '#1e90ff',
      secondary: '#D1E8FF',
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA',
    },
  };

  // users = users;
  users = [];




  // events: CalendarEvent[] = []
  events: CalendarEvent[] = []


  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {

    // if (!newStart || !newEnd) {
    //   return
    // }
    // if (newStart < this.minDate) {
    //   return
    // }
    // let isSameDay = this.momentService.isSameDayTime(event.start, newStart)
    // if (isSameDay) {
    //   return
    // }

    event.start = newStart;
    event.end = newEnd;
    this.events = [...this.events];
    // this.onAppointment(event)
  }

  userChanged({ event, newUser }) {
    // event.color = newUser.color;
    event.meta.user = newUser;
    this.events = [...this.events];

  }

  userSelected(user) {
    if (user.isSelectable)
      this.selectedUser = user
  }

  onDayChange() {
    this.reLoadData()

    // this.scheduleForm.get('appointmentDate').setValue(this.viewDate)
    // this.scheduleForm.get('appointmentStartDate').setValue(this.viewDate)
  }

  reLoadData() {

    let scheduledData = this.filterForm.value
    Object.keys(scheduledData).forEach(key => {
      if (scheduledData[key] === "") {
        delete scheduledData[key]
      }
    });
    scheduledData.appointmentStartDate = this.momentService.toFormateType(scheduledData.appointmentStartDate, 'YYYY-MM-DD')
    scheduledData.appointmentEndDate = this.momentService.toFormateType(scheduledData.appointmentEndDate, 'YYYY-MM-DD')
    let filterdNewData1 = Object.entries(scheduledData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

    return of(this.getTechniciansList(filterdNewData1));

  }


  getTechniciansList(otherParams?: object) {
    if (Object.keys(otherParams).length > 0) {
      // logic for split columns and its values to key value pair

      Object.keys(otherParams).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          otherParams[key] = this.momentService.localToUTCDateTime(otherParams[key])
          // otherParams[key] =  this.convertData(otherParams[key])


          // otherParams[key] = this.momentService.localToUTC(otherParams[key]);
        } else {
          otherParams[key] = otherParams[key];
        }
      });
    }
    // this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.loadingEvents = true
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, OutOfOfficeModuleApiSuffixModels.RADIOREMOVAL_APPOINTMENT_CALENDAR, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);

        this.loadingEvents = false
        if (response.resources) {
          this.selectedUser = null
          this.events = []

          response.resources.rows.forEach(element => {
            if (element.appointments.length > 0) {
              element.appointments.forEach((element1, index) => {
                let fromHour = element1.appointmentTimeFrom ? element1.appointmentTimeFrom.split(':') : null
                let toHour = element1.appointmentTimeTo ? element1.appointmentTimeTo.split(':') : null
                let existAppoitmnt = {
                  id: element1.radioRemovalAppointmentId,
                  radioRemovalAppointmentId: element1.radioRemovalAppointmentId,
                  radioRemovalAppointmentLogId: element1.radioRemovalAppointmentLogId,
                  appointmentTimeFrom: element1.appointmentTimeFrom,
                  appointmentTimeTo: element1.appointmentTimeTo,
                  name: null,
                  // hoverText: element1?.hoverText ? element1?.hoverText : element1.serviceCallNumber,
                  // title: element1.statusIconPath ? title : element1.serviceCallNumber,
                  title: ' <span title="' + (element1?.hoverText ? element1?.hoverText : element1.displayText) + '">' + element1.displayText + ' </span> ',
                  start: element1.appointmentTimeFrom ? new Date(this.momentService.addHoursMinits(element1.appointmentDate, fromHour[0], fromHour[1])) : null,
                  end: element1.appointmentTimeTo ? new Date(this.momentService.addHoursMinits(element1.appointmentDate, toHour[0], toHour[1])) : null,
                  color: element1.cssClass == 'status-label-red' ? this.colors.red : this.colors.blue,
                  draggable: false,
                }
                this.events.push(existAppoitmnt)

              })
            }
          });



          setTimeout(() => {
            if ($(".cal-time-events-wrapper")?.length == 0) {
              $('.cal-time-events').wrapAll('<div class="cal-time-events-wrapper"></div>');
            }
            $('.cal-time-events-wrapper').animate({
              scrollTop: 540
            }, 1000, function () {
            });
          }, 1000);

        }
      });
  }

  resetForm() {
    this.filterForm.reset()
    this.filterForm.get('subContractorId').setValue(this.loggedUser.userId)
  }




  onDaySelect() {

    // this.scheduleForm.get('appointmentDate').setValue(this.viewDate)
    this.reLoadData()

  }

  submitFilter(): void {

    if (this.filterForm.invalid) {
      return;
    }
    this.viewDate = new Date(this.filterForm.get('appointmentStartDate').value)
    this.rxjsService.setFormChangeDetectionProperty(true)
    this.showFilterForm = false
    this.reLoadData()

  }

  // dataURIToBlob(dataURI: string) { //Convert Base64 to Image

  //   const splitDataURI = dataURI.split(',')
  //   const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1])
  //   const mimeString = splitDataURI[0].split(':')[1].split(';')[0]

  //   const ia = new Uint8Array(byteString.length)
  //   for (let i = 0; i < byteString.length; i++)
  //     ia[i] = byteString.charCodeAt(i)

  //   return new Blob([ia], { type: mimeString })
  // }

  // createImageFromBlob(image: Blob) {
  //   let reader = new FileReader();
  //   reader.addEventListener("load", () => {
  //     this.imageBlobUrl = reader.result;
  //   }, false);

  //   if (image) {
  //     reader.readAsDataURL(image);
  //   }
  // }

  saveImage(img: string) {

  }
 

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
        int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
}

  onSentMail() {
    this.showFilterForm = false
    this.rxjsService.setGlobalLoaderProperty(true);
    this.captureService.getImage(this.screen.nativeElement, true).subscribe(b64Data => {
      this.capturedImagBase64 = b64Data
      // let capturedImg = this.dataURIToBlob(b64Data)
      const imageBlob = this.dataURItoBlob(b64Data.replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
      const imageFile = new File([imageBlob], '.png', { type: 'image/jpeg' });
    
      let formData = new FormData();
      formData.append('file', imageFile);
      let jsonData = {
        // radioRemovalWorkListId: this.radioRemovalWorkListId,
        SubContractorId:this.loggedUser.userId
      }
      formData.append("subContractorEmailSenderDTO", JSON.stringify(jsonData));
      this.httpCancelService.cancelPendingRequestsOnFormSubmission();
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_EMAIL, formData)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
          }
        })

    })

  }


}
