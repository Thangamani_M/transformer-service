class RadioRemovalCompleteModel {
    constructor(radioRemovalCompleteModel?: RadioRemovalCompleteModel) {
        this.radioRemovalAppointmentId =  radioRemovalCompleteModel ?  radioRemovalCompleteModel.radioRemovalAppointmentId == undefined ? '' :  radioRemovalCompleteModel.radioRemovalAppointmentId : '';
        this.radioRemovalWorkListId =  radioRemovalCompleteModel ?  radioRemovalCompleteModel.radioRemovalWorkListId == undefined ? "" :  radioRemovalCompleteModel.radioRemovalWorkListId : "";
        // this.removalCallCompleteDetailsPostDTO =  radioRemovalCompleteModel ?  radioRemovalCompleteModel.removalCallCompleteDetailsPostDTO == undefined ? null :  radioRemovalCompleteModel.removalCallCompleteDetailsPostDTO : null;
        this.radioRemovalAppointmentSummaryId = radioRemovalCompleteModel ? radioRemovalCompleteModel.radioRemovalAppointmentSummaryId == undefined ? '' : radioRemovalCompleteModel.radioRemovalAppointmentSummaryId : '';
        // this.radioRemovalAppointmentId = removalCallCompleteDetailsPostDTOModel ? removalCallCompleteDetailsPostDTOModel.radioRemovalAppointmentId == undefined ? '' : removalCallCompleteDetailsPostDTOModel.radioRemovalAppointmentId : '';
        this.personOnSite = radioRemovalCompleteModel ? radioRemovalCompleteModel.personOnSite == undefined ? '' : radioRemovalCompleteModel.personOnSite : '';
        this.feedback = radioRemovalCompleteModel ? radioRemovalCompleteModel.feedback == undefined ? null : radioRemovalCompleteModel.feedback : null;
   
        this.createdUserId =  radioRemovalCompleteModel ?  radioRemovalCompleteModel.createdUserId == undefined ? null :  radioRemovalCompleteModel.createdUserId : null;
        this.modifiedUserId =  radioRemovalCompleteModel ?  radioRemovalCompleteModel.modifiedUserId == undefined ? "" :  radioRemovalCompleteModel.modifiedUserId : "";
        this.removalDeCoderDetailsPostDTO =  radioRemovalCompleteModel ?  radioRemovalCompleteModel.removalDeCoderDetailsPostDTO == undefined ? [] :  radioRemovalCompleteModel.removalDeCoderDetailsPostDTO : [];
        this.radioRemovalUnSerializedItemsDetailsPostDTO =  radioRemovalCompleteModel ?  radioRemovalCompleteModel.radioRemovalUnSerializedItemsDetailsPostDTO == undefined ? [] :  radioRemovalCompleteModel.radioRemovalUnSerializedItemsDetailsPostDTO : [];
        this.radioRemovalISerializedtemsPostDTO =  radioRemovalCompleteModel ?  radioRemovalCompleteModel.radioRemovalISerializedtemsPostDTO == undefined ? [] :  radioRemovalCompleteModel.radioRemovalISerializedtemsPostDTO : [];

    }
    radioRemovalAppointmentId?: string;
    // removalCallCompleteDetailsPostDTO: RemovalCallCompleteDetailsPostDTOModel;
    radioRemovalAppointmentSummaryId:string;
    // radioRemovalAppointmentId:string;
    personOnSite:string;
    feedback:string;

    removalDeCoderDetailsPostDTO: RemovalDeCoderDetailsPostDTOModel[]; 
    radioRemovalUnSerializedItemsDetailsPostDTO: RadioRemovalUnSerializedItemsDetailsPostDTOModel[]; 
    radioRemovalISerializedtemsPostDTO: RadioRemovalISerializedtemsPostDTOModel[]; 
    radioRemovalWorkListId:string;
    modifiedUserId: string;
    createdUserId: string;
}


class RemovalDeCoderDetailsPostDTOModel {
    constructor(removalDeCoderDetailsPostDTOModel?: RemovalDeCoderDetailsPostDTOModel) {
        this.radioRemovalAppointmentSummaryDecoderId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.radioRemovalAppointmentSummaryDecoderId == undefined ? '' : removalDeCoderDetailsPostDTOModel.radioRemovalAppointmentSummaryDecoderId : '';
        this.radioRemovalAppointmentSummaryId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.radioRemovalAppointmentSummaryId == undefined ? '' : removalDeCoderDetailsPostDTOModel.radioRemovalAppointmentSummaryId : '';
        this.decoderId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.decoderId == undefined ? '' : removalDeCoderDetailsPostDTOModel.decoderId : '';
        this.name = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.name == undefined ? '' : removalDeCoderDetailsPostDTOModel.name : '';
        this.transmitterNumber = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.transmitterNumber == undefined ? '' : removalDeCoderDetailsPostDTOModel.transmitterNumber : '';
        this.radioRemovalNonRecoveryReasonCofigId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.radioRemovalNonRecoveryReasonCofigId == undefined ? null : removalDeCoderDetailsPostDTOModel.radioRemovalNonRecoveryReasonCofigId : null;
        this.isRecovered = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.isRecovered == undefined ? false : removalDeCoderDetailsPostDTOModel.isRecovered : false;
    }
    radioRemovalAppointmentSummaryDecoderId:string;
    radioRemovalAppointmentSummaryId:string;
    decoderId:string;
    name:string
    transmitterNumber:string
    radioRemovalNonRecoveryReasonCofigId:null;
    isRecovered:boolean;

}

class RadioRemovalUnSerializedItemsDetailsPostDTOModel {
    constructor(removalDeCoderDetailsPostDTOModel?: RadioRemovalUnSerializedItemsDetailsPostDTOModel) {
        this.radioRemovalAppointmentSummaryItemId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.radioRemovalAppointmentSummaryItemId == undefined ? '' : removalDeCoderDetailsPostDTOModel.radioRemovalAppointmentSummaryItemId : '';
        this.radioRemovalAppointmentSummaryId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.radioRemovalAppointmentSummaryId == undefined ? '' : removalDeCoderDetailsPostDTOModel.radioRemovalAppointmentSummaryId : '';
        this.billOfMaterialItemDetailId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.billOfMaterialItemDetailId == undefined ? '' : removalDeCoderDetailsPostDTOModel.billOfMaterialItemDetailId : '';
        this.radioRemovalNonRecoveryReasonCofigId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.radioRemovalNonRecoveryReasonCofigId == undefined ? null : removalDeCoderDetailsPostDTOModel.radioRemovalNonRecoveryReasonCofigId : null;
        this.itemId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.itemId == undefined ? null : removalDeCoderDetailsPostDTOModel.itemId : null;
        this.itemName = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.itemName == undefined ? null : removalDeCoderDetailsPostDTOModel.itemName : null;
        this.serialNumber = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.serialNumber == undefined ? null : removalDeCoderDetailsPostDTOModel.serialNumber : null;
        this.isRecovered = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.isRecovered == undefined ? false : removalDeCoderDetailsPostDTOModel.isRecovered : false;
    }
    radioRemovalAppointmentSummaryItemId:string;
    radioRemovalAppointmentSummaryId:string;
    billOfMaterialItemDetailId:string;
    radioRemovalNonRecoveryReasonCofigId:null;
    itemId:string;
    itemName:string
    serialNumber:string
    isRecovered:boolean;

}


class RadioRemovalISerializedtemsPostDTOModel {
    constructor(removalDeCoderDetailsPostDTOModel?: RadioRemovalISerializedtemsPostDTOModel) {
        this.radioRemovalAppointmentSummaryItemId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.radioRemovalAppointmentSummaryItemId == undefined ? '' : removalDeCoderDetailsPostDTOModel.radioRemovalAppointmentSummaryItemId : '';
        this.radioRemovalAppointmentSummaryId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.radioRemovalAppointmentSummaryId == undefined ? '' : removalDeCoderDetailsPostDTOModel.radioRemovalAppointmentSummaryId : '';
        this.billOfMaterialItemDetailId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.billOfMaterialItemDetailId == undefined ? '' : removalDeCoderDetailsPostDTOModel.billOfMaterialItemDetailId : '';
        this.radioRemovalNonRecoveryReasonCofigId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.radioRemovalNonRecoveryReasonCofigId == undefined ? null : removalDeCoderDetailsPostDTOModel.radioRemovalNonRecoveryReasonCofigId : null;
        this.itemId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.itemId == undefined ? null : removalDeCoderDetailsPostDTOModel.itemId : null;
        this.itemName = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.itemName == undefined ? null : removalDeCoderDetailsPostDTOModel.itemName : null;
        this.radioRemovalAppointmentSummaryItemDetailId = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.radioRemovalAppointmentSummaryItemDetailId == undefined ? null : removalDeCoderDetailsPostDTOModel.radioRemovalAppointmentSummaryItemDetailId : null;
        this.isRecovered = removalDeCoderDetailsPostDTOModel ? removalDeCoderDetailsPostDTOModel.isRecovered == undefined ? false : removalDeCoderDetailsPostDTOModel.isRecovered : false;
    }
    radioRemovalAppointmentSummaryItemId:string;
    radioRemovalAppointmentSummaryId:string;
    billOfMaterialItemDetailId:string;
    radioRemovalNonRecoveryReasonCofigId:null;
    radioRemovalAppointmentSummaryItemDetailId:string;
    itemId:string;
    itemName:string;
    isRecovered:boolean;

}


export { RadioRemovalCompleteModel, RemovalDeCoderDetailsPostDTOModel, RadioRemovalUnSerializedItemsDetailsPostDTOModel, RadioRemovalISerializedtemsPostDTOModel };

