
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, debounceTimeForDeepNestedObjectSearchkeyword, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { TableFilterFormService } from "@app/shared/services/create-form.services";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from '@modules/others/';
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { Observable, of } from "rxjs";
import { debounceTime, distinctUntilChanged, switchMap } from "rxjs/operators";
import {MenuItem} from 'primeng/api';
@Component({
  selector: 'app-radio-removals-calendar-history',
  templateUrl: './radio-removals-calendar-history.component.html',
  
})
export class RadioRemovalsCalendarHistoryComponent implements OnInit {
  dealerDetail: any;
  viewable: boolean = true;
  primengTableConfigProperties: any;
  limit: number = 10;
  totalLength: number = 0;
  pageIndex: any = 0;
  pageLimit: number[] = [10, 50, 75, 100];
  clientDetails: any;
  dataDecodersList: any = [];
  dataContactList: any = [];
  dataEquipmentList: any = [];
  totalRecords: any = 0;
  totalContactRecords: any = 0;
  totalEquipmentRecords: any = 0;
  row: any = {};
  selectedRows: any;
  loading: Boolean = false;
  selectedTabIndex: any = 0;
  addressId: any;
  customerId: any;
  isShowNoContactRecords: any = true;
  isShowNoDecodersRecords: any = true;
  isShowNoEquipmentRecords: any = true;
  radioRemovalWorkListId: any;
  status: boolean = false;
  servicecancellationId: any;
  subContractorId: any
  loggedUser: any
  cancelDialogForm: FormGroup
  isCancelAppointmentDialog: boolean = false
  reasionDropdown: any = []
  dataList: any = []
  pageSize: number = 10;
  columnFilterForm: FormGroup
  searchColumns: any
  id;
  items = [];
  constructor( private tableFilterFormService: TableFilterFormService, private httpCancelService: HttpCancelService, private snackbarService: SnackbarService, private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private _fb: FormBuilder, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
    this.primengTableConfigProperties = {
      tableCaption: "Radio Removal Worklist",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal WorkList', relativeRouterUrl: '' }, { displayName: 'Radio Removal WorkList', relativeRouterUrl: '' }, { displayName: 'Radio Removal' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Radio Removals',
            dataKey: 'radioRemovalAppointmentId',
            selectedTabIndex: 0,
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'appointmentDate', header: 'Date Time' },
              { field: 'spokeTo', header: 'Spoke to' },
              { field: 'equipment', header: 'Equipment' },
              { field: 'comment', header: 'Comment' },
              { field: 'feedback', header: 'FeedBack' },
              { field: 'status', header: 'Status' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true
          },

        ]
      }
    }

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.customerId = (Object.keys(params['params']).length > 0) ? params['params']['customerId'] : '';
      this.addressId = (Object.keys(params['params']).length > 0) ? params['params']['addressId'] : '';
      this.id = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      // this.subContractorId = (Object.keys(params['params']).length > 0) ? params['params']['subContractorId'] : '';
      this.radioRemovalWorkListId = (Object.keys(params['params']).length > 0) ? params['params']['radioRemovalWorkListId'] : '';
    });
  }
  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.getClientDetails();
    this.columnFilterRequest()
    this.getAppointmentList();
    this.getReationDropdown()
    this.createCancelForm();
    this.items = [
      {label: 'Reschedule', data: {}, command: (event) => {
        this.navigateToReschedule();
      }},
      {label: 'Cancel', data: {}, command: (event) => {
        this.openCancelDialog();
      }},
      {label: 'Complete', data: {}, command: (event) => {
        this.navigateToComplete();
      }},
          
      ]
  }

  createCancelForm() {
    this.cancelDialogForm = this._fb.group({
      radioRemovalAppointmentId: ['', Validators.required],
      cancelReasonId: ['', Validators.required],
      comments: ['', Validators.required],
      modifiedUserId: [this.loggedUser?.userId],
    })
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.getAppointmentList(this.row['pageIndex'], this.row['pageSize'], this.row));
        })
      )
      .subscribe();
  }

  getClientDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {
    let filterData = { customerId: this.customerId, addressId: this.addressId };
    otherParams = { ...filterData };
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIOREMOVAL_DETAILS,
      null, false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.clientDetails = response.resources;
          this.radioRemovalWorkListId = response.resources.radioRemovalWorkListId;
          this.onShowValue();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getReationDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIOREMOVAL_CANCEL_REASON)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.reasionDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }


  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    row["searchColumns"] = this.searchColumns;
    this.row = row;
    this.getAppointmentList(this.row['pageIndex'], this.row['pageSize'], this.row);
  }


  getAppointmentList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    let filterData = { customerId: this.customerId, addressId: this.addressId, subContractorId: this.loggedUser?.userId };
    otherParams = { ...filterData };
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIOREMOVAL_APPOINTMENT,
      null, false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.dataList = response.resources;
          // this.selectedRows = []
          // data.resources.forEach(element => {
          //   if (element.isActive) {
          //     this.selectedRows.push(element)
          //   }
          // });
          this.totalRecords = response.totalCount;
        } else {
          this.dataList = null
          this.totalRecords = 0;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue() {
    this.dealerDetail = [
      {
        name: '', columns: [
          { name: 'Client', value: this.clientDetails && this.clientDetails.customerName ? this.clientDetails.customerName : '-', order: 1 },
          { name: 'Site Type', value: this.clientDetails && this.clientDetails.siteType ? this.clientDetails.siteType : '-', order: 2 },
          { name: 'Bill Name', value: this.clientDetails && this.clientDetails.billName ? this.clientDetails.billName : '-', order: 3 },
          { name: 'Site Details', value: this.clientDetails && this.clientDetails.siteDetails ? this.clientDetails.siteDetails : '-', order: 4 },
          { name: 'Dealer Type', value: this.clientDetails && this.clientDetails.dealType ? this.clientDetails.dealType : '-', order: 5 },
          { name: 'Customer ID', value: this.clientDetails && this.clientDetails.customerId ? this.clientDetails.customerId : '-', order: 96 },
          { name: 'Division', value: this.clientDetails && this.clientDetails.division ? this.clientDetails.division : '-', order: 7 },
          { name: 'Origin', value: this.clientDetails && this.clientDetails.origin ? this.clientDetails.origin : '-', order: 8 },
          { name: 'Customer Code', value: this.clientDetails && this.clientDetails.customerCode ? this.clientDetails.customerCode : '-', order: 9 },
          { name: 'Address', value: this.clientDetails && this.clientDetails.address ? this.clientDetails.address : '-', order: 10 },
          { name: 'Main Area', value: this.clientDetails && this.clientDetails.mainArea ? this.clientDetails.mainArea : '-', order: 11 },
          { name: 'Email Address', value: this.clientDetails && this.clientDetails.emailAddress ? this.clientDetails.emailAddress : '-', order: 12 },
          { name: 'Termination Reason', value: this.clientDetails && this.clientDetails.terminationReason ? this.clientDetails.terminationReason : '-', order: 13 },
          { name: 'Termination Subtype', value: this.clientDetails && this.clientDetails.terminationSubType ? this.clientDetails.terminationSubType : '-', order: 14 },
          { name: 'Termination Date', value: this.clientDetails && this.clientDetails.terminationDate ? this.clientDetails.terminationDate : '-', order: 15 },
        ]
      }
    ];
  }

  navigateAppointment() {
    this.router.navigate(['radio-removal/radio-removals-worklist/schedule-calendar-view'], {
      queryParams: {
        // subContractorId: this.clientDetails.servicecancellationId,
        radioRemovalWorkListId: this.radioRemovalWorkListId,
        customerId: this.customerId,
        addressId: this.addressId,
        type: 'appointment-history',
        appointmentStartDate: this.clientDetails?.AppointmentStartDate ? this.clientDetails?.AppointmentStartDate : new Date(),
      }
    });

  }

  navigateAppointmentPrint() {
    this.router.navigate(['radio-removal/radio-removals-worklist/schedule-calendar-view/print'], {
      queryParams: {
        // radioRemovalAppointmentId: this.selectedRows[0].radioRemovalAppointmentId,
        radioRemovalWorkListId: this.radioRemovalWorkListId,
        customerId: this.customerId,
        addressId: this.addressId,
        appointmentStartDate: this.clientDetails?.AppointmentStartDate ? this.clientDetails?.AppointmentStartDate : new Date(),
      }
    });

  }


  navigateToReschedule() {
    // if (!this.selectedRows) { // #Karthik  -- Selected row Single Object
    if (!this.selectedRows) {
      this.snackbarService.openSnackbar("Please select atleast one item", ResponseMessageTypes.WARNING);
      return
    }
    this.router.navigate(['radio-removal/radio-removals-worklist/schedule-calendar-view'], {
      queryParams: {
        radioRemovalAppointmentId: this.selectedRows.radioRemovalAppointmentId,
        radioRemovalWorkListId: this.radioRemovalWorkListId,
        customerId: this.customerId,
        addressId: this.addressId,
        appointmentStartDate: this.selectedRows.appointmentDate ? this.selectedRows.appointmentDate : new Date(),
      }
    });
  }

  openCancelDialog() {
    if (!this.selectedRows) {
      this.snackbarService.openSnackbar("Please select atleast one item", ResponseMessageTypes.WARNING);
      return
    }
    this.isCancelAppointmentDialog = true
    this.cancelDialogForm.get('radioRemovalAppointmentId').setValue(this.selectedRows ? this.selectedRows.radioRemovalAppointmentId : '')
    this.cancelDialogForm.get('cancelReasonId').setValue(null)
    this.cancelDialogForm.get('comments').setValue(null)

  }

  navigateToComplete() {
    // if (!this.selectedRows) { // #Karthik  -- Selected row Single Object
    if (!this.selectedRows) {
      this.snackbarService.openSnackbar("Please select atleast one item", ResponseMessageTypes.WARNING);
      return
    }
    this.router.navigate(['radio-removal/radio-removals-worklist/schedule-calendar-view/complete'], {
      queryParams: {
        radioRemovalAppointmentId: this.selectedRows.radioRemovalAppointmentId,
        radioRemovalWorkListId: this.radioRemovalWorkListId,
        customerId: this.customerId,
        addressId: this.addressId,
        appointmentStartDate: this.selectedRows.appointmentDate ? this.selectedRows.appointmentDate : new Date(),
      }
    });
  }

  navigateToCount() {

    this.router.navigate(['radio-removal/radio-removals-worklist/schedule-calendar-view/count'], {
      queryParams: {
        customerId: this.customerId,
        addressId: this.addressId,
        radioRemovalWorkListId: this.radioRemovalWorkListId,
        appointmentStartDate: this.clientDetails?.AppointmentStartDate ? this.clientDetails?.AppointmentStartDate : new Date()
      }
    });
  }

  onSubmitCancelDialog() {

    if (this.cancelDialogForm.invalid) {
      return;
    }
    let formValue = this.cancelDialogForm.value;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIOREMOVAL_CANCEL, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        this.isCancelAppointmentDialog = false
        this.selectedRows = []
        this.getAppointmentList();
      }
    });
  }

  onChangeStatus(row, i) {
  }

  onCRUDRequested(type?: any, row?: any) {
  }
}
