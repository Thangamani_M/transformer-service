import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgxCaptureModule } from 'ngx-capture';
import { NgxPrintModule } from 'ngx-print';
import { RadioRemovalsCalendarCompleteComponent } from './radio-removals-calendar-complete.component';
import { RadioRemovalsCalendarCountComponent } from './radio-removals-calendar-count.component';
import { RadioRemovalsCalendarHistoryComponent } from './radio-removals-calendar-history.component';
import { RadioRemovalsCalendarPrintComponent } from './radio-removals-calendar-print.component';
import { RadioRemovalsCalendarScheduleComponent } from './radio-removals-calendar-schedule.component';
import { RadioRemovalsCalendarViewRoutingModule } from './radio-removals-calendar-view-routing.module';

import {TieredMenuModule} from 'primeng/tieredmenu';
@NgModule({
  declarations: [RadioRemovalsCalendarScheduleComponent, RadioRemovalsCalendarHistoryComponent, RadioRemovalsCalendarPrintComponent, RadioRemovalsCalendarCompleteComponent, RadioRemovalsCalendarCountComponent],
  imports: [
    CommonModule,
    RadioRemovalsCalendarViewRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    NgxPrintModule,
    NgxCaptureModule,TieredMenuModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
  ]
})
export class RadioRemovalsCalendarViewModule { }
