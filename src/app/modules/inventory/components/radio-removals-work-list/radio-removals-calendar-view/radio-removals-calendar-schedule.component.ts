import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from "@angular/router";
import { AppState } from '@app/reducers';
import { clearFormControlValidators, CrudService, debounceTimeForDeepNestedObjectSearchkeyword, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { CustomEventTitleFormatter } from '@modules/customer/components/customer/technician-installation/technician-allocation-calender-view/custom-event-title-formatter.provider';
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from '@modules/others/';
import { UserLogin } from '@modules/others/models';
import { OutOfOfficeModuleApiSuffixModels } from '@modules/out-of-office/enums/out-of-office-employee-request.enum';
import { select, Store } from '@ngrx/store';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarEventTitleFormatter, CalendarView } from 'angular-calendar';
import { isSameDay, isSameMonth } from 'date-fns';
import { Observable, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-radio-removals-calendar-schedule',
  templateUrl: './radio-removals-calendar-schedule.component.html',
  providers: [
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter,
    },]
})
export class RadioRemovalsCalendarScheduleComponent implements OnInit {
  refresh = new Subject<void>();
  dealerDetail: any;
  viewable: boolean = true;
  primengTableConfigProperties: any;
  limit: number = 10;
  totalLength: number = 0;
  pageIndex: any = 0;
  pageLimit: number[] = [10, 50, 75, 100];
  clientDetails: any;
  dataDecodersList: any = [];
  dataContactList: any = [];
  dataEquipmentList: any = [];
  totalRecords: any = 0;
  totalContactRecords: any = 0;
  totalEquipmentRecords: any = 0;
  row: any = {};
  selectedRows: any[] = [];
  loading: Boolean = false;
  selectedTabIndex: any = 0;
  addressId: any;
  customerId: any;
  isShowNoContactRecords: any = true;
  isShowNoDecodersRecords: any = true;
  isShowNoEquipmentRecords: any = true;
  radioRemovalWorkListId: any;
  status: boolean = false;
  servicecancellationId: any;

  scheduleForm: FormGroup
  searchTechForm: FormGroup
  activeDayIsOpen: boolean = false
  viewDate: any = new Date()
  loadingEvents: boolean = false
  loggedUser: any
  selectedUser: any
  subContractorId: any
  reasonDropdown: any = []
  equipmentTobeRemoved: any
  radioRemovalAppointmentId: any
  appointmentStartDate: any = new Date()
  cepFormatadoValue: string;
  type;
  constructor( private httpCancelService: HttpCancelService, private activatedRoute: ActivatedRoute,
    private store: Store<AppState>, private momentService: MomentService, private _fb: FormBuilder, private rxjsService: RxjsService, private crudService: CrudService) {

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.customerId = (Object.keys(params['params']).length > 0) ? params['params']['customerId'] : '';
      this.addressId = (Object.keys(params['params']).length > 0) ? params['params']['addressId'] : '';
      this.equipmentTobeRemoved = (Object.keys(params['params']).length > 0) ? params['params']['equipmentTobeRemoved'] : '';
      this.radioRemovalWorkListId = (Object.keys(params['params']).length > 0) ? params['params']['radioRemovalWorkListId'] : '';
      this.radioRemovalAppointmentId = (Object.keys(params['params']).length > 0) ? params['params']['radioRemovalAppointmentId'] : '';
      this.appointmentStartDate = (Object.keys(params['params']).length > 0) ? params['params']['appointmentStartDate'] : '';
      this.viewDate = new Date(this.appointmentStartDate);
      this.type = (Object.keys(params['params']).length > 0) ? params['params']['type'] : '';
    });
  }
  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getClientDetails();
    this.getReasonDropdown();
    this.createScheduleForm()
    this.createSearchForm()
    this.searchKeywordRequest()
    this.reLoadData()
    if (this.radioRemovalAppointmentId) {
      this.getAppointmentListById()
    }
  }

  getClientDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {
    let filterData = { customerId: this.customerId, addressId: this.addressId };
    otherParams = { ...filterData };
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIOREMOVAL_DETAILS,
      null, false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.clientDetails = response.resources;
          this.radioRemovalWorkListId = response.resources.radioRemovalWorkListId;
          this.onShowValue();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getReasonDropdown(pageIndex?: string, pageSize?: string, otherParams?: object) {
    let filterData = { customerId: this.customerId, addressId: this.addressId };
    otherParams = { ...filterData };
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIOREMOVAL_RESCHEDULE_REASON,
      null, false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.reasonDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue() {
    this.dealerDetail = [
      {
        name: '', columns: [
          { name: 'Client', value: this.clientDetails && this.clientDetails.customerName ? this.clientDetails.customerName : '-', order: 1 },
          { name: 'Site Type', value: this.clientDetails && this.clientDetails.siteType ? this.clientDetails.siteType : '-', order: 2 },
          { name: 'Bill Name', value: this.clientDetails && this.clientDetails.billName ? this.clientDetails.billName : '-', order: 3 },
          { name: 'Site Details', value: this.clientDetails && this.clientDetails.siteDetails ? this.clientDetails.siteDetails : '-', order: 4 },
          { name: 'Dealer Type', value: this.clientDetails && this.clientDetails.dealType ? this.clientDetails.dealType : '-', order: 5 },
          { name: 'Customer ID', value: this.clientDetails && this.clientDetails.customerId ? this.clientDetails.customerId : '-', order: 96 },
          { name: 'Division', value: this.clientDetails && this.clientDetails.division ? this.clientDetails.division : '-', order: 7 },
          { name: 'Origin', value: this.clientDetails && this.clientDetails.origin ? this.clientDetails.origin : '-', order: 8 },
          { name: 'Customer Code', value: this.clientDetails && this.clientDetails.customerCode ? this.clientDetails.customerCode : '-', order: 9 },
          { name: 'Address', value: this.clientDetails && this.clientDetails.address ? this.clientDetails.address : '-', order: 10 },
          { name: 'Main Area', value: this.clientDetails && this.clientDetails.mainArea ? this.clientDetails.mainArea : '-', order: 11 },
          { name: 'Email Address', value: this.clientDetails && this.clientDetails.emailAddress ? this.clientDetails.emailAddress : '-', order: 12 },
          { name: 'Termination Reason', value: this.clientDetails && this.clientDetails.terminationReason ? this.clientDetails.terminationReason : '-', order: 13 },
          { name: 'Termination Subtype', value: this.clientDetails && this.clientDetails.terminationSubType ? this.clientDetails.terminationSubType : '-', order: 14 },
          { name: 'Termination Date', value: this.clientDetails && this.clientDetails.terminationDate ? this.clientDetails.terminationDate : '-', order: 15 },
        ]
      }
    ];
  }

  createScheduleForm() {
    this.scheduleForm = this._fb.group({
      appointmentStartDate: [this.viewDate ? this.viewDate : new Date()],
      appointmentScheduledDateTime: ['', Validators.required],
      radioRemovalAppointmentId: [this.radioRemovalAppointmentId ? this.radioRemovalAppointmentId : ''],
      rescheduleReasonId: [''],
      // scheduleType: ['1'],
      periodType: ['week'],
      subContractorId: [this.loggedUser?.userId],
      radioRemovalWorkListId: [this.radioRemovalWorkListId],
      confirmedWith: [''],
      poComments: [''],
      createdUserId: [this.loggedUser?.userId],
      modifiedUserId: [this.loggedUser?.userId],
      estimatedDuration: ['', Validators.required],
      search: ['']
    });

    if (this.radioRemovalAppointmentId) {
      this.scheduleForm = setRequiredValidator(this.scheduleForm, ['rescheduleReasonId', 'poComments']);
      // let otherParams = {}
      // this.getAppointmentListById(null, null, otherParams)
    }
    else {
      this.scheduleForm = clearFormControlValidators(this.scheduleForm, ["rescheduleReasonId", 'poComments']);
    }

  }

  getAppointmentListById() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIOREMOVAL_APPOINTMENT,
      this.radioRemovalAppointmentId, false)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          response.resources.poComments = response.resources.comments
          response.resources.appointmentScheduledDateTime = new Date(response.resources.appointmentScheduledDateTime)
          this.scheduleForm.patchValue(response.resources)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  createSearchForm() {
    this.searchTechForm = this._fb.group({
      search: [''],
    });
  }

  searchKeywordRequest() {
    this.searchTechForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          this.scheduleForm.get('search').setValue(val.search)

          return this.reLoadData()
        })
      )
      .subscribe();
  }

  // appinement booking calender view starts
  view: CalendarView = CalendarView.Week;

  CalendarView = CalendarView;
  yesterday = this.momentService.getYesterday()
  minDate: Date = new Date(this.yesterday);


  setView(view) {
    this.view = view;
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  onHoursSegmentClick(daytime) {
    return

  }


  handleEvent(action: string, event: any): void {
    return

  }


  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-pencil"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      },
    },
    // {
    //   label: '<i class="fa fa-trash"></i>',
    //   a11yLabel: 'Delete',
    //   onClick: ({ event }: { event: CalendarEvent }): void => {
    //     this.events = this.events.filter((iEvent) => iEvent !== event);
    //     this.handleEvent('Deleted', event);
    //   },
    // },
  ];

  colors: any = {
    red: {
      primary: '#ad2121',
      secondary: '#FAE3E3',
    },
    blue: {
      primary: '#1e90ff',
      secondary: '#D1E8FF',
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA',
    },
  };

  // users = users;
  users = [];




  // events: CalendarEvent[] = []
  events: CalendarEvent[] = []


  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {

    // if (!newStart || !newEnd) {
    //   return
    // }
    // if (newStart < this.minDate) {
    //   return
    // }
    // let isSameDay = this.momentService.isSameDayTime(event.start, newStart)
    // if (isSameDay) {
    //   return
    // }

    event.start = newStart;
    event.end = newEnd;
    this.events = [...this.events];
    // this.onAppointment(event)
  }

  userChanged({ event, newUser }) {
    // event.color = newUser.color;
    event.meta.user = newUser;
    this.events = [...this.events];

  }

  userSelected(user) {
    if (user.isSelectable)
      this.selectedUser = user
  }

  onDayChange() {

    // this.scheduleForm.get('appointmentDate').setValue(this.viewDate)
    this.scheduleForm.get('appointmentStartDate').setValue(this.viewDate)
    this.reLoadData()

  }

  onDaySelect() {

    // this.scheduleForm.get('appointmentDate').setValue(this.viewDate)
    this.scheduleForm.get('appointmentStartDate').setValue(this.viewDate)
    this.reLoadData()

  }

  reLoadData() {

    let scheduledData = this.scheduleForm.value
    Object.keys(scheduledData).forEach(key => {
      if (scheduledData[key] === "") {
        delete scheduledData[key]
      }
    });
    let filterdNewData1 = Object.entries(scheduledData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

    return of(this.getTechniciansList(filterdNewData1));

  }


  getTechniciansList(otherParams?: object) {
    if (Object.keys(otherParams).length > 0) {
      // logic for split columns and its values to key value pair

      Object.keys(otherParams).forEach((key) => {
        if (key.toLowerCase().includes('date')) {
          otherParams[key] = this.momentService.localToUTCDateTime(otherParams[key])
          // otherParams[key] =  this.convertData(otherParams[key])


          // otherParams[key] = this.momentService.localToUTC(otherParams[key]);
        } else {
          otherParams[key] = otherParams[key];
        }
      });
    }
    // this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.rxjsService.setGlobalLoaderProperty(false);
    this.loadingEvents = true
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, OutOfOfficeModuleApiSuffixModels.RADIOREMOVAL_APPOINTMENT_CALENDAR, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);

        this.loadingEvents = false
        if (response.resources) {
          this.selectedUser = null
          this.events = []

          response.resources.rows.forEach(element => {
            if (element.appointments.length > 0) {
              element.appointments.forEach((element1, index) => {
                let fromHour = element1.appointmentTimeFrom ? element1.appointmentTimeFrom.split(':') : null
                let toHour = element1.appointmentTimeTo ? element1.appointmentTimeTo.split(':') : null
                let existAppoitmnt = {
                  id: element1.radioRemovalAppointmentId,
                  radioRemovalAppointmentId: element1.radioRemovalAppointmentId,
                  radioRemovalAppointmentLogId: element1.radioRemovalAppointmentLogId,
                  appointmentTimeFrom: element1.appointmentTimeFrom,
                  appointmentTimeTo: element1.appointmentTimeTo,
                  name: null,
                  // hoverText: element1?.hoverText ? element1?.hoverText : element1.serviceCallNumber,
                  // title: element1.statusIconPath ? title : element1.serviceCallNumber,
                  title: ' <span title="' + (element1?.hoverText ? element1?.hoverText : element1.displayText) + '">' + element1.displayText + ' </span> ',
                  start: element1.appointmentTimeFrom ? new Date(this.momentService.addHoursMinits(element1.appointmentDate, fromHour[0], fromHour[1])) : null,
                  end: element1.appointmentTimeTo ? new Date(this.momentService.addHoursMinits(element1.appointmentDate, toHour[0], toHour[1])) : null,
                  color: element1.cssClass == 'status-label-red' ? this.colors.red : this.colors.blue,
                  draggable: false,
                }
                this.events.push(existAppoitmnt)

              })
            }
          });



          setTimeout(() => {
            if ($(".cal-time-events-wrapper")?.length == 0) {
              $('.cal-time-events').wrapAll('<div class="cal-time-events-wrapper"></div>');
            }
            $('.cal-time-events-wrapper').animate({
              scrollTop: 540
            }, 1000, function () {
            });
          }, 1000);

        }
      });
  }


  resetForm() {
    this.scheduleForm.reset()
    this.scheduleForm.get('appointmentStartDate').setValue(this.viewDate ? this.viewDate : new Date())
    // this.scheduleForm.get('scheduleType').setValue('1')
    this.scheduleForm.get('periodType').setValue('week')
    this.scheduleForm.get('subContractorId').setValue(this.loggedUser?.userId)
    this.scheduleForm.get('radioRemovalWorkListId').setValue(this.radioRemovalWorkListId)
    this.scheduleForm.get('radioRemovalAppointmentId').setValue(this.radioRemovalAppointmentId ? this.radioRemovalAppointmentId : '')
    this.scheduleForm.get('createdUserId').setValue(this.loggedUser?.userId)
    this.scheduleForm.get('modifiedUserId').setValue(this.loggedUser?.userId)
    // this.scheduleForm.get('estimatedDuration').markAsUntouched()
    // this.scheduleForm.get('estimatedDuration').updateValueAndValidity()
    this.reLoadData()
  }

  guardaValorCEPFormatado(evento) {
    this.cepFormatadoValue = evento;
    this.scheduleForm.get('estimatedDuration').setValue(evento)
  }

  onSubmit(): void {

    if (this.scheduleForm.invalid) {
      return;
    }
    let formValue = this.scheduleForm.value;
    formValue.comments = formValue.poComments
    formValue.appointmentScheduledDateTime = this.momentService.toFormateType(formValue.appointmentScheduledDateTime, 'YYYY-MM-DD HH:MM')
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      !formValue.radioRemovalAppointmentId ?
        this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIOREMOVAL_APPOINTMENT, formValue)
        :
        this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIOREMOVAL_RESCHEDULE, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
        if (!this.radioRemovalAppointmentId) {
          this.resetForm()
        }else{
          this.reLoadData()
        }
      }
    })
  }


}
