import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RadioRemovalsCalendarCompleteComponent } from './radio-removals-calendar-complete.component';
import { RadioRemovalsCalendarCountComponent } from './radio-removals-calendar-count.component';
import { RadioRemovalsCalendarHistoryComponent } from './radio-removals-calendar-history.component';
import { RadioRemovalsCalendarPrintComponent } from './radio-removals-calendar-print.component';
import { RadioRemovalsCalendarScheduleComponent } from './radio-removals-calendar-schedule.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';


const routes: Routes = [
  { path: '', component: RadioRemovalsCalendarScheduleComponent, canActivate: [AuthGuard], data: { title: 'Radio Removals Calendar View' } },
  { path: 'history', component: RadioRemovalsCalendarHistoryComponent, canActivate: [AuthGuard], data: { title: 'Radio Removals Calendar History' } },
  { path: 'print', component: RadioRemovalsCalendarPrintComponent, canActivate: [AuthGuard], data: { title: 'Radio Removals Calender Print' } },
  { path: 'complete', component: RadioRemovalsCalendarCompleteComponent, canActivate: [AuthGuard], data: { title: 'Radio Removals Calender Complete' } },
  { path: 'count', component: RadioRemovalsCalendarCountComponent, canActivate: [AuthGuard], data: { title: 'Radio Removals Count' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class RadioRemovalsCalendarViewRoutingModule { }
