
import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from '@modules/others/';
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { RadioRemovalCompleteModel, RadioRemovalISerializedtemsPostDTOModel, RadioRemovalUnSerializedItemsDetailsPostDTOModel, RemovalDeCoderDetailsPostDTOModel } from "./radio-removal-complete-model";
@Component({
  selector: 'app-radio-removals-calendar-complete',
  templateUrl: './radio-removals-calendar-complete.component.html'
})
export class RadioRemovalsCalendarCompleteComponent implements OnInit {
  dealerDetail: any;
  viewable: boolean = true;
  clientDetails: any;
  row: any = {};
  addressId: any;
  customerId: any;
  radioRemovalWorkListId: any;
  loggedUser: any
  completeForm: FormGroup
  reasionDropdown: any = []
  radioRemovalAppointmentId: any
  constructor( private httpCancelService: HttpCancelService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder,
    private store: Store<AppState>,   private rxjsService: RxjsService, private crudService: CrudService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.customerId = (Object.keys(params['params']).length > 0) ? params['params']['customerId'] : '';
      this.addressId = (Object.keys(params['params']).length > 0) ? params['params']['addressId'] : '';
      this.radioRemovalAppointmentId = (Object.keys(params['params']).length > 0) ? params['params']['radioRemovalAppointmentId'] : '';
      this.radioRemovalWorkListId = (Object.keys(params['params']).length > 0) ? params['params']['radioRemovalWorkListId'] : '';
    });
  }

  ngOnInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
    this.createForm()
    this.getReationDropdown()
    if (this.radioRemovalAppointmentId) {
      this.getSummaryDetails();
    }
  }

  createForm() {
    let radioRemovalCompleteModel = new RadioRemovalCompleteModel();
    this.completeForm = this.formBuilder.group({
      removalDeCoderDetailsPostDTO: this.formBuilder.array([]),
      radioRemovalUnSerializedItemsDetailsPostDTO: this.formBuilder.array([]),
      radioRemovalISerializedtemsPostDTO: this.formBuilder.array([]),
    });
    Object.keys(radioRemovalCompleteModel).forEach((key) => {
      this.completeForm.addControl(key, new FormControl(radioRemovalCompleteModel[key]));
    });
    this.completeForm = setRequiredValidator(this.completeForm, ["personOnSite", "feedback"]);
    this.completeForm.get('createdUserId').setValue(this.loggedUser.userId)
    this.completeForm.get('modifiedUserId').setValue(this.loggedUser.userId)
  }

  get getDecoderListArray(): FormArray {
    if (!this.completeForm) return;
    return this.completeForm.get("removalDeCoderDetailsPostDTO") as FormArray;
  }

  createDecoderListModel(cmcSmsGroupEmployeeListModel?: RemovalDeCoderDetailsPostDTOModel): FormGroup {
    let cmcSmsGroupEmployeeListFormControlModel = new RemovalDeCoderDetailsPostDTOModel(cmcSmsGroupEmployeeListModel);
    let formControls = {};
    Object.keys(cmcSmsGroupEmployeeListFormControlModel).forEach((key) => {
      formControls[key] = [{ value: cmcSmsGroupEmployeeListFormControlModel[key], disabled: false }]
    });
    let formBulderControls = this.formBuilder.group(formControls)
    formBulderControls.get('isRecovered').valueChanges.subscribe(bool => {
      if (bool) {
        formBulderControls.get('isRecovered').setValidators(Validators.required)
      } else {
        formBulderControls.get('isRecovered').setValidators(null)
        formBulderControls.get('isRecovered').updateValueAndValidity()
      }
    })
    return formBulderControls;
  }

  get getUnSerializedListArray(): FormArray {
    if (!this.completeForm) return;
    return this.completeForm.get("radioRemovalUnSerializedItemsDetailsPostDTO") as FormArray;
  }

  createUnSerialisedListModel(cmcSmsGroupEmployeeListModel?: RadioRemovalUnSerializedItemsDetailsPostDTOModel): FormGroup {
    let cmcSmsGroupEmployeeListFormControlModel = new RadioRemovalUnSerializedItemsDetailsPostDTOModel(cmcSmsGroupEmployeeListModel);
    let formControls = {};
    Object.keys(cmcSmsGroupEmployeeListFormControlModel).forEach((key) => {
      formControls[key] = [{ value: cmcSmsGroupEmployeeListFormControlModel[key], disabled: false }]
    });
    let formBulderControls = this.formBuilder.group(formControls)
    formBulderControls.get('isRecovered').valueChanges.subscribe(bool => {
      if (bool) {
        formBulderControls.get('isRecovered').setValidators(null);
      } else {
        formBulderControls.get('isRecovered').setValidators(Validators.required);
        formBulderControls.get('isRecovered').updateValueAndValidity()
      }
    })
    return formBulderControls;
  }

  get getSerializedListArray(): FormArray {
    if (!this.completeForm) return;
    return this.completeForm.get("radioRemovalISerializedtemsPostDTO") as FormArray;
  }

  createSerializedListModel(cmcSmsGroupEmployeeListModel?: RadioRemovalISerializedtemsPostDTOModel): FormGroup {
    let cmcSmsGroupEmployeeListFormControlModel = new RadioRemovalISerializedtemsPostDTOModel(cmcSmsGroupEmployeeListModel);
    let formControls = {};
    Object.keys(cmcSmsGroupEmployeeListFormControlModel).forEach((key) => {
      formControls[key] = [{ value: cmcSmsGroupEmployeeListFormControlModel[key], disabled: false }]
    });
    let formBulderControls = this.formBuilder.group(formControls)
    formBulderControls.get('isRecovered').valueChanges.subscribe(bool => {
      if (bool) {
        formBulderControls.get('isRecovered').setValidators(Validators.required);
      } else {
        formBulderControls.get('isRecovered').setValidators(null)
        formBulderControls.get('isRecovered').updateValueAndValidity()
      }
    })
    return formBulderControls;
  }
  setReasonDecoder(index) {
    if(this.getDecoderListArray.controls[index].get('isRecovered').value == false || this.getDecoderListArray.controls[index].get('isRecovered').value == "2") {
      this.getDecoderListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').setValidators(Validators.required);
      this.getDecoderListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').enable();
    }
    else {
      this.getDecoderListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').clearValidators();
      this.getDecoderListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').setValidators(null);
      this.getDecoderListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').disable();
    }    
  }
  setReasonUnSerialized(index) {
    if(this.getUnSerializedListArray.controls[index].get('isRecovered').value == false || this.getUnSerializedListArray.controls[index].get('isRecovered').value == "2") {
      this.getUnSerializedListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').setValidators(Validators.required);
      this.getUnSerializedListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').enable();
    }
    else {
      this.getUnSerializedListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').clearValidators();
      this.getUnSerializedListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').setValidators(null);
      this.getUnSerializedListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').disable();
    }    
  }
  setReasonSerialized(index) {
    if(this.getSerializedListArray?.controls[index].get("isRecovered").value == false || this.getSerializedListArray?.controls[index].get("isRecovered").value == "2") {
      this.getSerializedListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').setValidators(Validators.required);
      this.getSerializedListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').enable();
    }
    else {
      this.getSerializedListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').clearValidators();
      this.getSerializedListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').setValidators(null);
      this.getSerializedListArray.controls[index].get('radioRemovalNonRecoveryReasonCofigId').disable();
    }    
  }
  getSummaryDetails() {
    let otherParams = {}
    otherParams['RadioRemovalAppointmentsId'] = this.radioRemovalAppointmentId
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_CLIENT_SUMMARY,
      undefined,
      false, prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.clientDetails = response.resources;
          if (this.clientDetails.radioRemovalCallCompleteDetails) {
            this.clientDetails.radioRemovalAppointmentSummaryId = this.clientDetails.radioRemovalCallCompleteDetails.radioRemovalAppointmentSummaryId
            this.clientDetails.personOnSite = this.clientDetails.radioRemovalCallCompleteDetails.personOnSite
            this.clientDetails.feedback = this.clientDetails.radioRemovalCallCompleteDetails.feedback
          }
          this.clientDetails.removalDeCoderDetailsPostDTO = this.clientDetails.radioRemovalDeCoderDetails
          this.clientDetails.radioRemovalUnSerializedItemsDetailsPostDTO = this.clientDetails.radioRemovalUnSerializedItemsDetailsDTOs
          this.clientDetails.radioRemovalISerializedtemsPostDTO = this.clientDetails.radioRemovalISerializedtemsDetailsDTOs
          if (this.clientDetails.removalDeCoderDetailsPostDTO.length > 0) {
            this.clientDetails.removalDeCoderDetailsPostDTO.forEach((removalDeCoderDetailsPostDTOModel: RemovalDeCoderDetailsPostDTOModel) => {
              this.getDecoderListArray.push(this.createDecoderListModel(removalDeCoderDetailsPostDTOModel));
            });
          }
          if (this.clientDetails.radioRemovalUnSerializedItemsDetailsPostDTO.length > 0) {
            this.clientDetails.radioRemovalUnSerializedItemsDetailsPostDTO.forEach((radioRemovalUnSerializedItemsDetailsPostDTOModel: RadioRemovalUnSerializedItemsDetailsPostDTOModel) => {
              this.getUnSerializedListArray.push(this.createUnSerialisedListModel(radioRemovalUnSerializedItemsDetailsPostDTOModel));
            });
          }
          if (this.clientDetails.radioRemovalISerializedtemsPostDTO.length > 0) {
            this.clientDetails.radioRemovalISerializedtemsPostDTO.forEach((radioRemovalISerializedtemsPostDTOModel: RadioRemovalISerializedtemsPostDTOModel) => {
              this.getSerializedListArray.push(this.createSerializedListModel(radioRemovalISerializedtemsPostDTOModel));
            });
          }
          this.onShowValue();
          this.completeForm.patchValue(this.clientDetails)
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  getReationDropdown() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_RADIO_REMOVAL_NONRECOVERY_REASON_CONFIG)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.reasionDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue() {
    this.dealerDetail = [
      {
        name: '', columns: [
          { name: 'Client', value: this.clientDetails && this.clientDetails.customerName ? this.clientDetails.customerName : '-', order: 1 },
          { name: 'Site Type', value: this.clientDetails && this.clientDetails.siteType ? this.clientDetails.siteType : '-', order: 2 },
          { name: 'Bill Name', value: this.clientDetails && this.clientDetails.billName ? this.clientDetails.billName : '-', order: 3 },
          { name: 'Site Details', value: this.clientDetails && this.clientDetails.siteDetails ? this.clientDetails.siteDetails : '-', order: 4 },
          { name: 'Dealer Type', value: this.clientDetails && this.clientDetails.dealType ? this.clientDetails.dealType : '-', order: 5 },
          { name: 'Customer ID', value: this.clientDetails && this.clientDetails.customerId ? this.clientDetails.customerId : '-', order: 96 },
          { name: 'Division', value: this.clientDetails && this.clientDetails.division ? this.clientDetails.division : '-', order: 7 },
          { name: 'Origin', value: this.clientDetails && this.clientDetails.origin ? this.clientDetails.origin : '-', order: 8 },
          { name: 'Customer Code', value: this.clientDetails && this.clientDetails.customerCode ? this.clientDetails.customerCode : '-', order: 9 },
          { name: 'Address', value: this.clientDetails && this.clientDetails.address ? this.clientDetails.address : '-', order: 10 },
          { name: 'Main Area', value: this.clientDetails && this.clientDetails.mainArea ? this.clientDetails.mainArea : '-', order: 11 },
          { name: 'Email Address', value: this.clientDetails && this.clientDetails.emailAddress ? this.clientDetails.emailAddress : '-', order: 12 },
          { name: 'Termination Reason', value: this.clientDetails && this.clientDetails.terminationReason ? this.clientDetails.terminationReason : '-', order: 13 },
          { name: 'Termination Subtype', value: this.clientDetails && this.clientDetails.terminationSubType ? this.clientDetails.terminationSubType : '-', order: 14 },
          { name: 'Termination Date', value: this.clientDetails && this.clientDetails.terminationDate ? this.clientDetails.terminationDate : '-', order: 15 },
        ]
      }
    ];
  }

  onSubmit() {
    if (this.completeForm.invalid) {
      return;
    }
    let formValue = this.completeForm.value;
    formValue.removalCallCompleteDetailsPostDTO = {
      radioRemovalAppointmentSummaryId: formValue.radioRemovalAppointmentSummaryId,
      radioRemovalAppointmentId: formValue.radioRemovalAppointmentId,
      personOnSite: formValue.personOnSite,
      feedback: formValue.feedback,
    }

    formValue?.removalDeCoderDetailsPostDTO.forEach(element => {
      element.isRecovered  =  element.isRecovered || element.isRecovered == "1" ? true :false;
    });
    formValue?.radioRemovalUnSerializedItemsDetailsPostDTO.forEach(element => {
      element.isRecovered  =  element.isRecovered || element.isRecovered == "1" ? true :false;
    });
    formValue?.radioRemovalISerializedtemsPostDTO.forEach(element => {
      element.isRecovered  =  element.isRecovered || element.isRecovered == "1" ? true :false;
    });
    
    formValue.radioRemovalISerializedtemsPostDTO.forEach(element => {
      element.radioRemovalISerializedtemsDetailsPostDTO = {
        radioRemovalAppointmentSummaryItemDetailId: formValue.radioRemovalAppointmentSummaryItemDetailId,
        radioRemovalAppointmentSummaryItemId: formValue.radioRemovalAppointmentSummaryItemId,
        billOfMaterialItemDetailId: formValue.billOfMaterialItemDetailId,
        radioRemovalNonRecoveryReasonCofigId: formValue.radioRemovalNonRecoveryReasonCofigId,
      }
    });
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_CLIENT_SUMMARY, formValue)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {
      }
    });
  }
}