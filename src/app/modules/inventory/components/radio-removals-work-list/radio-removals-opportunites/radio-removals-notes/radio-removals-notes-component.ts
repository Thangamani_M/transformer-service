import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";

@Component({
    selector: 'app-radio-removals-notes',
    templateUrl: './radio-removals-notes-component.html',
    styleUrls: ['./radio-removals-notes.component.scss']
  })

  export class RadioRemovalsNotesComponent implements OnInit {

    screen:any;
    customerId:any;
    addressId:any;
    notesForm : FormGroup;
    userData: any;
    isScreen : any;
    notesList:any;
    servicecancellationId:any;

    constructor( private config: DynamicDialogConfig,private ref:DynamicDialogRef, private crudService: CrudService,private store: Store<AppState>,private rxjsService: RxjsService){
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      })
    }


    ngOnInit() {
      this.screen = this.config?.data?.row.screen;
      this.isScreen = this.screen;
      this.screen = this.screen == 'add' ? 'Add Note' : 'Notes';
      this.addressId = this.config?.data?.row.addressId;
      this.customerId = this.config?.data?.row.customerId;
      this.servicecancellationId = this.config?.data?.row.servicecancellationId;

      this.createForm();
      let params = {customerId:this.customerId,addressId:this.addressId};
      this.getNotes(params);
     }
     createForm(): void {
      this.notesForm = new FormGroup({
        'customerNotes': new FormControl(null),
      });
      this.notesForm = setRequiredValidator(this.notesForm, ['customerNotes']);
    }
    getNotes(params) {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_CUSTOMER_NOTES_DETAILS, null,false,prepareRequiredHttpParams(params))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
           this.notesList = response.resources;
        }
      });
    }
    onSubmit() {
      if (this.notesForm.invalid) {
        return;
      }
      let formValue = this.notesForm.value;
      let finalObject =  {
        customerNotes: formValue.customerNotes,
        customerId: this.customerId,
        addressId: this.addressId,
        servicecancellationId: this.servicecancellationId,
        createdUserId: this.userData.userId
      }
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_CUSTOMER_NOTES, finalObject, 1)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.ref.close(response);
            this.rxjsService.setDialogOpenProperty(false);
        }
      });
    }
     btnCloseClick() {
      this.ref.close(false);
    }
  }
