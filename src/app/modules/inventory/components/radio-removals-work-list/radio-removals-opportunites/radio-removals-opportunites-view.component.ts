import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { DialogService } from "primeng/api";
import { RadioRemovalsNotesComponent } from "./radio-removals-notes/radio-removals-notes-component";
import { RadioRemovalsUnrecoverableComponent } from "./radio-removals-unrecoverable/radio-removals-unrecoverable-component";
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { AppState } from "@app/reducers";
import { Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-radio-removals-opportunites-view',
  templateUrl: './radio-removals-opportunites-view.component.html',
  styleUrls: ['./radio-removals-opportunites-view.component.scss']
})

export class RadioRemovalsOpportunitesViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  dealerDetail: any;
  viewable: boolean = true;
  primengTableConfigProperties: any;
  pageIndex: any = 0;
  loadings: Boolean = false;
  clientDetails: any;
  dataDecodersList: any = [];
  dataContactList: any = [];
  dataEquipmentList: any = [];
  totalRecords: any = 0;
  totalContactRecords: any = 0;
  totalEquipmentRecords: any = 0;
  row: any = {};
  status1: boolean = false;
  addressId: any;
  customerId: any;
  isShowNoContactRecords: any = true;
  isShowNoDecodersRecords: any = true;
  isShowNoEquipmentRecords: any = true;
  radioRemovalWorkListId: any;
  servicecancellationId: any;
  equipmentTobeRemoved: any
  allPermission = []
  constructor(private dialogService: DialogService, private activatedRoute: ActivatedRoute, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private store: Store<AppState>, private snackbarService: SnackbarService) {
    super()
    this.primengTableConfigProperties = {
      tableCaption: "Client Details",
      selectedTabIndex: 0,
      breadCrumbItems: [{displayName: 'Radio Removal', relativeRouterUrl: ''},{ displayName: 'Radio Removal WorkList', relativeRouterUrl: '/inventory/radio-removals-worklist' }, { displayName: 'Radio Removal Details', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Radio Removals',
            dataKey: 'radioRemovalWorkListId',
            selectedTabIndex: 0,
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'decoder', header: 'Decoder' },
              { field: 'terminatorNumber', header: 'Transmitter Number', width: '250px'},
              { field: 'description', header: 'Description' },
              { field: 'area', header: 'Area' },
              { field: 'dateLoaded', header: 'Date Loaded', isDateTime:true },
              { field: 'status', header: 'Status' }],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true
          },
          {
            caption: 'Radio Removals',
            dataKey: 'radioRemovalWorkListId',
            selectedTabIndex: 1,
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'contact', header: 'Contact' },
              { field: 'number', header: 'Number' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true
          },
          {
            caption: 'Radio Removals',
            dataKey: 'radioRemovalWorkListId',
            selectedTabIndex: 2,
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'equipment', header: 'Equiment' },
              { field: 'serialNumber', header: 'Serial Number' },
            ],
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true
          }
        ]
      }
    }

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.customerId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.addressId = (Object.keys(params['params']).length > 0) ? params['params']['addressId'] : '';
      this.equipmentTobeRemoved = (Object.keys(params['params']).length > 0) ? params['params']['equipmentTobeRemoved'] : '';

    });
    this.onShowValue();
  }
  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.getClientDetails();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Worklist"];
      this.allPermission = permission;
    });
  }

  getClientDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {
    let filterData = { customerId: this.customerId, addressId: this.addressId };
    otherParams = { ...filterData };
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIOREMOVAL_DETAILS,
      null, false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response?.isSuccess && response?.statusCode == 200) {
          this.clientDetails = response.resources;
          this.radioRemovalWorkListId = response.resources.radioRemovalWorkListId;
          this.dataDecodersList = this.clientDetails.decodersListDetails;
          this.dataContactList = this.clientDetails.contactListDetails;
          this.dataEquipmentList = this.clientDetails.equipmentRemovedListDetails;
          this.isShowNoContactRecords = this.dataContactList && this.dataContactList.length ? false : true;
          this.isShowNoDecodersRecords = this.dataDecodersList && this.dataDecodersList.length ? false : true;
          this.isShowNoEquipmentRecords = this.dataEquipmentList && this.dataEquipmentList.length ? false : true;
          this.servicecancellationId = response.resources.servicecancellationId;
          response.resources.status = response.resources.status ? response?.resources?.status?.toString().trim() : null;
          if (response.resources.status == 'Unrecoverable' || response.resources.status == 'Completed' ||
            response.resources.status == 'unrecoverable' || response.resources.status == 'completed')
            this.status1 = true;
          this.onShowValue();
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue() {
    this.dealerDetail = [
      {
        name: '', columns: [
          { name: 'Client', value: this.clientDetails && this.clientDetails.customerName ? this.clientDetails.customerName : '-', order: 1 },
          { name: 'Site Type', value: this.clientDetails && this.clientDetails.siteType ? this.clientDetails.siteType : '-', order: 2 },
          { name: 'Bill Name', value: this.clientDetails && this.clientDetails.billName ? this.clientDetails.billName : '-', order: 3 },
          { name: 'Site Details', value: this.clientDetails && this.clientDetails.siteDetails ? this.clientDetails.siteDetails : '-', order: 4 },
          { name: 'Dealer Type', value: this.clientDetails && this.clientDetails.dealType ? this.clientDetails.dealType : '-', order: 5 },
          { name: 'Customer ID', value: this.clientDetails && this.clientDetails.customerId ? this.clientDetails.customerId : '-', order: 96 },
          { name: 'Division', value: this.clientDetails && this.clientDetails.division ? this.clientDetails.division : '-', order: 7 },
          { name: 'Origin', value: this.clientDetails && this.clientDetails.origin ? this.clientDetails.origin : '-', order: 8 },
          { name: 'Customer Code', value: this.clientDetails && this.clientDetails.customerCode ? this.clientDetails.customerCode : '-', order: 9 },
          { name: 'Address', value: this.clientDetails && this.clientDetails.address ? this.clientDetails.address : '-', order: 10 },
          { name: 'Main Area', value: this.clientDetails && this.clientDetails.mainArea ? this.clientDetails.mainArea : '-', order: 11 },
          { name: 'Email Address', value: this.clientDetails && this.clientDetails.emailAddress ? this.clientDetails.emailAddress : '-', order: 12 },
          { name: 'Termination Reason', value: this.clientDetails && this.clientDetails.terminationReason ? this.clientDetails.terminationReason : '-', order: 13 },
          { name: 'Termination Subtype', value: this.clientDetails && this.clientDetails.terminationSubType ? this.clientDetails.terminationSubType : '-', order: 14 },
          { name: 'Termination Date', value: this.clientDetails && this.clientDetails.terminationDate ? this.clientDetails.terminationDate : '-', isDate: true, order: 15 },
        ]
      }
    ];
  }
  openAddNotesPopup(type) {
    let action =  type =='add' ? "Add Note" : "Notes";

    let isAccessDeined = this.getPermissionByActionType(action);
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }

    const ref = this.dialogService.open(RadioRemovalsNotesComponent, {
      header: 'Suspensions (Dealer and IIP)',
      showHeader: false,
      baseZIndex: 1000,
      width: '63vw',
      height: '470px',
      data: {
        row: { screen: type, customerId: this.customerId, addressId: this.addressId, servicecancellationId: this.servicecancellationId }
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
      }
      this.rxjsService.setGlobalLoaderProperty(false);
      this.loadings = false;
    });
  }
  openUnRecoverablePopup() {
    let isAccessDeined = this.getPermissionByActionType("Unrecoverable");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (!this.radioRemovalWorkListId)
      return;

    const ref = this.dialogService.open(RadioRemovalsUnrecoverableComponent, {
      header: 'Suspensions (Dealer and IIP)',
      showHeader: false,
      baseZIndex: 1000,
      width: '42vw',
      height: '470px',
      data: {
        row: { customerId: this.customerId, addressId: this.addressId, radioRemovalWorkListId: this.radioRemovalWorkListId }
      }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
      }
    });
  }

  openAppointment() {
    let isAccessDeined = this.getPermissionByActionType("Schedule Appointment");
    if (isAccessDeined) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['radio-removal/radio-removals-worklist/schedule-calendar-view'], {
      queryParams: {
        equipmentTobeRemoved: this.equipmentTobeRemoved,
        radioRemovalWorkListId: this.radioRemovalWorkListId,
        customerId: this.customerId,
        addressId: this.addressId,
        type: 'client-details',
        appointmentStartDate: this.clientDetails.AppointmentStartDate ? this.clientDetails.AppointmentStartDate : new Date(),
      }
    });

  }

  openAppointmentHistory() {
    let isAccessDeined = this.getPermissionByActionType("Appointment History");
      if (isAccessDeined) {
        return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
      }
    this.router.navigate(['radio-removal/radio-removals-worklist/schedule-calendar-view/history'], {
      queryParams: {
        equipmentTobeRemoved: this.equipmentTobeRemoved,
        radioRemovalWorkListId: this.radioRemovalWorkListId,
        customerId: this.customerId,
        addressId: this.addressId,
        appointmentStartDate: this.clientDetails.AppointmentStartDate ? this.clientDetails.AppointmentStartDate : new Date(),
      }
    });

  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      default:
    }
  }
  onChangeSelecedRows(e) {
    //this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  getPermissionByActionType(actionTypeMenuName): boolean {
    let foundObj = this.allPermission.find(fSC => fSC.menuName == actionTypeMenuName);
    if (foundObj) {
      return false;
    }
    else {
      return true;
    }
  }
}
