import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { EventMgntModuleApiSuffixModels } from '@modules/event-management/shared/enums/configurations.enum';
import { RadioRemovalOppFilter } from '@modules/inventory/models/radio-removal-opportunity.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils/inventory-module.enums';
import { ItManagementApiSuffixModels, loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-radio-removals-opportunites',
  templateUrl: './radio-removals-opportunites.component.html',
  styleUrls: ['./radio-removals-opportunites.component.scss']
})
export class RadioRemovalsOpportunitesComponent extends PrimeNgTableVariablesModel implements OnInit {
  startTodayDate = new Date()
  primengTableConfigProperties: any;
  scrollEnabled: boolean = false;
  observableResponse: any;
  totalRecords: any = 0;
  row: any = {};
  radioRemovalForm: FormGroup;
  loggedUser: any;
  filterData: any;
  mainAreaDropDown: any;
  subAreaDropDown: any;
  regionDropDown: any;
  subDivisionDrop: any;
  branchDropDown: any;
  statusDropDown: any;
  subRubDropDown: any;
  radioRemovalFilter: any = false;
  pageSize: any = 10;
  first = 0;
  reset: boolean;
  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private datePipe: DatePipe,
    private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Radio Removal Worklist",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '/radio-removal' }, { displayName: 'Radio Removal WorkList', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Radio Removals',
            dataKey: 'radioRemovalWorkListId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enablePrintBtn: true,
            printTitle: 'Radio Removals',
            printSection: 'print-section0',
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'customerName', header: 'CustomerName' },
            { field: 'status', header: 'Status' }, { field: 'region', header: 'Region' },
            { field: 'branch', header: 'Branch' }, { field: 'mainArea', header: 'MainArea' },
            { field: 'subArea', header: 'SubArea' }, { field: 'suburb', header: 'Suburb' },
            { field: 'suspendedOn', header: 'SuspendedOn', type: 'date' }, { field: 'equipmentTobeRemoved', header: 'EquipmentTobe Removed' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.RADIO_REMOVAL_WORKLIST,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            enableExportBtn: false,
            enableEmailBtn: false,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true
          },
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.filterData = (Object.keys(params['params']).length > 0) ? params['params'] : null;
    });
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.onCRUDRequested('get');
    this.getMainAreaDropDown();
    this.getSubAreaDropDown();
    this.getBranchDropDown();
    this.getRegionDropDown();
    this.getSubdivisionDropDown();
    this.getStatusDropDown();
    this.getSubRubDropDown();
    this.radioRemovalFilterForm();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Worklist"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  radioRemovalFilterForm(stockListModel?: RadioRemovalOppFilter) {
    let stockMovementsListModel = new RadioRemovalOppFilter(stockListModel);
    this.radioRemovalForm = this.formBuilder.group({});
    Object.keys(stockMovementsListModel).forEach((key) => {
      if (typeof stockMovementsListModel[key] === 'string') {
        this.radioRemovalForm.addControl(key, new FormControl(stockMovementsListModel[key]));
      }
    });
  }

  getMainAreaDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_MAIN_AREA_ALL, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.mainAreaDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getSubAreaDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.EVENT_MANAGEMENT, EventMgntModuleApiSuffixModels.UX_SUB_AREA, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subAreaDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getRegionDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_REGION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.regionDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getBranchDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_BRANCHES, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.branchDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getSubdivisionDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_SUBDIVISION, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subDivisionDrop = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getStatusDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STATUSDROPDOWN, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.statusDropDown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getSubRubDropDown() {
    this.crudService.get(ModulesBasedApiSuffix.IT_MANAGEMENT, ItManagementApiSuffixModels.UX_SUBRUB, null, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources) {
          this.subRubDropDown = response.resources;
        }
      });
  }

  submitFilter() {
    let regionIds = this.filterIds(Array(this.radioRemovalForm.value.regionIds));
    let subDivisionIds = this.filterIds(Array(this.radioRemovalForm.value.subDivisionIds));
    let mainIds = this.filterIds(Array(this.radioRemovalForm.value.mainIds));
    let subAreaIds = this.filterIds(Array(this.radioRemovalForm.value.subAreaIds));
    let subRubIds = this.filterIds(Array(this.radioRemovalForm.value.subRubIds));
    let statusIds = this.filterIds(Array(this.radioRemovalForm.value.statusIds));
    let formDate = this.radioRemovalForm.value.formDate ? this.datePipe.transform(this.radioRemovalForm.value.formDate, 'dd-MM-yyyy, h:mm:ss a') : null;
    let todate = this.radioRemovalForm.value.todate ? this.datePipe.transform(this.radioRemovalForm.value.todate, 'dd-MM-yyyy, h:mm:ss a') : null;
    let filteredData = Object.assign({},
      { regionIds: this.radioRemovalForm.get('regionIds').value ? regionIds.toString() : '' },
      { subDivisionIds: this.radioRemovalForm.get('subDivisionIds').value ? subDivisionIds.toString() : '' },
      { mainIds: this.radioRemovalForm.get('mainIds').value ? mainIds.toString() : '' },
      { subAreaIds: this.radioRemovalForm.get('subAreaIds').value ? subAreaIds.toString() : '' },
      { subRubIds: this.radioRemovalForm.get('subRubIds').value ? subRubIds.toString() : '' },
      { statusIds: this.radioRemovalForm.get('statusIds').value ? statusIds.toString() : '' },
      { formDate: this.radioRemovalForm.get('formDate').value ? formDate : '' },
      { todate: this.radioRemovalForm.get('todate').value ? todate : '' },
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    this.scrollEnabled = false;
    this.row['pageIndex'] = 0
    this.observableResponse = this.radioRemovalWorkList(this.row['pageIndex'], this.row['pageSize'], filterdNewData);

    this.radioRemovalFilter = !this.radioRemovalFilter;
  }
  filterIds(arr) {
    let ids = [];
    if (arr && arr.length > 0) {
      arr.forEach(element => {
        ids.push(element.id)
      });
      return ids;
    }
    else
      return ids;
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: this.pageSize };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        unknownVar = { ...this.filterData, ...unknownVar };
        this.radioRemovalWorkList(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        break;
      case CrudType.DELETE:
        break;
      case CrudType.EXPORT:
        break;
      case CrudType.FILTER:
        this.radioRemovalFilter = !this.radioRemovalFilter;
        break;
      case CrudType.VIEW:
        this.router.navigate(['radio-removal/radio-removals-worklist/view'], {
          queryParams: {
            id: row['customerId'], addressId: row['addressId'],
            equipmentTobeRemoved: row['equipmentTobeRemoved']
          }
        });

        break;
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.FILTER:
        switch (this.selectedTabIndex) {
          case 2:
            this.radioRemovalFilter = !this.radioRemovalFilter;
            break;
          case 3:
            this.radioRemovalFilter = !this.radioRemovalFilter;
            break;
        }
        break;
    }
  }
  radioRemovalWorkList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true
    let otherParamsAll = Object.assign(otherParams, this.filterData);
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_WORKLIST,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParamsAll)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.suspendedOn = this.datePipe.transform(val.suspendedOn, 'dd-MM-yyyy, h:mm:ss a');
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
      this.reset = false;
    });
  }

  resetForm() {
    this.radioRemovalFilter = !this.radioRemovalFilter;
    this.radioRemovalForm.reset();
    this.reset = true;
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet((this.selectedRows.length > 0) ? this.selectedRows : this.dataList);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }


  onChangeSelecedRows(e) {
    //this.selectedRows = e;
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}

