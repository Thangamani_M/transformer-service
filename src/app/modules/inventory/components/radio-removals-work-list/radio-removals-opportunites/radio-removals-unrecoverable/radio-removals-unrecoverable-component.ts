import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService, setRequiredValidator } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";

@Component({
    selector: 'app-radio-removals-unrecoverable',
    templateUrl: './radio-removals-unrecoverable-component.html',
    // styleUrls: ['./radio-removals-unrecoverable-component.scss']
  })

  export class RadioRemovalsUnrecoverableComponent implements OnInit {

    customerId:any;
    addressId:any;
    unrecoverableForm : FormGroup;
    userData: any;
    alarmEquipmentList :any=[];
    radioTransmitterList :any=[];
    radioRemovalWorkListId:any;

    constructor( private config: DynamicDialogConfig,private ref:DynamicDialogRef, private crudService: CrudService,private store: Store<AppState>,private rxjsService: RxjsService){
      this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
        if (!userData) return;
        this.userData = userData;
      });
    }

    ngOnInit() {
        this.addressId = this.config?.data?.row.addressId;
        this.customerId = this.config?.data?.row.customerId;
        this.radioRemovalWorkListId= this.config?.data?.row.radioRemovalWorkListId;
        this.createForm();
        this.getAlarmEquipmentList();
    }
    createForm(): void {
        this.unrecoverableForm = new FormGroup({
          'radioTransmitterUnrecoverableReasonConfigId': new FormControl(null),
          'alarmEquipmentUnrecoverableReasonConfigId': new FormControl(null),
          'comments': new FormControl(null)
        });
        this.unrecoverableForm = setRequiredValidator(this.unrecoverableForm, ['comments','radioTransmitterUnrecoverableReasonConfigId','alarmEquipmentUnrecoverableReasonConfigId']);
      }
     getAlarmEquipmentList() {

        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RADIO_REMOVAL_UNRECOVERABLE_REASONS, null, false, null)
        .subscribe((response: IApplicationResponse) => {
            if (response.resources) {
            this.alarmEquipmentList = response.resources;
            this.radioTransmitterList = response.resources;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });

     }
   onSubmit() {
        if (this.unrecoverableForm.invalid) {
          return;
        }

        let formValue = this.unrecoverableForm.value;
        let finalObject =  {
          radioTransmitterUnrecoverableReasonConfigId: formValue.radioTransmitterUnrecoverableReasonConfigId,
          alarmEquipmentUnrecoverableReasonConfigId: formValue.alarmEquipmentUnrecoverableReasonConfigId,
          comments: formValue.comments,
          customerId: this.customerId,
          addressId: this.addressId,
          radioRemovalWorkListId:this.radioRemovalWorkListId,
          isActive:true,
          createdUserId: this.userData.userId
        }
        this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_UNRECOVERABLE_REASONS, finalObject, 1)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.ref.close(response);
              this.rxjsService.setDialogOpenProperty(false);
          }
        });
    }
    btnCloseClick() {
        this.ref.close(false);
    }
}
