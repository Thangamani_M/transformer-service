import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { RadioRemovalsNotesComponent } from './radio-removals-notes/radio-removals-notes-component';
import { RadioRemovalsOpportunitesViewComponent } from './radio-removals-opportunites-view.component';
import { RadioRemovalsOpportunitesComponent } from './radio-removals-opportunites.component';
import { RadioRemovalsUnrecoverableComponent } from './radio-removals-unrecoverable/radio-removals-unrecoverable-component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

@NgModule({
  declarations: [RadioRemovalsOpportunitesComponent, RadioRemovalsOpportunitesViewComponent,RadioRemovalsNotesComponent ,RadioRemovalsUnrecoverableComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    MatIconModule,
    NgxBarcodeModule,
    NgxPrintModule,
    AutoCompleteModule,
    RouterModule.forChild([
       {path: '', component: RadioRemovalsOpportunitesComponent,canActivate:[AuthGuard], data: { title: 'Radio Removal' }},
       { path: 'view', component: RadioRemovalsOpportunitesViewComponent, canActivate:[AuthGuard],data: { title: 'Radio Removal Worklist' } },
    ])
  ],
  entryComponents: [ RadioRemovalsOpportunitesViewComponent,RadioRemovalsNotesComponent,RadioRemovalsUnrecoverableComponent ]
})
export class RadioRemovalsOpportunitesModule { }
