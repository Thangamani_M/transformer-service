import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', data: { index: 0, title: 'radio-removals-opportunites' }, loadChildren: () => import('./radio-removals-opportunites/radio-removals-opportunites.module').then(m => m.RadioRemovalsOpportunitesModule) },
  { path: 'schedule-calendar-view', loadChildren: () => import('./radio-removals-calendar-view/radio-removals-calendar-view.module').then(m => m.RadioRemovalsCalendarViewModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class RadioRemovalsWorkListRoutingModule { }
