import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService } from '@app/shared';
import { IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared/utils';
import { WasteDisposalApproverInfoModel } from '@modules/inventory/models/WasteDisposalApproverInfo.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-inventory-manager-request-view',
  templateUrl: './inventory-manager-request-view.component.html',
  styleUrls: ['./inventory-manager.component.scss']
})
export class InventoryManagerRequestViewComponent implements OnInit {

  itemId: string;
  wasteDisposalRequestModel: any = {};
  wasteDisposalApproverInfoForm: FormGroup;
  actionTypesList: any[];
  loggedUser: UserLogin;
  selectedTabIndex: any = 0;

  constructor(private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,
    private router: Router,
    private formBuilder: FormBuilder,
    private crudService: CrudService,
    private store: Store<AppState>) {
    this.itemId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.loadActionTypes();
    this.createWDApproverInfoForm();
    if (this.itemId != undefined) {
      this.getWDInventoryManagerById(this.itemId);
    }
  }

  loadActionTypes(): void {
    forkJoin(this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WASTE_DISPOSAL_ACTION_TYPES))
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp.isSuccess && resp.statusCode === 200) {
            switch (ix) {
              case 0:
                this.actionTypesList = resp.resources;
                break;
            }
          }
        });

        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }



  getWDInventoryManagerById(id: string) {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WASTE_DISPOSAL_MANAGER_VIEW, id, true, null)
      .subscribe(
        response => {
          if (response.isSuccess && response.statusCode === 200) {
            this.wasteDisposalRequestModel = response.resources;
            this.wasteDisposalApproverInfoForm.get('wdRequestId').setValue(this.wasteDisposalRequestModel['wdRequestId']);
            this.wasteDisposalApproverInfoForm.get('wdRequestDate').setValue(this.wasteDisposalRequestModel['wdRequestDate']);
            this.wasteDisposalApproverInfoForm.get('reason').setValue(this.wasteDisposalRequestModel['reason']);
            this.wasteDisposalApproverInfoForm.get('overallWriteOffValue').setValue(this.wasteDisposalRequestModel['overallWriteOffValue']);
            this.wasteDisposalApproverInfoForm.get('wdActionTypeId').setValue(this.wasteDisposalRequestModel['wdActionTypeId']);
            this.wasteDisposalApproverInfoForm.get('IMApproverId').setValue(this.loggedUser.userId);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  createWDApproverInfoForm(wasteDisposalApproverInfoModelData?: WasteDisposalApproverInfoModel): void {
    let wasteDisposalApproverInfoModel = new WasteDisposalApproverInfoModel(wasteDisposalApproverInfoModelData);
    this.wasteDisposalApproverInfoForm = this.formBuilder.group({});
    Object.keys(wasteDisposalApproverInfoModel).forEach((key) => {
      this.wasteDisposalApproverInfoForm.addControl(key, new FormControl(wasteDisposalApproverInfoModel[key]));
    });
  }

  onSubmit(): void {
    if (this.wasteDisposalApproverInfoForm.invalid) {
      return;
    }
    this.crudService
      .update(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WASTE_DISPOSAL_MANAGER,
        this.wasteDisposalApproverInfoForm.value
      )
      .pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
      .subscribe(response => {
        if (response.isSuccess) {
          this.router.navigate(['/inventory/waste-disposal/request-list'])
        }

      })
  }
}
