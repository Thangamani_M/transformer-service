import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { HttpCancelService, IApplicationResponse, LoggedInUserModel, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import {
  ModulesBasedApiSuffix, setRequiredValidator
} from '@app/shared/utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { WasteDisposalItemsModel, WasteDisposalRequestAddEditModel, WasteDisposalStocksModel } from '@modules/inventory/models/WasteDisposalRequestAddEdit';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { InventoryStaffStockInfoComponent } from './inventory-staff-stock-info.component';

@Component({
  selector: 'app-inventory-staff-request-add-edit',
  templateUrl: './inventory-staff-request-add-edit.component.html',
  styleUrls: ['./inventory-staff.component.scss']
})
export class InventoryStaffRequestAddEditComponent implements OnInit {


  wasteDisposalRequestDto: WasteDisposalRequestAddEditModel;
  wasteDisposalRequestAddEditForm: FormGroup;
  selectedTabIndex = 0;
  wdRequestId: any;
  btnName: any;
  wasteDisposalItems: FormArray;
  wasteDisposalStocks: FormArray;
  companyList = [];
  companyId: any;
  wasteDisposalItemsArray = [];
  wasteDisposalStocksArray = [];
  itemCode: any;
  warehouseId: any;
  loggedInUserData: LoggedInUserModel;
  errorMessage: any;
  controllerId: any;
  storageLocationId: any;
  checkList: any;
  selectedItemCode: any;
  submitted: boolean = false;

  constructor(private activatedRoute: ActivatedRoute, private dialog: MatDialog,
    private router: Router,
    private crudservice: CrudService, private rxjsService: RxjsService, private formBuilder: FormBuilder,
    private httpCancelService: HttpCancelService, private store: Store<AppState>) {
    this.wdRequestId = this.activatedRoute.snapshot.queryParams.id;
    this.combineLatestNgrxStoreData();
  }

  ngOnInit(): void {
    this.createWasteDisposalRequestAddEditForm();
    this.onFormControlChanges();
    this.forkJoinRequests();
    if (this.wdRequestId) {
      this.btnName = "Update";
      this.GetWasteDisposalDetailsByWarehouseIdUpdate().subscribe((response: IApplicationResponse) => {
        this.wasteDisposalItems = this.getWasteDisposalItems;
        this.wasteDisposalStocks = this.getWasteDisposalStocks;
        let wasteDisposalUpdate = new WasteDisposalRequestAddEditModel(response.resources);
        this.wasteDisposalRequestAddEditForm.patchValue(response.resources);
        if (response.resources.wasteDisposalItems.length > 0) {
          response.resources.wasteDisposalItems.forEach((wasteDisposalItemsModel: WasteDisposalItemsModel, i: number) => {
            this.wasteDisposalItems.push(this.createWasteDisposalItems(wasteDisposalItemsModel));
          });
        }
        this.wasteDisposalRequestAddEditForm.get('companyId').setValue(wasteDisposalUpdate.companyId);
        this.wasteDisposalRequestAddEditForm.get('controllerId').setValue(this.controllerId)
        this.wasteDisposalRequestAddEditForm.get('companyId').disable();
        this.wasteDisposalRequestAddEditForm.get('subLocation').disable();
        this.wasteDisposalRequestAddEditForm.get('subLocationCode').disable();
        wasteDisposalUpdate.wasteDisposalItems.forEach(element => {
          this.wasteDisposalItemsArray.push(element);
        })
      })
    }
    else {
      this.btnName = "Add";
      this.wasteDisposalRequestAddEditForm.get('subLocation').disable();
      this.wasteDisposalRequestAddEditForm.get('subLocationCode').disable();
    }
  }

  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      this.warehouseId = this.loggedInUserData.warehouseId;
      this.controllerId = this.loggedInUserData.userId;
    });
  }

  createWasteDisposalRequestAddEditForm(wasteDisposalRequestAddEdit?: WasteDisposalRequestAddEditModel): void {
    let wasteDisposalRequestAddEditModel = new WasteDisposalRequestAddEditModel();
    this.wasteDisposalRequestAddEditForm = this.formBuilder.group({});
    Object.keys(wasteDisposalRequestAddEditModel).forEach((key) => {
      this.wasteDisposalRequestAddEditForm.addControl(key, key === 'wasteDisposalItems' ? new FormArray([]) :
        new FormControl((key === 'subLocation' ? { value: wasteDisposalRequestAddEditModel[key], disabled: false } : wasteDisposalRequestAddEditModel[key])));
    });
    this.wasteDisposalRequestAddEditForm = setRequiredValidator(this.wasteDisposalRequestAddEditForm, ["companyId"]);

  }

  onFormControlChanges(): void {
    if (this.wdRequestId)
      return

    else {
      this.GetWasteDisposalDetailsByWarehouseId();
    }
  }

  GetWasteDisposalDetailsByWarehouseId() {
    if (!this.wdRequestId) {
      let params = new HttpParams();
      params = params.append('WarehouseId', this.loggedInUserData.warehouseId)
      params = params.append('WDRequestId', '');

      this.crudservice.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WASTE_DISPOSAL_GET,
        undefined,
        false,
        params
      ).subscribe((response: IApplicationResponse) => {
        this.wasteDisposalItems = this.getWasteDisposalItems;
        this.wasteDisposalStocks = this.getWasteDisposalStocks;
        if (response.isSuccess && response.statusCode === 200) {

          response.resources.wasteDisposalItems.forEach(element => {
            this.wasteDisposalItemsArray.push(element)
          });
          this.storageLocationId = response.resources.storageLocationId;

          this.wasteDisposalItemsArray.forEach(element => {
            this.wasteDisposalStocksArray.push(element.wasteDisposalStocks)
          })

          this.wasteDisposalRequestAddEditForm.get('companyId').setValue(this.companyId);
          this.wasteDisposalRequestAddEditForm.get('controllerId').setValue(this.controllerId);

          if (response.resources.wasteDisposalItems.length > 0) {
            response.resources.wasteDisposalItems.forEach((wasteDisposalItemsModel: WasteDisposalItemsModel, i: number) => {
              this.wasteDisposalItems.push(this.createWasteDisposalItems(wasteDisposalItemsModel));
            });
          }
          this.wasteDisposalRequestAddEditForm.patchValue(
            {
              companyId: response.resources.companyId === null ? '' : response.resources.companyId,
              controllerId: this.controllerId,
              locationId: response.resources.locationId,
              storageLocationId: response.resources.storageLocationId,
              subLocation: response.resources.subLocation,
              subLocationCode: response.resources.subLocationCode,
              warehouseId: response.resources.warehouseId,
            });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  GetWasteDisposalDetailsByWarehouseIdUpdate(): Observable<IApplicationResponse> {
    let params = new HttpParams();
    params = params.append('WarehouseId', this.loggedInUserData.warehouseId)
    params = params.append('WDRequestId', this.wdRequestId);
    return this.crudservice.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WASTE_DISPOSAL_GET,
      undefined,
      false,
      params
    );
  }

  get getWasteDisposalItems(): FormArray {
    if (!this.wasteDisposalRequestAddEditForm) return;
    return this.wasteDisposalRequestAddEditForm.get("wasteDisposalItems") as FormArray;
  }

  get getWasteDisposalStocks(): FormArray {
    if (!this.wasteDisposalRequestAddEditForm) return;
    return this.wasteDisposalRequestAddEditForm.get("wasteDisposalStocks") as FormArray
  }

  forkJoinRequests(): void {
    forkJoin(this.crudservice.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WASTE_DISPOSAL_COMPANIES))
      .subscribe((response: IApplicationResponse[]) => {
        response.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp.isSuccess && resp.statusCode === 200) {
            switch (ix) {
              case 0:
                this.companyList = resp.resources;
                break;
            }
          }
        });

        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }



  createWasteDisposalItems(wasteDisposalItemModel?: WasteDisposalItemsModel): FormGroup {
    let wasteDisposalItemFormControlsModel = new WasteDisposalItemsModel(wasteDisposalItemModel);
    let formControls = {};
    Object.keys(wasteDisposalItemFormControlsModel).forEach((key) => {
      if (key === 'itemCode' || key === 'itemName' || key === 'quantity') {
        formControls[key] = [{ value: wasteDisposalItemFormControlsModel[key], disabled: true }]
      } else {
        formControls[key] = new FormControl(wasteDisposalItemFormControlsModel[key]);
      }
    });
    return this.formBuilder.group(formControls);
  }

  createdWasteDisposalStocks(wasteDisposalStocksModel?: WasteDisposalStocksModel): FormGroup {
    let wasteDisposalItemStocksFormControlsModel = new WasteDisposalStocksModel(wasteDisposalStocksModel);
    let formControls = {};
    Object.keys(wasteDisposalItemStocksFormControlsModel).forEach((key) => {

      formControls[key] = [{ value: wasteDisposalItemStocksFormControlsModel[key] }]

    });
    return this.formBuilder.group(formControls);
  }


  openStockInfo(items) {
    this.rxjsService.setDialogOpenProperty(true);
    const dialogReff = this.dialog.open(InventoryStaffStockInfoComponent, { width: '700px', disableClose: true, data: { data: items } });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });
    dialogReff.componentInstance.outputData.subscribe(ele => {
      this.checkList = ele;
      items.quantity = this.checkList.length;
    })
    dialogReff.componentInstance.selectedItemCode.subscribe(ele => {
      this.selectedItemCode = ele
    })
  }

  saveDisposalRequest() {

    if (!this.wasteDisposalRequestAddEditForm.valid) {
      //this.submitted = false;
      return;
    }
    this.wasteDisposalRequestAddEditForm.get('subLocation')
    let wasteDisposalRequest = this.wasteDisposalRequestAddEditForm.getRawValue();

    let finalArray = [];
    wasteDisposalRequest.wasteDisposalItems.forEach(element => {
      let wasteDisposalStocks = [];
      wasteDisposalStocks = element.wasteDisposalStocks.filter(wd => wd['selectedFlag']);
      element.wasteDisposalStocks = wasteDisposalStocks;
      if (wasteDisposalStocks.length > 0) {

        wasteDisposalRequest.wasteDisposalItems = [];
        finalArray.push(element);
        finalArray.forEach(obj => {
          obj.quantity = obj.wasteDisposalStocks.length;
          wasteDisposalRequest.wasteDisposalItems.push(obj);
        })
      }
    })
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    if (this.wdRequestId) {
      wasteDisposalRequest.wdRequestId = this.wdRequestId;

      this.crudservice.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WASTE_DISPOSAL_ADD_EDIT, wasteDisposalRequest)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/inventory/waste-disposal/staff-request-list']);

            }
          },
          error: err => this.errorMessage = err
        });
    } else {

      this.crudservice.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WASTE_DISPOSAL_ADD_EDIT, wasteDisposalRequest)
        .subscribe({
          next: response => {
            if (response.isSuccess) {
              this.router.navigate(['/inventory/waste-disposal/staff-request-list']);

            }
          },
          error: err => this.errorMessage = err
        });
    }
  }

  navigateToList() {
    this.router.navigate(['/inventory/waste-disposal/staff-request-list']);
  }
}

