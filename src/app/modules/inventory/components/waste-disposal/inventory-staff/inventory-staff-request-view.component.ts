import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { WasteDisposalRequest } from '@modules/inventory/models/WasteDisposalRequest';
@Component({
  selector: 'app-inventory-staff-request-view',
  templateUrl: './inventory-staff-request-view.component.html',
  styleUrls: ['./inventory-staff.component.scss']
})
export class InventoryStaffRequestViewComponent implements OnInit {

  wdRequestId: string;
  wasteDisposalRequestDto: WasteDisposalRequest | any;
  selectedTabIndex = 0;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private commonService: CrudService, private rxjsService: RxjsService) {
    this.wdRequestId = this.activatedRoute.snapshot.queryParams.id
  }

  ngOnInit(): void {
    this.wasteDisposalRequestDto = new WasteDisposalRequest();
    this.GetWasteDisposalById(this.wdRequestId);
  }

  GetWasteDisposalById(wdRequestId: string) {
    this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WASTE_DISPOSAL_VIEW, wdRequestId, false, null).subscribe((response: IApplicationResponse) => {
      this.wasteDisposalRequestDto = response.resources;

      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  navigateToEdit() {
    this.router.navigate(['/inventory/waste-disposal/staff-request-list/add-edit'], { queryParams: { id: this.wdRequestId } });
  }

  navigateToList() {
    this.router.navigate(['/inventory/waste-disposal/staff-request-list']);
  }
}
