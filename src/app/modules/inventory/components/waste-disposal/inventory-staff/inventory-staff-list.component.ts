import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudService, CrudType, CustomDirectiveConfig, debounceTimeForDeepNestedObjectSearchkeyword, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,
  RxjsService
} from '@app/shared';
import { TableFilterFormService } from '@app/shared/services/create-form.services';
import { MomentService } from '@app/shared/services/moment.service';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-inventory-staff-list',
  templateUrl: './inventory-staff-list.component.html',
  styleUrls: ['./inventory-staff.component.scss']
})
export class InventoryStaffListComponent {

  userData: UserLogin;
  searchForm: FormGroup;
  observableResponse: any;
  onRowClick: any;
  onSearchInputChange: any;
  dataList: any
  status: any = [];
  selectedTabIndex: any = 0;
  totalRecords: any;
  pageLimit: number[] = [10, 25, 50, 75, 100];
  loggedInUserData: LoggedInUserModel;
  primengTableConfigProperties: any;
  pageSize: number = 10;
  columnFilterForm: FormGroup;
  searchColumns: any
  row: any = {}
  loading: boolean;
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  selectedRows: string[] = [];

  constructor(
    private commonService: CrudService,
    private tableFilterFormService: TableFilterFormService,
    private router: Router,
    private rxjsService: RxjsService,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private _fb: FormBuilder, private momentService: MomentService,
  ) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Waste Disposal Inventory Staff",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Waste Disposal Inventory Staff' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Waste Disposal Inventory Staff',
            dataKey: 'wdRequestId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'wdRequestNumber', header: 'WD Request ID' }, { field: 'controllerName', header: 'Controller Name' },
            { field: 'wdRequestDate', header: 'Created Date & Time' }, { field: 'storageLocationName', header: 'Sub Location' }, { field: 'imApproverName', header: 'Approved By' },
            { field: 'createdDate', header: 'Collection Date & Time' }, { field: 'status', header: 'Status' }],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            ebableAddActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.WASTE_DISPOSAL,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }

    this.searchForm = this._fb.group({ searchKeyword: "" });
    this.columnFilterForm = this._fb.group({});

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });

  }

  ngOnInit(): void {
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].columns);
    this.searchKeywordRequest();
    this.columnFilterRequest();
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }



  combineLatestNgrxStoreData() {
    combineLatest(
      this.store.select(loggedInUserData)
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }

  searchKeywordRequest() {
    this.searchForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
        distinctUntilChanged(),
        switchMap(val => {
          return of(this.onCRUDRequested(CrudType.GET, {}));
        })
      )
      .subscribe();
  }

  columnFilterRequest() {
    this.columnFilterForm.valueChanges
      .pipe(
        debounceTime(debounceTimeForDeepNestedObjectSearchkeyword),
        distinctUntilChanged(),
        switchMap(obj => {
          Object.keys(obj).forEach(key => {
            if (obj[key] === "") {
              delete obj[key]
            }
          });
          this.searchColumns = Object.entries(obj).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
          this.row['searchColumns'] = this.searchColumns
          return of(this.onCRUDRequested(CrudType.GET, this.row));
        })
      )
      .subscribe();
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {
          WarehouseId: this.userData.warehouseId,
          UserId: this.userData.userId
        })
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;

      }
    })
  }

  loadPaginationLazy(event) {
    let row = {}
    row['pageIndex'] = event.first / event.rows;
    row["pageSize"] = event.rows;
    row["sortOrderColumn"] = event.sortField;
    row["sortOrder"] = event.sortOrder == 1 ? 'ASC' : 'DESC';
    this.row = row;
    this.onCRUDRequested(CrudType.GET, this.row);
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (this.searchForm.value.searchKeyword) {
          otherParams["search"] = this.searchForm.value.searchKeyword;
        }
        if (Object.keys(this.row).length > 0) {
          // logic for split columns and its values to key value pair

          if (this.row['searchColumns']) {
            Object.keys(this.row['searchColumns']).forEach((key) => {
              if (key.toLowerCase().includes('date')) {
                otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]);
              } else {
                otherParams[key] = this.row['searchColumns'][key];
              }
            });
          }
          if (this.row['sortOrderColumn']) {
            otherParams['sortOrder'] = this.row['sortOrder'];
            otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
          }
        }
        otherParams['WarehouseId'] = this.userData.warehouseId;
        otherParams['UserId'] = this.userData.userId;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], otherParams)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      default:
    }
  }

  onTabChange(event) {
    this.row = {}
    this.columnFilterForm = this._fb.group({})
    this.columnFilterForm = this.tableFilterFormService.createFormGroup(this.primengTableConfigProperties.tableComponentConfigs.tabsList[event.index].columns);
    this.columnFilterRequest();
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/inventory/waste-disposal/company-addition-list'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigateByUrl("/inventory/waste-disposal/staff-request-list/add-edit");
        break;
      case CrudType.VIEW:
        this.router.navigate(["/inventory/waste-disposal/staff-request-list/view"], { queryParams: { id: editableObject['wdRequestId'] } });
        break;
    }
  }

}
