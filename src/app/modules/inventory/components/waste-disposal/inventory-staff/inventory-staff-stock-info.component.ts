import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-inventory-staff-stock-info',
  templateUrl: './inventory-staff-stock-info.component.html',
  styleUrls: ['./inventory-staff.component.scss']
})
export class InventoryStaffStockInfoComponent implements OnInit {

  wasteDisposalStocksArray = []
  wasteDisposalItems: any;
  checkedList = [];
  @Output() outputData = new EventEmitter<any>();
  @Output() selectedItemCode = new EventEmitter<any>();
  masterSelected: boolean;


  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.wasteDisposalItems = this.data.data;

    this.wasteDisposalItems.wasteDisposalStocks.forEach(element => {
      if (element.selectedFlag === true) {
        this.checkedList.push(element)
      } else {
        this.checkedList = [];
      }
    })
  }

  onCheckboxChange(option, event) {

    if (event.target.checked) {
      option.selectedFlag = !option.selectedFlag;
      this.checkedList.push(option);
    } else {
      for (var i = 0; i < this.wasteDisposalItems.wasteDisposalStocks.length; i++) {
        if (this.checkedList[i] == option) {
          this.checkedList.splice(i, 1);
        }
      }
    }

  }

  checkUncheckAll(event) {
    this.checkedList = [];

    this.wasteDisposalItems.wasteDisposalStocks.forEach(element => {
      if (event.target.checked === true) {
        element.selectedFlag = true;
        this.checkedList.push(element)
      }
      else {
        element.selectedFlag = false;
        this.checkedList = [];
      }
    });

  }
  gotoWasteDisposalRequestAddEdit() {
    this.outputData.emit(this.checkedList)
    this.selectedItemCode.emit(this.wasteDisposalItems.itemCode);
  }
}
