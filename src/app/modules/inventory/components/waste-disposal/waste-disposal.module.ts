import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { ConfigCompanyAddEditComponent, ConfigCompanyListComponent, DOAApprovalAddEditComponent, DOAApprovalListComponent, InventoryManagerRequestListComponent, InventoryManagerRequestViewComponent, InventoryStaffListComponent, InventoryStaffRequestAddEditComponent, InventoryStaffRequestViewComponent, InventoryStaffStockInfoComponent, WasteDisposalListComponent, WasteDisposalRoutingModule, WasteDisposalviewComponent } from '@inventory/components/waste-disposal';
import { SignaturePadModule } from 'ngx-signaturepad';
import { WasteDisposalImapprovalListComponent } from './waste-disposal-imapproval/waste-disposal-imapproval-list/waste-disposal-imapproval-list.component';
import { WasteDisposalImapprovalUpdateComponent } from './waste-disposal-imapproval/waste-disposal-imapproval-update/waste-disposal-imapproval-update.component';
import { WasteDisposalImapprovalViewComponent } from './waste-disposal-imapproval/waste-disposal-imapproval-view/waste-disposal-imapproval-view.component';
import { WasteDisposalStockCollectionListComponent } from './waste-disposal-stock-collection/waste-disposal-stock-collection-list/waste-disposal-stock-collection-list.component';
import { WasteDisposalStockCollectionUpdateComponent } from './waste-disposal-stock-collection/waste-disposal-stock-collection-update/waste-disposal-stock-collection-update.component';
import { WasteDisposalStockCollectionViewComponent } from './waste-disposal-stock-collection/waste-disposal-stock-collection-view/waste-disposal-stock-collection-view.component';

@NgModule({
  declarations: [
    DOAApprovalListComponent, DOAApprovalAddEditComponent, ConfigCompanyListComponent, ConfigCompanyAddEditComponent,
    InventoryManagerRequestListComponent, InventoryManagerRequestViewComponent,
    WasteDisposalListComponent, WasteDisposalviewComponent, InventoryStaffListComponent, InventoryStaffRequestAddEditComponent, InventoryStaffRequestViewComponent, InventoryStaffStockInfoComponent,
    WasteDisposalImapprovalListComponent, WasteDisposalImapprovalUpdateComponent, WasteDisposalImapprovalViewComponent, WasteDisposalStockCollectionListComponent, WasteDisposalStockCollectionViewComponent, WasteDisposalStockCollectionUpdateComponent
  ],

  imports: [
    CommonModule, MaterialModule, SharedModule, LayoutModule,
    WasteDisposalRoutingModule, ReactiveFormsModule, FormsModule, SignaturePadModule
  ],
  entryComponents: [InventoryStaffStockInfoComponent]
})
export class WasteDisposalModule { }
