import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { StockAssetConsumableItemsModel, StockConsumableDocItemsModel, WasteDisposalStockCollectModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-waste-disposal-stock-collection-update',
  templateUrl: './waste-disposal-stock-collection-update.component.html',
  styleUrls: ['./waste-disposal-stock-collection-update.component.scss']
})
export class WasteDisposalStockCollectionUpdateComponent implements OnInit {

  wdRequestId: any;
  wdApprovalId: any;
  wdRequestApprovalDetails: any = {};
  wdRequestTabsDetails: any = {};
  wasteDisposalStockCollectUpdateForm: FormGroup;
  assetItemsDetails: FormArray;
  consumableItemsDetails: FormArray;
  wasteDocumentTypeDTOs: FormArray;
  attachmentTypeDropDown: any = [];
  userData: UserLogin;
  modalDetails: any = {};
  isWasteDisposalDialogModal: any;
  selectedTabIndex: any = 0;
  totalAssetWriteValue: any = 0;
  totalConsumeWriteValue: any = 0
  roleName: any;
  selectedFiles = new Array();
  totalFileSize = 0;
  public formData = new FormData();
  imageName: string;
  documentsDetails = new Array();
  showRemove: boolean = false;

  constructor(
    private router: Router, private crudService: CrudService, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private snackbarService: SnackbarService, private store: Store<AppState>,
  ) {

    this.wdRequestId = this.activatedRoute.snapshot.queryParams.requestId;
    this.wdApprovalId = this.activatedRoute.snapshot.queryParams.approvalId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.roleName = this.userData.roleName;
  }
  @ViewChild(SignaturePad, { static: false }) signaturePad: any;

  public signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 500,
    'canvasHeight': 80
  };

  ngOnInit(): void {

    this.createwasteDisposalApprovalUpdateForm();
    this.getDropdown();
    if (this.wdRequestId) {
      this.getwdRequestApprovalDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.wdRequestApprovalDetails = response.resources;
          this.wdRequestTabsDetails = response.resources.wasteDisposalDetails;
          this.wasteDisposalStockCollectUpdateForm.patchValue(response.resources);
          this.wasteDisposalStockCollectUpdateForm.get('userId').patchValue(this.userData.userId)
          this.wasteDisposalStockCollectUpdateForm.get('comments').patchValue(this.wdRequestApprovalDetails.headerDocuments.comments)
          this.assetItemsDetails = response.resources.assetWasteDisposalItems;
          this.assetItemsDetails = this.getWasteDisposalAssetFormArray;
          this.consumableItemsDetails = response.resources.consumableWasteDisposalItems;
          this.consumableItemsDetails = this.getWasteDisposalConsumeFormArray;
          this.wasteDocumentTypeDTOs = response.resources.signatureDocuments;
          this.wasteDocumentTypeDTOs = this.getWasteDisposalDocumentFormArray;

          this.totalAssetWriteValue = 0;
          this.totalConsumeWriteValue = 0;

          if (this.wdRequestTabsDetails.assetWasteDisposalItems.length > 0) {
            this.wdRequestTabsDetails.assetWasteDisposalItems.forEach((asset) => {
              this.totalAssetWriteValue += asset.writeOffValue;
              this.assetItemsDetails.push(this.createWasteDisposalGroupForm(asset));
            });
          }
          else {
            this.assetItemsDetails.push(this.createWasteDisposalGroupForm());
          }
          if (this.wdRequestTabsDetails.consumableWasteDisposalItems.length > 0) {
            this.wdRequestTabsDetails.consumableWasteDisposalItems.forEach((consume) => {
              this.totalConsumeWriteValue += consume.writeOffValue;
              this.consumableItemsDetails.push(this.createWasteDisposalGroupForm(consume));
            });
          }
          else {
            this.consumableItemsDetails.push(this.createWasteDisposalGroupForm());
          }
          if (this.wdRequestApprovalDetails.signatureDocuments.length > 0) {
            this.wdRequestApprovalDetails.signatureDocuments.forEach((consume) => {
              consume['wDRequestId'] = this.wdRequestId;
              consume['createdUserId'] = this.userData.userId;
              this.wasteDocumentTypeDTOs.push(this.createWasteDisposalDocGroupForm(consume));
            });
          }
          else {
            this.wasteDocumentTypeDTOs.push(this.createWasteDisposalDocGroupForm());
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.consumableItemsDetails = this.getWasteDisposalConsumeFormArray;
          this.consumableItemsDetails.push(this.createWasteDisposalGroupForm());
          this.assetItemsDetails = this.getWasteDisposalAssetFormArray;
          this.assetItemsDetails.push(this.createWasteDisposalGroupForm());
          this.wasteDocumentTypeDTOs = this.getWasteDisposalDocumentFormArray;
          this.wasteDocumentTypeDTOs.push(this.createWasteDisposalDocGroupForm());
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  //Get Details
  getwdRequestApprovalDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WASTE_DISPOSAL,
      this.wdRequestId
    );
  }

  getDropdown(): void {
    /* Company dropdown */
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WDREQUEST_ATTACHMENT_TYPE)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.attachmentTypeDropDown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  /* Create formArray start */
  createwasteDisposalApprovalUpdateForm(): void {
    /*Assets Tab*/
    let stockOrderModel = new WasteDisposalStockCollectModel();
    this.wasteDisposalStockCollectUpdateForm = this.formBuilder.group({
      assetItemsDetails: this.formBuilder.array([]),
      consumableItemsDetails: this.formBuilder.array([]),
      wasteDocumentTypeDTOs: this.formBuilder.array([])
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.wasteDisposalStockCollectUpdateForm.addControl(key, new FormControl(stockOrderModel[key]));
    });

  }

  get getWasteDisposalConsumeFormArray(): FormArray {
    if (this.wasteDisposalStockCollectUpdateForm !== undefined) {
      return this.wasteDisposalStockCollectUpdateForm.get("consumableItemsDetails") as FormArray;
    }
  }

  get getWasteDisposalAssetFormArray(): FormArray {
    if (this.wasteDisposalStockCollectUpdateForm !== undefined) {
      return this.wasteDisposalStockCollectUpdateForm.get("assetItemsDetails") as FormArray;
    }
  }

  get getWasteDisposalDocumentFormArray(): FormArray {
    if (this.wasteDisposalStockCollectUpdateForm !== undefined) {
      return this.wasteDisposalStockCollectUpdateForm.get("wasteDocumentTypeDTOs") as FormArray;
    }
  }

  /* Create FormArray controls */
  createWasteDisposalGroupForm(approvalDetails?: StockAssetConsumableItemsModel): FormGroup {
    let structureTypeData = new StockAssetConsumableItemsModel(approvalDetails ? approvalDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  /* Create FormArray controls */
  createWasteDisposalDocGroupForm(approvalDetails?: StockConsumableDocItemsModel): FormGroup {
    let structureTypeData = new StockConsumableDocItemsModel(approvalDetails ? approvalDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      formControls[key] = [{ value: structureTypeData[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  // onFileChange uploadFiles
  uploadFiles(event) {
    this.selectedFiles = [];
    const supportedExtensions = ['jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'];
    for (var i = 0; i < event.target.files.length; i++) {
      let selectedFile = event.target.files[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension)) {
        this.selectedFiles.push(event.target.files[i]);
        this.totalFileSize += event.target.files[i].size;
        this.showRemove = true;
      }
      else {
        this.snackbarService.openSnackbar("Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx", ResponseMessageTypes.WARNING);
      }
    }
  }

  removeSelectedFile(index) {
    this.selectedFiles.splice(index, 1);
    this.showRemove = false;
  }

  drawComplete() {

  }

  clearPad() {
    this.signaturePad.clear();
  }

  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.resizeCanvas();
    this.signaturePad.clear();
  }

  addDocumentType(): void {
    let obj = this.attachmentTypeDropDown.filter(data => {
      if (data.id == this.wasteDisposalStockCollectUpdateForm.get('wdRequestAttachmentId').value) {
        return data.id;
      }
    });

    if (obj.length >= 0) {
      if (this.getWasteDisposalDocumentFormArray.value.find(x => x.wdRequestAttachmentTypeId ==
        this.wasteDisposalStockCollectUpdateForm.controls['wdRequestAttachmentId'].value) == null) {
        let addSupportingDocObj = this.formBuilder.group({
          "wdRequestAttachmentTypeId": obj[0].id,
          "wdRequestAttachmentTypeName": obj[0].displayName,
          "docName": this.selectedFiles[0].name,
          "wDRequestId": this.wdRequestId,
          "createdUserId": this.userData.userId,
          "comments": this.wasteDisposalStockCollectUpdateForm.get('comments').value
        });
        this.wasteDisposalStockCollectUpdateForm.controls['wdRequestAttachmentId'].patchValue('');
        this.getWasteDisposalDocumentFormArray.push(addSupportingDocObj);
      }
      else {
        this.snackbarService.openSnackbar('Document Type Already Added', ResponseMessageTypes.WARNING);
        this.wasteDisposalStockCollectUpdateForm.controls['wdRequestAttachmentId'].patchValue('');
      }
    }
  }

  onTabClicked(tabIndex) {
    this.selectedTabIndex = tabIndex.index;
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  openModelSerialPopup(type: any, itemId: any, itemCode: any, itemName: any) {
    let data = {};
    if (type == 'assets') {
      data = this.wdRequestTabsDetails.assetWasteDisposalItems.filter(x => {
        return x.itemId == itemId;
      });
    }
    else {
      data = this.wdRequestTabsDetails.consumableWasteDisposalItems.filter(y => {
        return y.itemId == itemId;
      });
    }
    this.modalDetails = {
      stockCode: itemCode,
      stockDescription: itemName,
      serialNumbers: data[0].wasteDisposalStocks
    }
    this.isWasteDisposalDialogModal = true;
  }

  onSubmit(): void {

    let obj = {}
    let object = {};
    if (this.wdRequestApprovalDetails.status == 'Approved') {
      if (this.wasteDisposalStockCollectUpdateForm.invalid || this.signaturePad.signaturePad._data.length == 0) {
        this.snackbarService.openSnackbar("Please Enter comments and signature", ResponseMessageTypes.WARNING);
        return;
      }

      if (this.signaturePad.signaturePad._data.length > 0) {
        this.imageName = 'waste-disposal' + "-signature.jpeg";
        const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
        const imageFile = new File([imageBlob], this.imageName, { type: 'image/jpeg' });
        this.formData.append('document_files[]', imageFile);
      }

      obj = {
        WDRequestId: this.wdRequestId,
        createdUserId: this.userData.userId,
        CollectedBy: this.userData.userId,
        Comments: this.wasteDisposalStockCollectUpdateForm.get('comments').value
      }
      this.formData.append("creditNoteDetails", JSON.stringify(obj));
    }

    if (this.wdRequestApprovalDetails.status == 'Collected') {
      object = this.getWasteDisposalDocumentFormArray.value;
      if (this.selectedFiles.length > 0) {
        for (const file of this.selectedFiles) {
          this.formData.append('File', file);
        }
      }
      this.formData.append("documentDetails", JSON.stringify(object));
    }

    let crudService: Observable<IApplicationResponse> =
      this.wdRequestApprovalDetails.status == 'Collected' ?
        this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.WASTE_DISPOSAL_DOCUMENTS, this.formData) :
        this.crudService.update(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.WASTE_DISPOSAL_COLLECT, this.formData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'waste-disposal', 'waste-disposal-stock'], { skipLocationChange: true });
  }

  navigateToViewPage() {
    this.router.navigate(['/inventory', 'waste-disposal', 'waste-disposal-stock', 'view'], {
      queryParams: {
        requestId: this.wdRequestId,
        approvalId: this.wdApprovalId,
      }, skipLocationChange: true
    });
  }

}
