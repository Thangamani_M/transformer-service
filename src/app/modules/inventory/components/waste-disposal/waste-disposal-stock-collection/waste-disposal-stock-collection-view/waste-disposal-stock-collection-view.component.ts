import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-waste-disposal-stock-collection-view',
  templateUrl: './waste-disposal-stock-collection-view.component.html',
  styleUrls: ['./waste-disposal-stock-collection-view.component.scss']
})
export class WasteDisposalStockCollectionViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  WastDisposalstockcollectionView: any;
  wdRequestId: any;
  wdApprovalId: any;
  wdRequestApprovalDetails: any = {};
  wdRequestTabsDetails: any = {};
  modalDetails: any = {};
  isWasteDisposalDialogModal: any;
  primengTableConfigProperties: any;
  consumableWasteDisposalItems: any;
  totalAssetWriteValue: any = 0;
  totalConsumeWriteValue: any = 0

  constructor(
    private router: Router, private crudService: CrudService,private store: Store<AppState>,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private snackbarService: SnackbarService) {
    super();
    this.wdRequestId = this.activatedRoute.snapshot.queryParams.requestId;
    this.wdApprovalId = this.activatedRoute.snapshot.queryParams.approvalId;
    this.primengTableConfigProperties = {
      tableCaption: "Waste Diposal Collection",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Waste Disposal', relativeRouterUrl: '' },
      { displayName: 'Waste Disposal Collection list', relativeRouterUrl: '/inventory/waste-disposal/waste-disposal-stock' }, { displayName: 'Waste Disposal Collection View', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: true,
            enableClearfix: true,
          }]
      }
    }
    this.WastDisposalstockcollectionView = [
      { name: 'WD Request Number', value: '' },
      { name: 'Warehouse', value: '' },
      { name: 'Created By', value: '' },
      { name: 'Created Date & Time', value: '' },
      { name: 'Actioned By', value: '' },
      { name: 'Actioned Date & Time', value: '' },
      { name: 'Waste Disposal Company', value: '' },
      { name: 'Collected By', value: '' },
      { name: 'Collected Date & Time', value: '' },
      { name: 'Status', value: '' }
    ]
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.wdRequestId) {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WASTE_DISPOSAL, this.wdRequestId, this.wdApprovalId)
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            this.wdRequestApprovalDetails = response.resources;
            this.wdRequestTabsDetails = response.resources.wasteDisposalDetails;
            this.WastDisposalstockcollectionView = [
              { name: 'WD Request Number', value: response.resources?.warehouseName },
              { name: 'Warehouse', value: response.resources?.warehouseName },
              { name: 'Created By', value: response.resources?.createdBy },
              { name: 'Created Date & Time', value: response.resources?.createdDate },
              { name: 'Actioned By', value: response.resources?.actionedBy },
              { name: 'Actioned Date & Time', value: response.resources?.actionedDate },
              { name: 'Waste Disposal Company', value: response.resources?.companyName },
              { name: 'Collected By', value: response.resources?.collectedBy },
              { name: 'Collected Date & Time', value: response.resources?.collectedDate },
              { name: 'Status', value: response.resources?.status, statusClass: response.resources?.cssClass }
            ]
            this.totalAssetWriteValue = 0;
            this.totalConsumeWriteValue = 0;
            if (response.resources.wasteDisposalDetails.assetWasteDisposalItems.length > 0) {
              response.resources.wasteDisposalDetails.assetWasteDisposalItems.forEach((asset) => {
                this.totalAssetWriteValue += asset.writeOffValue;
              });
            }
            if (response.resources.wasteDisposalDetails.consumableWasteDisposalItems.length > 0) {
              response.resources.wasteDisposalDetails.consumableWasteDisposalItems.forEach((consume) => {
                this.totalConsumeWriteValue += consume.writeOffValue;
              });
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          } else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.WASTE_DIPOSAL_COLLECTION];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToViewPage();
        break;
    }
  }

  onTabClicked(tabIndex) {
    this.selectedTabIndex = tabIndex.index;
  }

  openModelSerialPopup(type: any, itemId: any, itemCode: any, itemName: any) {
    let data = {};
    if (type == 'assets') {
      data = this.wdRequestApprovalDetails.wasteDisposalDetails.assetWasteDisposalItems.filter(i => {
        return i.itemId == itemId
      });
    }
    else {
      data = this.wdRequestApprovalDetails.wasteDisposalDetails.consumableWasteDisposalItems.filter(i => {
        return i.itemId == itemId
      });
    }
    this.modalDetails = {
      stockCode: itemCode,
      stockDescription: itemName,
      serialNumbers: data[0].wasteDisposalStocks
    }
    this.isWasteDisposalDialogModal = true;
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'waste-disposal', 'waste-disposal-stock'], { skipLocationChange: true });
  }

  navigateToViewPage() {
    this.router.navigate(['/inventory', 'waste-disposal', 'waste-disposal-stock', 'update'], {
      queryParams: {
        requestId: this.wdRequestId,
        approvalId: this.wdApprovalId,
      }, skipLocationChange: true
    });
  }

}
