import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';

@Component({
  selector: 'app-waste-disposal-imapproval-view',
  templateUrl: './waste-disposal-imapproval-view.component.html',
  styleUrls: ['./waste-disposal-imapproval-view.component.scss']
})
export class WasteDisposalImapprovalViewComponent implements OnInit {

  wdRequestId: any;
  wdApprovalId: any;
  wdRequestApprovalDetails: any;
  totalAssetWriteValue: any = 0;
  totalConsumeWriteValue: any = 0
  modalDetails: any = {};
  isWasteDisposalDialogModal: any;
  selectedTabIndex: any = 0;

  constructor(
    private router: Router, private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private snackbarService: SnackbarService
  ) {
    this.wdRequestId = this.activatedRoute.snapshot.queryParams.requestId;
    this.wdApprovalId = this.activatedRoute.snapshot.queryParams.approvalId;
  }

  ngOnInit(): void {
    this.getWDRequestApprovalById();
  }

  getWDRequestApprovalById() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WASTE_DISPOSAL_APPROVAL,
      this.wdApprovalId).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.wdRequestApprovalDetails = response.resources;
          this.totalAssetWriteValue = 0;
          this.totalConsumeWriteValue = 0;

          if (response.resources.assetItems.length > 0) {
            response.resources.assetItems.forEach((asset) => {
              this.totalAssetWriteValue += asset.writeOffValue;
            });
          }
          if (response.resources.consumableItems.length > 0) {
            response.resources.consumableItems.forEach((consume) => {
              this.totalConsumeWriteValue += consume.writeOffValue;
            });
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onTabClicked(tabIndex) {
    this.selectedTabIndex = tabIndex.index;
  }

  openModelSerialPopup(type: any, itemId: any, itemCode: any, itemName: any) {
    let data = {};
    if (type == 'assets') {
      data = this.wdRequestApprovalDetails.assetItems.filter(i => {
        return i.itemId == itemId
      });
    }
    else {
      data = this.wdRequestApprovalDetails.consumableItems.filter(i => {
        return i.itemId == itemId
      });
    }
    this.modalDetails = {
      stockCode: itemCode,
      stockDescription: itemName,
      serialNumbers: data[0].itemDetails
    }
    this.isWasteDisposalDialogModal = true;
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'waste-disposal', 'waste-disposal-imapproval'], { skipLocationChange: true });
  }

  navigateToViewPage() {
    this.router.navigate(['/inventory', 'waste-disposal', 'waste-disposal-imapproval', 'update'], {
      queryParams: {
        requestId: this.wdRequestId,
        approvalId: this.wdApprovalId,
      }, skipLocationChange: true
    });
  }

}
