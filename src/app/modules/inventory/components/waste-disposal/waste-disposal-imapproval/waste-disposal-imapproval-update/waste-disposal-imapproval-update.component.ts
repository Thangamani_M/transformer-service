import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { AssetConsumableItemsModel, WDIMApprovalUpdateModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-waste-disposal-imapproval-update',
  templateUrl: './waste-disposal-imapproval-update.component.html',
  styleUrls: ['./waste-disposal-imapproval-update.component.scss']
})
export class WasteDisposalImapprovalUpdateComponent implements OnInit {

  wdRequestId: any;
  wdApprovalId: any;
  wdRequestApprovalDetails: any;
  wasteDisposalAssetUpdateForm: FormGroup;
  wasteDisposalConsumeUpdateForm: FormGroup;
  assetItemsDetails: FormArray;
  consumableItemsDetails: FormArray;
  actionDropDown: any = [];
  companyDropDown: any = [];
  maxLevel: any;
  level: any;
  userData: UserLogin;
  isWDRequestApprovalDialog = false;
  showWDRequestApproval = false;
  wasteDisposalDislogForm: FormGroup;
  modalDetails: any = {};
  isWasteDisposalDialogModal: any;
  selectedTabIndex: any = 0;
  totalAssetWriteValue: any = 0;
  totalConsumeWriteValue: any = 0
  companyId: any = null;
  showCompanyError: boolean = false;
  companyErrorMsg: any;
  roleName: any;

  constructor(
    private router: Router, private crudService: CrudService, private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private snackbarService: SnackbarService, private dialog: MatDialog, private store: Store<AppState>,
  ) {

    this.wdRequestId = this.activatedRoute.snapshot.queryParams.requestId;
    this.wdApprovalId = this.activatedRoute.snapshot.queryParams.approvalId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.roleName = this.userData.roleName;
  }

  ngOnInit(): void {

    this.createwasteDisposalApprovalUpdateForm();
    this.getDropdown();
    if (this.wdRequestId) {
      this.getwdRequestApprovalDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.wdRequestApprovalDetails = response.resources;
          this.wasteDisposalAssetUpdateForm.patchValue(response.resources);
          this.wasteDisposalAssetUpdateForm.get('userId').patchValue(this.userData.userId)
          this.maxLevel = response.resources.maxLevel;
          this.level = response.resources.level;
          this.companyId = this.wdRequestApprovalDetails.companyId;
          this.assetItemsDetails = response.resources.assetItems;
          this.assetItemsDetails = this.getWasteDisposalAssetFormArray;
          this.consumableItemsDetails = response.resources.consumableItems;
          this.consumableItemsDetails = this.getWasteDisposalConsumeFormArray;

          this.totalAssetWriteValue = 0;
          this.totalConsumeWriteValue = 0;

          if (response.resources.assetItems.length > 0) {
            response.resources.assetItems.forEach((asset) => {
              this.totalAssetWriteValue += asset.writeOffValue;
              this.assetItemsDetails.push(this.createWasteDisposalGroupForm(asset));
            });
          }
          else {
            this.assetItemsDetails.push(this.createWasteDisposalGroupForm());
          }
          if (response.resources.consumableItems.length > 0) {
            response.resources.consumableItems.forEach((consume) => {
              this.totalConsumeWriteValue += consume.writeOffValue;
              this.consumableItemsDetails.push(this.createWasteDisposalGroupForm(consume));
            });
          }
          else {
            this.consumableItemsDetails.push(this.createWasteDisposalGroupForm());
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.consumableItemsDetails = this.getWasteDisposalConsumeFormArray;
          this.consumableItemsDetails.push(this.createWasteDisposalGroupForm());
          this.assetItemsDetails = this.getWasteDisposalAssetFormArray;
          this.assetItemsDetails.push(this.createWasteDisposalGroupForm());
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  //Get Details
  getwdRequestApprovalDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WASTE_DISPOSAL_APPROVAL,
      this.wdApprovalId
    );
  }

  getDropdown(): void {
    /* Action dropdown */
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WASTE_DISPOSAL_ACTION_TYPES)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.actionDropDown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    /* Company dropdown */
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WASTE_DISPOSAL_COMPANIES)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.companyDropDown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  /* Create formArray start */
  createwasteDisposalApprovalUpdateForm(): void {
    /*Assets Tab*/
    let stockOrderModel = new WDIMApprovalUpdateModel();
    this.wasteDisposalAssetUpdateForm = this.formBuilder.group({
      assetItemsDetails: this.formBuilder.array([]),
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.wasteDisposalAssetUpdateForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    /*Consumable Tab*/
    let stockOrderModelss = new WDIMApprovalUpdateModel();
    this.wasteDisposalConsumeUpdateForm = this.formBuilder.group({
      consumableItemsDetails: this.formBuilder.array([])
    });
    Object.keys(stockOrderModelss).forEach((key) => {
      this.wasteDisposalConsumeUpdateForm.addControl(key, new FormControl(stockOrderModelss[key]));
    });
    /*Dialog form*/
    this.wasteDisposalDislogForm = this.formBuilder.group({
      reasons: ['', Validators.required]
    });
  }

  get getWasteDisposalConsumeFormArray(): FormArray {
    if (this.wasteDisposalConsumeUpdateForm !== undefined) {
      return this.wasteDisposalConsumeUpdateForm.get("consumableItemsDetails") as FormArray;
    }
  }

  get getWasteDisposalAssetFormArray(): FormArray {
    if (this.wasteDisposalAssetUpdateForm !== undefined) {
      return this.wasteDisposalAssetUpdateForm.get("assetItemsDetails") as FormArray;
    }
  }

  /* Create FormArray controls */
  createWasteDisposalGroupForm(approvalDetails?: AssetConsumableItemsModel): FormGroup {
    let structureTypeData = new AssetConsumableItemsModel(approvalDetails ? approvalDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      if (this.level == 1) {
        formControls[key] = [{ value: structureTypeData[key], disabled: false },
        (key === 'level1ActionId') ? [Validators.required] : []]
      }
      if (this.level == 2) {
        formControls[key] = [{ value: structureTypeData[key], disabled: false },
        (key === 'level2ActionId') ? [Validators.required] : []]
      }
    });
    return this.formBuilder.group(formControls);
  }

  selectAll(checked: boolean) {
    if (this.selectedTabIndex == 0) {
      this.getWasteDisposalAssetFormArray.controls.forEach(control => {
        checked ? control.get('isActive').setValue(true) : control.get('isActive').setValue(false);
      });
    }
    else {
      this.getWasteDisposalConsumeFormArray.controls.forEach((controls) => {
        checked ? controls.get('isActive').setValue(true) : controls.get('isActive').setValue(false);
      });
    }
  }

  submitFeedback(type: any) {

    if (type == 'rejected' && this.wasteDisposalDislogForm.invalid) {
      return;
    }
    let reasons = this.wasteDisposalDislogForm.get('reasons').value;
    let obj = this.actionDropDown.filter(data => {
      if (data.displayName == 'Approved') {
        return data.id;
      }
    });

    if (this.selectedTabIndex == 0) {
      this.getWasteDisposalAssetFormArray.value.forEach((key, index) => {
        if (key.isActive && obj.length > 0) {
          if (this.level == '1') {
            type == 'approved' ? this.getWasteDisposalAssetFormArray.controls[index].get('level1ActionId').patchValue(obj[0].id) :
              this.getWasteDisposalAssetFormArray.controls[index].get('level1Reason').patchValue(reasons);
          }
          if (this.level == '2') {
            type == 'approved' ? this.getWasteDisposalAssetFormArray.controls[index].get('level2ActionId').patchValue(obj[0].id) :
              this.getWasteDisposalAssetFormArray.controls[index].get('level2Reason').patchValue(reasons);
          }
        }
      });
    }
    else {
      this.getWasteDisposalConsumeFormArray.value.forEach((con, i) => {
        if (con.isActive && obj.length > 0) {
          if (this.level == '1') {
            type == 'approved' ? this.getWasteDisposalConsumeFormArray.controls[i].get('level1ActionId').patchValue(obj[0].id) :
              this.getWasteDisposalConsumeFormArray.controls[i].get('level1Reason').patchValue(reasons);
          }
          if (this.level == '2') {
            type == 'approved' ? this.getWasteDisposalConsumeFormArray.controls[i].get('level2ActionId').patchValue(obj[0].id) :
              this.getWasteDisposalConsumeFormArray.controls[i].get('level2Reason').patchValue(reasons);
          }
        }
      });
    }
    this.isWDRequestApprovalDialog = false;
  }

  feedbackMethod(type: any) {
    let activeVal = false;
    if (this.selectedTabIndex == 0) {
      this.getWasteDisposalAssetFormArray.value.forEach((asset) => {
        if (asset.isActive) {
          activeVal = true;
        }
      });
    }
    else {
      this.getWasteDisposalConsumeFormArray.value.forEach((consume) => {
        if (consume.isActive) {
          activeVal = true;
        }
      });
    }
    if (!activeVal) {
      this.isWDRequestApprovalDialog = false;
      this.snackbarService.openSnackbar('Atleast one checkbox is required', ResponseMessageTypes.ERROR);
      return;
    }
    else {
      if (type == 'rejected') {
        this.isWDRequestApprovalDialog = true;
        this.showWDRequestApproval = false;
      }
      else {
        this.submitFeedback('approved')
      }
    }
  }

  selectAction(selectvalue: any, index: number, colName: any) {

    let obj = this.actionDropDown.filter(data => {
      if (data.id == selectvalue) {
        return true
      }
    })
    if (obj.length > 0) {
      if (obj[0].displayName == 'Rejected' || obj[0].displayName == 'Cancelled') {
        if (this.selectedTabIndex == 0) {
          this.getWasteDisposalAssetFormArray.controls[index].get(colName).setValidators([Validators.required]);
          this.getWasteDisposalAssetFormArray.controls[index].get(colName).updateValueAndValidity();
        }
        else {
          this.getWasteDisposalConsumeFormArray.controls[index].get(colName).setValidators([Validators.required]);
          this.getWasteDisposalConsumeFormArray.controls[index].get(colName).updateValueAndValidity();
        }
      }
      else {
        if (this.selectedTabIndex == 0) {
          this.getWasteDisposalAssetFormArray.controls[index].get(colName).setErrors(null);
          this.getWasteDisposalAssetFormArray.controls[index].get(colName).clearValidators();
          this.getWasteDisposalAssetFormArray.controls[index].markAllAsTouched();
          this.getWasteDisposalAssetFormArray.controls[index].get(colName).updateValueAndValidity();
        }
        else {
          this.getWasteDisposalConsumeFormArray.controls[index].get(colName).setErrors(null);
          this.getWasteDisposalConsumeFormArray.controls[index].get(colName).clearValidators();
          this.getWasteDisposalConsumeFormArray.controls[index].markAllAsTouched();
          this.getWasteDisposalConsumeFormArray.controls[index].get(colName).updateValueAndValidity();
        }
      }
    }
  }

  onTabClicked(tabIndex) {
    this.selectedTabIndex = tabIndex.index;
  }

  openModelSerialPopup(type: any, itemId: any, itemCode: any, itemName: any) {
    let data = {};
    if (type == 'assets') {
      data = this.wdRequestApprovalDetails.assetItems.filter(i => {
        return i.itemId == itemId;
      });
    }
    else {
      data = this.wdRequestApprovalDetails.consumableItems.filter(i => {
        return i.itemId == itemId;
      });
    }
    this.modalDetails = {
      stockCode: itemCode,
      stockDescription: itemName,
      serialNumbers: data[0].itemDetails
    }
    this.isWasteDisposalDialogModal = true;
  }

  onSubmit(): void {

    this.rxjsService.setFormChangeDetectionProperty(true);
    this.showCompanyError = false;
    this.companyErrorMsg = '';

    if (this.selectedTabIndex == 0) {
      if (this.getWasteDisposalAssetFormArray.invalid) {
        return;
      }
    }
    else {
      if (this.getWasteDisposalConsumeFormArray.invalid) {
        return;
      }
    }

    if (this.companyId == null || this.companyId == '') {
      this.showCompanyError = true;
      this.companyErrorMsg = 'Company is required';
      return;
    }

    let formArray = [];
    this.getWasteDisposalAssetFormArray.value.forEach(assets => {
      formArray.push(assets);
    });

    this.getWasteDisposalConsumeFormArray.value.forEach(consume => {
      formArray.push(consume);
    });

    let approvalData = {};
    approvalData = {
      WDRequestId: this.wasteDisposalAssetUpdateForm.get('wdRequestId').value,
      WDCompanyId: this.companyId,
      WDRequestApprovalId: this.wasteDisposalAssetUpdateForm.get('wdRequestApprovalId').value,
      Level: this.wasteDisposalAssetUpdateForm.get('level').value,
      UserId: this.wasteDisposalAssetUpdateForm.get('userId').value,
      ApprovalDetails: formArray
    }


    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WASTE_DISPOSAL_APPROVAL, approvalData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'waste-disposal', 'waste-disposal-imapproval'], { skipLocationChange: true });
  }

  navigateToViewPage() {
    this.router.navigate(['/inventory', 'waste-disposal', 'waste-disposal-imapproval', 'view'], {
      queryParams: {
        requestId: this.wdRequestId,
        approvalId: this.wdApprovalId,
      }, skipLocationChange: true
    });
  }

}