import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService } from '@app/shared';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { SystemCheckerModel } from '@modules/inventory/models/system-checker.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-waste-disposal-view',
  templateUrl: './waste-disposal-view.component.html',
  styleUrls: ['./radio-system-checker.component.scss']
})
export class WasteDisposalviewComponent implements OnInit {
  itemId: string;
  systemCheckerRequestModel: any = {};
  systemChekerForm: FormGroup;
  loggedUser: UserLogin;
  selectedTabIndex = 0;

  @ViewChild(SignaturePad, { static: false }) signaturePad: any;

  signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 500,
    'canvasHeight': 80
  };
  constructor(private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private router: Router,
    private formBuilder: FormBuilder,
    private crudService: CrudService,
    private store: Store<AppState>) {
    this.itemId = this.activatedRoute.snapshot.queryParams.id
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createSystemCheckerForm();
    if (this.itemId) {
      this.getWDInventoryManagerById(this.itemId);
    }
  }

  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.resizeCanvas();
    this.signaturePad.clear();
  }



  clearPad() {
    this.signaturePad.clear();
  }

  getWDInventoryManagerById(id: string) {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WASTE_DISPOSAL_APPROVAL_VIEW, id, true, null)
      .subscribe(
        response => {
          if (response.isSuccess && response.statusCode === 200) {
            let systemCheckerModelData = new SystemCheckerModel(response.resources);
            this.systemCheckerRequestModel = response.resources;
            this.systemChekerForm.setValue(systemCheckerModelData);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
  }

  createSystemCheckerForm(systemCheckerModelData?: SystemCheckerModel): void {
    let systemCheckerModel = new SystemCheckerModel(systemCheckerModelData);
    this.systemChekerForm = this.formBuilder.group({});
    Object.keys(systemCheckerModel).forEach((key) => {
      this.systemChekerForm.addControl(key, new FormControl(systemCheckerModel[key]));
    });
  }

  onSubmit() {
    const wDRequestApprovedDTO = {
      wdRequestId: this.systemCheckerRequestModel['wdRequestId'],
      supplierName: this.systemChekerForm.get('supplierName').value,
      checkerId: this.loggedUser.userId
    }

    const signatureFormData = new FormData();
    const item_image = this.dataURLToBlob(this.signaturePad.toDataURL());
    signatureFormData.append('wDRequestApprovedDTO', JSON.stringify(wDRequestApprovedDTO));
    signatureFormData.append('file', item_image);
    this.crudService
      .create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WASTE_DISPOSAL_APPROVAL_SUBMIT,
        signatureFormData
      )
      .pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
      .subscribe((response) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.naviagteToList();
        }
      })
  }

  dataURLToBlob(dataURL) {
    // https://github.com/szimek/signature_pad/blob/gh-pages/js/app.js
    var parts = dataURL.split(';base64,');
    var contentType = parts[0].split(":")[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;
    var uInt8Array = new Uint8Array(rawLength);
    for (var i = 0; i < rawLength; ++i) {
      uInt8Array[i] = raw.charCodeAt(i);
    }
    return new Blob([uInt8Array], { type: contentType });
  }

  naviagteToList() {
    this.router.navigate(['/inventory', 'waste-disposal', 'list']);
  }

}
