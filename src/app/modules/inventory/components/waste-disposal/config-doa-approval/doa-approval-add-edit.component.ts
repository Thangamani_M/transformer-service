import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';

@Component({
  selector: 'app-doa-approval-add-edit',
  templateUrl: './doa-approval-add-edit.component.html',
  styleUrls: ['./doa-approval.component.scss']
})
export class DOAApprovalAddEditComponent implements OnInit {

  doaConfigId: any;
  configDOAAddEditForm: FormGroup;
  constructor(private activatedRoute: ActivatedRoute, private crudService: CrudService,
    private rxjsService: RxjsService) {
    this.doaConfigId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit() {
    if (this.doaConfigId) {
      this.getDOAConfigDetail().subscribe((responseConfigCompany: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
    else {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }

  getDOAConfigDetail() {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WASTE_DISPOSAL_COMPANIES,
      this.doaConfigId
    );
  }
}
