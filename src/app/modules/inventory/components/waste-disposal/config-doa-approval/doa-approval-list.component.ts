import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RolesAndPermissionsObj, TabsList } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-doa-approval-list',
  templateUrl: './doa-approval-list.component.html',
  styleUrls: ['./doa-approval.component.scss']
})
export class DOAApprovalListComponent extends PrimeNgTableVariablesModel implements OnInit {

  observableResponse;
  userData: UserLogin;
  primengTableConfigProperties: any;
  row: any = {};
  first: any = 0;
  reset: boolean;
  listSubscribtion: any;

  constructor(
    private activatedRoute: ActivatedRoute, private router: Router,
    private datePipe: DatePipe,
    private crudService: CrudService, private store: Store<AppState>,
    private rxjsService: RxjsService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "DOA Configuration",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'DOA Configuration' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'DOA Configuration',
            enableBreadCrumb: true,
            checkBox: false,
            enableFieldsSearch: true,
            columns: [
              { field: 'configID', header: 'Config ID', width: '170px' },
              { field: 'configType', header: 'Config Type', width: '150px' },
              { field: 'levels', header: 'No of Levels', width: '200px' },
              { field: 'createdBy', header: 'Created By', width: '90px' },
              { field: 'createdDate', header: 'created Date', width: '100px' },
              { field: 'status', header: 'status', width: '100px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.WASTE_DISPOSAL_COMPANIES,
            moduleName: ModulesBasedApiSuffix.INVENTORY,

          },
        ]
      }
    }

  }

  ngOnInit() {
    this.getDOAConfiguration();
    this.rxjsService.getRolesAndPermissionsProperty().subscribe((rolesAndPermissionsObj: RolesAndPermissionsObj) => {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList.forEach((tabObj: TabsList) => {
        tabObj.rolesAndPermissionsObj = rolesAndPermissionsObj;
      })
    })
  }

  getDOAConfiguration(pageIndex?: string, pageSize?: string, searchKey?: any) {
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    if (typeof searchKey == 'object') {
      searchKey = { ...searchKey }
    }
    this.listSubscribtion = this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WASTE_DISPOSAL_COMPANIES,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, searchKey)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.createdDate = this.datePipe.transform(val?.createdDate, 'dd-MM-yyyy, h:mm:ss a');
          return val;
        })
        return res;
      }
    })).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;
        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        this.getDOAConfiguration(this.row["pageIndex"], this.row["pageSize"], unknownVar);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(["inventory/waste-disposal/doa-apporval-add-edit"], { skipLocationChange: true });
        break;
      case CrudType.EDIT:
        this.router.navigate(["inventory/waste-disposal/doa-apporval-add-edit"], { queryParams: { id: editableObject['companyId'] }, skipLocationChange: true });
        break;
    }
  }

  ngOnDestroy() {
    if (this.listSubscribtion) {
      this.listSubscribtion.unsubscribe();
    }
  }

}
