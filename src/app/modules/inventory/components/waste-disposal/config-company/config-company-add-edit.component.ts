import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, RxjsService, setRequiredValidator, prepareDynamicTableTabsFromPermissions, LoggedInUserModel, SnackbarService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes } from '@app/shared';
import { WasteDisposalCompanyAddEditModel } from '@modules/inventory/models/waste-disposal-company.model';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@app/modules';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-config-company-add-edit',
  templateUrl: './config-company-add-edit.component.html',
  styleUrls: ['./config-company.component.scss']
})
export class ConfigCompanyAddEditComponent implements OnInit {

  configCompanyId: any;
  configType: any;
  configCompanyDetail: any;
  wdCompanyAddEditForm: FormGroup;
  userData: UserLogin;
  ConfigCompanyviewDetail: any;
  loggedInUserData;
  primengTableConfigProperties;
  selectedTabIndex = 0;
  constructor(private snackbarService: SnackbarService,
    private formBuilder: FormBuilder, private crudService: CrudService, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private router: Router, private rxjsService: RxjsService) {
    this.configCompanyId = this.activatedRoute.snapshot.queryParams.id;
    this.configType = this.activatedRoute.snapshot.queryParams.type;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Company Configuration",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Company Configuration', relativeRouterUrl: '' },
      { displayName: 'Waste Disposal', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Company Configuration',
            dataKey: 'cycleCountId',
            enableAction: true,
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enablePrintBtn: true,
            printTitle: 'Company Configuration',
            printSection: 'print-section0',
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableAddActionBtn: true,
            enableExportBtn: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.WASTE_DISPOSAL_COMPANIES,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.createWDCompanyAddEditForm();
    if (this.configCompanyId) {
      this.getConfigCompanyDetail().subscribe((responseConfigCompany: IApplicationResponse) => {
        this.configCompanyDetail = responseConfigCompany.resources;
        this.onShowValue(responseConfigCompany.resources);
        this.wdCompanyAddEditForm.patchValue(responseConfigCompany.resources);
        this.wdCompanyAddEditForm.get('mobile').patchValue(responseConfigCompany.resources.mobileNumber);
        this.wdCompanyAddEditForm.get('modifiedUserId').patchValue(this.userData.userId);
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
    else {
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.COMPANY_CONFIGURATION]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onShowValue(response?: any) {
    this.ConfigCompanyviewDetail = [
      { name: 'Company Name', value: response?.companyName },
      { name: 'Company Code', value: response?.companyCode },
      { name: 'Contact Person ', value: response?.contactPerson },
      { name: 'Contact Number ', value: response?.contactNumber },
      { name: 'Email Address ', value: response?.email },
      { name: 'Mobile Number ', value: response?.mobileNumber },
      { name: 'Status ', value: response?.status, statusClass: response ? response?.cssClass : '' },
    ]
  }

  getConfigCompanyDetail() {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WASTE_DISPOSAL_COMPANIES,
      this.configCompanyId
    );
  }

  createWDCompanyAddEditForm(): void {
    let stockOrderModel = new WasteDisposalCompanyAddEditModel();
    // create form controls dynamically from model class
    this.wdCompanyAddEditForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.wdCompanyAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.wdCompanyAddEditForm.get('createdUserId').patchValue(this.userData.userId);
    this.wdCompanyAddEditForm = setRequiredValidator(this.wdCompanyAddEditForm, ['companyName', 'companyCode', 'title', 'firstName', 'surName']);
  }

  submit(): void {
    if (this.wdCompanyAddEditForm.invalid) {
      return;
    }
    const submit$ = this.configCompanyId ? this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WASTE_DISPOSAL_COMPANIES, this.wdCompanyAddEditForm.value
    ) : this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WASTE_DISPOSAL_COMPANIES, this.wdCompanyAddEditForm.value
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList('List');
      }
    });
  }

  navigateToList(type: any): void {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    if (type == 'List') {
      this.router.navigate(['/inventory', 'waste-disposal', 'company-addition-list'], { skipLocationChange: true });
    }
    else {
      this.configType = type;
    }
  }

}
