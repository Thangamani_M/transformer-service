import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { setRequiredValidator } from '@app/shared';
import { WasteDisposalRequestDetailsModel } from '@modules/inventory/models';
import { DialogService } from 'primeng/components/dynamicdialog/dialogservice';
import { StockInfoDialogComponent } from '../stock-info-dialog';

@Component({
  selector: 'app-request-details-item',
  templateUrl: './request-details-item.component.html',
  styleUrls: ['./request-details-item.component.scss']
})
export class RequestDetailsItemComponent implements OnInit {

  deletedId: any = [];
  @Input() requestFormGroup: FormGroup;
  @Input() showAction: any;
  @Input() wdRequestId: any;
  @Input() viewable: any;
  @Input() selectedIndex: any;
  @Input() wasteDisposalDetails: any;
  @Input() isLoading: boolean;
  @Output() onQuantitySubmitted = new EventEmitter<any>();
  @Output() onStockInfoSubmitted = new EventEmitter<any>();
  @Output() itemDeleteSubmitted = new EventEmitter<any>();
  totalWriteValue: any = 0;

  constructor(private formBuilder: FormBuilder, public dialogService: DialogService,) { }

  ngOnInit(): void {

  }

  ngOnChanges() {
    this.totalWriteValue = 0;
    this.onTabChange();
  }

  onTabChange() {
    switch (this.selectedIndex) {
      case 0:
        if (this.wasteDisposalDetails?.assetWasteDisposalItems?.length > 0) {
          this.wasteDisposalDetails?.assetWasteDisposalItems?.forEach(el => {
            this.onLoadFormArray(el);
          });
        }
        break;
      case 1:
        if (this.wasteDisposalDetails?.consumableWasteDisposalItems?.length > 0) {
          this.wasteDisposalDetails?.consumableWasteDisposalItems?.forEach(el => {
            this.onLoadFormArray(el);
          });
        }
        break;
      default:
        break;
    }
  }

  onLoadFormArray(el) {
    if (el) {
      this.totalWriteValue += el.writeOffValue;
      if (!this.viewable) {
        this.initFormArray({
          stockCode: el?.itemCode,
          stockDescription: el?.itemName,
          quantity: el?.quantity,
          movingAveragePrice: el?.movingAveragePrice,
          writeOffValue: el?.writeOffValue,
        });
      } else if (this.viewable) {
        this.initFormArray({
          stockCode: el?.itemCode,
          stockDescription: el?.itemName,
          quantity: el?.quantity,
          movingAveragePrice: el?.movingAveragePrice,
          writeOffValue: el?.writeOffValue,
          level1Action: el?.level1Action ? el?.level1Action : '-',
          level1Reason: el?.level1Action ? el?.level1Action : '-',
          level2Action: el?.level2Action ? el?.level2Action : '-',
          level2Reason: el?.level2Action ? el?.level2Action : '-',
          level3Action: el?.level3Action ? el?.level3Action : '-',
          level3Reason: el?.level3Action ? el?.level3Action : '-',
          level4Action: el?.level4Action ? el?.level4Action : '-',
          level4Reason: el?.level4Action ? el?.level4Action : '-',
          level5Action: el?.level5Action ? el?.level5Action : '-',
          level5Reason: el?.level5Action ? el?.level5Action : '-',
          level6Action: el?.level6Action ? el?.level6Action : '-',
          level6Reason: el?.level6Action ? el?.level6Action : '-',
          level7Action: el?.level7Action ? el?.level7Action : '-',
          level7Reason: el?.level7Action ? el?.level7Action : '-',
          level8Action: el?.level8Action ? el?.level8Action : '-',
          level8Reason: el?.level8Action ? el?.level8Action : '-',
          level9Action: el?.level9Action ? el?.level9Action : '-',
          level9Reason: el?.level9Action ? el?.level9Action : '-',
          level10Action: el?.level10Action ? el?.level10Action : '-',
          level10Reason: el?.level10Action ? el?.level10Action : '-',
        });
      }
    }
  }

  initFormArray(wasteDisposalRequestDetailsModel?: WasteDisposalRequestDetailsModel) {
    let requestDetailsModel = new WasteDisposalRequestDetailsModel(wasteDisposalRequestDetailsModel);
    let requestFormGroup = this.formBuilder.group({});
    Object.keys(requestDetailsModel).forEach((key) => {
      requestFormGroup.addControl(key, new FormControl({ value: requestDetailsModel[key], disabled: true }));
    });
    if (this.selectedIndex == 1 && !this.viewable) {
      requestFormGroup.get('quantity').enable();
      requestFormGroup = setRequiredValidator(requestFormGroup, ["quantity"]);
    }
    if (!this.viewable) {
      requestFormGroup.get('selectedFlag').enable();
    }
    this.getRequestFormArray.push(requestFormGroup);
  }

  get getRequestFormArray(): FormArray {
    if (!this.requestFormGroup) return;
    return this.requestFormGroup.get('requestDetailsArray') as FormArray;
  }


  isCheckAll() {
    const isMasterSel = this.getRequestFormArray.value.every(function (item: any) {
      return item.selectedFlag == true;
    });
    this.requestFormGroup.get('checkAll').setValue(isMasterSel);
  }

  isAllSelected(i) {
    this.isCheckAll();
    if (this.selectedIndex == 0) {
      if (this.wasteDisposalDetails?.assetWasteDisposalItems?.length > 0) {
        this.wasteDisposalDetails.assetWasteDisposalItems[i].selectedFlag = this.getRequestFormArray.value[i].selectedFlag;
        if (this.getRequestFormArray.value[i].selectedFlag && this.wdRequestId) {
          this.deletedId.push(this.wasteDisposalDetails.assetWasteDisposalItems[i].wdRequestItemId)
        } else if (!this.getRequestFormArray.value[i].selectedFlag && this.wdRequestId) {
          this.deletedId.splice((this.deletedId).indexOf(this.wasteDisposalDetails.assetWasteDisposalItems[i].wdRequestItemId), 1);
        } else if (this.getRequestFormArray.value[i].selectedFlag && !this.wdRequestId) {
          this.deletedId.push(this.wasteDisposalDetails.assetWasteDisposalItems[i].itemId)
        } else if (!this.getRequestFormArray.value[i].selectedFlag && !this.wdRequestId) {
          this.deletedId.splice((this.deletedId).indexOf(this.wasteDisposalDetails.assetWasteDisposalItems[i].itemId), 1);
        }
      }
    } else if (this.selectedIndex == 1) {
      if (this.wasteDisposalDetails?.consumableWasteDisposalItems?.length > 0) {
        this.wasteDisposalDetails.consumableWasteDisposalItems[i].selectedFlag = this.getRequestFormArray.value[i].selectedFlag;
        if (this.getRequestFormArray.value[i].selectedFlag && this.wdRequestId) {
          this.deletedId.push(this.wasteDisposalDetails.consumableWasteDisposalItems[i].wdRequestItemId)
        } else if (!this.getRequestFormArray.value[i].selectedFlag && this.wdRequestId) {
          this.deletedId.splice((this.deletedId).indexOf(this.wasteDisposalDetails.consumableWasteDisposalItems[i].wdRequestItemId), 1);
        } else if (this.getRequestFormArray.value[i].selectedFlag && !this.wdRequestId) {
          this.deletedId.push(this.wasteDisposalDetails.consumableWasteDisposalItems[i].itemId)
        } else if (!this.getRequestFormArray.value[i].selectedFlag && !this.wdRequestId) {
          this.deletedId.splice((this.deletedId).indexOf(this.wasteDisposalDetails.consumableWasteDisposalItems[i].itemId), 1);
        }
      }
    }
    this.itemDeleteSubmitted.emit(this.deletedId);
  }


  checkAll(event) {
    let formArray = this.getRequestFormArray;
    if (event.checked) {
      formArray['controls'].map(element => element.get('selectedFlag').setValue(true));
      if (this.selectedIndex == 0) {
        if (this.wasteDisposalDetails?.assetWasteDisposalItems?.length > 0) {
          this.wasteDisposalDetails?.assetWasteDisposalItems?.map(el => {
            if (!el.selectedFlag) {
              el.selectedFlag = true;
              if (el?.wdRequestItemDetailId) {
                this.deletedId.push(el?.wdRequestItemDetailId);
              } else if (el?.itemId) {
                this.deletedId.push(el?.itemId);
              }
            }
            return el;
          });
        }
      } else if (this.selectedIndex == 1) {
        if (this.wasteDisposalDetails?.consumableWasteDisposalItems?.length > 0) {
          this.wasteDisposalDetails?.consumableWasteDisposalItems?.map(el => {
            if (!el.selectedFlag) {
              el.selectedFlag = true;
              if (el?.wdRequestItemDetailId) {
                this.deletedId.push(el?.wdRequestItemDetailId);
              } else if (el?.itemId) {
                this.deletedId.push(el?.itemId);
              }
            }
            return el;
          });
        }
      }
    } else {
      formArray['controls'].map(element => element.get('selectedFlag').setValue(false));
      if (this.selectedIndex == 0) {
        if (this.wasteDisposalDetails?.assetWasteDisposalItems?.length > 0) {
          this.wasteDisposalDetails?.assetWasteDisposalItems?.map(el => {
            if (el.selectedFlag) {
              el.selectedFlag = false;
              if (el?.wdRequestItemDetailId) {
                this.deletedId.splice(this.deletedId.indexOf(el?.wdRequestItemDetailId), 1);
              } else if (el?.itemId) {
                this.deletedId.splice(this.deletedId.indexOf(el?.itemId), 1);
              }
            }
            return el;
          });
        }
      } else if (this.selectedIndex == 1) {
        if (this.wasteDisposalDetails?.consumableWasteDisposalItems?.length > 0) {
          this.wasteDisposalDetails?.consumableWasteDisposalItems?.map(el => {
            if (el.selectedFlag) {
              el.selectedFlag = false;
              if (el?.wdRequestItemDetailId) {
                this.deletedId.splice(this.deletedId.indexOf(el?.wdRequestItemDetailId), 1);
              } else if (el?.itemId) {
                this.deletedId.splice(this.deletedId.indexOf(el?.itemId), 1);
              }
            }
            return el;
          });
        }
      }
    }
    this.itemDeleteSubmitted.emit(this.deletedId);
  }

  ValidateReqQty(ReqQty, i) {
    if (!ReqQty) {
      this.getRequestFormArray.at(i).patchValue({
        writeOffValue: 0
      });
      return;
    }
    this.totalWriteValue = 0;
    this.getRequestFormArray.controls.forEach((el: any, id) => {
      if (id == i) {
        el.controls.writeOffValue.value = el?.controls.movingAveragePrice.value * parseInt(ReqQty);
        this.wasteDisposalDetails.consumableWasteDisposalItems[id].quantity = ReqQty;
        this.wasteDisposalDetails.consumableWasteDisposalItems[id].writeOffValue = this.wasteDisposalDetails.consumableWasteDisposalItems[id].quantity * this.wasteDisposalDetails.consumableWasteDisposalItems[id].movingAveragePrice;
      }
      this.totalWriteValue += el.controls.writeOffValue.value;
    })
    this.getRequestFormArray.at(i).patchValue({
      writeOffValue: this.wasteDisposalDetails.consumableWasteDisposalItems[i].writeOffValue
    });
    if (this.selectedIndex == 1) {
      this.onQuantitySubmitted.emit(this.wasteDisposalDetails.consumableWasteDisposalItems);
    }
  }

  numericOnly(event): boolean {
    let patt = /^([0-9])$/;
    let result = patt.test(event.key);
    return result;
  }

  removeKitItem(i) {
    let deleteId = this.wasteDisposalDetails?.assetWasteDisposalItems[i].wdRequestItemId;
    if (this.selectedIndex == 0) {
      this.wasteDisposalDetails?.assetWasteDisposalItems.splice(i, 1);
    } else if (this.selectedIndex == 1) {
      this.wasteDisposalDetails?.consumableWasteDisposalItems.splice(i, 1);
    }
    this.getRequestFormArray.removeAt(i);
    if (deleteId) {
      this.itemDeleteSubmitted.emit(deleteId);
    }
  }

  onOpenStockInfo(i) {
    if (!this.viewable && this.selectedIndex == 0) {
      let rowData;
      if (this.selectedIndex == 0) {
        rowData = { id: i, header: "Stock Info", data: this.wasteDisposalDetails?.assetWasteDisposalItems, tab: this.selectedIndex };
      } else if (this.selectedIndex == 1) {
        rowData = { id: i, header: "Stock Info", data: this.wasteDisposalDetails?.consumableWasteDisposalItems, tab: this.selectedIndex };
      }
      if (i == null || i == undefined || !rowData) {
        return;
      }
      const ref = this.dialogService.open(StockInfoDialogComponent, {
        showHeader: false,
        baseZIndex: 1000,
        width: '660px',
        data: rowData,
      });
      ref.onClose.subscribe((result) => {
        if (result) {
          if (result?.tab == 0) {
            this.wasteDisposalDetails.assetWasteDisposalItems = result?.data;
            this.getRequestFormArray.controls[result.id].get('quantity').setValue(this.wasteDisposalDetails.assetWasteDisposalItems[result.id].quantity);
            this.getRequestFormArray.controls[result.id].get('writeOffValue').setValue(this.wasteDisposalDetails.assetWasteDisposalItems[result.id].writeOffValue);
          } else if (result?.tab == 1) {
            this.wasteDisposalDetails.consumableWasteDisposalItems = result?.data;
          }
          this.onStockInfoSubmitted.emit(result);
        }
      });
    }
  }
}
