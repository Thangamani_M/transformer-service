import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-stock-info-dialog',
  templateUrl: './stock-info-dialog.component.html',
  styleUrls: ['./stock-info-dialog.component.scss']
})
export class StockInfoDialogComponent implements OnInit {

  stockInfoForm: FormGroup;
  orgConfig: any; //dont delete this for deep copy

  constructor(private rxjsService: RxjsService, public config: DynamicDialogConfig, public ref: DynamicDialogRef, private snackbarService: SnackbarService) {
    this.rxjsService.setDialogOpenProperty(true);
    this.orgConfig = JSON.parse(JSON.stringify(this.config.data)); //dont delete this for deep copy
  }

  ngOnInit(): void {
    this.initForm();
  }

  btnCloseClick() {
    this.ref.close(this.orgConfig); //dont delete this for deep copy
  }

  initForm() {
    this.stockInfoForm = new FormGroup({
      checkAll: new FormControl(),
      stockInfoArray: new FormArray([])
    })
    if (this.config.data?.data[this.config?.data?.id]?.wasteDisposalStocks?.length > 0) {
      this.config.data?.data[this.config.data?.id]?.wasteDisposalStocks.forEach(el => {
        this.initFormArray(el);
      });
      this.isStockCheckAll();
    }
  }

  initFormArray(el) {
    this.getStockInfoArray.push(new FormGroup({
      selectedFlag: new FormControl(el.selectedFlag),
      serialNumber: new FormControl({ value: el.serialNumber, disabled: true }),
      referrenceNumber: new FormControl({ value: el?.referenceNumber, disabled: true }),
      comments: new FormControl({ value: el?.comment, disabled: true }),
    }))
  }

  get getStockInfoArray() {
    if (!this.stockInfoForm) return;
    return this.stockInfoForm.get('stockInfoArray') as FormArray;
  }

  isStockCheckAll() {
    const isMasterSel = this.getStockInfoArray.value.every(function (item: any) {
      return item.selectedFlag == true;
    });
    this.stockInfoForm.get('checkAll').setValue(isMasterSel);
  }

  isAllStockSelected(i) {
    this.isStockCheckAll();
    this.config.data.data[this.config?.data?.id].wasteDisposalStocks[i].selectedFlag = this.getStockInfoArray.value[i].selectedFlag;
    this.config.data.data[this.config?.data?.id].quantity = this.getStockInfoArray.value[i].selectedFlag ? this.config.data.data[this.config?.data?.id].quantity + 1 : this.config.data.data[this.config?.data?.id].quantity - 1;
    this.config.data.data[this.config?.data?.id].writeOffValue = this.config.data.data[this.config?.data?.id].quantity * this.config.data.data[this.config?.data?.id].movingAveragePrice;
  }

  stockCheckAll(event) {
    let formArray = this.getStockInfoArray;
    if (event.checked) {
      formArray['controls'].map(element => element.get('selectedFlag').setValue(true));
      this.config.data.data[this.config?.data?.id]?.wasteDisposalStocks?.map(el => {
        if (!el.selectedFlag) {
          el.selectedFlag = true;
          this.config.data.data[this.config?.data?.id].quantity = this.config.data.data[this.config?.data?.id].quantity + 1;
          this.config.data.data[this.config?.data?.id].writeOffValue = this.config.data.data[this.config?.data?.id].quantity * this.config.data.data[this.config?.data?.id].movingAveragePrice;
        }
        return el;
      });

    } else {
      formArray['controls'].map(element => element.get('selectedFlag').setValue(false));
      this.config.data.data[this.config?.data?.id]?.wasteDisposalStocks?.map(el => {
        if (el.selectedFlag) {
          el.selectedFlag = false;
          this.config.data.data[this.config?.data?.id].quantity = this.config.data.data[this.config?.data?.id].quantity - 1;
          this.config.data.data[this.config?.data?.id].writeOffValue = this.config.data.data[this.config?.data?.id].quantity * this.config.data.data[this.config?.data?.id].movingAveragePrice;
        }
        return el;
      });
    }
  }

  onSubmitDialog() {
    const isMasterSel = this.getStockInfoArray.value.some(function (item: any) {
      return item.selectedFlag == true;
    });
    if (!isMasterSel) {
      this.snackbarService.openSnackbar("Please select atleast one stock details", ResponseMessageTypes.WARNING);
      return;
    }
    this.ref.close(this.config.data);
  }
}
