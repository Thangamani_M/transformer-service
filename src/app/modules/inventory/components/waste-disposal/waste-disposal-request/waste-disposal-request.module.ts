import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { RequestAddEditComponent, RequestDetailsItemComponent, RequestListComponent } from '@modules/inventory/components/waste-disposal/waste-disposal-request';
import { StockInfoDialogComponent } from './stock-info-dialog';
import { WasteDisposalRequestRoutingModule } from './waste-disposal-request-routing.module';

@NgModule({
  declarations: [RequestListComponent, RequestAddEditComponent, RequestDetailsItemComponent, StockInfoDialogComponent],
  imports: [
    CommonModule,
    WasteDisposalRequestRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents: [StockInfoDialogComponent]
})
export class WasteDisposalRequestModule { }