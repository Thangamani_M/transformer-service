import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { WasteDisposalRequestAddEditModel } from '@modules/inventory/models';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { filter } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-request-add-edit',
  templateUrl: './request-add-edit.component.html',
  styleUrls: ['./request-add-edit.component.scss']
})
export class RequestAddEditComponent extends PrimeNgTableVariablesModel implements OnInit {
  requestAddEditForm: FormGroup;
  primengTableConfigProperties: any;
  warehouseList: any;
  userData: any;
  wdRequestId: any;
  warehouseId: any;
  deletedId: any = [];
  wasteDisposalDetails: any;
  viewWasteDisposalRequestDetails: any;
  viewable: boolean;
  showAction: boolean;
  isSubmitted: boolean;
  btnName: string;
  itemIdList: any = [];
  selectedIndex = 0;
  Subscription: any;
  isLoading: boolean;
  warhouseSubscription: any;
  ModifiedUserId: any;
  eventSubscription: any;
  loggedUser: UserLogin;
  constructor(private router: Router, private activateRoute: ActivatedRoute, private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private snackbarService: SnackbarService, private crudService: CrudService, private dialog: MatDialog) {
    super();
    this.activateRoute.queryParamMap.subscribe((params) => {
      this.wdRequestId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
      this.warehouseId = (Object.keys(params['params']).length > 0) ? params['params']['warehouseId'] : '';
    });
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: this.wdRequestId && !this.viewable ? 'Update Waste Disposal Request' : this.viewable ? 'View Waste Disposal Request' : 'Add Waste Disposal Request',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Waste Disposal', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Waste Disposal Request', relativeRouterUrl: '/inventory/waste-disposal/waste-disposal-request', }, { displayName: 'Add Waste Disposal Request', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.eventSubscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((data: any) => {
      if (data?.url?.indexOf('kit-config/add-edit?id') > -1) {
        this.viewable = false;
        this.onLoadValue();
      }
    })
  }

  ngOnInit(): void {
    this.onShowValue();
    this.onLoadValue();
  }

  onLoadValue() {
    this.initForm();
    this.onPrimeTitleChanges();
    this.loadAllDropdown();
  }

  getRequestValue(response) {
    if (response.isSuccess && response.resources) {
      this.onShowValue(response);
      this.warehouseId = response?.resources?.warehouseId;
      this.wasteDisposalDetails = { ...response?.resources };
      delete this.wasteDisposalDetails.wasteDisposalDetails;
      if (this.viewable) {
        this.wasteDisposalDetails['assetWasteDisposalItems'] = response?.resources?.wasteDisposalDetails['assetWasteDisposalItems'];
        this.wasteDisposalDetails['consumableWasteDisposalItems'] = response?.resources?.wasteDisposalDetails['consumableWasteDisposalItems'];
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
        this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
      }
    }
  }

  onShowValue(response?: any) {
    this.viewWasteDisposalRequestDetails = [
      { name: 'Request Number', value: response?.resources?.wdRequestNumber },
      { name: 'warehouse', value: response?.resources?.warehouseName },
      { name: 'Created By', value: response?.resources?.createdBy },
      { name: 'Created Date', value: response?.resources?.createdDate },
      { name: 'Actioned By', value: response?.resources?.actionedBy },
      { name: 'Actioned Date', value: response?.resources?.actionedDate },
      { name: 'Company Name', value: response?.resources?.companyName },
      { name: 'Collected By', value: response?.resources?.collectedBy },
      { name: 'Collected Date', value: response?.resources?.collectedDate },
      { name: 'Status', value: response?.resources?.status, statusClass: response?.resources?.cssClass },
    ]
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        this.onEditButtonClicked();
        break;
      default:
        break;
    }
  }

  onEditButtonClicked(): void {
    if (!this.warehouseId) {
      return;
    } else if (!this.wdRequestId) {
      return;
    }
    this.router.navigate(['inventory/waste-disposal/waste-disposal-request/add-edit'], { queryParams: { id: this.wdRequestId, warehouseId: this.warehouseId }, skipLocationChange: true })
  }

  initForm(wasteDisposalRequestAddEditModel?: WasteDisposalRequestAddEditModel) {
    let requestAddEditModel = new WasteDisposalRequestAddEditModel(wasteDisposalRequestAddEditModel);
    this.requestAddEditForm = this.formBuilder.group({});
    Object.keys(requestAddEditModel).forEach((key) => {
      if (typeof requestAddEditModel[key] === 'object') {
        this.requestAddEditForm.addControl(key, new FormArray(requestAddEditModel[key]));
      } else if (!this.viewable) {
        this.requestAddEditForm.addControl(key, new FormControl(requestAddEditModel[key]));
      }
    });
    if (!this.viewable && !this.wdRequestId) {
      this.requestAddEditForm = setRequiredValidator(this.requestAddEditForm, ["warehouseId"]);
      this.onWarehouseValueChanges();
    }
  }

  onWarehouseValueChanges() {
    this.requestAddEditForm.get('warehouseId').valueChanges
      .subscribe(res => {
        if (res.length == 0) {
          return;
        }
        this.onLoadAssetAndConsumables(res);
        this.getRequestFormArray.clear();
      });
  }

  onLoadAssetAndConsumables(warehouseId?) {
    if (warehouseId) {
      this.isLoading = true;
      if (this.warhouseSubscription && !this.warhouseSubscription.closed) {
        this.warhouseSubscription.unsubscribe();
      }
      let params = InventoryModuleApiSuffixModels.WASTE_DISPOSAL_ASSETS;
      this.warhouseSubscription = this.crudService.get(ModulesBasedApiSuffix.INVENTORY, params, undefined, false, prepareRequiredHttpParams({ WarehouseId: warehouseId }))
        .subscribe((res: IApplicationResponse) => {
          if (res?.isSuccess) {
            this.wasteDisposalDetails = res?.resources;
          }
          this.isLoading = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        })
    }
  }

  onPrimeTitleChanges() {
    this.viewable = this.router.url.indexOf('view') !== -1;
    this.primengTableConfigProperties.tableCaption = this.wdRequestId && !this.viewable ? 'Update Waste Disposal Request' : this.viewable ? 'View Waste Disposal Request' : 'Add Waste Disposal Request';
    this.showAction = this.wdRequestId && !this.viewable ? true : this.viewable ? true : true;
    this.btnName = this.wdRequestId ? 'Update' : 'Add';
    this.primengTableConfigProperties.breadCrumbItems[3]['displayName'] = this.viewable ? 'View Waste Disposal Request' : this.wdRequestId && !this.viewable ? 'Update Waste Disposal Request' : 'Add Waste Disposal Request';
  }

  loadAllDropdown() {
    let api;
    let params = new HttpParams().set('UserId', this.loggedUser.userId);
    if (!this.viewable && !this.wdRequestId) {
      api = this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE, params);
    } else if (this.wdRequestId && this.viewable) {
      api = this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WASTE_DISPOSAL, this.wdRequestId, true);
    } else if (this.wdRequestId && !this.viewable) {
      api = forkJoin([this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WASTE_DISPOSAL, this.wdRequestId, true),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WASTE_DISPOSAL_ASSETS, undefined, false, prepareRequiredHttpParams({ WDRequestId: this.wdRequestId, WarehouseId: this.warehouseId }))
      ])
    }
    if (this.Subscription && !this.Subscription?.closed) {
      this.Subscription?.unsubscribe();
      this.rxjsService.setGlobalLoaderProperty(false);
    }
    if (!this.viewable && this.wdRequestId) {
      this.isLoading = true;
      this.Subscription = api.subscribe((response: IApplicationResponse[]) => {
        response.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp.isSuccess && resp.statusCode === 200) {
            switch (ix) {
              case 0:
                this.getRequestValue(resp);
                break;
              case 1:
                this.wasteDisposalDetails['assetWasteDisposalItems'] = resp?.resources?.['assetWasteDisposalItems'];
                this.wasteDisposalDetails['consumableWasteDisposalItems'] = resp?.resources?.['consumableWasteDisposalItems'];
                this.isLoading = false;
                break;
            }
          }
        });
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    } else {
      this.isLoading = true;
      this.Subscription = api.subscribe((response: IApplicationResponse) => {
        if (!this.viewable && !this.wdRequestId) {
          this.warehouseList = response.resources;
        }
        else if (this.viewable) {
          this.getRequestValue(response);
        }
        this.isLoading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  get getRequestFormArray(): FormArray {
    if (!this.requestAddEditForm) return;
    return this.requestAddEditForm.get('requestDetailsArray') as FormArray;
  }

  tabClick(val) {
    this.selectedIndex = val?.index;
    this.getRequestFormArray.clear();
  }

  onConsumeQuantitySubmit(result) {
    if (result) {
      this.wasteDisposalDetails.consumableWasteDisposalItems = result;
    }
  }

  onStockInfoSubmit(res: any) {
    switch (res?.tab) {
      case 0:
        this.wasteDisposalDetails.assetWasteDisposalItems = res?.data;
        break;
      case 1:
        this.wasteDisposalDetails.consumableWasteDisposalItems = res?.data;
        break;
      default:
        break;
    }
  }

  onItemDeleteSubmit(res: any) {
    if (res) {
      this.deletedId.push(...res);
    } else {
      this.deletedId = [];
    }
  }

  onDeleteAllItem() {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      this.isSubmitted = true;
      if (this.deletedId?.join(",") && this.wdRequestId && !this.viewable) {
        this.ModifiedUserId = this.userData.userId;
        this.crudService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WASTE_DISPOSAL,
          this.deletedId.join(","), prepareRequiredHttpParams({
            ModifiedUserId: this.ModifiedUserId
          })).subscribe((res: IApplicationResponse) => {
            if (res?.isSuccess) {
              this.onDeleteItemAfterUpdate();
              this.onAfterDelete();
            } else {
              this.isSubmitted = false;
            }
          })
      } else if (this.deletedId?.join(",") && !this.wdRequestId) {
        this.onDeleteItemAfterCreate();
        this.onAfterDelete();
      } else {
        const tab = this.selectedIndex === 0 ? 'assets' : 'consumable';
        this.snackbarService.openSnackbar(`Please select a valid item on ${tab}`, ResponseMessageTypes.WARNING);
        this.isSubmitted = false;
      }
    });
  }

  onDeleteItemAfterUpdate() {
    switch (this.selectedIndex) {
      case 0:
        this.wasteDisposalDetails.assetWasteDisposalItems = this.wasteDisposalDetails.assetWasteDisposalItems.filter(el =>
          this.deletedId.indexOf(el?.wdRequestItemDetailId) == -1);
        break;
      case 1:
        this.wasteDisposalDetails.consumableWasteDisposalItems = this.wasteDisposalDetails.consumableWasteDisposalItems.filter(el =>
          this.deletedId.indexOf(el?.wdRequestItemDetailId) == -1);
        break;
      default:
        break;
    }
  }

  onDeleteItemAfterCreate() {
    switch (this.selectedIndex) {
      case 0:
        this.wasteDisposalDetails.assetWasteDisposalItems = this.wasteDisposalDetails.assetWasteDisposalItems.filter(el =>
          this.deletedId.indexOf(el?.itemId) == -1);
        break;
      case 1:
        this.wasteDisposalDetails.consumableWasteDisposalItems = this.wasteDisposalDetails.consumableWasteDisposalItems.filter(el =>
          this.deletedId.indexOf(el?.itemId) == -1);
        break;
      default:
        break;
    }
  }

  onAfterDelete() {
    const isAllSelect = this.getRequestFormArray.value.every(function (item: any) {
      return item.selectedFlag == true;
    });
    if (isAllSelect) {
      this.clearFormArray(this.getRequestFormArray);
    } else {
      this.getRequestFormArray.value.forEach((el, i) => {
        if (el?.selectedFlag) {
          this.getRequestFormArray.removeAt(i);
        }
      });;
    }
    this.requestAddEditForm.get("checkAll").setValue(false);
    this.deletedId = [];
    this.isSubmitted = false;
  }
  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  onBeforeSubmitReqDetails(arr, flag) {
    const reqObj = {
      wasteDisposalItems: []
    };
    arr.forEach(el => {
      const wasteObj = {
        wdRequestItemId: el.wdRequestItemId,
        itemId: el.itemId,
        quantity: el.quantity,
        movingAveragePrice: el.movingAveragePrice,
        writeOffValue: el.writeOffValue,
        cosumable: flag,
        wasteDisposalStocks: []
      }
      el?.wasteDisposalStocks?.forEach((elm, i) => {
        wasteObj['wasteDisposalStocks'].push({
          selectedFlag: elm.selectedFlag,
          wdRequestItemDetailId: elm.wdRequestItemDetailId,
          serialNumber: elm.serialNumber,
          barcode: elm.barcode,
          isActive: true,
        })
        if (this.wdRequestId && elm.selectedFlag) {
          wasteObj['wasteDisposalStocks'][i]['isActive'] = elm.isActive;
        }
      })
      reqObj['wasteDisposalItems'].push(wasteObj)
    });
    return reqObj['wasteDisposalItems'];
  }

  onSubmitRequest() {
    this.isSubmitted = true;
    if (this.requestAddEditForm.invalid) {
      this.isSubmitted = false;
      return;
    } else if (!this.onValidateSelectedFlag()) {
      this.isSubmitted = false;
      this.snackbarService.openSnackbar("Select at least one item in the list.", ResponseMessageTypes.WARNING);
      return;
    }
    const reqObj = {
      warehouseId: this.requestAddEditForm.value?.warehouseId,
      WDCompanyId: null,
      LocationId: this.wasteDisposalDetails?.assetWasteDisposalItems ? this.wasteDisposalDetails?.assetWasteDisposalItems[0].locationId : null,
      CreatedUserId: this.userData?.userId,
      wasteDisposalItems: []
    }
    reqObj['wasteDisposalItems'] = this.onBeforeSubmitReqDetails(this.wasteDisposalDetails?.assetWasteDisposalItems, false);
    reqObj['wasteDisposalItems'].push(...this.onBeforeSubmitReqDetails(this.wasteDisposalDetails?.consumableWasteDisposalItems, true))
    let api = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WASTE_DISPOSAL, reqObj)
    if (this.wdRequestId) {
      reqObj['warehouseId'] = this.warehouseId;
      reqObj['LocationId'] = this.wasteDisposalDetails?.locationId;
      reqObj['WDRequestId'] = this.wdRequestId;
      api = this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.WASTE_DISPOSAL, reqObj)
    }
    api.subscribe((res: IApplicationResponse) => {
      if (res?.isSuccess) {
        this.router.navigate(["../"], { relativeTo: this.activateRoute })
      }
      this.isSubmitted = false;
    })
  }

  onValidateSelectedFlag() {
    const validateSelectedFlag = []
    this.wasteDisposalDetails?.assetWasteDisposalItems.filter(el => {
      validateSelectedFlag.push(el.wasteDisposalStocks.some(function (item: any) {
        return item.selectedFlag == true;
      }))
    })
    return validateSelectedFlag.every(el => el == true);
  }

  ngOnDestroy() {
    if (this.Subscription && !this.Subscription.closed) {
    }
    if (this.warhouseSubscription) {
      this.warhouseSubscription?.unsubscribe();
    }
    if (this.eventSubscription) {
      this.eventSubscription?.unsubscribe();
    }
  }
}
