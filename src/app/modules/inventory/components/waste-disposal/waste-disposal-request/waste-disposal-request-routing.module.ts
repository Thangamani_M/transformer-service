import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RequestAddEditComponent, RequestListComponent } from '@modules/inventory/components/waste-disposal/waste-disposal-request';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const wasteDisposalRequest: Routes = [
    { path: '', redirectTo: 'list', canActivate: [AuthGuard] },
    { path: 'list', component: RequestListComponent, data: { title: 'Waste Disposal Request List' }, canActivate: [AuthGuard] },
    { path: 'view', component: RequestAddEditComponent, data: { title: 'Waste Disposal Request View' } },
    { path: 'add-edit', component: RequestAddEditComponent, data: { title: 'Waste Disposal Request Add Edit' } },
];
@NgModule({
    imports: [RouterModule.forChild(wasteDisposalRequest)],

})

export class WasteDisposalRequestRoutingModule { }