import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigCompanyAddEditComponent, ConfigCompanyListComponent, DOAApprovalAddEditComponent, DOAApprovalListComponent, InventoryManagerRequestListComponent, InventoryManagerRequestViewComponent, InventoryStaffListComponent, InventoryStaffRequestAddEditComponent, InventoryStaffRequestViewComponent, WasteDisposalListComponent, WasteDisposalviewComponent } from '@inventory/components/waste-disposal';
import { WasteDisposalImapprovalListComponent } from './waste-disposal-imapproval/waste-disposal-imapproval-list/waste-disposal-imapproval-list.component';
import { WasteDisposalImapprovalUpdateComponent } from './waste-disposal-imapproval/waste-disposal-imapproval-update/waste-disposal-imapproval-update.component';
import { WasteDisposalImapprovalViewComponent } from './waste-disposal-imapproval/waste-disposal-imapproval-view/waste-disposal-imapproval-view.component';
import { WasteDisposalStockCollectionListComponent } from './waste-disposal-stock-collection/waste-disposal-stock-collection-list/waste-disposal-stock-collection-list.component';
import { WasteDisposalStockCollectionUpdateComponent } from './waste-disposal-stock-collection/waste-disposal-stock-collection-update/waste-disposal-stock-collection-update.component';
import { WasteDisposalStockCollectionViewComponent } from './waste-disposal-stock-collection/waste-disposal-stock-collection-view/waste-disposal-stock-collection-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: 'doa-apporval-list', component: DOAApprovalListComponent, data: { title: 'DOA Approval List' }  ,canActivate:[AuthGuard]},
  { path: 'doa-apporval-add-edit', component: DOAApprovalAddEditComponent, data: { title: 'DOA Approval Add/Edit' } ,canActivate:[AuthGuard]},
  { path: 'company-addition-list', component: ConfigCompanyListComponent, data: { title: 'Company Configuration' } ,canActivate:[AuthGuard] },
  { path: 'company-addition-list/add-edit', component: ConfigCompanyAddEditComponent, data: { title: 'Company Configuration' }  ,canActivate:[AuthGuard]},
  { path: 'waste-disposal-request', loadChildren: () => import('./waste-disposal-request/waste-disposal-request.module').then(m => m.WasteDisposalRequestModule) },
  { path: 'request-list', component: InventoryManagerRequestListComponent, data: { title: 'Waste Disposal Request List ' } ,canActivate:[AuthGuard]},
  { path: 'request-list/view', component: InventoryManagerRequestViewComponent, data: { title: 'Waste Disposal Request View' } ,canActivate:[AuthGuard] },
  { path: 'list', component: WasteDisposalListComponent, data: { title: 'Waste Disposal List' } ,canActivate:[AuthGuard] },
  { path: 'list/view', component: WasteDisposalviewComponent, data: { title: 'Waste Disposal View' } ,canActivate:[AuthGuard] },
  { path: 'staff-request-list', component: InventoryStaffListComponent, data: { title: 'Waste Disposal Request List' } ,canActivate:[AuthGuard] },
  { path: 'staff-request-list/add-edit', component: InventoryStaffRequestAddEditComponent, data: { title: 'Waste Disposal Request Add/Edit' } ,canActivate:[AuthGuard]},
  { path: 'staff-request-list/view', component: InventoryStaffRequestViewComponent, data: { title: 'Waste Disposal Request View' } ,canActivate:[AuthGuard] },
  { path: 'waste-disposal-imapproval', component: WasteDisposalImapprovalListComponent, data: { title: 'Waste Disposal Request List' } ,canActivate:[AuthGuard] },
  { path: 'waste-disposal-imapproval/view', component: WasteDisposalImapprovalViewComponent, data: { title: 'Waste Disposal Request View' } ,canActivate:[AuthGuard]},
  { path: 'waste-disposal-imapproval/update', component: WasteDisposalImapprovalUpdateComponent, data: { title: 'Waste Disposal Request Update' } ,canActivate:[AuthGuard] },
  { path: 'waste-disposal-stock', component: WasteDisposalStockCollectionListComponent, data: { title: 'Waste Disposal Collection List' }  ,canActivate:[AuthGuard]},
  { path: 'waste-disposal-stock/view', component: WasteDisposalStockCollectionViewComponent, data: { title: 'Waste Disposal Collection View' } ,canActivate:[AuthGuard] },
  { path: 'waste-disposal-stock/update', component: WasteDisposalStockCollectionUpdateComponent, data: { title: 'Waste Disposal Collection Update' } ,canActivate:[AuthGuard]},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class WasteDisposalRoutingModule { }
