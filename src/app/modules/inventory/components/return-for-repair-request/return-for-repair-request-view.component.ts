import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, ModulesBasedApiSuffix,CrudType, prepareRequiredHttpParams,currentComponentPageBasedPermissionsSelector$, RxjsService, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
    selector: 'app-return-for-repair-request-view',
    templateUrl: './return-for-repair-request-view.component.html',
    styleUrls: ['./return-for-repair-request.component.scss']
})
export class ReturnForRepairRequestViewComponent extends PrimeNgTableVariablesModel {
    primengTableConfigProperties: any;
    primengTableConfigProperties1:any;
    loggedUser: any;
    id;
    ReturnForRepairRequestviewDetail:any;
    rfrDetails;
    selectedTabIndex1:any=0;
    repairRequestDTO:any=[];
    repairRequestReceivingBatches:any=[];
    repairRequestTestingResult:any=[];
    documentDetails;
    itemCodeTab2;
    itemNameTab2;
    returnQty;
    repairRequestItemDetailsDTO:any=[];
    serialInfo: boolean = false;
    constructor(private activatedRoute: ActivatedRoute, private router: Router,private snackbarService:SnackbarService,
        private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
            super()
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: "View Return For Repair Request",
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Return For Repair Request List', relativeRouterUrl: '/inventory/return-for-repair-request' }, { displayName: 'View Return For Repair Request', relativeRouterUrl: '' }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: "RETURN FOR REPAIR REQUEST",
                        dataKey: "rtrRequestId",
                        enableBreadCrumb: true,
                        enableAction:true,
                        enableEditActionBtn: true,
                    },
                    {
                        caption: "RECEIVING BATCHES",
                        dataKey: "rtrRequestId",
                        enableBreadCrumb: true ,
                        enableAction:true,
                        enableEditActionBtn: true,                   
                    },
                    {
                        caption: "TESTING RESULTS",
                        dataKey: "rtrRequestId",
                        enableBreadCrumb: true,
                        enableAction:true,
                        enableEditActionBtn: true,
                    }
                    
                ]
            }
        }
        this.primengTableConfigProperties1 = {
            tableCaption: "",
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Return For Repair Request List', relativeRouterUrl: '/inventory/return-for-repair-request' }, { displayName: 'View Return For Repair Request', relativeRouterUrl: '' }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: "SUPPORTING DOCUMENTS",
                        dataKey: "rtrRequestId",
                        enableBreadCrumb: true,
                        enableAction:true
                    }
                ]
            }
        }
        this.id = this.activatedRoute.snapshot.queryParams.id;
    this.onShowValue()
    }
    ngOnInit(): void {
        this.getReturnForRequestDetails();
        this.getReturnForRequestDocDetails();
        this.combineLatestNgrxStoreData();
    }
    combineLatestNgrxStoreData() {
        combineLatest([
          this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
          let permission = response[0][INVENTORY_COMPONENT.RETURN_FOR_REPAIR_REQUEST];
          if (permission) {
            let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
            this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
          }
        });
      }
    getReturnForRequestDetails() {
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.RETURN_REPAIR_DETAILS, null, false,
            prepareRequiredHttpParams({ RTRRequestId : this.id}
        )).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200 && data.resources) {
                this.rfrDetails = data.resources;
                this.onShowValue(data.resources);
                this.repairRequestDTO = this.rfrDetails.repairRequestItemDTO
                this.repairRequestReceivingBatches = this.rfrDetails.repairRequestReceivingBatches;
                this.repairRequestTestingResult = this.rfrDetails.repairRequestTestingResult
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }
    onShowValue(response?: any) {
        this.ReturnForRepairRequestviewDetail = [
              { name: 'RFR Request No', value: response?.rtrRequestNumber},
              { name: 'Receiving ID', value: response?.receivingId},
              { name: 'Receiving Bardcode', value: response?.receivingBarCode},
              { name: 'QR Code', value: response?.qrCode},
              { name: 'Created By', value: response?.createBy},
              { name: 'Tech Stock Location', value: response?.price},
              { name: 'Warehouse', value: response?.warehouseName},
              { name: 'Tech Stock Location Name', value: response?.techStockLocationName},
        ]
        
      }
    getReturnForRequestDocDetails() {
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.RTR_REQUEST_DOCUMENTS_DETAILS, null, false,
            prepareRequiredHttpParams({ RTRRequestId: this.id }
            )).subscribe(data => {
                if (data.isSuccess && data.statusCode == 200 && data.resources) {
                    this.documentDetails = data.resources;
   
                   // this.addItem(data.resources);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    tabClick(event){
        this.selectedTabIndex = event?.index;
    }
    tabClick1(event){
        this.selectedTabIndex1 = event?.index;
    }
   
    navigate() {
        this.router.navigate(["inventory/return-for-repair-request"] );
    }
    edit(){
        this.router.navigate(["inventory/return-for-repair-request/update"], { queryParams: {id:this.id} }); 
    }
    onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
        switch (type) {
          case CrudType.EDIT:
            if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
                return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
              }
            this.edit()
            break;
         
        }
      }
      onClickStockCode1(itemCode,index) {
        this.serialInfo = !this.serialInfo;

        if(this.repairRequestReceivingBatches &&  this.repairRequestReceivingBatches[index]?.repairRequestReceivedBatchesItemDTO && this.repairRequestReceivingBatches[index]?.repairRequestReceivedBatchesItemDTO?.length > 0){
            let filter = this.repairRequestReceivingBatches[index]?.repairRequestReceivedBatchesItemDTO?.filter(x => x.itemCode == itemCode);
            this.repairRequestItemDetailsDTO=filter[0].repairRequestItemDetailsDTO;
            this.itemCodeTab2 = this.repairRequestReceivingBatches[index]?.repairRequestReceivedBatchesItemDTO[0].itemCode;
            this.itemNameTab2 =  this.repairRequestReceivingBatches[index]?.repairRequestReceivedBatchesItemDTO[0].itemName;
            this.returnQty =  this.repairRequestReceivingBatches[index]?.repairRequestReceivedBatchesItemDTO[0].returnQty;
        }
        
    }
}