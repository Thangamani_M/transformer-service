import { DatePipe } from "@angular/common";
import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, exportList,currentComponentPageBasedPermissionsSelector$,prepareDynamicTableTabsFromPermissions, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { combineLatest } from 'rxjs';
import { select, Store } from "@ngrx/store";
import { map } from "rxjs/operators";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
    selector: 'app-return-for-repair-request',
    templateUrl: './return-for-repair-request.component.html',
    styleUrls: ['./return-for-repair-request.component.scss']
})
export class ReturnForRepairRequestListComponent extends PrimeNgTableVariablesModel {
    primengTableConfigProperties: any;
    row: any = {};
    loggedUser: any;
    otherParams;
    constructor(private snackbarService:SnackbarService,private router: Router, private rxjsService: RxjsService, private datePipe: DatePipe, private crudService: CrudService, private store: Store<AppState>) {
        super();
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: "Return For Repair Request",
            breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Return For Repair Request', relativeRouterUrl: '' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Return For Repair Request',
                        dataKey: 'handlingFeeConfigId',
                        captionFontSize: '21px',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        enableGlobalSearch: false,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        enableRowDelete: false,
                        enableFieldsSearch: true,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        enablePrintBtn: true,
                        printTitle: 'Return For Repair Request',
                        printSection: 'print-section0',
                        columns: [
                            { field: 'rtrRequestNumber', header: 'RFR Request Number', width: '200px' },
                            { field: 'serviceCallNumber', header: 'Service Call Number', width: '200px' },
                            { field: 'qrCode', header: 'QR Code', width: '200px' },
                            { field: 'warehouseName', header: 'Warehouse', width: '200px' },
                            { field: 'techStockLocation', header: 'Tech Stock Location', width: '200px' },
                            { field: 'techStockLocationName', header: 'Tech Stock Location Name', width: '200px' },
                            { field: 'createdBy', header: 'Created By', width: '200px' },
                            { field: 'createdDate', header: 'Created Date', width: '200px' },
                            { field: 'status', header: 'Status', width: '200px' },
                        ],
                        enableMultiDeleteActionBtn: false,
                        enableAddActionBtn: false,
                        shouldShowFilterActionBtn: false,
                        areCheckboxesRequired: false,
                        isDateWithTimeRequired: true,
                        enableExportCSV: false,
                        enableExportBtn: true,
                        apiSuffixModel: InventoryModuleApiSuffixModels.RETURN_REPAIR_REQUEST,
                        moduleName: ModulesBasedApiSuffix.INVENTORY,
                    }
                ]
            }
        }
    }
    ngOnInit(): void {

        this.getRequiredList();
        this.combineLatestNgrxStoreData();
        this.rxjsService.setGlobalLoaderProperty(false);
    }
    combineLatestNgrxStoreData() {
        combineLatest([
          this.store.select(currentComponentPageBasedPermissionsSelector$)]
        ).subscribe((response) => {
          let permission = response[0][INVENTORY_COMPONENT.RETURN_FOR_REPAIR_REQUEST];
          if (permission) {
            let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
            this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
          }
        });
      }
    getRequiredList(pageIndex?: string, pageSize?: string, otherParams?: object) {
        this.loading = true;
        otherParams = {...otherParams, userId: this.loggedUser?.userId};
        this.otherParams = otherParams
        let apiSuffixModels: InventoryModuleApiSuffixModels;
        apiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
        this.crudService.get(
            this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
            apiSuffixModels,
            undefined,
            false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
        ).pipe(map((res: IApplicationResponse) => {
            if (res?.resources) {
                res?.resources?.forEach(val => {
                    val.createdDate = val.createdDate ? this.datePipe.transform(val.createdDate, 'dd-MM-yyyy hh:mm:ss') : '';
                    return val;
                })
            }
            return res;
        })).subscribe((data: IApplicationResponse) => {
                this.loading = false;
                this.rxjsService.setGlobalLoaderProperty(false);
                if (data.isSuccess) {
                    this.dataList = data.resources;
                    this.totalRecords = data.totalCount;
                } else {
                    data.resources = null;
                    this.dataList = data.resources
                    this.totalRecords = 0;
                }
            });
        this.rxjsService.setGlobalLoaderProperty(false);
    }
    onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
        switch (type) {
            case CrudType.GET:
                this.row=row;
                this.getRequiredList(row["pageIndex"], row["pageSize"], unknownVar)
                break;
            case CrudType.VIEW:
                this.router.navigate(['/inventory/return-for-repair-request/view'],{queryParams:{id:row['rtrRequestId']}});
                break;
             case CrudType.EXPORT:
                if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
                    return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
                  }
                    let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
                    let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
                    this.exportList(pageIndex,pageSize);
               break;
            default:
        }
    }
    exportList(pageIndex?: any, pageSize?: any) {
        exportList(this.otherParams,0,pageSize,ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.RTR_REQUEST_EXPORT,this.crudService,this.rxjsService,'Return For Repair Request');
    }
    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }
}

