import { HttpParams } from "@angular/common/http";
import { Component, Inject } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, fileUrlDownload, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { QuoteApprovalModel, QuoteCreationModel, RFRFormCreationModel } from "@modules/inventory/models/rfr-creation.model";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { WINDOW } from "./window.providers";

@Component({
    selector: 'app-return-for-repair-request-update',
    templateUrl: './return-for-repair-request-update.component.html',
    styleUrls: ['./return-for-repair-request.component.scss']
})
export class ReturnForRepairRequestUpdateComponent {
    primengTableConfigProperties: any;
    primengTableConfigProperties1: any;
    primengTableConfigPropertiesSerialInfo: any;
    loggedUser: any;
    id;
    rfrDetails;
    totalRecords;
    selectedTabIndex: any = 0;
    selectedTabIndex1: any = 0;
    selectedTabIndexSerial: any = 0;
    repairRequestDTO: any = [];
    repairRequestReceivingBatches: any = [];
    repairRequestTestingResult: any = [];
    fileName;
    rfrForm: FormGroup;
    rfrItemDetail: FormGroup;
    quoteForm: FormGroup;
    quoteApprovalForm: FormGroup;
    documentType: any = [];
    quotePostDTO;
    serialInfo: boolean = false;
    dataList = [];
    loading: boolean = false;
    panelOpenState: boolean = false;
    selectedRows: any = [];
    fileObjArr: any = [];
    formData = new FormData();
    itemCodeTab1;
    itemNameTab1;
    returnQtyTab1;
    itemCodeTab2;
    itemNameTab2;
    documentDetails;
    len:any=0;
    finalObj;
    type;
    requestQuoteApprovalTypeList:any=[];
    requestQuoteApprovalStatusList:any=[];
    isWarehouseQuote:boolean=false;
    isUpdate:boolean=false;
    filteredSuppliers:any=[];
    filterAddress:any=[];
    constructor( private snackbarService: SnackbarService,@Inject(WINDOW) private window: Window,private activatedRoute: ActivatedRoute, private router: Router, private formBuilder: FormBuilder,
        private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.id = this.activatedRoute.snapshot.queryParams.id;
        this.type = this.activatedRoute.snapshot.queryParams.type;
        this.primengTableConfigProperties = {
            tableCaption: "Update Return For Repair Request",
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Return For Repair Request List', relativeRouterUrl: '/inventory/return-for-repair-request' }, { displayName: 'View Return For Repair Request', relativeRouterUrl: '/inventory/return-for-repair-request/view' , queryParams: { id: this.id }}, { displayName: 'Update Return For Repair Request', relativeRouterUrl: '' }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: "RETURN FOR REPAIR REQUEST",
                        dataKey: "rtrRequestId",
                        enableBreadCrumb: true,
                        enableAction: true
                    },
                    {
                        caption: "RECEIVING BATCHES",
                        dataKey: "rtrRequestId",
                        enableBreadCrumb: true,
                        enableAction: true
                    },
                    {
                        caption: "TESTING RESULTS",
                        dataKey: "rtrRequestId",
                        enableBreadCrumb: true,
                        enableAction: true
                    }
                ]
            }
        }
        this.primengTableConfigProperties1 = {
            tableCaption: "",
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Return For Repair Request List', relativeRouterUrl: '/inventory/return-for-repair-request' }, { displayName: 'View Return For Repair Request', relativeRouterUrl: '/inventory/return-for-repair-request/view' , queryParams: { id: this.id }}],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: "SUPPORTING DOCUMENTS",
                        dataKey: "rtrRequestId",
                        enableBreadCrumb: true,
                        enableAction: true,
                        disabled: false,
                    },
                    {
                        caption: "QUOTE GENERATION",
                        dataKey: "rtrRequestId",
                        enableBreadCrumb: true,
                        enableAction: true,
                        disabled: false,
                    },
                    {
                        caption: "QUOTE APPROVAL",
                        dataKey: "rtrRequestId",
                        enableBreadCrumb: true,
                        enableAction: true,
                        disabled: false,
                    }
                ]
            }
        }
        this.primengTableConfigPropertiesSerialInfo = {
            tableCaption: "Return For Repair Request",
            breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Return For Repair Request', relativeRouterUrl: '' }],
            selectedTabIndex: 0,
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: 'Return For Repair Request',
                        dataKey: 'handlingFeeConfigId',
                        captionFontSize: '21px',
                        enableBreadCrumb: true,
                        enableAction: true,
                        enableReset: false,
                        checkBox: true,
                        reorderableColumns: false,
                        resizableColumns: false,
                        enableScrollable: true,
                        enablePrintBtn: true,
                        printTitle: 'Pro Forma Invoice',
                        printSection: 'print-section0',
                        enableFieldsSearch: false,
                        enableHyperLink: true,
                        cursorLinkIndex: 0,
                        columns: [
                            { field: 'serialNumber', header: 'Serial Number', width: '200px' },
                            { field: 'tempStockCode', header: 'Temp Stock Code', width: '200px' },
                            { field: 'warrentyStatusName', header: 'Warrenty Status Name', width: '200px' },
                            { field: 'action', header: 'Actions', width: '200px' },
                            { field: 'itemStatusName', header: 'Item Status Name', width: '200px' },
                            { field: 'returnToCustomer', header: 'Return To Customer', width: '200px' },
                            { field: 'supplierName', header: 'Supplier Name', width: '200px' },
                            { field: 'supplierAddressName', header: 'Supplier Address Name', width: '200px' },
                        ],
                        enableMultiDeleteActionBtn: false,
                        enableAddActionBtn: true,
                        shouldShowFilterActionBtn: false,
                        areCheckboxesRequired: false,
                    }
                ]
            }
        }

    }
    ngOnInit(): void {
        this.getReturnForRequestDetails();
        this.getDocType();
        this.getReturnForRequestQuoteApprovalType();
        this.getReturnForRequestDocDetails();
        this.getReturnForRequestQuoteApprovalStatus();
        this.createRFRCreationForm();
        this.createQuoteCreationForm();
        if(this.type == 'task-list'){
           // this.getQuoteApproval();
            let event={index:2};
            this.tabClick1(event);
            this.primengTableConfigProperties1.tableComponentConfigs.tabsList[0].disabled=false;
            this.primengTableConfigProperties1.tableComponentConfigs.tabsList[1].disabled=false;
            this.primengTableConfigProperties1.tableComponentConfigs.tabsList[2].disabled=false;
        }
    }
    createRFRCreationForm(): void {
        let rfrModel = new RFRFormCreationModel();
        //create form controls dynamically from model class
        this.rfrForm = this.formBuilder.group({
            rfrItemsPostDTO: this.formBuilder.array([])
        });
        this.rfrItemDetail = this.formBuilder.group({
            rfrItemsDetailDTO: this.formBuilder.array([])
        });
        Object.keys(rfrModel).forEach((key) => {
            this.rfrForm.addControl(key, new FormControl(rfrModel[key]));
        });
        this.rfrForm = setRequiredValidator(this.rfrForm, ["RTRRequestDocumentTypeId", "SerialNumber", "stockCode"]);
    }
    createQuoteCreationForm(): void {
        let rfrModel = new QuoteCreationModel();
        // Object.keys(rfrModel).forEach((key) => {
        //     this.quoteForm.addControl(key, new FormControl(rfrModel[key]));
        // });
        //create form controls dynamically from model class
        this.quoteForm = this.formBuilder.group({
            quotePostDTO: this.formBuilder.array([])
        });     
        // this.rfrForm = setRequiredValidator(this.rfrForm, ["RTRRequestDocumentTypeId","SerialNumber","stockCode"]);
    }
    createQuoteApprovalForm(): void {
        let rfrModel = new QuoteApprovalModel();
        //create form controls dynamically from model class
        this.quoteApprovalForm = this.formBuilder.group({
            quoteApprovalPostDTO: this.formBuilder.array([])
        });     
        // this.rfrForm = setRequiredValidator(this.rfrForm, ["RTRRequestDocumentTypeId","SerialNumber","stockCode"]);
    }
    get getRFRItemsDetailsArray(): FormArray {
        if (this.rfrItemDetail !== undefined) {
            return (<FormArray>this.rfrItemDetail.get('rfrItemsDetailDTO'));
        }
    }
    get getRFRItemsArray(): FormArray {
        if (this.rfrForm !== undefined) {
            return (<FormArray>this.rfrForm.get('rfrItemsPostDTO'));
        }
    }
    get getQuoteItemsArray(): FormArray {
        if (this.quoteForm !== undefined) {
            return (<FormArray>this.quoteForm.get('quotePostDTO'));
        }
    }
    get getQuoteApproveItemsArray(): FormArray {
        if (this.quoteApprovalForm !== undefined) {
            return (<FormArray>this.quoteApprovalForm.get('quoteApprovalPostDTO'));
        }
    }
    getReturnForRequestQuoteApprovalType() {
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.UX_RTRREQUESTQUOTEAPPROVAL_TYPE, null, false, null
        ).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200 && data.resources) {
               this.requestQuoteApprovalTypeList = data.resources;
            }
       });
    }
    getReturnForRequestQuoteApprovalStatus() {
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.UX_FAULTYWORKLIST_STATUS, null, false, null
        ).subscribe(data => {
            if (data.isSuccess && data.statusCode == 200 && data.resources) {
               this.requestQuoteApprovalStatusList = data.resources;

            }
       });
    }
    
    getReturnForRequestDetails() {
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.RETURN_REPAIR_DETAILS, null, false,
            prepareRequiredHttpParams({ RTRRequestId: this.id }
            )).subscribe(data => {
                if (data.isSuccess && data.statusCode == 200 && data.resources) {
                    this.rfrDetails = data.resources;
                    this.repairRequestDTO = this.rfrDetails.repairRequestItemDTO
                    this.repairRequestReceivingBatches = this.rfrDetails.repairRequestReceivingBatches;
                    this.repairRequestTestingResult = this.rfrDetails.repairRequestTestingResult;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    getDocType(){
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.UX_RTRREQUESTDOCUMENT_TYPE, null, false,
            prepareRequiredHttpParams({ RTRRequestId: this.id }
            )).subscribe(data => {
                if (data.isSuccess && data.statusCode == 200 && data.resources) {
                    this.documentType = data.resources;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            }); 
    }
    getReturnForRequestDocDetails() {
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.RTR_REQUEST_DOCUMENTS_DETAILS, null, false,
            prepareRequiredHttpParams({ RTRRequestId: this.id }
            )).subscribe(data => {
                if (data.isSuccess && data.statusCode == 200 && data.resources) {
                    this.documentDetails = data.resources;
                    let isQuoteGenerate=false;let isUpdateArr=[];
                    this.repairRequestDTO.forEach(element => {
                       let docFilter = this.documentDetails.filter(x=>x.stockCode == element.itemCode);
                       if(docFilter.length == element?.repairRequestItemDetailsDTO.length)
                          isUpdateArr.push(true);
                       else
                          isUpdateArr.push(false);
                    });
                    let isUpdateArrFilter = isUpdateArr.filter(xi=>xi==false);
                    this.isUpdate = (isUpdateArrFilter && isUpdateArrFilter.length >0) ? false : false;

                    this.documentDetails.forEach(element => {// isQuoteGenerate is true allow Quote Generation
                        isQuoteGenerate = element.isQuoteGenerate? element.isQuoteGenerate :false;
                        if(element.isQuoteGenerate == false) return;                    
                    });
                    if(isQuoteGenerate){
                        this.primengTableConfigProperties1.tableComponentConfigs.tabsList[1].disabled=false;
                        this.getQuoteGeneration('initial');
                    }
                    this.addItem(data.resources);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    addItem(data){
       data.forEach(element => {
            let item = this.formBuilder.group({
                RTRRequestId: this.id,
                RTRRequestItemDetailId: element.rtrRequestItemDetailId,
                RTRRequestDocumentTypeId: element.rtrRequestDocumentId,
                stockCode: element.stockCode,
                tempStockCode: element.tempStockCode,
                CreatedUserId: this.loggedUser.userId,
                DocName: element?.supplierDocName,
                rfrSigedDocName:element?.rfrSigedDocName,
                rfrSigedPath:element?.rfrSigedPath,
                supplierDocName:element?.supplierDocName,
                supplierPath:element?.supplierPath,
                warehouseQuoteDocName:element?.warehouseQuoteDocName,
                warehouseQuotePath:element?.warehouseQuotePath,
                finalQuoteDocName:element?.finalQuoteDocName,
                finalQuotePath:element?.finalQuotePath,
                ItemId: element.itemId,
                SerialNumber: element.serialNumber,
            })
            // item = setRequiredValidator(item, ["quantity","stockCode"]);
            this.getRFRItemsArray.push(item);
        });
        
    }
    download(path) {
        if (path) return fileUrlDownload(path, true);
    }
    getSerialNumberDetail(serialNumber) {
       let itemArr= this.repairRequestTestingResult.filter(x=>x.serialNumber==serialNumber);
       let ItemId = (itemArr && itemArr.length>0) ? itemArr[0].itemId : null;
       if(ItemId){
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.RTR_REQUEST_SERIALNUMBER_DETAILS, null, false,
            prepareRequiredHttpParams({ RTRRequestId: this.id, serialNumber: serialNumber, ItemId: ItemId }
            )).subscribe(data => {
                if (data.isSuccess && data.statusCode == 200 && data.resources) {
                    this.rfrForm.get("RTRRequestItemDetailId").setValue(data.resources[0].rtrRequestItemDetailId),
                        this.rfrForm.get("ItemId").setValue(data.resources[0].rtrRequestItemId);
                    this.rfrForm.get("tempStockCode").setValue(data.resources[0].tempStockCode);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
            this.rxjsService.setGlobalLoaderProperty(false);
       }       
    }
    onClickStockCode(itemCode) {
        this.serialInfo = !this.serialInfo;
        let filter = this.repairRequestDTO.filter(x => x.itemCode == itemCode);
        this.itemCodeTab1 = filter[0].itemCode;
        this.itemNameTab1 = filter[0].itemName;
        this.returnQtyTab1 = filter[0].returnQty;
        this.dataList = filter[0].repairRequestItemDetailsDTO;
        if(filter[0].repairRequestItemDetailsDTO && filter[0].repairRequestItemDetailsDTO.length >0){
            filter[0].repairRequestItemDetailsDTO.forEach((element,i) => {
                this.getRFRItemsDetailsArray.removeAt(i);
            });
            filter[0].repairRequestItemDetailsDTO.forEach((element,i) => {
                if(element.supplierId)
                  this.onSupplierAddressSelected(element.supplierId, i);
                let Item = this.formBuilder.group({
                    serialNumber:element.serialNumber,
                    RTRRequestItemDetailId: element.rtrRequestItemDetailId,
                    OrderReceiptItemDetailId: element.orderReceiptItemDetailId,
                    WarrentyStatusId: element.warrentyStatusId,
                    IsReturnToCustomer: element.returnToCustomer,
                    TempStockCode: element.tempStockCode,
                    RTRRequestItemStatusId: element.itemStatusId,
                    SupplierId: element.supplierId,
                    SupplierAddressId:element.supplierAddressId,
                    warrentyStatusName:element.warrentyStatusName,
                    action:element.action,
                    CreatedUserId: this.loggedUser.userId
                });
                // dealerTypeItem = setRequiredValidator(dealerTypeItem, ["quantity","stockCode"]);
                this.getRFRItemsDetailsArray.push(Item);
            });
        }
     
    }
    /* Supplier Address dropdown */
    onSupplierAddressSelected(obj: any, index: number) {
        if (obj) {
            if (obj.id == null) return;
            let params = new HttpParams().set('supplierId', obj.id);
            this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIER_ADDRESS, undefined, true, params)
                .subscribe((response: IApplicationResponse) => {
                    if (response.resources && response.statusCode === 200) {
                        //this.getOrderReceiptItemDetailsArray.controls[index].get('addressDropdown').patchValue(response.resources);
                      
                         this.filterAddress = response.resources;
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                });
        }
    }
    inputChangeSupplierFilter(text: any) {
        let otherParams = {}
        if (text.query == null || text.query == '') return;
        otherParams['searchText'] = text.query;
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_AUTOCOMPLETE, null, false,
            prepareGetRequestHttpParams(null, null, otherParams))
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.resources.length > 0) {
                    this.filteredSuppliers = response.resources;               
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    onClickStockCode1(itemCode) {
        this.serialInfo = !this.serialInfo;
        let filter = this.repairRequestReceivingBatches.repairRequestReceivedBatchesItemDTO.filter(x => x.itemCode == itemCode);
        this.itemCodeTab2 = filter[0].itemCode;
        this.itemNameTab2 = filter[0].itemName;
    }
    //Add RCFR Details
    addRFRItem() {
        if (this.rfrForm.invalid) {
            this.rfrForm.markAllAsTouched();
            return;
        }
        let dealerTypeItem = this.formBuilder.group({
            RTRRequestId: this.id,
            RTRRequestItemDetailId: this.rfrForm.get("RTRRequestItemDetailId").value,
            RTRRequestDocumentTypeId: this.rfrForm.get("RTRRequestDocumentTypeId").value,
            stockCode: this.rfrForm.get("stockCode").value,
            tempStockCode: this.rfrForm.get("tempStockCode").value,
            CreatedUserId: this.loggedUser.userId,
            DocName: this.fileName,
            ItemId: this.rfrForm.get("ItemId").value,
            SerialNumber: this.rfrForm.get("SerialNumber").value,
            rfrSigedDocName:null,
            rfrSigedPath:null,
            supplierDocName:null,
            supplierPath:null,
            warehouseQuoteDocName:null,
            warehouseQuotePath:null,
            finalQuoteDocName:null,
            finalQuotePath:null
        });
        // dealerTypeItem = setRequiredValidator(dealerTypeItem, ["quantity","stockCode"]);
        this.getRFRItemsArray.push(dealerTypeItem);
        // this.isAddedErr = false;
        this.fileName = null;
        // this.stockOrderCreationForm.get('quantity').reset();
        let isUpdateArr=[]; //all stock code serial number should have document uploaded
        this.repairRequestDTO.forEach(element => {
        let docFilter = this.rfrForm?.get('rfrItemsPostDTO').value.filter(x=>x.stockCode == element.itemCode);
            if(docFilter.length == element?.repairRequestItemDetailsDTO.length)
            isUpdateArr.push(true);
            else
            isUpdateArr.push(false);
        });
        this.rfrForm.get("stockCode").reset();
        this.rfrForm.get("SerialNumber").reset();
        this.rfrForm.get("RTRRequestDocumentTypeId").reset();
        this.rfrForm.get("tempStockCode").reset();
        let isUpdateArrFilter = isUpdateArr.filter(xi=>xi==false);
       this.isUpdate = (isUpdateArrFilter && isUpdateArrFilter.length >0) ? false : false;

    }
    onChangeSelecedRows(e) {
        this.selectedRows = e;
    }
    tabClick(event) {
        this.selectedTabIndex = event?.index;
    }
    tabClick1(event) {
        this.selectedTabIndex1 = event?.index;
        if( this.selectedTabIndex1==0){
            this.getReturnForRequestDetails();
        }
        if( this.selectedTabIndex1==1){
            this.getQuoteGeneration('initial');
        }
        if( this.selectedTabIndex1==2){
            this.getQuoteApproval();
        }
    }
    onBreadCrumbClick(breadCrumbItem: object): void {
        this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`]);
    }
    obj: any;
    onFileSelected(files: FileList): void {
        //   this.showFailedImport = false;
        const fileObj = files.item(0);
        this.fileName = fileObj.name;
        this.obj = fileObj;
        this.fileObjArr.push(this.obj);
    }
    onSubmit() {
        this.formData = new FormData();
        if (this.selectedTabIndex1 == 1 && this.quoteForm.invalid ) {
            this.quoteForm.markAllAsTouched();
           return;
        }
        if (this.selectedTabIndex1 == 0) {
            let formValueArr=[];
            let formValue = this.rfrForm.value.rfrItemsPostDTO;
            formValue.forEach(element => {
                if(element.RTRRequestDocumentTypeId != null && element.RTRRequestDocumentTypeId)
                   formValueArr.push(element);
            });
            if(formValueArr && formValueArr.length==0){
                this.snackbarService.openSnackbar("Upload the rest of the documents for correspoding stock codes.",ResponseMessageTypes.WARNING);
                return;
            }
            this.formData.append('Obj', JSON.stringify(formValueArr));

            this.fileObjArr.forEach(element => {
                this.formData.append(element.name, element);
            });

            let api = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RETURN_REPAIR_DOCUMENTS, this.formData);
            api.subscribe((res: any) => {
                if (res?.isSuccess == true && res?.statusCode == 200) {
                    if (this.selectedTabIndex1 == 0) {
                           // this.primengTableConfigProperties1.tableComponentConfigs.tabsList[1].disabled = false;
                            // this.primengTableConfigProperties1.tableComponentConfigs.tabsList[0].disabled = true;
                           //  let event={index:1};
                        this.rfrForm.value.rfrItemsPostDTO.forEach((element,index) => {
                            this.getRFRItemsArray.removeAt(index);
                        });
                        this.rfrForm.value.rfrItemsPostDTO=null;
                        this.rfrForm = this.formBuilder.group({
                            rfrItemsPostDTO: this.formBuilder.array([])
                        });
                        //this.ngOnInit();
                        this.getReturnForRequestDetails();
                        this.getDocType();
                        this.getReturnForRequestDocDetails();
                        this.getQuoteGeneration('submit');
                    }
                }
                // if (this.selectedTabIndex1 == 0) {
                //     this.getQuoteGeneration('submit');
                // }      
            })
        }
        else if (this.selectedTabIndex1 == 1) {
            let formValue = this.quoteForm.value.quotePostDTO;
            let finalObj=[];
            formValue.forEach(element => {
                let item =   {
                    RTRRequestId:element.RTRRequestId,
                    RTRRequestItemDetailId:element.RTRRequestItemDetailId,
                    CreatedUserId:element.CreatedUserId,
                    SerialNumber:element.SerialNumber,
                    QuotedAmount:element.QuotedAmount,
                 }
              
                finalObj.push(item);
            });
            let api = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.QUOTE_GENERATE, finalObj);
            api.subscribe((res: any) => {
                if (res?.isSuccess == true && res?.statusCode == 200) {
                   // this.navigate();
                    if (this.selectedTabIndex1 == 1) {
                    // let event={index:2};
                     //this.tabClick1(event);  
                     this.getQuoteGeneration('submit');
                     this.getQuoteApproval();
                    }
                }
            });
        }
        else if(this.selectedTabIndex1 == 2){
            let formValue = this.quoteApprovalForm.value.quoteApprovalPostDTO;
        
            let api = this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.QUOTE_APPROVAL, formValue);
            api.subscribe((res: any) => {
                if (res?.isSuccess == true && res?.statusCode == 200) {
                    // this.navigate();
                    // if (this.selectedTabIndex1 == 1) {
                    // this.getQuoteApproval();
                    //  }
                }
            })
        }
        this.rxjsService.setDialogOpenProperty(false);
    }
    onSubmitPopup() {
        if (this.selectedTabIndex1 == 1 && this.quoteForm.invalid ) {
            this.rfrItemDetail.markAllAsTouched();
           return;
        } 
        let formValue = this.rfrItemDetail.value.rfrItemsDetailDTO;
        formValue.forEach(element => {
            element.SupplierId =  element.SupplierId.id
        });
        let api = this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RETURN_REPAIR_ITEMDETAILS, formValue);
        api.subscribe((res: any) => {
            if (res?.isSuccess == true && res?.statusCode == 200) {
                // this.navigate();
                // if (this.selectedTabIndex1 == 1) {
                // this.getQuoteApproval();
                //  }
            }
        })
    }
    getQuoteGeneration(type) {
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.RETURN_REQUEST_WAREHOUSE_DETAILS, null, false,
            prepareRequiredHttpParams({ RTRRequestId: this.id }
            )).subscribe(data => {
                let flagArr=[];let warehouseFlagArr=[];
                if (data.isSuccess && data.statusCode == 200 && data.resources) {
                    if (data.resources && data.resources.length > 0) {  
                        if(type=='submit'){
                           // let event={index:1};
                           // this.tabClick1(event);  
                        }                      
                        if(this.selectedTabIndex1==1){
                            this.createQuoteCreationForm();
                        } 
                        data.resources.forEach(element => {
                            flagArr.push(element?.isQuoteApproval)
                            warehouseFlagArr.push(element?.isWarehouseQuote)
                            let item = this.formBuilder.group({
                                RTRRequestId: this.id,
                                RTRRequestItemDetailId: element.rtrRequestItemDetailId,
                                CreatedUserId: this.loggedUser.userId,
                                SerialNumber: element.serialNumber,
                                QuotedAmount: element.quotedAmount,
                                stockCode:element.stockCode,
                                tempStockCode:element.tempStockCode,
                            });                   
                            item = setRequiredValidator(item, ["QuotedAmount"]);
                           this.getQuoteItemsArray.push(item);
                           this.len = this.quoteForm?.get('quotePostDTO')?.value.length;
                           this.finalObj = this.getQuoteItemsArray.value
                        });
                        let flag;
                        flagArr = flagArr.filter(x=>x==false);
                        if(flagArr && flagArr.length ==0){
                            if (this.selectedTabIndex1 == 0) {
                                this.primengTableConfigProperties1.tableComponentConfigs.tabsList[1].disabled = false;
                                this.primengTableConfigProperties1.tableComponentConfigs.tabsList[0].disabled = false;
                            }   
                        }  
                        warehouseFlagArr =  warehouseFlagArr.filter(x=>x==false);
                        this.isWarehouseQuote = (warehouseFlagArr && warehouseFlagArr.length >0 )? false :true;
                    }
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    getQuoteApproval(){
        // let event={index:2};
        // this.tabClick1(event);  
        if(this.selectedTabIndex1==2){
            this.createQuoteApprovalForm();
        } 
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.RETURN_REQUEST_WAREHOUSE_DETAILS, null, false,
            prepareRequiredHttpParams({ RTRRequestId: this.id }
            )).subscribe(data => {         
                  if (data.isSuccess && data.statusCode == 200 && data.resources) {
                    if (data.resources && data.resources.length > 0) {   
                         
                    }                
                  } 
                  this.rxjsService.setGlobalLoaderProperty(false);
         });
         this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.RTR_REQUEST_QUOTE_DETAILS, null, false,
            prepareRequiredHttpParams({ RTRRequestId: this.id }
            )).subscribe(data => {         
                  if (data.isSuccess && data.statusCode == 200 && data.resources) {
                    if (data.resources && data.resources.length > 0) {                       
                        let flag;let flagArr=[];                    
                        data.resources.forEach(element => {
                            flagArr.push(element?.isQuoteApproval)
                            let item = this.formBuilder.group({
                                RTRRequestItemDetailId: element.rtrRequestItemDetailId,                               
                                Reason: element.comments,
                                quotedAmount:element.quotedAmount,
                                quoteNumber:element.quoteNumber,
                                RTRRequestQuoteApprovalStatusId: null,
                                CreatedUserId: this.loggedUser.userId
                            });                   
                           //  item = setRequiredValidator(item, ["QuotedAmount"]);
                           this.getQuoteApproveItemsArray.push(item);
                           this.finalObj = this.getQuoteApproveItemsArray.value
                        });
                        flagArr = flagArr.filter(x=>x==true);
                        if(flagArr && flagArr.length > 0){ }
                    }                
                  } 
                  this.rxjsService.setGlobalLoaderProperty(false);
         });
    }
    generateWareHouse(){
        let url = this.getUrl();
        let finalObj={
            rtrRequestId:  this.id,
            quotationType: "Warehouse",   
            applicationUrl: url,
            createdUserId: this.loggedUser.userId
        }
        let api = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RFR_QUOTATION_GENERATE, finalObj);
        api.subscribe((res: any) => {
            if (res?.isSuccess == true && res?.statusCode == 200) {
                this.navigate();
            }
        })
    }
    getUrl(){
        if(this.window.location.hostname.includes('localhost')){
               return "https://transformer-service-dev.azurewebsites.net";
        }else if(this.window.location.hostname.includes('-dev')) {
            return "https://transformer-service-dev.azurewebsites.net";
        }
        else if(this.window.location.hostname.includes('-staging')) {
            return "https://transformer-service-staging.azurewebsites.net/";
        }
        else if(this.window.location.hostname.includes('-testing')) {
            return "https://transformer-service-testing.azurewebsites.net/";
        }
    }
    navigate() {
        this.router.navigate(["inventory/return-for-repair-request"]);
    }
    edit() {
        this.router.navigate(["inventory/return-for-repair-request/update"], { queryParams: { id: this.id } });
    }
    onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
        switch (type) {
            case CrudType.VIEW:

                break;
            default:
        }
    }
    onActionSubmited(e: any) {
        if (e.data && !e.search && !e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data);
        } else if (e.data && e.search && !e?.col) {
            this.onCRUDRequested(e.type, e.data, e.search);
        } else if (e.type && !e.data && !e?.col) {
            this.onCRUDRequested(e.type, {});
        } else if (e.type && e.data && e?.col?.toString()) {
            this.onCRUDRequested(e.type, e.data, e?.col);
        }
    }
}