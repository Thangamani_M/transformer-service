import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from "@app/shared";
import { MaterialModule } from '@app/shared/material.module';
import { ReturnForRepairRequestUpdateComponent } from './return-for-repair-request-update.component';
import { ReturnForRepairRequestViewComponent } from './return-for-repair-request-view.component';
import { ReturnForRepairRequestListComponent } from './return-for-repair-request.component';
import { WINDOW_PROVIDERS } from './window.providers';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
  {
    path: '', component: ReturnForRepairRequestListComponent, canActivate:[AuthGuard],data: {title: 'Return For Repair Request'},
  },
  {
    path: 'view', component: ReturnForRepairRequestViewComponent, canActivate:[AuthGuard],data: {title: 'Return For Repair Request View'},
  },
  {
    path: 'update', component: ReturnForRepairRequestUpdateComponent,canActivate:[AuthGuard], data: {title: 'Return For Repair Request Update'},
  }
]
@NgModule({
  declarations: [ReturnForRepairRequestListComponent,ReturnForRepairRequestViewComponent,ReturnForRepairRequestUpdateComponent],
  imports: [ CommonModule,MaterialModule,FormsModule,ReactiveFormsModule,SharedModule, RouterModule.forChild(routes)],
  
  entryComponents:[],
  providers:[DatePipe,WINDOW_PROVIDERS]
})

export class ReturnForRepairRequestModule {}
