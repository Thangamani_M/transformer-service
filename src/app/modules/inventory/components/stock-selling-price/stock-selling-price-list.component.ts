import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@app/modules';
import { AppState } from '@app/reducers';
import { CrudType, IApplicationResponse, LoggedInUserModel,
  ModulesBasedApiSuffix, prepareGetRequestHttpParams,exportList, RxjsService, currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, SnackbarService} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { map, } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-stock-selling-price-list',
  templateUrl: './stock-selling-price-list.component.html'
})

export class StockSellingPriceListComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties: any;
  row: any = {}
  first: number = 0;
  otherParams;
  constructor(private crudService: CrudService, private commonService: CrudService,
    private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    public dialogService: DialogService, private router: Router,
    private store: Store<AppState>, private rxjsService: RxjsService,private snackbarService  : SnackbarService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Stock Table Selling Price",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Table Selling Price', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Stock Table Selling Price',
            dataKey: 'stockSellingPriceId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableAction: true,
            enableScheduleActionBtn: true,
            enableExportBtn: true,
            enableFieldsSearch: true,
            enablePrintBtn: true,
            printTitle: 'Stock Table Selling Price',
            printSection: 'print-section0',
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'stockSellingPriceCode', header: 'Selling Price ID', width: '200px' }, { field: 'stockCode', header: 'Stock Code', width: '200px' },
            { field: 'stockDescription', header: 'Stock Description', width: '200px' }, { field: 'sellingPrice', header: 'Selling Price', width: '200px' },
            { field: 'leadGroup', header: 'Lead Group', width: '200px' }, { field: 'district', header: 'District', width: '200px' },
            { field: 'systemType', header: 'System Type', width: '200px' }, { field: 'componentGroup', header: 'Component Group', width: '200px' },
            { field: 'spLastUpdate', header: 'SP Last Update', width: '200px' }],
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_SELLING_PRICE,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          },
        ]
      }
    }

    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.STOCK_TABLE_SELLING_PRICE]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
    }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = {
      userId: this.loggedInUserData?.userId,
      ...otherParams,
    }
    this.otherParams = otherParams;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.spLastUpdate = this.datePipe.transform(val.spLastUpdate, 'yyyy-MM-dd HH:mm:ss');
          val.sellingPrice = 'R' + ' ' + val.sellingPrice;
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;

      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row;
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.SCHEDULE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canSchedule) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(['/inventory/stock-scheduling']);
        break;
      case CrudType.Export:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
        let pageIndex = this.row && this.row["pageIndex"] ? this.row["pageIndex"] : 0;
        let pageSize = this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
        this.exportList(pageIndex, pageSize);
        break;
      default:
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/inventory', 'stock-selling-price', 'view'], {
          queryParams: {
            id: editableObject['stockSellingPriceId']
          }, skipLocationChange: true
        });
        break;
    }
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onChangeSelecedRows(event) { }


  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams, 0, pageSize, ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_SELLING_PRICE_EXPORT, this.commonService, this.rxjsService, 'Stock Selling Price');
  }
}
