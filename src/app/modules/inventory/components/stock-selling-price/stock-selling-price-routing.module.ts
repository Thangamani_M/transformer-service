import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockSellingPriceListComponent, StockSellingPriceViewComponent } from '@inventory/components/stock-selling-price';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: StockSellingPriceListComponent, data: { title: 'Stock Table Selling Price' },canActivate:[AuthGuard] },
    { path: 'list', component: StockSellingPriceListComponent, data: { title: 'Stock Table Selling Price' },canActivate:[AuthGuard] },
    { path: 'view', component: StockSellingPriceViewComponent, data: { title: 'Stock Table Selling Price View' } ,canActivate:[AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class StockSellingPriceRoutingModule { }
