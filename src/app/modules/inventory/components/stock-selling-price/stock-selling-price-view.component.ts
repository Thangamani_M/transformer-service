import { SelectionModel } from '@angular/cdk/collections';
import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, Sort } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { ComponentProperties, CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, RxjsService } from '@app/shared';
import { CrudType, sortOrder } from '@app/shared/utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { StockSellingPriceDetails } from '@modules/inventory/models/stock-selling-price.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-stock-selling-price-view',
  templateUrl: './stock-selling-price-view.component.html'
})
export class StockSellingPriceViewComponent  extends PrimeNgTableVariablesModel implements OnInit {
  displayedColumns: string[] = ['updatedDate', 'sellingPrice', 'costPrice', 'labourRate', 'materialMarkup', 'consumableMarkup', 'labourComponent', 'updatedBy', 'isActive'];
  selection = new SelectionModel<StockSellingPriceDetails>(true, []);
  primengTableConfigProperties: any;
  sellableDetailModel: any = {};
  itemSellingPriceId: string;
  StockSellingPriceviewDetail:any;
  applicationResponse: IApplicationResponse;
  stockselling: [];
  stockSellingPrice: StockSellingPriceDetails[];
  status: any = [];
  first:number = 0;
  limit: number = 25;
  skip: number = 0;
  totalLength;
  pageIndex: number = 0;
  searchText: any = { Search: '', IsAll: true };
  componentProperties = new ComponentProperties();
  dateFormat = 'MMM dd, yyyy';
  dataSource = new MatTableDataSource<StockSellingPriceDetails>();
  row: any = {}
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private datePipe: DatePipe,  private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private httpService: CrudService) {
    super();
    this.itemSellingPriceId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption: "Stock Selling Price View",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Selling Price', relativeRouterUrl: '' },{ displayName: 'Stock Selling View', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Stock Table Selling Price',
            dataKey: 'stockSellingPriceId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableAction:true,
            enableScheduleActionBtn:false,
            enableExportBtn:false,
            enableFieldsSearch: false,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            columns: [{ field: 'updatedDate', header: 'Updated Date', width: '180px' }, 
            { field: 'sellingPrice', header: 'SellingPrice', width: '150px' },
            { field: 'costPrice', header: 'CostPrice', width: '130px' }, 
            { field: 'labourRate', header: 'LabourRate', width: '130px' },
            { field: 'materialMarkup', header: 'Material Markup', width: '150px' }, 
            { field: 'consumableMarkup', header: 'Consumable Markup', width: '180px' },
            { field: 'labourComponent', header: 'Labour Component', width: '150px' }, 
            { field: 'updatedBy', header: 'Updated By', width: '150px' },
            { field: 'status', header: 'Status', width: '130px',type:'' }],
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_SELLING_PRICE,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.searchText['ItemSellingPriceId'] = this.itemSellingPriceId;
    this.dataSource.sortingDataAccessor = (data: any, sortHeaderId: string): string => {
      if (typeof data[sortHeaderId] === 'string') {
        return data[sortHeaderId].toLocaleLowerCase();
      }
      return data[sortHeaderId];
    };
    this.getSelleblePriceIdDetails().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.sellableDetailModel = response.resources;
        this.onShowValue(response.resources);
        this.getSelleblePriceId().pipe(map((res: IApplicationResponse) => {
          if (res?.resources) {
            res?.resources?.forEach(val => {
                val.updatedDate = this.datePipe.transform(val.updatedDate, 'yyyy-MM-dd HH:mm:ss');
                val.sellingPrice = 'R' + val.sellingPrice;
                    val.costPrice = 'R' + val.costPrice;
                    val.labourRate = 'R' + val.labourRate;
              return val;
            })
          }
          return res;
        }))
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.applicationResponse = response;
            this.stockselling = response.resources;
            this.dataList = response.resources;
            this.totalRecords = this.applicationResponse.totalCount;
            this.dataSource.data = this.stockselling;
            this.totalLength = this.applicationResponse.totalCount;
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        })
      }
    })
  }
  applyFilter(filterValue: string) {
    this.searchText.Search = filterValue.trim();
    this.getSelleblePriceId();
  }
  
  onShowValue(response?: any) {
    this.StockSellingPriceviewDetail = [
          { name: 'Selling Price ID', value: response ? response?.sellingPriceCode : '' },
          { name: 'Stock Code', value: response ? response?.stockCode : '' },
          { name: 'Stock Description', value: response ? response?.stockDescription : '' },
          { name: 'Selling Price', value: response ? response?.sellingPrice : '' },
          { name: 'Ownership', value: response ? response?.ownership : '' },
          { name: 'District', value: response ? response?.district : '' },
          { name: 'System Type', value: response ? response?.systemType : '' },
          { name: 'Component Group', value: response ? response?.componentGroup : '' },
          { name: 'SP Last Updated', value: (response && response?.spLastUpdated) ?  this.datePipe.transform(response?.spLastUpdated, 'yyyy-MM-dd HH:mm:ss') : ''},
          { name: 'Lead Group', value: response ? response ?.leadGroup : ''},
    ]
  }
  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
  changePage(event) {
    this.pageIndex = event.pageIndex;
    this.limit = event.pageSize;
    this.getSelleblePriceId();

  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {

    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  sortOrder(sort: Sort): void {
    this.dataSource = new MatTableDataSource(sortOrder(this.stockselling, sort));
  }

  getSelleblePriceIdDetails(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('ItemSellingPriceId', (this.itemSellingPriceId && this.itemSellingPriceId != undefined) ? this.itemSellingPriceId : '');
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_SELLING_PRICE_DETAILS, null, null, params);
  }

  getSelleblePriceId(pageIndex?: string, maximumRows?: string, search?: string, ItemSellingPriceId?: any): Observable<any> {
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.STOCK_SELLING_PRICE_HISTORY,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, maximumRows, {
        search: search ? this.searchText : '',
        ItemSellingPriceId: this.itemSellingPriceId ? this.itemSellingPriceId : ''
      }
      ));
  }
  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
     switch (type) { 
      case CrudType.GET:
         this.getSelleblePriceIdDetails().subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.sellableDetailModel = response.resources;
            this.getSelleblePriceId(row["pageIndex"], row["pageSize"]).pipe(map((res: IApplicationResponse) => {
              if (res?.resources) {
                res?.resources?.forEach(val => {
                    val.updatedDate = this.datePipe.transform(val.updatedDate, 'yyyy-MM-dd HH:mm:ss');
                  return val;
                })
              }
              return res;
            })).subscribe((response: IApplicationResponse) => {
              if (response.isSuccess) {
                this.applicationResponse = response;
                this.stockselling = response.resources;
                this.dataList = response.resources;
                this.totalRecords = this.applicationResponse.totalCount;
                this.dataSource.data = this.stockselling;
                this.totalLength = this.applicationResponse.totalCount;
                this.rxjsService.setGlobalLoaderProperty(false);
              }
            })
          }
        })
    
        break;
        default:
     }}
     onActionSubmited(e: any) {
      if (e.data && !e.search && !e?.col?.toString()) {
          this.onCRUDRequested(e.type, e.data);
      } else if (e.data && e.search && !e?.col) {
          this.onCRUDRequested(e.type, e.data, e.search);
      } else if (e.type && !e.data && !e?.col) {
          this.onCRUDRequested(e.type, {});
      } else if (e.type && e.data && e?.col?.toString()) {
          this.onCRUDRequested(e.type, e.data, e?.col);
      }
    }
    onChangeSelecedRows(event){}
}
