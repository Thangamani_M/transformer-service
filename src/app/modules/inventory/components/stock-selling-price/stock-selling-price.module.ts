import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { StockSellingPriceListComponent, StockSellingPriceRoutingModule, StockSellingPriceViewComponent } from '@inventory/components/stock-selling-price';

@NgModule({
  declarations: [StockSellingPriceListComponent, StockSellingPriceViewComponent],
  imports: [
    CommonModule,
    StockSellingPriceRoutingModule,
    ReactiveFormsModule,
    FormsModule, LayoutModule,
    MaterialModule,
    SharedModule
  ]
})
export class StockSellingPriceModule { }