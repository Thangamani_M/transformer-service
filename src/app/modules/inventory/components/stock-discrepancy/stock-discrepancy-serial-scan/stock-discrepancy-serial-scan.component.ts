import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { ConsumableStockAddEditModal, DiscrepancyDocumentsList, DiscrepancyItemSerialNumberList, InvestigationInfoAddEditModal, ReturnToSupplierAddEditModal, StockDiscrepancyAddEditModel } from '@modules/inventory/models/stock-discrepancy.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import moment from 'moment';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-stock-discrepancy-serial-scan',
  templateUrl: './stock-discrepancy-serial-scan.component.html',
  styleUrls: ['./stock-discrepancy-serial-scan.component.scss']
})
export class StockDiscrepancySerialScanComponent implements OnInit {

  stockDiscrepancyId: any;
  stockDiscrepancyItemId: any;
  stockDiscrepancySerialDetails: any;
  isNotSerialized: any;
  selectedIndex: any;
  userData:UserLogin;
  stockDiscrepancyAddEditForm: FormGroup;
  stockDiscrepancyConsumableAddEditForm: FormGroup;
  discrepancySerializeditems: FormArray;
  returnToSupplier: FormArray;
  discrepancyDocumentItemList: FormArray;
  discrepancyDocuments: FormArray;
  discrepancyStatusDropdown: any = [];
  discrepancyReasonsDropdown: any = [];
  warrantyStatusDropdown: any = [];
  documentTypesDropdown: any = [];
  faultyPartyDropdown: any = [];
  filteredSuppliers: any = [];

  selectedFiles = new Array();
  selectedFilesTemp= new Array();
  totalFileSize = 0;
  maxFileSize = 104857600; //in Bytes 100MB
  stockDiscrepancyDetails: any;
  serialNumberError: boolean = false;
  attachmentError: boolean = false;
  documentTypeError = false;
  serialBarcode: any;
  stockConsumableDetails: any = {}
  consumeSuppliers: any = [];


  isCancelOrderSuccessDialog: boolean = false;
  responseHeader: any;
  responseMessage: any;
  public formData = new FormData();
  showDuplicateError: boolean;
  duplicateError: any;
  selectedAttachment: any = '';

  serialNumberErrorMsg: any;
  successMessage: any = [];
  showSupplierError: boolean = false;
  status;
  @ViewChild('fileInput', null) fileInput: ElementRef;
  uploader;
  discrepancyItemSeriealNumberList;
  discrepancyItemSeriealNumberListTemp;
  constructor(
    private router: Router, private crudService: CrudService,private store: Store<AppState>,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private snackbarService: SnackbarService,
    private formBuilder: FormBuilder
  ) {
    this.stockDiscrepancyId = this.activatedRoute.snapshot.queryParams.id;
    this.stockDiscrepancyItemId = this.activatedRoute.snapshot.queryParams.itemId;
    this.isNotSerialized = this.activatedRoute.snapshot.queryParams.isNotSerialized;
    this.status = this.activatedRoute.snapshot.queryParams.status;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createSerialInfoAddEditForm();
    this.consumableInfoAddEditForm();
    this.getDropdown();
    this.getSerialInfoStockDetails();
    this.consumableGetItemsDetails();
  }


  /* ================ Serialized Item Post Start ================== */
  getSerialInfoStockDetails(){

    if(this.stockDiscrepancyId && this.stockDiscrepancyItemId && this.isNotSerialized == 'false'){
      let params = new HttpParams().set('StockDiscrepancyId', this.stockDiscrepancyId)
      .set('StockDiscrepancyItemId', this.stockDiscrepancyItemId).set('IsNotSerialized', 'false');
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,InventoryModuleApiSuffixModels.DISCREPANCY_ITEM_DETAILS,
        undefined, true, params).subscribe((response :IApplicationResponse) => {
          if(response.isSuccess && response.statusCode === 200){
            this.stockDiscrepancySerialDetails = response.resources;
            this.discrepancyDocumentItemList = this.getInvestigationFormArray;
            this.discrepancySerializeditems = this.getstockDiscrepancyFormArray;
            if (response.resources.discrepancyItemSeriealNumberList.length > 0) {
              this.discrepancyItemSeriealNumberList =  response.resources.discrepancyItemSeriealNumberList;
              this.discrepancyItemSeriealNumberListTemp =  response.resources.discrepancyItemSeriealNumberList;
              response.resources.discrepancyItemSeriealNumberList.forEach((discrepancy, index) => {
                let suppliers = {
                  displayName: discrepancy.supplierName,
                  id: discrepancy.supplierId,
                }

                if( discrepancy.stockDiscrepancyReasonName == 'Item Damaged' &&   discrepancy.stockDiscrepancyWarrantyName == 'In Full Warranty'){
                  discrepancy['supplierId'] = suppliers;
                  this.onSupplierAddressSelected(suppliers, index, 'get')
                }
                this.filteredSuppliers.push(suppliers);
                discrepancy['createdUserId'] = this.userData.userId;
                this.discrepancySerializeditems.push(this.createSerialInfoGroupForm(discrepancy));
              });
            }
            // else {
            //   this.discrepancySerializeditems.push(this.createSerialInfoGroupForm());
            // }
            if (response.resources.discrepancyDocumentItemList.length > 0) {
              response.resources.discrepancyDocumentItemList.forEach((element: any, index: number) => {
                this.discrepancyDocumentItemList.push(this.createInvestigationGroupForm(element));
                element.discrepancyDocuments.forEach((discrepancyDocumentsList: DiscrepancyDocumentsList, i: number) => {
                  discrepancyDocumentsList['actionedDate'] =  moment(discrepancyDocumentsList.actionedDate).format('YYYY-MM-DD hh:ss'),
                  this.discrepancyDocuments = this.getDiscrepancyDocumentsList(index);
                  this.discrepancyDocuments.insert(index, this.createDocumentsGroupForm(discrepancyDocumentsList));
                });
              });
            }
            // else {
            //   this.discrepancyDocumentItemList.push(this.createInvestigationGroupForm());
            //   this.discrepancyDocuments = this.getDiscrepancyDocumentsList(0)
            //   this.discrepancyDocuments.push(this.createDocumentsGroupForm());
            // }
            this.rxjsService.setGlobalLoaderProperty(false);

          }
          else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
            this.discrepancySerializeditems = this.getstockDiscrepancyFormArray;
            this.discrepancySerializeditems.push(this.createSerialInfoGroupForm());
            this.discrepancyDocumentItemList = this.getInvestigationFormArray;
            this.discrepancyDocumentItemList.push(this.createInvestigationGroupForm());
          }
      });
    }
    else{
      this.discrepancySerializeditems = this.getstockDiscrepancyFormArray;
      this.discrepancySerializeditems.push(this.createSerialInfoGroupForm());
      this.discrepancyDocumentItemList = this.getInvestigationFormArray;
      this.discrepancyDocumentItemList.push(this.createInvestigationGroupForm());
    }
  }
  redirectLink(referenceUrl){
    if(referenceUrl){
      this.router.navigateByUrl(referenceUrl)
    }
  }
  getDropdown(): void {

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STOCK_DISCREPANCY_REASONS)
    .subscribe((response: IApplicationResponse) => {
        if(response.resources && response.statusCode === 200){
          this.discrepancyReasonsDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
    });

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WARRANTY_STATUS)
    .subscribe((response: IApplicationResponse) => {
        if(response.resources && response.statusCode === 200){
          this.warrantyStatusDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
    });

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STOCK_DSCREPANCY_STATUS)
    .subscribe((response: IApplicationResponse) => {
        if(response.resources && response.statusCode === 200){
          this.discrepancyStatusDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
    });

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STOCK_DISCREPANCY_DOCUMENT_TYPES)
    .subscribe((response: IApplicationResponse) => {
        if(response.resources && response.statusCode === 200){
          this.documentTypesDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
    });

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_FAULTY_PARTY)
    .subscribe((response: IApplicationResponse) => {
        if(response.resources && response.statusCode === 200){
          this.faultyPartyDropdown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
    });
  }

  createSerialInfoAddEditForm(){
    let stockOrderModel = new StockDiscrepancyAddEditModel();
    this.stockDiscrepancyAddEditForm = this.formBuilder.group({
      discrepancySerializeditems: this.formBuilder.array([]),
      discrepancyDocumentItemList: this.formBuilder.array([]),
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.stockDiscrepancyAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
    });

    // this.stockDiscrepancyAddEditForm = setRequiredValidator(this.stockDiscrepancyAddEditForm, []);
  }

  get getstockDiscrepancyFormArray(): FormArray {
    if (this.stockDiscrepancyAddEditForm !== undefined) {
      return this.stockDiscrepancyAddEditForm.get("discrepancySerializeditems") as FormArray;
    }
  }

  get getInvestigationFormArray(): FormArray {
    if (this.stockDiscrepancyAddEditForm !== undefined) {
      return this.stockDiscrepancyAddEditForm.get("discrepancyDocumentItemList") as FormArray;
    }
  }

  //Create FormArray
  getDiscrepancyDocumentsList(index: number): FormArray {
    if (!this.stockDiscrepancyAddEditForm) return;
    return this.discrepancyDocumentItemList.at(index).get("discrepancyDocuments") as FormArray
  }

  /* Create FormArray controls */
  createSerialInfoGroupForm(discrepancySerializeditems?: DiscrepancyItemSerialNumberList): FormGroup {
    let serialInfoData = new DiscrepancyItemSerialNumberList(discrepancySerializeditems ? discrepancySerializeditems : undefined);
    let formControls = {};
    Object.keys(serialInfoData).forEach((key) => {
      formControls[key] = [{ value: serialInfoData[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray controls
  createInvestigationGroupForm(serviceCategoryListModel?: InvestigationInfoAddEditModal): FormGroup {
    let serviceCategoryListModelControl = new InvestigationInfoAddEditModal(serviceCategoryListModel);
    let formControls = {};
    Object.keys(serviceCategoryListModelControl).forEach((key) => {
      if (key === 'discrepancyDocuments') {
        formControls[key] = this.formBuilder.array([])
      }
      else {
        formControls[key] = [{ value: serviceCategoryListModelControl[key], disabled: false }]
      }
    });
    return this.formBuilder.group(formControls);
  }

  /* Create FormArray controls */
  createDocumentsGroupForm(discrepancyDocuments?: DiscrepancyDocumentsList): FormGroup {
    let serialInfoData = new DiscrepancyDocumentsList(discrepancyDocuments ? discrepancyDocuments : undefined);
    let formControls = {};
    Object.keys(serialInfoData).forEach((key) => {
      formControls[key] = [{ value: serialInfoData[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  inputChangeSupplierFilter(text:any, index: number){
    let otherParams = {}
    this.showSupplierError = false;
    if(text.query == null || text.query == '')return;
    otherParams['searchText'] = text.query;
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_AUTOCOMPLETE, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.filteredSuppliers = [];
          this.filteredSuppliers = response.resources;
          if(response.resources.length == 0){
            this.showSupplierError = true;
            this.selectedIndex = index;
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  /* Supplier Address dropdown */
  onSupplierAddressSelected(obj: any, index: number, type: any){
    this.showSupplierError = false;
    if(obj.id == null)return;
    let params = new HttpParams().set('supplierId', obj.id);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIER_ADDRESS, undefined, true, params)
    .subscribe((response: IApplicationResponse) => {
      if(response.resources && response.statusCode === 200){
        this.getstockDiscrepancyFormArray.controls[index].get('serializedSuppliers').patchValue(response.resources);
        type == 'dropdown' ? this.getstockDiscrepancyFormArray.controls[index].get('supplierAddressId').patchValue('') : '';

       if(!this.discrepancySerializeditems.controls[index].get('supplierAddressId').value) {
         this.getstockDiscrepancyFormArray.controls[index].get('supplierAddressId').patchValue(response.resources[0].supplierAddressId);
       }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  // Discrepancy reasons change event
  selectedReasons(index: number){
    let reasons = this.discrepancyReasonsDropdown.find(x => x.id == this.discrepancySerializeditems.controls[index].get('stockDiscrepancyReasonId').value);
    if(reasons != undefined){
      if(reasons.displayName != "Item Missing" && reasons.displayName != "Item Found"){
        this.discrepancySerializeditems.controls[index].get('supplierId').enable();
        this.discrepancySerializeditems.controls[index].get('supplierAddressId').enable();
      }
      if(reasons.displayName == "Item Missing"){
        this.discrepancySerializeditems.controls[index].get('stockDiscrepancyActionName').patchValue('Stock Adjustment');
        this.discrepancySerializeditems.controls[index].get('itemStatusName').patchValue('Item Disposal');
        this.discrepancySerializeditems.controls[index].get('supplierId').disable();
        this.discrepancySerializeditems.controls[index].get('supplierAddressId').disable();
        this.discrepancySerializeditems.controls[index].get('supplierId').setValue(null);
        this.discrepancySerializeditems.controls[index].get('supplierAddressId').setValue(null);
      }
      else if(reasons.displayName == "Item Found"){
        this.discrepancySerializeditems.controls[index].get('stockDiscrepancyActionName').patchValue('Item Returned');
        this.discrepancySerializeditems.controls[index].get('itemStatusName').patchValue('Item Returned');
        this.discrepancySerializeditems.controls[index].get('supplierId').disable();
        this.discrepancySerializeditems.controls[index].get('supplierAddressId').disable();
        this.discrepancySerializeditems.controls[index].get('supplierId').setValue(null);
        this.discrepancySerializeditems.controls[index].get('supplierAddressId').setValue(null);
      }
      else{
        this.selectedWarranty(index);
      }
    }
    this.setSupplier(index);
  }
  setSupplier(index){

    let stockDiscrepancyReason =  this.discrepancyReasonsDropdown.filter(x=>x.id==this.discrepancySerializeditems.controls[index].get('stockDiscrepancyReasonId').value);
    let stockDiscrepancyWarranty =  this.warrantyStatusDropdown.filter(x=>x.id==this.discrepancySerializeditems.controls[index].get('stockDiscrepancyWarrantyId').value);
    if( (stockDiscrepancyReason && stockDiscrepancyReason[0].displayName == 'Item Damaged')  &&  (stockDiscrepancyWarranty && stockDiscrepancyWarranty[0].displayName == 'In Full Warranty')){
      let suppliers = {
        displayName: this.discrepancyItemSeriealNumberListTemp[index].supplierName,
        id: this.discrepancyItemSeriealNumberListTemp[index].supplierId,
      }

      this.discrepancySerializeditems.controls[index].get('supplierId').setValue(suppliers);
      this.onSupplierAddressSelected(suppliers, index, 'get')
    }
  }
  // Warranty status change event
  selectedWarranty(index: number){
    let warranty = this.discrepancySerializeditems.controls[index].get('stockDiscrepancyWarrantyId').value;
    let reasons = this.discrepancySerializeditems.controls[index].get('stockDiscrepancyReasonId').value;
    if((warranty != '' && warranty != null) && (reasons != '' && reasons != null)){
      let reasons = this.discrepancyReasonsDropdown.find(x => x.id == this.discrepancySerializeditems.controls[index].get('stockDiscrepancyReasonId').value);
      let statuses = this.warrantyStatusDropdown.find(y => y.id == this.discrepancySerializeditems.controls[index].get('stockDiscrepancyWarrantyId').value);
      if(statuses != undefined && reasons.displayName === "Item Damaged"){
        if(statuses.displayName == "Out of Warranty" || statuses.displayName == 'In Customer Warranty'){
          this.discrepancySerializeditems.controls[index].get('supplierId').disable();
          this.discrepancySerializeditems.controls[index].get('supplierAddressId').disable();
          this.discrepancySerializeditems.controls[index].get('supplierId').setValue(null);
          this.discrepancySerializeditems.controls[index].get('supplierAddressId').setValue(null);
        }else {
          this.discrepancySerializeditems.controls[index].get('supplierId').enable();
          this.discrepancySerializeditems.controls[index].get('supplierAddressId').enable();
        }
        if(statuses.displayName == "In Full Warranty" || statuses.displayName.toLowerCase() == "In Full Warranty".toLowerCase()){
          this.discrepancySerializeditems.controls[index].get('stockDiscrepancyActionName').patchValue('Return to Supplier');
          this.discrepancySerializeditems.controls[index].get('itemStatusName').patchValue('For Supplier');
        }
        else{
          this.discrepancySerializeditems.controls[index].get('stockDiscrepancyActionName').patchValue('Return for Repair');
          this.discrepancySerializeditems.controls[index].get('itemStatusName').patchValue('For Testing');
        }
      }
    }
    this.setSupplier(index);
  }

  // onFileChange uploadFiles
  uploadFiles(event) {

    this.selectedFiles = [];
    const supportedExtensions = ['jpeg', 'jpg', 'png', 'gif','pdf','doc','docx','xls','xlsx'];
    for (var i = 0; i < event.target.files.length; i++) {
      let selectedFile = event.target.files[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if(supportedExtensions.includes(extension)) {
        //this.fileInput.nativeElement.value = '';
        this.selectedFiles.push(event.target.files[i]);
        this.selectedFilesTemp.push(event.target.files[i]);
        this.totalFileSize += event.target.files[i].size;
        this.selectedAttachment = event.target.files[i].name+'_'+(new Date());
      }
      else {
        this.snackbarService.openSnackbar("Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx", ResponseMessageTypes.WARNING);
      }
    }
    this.fileInput.nativeElement.value = '';
  }

  createInvestigationInfo(){

    this.serialNumberError = false;
    this.attachmentError = false;
    this.documentTypeError = false;
    this.serialNumberErrorMsg = '';

    this.rxjsService.setFormChangeDetectionProperty(true);
    let serialvalue = this.stockDiscrepancyAddEditForm.get('serialNumberBarcode').value;
    let stockVal = this.getstockDiscrepancyFormArray.value.find(x => x.serialNumber == serialvalue);

    if(this.stockDiscrepancyAddEditForm.get('documentType').value == ''){
      this.documentTypeError = true;
      return;
    }

    if(this.stockDiscrepancyAddEditForm.get('serialNumberBarcode').value == ''){
      this.serialNumberError = true;
      this.serialNumberErrorMsg = 'Serial number is required';
      return;
    }

    if(stockVal == undefined || stockVal == null){
      this.serialNumberError = true;
      this.serialNumberErrorMsg = 'Serial number is wrong';
      return;
    }

    if(this.selectedFiles.length <= 0){
      this.attachmentError = true;
      return;
    }

    if(this.getstockDiscrepancyFormArray.value.find(x => x.serialNumber == serialvalue)){

        let addDiscrepancy: DiscrepancyDocumentsList = {
          "stockDiscrepancyDocumentId": null,
          "stockDiscrepancyItemDetailId": stockVal.stockDiscrepancyItemDetailId,
          "stockDiscrepancyDocumentTypeId": this.stockDiscrepancyAddEditForm.controls['documentType'].value,
          "docName": this.selectedFiles[0].name,
          "path": null,
          "actionedBy": this.userData.displayName,
          "actionedByUserId": this.userData.userId,
          "actionedDate": moment().format('YYYY-MM-DD hh:ss'),
          "comments": null,
          "faultyPartyId": null,
          "createdUserId": this.userData.userId
      };

      let parentArray : InvestigationInfoAddEditModal =  {
        'serialNumber': this.stockDiscrepancyAddEditForm.get('serialNumberBarcode').value,
        'stockDiscrepancyItemDetailId': stockVal.stockDiscrepancyItemDetailId,
        // 'discrepancyDocuments': addDiscrepancy[desc]
      };

      let oldItems: boolean = false;
      this.getInvestigationFormArray.value.forEach((element, index) => {
        if(element.serialNumber == serialvalue){
          oldItems = true;
          this.discrepancyDocuments = this.getDiscrepancyDocumentsList(index);
          this.discrepancyDocuments.insert(index, this.createDocumentsGroupForm(addDiscrepancy));
        }
      });

      if(!oldItems) {
        this.discrepancyDocumentItemList = this.getInvestigationFormArray;
        this.discrepancyDocumentItemList.insert(0, this.createInvestigationGroupForm(parentArray));
        this.discrepancyDocuments = this.getDiscrepancyDocumentsList(0);
        this.discrepancyDocuments.insert(0, this.createDocumentsGroupForm(addDiscrepancy));
      }

      this.stockDiscrepancyAddEditForm.get('serialNumberBarcode').patchValue('');
      this.stockDiscrepancyAddEditForm.get('documentType').patchValue('');
      this.selectedFiles = [];
      this.selectedAttachment = '';
    }
    else {
      // this.discrepancyDocumentItemList = this.getInvestigationFormArray;
      // this.discrepancyDocumentItemList.push(this.createInvestigationGroupForm());
    }
  }

  removeSerialNumber(i?: number){
    if(i !== undefined) {
      this.getstockDiscrepancyFormArray.removeAt(i);
    }
  }

  successDialog(){}

  checkScanItem() {}

  onTabChange(e) {}

  successDialogOpen(){
    if(this.successMessage.length > 1){
      this.successMessage.splice(0, 1);
    }
    else {
      this.isCancelOrderSuccessDialog = false;
      this.navigateToViewPage();
    }
  }

  onSubmit(){

    this.rxjsService.setFormChangeDetectionProperty(true);
    if(this.stockDiscrepancyAddEditForm.invalid){
      return;
    }

    let documents = [];

    this.getInvestigationFormArray.value.forEach((key) => {
      key.discrepancyDocuments.forEach(element => {
        documents.push(element);
      });
    });

    let discrepancyArr = [];
    this.getstockDiscrepancyFormArray.value.forEach(key => {
      let tmp = {}
      tmp['stockDiscrepancyItemid'] = key.stockDiscrepancyItemid;
      tmp['stockDiscrepancyItemDetailId'] = key.stockDiscrepancyItemDetailId;
      tmp['serialNumber'] = key.serialNumber;
      tmp['stockDiscrepancyReasonId'] = key.stockDiscrepancyReasonId;
      tmp['stockDiscrepancyActionName'] = key.stockDiscrepancyActionName;
      tmp['stockDiscrepancyWarrantyId'] = key.stockDiscrepancyWarrantyId;
      tmp['itemStatusName'] = key.itemStatusName;
      tmp['supplierAddressId'] = key.supplierAddressId;
      tmp['createdUserId'] = key.createdUserId;
      tmp['supplierId'] = key.supplierId?.id;
      tmp['faultyPartyId'] = key.faultyPartyId;
      tmp['comment'] = key.comment;
      discrepancyArr.push(tmp);
    });

    // if(documents.length == 0){
    //   this.snackbarService.openSnackbar('Supporting documents cannot be empty', ResponseMessageTypes.WARNING);
    //   return;
    // }

    let formData = new FormData();
    let postData;


    this.selectedFilesTemp.forEach((ele)=>{
      formData.append("file", ele, ele['name']);
    })
   
    formData.append("serializedDetails", JSON.stringify(postData));
    

    const submit$ = this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.DISCREPANCY_INVESTIGATION_POST,
      formData
    );
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.successMessage = response.resources;
        this.isCancelOrderSuccessDialog = true;
      }
    });
  }
  /* ============= Serialized Item Post End ==============*/

  /* ============= Consumable Item Post Start ============= */

  consumableGetItemsDetails(){
    if(this.stockDiscrepancyId && this.stockDiscrepancyItemId && this.isNotSerialized == 'true'){
      let params = new HttpParams().set('StockDiscrepancyId', this.stockDiscrepancyId)
      .set('StockDiscrepancyItemId', this.stockDiscrepancyItemId).set('IsNotSerialized', 'true');
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,InventoryModuleApiSuffixModels.DISCREPANCY_CONSUMABLE_ITEM_DETAILS,
        undefined, true, params).subscribe((response :IApplicationResponse) => {
          if(response.isSuccess && response.statusCode === 200){
            this.stockConsumableDetails = response.resources;
            let consumable = response.resources.discrepancyConsumableItemList;
            this.stockDiscrepancyConsumableAddEditForm.patchValue(consumable);
            this.returnToSupplier = this.getReturnToSupplierFormArray;
            if (response.resources.returnToSupplier.length > 0) {
              response.resources.returnToSupplier.forEach((supp, index) => {
                let suppliers = {
                  displayName: supp.supplierName,
                  id: supp.supplierId,
                }
                this.consumeSuppliers = suppliers;
                supp['supplierId'] = suppliers;
                supp['createdUserId'] = this.userData.userId;
                this.onConsumeSupplierAddress(suppliers, index)
                this.returnToSupplier.push(this.createReturnToSupplierGroupForm(supp));
              });
            }
            else {
              this.returnToSupplier.push(this.createReturnToSupplierGroupForm());
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          }
      });
    }
  }

  consumableInfoAddEditForm(){
    let consumable = new ConsumableStockAddEditModal();
    this.stockDiscrepancyConsumableAddEditForm = this.formBuilder.group({
      returnToSupplier: this.formBuilder.array([])
    });
    Object.keys(consumable).forEach((key) => {
      this.stockDiscrepancyConsumableAddEditForm.addControl(key, new FormControl(consumable[key]));
    });
  }

  get getReturnToSupplierFormArray(): FormArray {
    if (!this.stockDiscrepancyConsumableAddEditForm)return;
    return this.stockDiscrepancyConsumableAddEditForm.get("returnToSupplier") as FormArray;
  }

  /* Create FormArray controls */
  createReturnToSupplierGroupForm(consumableDetails?: ReturnToSupplierAddEditModal): FormGroup {
    let consumableInfoData = new ReturnToSupplierAddEditModal(consumableDetails ? consumableDetails : undefined);
    let formControls = {};
    Object.keys(consumableInfoData).forEach((key) => {
      formControls[key] = [{ value: consumableInfoData[key], disabled: false }]
    });
    return this.formBuilder.group(formControls);
  }

  changeConsumeSuppliers(type:any, text:any){
    let otherParams = {}
    if(text.query == null || text.query == '')return;
    otherParams['searchText'] = text.query;
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_AUTOCOMPLETE, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.resources.length > 0) {
        this.consumeSuppliers = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  /* Supplier Address dropdown */
  onConsumeSupplierAddress(obj: any, index: number){
    if(obj){
      if(obj.id == null)return;
      let params = new HttpParams().set('supplierId', obj.id);
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIER_ADDRESS, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if(response.resources && response.statusCode === 200){
          this.getReturnToSupplierFormArray.controls[index].get('supplierAddressList').patchValue(response.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  /* Add items */
  addConsumableItemsGroup(): void {
    if (this.stockDiscrepancyConsumableAddEditForm.invalid) return;
    this.returnToSupplier = this.getReturnToSupplierFormArray;
    let groupData = new ReturnToSupplierAddEditModal();
    this.returnToSupplier.insert(0, this.createReturnToSupplierGroupForm(groupData));
  }

  /* Remove items */
  removeConsumableGroup(i?: number) {
    if(i !== undefined) {
      this.getReturnToSupplierFormArray.removeAt(i);
    }
  }

  onConsumableSubmit(){

    if(this.stockDiscrepancyConsumableAddEditForm.invalid){
      return;
    }

    let suppArray = [];
    if(this.getReturnToSupplierFormArray.value != undefined && this.getReturnToSupplierFormArray.value.length > 0){

      this.getReturnToSupplierFormArray.value.forEach((key, i) => {
        // delete key.supplierAddressList;
        key['stockDiscrepancyItemid'] = this.stockDiscrepancyItemId;
        key['stockDiscrepancyItemDetailId'] = this.stockConsumableDetails.discrepancyConsumableItemList.stockDiscrepancyItemDetailId;
        key['supplierId'] = key?.supplierId?.id;
        key['supplierName'] = key?.supplierId?.displayName;
      });
    }


    const consumable = {
      "stockDiscrepancyItemid":  this.stockDiscrepancyConsumableAddEditForm.get('stockDiscrepancyItemid').value,
      "stockDiscrepancyItemDetailId": this.stockDiscrepancyConsumableAddEditForm.get('stockDiscrepancyItemDetailId').value,
      "discrepancyQty": this.stockDiscrepancyConsumableAddEditForm.get('discrepancyQty').value,
      "itemMissedQty": this.stockDiscrepancyConsumableAddEditForm.get('itemMissedQty').value,
      "itemDamagedQty": this.stockDiscrepancyConsumableAddEditForm.get('itemDamagedQty').value,
      "itemFoundQty": this.stockDiscrepancyConsumableAddEditForm.get('itemFoundQty').value,
      "returnToSupplierQty": this.stockDiscrepancyConsumableAddEditForm.get('returnToSupplierQty').value,
      "returnToRepairQty": this.stockDiscrepancyConsumableAddEditForm.get('returnToRepairQty').value,
      "totalWriteoffQty": this.stockDiscrepancyConsumableAddEditForm.get('totalWriteoffQty').value,
      "references": this.stockDiscrepancyConsumableAddEditForm.get('references').value,
      "createdUserId": this.userData.userId
    };

    let postData = {};
    postData = { _discrepancyConsumableItemPostDetails : consumable, _returnToSupplier: this.getReturnToSupplierFormArray.value }
    this.rxjsService.setFormChangeDetectionProperty(true);

    const submit$ = this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.DISCREPANCY_CONSUMABLE_ITEM_INVESTIGATION_POST,
      postData
    )
    submit$.pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        if(this.isNotSerialized == 'true'){
          // this.navigateToViewPage();
        }
      }
    });
  }
  /* ============= Consuamble Item Post End ==============*/

  /* Naviagation */

  navigateToList(){
    this.router.navigate(['/inventory/stock-discrepancy'], { skipLocationChange:true });
  }

  navigateToViewPage(){
    this.router.navigate(['/inventory/stock-discrepancy/view'], {
      queryParams: {
        id: this.stockDiscrepancyId,
      }, skipLocationChange:true
    });
  }
  copyToClipboard(element) {
    element.select();
    document.execCommand('copy');
  }

}
