import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { StockDiscrepancyListComponent } from './stock-discrepancy-list/stock-discrepancy-list.component';
import { StockDiscrepancyRoutingModule } from './stock-discrepancy-routing.module';
import { StockDiscrepancySerialScanComponent } from './stock-discrepancy-serial-scan/stock-discrepancy-serial-scan.component';
import { StockDiscrepancyViewComponent } from './stock-discrepancy-view/stock-discrepancy-view.component';
@NgModule({
    declarations: [
        StockDiscrepancyListComponent,
        StockDiscrepancyViewComponent,
        StockDiscrepancySerialScanComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule, FormsModule,
        StockDiscrepancyRoutingModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
    ],
    entryComponents: []
})

export class StockDiscrepancyModule { }
