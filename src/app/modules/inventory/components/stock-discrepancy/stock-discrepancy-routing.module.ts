import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StockDiscrepancyListComponent } from './stock-discrepancy-list/stock-discrepancy-list.component';
import { StockDiscrepancySerialScanComponent } from './stock-discrepancy-serial-scan/stock-discrepancy-serial-scan.component';
import { StockDiscrepancyViewComponent } from './stock-discrepancy-view/stock-discrepancy-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: StockDiscrepancyListComponent, canActivate: [AuthGuard], data: { title: 'Stock Discrepancy List' } },
  { path: 'view', component: StockDiscrepancyViewComponent, canActivate: [AuthGuard], data: { title: 'Stock Discrepancy View' } },
  { path: 'serial-info', component: StockDiscrepancySerialScanComponent, canActivate: [AuthGuard], data: { title: 'Stock Discrepancy Serial Info' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class StockDiscrepancyRoutingModule { }
