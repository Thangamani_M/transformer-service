import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-stock-discrepancy-view',
  templateUrl: './stock-discrepancy-view.component.html',
  styleUrls: ['./stock-discrepancy-view.component.scss']
})
export class StockDiscrepancyViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  stockDiscrepancyId: any;
  primengTableConfigProperties: any
  stockDiscrepancyDetails: any;
  userData: UserLogin;
  showUpdate: boolean = false;
  StockDiscrepancyviewDetail:any;
  status;
  constructor(private router: Router, private crudService: CrudService, private store: Store<AppState>,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private snackbarService: SnackbarService, private datePipe: DatePipe
  ) {
    super()
    this.stockDiscrepancyId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption:'View Stock Discrepancy',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Stock Discrepancy', relativeRouterUrl: '' }, { displayName: 'Stock Discrepancy List', relativeRouterUrl: '/inventory/stock-discrepancy' }, { displayName: 'View Stock Discrepancy', relativeRouterUrl: '', }],
      tableComponentConfigs: {
          tabsList: [
              {
                  enableAction: true,
                  enableEditActionBtn: false,
                  enableBreadCrumb: true,
                  enableExportBtn: false,
                  enablePrintBtn: false,
                  printTitle: 'Daily Staging Bay Report',
                  printSection: 'print-section',
              }
          ]
      }
  }
  this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  ngOnInit(): void {
    this.getStockDiscrepancyDetailsById();
  }

  getStockDiscrepancyDetailsById() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.DISCREPANCY,
      this.stockDiscrepancyId).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.stockDiscrepancyDetails = response.resources;
          this.onShowValue(response.resources);
          this.status = response.resources.status;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }
  onShowValue(response?: any) {
    this.StockDiscrepancyviewDetail = [
          { name: 'Discrepancy ID', value: response?.stockDiscrepancyNumber},
          { name: 'Stock Order Number', value: response?.stockOrderNumber},
          { name: 'Reference Number', value: response?.referenceNumber},
          { name: 'Warehouse', value: response?.warehouseName},
          { name: 'Storage Location', value: response?.storageLocationName},
          { name: 'Overridden By', value: response?.overRideBy},
          { name: 'Initiated by', value: response?.createdBy},
          { name: 'Initiated date', value:  this.datePipe.transform(response?.createdDate, 'yyyy-MM-dd HH:mm:ss')},
          { name: 'status', value: response?.status, statusClass: response ? response?.cssClass : '' },  
    ]
  }

  navigateToSerialScanPage(stockDiscrepancyItemid: string, isNotSerialized: boolean) {
    this.router.navigate(['/inventory', 'stock-discrepancy', 'serial-info'], {
      queryParams: {
        id: this.stockDiscrepancyId,
        itemId: stockDiscrepancyItemid,
        isNotSerialized: isNotSerialized,
        status : this.status,
      }, skipLocationChange: true
    });
  }

}
