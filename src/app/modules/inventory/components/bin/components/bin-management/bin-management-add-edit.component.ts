import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { BinManagementMasterModel, BinManagementModel } from "@app/modules/inventory/models/bin-management.model";
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, setRequiredValidator } from '@app/shared';
import { ResponseMessageTypes } from "@app/shared/enums";
import { AlertService, CrudService, HttpCancelService, RxjsService, SnackbarService } from "@app/shared/services";
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams } from "@app/shared/utils";
import { IApplicationResponse } from "@app/shared/utils/interfaces.utils";
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from "rxjs";
@Component({
  selector: "app-bin-management-add-edit",
  templateUrl: "./bin-management-add-edit.component.html",
  styleUrls: ["./bin-management.component.scss"]
})

export class BinManagementAddEditComponent implements OnInit {
  binManagementModel: BinManagementModel
  binAddEditForm: FormGroup;
  binManagementDetailList: FormArray;
  isButtondisabled = false;
  errorMessage: string;
  locationBinConfigId: any;
  warehouseList = [];
  subLocationList = [];
  IsDisabled: boolean = false;
  header: string;
  isLoading = false;
  btnName: string;
  warehouseName: string;
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  binManagementdetails: {};
  loggedUser: UserLogin;
  constructor(private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private crudService: CrudService,
    private snackbarService: SnackbarService, private router: Router, private rxjsService: RxjsService, private alertService: AlertService,
    private store: Store<AppState>, private httpCancelService: HttpCancelService, private changeDetection: ChangeDetectorRef) {
    this.locationBinConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.createBinManagementMasterAddForm();
    this.LoadWarehouse();

    if (this.locationBinConfigId) {
      this.header = "Update";
      this.btnName = 'Update';
      this.getBinManagementMasterById().subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200) {
          let binManagementMaster = new BinManagementMasterModel(response.resources);
          this.binManagementdetails = binManagementMaster;
          this.binAddEditForm.patchValue(binManagementMaster);

          this.binAddEditForm.patchValue({
            warehouseId: binManagementMaster.warehouseId,
            locationId: binManagementMaster.locationId
          });

          this.LoadSubLocation('');
          this.LoadExistingBins();

        } else {
          this.alertService.processAlert(response);
        }
      });
      this.IsDisabled = true;
    }
    else {
      this.header = "Create";
      this.btnName = 'Save';
      this.binAddEditForm.patchValue({
        warehouseId: "",
        locationId: "",
        noOfBin: ""
      });
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  ngAfterViewInit(): void {
    this.changeDetection.detectChanges();
  }

  createbinAddEditFormGroup(binManagementModel?: BinManagementModel): FormGroup {
    let binManagementDetailListModelData = new BinManagementModel(binManagementModel);
    let formControls = {};
    Object.keys(binManagementDetailListModelData).forEach((key) => {
      formControls[key] = [binManagementDetailListModelData[key]]
    });
    return this.formBuilder.group(formControls);
  }

  //Get Master
  getBinManagementMasterById(): Observable<IApplicationResponse> {
    if (this.locationBinConfigId !== undefined) {
      return this.crudService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.BIN_MANAGEMENT,
        this.locationBinConfigId
      );
    }
  }

  //Get Details
  getBinManagementDetailById() {
    if (this.binAddEditForm.controls.warehouseId.value !== undefined && this.binAddEditForm.controls.locationId.value !== undefined) {
      return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.BIN_MANAGEMENT_BY_WAREHOUSE_LOCATION, undefined, true, prepareGetRequestHttpParams(null, null, {
        warehouseId: this.binAddEditForm.controls.warehouseId.value,
        locationId: this.binAddEditForm.controls.locationId.value
      }))
    }
  }

  //Get Auto Details
  getBinManagementDetailAutoById() {
    if (this.binAddEditForm.controls.warehouseId.value !== undefined && this.binAddEditForm.controls.locationId.value !== undefined && this.binAddEditForm.controls.noOfBin.value !== undefined) {
      return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.BIN_MANAGEMENT_AUTO_LIST, undefined, true, prepareGetRequestHttpParams(null, null, {
        warehouseId: this.binAddEditForm.controls.warehouseId.value,
        locationId: this.binAddEditForm.controls.locationId.value,
        noOfBin: this.binAddEditForm.controls.noOfBin.value,
        createdUserId: this.loggedUser.userId
      }))
    }
  }

  //Warehouse Dropdown
  LoadWarehouse() {
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.loggedUser?.userId })).subscribe((response) => {
      if (response.statusCode == 200) {
        this.warehouseList = response.resources;
      } else {
        this.alertService.processAlert(response);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  //SubLocation Dropdown
  LoadSubLocation(warehouseName) {
    if (this.binAddEditForm.controls.warehouseId.value !== undefined) {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STORAGE_LOCATION_WAREHOUSE, undefined, true, prepareGetRequestHttpParams(null, null, {
        warehouseId: this.binAddEditForm.controls.warehouseId.value
      })).subscribe((res) => {
        this.subLocationList = res.resources;
        if (this.subLocationList.length == 0 && !warehouseName && warehouseName !== '') {
          this.snackbarService.openSnackbar("Location not available for " + warehouseName, ResponseMessageTypes.WARNING);
        }
      })
      this.binAddEditForm.patchValue({ noOfBin: 0 });
    }
    else {
      this.subLocationList = [];
    }
  }

  //Get existing bins
  LoadExistingBins() {
    this.getBinManagementDetailById().subscribe((responseDetails: IApplicationResponse) => {
      if (responseDetails.statusCode == 200) {
        this.binManagementDetailList = this.getBinManagementDetailList;
        if (responseDetails.resources.length > 0) {
          responseDetails.resources.forEach((binManagement: BinManagementModel) => {
            this.binManagementDetailList.push(this.createBinManagementDetailListFormGroup(binManagement));
          });
        }
        this.binAddEditForm.patchValue({ noOfBin: responseDetails.resources.length });
      } else {
        this.alertService.processAlert(responseDetails);
      }
    });
  }

  onWarehouseChange(value, event) {
    this.warehouseName = event.target.options[event.target.selectedIndex].text;
    if (value) {
      this.binAddEditForm.patchValue({
        warehouseId: value,
        locationId: "",
        noOfBin: 0
      });
      this.binManagementDetailList = this.getBinManagementDetailList;
      this.binManagementDetailList.clear();
      this.LoadSubLocation(this.warehouseName);
    }
  }

  onLocationChange(value, event) {
    if (value) {
      this.binAddEditForm.patchValue({
        locationId: value,
        noOfBin: 0
      });
      this.binManagementDetailList = this.getBinManagementDetailList;
      this.binManagementDetailList.clear();
      this.LoadExistingBins();
    }
  }

  createBinManagementMasterAddForm(): void {
    let binManagementMasterModel = new BinManagementMasterModel();
    this.binAddEditForm = this.formBuilder.group({
      binManagementDetailList: this.formBuilder.array([])
    });
    Object.keys(binManagementMasterModel).forEach((key) => {
      this.binAddEditForm.addControl(key, new FormControl(binManagementMasterModel[key]));
    });
    this.binAddEditForm = setRequiredValidator(this.binAddEditForm, ["warehouseId", "locationId", "noOfBin"]);
  }

  onDescriptionChange(obj) {
    obj.patchValue({ altered: true });
  }

  //Create FormArray
  get getBinManagementDetailList(): FormArray {
    if (!this.binAddEditForm) return;
    return this.binAddEditForm.get("binManagementDetailList") as FormArray;
  }

  //Create FormArray controls
  createBinManagementDetailListFormGroup(binManagementDetailListModel?: BinManagementModel): FormGroup {
    let binManagementDetailListModelData = new BinManagementModel(binManagementDetailListModel ? binManagementDetailListModel : undefined);
    let formControls = {};
    Object.keys(binManagementDetailListModelData).forEach((key) => {

      formControls[key] = [{
        value: binManagementDetailListModelData[key],
        disabled: binManagementDetailListModel && (key === 'bincode') && binManagementDetailListModelData[key] !== '' ? true : false
      },
      (key === 'binManagementDetailListName' ? [Validators.pattern('^[a-zA-Z \-\']+')] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  onBinCountChange(): void {
    if (this.binAddEditForm.invalid) {
      this.binAddEditForm.markAllAsTouched();
      return;
    }

    if (this.getBinManagementDetailList.length > this.binAddEditForm.controls.noOfBin.value) {
      this.binAddEditForm.patchValue({
        noOfBin: this.getBinManagementDetailList.length
      });

      this.snackbarService.openSnackbar("No of bin cannot be reduced. It can be increased.", ResponseMessageTypes.WARNING);
    }
    else {
      this.getBinManagementAuto();
    }
  }

  getBinManagementAuto() {
    this.getBinManagementDetailAutoById().subscribe((responseDetails: IApplicationResponse) => {
      if (responseDetails.statusCode == 200) {
        this.binManagementDetailList = this.getBinManagementDetailList;
        this.binManagementDetailList.clear();
        if (responseDetails.resources.length > 0) {
          responseDetails.resources.forEach((binManagement: BinManagementModel) => {
            this.binManagementDetailList.push(this.createBinManagementDetailListFormGroup(binManagement));
          });
        }
        this.binAddEditForm.patchValue({ noOfBin: responseDetails.resources.length });
      } else {
        this.alertService.processAlert(responseDetails);
      }
    });
  }

  onSubmit(): void {
    if (this.binAddEditForm.invalid) {
      return;
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    var putRequest = this.binAddEditForm.value.binManagementDetailList;
    var putFilteredRequest = putRequest.filter(x => x.altered === true);
    putFilteredRequest.forEach(x => x.modifiedUserId = this.loggedUser.userId);

    if (putFilteredRequest.length > 0) {
      let crudService: Observable<IApplicationResponse> =
        this.crudService.update(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.BIN_MANAGEMENT,
          putFilteredRequest
        );
      crudService.subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.redirectToListPage();
        }
      });
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  redirectToListPage(): void {
    this.router.navigateByUrl("inventory/bin");
  }
}
