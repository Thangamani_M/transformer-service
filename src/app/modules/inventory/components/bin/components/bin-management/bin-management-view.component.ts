import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { BinManagementModel } from '@modules/inventory/models/bin-management.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-bin-management-view',
  templateUrl: './bin-management-view.component.html',
  styleUrls: ['./bin-management-view.component.scss']
})
export class BinManagementViewComponent implements OnInit {
  binManagementForm: FormGroup
  locationBinConfigId: any;
  binManagement: FormArray;
  bincode: any;
  qrCode: any;
  description: any;
  warehouseName: any;
  storageLocationName: any;
  availableQuantity: any;
  constructor(private router: Router, private formBuilder: FormBuilder, private crudService: CrudService, private activatedRoute: ActivatedRoute) {
    this.locationBinConfigId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    this.createBinManagementForm();
    if (this.locationBinConfigId) {
      this.getBinManagementById().subscribe((response: IApplicationResponse) => {
        this.binManagement = this.getBinManagement;
        let binManagement = new BinManagementModel(response.resources);
        this.bincode = binManagement.bincode;
        this.qrCode = binManagement.qrCode;
        this.description = binManagement.description;
        this.warehouseName = binManagement.warehouseName;
        this.storageLocationName = binManagement.storageLocationName;
        this.availableQuantity = binManagement.availableQuantity;
        this.binManagement.push(this.createBinManagementFormGroup(binManagement));
      })
    }
  }

  //Get Details 
  getBinManagementById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.BIN_MANAGEMENT,
      this.locationBinConfigId
    );
  }

  createBinManagementForm(): void {
    this.binManagementForm = this.formBuilder.group({
      binManagement: this.formBuilder.array([])
    });
  }

  //Create FormArray
  get getBinManagement(): FormArray {
    if (!this.binManagementForm) return null;
    return this.binManagementForm.get("binManagement") as FormArray;
  }

  //Create FormArray controls
  createBinManagementFormGroup(binManagement?: BinManagementModel): FormGroup {

    let binManagementData = new BinManagementModel(binManagement ? binManagement : undefined);
    let formControls = {};
    Object.keys(binManagementData).forEach((key) => {
      formControls[key] = [{ value: binManagementData[key], disabled: binManagement && (key == 'warehouseId' || key == 'locationIds') && binManagementData[key] !== '' ? true : false },
      (key === 'warehouseId' ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  NavigateToEdit() {
    this.router.navigate(['/inventory/bin/add-edit/'], { queryParams: { id: this.locationBinConfigId }, skipLocationChange: true });
  }
}
