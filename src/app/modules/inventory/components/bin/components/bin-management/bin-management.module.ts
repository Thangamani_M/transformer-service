import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { BinManagementAddEditComponent, BinManagementComponent, BinManagementViewComponent } from '@inventory-components/bin';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { BinManagementRoutingModule } from './bin-management-routing.module';

@NgModule({
  declarations: [BinManagementComponent, BinManagementAddEditComponent, BinManagementViewComponent],
  imports: [
    CommonModule,
    BinManagementRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ],
})
export class BinManagementModule { }
