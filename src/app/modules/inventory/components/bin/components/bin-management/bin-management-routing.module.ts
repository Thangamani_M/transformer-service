import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BinManagementComponent, BinManagementAddEditComponent } from '@inventory-components/bin';
import { BinManagementViewComponent } from './bin-management-view.component';
const routes: Routes = [
    { path: '', component: BinManagementComponent, data: { title: 'Bin Management' } },
    { path: 'add-edit', component: BinManagementAddEditComponent, data: { title: 'Bin Creation' } },
    { path: 'view', component: BinManagementViewComponent, data: { title: 'View Bin Info' } },
    { path: 'list', component: BinManagementComponent, data: { title: 'Bin Management' } },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
})

export class BinManagementRoutingModule { }