import { Component, OnInit } from '@angular/core';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams, CrudType, IApplicationResponse, RxjsService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { Router } from '@angular/router';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-bin-management',
  templateUrl: './bin-management.component.html',
  styleUrls: ['./bin-management.component.scss']
})
export class BinManagementComponent extends PrimeNgTableVariablesModel implements OnInit {
  primengTableConfigProperties: any;
  row: any = {};
  first: any = 0;
  reset: boolean;
  listSubscribtion: any;
  constructor(private crudService: CrudService,private rxjsService: RxjsService,private router: Router) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Bin Management List",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Bin Management List' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Bin Management List',
            enableBreadCrumb: true,
            checkBox: false,
            enableFieldsSearch: true,
            columns: [
              { field: 'bincode', header: 'Bin Code', width: '170px' },
              { field: 'qrCode', header: 'QR Code', width: '150px' },
              { field: 'description', header: 'Description', width: '200px' },
              { field: 'warehouseName', header: 'Warehouse', width: '90px' },
              { field: 'storageLocationName', header: 'Storage Location', width: '100px' },
              { field: 'availableQuantity', header: 'Available Quantity', width: '100px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.BIN_MANAGEMENT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          },
        ]
      }
    }
  }

  ngOnInit(): void {
    this.getStockInitiation();
  }
  getStockInitiation(pageIndex?: string, pageSize?: string, searchKey?: any) {
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    if(typeof searchKey == 'object') {
      searchKey = {...searchKey}
    }
    this.listSubscribtion = this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.BIN_MANAGEMENT,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, searchKey)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;
      }
      this.reset = false;
    })
  }
  onActionSubmited(e: any) {
      if (e.data && !e.search && !e?.col?.toString()) {
          this.onCRUDRequested(e.type, e.data);
      } else if (e.data && e.search && !e?.col) {
          this.onCRUDRequested(e.type, e.data, e.search);
      } else if (e.type && !e.data && !e?.col) {
          this.onCRUDRequested(e.type, {});
      } else if (e.type && e.data && e?.col?.toString()) {
          this.onCRUDRequested(e.type, e.data, e?.col);
      }
  }
  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.row = row ? row : { pageIndex: 0, pageSize: 10 };
            this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
            this.getStockInitiation(this.row["pageIndex"], this.row["pageSize"], unknownVar);
            break;
        }
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, row);
        break;
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        this.router.navigate(["inventory/bin/add-edit"], { skipLocationChange: true });
        break;
      case CrudType.EDIT:
        this.router.navigate(['inventory/bin/view'], { queryParams: { id: editableObject['locationBinConfigId'] }, skipLocationChange: true });
        break;
    }
  }
  
  ngOnDestroy() {
    if (this.listSubscribtion) {
        this.listSubscribtion.unsubscribe();
    }
  }
}
