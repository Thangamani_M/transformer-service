import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BinManagementComponent } from '@inventory/components/bin';
const routes: Routes = [
    { path: 'bin-management', component: BinManagementComponent, data: { title: 'Bin List' } }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})
export class BinComponentRoutingModule { }
