import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PackingBranchFilterList } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { CrudType, currentComponentPageBasedPermissionsSelector$, exportList, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-packing-list',
  templateUrl: './packing-list.component.html',
})
export class PackingListComponent extends PrimeNgTableVariablesModel {
  userData: UserLogin;
  packingOrdersFilterForm: FormGroup;
  primengTableConfigProperties: any;
  row: any = {};
  showPackingOrdersFilterForm = false;
  showFilterForm = false;
  statusList = [];
  receivingIdList = [];
  BarcodeList = [];
  ReferenceList = [];
  warehouseName = [];
  first: any = 0;
  PackingFilterColumn;
  otherParams;
  constructor(private snackbarService:SnackbarService,private commonService: CrudService,private datePipe: DatePipe,private router: Router,private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService,private formBuilder: FormBuilder,private store: Store<AppState>) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Packing Dashboard",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Packing Dashboard', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Packing Dashboard',
            dataKey: 'PackingCountId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enablePrintBtn: true,
            printTitle: 'Packing Dashboard',
            printSection: 'print-section0',
            cursorLinkIndex: 0,
            cursorSecondLinkIndex: 1,
            columns: [
              { field: 'packingJobId', header: 'Packing job ID' },
              { field: 'status', header: 'Status' },
              { field: 'receivingId', header: 'Receiving ID' },
              { field: 'receivingBarcode', header: 'Receiving Barcode' },
              { field: 'referenceNumber', header: 'Reference Number' },
              { field: 'warehouse', header: 'Warehouse' },
              { field: 'packerName', header: 'Packer Name' },
              { field: 'orderReceiptDate', header: 'Received Date' },
              { field: 'packedDate', header: 'Packed Date' },
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportBtn:true,
            apiSuffixModel: InventoryModuleApiSuffixModels.PACKING_DASHBOARD_LIST,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            shouldShowFilterActionBtn: true,
            ebableAddActionBtn: false,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getForkJoinRequests();
    this.getRequiredListData();
    this.packingDashboardsFilterFunction();
    this.rxjsService.setFormChangeDetectionProperty(true);
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.PACKING_DASHBOARD]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getForkJoinRequests(): void {
    let params = new HttpParams().set('userId', this.loggedInUserData?.userId);
    forkJoin([
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RECEIVINGID, undefined, true, null, 1),
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RECEIVINGBARCODE, undefined, true, null, 1),
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RECEIVING_REFERENCENUMBER, undefined, true, null, 1),
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UI_WAREHOUSES, undefined, true, params, 1),
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PACKIN_DASHBOARD_STATUS, undefined, true, null, 1)
    ]
    ).subscribe((response: IApplicationResponse[]) => {
        response.forEach((respObj: IApplicationResponse, ix: number) => {
          if (respObj.isSuccess) {
            switch (ix) {
              case 0:
                //  this.receivingIdList = respObj.resources;
                 let receivingIdList = respObj.resources;
                 for (var i = 0; i < receivingIdList.length; i++) {
                   let tmp = {};
                   tmp['value'] = receivingIdList[i].id;
                   tmp['display'] = receivingIdList[i].displayName;
                   this.receivingIdList.push(tmp)
                 }
                break;
              case 1:
                  // this.BarcodeList = respObj.resources;
                  let BarcodeList = respObj.resources;
                  for (var i = 0; i < BarcodeList.length; i++) {
                    let tmp = {};
                    tmp['value'] = BarcodeList[i].displayName;
                    tmp['display'] = BarcodeList[i].displayName;
                    this.BarcodeList.push(tmp)
                  }
                break;
              case 2:
                  // this.ReferenceList = respObj.resources;
                  let ReferenceList = respObj.resources;
                  for (var i = 0; i < ReferenceList.length; i++) {
                    let tmp = {};
                    tmp['value'] = ReferenceList[i].id;
                    tmp['display'] = ReferenceList[i].displayName;
                    this.ReferenceList.push(tmp)
                  }
                break;
              case 3:
                  // this.warehouseName = respObj.resources;
                  let warehouseName = respObj.resources;
                  for (var i = 0; i < warehouseName.length; i++) {
                    let tmp = {};
                    tmp['value'] = warehouseName[i].id;
                    tmp['display'] = warehouseName[i].displayName;
                    this.warehouseName.push(tmp)
                  }
                break;
              case 4:
                  // this.statusList = respObj.resources;
                  let statusList = respObj.resources;
                  for (var i = 0; i < statusList.length; i++) {
                    let tmp = {};
                    tmp['value'] = statusList[i].id;
                    tmp['display'] = statusList[i].displayName;
                    this.statusList.push(tmp)
                  }
                break;
            }
          }
        
        });
      });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = {...otherParams,...{UserId: this.userData.userId}};
    this.otherParams=otherParams;
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.packedDate = this.datePipe.transform(val?.packedDate, 'yyyy-MM-dd HH:mm:ss');
          val.orderReceiptDate = this.datePipe.transform(val?.orderReceiptDate, 'yyyy-MM-dd HH:mm:ss');
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;
      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
         this.row = row ? row : { pageIndex: 0, pageSize: 10 };
         this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
         unknownVar = {...unknownVar,...this.PackingFilterColumn}
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FILTER:
        this.openAddEditPage(CrudType.FILTER, row);
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
          let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
          this.exportList(pageIndex,pageSize);
     break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams,0,pageSize,ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.PACKING_DASHBOARD_EXPORT,this.commonService,this.rxjsService,'Packing List');
  }  


  getSelectedReceiving(BarCode): void {
    this.BarcodeList = [];
    if (BarCode != '') {
      let params = new HttpParams().set('Id', BarCode);
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RECEIVINGBARCODE, undefined, true, params).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let BarcodeList = response.resources;
          for (var i = 0; i < BarcodeList.length; i++) {
            let tmp = {};
            tmp['value'] = BarcodeList[i].displayName;
            tmp['display'] = BarcodeList[i].displayName;
            this.BarcodeList.push(tmp)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  getSelectedReferenceOptions(ReferenceNumber): void {
    this.ReferenceList = [];
    if (ReferenceNumber != '') {
      let params = new HttpParams().set('ReferencenumberIds', ReferenceNumber);
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RECEIVING_REFERENCENUMBER, undefined, true, params).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let ReferenceList = response.resources;
          for (var i = 0; i < ReferenceList.length; i++) {
            let tmp = {};
            tmp['value'] = ReferenceList[i].id;
            tmp['display'] = ReferenceList[i].displayName;
            this.ReferenceList.push(tmp)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  getWareshouseOption(WareShouse): void {
    this.warehouseName = [];
    if (WareShouse != '') {
      let params = new HttpParams().set('ReferencenumberIds', WareShouse);
      params = params.append('userId', this.loggedInUserData?.userId);
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UI_WAREHOUSES, undefined, true, params).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let warehouseName = response.resources;
          for (var i = 0; i < warehouseName.length; i++) {
            let tmp = {};
            tmp['value'] = warehouseName[i].id;
            tmp['display'] = warehouseName[i].displayName;
            this.warehouseName.push(tmp)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  getStatusOption(status): void {
    this.statusList = [];
    if (status != '') {
      let params = new HttpParams().set('ReferencenumberIds', status);
      this.commonService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_JOBORDERSTATUS, undefined, true, params).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let statusList = response.resources;
          for (var i = 0; i < statusList.length; i++) {
            let tmp = {};
            tmp['value'] = statusList[i].id;
            tmp['display'] = statusList[i].displayName;
            this.statusList.push(tmp)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(["inventory/packing-dashboard/view"], {
          queryParams: {
            id: editableObject['pickerJobId'],
            receivingId:editableObject['receivingId']
          }, skipLocationChange: true
        });
        break;
      case CrudType.FILTER:
        this.showPackingOrdersFilterForm = !this.showPackingOrdersFilterForm;
        break;
    }
  }

  packingDashboardsFilterFunction(stagingPayListModel?: PackingBranchFilterList) {
    let packingOrdersListModel = new PackingBranchFilterList(stagingPayListModel);
    /*Packing orders*/
    this.packingOrdersFilterForm = this.formBuilder.group({});
    Object.keys(packingOrdersListModel).forEach((key) => {
      if (typeof packingOrdersListModel[key] === 'string') {
        this.packingOrdersFilterForm.addControl(key, new FormControl(packingOrdersListModel[key]));
      }
    });
  }

  submitFilter() {
    this.PackingFilterColumn=null;

    let PackingFilterColumn = Object.assign({},
      this.packingOrdersFilterForm.get('receivingId').value == '' || this.packingOrdersFilterForm.get('receivingId').value == null ? null : {
        receivingIds: this.packingOrdersFilterForm.get('receivingId').value
      },
      this.packingOrdersFilterForm.get('referenceNumber').value == '' || this.packingOrdersFilterForm.get('referenceNumber').value == null ? null : {
        referenceNumberIds: this.packingOrdersFilterForm.get('referenceNumber').value
      },
      this.packingOrdersFilterForm.get('warehouseName').value == '' || this.packingOrdersFilterForm.get('warehouseName').value == null ? null : {
        warehouseIds: this.packingOrdersFilterForm.get('warehouseName').value
      },
      this.packingOrdersFilterForm.get('receivingBarCode').value == '' || this.packingOrdersFilterForm.get('receivingBarCode').value == null ? null : {
        receivingBarcodeIds: this.packingOrdersFilterForm.get('receivingBarCode').value
      },
      this.packingOrdersFilterForm.get('status').value == '' || this.packingOrdersFilterForm.get('status').value == null ? null : {
        statusIds: this.packingOrdersFilterForm.get('status').value
      },
      { UserId: this.userData.userId }
    );
    this.PackingFilterColumn = PackingFilterColumn;
    this.getRequiredListData('0', this.row["pageSize"], PackingFilterColumn);
    this.showPackingOrdersFilterForm = !this.showPackingOrdersFilterForm;
  }

  resetForm() {
    this.packingOrdersFilterForm.reset();
    this.PackingFilterColumn = null;
    this.showPackingOrdersFilterForm = !this.showPackingOrdersFilterForm;
    this.getRequiredListData('', '', null);
  }
}