import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PackingListComponent, PackingViewComponent } from "@inventory-components/packing-dashboard";
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: PackingListComponent, canActivate: [AuthGuard], data: { title: 'packing list' } },
  { path: 'view', component: PackingViewComponent, canActivate: [AuthGuard], data: { title: 'packing view' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class PackingDashboardRoutingModule { }
