import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { BarcodePrinterModule } from '../barcode-printer/barcode-printer/barcode-printer.module';
import { PackingDashboardRoutingModule } from './packing-dashboard-routing.module';
import { PackingListComponent } from './packing-list/packing-list.component';
import { PackingViewComponent } from './packing-view/packing-view.component';
@NgModule({
  declarations: [PackingListComponent, PackingViewComponent],
  imports: [
    CommonModule,
    PackingDashboardRoutingModule,
    LayoutModule,
    BarcodePrinterModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ReactiveFormsModule, FormsModule,
    MaterialModule,
    SharedModule
  ]
})
export class PackingDashboardModule { }
