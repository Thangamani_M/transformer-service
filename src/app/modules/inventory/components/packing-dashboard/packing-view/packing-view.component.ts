import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-packing-view',
  templateUrl: './packing-view.component.html',
  styleUrls: ['./packing-view.component.scss']
})
export class PackingViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  PickerJobId: string;
  PackingViewDetails: any = null;
  userData: UserLogin;
  jobNumber: any;
  PackingDetails: any = {};
  isWasteDisposalDialogModal: boolean;
  barcodes: any = [];
  receivingId;
  constructor(
    private httpService: CrudService, private router: Router, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private store: Store<AppState>
  ) {
    super();
    this.PickerJobId = this.activatedRoute.snapshot.queryParams.id;
    this.receivingId =   this.activatedRoute.snapshot.queryParams.receivingId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
   
  }

  ngOnInit() {
    if (this.PickerJobId) {
      this.getPackingOrderDetail().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.PackingViewDetails = response.resources;
          this.barcodes = new Array(this.PackingViewDetails?.receivingBarCode);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  getPackingOrderDetail(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('PickerJobId',this.PickerJobId).set('ReceivingId',this.receivingId)
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, 
    InventoryModuleApiSuffixModels.PACKINGDASHBOARD_DETAILS, undefined, false, params);
  }

  navigateToEdit(): void {
    this.router.navigate(['inventory/purchase-order/add-edit'], { queryParams: { id: this.PickerJobId }, skipLocationChange: true })
  }

  onModalSerialPopupOpen(PackingCountItemId) {

    let itemDetails = PackingCountItemId.packingDashboardDetailsSerialInfos;
    let SerialNo = [];
    itemDetails.forEach(itemDetails => {
      SerialNo.push(itemDetails.serialNumber)
    });
    this.PackingDetails = {
      ItemCode: PackingCountItemId.itemCode,
      ITEMNAME: PackingCountItemId.itemName,
      serialNumbers: SerialNo
    }
    this.isWasteDisposalDialogModal = true;
  }
}
