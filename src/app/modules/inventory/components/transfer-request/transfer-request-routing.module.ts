

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransferRequestComponent } from './transfer-request-list.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: TransferRequestComponent, canActivate: [AuthGuard], data: { title: 'Transfer Request' } },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class TransferRequestRoutingModule { }

