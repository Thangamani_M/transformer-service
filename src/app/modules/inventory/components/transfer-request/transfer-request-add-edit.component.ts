import { Component, ViewChild } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, fileUrlDownload, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { TransferRequestModel } from "@modules/inventory/models/transfer-request.model";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { SignaturePad } from "ngx-signaturepad/signature-pad";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Component({
    selector: 'app-transfer-request-add-edit',
    templateUrl: './transfer-request-add-edit.component.html',
    styleUrls: ['./transfer-request-list.component.scss']
})
export class TransferRequestAddEditComponent {
    transferRequestCreationForm: FormGroup;
    primengTableConfigProperties: any;
    id;
    userData;
    btnName;
    selectedTabIndex: any = 0;
    issuingWarehouseList = [];
    _issuingWarehouseListCopy = [];
    issuingWarehouseLocation = [];
    receivingWarehouseList = [];
    _receivingWarehouseListCopy = [];
    receivingWarehouseLocation = [];
    requestDetails; items;
    listOfFiles: any = [];
    fileLists: any = [];
    filteredStockCodes: any = [];
    filteredStockDescription: any = [];
    transferRequestDocument: any = [];
    maxFilesUpload: Number = 5;
    isAddedErr: boolean = false;
    formData = new FormData();
    loading: boolean = false;
    isLoading: boolean = false;
    stockOrderId;
    deletePopup: boolean = false;
    index; stockCode; stockDescription;
    qtyFlag: boolean = false;
    type; currentLevel;
    isDraft: boolean = false;
    isCollectSubmit: boolean = false;
    isCourier: boolean = false;
    isDirectCollection: boolean = false;
    secTitle; navUrl;
    wareHouseId;
    serialPopup: boolean = false;
    subDetails;
    serialInfoPopupDetails;
    itemCodePoup;
    itemNamePopup;
    filterDetails;
    mainIndex;
    radioRemovalTransferRequestStatusName;
    isSerializeHasData : boolean = true;
    @ViewChild(SignaturePad, { static: false }) signaturePad: any;

    public signaturePadOptions: Object = {
      'minWidth': 1,
      'canvasWidth': 500,
      'canvasHeight': 80
    };

    constructor(private router: Router, private snackbarService: SnackbarService, private crudService: CrudService, private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private store: Store<AppState>) {
        this.activatedRoute.queryParamMap.subscribe((params) => {
            this.id = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
            this.type = (Object.keys(params['params']).length > 0) ? params['params']['type'] : '';//issuing
            this.currentLevel = (Object.keys(params['params']).length > 0) ? params['params']['currentLevel'] : '';
        });
        let title = this.id ? 'Update Transfer Request' : 'Add Transfer Request'
        this.btnName = this.id ? 'Update' : 'Save',
            this.primengTableConfigProperties = {
                tableCaption: title,
                selectedTabIndex: 0,
                breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }],
                tableComponentConfigs: {
                    tabsList: [
                        {
                            enableAction: false,
                            enableBreadCrumb: true,
                        }
                    ]
                }
            }
        this.secTitle = (this.currentLevel == 1 || this.currentLevel == 2) ? 'Task List' : 'Transfer Request';
        this.navUrl = (this.currentLevel == 1 || this.currentLevel == 2) ? '/my-tasks/inventory-task-list' : '/radio-removal/transfer-request';
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName: this.secTitle, relativeRouterUrl: this.navUrl });
        this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title, relativeRouterUrl: '' });
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
        this.wareHouseId = this.userData.warehouseId;
    }
    ngOnInit() {
        this.createTransferRequestForm();
        if (this.id)
            this.getTransferRequestDetails();

        this.getIssuingWarehouse();
        this.rxjsService.setGlobalLoaderProperty(false);
    }
    createTransferRequestForm(transferRequestModel?: TransferRequestModel): void {
        let requestModel = new TransferRequestModel(transferRequestModel);
        // create form controls dynamically from model class
        this.transferRequestCreationForm = this.formBuilder.group({
            items: this.formBuilder.array([])
        });
        Object.keys(requestModel).forEach((key) => {
            this.transferRequestCreationForm.addControl(key, new FormControl(requestModel[key]));
        });
        this.transferRequestCreationForm.addControl('isDirectCollection', new FormControl());
        this.transferRequestCreationForm.addControl('courierName', new FormControl());
        if (this.type == 'task-list' && this.isCourier && (this.radioRemovalTransferRequestStatusName=='Pending Acceptance' || this.radioRemovalTransferRequestStatusName=='Rejected')) {
            this.transferRequestCreationForm.addControl('isAccepted', new FormControl());
            this.transferRequestCreationForm.addControl('reasons', new FormControl());
            this.transferRequestCreationForm.get('isDirectCollection').disable();
            this.transferRequestCreationForm.get('courierName').disable();
            this.transferRequestCreationForm = setRequiredValidator(this.transferRequestCreationForm, ["reasons", "isAccepted"]);
        }
        if(!this.type)
           this.transferRequestCreationForm = setRequiredValidator(this.transferRequestCreationForm, ["issuingWarehouseId", "issuingLocationId", "receivingWarehouseId", "receivingLocationId"]);

        this.onValueChanges();
    }
    get getItemsArray(): FormArray {
        if (this.transferRequestCreationForm !== undefined) {
            return (<FormArray>this.transferRequestCreationForm.get('items'));
        }
    }
    onValueChanges() {
        this.transferRequestCreationForm.get('issuingWarehouseId').valueChanges.subscribe(issuingWarehouseId => {
            let receivingWarehouseId = this.transferRequestCreationForm.get('receivingWarehouseId').value;
            if (receivingWarehouseId && (receivingWarehouseId == issuingWarehouseId)) {
                this.snackbarService.openSnackbar("IssuingWarehouse and ReceivingWarehouse should not be same.", ResponseMessageTypes.WARNING)
            }
        });
        this.transferRequestCreationForm.get('receivingWarehouseId').valueChanges.subscribe(receivingWarehouseId => {
            let issuingWarehouseId = this.transferRequestCreationForm.get('issuingWarehouseId').value;
            if (issuingWarehouseId && (issuingWarehouseId == receivingWarehouseId)) {
                this.snackbarService.openSnackbar("IssuingWarehouse and ReceivingWarehouse should not be same.", ResponseMessageTypes.WARNING)
            }
        });
        this.transferRequestCreationForm.get('isDirectCollection').valueChanges.subscribe(res => {
            if (res) {
                this.transferRequestCreationForm.get('courierName').reset();
                this.transferRequestCreationForm.get('courierName').disable();
            }
            else
                this.transferRequestCreationForm.get('courierName').enable();
        });
    }
    getIssuingWarehouse(): void {
        //this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION)])
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE, undefined, false, prepareRequiredHttpParams({ }))
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.issuingWarehouseList = response.resources;
                    this.receivingWarehouseList = response.resources;
                    this._issuingWarehouseListCopy = response.resources;
                    this._receivingWarehouseListCopy = response.resources;
                    if (this.issuingWarehouseList && this.issuingWarehouseList.length > 0) {
                        let filter = this.issuingWarehouseList.filter(x => x.id == this.wareHouseId);
                        if (filter && filter.length > 0) {
                            this.transferRequestCreationForm.get('issuingWarehouseId').setValue(filter[0].id);
                            this.onChangeIssuingWarehouse(filter[0].id, 'default')
                        }
                    }
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    onChangeIssuingWarehouse(val, type) {
        let params = { WarehouseId: val }
        this.receivingWarehouseList =  this._receivingWarehouseListCopy.filter(item=> item.id !=val)
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION, undefined,
            false, prepareRequiredHttpParams(params))
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.issuingWarehouseLocation = response.resources;
                    if (type == 'default' && this.issuingWarehouseLocation && this.issuingWarehouseLocation.length > 0) {
                        let filter = this.issuingWarehouseLocation.filter(word => { return word.displayName.includes('Radio/System Removal Location'); });
                        if (filter && filter.length > 0)
                            this.transferRequestCreationForm.get('issuingLocationId').setValue(filter[0].id);
                    }
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    onChangeReceivingWarehouse(val) {
      this.issuingWarehouseList =  this._issuingWarehouseListCopy.filter(item=> item.id !=val)
        let params = { WarehouseId: val }
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION, undefined,
            false, prepareRequiredHttpParams(params))
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200) {
                    this.receivingWarehouseLocation = response.resources;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    details;
    getTransferRequestDetails() {
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.RADIO_REMOVAL_TRANSFER_EDIT,
            this.id,
            false, null
        ).subscribe((data: IApplicationResponse) => {
            if (data.isSuccess && data.resources) {
                this.requestDetails = data.resources;
                if (this.requestDetails.radioRemovalTransferRequestStatusName == 'Save as Draft')
                    this.isDraft = true;

                this.setValue(data.resources);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    setValue(data) {

        this.isDirectCollection = data.isDirectCollection;
        this.radioRemovalTransferRequestStatusName = data.radioRemovalTransferRequestStatusName;
        this.isCourier = data.isCourier;
         if(this.radioRemovalTransferRequestStatusName=="Pending Collection" && this.isDirectCollection){
            this.transferRequestCreationForm.get('isDirectCollection').disable();
         }
         if(this.radioRemovalTransferRequestStatusName=="Rejected")
          this.transferRequestCreationForm?.get('isAccepted')?.setValue(false);


          data.radioRemovalTransferRequestItemsEditDTOs.forEach(element => {
            if((this.type=='task-list' && (this.isDirectCollection || this.isCourier)) && element.isNotSerialized==false){
                this.isSerializeHasData =  element.radioRemovalTransferItemsDetailsEditDTOs.length > 0 ? true : false;
                if(this.isSerializeHasData == false){
                    this.snackbarService.openSnackbar("Want to complete the Job Selection process.",ResponseMessageTypes.WARNING);
                    return;
                }
            }
        });
        this.transferRequestCreationForm.get('issuingWarehouseId').setValue(data.issuingWarehouseId);
        this.onChangeIssuingWarehouse(data.issuingWarehouseId, 'nodefault');
        this.transferRequestCreationForm.get('issuingLocationId').setValue(data.issuingLocationId);
        this.transferRequestCreationForm.get('receivingWarehouseId').setValue(data.receivingWarehouseId);
        this.onChangeReceivingWarehouse(data.receivingWarehouseId);
        this.transferRequestCreationForm.get('receivingLocationId').setValue(data.receivingLocationId);
        if (this.type == "task-list") {
            this.transferRequestCreationForm.get('isDirectCollection').setValue(data.isDirectCollection);
            this.transferRequestCreationForm.get('courierName').setValue(data.courierName);
            if(data.courierName)
              this.transferRequestCreationForm.get('isDirectCollection').disable();

              //this.transferRequestCreationForm.get('courierName').disable();
        }
        this.subDetails = data.radioRemovalTransferRequestItemsEditDTOs;
        data.radioRemovalTransferRequestItemsEditDTOs.forEach(element => {
            let dealerTypeItem = this.formBuilder.group({
                quantity: element.quantity,
                stockCode: element.itemCode,
                stockDescription: element.itemName,
                isNotSerialized: element.isNotSerialized,
                collectedQty: element.collectedQty ? element.collectedQty:0,
                outStandingQty: element.outStandingQty?element.outStandingQty:0,
                itemId: element.itemId,
                radioRemovalTransferRequestItemId: element.radioRemovalTransferRequestItemId
            });
            if(element.isNotSerialized==false){
                dealerTypeItem.get('collectedQty').disable();
                dealerTypeItem.get('outStandingQty').disable();
            }
            dealerTypeItem = setRequiredValidator(dealerTypeItem, ["quantity", "stockCode", "stockDescription"]);
            this.getItemsArray.push(dealerTypeItem);
            this.getItemsArray.value[this.getItemsArray.value.length -1].collectedQty = element.collectedQty ? element.collectedQty:0;
            this.getItemsArray.value[this.getItemsArray.value.length -1].outStandingQty = element.outStandingQty ? element.outStandingQty:0;
        });
        data.radioRemovalTransferRequestDocumentsEditDTOs.forEach(element => {
            let docObj = {
                radioRemovalTransferRequestDocumentId: element.radioRemovalTransferRequestDocumentId,
                docName: element.docName,
                filePath: element.filePath
            }
            this.transferRequestDocument.push(docObj);
        });

    }
    uploadFiles(file) {
        if (file && file.length == 0)
            return;

        var numberOfFilesUploaded = this.listOfFiles.length;
        if (file.length > this.maxFilesUpload || numberOfFilesUploaded >= this.maxFilesUpload) {
            this.snackbarService.openSnackbar(`You can upload maximum ${this.maxFilesUpload} files`, ResponseMessageTypes.WARNING);
            return;
        }
        const path = file[0].name.split('.');
        const extension = path[path.length - 1];
        const supportedExtensions = [
            'jpeg',
            'jpg',
            'png',
            'gif',
            'pdf',
            'doc',
            'docx',
            'xls',
            'xlsx',
        ];
        if (supportedExtensions.includes(extension)) {
            this.listOfFiles.push({ name: file[0].name, index: this.listOfFiles.length + 1, path: '' });
            this.fileLists.push(file[0]);
        } else {
            this.snackbarService.openSnackbar('Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx', ResponseMessageTypes.WARNING);
        }
    }

    onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
        switch (type) {
            default:
                break;
        }
    }
    removeSelectedFile(index, id) {
        // // Delete the item from fileNames list
        this.listOfFiles.splice(index, 1);
        this.fileLists.splice(index, 1);
    }
    removeSelectedFiles(index, id) {
        // // Delete the item from fileNames list
        this.transferRequestDocument.splice(index, 1);
    }
    download(path) {
        if (path) return fileUrlDownload(path, true);
    }
    onSelectSerialNo(checked,index) {
        let collectedQty=this.getItemsArray.controls[this.mainIndex].get('collectedQty').value?this.getItemsArray.controls[this.mainIndex].get('collectedQty').value : 0;
        let outStandingQty=this.getItemsArray.controls[this.mainIndex].get('outStandingQty').value?this.getItemsArray.controls[this.mainIndex].get('outStandingQty').value : 0;
        if(checked)
          collectedQty = Number(collectedQty) + 1;
        else if(!checked)
          collectedQty = Number(collectedQty) - 1;

        outStandingQty = Number(this.getItemsArray.controls[this.mainIndex].get('quantity').value) -  Number(collectedQty);
        outStandingQty = collectedQty==0 ? 0:outStandingQty
        this.getItemsArray.controls[this.mainIndex].get('collectedQty').setValue(collectedQty);
        this.getItemsArray.controls[this.mainIndex].get('outStandingQty').setValue(outStandingQty);
        this.getItemsArray.value[this.mainIndex].collectedQty = collectedQty;
        this.getItemsArray.value[this.mainIndex].outStandingQty = outStandingQty;
        let filter = this.subDetails.filter(x => x.itemCode == this.getItemsArray.value[this.mainIndex].stockCode);
        this.getItemsArray.value[this.mainIndex].itemDetails = this.subDetails[this.mainIndex].itemDetails;
        this.getItemsArray.value[this.mainIndex].itemDetails[index].isCollected = checked ?checked:false;

    }
    onChangeStockCodes(event, i) {
        if (event) {
            this.serialPopup = !this.serialPopup;
            this.mainIndex = i;
            let filter = this.subDetails.filter(x => x.itemCode == event.value.stockCode);
            if(filter && filter.length>0 && filter[0].radioRemovalTransferItemsDetailsEditDTOs && filter[0].radioRemovalTransferItemsDetailsEditDTOs.length >0){
                this.serialInfoPopupDetails = filter[0].radioRemovalTransferItemsDetailsEditDTOs;
                this.filterDetails = filter[0];
                this.itemNamePopup = filter[0].itemName;
                this.itemCodePoup = filter[0].itemCode;
                this.filterDetails.itemDetails = filter[0].radioRemovalTransferItemsDetailsEditDTOs;
                let finalData=[];
                filter[0].radioRemovalTransferItemsDetailsEditDTOs.forEach(element => {
                    element.isCollected = false;
                    element.CreatedUserId =  this.userData.userId;
                    element.radioRemovalTransferRequestItemId = filter[0].radioRemovalTransferRequestItemId;
                    finalData.push(element);
                });
                this.getItemsArray.value[i].itemDetails =  finalData;
                this.subDetails[i].itemDetails =  finalData;

                this.filterDetails.modifiedUserId = this.userData.userId;
            }
        }
    }
    onChangeStockCode(event, type, index?: any) {
        if (event.target.value) {
            let params = { LocationId: this.transferRequestCreationForm.get('issuingLocationId').value, searchText: event.target.value };
            // this.searchTerm = event.target.value;
            let api = (type == 'stock-code') ? this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_TRANSFER_REQUEST, null, false, prepareRequiredHttpParams(params)) : this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_TRANSFER_REQUEST, null, false, prepareRequiredHttpParams(params));
            api.subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode == 200 && response.resources) {
                    if (type == 'stock-code')
                        this.filteredStockCodes = response.resources;
                    else
                        this.filteredStockDescription = response.resources;

                    this.rxjsService.setGlobalLoaderProperty(false);
                    //  this.isLoading=false;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
    }
    onStatucCodeSelected(value, type, index?: any) {
        let TYPE = (type == 'stock-description') ? 'stockDescription' : 'stockCode';
        if (value) {
            let filter = (type == 'stock-code' && this.filteredStockCodes) ? this.filteredStockCodes.filter(x => x.itemCode == value) : (type == 'stock-description' && this.filteredStockCodes) ? this.filteredStockCodes.filter(x => x.itemName == value) : null;
            if (filter && filter.length > 0) {
                if (index || index == 0) {
                    // this.getItemsArray.controls[index].get(TYPE).setValue(filter[0].displayName);
                    this.getItemsArray.controls[index].get('itemId').setValue(filter[0].itemId);
                    this.getItemsArray.controls[index].get('stockCode').setValue(filter[0].itemCode);
                    this.getItemsArray.controls[index].get('stockDescription').setValue(filter[0].itemName);
                    this.getItemsArray.controls[index].get('balance').setValue(filter[0].balance);
                }
                else if (index == undefined || index == null) {
                    this.transferRequestCreationForm.get('itemId').setValue(filter[0].itemId);
                    this.transferRequestCreationForm.get('stockDescription').setValue(filter[0].itemName);
                    this.transferRequestCreationForm.get('stockCode').setValue(filter[0].itemCode);
                    this.transferRequestCreationForm.get('balance').setValue(filter[0].balance);
                }
            }
        }

    }
    addDealerStockOrderItem() {
        if (this.transferRequestCreationForm.get("stockDescription").value == '' || this.transferRequestCreationForm.get("stockCode").value == '' ||
            this.transferRequestCreationForm.get("stockDescription").value == null || this.transferRequestCreationForm.get("stockCode").value == null ||
            this.transferRequestCreationForm.get("quantity").value == '' || this.transferRequestCreationForm.get("quantity").value == null) {
            this.isAddedErr = true;
            return;
        }
        if (this.qtyFlag) {
            this.snackbarService.openSnackbar("Quantity should not be more than the Balance - " + this.transferRequestCreationForm.get("balance").value, ResponseMessageTypes.ERROR);
            return;
        }
        let dealerTypeItem = this.formBuilder.group({
            quantity: this.transferRequestCreationForm.get("quantity").value,
            stockCode: this.transferRequestCreationForm.get("stockCode").value,
            stockDescription: this.transferRequestCreationForm.get("stockDescription").value,
            itemId: this.transferRequestCreationForm.get("itemId").value,
            collectedQty: this.transferRequestCreationForm.get("collectedQty").value,
            outStandingQty: this.transferRequestCreationForm.get("outStandingQty").value,
            radioRemovalTransferRequestItemId: null
        })
        dealerTypeItem = setRequiredValidator(dealerTypeItem, ["quantity", "stockCode", "stockDescription"]);
        this.getItemsArray.push(dealerTypeItem);
        this.isAddedErr = false;
        this.transferRequestCreationForm.get('quantity').reset();
        this.transferRequestCreationForm.get('stockCode').reset();
        this.transferRequestCreationForm.get('stockDescription').reset();
        this.transferRequestCreationForm.get('itemId').reset();
        // this.calculateTax();
    }
    getPictureProfile(id): Observable<any> {
        return this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.ITEM,
            id,
            false,
            prepareRequiredHttpParams({})
        ).pipe(
            map(result => {
                return result;
            })
        );
    }
    onChangeRequestedQty(event, index?: any) {
        let val = event.target.value;
        if (index || index == 0) {

            let balance = parseInt(this.getItemsArray.controls[index].get('balance').value);
            let quantity = parseInt(this.getItemsArray.controls[index].get('quantity').value);
            this.qtyFlag = quantity > balance ? true : false;
            if (quantity > balance)
                this.snackbarService.openSnackbar("Quantity should not be more than the Balance - " + balance, ResponseMessageTypes.ERROR);
        }
        else if (index == undefined) {

            let balance = parseInt(this.transferRequestCreationForm.get('balance').value);
            let quantity = parseInt(this.transferRequestCreationForm.get('quantity').value);
            this.qtyFlag = quantity > balance ? true : false;
            if (quantity > balance)
                this.snackbarService.openSnackbar("Quantity should not be more than the Balance - " + balance, ResponseMessageTypes.ERROR);
        }

    }
    onChangeCollectedQty(event, index?: any){
        let val = parseInt(event.target.value);
        if ((index || index == 0 )&& val) {
            let quantity = parseInt(this.getItemsArray.controls[index].get('quantity').value);
            if (val > quantity){
                this.snackbarService.openSnackbar("Collected Quantity should be less than the Quantity", ResponseMessageTypes.WARNING);
                return;
            }
            this.getItemsArray.controls[index].get('outStandingQty').setValue(quantity-val);
        }
    }
    removeStockOrderItems(index, obj) {
        this.deletePopup = !this.deletePopup;
        this.index = index;
        this.stockCode = this.getItemsArray.value[this.index].stockCode;
        this.stockDescription = this.getItemsArray.value[this.index].stockDescription;

    }
    delete() {
        if (this.getItemsArray.value[this.index]?.radioRemovalTransferRequestItemId) {
            this.crudService.deleteByParams(
                ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.RADIO_REMOVAL_TRANSFER_REQUEST_ITEM,
                {
                    body: {
                        RadioRemovalTransferRequestId: this.id,
                        ItemId: this.getItemsArray.value[this.index]?.itemId,
                        ModifiedUserId: this.userData.userId
                    }
                }
            ).subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                    this.getItemsArray.removeAt(this.index);
                    this.deletePopup = !this.deletePopup;
                    this.getTransferRequestDetails();
                    //this.getPriceIncreaseList();
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
        else {
            this.getItemsArray.removeAt(this.index);
            this.deletePopup = !this.deletePopup;
        }
    }
    cancelPopup() {
        this.deletePopup = !this.deletePopup;
    }
    serialPopupCancel(){
        this.serialPopup = !this.serialPopup;
    }
    cancel() {
        if (this.currentLevel == 1 || this.currentLevel == 2)
            this.router.navigate(['/my-tasks/inventory-task-list']);
        else if (!this.type)
            this.router.navigate(['/radio-removal/transfer-request']);
    }
    onSubmit(isDraft) {
       // this.rxjsService.setFormChangeProperty
        this.rxjsService.setFormChangeDetectionProperty(true);
        this.formData = new FormData();
        if (this.transferRequestCreationForm.invalid) {
            return;
        }
        let receivingWarehouseId = this.transferRequestCreationForm.get('receivingWarehouseId').value;
        let issuingWarehouseId = this.transferRequestCreationForm.get('issuingWarehouseId').value;
        if (receivingWarehouseId == issuingWarehouseId) {
            this.snackbarService.openSnackbar("IssuingWarehouse and ReceivingWarehouse should not be same.", ResponseMessageTypes.WARNING); return;
        }
        let formValue = this.transferRequestCreationForm.value;
        let api;
        if (!this.type) {
            if (formValue.items && formValue.items.length == 0) {
                this.snackbarService.openSnackbar("Add atleast one stock details.", ResponseMessageTypes.WARNING); return;
            }
            let final;
            final = {
                radioRemovalTransferRequestId: this.id ? this.id : null,
                issuingWarehouseId: formValue.issuingWarehouseId,
                issuingLocationId: formValue.issuingLocationId,
                receivingWarehouseId: formValue.receivingWarehouseId,
                receivingLocationId: formValue.receivingLocationId,
                isDraft: isDraft,
                createdUserId: this.userData.userId,
                Items: formValue.items,
                TransferRequestDocument: this.transferRequestDocument
            }
            if (this.fileLists && this.fileLists.length > 0) {
                this.fileLists.forEach(element => {
                    this.formData.append(element.name, element);
                });
            }
            if (this.id)
                final.ModifiedUserId = this.userData.userId;

            this.formData.append('TransferRequestDocument', JSON.stringify(final));
            api = this.id ? this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_TRANSFER, this.formData) : this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_TRANSFER, this.formData);
        }
        else if (this.type == 'task-list' && (formValue.isDirectCollection ||formValue.courierName) && !this.isDirectCollection && !this.isCourier) {
            let finalObj = {
                RadioRemovalTransferRequestId: this.id,
                IsDirectCollection: formValue.isDirectCollection ? formValue.isDirectCollection : false,
                IsCourier: formValue.courierName ? true : false,
                CourierName: formValue.courierName,
                ModifiedUserId: this.userData.userId
            }
            api = this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_TRANSFER_REQUEST_COLLECTION, finalObj);
        }
        else if (this.type == 'task-list' && this.isCourier && (this.radioRemovalTransferRequestStatusName=="Pending Acceptance"|| this.radioRemovalTransferRequestStatusName=='Rejected')) {
            let finalObj = {
                RadioRemovalTransferRequestId: this.id,
                UserId: this.userData.userId,
                IsAccepted: formValue.isAccepted ? true : false,
                IsRejected: !formValue.isAccepted ? true : false,
                Reasons: formValue.reasons,
                CreatedUserId: this.userData.userId
            }
            api = this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_TRANSFER_REQUEST_STATUS_UPDATE, finalObj);
        }
        else if (this.type == 'task-list' && (this.isDirectCollection  || this.isCourier) && this.radioRemovalTransferRequestStatusName=="Pending Collection") {
            let  val={items:null};
            val.items = this.transferRequestCreationForm.value.items;
            let arr=[];let flag=false;
            val.items.forEach(element => {
                if(element.collectedQty==0 || element.collectedQty==null) {
                    flag=true;
                    this.snackbarService.openSnackbar("Please update collect quanity for all Stock Codes.",ResponseMessageTypes.WARNING)
                    return;
                }
                let obj ={
                    radioRemovalTransferRequestItemId:element.radioRemovalTransferRequestItemId,
                    radioRemovalTransferRequestId :this.id,
                    modifiedUserId : this.userData.userId,
                    itemId: element.itemId,
                    quantity: element.quantity,
                    collectedQty: element.collectedQty,
                    outStandingQty: element.outStandingQty,
                    itemDetails:element.itemDetails
                }
                arr.push(obj);
            });
            if(flag){return;}
            val.items=arr;
            api = this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_TRANSFER_REQUEST_COLLECTED_QTY_UPDATE, val);
        }
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                if (this.type == 'task-list' && (this.isDirectCollection  || this.isCourier)) {
                    this.isCollectSubmit=true;
                    // this.router.navigate(['/my-tasks/inventory-task-list']);
                }
                else
                    this.navigateToList();
            }
        });
    }
    dataURItoBlob(dataURI) {
        const byteString = window.atob(dataURI);
        const arrayBuffer = new ArrayBuffer(byteString.length);
        const int8Array = new Uint8Array(arrayBuffer);
        for (let i = 0; i < byteString.length; i++) {
          int8Array[i] = byteString.charCodeAt(i);
        }
        const blob = new Blob([int8Array], { type: 'image/jpeg' });
        return blob;
      }

    verify() {
        if (this.signaturePad.signaturePad._data.length == 0) {
            this.snackbarService.openSnackbar('Signature is required', ResponseMessageTypes.WARNING);
            return;
          }
          let formData = new FormData();
          if (this.signaturePad.signaturePad._data.length > 0) {
            let imageName = "signature_" +new Date()+".jpeg";
            const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
            const imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' });
            formData.append("document_files", imageFile);
          }
        let formValue = this.transferRequestCreationForm.value;
        let finalObj = {
            radioRemovalTransferRequestId: this.id,
            collectorName: formValue.name,
            comments: formValue.reasons,
            modifiedUserId: this.userData.userId
        }
        formData.append("details", JSON.stringify(finalObj));
        let api = this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_TRANSFER_REQUEST_COLLECTOR_STATUS, formData);
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                if (this.currentLevel == 1 || this.currentLevel == 2)
                    this.router.navigate(['/my-tasks/inventory-task-list']);
                else
                    this.navigateToList();
            }
        });
    }
    drawComplete() {

    }
    clearPad() {
        this.signaturePad.clear();
    }
    navigateToList() {
        this.router.navigate(['/radio-removal/transfer-request']);
    }
}
