import { DatePipe } from "@angular/common";
import { Component } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { map } from "rxjs/operators";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-transfer-request-list',
  templateUrl: './transfer-request-list.component.html',
  styleUrls: ['./transfer-request-list.component.scss']
})
export class TransferRequestComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties: any;
  loggedUser: any;
  first: any = 0;
  row: any = {};
  filterForm: FormGroup;
  showFilterForm: boolean = false;
  issuingWarehouseList = [];
  issuingLocationList = [];
  receivingWarehouseList = [];
  receivingLocationList = [];
  statusList = [];
  observableResponse;
  startTodayDate = 0;
  filterdNewData: any;
  constructor(private _fb: FormBuilder, private datePipe: DatePipe,
    private store: Store<AppState>, private router: Router, private rxjsService: RxjsService, private crudService: CrudService,
    private snackbarService: SnackbarService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Transfer Request",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radio Removal WorkList', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Transfer Request',
            dataKey: 'radioRemovalTransferRequestId',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enablePrintBtn: true,
            printTitle: 'Radio Removals',
            printSection: 'print-section0',
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [{ field: 'requestNumber', header: 'Transfer Request Number' },
            { field: 'status', header: 'Status' },
            { field: 'transferBarcode', header: 'Transfer Barcode' },
            { field: 'issuingWarehouse', header: 'Issuing Warehouse' },
            { field: 'issuingLocation', header: 'Issuing Location' },
            { field: 'receivingWarehouse', header: 'Receiving Warehouse' },
            { field: 'receivingLocation', header: 'Receiving Location' },
            { field: 'createdBy', header: 'Created By' },
            { field: 'createdDate', header: 'Created Date' },
            { field: 'actionedBy', header: 'Actioned By' },
            { field: 'actionedDate', header: 'Actioned Date' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.RADIO_REMOVAL_TRANSFER_LIST,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            shouldShowFilterActionBtn: true,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableExportBtn: false,
            enableEmailBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true
          },
        ]
      }
    }
  }
  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getTransferRequestList(null, null, { IsAll: true });
    this.createFilterForm();
    this.getIssuingWarehouse();
    this.getReceivingWarehouse();
    this.getStatus();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Transfer Request"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createFilterForm() {
    this.filterForm = this._fb.group({
      IssuingWarehouseIds: [""],
      IssuingLocationIds: [""],
      ReceivingWarehouseIds: [""],
      ReceivingLocationIds: [""],
      StatusIds: [""],
      CreatedFromDate: [""],
      CreatedToDate: [""],
    })
  }
  getIssuingWarehouse(): void {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE, undefined, false, prepareRequiredHttpParams({ userId: this.loggedUser.userId }))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.issuingWarehouseList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onChangeIssuingWarehouse(val) {
    let params = { WarehouseId: val }
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION, undefined,
      false, prepareRequiredHttpParams(params))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.issuingLocationList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getReceivingWarehouse(): void {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE, undefined, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.receivingWarehouseList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  onChangeReceivingWarehouse(val) {
    let params = { WarehouseId: val }
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_LOCATION, undefined,
      false, prepareRequiredHttpParams(params))
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.receivingLocationList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getStatus(): void {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RADIO_REMOVAL_TRANSFER_REQUEST_STATUS, undefined, false, null)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.statusList = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getTransferRequestList(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true
    let otherParamsAll = Object.assign(otherParams);
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_TRANSFER_LIST,
      undefined,
      false,
      prepareGetRequestHttpParams(pageIndex, pageSize, otherParamsAll)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.suspendedOn = this.datePipe.transform(val.suspendedOn, 'dd-MM-yyyy, h:mm:ss a');
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = data.resources;
        this.totalRecords = 0;
      }
      // this.reset = false;
    });
  }
  submitFilter() {

    let StatusIds = [];
    if (this.filterForm.value.StatusIds && this.filterForm.value.StatusIds.length > 0) {
      this.filterForm.value.StatusIds.forEach(element => {
        StatusIds.push(element.id);
      });
      this.filterForm.value.StatusIds = StatusIds.toString();
    }
    if (this.filterForm.value.CreatedFromDate)
      this.filterForm.value.CreatedFromDate = this.datePipe.transform(this.filterForm.get('CreatedFromDate').value, 'yyyy-MM-dd')
    if (this.filterForm.value.CreatedToDate)
      this.filterForm.value.CreatedToDate = this.datePipe.transform(this.filterForm.get('CreatedToDate').value, 'yyyy-MM-dd')

    let filteredData = this.filterForm.value;
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || (filteredData[key] && filteredData[key].length == 0)) {
        delete filteredData[key]
      }
    });
    let filterdNewData = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )
    // this.scrollEnabled = false;
    this.row = this.row ? this.row : { pageIndex: 0, pageSize: 10 };
    this.filterdNewData = filterdNewData;
    this.observableResponse = this.onCRUDRequested('get', this.row);
    //this.observableResponse = this.getTransferRequestList(this.row['pageIndex'], this.row['pageSize'], filterdNewData);
    this.showFilterForm = !this.showFilterForm;
  }
  onChangeSelecedRows(e) {
    //this.selectedRows = e;
  }
  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(["/radio-removal/transfer-request/add-edit"]);
        break;
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
        if (unknownVar.requestNumber)
          unknownVar.transferRequestNumber = unknownVar.requestNumber;

        if (this.filterdNewData)
          unknownVar = { ...{ IsAll: true }, ...this.filterdNewData, ...unknownVar };
        else
          unknownVar = { ...{ IsAll: true }, ...unknownVar };

        this.getTransferRequestList(row?.pageIndex, row?.pageSize, unknownVar)
        break;
      case CrudType.VIEW:
        this.router.navigate(["/radio-removal/transfer-request/view"], { queryParams: { id: row['radioRemovalTransferRequestId'] } });
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      default:
    }
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  resetForm() {
    this.getTransferRequestList(null, null, { IsAll: true });
    this.filterForm.reset();
    this.filterdNewData = null;
    this.showFilterForm = !this.showFilterForm;
  }
}
