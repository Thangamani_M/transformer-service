
import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { TransferRequestAddEditComponent } from './transfer-request-add-edit.component';
import { TransferRequestComponent } from './transfer-request-list.component';
import { TransferRequestViewComponent } from './transfer-request-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
@NgModule({
  declarations: [TransferRequestComponent, TransferRequestAddEditComponent, TransferRequestViewComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: TransferRequestComponent, canActivate: [AuthGuard], data: { title: 'Transfer Request' } },
      { path: 'add-edit', component: TransferRequestAddEditComponent, canActivate: [AuthGuard], data: { title: 'Transfer Request Add/Edit' } },
      { path: 'view', component: TransferRequestViewComponent, canActivate: [AuthGuard], data: { title: 'View Transfer Request' } }
    ])
  ],
  providers: [
    DatePipe
  ],
  entryComponents: [],
  exports: [],
})
export class TransferRequestModule { }

