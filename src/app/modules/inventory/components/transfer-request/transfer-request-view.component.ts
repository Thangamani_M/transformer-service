import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, fileUrlDownload, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";

@Component({
  selector: 'app-transfer-request-view',
  templateUrl: './transfer-request-view.component.html',
  styleUrls: ['./transfer-request-list.component.scss']
})
export class TransferRequestViewComponent {
  primengTableConfigProperties: any;
  id;
  userData;
  btnName;
  selectedTabIndex: any = 0;
  issuingWarehouseList = [];
  issuingWarehouseLocation = [];
  receivingWarehouseList = [];
  receivingWarehouseLocation = [];
  requestDetails; items;
  listOfFiles: any = [];
  fileLists: any = [];
  filteredStockCodes: any = [];
  filteredStockDescription: any = [];
  maxFilesUpload: Number = 5;
  isAddedErr: boolean = false;
  formData = new FormData();
  loading: boolean = false;
  constructor(private router: Router, private crudService: CrudService, private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private store: Store<AppState>,
    private snackbarService: SnackbarService) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.id = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    });
    this.primengTableConfigProperties = {
      tableCaption: 'View Transfer Request',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Transfer Request', relativeRouterUrl: '/radio-removal/transfer-request' }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableBreadCrumb: true,
            enableEditActionBtn: true,
          }
        ]
      }
    }
    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Transfer Request', relativeRouterUrl: '/inventory/transfer-request' });
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
  }
  ngOnInit() {
    this.combineLatestNgrxStoreData()
    this.getTransferRequestDetails();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio Removal Transfer Request"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getTransferRequestDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_TRANSFER_EDIT,
      this.id,
      false, null
    ).subscribe((data: IApplicationResponse) => {
      if (data.isSuccess && data.resources) {
        this.requestDetails = data.resources;
        if (this.requestDetails && this.requestDetails.radioRemovalTransferRequestStatusName == 'Save as Draft') {
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableEditActionBtn = true;
        }
        else
          this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableEditActionBtn = false;
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(["/radio-removal/transfer-request/add-edit"], { queryParams: { id: this.id } });
        break;
      default:
        break;
    }
  }
  download(path) {
    if (path) return fileUrlDownload(path, true);
  }
}
