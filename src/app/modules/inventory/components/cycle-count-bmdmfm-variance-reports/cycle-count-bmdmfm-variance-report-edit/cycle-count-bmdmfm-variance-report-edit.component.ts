import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { ApprovalDetailsItemsModel, VarianceReportsBMDMFMAddEditModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CycleCountVarianceReportsModalComponent } from './cycle-count-variance-reports-modal.component';
@Component({
  selector: 'app-cycle-count-bmdmfm-variance-report-edit',
  templateUrl: './cycle-count-bmdmfm-variance-report-edit.component.html',
  styleUrls: ['./cycle-count-bmdmfm-variance-report-edit.component.scss']
})
export class CycleCountBmdmfmVarianceReportEditComponent implements OnInit {
  varianceReportApprovalId: any;
  varianceReportDetails: any;
  varianceReportAddEditForm: FormGroup;
  approvalDetails: FormArray;
  actionDropDown: any = [];
  maxLevel: any;
  level: any;
  userData: UserLogin;
  isCycleCountRejectsDialog = false;
  showSaveCycleCount = false;
  cycleCountDialogForm: FormGroup;
  constructor(private router: Router, private crudService: CrudService, private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private snackbarService: SnackbarService, private dialog: MatDialog, private store: Store<AppState>,
  ) {
    this.varianceReportApprovalId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createVarianceReportAddEditForm();
    this.getDropdown();
    if (this.varianceReportApprovalId) {
      this.getVarianceReportDetailsById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.varianceReportDetails = response.resources;
          this.varianceReportAddEditForm.patchValue(response.resources);
          this.varianceReportAddEditForm.get('userId').patchValue(this.userData.userId)
          this.maxLevel = response.resources.maxLevel;
          this.level = response.resources.level;
          this.approvalDetails = response.resources.items;
          this.approvalDetails = this.getVarianceReportFormArray;
          if (response.resources.items.length > 0) {
            response.resources.items.forEach((approval) => {
              this.approvalDetails.push(this.createVarianceReportGroupForm(approval));
            });
          }
          else {
            this.approvalDetails.push(this.createVarianceReportGroupForm());
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.approvalDetails = this.getVarianceReportFormArray;
          this.approvalDetails.push(this.createVarianceReportGroupForm());
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  //Get Details
  getVarianceReportDetailsById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.VARIANCE_REPORT_APPROVAL,
      this.varianceReportApprovalId
    );
  }

  getDropdown(): void {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_VARIANCE_REPORT_ACTION_TYPE)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.actionDropDown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  /* Create formArray start */
  createVarianceReportAddEditForm(): void {
    let stockOrderModel = new VarianceReportsBMDMFMAddEditModel();
    this.varianceReportAddEditForm = this.formBuilder.group({
      approvalDetails: this.formBuilder.array([])
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.varianceReportAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.cycleCountDialogForm = this.formBuilder.group({
      reasons: ['', Validators.required]
    });
  }

  get getVarianceReportFormArray(): FormArray {
    if (this.varianceReportAddEditForm !== undefined) {
      return this.varianceReportAddEditForm.get("approvalDetails") as FormArray;
    }
  }

  /* Create FormArray controls */
  createVarianceReportGroupForm(approvalDetails?: ApprovalDetailsItemsModel): FormGroup {
    let structureTypeData = new ApprovalDetailsItemsModel(approvalDetails ? approvalDetails : undefined);
    let formControls = {};
    Object.keys(structureTypeData).forEach((key) => {
      if (this.level == '1') {
        formControls[key] = [{ value: structureTypeData[key], disabled: false },
        (key === 'level1ActionId' ? [Validators.required] : [])]
      }
      if (this.level == '2') {
        formControls[key] = [{ value: structureTypeData[key], disabled: false },
        (key === 'level2ActionId' ? [Validators.required] : [])]
      }
      if (this.level == '3') {
        formControls[key] = [{ value: structureTypeData[key], disabled: false },
        (key === 'level3ActionId' ? [Validators.required] : [])]
      }
      if (this.level == '4') {
        formControls[key] = [{ value: structureTypeData[key], disabled: false },
        (key === 'level4ActionId' ? [Validators.required] : [])]
      }
      if (this.level == '5') {
        formControls[key] = [{ value: structureTypeData[key], disabled: false },
        (key === 'level5ActionId' ? [Validators.required] : [])]
      }
      if (this.level == '6') {
        formControls[key] = [{ value: structureTypeData[key], disabled: false },
        (key === 'level6ActionId' ? [Validators.required] : [])]
      }
      if (this.level == '7') {
        formControls[key] = [{ value: structureTypeData[key], disabled: false },
        (key === 'level7ActionId' ? [Validators.required] : [])]
      }
      if (this.level == '8') {
        formControls[key] = [{ value: structureTypeData[key], disabled: false },
        (key === 'level8ActionId' ? [Validators.required] : [])]
      }
      if (this.level == '9') {
        formControls[key] = [{ value: structureTypeData[key], disabled: false },
        (key === 'level9ActionId' ? [Validators.required] : [])]
      }
      if (this.level == 'Final') {
        formControls[key] = [{ value: structureTypeData[key], disabled: false },
        (key === 'finalApprovalActionId' ? [Validators.required] : [])]
      }
    });
    return this.formBuilder.group(formControls);
  }

  selectAll(checked: boolean) {
    this.getVarianceReportFormArray.controls.forEach(control => {
      checked ? control.get('isActive').setValue(true) : control.get('isActive').setValue(false);
    });
  }

  submitFeedback(type: any) {
    if (type == 'rejected' && this.cycleCountDialogForm.invalid) {
      return;
    }
    let reasons = this.cycleCountDialogForm.get('reasons').value;
    let obj = this.actionDropDown.filter(data => {
      if (data.displayName == 'Approve') {
        return data;
      }
    });
    this.getVarianceReportFormArray.value.forEach((key, index) => {
      if (key.isActive && obj.length > 0) {
        if (this.level == 'Final') {
          type == 'approved' ? this.getVarianceReportFormArray.controls[index].get('finalApprovalActionId').patchValue(obj[0].id) :
            this.getVarianceReportFormArray.controls[index].get('finalApprovalReason').patchValue(reasons);
        }
        if (this.level == '1') {
          type == 'approved' ? this.getVarianceReportFormArray.controls[index].get('level1ActionId').patchValue(obj[0].id) :
            this.getVarianceReportFormArray.controls[index].get('level1Reason').patchValue(reasons);
        }
        if (this.level == '2') {
          type == 'approved' ? this.getVarianceReportFormArray.controls[index].get('level2ActionId').patchValue(obj[0].id) :
            this.getVarianceReportFormArray.controls[index].get('level2Reason').patchValue(reasons);
        }
        if (this.level == '3') {
          type == 'approved' ? this.getVarianceReportFormArray.controls[index].get('level3ActionId').patchValue(obj[0].id) :
            this.getVarianceReportFormArray.controls[index].get('level3Reason').patchValue(reasons);
        }
        if (this.level == '4') {
          type == 'approved' ? this.getVarianceReportFormArray.controls[index].get('level4ActionId').patchValue(obj[0].id) :
            this.getVarianceReportFormArray.controls[index].get('level4Reason').patchValue(reasons);
        }
        if (this.level == '5') {
          type == 'approved' ? this.getVarianceReportFormArray.controls[index].get('level5ActionId').patchValue(obj[0].id) :
            this.getVarianceReportFormArray.controls[index].get('level5Reason').patchValue(reasons);
        }
        if (this.level == '6') {
          type == 'approved' ? this.getVarianceReportFormArray.controls[index].get('level6ActionId').patchValue(obj[0].id) :
            this.getVarianceReportFormArray.controls[index].get('level6Reason').patchValue(reasons);
        }
        if (this.level == '7') {
          type == 'approved' ? this.getVarianceReportFormArray.controls[index].get('level7ActionId').patchValue(obj[0].id) :
            this.getVarianceReportFormArray.controls[index].get('level7Reason').patchValue(reasons);
        }
        if (this.level == '8') {
          type == 'approved' ? this.getVarianceReportFormArray.controls[index].get('level8ActionId').patchValue(obj[0].id) :
            this.getVarianceReportFormArray.controls[index].get('level8Reason').patchValue(reasons);
        }
        if (this.level == '9') {
          type == 'approved' ? this.getVarianceReportFormArray.controls[index].get('level9ActionId').patchValue(obj[0].id) :
            this.getVarianceReportFormArray.controls[index].get('level9Reason').patchValue(reasons);
        }
      }
    });
    this.isCycleCountRejectsDialog = false;
  }

  feedbackMethod(type: any) {
    let activeVal = false;
    this.getVarianceReportFormArray.value.forEach((key, index) => {
      if (key.isActive) {
        activeVal = true;
      }
    });
    if (!activeVal) {
      this.isCycleCountRejectsDialog = false;
      this.snackbarService.openSnackbar('Atleast one checkbox is required', ResponseMessageTypes.ERROR);
      return;
    }
    else {
      if (type == 'rejected') {
        this.isCycleCountRejectsDialog = true;
        this.showSaveCycleCount = false;
      }
      else {
        this.submitFeedback('approved')
      }
    }
  }

  selectAction(selectvalue: any, index: number, colName: any) {
    let obj = this.actionDropDown.filter(data => {
      if (data.id == selectvalue) {
        return true
      }
    })
    if (obj.length > 0) {
      if (obj[0].displayName == 'Reject') {
        this.getVarianceReportFormArray.controls[index].get(colName).setValidators([Validators.required]);
        this.getVarianceReportFormArray.controls[index].get(colName).updateValueAndValidity();
      }
      if (obj[0].displayName == 'Pending' || obj[0].displayName == 'Approve' || obj[0].displayName == 'Rework') {
        this.getVarianceReportFormArray.controls[index].get(colName).setErrors(null);
        this.getVarianceReportFormArray.controls[index].get(colName).clearValidators();
        this.getVarianceReportFormArray.controls[index].markAllAsTouched();
        this.getVarianceReportFormArray.controls[index].get(colName).updateValueAndValidity();
      }
    }
  }

  openModelSerialPopup(itemId: any, itemCode: any, itemName: any) {
    let data = this.varianceReportDetails.items.filter(i => {
      return i.itemId == itemId
    });
    let formArray = {
      stockCode: itemCode,
      stockDescription: itemName,
      serialNumbers: data[0].itemDetails
    }
    const dialogRemoveStockCode = this.dialog.open(CycleCountVarianceReportsModalComponent, {
      width: '500px',
      data: {
        serialNumberDetails: formArray
      }, disableClose: true
    });
    dialogRemoveStockCode.afterClosed().subscribe(result => {
      if (!result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  onSubmit(type: any): void {
    if (type == 'submit') {
      if (this.getVarianceReportFormArray.invalid) {
        return;
      }
    }
    const varianceData = {
      ...this.varianceReportAddEditForm.value,
      IsDraft: type == 'draft' ? true : false, IsSubmit: type == 'submit' ? true : false
    }
    this.rxjsService.setFormChangeDetectionProperty(true);
    let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.VARIANCE_REPORT_APPROVAL, varianceData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
      }
    });
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'cycle-count-bmdmfm-reports'], { skipLocationChange: true });
  }

  navigateToViewPage() {
    this.router.navigate(['/inventory', 'cycle-count-bmdmfm-reports', 'view'], {
      queryParams: {
        id: this.varianceReportApprovalId,
      }, skipLocationChange: true
    });
  }

}
