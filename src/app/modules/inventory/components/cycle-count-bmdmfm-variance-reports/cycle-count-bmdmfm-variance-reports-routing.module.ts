import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CycleCountBmdmfmVarianceReportEditComponent } from './cycle-count-bmdmfm-variance-report-edit/cycle-count-bmdmfm-variance-report-edit.component';
import { CycleCountBmdmfmVarianceReportsListComponent } from './cycle-count-bmdmfm-variance-reports-list/cycle-count-bmdmfm-variance-reports-list.component';
import { CycleCountBmdmfmVarianceReportsViewComponent } from './cycle-count-bmdmfm-variance-reports-view/cycle-count-bmdmfm-variance-reports-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
    { path: '', component: CycleCountBmdmfmVarianceReportsListComponent, data: { title: 'Cycle Count Variance Reports List' } ,canActivate:[AuthGuard]},
    { path: 'view', component: CycleCountBmdmfmVarianceReportsViewComponent, data: { title: 'Cycle Count Variance Reports View' },canActivate:[AuthGuard] },
    { path: 'update', component: CycleCountBmdmfmVarianceReportEditComponent, data: { title: 'Cycle Count Variance Reports Update' },canActivate:[AuthGuard] },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
})

export class CycleCountBMDMFMVarianceReportsRoutingModule { }