import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, LoggedInUserModel,currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { CycleCountVarianceReportsModalComponent } from '../cycle-count-bmdmfm-variance-report-edit/cycle-count-variance-reports-modal.component';
@Component({
  selector: 'app-cycle-count-bmdmfm-variance-reports-view',
  templateUrl: './cycle-count-bmdmfm-variance-reports-view.component.html',
  styleUrls: ['./cycle-count-bmdmfm-variance-reports-view.component.scss']
})
export class CycleCountBmdmfmVarianceReportsViewComponent implements OnInit {
  varianceReportApprovalId: any;
  varianceReportDetails: any;
  userData: UserLogin;
  primengTableConfigProperties: any;
  loggedInUserData;
  selectedTabIndex=0;
  constructor(private router: Router, private crudService: CrudService, private store: Store<AppState>,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService,
    private snackbarService: SnackbarService, private dialog: MatDialog
  ) {
    this.varianceReportApprovalId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Variance Report",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Stock Take', relativeRouterUrl: '' },{ displayName: 'Variance Report', relativeRouterUrl: '' },
      { displayName: 'Variance Report List', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Variance Report',
            dataKey: 'varianceReportApprovalId',
            enableBreadCrumb: true,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getCycleCountVarianceReport();
  }
  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.VARIANCE_REPORT];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getCycleCountVarianceReport() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.VARIANCE_REPORT_APPROVAL,
      this.varianceReportApprovalId).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.varianceReportDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  openModelSerialPopup(itemId: any, itemCode: any, itemName: any) {
    let data = this.varianceReportDetails.items.filter(i => {
      return i.itemId == itemId
    });
    let formArray = {
      stockCode: itemCode,
      stockDescription: itemName,
      serialNumbers: data[0].itemDetails
    }
    const dialogRemoveStockCode = this.dialog.open(CycleCountVarianceReportsModalComponent, {
      width: '500px',
      data: {
        serialNumberDetails: formArray
      }, disableClose: true
    });
    dialogRemoveStockCode.afterClosed().subscribe(result => {
      if (!result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'cycle-count-bmdmfm-reports'], { skipLocationChange: true });
  }

  navigateToViewPage() {
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(['/inventory', 'cycle-count-bmdmfm-reports', 'update'], {
      queryParams: {
        id: this.varianceReportApprovalId,
      }, skipLocationChange: true
    });
  }

}
