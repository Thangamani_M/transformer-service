import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { CycleCountBmdmfmVarianceReportEditComponent } from './cycle-count-bmdmfm-variance-report-edit/cycle-count-bmdmfm-variance-report-edit.component';
import { CycleCountVarianceReportsModalComponent } from './cycle-count-bmdmfm-variance-report-edit/cycle-count-variance-reports-modal.component';
import { CycleCountBmdmfmVarianceReportsListComponent } from './cycle-count-bmdmfm-variance-reports-list/cycle-count-bmdmfm-variance-reports-list.component';
import { CycleCountBMDMFMVarianceReportsRoutingModule } from './cycle-count-bmdmfm-variance-reports-routing.module';
import { CycleCountBmdmfmVarianceReportsViewComponent } from './cycle-count-bmdmfm-variance-reports-view/cycle-count-bmdmfm-variance-reports-view.component';

@NgModule({
    declarations: [
        CycleCountBmdmfmVarianceReportsListComponent,
        CycleCountBmdmfmVarianceReportsViewComponent,
        CycleCountBmdmfmVarianceReportEditComponent,
        CycleCountVarianceReportsModalComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule, FormsModule,
        CycleCountBMDMFMVarianceReportsRoutingModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
    ],
    entryComponents: [
        CycleCountVarianceReportsModalComponent
    ]
})

export class CycleCountBMDMFMVarianceReportsModule { }
