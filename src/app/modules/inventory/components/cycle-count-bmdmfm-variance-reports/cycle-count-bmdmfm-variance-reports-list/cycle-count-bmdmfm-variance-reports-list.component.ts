import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
  CrudType, LoggedInUserModel, ModulesBasedApiSuffix, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, RxjsService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { exportList } from '@app/shared/utils';
@Component({
  selector: 'app-cycle-count-bmdmfm-variance-reports-list',
  templateUrl: './cycle-count-bmdmfm-variance-reports-list.component.html',
  styleUrls: ['./cycle-count-bmdmfm-variance-reports-list.component.scss']
})
export class CycleCountBmdmfmVarianceReportsListComponent extends PrimeNgTableVariablesModel implements OnInit {
  userData: UserLogin;
  primengTableConfigProperties: any;
  row: any = {};
  otherParams;

  constructor(
    private commonService: CrudService, private snackbarService: SnackbarService,
    private router: Router, private activatedRoute: ActivatedRoute,
    private rxjsService: RxjsService, private store: Store<AppState>,
  ) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Variance Report",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Variance Report', relativeRouterUrl: '' },
      { displayName: 'Variance Report List', relativeRouterUrl: '' }],
      //selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Variance Report',
            dataKey: 'varianceReportApprovalId',
            enableBreadCrumb: true,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enablePrintBtn: true,
            printTitle: 'Variance Report',
            printSection: 'print-section0',
            enableExportBtn: true,
            columns: [{ field: 'varianceReportNumber', header: 'Variance Report ID', width: '200px' },
            { field: 'status', header: 'Status', width: '150px' },
            { field: 'warehouse', header: 'Warehouse', width: '200px' },
            { field: 'stockCountType', header: 'Stock Count Type', width: '200px' },
            { field: 'investigatedBy', header: ' Investigated By', width: '200px' },
            { field: 'investigatedDate', header: 'Investigated Date & Time', width: '200px' },
            { field: 'reviewedBy', header: ' Reviewed By', width: '200px' },
            { field: 'reviewedDate', header: 'Reviewed Date & Time', width: '200px' },
            { field: 'level1Approval', header: 'Level 1 Approval', width: '200px' },
            { field: 'level1ApprovedDate', header: 'Level 1 Approved Date', width: '200px' },
            { field: 'level2Approval', header: 'Level 2 Approval', width: '200px' },
            { field: 'level2ApprovedDate', header: 'Level 2 Approved Date', width: '200px' },
            { field: 'level3Approval', header: 'Level 3 Approval', width: '200px' },
            { field: 'level3ApprovedDate', header: 'Level 3 Approved Date' },
            { field: 'level4Approval', header: 'Level 4 Approval', width: '200px' },
            { field: 'level4ApprovedDate', header: 'Level 4 Approved Date', width: '200px' },
            { field: 'level5Approval', header: 'Level 5 Approval', width: '200px' },
            { field: 'level5ApprovedDate', header: 'Level 5 Approved Date', width: '200px' },
            { field: 'level6Approval', header: 'Level 6 Approval', width: '200px' },
            { field: 'level6ApprovedDate', header: 'Level 6 Approved Date', width: '200px' },
            { field: 'level7Approval', header: 'Level 7 Approval', width: '200px' },
            { field: 'level7ApprovedDate', header: 'Level 7 Approved Date', width: '200px' },
            { field: 'level8Approval', header: 'Level 8 Approval', width: '200px' },
            { field: 'level8ApprovedDate', header: 'Level 8 Approved Date', width: '200px' },
            { field: 'level9Approval', header: 'Level 9 Approval', width: '200px' },
            { field: 'level9ApprovedDate', header: 'Level 9 Approved Date', width: '200px' },
            { field: 'finalApproval', header: 'Final Approval', width: '200px' },
            { field: 'finalApprovedDate', header: 'Final Approved Date', width: '200px' },
            ],
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.VARIANCE_REPORT_APPROVAL,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }
    // this.activatedRoute.queryParamMap.subscribe((params) => {
    //   this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
    //   this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
    //   this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    // });

  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
    this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.VARIANCE_REPORT];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }


  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = { ...otherParams, ...{ ApproverId: this.userData.userId } }
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row;
        if (this.row['sortOrderColumn']) {
          unknownVar['sortOrder'] = this.row['sortOrder'];
          unknownVar['sortOrderColumn'] = this.row['sortOrderColumn'];
        }
        unknownVar['UserId'] = this.userData.userId;
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;

      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let pageIndex = this.row && this.row["pageIndex"] ? this.row["pageIndex"] : 0;
        let pageSize = this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
        this.exportList(pageIndex, pageSize);
        break;
      default:
    }
  }

  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams, 0, pageSize, ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.VARIANCE_REPORT_EXPORT, this.commonService, this.rxjsService, 'Variance Report');
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/inventory', 'cycle-count-bmdmfm-reports', 'view'], {
          queryParams: {
            id: editableObject['varianceReportApprovalId']
          }, skipLocationChange: true
        });
        break;
    }
  }
}
