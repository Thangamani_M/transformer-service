import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType, exportList, LoggedInUserModel, ModulesBasedApiSuffix, currentComponentPageBasedPermissionsSelector$, prepareGetRequestHttpParams, RxjsService, prepareDynamicTableTabsFromPermissions, SnackbarService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-audit-cycle-count-manager-list',
  templateUrl: './audit-cycle-count-manager-list.component.html'
})
export class AuditCycleCountManagerListComponent extends PrimeNgTableVariablesModel {
  userData: UserLogin;
  primengTableConfigProperties: any;
  row: any = {};
  otherParams;
  constructor(private snackbarService: SnackbarService, private commonService: CrudService, public dialogService: DialogService, private router: Router, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private store: Store<AppState>) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Audit Cycle Count Progress",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Audit Cycle Count Manager' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Audit Cycle Count Manager',
            dataKey: 'cycleCountId',
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableAction: true,
            enablePrintBtn: true,
            printTitle: 'Packing-dashboard Report',
            printSection: 'print-section0',
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: true,
            cursorSecondLinkIndex: 5,
            columns: [{ field: 'auditCycleCountNumber', header: 'Audit Cycle Count ID' },
            { field: 'auditCycleCountStatusName', header: 'Status' },
            { field: 'progressPercentage', header: 'Progress', Isprogress: true },
            { field: 'warehouseName', header: 'Warehouse' },
            { field: 'totalQty', header: 'On Hand Qty' },
            { field: 'scannedQty', header: 'Final Qty' },
            { field: 'varianceQty', header: 'Variance Qty' },
            { field: 'auditCycleCountInitatedDate', header: 'Initiated Date & Time' },
            ],
            enableExportBtn: true,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_MANAGER,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[2].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
    this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.AUDIT_CYCLE_COUNT_MANAGER];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = {
      userId: this.loggedInUserData?.userId,
      ...otherParams,
    }
    this.otherParams = otherParams;
    this.commonService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams)
    ).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;

        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row;
        if (this.row['sortOrderColumn']) {
          unknownVar['sortOrder'] = this.row['sortOrder'];
          unknownVar['sortOrderColumn'] = this.row['sortOrderColumn'];
        }
        this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let pageIndex = this.row && this.row["pageIndex"] ? this.row["pageIndex"] : 0;
        let pageSize = this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
        this.exportList(pageIndex, pageSize);
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams, 0, pageSize, ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_MANAGER_EXPORT, this.commonService, this.rxjsService, 'Audit Cycle Count Manager');
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        this.router.navigate(['/inventory', 'audit-cycle-count-manager', 'manager-view'], {
          queryParams: {
            id: editableObject['auditCycleCountId'],
            number: editableObject['auditCycleCountNumber'],
            warehouse: editableObject['warehouseName'],
            actionedDate: editableObject['auditCycleCountInitatedDate'],
            status: editableObject['auditCycleCountStatusName'],
            cssClass: editableObject['cssClass']
          }, skipLocationChange: true
        });

        break;
    }
  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
