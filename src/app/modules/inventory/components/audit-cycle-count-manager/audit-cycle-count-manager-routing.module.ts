import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { AuditCycleCountManagerListComponent } from './audit-cycle-count-manager-list/audit-cycle-count-manager-list.component';
import { AuditCycleCountManagerViewComponent } from './audit-cycle-count-manager-view/audit-cycle-count-manager-view.component';
const routes: Routes = [
    { path: '', component: AuditCycleCountManagerListComponent, data: { title: 'Audit Cycle Count Progress List' },canActivate:[AuthGuard] },
    { path: 'manager-view', component: AuditCycleCountManagerViewComponent, data: { title: 'Audit Cycle Count Progress View' } ,canActivate:[AuthGuard]},
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})
export class AuditCycleCountManagerRoutingModule { }