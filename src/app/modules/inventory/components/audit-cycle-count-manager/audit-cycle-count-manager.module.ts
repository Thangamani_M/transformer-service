import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { NgxBarcodeModule } from 'ngx-barcode';
import { NgxPrintModule } from 'ngx-print';
import { AuditCycleCountManagerListComponent } from './audit-cycle-count-manager-list/audit-cycle-count-manager-list.component';
import { AuditCycleCountManagerRoutingModule } from './audit-cycle-count-manager-routing.module';
import { AuditCycleCountManagerViewComponent } from './audit-cycle-count-manager-view/audit-cycle-count-manager-view.component';

@NgModule({
    declarations: [
        AuditCycleCountManagerListComponent,
        AuditCycleCountManagerViewComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule, FormsModule,
        AuditCycleCountManagerRoutingModule,
        LayoutModule,
        MaterialModule,
        SharedModule,
        NgxBarcodeModule,
        NgxPrintModule,
    ],
    entryComponents: [
    ]
})

export class AuditCycleCountManagerModule { }
