import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, of } from 'rxjs';
import { CrudService, CrudType, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService, currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR } from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { loggedInUserData } from '@modules/others';
import { AppState } from '@app/reducers';
import { Store } from '@ngrx/store';
@Component({
  selector: 'app-audit-cycle-count-manager-view',
  templateUrl: './audit-cycle-count-manager-view.component.html',
  styleUrls: ['./audit-cycle-count-manager-view.component.scss']
})
export class AuditCycleCountManagerViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  auditCycleCountId: any;
  auditCycleCountNumber: any;
  auditWarehouse: any;
  auditActionedDate: any;
  auditStatus: any;
  cssClass: any;
  auditActionby: any;
  assignedby: any;
  auditCycleCountManagerDetails: any = {};
  auditCycleCountProgressDetails: any = {};
  isWasteDisposalDialogModal: boolean;
  modalDetails: any = {};
  AuditCycleCountManagerviewDetail: any;
  primengTableConfigProperties: any;
  constructor(private store: Store<AppState>,
    private router: Router, private crudService: CrudService, private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private snackbarService: SnackbarService) {
    super();
    this.auditCycleCountId = this.activatedRoute.snapshot.queryParams.id;
    this.auditCycleCountNumber = this.activatedRoute.snapshot.queryParams.number;
    this.auditWarehouse = this.activatedRoute.snapshot.queryParams.warehouse;
    this.auditActionedDate = this.activatedRoute.snapshot.queryParams.actionedDate;
    this.auditStatus = this.activatedRoute.snapshot.queryParams.status;
    this.cssClass = this.activatedRoute.snapshot.queryParams.cssClass;
    this.assignedby = this.activatedRoute.snapshot.queryParams.assignedby;
    this.primengTableConfigProperties = {
      tableCaption: "Audit Cycle Count Progress",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Audit Cycle Count Manager', relativeRouterUrl: '/inventory/audit-cycle-count-manager' },
      { displayName: 'Audit Cycle Count Progress View', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableViewBtn: false,
            enableClearfix: true,
          }]
      }
    }
    this.onShowValue();
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getAuditCycleCountManager();
    this.getAuditCycleCountProgress();
  }
  combineLatestNgrxStoreData() {
    combineLatest([this.store.select(loggedInUserData),
    this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.AUDIT_CYCLE_COUNT_MANAGER];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onShowValue() {
    this.AuditCycleCountManagerviewDetail = [
      { name: 'Audit Cycle Count ID', value: this.auditCycleCountNumber },
      { name: 'Warehouse', value: this.auditWarehouse },
      { name: 'Actioned Date & Time', value: this.auditActionedDate },
      { name: 'Status', value: this.auditStatus, statusClass: this.cssClass },
    ]
  }

  getAuditCycleCountManager() {
    let serialParams = new HttpParams().set('AuditCycleCountId', this.auditCycleCountId);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_MANAGER_REPORT,
      undefined, true, serialParams).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.auditCycleCountManagerDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getAuditCycleCountProgress() {
    let serialParams = new HttpParams().set('AuditCycleCountId', this.auditCycleCountId);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_MANAGER_PROGRESS_REPORT,
      undefined, true, serialParams).subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.auditCycleCountProgressDetails = response.resources;

          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onModalSerialPopupOpen(cycleCountItemId) {
    if (cycleCountItemId != '') {
      let serialParams = new HttpParams().set('AuditCycleCountId', this.auditCycleCountId).set('ItemId', cycleCountItemId)
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_CYCLE_COUNT_SERIALNUMBERS, undefined, true, serialParams)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            let resp = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
            let itemDetails = resp.serialNumber.split(',')
            this.modalDetails = {
              stockCode: resp.itemCode,
              stockDescription: resp.displayName,
              serialNumbers: itemDetails
            }
            this.isWasteDisposalDialogModal = true;
          }
          else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToList();
        break;
    }
  }
  navigateToList() {
    this.router.navigate(['/inventory', 'audit-cycle-count-manager']);
  }

}
