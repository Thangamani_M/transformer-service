import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutomatedGrvCreationComponent, AutomatedGrvCreationViewComponent } from '.';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const routes: Routes = [
    { path: '', component: AutomatedGrvCreationComponent, canActivate:[AuthGuard],data: { title: 'Automated GRV Creation' } },
    { path: 'view', component: AutomatedGrvCreationViewComponent, canActivate:[AuthGuard],data: { title: 'Automated GRV Creation' } }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
})
export class AutomatedGrvCreationRoutingModule { }
