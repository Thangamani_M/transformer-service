import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, exportList, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-automated-grv-creation',
  templateUrl: './automated-grv-creation.component.html'
})
export class AutomatedGrvCreationComponent extends PrimeNgTableVariablesModel implements OnInit {
  userData: UserLogin;
  primengTableConfigProperties: any;
  row: any = {};
  otherParams;
  constructor(private snackbarService:SnackbarService,private crudService: CrudService, public dialogService: DialogService, private router: Router, private rxjsService: RxjsService, private store: Store<AppState>, private datePipe: DatePipe) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Goods Return List",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Goods Return List' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Call Status Report',
            dataKey: 'goodsReturnId',
            enableAction: true,
            enableBreadCrumb: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enablePrintBtn: true,
            printTitle: 'Goods Return List',
            printSection: 'print-section0',
            printStyle: true,
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            enableScanActionBtn: false,
            cursorLinkIndex: 0,
            columns: [
              { field: 'goodsReturnNumber', header: 'GRV Number', width: '200px' },
              { field: 'status', header: 'Status', width: '200px' },
              { field: 'qrCode', header: 'QR Code', width: '200px' },
              { field: 'warehouseName', header: 'Warehouse', width: '200px' },
              { field: 'referenceId', header: 'Reference ID', width: '200px' },
              { field: 'returnType', header: 'Return Type', width: '200px' },
              { field: 'createdBy', header: 'Created By', width: '200px' },
              { field: 'createdDate', header: 'Created Date & Time', width: '200px' },

            ],
            shouldShowDeleteActionBtn: false,
            enableAddActionBtn: false, // API is not ready
            shouldShowFilterActionBtn: false, // API is not ready
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportBtn: true,
            enableExportCSV: false,
            apiSuffixModel: InventoryModuleApiSuffixModels.GRV_LIST_DETAILS,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getGRVDetails();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][INVENTORY_COMPONENT.GOODS_RETURN]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getGRVDetails(pageIndex?: string, pageSize?: string, otherParams?: object) {

    otherParams = { ...otherParams, ...{ UserId: this.userData.userId } };
    this.otherParams = otherParams;
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].apiSuffixModel;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize,
        otherParams ? otherParams : {
          UserId: this.userData.userId
        })
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.createdDate = this.datePipe.transform(val?.createdDate, 'dd-MM-yyyy, h:mm:ss a');
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        data.resources = null;
        this.dataList = data.resources
        this.totalRecords = 0;
      }
    })
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        this.row = row;
        unknownVar = { ...unknownVar, ...{ UserId: this.userData.userId } }
        this.getGRVDetails(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        let pageIndex = this.row && this.row["pageIndex"] ? this.row["pageIndex"] : 0;
        let pageSize = this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
        this.exportList(pageIndex, pageSize);
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    exportList(this.otherParams, 0, pageSize, ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.GOODSRETURN_EXPORT, this.crudService, this.rxjsService, 'Goods Return List');
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.CREATE:
        break;
      case CrudType.VIEW:
        this.router.navigate(['/inventory/grv-creation/view'], { queryParams: { id: editableObject['goodsReturnId'] } });
        break;
    }
  }
}
