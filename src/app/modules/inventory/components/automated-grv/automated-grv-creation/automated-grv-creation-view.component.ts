import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-automated-grv-creation-view',
  templateUrl: './automated-grv-creation-view.component.html',
  styleUrls: ['./automated-grv-creation-view.component.scss']
})
export class AutomatedGrvCreationViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  goodsReturnId: any;
  grvDetails: any = {};
  stockDetails;
  stockDetailListDialog: boolean = false;
  AutomatedGrvCreationDetails:any;
  primengTableConfigProperties: any
  constructor(private rjxService: RxjsService,
    private crudService: CrudService, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute) {
      super();
    this.goodsReturnId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption:'View Goods Return Request',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Goods Return List', relativeRouterUrl: '/inventory/grv-creation' }, { displayName: 'View Goods Return Request', relativeRouterUrl: '', }],
      tableComponentConfigs: {
          tabsList: [
              {
                  enableAction: false,
                  enableEditActionBtn: false,
                  enableBreadCrumb: true,
              }
          ]
      }
  }
  this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }

  ngOnInit(): void {
    if (this.goodsReturnId) {
      this.getAdminFeeDetails();
    }
  }

  //Get Details 
  getAdminFeeDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.GRV_DETAILS,
      undefined,
      false,
      prepareRequiredHttpParams({ GoodsReturnId: this.goodsReturnId })
    ).subscribe((response: IApplicationResponse) => {
      this.grvDetails = response.resources;
      this.viewAutomatedGRVCreationDetails(response.resources);
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }
  viewAutomatedGRVCreationDetails(response?:any){
    this.AutomatedGrvCreationDetails = [
      { name: 'GRV Number', value: response?.goodsReturnNumber, },
      { name: 'QR Code', value: response?.qrCode, },
      { name: 'Reference ID', value: response?.referenceId, },
      { name: 'Return Type', value: response?.returnType, },
      { name: 'Created By', value: response?.createdBy, },
      { name: 'Created Date & Time', value: response?.createdDate, },
      { name: 'Transfer From Location', value: response?.transferFromLocation, },
      { name: 'Transfer To Location', value: response?.transferToLocation, },
      { name: 'Warehouse', value: response?.warehouseName, },
      { name: 'Status', value: response?.status, statusClass: response ? response?.cssClass : '' },  
    ]
  }

  stockDetail(goodsReturnItemId: any) {
    if (goodsReturnItemId != '') {
      let serialParams = new HttpParams().set('GoodsReturnItemId', goodsReturnItemId);
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.GOODS_RETURN_ITEM_SERIALNUMBERS, undefined, true, serialParams)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode == 200) {
            this.stockDetails = {
              stockCode: response.resources.itemCode,
              stockDescription: response.resources.itemDescription,
              serialNumbers: response.resources.goodsReturnSerialNumbersList
            }
            this.stockDetailListDialog = true;
            this.rjxService.setGlobalLoaderProperty(false);
          }
          else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rjxService.setGlobalLoaderProperty(false);
          }
        });
    }
  }
}
