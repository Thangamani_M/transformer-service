import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AutomatedGrvCreationComponent, AutomatedGrvCreationRoutingModule } from '.';
import { AutomatedGrvCreationViewComponent } from './automated-grv-creation/automated-grv-creation-view.component';
@NgModule({
  declarations: [AutomatedGrvCreationComponent, AutomatedGrvCreationViewComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    AutomatedGrvCreationRoutingModule,
    MaterialModule,
    SharedModule
  ]
})
export class AutomatedGrvCreationModule { }
