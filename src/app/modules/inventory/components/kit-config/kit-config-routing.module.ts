import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { KitConfigurationAddEditComponent, KitConfigurationListComponent } from '@inventory/components/kit-config';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
    { path: '', component: KitConfigurationListComponent, data: { title: 'Kit Configuration List' },canActivate:[AuthGuard] },
    { path: 'view', component: KitConfigurationAddEditComponent, data: { title: 'Kit Configuration View' },canActivate:[AuthGuard] },
    { path: 'add-edit', component: KitConfigurationAddEditComponent, data: { title: 'Kit Configuration Add Edit' },canActivate:[AuthGuard] },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class KitConfigRoutingModule { }