import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { clearFormControlValidators, ConfirmDialogModel, ConfirmDialogPopupComponent, CrudService, CrudType, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, currentComponentPageBasedPermissionsSelector$,prepareDynamicTableTabsFromPermissions,prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService, LoggedInUserModel } from '@app/shared';
import { MomentService } from '@app/shared/services/moment.service';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory';
import { KitConfigurationAddEditModel, KitStockDetailsModel } from '@modules/inventory/models';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin, Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-kit-configuration-add-edit',
  templateUrl: './kit-configuration-add-edit.component.html',
  styleUrls: ['./kit-configuration-add-edit.component.scss']
})
export class KitConfigurationAddEditComponent implements OnInit {

  primengTableConfigProperties: any;
  selectedTabIndex: any = 0; //selected tab
  kitConfigurationAddEditForm: FormGroup; // form group
  viewable: boolean; //view page
  btnName; //submit button name
  kitConfigId; //to save id
  kitConfigDetail: any; //to save edited info
  itemId: any;
  itemIdList: any = []; //to save itemCode info
  isSubmitted: boolean; //disabled after submit clicked
  selectedIndex: any = 0;
  kitIdList: any;
  isKitIdError: boolean
  kitIdErrorMessage: string;
  isKitIdBlank: boolean;
  showKitIdError: boolean;
  isKitIdLoading: boolean;
  isShowKitTypeStatus: boolean;
  isShowRevertDate: boolean;
  kitNameList: any;
  isKitNameError: boolean;
  showKitNameError: boolean = false;
  kitNameErrorMessage: any = '';
  isKitNameBlank: boolean;
  isKitNameLoading: boolean;
  filteredStockCodes: any;
  isStockCodeBlank: boolean = false;
  isStockCodeLoading: boolean;
  showStockCodeError: boolean = false;
  stockCodeErrorMessage: any = '';
  filteredStockDescription: any;
  isStockDescriptionSelected: boolean = false;
  isstockDescLoading: boolean;
  isLoading: boolean;
  StockDescErrorMessage: any = '';
  showStockDescError: boolean = false;
  userData: any;
  commentsList: any;
  kitTypeList: any;
  warehouseList: any = [];
  ownershipList: any;
  kitTypeStatusList: any;
  statusList: any = [{ displayName: 'Active', id: true }, { displayName: 'InActive', id: false }];
  showAction: boolean;
  todayDate: any;
  dropdownSubscription: any;
  eventSubscription: any;
  isANumberOnly = new CustomDirectiveConfig({ isANumberOnly: true });
  ModifiedUserId: any;
  startTodayDate = new Date();
  loggedInUserData;
  
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private store: Store<AppState>, private formBuilder: FormBuilder, private rxjsService: RxjsService,
    private snackbarService: SnackbarService, private datePipe: DatePipe, private momentService: MomentService, private crudService: CrudService, private dialog: MatDialog,) {
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.kitConfigId = (Object.keys(params['params']).length > 0) ? params['params']['id'] : '';
    });
    this.primengTableConfigProperties = {
      tableCaption: this.kitConfigId && !this.viewable ? 'Update Kit Configuration' : this.viewable ? 'View Kit Configuration' : 'Add Kit Configuration',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Kit Configuration', relativeRouterUrl: '/inventory/kit-config', }, { displayName: 'Add Kit', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: false,
            enableBreadCrumb: true,
            dataKey: 'kitConfigItemId',
            formArrayName: 'kitStockDetailsArray',
            columns: [
              { field: 'stockCode', displayName: 'Stock Code', type: 'input_text', className: 'col-md-2', isTooltip: true },
              { field: 'stockDescription', displayName: 'Stock Description', type: 'input_text', className: 'col-md-5', isTooltip: true },
              {
                field: 'comments', displayName: 'Comments', type: 'input_pdropdown', className: 'col-md-2', assignValue: 'id', options: [],
                displayValue: 'displayName', emptyFilterMessage: 'No Comments Exists', placeholder: 'Select Comments', showClear: false, disabled: false
              },
              { field: 'quantity', displayName: 'quantity', type: 'input_number', className: 'col-md-2', validateInput: this.isANumberOnly },
            ],
            isRemoveFormArray: true,
            deleteAPI: InventoryModuleApiSuffixModels.KIT_CONFIGURATION_ITEM,
          }
        ]
      }
    }
    if (!this.viewable && !this.kitConfigId) {
      this.todayDate = new Date();
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.eventSubscription = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((data: any) => {
      if (data?.url?.indexOf('kit-config/add-edit?id') > -1) {
        this.viewable = false;
        this.onLoadValue();
      }
    })
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.onLoadValue();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.KIT_CONFIGURATION]

      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onLoadValue() {
    this.initForm();
    this.onPrimeTitleChanges();
    this.loadAllDropdown();
    if (!this.viewable) {
      this.kitConfigAddEditValueChanges();
    }
    this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = false;
    if (this.viewable) {
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableAction = true;
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].enableViewBtn = true;
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].isRemoveFormArray = false;
      this.onShowValue();
    }
  }

  onPrimeTitleChanges() {
    this.viewable = this.router.url.indexOf('view') !== -1;
    this.primengTableConfigProperties.tableCaption = this.kitConfigId && !this.viewable ? 'Update Kit Configuration' : this.viewable ? 'View Kit Configuration' : 'Add Kit Configuration';
    this.showAction = this.viewable ? false : true;
    this.btnName = this.kitConfigId ? 'Update' : 'Add';
    this.primengTableConfigProperties.breadCrumbItems[2]['displayName'] = this.kitConfigId ? 'View Kit Configuration' : 'Add Kit Configuration';
    this.primengTableConfigProperties.breadCrumbItems[2]['relativeRouterUrl'] = this.kitConfigId && !this.viewable ? '/inventory/kit-config/view' : '';
    this.primengTableConfigProperties.breadCrumbItems[2]['queryParams'] = this.kitConfigId && !this.viewable ? { id: this.kitConfigId } : '';
    if (!this.viewable && this.kitConfigId && this.primengTableConfigProperties.breadCrumbItems?.length == 3) {
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'Update Kit configuration' })
    }
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: number | string): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
        this.onEditButtonClicked();
        break;
      default:
        break;
    }
  }

  onEditButtonClicked(): void {
    this.router.navigate(['./../add-edit'], { relativeTo: this.activatedRoute, queryParams: { id: this.kitConfigId }, skipLocationChange: true })
  }

  viewValue(response) {
    if (response?.isSuccess && response?.statusCode == 200) {
      this.onShowValue(response);
      this.getFormArrayValue(response);
    }
  }

  onShowValue(response?: any) {
    this.kitConfigDetail = [
      { name: 'Kit ID', value: response ? response?.resources?.kitId : '' },
      { name: 'Kit Name', value: response ? response?.resources?.kitName : '' },
      { name: 'Warehouse', value: response ? response?.resources?.warehouse : '' },
      { name: 'Ownership', value: response ? response?.resources?.ownership : '' },
      { name: 'Kit Type', value: response ? response?.resources?.kitType : '' },
      { name: 'Kit Type Status', value: response ? response?.resources?.kitTypeStatus : '' },
      { name: 'Revert Date', value: response ? this.datePipe.transform(response?.resources?.revertDate, 'yyyy-MM-dd'): '', isValue:true },
      { name: 'Created By', value: response ? response?.resources?.createdBy : '' },
      { name: 'Created Date', value: response ? response?.resources?.createdDate : '' },
      { name: 'Status', value: response ? response?.resources?.status : '', statusClass: response ? response?.resources?.cssClass : '' },
    ];
  }

  editValue(response) {
    if (response?.isSuccess && response?.statusCode == 200) {
      this.kitConfigDetail = response?.resources;
      const kitType = this.kitTypeList?.find(el => el.displayName == response?.resources?.kitType);
      const warehouse = this.warehouseList?.find(el => el.displayName == response?.resources?.warehouse)?.id;
      const ownership = this.ownershipList?.find(el => el.displayName == response?.resources?.ownership);
      const kitTypeStatus = this.kitTypeStatusList?.find(el => el.displayName == response?.resources?.kitTypeStatus);
      const status = this.statusList?.find(el => el.displayName == response?.resources?.status);
      this.todayDate = response?.resources?.revertDate ? new Date(response?.resources?.revertDate) : '';
      this.kitConfigurationAddEditForm.patchValue({
        kitType: kitType,
        warehouse: warehouse ? warehouse : this.userData?.warehouseId,
        ownership: ownership,
        kitId: { kitId: response?.resources?.kitId },
        kitName: { kitName: response?.resources?.kitName },
        kitTypeStatus: kitTypeStatus,
        revertDate: response?.resources?.revertDate ? new Date(response?.resources?.revertDate) : '',
        createdBy: response?.resources?.createdBy,
        createdDate: this.datePipe.transform(response?.resources?.createdDate, 'dd-MM-yyyy hh:mm a'),
        status: status,
      }, { emitEvent: false });
      this.kitIdList = this.kitNameList = [{ kitId: response?.resources?.kitId, kitName: response?.resources?.kitName }];
      this.isKitIdBlank = this.isKitNameBlank = true;
      this.onChangeKitType(response?.resources?.kitType);
      this.getFormArrayValue(response);
    }
  }

  getFormArrayValue(response) {
    response?.resources?.kitConfigItemDetail.forEach(el => {
      const addObj = {
        stockCode: el?.stockCode,
        stockDescription: el?.stockDescription,
        comments: { id: el?.kitCommentTypeId, displayName: el?.comment },
        quantity: el?.requestedQty,
      }
      if (!this.viewable && this.kitConfigId) {
        this.itemIdList.push(el?.itemId);
      }
      this.initFormArray(addObj);
    });
    this.isLoading = false;
  }

  getValue() {
    let passObj;
    if (this.kitConfigId) {
      passObj = { KitConfigId: this.kitConfigId };
    } else if (this.itemId) {
      passObj = { itemId: this.itemId };
    }
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.KIT_CONFIGURATION_DETAILS, null, true,
      prepareRequiredHttpParams(passObj))
  }

  getReqRevertDate() {
    return this.kitConfigurationAddEditForm.get('kitTypeStatus').value?.displayName?.toLowerCase() == 'temporary';
  }

  kitConfigAddEditValueChanges() {
    this.kitConfigurationAddEditForm.get('kitType').valueChanges
      .subscribe(res => {
        this.onChangeKitType(res?.displayName);
        this.kitConfigurationAddEditForm.patchValue({
          revertDate: '',
          kitTypeStatus: '',
        })
        this.setFormValidators();
      })
    this.kitConfigurationAddEditForm.get('warehouse').valueChanges
      .subscribe(res => {
        if (!this.viewable && res && !this.kitConfigId) {
          this.clearFormArray(this.getKitFormArray);
          this.itemIdList = [];
          this.setFormValidators();
          this.kitConfigurationAddEditForm.patchValue({
            stockCode: '',
            stockDescription: '',
            comments: '',
            quantity: '',
          });
          this.filteredStockCodes = [];
          this.filteredStockDescription = [];
        }
      })
    this.kitConfigurationAddEditForm.get('kitTypeStatus').valueChanges
      .subscribe(res => {
        if (res) {
          if (this.getReqRevertDate()) {
            this.kitConfigurationAddEditForm = setRequiredValidator(this.kitConfigurationAddEditForm, ["revertDate"]);
          } else {
            this.kitConfigurationAddEditForm = clearFormControlValidators(this.kitConfigurationAddEditForm, ["revertDate"]);
          }
        }
      });
    this.kitConfigurationAddEditForm.get('stockCode').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        this.isStockCodeLoading = false;
        this.showHideStockDescEmptyError();
        if (this.showStockCodeError == false) {
          this.showHideStockCodeEmptyError();
        }
        else if (!this.validationKitIdStockCode() && !this.validationKitIdStockCodeError()) {
          this.showStockCodeError = false;
        }
        if (searchText != null) {
          if (this.isStockCodeBlank == false) {
            this.kitConfigurationAddEditForm.controls.stockDescription.patchValue(null);
          }
          else {
            this.isStockCodeBlank = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.showHideStockCodeEmptyError();
            }
            else {
              this.showHideStockCodeEmptyError('Stock code is not available in the system');
            }
            return this.filteredStockCodes = [];
          } else {
            if (typeof searchText == 'object') {
              searchText = searchText?.displayName;
            }
            this.isStockCodeLoading = true;
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_CODE, null, true,
              prepareRequiredHttpParams({ searchText, isAll: false, warehouseId: this.kitConfigurationAddEditForm.value?.warehouse ? this.kitConfigurationAddEditForm.value?.warehouse : this.kitConfigurationAddEditForm.controls?.warehouse.value }))
          }
        }
        else {
          return this.filteredStockCodes = [];
        }
      })
    ).subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200 && response?.resources?.length > 0) {
        this.filteredStockCodes = response?.resources;
        if (!this.validationKitIdStockCode() && !this.validationKitIdStockCodeError()) {
          this.showHideStockCodeEmptyError();
        }
      } else {
        this.filteredStockCodes = [];
        this.showHideStockCodeEmptyError('Stock code is not available in the system');
      }
      this.isStockCodeLoading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    });

    this.kitConfigurationAddEditForm.get('stockDescription').valueChanges.pipe(debounceTime(100), distinctUntilChanged(),
      switchMap(searchText => {
        this.isstockDescLoading = false;
        if ((searchText != null)) {
          this.showHideStockCodeEmptyError();
          if (this.showStockCodeError == false) {
            this.showHideStockDescEmptyError();
          }
          else {
            this.showStockDescError = false;
          }
          if (this.isStockDescriptionSelected == false) {
            this.kitConfigurationAddEditForm.controls.stockCode.patchValue(null);
          }
          else {
            this.isStockDescriptionSelected = false;
          }
          if (!searchText) {
            if (searchText === '') {
              this.showHideStockDescEmptyError();
            }
            else {
              this.showHideStockDescEmptyError('Stock description is not available in the system');
            }
            return this.filteredStockDescription = [];
          } else {
            if (typeof searchText == 'object') {
              searchText = searchText?.displayName;
            }
            this.isstockDescLoading = true;
            return this.crudService.get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.UX_ITEM_SEARCH_BY_NAME, null, true,
              prepareRequiredHttpParams({ searchText, isAll: false, warehouseId: this.kitConfigurationAddEditForm.value?.warehouse ? this.kitConfigurationAddEditForm.value?.warehouse : this.kitConfigurationAddEditForm.controls?.warehouse.value }))
          }
        } else {
          return this.filteredStockDescription = [];
        }
      })).subscribe((response: IApplicationResponse) => {
        if (response?.isSuccess && response?.statusCode == 200 && response?.resources?.length > 0) {
          this.filteredStockDescription = response?.resources;
          this.showHideStockDescEmptyError();
        } else {
          this.filteredStockDescription = [];
          this.showHideStockDescEmptyError('Stock description is not available in the system');
        }
        this.isStockDescriptionSelected = this.isstockDescLoading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onSearchKitID(searchText) {
    if (searchText?.query) {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_KIT_NAME, null, true, prepareRequiredHttpParams({ kitId: searchText?.query }), 1)
        .subscribe((response: any) => {
          if (response?.isSuccess && response?.statusCode == 200) {
            if (response.resources != null) {
              this.kitIdList = response.resources;
              this.showHideKitIdEmptyError();
              if (this.kitIdList.length == 0) {
                this.kitIdList = [];
                this.showHideKitIdEmptyError('Kit Id is not available in the system');
              }
            } else {
              this.kitIdList = [];
              this.showHideKitIdEmptyError('Kit Id is not available in the system');
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else {
      this.kitConfigurationAddEditForm?.get('kitName')?.setValue('', { emitEvent: false });
      this.kitIdList = [];
      this.kitNameList = [];
      this.showHideKitIdEmptyError('');
    }
  }

  onSearchKitName(searchText) {
    if (searchText?.query) {
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_KIT_NAME, null, true, prepareRequiredHttpParams({ kitName: searchText?.query }), 1)
        .subscribe((response: any) => {
          this.kitNameList = response?.resources?.length ? response?.resources : [];
          this.showHideKitNameEmptyError(response?.resources?.length ? '' : 'Kit Name does not exist');
          this.isKitNameLoading = false;
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    } else {
      this.kitConfigurationAddEditForm?.get('kitId')?.setValue('', { emitEvent: false });
      this.kitIdList = [];
      this.kitNameList = [];
      this.showHideKitNameEmptyError('');
    }
  }

  onSelectKitID(searchText, type) {
    this.onSelectKitAutoComplete(searchText?.kitId, type);
  }

  onSelectKitName(searchText, type) {
    this.onSelectKitAutoComplete(searchText?.kitName, type);
  }

  onChangeKitType(val) {
    if (val?.toLowerCase() == "off the shelf") {
      this.isShowKitTypeStatus = this.isShowRevertDate = false;
    } else if (val?.toLowerCase() == "buildup kit") {
      this.isShowKitTypeStatus = this.isShowRevertDate = true;
    }
  }

  onStatusChange() {
    this.isSubmitted = true;
    this.ModifiedUserId = this.userData.userId;
    this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.KIT_CONFIGURATION_STATUS,
      { ids: this.kitConfigId, isActive: this.kitConfigurationAddEditForm.value.status?.id, ModifiedUserId: this.ModifiedUserId }).subscribe(res => {
        if (res.isSuccess) {
          this.isSubmitted = false;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      })
  }

  filterServiceTypeDetailsBySearchOptions(searchValue?: string, searchKey?: any): Observable<IApplicationResponse> {
    if (searchValue?.toString()?.length < 0) {
      return;
    } else {
      const searchObj = {};
      searchObj[searchKey] = searchValue;
      return searchValue ? this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_KIT_NAME, null, true, prepareRequiredHttpParams(searchObj), 1) : of();
    }
  }

  showHideKitIdEmptyError(msg?) {
    this.kitIdErrorMessage = msg ? msg : '';
    this.showKitIdError = msg ? true : false;
  }

  showHideKitNameEmptyError(msg?) {
    this.kitNameErrorMessage = msg ? msg : '';
    this.showKitNameError = msg ? true : false;
  }

  showHideStockCodeEmptyError(msg?) {
    this.stockCodeErrorMessage = msg ? msg : '';
    this.showStockCodeError = msg ? true : false;
  }

  showHideStockDescEmptyError(msg?) {
    this.StockDescErrorMessage = msg ? msg : '';
    this.showStockDescError = msg ? true : false;
  }

  onStatusCodeSelected(value, type) {
    let stockItemData;
    if (type === 'stockCode') {
      stockItemData = this.getKitFormArray.value.find(stock => stock.stockCode === value);
      if (this.validationKitIdStockCode() || this.validationKitIdStockCodeError()) {
        this.showHideStockCodeEmptyError("Kid Id and Stock Code should not be the same");
        return;
      } else if (!stockItemData) {
        this.isStockDescriptionSelected = true;
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM, this.filteredStockCodes.find(x => x.displayName === value.displayName).id).subscribe((response) => {
          if (response?.statusCode == 200) {
            this.filteredStockDescription = [{
              id: response?.resources?.itemId,
              displayName: response?.resources?.itemName
            }];
            this.kitConfigurationAddEditForm.controls.stockDescription.patchValue(this.filteredStockDescription[0]);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
      }
      else {
        this.kitConfigurationAddEditForm.controls.stockDescription.patchValue(null);
        this.kitConfigurationAddEditForm.controls.stockCode.patchValue(null);
        this.showHideStockCodeEmptyError("Stock code is already present");
      }
    } else if (type === 'stockDescription') {
      stockItemData = this.getKitFormArray.value.find(stock => stock.stockDescription === value);
      if (!stockItemData) {
        this.isStockCodeBlank = true;
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ITEM, this.filteredStockDescription.find(x => x.displayName === value.displayName).id).subscribe((response) => {
          if (response?.statusCode == 200) {
            this.filteredStockCodes = [{
              id: response?.resources?.itemId,
              displayName: response?.resources?.itemCode
            }];
            this.kitConfigurationAddEditForm.controls.stockCode.patchValue(this.filteredStockCodes[0]);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
      }
      else {
        this.kitConfigurationAddEditForm.controls.stockDescription.patchValue(null);
        this.kitConfigurationAddEditForm.controls.stockCode.patchValue(null);
        this.showStockDescError = true;
        this.StockDescErrorMessage = 'Stock Description is already present';
      }
    }
  }

  displayKitFn(val) {
    if (val && !this.viewable) { return val?.kitName ? val?.kitName : val; } else { return val }
  }

  displayFn(val) {
    if (val && !this.viewable) { return val?.displayName ? val?.displayName : val; } else { return val }
  }

  onSelectKitAutoComplete(value, type) {
    let kitItemSelected;
    if (type == 'kitId' && !kitItemSelected) {
      if (this.validationKitIdStockCode() || this.validationKitIdStockCodeError()) {
        this.kitIdList = [];
        this.showHideKitIdEmptyError("Kid Id and Stock Code should not be the same");
        return;
      } else if (!kitItemSelected) {
        this.filterServiceTypeDetailsBySearchOptions(value, 'kitId').subscribe((res: any) => {
          if (res.statusCode == 200 && res?.statusCode == 200) {
            this.kitNameList = res?.resources;
            this.showKitNameError = res?.resources?.length ? false : true;
            this.kitConfigurationAddEditForm.patchValue({ kitName: (!res?.resources) ? '' : res?.resources[0] }, { emitEvent: false })
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
      } else {
        this.kitConfigurationAddEditForm.patchValue({ kitName: null }, { emitEvent: false })
      }
    } else if (type == 'kitName' && !kitItemSelected) {
      if (!kitItemSelected) {
        this.filterServiceTypeDetailsBySearchOptions(value, 'kitName').subscribe((res: any) => {
          if (res.statusCode == 200 && res?.statusCode == 200) {
            this.kitIdList = res?.resources;
            this.showKitIdError = res?.resources?.length ? false : true;
            this.kitConfigurationAddEditForm.patchValue({ kitId: (!res?.resources) ? '' : res?.resources[0] }, { emitEvent: false })
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        })
      } else {
        this.kitConfigurationAddEditForm.patchValue({ kitId: null }, { emitEvent: false })
      }
    }
  }

  initForm(kitConfigurationAddEditModel?: KitConfigurationAddEditModel) {
    let kitConfigurationModel = new KitConfigurationAddEditModel(kitConfigurationAddEditModel);
    this.kitConfigurationAddEditForm = this.formBuilder.group({});
    Object.keys(kitConfigurationModel).forEach((key) => {
      if (typeof kitConfigurationModel[key] === 'object') {
        this.kitConfigurationAddEditForm.addControl(key, new FormArray(kitConfigurationModel[key]));
      } else if (!this.viewable) {
        this.kitConfigurationAddEditForm.addControl(key, new FormControl(kitConfigurationModel[key]));
      }
    });
    if (!this.viewable) {
      this.setFormValidators();
    }
    if (this.kitConfigId && !this.viewable) {
      this.kitConfigurationAddEditForm.get('kitId').disable();
      this.kitConfigurationAddEditForm.get('kitName').disable();
    }
  }

  setFormValidators() {
    this.kitConfigurationAddEditForm = setRequiredValidator(this.kitConfigurationAddEditForm,
      ["kitType", "warehouse", "ownership", "kitId", "kitName", "kitTypeStatus", "stockCode",
        "stockDescription", "comments", "quantity", "kitStockDetailsArray"]);
    this.clearKitTypeStatus();
  }

  clearKitTypeStatus() {
    if (this.kitConfigurationAddEditForm.value.kitType?.displayName?.toLowerCase() == "off the shelf") {
      this.kitConfigurationAddEditForm = clearFormControlValidators(this.kitConfigurationAddEditForm, ["kitTypeStatus"]);
    }
  }

  get getKitFormArray(): FormArray {
    if (!this.kitConfigurationAddEditForm) return;
    return this.kitConfigurationAddEditForm.get("kitStockDetailsArray") as FormArray;
  }

  initFormArray(kitStockDetailsModel?: KitStockDetailsModel) {
    let kitConfigurationModel = new KitStockDetailsModel(kitStockDetailsModel);
    let kitConfigurationFormArray = this.formBuilder.group({});
    Object.keys(kitConfigurationModel).forEach((key) => {
      kitConfigurationFormArray.addControl(key, new FormControl(kitConfigurationModel[key]));
    });
    kitConfigurationFormArray = setRequiredValidator(kitConfigurationFormArray,
      ["stockCode", "stockDescription", "comments", "quantity"]);
    kitConfigurationFormArray.get('stockCode').disable();
    kitConfigurationFormArray.get('stockDescription').disable();
    if (this.viewable) {
      kitConfigurationFormArray.get('comments').disable();
      kitConfigurationFormArray.get('quantity').disable();
    }
    this.getKitFormArray.push(kitConfigurationFormArray);
    this.kitConfigurationAddEditForm = clearFormControlValidators(this.kitConfigurationAddEditForm, ["stockCode",
      "stockDescription", "comments", "quantity"]);
    this.kitConfigurationAddEditForm.patchValue({
      stockCode: '',
      stockDescription: '',
      comments: '',
      quantity: '',
    })
    if (!this.viewable) {
      this.setStockDetailValueChanges();
    }
  }

  setStockDetailValueChanges() {
    this.kitConfigurationAddEditForm.get("stockCode").valueChanges.subscribe(val => {
      this.stockDetailsValue(val);
    })
    this.kitConfigurationAddEditForm.get("stockDescription").valueChanges.subscribe(val => {
      this.stockDetailsValue(val);
    })
    this.kitConfigurationAddEditForm.get("comments").valueChanges.subscribe(val => {
      this.stockDetailsValue(val);
    })
    this.kitConfigurationAddEditForm.get("quantity").valueChanges.subscribe(val => {
      this.stockDetailsValue(val);
    })
  }

  stockDetailsValue(val) {
    if (val) {
      this.kitConfigurationAddEditForm = setRequiredValidator(this.kitConfigurationAddEditForm,
        ["kitType", "warehouse", "ownership", "kitId", "kitName", "kitTypeStatus", "stockCode",
          "stockDescription", "comments", "quantity"]);
    } else {
      this.kitConfigurationAddEditForm = clearFormControlValidators(this.kitConfigurationAddEditForm, ["stockCode",
        "stockDescription", "comments", "quantity"]);
    }
    this.clearKitTypeStatus();
  }

  loadAllDropdown() {
    let api: any;
    let params = new HttpParams().set('userId', this.userData.userId)
    if (!this.viewable) {
      api = [this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE, undefined, true, params).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_KIT_TYPE, null).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM_OWNER_SHIP_TYPE, null).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_KIT_TYPE_STATUS, null).pipe(map(result => result), catchError(error => of(error))),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_KIT_COMMENT_TYPE, null).pipe(map(result => result), catchError(error => of(error))),
      ]
    }
    if (this.viewable) {
      this.isLoading = true;
      api = [this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_KIT_COMMENT_TYPE, null).pipe(map(result => result), catchError(error => of(error))),]
    }
    if (this.kitConfigId) {
      api.push(this.getValue().pipe(map(result => result), catchError(error => of(error))))
    }
    if (this.dropdownSubscription && !this.dropdownSubscription.closed) {
      this.dropdownSubscription.unsubscribe();
    }
    this.dropdownSubscription = forkJoin(api).subscribe((response: IApplicationResponse[]) => {
      response?.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200 && !this.viewable) {
          switch (ix) {
            case 0:
              this.warehouseList = resp.resources;
              if (!this.viewable && !this.kitConfigId && this.warehouseList?.length) {
                this.kitConfigurationAddEditForm?.get('warehouse')?.setValue(this.userData?.warehouseId);
              }
              break;
            case 1:
              this.kitTypeList = resp.resources;
              break;
            case 2:
              this.ownershipList = resp.resources;
              break;
            case 3:
              this.kitTypeStatusList = resp.resources;
              break;
            case 4:
              this.commentsList = resp.resources;
              this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns[2].options = resp.resources;
              break;
            case 5:
              this.editValue(resp);
              break;
          }
        } else if (resp.isSuccess && resp.statusCode === 200 && this.viewable) {
          switch (ix) {
            case 0:
              this.commentsList = resp.resources;
              this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].columns[2].options = resp.resources;
              break;
            case 1:
              this.viewValue(resp);
              break;
          }
        }
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  validationKitIdStockCode() {
    if (this.kitConfigurationAddEditForm.value?.kitId == this.kitConfigurationAddEditForm.value?.stockCode?.displayName) {
      return true;
    }
  }

  validationKitIdStockCodeError() {
    let flag;
    this.getKitFormArray.controls.forEach((el: any) => {
      if (el?.controls.stockCode?.value == this.kitConfigurationAddEditForm.value?.kitId) {
        flag = true;
      }
    });
    return flag;
  }

  ValidateReqQty(ReqQty) {
    if (parseInt(ReqQty) == 0) {
      this.snackbarService.openSnackbar("Please enter valid Request Qty", ResponseMessageTypes.WARNING);
    }
  }

  addKitItem() {
    let condition;
    this.getKitFormArray.controls.forEach(el => {
      if (el?.get('stockCode')?.value === this.kitConfigurationAddEditForm.value.stockCode?.displayName ||
        el?.get('stockDescription')?.value === this.kitConfigurationAddEditForm.value.stockDescription?.displayName) {
        condition = true;
      }
    });
    if ((!this.kitConfigurationAddEditForm.value.stockCode || !this.kitConfigurationAddEditForm.value.stockDescription ||
      !this.kitConfigurationAddEditForm.value.comments || !this.kitConfigurationAddEditForm.value.quantity)
      || !this.kitConfigurationAddEditForm.value.stockCode?.id) {
      this.setFormValidators();
      this.kitConfigurationAddEditForm.get('stockCode').markAsTouched();
      this.kitConfigurationAddEditForm.get('stockDescription').markAsTouched();
      this.kitConfigurationAddEditForm.get('comments').markAsTouched();
      this.kitConfigurationAddEditForm.get('quantity').markAsTouched();
      return;
    } else if (condition) {
      this.snackbarService.openSnackbar("Kit item already exists", ResponseMessageTypes.WARNING);
      return;
    } else if (this.validationKitIdStockCode() || this.validationKitIdStockCodeError()) {
      this.snackbarService.openSnackbar("Kid Id and Stock Code should not be the same", ResponseMessageTypes.WARNING);
      return;
    } else if (this.kitConfigurationAddEditForm.get('quantity')?.value == 0) {
      this.snackbarService.openSnackbar("Please enter valid Request Qty", ResponseMessageTypes.WARNING);
      return;
    }
    const addObj = {
      stockCode: this.kitConfigurationAddEditForm.value.stockCode?.displayName,
      stockDescription: this.kitConfigurationAddEditForm.value.stockDescription?.displayName,
      comments: this.kitConfigurationAddEditForm.value.comments,
      quantity: this.kitConfigurationAddEditForm.value.quantity,
    }
    const itemId = this.kitConfigurationAddEditForm.value.stockCode?.id ? this.kitConfigurationAddEditForm.value.stockCode?.id : this.kitConfigurationAddEditForm.value.stockDescription?.id;
    this.itemIdList.push(itemId);
    this.initFormArray(addObj);
    this.kitConfigurationAddEditForm.get('stockCode').markAsUntouched();
    this.kitConfigurationAddEditForm.get('stockDescription').markAsUntouched();
    this.kitConfigurationAddEditForm.get('comments').markAsUntouched();
    this.kitConfigurationAddEditForm.get('quantity').markAsUntouched();
  }

  removeKitItem(i) {
    const message = `Are you sure you want to delete this?`;
    const dialogData = new ConfirmDialogModel("Confirm Action", message);
    const dialogRef = this.dialog.open(ConfirmDialogPopupComponent, {
      maxWidth: "400px",
      data: dialogData,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (!dialogResult) return;
      if (this.kitConfigDetail?.kitConfigItemDetail[i]?.kitConfigItemId && this.getKitFormArray.length > 1) {
        this.crudService.deleteByParams(ModulesBasedApiSuffix.INVENTORY, this?.primengTableConfigProperties?.tableComponentConfigs?.tabsList[this.selectedIndex].deleteAPI, { body: { ids: this.kitConfigDetail?.kitConfigItemDetail[i]?.kitConfigItemId, isDeleted: true, modifiedUserId: this.userData?.userId } })
          .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response?.statusCode == 200) {
              this.onAfterRemoveKitItem(i);
            }
            this.isSubmitted = false;
            this.rxjsService.setGlobalLoaderProperty(false);
          })
      } else if (this.getKitFormArray.length == 1) {
        this.snackbarService.openSnackbar('Atleast one required', ResponseMessageTypes.WARNING);
        return;
      } else {
        this.onAfterRemoveKitItem(i);
      }
    });
  }

  onAfterRemoveKitItem(i) {
    this.itemIdList.splice(i, 1);
    this.kitConfigDetail?.kitConfigItemDetail.splice(i, 1);
    this.getKitFormArray.removeAt(i);
  }

  //Clear from arry
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  };

  getValidForm() {
    let condition = [];
    Object.keys(this.kitConfigurationAddEditForm.controls).forEach(key => {
      if (key != "stockCode" && key != "stockDescription" && key != "comments" && key != "quantity" && !this.kitConfigId) {
        condition.push(this.kitConfigurationAddEditForm.controls[key].valid);
      } else if (key != "warehouse" && key != "stockCode" && key != "stockDescription" && key != "comments" && key != "quantity" && key != "kitId" && key != "kitName" && this.kitConfigId) {
        condition.push(this.kitConfigurationAddEditForm.controls[key].valid);
      }
    });
    return condition.every(el => el == true);
  }

  onSubmit() {
    this.kitConfigurationAddEditForm.markAsUntouched();
    if (!this.getValidForm()) {
      this.kitConfigurationAddEditForm.markAllAsTouched();
      return;
    }
    if (this.getKitFormArray.value.length == 0) {
      this.snackbarService.openSnackbar('Please Enter Stock Details', ResponseMessageTypes.WARNING);
      return;
    }
    this.onAfterSubmit();
  }

  onAfterSubmit() {
    const revertDate = this.kitConfigurationAddEditForm.value?.revertDate ?
      this.momentService.toMoment(this.kitConfigurationAddEditForm.value?.revertDate).format('YYYY-MM-DD hh:mm:ss.ms') : '';
    const kitConfigObj = {
      kitTypeId: this.kitConfigurationAddEditForm.value?.kitType?.id,
      ownershipTypeId: this.kitConfigurationAddEditForm.value?.ownership?.id,
      kitItemId: this.kitConfigurationAddEditForm.value?.kitName?.itemId,
      warehouseId: this.kitConfigurationAddEditForm.value?.warehouse ? this.kitConfigurationAddEditForm.value?.warehouse : this.kitConfigurationAddEditForm.controls?.warehouse.value,
      kitTypeStatus: this.kitConfigurationAddEditForm.value?.kitTypeStatus?.id,
      revertDate: revertDate,
      createdUserId: this.userData?.userId,
    }
    if (this.kitConfigurationAddEditForm.value?.kitType?.displayName?.toLowerCase() == "off the shelf") {
      delete kitConfigObj.kitTypeStatus;
      delete kitConfigObj.revertDate;
    }
    if (this.kitConfigId) {
      delete kitConfigObj.createdUserId;
      kitConfigObj['modifieduserId'] = this.userData?.userId;
      kitConfigObj['kitConfigId'] = this.kitConfigId;
      kitConfigObj['IsActive'] = this.kitConfigurationAddEditForm.value.status?.id;
    }
    kitConfigObj['kitConfigItemDTO'] = [];
    this.getKitFormArray.value.forEach((el, i) => {
      kitConfigObj['kitConfigItemDTO'][i] = {
        kitCommentTypeId: el.comments?.id,
        itemId: this.itemIdList[i],
        requestedQty: el.quantity,
        isActive: true,
      }
      if (this.kitConfigId) {
        kitConfigObj['kitConfigItemDTO'][i]['kitConfigItemId'] = this.kitConfigDetail?.kitConfigItemDetail[i]?.kitConfigItemId ? this.kitConfigDetail?.kitConfigItemDetail[i]?.kitConfigItemId : null;
        kitConfigObj['kitConfigItemDTO'][i]['kitConfigId'] = this.kitConfigId;
      }
    });
    let api = this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.KIT_CONFIGURATION_LIST, kitConfigObj);
    if (this.kitConfigId) {
      api = this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.KIT_CONFIGURATION_LIST, kitConfigObj)
    }
    this.isSubmitted = true;
    api.subscribe((response: IApplicationResponse) => {
      if (response?.isSuccess && response?.statusCode == 200) {
        this.router.navigate(['/inventory/kit-config']);
      }
      this.isSubmitted = false;
      this.rxjsService.setGlobalLoaderProperty(false);
    })
  }

  ngOnDestroy() {
    if (this.dropdownSubscription) {
      this.dropdownSubscription.unsubscribe();
    }
    if (this.eventSubscription) {
      this.eventSubscription?.unsubscribe();
    }
  }
}

