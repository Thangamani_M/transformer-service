import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { KitConfigRoutingModule, KitConfigurationAddEditComponent, KitConfigurationListComponent } from '@modules/inventory/components/kit-config';
import { OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';


@NgModule({
  declarations: [KitConfigurationListComponent, KitConfigurationAddEditComponent],
  imports: [
    CommonModule,
    KitConfigRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule
  ],
  providers: [
    { provide: OWL_DATE_TIME_LOCALE, useValue: 'en-GB' },]
})
export class KitConfigModule { }