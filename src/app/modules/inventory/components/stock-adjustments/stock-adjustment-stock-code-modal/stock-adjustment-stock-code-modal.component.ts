import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-stock-adjustment-stock-code-modal',
  templateUrl: './stock-adjustment-stock-code-modal.component.html'
})
export class StockAdjustmentStockCodeModalComponent implements OnInit {
  @Output() outputData = new EventEmitter<any>();
  constructor(@Inject(MAT_DIALOG_DATA) public popupData: any, public dialogRef: MatDialogRef<StockAdjustmentStockCodeModalComponent>) { }

  ngOnInit(): void {
  }
}
