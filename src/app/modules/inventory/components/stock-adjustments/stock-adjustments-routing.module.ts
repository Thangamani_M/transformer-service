import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockAdjustmentsAddEditComponent } from './stock-adjustments-add-edit/stock-adjustments-add-edit.component';
import { StockAdjustmentsListComponent } from './stock-adjustments-list/stock-adjustments-list.component';
import { StockAdjustmentsViewComponent } from './stock-adjustments-view/stock-adjustments-view.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: StockAdjustmentsListComponent, data: { title: 'Stock Adjustment List' },canActivate:[AuthGuard] },
  { path: 'view', component: StockAdjustmentsViewComponent, data: { title: 'Stock Adjustment View' },canActivate:[AuthGuard] },
  { path: 'add-edit', component: StockAdjustmentsAddEditComponent, data: { title: 'Stock Adjustment Add Edit' },canActivate:[AuthGuard] },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})
export class StockAdjustmentsRoutingModule { }
