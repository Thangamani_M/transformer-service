import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareDynamicTableTabsFromPermissions, RxjsService ,currentComponentPageBasedPermissionsSelector$, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService} from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { StockAdjustmentStockCodeModalComponent } from '../stock-adjustment-stock-code-modal/stock-adjustment-stock-code-modal.component';
@Component({
  selector: 'app-stock-adjustments-view',
  templateUrl: './stock-adjustments-view.component.html'
})
export class StockAdjustmentsViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  stockAdjustmentId: any = '';
  stockAdjustmentDetail: any = {};
  userData: any;
  StockAdjustmentsviewDetail:any;
  selectedIndex = 0;
  primengTableConfigProperties: any
  totalValue = 0;
  constructor(private snackbarService:SnackbarService,private datePipe: DatePipe,private crudService: CrudService,private activatedRoute: ActivatedRoute,private router: Router,private store: Store<AppState>, private dialog: MatDialog,private rxjsService: RxjsService) {
  super()
    this.stockAdjustmentId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    })
    this.primengTableConfigProperties = {
      tableCaption:'View Stock Adjustment Request',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Stock Adjustment', relativeRouterUrl: '' }, { displayName: 'Stock Adjustment Request List', relativeRouterUrl: '/inventory/stock-adjustment' }, { displayName: 'View Stock Adjustment Request', relativeRouterUrl: '', }],
      tableComponentConfigs: {
          tabsList: [
              {
                  enableAction: true,
                  enableEditActionBtn: true,
                  enableBreadCrumb: true,
                  enableExportBtn: false,
                  enablePrintBtn: false,
                  printTitle: 'Daily Staging Bay Report',
                  printSection: 'print-section',
              }
          ]
      }
  }
  this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  }
 
  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getStockAdjustmentDetails().subscribe((response: IApplicationResponse) => {
      this.stockAdjustmentDetail = response.resources;
      this.onShowValue(response.resources);
      this.stockAdjustmentDetail.items.forEach((val) => {
        this.totalValue += val.value;
      });
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.STOCK_ADJUSTMENT]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onShowValue(response?: any) {
    this.StockAdjustmentsviewDetail = [
          { name: 'Adjustment ID', value: response?.stockAdjustmentNumber},
          { name: 'Created By', value: response?.createdBy},
          { name: 'Warehouse', value: response?.warehouse},
          { name: 'Reference ID', value: response?.referenceId},
          { name: 'Created Date', value:   response?.createdDate ? this.datePipe.transform( response.createdDate, 'yyyy-MM-dd HH:mm:ss') : "-" },
          { name: 'Storage Location', value: response?.storageLocation},
          { name: 'Status', value: response.status, statusClass: response.cssClass },
    ]
    
  }

  getStockAdjustmentDetails(): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_ADJUSTMENT, this.stockAdjustmentId, false);
  }

  navigateToEdit() {
    this.router.navigate(['inventory/stock-adjustment/add-edit'], { queryParams: { id: this.stockAdjustmentId }, skipLocationChange: true });
  }

  stockCodeClick(item) {
    const dialogReff = this.dialog.open(StockAdjustmentStockCodeModalComponent, {
      width: '600px',
      data: {
        stockDetails: item
      }, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (!result) return;
    });
  }
  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
         }
        this.navigateToEdit()
        break;
    }
  }

  downloadFile(item: any) {
    window.location.href = item.documentPath;
  }
}
