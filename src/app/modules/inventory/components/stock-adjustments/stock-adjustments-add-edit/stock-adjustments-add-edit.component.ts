import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { StockAdjustmentModel } from '@modules/inventory/models/stock-adjustment.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { StockAdjustmentStockCodeModalComponent } from '../stock-adjustment-stock-code-modal/stock-adjustment-stock-code-modal.component';
@Component({
  selector: 'app-stock-adjustments-add-edit',
  templateUrl: './stock-adjustments-add-edit.component.html',
  styleUrls: ['./stock-adjustments-add-edit.scss']
})
export class StockAdjustmentsAddEditComponent implements OnInit {
  stockAdjustmentId: any = '';
  stockAdjustmentDetail: any = {};
  userData: any;
  documentTypeList: any = [];
  actionList = [];
  stockAdjustmentForm: FormGroup;
  documenType: any = '';
  fileList = new Array();
  selectedIndex = 0;
  totalValue = 0;
  selectedFiles = new Array();
  totalFileSize = 0;
  maxFileSize = 104857600;
  actionId;
  constructor(private crudService: CrudService, private snackbarService: SnackbarService,
    private activatedRoute: ActivatedRoute, private dialog: MatDialog,
    private router: Router,
    private store: Store<AppState>,
    private rxjsService: RxjsService, private formBuilder: FormBuilder) {
    this.stockAdjustmentId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      this.userData = userData;
    })
  }

  ngOnInit(): void {
    this.createStockCodeForm();

    this.getDocumentTypeDropdown().subscribe((response: IApplicationResponse) => {
      this.documentTypeList = response.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    });

    this.getActionDropdown().subscribe((response: IApplicationResponse) => {
      this.actionList = response.resources;
      this.rxjsService.setGlobalLoaderProperty(false);
    });

    this.getStockAdjustmentDetails().subscribe((response: IApplicationResponse) => {
      let auditCycleDetail = Object.assign({}, response.resources);
      this.stockAdjustmentDetail = response.resources;
      this.stockAdjustmentDetail.items.forEach((val) => {
        this.totalValue += val.value;
      });

      if (this.stockAdjustmentDetail.doaApprovals.find(x => x.responsibleRole == this.userData.roleName) != undefined) {
        this.stockAdjustmentForm.get('Action').patchValue(this.stockAdjustmentDetail.doaApprovals.find(x => x.responsibleRole == this.userData.roleName).stockAdjustmentActionTypeId)
        this.stockAdjustmentForm.get('reason').patchValue(this.stockAdjustmentDetail.doaApprovals.find(x => x.responsibleRole == this.userData.roleName).reason)
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });


  }

  stockCodeClick(item) {
    const dialogReff = this.dialog.open(StockAdjustmentStockCodeModalComponent, {
      width: '600px',
      data: {
        stockDetails: item
      }, disableClose: true
    });
    dialogReff.afterClosed().subscribe(result => {
      if (!result) return;
    });
  }

  downloadFile(item: any) {
    window.location.href = item.documentPath;
  }

  onTabChange(e) {
    this.selectedIndex = e.index;
  }

  createStockCodeForm(): void {
    let stockAdjustmentAddEditModel = new StockAdjustmentModel();
    // create form controls dynamically from model class
    this.stockAdjustmentForm = this.formBuilder.group({});
    Object.keys(stockAdjustmentAddEditModel).forEach((key) => {
      this.stockAdjustmentForm.addControl(key, new FormControl(stockAdjustmentAddEditModel[key]));
    });
  }

  selectedAttachment: any = '';

  uploadFiles(event) {

    this.selectedFiles = [];
    const supportedExtensions = ['jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'];
    for (var i = 0; i < event.target.files.length; i++) {
      let selectedFile = event.target.files[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension)) {
        this.fileList.push(event.target.files[i]);
        this.totalFileSize += event.target.files[i].size;
        this.selectedAttachment = event.target.files[i].name;
        let temp = {};
        temp['FileName'] = event.target.files[i].name;
        temp['StockAdjustmentAttachmentTypeId'] = this.stockAdjustmentForm.value.documentId;
        this.selectedFiles.push(temp);
      }
      else {
        this.snackbarService.openSnackbar("Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx", ResponseMessageTypes.WARNING);
      }
    }
  }

  submitClick() {
    if (this.selectedIndex == 0) {
      if (this.stockAdjustmentForm.value.documentId == '') {
        this.snackbarService.openSnackbar('Select Document Type ', ResponseMessageTypes.WARNING);
      }
      else if (this.fileList.length == 0) {
        this.snackbarService.openSnackbar('Select at least one file', ResponseMessageTypes.WARNING);
      }
      else {
        let formData = new FormData();
        let body = {
          StockAdjustmentId: this.stockAdjustmentId,
          CreatedUserId: this.userData.userId,
          DocumentDetails: this.selectedFiles
        }
        formData.append('file', JSON.stringify(body));
        for (const file of this.fileList) {
          formData.append('File', file);
        }
        this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.STOCK_ADJUSTMENT_DOCUMENTS, formData)
          .subscribe((response: IApplicationResponse) => {
            this.rxjsService.setGlobalLoaderProperty(false);
            this.router.navigate(['inventory/stock-adjustment']);
          });
      }
    }
    else {
      if (this.stockAdjustmentDetail.doaApprovals.find(x => x.responsibleRole == this.userData.roleName) != undefined) {
        let doaApprovalBody = {
          "StockAdjustmentId": this.stockAdjustmentId,
          "StockAdjustmentApprovalId": this.stockAdjustmentDetail.doaApprovals.find(x => x.responsibleRole == this.userData.roleName).stockAdjustmentApprovalId,
          // "StockAdjustmentActionTypeId": this.stockAdjustmentForm.value.Action,
          "StockAdjustmentActionTypeId": this.actionId,
          "Reason": this.stockAdjustmentForm.value.reason,
          "UserId": this.userData.userId
        }
        this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_ADJUSTMENT_APPROVAL, doaApprovalBody)
          .subscribe((response: IApplicationResponse) => {
            this.rxjsService.setGlobalLoaderProperty(false);
            this.router.navigate(['inventory/stock-adjustment']);
          });
      }
    }
  }
  change(event){
   this.actionId = event.target.value;
  }
  getStockAdjustmentDetails(): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_ADJUSTMENT, this.stockAdjustmentId, false);
  }

  getDocumentTypeDropdown() {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STOCK_ADJUSTMENT_DOCUMENT_TYPE, undefined, false);
  }

  getActionDropdown() {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STOCK_ADJUSTMENT_ACTION_TYPE, undefined, false);
  }
}
