import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { StockAdjustmentStockCodeModalComponent } from './stock-adjustment-stock-code-modal/stock-adjustment-stock-code-modal.component';
import { StockAdjustmentsAddEditComponent } from './stock-adjustments-add-edit/stock-adjustments-add-edit.component';
import { StockAdjustmentsListComponent } from './stock-adjustments-list/stock-adjustments-list.component';
import { StockAdjustmentsRoutingModule } from './stock-adjustments-routing.module';
import { StockAdjustmentsViewComponent } from './stock-adjustments-view/stock-adjustments-view.component';

@NgModule({
  declarations: [StockAdjustmentsListComponent, StockAdjustmentsViewComponent, StockAdjustmentsAddEditComponent, StockAdjustmentStockCodeModalComponent],
  imports: [
    CommonModule,
    StockAdjustmentsRoutingModule,
    ReactiveFormsModule,LayoutModule,
    FormsModule,
    MaterialModule,
    SharedModule
  ],entryComponents:[StockAdjustmentStockCodeModalComponent]
})
export class StockAdjustmentsModule { }
