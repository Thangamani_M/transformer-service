import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    PickingDashboardOrdersComponent, PickingDashboardSerialNumberPoupComponent, PickingDashboardViewComponent,
    StockOrderQueryAddEditComponent, StockOrderQueryResubmitComponent, StockOrderQueryViewComponent
} from '@inventory/components/picking-dashboard-orders';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
import { PickingDashboardOrdersComponentNew } from './picking-dashboard-orders-new.component';
import { StockOrderViewModalComponent } from './picking-stock-order-query/stock-order-view-modal.component';

const routes: Routes = [
    { path: 'new', component: PickingDashboardOrdersComponent, data: { title: 'Picking Dashboard Orders' },canActivate:[AuthGuard] },
    { path: '', component: PickingDashboardOrdersComponentNew, data: { title: 'Picking Dashboard Orders' },canActivate:[AuthGuard] },
    { path: 'picking-dashboard-view', component: PickingDashboardViewComponent, data: { title: 'Picking Dashboard View' },canActivate:[AuthGuard] },
    { path: 'stock-order-add-edit', component: StockOrderQueryAddEditComponent, data: { title: 'Stock Order Query Add Edit' },canActivate:[AuthGuard] },
    { path: 'stock-order-view', component: StockOrderQueryViewComponent, data: { title: 'Stock Order Query View' },canActivate:[AuthGuard] },
    { path: 'stock-order-resubmit', component: StockOrderQueryResubmitComponent, data: { title: 'Stock Order Query' },canActivate:[AuthGuard] },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    
})

export class PickingDashboardOrdersRoutingModule { }

export const declarationComponents = [
    PickingDashboardOrdersComponent,
    PickingDashboardOrdersComponentNew,
    PickingDashboardViewComponent,
    StockOrderQueryAddEditComponent,
    StockOrderQueryViewComponent,
    StockOrderQueryResubmitComponent,
    StockOrderViewModalComponent,
    PickingDashboardSerialNumberPoupComponent,
  ];