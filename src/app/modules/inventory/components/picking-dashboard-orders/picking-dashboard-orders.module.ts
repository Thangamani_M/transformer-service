import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import {
 PickingDashboardOrdersRoutingModule, PickingDashboardSerialNumberPoupComponent,
    declarationComponents,

} from '@inventory/components/picking-dashboard-orders';
import { NgxPrintModule } from 'ngx-print';
import { StockOrderViewModalComponent } from './picking-stock-order-query/stock-order-view-modal.component';

@NgModule({
  declarations: [
    ...declarationComponents
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPrintModule,
    LayoutModule,
    PickingDashboardOrdersRoutingModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents: [
    StockOrderViewModalComponent,
    PickingDashboardSerialNumberPoupComponent
  ]
})
export class PickingDashboardOrdersModule { }