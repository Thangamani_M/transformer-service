import { DatePipe } from "@angular/common";
import { HttpParams } from "@angular/common/http";
import { Component } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, MomentService, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, removeFormControlError, ResponseMessageTypes, RxjsService, SnackbarService, TableFilterFormService } from "@app/shared";
import { PrimengDeleteConfirmDialogComponent } from "@app/shared/components/primeng-delete-confirm-dialog/primeng-delete-confirm-dialog.component";
import { InterBranchOrderListFilter } from "@modules/inventory/models";
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from "@modules/inventory/shared";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";

import { select, Store } from '@ngrx/store';
import { validateEvents } from "angular-calendar/modules/common/util";
import { DialogService } from "primeng/api";
import { combineLatest, forkJoin, Observable } from "rxjs";
import { map } from "rxjs/operators";
import {PrimeNgTableVariablesModel} from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-picking-dashboard-orders-new',
  templateUrl: './picking-dashboard-orders-new.component.html',
  styleUrls: ['./picking-dashboard-orders-new.component.scss']
})

export class PickingDashboardOrdersComponentNew extends PrimeNgTableVariablesModel {
  userData: UserLogin;
  loggedInUserData;
  primengTableConfigProperties: any;
  status: any = [];
  searchKeyword: FormControl;
  searchForm: FormGroup
  todayDate: any = new Date();
  minDate = new Date('1984');
  startTodayDate = 0;
  pickingDashboardFilterForm: FormGroup;
  pickingOrdersFilterForm: FormGroup;
  pickedOrdersFilterForm: FormGroup;
  scheduledOrdersFilterForm: FormGroup;
  technicianOrdersFilterForm: FormGroup;
  interBranchtransferFilterForm: FormGroup;
  acceptedOrdersFilterForm: FormGroup;
  showPickingDashboardFilterForm = false;
  showPickingOrdersFilterForm = false;
  showPickedOrdersFilterForm = false;
  showScheduledOrdersFilterForm = false;
  showTechnicianOrdersFilterForm = false;
  showInterBranchFilterForm = false;
  showAcceptedOrdersFilterForm = false;
  warehouseList: any = [];
  stockOrderList: any = [];
  requestWarehouseList: any = [];
  issueWarehouseList: any = [];
  stockCodeList: any = [];
  stockOrderNumberList: any = [];
  dispatchTRFNumberList: any = [];
  stockTRFNumber: any = [];
  stockTRFNumberList: any = [];
  salesRepList: any = [];
  stockOrderTypeList: any = [];
  priorityList: any = [];
  statusList: any = [];
  divisionList: any = [];
  issueingTechLocation: any = [];
  requestingLocationList: any = [];
  pickerDrordown: any = [];
  requestingLocationNameList: any = [];
  showPickedOrdersPopup = false;
  selectedriIndex: any;
  isPickedCancelOrdersDialog = false;
  cancelOrderDialogForm: FormGroup;
  showSaveCancelOrder = false;
  isCancelOrderSuccessDialog = false;
  grvNumer: any;
  packerJobId: any;
  stockOrderNumber: any;
  pageSize: number = 10;
  popupHeader: string = '';
  popupMessage: any;
  showSaveAssignOrder = false;
  pickersList: any = [];
  cancelPickedOrder = false;
  assignPickingorders = false;
  row: any = {};
  showPickerName: string = '';
  otherParams;
  filterParams;
  totalRecords = null;
  listSubscribtion;
  reset=false;
  editPermissions: any = {
    "Picking Dashboard": { edit: false, view: false },
    "Picking Orders": { edit: false, view: false },
    "Picked Orders": { edit: false, view: false },
    "Scheduled Customer Orders": { edit: false, view: false },
    "Technician Orders": { edit: false, view: false },
    "Inter Branch Orders": { edit: false, view: false },
    "Accepted Customer Orders": { edit: false, view: false },
  }
  columnFilterForm: FormGroup;
  searchColumns: any
  pickingOptions= [{type:'custom-view',label:'View'},{type:'custom-assign',label:'Assign'}];
  pickedOptions= [{type:'custom-view',label:'View'},{type:'custom-cancel',label:'Cancel'}];
  constructor(private crudService: CrudService,
    private activatedRoute: ActivatedRoute, private datePipe: DatePipe,
    public dialogService: DialogService, private snackbarService: SnackbarService, private router: Router,
    private store: Store<AppState>, private momentService: MomentService, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private _fb: FormBuilder, private tableFilterFormService: TableFilterFormService,
  ) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Picking Dashboard",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Picking Dashboard', relativeRouterUrl: '' }, { displayName: 'Picking Dashboard' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Picking Dashboard',
            dataKey: 'stackConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Picking Dashboard',
            printSection: 'print-section0',
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            disabled: false,
            columns: [{ field: 'issuingWarehouse', header: 'Issuing Warehouse', width: '200px' },
            { field: 'allPackingOrders', header: 'All Picking Orders', width: '200px' },
            { field: 'scheduledCustomerOrders', header: 'Scheduled Customer Orders', width: '200px' },
            { field: 'techinicainOrders', header: 'Technician Orders', width: '150px' },
            { field: 'interBranchOrdes', header: 'Inter Branch Orders', width: '200px' },
            { field: 'acceptedCustomerOrders', header: 'Accepted Customer Orders', width: '200px' }],
            apiSuffixModel: InventoryModuleApiSuffixModels.PICKING_DASHBOARD,
            exportApiSuffixModel: InventoryModuleApiSuffixModels.PICKING_DASHBOARD_EXPORT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            shouldShowFilterActionBtn: true,
          },
          {
            caption: 'Picking Orders',
            dataKey: 'stackAreaConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Picking Orders',
            printSection: 'print-section1',
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            enableHyperLinkView: true,
            enableHyperLinkAssign: true,
            enableHyperLinkCancel: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            disabled: false,
            columns: [{ field: 'stockOrderNumber', header: 'Stock Order No', width: '150px' ,enableCustomLink:true,options:this.pickingOptions},
            { field: 'status', header: 'Status', width: '180px' },
            { field: 'stockOrderType', header: 'Stock Order Type', width: '100px' },
            { field: 'orderSubmittedDate', header: 'Order Submitted Date & Time', isDateTime: true, width: '200px' },
            { field: 'priority', header: 'Priority', width: '100px' },
            { field: 'division', header: 'Division', width: '100px' },
            { field: 'issuingWarehouse', header: 'Issuing Warehouse', width: '200px' },
            { field: 'issuingTechLocation', header: 'Issuing Tech Location', width: '150px' },
            { field: 'requestingLocation', header: 'Requesting Location', width: '150px' },
            { field: 'requestingLocationName', header: 'Requesting Location Name', width: '150px' },
            { field: 'picker', header: 'Picker Name', width: '200px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.PICKING_ORDERS,
            exportApiSuffixModel: InventoryModuleApiSuffixModels.PICKING_ORDERS_EXPORT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            shouldShowFilterActionBtn: true,
            ebableAddActionBtn: false
          },
          {
            caption: 'Picked Orders',
            dataKey: 'namedStackConfigId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Picked Orders',
            printSection: 'print-section2',
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: false,
            enableHyperLinkView: true,
            enableHyperLinkAssign: true,
            enableHyperLinkCancel: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            disabled: false,
            columns: [{ field: 'stockOrderNumer', header: 'Stock Order No', width: '150px',enableCustomLink:true ,options:this.pickedOptions},
            { field: 'status', header: 'Status', width: '100px' },
            { field: 'stockOrderType', header: 'Stock Order Type', width: '100px' },
            { field: 'orderSubmittedDate', header: 'Order Submitted Date & Time', isDateTime: true, width: '200px' },
            { field: 'priority', header: 'Priority', width: '100px' },
            { field: 'division', header: 'Division', width: '100px' },
            { field: 'issuingWarehouse', header: 'Issuing Warehouse', width: '200px' },
            { field: 'issuingTechLocation', header: 'Issuing Tech Location', width: '150px' },
            { field: 'requestingLocation', header: 'Receiving Location', width: '150px' },
            { field: 'requestingLocationName', header: 'Receiving Location Name', width: '150px' },
            { field: 'picker', header: 'Picker', width: '200px' },
            { field: 'pickingStartDateTime', header: 'Picking Start Date & Time',isDateTime: true,  width: '150px' },
            { field: 'pickingEndDateTime', header: 'Picking End Date & Time', isDateTime: true, width: '200px' },
            { field: 'collectionDateTime', header: 'Collection Date & Time', isDateTime: true, width: '200px' },
            { field: 'pickingBatch', header: 'Picking Batch', width: '200px',enableCustomIcon:true },
            { field: 'dispatchTRFNumber', header: 'Dispatch TRF Number', width: '100px' },
            { field: 'stockTRFNumber', header: 'Stock TRF Number', width: '100px' },

            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.PICKED_ORDERS,
            exportApiSuffixModel: InventoryModuleApiSuffixModels.PICKED_ORDERS_EXPORT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            shouldShowFilterActionBtn: true,
            ebableAddActionBtn: false
          },
          {
            caption: 'Scheduled Customer Orders',
            dataKey: 'namedStackId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Scheduled Customer Orders',
            printSection: 'print-section3',
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            disabled: false,
            columns: [{ field: 'stockOrderNumber', header: 'Stock Order No', width: '100px' },
            { field: 'stockOrderType', header: 'Stock Order Type', width: '100px' },
            { field: 'orderSubmittedDate', header: 'Order Submitted Date & Time', isDateTime: true, width: '200px' },
            { field: 'priority', header: 'Priority', width: '100px' },
            { field: 'division', header: 'Division', width: '100px' },
            { field: 'issuingWarehouse', header: 'Issuing Warehouse', width: '200px' },
            { field: 'stockCode', header: 'Stock Code', width: '100px' },
            { field: 'stockDescription', header: 'Stock Description', width: '200px' },
            { field: 'quantity', header: 'Quantity', width: '150px' },
            { field: 'requestingLocationCode', header: 'Receiving Location', width: '150px' },
            { field: 'requestingLocationName', header: 'Receiving Location Name', width: '150px' },
            { field: 'serviceCallNo', header: 'Service Call No', width: '100px' },
            { field: 'quoteNo', header: 'Quote No', width: '150px' },
            { field: 'defaultBranch', header: 'Default Warehouse', width: '150px' },
            { field: 'callCreationDate', header: 'Call Creation Date & Time', isDateTime: true, width: '200px' },
            { field: 'shceduledDate', header: 'Scheduled Date & Time', isDateTime: true, width: '200px' },
            { field: 'salesRep', header: 'SalesRep', width: '100px' },],
            apiSuffixModel: InventoryModuleApiSuffixModels.SCHEDULED_CUSTOMER_ORDERS,
            exportApiSuffixModel: InventoryModuleApiSuffixModels.SCHEDULED_CUSTOMER_ORDERS_EXPORT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            shouldShowFilterActionBtn: true,
            ebableAddActionBtn: false
          },
          {
            caption: 'Technician Orders',
            dataKey: 'stockOrderId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Technician Orders',
            printSection: 'print-section4',
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            disabled: false,
            columns: [{ field: 'stockOrderNumber', header: 'Stock Order No', width: '100px' },
            // { field: 'status', header: 'Status', width: '200px' },
            { field: 'stockOrderType', header: 'Stock Order Type', width: '100px' },
            { field: 'orderSubmittedDate', header: 'Order Submitted Date & Time', isDateTime: true, width: '200px' },
            { field: 'priority', header: 'Priority', width: '100px' },
            { field: 'division', header: 'Division', width: '100px' },
            { field: 'issuingWarehouse', header: 'Issuing Warehouse', width: '200px' },
            { field: 'stockCode', header: 'Stock Code', width: '150px' },
            { field: 'stockDescription', header: 'Stock Description', width: '200px' },
            { field: 'quantity', header: 'Quantity', width: '100px' },
            { field: 'requestingLocationCode', header: 'Receiving Location', width: '150px' },
            { field: 'requestingLocationName', header: 'Receiving Location Name', width: '150px' },
            { field: 'issuingWarehouse', header: 'Default Warehouse', width: '150px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.DASHBOARD_TECHNICIAN_ORDERS,
            exportApiSuffixModel: InventoryModuleApiSuffixModels.DASHBOARD_TECHNICIAN_ORDERS_EXPORT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            shouldShowFilterActionBtn: true,
            ebableAddActionBtn: false
          },
          {
            caption: 'Inter Branch Orders',
            dataKey: 'interBranchStockOrderId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Inter Branch Orders',
            printSection: 'print-section5',
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            disabled: false,
            columns: [{ field: 'stockOrderNumber', header: 'Stock Order No', width: '100px' },
            // { field: 'status', header: 'Status', width: '200px' },
            { field: 'stockOrderType', header: 'Stock Order Type', width: '100px' },
            { field: 'orderSubmittedDate', header: 'Order Submitted Date & Time', isDateTime: true, width: '200px' },
            { field: 'priority', header: 'Priority', width: '100px' },
            { field: 'division', header: 'Division', width: '100px' },
            { field: 'issuingWarehouse', header: 'Issuing Warehouse', width: '200px' },
            { field: 'stockCode', header: 'Stock Code', width: '100px' },
            { field: 'stockDescription', header: 'Stock Description', width: '200px' },
            { field: 'quantity', header: 'Quantity', width: '100px' },
            { field: 'requestingLocationCode', header: 'Requesting Location', width: '150px' },
            { field: 'requestingLocationName', header: 'Requesting Location Name', width: '150px' },
              // { field: 'defaultBranch', header: 'Default Branch', width: '200px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.DASHBOARD_STOCK_ORDER,
            exportApiSuffixModel: InventoryModuleApiSuffixModels.DASHBOARD_STOCK_ORDER_EXPORT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            shouldShowFilterActionBtn: true,
          },
          {
            caption: 'Accepted Customer Orders',
            dataKey: 'stockOrderId',
            enableBreadCrumb: true,
            enableAction: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Accepted Customer Orders',
            printSection: 'print-section6',
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            disabled: false,
            columns: [{ field: 'stockOrderNumber', header: 'Stock Order No', width: '100px' },
            { field: 'stockOrderType', header: 'Stock Order Type', width: '100px' },
            { field: 'priority', header: 'Priority', width: '100px' },
            { field: 'division', header: 'Division', width: '100px' },
            { field: 'issueingWarehouse', header: 'Issuing Warehouse', width: '200px' },
            { field: 'stockCode', header: 'Stock Code', width: '100px' },
            { field: 'stockDescription', header: 'Stock Description', width: '200px' },
            { field: 'quantity', header: 'Quantity', width: '100px' },
            { field: 'serviceCallNo', header: 'Service Call No', width: '100px' },
            { field: 'quoteNo', header: 'Quote No', width: '100px' },
            { field: 'defaultBranch', header: 'Default Warehouse', width: '200px' },
            { field: 'callCreationDate', header: 'Call Creation Date & Time', isDateTime: true, width: '200px' },
            { field: 'shceduledDate', header: 'Scheduled Date & Time', isDateTime: true, width: '200px' },
            { field: 'salesRep', header: 'SalesRep', width: '100px' }],
            apiSuffixModel: InventoryModuleApiSuffixModels.DASHBOARD_ACCEPTED_CUSTOMER,
            exportApiSuffixModel: InventoryModuleApiSuffixModels.DASHBOARD_ACCEPTED_CUSTOMER_EXPORT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            shouldShowFilterActionBtn: true,
            ebableAddActionBtn: false
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    this.getRequiredListData();
    this.pickingDashboardsFilterFunction();
    this.getStockOrderNumberDropdown(); 
    this.getDivivsionDropwn();
    this.getStockCodeDropdown(); 
    this.getRequestingLocationDrodown();
    this.getPriorityDrodown();

    // this.getStockOrderTypeDropdown();
    // this.getPriorityDrodown(); 
    // this.getStatusDropdown(); 
    // this.getDivivsionDropwn();
    // this.getIssueingTechLocationDropdown(); 
    // this.getRequestingLocationDrodown();
    //this.getPickersDropdown();
    this.getAllFilterDropdown();
   // this.getWarehouseDropdown();
    this.cancelForm();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.PICKING_DASHBOARD];

      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
    /* Tab filter start */
    getWarehouseDropdown() {
      this.warehouseList = [];
      this.requestWarehouseList = [];
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId }))
        .subscribe((response: IApplicationResponse) => {
          if (response.resources && response.statusCode === 200) {
            let warehouseList = response.resources;
            for (var i = 0; i < warehouseList.length; i++) {
              let tmp = {};
              tmp['value'] = warehouseList[i].id;
              tmp['display'] = warehouseList[i].displayName;
              this.warehouseList.push(tmp);
              this.requestWarehouseList.push(tmp);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  
  cancelForm(){
    this.cancelOrderDialogForm = this.formBuilder.group({
      reasons: [''],
      packerId: [''],
      isTechnicianCollection: [false]
    });
  }
  getAllFilterDropdown(){
    let params = new HttpParams().set('tabType', this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    let params1; let params2; 
    if(this.loggedInUserData.warehouseId){
       params1 = new HttpParams().set('userId', this.loggedInUserData.userId).set('warehouseId', this.loggedInUserData.warehouseId);
       params2 = new HttpParams().set('userId', this.loggedInUserData.userId).set('warehouseId', this.loggedInUserData.warehouseId);
    }else {
       params1 = new HttpParams().set('userId', this.loggedInUserData.userId);
       params2 = new HttpParams().set('userId', this.loggedInUserData.userId);
    }
    let issueTecApi = (this.selectedTabIndex==1 || this.selectedTabIndex==0) ? this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PICKING_ORDER_ISSUE_TECH_LOCATION, undefined, true, params2) : 
       this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PICKED_ORDER_ISSUE_TECH_LOCATION, undefined, true, params2);
    let requestTecApi =  (this.selectedTabIndex==1 || this.selectedTabIndex==0)? this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PICKING_ORDER_REQUESTING_LOCATION, undefined, true, params1) : 
       this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PICKING_ORDER_REQUESTING_LOCATION, undefined, true, params1);

    let dropdownsAndData = [
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_DISPATCH_TRF_NUM),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STOCK_TRF_NUM),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SALESREP),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_TYPES),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PRIORITY),
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_JOB_ORDER_STATUS, undefined, true, params),
      this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, InventoryModuleApiSuffixModels.DIVISIONS),
      issueTecApi,
      requestTecApi,
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PICKEDJOBS_INVENTORYSTAFF),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WAREHOUSE,prepareRequiredHttpParams({ userId: this.loggedInUserData?.userId }))
    ];
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              let dispatchTRFNumberList = resp.resources;
              for (var i = 0; i < dispatchTRFNumberList.length; i++) {
                let tmp = {};
                tmp['value'] = dispatchTRFNumberList[i].id;
                tmp['display'] = dispatchTRFNumberList[i].displayName;
                this.dispatchTRFNumberList.push(tmp);
              }
              break;
            case 1:
              let stockTRFNumberList = resp.resources;
              for (var i = 0; i < stockTRFNumberList.length; i++) {
                let tmp = {};
                tmp['value'] = stockTRFNumberList[i].id;
                tmp['display'] = stockTRFNumberList[i].displayName;
                this.stockTRFNumberList.push(tmp);
              }
              break;
            case 2:
              this.salesRepList=[];
              let salesRepList = resp.resources;
              for (var i = 0; i < salesRepList.length; i++) {
                let tmp = {};
                tmp['value'] = salesRepList[i].id;
                tmp['display'] = salesRepList[i].displayName;
                this.salesRepList.push(tmp);
              }
              break;
            case 3:    
                  this.stockOrderTypeList=[];            
                  let stockOrderTypeList = resp.resources;
                  for (var i = 0; i < stockOrderTypeList.length; i++) {
                    let tmp = {};
                    tmp['value'] = stockOrderTypeList[i].id;
                    tmp['display'] = stockOrderTypeList[i].displayName;
                    this.stockOrderTypeList.push(tmp);
                  }            
               break;
            case 4:
              this.priorityList=[];
              let priorityList = resp.resources;
              for (var i = 0; i < priorityList.length; i++) {
                let tmp = {};
                tmp['value'] = priorityList[i].id;
                tmp['display'] = priorityList[i].displayName;
                this.priorityList.push(tmp);
              }
              break;
            case 5:
                let statusList = resp.resources;
                this.statusList = [];
                for (var i = 0; i < statusList.length; i++) {
                  let tmp = {};
                  tmp['value'] = statusList[i].id;
                  tmp['display'] = statusList[i].displayName;
                  this.statusList.push(tmp);
                }
                break;
             case 6:
              let divisionList = resp.resources;
              this.divisionList=[];
              for (var i = 0; i < divisionList.length; i++) {
                let tmp = {};
                tmp['value'] = divisionList[i].id;
                tmp['display'] = divisionList[i].displayName;
                this.divisionList.push(tmp);
              }
              break;
             case 7:
                let issueingTechLocation = resp.resources;
                this.issueingTechLocation = [];
                for (var i = 0; i < issueingTechLocation.length; i++) {
                  let tmp = {};
                  tmp['value'] = issueingTechLocation[i].id;
                  tmp['display'] = issueingTechLocation[i].displayName;
                  this.issueingTechLocation.push(tmp);
                }
                break;
             case 8:
                  let requestingLocationList = resp.resources;
                  for (var i = 0; i < requestingLocationList.length; i++) {
                    let tmp = {};
                    tmp['value'] = requestingLocationList[i].id;
                    tmp['display'] = requestingLocationList[i].displayName;
                    if (requestingLocationList[i].displayName != null &&
                      requestingLocationList[i].displayName != '') {
                      this.requestingLocationList.push(tmp)
                    }
                  }
                  break;
              case 9:
                let pickerDrordown = resp.resources;
                this.pickerDrordown=[];
                for (var i = 0; i < pickerDrordown.length; i++) {
                  let tmp = {};
                  tmp['value'] = pickerDrordown[i].id;
                  tmp['display'] = pickerDrordown[i].displayName;
                  this.pickerDrordown.push(tmp)
                }
                break;
              case 10:
                let warehouseList = resp.resources;
                this.warehouseList=[];
                for (var i = 0; i < warehouseList.length; i++) {
                  let tmp = {};
                  tmp['value'] = warehouseList[i].id;
                  tmp['display'] = warehouseList[i].displayName;
                  this.warehouseList.push(tmp);
                  this.requestWarehouseList.push(tmp);
                }
                break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    otherParams = { ...otherParams, ...this.filterParams, ...{ UserId: this.loggedInUserData.userId } }
    this.otherParams = otherParams;
    let inventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    inventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if (this.listSubscribtion && !this.listSubscribtion.closed) {
      this.listSubscribtion.unsubscribe();
    }
    this.listSubscribtion = this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      inventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          if(this.selectedTabIndex==2 && val.isCancel){ 
            val.customLinkoptions=[{type:'custom-view',label:'View'}];
            //if(val.isvariance){}
            val.customIcon ='fa fa-flag';
            val.flagCssClass = 'flag-red';
            val.showCustomLink = (val.status == 'Variance Escalation') ? true:false ; 
            //val.varianceurl="/inventory/stock-adjustment/view?id=BFF3A312-FDDF-4EEA-A82B-33C086E70535";
          }else if(this.selectedTabIndex==2 && !val.isCancel) {   
            val.customIcon ='fa fa-flag';
            val.flagCssClass = 'flag-red';
            val.showCustomLink = (val.status == 'Variance Escalation') ? true:false ; 
           // val.varianceurl="/inventory/stock-adjustment/view?id=BFF3A312-FDDF-4EEA-A82B-33C086E70535";
            val.customLinkoptions=[{type:'custom-view',label:'View'},{type:'custom-cancel',label:'Cancel'}];
          }         
          return val;
        })
      }
      return res;
    })).subscribe((data: IApplicationResponse) => {
      this.loading = true;
      this.rxjsService.setGlobalLoaderProperty(true);
      if (data.isSuccess == true && data.statusCode == 200) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      } else {
        this.loading = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      this.reset = false;
    });
  }

  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.CREATE:
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.GET:
        let otherParams = {};
        if (unknownVar.stockTRFNumber) {
          otherParams['StockTransferNos'] = unknownVar.stockTRFNumber;
        }
        if (unknownVar.dispatchTRFNumber) {
          otherParams['DispatchTransferNos'] = unknownVar.dispatchTRFNumber;
        }
        otherParams['UserId'] = this.loggedInUserData.userId;
        otherParams = { ...otherParams, ...unknownVar };
        this.first = row["pageIndex"] && row["pageIndex"] ? row["pageIndex"] * row["pageSize"] : 0;
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.getRequiredListData(row["pageIndex"], row["pageSize"], otherParams)
        break;
      case CrudType.FILTER:
        this.openAddEditPage(CrudType.FILTER, row);
        break;
      case CrudType.EDIT:
          this.openAddEditPage(CrudType.VIEW, row);
          break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.FLAG:
          if(row['pickingBatch'] && row['status']  == 'Variance Escalation')
             this.router.navigate(["/inventory/variance/variance-esclation"], {
              queryParams: {
                pickingBatch: row['pickingBatch']
              }, skipLocationChange: true
            });
          break;
      case CrudType.CUSTOM_VIEW:
         this.onCRUDRequested(CrudType.VIEW,row);
         break;
      case CrudType.CUSTOM_ASSIGN:
         this.cancelOrderPopupOpen(row,'picking');
         break;
      case CrudType.CUSTOM_CANCEL:
         this.cancelOrderPopupOpen(row,'picked');
         break;
      case CrudType.DELETE:
        if (!row) {
          if (this.selectedRows.length == 0) {
            this.snackbarService.openSnackbar("Please select atleast one item to delete", ResponseMessageTypes.WARNING);
          } else {
            this.onOneOrManyRowsDelete()
          }
        } else {
          this.onOneOrManyRowsDelete(row)
        }
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.exportList();
        break;
      default:
    }
  }

  gePickersDropdown(warehouseId?:any) {
    let params;
    if(warehouseId)
      params = new HttpParams().set('RoleName', this.loggedInUserData.roleName).set('warehouseid', warehouseId);
    else
      params = new HttpParams().set('RoleName', this.loggedInUserData.roleName).set('warehouseid', this.loggedInUserData.warehouseId);

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PICKER,
      undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.pickersList = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onOneOrManyRowsDelete(rowData?: object) {
    if (rowData) {
      var deletableIds = rowData[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey];
    } else {
      if (this.selectedRows.length > 0) {
        var deletableIds = []
        this.selectedRows.forEach((element: any) => {
          deletableIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
        });
      }
    }
    const ref = this.dialogService.open(PrimengDeleteConfirmDialogComponent, {
      showHeader: false,
      baseZIndex: 10000,
      width: '400px',
      data: {
        deletableIds: deletableIds,
        modifiedUserId: this.loggedInUserData.userId,
        moduleName: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
        apiSuffixModel: this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel
      },
    });
    ref.onClose.subscribe((result) => {
      this.selectedRows = [];
      if (result) {
        this.getRequiredListData()
      }
    });
  }
  assignpackerJob(i) {
    if (this.cancelOrderDialogForm.invalid) {
      return;
    }
    let params = new HttpParams().set('PackerJobId', this.packerJobId)
      .set('CreatedUserId', this.loggedInUserData.userId);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PACKERJOBS_REASSIGN,
      undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200 && response.resources.issuccess ) {
          // this.changeDetectorRef.detectChanges();
          this.cancelOrderDialogForm.get('reasons').patchValue('');
          this.isPickedCancelOrdersDialog = false;
          this.isCancelOrderSuccessDialog = true;
          this.assignPickingorders = true;
          this.cancelPickedOrder = false;
          // this.changeDetectorRef.detectChanges();
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.resources.responseMsg, ResponseMessageTypes.ERROR);
          this.isPickedCancelOrdersDialog = true;
          this.rxjsService.setGlobalLoaderProperty(false);
          return;
        }
      });
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            break;
          case 1:
            this.router.navigate(["/inventory/picking-dashboard-orders/picking-dashboard-view"], {
              queryParams: {
                id: editableObject['stockOrderId'],
                type: editableObject['stockOrderType'],
                packerJobId: editableObject['packerJobId'],
                status: editableObject['status'],
                header: 'Picking Orders',
                tab: 1
              }, skipLocationChange: true
            });
            break;
          case 2:
            this.router.navigate(["/inventory/picking-dashboard-orders/picking-dashboard-view"], {
              queryParams: {
                id: editableObject['stockOrderId'],
                type: editableObject['stockOrderType'],
                packerJobId: editableObject['packerJobId'],
                header: 'Picked Orders',
                tab: 2
              }, skipLocationChange: true
            });
            break;
          case 3:
            this.router.navigate(["/inventory/picking-dashboard-orders/picking-dashboard-view"], {
              queryParams: {
                id: editableObject['stockOrderId'],
                type: editableObject['stockOrderType'],
                header: 'Scheduled Customer Orders',
                packerJobId: editableObject['packerJobId'],
                tab: 3
              }, skipLocationChange: true
            });
            break;
          case 4:
            this.router.navigate(["/inventory/picking-dashboard-orders/picking-dashboard-view"], {
              queryParams: {
                id: editableObject['stockOrderId'],
                packerJobId: editableObject['packerJobId'],
                type: editableObject['stockOrderType'],
                header: 'Technician Orders',
                tab: 4
              }, skipLocationChange: true
            });
            break;
          case 5:
            this.router.navigate(["/inventory/picking-dashboard-orders/picking-dashboard-view"], {
              queryParams: {
                id: editableObject['stockOrderId'],
                packerJobId: editableObject['packerJobId'],
                type: editableObject['stockOrderType'],
                header: 'Inter Branch Orders',
                tab: 5
              }, skipLocationChange: true
            });
            break;
          case 6:
            this.router.navigate(["/inventory/picking-dashboard-orders/picking-dashboard-view"], {
              queryParams: {
                id: editableObject['stockOrderId'],
                packerJobId: editableObject['packerJobId'],
                type: editableObject['stockOrderType'],
                header: 'Accepted Customer Orders',
                tab: 6
              }, skipLocationChange: true
            });
            break;
        }
        break;
      case CrudType.FILTER:
   
        switch (this.selectedTabIndex) {
          case 0:
            this.showPickingDashboardFilterForm = !this.showPickingDashboardFilterForm;
            break;
          case 1:
            this.showPickingOrdersFilterForm = !this.showPickingOrdersFilterForm;
            this.getStockOrderNumberDropdown(); this.getStockOrderTypeDropdown();
            this.getPriorityDrodown(); this.getStatusDropdown(); this.getDivivsionDropwn();
            this.getIssueingTechLocationDropdown(); this.getRequestingLocationDrodown();
            break;
          case 2:
            this.showPickedOrdersFilterForm = !this.showPickedOrdersFilterForm;
            this.getStockOrderNumberDropdown(); this.getStockOrderTypeDropdown();
            this.getPriorityDrodown(); this.getStatusDropdown(); this.getDivivsionDropwn();
            this.getIssueingTechLocationDropdown(); this.getRequestingLocationDrodown();
            this.getPickersDropdown();
            break;
          case 3:
            this.showScheduledOrdersFilterForm = !this.showScheduledOrdersFilterForm;
            this.getStockOrderNumberDropdown(); this.getPriorityDrodown();
            this.getDivivsionDropwn(); this.getStockCodeDropdown();
            this.getRequestingLocationDrodown(); this.getRequestingLocationNameDropdown();
            break;
          case 4:
            this.showTechnicianOrdersFilterForm = !this.showTechnicianOrdersFilterForm;
            this.getStockOrderNumberDropdown(); this.getDivivsionDropwn(); this.getStockCodeDropdown();
            this.getRequestingLocationDrodown(); this.getRequestingLocationNameDropdown();
            break;
          case 5:
            this.showInterBranchFilterForm = !this.showInterBranchFilterForm;
            // this.getStockOrderDropdown();
            this.getDivivsionDropwn();
            this.getStockCodeDropdown(); this.getRequestingLocationDrodown();
            break;
          case 6:
            this.showAcceptedOrdersFilterForm = !this.showAcceptedOrdersFilterForm;
            this.getStockOrderNumberDropdown(); this.getPriorityDrodown();
            this.getDivivsionDropwn(); this.getStockCodeDropdown();
            break;
        }
        break;
     case CrudType.CUSTOM_VIEW:

          break;
     case CrudType.CUSTOM_ASSIGN:
 
          break;
     case CrudType.CUSTOM_CANCEL:

          break;
    }
  }
  getStockOrderTypeDropdown() {
    this.stockOrderTypeList = [];
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_TYPES)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let stockOrderTypeList = response.resources;
          for (var i = 0; i < stockOrderTypeList.length; i++) {
            let tmp = {};
            tmp['value'] = stockOrderTypeList[i].id;
            tmp['display'] = stockOrderTypeList[i].displayName;
            this.stockOrderTypeList.push(tmp);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }
  getStockCodeDropdown() {
    this.stockCodeList = [];
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_NEW_ITEMS_CODES)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let stockCodeList = response.resources;
          for (var i = 0; i < stockCodeList.length; i++) {
            let tmp = {};
            tmp['value'] = stockCodeList[i].id;
            tmp['display'] = stockCodeList[i].displayName;
            this.stockCodeList.push(tmp);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getStockOrderNumberDropdown() {
    let dropdownapi;
    switch (this.selectedTabIndex) {
      case 0:
        dropdownapi = InventoryModuleApiSuffixModels.STOCKORDERS_DROPDOWN;//this was the default which was old
        break;
      case 1:
        dropdownapi = InventoryModuleApiSuffixModels.PICKING_ORDERS_DROPDOWN;
        break;
      case 2:
        dropdownapi = InventoryModuleApiSuffixModels.PICKED_ORDERS_DROPDOWN;
        break;
      case 3:
        dropdownapi = InventoryModuleApiSuffixModels.SCHEDULED_CUSTOMERS_ORDERS_DROPDOWN;
        break;
      case 4:
        dropdownapi = InventoryModuleApiSuffixModels.TECHNICIAN_ORDERS_DROPDOWN;
        break;
      case 5:
        dropdownapi = InventoryModuleApiSuffixModels.INTERBRANCH_ORDERS_DROPDOWN;
        break;
      case 6:
        dropdownapi = InventoryModuleApiSuffixModels.ACCEPTED_ORDERS_DROPDOWN;
        break;
    }
    let params = new HttpParams().set('tabType', this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, dropdownapi, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(true);
        if (response.resources && response.statusCode === 200) {
          let stockOrderNumberList = response.resources;
          this.stockOrderNumberList = [];
          for (var i = 0; i < stockOrderNumberList.length; i++) {
            let tmp = {};
            tmp['value'] = stockOrderNumberList[i].id;
            tmp['display'] = stockOrderNumberList[i].displayName;
            this.stockOrderNumberList.push(tmp);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        } else {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  /* Picked Cancel orders*/
  cancelShowPopup(index: number, type: any) {
    this.showSaveCancelOrder = false;
    this.showSaveAssignOrder = false;
    if (type == 'enter') {
      this.showPickedOrdersPopup = true;
      this.selectedriIndex = index;
    }
    else {
      this.showPickedOrdersPopup = false;
      this.selectedriIndex = null;
    }
  }

  pickerName: any;

  cancelOrderPopupOpen(data: any, tab: any) {
    this.isPickedCancelOrdersDialog = true;
    this.packerJobId = data.packerJobId;
    this.cancelOrderDialogForm.get('reasons').patchValue('');
    if (tab == 'picked') {
      this.popupHeader = 'Confirmation';
      this.cancelPickedOrder = true;
      this.assignPickingorders = false;
      this.stockOrderNumber = data.stockOrderNumer;
      this.cancelOrderDialogForm.get('reasons').setValidators([Validators.required]);
      this.cancelOrderDialogForm.get('reasons').updateValueAndValidity();
      this.cancelOrderDialogForm.get('packerId').setErrors(null);
      this.cancelOrderDialogForm.get('packerId').setValue(null);
      this.cancelOrderDialogForm.get('packerId').clearValidators();
      this.cancelOrderDialogForm.markAllAsTouched();
      this.cancelOrderDialogForm.get('packerId').updateValueAndValidity();
      this.cancelOrderDialogForm = removeFormControlError(this.cancelOrderDialogForm, 'packerId');
    }
    else {
    //  this.gePickersDropdown();
    this.gePickersDropdown(data.warehouseId);
      if(data.picker){
        this.popupHeader = 'Re-Assign Job';
      }
      else {
        this.popupHeader = 'Assign Job';
      }
      this.assignPickingorders = true;
      this.cancelPickedOrder = false;
      this.stockOrderNumber = data.stockOrderNumber;
      this.pickerName = data.picker;
      this.cancelOrderDialogForm.get('packerId').setValidators([Validators.required]);
      this.cancelOrderDialogForm.get('packerId').updateValueAndValidity();
      this.cancelOrderDialogForm.get('reasons').setErrors(null);
      this.cancelOrderDialogForm.get('reasons').setValue(null);
      this.cancelOrderDialogForm.get('reasons').clearValidators();
      this.cancelOrderDialogForm.markAllAsTouched();
      this.cancelOrderDialogForm.get('reasons').updateValueAndValidity();
      this.cancelOrderDialogForm = removeFormControlError(this.cancelOrderDialogForm, 'reasons');
    }
  }

  getStatusDropdown() {

    let params = new HttpParams().set('tabType', this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_JOB_ORDER_STATUS, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let statusList = response.resources;
          this.statusList = [];
          for (var i = 0; i < statusList.length; i++) {
            let tmp = {};
            tmp['value'] = statusList[i].id;
            tmp['display'] = statusList[i].displayName;
            this.statusList.push(tmp);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getDivivsionDropwn() {
    this.divisionList = [];
    this.crudService.dropdown(ModulesBasedApiSuffix.IT_MANAGEMENT, InventoryModuleApiSuffixModels.DIVISIONS)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let divisionList = response.resources;
          for (var i = 0; i < divisionList.length; i++) {
            let tmp = {};
            tmp['value'] = divisionList[i].id;
            tmp['display'] = divisionList[i].displayName;
            this.divisionList.push(tmp);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }
  getPriorityDrodown() {
    this.priorityList = [];
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PRIORITY)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let priorityList = response.resources;
          for (var i = 0; i < priorityList.length; i++) {
            let tmp = {};
            tmp['value'] = priorityList[i].id;
            tmp['display'] = priorityList[i].displayName;
            this.priorityList.push(tmp);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getIssueingTechLocationDropdown() {

    let params;
    params = new HttpParams().set('userId', this.loggedInUserData.userId);
    if(this.loggedInUserData.warehouseId)
       params.set('warehouseId', this.loggedInUserData.warehouseId);

    let issueTecApi = this.selectedTabIndex==1 ? this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PICKING_ORDER_ISSUE_TECH_LOCATION, undefined, true, params) : 
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PICKED_ORDER_ISSUE_TECH_LOCATION, undefined, true, params);
     issueTecApi.subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let issueingTechLocation = response.resources;
          this.issueingTechLocation = [];
          for (var i = 0; i < issueingTechLocation.length; i++) {
            let tmp = {};
            tmp['value'] = issueingTechLocation[i].id;
            tmp['display'] = issueingTechLocation[i].displayName;
            this.issueingTechLocation.push(tmp);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getRequestingLocationDrodown() {
    this.requestingLocationList = [];
    let params;
    if(this.loggedInUserData.warehouseId)
       params = new HttpParams().set('userId', this.loggedInUserData.userId).set('warehouseId', this.loggedInUserData.warehouseId);
    else
       params = new HttpParams().set('userId', this.loggedInUserData.userId);

    let requestTecApi = this.selectedTabIndex==1 ? this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PICKING_ORDER_REQUESTING_LOCATION, undefined, true, params) : 
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PICKED_ORDER_REQUESTING_LOCATION, undefined, true, params);
    requestTecApi.subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let requestingLocationList = response.resources;
          for (var i = 0; i < requestingLocationList.length; i++) {
            let tmp = {};
            tmp['value'] = requestingLocationList[i].id;
            tmp['display'] = requestingLocationList[i].displayName;
            // if (requestingLocationList[i].displayName != null &&
            //   requestingLocationList[i].displayName != '') {
          
            // }
            this.requestingLocationList.push(tmp);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getPickersDropdown() {
    let params = new HttpParams().set('RoleName', 'technician');
    this.pickerDrordown = [];
    this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_PICKEDJOBS_INVENTORYSTAFF)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let pickerDrordown = response.resources;
          for (var i = 0; i < pickerDrordown.length; i++) {
            let tmp = {};
            tmp['value'] = pickerDrordown[i].id;
            tmp['display'] = pickerDrordown[i].displayName;
            this.pickerDrordown.push(tmp)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  getRequestingLocationNameDropdown() {
    this.requestingLocationNameList = [];
    this.crudService.dropdown(ModulesBasedApiSuffix.TECHNICIAN, InventoryModuleApiSuffixModels.UX_TECHNICIAN_BY_LOCATION)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let requestingLocationNameList = response.resources;
          for (var i = 0; i < requestingLocationNameList.length; i++) {
            let tmp = {};
            tmp['value'] = requestingLocationNameList[i].id;
            tmp['display'] = requestingLocationNameList[i].displayName;
            this.requestingLocationNameList.push(tmp)
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }
  exportList() {

    let otherParams = {};
    // if (this.searchForm.value.searchKeyword) {
    //   otherParams["search"] = this.searchForm.value.searchKeyword;
    // }
    // if (Object.keys(this.row).length > 0) {
    //   // logic for split columns and its values to key value pair

    //   if (this.row['searchColumns']) {
    //     Object.keys(this.row['searchColumns']).forEach((key) => {
    //       if (key.toLowerCase().includes('date')) {
    //         otherParams[key] = this.momentService.localToUTCDateTime(this.row['searchColumns'][key]['value']);
    //       } else {
    //         otherParams[key] = this.row['searchColumns'][key]['value'];
    //       }
    //     });
    //   }
    //   if (this.row['sortOrderColumn']) {
    //     otherParams['sortOrder'] = this.row['sortOrder'];
    //     otherParams['sortOrderColumn'] = this.row['sortOrderColumn'];
    //   }
    // }

     otherParams['UserId'] = this.loggedInUserData.userId;
    // otherParams['pageIndex'] = 0;
    // otherParams['maximumRows'] = this.totalRecords;

    const queryParams = this.generateQueryParams(otherParams);

    let inventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    inventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].exportApiSuffixModel;

    this.crudService.downloadFile(ModulesBasedApiSuffix.INVENTORY, inventoryModuleApiSuffixModels, null, null, queryParams).subscribe((response: any) => {
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response) {
        this.saveAsExcelFile(response, this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }
  onPickerSelected(event){
    if(event.target.value != null && event.target.value != ''){
      let picks = this.pickersList.find(x => x.id == event.target.value);
      this.showPickerName = picks.displayName;
    }
  }
  submitCancelOrder() {
    if (this.cancelOrderDialogForm.invalid) {
      this.cancelOrderDialogForm.markAllAsTouched();
      return;
    }
    let params = {}
    params = {
      "packerJobId": this.packerJobId,
      "modifiedUserId": this.loggedInUserData.userId,
      "modifiedDate": new Date().toISOString(),
      "isCancel": true,
      "CancelReason": this.cancelOrderDialogForm.get('reasons').value
    }

    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.PICKING_CANCELLING_ORDER, params)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.isPickedCancelOrdersDialog = false;
        this.cancelOrderDialogForm.get('reasons').patchValue('');
        this.isCancelOrderSuccessDialog = true;
        this.grvNumer = response.resources;
        this.getRequiredListData('', '', null);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        return;
      }
    });
  }
  reAssignPickingOrders() {
    let params = {}
    params = {
      "packerJobId": this.packerJobId,
      "createdUserId": this.loggedInUserData.userId,
      "packerId": this.cancelOrderDialogForm.get('packerId').value,
      "isTechnicianCollection": this.cancelOrderDialogForm.get('isTechnicianCollection').value
    }
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PACKERJOBS_REASSIGN, params)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.isCancelOrderSuccessDialog = false;
        this.grvNumer = response.resources;
        this.getRequiredListData('', '', null);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        return;
      }
    });
  }
  generateQueryParams(queryParams) {
    const queryParamsString = new HttpParams({ fromObject: queryParams }).toString();
    return '?' + queryParamsString;
  }
  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  pickingDashboardsFilterFunction(stagingPayListModel?: InterBranchOrderListFilter) {
    let pickingOrdersListModel = new InterBranchOrderListFilter(stagingPayListModel);
    /*Picking dashboard*/
    this.pickingDashboardFilterForm = this.formBuilder.group({});
    Object.keys(pickingOrdersListModel).forEach((key) => {
      if (typeof pickingOrdersListModel[key] === 'string') {
        this.pickingDashboardFilterForm.addControl(key, new FormControl(pickingOrdersListModel[key]));
      }
    });
    /*Picking orders*/
    this.pickingOrdersFilterForm = this.formBuilder.group({});
    Object.keys(pickingOrdersListModel).forEach((key) => {
      if (typeof pickingOrdersListModel[key] === 'string') {
        this.pickingOrdersFilterForm.addControl(key, new FormControl(pickingOrdersListModel[key]));
      }
    });
    /*Picked orders*/
    this.pickedOrdersFilterForm = this.formBuilder.group({});
    Object.keys(pickingOrdersListModel).forEach((key) => {
      if (typeof pickingOrdersListModel[key] === 'string') {
        this.pickedOrdersFilterForm.addControl(key, new FormControl(pickingOrdersListModel[key]));
      }
    });
    /*Scheduled orders*/
    this.scheduledOrdersFilterForm = this.formBuilder.group({});
    Object.keys(pickingOrdersListModel).forEach((key) => {
      if (typeof pickingOrdersListModel[key] === 'string') {
        this.scheduledOrdersFilterForm.addControl(key, new FormControl(pickingOrdersListModel[key]));
      }
    });
    /*Technician orders*/
    this.technicianOrdersFilterForm = this.formBuilder.group({});
    Object.keys(pickingOrdersListModel).forEach((key) => {
      if (typeof pickingOrdersListModel[key] === 'string') {
        this.technicianOrdersFilterForm.addControl(key, new FormControl(pickingOrdersListModel[key]));
      }
    });
    /*Interbranch dashboard*/
    this.interBranchtransferFilterForm = this.formBuilder.group({});
    Object.keys(pickingOrdersListModel).forEach((key) => {
      if (typeof pickingOrdersListModel[key] === 'string') {
        this.interBranchtransferFilterForm.addControl(key, new FormControl(pickingOrdersListModel[key]));
      }
    });
    /*Accepted orders*/
    this.acceptedOrdersFilterForm = this.formBuilder.group({});
    Object.keys(pickingOrdersListModel).forEach((key) => {
      if (typeof pickingOrdersListModel[key] === 'string') {
        this.acceptedOrdersFilterForm.addControl(key, new FormControl(pickingOrdersListModel[key]));
      }
    });
  }

  submitFilter() {
    this.row = { pageIndex: 0, pageSize: 10 };
    this.pageSize = 10;
    if (this.selectedTabIndex == 0) {
      let dashboard = Object.assign({},
        this.pickingDashboardFilterForm.get('warehouseId').value == '' ? null : { WarehouseId: this.pickingDashboardFilterForm.get('warehouseId').value },
        { UserId: this.loggedInUserData.userId }
      );
      this.filterParams = dashboard;
      this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], dashboard);
      this.showPickingDashboardFilterForm = !this.showPickingDashboardFilterForm;
    }
    else if (this.selectedTabIndex == 1) {
      let picking = Object.assign({},
        this.pickingOrdersFilterForm.get('stockOrderNumberId').value == '' ? null : { StockOrderIds: this.pickingOrdersFilterForm.get('stockOrderNumberId').value },
        this.pickingOrdersFilterForm.get('orderTypeId').value == '' ? null : { OrderTypeIds: this.pickingOrdersFilterForm.get('orderTypeId').value },
        this.pickingOrdersFilterForm.get('priorityId').value == '' ? null : { PriorityIds: this.pickingOrdersFilterForm.get('priorityId').value },
        this.pickingOrdersFilterForm.get('statusId').value == '' ? null : { StatusIds: this.pickingOrdersFilterForm.get('statusId').value },
        this.pickingOrdersFilterForm.get('divisionId').value == '' ? null : { DivisionIds: this.pickingOrdersFilterForm.get('divisionId').value },
        this.pickingOrdersFilterForm.get('fromDate').value == '' || this.pickingOrdersFilterForm.get('fromDate').value == null ? null : { FromDate: this.datePipe.transform(this.pickingOrdersFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
        this.pickingOrdersFilterForm.get('toDate').value == '' || this.pickingOrdersFilterForm.get('toDate').value == null ? null : { ToDate: this.datePipe.transform(this.pickingOrdersFilterForm.get('toDate').value, 'yyyy-MM-dd') },
        this.pickingOrdersFilterForm.get('issueingLocationId').value == '' ? null : { WarehouseIds: this.pickingOrdersFilterForm.get('issueingLocationId').value },
        this.pickingOrdersFilterForm.get('issueingTechLocationId').value == '' ? null : { IssueingLocationIds: this.pickingOrdersFilterForm.get('issueingTechLocationId').value },
        this.pickingOrdersFilterForm.get('receivingLocationId').value == '' ? null : { RequestingLocationIds: this.pickingOrdersFilterForm.get('receivingLocationId').value },
        { UserId: this.loggedInUserData.userId }
      );
      this.filterParams = picking;
      this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], picking);
      this.showPickingOrdersFilterForm = !this.showPickingOrdersFilterForm;
    }
    else if (this.selectedTabIndex == 2) {

      let picked = Object.assign({},
        this.pickedOrdersFilterForm.get('stockOrderNumberId').value == '' ? null : { StockOrderIds: this.pickedOrdersFilterForm.get('stockOrderNumberId').value },
        this.pickedOrdersFilterForm.get('orderTypeId').value == '' ? null : { OrderTypeIds: this.pickedOrdersFilterForm.get('orderTypeId').value },
        this.pickedOrdersFilterForm.get('priorityId').value == '' ? null : { PriorityIds: this.pickedOrdersFilterForm.get('priorityId').value },
        this.pickedOrdersFilterForm.get('statusId').value == '' ? null : { StatusIds: this.pickedOrdersFilterForm.get('statusId').value },
        this.pickedOrdersFilterForm.get('divisionId').value == '' ? null : { DivisionIds: this.pickedOrdersFilterForm.get('divisionId').value },
        this.pickedOrdersFilterForm.get('fromDate').value == '' || this.pickedOrdersFilterForm.get('fromDate').value == null ? null : { FromDate: this.datePipe.transform(this.pickedOrdersFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
        this.pickedOrdersFilterForm.get('toDate').value == '' || this.pickedOrdersFilterForm.get('toDate').value == null ? null : { ToDate: this.datePipe.transform(this.pickedOrdersFilterForm.get('toDate').value, 'yyyy-MM-dd') },
        this.pickedOrdersFilterForm.get('issueingLocationId').value == '' ? null : { WarehouseIds: this.pickedOrdersFilterForm.get('issueingLocationId').value },
        this.pickedOrdersFilterForm.get('issueingTechLocationId').value == '' ? null : { IssueingLocationIds: this.pickedOrdersFilterForm.get('issueingTechLocationId').value },
        this.pickedOrdersFilterForm.get('receivingLocationId').value == '' ? null : { RequestingLocationIds: this.pickedOrdersFilterForm.get('receivingLocationId').value },
        this.pickedOrdersFilterForm.get('pickerId').value == '' ? null : { PickerIds: this.pickedOrdersFilterForm.get('pickerId').value },
        this.pickedOrdersFilterForm.get('pickingFromDate').value == '' || this.pickedOrdersFilterForm.get('pickingFromDate').value == null ? null : { PickingFromDate: this.datePipe.transform(this.pickedOrdersFilterForm.get('pickingFromDate').value, 'yyyy-MM-dd') },
        this.pickedOrdersFilterForm.get('pickingToDate').value == '' || this.pickedOrdersFilterForm.get('pickingToDate').value == null ? null : { PickingToDate: this.datePipe.transform(this.pickedOrdersFilterForm.get('pickingToDate').value, 'yyyy-MM-dd') },
        this.pickedOrdersFilterForm.get('collectedFromDate').value == '' || this.pickedOrdersFilterForm.get('collectedFromDate').value == null ? null : { CollectedFromDate: this.datePipe.transform(this.pickedOrdersFilterForm.get('collectedFromDate').value, 'yyyy-MM-dd') },
        this.pickedOrdersFilterForm.get('collectedToDate').value == '' || this.pickedOrdersFilterForm.get('collectedToDate').value == null ? null : { CollectedToDate: this.datePipe.transform(this.pickedOrdersFilterForm.get('collectedToDate').value, 'yyyy-MM-dd') },
        this.pickedOrdersFilterForm.get('dispatchTRFNumber').value == '' ? null : { DispatchTRFNumberIds: this.pickedOrdersFilterForm.get('dispatchTRFNumber').value },
        this.pickedOrdersFilterForm.get('stockTRFNumber').value == '' ? null : { StockTRFNumberIds: this.pickedOrdersFilterForm.get('stockTRFNumber').value },
        { UserId: this.loggedInUserData.userId }
      );

      this.filterParams = picked;
      this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], picked);
      this.showPickedOrdersFilterForm = !this.showPickedOrdersFilterForm;
    }
    else if (this.selectedTabIndex == 3) {
      let scheduled = Object.assign({},
        this.scheduledOrdersFilterForm.get('stockOrderNumberId').value == '' ? null : { StockOrderIds: this.scheduledOrdersFilterForm.get('stockOrderNumberId').value },
        this.scheduledOrdersFilterForm.get('priorityId').value == '' ? null : { PriorityIds: this.scheduledOrdersFilterForm.get('priorityId').value },
        this.scheduledOrdersFilterForm.get('divisionId').value == '' ? null : { DivisionIds: this.scheduledOrdersFilterForm.get('divisionId').value },
        this.scheduledOrdersFilterForm.get('fromDate').value == '' || this.scheduledOrdersFilterForm.get('fromDate').value == null ? null : { FromDate: this.datePipe.transform(this.scheduledOrdersFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
        this.scheduledOrdersFilterForm.get('toDate').value == '' || this.scheduledOrdersFilterForm.get('toDate').value == null ? null : { ToDate: this.datePipe.transform(this.scheduledOrdersFilterForm.get('toDate').value, 'yyyy-MM-dd') },
        this.scheduledOrdersFilterForm.get('issueingLocationId').value == '' ? null : { WarehouseIds: this.scheduledOrdersFilterForm.get('issueingLocationId').value },
        this.scheduledOrdersFilterForm.get('stockCodeId').value == '' ? null : { ItemIds: this.scheduledOrdersFilterForm.get('stockCodeId').value },
        this.scheduledOrdersFilterForm.get('receivingLocationId').value == '' ? null : { RequestingLocationIds: this.scheduledOrdersFilterForm.get('receivingLocationId').value },
        this.scheduledOrdersFilterForm.get('receivingLocationNameId').value == '' ? null : { RequestingLocationNames: this.scheduledOrdersFilterForm.get('receivingLocationNameId').value },
        this.scheduledOrdersFilterForm.get('callCreationFromDate').value == '' || this.scheduledOrdersFilterForm.get('callCreationFromDate').value == null ? null : { CallCreationFromDate: this.datePipe.transform(this.scheduledOrdersFilterForm.get('callCreationFromDate').value, 'yyyy-MM-dd') },
        this.scheduledOrdersFilterForm.get('callCreationToDate').value == '' || this.scheduledOrdersFilterForm.get('callCreationToDate').value == null ? null : { CallCreationToDate: this.datePipe.transform(this.scheduledOrdersFilterForm.get('callCreationToDate').value, 'yyyy-MM-dd') },
        this.scheduledOrdersFilterForm.get('salesRepId').value == '' ? null : { SalesrepIds: this.scheduledOrdersFilterForm.get('salesRepId').value },
        { UserId: this.loggedInUserData.userId }
      );
      this.filterParams = scheduled;
      this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], scheduled);
      this.showScheduledOrdersFilterForm = !this.showScheduledOrdersFilterForm;
    }
    else if (this.selectedTabIndex == 4) {
      let tech = Object.assign({},
        this.technicianOrdersFilterForm.get('stockOrderNumberId').value == '' ? null : { StockOrderIds: this.technicianOrdersFilterForm.get('stockOrderNumberId').value },
        this.technicianOrdersFilterForm.get('divisionId').value == '' ? null : { DivisionIds: this.technicianOrdersFilterForm.get('divisionId').value },
        this.technicianOrdersFilterForm.get('fromDate').value == '' || this.technicianOrdersFilterForm.get('fromDate').value == null ? null : { OrderSubmittedFromdate: this.datePipe.transform(this.technicianOrdersFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
        this.technicianOrdersFilterForm.get('toDate').value == '' || this.technicianOrdersFilterForm.get('toDate').value == null ? null : { OrderSubmittedToDate: this.datePipe.transform(this.technicianOrdersFilterForm.get('toDate').value, 'yyyy-MM-dd') },
        this.technicianOrdersFilterForm.get('issueingLocationId').value == '' ? null : { WarehouseIds: this.technicianOrdersFilterForm.get('issueingLocationId').value },
        this.technicianOrdersFilterForm.get('stockCodeId').value == '' ? null : { ItemIds: this.technicianOrdersFilterForm.get('stockCodeId').value },
        this.technicianOrdersFilterForm.get('receivingLocationId').value == '' ? null : { RequestingLocationIds: this.technicianOrdersFilterForm.get('receivingLocationId').value },
        this.technicianOrdersFilterForm.get('receivingLocationNameId').value == '' ? null : { RequestingLocationNames: this.technicianOrdersFilterForm.get('receivingLocationNameId').value },
        { UserId: this.loggedInUserData.userId }
      );
      this.filterParams = tech;
      this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], tech);
      this.showTechnicianOrdersFilterForm = !this.showTechnicianOrdersFilterForm;
    }
    else if (this.selectedTabIndex == 5) {
      let interbranch = Object.assign({},
        this.interBranchtransferFilterForm.get('fromWarehouseId').value == '' ? null : { RequestingLocationIds: this.interBranchtransferFilterForm.get('fromWarehouseId').value },
        this.interBranchtransferFilterForm.get('toWarehouseId').value == '' ? null : { WarehouseIds: this.interBranchtransferFilterForm.get('toWarehouseId').value },
        this.interBranchtransferFilterForm.get('stockOrderNumberId').value == '' ? null : { StockOrderIds: this.interBranchtransferFilterForm.get('stockOrderNumberId').value },
        this.interBranchtransferFilterForm.get('fromDate').value == '' || this.interBranchtransferFilterForm.get('fromDate').value == null ? null :
          { FromDate: this.datePipe.transform(this.interBranchtransferFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
        this.interBranchtransferFilterForm.get('toDate').value == '' || this.interBranchtransferFilterForm.get('toDate').value == null ? null :
          { ToDate: this.datePipe.transform(this.interBranchtransferFilterForm.get('toDate').value, 'yyyy-MM-dd') },
        this.interBranchtransferFilterForm.get('stockCodeId').value == '' ? null : { ItemIds: this.interBranchtransferFilterForm.get('stockCodeId').value },
        this.interBranchtransferFilterForm.get('divisionId').value == '' ? null : { DivisionIds: this.interBranchtransferFilterForm.get('divisionId').value },
        { UserId: this.loggedInUserData.userId }

      );
      this.filterParams = interbranch;
      this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], interbranch);
      this.showInterBranchFilterForm = !this.showInterBranchFilterForm;
    }
    else if (this.selectedTabIndex == 6) {
      let accepted = Object.assign({},
        this.acceptedOrdersFilterForm.get('stockOrderNumberId').value == '' ? null : { StockOrderIds: this.acceptedOrdersFilterForm.get('stockOrderNumberId').value },
        this.acceptedOrdersFilterForm.get('priorityId').value == '' ? null : { PriorityIds: this.acceptedOrdersFilterForm.get('priorityId').value },
        this.acceptedOrdersFilterForm.get('divisionId').value == '' ? null : { DivisionIds: this.acceptedOrdersFilterForm.get('divisionId').value },
        this.acceptedOrdersFilterForm.get('issueingLocationId').value == '' ? null : { WarehouseIds: this.acceptedOrdersFilterForm.get('issueingLocationId').value },
        this.acceptedOrdersFilterForm.get('stockCodeId').value == '' ? null : { ItemIds: this.acceptedOrdersFilterForm.get('stockCodeId').value },
        this.acceptedOrdersFilterForm.get('callCreationFromDate').value == '' || this.acceptedOrdersFilterForm.get('callCreationFromDate').value == null ? null :
          { CallCreationFromDate: this.datePipe.transform(this.acceptedOrdersFilterForm.get('callCreationFromDate').value, 'yyyy-MM-dd') },
        this.acceptedOrdersFilterForm.get('callCreationToDate').value == '' || this.acceptedOrdersFilterForm.get('callCreationToDate').value == null ? null :
          { CallCreationToDate: this.datePipe.transform(this.acceptedOrdersFilterForm.get('callCreationToDate').value, 'yyyy-MM-dd') },
        this.acceptedOrdersFilterForm.get('scheduleFromdate').value == '' || this.acceptedOrdersFilterForm.get('scheduleFromdate').value == null ? null :
          { ScheduleFromdate: this.datePipe.transform(this.acceptedOrdersFilterForm.get('scheduleFromdate').value, 'yyyy-MM-dd') },
        this.acceptedOrdersFilterForm.get('scheduleToDate').value == '' || this.acceptedOrdersFilterForm.get('scheduleToDate').value == null ? null :
          { ScheduleToDate: this.datePipe.transform(this.acceptedOrdersFilterForm.get('scheduleToDate').value, 'yyyy-MM-dd') },
        { UserId: this.loggedInUserData.userId }
      );
      this.filterParams = accepted;
      this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], accepted);
      this.showAcceptedOrdersFilterForm = !this.showAcceptedOrdersFilterForm;
    }
  }

  resetForm(type?:any) {
    this.pageSize = 10;
    this.reset = true;
    if (this.selectedTabIndex == 0) {
      this.pickingDashboardFilterForm.reset();
      if(type!='tabchange')
       this.showPickingDashboardFilterForm = !this.showPickingDashboardFilterForm;
    }
    else if (this.selectedTabIndex == 1) {
      this.pickingOrdersFilterForm.reset();
      if(type!='tabchange')
       this.showPickingOrdersFilterForm = !this.showPickingOrdersFilterForm;
    }
    else if (this.selectedTabIndex == 2) {
      this.pickedOrdersFilterForm.reset();
      if(type!='tabchange')
       this.showPickedOrdersFilterForm = !this.showPickedOrdersFilterForm;
    }
    else if (this.selectedTabIndex == 3) {
      this.scheduledOrdersFilterForm.reset();
      if(type!='tabchange')
       this.showScheduledOrdersFilterForm = !this.showScheduledOrdersFilterForm;
    }
    else if (this.selectedTabIndex == 4) {
      this.technicianOrdersFilterForm.reset();
      if(type!='tabchange')
       this.showTechnicianOrdersFilterForm = !this.showTechnicianOrdersFilterForm;
    }
    else if (this.selectedTabIndex == 5) {
      this.interBranchtransferFilterForm.reset();
      if(type!='tabchange')
       this.showInterBranchFilterForm = !this.showInterBranchFilterForm;
    }
    else if (this.selectedTabIndex == 6) {
      this.acceptedOrdersFilterForm.reset();
      if(type!='tabchange')
       this.showAcceptedOrdersFilterForm = !this.showAcceptedOrdersFilterForm;
    }
    this.filterParams = null;
    //this.getRequiredListData('', '', null);
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/inventory/picking-dashboard-orders'], { queryParams: { tab: this.selectedTabIndex } });
    this.resetForm('tabchange');
    if(this.selectedTabIndex==1 || this.selectedTabIndex==2){
      this.getIssueingTechLocationDropdown(); 
      this.getRequestingLocationDrodown()
    }

    if (this.selectedTabIndex == 6) {
    } else {
      this.getRequiredListData()
    }
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
}