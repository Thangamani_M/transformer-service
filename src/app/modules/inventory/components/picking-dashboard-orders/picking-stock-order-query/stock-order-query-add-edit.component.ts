import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { StockOrderItemsItemsModel, StockOrderQueryCreationModel } from '@modules/inventory/models/stock-order-query.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-stock-order-query-add-edit',
    templateUrl: './stock-order-query-add-edit.component.html',
})

export class StockOrderQueryAddEditComponent {

    stockOrderId: any;
    getDetailsData: any;
    stockOrderType: any;
    stockOrderQueryCreationForm: FormGroup;
    stockOrderDetails: FormArray;
    managerDropDown: any = [];
    stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
    userData: UserLogin;
    viewType: any;
    stockOrderQueryId: any;
    header: string = '';
    tabsNumber: any;
    packerJobId;
    redirectType;
    screenType;
    constructor(
        private activatedRoute: ActivatedRoute, private router: Router,private snackbarService:SnackbarService,
        private rxjsService: RxjsService, private crudService: CrudService,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private store: Store<AppState>,
    ) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });

        this.stockOrderId = this.activatedRoute.snapshot.queryParams.id;
        this.packerJobId = this.activatedRoute.snapshot.queryParams.packerJobId;
        this.stockOrderQueryId = this.activatedRoute.snapshot.queryParams.stockOrderQueryId;
        this.stockOrderType = this.activatedRoute.snapshot.queryParams.type;
        this.viewType = this.activatedRoute.snapshot.queryParams.viewType;
        this.header = this.activatedRoute.snapshot.queryParams.header;
        this.tabsNumber = this.activatedRoute.snapshot.queryParams.tab;
        this.redirectType = this.activatedRoute.snapshot.queryParams.redirectType;
        this.screenType = this.activatedRoute.snapshot.queryParams.screenType;
    }

    ngOnInit() {

        //this.getDropDown();
        this.createstockOrderQueryCreationForm();

        if (this.packerJobId || this.stockOrderQueryId) {

            this.getStockOrderQueryDetailsById().subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode == 200) {
                    let stockOrderModel = new StockOrderQueryCreationModel(response.resources);
                    this.getDetailsData = response.resources;          
                    this.stockOrderId =  response.resources?.stockOrderId         
                    this.stockOrderQueryCreationForm.get('packerJobId').setValue(response.resources?.packerJobId);
                    this.stockOrderQueryCreationForm.get('tamId').setValue(response.resources?.tamId);
                    this.stockOrderQueryId = response.resources.stockOrderQueryId;
                    this.stockOrderQueryCreationForm.patchValue(stockOrderModel);
                    this.stockOrderQueryCreationForm.get('referenceId').patchValue(this.stockOrderId? this.stockOrderId : response.resources?.stockOrderId)
                    this.stockOrderDetails = this.getStockOrderQueryItemsArray;

                    if (response.resources.stockOrderItems && response.resources.stockOrderItems.length >= 0) {
                        response.resources.stockOrderItems.forEach((stockOrder) => {
                            this.stockOrderDetails.push(this.createStockOrderQueryItemsModel(stockOrder));
                        });
                    }
                    else {
                        this.stockOrderDetails.push(this.createStockOrderQueryItemsModel());
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                else {
                    this.stockOrderQueryCreationForm.patchValue(null);
                    this.stockOrderDetails = this.getStockOrderQueryItemsArray;
                    this.stockOrderDetails.push(this.createStockOrderQueryItemsModel());
                }
            });
        }
        else {
            this.stockOrderQueryCreationForm.patchValue(null);
            this.stockOrderDetails = this.getStockOrderQueryItemsArray;
            this.stockOrderDetails.push(this.createStockOrderQueryItemsModel());
        }
    }

    getStockOrderQueryDetailsById(): Observable<IApplicationResponse> {
        if (this.screenType == 'picking') {
            let params = new HttpParams().set('packerJobId', this.packerJobId).set('OrderType', this.stockOrderType);
            return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_ORDER_DETALS, null, null, params);
            //return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PICKING_DASHBOARD_VIEW, null, null, params);
        }
        else if(this.screenType == 'task-list' || this.screenType == 'notification') {
            let stockId = new HttpParams().set('packerJobId', this.packerJobId);
            return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_ORDER_DETALS, null, null, stockId);
        }
    }

    getDropDown() {
        let roles = new HttpParams().set('RoleName', 'Technical Area Manager').set('WarehouseId', this.userData.warehouseId);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_ROLE, null, null, roles)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode == 200) {
                    this.managerDropDown = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    createstockOrderQueryCreationForm(): void {
        // create form controls dynamically from model class
        let stockOrderModel = new StockOrderQueryCreationModel();
        this.stockOrderQueryCreationForm = this.formBuilder.group({
            stockOrderDetails: this.formBuilder.array([])
        });
        Object.keys(stockOrderModel).forEach((key) => {
            this.stockOrderQueryCreationForm.addControl(key, new FormControl(stockOrderModel[key]));
        });
        // if (this.viewType == 'Inventory Manager') {
        //     this.stockOrderQueryCreationForm = setRequiredValidator(this.stockOrderQueryCreationForm, ["tamId","packerJobId"]);
        // }
    }

    //Create FormArray
    get getStockOrderQueryItemsArray(): FormArray {
        if (!this.stockOrderQueryCreationForm) return;
        return this.stockOrderQueryCreationForm.get("stockOrderDetails") as FormArray;
    }

    //Create FormArray controls
    createStockOrderQueryItemsModel(interBranchModel?: StockOrderItemsItemsModel): FormGroup {
        let stockQueryModelData = new StockOrderItemsItemsModel(interBranchModel);
        let formControls = {};
        Object.keys(stockQueryModelData).forEach((key) => {           
            if(this.screenType == 'task-list' || this.screenType == 'notification') {
                formControls[key] = [stockQueryModelData[key], (key === 'tamFeedback') ? [Validators.required] : []]
            }
            else 
              formControls[key] = [stockQueryModelData[key], (key === 'comment') ?  [] : []]
            
        });
        return this.formBuilder.group(formControls);
    }
    navigateToViewPage() {
        //http://localhost:4200//technical-management/tech-stock-order/
        //approval/view?id=ac2c35cc-8cae-4a86-b924-cee0b5cfaf04&stockOrderApprovalId =&type=add-edit
           //this.router.navigateByUrl(editableObject['navigationURL']);
           console.log('this.stockOrderId',this.packerJobId)
           console.log('this.stockOrderId',this.stockOrderId)
           this.router.navigate(['/technical-management/tech-stock-order/approval/add-edit'], {
               queryParams: {id: this.stockOrderId,packerJobId:this.packerJobId, type: 'approval-history', screenType:'stock-order-query'}
             });
    }
    onSubmit(value: number) {

        if (this.stockOrderQueryCreationForm.invalid) {
            return;
        }
        // let filter  = this.stockOrd1erQueryCreationForm.value.stockOrderDetails.filter( x => (x.comment!=null && x.comment!='') );
        if(this.getDetailsData?.status!='Query Submitted' && this.stockOrderQueryCreationForm.value.stockOrderDetails?.length > 0){
            let filter = this.stockOrderQueryCreationForm.value.stockOrderDetails.filter(x=> (!x.comment || x.comment==null));
            if(filter && filter.length == this.stockOrderQueryCreationForm.value.stockOrderDetails.length){
                this.snackbarService.openSnackbar("Add a comment for at least one stock code.", ResponseMessageTypes.WARNING);
                return;
            }
        }
       
        // if(filter?.length==0){
        //     this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        //     return;
        // }
  
        let queryData;
        if (this.screenType == 'picking') {
            delete this.stockOrderQueryCreationForm.value.warehouseId;
               
            queryData = {
                ...this.stockOrderQueryCreationForm.value,
                stockOrderQueryStatusId: value,
                createdUserId: this.userData.userId,
                createdDate: new Date().toISOString()
            }

            if(this.userData.warehouseId)
               queryData.warehouseId =  this.userData.warehouseId;

            queryData.StockOrderQueryComments = queryData.stockOrderDetails;

        
        }
        else if(this.screenType == 'task-list' || this.screenType == 'notification') {
            let stockParams = [];
            this.stockOrderQueryCreationForm.value.stockOrderDetails.forEach((key) => {
                console.log('key',key)
                let tmp = {}
                tmp['stockOrderQueryCommentId'] = key.stockOrderQueryCommentId;
                tmp['itemId'] = key.itemId;
                tmp['comment'] = key.comment;
                tmp['tamFeedback'] = key.tamFeedback;
                tmp['tamFeedbackDate'] = new Date().toISOString();
                tmp['isTAMReplied'] = true;
                stockParams.push(tmp)
            });
            queryData = {
                stockOrderQueryCommentUpdate: stockParams,
                stockOrderQueryStatusId: value,
                stockOrderId:this.stockOrderQueryCreationForm.value.stockOrderId,
                modifiedUserId: this.userData.userId,
                // modifiedDate: new Date().toISOString(),
                stockOrderQueryId: this.stockOrderQueryId
            }
            queryData.StockOrderQueryComments = queryData.stockOrderQueryCommentUpdate;
        }

        queryData.packerJobId = this.packerJobId;
        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
   
        let queryDataTemp;
        queryDataTemp =  {...queryData,...{ModifiedUserId:this.userData.userId}};
        delete queryDataTemp.stockOrderQueryCommentUpdate;
        if(this.stockOrderQueryId)
        queryDataTemp = {...queryDataTemp,...{stockOrderQueryId:this.stockOrderQueryId}};
        let crudService: Observable<IApplicationResponse> = ( !this.stockOrderQueryId) ? this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_ORDER_QUERY, queryData) :
            this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_ORDER_QUERY,queryDataTemp)

        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                let stockOrderQueryId = response.resources;
                this.navigateToList();
            }
        });
        this.navigateToList();
    };

    navigateToResubmit(queryId: string) {
        if (this.screenType == 'picking') {
            this.router.navigate(['/inventory', 'picking-dashboard-orders', 'stock-order-resubmit'], {
                queryParams: {
                    id: queryId,
                    packerJobId:this.packerJobId,
                    type: this.stockOrderType,
                    viewType: this.viewType,
                    header: this.header,
                    tab:this.tabsNumber,
                }, skipLocationChange: true
            });
        }
        else {
            this.navigateToList();
        }
    }

    navigateToList() {
        if(this.redirectType=='task-list' ){
            this.router.navigate(['/technical-management/technical-area-manager-worklist']);
        }else {        
            this.router.navigate(['/inventory', 'picking-dashboard-orders', 'picking-dashboard-view'], {
                queryParams: {
                    id: this.stockOrderId ? this.stockOrderId : this.getDetailsData?.stockOrderId,
                    packerJobId:this.packerJobId,
                    type: this.stockOrderType,
                    header: this.header,
                    tab: this.screenType == 'notification' ? 1 : this.tabsNumber
                }
            });
        }      
    }


}