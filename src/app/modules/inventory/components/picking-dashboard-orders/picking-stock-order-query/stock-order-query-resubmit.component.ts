import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import {
    CrudService, CustomDirectiveConfig, HttpCancelService, IApplicationResponse,
    ModulesBasedApiSuffix, RxjsService
} from '@app/shared';
import { StockOrderItemsItemsModel, StockOrderQueryCreationModel } from '@modules/inventory/models/stock-order-query.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { TechnicalMgntModuleApiSuffixModels } from '@modules/technical-management/shared/enum.ts/technical.enum';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-stock-order-query-resubmit',
    templateUrl: './stock-order-query-resubmit.component.html',
})

export class StockOrderQueryResubmitComponent {
    stockOrderId: any;
    getDetailsData: any;
    stockOrderType: any;
    stockOrderQueryCreationForm: FormGroup;
    stockOrderDetails: FormArray;
    managerDropDown: any = [];
    stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
    userData: UserLogin;
    getStockOrderQueryId: any;
    viewType: any;
    header: string = '';
    packerJobId;
    constructor(
        private activatedRoute: ActivatedRoute, private router: Router,
        private rxjsService: RxjsService, private crudService: CrudService,
        private formBuilder: FormBuilder, private httpCancelService: HttpCancelService,
        private store: Store<AppState>,
    ) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });

        this.stockOrderId = this.activatedRoute.snapshot.queryParams.id;
        this.stockOrderType = this.activatedRoute.snapshot.queryParams.type;
        this.viewType = this.activatedRoute.snapshot.queryParams.viewType;
        this.header = this.activatedRoute.snapshot.queryParams.header;
    }

    ngOnInit() {

        this.getDropDown();
        this.createstockOrderQueryCreationForm();

        if (this.stockOrderId) {
            this.getStockOrderQueryDetailsById().subscribe((response: IApplicationResponse) => {
                let stockOrderModel = new StockOrderQueryCreationModel(response.resources);
                this.getDetailsData = response.resources;
                this.getStockOrderQueryId = response.resources.stockOrderQueryId;
                this.stockOrderQueryCreationForm.patchValue(stockOrderModel);
                this.stockOrderQueryCreationForm.get('referenceId').patchValue(this.stockOrderId)
                this.stockOrderDetails = this.getStockOrderQueryItemsArray;

                if (response.resources.stockOrderItems.length >= 0) {
                    response.resources.stockOrderItems.forEach((stockOrder) => {
                        this.stockOrderDetails.push(this.createStockOrderQueryItemsModel(stockOrder));
                    });

                }
                else {
                    this.stockOrderDetails.push(this.createStockOrderQueryItemsModel());
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
        else {
            this.stockOrderDetails = this.getStockOrderQueryItemsArray;
            this.stockOrderDetails.push(this.createStockOrderQueryItemsModel());
        }

    }

    getStockOrderQueryDetailsById(): Observable<IApplicationResponse> {
        let params = new HttpParams().set('packerJobId', this.packerJobId).set('orderType', this.stockOrderType);
        return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_ORDER_DETALS, null, null, params);
    }

    getDropDown() {
        // RoleName=Technical Area Manager&WarehouseId=04B8B10F-F1E4-4FC9-95F4-0BF200292698
        let params = new HttpParams().set('RoleName', 'Technical Area Manager').set('WarehouseId', this.userData.warehouseId);
        this.crudService.get(ModulesBasedApiSuffix.TECHNICIAN, TechnicalMgntModuleApiSuffixModels.TECHNICIAN_ROLE, null, null, params)
        .subscribe((response: IApplicationResponse) => {
            if (response.resources) {
                this.managerDropDown = response.resources;
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }

    createstockOrderQueryCreationForm(): void {
        let stockOrderModel = new StockOrderQueryCreationModel();
        // create form controls dynamically from model class
        this.stockOrderQueryCreationForm = this.formBuilder.group({
            stockOrderDetails: this.formBuilder.array([])
        });
        Object.keys(stockOrderModel).forEach((key) => {
            this.stockOrderQueryCreationForm.addControl(key, new FormControl(stockOrderModel[key]));
        });
    }

    //Create FormArray
    get getStockOrderQueryItemsArray(): FormArray {
        if (!this.stockOrderQueryCreationForm) return;
        return this.stockOrderQueryCreationForm.get("stockOrderDetails") as FormArray;
    }

    //Create FormArray controls
    createStockOrderQueryItemsModel(interBranchModel?: StockOrderItemsItemsModel): FormGroup {
        let stockQueryModelData = new StockOrderItemsItemsModel(interBranchModel);
        let formControls = {};
        Object.keys(stockQueryModelData).forEach((key) => {
            formControls[key] = [stockQueryModelData[key], (key === 'comment') ? [Validators.required] : []]
        });

        return this.formBuilder.group(formControls);
    }

    onSubmit(value: number) {

        if (this.stockOrderQueryCreationForm.invalid) {
            return;
        }

        let queryData;

        let stockParams = [];
        this.stockOrderQueryCreationForm.value.stockOrderDetails.forEach((key) => {
            let tmp = {}
            tmp['stockOrderQueryCommentId'] = key.stockOrderQueryCommentId ? key.stockOrderQueryCommentId : null;
            tmp['comment'] = key.comment;

            if (key['tamFeedback'] == '') {
                tmp['tamFeedback'] = null
                tmp['tamFeedbackDate'] = null;
                tmp['isTAMReplied'] = false;
            }
            else {
                tmp['tamFeedback'] = key.tamFeedback;
                tmp['tamFeedbackDate'] = new Date().toISOString();
                tmp['isTAMReplied'] = true;
            }
            stockParams.push(tmp)
        });

        queryData = {
            stockOrderQueryCommentUpdate: stockParams,
            stockOrderQueryStatusId: value,
            modifiedUserId: this.userData.userId,
            modifiedDate: new Date().toISOString(),
            // stockOrderQueryId: this.getStockOrderQueryId
            stockOrderId: this.stockOrderId
        }


        this.httpCancelService.cancelPendingRequestsOnFormSubmission();
        let crudService: Observable<IApplicationResponse> = this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_ORDER_RESUBMIT, queryData)
        crudService.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.navigateToList();
            }
        });
    };

    navigateToList() {
        this.router.navigate(['/inventory', 'picking-dashboard-orders', 'picking-dashboard-view'], {
            queryParams: {
                id: this.stockOrderId,
                type: this.stockOrderType,
                header: this.header
            }, skipLocationChange: true
        });
    }

}