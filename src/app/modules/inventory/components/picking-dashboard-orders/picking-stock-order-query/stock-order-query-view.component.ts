import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, RxjsService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { Observable } from 'rxjs';
import { StockOrderViewModalComponent } from './stock-order-view-modal.component';

@Component({
    selector: 'app-stock-order-query-view',
    templateUrl: './stock-order-query-view.component.html',
})

export class StockOrderQueryViewComponent {

    stockOrderId: any;
    getDetailsData: any;
    auditReportDetailsData: any;
    packerJobId;
    constructor(
        private activatedRoute: ActivatedRoute, private router: Router,
        private rxjsService: RxjsService, private crudService: CrudService,
        private dialog: MatDialog,
    ) {
        this.stockOrderId = this.activatedRoute.snapshot.queryParams.id;
        this.packerJobId = this.activatedRoute.snapshot.queryParams.packerJobId;
    }

    ngOnInit() {
        this.getStockOrderQueryDetailsById().subscribe((response: IApplicationResponse) => {
            this.getDetailsData = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
        });

        this.getAuditReportModalDetails().subscribe((response: IApplicationResponse) => {
            this.auditReportDetailsData = response.resources;
            this.rxjsService.setGlobalLoaderProperty(false);
        });

    }

    getStockOrderQueryDetailsById(): Observable<IApplicationResponse> {
        let params = new HttpParams().set('packerJobId', this.packerJobId);
        return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_ORDER_DETALS, null, null, params);
    }

    getAuditReportModalDetails(): Observable<IApplicationResponse> {
        let params = new HttpParams().set('StockOrderQueryId', this.stockOrderId);
        return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.AUDIT_REPORT, null, null, params);
    }

    auditInfoModal() {

        const dialogReff = this.dialog.open(StockOrderViewModalComponent, {
            width: '1000px',
            data: {
                stockOrderModalDetails: this.auditReportDetailsData
            }, disableClose: true
        });
        dialogReff.afterClosed().subscribe(result => {
            if (result) return;
            this.dialog.closeAll();
            this.rxjsService.setDialogOpenProperty(false);
        });
    }
}

