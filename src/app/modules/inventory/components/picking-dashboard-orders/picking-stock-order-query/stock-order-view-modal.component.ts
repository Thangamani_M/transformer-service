import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ResponseMessageTypes, SnackbarService } from '@app/shared';

@Component({
    selector: 'app-stock-order-view-modal',
    templateUrl: './stock-order-view-modal.component.html',
})

export class StockOrderViewModalComponent implements OnInit {

    exportListDetails: any;
    dateFormat = 'MMM dd, yyyy';

    constructor(
        @Inject(MAT_DIALOG_DATA) public stockOrderModalData,
        private snackbarService: SnackbarService, private datePipe: DatePipe,
    ) {
        this.exportListDetails = stockOrderModalData.stockOrderModalDetails;

    }

    ngOnInit(): void {
    }

    exportList() {
        if (this.exportListDetails.length != 0) {
            let fileName = 'Audit Info Details' + this.datePipe.transform((new Date()), this.dateFormat) + '.csv';
            let columnNames = ["Stock Code", "Audit Date and Time", "Audit Action", "Field Name", "AuditIs", "AuditWas", "User"];
            let header = columnNames.join(',');
            let csv = header;
            csv += '\r\n';
            this.exportListDetails.map(c => {
                csv += [c['stockCode'], c['createdDate'], c['actions'], c['fieldName'], c['currentComment'], c['previousComment'], c['userName']].join(',');
                csv += '\r\n';
            })
            var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
            var link = document.createElement("a");
            if (link.download !== undefined) {
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", fileName);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
        else {
            this.snackbarService.openSnackbar('No records found', ResponseMessageTypes.WARNING);
        }
    }
}