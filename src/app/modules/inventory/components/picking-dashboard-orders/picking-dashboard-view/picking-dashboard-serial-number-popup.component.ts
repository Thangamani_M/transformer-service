import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RxjsService } from '@app/shared';


@Component({
    selector: 'app-picking-dashboard-serial-number-popup',
    templateUrl: './picking-dashboard-serial-number-popup.component.html',
})

export class PickingDashboardSerialNumberPoupComponent implements OnInit {

    modalDetails: any;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data,
        private rxjsService: RxjsService,
    ) {

        this.modalDetails = data.serialNumberDetails;
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
    }
}