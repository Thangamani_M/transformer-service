import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models/others-module-models';
import { select, Store } from '@ngrx/store';
import { StockOrderViewModalComponent } from '../picking-stock-order-query/stock-order-view-modal.component';
import { PickingDashboardSerialNumberPoupComponent } from './picking-dashboard-serial-number-popup.component';

@Component({
    selector: 'app-picking-dashboard-view',
    templateUrl: './picking-dashboard-view.component.html',
    // styleUrls: ['./picking-dashboard-view.component.scss']
})
export class PickingDashboardViewComponent {

    stockOrderId: any;
    getDetailsData: any;
    getStockDetailsData: any;
    stockOrderType: any;
    auditReportDetailsData: any;
    roleName: any;
    userData: UserLogin;
    tabIndex: any;
    status: any;
    header: string = '';
    tabsNumber: Number;
    selectedTabIndex = 0;
    packerJobId;
    statusName;
    isShowQuery:boolean=false;
    cols = [
        { field: 'stock', header: 'Stock Code' },
        { field: 'stockDescription', header: 'Stock Description' },
        { field: 'quantity', header: 'Qty' }
    ];

    constructor(
        private activatedRoute: ActivatedRoute, private router: Router,
        private store: Store<AppState>, private dialog: MatDialog,private snackbarService:SnackbarService,
        private rxjsService: RxjsService, private crudService: CrudService) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });
        
        this.roleName = this.userData.roleName;
        // this.roleName = "Technical Manager";
        // roleName: "Inventory Manager" "Technical Manager"
        this.stockOrderId = this.activatedRoute.snapshot.queryParams.id;
        this.stockOrderType = this.activatedRoute.snapshot.queryParams.type;
        this.header = this.activatedRoute.snapshot.queryParams.header;
        this.tabsNumber = this.activatedRoute.snapshot.queryParams.tab;
        this.packerJobId = this.activatedRoute.snapshot.queryParams.packerJobId;
        this.statusName = this.activatedRoute.snapshot.queryParams.status;
        // this.stockOrderId = "85A614D1-DDFC-42C0-96C3-29CD1595ED63";
        // this.stockOrderType = "Technician";
        // 85A614D1-DDFC-42C0-96C3-29CD1595ED63&OrderType=Technician
    }

    ngOnInit() {
         //this.stockOrderType Customer Technician
        if (this.stockOrderId) {

            var params1; var params;
            params1 = new HttpParams().set('StockOrderId', this.stockOrderId).set('OrderType', this.stockOrderType);
            params =  new HttpParams().set('OrderType', this.stockOrderType);

            if(this.packerJobId && (this.stockOrderType=='Customer' || this.stockOrderType=='Technician')){
                if(this.tabsNumber==1 || this.tabsNumber==2)
                   params1 = new HttpParams().set('packerJobId', this.packerJobId).set('StockOrderId', this.stockOrderId).set('OrderType', this.stockOrderType);
               
                params =  new HttpParams().set('packerJobId', this.packerJobId).set('OrderType', this.stockOrderType);
            }
            this.crudService.get(
                ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.PICKING_DASHBOARD_VIEW, undefined, true, params1)
                .subscribe((response: IApplicationResponse) => {
                    if (response.isSuccess && response.statusCode === 200) {
                        this.getDetailsData = response.resources;
                        this.isShowQuery =  response.resources.isStockOrderQueryAdd;
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                });
            
                if(this.packerJobId && this.tabsNumber==1 && (this.stockOrderType=='Customer' || this.stockOrderType=='Technician')){
                    this.crudService.dropdown(
                        ModulesBasedApiSuffix.INVENTORY,
                        InventoryModuleApiSuffixModels.STOCK_ORDER_DETALS, params)
                        .subscribe((response: IApplicationResponse) => {
                            if (response.isSuccess && response.statusCode === 200) {
                                this.getStockDetailsData = response.resources;
                                this.rxjsService.setGlobalLoaderProperty(false);
                            }
                     });
                }            
     
            var stockId = new HttpParams().set('StockOrderId', this.stockOrderId);
            this.crudService.dropdown(
                ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.AUDIT_REPORT, stockId)
                .subscribe((response: IApplicationResponse) => {
                    if (response.isSuccess && response.statusCode === 200) {
                        this.auditReportDetailsData = response.resources;
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                });
        }
    }

    auditInfoModal() {

        const dialogReff = this.dialog.open(StockOrderViewModalComponent, {
            width: '1000px',
            data: {
                stockOrderModalDetails: this.auditReportDetailsData
            }, disableClose: true
        });
        dialogReff.afterClosed().subscribe(result => {
            if (result) return;
            this.dialog.closeAll();
            this.rxjsService.setDialogOpenProperty(false);
        });
    }

    stockDetailListDialog: boolean = false;
    modalDetails: any = {};

    onModalSerialPopupOpen(details: any, type: string) {

        this.stockDetailListDialog = false;
        this.modalDetails = {
            stockCode: details.stock,
            stockDescription: details.stockDescription,
            serialNumbers: type == 'COLLECTED BATCHES' ? details.collectedBatcheItemDetails : details.pickedBatchesItemDetails
        };
        this.stockDetailListDialog = true;

    }

    onTabClicked(tabIndex) {
        this.tabIndex = tabIndex.index;
    }

    navigateToList() {
        this.router.navigate(['/inventory/picking-dashboard-orders'], { queryParams: { tab: this.tabsNumber} });
    }

    addQueryPage() {
        // return;
        // if(!this.getStockDetailsData.tamId){
        //      this.snackbarService.openSnackbar("Technical Area Manager is not available for this stock order.", ResponseMessageTypes.WARNING);
        //     return;
        // }
        this.router.navigate(['/inventory', 'picking-dashboard-orders', 'stock-order-add-edit'], {
            queryParams: {
                id: this.stockOrderId,
                packerJobId:this.packerJobId,
                type: this.stockOrderType,
                viewType: this.roleName,
                screenType: 'picking',
                header: this.header,
                tab: this.tabsNumber
            }
        });
    }
    navigateToViewPage() {
     //http://localhost:4200//technical-management/tech-stock-order/approval/view?id=ac2c35cc-8cae-4a86-b924-cee0b5cfaf04&stockOrderApprovalId =&type=add-edit
        //this.router.navigateByUrl(editableObject['navigationURL']);
        this.router.navigate(['/technical-management/tech-stock-order/approval/view'], {
            queryParams: { type: 'add-edit', id: this.stockOrderId }
          });
    }

}
