import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, CrudType, currentComponentPageBasedPermissionsSelector$, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, SnackbarService } from "@app/shared";
import { MomentService } from "@app/shared/services/moment.service";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
import { PrimeNgTableVariablesModel } from '../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-radio-system-checker',
  templateUrl: './radio-system-checker.component.html',
  styleUrls: ['./radio-system-checker.component.scss']
})
export class RadioSystemCheckerWorkListComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties: any;
  row: any = {}
  loggedUser: any;
  receivingList: any = [];
  wareHouseList: any = [];
  radioSystemFilterForm: FormGroup;
  showFilterForm: boolean = false;
  startTodayDate = 0;
  filerParams;
  constructor(private momentService: MomentService, private router: Router, private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
    private snackbarService: SnackbarService) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Radio/System Checker Worklist",
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radio/System Checker Worklist', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Radio/System Checker Worklist',
            dataKey: 'radioSystemCheckerWorklistId',
            captionFontSize: '21px',
            enableBreadCrumb: true,
            enableAction: true,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enablePrintBtn: true,
            printTitle: 'Radio Removals',
            printSection: 'print-section0',
            enableRowDelete: false,
            enableFieldsSearch: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'worklistId', header: 'Worklist Id', width: '120px' },
              { field: 'status', header: 'Status', width: '130px' },
              { field: 'receivingId', header: 'Receiving Id', width: '130px' },
              { field: 'receivingBarcode', header: 'Receiving Barcode', width: '130px' },
              { field: 'referenceNumber', header: 'Reference Number', width: '130px' },
              { field: 'serialNumber', header: 'Serial Number', width: '120px' },
              { field: 'receivedBy', header: 'Received By', width: '150px' },
              { field: 'receivedDate', header: 'Received Date', width: '150px', type: 'date', isDateTime: true },
              { field: 'testedBy', header: 'Tested By', width: '150px' },
              { field: 'testedDate', header: 'Tested Date', width: '150px', type: 'date', isDateTime: true },
              { field: 'action', header: 'Actions', width: '130px' },

            ],
            enableMultiDeleteActionBtn: false,
            enableScanActionBtn: true,
            shouldShowFilterActionBtn: true,
            areCheckboxesRequired: false,
            isDateWithTimeRequired: true,
            enableExportCSV: false,
            apiSuffixModel: InventoryModuleApiSuffixModels.RADIO_SYSTEM_CHECKER_WORKLIST,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
          }
        ]
      }
    }
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.rxjsService.setGlobalLoaderProperty(false);
    this.createFilterForm();
    this.getRequiredListData();
    this.getReceivingDropDown();
    this.getWareHouseDropDown();
    this.getStatusDropDown();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio/System Checker"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  createFilterForm(): void {
    this.radioSystemFilterForm = new FormGroup({
      'receivingId': new FormControl(null),
      'receivingBarcode': new FormControl(null),
      'referenceNumber': new FormControl(null),
      'warehouse': new FormControl(null),
      'receivedBy': new FormControl(null),
      'receivedDate': new FormControl(null),
      'testedBy': new FormControl(null),
      'testedDate': new FormControl(null),
      'status': new FormControl(null),
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }
  getReceivingDropDown() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_RADIO_SYSTEM_CHECKER_RECEIVING, null, false,
      null).subscribe(data => {
        if (data.isSuccess && data.statusCode == 200 && data.resources) {
          this.receivingList = data.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getWareHouseDropDown() {

    this.crudService.dropdown(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.loggedUser?.userId })).subscribe(data => {
        if (data.isSuccess && data.statusCode == 200 && data.resources) {
          this.wareHouseList = data.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getStatusDropDown() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_RADIO_SYSTEM_CHECKER_STATUS_LIST, null, false,
      null).subscribe(data => {
        if (data.isSuccess && data.statusCode == 200 && data.resources) {
          this.status = data.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let dealerModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    dealerModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;

    this.crudService.get(
      this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].moduleName,
      dealerModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((data: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.dataList = data.resources;
        this.totalRecords = data.totalCount;
      } else {
        this.dataList = null;
        this.totalRecords = 0;
      }
    })
  }
  submitFilter() {
    let wareHouseArr = []; let wareHouse;
    if (this.radioSystemFilterForm.get('warehouse').value) {
      this.radioSystemFilterForm.get('warehouse').value.forEach(element => {
        wareHouseArr.push(element.id);
      });
    }
    let receivedDate = this.radioSystemFilterForm.get('receivedDate').value;
    receivedDate = (receivedDate && receivedDate != null) ? this.momentService.toMoment(receivedDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    let testedDate = this.radioSystemFilterForm.get('testedDate').value;
    testedDate = (testedDate && testedDate != null) ? this.momentService.toMoment(testedDate).format('YYYY-MM-DDThh:mm:ss[Z]') : null;
    wareHouse = (wareHouseArr && wareHouseArr.length > 0) ? wareHouseArr.toString() : null;
    let filteredData = Object.assign({},
      { ReceivingIds: this.radioSystemFilterForm.get('receivingId').value ? this.radioSystemFilterForm.get('receivingId').value : '' },
      { ReceivingBarcodes: this.radioSystemFilterForm.get('receivingBarcode').value ? this.radioSystemFilterForm.get('receivingBarcode').value : '' },
      { ReferenceNumbers: this.radioSystemFilterForm.get('referenceNumber').value ? this.radioSystemFilterForm.get('referenceNumber').value : '' },
      { WarehouseIds: wareHouse ? wareHouse : '' },
      { ReceivedBys: this.radioSystemFilterForm.get('receivedBy').value ? this.radioSystemFilterForm.get('receivedBy').value : '' },
      { ReceivedDates: receivedDate ? receivedDate : '' },
      { TestedBys: this.radioSystemFilterForm.get('testedBy').value ? this.radioSystemFilterForm.get('testedBy').value : '' },
      { TestedDates: testedDate ? testedDate : '' },
      { Statuses: this.radioSystemFilterForm.get('status').value ? this.radioSystemFilterForm.get('status').value : '' }
    );
    Object.keys(filteredData).forEach(key => {
      if (filteredData[key] === "" || filteredData[key].length == 0) {
        delete filteredData[key]
      }
    });
    this.filerParams = Object.entries(filteredData).reduce((a, [k, v]) => (v == null ? a : (a[k] = v, a)), {}) //Filter all falsy values ( null, undefined )

    this.row['pageIndex'] = 0;
    this.getRequiredListData(this.row['pageIndex'], this.row['pageSize'], this.filerParams);
    this.showFilterForm = !this.showFilterForm;
  }
  resetForm() {
    this.showFilterForm = !this.showFilterForm;
    this.radioSystemFilterForm.reset();
    this.filerParams = null;
    this.getRequiredListData();
  }
  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.SCAN:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canScan) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.router.navigate(["/radio-removal/radio-system-checker/search"]);
        break;
      case CrudType.GET:

        if (this.filerParams)
          unknownVar = { ...unknownVar, ...this.filerParams }

        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.VIEW:
        this.router.navigate(["/radio-removal/radio-system-checker/view"], { queryParams: { id: row['radioSystemCheckerWorklistId'] } });
        break;
      case CrudType.FILTER:
        this.showFilterForm = !this.showFilterForm;
        break;
      default:
    }
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  cancel() {

  }

  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }
}
