
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { RadioSystemCheckerWorkListRoutingModule } from './radio-system-checker-routing.module';
import { RadioSystemCheckerSearchomponent } from './radio-system-checker-search.component';
import { RadioSystemCheckerUpdateComponent } from './radio-system-checker-update.component';
import { RadioSystemCheckerViewomponent } from './radio-system-checker-view.component';
import { RadioSystemCheckerWorkListComponent } from './radio-system-checker.component';

@NgModule({
    declarations: [RadioSystemCheckerWorkListComponent,RadioSystemCheckerViewomponent,RadioSystemCheckerUpdateComponent,RadioSystemCheckerSearchomponent ],
    imports: [
        CommonModule,
        MaterialModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        RadioSystemCheckerWorkListRoutingModule
    ],
    entryComponents: [],
    providers: []
})
export class RadioSystemCheckerModule { }
