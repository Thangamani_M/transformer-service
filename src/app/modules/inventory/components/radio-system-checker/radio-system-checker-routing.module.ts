import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RadioSystemCheckerSearchomponent } from './radio-system-checker-search.component';
import { RadioSystemCheckerUpdateComponent } from './radio-system-checker-update.component';
import { RadioSystemCheckerViewomponent } from './radio-system-checker-view.component';
import { RadioSystemCheckerWorkListComponent } from './radio-system-checker.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  { path: '', component: RadioSystemCheckerWorkListComponent, canActivate: [AuthGuard], data: { title: 'Radio System Checker Worklist' } },
  { path: 'view', component: RadioSystemCheckerViewomponent, canActivate: [AuthGuard], data: { title: 'Radio System Checker View' } },
  { path: 'update', component: RadioSystemCheckerUpdateComponent, canActivate: [AuthGuard], data: { title: 'Radio System Checker Update' } },
  { path: 'search', component: RadioSystemCheckerSearchomponent, canActivate: [AuthGuard], data: { title: 'Radio System Checker Search' } }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],

})
export class RadioSystemCheckerWorkListRoutingModule { }
