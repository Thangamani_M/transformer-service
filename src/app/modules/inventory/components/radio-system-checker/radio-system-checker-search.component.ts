import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";

@Component({
    selector: 'app-radio-system-checker-search',
    templateUrl: './radio-system-checker-search.component.html',
    styleUrls: ['./radio-system-checker.component.scss']
})
export class RadioSystemCheckerSearchomponent {
    primengTableConfigProperties: any;
    loggedUser: any;
    id;
    rsDetails;
    radioSystemCheckerForm: FormGroup;
    serialNumberList=[];
    filteredStockCodes:any=[];
    selectedTabIndex:any=0;
    label:string='';
    isLoading: boolean = false;

    constructor(private activatedRoute: ActivatedRoute, private router: Router,
        private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: "Radio/System Checker Worklist",
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radio/System Checker WorkList', relativeRouterUrl: '/radio-removal/radio-system-checker' }, { displayName: 'Radio/System Checker Search', relativeRouterUrl: '' }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: "Radio/System Checker Worklist",
                        dataKey: "rtrRequestId",
                        enableBreadCrumb: true,
                        enableAction: true,
                        selectedTabIndex: 0,
                    }
                ]
            }
        }
        this.id = this.activatedRoute.snapshot.queryParams.id;
    }
    ngOnInit() {
        this.createForm();
        this.rxjsService.setGlobalLoaderProperty(false);
    }
    createForm(): void {
        this.radioSystemCheckerForm = new FormGroup({
            'stockCode': new FormControl(null),
            'stockDescription': new FormControl(null),
            'search': new FormControl(null),
            'itemId': new FormControl(null),
        });
    }

    scanSerialNumber() {
        let serialNumber = this.radioSystemCheckerForm.get('search').value;
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.RADIO_SYSTEM_CHECKER_SERIAL_NUMBER,
            undefined,
            false, prepareRequiredHttpParams({serialNumber:serialNumber})
          ).subscribe((data: IApplicationResponse) => {
              if(data.isSuccess && data.statusCode==200 && data.resources){
                  this.serialNumberList = data.resources;
              }
              this.rxjsService.setGlobalLoaderProperty(false);
          });
    }
    onChangeCode(value,type) {
      let stockCode =  type =='stock-code' ?  {stockCode:value} : {stockDescription:value};
      if(value.length > 1){
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.RADIO_SYSTEM_CHECKER_STOCKCODE_LIST,
            undefined,
            false, prepareRequiredHttpParams(stockCode)
          ).subscribe((data: IApplicationResponse) => {
              if(data.isSuccess && data.statusCode==200 && data.resources){
                  this.filteredStockCodes = data.resources;

              }
              this.rxjsService.setGlobalLoaderProperty(false);
          });
      }
    }
    onStatucCodeSelected(value,type){
        if(type=='stock-code'){
            let filter = this.filteredStockCodes.filter(x=>x.stockCode==value);
            this.radioSystemCheckerForm.get('stockDescription').setValue(filter[0].stockDescription);
           this.radioSystemCheckerForm.get('itemId').setValue(filter[0].itemId);
       }
        else if(type=='stock-description'){
            let filter = this.filteredStockCodes.filter(x=>x.stockDescription==value);
           this.radioSystemCheckerForm.get('stockCode').setValue(filter[0].stockCode);
           this.radioSystemCheckerForm.get('itemId').setValue(filter[0].itemId);
        }
    }
    search() {
        if(this.radioSystemCheckerForm.get('itemId').value){
            let itemId = this.radioSystemCheckerForm.get('itemId').value;
            this.crudService.get(
                ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.RADIO_SYSTEM_CHECKER_SERIAL_NUMBER,
                undefined,
                false, prepareRequiredHttpParams({ItemId:itemId})
              ).subscribe((data: IApplicationResponse) => {
                  if(data.isSuccess && data.statusCode==200 && data.resources){
                      this.serialNumberList = data.resources;
                      if(this.serialNumberList && this.serialNumberList.length >0){
                        this.label = this.serialNumberList[0].isNotSerialized ? 'WorkList Number' : 'Serial Number';
                      }
                  }
                  this.rxjsService.setGlobalLoaderProperty(false);
              });
        }
    }
    onBreadCrumbClick(breadCrumbItem: object): void {
        if (breadCrumbItem.hasOwnProperty('queryParams')) {
          this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
            { queryParams: breadCrumbItem['queryParams']} )
        }
        else {
          this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
        }
    }
    onChangeSearchResult(item){
         this.router.navigate(["/inventory/radio-system-checker/view"], { queryParams: { id: item.radioSystemCheckerWorklistId } });
    }
    cancel() {

    }
}
