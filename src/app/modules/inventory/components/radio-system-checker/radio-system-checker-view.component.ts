import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, ModulesBasedApiSuffix, CrudType, RxjsService, currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";
import { combineLatest } from "rxjs";
@Component({
  selector: 'app-radio-system-checker-view',
  templateUrl: './radio-system-checker-view.component.html',
  styleUrls: ['./radio-system-checker.component.scss']
})
export class RadioSystemCheckerViewomponent {
  primengTableConfigProperties: any;
  loggedUser: any;
  RadioSystemCheckerviewDetail: any;
  id;
  rsDetails;
  isSerialized: boolean = false;
  selectedTabIndex: any = 0;
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
    private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>,
    private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "View Radio/System Checker",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radio/System Checker WorkList', relativeRouterUrl: '/radio-removal/radio-system-checker' }, { displayName: 'View Radio/System Checker', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: "Radio/System Checker",
            dataKey: "rtrRequestId",
            enableBreadCrumb: true,
            enableAction: true,
            selectedTabIndex: 0,
            enableEditActionBtn: true,
          }
        ]
      }
    }
    this.id = this.activatedRoute.snapshot.queryParams.id;
  }
  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredDetails();
  }

  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0]["Radio/System Checker"]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_SYSTEM_CHECKER_WORKLIST_DETAILS, this.id, false,
      null).subscribe(data => {
        if (data.isSuccess && data.statusCode == 200 && data.resources) {
          this.rsDetails = data.resources;
          this.onShowValue(data.resources);
          if (this.rsDetails && this.rsDetails.items && this.rsDetails.items.length > 0) {
            let filter = this.rsDetails.items.filter(x => x.isNotSerialized == false);
            if (filter && filter.length > 0) {
              this.isSerialized = true;
            }
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  onShowValue(response?: any) {
    this.RadioSystemCheckerviewDetail = [
      { name: 'WorkList ID', value: response?.radioSystemCheckerWorklistNumber },
      { name: 'Receiving ID', value: response?.batchNumber },
      { name: 'Receiving Bardcode', value: response?.barcodeNumber },
      { name: 'Reference Number', value: response?.orderNumber },
      { name: 'Receiving Clerk', value: response?.displayName },
      { name: 'Received Date', value: response?.orderReceiptDate },
      { name: 'Warehouse', value: response?.warehouseName },
      { name: 'Tested By', value: response?.testedby },
      { name: 'Tested Date', value: response?.testedDate },
    ]

  }
  onCRUDRequested(type: CrudType | string, row?: object, otherParams?: any): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[0].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }

        this.edit()
        break;
    }
  }
  edit() {
    this.router.navigate(["/radio-removal/radio-system-checker/update"], { queryParams: { id: this.id } });
  }
}
