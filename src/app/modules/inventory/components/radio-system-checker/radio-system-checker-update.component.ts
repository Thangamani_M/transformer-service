import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "@app/reducers";
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from "@app/shared";
import { InventoryModuleApiSuffixModels } from "@modules/inventory";
import { loggedInUserData } from "@modules/others";
import { UserLogin } from "@modules/others/models";
import { select, Store } from "@ngrx/store";

@Component({
    selector: 'app-radio-system-checker-update',
    templateUrl: './radio-system-checker-update.component.html',
    styleUrls: ['./radio-system-checker.component.scss']
})
export class RadioSystemCheckerUpdateComponent {
    primengTableConfigProperties: any;
    loggedUser: any;
    id;
    rsDetails;
    isSerialized: boolean = false;
    isUpdate: boolean = true;
    actionList: any = [];
    statuList: any = [];
    radioSystemCheckerForm: FormGroup;
    selectedTabIndex:any=0;
    constructor(private snackbarService: SnackbarService,private activatedRoute: ActivatedRoute, private router: Router, private formBuilder: FormBuilder,
        private rxjsService: RxjsService, private crudService: CrudService, private store: Store<AppState>) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.loggedUser = userData;
        });
        this.primengTableConfigProperties = {
            tableCaption: "Update Radio/System Checker",
            selectedTabIndex: 0,
            breadCrumbItems: [{ displayName: 'Radio Removal', relativeRouterUrl: '' }, { displayName: 'Radio/System Checker WorkList', relativeRouterUrl: '/radio-removal/radio-system-checker' }, { displayName: 'Update Radio/System Checker', relativeRouterUrl: '' }],
            tableComponentConfigs: {
                tabsList: [
                    {
                        caption: "Radio/System Checker",
                        dataKey: "rtrRequestId",
                        enableBreadCrumb: true,
                        enableAction: true,
                        selectedTabIndex:0
                    }
                ]
            }
        }
        this.id = this.activatedRoute.snapshot.queryParams.id;
    }
    ngOnInit(): void {
        this.createRFRCreationForm();
        this.getRequiredDetails();
        this.getActionDropDown();
        this.getStatusList();
    }
    createRFRCreationForm(): void {
        // let rfrModel = new RFRFormCreationModel();
        //create form controls dynamically from model class
        this.radioSystemCheckerForm = this.formBuilder.group({
            radioSystemCheckerDTO: this.formBuilder.array([])
        });
        this.radioSystemCheckerForm.addControl('quantity', new FormControl());
        this.radioSystemCheckerForm.addControl('comments', new FormControl());
        this.radioSystemCheckerForm.addControl('radioSystemCheckerWorklistActionId', new FormControl());
        this.radioSystemCheckerForm.addControl('radioSystemCheckerWorklistItemStatusId', new FormControl());
        this.radioSystemCheckerForm.addControl('radioSystemCheckerWorklistItemStatusName', new FormControl());
    }
    get getItemsArray(): FormArray {
        if (this.radioSystemCheckerForm !== undefined) {
            return (<FormArray>this.radioSystemCheckerForm.get('radioSystemCheckerDTO'));
        }
    }
    getActionDropDown() {
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.UX_RADIO_SYSTEM_CHECKER_ACTION, null, false,
            null).subscribe(data => {
                if (data.isSuccess && data.statusCode == 200 && data.resources) {
                    this.actionList = data.resources;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    getStatusList() {
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.UX_RADIO_SYSTEM_CHECKER_STATUS_LIST, null, false,
            null).subscribe(data => {
                if (data.isSuccess && data.statusCode == 200 && data.resources) {
                    this.statuList = data.resources;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    getRequiredDetails() {
        this.crudService.get(
            ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.RADIO_SYSTEM_CHECKER_WORKLIST_DETAILS, this.id, false,
            null).subscribe(data => {
                if (data.isSuccess && data.statusCode == 200 && data.resources) {
                    this.rsDetails = data.resources;
                    if (this.rsDetails && this.rsDetails.items && this.rsDetails.items.length > 0) {
                       let updateFilter = this.rsDetails.items.filter(x => x.radioSystemCheckerWorklistItemStatusName=="For Testing");
                         this.isUpdate = (updateFilter && updateFilter.length > 0) ? true : false;

                        let filter = this.rsDetails.items.filter(x => x.isNotSerialized == false);
                        if (filter && filter.length > 0) {
                            this.isSerialized = true;
                        }
                        this.addItem(this.rsDetails.items);
                    }
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
    }
    addSingleItem() {
       // this.rsDetails.items
       let formvalue = this.radioSystemCheckerForm.value;
       let quantity=formvalue.quantity;
       this.getItemsArray.value.forEach(element => {
           quantity = parseInt(quantity) + parseInt(element.quantity);
       });
       if(this.rsDetails && this.rsDetails.items.length >0){
        if(parseInt(quantity) > parseInt(this.rsDetails.items[0].receivedQuantity)){
            this.snackbarService.openSnackbar("Quantity Total Should be less than equal to ReceivedQuantity", ResponseMessageTypes.WARNING);
             return;
        }
       }
        let element = this.rsDetails.items[0];
            let item = this.formBuilder.group({
                radioSystemCheckerWorklistItemDetailId: element.radioSystemCheckerWorklistItemDetailId,
                radioSystemCheckerWorklistItemId: element.radioSystemCheckerWorklistItemId,
                quantity:formvalue.quantity,
                comments: formvalue.comments,
                radioSystemCheckerWorklistActionId: formvalue.radioSystemCheckerWorklistActionId,
                radioSystemCheckerWorklistItemStatusId: formvalue.radioSystemCheckerWorklistItemStatusId,
                modifiedUserId: this.loggedUser.userId,
                itemCode: element?.itemCode,
                radioSystemCheckerWorklistItemStatusName:formvalue.radioSystemCheckerWorklistItemStatusName,
                displayName: element?.displayName,
                serialNumber: element?.serialNumber
            })
            item = setRequiredValidator(item, ["radioSystemCheckerWorklistActionId"]);
            this.getItemsArray.push(item);
    }
    addItem(data) {
        data.forEach(element => {
            let item = this.formBuilder.group({
                radioSystemCheckerWorklistItemDetailId: element.radioSystemCheckerWorklistItemDetailId,
                radioSystemCheckerWorklistItemId: element.radioSystemCheckerWorklistItemId,
                quantity: element.isNotSerialized ?  element.quantity:element.receivedQuantity,
                comments: element.comments,
                radioSystemCheckerWorklistActionId: element.radioSystemCheckerWorklistActionId,
                radioSystemCheckerWorklistItemStatusId: element.radioSystemCheckerWorklistItemStatusId,
                modifiedUserId: this.loggedUser.userId,
                itemCode: element?.itemCode,
                radioSystemCheckerWorklistItemStatusName:element.radioSystemCheckerWorklistItemStatusName,
                displayName: element?.displayName,
                serialNumber: element?.serialNumber
            });
            if(!this.isUpdate){
                item.get('quantity').disable();
                item.get('radioSystemCheckerWorklistActionId').disable();
                item.get('radioSystemCheckerWorklistItemStatusId').disable();
                item.get('comments').disable();
            }
            item = setRequiredValidator(item, ["radioSystemCheckerWorklistActionId"]);
            this.getItemsArray.push(item);
        });
    }
    onActionChange(value, index) {
       if(value){
        let actionFilter = this.actionList.filter(x=>x.id==value); //based on action status has to be set this way
          if(index || index==0){
            if(actionFilter && actionFilter[0].displayName=="For Disposal"){
                let statusFilter = this.statuList.filter(x=>x.displayName=='To Be Disposal');
                this.getItemsArray.controls[index].get('radioSystemCheckerWorklistItemStatusId').setValue(statusFilter[0].id);
                this.getItemsArray.controls[index].get('radioSystemCheckerWorklistItemStatusName').setValue(statusFilter[0].displayName);
             }
             else  if(actionFilter && actionFilter[0].displayName=="Item Repaired" || actionFilter && actionFilter[0].displayName=="Item not Faulty"){
                 let statusFilter = this.statuList.filter(x=>x.displayName=='Item Tested');
                 this.getItemsArray.value[index].radioSystemCheckerWorklistItemStatusId=statusFilter[0].id;
                 this.getItemsArray.controls[index].get('radioSystemCheckerWorklistItemStatusId').setValue(statusFilter[0].id);
                 this.getItemsArray.controls[index].get('radioSystemCheckerWorklistItemStatusName').setValue(statusFilter[0].displayName);
             }
          }else{
            if(actionFilter && actionFilter[0].displayName=="For Disposal"){
                let statusFilter = this.statuList.filter(x=>x.displayName=='To Be Disposal');
                this.radioSystemCheckerForm.get('radioSystemCheckerWorklistItemStatusId').setValue(statusFilter[0].id);
                this.radioSystemCheckerForm.get('radioSystemCheckerWorklistItemStatusName').setValue(statusFilter[0].displayName);
             }
             else  if(actionFilter && actionFilter[0].displayName=="Item Repaired" || actionFilter && actionFilter[0].displayName=="Item not Faulty"){
                 let statusFilter = this.statuList.filter(x=>x.displayName=='Item Tested');
                 this.radioSystemCheckerForm.get('radioSystemCheckerWorklistItemStatusId').setValue(statusFilter[0].id);
                 this.radioSystemCheckerForm.get('radioSystemCheckerWorklistItemStatusName').setValue(statusFilter[0].displayName);
             }

          }
       }

    }
    onBreadCrumbClick(breadCrumbItem: object): void {
        if (breadCrumbItem.hasOwnProperty('queryParams')) {
          this.router.navigate([`${breadCrumbItem['relativeRouterUrl']}`],
            { queryParams: breadCrumbItem['queryParams']} )
        }
        else {
          this.router.navigateByUrl(`${breadCrumbItem['relativeRouterUrl']}`)
        }
    }
    onSubmit(isDraft) {
        if (this.radioSystemCheckerForm.invalid) {
            this.radioSystemCheckerForm.markAllAsTouched();
            return;
        }
         let finalObject = {
            radioSystemCheckerWorklistId: this.id,
            testedBy: this.loggedUser.userId,
            isDraft: isDraft,
            modifiedUserId: this.loggedUser.userId,
            itemdetails: this.radioSystemCheckerForm.value.radioSystemCheckerDTO
        }
        let api =  this.crudService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_SYSTEM_CHECKER_WORKLIST_SERIALIZED, finalObject) ;
        api.subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
                this.router.navigate(['inventory/radio-system-checker']);
            }
            this.rxjsService.setDialogOpenProperty(false);
        });
    }
}
