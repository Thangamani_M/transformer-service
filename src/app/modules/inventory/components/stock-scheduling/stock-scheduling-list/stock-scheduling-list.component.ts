import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ComponentProperties, CrudType, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ExecutePriceSchedulingComponent } from '.';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-stock-scheduling-list',
  templateUrl: './stock-scheduling-list.component.html',
  styleUrls: ['./stock-scheduling-list.component.scss']
})
export class StockSchedulingListComponent extends PrimeNgTableVariablesModel implements OnInit {

  loggedUser: UserLogin;
  componentProperties = new ComponentProperties();
  primengTableConfigProperties: any;
  tasksObservable;
  selectedSellingPriceItems = [];
  executeAllRecords: boolean = false;
  first: number = 0;
  seletedItemsIds: any = [];
  constructor(private store: Store<AppState>, private dataroute: ActivatedRoute, private crudService: CrudService, private dialogService: DialogService,  private router: Router,
    private rxjsService: RxjsService, private datePipe: DatePipe,) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: 'Stock Scheduling',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Table Selling Price', relativeRouterUrl: '' }, { displayName: 'Schedule Selling Price', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'SCHEDULE SELLING PRICE',
            dataKey: 'itemSellingPriceId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: true,
            disabled: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'itemSellingPriceCode', header: 'Selling Price ID', width: '150px' },
              { field: 'itemCode', header: 'Stock Code', width: '150px' },
              { field: 'leadGroupName', header: 'Lead Group', width: '140px' },
              { field: 'districtName', header: 'District', width: '100px' },
              { field: 'sellingPrice', header: 'Selling Price', width: '130px' },
              { field: 'costPrice', header: 'Cost Price', width: '100px' },
              { field: 'labourRate', header: 'Labour Rate', width: '120px' },
              { field: 'materialMarkup', header: 'Material Markup', width: '150px' },
              { field: 'consumableMarkup', header: 'Consumable Markup', width: '200px' },
              { field: 'laborComponent', header: 'Labour Component', width: '120px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableBreadCrumb: true,
            enableAddActionBtn: false,
            enableReloadBtn: true,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_SCHEDULING_SELLING_PRICE,
            moduleName: ModulesBasedApiSuffix.INVENTORY
          },
          {
            caption: 'SCHEDULED JOBS',
            dataKey: 'dealerId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            disabled: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'sellingPriceJobCode', header: 'Job ID', width: '100px' },
              { field: 'executionDate', header: 'Execution Date', width: '70px' },
              { field: 'userName', header: 'Scheduled By', width: '100px' },
              { field: 'createdDate', header: 'Created Date', width: '100px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableBreadCrumb: true,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_SCHEDULING_JOBS,
            moduleName: ModulesBasedApiSuffix.INVENTORY
          },
          {
            caption: 'ARCHIVE',
            dataKey: 'dealerBranchId',
            enableAction: true,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            disabled: false,
            enableRowDelete: false,
            enableFieldsSearch: false,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            columns: [
              { field: 'sellingPriceJobCode', header: 'Job ID', width: '100px' },
              { field: 'executionDate', header: 'Execution Date', width: '70px' },
              { field: 'userName', header: 'Scheduled By', width: '100px' },
              { field: 'createdDate', header: 'Created Date', width: '100px' }
            ],
            enableMultiDeleteActionBtn: false,
            enableBreadCrumb: true,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            enableStatusActiveAction: false,
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_SCHEDULING_JOBS,
            moduleName: ModulesBasedApiSuffix.INVENTORY
          }

        ]
      }
    }
    this.componentProperties = {
      tableCaption: "Stock Scheduling",
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'SCHEDULE SELLING PRICE',
            breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: "Stock Table Selling Price", relativeRouterUrl: '/inventory/stock-selling-price' }, { displayName: 'Schedule Selling Price' }],
            dataSource: new MatTableDataSource<any>([]),
            displayedColumns: ['select', 'Selling Price ID', 'Stock Code', 'Lead Group', 'District', 'Selling Price', 'Cost Price', 'Labour Rate', 'Material Markup'
              , 'Consumable Markup', 'Labour Component'],
            apiColumns: ['itemSellingPriceCode', 'itemCode', 'leadGroupName', 'districtName', 'sellingPrice', 'costPrice', 'labourRate', 'materialMarkup'
              , 'consumableMarkup', 'laborComponent'],
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_SCHEDULING_SELLING_PRICE,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
          },
          {
            caption: 'SCHEDULED JOBS',
            breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: "Stock Table Selling Price", relativeRouterUrl: '/inventory/stock-selling-price' }, { displayName: 'Scheduled Jobs' }],
            dataSource: new MatTableDataSource<any>([]),
            displayedColumns: ['select', 'Job ID', 'Execution Date', 'Scheduled By', 'Created Date'],
            apiColumns: ['sellingPriceJobCode', 'executionDate', 'userName', 'createdDate'],
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_SCHEDULING_JOBS,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            shouldShowDeleteActionBtn: true,
            shouldShowCreateActionBtn: false,
          },
          {
            caption: 'ARCHIVE',
            breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: "Stock Table Selling Price", relativeRouterUrl: '/inventory/stock-selling-price' }, { displayName: 'Archive' }],
            dataSource: new MatTableDataSource<any>([]),
            displayedColumns: ['select', 'Job ID', 'Execution Date', 'Scheduled By', 'Created Date'],
            apiColumns: ['sellingPriceJobCode', 'executionDate', 'userName', 'createdDate'],
            apiSuffixModel: InventoryModuleApiSuffixModels.STOCK_SCHEDULING_JOBS,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            shouldShowDeleteActionBtn: false,
            shouldShowCreateActionBtn: false,
          },
        ]
      }
    }
    this.dataroute.queryParams.subscribe((res) => {
      this.selectedTabIndex = res.tab ? +res.tab : this.selectedTabIndex;
      // mutate object's property in order to put these property under change detection supervision for table component
      this.componentProperties = { ...this.componentProperties, ...{ selectedTabIndex: this.selectedTabIndex } };
    });
  }

  ngOnInit() {
    this.selectAndAssignObservable();
  }

  selectAndAssignObservable(): void {
    if (this.selectedTabIndex == 0) {
      this.tasksObservable = this.getScheduleSellingPrice();
    } else if (this.selectedTabIndex == 1) {
      this.tasksObservable = this.getScheduledjobs();
    } else if (this.selectedTabIndex == 2) {
      this.tasksObservable = this.getArchive();
    }
    this.tasksObservable.pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          val.executionDate = this.datePipe.transform(val.executionDate, 'dd-MM-yyyy, h:mm:ss a');
          val.createdDate = this.datePipe.transform(val.createdDate, 'dd-MM-yyyy, h:mm:ss a');
          val.sellingPrice = 'R' + ' ' + val.sellingPrice;
          val.costPrice = 'R' + ' ' + val.costPrice;
          val.labourRate = 'R' + ' ' + val.labourRate;
          return val;
        })
      }
      return res;
    })).subscribe((res: any) => {
      if (res.isSuccess == true && res.statusCode == 200) {
        this.dataList = res.resources;
        this.totalRecords = res.totalCount;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  onTotalRecords(e) {
    this.executeAllRecords = e > 0 ? false : true;
  }

  getScheduleSellingPrice(pageIndex?: string, pageSize?: string, searchKey?: string): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_SCHEDULING_SELLING_PRICE, undefined, false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        search: searchKey ? searchKey : ''
      })
    );
  }

  getScheduledjobs(pageIndex?: string, pageSize?: string, searchKey?: string): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_SCHEDULING_JOBS, undefined, false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        search: searchKey ? searchKey : '',
        isArchive: false
      })
    );
  }

  getArchive(pageIndex?: string, pageSize?: string, searchKey?: string): Observable<any> {
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_SCHEDULING_JOBS, undefined, false,
      prepareGetRequestHttpParams(pageIndex, pageSize, {
        search: searchKey ? searchKey : '',
        isArchive: true
      })
    );
  }

  tabClick(e) {
    this.seletedItemsIds = [];
    this.selectedTabIndex = e?.index;
    if (this.selectedTabIndex == 0) {

    }
    this.selectAndAssignObservable();
  }
  onCRUDRequest(obj: object): void {
    switch (obj["type"]) {
      case CrudType.GET:
        switch (this.selectedTabIndex) {
          case 0:
            this.tasksObservable =
              this.getScheduleSellingPrice(
                obj['row']["pageIndex"].toString(),
                obj['row']["pageSize"].toString(), obj['row']['searchKey']);
            break;
          case 1:
            this.tasksObservable =
              this.getScheduledjobs(
                obj['row']["pageIndex"].toString(),
                obj['row']["pageSize"].toString(), obj['row']['searchKey']);
            break;
          case 2:
            this.tasksObservable =
              this.getArchive(
                obj['row']["pageIndex"].toString(),
                obj['row']["pageSize"].toString(), obj['row']['searchKey']);
            break;
        }
        break;
      case CrudType.EDIT:
        this.openAddEditPage(CrudType.EDIT, obj["row"], obj["index"]);
        break;
      case CrudType.CHECK:
        this.selectedSellingPriceItems = obj["selected"];
        break;
      case CrudType.RELOAD:

        this.selectAndAssignObservable();
        break;
    }
  }
  onCRUDRequesteds(type: CrudType | string, row?: any, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        if (this.selectedTabIndex == 0) {
          this.tasksObservable =
            this.getScheduleSellingPrice(
              row["pageIndex"].toString(),
              row["pageSize"].toString(), unknownVar['searchKey']);

        }
        else if (this.selectedTabIndex == 1) {
          this.tasksObservable =
            this.getScheduledjobs(
              row["pageIndex"].toString(),
              row["pageSize"].toString(), unknownVar['searchKey']);
        }
        else if (this.selectedTabIndex == 2) {

          this.tasksObservable =
            this.getArchive(
              row["pageIndex"].toString(),
              row["pageSize"].toString(), unknownVar['searchKey']);
        }
        this.tasksObservable.pipe(map((res: IApplicationResponse) => {
          if (res?.resources && this.selectedTabIndex != 0) {
            res?.resources?.forEach(val => {
              val.executionDate = this.datePipe.transform(val.executionDate, 'dd-MM-yyyy, h:mm:ss a');
              val.createdDate = this.datePipe.transform(val.createdDate, 'dd-MM-yyyy, h:mm:ss a');
              return val;
            })
          }
          return res;
        })).subscribe((res: any) => {
          if (res.isSuccess == true && res.statusCode == 200) {
            this.dataList = res.resources;
            this.totalRecords = res.totalCount;
          }
        });
        break;
      case CrudType.RELOAD:
        this.selectAndAssignObservable();
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.EDIT, row, unknownVar);
        break;
    }
  }
  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {
      case CrudType.VIEW:
        if (this.selectedTabIndex == 0) {
        } else if (this.selectedTabIndex == 1) {
          this.router.navigate(['inventory/stock-scheduling/scheduled-job-view'], { queryParams: { sellingPriceJobId: editableObject['sellingPriceJobId'], archive: false }, skipLocationChange: true });
        } else if (this.selectedTabIndex == 2) {
          this.router.navigate(['inventory/stock-scheduling/scheduled-job-view'], { queryParams: { sellingPriceJobId: editableObject['sellingPriceJobId'], archive: true }, skipLocationChange: true });
        }
        break;
      case CrudType.EDIT:
        if (this.selectedTabIndex == 0) {
        } else if (this.selectedTabIndex == 1) {
          this.router.navigate(['inventory/stock-scheduling/scheduled-job-view'], { queryParams: { sellingPriceJobId: editableObject['sellingPriceJobId'], archive: false }, skipLocationChange: true });
        } else if (this.selectedTabIndex == 2) {
          this.router.navigate(['inventory/stock-scheduling/scheduled-job-view'], { queryParams: { sellingPriceJobId: editableObject['sellingPriceJobId'], archive: true }, skipLocationChange: true });
        }
        break;
    }
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
    var seletedIds = [];
    this.selectedRows.forEach((element: any) => {
      seletedIds.push(element[this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].dataKey])
    });
    this.seletedItemsIds = seletedIds;
    this.selectedSellingPriceItems.length = (this.seletedItemsIds && this.seletedItemsIds.length > 0) ? this.seletedItemsIds.length : 0;
  }
  proceed() {
    let seletedItemsId = this.seletedItemsIds.toString();
    // const dialogReff = this.dialog.open(ExecutePriceSchedulingComponent, { width: '400px', disableClose: true, data: { selectedSellingPriceItems: seletedItemsId, flag: 'proceed' } });
    // dialogReff.afterClosed().subscribe(result => {
    //   this.selectedTabIndex = 1;
    //   this.selectAndAssignObservable();
    //   this.rxjsService.setDialogOpenProperty(false);
    // });
    const ref = this.dialogService.open(ExecutePriceSchedulingComponent, {
      header: 'Execute',
      showHeader: false,
      baseZIndex: 1000,
      width: '400px',
      // height: '300px',
      data: { selectedSellingPriceItems: seletedItemsId, flag: 'proceed' }
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.selectedTabIndex = 1;
        this.selectAndAssignObservable();
        this.rxjsService.setDialogOpenProperty(false);
      }
    });
  }
  proceedAll() {
    let seletedItemsId = this.seletedItemsIds.toString();
    // const dialogReff = this.dialog.open(ExecutePriceSchedulingComponent, { width: '400px', disableClose: true, data: { selectedSellingPriceItems: seletedItemsId, flag: 'proceed-all' } });
    // dialogReff.afterClosed().subscribe(result => {
    //   this.selectedTabIndex = 1;
    //   this.selectAndAssignObservable();
    //   this.rxjsService.setDialogOpenProperty(false);
    // });
    const ref = this.dialogService.open(ExecutePriceSchedulingComponent, {
      header: 'Execute',
      showHeader: false,
      baseZIndex: 1000,
      width: '400px',
      // height: '300px',
      data: { selectedSellingPriceItems: seletedItemsId, flag: 'proceed-all' } 
    });
    ref.onClose.subscribe((result) => {
      if (result) {
        this.selectedTabIndex = 1;
        this.selectAndAssignObservable();
        this.rxjsService.setDialogOpenProperty(false);
      }
    });
  }

  cancel() {
    this.router.navigate(['/inventory/stock-selling-price']);
  }
  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequesteds(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequesteds(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequesteds(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequesteds(e.type, e.data, e?.col);
    }
  }
}
