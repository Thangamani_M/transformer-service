import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { StockSellingPriceExecuteModel } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, ModulesBasedApiSuffix, setRequiredValidator } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { addHours } from 'date-fns';
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/api";

@Component({
  selector: 'app-execute-price-scheduling',
  templateUrl: './execute-price-scheduling-component.html',
  styleUrls: ['./stock-scheduling-list.component.scss']
})
export class ExecutePriceSchedulingComponent implements OnInit {
  minDate: Date;
  selectedSellingPriceItems: string;
  loggedUser: UserLogin;
  sellingPriceExecuteItems: any;// Array<StockSellingPriceExecuteModel>[];
  executeForm: FormGroup;
  startTodayDate=0;

  constructor( public config: DynamicDialogConfig, private httpService: CrudService,private ref: DynamicDialogRef,
    private store: Store<AppState>, private formBuilder: FormBuilder, private httpCancelService: HttpCancelService
    ) {
    this.selectedSellingPriceItems = config.data.selectedSellingPriceItems;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });
  }

  ngOnInit(): void {
    this.minDate = addHours(new Date(), 1);
    this.createMappingForm();
  }

  createMappingForm(): void {
    let sellingPriceExecuteModel = new StockSellingPriceExecuteModel();
    this.executeForm = this.formBuilder.group({});
    Object.keys(sellingPriceExecuteModel).forEach((key) => {
      this.executeForm.addControl(key, new FormControl(sellingPriceExecuteModel[key]));
    });
    this.executeForm = setRequiredValidator(this.executeForm, ["executionDate"]);
  }

  onSubmit() {
    if (this.executeForm.invalid) {
      return;
    }
    this.executeForm.value.executionDate = this.executeForm.value.executionDate.toLocaleString();

    if (this.config.data.flag == 'proceed') {
      this.executeForm.value.itemSellingPriceId = this.selectedSellingPriceItems;
    } else {
      this.executeForm.value.itemSellingPriceId = null;
    }
    this.executeForm.value.createdUserId = this.loggedUser.userId;
    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    this.httpService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_SCHEDULING_JOBS,
      this.executeForm.value).subscribe((response) => {
        if (response.isSuccess) {
          this.ref.close(false);
        }
      });
  }
  btnCloseClick() {
    this.ref.close(false);
  }
}
