import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockSchedulingListComponent } from '@inventory/components/stock-scheduling';
import { ScheduledJobViewComponent } from './scheduled-job';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: StockSchedulingListComponent, data: { title: 'Stock Scheduling' } },
      { path: 'scheduled-job-view', component: ScheduledJobViewComponent, data: { title: 'Scheduled Jobs View' } }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  
})

export class StockSchedulingRoutingModule { }
