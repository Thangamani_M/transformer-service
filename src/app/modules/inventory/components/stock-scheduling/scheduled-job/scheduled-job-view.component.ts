import { SelectionModel } from '@angular/cdk/collections';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, Sort } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { SheduleJobDetailList } from '@app/modules/inventory/models';
import { IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, SnackbarService, sortOrder } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
@Component({
  selector: 'app-scheduled-job-view',
  templateUrl: './scheduled-job-view.component.html'
})
export class ScheduledJobViewComponent implements OnInit {
  page: string;
  sellingPriceJobId: string;
  archive: string;
  params: any;
  scheduleJobDetails: any;
  scheduleJobDetailList: SheduleJobDetailList[];
  displayedColumns: string[] = ['select', 'itemSellingPriceCode', 'stockCode', 'stockDescription', 'sellingPrice', 'leadGroup', 'district', 'systemType', 'componenentGroup'];
  dataSource = new MatTableDataSource<SheduleJobDetailList>();
  limit: number = 25;
  pageIndex: number = 0;
  pageLimit: number[] = [5, 25, 50, 75, 100];
  search: string = "";
  detailsCount = 0;
  IsLoaded = false;
  selection = new SelectionModel<SheduleJobDetailList>(true, []);
  checkedItemIds: string[] = [];

  constructor(private activatedRoute: ActivatedRoute, private crudService: CrudService, private rxjsService: RxjsService,
    private snackbarService: SnackbarService) {
    this.sellingPriceJobId = this.activatedRoute.snapshot.queryParams.sellingPriceJobId;
    this.archive = this.activatedRoute.snapshot.queryParams.archive;
  }

  ngOnInit(): void {
    if (this.archive == "true")
      this.page = "Archive";
    else
      this.page = "Scheduled Jobs";
    this.getScheduledJobDetails();
  }

  getScheduledJobDetails() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_SCHEDULING_JOBS,
      this.sellingPriceJobId, true).subscribe((response: IApplicationResponse) => {
        this.detailsCount = response.totalCount;
        this.scheduleJobDetails = response.resources;
      });

    this.params = new HttpParams().set('sellingPriceJobId', this.sellingPriceJobId).set('isArchive', this.archive)
      .set('pageIndex', this.pageIndex.toString()).set('maximumRows', this.limit.toString()).set('search', this.search);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCK_SCHEDULING_JOBS_DETAILS,
      null, true, this.params).subscribe((response: IApplicationResponse) => {
        this.IsLoaded = true;
        this.detailsCount = response.totalCount;
        this.scheduleJobDetailList = response.resources;
        this.dataSource.data = response.resources;

      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  changePage(event) {
    this.limit = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.getScheduledJobDetails();
  }

  sortOrder(sort: Sort): void {
    this.dataSource = new MatTableDataSource(sortOrder(this.scheduleJobDetailList, sort));
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ? this.selection.clear() : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  private resetSelectedValues() {
    this.checkedItemIds = [];
    this.selection = new SelectionModel<SheduleJobDetailList>(true, []);
  }

  exportToExcel() {
    let excelList = [];
    this.selection.selected.forEach(element => {
      excelList.push(new SheduleJobDetailList(element));
    });
    if (excelList.length == 0) {
      this.snackbarService.openSnackbar("Select atleast one item to export", ResponseMessageTypes.WARNING);
      return;
    }
    let fileName = this.page.trim() + 'Details.csv';
    let columnNames = ["Selling price Id", "Stock Code", "Stock Description", "Selling Price", "Lead Group", "District", "SystemType",
      "ComponentGroup"];
    let header = columnNames.join(',');
    let csv = header;
    csv += '\r\n';
    excelList.map(c => {
      csv += [c["itemSellingPriceCode"], c["itemCode"], c["description"], c["sellingPrice"], c['leadGroupName'], c['districtName'], c['systemTypeName'], c['componentGroupName']].join(',');
      csv += '\r\n';
    })
    var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
    var link = document.createElement("a");
    if (link.download !== undefined) {
      var url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", fileName);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }

  Edit(id) { }
}
