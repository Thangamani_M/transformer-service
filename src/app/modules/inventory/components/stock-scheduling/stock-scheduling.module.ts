import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import {
    ExecutePriceSchedulingComponent, ScheduledJobViewComponent, StockSchedulingListComponent, StockSchedulingRoutingModule
} from '@inventory/components/stock-scheduling';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [StockSchedulingListComponent, ScheduledJobViewComponent, ExecutePriceSchedulingComponent],
  imports: [
    CommonModule,
    StockSchedulingRoutingModule,
    ReactiveFormsModule,
    FormsModule, LayoutModule,
    MaterialModule,
    SharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule
  ],
  entryComponents: [StockSchedulingListComponent, ExecutePriceSchedulingComponent]
})
export class StockSchedulingModule { }
