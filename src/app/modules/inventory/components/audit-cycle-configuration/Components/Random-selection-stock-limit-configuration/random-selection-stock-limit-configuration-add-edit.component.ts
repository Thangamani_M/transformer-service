import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { WarehouseRandomSelectionModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-random-selection-stock-limit-configuration-add-edit',
  templateUrl: './random-selection-stock-limit-configuration-add-edit.component.html'
})
export class RandomSelectionStockLimitConfigurationAddEditComponent implements OnInit {
  warehouseRandomSelectionForm: FormGroup;
  warehouseList = [];
  dropdownsAndData = [];
  warehouseStockLimitConfigId: any;
  warehouseStockLimitConfigDetails: any;
  isANumberWithZero = new CustomDirectiveConfig({ isANumberWithZero: true });
  divisionDuplicateError = '';
  minDate: Date;
  userData: UserLogin;
  primengTableConfigProperties;
  selectedTabIndex=0;
  constructor(private formBuilder: FormBuilder,
    private crudService: CrudService,private activatedRoute: ActivatedRoute,private rxjsService: RxjsService,private router: Router,private store: Store<AppState>,private changeDetectorRef: ChangeDetectorRef) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.warehouseStockLimitConfigId = this.activatedRoute.snapshot.queryParams.id;
    let title = this.warehouseStockLimitConfigId ? 'Update' :'Add';
    this.primengTableConfigProperties = {
      tableCaption:title+' Stock Limit Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Audit cycle configuration', relativeRouterUrl: '' },{ displayName: 'Stock Limit Config', relativeRouterUrl: '/inventory/audit-cycle-configuration' , queryParams: { tab: 0 }}],
      tableComponentConfigs: {
          tabsList: [
              {
                  enableAction: true,
                  enableEditActionBtn: false,
                  enableBreadCrumb: true,
              }
          ]
      }
    }
    if(this.warehouseStockLimitConfigId)
       this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Stock Limit Config', relativeRouterUrl: '/inventory/audit-cycle-configuration/random-selection-stock-limit-view',queryParams: { id: this.warehouseStockLimitConfigId } });

    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title+' Stock Limit Config', relativeRouterUrl: '', });
   
  }

  ngOnInit(): void {
    this.createWarehouseRandomSelectionForm();
    if (this.warehouseStockLimitConfigId) {
      this.dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.userData?.userId })),
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_LIMIT_CONFIG,
          this.warehouseStockLimitConfigId)
      ];
    } else {
      this.dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.userData?.userId }))
      ];
    }
    this.loadActionTypes(this.dropdownsAndData);
  }

  ngAfterViewChecked() {
    this.changeDetectorRef.detectChanges();
  }

  // Create form
  createWarehouseRandomSelectionForm(warehouseRandomSelectionModel?: WarehouseRandomSelectionModel) {
    let warehouseModel = new WarehouseRandomSelectionModel(warehouseRandomSelectionModel);
    this.warehouseRandomSelectionForm = this.formBuilder.group({});
    Object.keys(warehouseModel).forEach((key) => {
      this.warehouseRandomSelectionForm.addControl(key, new FormControl(warehouseModel[key]));
    });
    this.warehouseRandomSelectionForm = setRequiredValidator(this.warehouseRandomSelectionForm, ['warehouseId', 'randomStockLimit', 'systemGeneratedStockLimit', 'highRiskStockLimitPerItem']);
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              let respObj = resp.resources;
              if (Object.keys(respObj).length > 0) {
                for (let i = 0; i < respObj.length; i++) {
                  let temp = {};
                  temp['display'] = respObj[i].displayName;
                  temp['value'] = respObj[i].id;
                  this.warehouseList.push(temp);
                }
              }
              break;
            case 1:
              this.warehouseStockLimitConfigDetails = resp.resources;
              let warehouseModel = new WarehouseRandomSelectionModel(this.warehouseStockLimitConfigDetails);
              this.warehouseRandomSelectionForm.patchValue(warehouseModel);
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createWarehouseRandomSelectionConfig() {
    let submit$;
    let warehouseData: any;
    if (this.warehouseRandomSelectionForm.invalid) {
      this.warehouseRandomSelectionForm.markAllAsTouched();
      return;
    }
    let warehouseIds = this.warehouseRandomSelectionForm.get('warehouseId').value;
    if (warehouseIds.length == 0) {
      warehouseData = [];
    } else if (warehouseIds.length > 0 && !this.warehouseStockLimitConfigId) {
      warehouseData = [];
      warehouseIds.forEach(warehouse => {
        warehouseData.push({
          "randomStockLimit": +this.warehouseRandomSelectionForm.get('randomStockLimit').value,
          "systemGeneratedStockLimit": +this.warehouseRandomSelectionForm.get('systemGeneratedStockLimit').value,
          "highRiskStockLimitPerItem": +this.warehouseRandomSelectionForm.get('highRiskStockLimitPerItem').value,
          "warehouseId": warehouse,
          "createdUserId": this.userData.userId
        });
      });
    } else if (warehouseIds.length > 0 && this.warehouseStockLimitConfigId) {
      warehouseData = {
        "stockLimitConfigId": +this.warehouseStockLimitConfigId,
        "randomStockLimit": +this.warehouseRandomSelectionForm.get('randomStockLimit').value,
        "systemGeneratedStockLimit": +this.warehouseRandomSelectionForm.get('systemGeneratedStockLimit').value,
        "highRiskStockLimitPerItem": +this.warehouseRandomSelectionForm.get('highRiskStockLimitPerItem').value,
        "warehouseId": this.warehouseRandomSelectionForm.get('warehouseId').value.join(),
        "modifiedUserId": this.userData.userId
      }
    }
    if (this.warehouseStockLimitConfigId) {
      submit$ = this.crudService.update(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_LIMIT_CONFIG,
        warehouseData
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
    } else {
      submit$ = this.crudService.create(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_LIMIT_CONFIG,
        warehouseData
      ).pipe(tap(() => {
        this.rxjsService.setGlobalLoaderProperty(false);
      }))
    }
    submit$.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToList();
      }
    })
  }

  navigateToList() {
    this.router.navigate(['inventory/audit-cycle-configuration'], { queryParams: { tab: '0' }, skipLocationChange: true });
  }

  navigateToView() {
    this.router.navigate(['inventory/audit-cycle-configuration/random-selection-stock-limit-view'], { queryParams: { id: this.warehouseStockLimitConfigId }, skipLocationChange: true });
  }

}
