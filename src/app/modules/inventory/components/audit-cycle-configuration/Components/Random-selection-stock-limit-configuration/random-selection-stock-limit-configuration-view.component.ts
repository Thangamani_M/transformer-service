import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse,currentComponentPageBasedPermissionsSelector$, ModulesBasedApiSuffix,CrudType, RxjsService, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService, prepareDynamicTableTabsFromPermissions } from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { TECHNICAL_COMPONENT } from '@modules/technical-management/shared';
import { combineLatest } from 'rxjs';
import {  Store } from '@ngrx/store';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { AppState } from '@app/reducers';
@Component({
  selector: 'app-random-selection-stock-limit-configuration-view',
  templateUrl: './random-selection-stock-limit-configuration-view.component.html'
})
export class RandomSelectionStockLimitConfigurationViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  warehouseStockLimitConfigId: any;
  warehouseStockLimitConfigDetails: any;
  primengTableConfigProperties: any;
  RandomSelectionStockLimitviewDetail:any;
  constructor(private store: Store<AppState>,private rjxService: RxjsService,private snackbarService:SnackbarService,
    private crudService: CrudService,private activatedRoute: ActivatedRoute,private router: Router) {
      super();
    this.warehouseStockLimitConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption:'Stock Limit Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Audit cycle configuration', relativeRouterUrl: '' },{ displayName: 'Stock Limit Config', relativeRouterUrl: '/inventory/audit-cycle-configuration' , queryParams: { tab: 0 }}, { displayName: ' View Stock Limit Config', relativeRouterUrl: '', }],
      tableComponentConfigs: {
          tabsList: [
              {
                  enableAction: true,
                  enableEditActionBtn: true,
                  enableBreadCrumb: true,
              }
          ]
      }
  }
  this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
  this.RandomSelectionStockLimitviewDetail = [
    { name: 'Warehouse', value: ''},
    { name: 'Random Stock Limit', value: ''},
    { name: 'High Risk Stock Limit Per Item', value: ''},
    { name: 'System Generated Stock Limit', value: ''},
    { name: 'Created BY', value: ''},
    { name: 'Created Date', value: ''},
  ]
  }

  ngOnInit(): void {
    this.getAdminFeeDetails();
    this.combineLatestNgrxStoreData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][INVENTORY_COMPONENT.AUDIT_CYCLE_CONFIGURATION];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onShowValue(response?: any) {
    this.RandomSelectionStockLimitviewDetail = [
          { name: 'Warehouse', value: response?.warehouse},
          { name: 'Random Stock Limit', value: response?.randomStockLimit},
          { name: 'High Risk Stock Limit Per Item', value: response?.highRiskStockLimitPerItem},
          { name: 'System Generated Stock Limit', value: response?.systemGeneratedStockLimit},
          { name: 'Created BY', value: response?.createdBy},
          { name: 'Created Date', value: response.createdDate, isDate:true },
    ]
  }
  //Get Details 
  getAdminFeeDetails() {
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_LIMIT_CONFIG,
      this.warehouseStockLimitConfigId
    ).subscribe((response: IApplicationResponse) => {
      this.warehouseStockLimitConfigDetails = response.resources;
      this.onShowValue(response.resources);
      this.rjxService.setGlobalLoaderProperty(false);
    });
  }
  onCRUDRequested(type: CrudType | string): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEdit()
        break;
      case CrudType.GET:
        this.navigateToList();
        break;
    }
  }

  navigateToEdit() {
    if (this.warehouseStockLimitConfigId) {
      this.router.navigate(['inventory/audit-cycle-configuration/random-selection-stock-limit-add-edit'], { queryParams: { id: this.warehouseStockLimitConfigId }, skipLocationChange: true });
    }
  }

  navigateToList() {
    this.router.navigate(['inventory/audit-cycle-configuration'], { queryParams: { tab: '0' }, skipLocationChange: true });
  }

}
