import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, setRequiredValidator } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { HighRiskConfigurationUpdateModel, ImportantAllocationModel, ImportantAllocationTypeSubTypeModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-high-risk-stock-configuration-add-edit',
  templateUrl: './high-risk-stock-configuration-add-edit.component.html'
})
export class HighRiskStockConfigurationAddEditComponent implements OnInit {
  userData: UserLogin;
  highRiskConfigurationUpdateForm: FormGroup;
  dropdownsAndData = [];
  auditCycleHighRiskStockConfigId;
  warehouseList: any = [];
  allocationTypeList: any = [];
  highRiskConfigDetails;
  selectedTypeName = '';
  imporantAllocationTypeSubTypeForm: FormGroup;
  highRiskStockConfigurationForm: FormGroup;
  stockCount: any = 0;
  addedStockCount: any = 0;
  statusDropDown: any = [];
  typeExists: boolean = false;
  alertStockCountExceed: boolean = false;
  isANumberWithZero = new CustomDirectiveConfig({ isANumberWithZero: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  primengTableConfigProperties;
  selectedTabIndex=0;
  constructor(private formBuilder: FormBuilder,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,private router: Router,private store: Store<AppState>,private rxjsService: RxjsService) {
    this.auditCycleHighRiskStockConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    })
    this.statusDropDown = [
      { id: 'true', displayName: 'Active' },
      { id: 'false', displayName: 'InActive' }
    ];
    let title = this.auditCycleHighRiskStockConfigId ? 'Update' :'Add';
    this.primengTableConfigProperties = {
      tableCaption: title+' High Risk Stock Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Take', relativeRouterUrl: '' },{ displayName: 'Audit cycle configuration', relativeRouterUrl: '' },{ displayName: 'High Risk Stock Config', relativeRouterUrl: '/inventory/audit-cycle-configuration', queryParams: { tab: 1 } }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableEditActionBtn: false,
            enableBreadCrumb: true,
          }
        ]
      }
    }
    if(this.auditCycleHighRiskStockConfigId)
       this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View High Risk Stock Config', relativeRouterUrl: '/inventory/audit-cycle-configuration/high-risk-task-view',queryParams: { id: this.auditCycleHighRiskStockConfigId } });

    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title+' High Risk Stock Config', relativeRouterUrl: '', });
      
  }

  ngOnInit(): void {
    if (this.auditCycleHighRiskStockConfigId) {
      this.dropdownsAndData = [
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.AUDIT_CYCLE_HIGH_RISK_CONFIG_DETAILS,
          undefined, null, prepareGetRequestHttpParams(null, null, { AuditCycleHighRiskStockConfigId: this.auditCycleHighRiskStockConfigId }))
      ];
      this.createImporantAllocationUpdateForm();
    }
    else {
      this.createImporantAllocationTypeSubTypeForm();
      this.createImporantAllocationForm();
      this.imporantAllocationTypeSubTypeForm.get('type').valueChanges.subscribe(typeData => {
        this.selectedTypeName = '';
        this.selectedTypeName = this.allocationTypeList.find(v => v.id == typeData);
        this.selectedTypeName = this.selectedTypeName == undefined ? '' : this.selectedTypeName['displayName'];
      })
      this.dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.userData?.userId })),
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.AUDIT_CYCLE_CONFIG_TYPE, undefined, true)
      ];
      this.formControlChange();
    }
    this.loadActionTypes(this.dropdownsAndData);
  }

  formControlChange() {
    this.highRiskStockConfigurationForm.get('warehouse').valueChanges.subscribe((val) => {
      if (val != '') {
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.AUDIT_CYCLE_HIGH_RISK_STOCK_CONFIG_VALIDATE,
          undefined, null, prepareGetRequestHttpParams(null, null, { WarehouseId: val })).subscribe((response) => {

            this.rxjsService.setGlobalLoaderProperty(false);
            this.stockCount = response.resources == null ? 0 : response.resources;
          })
      }
    })
  }

  createImporantAllocationTypeSubTypeForm(importantAllocationTypeSubTypeModel?: ImportantAllocationTypeSubTypeModel) {
    let importantAllocModel = new ImportantAllocationTypeSubTypeModel(importantAllocationTypeSubTypeModel);
    this.imporantAllocationTypeSubTypeForm = this.formBuilder.group({});
    Object.keys(importantAllocModel).forEach((key) => {
      this.imporantAllocationTypeSubTypeForm.addControl(key, new FormControl());
    });
    this.imporantAllocationTypeSubTypeForm = setRequiredValidator(this.imporantAllocationTypeSubTypeForm, ['type', 'subType', 'description', 'stockCount', 'timeFrame', 'priority']);
  }

  createImporantAllocationForm(importantAllocationModel?: ImportantAllocationModel) {
    let importantAllocModel = new ImportantAllocationModel(importantAllocationModel);
    this.highRiskStockConfigurationForm = this.formBuilder.group({});
    let auditCycleConfigDetailsFormArray = this.formBuilder.array([]);
    Object.keys(importantAllocModel).forEach((key) => {
      if (typeof importantAllocModel[key] !== 'object') {
        this.highRiskStockConfigurationForm.addControl(key, new FormControl(importantAllocModel[key]));
      } else {
        this.highRiskStockConfigurationForm.addControl(key, auditCycleConfigDetailsFormArray);
      }
    });
    this.highRiskStockConfigurationForm = setRequiredValidator(this.highRiskStockConfigurationForm, ['warehouse']);
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          if (this.auditCycleHighRiskStockConfigId) {
            switch (ix) {
              case 0:
                this.highRiskConfigDetails = resp.resources;
                let highRiskConfigModel = new HighRiskConfigurationUpdateModel(this.highRiskConfigDetails);
                this.highRiskConfigurationUpdateForm.patchValue(highRiskConfigModel);
                this.highRiskConfigurationUpdateForm.controls['modifiedUserId'].patchValue(this.userData.userId)
                break;
            }
          } else if (!this.auditCycleHighRiskStockConfigId) {
            switch (ix) {
              case 0:
                let warehouseObj = resp.resources;
                if (Object.keys(warehouseObj).length > 0) {
                  for (let i = 0; i < warehouseObj.length; i++) {
                    let temp = {};
                    temp['display'] = warehouseObj[i].displayName;
                    temp['value'] = warehouseObj[i].id;
                    this.warehouseList.push(temp);
                  }
                }
                break;

              case 1:
                this.allocationTypeList = resp.resources;
                break;
            }
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  createImporantAllocationUpdateForm(highRiskConfigurationUpdateModel?: HighRiskConfigurationUpdateModel) {
    let highRiskConfigModel = new HighRiskConfigurationUpdateModel(highRiskConfigurationUpdateModel);
    this.highRiskConfigurationUpdateForm = this.formBuilder.group({});
    Object.keys(highRiskConfigModel).forEach((key) => {
      this.highRiskConfigurationUpdateForm.addControl(key, new FormControl(highRiskConfigModel[key]));
    });
  }

  get auditCycleConfigFormArray(): FormArray {
    if (this.highRiskStockConfigurationForm !== undefined) {
      return (<FormArray>this.highRiskStockConfigurationForm.get('auditCycleConfigDetails'));
    }
  }

  addTypeToTable() {
    if (this.imporantAllocationTypeSubTypeForm.get('type').value == ' ' || this.imporantAllocationTypeSubTypeForm.get('subType').value == ' ' || this.imporantAllocationTypeSubTypeForm.get('description').value == ' ' || this.imporantAllocationTypeSubTypeForm.get('stockCount').value == ' ' || this.imporantAllocationTypeSubTypeForm.get('timeFrame').value == ' ' || this.imporantAllocationTypeSubTypeForm.get('priority').value == ' ') {
      this.imporantAllocationTypeSubTypeForm.reset();
    }
    let auditCycleFormArray = this.auditCycleConfigFormArray;
    if (this.imporantAllocationTypeSubTypeForm.invalid) {
      this.imporantAllocationTypeSubTypeForm.markAllAsTouched();
      return;
    }
    let typeObj = auditCycleFormArray.value.find(v => v.auditCycleConfigSubType.toLocaleLowerCase() == this.imporantAllocationTypeSubTypeForm.get('subType').value.toLocaleLowerCase());
    if (!typeObj) {
      this.typeExists = false;
      let auditCycleObject = {
        auditCycleConfigTypeId: this.imporantAllocationTypeSubTypeForm.get('type').value,
        warehouseId: this.highRiskStockConfigurationForm.get('warehouse').value,
        auditCycleConfigSubType: this.imporantAllocationTypeSubTypeForm.get('subType').value,
        timeFrame: this.imporantAllocationTypeSubTypeForm.get('timeFrame').value,
        stockCount: this.imporantAllocationTypeSubTypeForm.get('stockCount').value,
        priority: this.imporantAllocationTypeSubTypeForm.get('priority').value,
        description: this.imporantAllocationTypeSubTypeForm.get('description').value,
        createdUserId: this.userData.userId,
        auditCycleConfigTypeName: this.selectedTypeName,
      }
      let addedTotalStockCount = this.addedStockCount;
      addedTotalStockCount = addedTotalStockCount + parseInt(this.imporantAllocationTypeSubTypeForm.get('stockCount').value);
      if (addedTotalStockCount <= this.stockCount) {
        this.addedStockCount = this.addedStockCount + parseInt(this.imporantAllocationTypeSubTypeForm.get('stockCount').value);
      }
      else {
        this.alertStockCountExceed = true;
        return;
      }
      let auditCycleFormGroup = this.formBuilder.group(auditCycleObject);
      auditCycleFormArray.push(auditCycleFormGroup);
      // addedStockCount
      this.imporantAllocationTypeSubTypeForm.patchValue({
        type: ' ',
        subType: ' ',
        description: ' ',
        stockCount: ' ',
        timeFrame: ' ',
        priority: ' '
      });
      this.selectedTypeName = '';
    }
    else {
      this.typeExists = true;
    }
  }

  deleteImportantAllocation(index) {
    if (index > -1) {
      this.addedStockCount = this.addedStockCount - parseInt(this.auditCycleConfigFormArray.value[index].stockCount)
      this.auditCycleConfigFormArray.removeAt(index);
    }
  }

  createImportantAllocation() {
    if (this.highRiskStockConfigurationForm.invalid) {
      this.highRiskStockConfigurationForm.markAllAsTouched();
      return;
    }
    let finalDataToSend = Object.assign([], this.highRiskStockConfigurationForm.value.auditCycleConfigDetails);

    finalDataToSend.forEach((value, index) => {

      Object.keys(value).forEach((key) => {

        if (key == 'auditCycleConfigTypeName') {
          delete finalDataToSend[index][key]
        }

      })
    })
    // let result = Array.from(list, o=> Object.fromEntries(Object.entries(o).filter((i) => i[1] != (null || ''))));
    this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.AUDIT_CYCLE_HIGH_RISK_STOCK_CONFIG,
      finalDataToSend
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToList();
      }
    })
  }

  updateHighRiskConfiguration() {
    this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.AUDIT_CYCLE_HIGH_RISK_STOCK_CONFIG,
      this.highRiskConfigurationUpdateForm.value
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToList();
      }
    })
  }

  navigateToList() {
    this.router.navigate(['inventory/audit-cycle-configuration'], { queryParams: { tab: '1' } });
  }

  navigateToView() {
    this.router.navigate(['inventory/audit-cycle-configuration/high-risk-task-view'], { queryParams: { id: this.auditCycleHighRiskStockConfigId } });
  }

}
