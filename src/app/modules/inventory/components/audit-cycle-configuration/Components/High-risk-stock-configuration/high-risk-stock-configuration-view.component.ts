import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService,currentComponentPageBasedPermissionsSelector$ } from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-high-risk-stock-configuration-view',
  templateUrl: './high-risk-stock-configuration-view.component.html'
})
export class HighRiskStockConfigurationViewComponent  extends PrimeNgTableVariablesModel implements OnInit {

  auditCycleHighRiskStockConfigId = '';
  HighRiskStockConfigurationviewDetail:any;
  primengTableConfigProperties: any
  constructor(private activatedRoute: ActivatedRoute,private snackbarService:SnackbarService,private store: Store<AppState>,
    private crudService: CrudService,private router: Router,private rjxService: RxjsService) {
      super();
    this.auditCycleHighRiskStockConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption:'View High Risk Stock Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' }, { displayName: 'Stock Take', relativeRouterUrl: '' },{ displayName: 'Audit cycle configuration', relativeRouterUrl: '' },{ displayName: 'High Risk Stock Config', relativeRouterUrl: '/inventory/audit-cycle-configuration', queryParams: { tab: 1 } }, { displayName: ' View High Risk Stock Config', relativeRouterUrl: '', }],
      tableComponentConfigs: {
        tabsList: [
          {
            enableAction: true,
            enableEditActionBtn: true,
            enableBreadCrumb: true,
          }
        ]
      }
  }
  this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    this.HighRiskStockConfigurationviewDetail = [
      { name: 'Warehouse', value: ''},
      { name: 'Time Frame (In Months)', value: ''},
      { name: 'Type', value: ''},
      { name: 'Description', value: ''},
      { name: 'Priority', value: ''},
      { name: 'Stock Count', value: ''},
      { name: 'Status', value: ''},
    ]
  }


  ngOnInit(): void {
    this.combineLatestNgrxStoreData();
    if (this.auditCycleHighRiskStockConfigId) {
      this.getallocationArrayById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.onShowValue(response.resources);
        }
        this.rjxService.setGlobalLoaderProperty(false);
      })
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.AUDIT_CYCLE_CONFIGURATION];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onCRUDRequested(type: CrudType | string): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEdit()
        break;
    }
  }
  onShowValue(response?: any) {
    this.HighRiskStockConfigurationviewDetail = [
          { name: 'Warehouse', value: response?.warehouse},
          { name: 'Time Frame (In Months)', value: response?.timeFrame},
          { name: 'Type', value: response?.auditCycleConfigType},
          { name: 'Description', value: response?.description},
          { name: 'Priority', value: response?.priority},
          { name: 'Stock Count', value: response?.stockCount},
          { name: 'Status', value: response?.status, statusClass: response.cssClass },
    ]
  }

  getallocationArrayById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.AUDIT_CYCLE_HIGH_RISK_CONFIG_DETAILS,
      undefined, null, prepareGetRequestHttpParams(null, null, { AuditCycleHighRiskStockConfigId: this.auditCycleHighRiskStockConfigId })
    );
  }

  navigateToEdit() {
    if (this.auditCycleHighRiskStockConfigId) {
      this.router.navigate(['inventory/audit-cycle-configuration/high-risk-task-add-edit'], { queryParams: { id: this.auditCycleHighRiskStockConfigId }, skipLocationChange: true });
    }
  }

}
