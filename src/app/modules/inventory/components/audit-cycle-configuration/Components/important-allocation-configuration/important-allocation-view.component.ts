import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService,currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels,INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { combineLatest, Observable } from 'rxjs';
import { CrudType } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { PrimeNgTableVariablesModel } from '../../../../../../shared/models/prime-ng-table-list-component-variables.model';
import { AppState } from '@app/reducers';
@Component({
  selector: 'app-important-allocation-view',
  templateUrl: './important-allocation-view.component.html'
})
export class ImportantAllocationViewComponent extends PrimeNgTableVariablesModel implements OnInit {
  auditCycleAllocationConfigId = '';
  primengTableConfigProperties: any
  ImportantAllocationviewDetail:any;
  constructor(private activatedRoute: ActivatedRoute,private store: Store<AppState>,private snackbarService:SnackbarService,
    private crudService: CrudService,private router: Router,private rjxService: RxjsService) {
    super();
    this.auditCycleAllocationConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.primengTableConfigProperties = {
      tableCaption:'Importance Allocation Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Audit cycle configuration', relativeRouterUrl: '' },{ displayName: 'Importance Allocation Config', relativeRouterUrl: '/inventory/audit-cycle-configuration', queryParams: { tab: 2 } }, { displayName: ' View Importance Allocation Config', relativeRouterUrl: '', }],
      tableComponentConfigs: {
          tabsList: [
              {
                  enableAction: true,
                  enableEditActionBtn: true,
                  enableBreadCrumb: true,
              }
          ]
      }
  }
  this.primengTableConfigProperties.breadCrumbItems.displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    this.ImportantAllocationviewDetail = [
      { name: 'Warehouse', value: ''},
      { name: 'Time Frame (In Months)', value: ''},
      { name: 'Type', value: ''},
      { name: 'Sub Type', value: ''},
      { name: 'Description', value: ''},
      { name: 'Stock Count', value: ''},
      { name: 'Priority', value: ''},
      { name: 'Status', value: ''},
]
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData();
    if (this.auditCycleAllocationConfigId) {
      this.getallocationArrayById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess == true && response.statusCode == 200) {
          this.onShowValue(response.resources);
        }
        this.rjxService.setGlobalLoaderProperty(false);
      })
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.AUDIT_CYCLE_CONFIGURATION];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onCRUDRequested(type: CrudType | string): void {
    switch (type) {
      case CrudType.EDIT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.navigateToEdit()
        break;
    }
  }
  onShowValue(response?: any) {
    this.ImportantAllocationviewDetail = [
      { name: 'Warehouse', value: response?.warehouse},
      { name: 'Time Frame (In Months)', value: response?.timeFrame},
      { name: 'Type', value: response?.auditCycleConfigType},
      { name: 'Sub Type', value: response?.auditCycleConfigSubType},
      { name: 'Description', value: response?.description},
      { name: 'Stock Count', value: response?.stockCount},
      { name: 'Priority', value: response?.priority},
      { name: 'Status', value: response?.status, statusClass: response.cssClass },
    ]
  }

  getallocationArrayById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ALLOCATION_CONFIG_DETAILS,
      undefined,
      false, prepareRequiredHttpParams({ AuditCycleAllocationConfigId: this.auditCycleAllocationConfigId })
    );
  }

  navigateToEdit() {
    if (this.auditCycleAllocationConfigId) {
      this.router.navigate(['inventory/audit-cycle-configuration/important-allocation-add-edit'], { queryParams: { id: this.auditCycleAllocationConfigId }, skipLocationChange: true });
    }
  }
}

