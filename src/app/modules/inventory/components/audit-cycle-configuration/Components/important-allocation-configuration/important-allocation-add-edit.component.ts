import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, RxjsService, setRequiredValidator } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { ImportantAllocationModel, ImportantAllocationTypeSubTypeModel, ImportantAllocationUpdateModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-important-allocation-add-edit',
  templateUrl: './important-allocation-add-edit.component.html'
})

export class ImportantAllocationAddEditComponent implements OnInit {
  auditCycleAllocationConfigId: any;
  imporantAllocationTypeSubTypeForm: FormGroup;
  imporantAllocationForm: FormGroup;
  imporantAllocationUpdateForm: FormGroup;
  userData: UserLogin;
  isANumberWithZero = new CustomDirectiveConfig({ isANumberWithZero: true });
  isAnAlphaNumericOnly = new CustomDirectiveConfig({ isAnAlphaNumericOnly: true });
  isAStringOnly = new CustomDirectiveConfig({ isAStringOnly: true });
  warehouseList: any = [];
  allocationTypeList: any = [];
  allocationSubTypeList: any = [];
  dropdownsAndData = [];
  selectedTypeName = '';
  importantAllocationDetails: any;
  typeExists = false;
  stockLimitForWarehpuse = 0;
  totalStockCount = 0;
  alertLocationClear = false;
  isWarningNotificationDialog = true;
  selectedTabIndex=0;
  primengTableConfigProperties;
  constructor(private formBuilder: FormBuilder,
    private crudService: CrudService,
    private activatedRoute: ActivatedRoute,private router: Router,private store: Store<AppState>,private rxjsService: RxjsService) {
    this.auditCycleAllocationConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    let title = this.auditCycleAllocationConfigId ? 'Update' :'Add';
    this.primengTableConfigProperties = {
      tableCaption: title+' Importance Allocation Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Stock Take', relativeRouterUrl: '' }, { displayName: 'Audit cycle configuration', relativeRouterUrl: '' },{ displayName: 'Importance Allocation Config', relativeRouterUrl: '/inventory/audit-cycle-configuration', queryParams: { tab: 2 } }],
      tableComponentConfigs: {
          tabsList: [
              {
                  enableAction: true,
                  enableEditActionBtn: false,
                  enableBreadCrumb: true,
              }
          ]
      }
    }
    if(this.auditCycleAllocationConfigId)
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Importance Allocation Config', relativeRouterUrl: '/inventory/audit-cycle-configuration/important-allocation-view',queryParams: { id: this.auditCycleAllocationConfigId } });

    this.primengTableConfigProperties.breadCrumbItems.push({ displayName: title+' Importance Allocation Config', relativeRouterUrl: '', });
  
  }

  ngOnInit(): void {
    if (this.auditCycleAllocationConfigId) {
      this.dropdownsAndData = [
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.AUDIT_CYCLE_CONFIG_SUB_TYPE, undefined, true),
        this.crudService.get(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.ALLOCATION_CONFIG_DETAILS,
          undefined,
          false, prepareRequiredHttpParams({ AuditCycleAllocationConfigId: this.auditCycleAllocationConfigId })
        )   
      ];
      this.createImporantAllocationUpdateForm();
    } else {
      this.createImporantAllocationTypeSubTypeForm();
      this.createImporantAllocationForm();
      this.imporantAllocationTypeSubTypeForm.get('type').valueChanges.subscribe(typeData => {
        this.selectedTypeName = '';
        this.selectedTypeName = this.allocationTypeList.find(v => v.id == typeData);
        this.selectedTypeName = this.selectedTypeName == undefined ? '' : this.selectedTypeName['displayName'];
        this.typeExists = false;
      });
      this.imporantAllocationForm.get('warehouse').valueChanges.subscribe(warehouseData => {
        this.stockLimitForWarehpuse = 0;
        this.totalStockCount = 0;
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.ALLOCATION_CONFIG_VALIDATE,
          undefined,
          false,
          prepareRequiredHttpParams({
            WarehouseId: warehouseData
          })).subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.statusCode == 200) {
              this.stockLimitForWarehpuse = +response.resources;
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          });
        this.imporantAllocationTypeSubTypeForm.reset({ emitEvent: false });
        this.selectedTypeName = '';
        this.typeExists = false;
        this.auditCycleConfigFormArray.clear();
      });
      this.imporantAllocationTypeSubTypeForm.get('stockCount').valueChanges.subscribe(typeData => {
        if (this.imporantAllocationTypeSubTypeForm.get('stockCount').value) {
          typeData = +typeData;
          this.totalStockCount = 0;
          this.auditCycleConfigFormArray.controls.forEach((control: FormGroup) => {
            this.totalStockCount += (+control.get('stockCount').value);
          });
          this.totalStockCount += typeData;
          if (this.totalStockCount > this.stockLimitForWarehpuse) {
            this.alertLocationClear = true;
            this.isWarningNotificationDialog = true;
          } else {
            this.isWarningNotificationDialog = false;
          }
        }
      });
      this.dropdownsAndData = [
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.UX_WAREHOUSE, prepareRequiredHttpParams({ userId: this.userData?.userId })),
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.AUDIT_CYCLE_CONFIG_TYPE, undefined, true),
          this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
            InventoryModuleApiSuffixModels.AUDIT_CYCLE_CONFIG_SUB_TYPE, undefined, true)
      ];
    }
    this.loadActionTypes(this.dropdownsAndData);
  }

  createImporantAllocationTypeSubTypeForm(importantAllocationTypeSubTypeModel?: ImportantAllocationTypeSubTypeModel) {
    let importantAllocModel = new ImportantAllocationTypeSubTypeModel(importantAllocationTypeSubTypeModel);
    this.imporantAllocationTypeSubTypeForm = this.formBuilder.group({});
    Object.keys(importantAllocModel).forEach((key) => {
      this.imporantAllocationTypeSubTypeForm.addControl(key, new FormControl());
    });
    this.imporantAllocationTypeSubTypeForm = setRequiredValidator(this.imporantAllocationTypeSubTypeForm, ['type', 'subType', 'description', 'timeFrame', 'priority']);
    this.imporantAllocationTypeSubTypeForm.controls['stockCount'].setValidators(Validators.compose([Validators.min(1), Validators.required]));
  }

  createImporantAllocationForm(importantAllocationModel?: ImportantAllocationModel) {
    let importantAllocModel = new ImportantAllocationModel(importantAllocationModel);
    this.imporantAllocationForm = this.formBuilder.group({});
    let auditCycleConfigDetailsFormArray = this.formBuilder.array([]);
    Object.keys(importantAllocModel).forEach((key) => {
      if (typeof importantAllocModel[key] !== 'object') {
        this.imporantAllocationForm.addControl(key, new FormControl(importantAllocModel[key]));
      } else {
        this.imporantAllocationForm.addControl(key, auditCycleConfigDetailsFormArray);
      }
    });
    this.imporantAllocationForm = setRequiredValidator(this.imporantAllocationForm, ['warehouse']);
  }

  createImporantAllocationUpdateForm(importantAllocationUpdateModel?: ImportantAllocationUpdateModel) {
    let importantAllocModel = new ImportantAllocationUpdateModel(importantAllocationUpdateModel);
    this.imporantAllocationUpdateForm = this.formBuilder.group({});
    Object.keys(importantAllocModel).forEach((key) => {
      this.imporantAllocationUpdateForm.addControl(key, new FormControl(importantAllocModel[key]));
    });
    this.imporantAllocationUpdateForm = setRequiredValidator(this.imporantAllocationUpdateForm, ['auditCycleConfigSubType', 'description', 'timeFrame', 'stockCount', 'priority']);
  }

  loadActionTypes(dropdownsAndData) {
    forkJoin(dropdownsAndData).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          if (this.auditCycleAllocationConfigId) {
            switch (ix) {
              case 0:
                this.allocationSubTypeList = resp.resources;
                break;
              case 1:
                this.importantAllocationDetails = resp.resources;
                this.importantAllocationDetails.auditCycleConfigSubType = this.importantAllocationDetails.auditCycleConfigsubTypeId;
                let importantAllocationModel = new ImportantAllocationUpdateModel(this.importantAllocationDetails);
                this.imporantAllocationUpdateForm.patchValue(importantAllocationModel);
                break;
            }
          } else if (!this.auditCycleAllocationConfigId) {
            switch (ix) {
              case 0:
                this.warehouseList = resp.resources;
                break;

              case 1:
                this.allocationTypeList = resp.resources;
                break;
              case 2:
                  this.allocationSubTypeList = resp.resources;
                  break;
            }
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  get auditCycleConfigFormArray(): FormArray {
    if (this.imporantAllocationForm !== undefined) {
      return (<FormArray>this.imporantAllocationForm.get('auditCycleConfigDetails'));
    }
  }

  addTypeToTable() {
    if (this.imporantAllocationForm.invalid) {
      this.imporantAllocationForm.markAllAsTouched();
      return;
    }
    let auditCycleFormArray = this.auditCycleConfigFormArray;
    if (this.imporantAllocationTypeSubTypeForm.invalid) {
      this.imporantAllocationTypeSubTypeForm.markAllAsTouched();
      return;
    }
    let typeObj = auditCycleFormArray.value.find(v => v.auditCycleConfigSubType.toLocaleLowerCase() == this.imporantAllocationTypeSubTypeForm.get('subType').value.toLocaleLowerCase());
    if (!typeObj) {
      this.typeExists = false;
      let auditCycleObject = {
        warehouseId: this.imporantAllocationForm.get('warehouse').value, //.join()
        auditCycleConfigTypeId: this.imporantAllocationTypeSubTypeForm.get('type').value,
        auditCycleConfigTypeName: this.selectedTypeName,
        auditCycleConfigSubType: this.imporantAllocationTypeSubTypeForm.get('subType').value,
        description: this.imporantAllocationTypeSubTypeForm.get('description').value,
        stockCount: this.imporantAllocationTypeSubTypeForm.get('stockCount').value,
        timeFrame: this.imporantAllocationTypeSubTypeForm.get('timeFrame').value,
        priority: this.imporantAllocationTypeSubTypeForm.get('priority').value,
        createdUserId: this.userData.userId
      }
      let auditCycleFormGroup = this.formBuilder.group(auditCycleObject, { emitEvent: false });
      auditCycleFormGroup = setRequiredValidator(auditCycleFormGroup, ['auditCycleConfigTypeId', 'auditCycleConfigSubType', 'description', 'timeFrame', 'priority']);
      auditCycleFormGroup.controls['stockCount'].setValidators(Validators.compose([Validators.min(1), Validators.required]));
      auditCycleFormGroup.get('stockCount').valueChanges.subscribe(data => {
        this.totalStockCount = 0;
        this.auditCycleConfigFormArray.controls.forEach((control, index) => {
          this.totalStockCount += (+control.get('stockCount').value);
        })
        if (this.totalStockCount > this.stockLimitForWarehpuse) {
          this.alertLocationClear = true;
          this.isWarningNotificationDialog = true;
        } else {
          this.isWarningNotificationDialog = false;
        }
      });
      this.imporantAllocationTypeSubTypeForm.reset({ emitEvent: false });
      this.selectedTypeName = '';
      auditCycleFormArray.push(auditCycleFormGroup);
    }
    else {
      this.typeExists = true;
    }
  }

  deleteImportantAllocation(index) {
    if (index > -1) {
      this.totalStockCount = this.totalStockCount - this.auditCycleConfigFormArray.controls[index].get('stockCount').value;
      this.isWarningNotificationDialog = this.totalStockCount == 0 ? true : false;
      this.auditCycleConfigFormArray.removeAt(index);
    }
  }

  createImportantAllocation() {
    if (this.auditCycleConfigFormArray.invalid) {
      this.auditCycleConfigFormArray.markAllAsTouched();
      return;
    }
    let finaldata=this.auditCycleConfigFormArray.value;
    finaldata.forEach((ele)=>{
      ele.AuditCycleConfigsubTypeId =  ele.auditCycleConfigSubType;
    })
    this.crudService.create(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ALLOCATION_CONFIG,
      finaldata
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToList();
      }
    })
  }

  updateImportantAllocation() {
    this.rxjsService.setFormChangeDetectionProperty(true);
    this.imporantAllocationUpdateForm.get('modifiedUserId').setValue(this.userData.userId);
    if (this.imporantAllocationUpdateForm.invalid) {
      this.imporantAllocationUpdateForm.markAllAsTouched();
      return;
    }
    let auditCycleConfigSubTypeId = this.imporantAllocationUpdateForm.value.auditCycleConfigSubType;
    let filter = this.allocationSubTypeList.filter(x=>x.id.toString() == auditCycleConfigSubTypeId);
    if(filter?.length > 0)
       this.imporantAllocationUpdateForm.value.auditCycleConfigSubType = filter[0].displayName;

    let finalData;
    finalData = {...this.imporantAllocationUpdateForm.value,...{auditCycleConfigSubTypeId:auditCycleConfigSubTypeId}};
    this.crudService.update(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ALLOCATION_CONFIG,
      finalData
    ).pipe(tap(() => {
      this.rxjsService.setGlobalLoaderProperty(false);
    })).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess == true && response.statusCode == 200) {
        this.navigateToList();
      }
    })
  }

  navigateToList() {
    this.router.navigate(['inventory/audit-cycle-configuration'], { queryParams: { tab: '2' }, skipLocationChange: true });
  }

  navigateToView() {
    this.router.navigate(['inventory/audit-cycle-configuration/important-allocation-view'], { queryParams: { id: this.auditCycleAllocationConfigId }, skipLocationChange: true });
  }

  closePopUp() {
    this.alertLocationClear = false;
  }

}
