import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CustomDirectiveConfig, IApplicationResponse, ModulesBasedApiSuffix, trimValidator } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, SnackbarService } from '@app/shared/services';
import { TimeFrameAddEditModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-time-frame-add-edit',
  templateUrl: './time-frame-add-edit.component.html',
  styleUrls: ['./time-frame-add-edit.component.scss']
})
export class TimeFrameAddEditComponent implements OnInit {
  selectedTabIndex = 0;
  auditCycleTimeFrameConfigId: any;
  timeFrameForm: FormGroup;
  isFoundSpecialcharacter: boolean = false;
  timeFrameArray: FormArray;
  timeFrameTypeList: any = [];
  userData: UserLogin;
  errorMessage: string;
  isButtondisabled = false;
  primengTableConfigProperties;
  isNumberOnly = new CustomDirectiveConfig({ isANumberOnly: true, shouldPasteKeyboardEventBeRestricted: true });
  stringConfig = new CustomDirectiveConfig({ isAStringOnly: true });
  constructor(private formBuilder: FormBuilder, private crudService: CrudService, private activatedRoute: ActivatedRoute,
    private router: Router, private store: Store<AppState>, private snackbarService: SnackbarService) {
    this.auditCycleTimeFrameConfigId = this.activatedRoute.snapshot.queryParams.id;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    let title = this.auditCycleTimeFrameConfigId ? 'Update':'Add';
    this.primengTableConfigProperties = {
      tableCaption:  title+' Time Frame Config',
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory', relativeRouterUrl: '' }, { displayName: 'Time Frame Config List' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: title+' Time Frame Config',
            dataKey: 'warehouseId',
            enableAction: true,
            enableBreadCrumb: true,
            enableExportBtn: true,
            enablePrintBtn: true,
            printTitle: 'Warehouse',
            printSection: 'print-section',
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            disabled:true
          }
        ]
      }
    }
    if(this.auditCycleTimeFrameConfigId)
      this.primengTableConfigProperties.breadCrumbItems.push({ displayName: 'View Time Frame Config', relativeRouterUrl: '/inventory/audit-cycle-configuration/time-config-view', queryParams: { id: this.auditCycleTimeFrameConfigId }});

     this.primengTableConfigProperties.breadCrumbItems.push({ displayName:  title+' Time Frame Config', relativeRouterUrl: '' });
  
  }

  ngOnInit(): void {
    this.createtimeFrameManualAddForm();
    this.LoadTimeFarmeType();
    if (this.auditCycleTimeFrameConfigId) {
      this.gettimeFrameById().subscribe((response: IApplicationResponse) => {
        this.timeFrameArray = this.gettimeFrame;
        this.timeFrameArray.push(this.createtimeFrameFormGroup(response.resources));
      })
    } else {
      this.timeFrameArray = this.gettimeFrame;
      this.timeFrameArray.push(this.createtimeFrameFormGroup());
    }
  }

  //Get Details
  gettimeFrameById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.TIME_FRAME_CONFIG,
      this.auditCycleTimeFrameConfigId
    );
  }
  LoadTimeFarmeType() {
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_TIMEFRAMETYPE, undefined, true).subscribe((res) => {
      this.timeFrameTypeList = res.resources;
    })
  }

  createtimeFrameManualAddForm(): void {
    this.timeFrameForm = this.formBuilder.group({
      timeFrameArray: this.formBuilder.array([])
    });
  }
  //Create FormArray
  get gettimeFrame(): FormArray {
    return this.timeFrameForm.get("timeFrameArray") as FormArray;
  }
  //Create FormArray controls
  createtimeFrameFormGroup(timeFrame?: TimeFrameAddEditModel): FormGroup {
    let timeFrameData = new TimeFrameAddEditModel(timeFrame ? timeFrame : undefined);
    let formControls = {};
    Object.keys(timeFrameData).forEach((key) => {
      formControls[key] = [{ value: timeFrameData[key], disabled: timeFrame && (key == '') && timeFrameData[key] !== '' ? true : false },
      (key === 'timeFrame' ? [Validators.required, trimValidator, Validators.min(1), Validators.max(12)] : (key === 'timeFrameTypeId') ? [Validators.required] : [])]
    });
    return this.formBuilder.group(formControls);
  }

  onChange() {
    const duplicate = this.duplicateValue();
    if (duplicate) {
      this.snackbarService.openSnackbar(
        `Time Frame Config Type  already exist `,
        ResponseMessageTypes.WARNING
      );
      return false;
    }
    return true;
  }

  duplicateValue() {
    const filterKey = [];
    const duplicate = [];
    this.gettimeFrame.controls.filter((k) => {
      if (filterKey.includes(k.value.timeFrameTypeId + ' ' + k.value.timeFrame)) {
        duplicate.push(k.value.timeFrameTypeId + ' ' + k.value.timeFrame);
      }
      filterKey.push(k.value.timeFrameTypeId + ' ' + k.value.timeFrame);
    });
    return duplicate.length ? duplicate.join(",") : false;
  }

  //Add items
  addtimeFrame(): void {
    if (this.timeFrameForm.invalid) return;
    this.timeFrameArray = this.gettimeFrame;
    let timeFrameData = new TimeFrameAddEditModel();
    this.timeFrameArray.push(this.createtimeFrameFormGroup(timeFrameData));
  }

  //Remove Items
  removetimeFrame(i: number): void {
    if (this.gettimeFrame.controls[i].value.auditCycleTimeFrameConfigId != null) {
      this.crudService.delete(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.TIME_FRAME_CONFIG,
        this.gettimeFrame.controls[i].value.auditCycleTimeFrameConfigId).subscribe((response: IApplicationResponse) => {
          if (response.isSuccess) {
            this.gettimeFrame.removeAt(i);
          }
          if (this.gettimeFrame.length === 0) {
            this.addtimeFrame();
          };
        });
    }
    else if (this.gettimeFrame.length === 1) {
      this.snackbarService.openSnackbar("Atleast one Time Frame  Type required", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.gettimeFrame.removeAt(i);
    }
  }

  submit() {
    if (!this.onChange()) {
      return;
    }
    if (this.gettimeFrame.invalid) {
      return;
    }
    this.gettimeFrame.value.forEach((key) => {
      key["createdUserId"] = this.userData.userId;
      key["modifiedUserId"] = this.userData.userId;
    })
    this.isButtondisabled = true;
    this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.TIME_FRAME_CONFIG, this.timeFrameForm.value["timeFrameArray"])
      .subscribe({
        next: response => {
          if (response.isSuccess) {
            this.router.navigate(['/inventory/audit-cycle-configuration'], { queryParams: { tab: 0 }, skipLocationChange: true });
          } else {
            this.isButtondisabled = false;
          }
        },
        error: err => this.errorMessage = err
      });
  }
  onCRUDRequested(ev) {}
}
