import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { TimeFrameAddEditModel } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { Observable } from 'rxjs';
@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-time-frame-view',
  templateUrl: './time-frame-view.component.html'
})
export class TimeFrameViewComponent implements OnInit {
  auditCycleTimeFrameConfigId: '';
  timeFrameDetails: TimeFrameAddEditModel;
  constructor(private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private router: Router) {
    this.auditCycleTimeFrameConfigId = this.activatedRoute.snapshot.queryParams.id;
  }

  ngOnInit(): void {
    if (this.auditCycleTimeFrameConfigId) {

      this.gettimeFrameArrayById().subscribe((response: IApplicationResponse) => {
        this.timeFrameDetails = response.resources;
      })
    }
  }
  //Get Details 
  gettimeFrameArrayById(): Observable<IApplicationResponse> {
    return this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.TIME_FRAME_CONFIG,
      this.auditCycleTimeFrameConfigId
    );
  }

  Edit() {
    if (this.auditCycleTimeFrameConfigId) {
      this.router.navigate(['inventory/audit-cycle-configuration/time-frame-add-edit'], { queryParams: { id: this.auditCycleTimeFrameConfigId }, skipLocationChange: true });
    }
  }
}

