import { Component, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudType,exportList,IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, prepareGetRequestHttpParams,RxjsService,currentComponentPageBasedPermissionsSelector$, prepareDynamicTableTabsFromPermissions, PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes, SnackbarService} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, of } from 'rxjs';
import { PrimeNgTableVariablesModel } from '.././../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-time-frame-configuration',
  templateUrl: './time-frame-configuration.component.html',
  styleUrls: ['./time-frame-configuration.component.scss']
})
export class TimeFrameConfigurationComponent extends PrimeNgTableVariablesModel {
  primengTableConfigProperties: any;
  otherParams;
  constructor(private snackbarService:SnackbarService,private crudService: CrudService,private activatedRoute: ActivatedRoute,public dialogService: DialogService,
    private router: Router, private store: Store<AppState>,private rxjsService: RxjsService) {
    super();
    this.primengTableConfigProperties = {
      tableCaption: "Audit Cycle Configuration",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '' },{ displayName: 'Stock Take', relativeRouterUrl: '' },{ displayName: 'Audit Cycle Configuration', relativeRouterUrl: '' }, { displayName: 'Audit Cycle Configuration', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Stock Limit Config',
            dataKey: 'stockLimitConfigId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableAddActionBtn: true,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableAction: true,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            enablePrintBtn: true,
            printTitle: 'Audit Cycle Configuration',
            printSection: 'print-section0',           
            cursorSecondLinkIndex: 1,
            disabled:true,
            columns: [
              { field: 'warehouse', header: 'Warehouse', width: '200px' },
              { field: 'randomStockLimit', header: 'Random Stock Limit', width: '200px' },
              { field: 'highRiskStockLimitPerItem', header: 'High Risk Stock Limit Per Item', width: '200px' },
              { field: 'systemGeneratedStockLimit', header: 'System Generated Stock Limit', width: '200px' },
              { field: 'createdBy', header: 'Created By', width: '200px' },
              { field: 'createdDate', header: 'Created Date', width: '200px' }
            ],
            enableExportBtn: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.WAREHOUSE_STOCK_LIMIT_CONFIG,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
          },
          {
            caption: 'High Risk Stock',
            dataKey: 'doaApprovalId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            enableAction: true,
            enablePrintBtn: true,
            printTitle: 'Audit Cycle Configuration',
            printSection: 'print-section0',
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            disabled:true,
            columns: [{ field: 'warehouse', header: 'Warehouse', width: '200px' }, { field: 'auditCycleConfigType', header: 'Type', width: '200px' },
            { field: 'description', header: 'Description', width: '200px' }, { field: 'stockCount', header: 'Stock Count', width: '200px' },
            { field: 'timeFrame', header: 'Time Frame(in Months)', width: '200px' }, { field: 'priority', header: 'Priority', width: '200px' }, { field: 'status', header: 'Status', width: '200px' }],
            apiSuffixModel: InventoryModuleApiSuffixModels.AUDIT_CYCLE_HIGH_RISK_STOCK_CONFIG,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableExportBtn: true,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
          },
          {
            caption: 'Importance Allocation Config',
            dataKey: 'auditCycleAllocationConfigId', //doaApprovalId
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            enableAction: true,
            enablePrintBtn: true,
            printTitle: 'Audit Cycle Configuration',
            printSection: 'print-section0',
            enableAddActionBtn: true,
            disabled:true,
            columns: [
              { field: 'warehouse', header: 'Warehouse', width: '200px' },
              { field: 'status', header: 'Status', width: '200px' },
              { field: 'auditCycleConfigType', header: 'Type', width: '200px' },
              { field: 'auditCycleConfigSubType', header: 'Sub Type', width: '200px' },
              { field: 'description', header: 'Description', width: '200px' },
              { field: 'stockCount', header: 'Stock Count', width: '200px' },
              { field: 'timeFrame', header: 'Time Frame (In Months)', width: '200px' },
              { field: 'priority', header: 'Priority', width: '200px' },
            ],
            enableExportBtn: true,
            apiSuffixModel: InventoryModuleApiSuffixModels.ALLOCATION_CONFIG,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            enableMultiDeleteActionBtn: false,
            ebableAddActionBtn: true,
            shouldShowFilterActionBtn: false,
          },
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[3].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
  }

  combineLatestNgrxStoreData() {
    combineLatest([  this.store.select(loggedInUserData),
      this.store.select(currentComponentPageBasedPermissionsSelector$)]     
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.AUDIT_CYCLE_CONFIGURATION];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    otherParams = {
      userId: this.loggedInUserData?.userId,...otherParams};
    this.otherParams = otherParams;
    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels,
      undefined,
      false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).subscribe((response: IApplicationResponse) => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (response.isSuccess && response.statusCode === 200) {
        this.dataList = response?.resources
        this.totalRecords = response?.totalCount;
      } else {
        response.resources = null;
        this.dataList = response?.resources
        this.totalRecords = 0;
      }
    })
  }


  onCRUDRequested(type: CrudType | string, row?: object, unknownVar?: any): void {
    switch (type) {
      case CrudType.GET:
        this.row = row ? row : { pageIndex: 0, pageSize: 10 };
        this.getRequiredListData(row["pageIndex"], row["pageSize"], unknownVar)
        break;
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        this.openAddEditPage(CrudType.CREATE, row);
        break;
      case CrudType.VIEW:
        this.openAddEditPage(CrudType.VIEW, row);
        break;
      case CrudType.EXPORT:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
          let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
          this.exportList(pageIndex,pageSize);
        break;
      default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    let apiName;let label;
    if(this.selectedTabIndex==0){
      apiName = InventoryModuleApiSuffixModels.STOCK_LIMIT_CONFIG_EXPORT;
      label = 'Stock Limit Config';
    }     
    else if(this.selectedTabIndex==1){
      apiName = InventoryModuleApiSuffixModels.AUDIT_CYCLE_CONFIG_EXPORT;
      label = 'High Risk Stock';
    }      
    else if(this.selectedTabIndex==2){
      apiName = InventoryModuleApiSuffixModels.AUDIT_CYCLE_ALLOCATION_CONFIG_EXPORT;
      label = 'Importance Allocation Config';
    }
     
    exportList(this.otherParams,0,pageSize,ModulesBasedApiSuffix.INVENTORY,
      apiName,this.crudService,this.rxjsService,label);
  }
  onActionSubmited(e) {
    if (e.data && !e.search && !e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
      this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
      this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
      this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }
  onChangeSelecedRows(e) {
    this.selectedRows = e;
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index
    this.router.navigate(['/inventory/audit-cycle-configuration'], { queryParams: { tab: this.selectedTabIndex } })
    this.getRequiredListData()
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string): void {
    switch (type) {
      case CrudType.CREATE:
        if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
          return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
        }
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["inventory/audit-cycle-configuration/random-selection-stock-limit-add-edit"], { skipLocationChange: true });
            break;
          case 1:
            this.router.navigate(["inventory/audit-cycle-configuration/high-risk-task-add-edit"], { skipLocationChange: false });
            break;
          case 2:
            this.router.navigate(["inventory/audit-cycle-configuration/important-allocation-add-edit"], { skipLocationChange: true });
            break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["inventory/audit-cycle-configuration/random-selection-stock-limit-view"], { queryParams: { id: editableObject['stockLimitConfigId'] }, skipLocationChange: true });
            break;
          case 1:
            this.router.navigate(["inventory/audit-cycle-configuration/high-risk-task-view"], { queryParams: { id: editableObject['auditCycleHighRiskStockConfigId'] }, skipLocationChange: true });
            break;
          case 2:
            this.router.navigate(["inventory/audit-cycle-configuration/important-allocation-view"], { queryParams: { id: editableObject['auditCycleAllocationConfigId'] }, skipLocationChange: true });
            break;
        }
        break;
    }
  }
}
