import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import { AuditCycleRoutingModule } from './audit-cycle-routing.module';
import { HighRiskStockConfigurationAddEditComponent } from './Components/High-risk-stock-configuration/high-risk-stock-configuration-add-edit.component';
import { HighRiskStockConfigurationViewComponent } from './Components/High-risk-stock-configuration/high-risk-stock-configuration-view.component';
import { ImportantAllocationAddEditComponent, ImportantAllocationViewComponent } from './Components/important-allocation-configuration';
import { RandomSelectionStockLimitConfigurationAddEditComponent } from './Components/Random-selection-stock-limit-configuration/random-selection-stock-limit-configuration-add-edit.component';
import { RandomSelectionStockLimitConfigurationViewComponent } from './Components/Random-selection-stock-limit-configuration/random-selection-stock-limit-configuration-view.component';
import { TimeFrameAddEditComponent, TimeFrameConfigurationComponent, TimeFrameViewComponent } from './Components/time-frame-configuration';
@NgModule({
  declarations: [TimeFrameConfigurationComponent, TimeFrameAddEditComponent, TimeFrameViewComponent, ImportantAllocationAddEditComponent, ImportantAllocationViewComponent, HighRiskStockConfigurationAddEditComponent, HighRiskStockConfigurationViewComponent, RandomSelectionStockLimitConfigurationAddEditComponent, RandomSelectionStockLimitConfigurationViewComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    AuditCycleRoutingModule,
    MaterialModule,
    SharedModule
  ]
})
export class AuditCycleModule { }
