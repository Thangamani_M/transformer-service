import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HighRiskStockConfigurationAddEditComponent } from './Components/High-risk-stock-configuration/high-risk-stock-configuration-add-edit.component';
import { HighRiskStockConfigurationViewComponent } from './Components/High-risk-stock-configuration/high-risk-stock-configuration-view.component';
import { ImportantAllocationAddEditComponent, ImportantAllocationViewComponent } from './Components/important-allocation-configuration';
import { RandomSelectionStockLimitConfigurationAddEditComponent } from './Components/Random-selection-stock-limit-configuration/random-selection-stock-limit-configuration-add-edit.component';
import { RandomSelectionStockLimitConfigurationViewComponent } from './Components/Random-selection-stock-limit-configuration/random-selection-stock-limit-configuration-view.component';
import { TimeFrameAddEditComponent, TimeFrameConfigurationComponent, TimeFrameViewComponent } from './Components/time-frame-configuration';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';
const auditCycleModuleRoutes: Routes = [
    { path: '', component: TimeFrameConfigurationComponent, data: { title: 'Audit Cycle Configuration' } ,canActivate:[AuthGuard]},
    { path: 'time-frame-add-edit', component: TimeFrameAddEditComponent, data: { title: 'Time Frame Add Edit' },canActivate:[AuthGuard] },
    { path: 'time-frame-view', component: TimeFrameViewComponent, data: { title: 'Time Frame View' } ,canActivate:[AuthGuard]},
    { path: 'important-allocation-add-edit', component: ImportantAllocationAddEditComponent, data: { title: 'Importance  Allocation Add Edit' } ,canActivate:[AuthGuard]},
    { path: 'important-allocation-view', component: ImportantAllocationViewComponent, data: { title: 'Importance  Allocation View' },canActivate:[AuthGuard] },
    { path: 'high-risk-task-add-edit', component: HighRiskStockConfigurationAddEditComponent, data: { title: 'High Risk Stock Add Edit' },canActivate:[AuthGuard] },
    { path: 'high-risk-task-view', component: HighRiskStockConfigurationViewComponent, data: { title: 'High Risk Stock View' } ,canActivate:[AuthGuard]},
    { path: 'random-selection-stock-limit-add-edit', component: RandomSelectionStockLimitConfigurationAddEditComponent, data: { title: 'Stock Limit Add Edit' },canActivate:[AuthGuard] },
    { path: 'random-selection-stock-limit-view', component: RandomSelectionStockLimitConfigurationViewComponent, data: { title: 'Stock Limit View' } ,canActivate:[AuthGuard]},
];

@NgModule({
    imports: [RouterModule.forChild(auditCycleModuleRoutes)],
    
})

export class AuditCycleRoutingModule { }
