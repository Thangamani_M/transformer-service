import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { BarcodePrintComponent } from '../../barcode-print';
import { MatDialog } from '@angular/material';
import { receivingTransferRequestAddEditModal } from '@modules/inventory/models/receiving-transfer-request.model';

@Component({
  selector: 'app-receiving-transfer-request-verify',
  templateUrl: './receiving-transfer-request-verify.component.html',
  styleUrls: ['./receiving-transfer-request-verify.component.scss']
})
export class ReceivingTransferRequestVerifyComponent implements OnInit {

  userData: UserLogin;
  transferRequestId: string;
  requestBatchId?: string;
  receiptingGetDetails: any;
  receivingNewStockDetails: any = {};
  stockInfoDetailDialog: boolean = false;
  receiptingNewVerifyForm: FormGroup;
  isPrintSuccessDialog: boolean = false;

  @ViewChild(SignaturePad, { static: false }) signaturePad: any;

  public signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 500,
    'canvasHeight': 80
  };

  constructor(
    private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private httpService: CrudService, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private router: Router, private crudService: CrudService,
    private httpCancelService: HttpCancelService, private dialog: MatDialog, private formBuilder: FormBuilder,
  ) { 
    this.transferRequestId = this.activatedRoute.snapshot.queryParams.id;
    this.requestBatchId = this.activatedRoute.snapshot.queryParams.batchId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createTransferRequestForm();
    if(this.transferRequestId && this.requestBatchId){
      this.getReceivingNewDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.receiptingGetDetails = response.resources;
          this.receiptingNewVerifyForm.patchValue(response.resources);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar("Something went wrong", ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    }
  }

  createTransferRequestForm(): void {
    let stockOrderModel = new receivingTransferRequestAddEditModal();
    // create form controls dynamically from model class
    this.receiptingNewVerifyForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.receiptingNewVerifyForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getReceivingNewDetails(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('RadioRemovalTransferRequestId', this.transferRequestId)
    .set('OrderReceiptBatchId', this.requestBatchId)
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.TRANSFER_REQUEST_RECEIVING_DETAILS, 
    undefined, true, params);
  }

  openStockModal(data: any){
    this.receivingNewStockDetails = data;
    this.stockInfoDetailDialog = true;
  }

  drawComplete() {

  }

  clearPad() {
    this.signaturePad.clear();
  }

  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.resizeCanvas();
    this.signaturePad.clear();  
  }

  onSubmit(){ 
    if (this.receiptingNewVerifyForm.invalid) {
      return;
    }
    
    if(this.signaturePad.signaturePad._data.length == 0){
      this.snackbarService.openSnackbar("Please Enter the signature", ResponseMessageTypes.WARNING);
      return;
    }

    let verifyDetails = {
      "RequestNumber": this.receiptingGetDetails.requestNumber,
      "OrderReceiptId": this.receiptingGetDetails.orderReceiptId,
      "OrderReceiptBatchId": this.receiptingGetDetails.orderReceiptBatchId,
      "DocumentType": this.receiptingGetDetails.documentType,
      "Comments": this.receiptingNewVerifyForm.get('comments').value,
      "Name": this.receiptingGetDetails.courierName,
      "OrderTypeId": this.receiptingGetDetails.orderTypeId,
      "CreatedUserId": this.userData.userId
    }

    const formData = new FormData();
    formData.append("receivingDetails", JSON.stringify(verifyDetails));
    if (this.signaturePad.signaturePad._data.length > 0) {
      let imageName =  this.receiptingGetDetails.receivingId + ".jpeg";
      const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
      const imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' });
      formData.append('document_files[]', imageFile);
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
    this.crudService.update(ModulesBasedApiSuffix.INVENTORY, 
      InventoryModuleApiSuffixModels.TRANSFER_REQUEST_RECEIVING_SUBMIT, formData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  dialogClose(){
    this.isPrintSuccessDialog = false;
  }

  Print(){
    this.receiptingGetDetails.transferBarcode
    const dialogReff = this.dialog.open(BarcodePrintComponent, 
      { width: '400px', height: '400px', disableClose: true, 
        data: { serialNumbers: this.receiptingGetDetails?.transferBarcode } 
      });
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  navigateToList(){
    this.router.navigate(['/inventory', 'order-receipts', 'receipting'], {
      queryParams: { tab: 5 },
      skipLocationChange: true
    });
  }

  navigateToEdit(){
    this.router.navigate(["inventory/order-receipts/receipting/receiving-transfer-request-add-edit"], { 
      queryParams: { 
        id: this.transferRequestId,
        batchId: this.requestBatchId,
      }, skipLocationChange: true 
    });
  }

}
