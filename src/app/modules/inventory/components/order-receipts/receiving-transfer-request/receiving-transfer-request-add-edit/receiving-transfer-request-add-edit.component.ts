import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { receiptingNewItemsScanModal, ReceivingNewItemsArrayModal } from '@modules/inventory/models/receipting-new-item.model';
import { receivingTransferRequestAddEditModal } from '@modules/inventory/models/receiving-transfer-request.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { Observable } from 'rxjs';
import { BarcodePrintComponent } from '../../barcode-print';

@Component({
  selector: 'app-receiving-transfer-request-add-edit',
  templateUrl: './receiving-transfer-request-add-edit.component.html'
})
export class ReceivingTransferRequestAddEditComponent implements OnInit {

  receiptingTransferAddEditForm: FormGroup;
  goodsReturnStockInfoForm: FormGroup;
  transferRequestId: string;
  requestBatchId?: any;
  userData: UserLogin;
  stockDetailListDialog: boolean = false;
  serialInfoIndex: number;
  stockDetails: any;
  showPONumberError: boolean = false;
  filteredPONumbers: any = [];
  receiptingGetDetails: any = {};
  showItemCodeError: boolean = false;
  showReceptingItems: boolean = false;
  receptingItemsDetails: any = {};
  receiptingNewItemsarray: FormArray;
  items;
  constructor(
    private formBuilder: FormBuilder, private rxjsService: RxjsService, private dialogService: DialogService,
    private snackbarService: SnackbarService, private crudService: CrudService, private dialog: MatDialog,
    private store: Store<AppState>, private router: Router, private activatedRoute: ActivatedRoute)
  {
    this.transferRequestId = this.activatedRoute.snapshot.queryParams.id;
    this.requestBatchId = this.activatedRoute.snapshot.queryParams.batchId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {

    this.createTransferRequestForm();
    this.createReceiptingNewItemForm();

    if(this.transferRequestId && this.requestBatchId){
      this.getReceiptingNewOrderItems(this.transferRequestId, 'get');
    }
  }

  createTransferRequestForm(): void {
    let stockOrderModel = new receivingTransferRequestAddEditModal();
    // create form controls dynamically from model class
    this.receiptingTransferAddEditForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.receiptingTransferAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  inputChangePoNumberFilter(text:any){

    if(this.userData.warehouseId == null || this.userData.warehouseId == ''){
      this.snackbarService.openSnackbar("User account not setup with default warehouse", ResponseMessageTypes.WARNING);
      return;
    }

    let otherParams = {}
    this.showPONumberError = false;
    if(text.query == null || text.query == '')return;
    otherParams['displayName'] = text.query;
    otherParams['userId'] = this.userData.userId;
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.
      UX_TRANSFER_REQUEST_RECEIVING, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.filteredPONumbers = response.resources;
          if(response.resources.length == 0){
            this.showPONumberError = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
    });
  }

  getReceiptingNewOrderItems(obj: any, type: string){

    if(this.userData.warehouseId == null || this.userData.warehouseId == ''){
      this.snackbarService.openSnackbar("User account not setup with default warehouse", ResponseMessageTypes.WARNING);
      return;
    }

    let params;
    this.showPONumberError = false;
    this.showItemCodeError = false;
    this.rxjsService.setFormChangeDetectionProperty(true);

    if(type == 'poNumber'){
      if(obj.id == null)return;
      params = new HttpParams().set('RadioRemovalTransferRequestId', obj.id)
      .set('UserId', this.userData.userId);
    }
    else if(type == 'get'){
      params = new HttpParams().set('RadioRemovalTransferRequestId', obj)
      .set('UserId', this.userData.userId);
    }
    else {
      if (this.receiptingTransferAddEditForm.get('transferBarcode').value == '' ||
        this.receiptingTransferAddEditForm.get('transferBarcode').value == null) {
        this.showItemCodeError = true;
        return;
      }
      params = new HttpParams().set('TransferBarcode ',
      this.receiptingTransferAddEditForm.get('transferBarcode').value)
      .set('UserId', this.userData.userId);
    }

    let crudService: Observable<IApplicationResponse> =
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.TRANSFER_REQUEST_RECEIVING_ITEMS, undefined, true, params)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.showReceptingItems = true;
        this.receiptingGetDetails = response.resources;
        this.receptingItemsDetails = response.resources.items;
        this.receiptingTransferAddEditForm.patchValue(response.resources);
        let poNumbers = {
          displayName: response.resources.requestNumber,
          id: response.resources.orderReferenceId,
        }
        this.filteredPONumbers.push(poNumbers);
        this.receiptingTransferAddEditForm.get('requestNumber').patchValue(poNumbers);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  stockDetail(getDetails: any, index:number) {

    this.stockDetailListDialog = true;
    this.showBarcodeError = false;
    this.serialInfoIndex = index;
    this.stockDetails = getDetails;

    let itemDetails = getDetails.itemDetails == undefined ? null : getDetails.itemDetails;
    this.getallBarcodesCount = getDetails.receivedQuantity + getDetails.outStandingQuantity;
    this.scannedBarcodesCount = getDetails.receivedQuantity;
    this.createReceiptingNewItemForm();
    this.receiptingNewItemsarray = this.getNewScanItemsArray;
    this.receiptingNewItemsarray.clear();
    if (itemDetails != null && itemDetails.length > 0) {
      itemDetails.forEach((items) => {
        this.receiptingNewItemsarray.push(this.createReceivingItemsarrayModel(items));
      });
    }
  }

  createReceiptingNewItemForm(): void {
    let stockOrderModel = new receiptingNewItemsScanModal();
    // create form controls dynamically from model class
    this.goodsReturnStockInfoForm = this.formBuilder.group({
      receiptingNewItemsarray: this.formBuilder.array([])
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.goodsReturnStockInfoForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    if(this.stockDetails?.isNotSerialized){
      this.goodsReturnStockInfoForm = setRequiredValidator(this.goodsReturnStockInfoForm, ["receivedQty"]);
      this.goodsReturnStockInfoForm.get('notReceivedQty').patchValue(this.receiptingGetDetails?.items?.receivedQuantity);
    }
  }

  //Create FormArray controls
  createReceivingItemsarrayModel(interBranchModel?: ReceivingNewItemsArrayModal): FormGroup {
    let interBranchModelData = new ReceivingNewItemsArrayModal(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      formControls[key] = [interBranchModelData[key], (key === '') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getNewScanItemsArray(): FormArray {
    if (!this.goodsReturnStockInfoForm) return;
    return this.goodsReturnStockInfoForm.get("receiptingNewItemsarray") as FormArray;
  }

  showBarcodeError: boolean = false;
  scannedBarcodesCount: number = 0;
  getallBarcodesCount: number = 0;

  scanSerialNumber(){

    this.showBarcodeError = false;
    let scanvalue: boolean = false
    this.rxjsService.setFormChangeDetectionProperty(true);

    let scanInput = this.goodsReturnStockInfoForm.get('scanSerialNumberInput').value;
    if (scanInput == '' || scanInput == null) {
      this.showBarcodeError = true;
      return;
    }

    this.receiptingNewItemsarray.value.forEach(e => {
      if(e.serialNumber == scanInput){
        scanvalue = true;
      }
    });

    this.goodsReturnStockInfoForm.get('scanSerialNumberInput').patchValue(null);
    if(scanvalue){
      this.snackbarService.openSnackbar('Serial number scanned already', ResponseMessageTypes.WARNING);
      return;
    }

    if (this.scannedBarcodesCount >= this.getallBarcodesCount) {
      this.snackbarService.openSnackbar("Enter a valid value in the Received Quantity field.", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.addNewSerialNumberFun(scanInput)
    }
  }

  addNewSerialNumberFun(value){
    if(value == undefined) return;
    let duplicate = this.receiptingNewItemsarray.value.filter(x => x.serialNumber === value);
    if(duplicate.length > 0) {
      this.snackbarService.openSnackbar("Serial Number '" + value + "' was already added", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.scannedBarcodesCount += 1;
      let newarray = {
        "serialNumber": value,
        "serialNumberChecked": false,
        "orderReceiptItemDetailId": null,
        "orderReceiptItemId": null,
        "barcode":value,
        "isDeleted":false
      }
      this.receiptingNewItemsarray.push(this.createReceivingItemsarrayModel(newarray));
      this.goodsReturnStockInfoForm.get('notReceivedQty').patchValue(null);
      this.goodsReturnStockInfoForm.get('scanSerialNumberInput').patchValue(null);
    }
  }

  onChangeFaultyQty(){
    if (this.scannedBarcodesCount > this.getallBarcodesCount) {
      this.snackbarService.openSnackbar("Enter a valid value in the Received Quantity field.", ResponseMessageTypes.WARNING);
      return;
    }
  }

  removeScanedBarcode(i:number, type: string){

    if (this.scannedBarcodesCount == 0) {
      this.snackbarService.openSnackbar("Deleted Item Should be Empty", ResponseMessageTypes.WARNING);
      return;
    }

    let customText = "Are you sure you want to delete this?"
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: 'Confirmation',
      showHeader: false,
      closable: true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText },
    });
    confirm.onClose.subscribe((resp) => {
      if (resp === false) {
        if(type == 'new'){
          this.receiptingNewItemsarray.controls[i].get('isDeleted').patchValue(true);
          this.scannedBarcodesCount = this.scannedBarcodesCount - 1;
        }
        else {
          this.receiptingNewItemsarray.controls.forEach(control => {
            control.get('isDeleted').setValue(true);
          });
          this.scannedBarcodesCount = 0;
        }
      }
    });
  }

  saveSerialInfo(){

    if(this.scannedBarcodesCount > this.getallBarcodesCount){
      this.snackbarService.openSnackbar("Enter a valid value in the Received Quantity field.", ResponseMessageTypes.WARNING);
      return;
    }

    let getValue = this.receiptingNewItemsarray.value.filter(x => x.isDeleted);

    if(!this.stockDetails?.isNotSerialized &&
      (this.receiptingNewItemsarray.value.length === getValue.length)){
      this.snackbarService.openSnackbar('"Atleast one record is required"', ResponseMessageTypes.WARNING);
      return;
    }

    let sendparams = {

      orderReceiptId: this.receiptingGetDetails?.orderReceiptId,
      orderReferenceId: this.receiptingGetDetails?.orderReferenceId,
      orderReceiptBatchId: this.receiptingGetDetails?.orderReceiptBatchId,
      createdUserId: this.userData.userId,
      item: {
        radioRemovalTransferRequestItemId: this.stockDetails?.radioRemovalTransferRequestItemId,
        orderReceiptItemId: this.stockDetails?.orderReceiptItemId,
        orderItemId: this.stockDetails?.orderItemId,
        quantity: !this.stockDetails?.isNotSerialized ? this.receiptingNewItemsarray.value.length: this.stockDetails?.qty,
        consumableQuantity: this.stockDetails?.isNotSerialized ? this.goodsReturnStockInfoForm.get('notReceivedQty').value : this.stockDetails?.qty,
        isSerialized: !this.stockDetails?.isNotSerialized ? true : false,
        isConsumable: this.stockDetails?.isNotSerialized ? true : false,
        itemDetails: this.receiptingNewItemsarray.value.length > 0 ? this.receiptingNewItemsarray.value : null
      }
    }

    let crudService: Observable<IApplicationResponse> =
    this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.TRANSFER_REQUEST_RECEIVING_ITEMS, sendparams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.stockDetailListDialog = false;
        this.getReceiptingNewOrderItems(this.receiptingGetDetails?.orderReferenceId, 'get');
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  printSerialNumbers: any = [];

  print() {
    if (this.scannedBarcodesCount == 0) {
      this.snackbarService.openSnackbar("Serial Number Barcode Empty", ResponseMessageTypes.WARNING);
      return;}
    this.printSerialNumbers = [];
    this.receiptingNewItemsarray.value.forEach(prints => {
      if(prints.serialNumberChecked){
        this.printSerialNumbers.push(prints.serialNumber);
      }
    });
    const dialogReff = this.dialog.open(BarcodePrintComponent,
    { width: '400px', height: '400px', disableClose: true,
      data: { serialNumbers: this.printSerialNumbers }
    });
  }

  saveAsDraft(){
    let saveAsDraft = {
      OrderReceiptId: this.receiptingGetDetails.orderReceiptId,
      OrderReceiptBatchId: this.receiptingGetDetails.orderReceiptBatchId,
      CreatedUserId: this.userData.userId,
      // DeliveryNote: this.receiptingNewItemAddEditForm.get('deliveryNote').value
    };
    this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ORDER_RECEIPTS_SAVE_AS_DRAFT, saveAsDraft).subscribe((response) => {
      if (response.isSuccess && response.statusCode == 200) {
        // this.navigateToList();
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  deleteConfirm: boolean = false;

  delete() {
    this.deleteConfirm = true;
  }

  deleteReceptingOrder() {
    let body = {
      "ids": this.receiptingGetDetails.orderReceiptBatchId,
      "ModifiedUserId": this.userData.userId
    }
    this.crudService.delete(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ORDER_RECEIPTS, null, prepareRequiredHttpParams(body)).subscribe({
        next: response => {
          this.deleteConfirm = false;
          if (response.isSuccess == true)
            // this.navigateToList();
            this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  navigateToList(){
    this.router.navigate(['/inventory', 'order-receipts', 'receipting'], {
      queryParams: { tab: 5 },
      skipLocationChange: true
    });
  }

  navigateToViewPage(){
    this.router.navigate(["inventory/order-receipts/receipting/receiving-transfer-request-view"], {
      queryParams: {
        id: this.receiptingGetDetails?.orderReferenceId,
        batchId: this.receiptingGetDetails?.orderReceiptBatchId,
      }, skipLocationChange: true
    });
  }

  navigateToVerifyPage(){
    this.router.navigate(["inventory/order-receipts/receipting/receiving-transfer-request-verify"], {
      queryParams: {
        id: this.receiptingGetDetails?.orderReferenceId,
        batchId: this.receiptingGetDetails?.orderReceiptBatchId,
      }, skipLocationChange: true
    });
  }
  onClose(){

  }
}
