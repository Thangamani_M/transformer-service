import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-receiving-transfer-request-view',
  templateUrl: './receiving-transfer-request-view.component.html'
})
export class ReceivingTransferRequestViewComponent implements OnInit {

  transferRequestId: string;
  requestBatchId?: string;
  receivingType: string;
  userData: UserLogin;
  receivingNewDetails: any = {}
  stockInfoDetailDialog: boolean = false;
  receivingNewStockDetails: any = {};
  receptingItemsDetails;
  constructor(
    private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private httpService: CrudService, private rxjsService: RxjsService,
    private snackbarService: SnackbarService, private router: Router,
  ) { 
    this.transferRequestId = this.activatedRoute.snapshot.queryParams.id;
    this.requestBatchId = this.activatedRoute.snapshot.queryParams.batchId;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });

  }

  ngOnInit(): void {
    if(this.transferRequestId && this.requestBatchId){
      this.getReceivingNewDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.receivingNewDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    }
  }

  getReceivingNewDetails(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('RadioRemovalTransferRequestId', this.transferRequestId).set('OrderReceiptBatchId', this.requestBatchId)
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.TRANSFER_REQUEST_RECEIVING_DETAILS, 
    undefined, true, params);
  }

  openStockModal(data: any){
    this.receivingNewStockDetails = data;
    this.stockInfoDetailDialog = true;
  }

  navigateToList(){
    this.router.navigate(['/inventory', 'order-receipts', 'receipting'], {
      queryParams: { tab: 5 },
      skipLocationChange: true
    });
  }

  openBarcodeModal(){
    
  }

  navigateToEdit(){
    this.router.navigate(["inventory/order-receipts/receipting/receiving-transfer-request-add-edit"], { 
      queryParams: { 
        id: this.transferRequestId,
        batchId: this.requestBatchId,
      }, skipLocationChange: true 
    });
  }

}
