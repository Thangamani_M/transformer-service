import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-radio-system-removal-receiving-verify',
  templateUrl: './radio-system-removal-receiving-verify.component.html',
  styleUrls: ['./radio-system-removal-receiving-verify.component.scss']
})
export class RadioSystemRemovalReceivingVerifyComponent implements OnInit {

  radioRemovalSystemDetails: any = {};
  radioRemovalReceiptId: string;
  radioSystemType: string = '';
  radioRemovalReceivingVerifyForm: FormGroup;
  deliveryNote: string = '';
  userId: string = '';
  selectedTabIndex:any=0;
  @ViewChild(SignaturePad, { static: false }) signaturePad: any;

  public signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 500,
    'canvasHeight': 80
  };

  constructor(
    private crudService: CrudService, private snackbarService: SnackbarService, private formBuilder: FormBuilder,
    private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private router: Router,
  ) { 
    this.radioRemovalReceiptId = this.activatedRoute.snapshot.queryParams.id;
    this.radioSystemType = this.activatedRoute.snapshot.queryParams.type;
    this.userId = this.activatedRoute.snapshot.queryParams.userId;
    this.deliveryNote = this.activatedRoute.snapshot.queryParams.deliveryNote;
  }

  ngOnInit(): void {
    if(this.radioRemovalReceiptId){
      this.radioRemovalDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.radioRemovalSystemDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }

    this.radioRemovalReceivingVerifyForm = this.formBuilder.group({
      comments: ['', Validators.required]
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  radioRemovalDetails(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('RadioRemovalReceiptId', this.radioRemovalReceiptId)
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING_DETAILS, 
    undefined, true, params);
  }

  drawComplete() {

  }

  clearPad() {
    this.signaturePad.clear();
  }

  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.resizeCanvas();
    this.signaturePad.clear();
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
        int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  isButtonDisabled: boolean = false;

  onSubmit(){

    if(this.radioRemovalReceivingVerifyForm.invalid){
      return;
    }

    if (this.signaturePad.signaturePad._data.length === 0) {
      this.snackbarService.openSnackbar("Signature is required", ResponseMessageTypes.WARNING);
      return;
    }

    let formData = new FormData();
    let data = {
      "RadioRemovalReceiptId": this.radioRemovalReceiptId,
      "Comments": this.radioRemovalReceivingVerifyForm.get('comments').value,
      "CreatedUserId": this.userId,
      "DocumentType": this.radioRemovalSystemDetails?.basicInfo?.DocumentTypeName,
      "DeliveryNotes": this.deliveryNote
    }

    this.isButtonDisabled = true;
    if (this.signaturePad.signaturePad._data.length > 0) {
      let imageName = this.radioRemovalSystemDetails?.basicInfo?.receivingId + "-signature.jpeg";
      const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
      const imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' });
      formData.append("document_files", imageFile);
    }

    formData.append("documentDetails", JSON.stringify(data));

    let crudService: Observable<IApplicationResponse> =
    this.crudService.update(ModulesBasedApiSuffix.INVENTORY, 
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING_SUBMIT, formData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.navigateToList();
        this.isButtonDisabled = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.isButtonDisabled = false;
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }


  navigateToList(){
    this.router.navigate(['/inventory', 'order-receipts', 'receipting'], {
      queryParams: { tab: 4 },
      skipLocationChange: true
    });
  }

  navigateToEditPage() {
    this.router.navigate(["inventory/order-receipts/receipting/radio-removal-receiving-add-edit"], { 
      queryParams: { 
        id: this.radioRemovalReceiptId,
      }, skipLocationChange: true 
    });
  }
  barcodeScan(data){

  }

}
