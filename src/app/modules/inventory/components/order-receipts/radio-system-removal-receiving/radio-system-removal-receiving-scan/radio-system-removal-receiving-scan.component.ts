import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatRadioChange } from '@angular/material';
import { Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { RadioSystemRemovalItemsArrayModal, receiptingNewItemsScanModal } from '@modules/inventory/models/receipting-new-item.model';
import { DialogService } from 'primeng/api';
import { Observable } from 'rxjs';
import { BarcodePrintComponent } from '../../barcode-print';

@Component({
  selector: 'app-radio-system-removal-receiving-scan',
  templateUrl: './radio-system-removal-receiving-scan.component.html'
})
export class RadioSystemRemovalReceivingScanComponent implements OnInit {

  radioRemovalReceiptId: string;
  purchaseOrderId: string;
  receivingType: string;
  radioRemovalReceivingScanForm: FormGroup;
  showAddQuantity: boolean = false;
  scannedBarcodesCount: number = 0;
  getallBarcodesCount: number = 0;
  showItemCodeError: boolean = false;
  radioRemovalReceivingItemsArray: FormArray;
  printSerialNumbers: any = [];

  getReceivingScanItemDetails: any = {};
  radioSystemType: string = '';
  radioRemovalUserId: string = '';

  constructor(
    private router: Router, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private httpService: CrudService, private dialogService: DialogService,
    private crudService: CrudService, private dialog: MatDialog,
  ) {
    let getDetails = localStorage.getItem('radio_removal_receiving_scan_items');
    this.getReceivingScanItemDetails = JSON.parse(getDetails);
    this.getallBarcodesCount = this.getReceivingScanItemDetails.items.receivedQty +
      this.getReceivingScanItemDetails.items.outStandingQty;
    this.scannedBarcodesCount = this.getReceivingScanItemDetails.items.receivedQty;
    this.radioRemovalReceiptId = this.getReceivingScanItemDetails?.basicInfo?.radioRemovalReceiptId;
    this.radioRemovalUserId = this.getReceivingScanItemDetails?.radioRemovalUserId;
    this.radioSystemType = this.getReceivingScanItemDetails?.type;
    this.serialCheckedEvent(this.getReceivingScanItemDetails?.items?.isSerialNumberAvailable);
  }

  ngOnInit(): void {
    this.createRadioRemovalNewItemForm();
    let itemDetails = (this.getReceivingScanItemDetails.items.itemDetails == null ||
      this.getReceivingScanItemDetails.items.itemDetails.length < 0) ? null : this.getReceivingScanItemDetails.items.itemDetails;
    this.radioRemovalReceivingItemsArray = this.getNewScanItemsArray;
    if (itemDetails != null && itemDetails.length > 0) {
      itemDetails.forEach((items) => {
        this.radioRemovalReceivingItemsArray.push(this.createRadioItemsArrayModel(items));
      });
    }

    if (this.getallBarcodesCount === this.scannedBarcodesCount) {

    }

    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createRadioRemovalNewItemForm(): void {
    let stockOrderModel = new receiptingNewItemsScanModal();
    // create form controls dynamically from model class
    this.radioRemovalReceivingScanForm = this.formBuilder.group({
      radioRemovalReceivingItemsArray: this.formBuilder.array([])
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.radioRemovalReceivingScanForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    if (this.getReceivingScanItemDetails?.items?.isNotSerialized) {
      this.radioRemovalReceivingScanForm = setRequiredValidator(this.radioRemovalReceivingScanForm, ["receivedQty"]);
    }
  }

  //Create FormArray controls
  createRadioItemsArrayModel(interBranchModel?: RadioSystemRemovalItemsArrayModal): FormGroup {
    let interBranchModelData = new RadioSystemRemovalItemsArrayModal(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      formControls[key] = [interBranchModelData[key], (key === '') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getNewScanItemsArray(): FormArray {
    if (!this.radioRemovalReceivingScanForm) return;
    return this.radioRemovalReceivingScanForm.get("radioRemovalReceivingItemsArray") as FormArray;
  }

  serialCheckedEvent($event: MatRadioChange) {
    $event.value == 'true' ? this.showAddQuantity = true : this.showAddQuantity = true;
  }

  showQuantityError: boolean = false;

  getStockCount() {

    this.showQuantityError = false;
    let qty = this.radioRemovalReceivingScanForm.get('notReceivedQty').value;
    if (qty == '' || qty == null) {
      this.showQuantityError = true;
      return;
    }
    let countValue = parseInt(qty);
    let scannedValue = this.scannedBarcodesCount + countValue;
    if (countValue == 0) {
      this.snackbarService.openSnackbar("Invalid Stock count", ResponseMessageTypes.WARNING);
      return;
    }
    if (scannedValue <= this.getallBarcodesCount) {
      this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SERIAL_NUMBER_GENERATE, countValue)
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources != null) {
            let resp = response.resources;
            for (var i = 0; i < resp.length; i++) {
              this.addNewSerialNumberFun(response.resources[i], null);
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    else {
      this.snackbarService.openSnackbar("All serial numbers scanned for this stock code.", ResponseMessageTypes.WARNING);
    }

  }

  scanSerialNumber() {

    this.showItemCodeError = false;
    let scanvalue: boolean = false
    this.rxjsService.setFormChangeDetectionProperty(true);

    let scanInput = this.radioRemovalReceivingScanForm.get('scanSerialNumberInput').value;
    if (scanInput == '' || scanInput == null) {
      this.showItemCodeError = true;
      return;
    }
    this.radioRemovalReceivingItemsArray.value.forEach(e => {
      if (e.serialNumber == scanInput) {
        scanvalue = true;
      }
    });

    this.radioRemovalReceivingScanForm.get('scanSerialNumberInput').patchValue(null);
    if (scanvalue) {
      this.snackbarService.openSnackbar('Serial number scanned already', ResponseMessageTypes.WARNING);
      return;
    }
    if (this.scannedBarcodesCount >= this.getallBarcodesCount) {
      this.snackbarService.openSnackbar("All serial numbers scanned for this stock code.", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      let params = new HttpParams().set('SerialNumber', scanInput)
      this.httpService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING_CUSTOMER_DETAILS, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources != null) {
            this.addNewSerialNumberFun(scanInput, response.resources.customer);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.addNewSerialNumberFun(scanInput, null);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  addNewSerialNumberFun(value: string, customer: any) {
    if (value == undefined) return;
    let duplicate = this.radioRemovalReceivingItemsArray.value.filter(x => x.serialNumber === value);
    if (duplicate.length > 0) {
      this.snackbarService.openSnackbar("Serial Number '" + value + "' was already added", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.scannedBarcodesCount += 1;
      let newarray = {
        "serialNumber": value,
        "serialNumberChecked": false,
        "customer": customer != null ? customer : '-',
        "radioRemovalReceiptItemDetailId": null,
        "radioRemovalReceiptItemId": null,
        "barcode": value,
        "isDeleted": false
      }
      this.radioRemovalReceivingItemsArray.push(this.createRadioItemsArrayModel(newarray));
      this.radioRemovalReceivingScanForm.get('notReceivedQty').patchValue(null);
      this.radioRemovalReceivingScanForm.get('scanSerialNumberInput').patchValue(null);
    }
  }

  removeScanedBarcode(i: number, type: string) {

    let customText = "Are you sure you want to delete this?"
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: 'Confirmation',
      showHeader: false,
      closable: true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText },
    });
    confirm.onClose.subscribe((resp) => {
      if (resp === false) {
        if (type == 'new') {
          this.radioRemovalReceivingItemsArray.controls[i].get('isDeleted').patchValue(true);
          this.scannedBarcodesCount = this.scannedBarcodesCount - 1;
        }
        else {
          this.radioRemovalReceivingItemsArray.controls.forEach(control => {
            control.get('isDeleted').setValue(true);
          });
          this.scannedBarcodesCount = 0;
        }
      }
    });
  }

  onSelectAll(isChecked: boolean) {
    this.printSerialNumbers = [];
    this.radioRemovalReceivingItemsArray.controls.forEach(control => {
      control.get('serialNumberChecked').setValue(isChecked);
      if (control.get('serialNumberChecked').value) {
        this.printSerialNumbers.push(control.get('serialNumber').value);
      }
    });
  }

  print() {
    this.printSerialNumbers = [];
    this.radioRemovalReceivingItemsArray.value.forEach(prints => {
      if (prints.serialNumberChecked) {
        this.printSerialNumbers.push(prints.serialNumber);
      }
    });
    if (this.printSerialNumbers.length == 0) {
      this.snackbarService.openSnackbar("Select at least one Serial Number to print", ResponseMessageTypes.WARNING);
      return;
    }
    const dialogReff = this.dialog.open(BarcodePrintComponent,
      {
        width: '400px', height: '400px', disableClose: true,
        data: { serialNumbers: this.printSerialNumbers }
      });
  }

  onSubmit() {

    if (this.radioRemovalReceivingScanForm.invalid) {
      return;
    }

    let getValue = this.radioRemovalReceivingItemsArray.value.filter(x => x.isDeleted);

    if (!this.getReceivingScanItemDetails?.items?.isNotSerialized &&
      (this.radioRemovalReceivingItemsArray.value.length === 0 && getValue.length === 0)) {
      this.snackbarService.openSnackbar('"Atleast one record is required"', ResponseMessageTypes.WARNING);
      return;
    }

    let sendparams = {};
    sendparams = {
      "radioRemovalReceiptId": this.radioRemovalReceiptId,
      "warehouseId": this.getReceivingScanItemDetails?.basicInfo.warehouseId,
      "radioRemovalUserId": this.getReceivingScanItemDetails?.basicInfo.radioRemovalUserId,
      "techStockLocationId": this.getReceivingScanItemDetails?.basicInfo.techStockLocationId,
      "DeliveryNotes": this.getReceivingScanItemDetails?.deliveryNotes,
      "createdUserId": this.getReceivingScanItemDetails?.userId,
      "item": {
        "radioRemovalReceiptItemId": this.getReceivingScanItemDetails?.items?.radioRemovalReceiptItemId,
        "radioRemovalReceiptId": this.radioRemovalReceiptId,
        "itemId": this.getReceivingScanItemDetails?.items?.itemId,
        "onHandQuantity": this.getReceivingScanItemDetails?.items?.qtyOnHand,
        "receivedQty": this.getReceivingScanItemDetails?.items?.receivedQty,
        "outStandingQty": this.getReceivingScanItemDetails?.items?.outStandingQty,
        "unitCost": this.getReceivingScanItemDetails?.items?.unitCost,
        "isSerialNumberAvailable": this.showAddQuantity,
        "isToBeTested": this.getReceivingScanItemDetails?.items?.isToBeTested,
        "isToBeDisposed": this.getReceivingScanItemDetails?.items?.isToBeDisposed,
        "isNotSerialized": this.getReceivingScanItemDetails?.items?.isNotSerialized,
        "ItemDetails": this.radioRemovalReceivingItemsArray.value.length > 0 ? this.radioRemovalReceivingItemsArray.value : null
      }
    }

    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING, sendparams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        // this.radioRemovalReceiptId = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
        this.navigateToEdit();
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'order-receipts', 'receipting'], {
      queryParams: { tab: 0 },
      skipLocationChange: true
    });
  }

  navigateToEdit() {
    this.router.navigate(["inventory/order-receipts/receipting/radio-removal-receiving-add-edit"], {
      queryParams: {
        id: this.radioRemovalReceiptId,
        radioRemovalUserId: this.radioRemovalUserId,
        type: this.radioSystemType
      }, skipLocationChange: true
    });
  }

}
