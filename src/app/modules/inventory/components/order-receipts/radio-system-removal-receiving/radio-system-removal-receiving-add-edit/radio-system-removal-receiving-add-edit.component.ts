import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { radioRemovalReceivingAddEditModal } from '@modules/inventory/models/receipting-new-item.model';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { HttpParams } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-radio-system-removal-receiving-add-edit',
  templateUrl: './radio-system-removal-receiving-add-edit.component.html'
})
export class RadioSystemRemovalReceivingAddEditComponent implements OnInit {

  radioRemovalReceivingAddEditForm: FormGroup;
  usersDropDown: any = [];
  userData: UserLogin;
  radioRemovalSystemDetails: any = {};
  radioRemovalReceiptId: string = '';
  radioSystemType: string = '';
  radioRemovalUserId: string = '';

  constructor(
    private crudService: CrudService, private rxjsService: RxjsService, private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder, private store: Store<AppState>, private router: Router,
    private snackbarService: SnackbarService,
  ) {

    this.radioRemovalReceiptId = this.activatedRoute.snapshot.queryParams.id;
    this.radioRemovalUserId = this.activatedRoute.snapshot.queryParams.radioRemovalUserId;
    this.radioSystemType = this.activatedRoute.snapshot.queryParams.type;

    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.createRadioRemovalReceivingForm();
    this.getUsersDropdown();
    if (this.radioRemovalReceiptId && this.radioSystemType == 'view') {
      this.radioRemovalDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.radioRemovalReceivingAddEditForm.patchValue(response.resources.basicInfo);
          this.onUserChange(response?.resources?.basicInfo?.radioRemovalUserId);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
    else {
      if (this.radioRemovalUserId) {
        this.onUserChange(this.radioRemovalUserId);
      }
    }
  }

  /* Create Form Controls */
  createRadioRemovalReceivingForm(): void {
    let stockOrderModel = new radioRemovalReceivingAddEditModal();
    // create form controls dynamically from model class
    this.radioRemovalReceivingAddEditForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.radioRemovalReceivingAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.radioRemovalReceivingAddEditForm = setRequiredValidator(this.radioRemovalReceivingAddEditForm, ["radioRemovalUserId"]);
  }

  radioRemovalDetails(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('RadioRemovalReceiptId', this.radioRemovalReceiptId)
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING_DETAILS,
      undefined, true, params);
  }

  getUsersDropdown() {
    let params = new HttpParams().set('UserId', this.userData.userId)
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_RADIO_REMOVAL_RECEIVING_USERS, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.usersDropDown = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  onUserChange(value: string) {

    if (this.userData.warehouseId == null || this.userData.warehouseId == '') {
      this.snackbarService.openSnackbar("User account not setup with default warehouse", ResponseMessageTypes.WARNING);
      return;
    }

    if (value == '' || value == null) return;
    let params = new HttpParams().set('UserId', this.userData.userId).set('RadioRemovalUserId', value);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING_ITEMS, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode == 200) {
          this.radioRemovalSystemDetails = response.resources;
          this.radioRemovalReceivingAddEditForm.patchValue(response.resources.basicInfo);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  barcodeScan(itemsArray: any) {

    let receivingDetails = {};
    receivingDetails = {
      radioRemovalUserId: this.radioRemovalReceivingAddEditForm.get('radioRemovalUserId').value,
      type: this.radioSystemType,
      deliveryNotes: this.radioRemovalReceivingAddEditForm.get('deliveryNote').value,
      userId: this.userData.userId,
      basicInfo: this.radioRemovalSystemDetails.basicInfo,
      items: itemsArray,

    }
    const jsonData = JSON.stringify(receivingDetails);
    localStorage.removeItem('radio_removal_receiving_scan_items');
    localStorage.setItem('radio_removal_receiving_scan_items', jsonData);
    this.router.navigate(['inventory/order-receipts/receipting/radio-removal-receiving-scan'], {
      queryParams: {
        type: 'scan'
      }, skipLocationChange: true
    });

  }

  onSubmit() {

    let sendparams = {
      "RadioRemovalReceiptId": this.radioRemovalSystemDetails?.basicInfo?.radioRemovalReceiptId,
      "DeliveryNotes": this.radioRemovalReceivingAddEditForm.get('deliveryNote').value,
      "CreatedUserId": this.userData.userId,
    }

    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING_SAVE_AS_DRAFT, sendparams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.radioRemovalReceiptId = response.resources;
        this.navigateToList();
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  delete() {

    this.crudService.delete(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING,
      '',
      prepareRequiredHttpParams({
        ids: this.radioRemovalSystemDetails?.basicInfo?.radioRemovalReceiptId,
        modifiedUserId: this.userData.userId,
      })
    ).subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.onUserChange(null);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'order-receipts', 'receipting'], {
      queryParams: { tab: 4 },
      skipLocationChange: true
    });
  }

  navigateToViewPage() {
    this.router.navigate(["inventory/order-receipts/receipting/radio-removal-receiving-view"], {
      queryParams: {
        id: this.radioRemovalSystemDetails?.basicInfo?.radioRemovalReceiptId,
        type: this.radioSystemType
      }, skipLocationChange: true
    });
  }

  navigateToVerifyPage() {
    this.router.navigate(["inventory/order-receipts/receipting/radio-removal-receiving-verify"], {
      queryParams: {
        id: this.radioRemovalSystemDetails?.basicInfo?.radioRemovalReceiptId,
        userId: this.userData.userId,
        deliveryNote: this.radioRemovalReceivingAddEditForm.get('deliveryNote').value,
        type: this.radioSystemType
      }, skipLocationChange: true
    });
  }

}
