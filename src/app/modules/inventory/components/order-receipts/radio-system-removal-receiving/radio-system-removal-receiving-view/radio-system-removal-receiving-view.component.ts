import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-radio-system-removal-receiving-view',
  templateUrl: './radio-system-removal-receiving-view.component.html'
})
export class RadioSystemRemovalReceivingViewComponent implements OnInit {

  radioRemovalSystemDetails: any = {};
  radioRemovalReceiptId: string;
  radioSystemType: string = '';
  selectedTabIndex:any=0;
  constructor(
    private crudService: CrudService, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private activatedRoute: ActivatedRoute, private router: Router,
  ) { 
    this.radioRemovalReceiptId = this.activatedRoute.snapshot.queryParams.id;
    this.radioSystemType = this.activatedRoute.snapshot.queryParams.type;
  }

  ngOnInit(): void {
    if(this.radioRemovalReceiptId){
      this.radioRemovalDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.radioRemovalSystemDetails = response.resources;
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  radioRemovalDetails(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('RadioRemovalReceiptId', this.radioRemovalReceiptId)
    return this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING_DETAILS, 
    undefined, true, params);
  }

  stockDetailListDialog: boolean = false;
  stockDetails: any = {};

  openSerialInfoPopup(data: any){
    this.stockDetails = {
      stockCode: data.itemCode,
      stockDescription: data.itemName,
      serialNumbers: data.itemDetails
    }
    this.stockDetailListDialog = true;
  }

  navigateToList(){
    this.router.navigate(['/inventory', 'order-receipts', 'receipting'], {
      queryParams: { tab: 4 },
      skipLocationChange: true
    });
  }

  navigateToEditPage() {
    this.router.navigate(["inventory/order-receipts/receipting/radio-removal-receiving-add-edit"], { 
      queryParams: { 
        id: this.radioRemovalReceiptId,
        type: this.radioSystemType
      }, skipLocationChange: true 
    });
  }
  barcodeScan(data){

  }
}
