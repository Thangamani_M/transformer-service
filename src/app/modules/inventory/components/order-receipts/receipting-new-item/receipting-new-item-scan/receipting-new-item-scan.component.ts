import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { PrimengCustomDialogComponent } from '@app/shared/components/primeng-custom-dialog';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { receiptingNewItemsScanModal, ReceivingNewItemsArrayModal } from '@modules/inventory/models/receipting-new-item.model';
import { DialogService } from 'primeng/api';
import { Observable } from 'rxjs';
import { BarcodePrintComponent } from '../../barcode-print';

@Component({
  selector: 'app-receipting-new-item-scan',
  templateUrl: './receipting-new-item-scan.component.html'
})
export class ReceiptingNewItemScanComponent implements OnInit {

  orderReceiptBatchId: string;
  purchaseOrderId: string;
  receivingType: string;
  receivingNewDetails: any = {};
  receiptingNewScanForm: FormGroup;
  showAddQuantity: boolean = false;
  scannedBarcodesCount: number = 0;
  getallBarcodesCount: number = 0;
  showItemCodeError: boolean = false;
  receiptingNewItemsarray: FormArray;
  printSerialNumbers: any = [];
  barcodes: any = [];
  number = 1;
  screenType;
  constructor(
    private router: Router, private formBuilder: FormBuilder, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private httpService: CrudService, private dialogService: DialogService,
    private crudService: CrudService, private dialog: MatDialog,
  ) {
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.receivingNewDetails = this.router.getCurrentNavigation().extras.state.data;
      this.orderReceiptBatchId = this.router.getCurrentNavigation().extras.state.data.orderReceiptBatchId;
      this.purchaseOrderId = this.router.getCurrentNavigation().extras.state.data.orderReferenceId;
      this.receivingType = this.router.getCurrentNavigation().extras.state.data.type;
      this.screenType  = this.router.getCurrentNavigation().extras.state.data.screenType;
      this.getallBarcodesCount = this.router.getCurrentNavigation().extras.state.data.items.receivedQuantity +
        this.router.getCurrentNavigation().extras.state.data.items.outStandingQuantity;
      this.scannedBarcodesCount = this.router.getCurrentNavigation().extras.state.data.items.receivedQuantity;

    }
  }

  ngOnInit(): void {
    this.createReceiptingNewItemForm();
    let itemDetails = this.receivingNewDetails.items.itemDetails == undefined ? null : this.receivingNewDetails.items.itemDetails;
    this.receiptingNewItemsarray = this.getNewScanItemsArray;
    if (itemDetails != null && itemDetails.length > 0) {
      itemDetails.forEach((items) => {
        this.receiptingNewItemsarray.push(this.createReceivingItemsarrayModel(items));
      });
    }
    this.receiptingNewScanForm.get('receivedQty').patchValue(this.scannedBarcodesCount);
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createReceiptingNewItemForm(): void {
    let stockOrderModel = new receiptingNewItemsScanModal();
    // create form controls dynamically from model class
    this.receiptingNewScanForm = this.formBuilder.group({
      receiptingNewItemsarray: this.formBuilder.array([])
    });
    Object.keys(stockOrderModel).forEach((key) => {
      this.receiptingNewScanForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    if (this.receivingNewDetails?.items?.isNotSerialized) {
      this.receiptingNewScanForm = setRequiredValidator(this.receiptingNewScanForm, ["receivedQty"]);
    }
  }

  //Create FormArray controls
  createReceivingItemsarrayModel(interBranchModel?: ReceivingNewItemsArrayModal): FormGroup {
    let interBranchModelData = new ReceivingNewItemsArrayModal(interBranchModel);
    let formControls = {};
    Object.keys(interBranchModelData).forEach((key) => {
      formControls[key] = [interBranchModelData[key], (key === '') ? [Validators.required] : []]
    });
    return this.formBuilder.group(formControls);
  }

  //Create FormArray
  get getNewScanItemsArray(): FormArray {
    if (!this.receiptingNewScanForm) return;
    return this.receiptingNewScanForm.get("receiptingNewItemsarray") as FormArray;
  }

  serialCheckedEvent(checked) {
    checked ? this.showAddQuantity = true : this.showAddQuantity = false;
    this.showQuantityError = false; this.showItemCodeError = false;
  }

  showQuantityError: boolean = false;

  getStockCount() {

    this.showQuantityError = false;
    let qty = this.receiptingNewScanForm.get('notReceivedQty').value;
    if (qty == '' || qty == null) {
      this.showQuantityError = true;
      return;
    }
    let countValue = parseInt(qty);
    let scannedValue = this.scannedBarcodesCount + countValue;
    if (countValue == 0) {
      this.snackbarService.openSnackbar("Enter a valid quantity in the Received Qty field.", ResponseMessageTypes.WARNING);
      return;
    }
    if (scannedValue <= this.getallBarcodesCount) {
      this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SERIAL_NUMBER_GENERATE, countValue)
        .subscribe((response: IApplicationResponse) => {
          if (response.statusCode == 200 && response.resources != null) {
            let resp = response.resources;
            for (var i = 0; i < resp.length; i++) {
              this.addNewSerialNumberFun(response.resources[i]);
            }
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        });
    }
    else {
      this.snackbarService.openSnackbar("All serial numbers scanned for this stock code.", ResponseMessageTypes.WARNING);
    }

  }

  scanSerialNumber() {

    this.showItemCodeError = false;
    let scanvalue: boolean = false
    this.rxjsService.setFormChangeDetectionProperty(true);

    let scanInput = this.receiptingNewScanForm.get('scanSerialNumberInput').value;

    if (scanInput == '' || scanInput == null) {
      this.showItemCodeError = true;
      return;
    }
    else
      this.showItemCodeError = false;

    this.receiptingNewItemsarray.value.forEach(e => {
      if (e.serialNumber == scanInput) {
        scanvalue = true;
      }
    });
    this.receiptingNewScanForm.get('scanSerialNumberInput').patchValue(null);
    if (scanvalue) {
      this.snackbarService.openSnackbar('Serial number scanned already', ResponseMessageTypes.WARNING);
      return;
    }
    if (this.scannedBarcodesCount >= this.getallBarcodesCount) {
      this.snackbarService.openSnackbar("All serial numbers scanned for this stock code.", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.addNewSerialNumberFun(scanInput)
    }
  }

  addNewSerialNumberFun(value) {
    if (value == undefined) return;
    let duplicate = this.receiptingNewItemsarray.value.filter(x => x.serialNumber === value);
    if (duplicate.length > 0) {
      this.snackbarService.openSnackbar("Serial Number '" + value + "' was already added", ResponseMessageTypes.WARNING);
      return;
    }
    else {
      this.scannedBarcodesCount += 1;
      let newarray = {
        "serialNumber": value,
        "serialNumberChecked": false,
        // "serialNumberScan": false,
        "orderReceiptItemDetailId": null,
        "orderReceiptItemId": null,
        "barcode": value,
        "isDeleted": false
      }
      this.receiptingNewItemsarray.push(this.createReceivingItemsarrayModel(newarray));
      this.receiptingNewScanForm.get('notReceivedQty').patchValue(null);
      this.receiptingNewScanForm.get('scanSerialNumberInput').patchValue(null);
    }
  }

  removeScanedBarcode(index: number, type: string) {

    if (this.scannedBarcodesCount == 0) {
      this.snackbarService.openSnackbar("Deleted Item Should be Empty", ResponseMessageTypes.WARNING);
      return
    }

    let customText = "Are you sure you want to delete this?"
    const confirm = this.dialogService.open(PrimengCustomDialogComponent, {
      header: 'Confirmation',
      showHeader: false,
      closable: true,
      baseZIndex: 10000,
      width: '400px',
      data: { customText: customText },
    });
    confirm.onClose.subscribe((resp) => {
      if (resp === false) {
        if (type == 'new') {
          this.receiptingNewItemsarray.controls[index].get('orderReceiptItemId').value == null ?
            this.receiptingNewItemsarray.removeAt(index) : this.receiptingNewItemsarray.controls[index].get('isDeleted').patchValue(true);
          this.scannedBarcodesCount = this.scannedBarcodesCount - 1;
        }
        else {
          let receipting = this.receiptingNewItemsarray?.value;
          let checkedArray = [];
          this.receiptingNewItemsarray.controls.forEach(control => {
            if (control.get('serialNumberChecked').value) {
              control.get('isDeleted').setValue(true);
              checkedArray.push(control.get('isDeleted').value);
            } else {
              control.get('isDeleted').setValue(false);
            }
          });
          this.receiptingNewItemsarray.removeAt(this.receiptingNewItemsarray.value.findIndex(y => y.orderReceiptItemId == null));
          this.scannedBarcodesCount = receipting.length - checkedArray.length;
        }
      }
    });
  }

  onSelectAll(isChecked: boolean, i: number) {

    this.printSerialNumbers = [];
    if (i != undefined || i != null) {
      this.receiptingNewItemsarray.controls[i].get('serialNumberChecked').patchValue(isChecked);
    }
    this.receiptingNewItemsarray.controls.forEach(control => {
      if (i == undefined || i == null) {
        control.get('serialNumberChecked').setValue(isChecked);
      }
      if (control.get('serialNumberChecked').value) {
        this.printSerialNumbers.push(control.get('serialNumber').value);
      }
    });

  }

  print() {

    if (this.scannedBarcodesCount == 0) {
      this.snackbarService.openSnackbar("Serial Number Barcode Empty", ResponseMessageTypes.WARNING);
      return;
    }
    this.printSerialNumbers = [];
    this.receiptingNewItemsarray.value.forEach(prints => {
      if (prints.serialNumberChecked) {
        this.printSerialNumbers.push(prints.serialNumber);
      }
    });

    const dialogReff = this.dialog.open(BarcodePrintComponent,
      {
        width: '400px', height: '400px', disableClose: true,
        data: { serialNumbers: this.printSerialNumbers }
      });
  }
  navigateToJobSelection() {
    this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
 }

  onSubmit() {

    if (this.receiptingNewScanForm.invalid) {
      return;
    }

    let getValue = this.receiptingNewItemsarray.value.filter(x => x.isDeleted);
    if (!this.receivingNewDetails?.items?.isNotSerialized &&
      (this.receiptingNewItemsarray.value.length === 0 && getValue.length === 0)) {
      this.snackbarService.openSnackbar('"Atleast one record is required"', ResponseMessageTypes.WARNING);
      return;
    }

    if (this.receivingNewDetails?.items?.isNotSerialized) {
      if (this.receiptingNewScanForm.get('receivedQty').value <= 0) {
        this.snackbarService.openSnackbar('Invalid quantity', ResponseMessageTypes.WARNING);
        return;
      }
      if (this.receiptingNewScanForm.get('receivedQty').value > this.getallBarcodesCount) {
        this.snackbarService.openSnackbar("All serial numbers scanned for this stock code.", ResponseMessageTypes.WARNING);
        return;
      }
    }

    let sendparams = {
      "OrderReceiptId": this.receivingNewDetails.orderReceiptId,
      "OrderReferenceId": this.receivingNewDetails.orderReferenceId,
      "OrderReceiptBatchId": this.receivingNewDetails.orderReceiptBatchId,
      "DeliveryNote": this.receivingNewDetails.deliveryNote,
      "CreatedUserId": this.receivingNewDetails.createdUserId,
      "Item": {
        "OrderReceiptItemId": this.receivingNewDetails.items.orderReceiptItemId,
        "OrderItemId": this.receivingNewDetails.items.orderItemId,
        "Quantity": this.scannedBarcodesCount,
        "ConsumableQuantity": this.receivingNewDetails?.items?.isNotSerialized ?
          this.receiptingNewScanForm.get('receivedQty').value : 0,
        "IsSerialized": !this.receivingNewDetails?.items?.isNotSerialized ? true : false,
        "IsConsumable": this.receivingNewDetails?.items?.isNotSerialized ? true : false,
        "ItemDetails": this.receiptingNewItemsarray.value.length > 0 ? this.receiptingNewItemsarray.value : null
      }
    }

    let crudService: Observable<IApplicationResponse> =
      this.crudService.create(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RECEIVING_NEW_ITEMS, sendparams)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.orderReceiptBatchId = response.resources;
        this.navigateToEdit();
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'order-receipts', 'receipting'], {
      queryParams: { tab: 0 },
      skipLocationChange: true
    });
  }

  navigateToEdit() {
    this.router.navigate(["inventory/order-receipts/receipting/new-item-add-edit"], {
      queryParams: {
        orderReceiptBatchId: this.orderReceiptBatchId,
        orderReferenceId: this.purchaseOrderId,
        type: this.receivingType,
        screenType:this.screenType,
        deliveryNote: this.receivingNewDetails.deliveryNote
      }, skipLocationChange: true
    });
  }

}
