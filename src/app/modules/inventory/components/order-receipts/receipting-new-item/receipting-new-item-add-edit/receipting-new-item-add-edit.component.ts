import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { receiptingNewItemAddEditModal } from '@modules/inventory/models/receipting-new-item.model';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { BarcodePrintComponent } from '../../barcode-print';
@Component({
  selector: 'app-receipting-new-item-add-edit',
  templateUrl: './receipting-new-item-add-edit.component.html',
  styleUrls: ['./receipting-new-item-add-edit.component.scss']
})
export class ReceiptingNewItemAddEditComponent implements OnInit {

  receiptingNewItemAddEditForm: FormGroup;
  receptingItemsDetails: any = {};
  isNoChange: boolean = false;
  orderReceiptBatchId: string;
  purchaseOrderId: string;
  receivingType: string;
  userData: UserLogin;
  filteredPONumbers: any = [];
  showPONumberError: boolean = false;
  showReceptingItems: boolean = false;
  showItemCodeError: boolean = false;
  receiptingGetDetails: any = {};
  deleteConfirm: boolean = false;
  itemCode: any = [];
  deliveryNote: string = '';
  isDeliveryNote: boolean = false;
  stockCodes: any = [];
  screenType;
  time;
  @ViewChild('divClick', null) divClick: ElementRef;
  warningMessage = '';
  constructor(
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute,
    private crudService: CrudService, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private store: Store<AppState>,
    private httpService: CrudService, private router: Router,
    private dialog: MatDialog
  ) {
    this.orderReceiptBatchId = this.activatedRoute.snapshot.queryParams.orderReceiptBatchId;
    this.purchaseOrderId = this.activatedRoute.snapshot.queryParams.orderReferenceId;
    this.receivingType = this.activatedRoute.snapshot.queryParams.type;
    this.deliveryNote = this.activatedRoute.snapshot.queryParams.deliveryNote;
    this.screenType = this.activatedRoute.snapshot.queryParams.screenType;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.showItemCodeError = false;
    this.createReceiptingNewItemForm();
    this.getReceivingNewDetails();
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  createReceiptingNewItemForm(): void {
    let stockOrderModel = new receiptingNewItemAddEditModal();
    // create form controls dynamically from model class
    this.receiptingNewItemAddEditForm = this.formBuilder.group({});
    Object.keys(stockOrderModel).forEach((key) => {
      this.receiptingNewItemAddEditForm.addControl(key, new FormControl(stockOrderModel[key]));
    });
    this.receiptingNewItemAddEditForm = setRequiredValidator(this.receiptingNewItemAddEditForm, ["deliveryNote"]);
   // this.valueChange();
  }
  // valueChange(){
  //   this.receiptingNewItemAddEditForm.get("poBarcode")
  //   .valueChanges.subscribe((poBarcode: string) => {
  //     console.log('poBarcode',poBarcode)
  //     if(poBarcode)
  //         this.getReceiptingNewOrderItems(null, 'poBarcode','scan');
  //   });
  // }
  isReceivedQuantity: boolean = false;

  getReceivingNewDetails() {

    if (this.purchaseOrderId) {
      let params;
      if (this.orderReceiptBatchId != undefined || this.orderReceiptBatchId != null) {
        params = new HttpParams().set('OrderReceiptBatchId', this.orderReceiptBatchId)
          .set('PurchaseOrderId', this.purchaseOrderId).set('UserId', this.userData.userId);
      }
      else {
        params = new HttpParams()
          .set('PurchaseOrderId', this.purchaseOrderId).set('UserId', this.userData.userId);
      }

      this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.
        RECEIVING_NEW_ITEMS, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.showReceptingItems = true;
            this.receiptingGetDetails = response.resources;
            this.receptingItemsDetails = response.resources.items;
            this.isReceivedQuantity = response.resources.items.some(x => x.receivedQuantity != 0);
            this.receiptingNewItemAddEditForm.patchValue(response.resources);
            let poNumbers = {
              displayName: response.resources.poNumber,
              id: response.resources.orderReferenceId,
            }
            this.filteredPONumbers.push(poNumbers);
            this.receiptingNewItemAddEditForm.get('poNumber').patchValue(poNumbers);
            this.receiptingNewItemAddEditForm.get('deliveryNote').patchValue(this.deliveryNote ? this.deliveryNote : response.resources.deliveryNote ? response.resources.deliveryNote : null);
            if (response.resources.warningMessage != null) {
              this.snackbarService.openSnackbar(response.resources.warningMessage, ResponseMessageTypes.ERROR);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  /* PO Number dropdown */
  onPoNumberSelected(obj: any) {
    this.showPONumberError = false;
    if (obj.id == null) return;
    let params = new HttpParams().set('PurchaseOrderId', obj.id)
      .set('UserId', this.userData.userId);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.RECEIVING_NEW_ITEMS, undefined, true, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.showReceptingItems = true;
          this.receiptingGetDetails = response.resources;
          this.receptingItemsDetails = response.resources.items;
          this.isReceivedQuantity = response.resources.items.some(x => x.receivedQuantity != 0);
          this.receiptingNewItemAddEditForm.patchValue(response.resources);
          let poNumbers = {
            displayName: response.resources.poNumber,
            id: response.resources.orderReferenceId,
          }
          this.filteredPONumbers.push(poNumbers);
          this.receiptingNewItemAddEditForm.get('poNumber').patchValue(poNumbers);
          if (response.resources.warningMessage != null) {
            this.snackbarService.openSnackbar(response.resources.warningMessage, ResponseMessageTypes.ERROR);
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  inputChangePoNumberFilter(text: any) {

    let otherParams = {}
    this.showPONumberError = false;
    if (text.query == null || text.query == '') return;
    otherParams['displayName'] = text.query;
    otherParams['userId'] = this.userData.userId;
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.
      UX_PURCHASEORDER_BY_WEREHOUSE, null, false,
      prepareGetRequestHttpParams(null, null, otherParams))
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.isSuccess) {
          this.filteredPONumbers = response.resources;
          if (response.resources.length == 0) {
            this.showPONumberError = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  printstock(data) {
    let params = new HttpParams().set('ItemId', data.orderItemId);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.STOCKCODEBARCODE_DETAILS, null, null, params)
      .subscribe((response: IApplicationResponse) => {
        if (response.statusCode == 200 && response.resources != null) {
          this.stockCodes = new Array(response.resources.stockCodeBarcode);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    // this.stockCodes =new Array(data.itemCode);
    setTimeout(() => {
      this.divClick.nativeElement.click();
    }, 800);
  }
  Print(data) {
    this.itemCode = Array.of(data.itemCode)
    const dialogReff = this.dialog.open(BarcodePrintComponent,
      {
        width: '400px', height: '400px', disableClose: true,
        data: { serialNumbers: this.itemCode }
      });
  }

  getReceiptingNewOrderItems(obj: any, type: string,scan?:any,event?:any) {
    //  this.time = event?.time;
    // if(scan=='scan' && this.isDesktop()){
    //   return;
    // }

    if (this.userData.warehouseId == null || this.userData.warehouseId == '') {
      this.snackbarService.openSnackbar("User account not setup with default warehouse", ResponseMessageTypes.WARNING);
      return;
    }

    let params;
    this.showPONumberError = false;
    this.showItemCodeError = false;
    this.rxjsService.setFormChangeDetectionProperty(true);

    if (type == 'poNumber') {
      if (obj.id == null) return;
      params = new HttpParams().set('PurchaseOrderId', obj.id)
        .set('UserId', this.userData.userId);
    }
    else {
      if (this.receiptingNewItemAddEditForm.get('poBarcode').value == '' || this.receiptingNewItemAddEditForm.get('poBarcode').value == null) {
        this.showItemCodeError = true;
        return;
      }
      params = new HttpParams().set('POBarcode',
        this.receiptingNewItemAddEditForm.get('poBarcode').value)
        .set('UserId', this.userData.userId);
    }

    let crudService: Observable<IApplicationResponse> =
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RECEIVING_NEW_ITEMS, undefined, true, params)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        this.showReceptingItems = true;
        this.receiptingGetDetails = response.resources;
        this.receptingItemsDetails = response.resources.items;
        this.receiptingNewItemAddEditForm.patchValue(response.resources);
        this.isReceivedQuantity = response.resources.items.some(x => x.receivedQuantity != 0);
        let poNumbers = {
          displayName: response.resources.poNumber,
          id: response.resources.orderReferenceId,
        }
        this.filteredPONumbers.push(poNumbers);
        this.receiptingNewItemAddEditForm.get('poNumber').patchValue(poNumbers);
        if (response.resources.warningMessage != null || response.resources.items.length == 0) {
          this.warningMessage = response.resources.warningMessage ? response.resources.warningMessage : 'All stock was received on the PO.';
          response.resources.warningMessage = response.resources.warningMessage ? response.resources.warningMessage : 'All stock was received on the PO.';
          this.snackbarService.openSnackbar(response.resources.warningMessage, ResponseMessageTypes.ERROR);
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  saveAsDraft() {

    if(!this.receiptingNewItemAddEditForm.get('deliveryNote').value)
       return;
    
    this.isDeliveryNote = !this.receiptingNewItemAddEditForm.get('deliveryNote').value ? true : false;

    if (this.receiptingNewItemAddEditForm.invalid)
      return;

    let saveAsDraft = {
      OrderReceiptId: this.receiptingGetDetails.orderReceiptId,
      OrderReceiptBatchId: this.receiptingGetDetails.orderReceiptBatchId,
      CreatedUserId: this.userData.userId,
      DeliveryNote: this.receiptingNewItemAddEditForm.get('deliveryNote').value
    };

    this.httpService.create(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ORDER_RECEIPTS_SAVE_AS_DRAFT, saveAsDraft).subscribe((response) => {
        if (response.isSuccess && response.statusCode == 200) {
          // this.navigateToList();
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
  }

  delete() {
    this.deleteConfirm = true;
  }

  deleteReceptingOrder() {
    let body = {
      "ids": this.receiptingGetDetails.orderReceiptBatchId,
      "ModifiedUserId": this.userData.userId
    }
    this.httpService.delete(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ORDER_RECEIPTS, null, prepareRequiredHttpParams(body)).subscribe({
        next: response => {
          this.deleteConfirm = false;
          if (response.isSuccess && response.statusCode == 200) {
            // this.navigateToList();
            this.router.navigate(['/inventory/order-receipts/receipting']);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        }
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  barcodeScan(data) {

    if (this.receiptingNewItemAddEditForm.invalid) {
      this.isDeliveryNote = true;
      this.receiptingNewItemAddEditForm.markAllAsTouched();
      return;
    }
    else
      this.isDeliveryNote = false;

    let items = {
      orderReceiptBatchId: this.receiptingGetDetails.orderReceiptBatchId,
      orderReferenceId: this.receiptingGetDetails.orderReferenceId,
      orderReceiptId: this.receiptingGetDetails.orderReceiptId,
      type: this.receivingType,
      deliveryNote: this.receiptingNewItemAddEditForm.get('deliveryNote').value,
      createdUserId: this.userData.userId,
      screenType : this.screenType,
      items: data
    }
    this.router.navigate(['/inventory/order-receipts/receipting/new-item-scan'], {
      state: { data: items }
    });
  }

  navigateToList() {
    if(this.screenType == 'job-selection')
      this.navigateToJobSelection();
    else {
      this.router.navigate(['/inventory', 'order-receipts', 'receipting'], {
        queryParams: { tab: 0 }
      });
    } 
  }
  onClose() { }

  navigateToViewPage() {
    this.router.navigate(["inventory/order-receipts/receipting/new-item-view"], {
      queryParams: {
        orderReceiptBatchId: this.receiptingGetDetails.orderReceiptBatchId,
        orderReferenceId: this.receiptingGetDetails.orderReferenceId,
        screenType:this.screenType,
        type: this.receivingType
      }, skipLocationChange: true
    });
  }

  navigateToVerifyPage() {

    if(!this.receiptingNewItemAddEditForm.get('deliveryNote').value)
       return;
    
    this.isDeliveryNote = !this.receiptingNewItemAddEditForm.get('deliveryNote').value ? true : false;

    if (this.receiptingNewItemAddEditForm.invalid) {
      this.receiptingNewItemAddEditForm.markAllAsTouched();
      return;
    }

    this.router.navigate(["inventory/order-receipts/receipting/new-item-verify"], {
      queryParams: {
        orderReceiptBatchId: this.receiptingGetDetails.orderReceiptBatchId,
        orderReferenceId: this.receiptingGetDetails.orderReferenceId,
        deliveryNote: this.receiptingNewItemAddEditForm.get('deliveryNote').value,
        screenType:this.screenType,
        type: this.receivingType
      }, skipLocationChange: true
    });
  }
  navigateToJobSelection (){
    this.router.navigate(['/inventory/job-selection'], { queryParams: { tab: 0 } });
  }

}
