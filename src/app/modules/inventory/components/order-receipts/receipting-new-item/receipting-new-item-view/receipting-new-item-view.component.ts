import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes,currentComponentPageBasedPermissionsSelector$, RxjsService, SnackbarService, prepareDynamicTableTabsFromPermissions, LoggedInUserModel, PERMISSION_RESTRICTION_ERROR } from '@app/shared';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { BarcodePrintComponent } from '../../barcode-print';
import { combineLatest } from 'rxjs';
@Component({
  selector: 'app-receipting-new-item-view',
  templateUrl: './receipting-new-item-view.component.html'
})
export class ReceiptingNewItemViewComponent implements OnInit {

  orderReceiptBatchId: string;
  purchaseOrderId: string;
  receivingType: string;
  userData: UserLogin;
  receivingNewDetails: any = {}
  stockInfoDetailDialog: boolean = false;
  receivingNewStockDetails: any = {};
  receptingItemsDetails:any=[];
  receivingBarcode: any = [];
  poBarcode: any = [];
  primengTableConfigProperties: any;
  loggedInUserData:any;
  selectedTabIndex = 0;
  constructor(
    private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private httpService: CrudService, private rxjsService: RxjsService,
    private snackbarService: SnackbarService, private router: Router,
    private dialog: MatDialog,
  ) { 
    this.orderReceiptBatchId = this.activatedRoute.snapshot.queryParams.orderReceiptBatchId;
    this.purchaseOrderId = this.activatedRoute.snapshot.queryParams.orderReferenceId;
    this.receivingType = this.activatedRoute.snapshot.queryParams.type;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "New",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
          tabsList: [{
              enableBreadCrumb: true,
              enableAction: true,
              enableViewBtn: false,
              enableClearfix: true,
          }]
      }
  }
  }

  ngOnInit(): void {
    if(this.orderReceiptBatchId && this.purchaseOrderId){
      this.getReceivingNewDetails().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.receivingNewDetails = response.resources;
          this.receivingBarcode = new Array(response?.resources?.receivingBarcode);
          this.poBarcode = new Array(response?.resources?.poBarcode);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
        else {
          this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    }
    this.combineLatestNgrxStoreData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.RECEIVING];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  getReceivingNewDetails(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('OrderReceiptBatchId', this.orderReceiptBatchId)
    .set('PurchaseOrderId', this.purchaseOrderId)
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RECEIVING_NEW_DETAILS, 
    undefined, true, params);
  }

  openStockModal(data: any){
    this.receivingNewStockDetails = data;
    this.stockInfoDetailDialog = true;
  }

  navigateToList(){
    this.router.navigate(['/inventory', 'order-receipts', 'receipting'], {
      queryParams: { tab: 0 },
      skipLocationChange: true
    });
  }

  openBarcodeModal(batchBarcode){
    const dialogReff = this.dialog.open(BarcodePrintComponent, 
    { width: '400px', height: '400px', disableClose: true, 
      data: { serialNumbers: new Array(batchBarcode) } 
    });
  }

  navigateToEdit(){
    if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canEdit) {
      return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
    }
    this.router.navigate(["inventory/order-receipts/receipting/new-item-add-edit"], { 
      queryParams: { 
        orderReceiptBatchId: this.orderReceiptBatchId,
        orderReferenceId: this.purchaseOrderId,
        type: this.receivingType
      }, skipLocationChange: true 
    });
  }

}
