import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { CrudService, HttpCancelService, IApplicationResponse, ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, setRequiredValidator, SnackbarService } from '@app/shared';
import { loggedInUserData } from '@modules/others';
import { InventoryModuleApiSuffixModels } from '@modules/inventory';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { BarcodePrintComponent } from '../../barcode-print';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-receipting-new-item-verify',
  templateUrl: './receipting-new-item-verify.component.html',
  styleUrls: ['./receipting-new-item-verify.component.scss']
})
export class ReceiptingNewItemVerifyComponent implements OnInit {

  userData: UserLogin;
  orderReceiptBatchId: string;
  purchaseOrderId: string;
  receivingType: string;
  receiptingGetDetails: any = {};
  receptingItemsDetails: any = {};
  receivingNewStockDetails: any = {};
  stockInfoDetailDialog: boolean = false;
  receiptingNewVerifyForm: FormGroup;
  receivingDropdown: any = [];
  technicicanDropdown: any = [];
  isPrintSuccessDialog: boolean = false;
  barcodes: any = [];
  deliveryNote: string = '';
  screenType;
  @ViewChild(SignaturePad, { static: false }) signaturePad: any;

  public signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 500,
    'canvasHeight': 80
  };

  constructor(
    private store: Store<AppState>, private activatedRoute: ActivatedRoute,
    private httpService: CrudService, private snackbarService: SnackbarService,
    private rxjsService: RxjsService, private router: Router, private crudService: CrudService,
    private httpCancelService: HttpCancelService,
    private dialog: MatDialog, private formBuilder: FormBuilder,
  ) {
    this.orderReceiptBatchId = this.activatedRoute.snapshot.queryParams.orderReceiptBatchId;
    this.purchaseOrderId = this.activatedRoute.snapshot.queryParams.orderReferenceId;
    this.receivingType = this.activatedRoute.snapshot.queryParams.type;
    this.deliveryNote = this.activatedRoute.snapshot.queryParams.deliveryNote;
    this.screenType = this.activatedRoute.snapshot.queryParams.screenType;
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
    this.getReceivingNewDetails();
    this.createForm();
    this.getDropdown();
  }

  createForm() {

    this.receiptingNewVerifyForm = this.formBuilder.group({
      comments: ['', Validators.required],
      receivedFromTypeId: [null, Validators.required],
      technicianStockCollectionId:[''],
    });
    this.receiptingNewVerifyForm.get('receivedFromTypeId').valueChanges.subscribe(receivedFromTypeId => {
      let filter =  this.receivingDropdown.filter(x=> x.id== receivedFromTypeId && x.displayName=='Inventory Manager');

      if (filter?.length >0 ) {
        this.receiptingNewVerifyForm.get('technicianStockCollectionId').enable();  
        this.getTechnicianDropdown();      
      }else{
        this.receiptingNewVerifyForm.get('technicianStockCollectionId').setValue(null);  
        this.receiptingNewVerifyForm.get('technicianStockCollectionId').disable();        
      }
    });
    // this.receiptingNewVerifyForm = new FormGroup({
    //   'receivedFromTypeId': new FormControl(null),
    //   'comments': new FormControl(null),
    // });
    // this.receiptingNewVerifyForm = setRequiredValidator(this.receiptingNewVerifyForm, ['receivedFromTypeId','comments']);
  }

  getDropdown() {
    this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.
      UX_RECEIVING_FROM_TYPES)
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.receivingDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getTechnicianDropdown() {
    let params=null;
     params =  { PurchaseOrderId:this.purchaseOrderId,PendingCollectionForIM:true,userId:this.userData.userId};
    this.httpService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.
      UX_TECHNICIAN_STOCK_COLLECTION, prepareGetRequestHttpParams(null, null, params))
      .subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          this.technicicanDropdown = response.resources;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }
  getReceivingNewDetails() {
    if (this.purchaseOrderId) {
      let params = new HttpParams().set('OrderReceiptBatchId', this.orderReceiptBatchId)
        .set('PurchaseOrderId', this.purchaseOrderId)
      this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.
        RECEIVING_NEW_DETAILS, undefined, true, params)
        .subscribe((response: IApplicationResponse) => {
          if (response.isSuccess && response.statusCode === 200) {
            this.receiptingGetDetails = response.resources;
            this.receptingItemsDetails = response.resources.items;
            this.receiptingNewVerifyForm.patchValue(response.resources);
            this.barcodes = new Array(this.receiptingGetDetails?.receivingBarcode);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.snackbarService.openSnackbar("Something went wrong ", ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
    }
  }

  openStockModal(data: any) {
    this.receivingNewStockDetails = data;
    this.stockInfoDetailDialog = true;
  }

  drawComplete() {

  }

  clearPad() {
    this.signaturePad.clear();
  }

  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.resizeCanvas();
    this.signaturePad.clear();
  }

  isPrintSuccessMsg: string = '';

  onSubmit() {

    if (this.receiptingNewVerifyForm.invalid) {
      this.receiptingNewVerifyForm.markAllAsTouched();
      return;
    }

    if (this.signaturePad.signaturePad._data.length == 0) {
      this.snackbarService.openSnackbar("Please Enter the signature", ResponseMessageTypes.WARNING);
      return;
    }

    let verifyDetails = {
      "pONumber": this.receiptingGetDetails?.poNumber,
      "orderReceiptId": this.receiptingGetDetails.orderReceiptId,
      "orderReceiptBatchId": this.receiptingGetDetails.orderReceiptBatchId,
      "documentType": "Supplier Invoice",
      "comments": this.receiptingNewVerifyForm.get('comments').value,
      "orderTypeId": 11,
      "receivedFromTypeId": this.receiptingNewVerifyForm.get('receivedFromTypeId').value,
      "technicianStockCollectionId": this.receiptingNewVerifyForm.get('technicianStockCollectionId').value,
      "createdUserId": this.userData.userId,
      "deliveryNote": this.deliveryNote
    }

    const formData = new FormData();
    formData.append("receivingDetails", JSON.stringify(verifyDetails));
    if (this.signaturePad.signaturePad._data.length > 0) {
      let imageName = this.receiptingGetDetails.receivingId + ".jpeg";
      const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
      const imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' });
      formData.append('document_files[]', imageFile);
    }

    this.httpCancelService.cancelPendingRequestsOnFormSubmission();
    let crudService: Observable<IApplicationResponse> =
      this.crudService.update(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.RECEIVING_NEW_SUBMIT, formData)
    crudService.subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200) {
        if (this.receiptingGetDetails?.receivingBarcode != null && this.receiptingGetDetails?.receivingBarcode != '') {
          this.isPrintSuccessDialog = true;
          this.isPrintSuccessMsg = response.message;
          this.selectedIndex == 0;
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  selectedIndex: number = 0;

  openPrintSuccesDialog() {
    this.selectedIndex = 1;
    this.isPrintSuccessMsg = `Do you want to print Batch Barcode ${this.receiptingGetDetails['receivingBarcode']}?`;
  }

  dialogClose() {
    this.isPrintSuccessDialog = false;
    if(this.screenType=='job-selection')
      this.navigateToJobSelection();
    else
       this.navigateToList();
  }

  navigateToJobSelection() {
    this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
  }

  Print() {
    const dialogReff = this.dialog.open(BarcodePrintComponent,
      {
        width: '400px', height: '400px', disableClose: true,
        data: { serialNumbers: this.receiptingGetDetails?.receivingBarcode }
      });
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  navigateToList() {
    this.router.navigate(['/inventory', 'order-receipts', 'receipting'], {
      queryParams: { tab: 0 }
    });
  }

  navigateToEdit() {
    this.router.navigate(["inventory/order-receipts/receipting/new-item-add-edit"], {
      queryParams: {
        orderReceiptBatchId: this.orderReceiptBatchId,
        orderReferenceId: this.purchaseOrderId,
        type: this.receivingType,
        screenType:this.screenType,
      }, skipLocationChange: true
    });
  }

}
