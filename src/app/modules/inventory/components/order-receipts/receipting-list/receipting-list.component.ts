import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component } from '@angular/core';
import { async } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ReceivingListFilter } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import {
  CrudType, currentComponentPageBasedPermissionsSelector$, exportList, IApplicationResponse, ModulesBasedApiSuffix, PERMISSION_RESTRICTION_ERROR, prepareDynamicTableTabsFromPermissions, prepareGetRequestHttpParams,
  prepareRequiredHttpParams,
  ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { CrudService } from '@app/shared/services';
import { MomentService } from '@app/shared/services/moment.service';
import { ReceivingIntransitFilter } from '@modules/inventory/models/ReceivingIntransitFilter.model';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT, OrderTypes } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { DialogService } from 'primeng/api';
import { combineLatest, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { PrimeNgTableVariablesModel } from '../../../../../shared/models/prime-ng-table-list-component-variables.model';
@Component({
  selector: 'app-receipting-list',
  templateUrl: './receipting-list.component.html',
  styleUrls: ['./receipting-list.component.scss']
})
export class ReceiptingListComponent extends PrimeNgTableVariablesModel{
  observableResponse;
  primengTableConfigProperties: any;
  receivingFilterForm: FormGroup;
  receivingIntransitFilterForm: FormGroup;
  showFilterForm = false;
  warehouseList = [];
  stockOrderList = [];
  stockCodeList = [];
  statusList: any;
  purchaseOrderList = [];
  suppliersList = [];
  loggedUser: UserLogin;
  receivingClerkList = [];
  technicianDropdown: any = [];
  goodsRetrunDropdown: any = [];
  receivingClerksDropdown: any = [];
  row: any = {}
  filterData: any;
  first: any = 0;
  startTodayDate = 0;
  otherParams;
  newStatusList = [
    { value: 'R', display: 'Received' },
    { value: 'I', display: 'In progress' },
    { value: 'S', display: 'Save as Draft' },
  ]

  constructor(
    private crudService: CrudService, private momentService: MomentService,
    private activatedRoute: ActivatedRoute,  private datePipe: DatePipe,
    public dialogService: DialogService, private snackbarService: SnackbarService,
    private router: Router, private store: Store<AppState>, private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
  ) {
    super();
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.loggedUser = userData;
    });

    this.primengTableConfigProperties = {
      tableCaption: "Receiving List",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management - Receiving', relativeRouterUrl: '' }, { displayName: 'Receiving List', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'New',
            dataKey: 'stockId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            enablePrintBtn: true,
            printTitle: 'New',
            printSection: 'print-section0',
            columns: [
              { field: 'receivingId', header: 'Receiving ID', width: '200px' },
              { field: 'status', header: 'Status', width: '200px', type: 'dropdown',
              options:[{ label: 'Received', value:'R' },{ label: 'In progress', value:'I' },{ label: 'Save as Draft', value:'S' }] },
              { field: 'receivingBarcode', header: 'Receiving Barcode', width: '200px' },
              { field: 'poNumber', header: 'PO Number', width: '200px' },
              { field: 'poBarcode', header: 'PO Barcode', width: '200px' },
              { field: 'warehouse', header: 'Warehouse', width: '200px' },
              { field: 'receivingClerk', header: 'Receiving Clerk', width: '200px' },
              { field: 'receivedDate', header: 'Received  On', width: '200px', type: 'date' },
              { field: 'supplier', header: 'Supplier', width: '200px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.RECEIVING_NEW,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            exportApi:InventoryModuleApiSuffixModels.RECEVING_EXPORT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableExportBtn: true,
            shouldShowFilterActionBtn: true,
            disabled:true
          },
          {
            caption: 'Technician',
            dataKey: 'requestId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            enablePrintBtn: true,
            printTitle: 'Technician',
            printSection: 'print-section1',
            columns: [
              { field: 'receivingId', header: 'Receiving ID', width: '200px' },
              { field: 'status', header: 'Status', width: '200px' },
              { field: 'barcodeNumber', header: 'Receiving Barcode', width: '200px' },
              { field: 'qrCode', header: 'QR code', width: '200px' },
              { field: 'orderTypeName', header: 'Receiving Type', width: '200px' },
              { field: 'referenceId', header: 'Reference ID', width: '200px' },
              { field: 'technician', header: 'Technician Name', width: '200px' },
              { field: 'warehouseName', header: 'Warehouse', width: '200px' },
              { field: 'storageLocationName', header: 'Storage Location', width: '200px' },
              { field: 'receivingClerk', header: 'Receiving Clerk', width: '200px' },
              { field: 'receivedOn', header: 'Received Date', width: '200px' },

            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.ORDERRECEIVE_TECHNICIAN,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            exportApi:InventoryModuleApiSuffixModels.TECHNICAL_ORDER_RECEVING_EXPORT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableExportBtn: true,
            shouldShowFilterActionBtn: true,
            disabled:true
          },
          {
            caption: 'Supplier Return',
            dataKey: 'requestId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableAction: true,
            enableExportCSVSelected: false,
            enableReset: false,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            enablePrintBtn: true,
            enableExportBtn: true,
            printTitle: 'Supplier Return',
            printSection: 'print-section2',
            columns: [
              { field: 'requestId', header: 'Supplier Return No', width: '200px' },
              { field: 'status', header: 'Status', width: '200px' },
              { field: 'orderNumber', header: 'Reference ID', width: '200px' },
              { field: 'batchNumber', header: 'Batch Number', width: '200px' },
              { field: 'storageLocationName', header: 'Sub Location', width: '200px' },
              { field: 'receivingClerk', header: 'Receiving Clerk', width: '200px' },
              { field: 'receivedOn', header: 'Received On', width: '200px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.ORDERRECEIVE_SUPPLIERRETURN,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            exportApi:InventoryModuleApiSuffixModels.SUPPLIER_RETURN_EXPORT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: false,
            shouldShowFilterActionBtn: false,
            disabled:true
          },
          {
            caption: 'Inter Branch Transfer',
            dataKey: 'requestId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            enablePrintBtn: true,
            printTitle: 'Inter Branch Transfer',
            printSection: 'print-section3',
            columns: [
              { field: 'receivingId', header: 'Receiving ID', width: '200px' },
              { field: 'status', header: 'Status', width: '200px' },
              { field: 'referenceId', header: 'IBT Request Number', width: '200px' },
              { field: 'receivingBarCode', header: 'Receiving BarCode', width: '200px' },
              { field: 'requestWarehouseName', header: 'Request Warehouse', width: '200px' },
              { field: 'issueWarehouseName', header: 'Issue Warehouse', width: '200px' },
              { field: 'receivingClerk', header: 'Receiving Clerk', width: '200px' },
              { field: 'receivedOn', header: 'Received Date', width: '200px' },

            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.ORDERRECEIV_INTRANSIT,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            exportApi:InventoryModuleApiSuffixModels.ORDER_RECEIPTS_INTRANSIT_EXPORT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            shouldShowFilterActionBtn: true,
            enableExportBtn: true,
            isDateWithTimeRequired: true,
          },
          {
            caption: 'Radio System Removal',
            dataKey: 'radioRemovalReceiptId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            enablePrintBtn: true,
            printTitle: 'Radio System Removal',
            printSection: 'print-section4',
            columns: [
              { field: 'receivingId', header: 'Receiving ID', width: '200px' },
              { field: 'status', header: 'Status', width: '200px' },
              { field: 'receivingBarcode', header: 'Receiving Barcode', width: '200px' },
              { field: 'warehouse', header: 'Warehouse', width: '200px' },
              { field: 'receivingClerk', header: 'Receiving Clerk', width: '200px' },
              { field: 'receivedDate', header: 'Received On', width: '200px' },
              { field: 'radioRemovalUser', header: 'Radio Removal User', width: '200px' },
              { field: 'techStockLocation', header: 'Tech Stock Location', width: '200px' },
              { field: 'techStockLocationName', header: 'Tech Stock Location Name', width: '200px' },
            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.RADIO_REMOVAL_RECEIVING,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            exportApi:InventoryModuleApiSuffixModels.RADIO_REMOVEL_EXPORT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableExportBtn: true,
            shouldShowFilterActionBtn: false,
            disabled:true
          },
          {
            caption: 'Transfer Request',
            dataKey: 'orderReferenceId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableAction: true,
            enableGlobalSearch: false,
            reorderableColumns: false,
            resizableColumns: false,
            enableScrollable: true,
            checkBox: false,
            enableRowDelete: false,
            enableStatusActiveAction: false,
            enableFieldsSearch: true,
            rowExpantable: false,
            rowExpantableIndex: 0,
            enableHyperLink: true,
            cursorLinkIndex: 0,
            enableSecondHyperLink: false,
            cursorSecondLinkIndex: 1,
            enablePrintBtn: true,
            printTitle: 'Transfer Request',
            printSection: 'print-section5',
            columns: [
              { field: 'requestNumber', header: 'Transfer Request Number', width: '200px' },
              { field: 'status', header: 'Status', width: '200px' },
              { field: 'transferBarcode', header: 'Transfer Barcode', width: '200px' },
              { field: 'issuingWarehouse', header: 'Issuing Warehouse', width: '200px' },
              { field: 'issuingLocation', header: 'Issuing Location', width: '200px' },
              { field: 'receivingWarehouse', header: 'Receiving Warehouse', width: '200px' },
              { field: 'receivingLocation', header: 'Receiving Location', width: '200px' },
              { field: 'receivedBy', header: 'Received By', width: '200px' },
              { field: 'receivedDate', header: 'Received Date', width: '200px' },

            ],
            apiSuffixModel: InventoryModuleApiSuffixModels.TRANSFER_REQUEST_RECEIVING,
            moduleName: ModulesBasedApiSuffix.INVENTORY,
            exportApi:InventoryModuleApiSuffixModels.TRANSFER_REQUEST_RECEIVING_EXPORT,
            enableMultiDeleteActionBtn: false,
            enableAddActionBtn: true,
            enableExportBtn: true,
            shouldShowFilterActionBtn: false,
            disabled:true
          }
        ]
      }
    }
    this.activatedRoute.queryParamMap.subscribe((params) => {
      this.selectedTabIndex = (Object.keys(params['params']).length > 0) ? +params['params']['tab'] : 0;
      this.primengTableConfigProperties.selectedTabIndex = this.selectedTabIndex;
      this.primengTableConfigProperties.breadCrumbItems[1].displayName = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;
    });
  }

  ngOnInit(): void {
    this.combineLatestNgrxStoreData()
    this.getRequiredListData();
    this.getWarehouses();
    // if (this.selectedTabIndex == 0 || this.selectedTabIndex == 1) {
      this.createReceivingFilterForm();
      this.getDropdownList();
      this.statusList = [{ value: "0", display: "In Progress" }, { value: "1", display: "Received" }];
      this.getSelectedWarehouseOptions();
      this.getInterBranchDropdownList();
    // }
    this.createReceivingIntransitFilterForm();
    this.getInterBranchDropdownList();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      let permission = response[0][INVENTORY_COMPONENT.RECEIVING]
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        // this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }

  onActionSubmited(e: any) {
    if (e.data && !e.search && !e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data);
    } else if (e.data && e.search && !e?.col) {
        this.onCRUDRequested(e.type, e.data, e.search);
    } else if (e.type && !e.data && !e?.col) {
        this.onCRUDRequested(e.type, {});
    } else if (e.type && e.data && e?.col?.toString()) {
        this.onCRUDRequested(e.type, e.data, e?.col);
    }
  }

  onTabChange(event) {
    this.dataList = [];
    this.totalRecords = null;
    this.selectedTabIndex = event.index;
    this.router.navigate(['/inventory', 'order-receipts', 'receipting'], { queryParams: { tab: this.selectedTabIndex } })

    if(this.loggedUser.warehouseId == null || this.loggedUser.warehouseId == ''){
      this.snackbarService.openSnackbar("User account not setup with default warehouse", ResponseMessageTypes.WARNING);
      this.loading = false;
      setTimeout(() => { this.rxjsService.setGlobalLoaderProperty(false); }, 500)
    }
    else {
      this.getRequiredListData();
    }
    this.filterData = null;
    this.primengTableConfigProperties.breadCrumbItems =
    [{ displayName: 'Inventory Management', relativeRouterUrl: '' },
    { displayName: this.selectedTabIndex == 0 ? 'New' : this.selectedTabIndex == 1 ? 'Technician' :
    this.selectedTabIndex == 2 ? 'Supplier Return' : this.selectedTabIndex == 3 ? 'Inter  Branch Transfer' :
    this.selectedTabIndex == 4 ? 'Radio System Removal' : 'Transfer Request',
    relativeRouterUrl: '/inventory/order-receipts/receipting' }];
    this.loading = false;
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getRequiredListData(pageIndex?: string, pageSize?: string, otherParams?: object) {
    if(this.loggedUser.warehouseId == null || this.loggedUser.warehouseId == ''){
      this.snackbarService.openSnackbar("User account not setup with default warehouse", ResponseMessageTypes.WARNING);
      this.loading = false;
      setTimeout(() => { this.rxjsService.setGlobalLoaderProperty(false); }, 500);
      return;
    }
    this.loading = true;
    let InventoryModuleApiSuffixModels: InventoryModuleApiSuffixModels;
    InventoryModuleApiSuffixModels = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].apiSuffixModel;
    if(otherParams) {
    } else {
      if(this.selectedTabIndex == 0 || this.selectedTabIndex == 1 ||
        this.selectedTabIndex == 2){
        otherParams = {
          userId: this.loggedUser.userId,
          IsAll: true,
        };
      } else if(this.selectedTabIndex == 3) {
        otherParams = {
          warehouseId: this.loggedUser.warehouseId,
          userId: this.loggedUser.userId,
          IsAll: true,
        };
      }
      else {
        otherParams = {
          userId: this.loggedUser.userId,
        };
      }
    }
    this.otherParams = otherParams

    this.crudService.get(
      ModulesBasedApiSuffix.INVENTORY,InventoryModuleApiSuffixModels,
      undefined,false, prepareGetRequestHttpParams(pageIndex, pageSize, otherParams)
    ).pipe(map((res: IApplicationResponse) => {
      if (res?.resources) {
        res?.resources?.forEach(val => {
          if(this.selectedTabIndex==0)
             val.receivedDate = val.receivedDate ? this.datePipe.transform(val.receivedDate, 'yyyy-MM-dd HH:mm:ss'):null; // , HH:mm:ss
          else
             val.receivedOn = val.receivedOn ? this.datePipe.transform(val.receivedOn, 'yyyy-MM-dd HH:mm:ss'):null; // , HH:mm:ss
          return val;
        })
      }
      return res;
    })).subscribe(data => {
      this.loading = false;
      this.rxjsService.setGlobalLoaderProperty(false);
      if (data.isSuccess) {
        this.observableResponse = data.resources;
        this.dataList = this.observableResponse;

        this.totalRecords = data.totalCount;
      } else {
        this.observableResponse = null;
        this.dataList = this.observableResponse
        this.totalRecords = 0;
      };
    });

  }

  onCRUDRequested(type: CrudType | string, row?: object | any, unknownVar?: any): void {
    switch (type) {
        case CrudType.CREATE:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canCreate) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
           }
            this.openAddEditPage(CrudType.CREATE, row);
            break;
        case CrudType.GET:
            this.row = row ? row : { pageIndex: 0, pageSize: 10 };
            this.first = row?.pageIndex && row?.pageSize ? row["pageIndex"] * row["pageSize"] : 0;
            if(this.selectedTabIndex == 0 || this.selectedTabIndex == 1 ||
              this.selectedTabIndex == 2){
              unknownVar = { ...this.filterData, ...unknownVar,
                userId: this.loggedUser.userId,
                IsAll: true,
              }
            }
            else if(this.selectedTabIndex == 3){
              unknownVar = { ...this.filterData, ...unknownVar,
                warehouseId: this.loggedUser.warehouseId,
                userId: this.loggedUser.userId,
                IsAll: true,
              }
            }
            else {
              unknownVar = { ...this.filterData, ...unknownVar,
                userId: this.loggedUser.userId,
              }
            }
            if(unknownVar['receivedOn'])
              unknownVar['receivedOn'] =  this.momentService.toMoment(unknownVar['receivedOn']).format('YYYY-MM-DD');

            this.getRequiredListData(this.row["pageIndex"], this.row["pageSize"], unknownVar)
            break;
        case CrudType.EDIT:
            this.openAddEditPage(CrudType.VIEW, row);
            break;
        case CrudType.VIEW:
            this.openAddEditPage(CrudType.VIEW, row);
            break;
        case CrudType.FILTER:
          this.showFilterForm = !this.showFilterForm;
          // this.displayAndLoadFilterData();
          break;
        case CrudType.EXPORT:
          if (!this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].canExport) {
            return this.snackbarService.openSnackbar(PERMISSION_RESTRICTION_ERROR, ResponseMessageTypes.WARNING);
           }
          let pageIndex =  this.row && this.row ["pageIndex"] ?  this.row ["pageIndex"]:0;
       let pageSize  =  this.row && this.row["pageSize"] ? this.row["pageSize"] : 10;
       this.exportList(pageIndex,pageSize);
        break;
        default:
    }
  }
  exportList(pageIndex?: any, pageSize?: any) {
    let exportAPI = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].exportApi;
    let caption = this.primengTableConfigProperties.tableComponentConfigs.tabsList[this.selectedTabIndex].caption;

    exportList(this.otherParams,0,pageSize,ModulesBasedApiSuffix.INVENTORY,
      exportAPI,this.crudService,this.rxjsService,caption);
  }

  openAddEditPage(type: CrudType | string, editableObject?: object | string, index?: number): void {
    switch (type) {

      case CrudType.CREATE:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["inventory/order-receipts/receipting/new-item-add-edit"],{ queryParams:
              { type: 'create' },
              skipLocationChange: true });
            break;
          case 1:
            this.router.navigate(["inventory/order-receipts/receipting/technician-receiving-goods-return-edit"], { queryParams:
              { orderTypeId: OrderTypes.NEW,
                isActiveNew: true,
              }, skipLocationChange: true
            });
            break;
          case 3:
            this.router.navigate(['inventory/order-receipts/receipting/intransit-add-edit'], { queryParams: { orderTypeId: OrderTypes.INTRANSIT }, skipLocationChange: true });
          break;
          case 4:
            this.router.navigate(['inventory/order-receipts/receipting/radio-removal-receiving-add-edit'], {
              queryParams: {
                type: 'create'
              }, skipLocationChange: true
            });
          break;
          case 5:
            this.router.navigate(['inventory/order-receipts/receipting/receiving-transfer-request-add-edit'], {
              queryParams: {}, skipLocationChange: true
            });
          break;
        }
        break;
      case CrudType.VIEW:
        switch (this.selectedTabIndex) {
          case 0:
            this.router.navigate(["inventory/order-receipts/receipting/new-item-view"], {
              queryParams: {
                orderReceiptBatchId: editableObject['orderReceiptBatchId'],
                orderReferenceId: editableObject['orderReferenceId'],
                type: 'view'
              }, skipLocationChange: true
            });
            break;
          case 1:
            let OrderTypeId: string = editableObject['orderTypeId'].toString();
            if (OrderTypeId === OrderTypes.FAULTY) {
              this.router.navigate(['inventory/order-receipts/receipting/technician-receiving-view'],
              { queryParams:
                {
                  RTRRequestId: editableObject['orderReferenceId'],
                  OrderTypeId: editableObject['orderTypeId'],
                  orderReceiptId: editableObject['orderReceiptId'],
                  isActiveNew: false,
                }, skipLocationChange: true
              });
            }
            else if (OrderTypeId === OrderTypes.GOODSRETURN) {
              this.router.navigate(['inventory/order-receipts/receipting/technician-receiving-goods-return-view'], { queryParams:
                { GoodsReturnId: editableObject['orderReferenceId'],
                OrderTypeId: editableObject['orderTypeId'],
                orderReceiptId: editableObject['orderReceiptId'],
                isActiveNew: false,
              }, skipLocationChange: true });
            }
            break;
          case 2:
            this.router.navigate(['inventory/order-receipts/receipting/supplier-return-receiving-view'], { queryParams: { orderReferenceId: editableObject['orderReferenceId'], orderReceiptId: editableObject['orderReceiptId'] }, skipLocationChange: true });
            break;
          case 3:
            this.router.navigate(['inventory/order-receipts/receipting/intransit-view'], { queryParams: { id: editableObject['interBranchStockOrderId'], batchId: editableObject['orderReceiptBatchId'],orderReceiptId: editableObject['orderReceiptId'],warehouseId: editableObject['requestWarehouseId']  }, skipLocationChange: true });
            break;
          case 4:
            this.router.navigate(['inventory/order-receipts/receipting/radio-removal-receiving-view'], {
              queryParams: {
                id: editableObject['radioRemovalReceiptId'],
                type: 'view'
              }, skipLocationChange: true
            });
          break;
          case 5:
            this.router.navigate(['inventory/order-receipts/receipting/receiving-transfer-request-view'], {
              queryParams: {
                id: editableObject['orderReferenceId'],
                batchId: editableObject['orderReceiptBatchId'],
              }, skipLocationChange: true
            });
          break;
        }
        break;
    }
  }

  displayAndLoadFilterData() {

    if (this.selectedTabIndex == 0 || this.selectedTabIndex == 1) {
    //  this.createReceivingFilterForm();
      this.getDropdownList();
      this.statusList = [{ value: "0", display: "In Progress" }, { value: "1", display: "Received" }];
      this.getSelectedWarehouseOptions();
      this.getInterBranchDropdownList();
    }
    else if (this.selectedTabIndex == 3) {
     // this.createReceivingIntransitFilterForm();
      this.getInterBranchDropdownList();
    }
    this.showFilterForm = !this.showFilterForm;
  }

  getWarehouses(){
    let params = new HttpParams().set('UserId', this.loggedUser.userId);
    this.crudService.get(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_WAREHOUSES, undefined, null, params)
      .subscribe((response: IApplicationResponse) => {
      if (response.resources && response.statusCode === 200) {
        let warehouseList = response.resources;
        this.warehouseList = [];
        this.rxjsService.setGlobalLoaderProperty(false);
        for (var i = 0; i < warehouseList.length; i++) {
          if(warehouseList[i].displayName != null){
            let tmp = {};
            tmp['value'] = warehouseList[i].id;
            tmp['display'] = warehouseList[i].displayName;
            this.warehouseList.push(tmp)
          }
        }
      }
    });
  }

  getInterBranchDropdownList() {
   let params = new HttpParams().set('Id', this.loggedUser.userId);
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEMS_BY_INTERBRANCH,params),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RECEIVINGCLERK_BY_INTERBRANCH,params),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_INTERBRANCH_ORDER_STATUS,params),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.stockCodeList = resp.resources;
              // this.stockCodeList = [];
              // let stockCodeList=[];
              // this.rxjsService.setGlobalLoaderProperty(true);
              // for (var i = 0; i < stockCodeList.length; i++) {
              //   if(stockCodeList[i].itemCode != null){
              //     let tmp = {};
              //     tmp['value'] = stockCodeList[i].id;
              //     tmp['display'] = stockCodeList[i].itemCode;
              //     this.stockCodeList.push(tmp);
              //   }
              // }
              break;
            case 1:
              let receivingClerkList = resp.resources;
              this.receivingClerkList = [];
              for (var i = 0; i < receivingClerkList.length; i++) {
                if(receivingClerkList[i].displayName != null){
                  let tmp = {};
                  tmp['value'] = receivingClerkList[i].id;
                  tmp['display'] = receivingClerkList[i].displayName;
                  this.receivingClerkList.push(tmp)
                }
              }
              break;
            case 2:
              let statusResponse = resp.resources;
              this.statusList = [];
              for (var i = 0; i < statusResponse.length; i++) {
                if(statusResponse[i].displayName != null){
                  let tmp = {};
                  tmp['value'] = statusResponse[i].id;
                  tmp['display'] = statusResponse[i].displayName;
                  this.statusList.push(tmp)
                }
              }
              break;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }else {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })

    });
  }

  getDropdownList() {
    this.receivingClerksDropdown = [];
    this.goodsRetrunDropdown = [];
    let param = new HttpParams().set('Role', "Technician");
    forkJoin([
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_TECHNICIAN, param),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_GOODS_RETURN),
      this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RECEIVING_CLERKS),
    ]).subscribe((response: IApplicationResponse[]) => {
      response.forEach((resp: IApplicationResponse, ix: number) => {
        if (resp.isSuccess && resp.statusCode === 200) {
          switch (ix) {
            case 0:
              this.technicianDropdown = resp.resources;
              break;
            case 1:
              let goodsResponse = resp.resources;
              for (var i = 0; i < goodsResponse.length; i++) {
                if(goodsResponse[i].displayName != null){
                  let tmp = {};
                  tmp['value'] = goodsResponse[i].id;
                  tmp['display'] = goodsResponse[i].displayName;
                  this.goodsRetrunDropdown.push(tmp)
                }
              }
              break;
            case 2:
              let receiving = resp.resources;
              for (var i = 0; i < receiving.length; i++) {
                if(receiving[i].displayName != null){
                  let tmp = {};
                  tmp['value'] = receiving[i].id;
                  tmp['display'] = receiving[i].displayName;
                  this.receivingClerksDropdown.push(tmp)
                }
              }
              break;
          }
        }
      })
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  getReceivingClerksOptions(receiving: string): void {
    if (receiving != '') {
      var params;
      if(this.receivingFilterForm.get('techwarehouseIds').value.length > 0){
        params = new HttpParams().set('WarehouseIds', this.receivingFilterForm.get('techwarehouseIds').value)
      }
      if(this.receivingFilterForm.get('goodsReturnNumbers').value.length > 0){
        params = new HttpParams().set('ReferenceIds', this.receivingFilterForm.get('goodsReturnNumbers').value);
      }
      if(this.receivingFilterForm.get('techwarehouseIds').value.length > 0 &&
        this.receivingFilterForm.get('goodsReturnNumbers').value.length > 0) {
        params = new HttpParams()
        .set('WarehouseIds', this.receivingFilterForm.get('techwarehouseIds').value)
        .set('ReferenceIds', this.receivingFilterForm.get('goodsReturnNumbers').value)
        .set('Id', this.loggedUser.userId);
      }
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RECEIVING_CLERKS, undefined, true, params).subscribe((response: IApplicationResponse) => {
        if (response.resources && response.statusCode === 200) {
          let receivinglist = response.resources;
          this.receivingClerksDropdown = [];
          for (var i = 0; i < receivinglist.length; i++) {
            if(receivinglist[i].displayName != null){
              let tmp = {};
              tmp['value'] = receivinglist[i].id;
              tmp['display'] = receivinglist[i].displayName;
              this.receivingClerksDropdown.push(tmp)
            }
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      });
    }
  }

  createReceivingFilterForm(receivingListModel?: ReceivingListFilter) {
    let receivingListFilterModel = new ReceivingListFilter(receivingListModel);
    this.receivingFilterForm = this.formBuilder.group({});
    Object.keys(receivingListFilterModel).forEach((key) => {
      if (typeof receivingListFilterModel[key] === 'string') {
        this.receivingFilterForm.addControl(key, new FormControl(receivingListFilterModel[key]));
      }
    });
  }

  createReceivingIntransitFilterForm(receivingIntransitListModel?: ReceivingIntransitFilter) {
    let receivingIntransitListFilterModel = new ReceivingIntransitFilter(receivingIntransitListModel);
    this.receivingIntransitFilterForm = this.formBuilder.group({});
    Object.keys(receivingIntransitListFilterModel).forEach((key) => {
      if (typeof receivingIntransitListFilterModel[key] === 'string') {
        this.receivingIntransitFilterForm.addControl(key, new FormControl(receivingIntransitListFilterModel[key]));
      }
    });
  }

  getSelectedWarehouseOptions(): void {

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PURCHASEORDER_SEARCH,null,false,prepareRequiredHttpParams({Id: this.loggedUser.userId}))
      .subscribe((response: IApplicationResponse) => {
      if (response.resources && response.statusCode === 200) {
        let purchaseOrderList = response.resources;
        for (var i = 0; i < purchaseOrderList.length; i++) {
          if(purchaseOrderList[i].displayName != null){
            let tmp = {};
            tmp['value'] = purchaseOrderList[i].id;
            tmp['display'] = purchaseOrderList[i].displayName;
            this.purchaseOrderList.push(tmp)
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIERS)
      .subscribe((response: IApplicationResponse) => {
      if (response.resources && response.statusCode === 200) {
        let suppliersList = response.resources;
        this.suppliersList = [];
        for (var i = 0; i < suppliersList.length; i++) {
          if(suppliersList[i].displayName != null){
            let tmp = {};
            tmp['value'] = suppliersList[i].id;
            tmp['display'] = suppliersList[i].displayName;
            this.suppliersList.push(tmp)
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_STOCK_ORDER)
      .subscribe((response: IApplicationResponse) => {
      if (response.resources && response.statusCode === 200) {
        let stockOrderList = response.resources;
        this.stockOrderList = [];
        for (var i = 0; i < stockOrderList.length; i++) {
          if(stockOrderList[i].displayName != null){
            let tmp = {};
            tmp['value'] = stockOrderList[i].id;
            tmp['display'] = stockOrderList[i].displayName;
            this.stockOrderList.push(tmp)
          }
        }
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

    this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_ITEM)
      .subscribe((response: IApplicationResponse) => {
      if (response.resources && response.statusCode === 200) {
        this.stockCodeList = response.resources;
        // let stockCodeList = response.resources;
        // this.stockCodeList = [];
        // for (var i = 0; i < stockCodeList.length; i++) {
        //   if(stockCodeList[i].itemCode != null){
        //     let tmp = {};
        //     tmp['value'] = stockCodeList[i].id;
        //     tmp['display'] = stockCodeList[i].itemCode;
        //     this.stockCodeList.push(tmp)
        //   }
        // }
    
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  getSelectedPurchaseOrderOptions(purchase): void {
    if (purchase != '') {
      this.suppliersList = [];
      this.stockOrderList = [];
      this.receivingClerkList = [];
      this.statusList = [];
      let params = new HttpParams().set('WarehouseIds', this.receivingFilterForm.get('warehouseIds').value).set('PurchaseOrderIds', purchase).set('Id', this.loggedUser.userId);
      forkJoin([
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PURCHASEORDER_SUPPLIER, params),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PURCHASEORDER_STOCKORDER, params),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_RECEIVINGCLERK_BY_PURCHASEORDER, params),
        this.crudService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_INTERBRANCH_ORDER_STATUS),
      ]).subscribe((response: IApplicationResponse[]) => {
        response.forEach((resp: IApplicationResponse, ix: number) => {
          if (resp.isSuccess && resp.statusCode === 200) {
            switch (ix) {
              case 0:
                let suppliersList = resp.resources;
                for (var i = 0; i < suppliersList.length; i++) {
                  if(suppliersList[i].displayName != null){
                    let tmp = {};
                    tmp['value'] = suppliersList[i].id;
                    tmp['display'] = suppliersList[i].displayName;
                    this.suppliersList.push(tmp)
                  }
                }
                break;
              case 1:
                let stockOrderList = resp.resources;
                for (var i = 0; i < stockOrderList.length; i++) {
                  if(stockOrderList[i].displayName != null){
                    let tmp = {};
                    tmp['value'] = stockOrderList[i].id;
                    tmp['display'] = stockOrderList[i].displayName;
                    this.stockOrderList.push(tmp)
                  }
                }
                break;
              case 2:
                let receivingClerkList = resp.resources;
                for (var i = 0; i < receivingClerkList.length; i++) {
                  if(receivingClerkList[i].displayName != null){
                    let tmp = {};
                    tmp['value'] = receivingClerkList[i].id;
                    tmp['display'] = receivingClerkList[i].displayName;
                    this.receivingClerkList.push(tmp)
                  }
                }
                break;
              case 3:
                if (this.selectedTabIndex != 0) {
                  let statusList = resp.resources;
                  for (var i = 0; i < statusList.length; i++) {
                    if(statusList[i].displayName != null){
                      let tmp = {};
                      tmp['value'] = statusList[i].id;
                      tmp['display'] = statusList[i].displayName;
                      this.statusList.push(tmp)
                    }
                  }
                }
                break;
            }
          }
        })
        this.rxjsService.setGlobalLoaderProperty(false);
      })
    }
  }

  getSelectedStockOrderOptions(stockOrder): void {
    if (stockOrder != '') {
      this.stockCodeList = [];
      let params = new HttpParams().set('PurchaseOrderIds', this.receivingFilterForm.get('purchaseOrderIds').value).set('StockOrderIds', stockOrder);
      this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_PURCHASEORDER_ITEMS, undefined, true, params).subscribe((response: IApplicationResponse) => {
        this.rxjsService.setGlobalLoaderProperty(true);
        if (response.resources && response.statusCode === 200) {
           this.stockCodeList = response.resources;
          //  this.rxjsService.setGlobalLoaderProperty(true);
          // let stockCodeList = response.resources;
          // for (var i = 0; i < stockCodeList.length; i++) {
          //   if(stockCodeList[i].itemCode != null){
          //     let tmp = {};
          //     tmp['value'] = stockCodeList[i].id;
          //     tmp['display'] = stockCodeList[i].itemCode;
          //     this.stockCodeList.push(tmp)
          //   }
          // }
          this.rxjsService.setGlobalLoaderProperty(false);
        }else {
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }

  receivingQRCodeScanItem(): void {
    let qrCode = Object.assign({},
      this.receivingFilterForm.get('scanQrCodeInput').value == '' ? null : { qrCode: this.receivingFilterForm.get('scanQrCodeInput').value },
    );
    this.observableResponse = this.getRequiredListData('', '', qrCode);
    this.showFilterForm = !this.showFilterForm;
  }

  receivingSerialNumberScanItem(): void {
    let qrCode = Object.assign({},
      this.receivingFilterForm.get('scanSerialNumberInput').value == '' ? null : { SerialNumber: this.receivingFilterForm.get('scanSerialNumberInput').value },
    );
    this.observableResponse = this.getRequiredListData('', '', qrCode);
    this.showFilterForm = !this.showFilterForm;
  }
  
  submitFilter() {
      
    let stockCodeIds=[];
    if (this.selectedTabIndex == 0) {
      if( this.selectedTabIndex != 1 && this.receivingFilterForm.get('stockCodeIds').value ){
   
       this.receivingFilterForm.get('stockCodeIds').value.forEach(async (ele)=>{
          stockCodeIds.push(ele.id);
        });
      }

      let receiveData = Object.assign({},
        this.receivingFilterForm.get('warehouseIds').value == '' || this.receivingFilterForm.get('warehouseIds').value == null ? null :
        { warehouseIds: this.receivingFilterForm.get('warehouseIds').value },
        this.receivingFilterForm.get('purchaseOrderIds').value == '' || this.receivingFilterForm.get('purchaseOrderIds').value == null ? null :
        { purchaseOrderIds: this.receivingFilterForm.get('purchaseOrderIds').value },
        this.receivingFilterForm.get('supplierIds').value == '' || this.receivingFilterForm.get('supplierIds').value == null ? null :
        { supplierIds: this.receivingFilterForm.get('supplierIds').value },
        this.receivingFilterForm.get('fromDate').value == '' || this.receivingFilterForm.get('fromDate').value == null ? null :
        { fromDate: this.datePipe.transform(this.receivingFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
        this.receivingFilterForm.get('toDate').value == '' || this.receivingFilterForm.get('toDate').value == null ? null :
        { toDate: this.datePipe.transform(this.receivingFilterForm.get('toDate').value, 'yyyy-MM-dd') },
        this.receivingFilterForm.get('receivingClerkIds').value == '' || this.receivingFilterForm.get('receivingClerkIds').value == null ? null :
        { receivingClerkIds: this.receivingFilterForm.get('receivingClerkIds').value },
        this.receivingFilterForm.get('stockOrderIds').value == '' || this.receivingFilterForm.get('stockOrderIds').value == null ? null :
        { stockOrderIds: this.receivingFilterForm.get('stockOrderIds').value },
        this.receivingFilterForm.get('stockCodeIds').value == '' || this.receivingFilterForm.get('stockCodeIds').value == null ? null :
        { stockCodeIds: stockCodeIds.toString() },
        this.receivingFilterForm.get('statuses').value == '' || this.receivingFilterForm.get('statuses').value == null ? null :
        { statuses: this.receivingFilterForm.get('statuses').value }, { userId: this.loggedUser.userId }
      );
      this.observableResponse = this.getRequiredListData('', '', receiveData);
      this.showFilterForm = !this.showFilterForm;
    }
    else if (this.selectedTabIndex == 1) {
      let receiveSecondData = Object.assign({},
        this.receivingFilterForm.get('techwarehouseIds').value == '' || this.receivingFilterForm.get('techwarehouseIds').value == null ? null :
        { WarehouseIds: this.receivingFilterForm.get('techwarehouseIds').value },
        this.receivingFilterForm.get('goodsReturnNumbers').value == '' || this.receivingFilterForm.get('goodsReturnNumbers').value == null ? null :
        { ReferenceIds: this.receivingFilterForm.get('goodsReturnNumbers').value },
        this.receivingFilterForm.get('scanQrCodeInput').value == '' || this.receivingFilterForm.get('scanQrCodeInput').value == null ? null :
        { qrCode: this.receivingFilterForm.get('scanQrCodeInput').value },
        this.receivingFilterForm.get('fromDate').value == '' || this.receivingFilterForm.get('fromDate').value == null ? null :
        { FromDate: this.datePipe.transform(this.receivingFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
        this.receivingFilterForm.get('toDate').value == '' || this.receivingFilterForm.get('toDate').value == null ? null :
        { ToDate: this.datePipe.transform(this.receivingFilterForm.get('toDate').value, 'yyyy-MM-dd') },
        this.receivingFilterForm.get('receivingClerkIds').value == '' || this.receivingFilterForm.get('receivingClerkIds').value == null ? null :
        { ReceivingClerkIds: this.receivingFilterForm.get('receivingClerkIds').value },
        this.receivingFilterForm.get('stockOrderIds').value == '' || this.receivingFilterForm.get('stockOrderIds').value == null ? null :
        { StockOrderNumber: this.receivingFilterForm.get('stockOrderIds').value },
        this.receivingFilterForm.get('stockCodeIds').value == '' || this.receivingFilterForm.get('stockCodeIds').value == null ? null :
        { StockCode: this.receivingFilterForm.get('stockCodeIds').value },
        this.receivingFilterForm.get('statuses').value == '' || this.receivingFilterForm.get('statuses').value == null ? null :
        { Statuses: this.receivingFilterForm.get('statuses').value },
        this.receivingFilterForm.get('technicianName').value == '' || this.receivingFilterForm.get('technicianName').value == null ? null :
        { TechnicianIds: this.receivingFilterForm.get('technicianName').value },
        this.receivingFilterForm.get('scanSerialNumberInput').value == '' || this.receivingFilterForm.get('scanSerialNumberInput').value == null ? null :
        { SerialNumber: this.receivingFilterForm.get('scanSerialNumberInput').value },
        { userId: this.loggedUser.userId },
      );
      this.observableResponse = this.getRequiredListData('', '', receiveSecondData);
      this.showFilterForm = !this.showFilterForm;
    }
    else {
      if( this.selectedTabIndex != 1 && this.receivingFilterForm.get('stockCodeIds').value ){
        this.receivingFilterForm.get('stockCodeIds').value.forEach((ele)=>{
          stockCodeIds.push(ele.id);
        })
      }
    
      
      let TransitData = Object.assign({},
        this.receivingIntransitFilterForm.get('fromWarehouseIds').value == '' || this.receivingIntransitFilterForm.get('fromWarehouseIds').value == null ? null : { fromWareHouseIds: this.receivingIntransitFilterForm.get('fromWarehouseIds').value },
        this.receivingIntransitFilterForm.get('toWarehouseIds').value == '' || this.receivingIntransitFilterForm.get('toWarehouseIds').value == null ? null : { toWareHouseIds: this.receivingIntransitFilterForm.get('toWarehouseIds').value },
        this.receivingIntransitFilterForm.get('fromDate').value == '' || this.receivingIntransitFilterForm.get('fromDate').value == null ? null : { fromDate: this.datePipe.transform(this.receivingIntransitFilterForm.get('fromDate').value, 'yyyy-MM-dd') },
        this.receivingIntransitFilterForm.get('toDate').value == '' || this.receivingIntransitFilterForm.get('toDate').value == null ? null : { toDate: this.datePipe.transform(this.receivingIntransitFilterForm.get('toDate').value, 'yyyy-MM-dd') },
        this.receivingIntransitFilterForm.get('receivingClerkIds').value == '' || this.receivingIntransitFilterForm.get('receivingClerkIds').value == null ? null : { receivingClerkIds: this.receivingIntransitFilterForm.get('receivingClerkIds').value },
        this.receivingIntransitFilterForm.get('stockCodeIds').value == '' || this.receivingIntransitFilterForm.get('stockCodeIds').value == null ? null : { stockCodeIds: stockCodeIds.toString()  },
        this.receivingIntransitFilterForm.get('statuses').value == '' || this.receivingIntransitFilterForm.get('statuses').value == null ? null : { statuses: this.receivingIntransitFilterForm.get('statuses').value },
        { userId: this.loggedUser.userId }, { warehouseId: this.loggedUser.warehouseId }
      );
      this.observableResponse = this.getRequiredListData('', '', TransitData);
      this.showFilterForm = !this.showFilterForm;
    }
  }

  resetForm() {
    if (this.selectedTabIndex == 0 || this.selectedTabIndex == 1) {
      this.receivingFilterForm.reset();
    }
    else if (this.selectedTabIndex == 3) {
      this.receivingIntransitFilterForm.reset();
    }
    this.observableResponse = this.getRequiredListData('', '', null);
    this.showFilterForm = !this.showFilterForm;
  }
}
