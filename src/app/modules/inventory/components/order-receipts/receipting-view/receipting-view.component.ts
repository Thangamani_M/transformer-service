import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { Observable } from 'rxjs';
import { BarcodePrintComponent } from '../barcode-print';
import { SerialnumberViewComponent } from '../intransit';

@Component({
  selector: 'app-receipting-view',
  templateUrl: './receipting-view.component.html',
  styleUrls: ['./receipting-view.component.scss']
})
export class ReceiptingViewComponent implements OnInit {
  orderReferenceId: string;
  batchNumber: any;
  OrderReceiptDetails: any = null;
  ReceiptStatus: string = "";
  ReceiptTye: string = "";
  IsLoaded: boolean = false;
  IsPurchaseOrder: boolean = false;
  ispending = false;
  OrderTypeId: string = "";
  barcodes: any;
  barcode: any;
  file: any;
  isBatchHeader: boolean = false;
  printSerialNumber: Array<string> = new Array<string>();

  constructor(private dialog: MatDialog, private httpService: CrudService, private router: Router,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
    this.orderReferenceId = this.activatedRoute.snapshot.queryParams.PurchaseOrderId;
    this.batchNumber = this.activatedRoute.snapshot.queryParams.BatchId;
    this.OrderTypeId = this.activatedRoute.snapshot.queryParams.OrderTypeId;
  }
  ngOnInit() {
    if (this.batchNumber != undefined) {
      this.isBatchHeader = false;
      this.getReceiveByIdAndBatch().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.OrderReceiptDetails = response.resources;
          if (this.OrderReceiptDetails.pendingItems.length == 0) {
            this.ispending = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })

    } else {
      this.isBatchHeader = true;
      this.getReceiveById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {

          this.OrderReceiptDetails = response.resources;
          if (this.OrderReceiptDetails.pendingBatch != null)
            this.batchNumber = this.OrderReceiptDetails.pendingBatch.orderReceiptBatchId;

          if (this.OrderReceiptDetails.pendingItems.length == 0) {
            this.ispending = true;
          }
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    }
  }
  getitemDetails(orderItems, itemCode) {
    if (orderItems.isNotSerialized == true)
      return;
    this.barcodes = orderItems.orderReceiptItemDetails;
    this.barcode = Array.prototype.map.call(this.barcodes, function (item) { return item.barcode; }).join(",");
    const dialogReff = this.dialog.open(SerialnumberViewComponent, { width: '700px', disableClose: true, data: { barcode: this.barcode, count: this.barcodes.length } });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  getReceiveById(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('PurchaseOrderId', (this.orderReferenceId && this.orderReferenceId != undefined) ? this.orderReferenceId : '');
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PURCHASEORDER_CONSOLIDATE, null, null, params);
  }
  getReceiveByIdAndBatch(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('PurchaseOrderId', (this.orderReferenceId && this.orderReferenceId != undefined) ? this.orderReferenceId : '').set('BatchId', this.batchNumber ? this.batchNumber : '');
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PURCHASEORDER_CONSOLIDATE, null, null, params);
  }

  navigateToEdit(): void {
    this.router.navigate(['inventory/order-receipts/receipting/order'], { queryParams: { id: this.orderReferenceId, orderTypeId: this.OrderTypeId, batchId: this.batchNumber ? this.batchNumber : '' }, skipLocationChange: true })
  }

  printReceivedBarcode() {
    this.printSerialNumber = new Array<string>();
    this.printSerialNumber.push(this.OrderReceiptDetails.barcodeNumber);
    const dialogReff = this.dialog.open(BarcodePrintComponent, { width: '400px', height: '400px', disableClose: true, data: { serialNumbers: this.printSerialNumber } });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
    });
  }

  printPOBarcode() {
    this.printSerialNumber = new Array<string>();
    this.printSerialNumber.push(this.OrderReceiptDetails.poBarcode);
    const dialogReff = this.dialog.open(BarcodePrintComponent, { width: '400px', height: '400px', disableClose: true, data: { serialNumbers: this.printSerialNumber } });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
    });
  }
}
