import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { KeyValuePair, OrderReceipt } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels, OrderTypes } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { debounceTime, finalize, switchMap, tap } from 'rxjs/operators';
import { Supplier } from '../../../models/Supplier';

@Component({
  selector: 'app-technician-receiving',
  templateUrl: './technician-receiving.component.html',
  styleUrls: ['./technician-receiving.component.scss']
})
export class TechnicianReceivingComponent implements OnInit {
  @ViewChild('serialNumber', { static: false }) serilNumberField: ElementRef;

  userData: UserLogin;
  orderReferenceId: string;
  orderNumberControl = new FormControl();
  applicationResponse: IApplicationResponse;
  uxOderTypes: any;
  orderType: string = '';
  storageLocation: string = '';
  isNew: boolean = false;
  showSupplier: boolean = false;
  receiptingOrderForm: FormGroup;
  keyValuePair: KeyValuePair;
  uxStorageLocations: any;
  filteredFaultys: any[] = [];
  purchaseOrderAutoFillForm: FormGroup;
  isLoading = false;
  supplierName: string = '';
  order: any;
  supplier = new Supplier();
  orderReceipt = new OrderReceipt();
  showBatchNo = false;
  orderTypes = OrderTypes;
  orderTypesOptions = [];
  receivingForm: FormGroup;
  receiptingType: any;
  batchId: string = '0';
  //mask validations
  public alphaNumericWithSpacePattern = { '0': { pattern: new RegExp('^[A-Za-z0-9 ]*$') } };

  @ViewChild('ddl', null) myDiv: ElementRef<HTMLElement>;
  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private httpService: CrudService, private router: Router,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.orderReceipt = this.router.getCurrentNavigation().extras.state.data;
      this.orderType = this.orderReceipt.orderTypeId;
      this.batchId = this.orderReceipt.orderReceiptBatchId;
    }
    if (this.orderReceipt.orderReceiptId == undefined) {
      this.orderReceipt.orderReferenceId = this.activatedRoute.snapshot.queryParams.id;
      this.orderType = this.activatedRoute.snapshot.queryParams.orderTypeId;
      this.batchId = this.activatedRoute.snapshot.queryParams.batchId;
    }
  }

  ngOnInit() {
    this.receivingForm = this.formBuilder.group({
      requestId: [''],
      orderTypeName: [''],
      orderNumber: [''],
      technicianName: [''],
      storageLocationName: [''],
      technicianLocation: [''],
    });

    if (this.orderReceipt.orderReferenceId != undefined) {
      this.getFaulty(this.orderReceipt.orderReferenceId);
    }

    this.orderTypesOptions = Object.entries(this.orderTypes).map(([key, value]) => ({ key, value }))
    //temporarly added this below code. we will be remove this code after implementation
    this.orderTypesOptions = this.orderTypesOptions.filter(function (obj) {
      return obj.key !== 'REPLACEMENT';
    });

    this.orderNumberControl.valueChanges.pipe(debounceTime(300), tap(() => this.isLoading = true),
      switchMap(value => {
        if (!value) {
          this.isLoading = false;
          return this.filteredFaultys;
        } else if (typeof value === 'object') {
          this.isLoading = false;
          return this.filteredFaultys = [];
        } else {
          if (this.orderReceipt.orderTypeId === OrderTypes.NEW) {
            let resources = this.getUXFaultySearch(value);
            return resources;
          } else if (this.orderReceipt.orderTypeId.toLocaleLowerCase() == OrderTypes.RETURN) {
            return this.getSupReturnNumbersUX(value);
          } else {
            let resources = this.getUXRepairRequestSearch(value);
            return resources;
          }

        }
      }
      )
    ).subscribe(results => { this.filteredFaultys = results.resources; });
  }

  getSupReturnNumbersUX(value: any): Observable<any> {
    return this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_SUP_RETURN_NUMBER_SEARCH, null, true, prepareGetRequestHttpParams(null, null, { displayName: value }))
      .pipe(
        finalize(() => {
          this.isLoading = false,
            this.rxjsService.setGlobalLoaderProperty(false);
        }),
      );
  }

  getUXRepairRequestSearch(value: string): Observable<any> {
    return this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_REPAIRREQUEST_SEARCH, null, true, prepareGetRequestHttpParams(null, null, { searchText: value, orderTypeId: this.orderReceipt.orderTypeId }))
      .pipe(
        finalize(() => {
          this.isLoading = false,
            this.rxjsService.setGlobalLoaderProperty(false);
        }),
      );

  }
  getUXFaultySearch(value: string): Observable<any> {
    return this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_PURCHASEORDER_SEARCH, null, true, prepareGetRequestHttpParams(null, null, { displayName: value }))
      .pipe(
        finalize(() => {
          this.isLoading = false,
            this.rxjsService.setGlobalLoaderProperty(false);
        }),
      );

  }

  getUXOrderType(): void {
    this.httpService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_TYPES)
      .subscribe(resp => {
        this.applicationResponse = resp;
        this.uxOderTypes = this.applicationResponse.resources;
        this.rxjsService.setGlobalLoaderProperty(false);

      });
  }

  //Redirect to barcode scan page and pass related items
  barcodeScan(purchaseOrderItem, purchaseOrderDetails) {
    if (purchaseOrderDetails) {
      this.orderReceipt.orderNumber = purchaseOrderDetails.orderNumber;
      this.orderReceipt.orderReferenceId = purchaseOrderDetails.orderReferenceId;
      this.orderReceipt.warehouseId = this.userData.warehouseId;
      this.orderReceipt.inventoryStaffId = this.userData.userId;
      this.orderReceipt.supplierId = purchaseOrderDetails.supplierId;
      this.orderReceipt.orderReceiptId = purchaseOrderItem.orderReceiptId;
      let order = {
        itemName: purchaseOrderItem.itemName,
        itemCode: purchaseOrderItem.itemCode,
        purchaseOrderItem: purchaseOrderItem,
        orderReceipt: this.orderReceipt
      }
      this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-item-scan'], { state: { data: order } });
    }
  }

  //Selected value to assign auto fill textbox
  displayFaultyNo(keyValuePair: KeyValuePair) {
    if (keyValuePair) { return keyValuePair.displayName; }
  }

  //Get purchase order with related items and suppliers
  getFaulty(pid) {
    if (pid && pid != null) {
      let pId = (typeof pid === 'object') ? pid.id : pid;
      let params;
      params = new HttpParams().set('RTRRequestId', pId);//.set('BatchId', this.batchId != '0' ? this.batchId : null);      
      this.httpService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.ORDERRECEIVE_TECHNICIAN_FAULTY, null, null, params).subscribe({
          next: response => {
            this.order = response.resources;

            if (this.order != null) {
              this.orderNumberControl.setValue({ displayName: this.order.orderNumber });
              this.orderReceipt.orderReceiptId = this.order.orderReceiptId;
              this.orderReceipt.isVerified = this.order.isVerified;
              this.orderReceipt.orderTypeId = this.orderType;
              this.orderReceipt.orderReceiptBatchId = this.order.orderReceiptBatchId;
              this.receivingForm.controls.requestId.setValue(this.order.requestId);
              this.receivingForm.controls.orderTypeName.setValue(this.order.orderTypeName);
              this.receivingForm.controls.orderNumber.setValue(this.order.rtrRequestNumber);
              this.receivingForm.controls.technicianName.setValue(this.order.technicianName);
              this.receivingForm.controls.technicianLocation.setValue(this.order.technicianLocation);
              this.receivingForm.controls.storageLocationName.setValue(this.order.storageLocationName);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
      if (pid.id !== undefined)
        this.orderReceipt.orderReferenceId = pid.id;
    }
  }

  getreceivedetailsById(purchaseOrderId: string) {
    this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PURCHASEORDER_RELATEDITEMS,
      purchaseOrderId,
      false,
      null
    ).subscribe((response: IApplicationResponse) => {
      if (response.resources) {

        this.order = response.resources;
        this.receivingForm.controls.requestId.setValue(this.order.requestId);
        this.receivingForm.controls.orderTypeName.setValue(this.order.orderTypeName);
        this.receivingForm.controls.orderNumber.setValue(this.order.orderNumber);
        this.receivingForm.controls.supplierName.setValue(this.order.supplierName);
        this.receivingForm.controls.storageLocationName.setValue(this.order.storageLocationName);
      }
    });
  }

  //Get storage location based on warehouse
  getUXstorageLocations() {
    const params = new HttpParams().set('warehouseid', this.userData.warehouseId)
    this.httpService.dropdown(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_LOCATION, params
    ).subscribe(resp => {
      this.uxStorageLocations = resp.resources;
      //Defalut storage location should be staging bay location
      this.orderReceipt.locationId = "72b3bff0-7425-475b-a554-7addbcc3e9fa";
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  //hide and show ui elements based on receipting type
  receiptingTypeChange(event) {
    let selectedReceiptingType = event;
    this.receiptingType = selectedReceiptingType.toLocaleLowerCase();
    //New
    if (selectedReceiptingType.toLocaleLowerCase() === 'new') {
      this.isNew = true;
      this.showSupplier = false;
      this.filteredFaultys = [];
      this.order = null;
      this.showBatchNo = false;
      this.orderNumberControl.setValue({ displayName: '' });
      this.orderReceipt.batchNumber = null;
    }
    //Return
    else if (selectedReceiptingType.toLocaleLowerCase() === 'return') {
      this.EnableDisableControls();
    }
    //Replacement
    else if (selectedReceiptingType.toLocaleLowerCase() === 'replacement') {
      this.EnableDisableControls();
    }//Faulty
    else if (selectedReceiptingType.toLocaleLowerCase() === 'faulty') {
      this.EnableDisableControls();
    }
  }

  private EnableDisableControls() {
    this.isNew = true;
    this.showSupplier = false;
    this.filteredFaultys = [];
    this.order = null;
    this.showBatchNo = false;
    this.orderNumberControl.setValue({ displayName: '' });
    this.orderReceipt.batchNumber = null;
  }

  public verifyFaulty() {
    let returnedItems = this.order.orderItems.filter(function (item) {
      return item.receivedQty > 0;
    });

    if (returnedItems.length === 0) {
      this.snackbarService.openSnackbar('Please scan stocks to be returned.', ResponseMessageTypes.WARNING);
      return;
    }

    if (this.orderReceipt.orderReceiptId != null) {
      this.orderReceipt.orderReceiptBatchId = this.batchId;
      this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-verify/'], { state: { data: this.orderReceipt } });
    }
    else {
      this.snackbarService.openSnackbar('Please check mandatory fields', ResponseMessageTypes.WARNING);
    }
  }
  navigateToJobSelection() {
    this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
  }

  View() {
    if (this.orderReceipt.orderReferenceId) {
      this.router.navigate(['inventory/order-receipts/receipting/technician-receiving-view'], { queryParams: { RTRRequestId: this.orderReceipt.orderReferenceId, OrderTypeId: this.orderReceipt.orderTypeId, orderReceiptId: this.orderReceipt.orderReceiptId }, skipLocationChange: true });
    }
  }
}
