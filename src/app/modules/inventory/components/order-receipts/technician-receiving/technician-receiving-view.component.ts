import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix,currentComponentPageBasedPermissionsSelector$, LoggedInUserModel, prepareDynamicTableTabsFromPermissions } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels, INVENTORY_COMPONENT } from '@modules/inventory/shared/utils';
import { Observable } from 'rxjs';
import { SerialnumberViewComponent } from '../intransit';
import { TechnicianReceivingFaultyItemModalComponent } from '../technician-receiving-faulty';
import { combineLatest } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { AppState } from '@app/reducers';
import { loggedInUserData } from '@modules/others';
@Component({
  selector: 'app-technician-receiving-view',
  templateUrl: './technician-receiving-view.component.html',
  styleUrls: ['./technician-receiving-view.component.scss']
})
export class TechnicianReceivingViewComponent implements OnInit {
  rtrRequestId: string;
  batchNumber: any;
  TechnicianReturnReceiptingDetails: any = null;
  TechnicianReceivingViewDetail:any;
  barcodes: any;
  barcode: any;
  isActiveNew: boolean;
  primengTableConfigProperties:any;
  loggedInUserData:any;
  constructor(private store: Store<AppState>,private dialog: MatDialog, private httpService: CrudService, private router: Router,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
    this.rtrRequestId = this.activatedRoute.snapshot.queryParams.RTRRequestId;
    this.batchNumber = this.activatedRoute.snapshot.queryParams.BatchId;
    this.isActiveNew = this.activatedRoute.snapshot.queryParams.isActiveNew
    this.primengTableConfigProperties = {
      tableCaption: "Technician",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
          tabsList: [{
              enableBreadCrumb: true,
              enableAction: true,
              enableViewBtn: false,
              enableClearfix: true,
          }]
      }
  }
  }

  ngOnInit() {
    this.getReceiveById().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.TechnicianReturnReceiptingDetails = response.resources;
        this.onShowValue(response.resources);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
    this.combineLatestNgrxStoreData();
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData), this.store.select(currentComponentPageBasedPermissionsSelector$)]
    ).subscribe((response) => {
      this.loggedInUserData= new LoggedInUserModel(response[0]);
      let permission = response[1][INVENTORY_COMPONENT.RECEIVING];
      if (permission) {
        let prepareDynamicTableTabsFromPermissionsObj = prepareDynamicTableTabsFromPermissions(this.primengTableConfigProperties, permission);
        this.primengTableConfigProperties = prepareDynamicTableTabsFromPermissionsObj['primengTableConfigProperties'];
      }
    });
  }
  onShowValue(response?: any) {
    this.TechnicianReceivingViewDetail = [
      {
        name: 'Basic Info', columns: [
          { name: 'Receiving ID', value: response?.receivingId},
          { name: 'Receiving Barcode', value: response?.receivingBarcode},
          { name: 'QR Code', value: response?.qrCode},
          { name: 'Created Date & Time', value: response?.createdDate},
          { name: 'Receiving Type', value: response?.orderTypeName},
          { name: 'Reference ID', value: response?.referenceNumber},
          { name: 'Tech Stock Location', value: response?.technicianStockLocation},
          { name: 'Tech Stock Location Name', value: response?.technicianStockLocationName},
          { name: 'Warehouse', value: response?.warehouseName},
          { name: 'Received Date', value: response?.receivedDate},
          { name: 'Receiving Clerk', value: response?.storageLocationName},
          { name: 'Service Call Number', value: response?.serviceCallNumber},
          { name: 'Status', value: response?.status, statusClass: "status-label-blue" },

        ]
      }
    ]
  }

  getReceiveById(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('RTRRequestId', (this.rtrRequestId && this.rtrRequestId != undefined) ? this.rtrRequestId : '');
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDERRECEIVE_TECHNICIAN_FAULTY, null, null, params);
  }

  getReceiveByIdAndBatch(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('RTRRequestId', (this.rtrRequestId && this.rtrRequestId != undefined) ? this.rtrRequestId : '').set('BatchId', this.batchNumber ? this.batchNumber : '');
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDERRECEIVE_TECHNICIAN_FAULTY, null, null, params);
  }

  navigateToEdit(): void {

    this.router.navigate(['inventory/order-receipts/receipting/technician-receiving-faulty-item'],
      {queryParams:{id: this.rtrRequestId,isActiveNew: this.isActiveNew}});
  }

  getserialnumber(orderItems, itemCode) {
    this.barcodes = orderItems.orderReceiptItemDetails.filter(b => b.isScanned == true);
    this.barcode = undefined;
    if (this.barcodes.length != 0) {
      this.barcode = Array.prototype.map.call(this.barcodes, function (item) { return item.barcode; }).join(",");
    }
    const dialogReff = this.dialog.open(SerialnumberViewComponent, { width: '700px', disableClose: true, data: { barcode: this.barcode, count: this.barcodes.length } });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

  barcodeScan(orderItems, itemCode, itemName, qty) {
    let modalDetails = {
      stockCode: itemCode,
      stockDescription: itemName,
      quantity: qty,
      serialNumbers: orderItems
    }
    this.rxjsService.setGlobalLoaderProperty(false);
    const dialogRemoveStockCode = this.dialog.open(TechnicianReceivingFaultyItemModalComponent, {
      width: '1000px',
      data: {
        serialNumberDetails: modalDetails
      }, disableClose: true
    });
    dialogRemoveStockCode.afterClosed().subscribe(result => {
      if (!result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });
  }

}
