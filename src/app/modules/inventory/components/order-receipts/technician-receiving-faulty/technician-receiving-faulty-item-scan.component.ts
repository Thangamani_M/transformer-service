import { HttpParams } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
    CrudService, CustomDirectiveConfig, IApplicationResponse,
    ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, RxjsService, SnackbarService
} from '@app/shared';
import { OrderReceiptItemDetailsModal, TechnicianReceivingFaultyModal } from '@modules/inventory/models';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared';

@Component({
    selector: 'app-technician-receiving-faulty-item-scan',
    templateUrl: './technician-receiving-faulty-item-scan.component.html',
    styleUrls: ['./technician-receiving-faulty-item-scan.component.scss']
})

export class TechnicianReceivingFaultyItemScanComponent implements OnInit {

    technicianReceivingFaultyForm: FormGroup;
    orderReceiptItemDetails: FormArray;
    warrantyStatusDropDown: any = [];
    itemStatusDropdown: any = [];
    getfaultyDetails: any;
    filteredSuppliers: any = [];
    showGenerateBarcode: boolean = false;
    cusQuantityError: boolean = false;
    qtyErrorMessage: any;
    showSerialNumberInput: boolean = false;
    serialErrorMessage: any;
    showStockCountInput = false;
    stockCountErrorMessage = '';
    rtrRequestId: any;
    isLoading: boolean;
    userId: any;
    numberConfig = new CustomDirectiveConfig({ isANumberOnly: true });
    isActiveNew: boolean;
    checkSerialNumber: boolean = false;
    scannedSerialNumber:any=[];
    isSubmit: boolean = false;
    isScanned: any;
    itemId;
    isSerialized;
    counter=0;
    screenType;
    returnCustomers = [
        { value: true, name: 'Yes' },
        { value: false, name: 'No' },
    ]
    isFaulty: any;

    constructor(
        private router: Router, private rxjsService: RxjsService,
        private formBuilder: FormBuilder, private crudService: CrudService,
        private snackbarService: SnackbarService, private changeDetectorRef: ChangeDetectorRef) {
        if (this.router && this.router.getCurrentNavigation().extras.state) {
            this.getfaultyDetails = this.router.getCurrentNavigation().extras.state.data;
            this.rtrRequestId = this.router.getCurrentNavigation().extras.state.id;
            this.userId = this.router.getCurrentNavigation().extras.state.createdUserId;
            this.isActiveNew = this.router.getCurrentNavigation().extras.state.isActiveNew;
            this.isFaulty= this.router.getCurrentNavigation().extras.state.isFaulty;
            this.screenType= this.router.getCurrentNavigation().extras.state.screenType;
        }
    }

    ngOnInit() {     
        this.getAllDropDown();
        this.createTechnicianReceivingFaultyForm();
        if (this.getfaultyDetails) {
            this.itemId = this.getfaultyDetails.faultyOrderItem.itemId;
            this.getOrderReceiptItemDetailsArray.clear();
            this.orderReceiptItemDetails = this.getOrderReceiptItemDetailsArray;
            let orderReceipt = this.getfaultyDetails.faultyOrderItem.orderReceiptItemDetails;
            this.technicianReceivingFaultyForm.get('isSerialized').setValue(this.getfaultyDetails.isSerialized);
            this.isSerialized = this.getfaultyDetails.isSerialized;
            if(this.isSerialized==false){
                let faultyOrderItem = this.getfaultyDetails.faultyOrderItem.orderReceiptItemDetails;
                this.technicianReceivingFaultyForm.get('stockCount').setValue(faultyOrderItem.length);
                this.technicianReceivingFaultyForm.get('stockCount').disable();
            }


            if (orderReceipt.length > 0) {
                this.isScanned = orderReceipt[0].isScanned;
                let serialDetails = [];
                orderReceipt.forEach((receipt, index) => {
       
                    if (receipt.serialNumber === null) {
                        serialDetails.push(receipt.orderItemId);
                    }
                    receipt['scannedSerialNumber'] = false;
                    receipt['itemStatusName'] = receipt.rtrRequestItemStatusName;
                    // receipt['serialNumber'] = receipt.tempStockCode;
                    let suppliers = {
                        displayName: receipt.supplierName,
                        id: receipt.supplierId,
                    }
                    this.filteredSuppliers.push(suppliers);
                    receipt['supplierName'] = suppliers;
                    this.onSupplierAddressSelected(suppliers, index,receipt.supplierAddressId);
             
                    this.orderReceiptItemDetails.push(this.createTechnicianFaultyItemsModel(receipt));
                });
                if (serialDetails.length == orderReceipt.length) {
                    this.showGenerateBarcode = true;
                    this.technicianReceivingFaultyForm.get('isBarcode').patchValue(true);
                    this.checkSerialNumber = true;
                } else { this.showGenerateBarcode = false }
            }
            // else {
            //     this.orderReceiptItemDetails.push(this.createTechnicianFaultyItemsModel());
            // }
        }

        this.cusQuantityError = false;
        this.qtyErrorMessage = '';
        this.technicianReceivingFaultyForm.get('customerQty').valueChanges.subscribe((qty: number) => {
            if (qty == null || qty == undefined) return;
            if (qty > this.technicianReceivingFaultyForm.get('stockCount').value) {
                this.cusQuantityError = true;
                this.qtyErrorMessage = "Customer quantity cannot be greater than stock quantity";
                return;
            }
        });

        this.showStockCountInput = false;
        this.stockCountErrorMessage = '';
        this.technicianReceivingFaultyForm.get('stockCount').valueChanges.subscribe((count: number) => {
            this.showStockCountInput = false;
            this.stockCountErrorMessage = '';
            if (this.getfaultyDetails.faultyOrderItem?.orderReceiptItemDetails?.length < count) {
                this.showStockCountInput = true;
                this.stockCountErrorMessage = "Stock count cannot be greater than order receipt item length";
                return;
            }
        });
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    createTechnicianReceivingFaultyForm(): void {
        let stockOrderModel = new TechnicianReceivingFaultyModal();
        // create form controls dynamically from model class
        this.technicianReceivingFaultyForm = this.formBuilder.group({
            orderReceiptItemDetails: this.formBuilder.array([])
        });
        Object.keys(stockOrderModel).forEach((key) => {
            this.technicianReceivingFaultyForm.addControl(key, new FormControl(stockOrderModel[key]));
        });
    }

    ngAfterViewChecked() {
        this.changeDetectorRef.detectChanges();
    }

    //Create FormArray controls
    createTechnicianFaultyItemsModel(interBranchModel?: OrderReceiptItemDetailsModal): FormGroup {
        let falutyModelData = new OrderReceiptItemDetailsModal(interBranchModel);
        let formControls = {};
        Object.keys(falutyModelData).forEach((key) => {
            if(falutyModelData['warrentyStatusName'] == 'In Full warranty'){
               if(key =='supplierName' || key =='supplierAddressId')
                formControls[key] = [{value: falutyModelData[key], disabled: false},[Validators.required]]               
               else{
                formControls[key] = [{value: falutyModelData[key], disabled: falutyModelData
                    && falutyModelData[key] !== '' ? false : false}]
               }                
            }else {
                if(key =='supplierName' || key =='supplierAddressId')
                  formControls[key] = [{value: falutyModelData[key], disabled: true}] 
                else{
                    formControls[key] = [{
                        value: falutyModelData[key], disabled: falutyModelData
                            && falutyModelData[key] !== '' ? false : false
                    },
                    (key === 'serialNumber') ? [Validators.required] : []]
                }              
            }
          
        });

        return this.formBuilder.group(formControls);
    }

    //Create FormArray
    get getOrderReceiptItemDetailsArray(): FormArray {
        if (!this.technicianReceivingFaultyForm) return;
        return this.technicianReceivingFaultyForm.get("orderReceiptItemDetails") as FormArray;
    }

    getAllDropDown() {
        /* Warrantty Status dropdown */
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_WARRANTY_STATUS)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.warrantyStatusDropDown = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
        /* Item Status dropdown */
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_REQUEST_ITEM_STATUS)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.itemStatusDropdown = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    selectItemStatus(index: number, event): void {
        let statusDetail = this.itemStatusDropdown.find(x => x.id == event.target.value);
        this.getOrderReceiptItemDetailsArray.controls[index].get('itemStatusName').patchValue(statusDetail.displayName)
    }

    inputChangeSupplierFilter(text: any) {
        let otherParams = {}
        if (text.query == null || text.query == '') return;
        otherParams['searchText'] = text.query;
        this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_AUTOCOMPLETE, null, false,
            prepareGetRequestHttpParams(null, null, otherParams))
            .subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.resources.length > 0) {
                    this.filteredSuppliers = response.resources;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
    }

    /* Supplier Address dropdown */
    onSupplierAddressSelected(obj: any, index: number,supplierAddressId?:any) {
        if (obj) {
            if (obj.id == null) return;
            let params = new HttpParams().set('supplierId', obj.id);
            this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.UX_SUPPLIER_ADDRESS, undefined, true, params)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    let addressDropdown= response.resources;
                    this.getOrderReceiptItemDetailsArray.controls[index].get('addressDropdown').patchValue(response.resources);
                    if(supplierAddressId){
                        if(addressDropdown?.length>0){
                           let result= addressDropdown.filter(x=>x.supplierAddressId == supplierAddressId);
                           this.getOrderReceiptItemDetailsArray.controls[index].get('supplierAddressId').patchValue(result[0].supplierAddressId);
                        }
                    }                   
                    else 
                       this.getOrderReceiptItemDetailsArray.controls[index].get('supplierAddressId').patchValue(response.resources[0].supplierAddressId);

                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
        }
    }

    generateBarcodeCheckbox(event) {
        this.showSerialNumberInput = false;
        this.showStockCountInput = false;
        this.stockCountErrorMessage = '';
        this.serialErrorMessage = '';
        if (event.checked) this.showGenerateBarcode = true;
        else this.showGenerateBarcode = false;
    }

    addStockCount() {

        this.showStockCountInput = false;
        this.stockCountErrorMessage = '';
        let stockCountVal = this.technicianReceivingFaultyForm.get('stockCount').value;
        if (stockCountVal == '' || stockCountVal == null) {
            this.showStockCountInput = true;
            this.stockCountErrorMessage = "Stock count is required";
            return;
        }

        if (this.getfaultyDetails.faultyOrderItem?.orderItems?.length < stockCountVal) {
            this.showStockCountInput = true;
            this.stockCountErrorMessage = "Stock count cannot be greater than order receipt item length";
            return;
        }

        if(this.isSerialized){
            let  checkDuplicate = this.getOrderReceiptItemDetailsArray.value.filter(x => x.serialNumber != '' || x.serialNumber != null );
            if(checkDuplicate.length == this.getOrderReceiptItemDetailsArray.length){
                this.snackbarService.openSnackbar('Serial number already available', ResponseMessageTypes.WARNING); 
                this.technicianReceivingFaultyForm.get('stockCount').patchValue('');
                return;
            }

            this.crudService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.FAULTY_GENERATE_BARCODE, this.technicianReceivingFaultyForm.get('stockCount').value)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    let resp = response.resources;
                    let newBarcodes = resp.split(',');
                    this.getOrderReceiptItemDetailsArray.controls.forEach(control => {
                        if (control.get('serialNumber').value == '' || control.get('serialNumber').value == undefined) {
                            if (newBarcodes.length > 0) {
                                control.get('serialNumber').setValue(newBarcodes[0]);
                                control.get('scannedSerialNumber').patchValue(true);
                                newBarcodes.splice(0, 1);
                            }
                        }
                    });
                    this.technicianReceivingFaultyForm.get('isBarcode').patchValue(false);
                    this.showGenerateBarcode = false;
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
            });
        }
  
    }

    inputChangeQuantity() {

        this.cusQuantityError = false;
        this.qtyErrorMessage = '';
        let quantity = this.technicianReceivingFaultyForm.get('customerQty').value;
        if (quantity == '' || quantity == undefined) {
            this.cusQuantityError = false;
            this.qtyErrorMessage = '';
            this.getOrderReceiptItemDetailsArray.controls.forEach(control => {
                control.get('isReturnToCustomer').setValue(false);
            });
        }

        if (quantity > this.technicianReceivingFaultyForm.get('stockCount').value) {
            this.cusQuantityError = true;
            this.qtyErrorMessage = "Customer quantity cannot be greater than stock quantity";
            return;
        }

        this.getOrderReceiptItemDetailsArray.controls.forEach(controls => {
            for (var i = 0; i < quantity; i++) {
                if (controls.get('serialNumber').value == '' || controls.get('serialNumber').value == undefined) {
                    // controls[i].get('isReturnToCustomer').setValue(true);
                    this.getOrderReceiptItemDetailsArray.controls[i].get('isReturnToCustomer').patchValue(true);
                }
            }
        });
    }

    scanSerialNumberInput() {
      
        this.showSerialNumberInput = false;
        this.serialErrorMessage = '';
        let serialNumber = this.technicianReceivingFaultyForm.get('scanSerialNumberInput').value;
       
        if (serialNumber == '') {
    
            this.showSerialNumberInput = true;
            this.serialErrorMessage = "Serial number is required";
            return;
        }

        let showSerialError: boolean = false;
        let serialNumberScanned: boolean = false;

        let  checkDuplicate = this.getOrderReceiptItemDetailsArray.value.find(x=> x.serialNumber == serialNumber && x.scannedSerialNumber);

        if(checkDuplicate){
           
            this.showSerialNumberInput = true;
            this.serialErrorMessage = "Duplicate Serial Number";
            return;
        }

        this.getOrderReceiptItemDetailsArray.controls.forEach((item) => {
       
            if ((item.get('serialNumber').value == serialNumber) && !item.get('scannedSerialNumber').value) {
                item.get('scannedSerialNumber').setValue(true);
            
                serialNumberScanned = true;
            }
            // if(item.get('serialNumber').value != serialNumber){
            
            //     showSerialError = true;
            // }
        });
        let showSerialErrors = this.getOrderReceiptItemDetailsArray.controls.filter(item=>item.get('serialNumber').value == serialNumber);
        showSerialError = showSerialErrors && showSerialErrors.length >0 ? false:true;
        if(serialNumberScanned){
            this.counter++;
            if(this.counter > this.getOrderReceiptItemDetailsArray.controls.length){
                this.showSerialNumberInput = true;
                this.serialErrorMessage = "Received Qty cannot be more than Return Qty";
                return;
            }
            this.snackbarService.openSnackbar('Scanned successfully', ResponseMessageTypes.SUCCESS); 
            this.technicianReceivingFaultyForm.get('scanSerialNumberInput').patchValue('');
        }
        if(showSerialError){
            this.showSerialNumberInput = true;
            this.serialErrorMessage = "Invalid Serial Number";
            return;
        }

        // this.getOrderReceiptItemDetailsArray.controls.forEach((item) => {
        //     if (item.get('serialNumber').value == serialNumber) {
        //         item.get('scannedSerialNumber').setValue(true);
        //         serialNumberScanned = true;
        //     }
        // });
        // let  scannedSerialNumberTemp = this.scannedSerialNumber.filter(x=> x == serialNumber);
        // if (scannedSerialNumberTemp && scannedSerialNumberTemp.length>0) {
        //     this.showSerialNumberInput = true;
        //     this.serialErrorMessage = "Duplicate Serial Number";
        //     return;
        // }
        // else {
        //     this.scannedSerialNumber.push(serialNumber);
        //     this.snackbarService.openSnackbar('Scanned successfully', ResponseMessageTypes.SUCCESS);
        //     this.technicianReceivingFaultyForm.get('scanSerialNumberInput').patchValue('');
        // }

    }

    onSubmit() {
        let exitFlag=false;
        this.isSubmit = true;
        this.showSerialNumberInput = false;
        this.serialErrorMessage = '';
        let scannedError = false;
       

        if(this.isSerialized && this.getOrderReceiptItemDetailsArray.invalid) 
          exitFlag = true;        

        let scannedSerials=[];
        if(this.isSerialized){
            this.getOrderReceiptItemDetailsArray.value.forEach((controls) => {           
                if(controls.serialNumber=='' || controls.serialNumber==null){
                 this.snackbarService.openSnackbar('Serial Number cannot be empty Generate Barcode', ResponseMessageTypes.WARNING);
                 exitFlag = true;
                }
                if (!controls.scannedSerialNumber) {
                    scannedError = true;
                    scannedSerials.push(controls.serialNumber);
                }
             });
        }     

        if( exitFlag == true) return;        
        if (scannedError && !this.checkSerialNumber) {
            this.showSerialNumberInput = true;
            //this.serialErrorMessage = `Serial numbers (${scannedSerials.toLocaleString()}) scanning is inprogress`;
            this.serialErrorMessage = 'No serial number scanned';
            return;
        }

        let receiptItems;

        this.getOrderReceiptItemDetailsArray.value.forEach(element => {
            delete element.addressDropdown;
            if(element.supplierName) {
               element['supplierId'] = element.supplierName.id;
               element['supplierName'] = element.supplierName.displayName;
            }
        });

        let serialNumbersArray = this.technicianReceivingFaultyForm.value['orderReceiptItemDetails'];

        receiptItems = {
            OrderReceiptItemId: null,
            OrderReceiptId: this.getfaultyDetails.faultyOrderItem.orderReceiptId,
            OrderItemId: this.getfaultyDetails.faultyOrderItem.itemId,
            Quantity: serialNumbersArray.length,
            IsSerialized: this.isSerialized,
            ReturnCustomerQuantity : this.technicianReceivingFaultyForm.get('customerQty').value ? Number(this.technicianReceivingFaultyForm.get('customerQty').value):0,
            OrderReceiptItemDetails: serialNumbersArray
        }

        const data = {
            OrderReceiptId: this.getfaultyDetails.faultyOrderItem?.orderReceiptId,
            OrderReceiptDate: this.getfaultyDetails.faultyOrderItem?.createdDate,
            OrderNumber: this.getfaultyDetails.faultyOrderItem?.qrCode,
            WarehouseId: this.getfaultyDetails.faultyOrderItem?.warehouseId,
            BatchNumber: this.getfaultyDetails.faultyOrderItem?.batchNumber,
            InventoryStaffId: this.getfaultyDetails.faultyOrderItem?.inventoryStaffId,
            Comment: this.getfaultyDetails.faultyOrderItem?.reason,
            CreditNoteId: null,
            LocationId: this.getfaultyDetails.faultyOrderItem?.locationId,
            OrderReferenceId: this.getfaultyDetails.faultyOrderItem?.orderReferenceId,
            OrderTypeId: this.getfaultyDetails.faultyOrderItem?.orderTypeId,
            BarcodeNumber: null,
            OrderReceiptNumber: this.getfaultyDetails.faultyOrderItem?.referenceNumber,
            CreatedUserId: this.userId,
            orderReceiptBatchId: this.getfaultyDetails.faultyOrderItem?.orderReceiptBatchId,
            ordertype: 'Faulty',
            OrderReceiptItem: receiptItems
        }


        this.crudService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RECEIPTING, data)
            .subscribe((response: IApplicationResponse) => {
                if (response.resources && response.statusCode === 200) {
                    this.isSubmit = false;
                    this.navigateToFaultyPage();
                    this.rxjsService.setGlobalLoaderProperty(false);              
                }
            });

    }

    navigateToList() {
        this.router.navigate(['inventory/order-receipts/receipting'], {
            queryParams: { tab: 1,screenType:this.screenType },
            skipLocationChange: true
        });
    }
    navigateToJobSelection() {
        this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
    }

    navigateToFaultyPage() {
        this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-faulty-item'],
        { queryParams: { id: this.rtrRequestId, isActiveNew: this.isActiveNew, isFaulty: this.isFaulty,screenType:this.screenType }, skipLocationChange: true });
    }

}