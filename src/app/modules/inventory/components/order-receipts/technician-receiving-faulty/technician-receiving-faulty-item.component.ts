import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderReceipt } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-technician-receiving-faulty-item',
    templateUrl: './technician-receiving-faulty-item.component.html',
    styleUrls: ['./technician-receiving-faulty-item.component.scss']
})
export class TechnicianReceivingFaultyItemComponent implements OnInit {

    receivingForm: FormGroup;
    userData: UserLogin;
    showDuplicateSerialError: boolean = false;
    duplicateSerialError: any;
    orderReceipt = new OrderReceipt();
    orderType: string = '';
    batchId: string = '0';
    isActiveNew: any = false;
    order: any;
    isDisabled: boolean = false;
    orderNumberControl = new FormControl();
    rtrRequestId: any;
    grvNumbersDisabled: boolean = false;
    filteredGRVNumbers: any = [];
    IsFaulty: boolean = false;
    screenType;
    constructor(
        private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute,
        private httpService: CrudService, private router: Router,
        private rxjsService: RxjsService, private snackbarService: SnackbarService,
        private store: Store<AppState>) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });

        this.rtrRequestId = this.activatedRoute.snapshot.queryParams.id;
        this.isActiveNew = this.activatedRoute.snapshot.queryParams.isActiveNew;
        this.screenType = this.activatedRoute.snapshot.queryParams.screenType;
        this.IsFaulty= this.activatedRoute.snapshot.queryParams.IsFaulty;
    }

    ngOnInit() {

        this.createTechnicianReceivingform();
        if (this.rtrRequestId) {
            this.getReceiveById().subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                    this.order = response.resources;
                    if (this.order != null) {
                        this.isDisabled = true;
                        this.grvNumbersDisabled = true;
                        this.receivingForm.controls.rtrRequestId.setValue(this.order.rtrRequestId);
                        this.receivingForm.controls.orderTypeName.setValue(this.order.orderTypeName);
                        this.receivingForm.controls.technicianName.setValue(this.order.technicianStockLocation);
                        this.receivingForm.controls.technicianLocation.setValue(this.order.technicianStockLocationName);
                        this.receivingForm.controls.warehouseName.setValue(this.order.warehouseName);
                        this.receivingForm.controls.serviceCallNumber.setValue(this.order.serviceCallNumber);
                        let poNumbers = {
                            displayName: response.resources.referenceNumber,
                            id: response.resources.orderReceiptId,
                        }
                        this.filteredGRVNumbers.push(poNumbers);
                        this.receivingForm.get('orderNumber').patchValue(poNumbers);
                        // this.receivingForm.controls.orderNumber.setValue(this.order.referenceNumber);
                        this.receivingForm.controls.qrCode.setValue(this.order.qrCode);
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                    else {
                        this.isDisabled = false;
                        this.rxjsService.setGlobalLoaderProperty(false);
                    }
                    this.rxjsService.setGlobalLoaderProperty(false);
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
        }
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    createTechnicianReceivingform() {
        this.receivingForm = this.formBuilder.group({
            rtrRequestId: [''],
            orderTypeName: [''],
            orderNumber: [''],
            technicianName: [''],
            technicianLocation: [''],
            warehouseName: [''],
            scanQrCodeInput: [''],
            serviceCallNumber: [''],
            qrCode: ['']
        });
        this.valueChange();
    }

    getReceiveById(): Observable<IApplicationResponse> {
        let params = new HttpParams().set('RTRRequestId', (this.rtrRequestId && this.rtrRequestId != undefined) ? this.rtrRequestId : '');
        return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDERRECEIVE_TECHNICIAN_FAULTY, null, null, params);
    }

    inputChangeRefNumbers(text: any) {
        
        if(this.userData.warehouseId == null || this.userData.warehouseId == ''){
            this.snackbarService.openSnackbar("User account not setup with default warehouse", ResponseMessageTypes.WARNING);
            return;
        }

        let otherParams = {}
        if (text.query == null || text.query == '') return;
        otherParams['DisplayName'] = text.query;
        otherParams['IsFaulty'] = this.IsFaulty;
        this.httpService.get(ModulesBasedApiSuffix.INVENTORY, 
        InventoryModuleApiSuffixModels.UX_ORDER_RECEIPTS_SEARCH, null, false,
        prepareGetRequestHttpParams(null, null, otherParams))
        .subscribe((response: IApplicationResponse) => {
            if (response.isSuccess && response.resources.length > 0) {
                this.filteredGRVNumbers = response.resources;
                this.rxjsService.setGlobalLoaderProperty(false);
            }
            else if(response.resources.length == 0) {
                this.snackbarService.openSnackbar("No Records Found", ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
            else {
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
            }
        });
    }
    valueChange(){
        // this.receivingForm.get("scanQrCodeInput")
        // .valueChanges.subscribe((scanQrCodeInput: string) => {
        // console.log('scanQrCodeInput',scanQrCodeInput?.length);
        // if(scanQrCodeInput?.length==1)
        //    setTimeout(()=>{
        //     this.goodReturnsScanItem('scan');
        //    }, 5000);
        //     //this.goodReturnsScanItem('scan');
        // });
    }
    goodReturnsScanItem(scan?:any): void {
        // if(scan=='scan' && this.isDesktop()){
        //     return;
        // }
        if(this.userData.warehouseId == null || this.userData.warehouseId == ''){
            this.snackbarService.openSnackbar("User account not setup with default warehouse", ResponseMessageTypes.WARNING);
            return;
        }

        this.showDuplicateSerialError = false;
        if (this.receivingForm.get('scanQrCodeInput').value == '') {
            this.showDuplicateSerialError = true;
            this.duplicateSerialError = "Serial number is required";
            return;
        }
        else {
            this.getFaulty(this.receivingForm.get('scanQrCodeInput').value, 'scan');
        }
    }
 
    getFaulty(pid, type) {

        if (pid && pid != null) {
            let pId = (typeof pid === 'object') ? pid.id : pid;
            let params;
            if (type == 'scan') {
                params = new HttpParams().set('QRCode', pId);
            }
            else if (type == 'order') {
                params = new HttpParams().set('ReferenceNumber', pId);
            }

            this.httpService.get(ModulesBasedApiSuffix.INVENTORY,
                InventoryModuleApiSuffixModels.ORDERRECEIVE_TECHNICIAN_FAULTY, null, null, params)
                .subscribe({
                    next: response => {
                      if(response.isSuccess){
                        this.order = response.resources;
                        if (this.order != null) {
                            this.isDisabled = true;
                            this.grvNumbersDisabled = true;
                            this.receivingForm.controls.rtrRequestId.setValue(this.order.rtrRequestId);
                            this.receivingForm.controls.orderTypeName.setValue(this.order.orderTypeName);
                           // this.receivingForm.controls.orderNumber.setValue(this.order.goodsReturnNumber);
                            this.receivingForm.controls.technicianName.setValue(this.order.techStockLocation);
                            this.receivingForm.controls.technicianLocation.setValue(this.order.techStockLocationName);
                            this.receivingForm.controls.warehouseName.setValue(this.order.warehouseName);
                            this.receivingForm.controls.serviceCallNumber.setValue(this.order.callInitiationNumber);
                            // this.receivingForm.controls.orderNumber.setValue(this.order.referenceNumber);
                            let poNumbers = {
                                displayName: response.resources.referenceNumber,
                                id: response.resources.orderReceiptId,
                            }
                            this.filteredGRVNumbers.push(poNumbers);
                            this.receivingForm.get('orderNumber').patchValue(poNumbers);
                            this.receivingForm.controls.qrCode.setValue(this.order.qrCode);
                            this.rxjsService.setGlobalLoaderProperty(false);
                        }
                        else {
                            this.isDisabled = false;
                            this.rxjsService.setGlobalLoaderProperty(false);
                        }
                      }
                      else{
                          if(response?.message)
                            this.snackbarService.openSnackbar(response.message,ResponseMessageTypes.ERROR)
                      }
                      this.rxjsService.setGlobalLoaderProperty(false);
                    }
                });
            if (pid.id !== undefined)
                this.orderReceipt.orderReferenceId = pid.id;
        }
    }

    //Redirect to barcode scan page and pass related items
    barcodeScan(faultyOrderItem, orderedItems) {
        if (faultyOrderItem) {
            let order = {
                itemName: faultyOrderItem.itemName,
                itemCode: faultyOrderItem.itemCode,
                isSerialized:!faultyOrderItem.isNotSerialized,
                faultyOrderItem: faultyOrderItem,
                orderedItems: orderedItems
            }
            this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-faulty-item-scan'],{
                state: {
                    data: order,
                    id: this.receivingForm.get('rtrRequestId').value,
                    createdUserId: this.userData.userId,
                    screenType:this.screenType,
                    isActiveNew: this.isActiveNew,
                    isFaulty: this.IsFaulty
                }
            });
        }
    }

    navigateToListPage() {
        this.router.navigate(['inventory/order-receipts/receipting'], {
            queryParams: { tab: 1 ,screenType:this.screenType},
            skipLocationChange: true
        });
    }

    naviagetToVerifyPage() {
        this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-faulty-item-verify'], {
            queryParams: {
                id: this.receivingForm.get('rtrRequestId').value,
                createdUserId: this.userData.userId,
                isActiveNew: this.isActiveNew,
                screenType:this.screenType
            }, skipLocationChange: true
        });
    }
    navigateToJobSelection() {
        this.router.navigate(['/inventory', 'job-selection'], { skipLocationChange: true });
    }

    View() {
        this.router.navigate(['inventory/order-receipts/receipting/technician-receiving-view'], {
            queryParams: {
                RTRRequestId: this.rtrRequestId,
                OrderTypeId: this.order.orderTypeId,
                orderReceiptId: this.order.orderReceiptId,
                screenType:this.screenType,
                isActiveNew: this.isActiveNew,
            }, skipLocationChange: true
        });
    }
}