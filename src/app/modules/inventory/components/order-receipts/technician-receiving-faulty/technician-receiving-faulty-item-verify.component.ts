import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-technician-receiving-faulty-item-verify',
    templateUrl: './technician-receiving-faulty-item-verify.component.html',
    styleUrls: ['./technician-receiving-faulty-item-verify.component.scss']
})
export class TechnicianReceivingFaultyItemVerifyComponent implements OnInit {

    userData: UserLogin;
    isActiveNew: boolean = false;
    isButtonDisabled: boolean = false;
    order: any;
    rtrRequestId: any;
    imageName: string;
    errorMessage: string;
    orderReceiptVerifyForm: FormGroup;
    userId: any;
    public formData = new FormData();
    screenType;
    @ViewChild(SignaturePad, { static: false }) signaturePad: any;

    public signaturePadOptions: Object = {
        'minWidth': 1,
        'canvasWidth': 500,
        'canvasHeight': 80
    };


    constructor(
        private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute,
        private httpService: CrudService, private router: Router,
        private rxjsService: RxjsService,
        private store: Store<AppState>) {
        this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
            if (!userData) return;
            this.userData = userData;
        });

        this.rtrRequestId = this.activatedRoute.snapshot.queryParams.id;
        this.isActiveNew = this.activatedRoute.snapshot.queryParams.isActiveNew;
        this.screenType = this.activatedRoute.snapshot.queryParams.screenType;
        this.userId = this.activatedRoute.snapshot.queryParams.createdUserId;
    }

    ngOnInit() {

        if (this.rtrRequestId) {
            this.getReceiveById().subscribe((response: IApplicationResponse) => {
                if (response.isSuccess && response.statusCode === 200) {
                    this.order = response.resources;
                }
                this.rxjsService.setGlobalLoaderProperty(false);
            });
        }

        this.orderReceiptVerifyForm = this.formBuilder.group({
            comments: ['', Validators.required]
        });

        this.rxjsService.setGlobalLoaderProperty(false);
    }

    getReceiveById(): Observable<IApplicationResponse> {
        let params = new HttpParams().set('RTRRequestId', (this.rtrRequestId && this.rtrRequestId != undefined) ? this.rtrRequestId : '');
        return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDERRECEIVE_TECHNICIAN_FAULTY, null, null, params);
    }

    drawComplete() {

    }

    clearPad() {
        this.signaturePad.clear();
    }

    ngAfterViewInit() {
        this.signaturePad.set('minWidth', 1);
        this.signaturePad.resizeCanvas();
        this.signaturePad.clear();
    }

    dataURItoBlob(dataURI) {
        const byteString = window.atob(dataURI);
        const arrayBuffer = new ArrayBuffer(byteString.length);
        const int8Array = new Uint8Array(arrayBuffer);
        for (let i = 0; i < byteString.length; i++) {
            int8Array[i] = byteString.charCodeAt(i);
        }
        const blob = new Blob([int8Array], { type: 'image/jpeg' });
        return blob;
    }

    onSubmit() {

        if (this.orderReceiptVerifyForm.invalid) {
            return;
        }

        let itemDetails = [];
        this.order.orderItems.forEach(element => {
            let tmp = {}
            tmp['ItemId'] = element.itemId;
            tmp['ItemCode'] = element.itemCode;
            tmp['ItemName'] = element.itemName;
            tmp['ReceivedQuantity'] = element.receivedQty;
            tmp['VarianceQuantity'] = element.varianceQty;
            itemDetails.push(tmp);
        });

        const data = {
            OrderReceiptId: this.order.orderReceiptId,
            BatchId: this.order.orderReceiptBatchId,
            DocumentType: this.order.orderTypeName,
            Comment: this.orderReceiptVerifyForm.get('comments').value,
            OrderTypeId: this.order.orderTypeId,
            InventoryStaffId: this.order.inventoryStaffId,
            CreatedUserId: this.userId,
            orderReceiptBatchId: this.order.orderReceiptBatchId,
            ItemList: itemDetails,
            OrderType: 'Faulty',
        }

        this.isButtonDisabled = true;
        if (this.signaturePad.signaturePad._data.length > 0) {
            this.imageName = this.order.referenceNumber + "-signature.jpeg";
            const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
            const imageFile = new File([imageBlob], this.imageName, { type: 'image/jpeg' });
            this.formData.append("document_files", imageFile);
        }

        this.formData.append("documentDetails", JSON.stringify(data));

        this.rxjsService.setGlobalLoaderProperty(true);
        this.httpService.fileUpload(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_RECEIPT_FINAL_SUBMIT, this.formData)
            .subscribe({
                next: response => {
                    if (response.isSuccess && response.statusCode == 200) {
                        if(this.screenType == 'job-selection')
                          this.navigateToJobSelection();
                        else
                          this.navigateToFaultyPage();
                    }
                },
                error: err => {
                    this.errorMessage = err;
                    this.isButtonDisabled = false;
                }
            });
        this.rxjsService.setGlobalLoaderProperty(false);
    }

    isStockInfoDialogModal: boolean = false;
    modalDetails: any = {};

    //Redirect to barcode scan page and pass related items
    barcodeScan(itemCode,itemName, items) {

        this.modalDetails = {
            stockCode: itemCode,
            stockDescription: itemName,
            screenType:this.screenType,
            serialNumbers: items?.orderReceiptItemDetails,
            isSerialized:items.isSerialized
        }
        this.isStockInfoDialogModal = true;

        // if (faultyOrderItem) {
        //     let order = {
        //         itemName: faultyOrderItem.itemName,
        //         itemCode: faultyOrderItem.itemCode,
        //         faultyOrderItem: faultyOrderItem,
        //         orderedItems: orderedItems
        //     }
        //     this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-faulty-item-scan'],{
        //         state: {
        //             data: order,
        //             id: this.rtrRequestId,
        //             createdUserId: this.userData.userId,
        //             isActiveNew: this.isActiveNew
        //         }
        //     });
        // }
    }

    navigateToList() {
        this.router.navigate(['inventory/order-receipts/receipting'], {
            queryParams: { tab: 1,screenType:this.screenType },
            skipLocationChange: true
        });
    }

    navigateToFaultyPage() {
        this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-faulty-item'],
        { queryParams: { id: this.rtrRequestId, isActiveNew: this.isActiveNew, createdUserId: this.userId }, skipLocationChange: true });
    }
    navigateToJobSelection (){
        this.router.navigate(['/inventory/job-selection'], { queryParams: { tab: 0 } });
    }
    View() { }
}


