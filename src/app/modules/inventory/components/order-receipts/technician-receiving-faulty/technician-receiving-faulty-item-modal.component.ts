import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RxjsService } from '@app/shared';


@Component({
    selector: 'app-technician-receiving-faulty-item-modal',
    templateUrl: './technician-receiving-faulty-item-modal.component.html',
    styleUrls: ['./technician-receiving-faulty-item-verify.component.scss']
})

export class TechnicianReceivingFaultyItemModalComponent implements OnInit {

    modalDetails: any;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data,
        private rxjsService: RxjsService,
    ) {

        this.modalDetails = data.serialNumberDetails;
    }

    ngOnInit(): void {
        this.rxjsService.setDialogOpenProperty(true);
    }
}