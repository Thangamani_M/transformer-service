import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { KeyValuePair, OrderReceipt } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels, OrderTypes } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { debounceTime, finalize, switchMap, tap } from 'rxjs/operators';
import { Supplier } from '../../../models/Supplier';

@Component({
  selector: 'app-technician-receiving-goods-return',
  templateUrl: './technician-receiving-goods-return.component.html',
  styleUrls: ['./technician-receiving-goods-return.component.scss']
})
export class TechnicianReceivingGoodsReturnComponent implements OnInit {
  @ViewChild('serialNumber', { static: false }) serilNumberField: ElementRef;

  userData: UserLogin;
  orderReferenceId: string;
  orderNumberControl = new FormControl();
  applicationResponse: IApplicationResponse;
  uxOderTypes: any;
  orderType: string = '';
  storageLocation: string = '';
  isNew: boolean = false;
  showSupplier: boolean = false;
  receiptingOrderForm: FormGroup;
  keyValuePair: KeyValuePair;
  uxStorageLocations: any;
  filteredFaultys: any[] = [];
  purchaseOrderAutoFillForm: FormGroup;
  isLoading = false;
  supplierName: string = '';
  order: any;
  supplier = new Supplier();
  orderReceipt = new OrderReceipt();
  showBatchNo = false;
  orderTypes = OrderTypes;
  orderTypesOptions = [];
  receivingForm: FormGroup;
  receiptingType: any;
  batchId: string = '0';
  //mask validations
  public alphaNumericWithSpacePattern = { '0': { pattern: new RegExp('^[A-Za-z0-9 ]*$') } };
  @ViewChild('ddl', null) myDiv: ElementRef<HTMLElement>;
  screenType;
  isDisabled: boolean = false;
  showDuplicateSerialError: boolean = false;
  duplicateSerialError: any;
  isActiveNew: boolean = false;

  constructor(
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute,
    private httpService: CrudService, private router: Router,
    private snackbarService: SnackbarService, private rxjsService: RxjsService,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });



    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.orderReceipt = this.router.getCurrentNavigation().extras.state.data;
      this.orderType = this.orderReceipt.orderTypeId;
      this.batchId = this.orderReceipt.orderReceiptBatchId;
      this.isActiveNew = this.router.getCurrentNavigation().extras.state.isActiveNew;
    }
    if (this.orderReceipt.orderReceiptId == undefined) {
      this.orderReceipt.orderReferenceId = this.activatedRoute.snapshot.queryParams.id;
      this.orderType = this.activatedRoute.snapshot.queryParams.orderTypeId;
      this.batchId = this.activatedRoute.snapshot.queryParams.batchId;
      this.isActiveNew = this.activatedRoute.snapshot.queryParams.isActiveNew;
    }
    this.screenType = this.activatedRoute.snapshot.queryParams.screenType;

  }

  ngOnInit() {

    this.receivingForm = this.formBuilder.group({
      requestId: [''],
      orderTypeName: [''],
      orderNumber: [''],
      technicianName: [''],
      technicianLocation: [''],
      warehouseName: [''],
      scanQrCodeInput: [''],
      receivingType: [true]

    });

    if (this.orderReceipt.orderReferenceId != undefined) {
      this.getFaulty(this.orderReceipt.orderReferenceId, 'id');
    }

    this.orderTypesOptions = Object.entries(this.orderTypes).map(([key, value]) => ({ key, value }))
    //temporarly added this below code. we will be remove this code after implementation
    this.orderTypesOptions = this.orderTypesOptions.filter(function (obj) {
      return obj.key !== 'REPLACEMENT';
    });

    this.orderNumberControl.valueChanges.pipe(debounceTime(300), tap(() => this.isLoading = true),
      switchMap(value => {
        if (!value) {
          this.isLoading = false;
          return this.filteredFaultys;
        } else if (typeof value === 'object') {
          this.isLoading = false;
          return this.filteredFaultys = [];
        } else {
          if (this.orderReceipt.orderTypeId === OrderTypes.NEW) {
            let resources = this.getUXFaultySearch(value);
            return resources;
          } else if (this.orderReceipt.orderTypeId.toLocaleLowerCase() == OrderTypes.RETURN) {
            return this.getSupReturnNumbersUX(value);
          } else {
            let resources = this.getUXRepairRequestSearch(value);
            return resources;
          }

        }
      })).subscribe(results => {
        this.filteredFaultys = results.resources;
      });

    this.rxjsService.setGlobalLoaderProperty(false);
  }

  getSupReturnNumbersUX(value: any): Observable<any> {
    return this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_SUP_RETURN_NUMBER_SEARCH, null, true, prepareGetRequestHttpParams(null, null, { displayName: value }))
      .pipe(
        finalize(() => {
          this.isLoading = false,
            this.rxjsService.setGlobalLoaderProperty(false);
        }),
      );
  }

  getUXRepairRequestSearch(value: string): Observable<any> {
    return this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_REPAIRREQUEST_SEARCH, null, true, prepareGetRequestHttpParams(null, null, { searchText: value, orderTypeId: this.orderReceipt.orderTypeId }))
      .pipe(
        finalize(() => {
          this.isLoading = false,
            this.rxjsService.setGlobalLoaderProperty(false);
        }),
      );

  }

  getUXFaultySearch(value: string): Observable<any> {
    return this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_PURCHASEORDER_SEARCH, null, true,
      prepareGetRequestHttpParams(null, null, { displayName: value }))
      .pipe(
        finalize(() => {
          this.isLoading = false,
            this.rxjsService.setGlobalLoaderProperty(false);
        }),
      );
  }

  getUXOrderType(): void {
    this.httpService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_TYPES)
      .subscribe(resp => {
        this.applicationResponse = resp;
        this.uxOderTypes = this.applicationResponse.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      });
  }

  goodReturnsScanItem(): void {

    if(this.userData.warehouseId == null || this.userData.warehouseId == ''){
      this.snackbarService.openSnackbar("User account not setup with default warehouse", ResponseMessageTypes.WARNING);
      return;
    }

    this.showDuplicateSerialError = false;
    if (this.receivingForm.get('scanQrCodeInput').value == '') {
      this.showDuplicateSerialError = true;
      this.duplicateSerialError = "Serial number is required";
      return;
    }
    else {
      this.getFaulty(this.receivingForm.get('scanQrCodeInput').value, 'scan');
    }
  }

  filteredGRVNumbers: any = [];

  inputChangeReferenceId(text: any): void {

    if(this.userData.warehouseId == null || this.userData.warehouseId == ''){
      this.snackbarService.openSnackbar("User account not setup with default warehouse", ResponseMessageTypes.WARNING);
      return;
    }

    let otherParams = {}
    if (text.query == null || text.query == '') return;
    otherParams['DisplayName'] = text.query;
    this.httpService.get(ModulesBasedApiSuffix.INVENTORY,
    InventoryModuleApiSuffixModels.UX_GOODS_RETURN, null, false,
    prepareGetRequestHttpParams(null, null, otherParams))
    .subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200 && response.resources.length > 0) {
        this.filteredGRVNumbers = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else {
        this.snackbarService.openSnackbar('Records not found', ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

    // this.getFaulty(this.receivingForm.get('orderNumber').value, 'order');
  }

  //Redirect to barcode scan page and pass related items
  barcodeScan(purchaseOrderItem, purchaseOrderDetails) {

    let viewType = this.receivingForm.get('receivingType').value;
    if (purchaseOrderDetails) {
      this.orderReceipt.orderNumber = purchaseOrderDetails.orderNumber;
      this.orderReceipt.orderReferenceId = purchaseOrderDetails.orderReferenceId;
      this.orderReceipt.warehouseId = this.userData.warehouseId;
      this.orderReceipt.inventoryStaffId = this.userData.userId;
      this.orderReceipt.supplierId = purchaseOrderDetails.supplierId;
      this.orderReceipt.orderReceiptId = purchaseOrderItem.orderReceiptId;
      let order = {
        itemName: purchaseOrderItem.itemName,
        itemCode: purchaseOrderItem.itemCode,
        purchaseOrderItem: purchaseOrderItem,
        orderReceipt: this.orderReceipt,
      }
      // if (purchaseOrderItem.isConsumable) {
      if (!purchaseOrderItem.isSerialized) {
        this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-goods-return-stock-count'],
          {
            state: {
              data: order,
              id: this.orderReceipt.orderReferenceId,
              orderTypeId: this.orderType,
              batchId: this.batchId ? this.batchId : '',
              isActiveNew: this.isActiveNew,
              screenType:this.screenType
            }
          });
      }
      else {
        this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-goods-return-item-scan'],
          {
            state: {
              data: order,
              id: this.orderReceipt.orderReferenceId,
              orderTypeId: this.orderType,
              batchId: this.batchId ? this.batchId : '',
              isActiveNew: this.isActiveNew,
              screenType:this.screenType
            }
          });
      }
    }
  }

  //Selected value to assign auto fill textbox
  displayFaultyNo(keyValuePair: KeyValuePair) {
    if (keyValuePair) { return keyValuePair.displayName; }
  }

  //Get purchase order with related items and suppliers
  getFaulty(pid, type) {

    let receiveType = this.receivingForm.get('receivingType').value;

    if (pid && pid != null) {
      let pId = (typeof pid === 'object') ? pid.id : pid;
      let params;
      if (type == 'scan') {
        params = new HttpParams().set('QRCode', pId);
      }
      // else if (type == 'order') {
      //   params = new HttpParams().set('GoodsReturnNumber', pId);
      // }
      else {
        params = new HttpParams().set('GoodsReturnId', pId);
      }



      this.httpService.get(ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.ORDERRECEIVE_TECHNICIAN_GOODSRETURN, null, null, params)
        .subscribe({
          next: response => {
            if(response.isSuccess && response.statusCode == 200){
              this.order = response.resources;
              if (this.order != null) {
                this.isDisabled = true;
                this.orderNumberControl.setValue({ displayName: this.order.orderNumber });
                this.orderReceipt.orderReceiptId = this.order.orderReceiptId;
                this.orderReceipt.isVerified = this.order.isVerified;
                this.orderType = this.order.orderTypeId;
                this.batchId = this.order.orderReceiptBatchId;
                this.orderReceipt.orderTypeId = this.orderType;
                this.orderReceipt.orderReceiptBatchId = this.order.orderReceiptBatchId;
                this.receivingForm.controls.requestId.setValue(this.order.requestId);
                this.receivingForm.controls.orderTypeName.setValue(this.order.orderTypeName);

                let orders = {
                  id: this.order.orderReferenceId,
                  displayName : this.order.goodsReturnNumber,
                }
                this.filteredGRVNumbers = orders;
                this.receivingForm.controls.orderNumber.setValue(orders);

                this.receivingForm.controls.technicianName.setValue(this.order.technicianName);
                this.receivingForm.controls.technicianLocation.setValue(this.order.technicianLocation);
                this.receivingForm.controls.warehouseName.setValue(this.order.warehouseName);
                this.receivingForm.get('scanQrCodeInput').patchValue(response.resources.qrCode);
                this.rxjsService.setGlobalLoaderProperty(false);
              }
              else {
                this.isDisabled = false;
                this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
                this.rxjsService.setGlobalLoaderProperty(false);
              }
            }
            else {
              this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          }
        });
      if (pid.id !== undefined)
        this.orderReceipt.orderReferenceId = pid.id;
    }
  }

  getreceivedetailsById(purchaseOrderId: string) {

    this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PURCHASEORDER_RELATEDITEMS,
      purchaseOrderId,
      false,
      null
    ).subscribe((response: IApplicationResponse) => {
      if (response.resources) {
        this.order = response.resources;

        this.receivingForm.controls.requestId.setValue(this.order.requestId);
        this.receivingForm.controls.orderTypeName.setValue(this.order.orderTypeName);
        // this.receivingForm.controls.orderNumber.setValue(this.order.orderNumber);
        let orders = {
          id: this.order.orderReferenceId,
          disaplayName : this.order.goodsReturnNumber,
        }
        this.filteredGRVNumbers = orders;
        this.receivingForm.controls.orderNumber.setValue(orders);
        this.receivingForm.get('scanQrCodeInput').patchValue(response.resources.qrCode);
        this.receivingForm.controls.supplierName.setValue(this.order.supplierName);
        this.receivingForm.controls.storageLocationName.setValue(this.order.storageLocationName);
        this.isDisabled = true;
        this.rxjsService.setGlobalLoaderProperty(false);

      }
    });
  }

  //Get storage location based on warehouse
  getUXstorageLocations() {
    const params = new HttpParams().set('warehouseid', this.userData.warehouseId)
    this.httpService.dropdown(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_LOCATION, params
    ).subscribe(resp => {
      this.uxStorageLocations = resp.resources;
      //Defalut storage location should be staging bay location
      this.orderReceipt.locationId = "72b3bff0-7425-475b-a554-7addbcc3e9fa";
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }

  //hide and show ui elements based on receipting type
  receiptingTypeChange(event) {
    let selectedReceiptingType = event;
    this.receiptingType = selectedReceiptingType.toLocaleLowerCase();
    //New
    if (selectedReceiptingType.toLocaleLowerCase() === 'new') {
      this.isNew = true;
      this.showSupplier = false;
      this.filteredFaultys = [];
      this.order = null;
      this.showBatchNo = false;
      this.orderNumberControl.setValue({ displayName: '' });
      this.orderReceipt.batchNumber = null;
    }
    //Return
    else if (selectedReceiptingType.toLocaleLowerCase() === 'return') {
      this.EnableDisableControls();
    }
    //Replacement
    else if (selectedReceiptingType.toLocaleLowerCase() === 'replacement') {
      this.EnableDisableControls();
    }//Faulty
    else if (selectedReceiptingType.toLocaleLowerCase() === 'faulty') {
      this.EnableDisableControls();
    }
  }

  private EnableDisableControls() {
    this.isNew = true;
    this.showSupplier = false;
    this.filteredFaultys = [];
    this.order = null;
    this.showBatchNo = false;
    this.orderNumberControl.setValue({ displayName: '' });
    this.orderReceipt.batchNumber = null;
  }

 verifyFaulty() {

    if (this.order != undefined) {
      let returnedItems = this.order.orderItems.filter(function (item) {
        return item.receivedQty > 0;
      });

      if (returnedItems.length === 0) {
        this.snackbarService.openSnackbar('Please scan stocks to be returned.', ResponseMessageTypes.WARNING);
        return;
      }
    }

    if (this.orderReceipt.orderReceiptId != null) {
      this.orderReceipt.orderReceiptBatchId = this.batchId;
      this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-goods-return-verify/'],
        {
          state: {
            data: this.orderReceipt,
            screenType:this.screenType,
            isActiveNew: this.isActiveNew
          },
          queryParams: { screenType: this.screenType }
        });
    }
    else {
      this.snackbarService.openSnackbar('Please check mandatory fields', ResponseMessageTypes.WARNING);
    }
  }

  private listPage() {
    this.router.navigate(['/inventory/order-receipts/receipting'], { queryParams: { tab: 1 } });
  }

  View() {
    if (this.orderReceipt.orderReferenceId) {
      this.router.navigate(['inventory/order-receipts/receipting/technician-receiving-goods-return-view'], {
        queryParams:
        {
          GoodsReturnId: this.orderReceipt.orderReferenceId,
          OrderTypeId: this.orderReceipt.orderTypeId,
          orderReceiptId: this.orderReceipt.orderReceiptId,
          isActiveNew: this.isActiveNew,
          screenType:this.screenType,
        }, skipLocationChange: true
      });
    }
  }

  navigateToListPage() {
    this.router.navigate(['/inventory/order-receipts/receipting'],
    { queryParams: { tab: 1 }, skipLocationChange: true });
  }

  navigateToFaultyPage() {
    this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-faulty-item'],
    {
      queryParams: { isActiveNew: true, IsFaulty: this.receivingForm.get('receivingType').value ,screenType:this.screenType},
      skipLocationChange: true
    });
  }
  navigateToJobSelection (){
    this.router.navigate(['/inventory/job-selection'], { queryParams: { tab: 0 } });
  }
}
