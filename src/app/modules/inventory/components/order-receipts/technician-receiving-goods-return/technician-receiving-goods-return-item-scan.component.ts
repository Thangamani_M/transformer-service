declare var $: any;
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { OrderReceipt, OrderReceiptItemDetail } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { AlertService, CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels, OrderTypes } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-technician-receiving-goods-return-item-scan',
  templateUrl: './technician-receiving-goods-return-item-scan.component.html',
  styleUrls: ['./technician-receiving-goods-return-item-scan.component.scss']
})

export class TechnicianReceivingGoodsReturnItemScanComponent implements OnInit, AfterViewInit {
  @ViewChild("serialNumber", { static: false }) serilNumberField: ElementRef;

  faulty: any;
  totalFaultyItemCount: number;
  orderItemBarcodeScanedCount: number = 0;
  itemName: string;
  orderReceiptItemDetails: OrderReceiptItemDetail[] = [];
  scanValue: any;
  faultyItems: any;
  orderReceipt = new OrderReceipt();
  duplicateMessage = '';
  applicationResponse: IApplicationResponse;
  faultyData: any;
  isDamanged: string = 'No';
  IsValidQuantity: boolean = true;
  damagedQty: number;
  oldSerilNumber: string = null;
  faultyItemBarcode: any[] = [];
  status: any = [];
  serialNumberReadOnly: boolean = false;
  orderReceiptItem: OrderReceiptItemDetail;
  isFaulty: boolean = false;
  isNewStatus: string = 'No';
  isSerialized: string = 'Yes';
  nonSerializedCount: number;
  isDisabled: boolean = true;
  alphabets: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  loggedUser: any;
  isbrudcrumb: string;
  isActiveNew: boolean;
  screenType;
  constructor(
    private router: Router, private httpService: CrudService,
    private alertService: AlertService,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData) => {
      this.loggedUser = userData;
    });
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.faulty = this.router.getCurrentNavigation().extras.state.data;
      this.isActiveNew = this.router.getCurrentNavigation().extras.state.isActiveNew;
      this.screenType= this.router.getCurrentNavigation().extras.state.screenType;
      this.itemName = this.faulty.itemName + " - " + this.faulty.itemCode;
      this.orderReceipt = this.faulty.orderReceipt;
      this.totalFaultyItemCount = this.faulty.purchaseOrderItem.quantity;

      if (this.orderReceipt.orderTypeId == OrderTypes.GOODSRETURN) {
        this.isFaulty = true;
        this.orderReceiptItemDetails.push(...this.faulty.purchaseOrderItem.orderReceiptItemDetails.filter(x => x.isScanned === true));
        this.getScanedCount(false);
      }
      else {
        this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-goods-return-edit'], {
          queryParams: {
            isActiveNew: this.isActiveNew
          }, skipLocationChange: true
        });
      }
    }
  }

  ngAfterViewInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  validateFaultyItemSerialNumber(newSerialNumber): boolean {
    let IsValid = true;
    if (newSerialNumber.length < 5) {
      IsValid = false;
    }

    if (this.orderReceipt.orderTypeId == OrderTypes.GOODSRETURN) {
      if (this.faulty.purchaseOrderItem.orderReceiptItemDetails.find(x => x.serialNumber !== null && x.serialNumber.toLowerCase() === newSerialNumber.toLowerCase())) {
        IsValid = true;
        this.oldSerilNumber = newSerialNumber;
        this.orderReceiptItem = this.faulty.purchaseOrderItem.orderReceiptItemDetails.find(x => x.serialNumber !== null && x.serialNumber.toLowerCase() === newSerialNumber.toLowerCase());
      } else if (this.orderReceiptItemDetails.find(x => x.serialNumber != null && x.serialNumber.toLowerCase() === newSerialNumber.toLowerCase())) {
        this.snackbarService.openSnackbar('"' + newSerialNumber + '"- serial number already exist', ResponseMessageTypes.WARNING);
        IsValid = false;

      } else {
        this.snackbarService.openSnackbar("Invalid serial number", ResponseMessageTypes.WARNING);
        IsValid = false;
      }
    }

    else if (newSerialNumber === "" || newSerialNumber === undefined) {
      IsValid = false;
      this.snackbarService.openSnackbar("Please scan/enter serial number", ResponseMessageTypes.WARNING);
    }
    return IsValid;
  }

  modelChanged(newSerialNumber: string) {
    if (this.validateFaultyItemSerialNumber(newSerialNumber)) {
      if (this.orderReceiptItemDetails.find(x => x.serialNumber != null && x.serialNumber.toLowerCase() === newSerialNumber.toLowerCase() && x.isDeleted != true)) {
        this.snackbarService.openSnackbar('"' + newSerialNumber + '"- serial number already exist', ResponseMessageTypes.WARNING);
      } else {
        let itemdetail = null;
        let warrantyStatus = null;
        if (this.orderReceiptItem.warrantyStatus != undefined) {
          warrantyStatus = this.orderReceiptItem.warrantyStatus;
        }
        itemdetail = {
          serialNumber: newSerialNumber,
          isDeleted: false,
          orderReceiptItemDetailId: null,
          orderReceiptItemId: this.orderReceipt.orderReceiptItem.orderReceiptItemId,
          orderItemId: this.orderReceipt.orderReceiptItem.orderItemId,
          barcode: '',
          returnBarcode: '',
          oldSerialNumber: this.oldSerilNumber,
          warrantyStatus: warrantyStatus
        };

        this.orderReceiptItemDetails.unshift(itemdetail);
        this.getScanedCount(false);
        this.oldSerilNumber = null;
      }
    }

    if (this.isSerialized === 'Yes') {
      this.serilNumberField.nativeElement.value = "";
      this.serilNumberField.nativeElement.focus();
    }
  }

  removeAllScanedBarcode() {
    this.orderReceiptItemDetails.forEach(element => {
      if (element.orderReceiptItemDetailId != null) {
        element.isDeleted = true;
      } else {
        const index: number = this.orderReceiptItemDetails.indexOf(element);

        this.orderReceiptItemDetails.splice(index);
      }
    });

    this.getScanedCount(false);
  }

  //delete scaned serial number or barcode
  removeScanedBarcode(barcode) {
    const index: number = this.orderReceiptItemDetails.indexOf(barcode);
    if (index !== -1) {
      if (this.orderReceiptItemDetails[index].orderReceiptItemDetailId != null) {
        this.orderReceiptItemDetails[index].isDeleted = true;
      }
      else {
        this.orderReceiptItemDetails.splice(index, 1);
      }
      this.getScanedCount(false);
    }
  }

  private getScanedCount(isDamaged) {
    let count = 0;

    if (this.orderReceipt.orderTypeId == OrderTypes.GOODSRETURN) {
      this.orderReceiptItemDetails.forEach(element => {
        if (!element.isDeleted && (element.serialNumber !== null && element.serialNumber !== undefined)) {
          count = count + 1;
        }
      });
    }

    if (isDamaged == true && this.damagedQty !== undefined && this.orderItemBarcodeScanedCount + Number(this.damagedQty) > this.totalFaultyItemCount) {
      this.snackbarService.openSnackbar("Please check quantity exceeds", ResponseMessageTypes.WARNING);
      this.orderItemBarcodeScanedCount = this.orderItemBarcodeScanedCount - this.damagedQty;
      this.damagedQty = null;
    } else if (isDamaged == false && this.orderItemBarcodeScanedCount > this.totalFaultyItemCount) {
      this.snackbarService.openSnackbar("Please check quantity exceeds", ResponseMessageTypes.WARNING);
    }

    this.orderItemBarcodeScanedCount = count;

    let dmg = this.damagedQty == undefined ? 0 : this.damagedQty;
    if ((this.orderItemBarcodeScanedCount + Number(dmg)) === this.totalFaultyItemCount) {
      this.serialNumberReadOnly = true;
    } else {
      this.serialNumberReadOnly = false;
    }

    if (this.orderItemBarcodeScanedCount === this.totalFaultyItemCount && this.damagedQty === null) {
      this.damagedQty = null;
      this.isDamanged = 'No';
    }
  }

  ngOnInit() {
    if (this.orderReceipt.orderTypeId == OrderTypes.GOODSRETURN) {
      this.isbrudcrumb = "Faulty";
    }
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  cancelOrderReceipt() {
    this.orderReceipt.orderReceiptItem.orderItemId = this.faulty.purchaseOrderItem.stockOrderItemId;
    this.orderReceipt.orderReceiptItem.quantity = this.faulty.purchaseOrderItem.quantity;
    this.orderReceipt.orderReceiptItem.orderReceiptItemId = this.faulty.purchaseOrderItem.orderReceiptItemId;
    this.orderReceipt.orderReceiptItem.orderReceiptItemDetails = this.orderReceiptItemDetails;
    this.orderReceipt.orderReceiptItem.orderReceiptItemDetails = [];

    if (this.orderReceipt.orderTypeId == OrderTypes.GOODSRETURN) {
      this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-goods-return-edit'],
        {
          queryParams: {
            id: this.faulty.orderReceipt.orderReferenceId,
            orderTypeId: this.orderReceipt.orderTypeId,
            screenType:this.screenType,
            isActiveNew: this.isActiveNew
          }
        });
    }
  }

  private assignOrderReceiptItemDetails() {
    this.orderReceiptItemDetails.forEach(element => {
      if (element.serialNumber !== null && element.serialNumber !== undefined) {
        this.orderReceipt.orderReceiptItem.orderReceiptItemDetails.push(element)
      }
    });
  }

  save() {
    this.assignOrderReceiptItemDetails();

    if (this.isDamanged === 'Yes' && (this.damagedQty == 0 || this.damagedQty == null)) {
      this.snackbarService.openSnackbar("Please enter damaged quantity", ResponseMessageTypes.WARNING);
    }
    else if (this.orderReceipt.orderReceiptItem.orderReceiptItemDetails.length > 0
      || (this.damagedQty !== undefined && (this.totalFaultyItemCount < Number(this.damagedQty))
        || (this.damagedQty !== undefined && Number(this.damagedQty) == this.totalFaultyItemCount))) {
      this.orderReceipt.orderReceiptItem.orderItemId = this.faulty.purchaseOrderItem.itemId;
      this.orderReceipt.orderReceiptItem.quantity = this.faulty.purchaseOrderItem.quantity;
      this.orderReceipt.orderReceiptItem.orderReceiptItemId = this.faulty.purchaseOrderItem.orderReceiptItemId;
      this.orderReceipt.orderReceiptItem.damagedQuantity = this.damagedQty;

      this.orderReceipt.inventoryStaffId = this.loggedUser["userId"];
      this.orderReceipt.createdUserId = this.loggedUser["userId"];
      this.orderReceipt.orderReceiptItem.createdUserId = this.loggedUser["userId"];

      this.httpService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_RECEIPTS, this.orderReceipt).subscribe((response) => {
        if (response.message != "Receipting  Please enter valid serial number") {
          if (this.orderReceipt.orderTypeId == OrderTypes.GOODSRETURN) {
            this.orderReceipt.orderReceiptItem.orderReceiptItemDetails = [];
            this.orderReceipt.orderReceiptBatchId = response.resources;
            if(this.screenType == 'job-selection') {
              this.navigateToJobSelection();
            }
            this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-goods-return-edit'], {
              queryParams:
              {
                id: this.faulty.orderReceipt.orderReferenceId,
                orderTypeId: this.orderReceipt.orderTypeId,
                batchId: this.orderReceipt.orderReceiptBatchId,
                isActiveNew: this.isActiveNew
              }
            });
          }
        }
      });
    } else {
      this.snackbarService.openSnackbar("Please scan/enter serial number", ResponseMessageTypes.WARNING);
    }
  }

  private ValidationMessage(message) {
    let response = {
      statusCode: 409,
      isSuccess: false,
      resources: null,
      totalCount: 0,
      message: message
    };
    this.applicationResponse = response;
    this.alertService.processAlert(this.applicationResponse);
  }

  randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
  }

  getStockCount(value) {
    let countValue = parseInt(value);
    let scannedValue = this.orderItemBarcodeScanedCount + countValue;
    if (scannedValue <= this.totalFaultyItemCount) {
      for (var i = 0; i < countValue; i++) {
        let randomValue = this.randomString(8, this.alphabets);
        this.modelChanged(randomValue);
      }
    } else {
      this.snackbarService.openSnackbar("Stock filled already or enter valid stock count", ResponseMessageTypes.WARNING);
    }
  }

  view() {
    if (this.orderReceipt.orderReferenceId) {
      this.router.navigate(['inventory/order-receipts/receipting/technician-receiving-goods-return-view'], {
        queryParams:
        {
          GoodsReturnId: this.orderReceipt.orderReferenceId,
          OrderTypeId: this.orderReceipt.orderTypeId,
          orderReceiptId: this.orderReceipt.orderReceiptId,
          isActiveNew: this.isActiveNew,
          screenType:this.screenType
        }, skipLocationChange: true
      });
    }
  }
  navigateToJobSelection (){
    this.router.navigate(['/inventory/job-selection'], { queryParams: { tab: 0 } });
  }
}
