import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderReceipt } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-technician-receiving-goods-return-stock-count',
  templateUrl: './technician-receiving-goods-return-stock-count.component.html',
  styleUrls: ['./technician-receiving-goods-return-view.component.scss']
})

export class TechnicianReceivingGoodsReturnStockCountComponent implements OnInit, AfterViewInit {

  purchaseOrder: any;
  itemName: string;
  stockCount: number;
  orderReceipt = new OrderReceipt();
  loggedUser: any;
  isbrudcrumb: string;
  totalPurchaseOrderItemCount: number;
  alreadyReceivedQty: number;
  isDraft: boolean = false;
  isActiveNew: boolean;
  orderType: any;
  batchId: any;
  screenType;
  constructor(
    private router: Router, private rxjsService: RxjsService,
    private store: Store<AppState>,
    private httpService: CrudService,
    private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData) => {
      this.loggedUser = userData;
    });
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.purchaseOrder = this.router.getCurrentNavigation().extras.state.data;

      this.isActiveNew = this.router.getCurrentNavigation().extras.state.isActiveNew;
      this.orderReceipt.orderReferenceId = this.router.getCurrentNavigation().extras.state.id;
      this.orderType = this.router.getCurrentNavigation().extras.state.orderTypeId;
      this.batchId = this.router.getCurrentNavigation().extras.state.batchId;
      this.screenType = this.router.getCurrentNavigation().extras.state.screenType;

      this.itemName = this.purchaseOrder.itemName + " - " + this.purchaseOrder.itemCode;
      this.orderReceipt = this.purchaseOrder.orderReceipt;
      this.isDraft = this.orderReceipt.pendingBatch != null ? this.orderReceipt.pendingBatch.isDraft : false;
      this.totalPurchaseOrderItemCount = this.purchaseOrder.purchaseOrderItem.quantity;
      this.stockCount = this.purchaseOrder.purchaseOrderItem.receivedQty;
    }
  }

  ngOnInit() {
  }


  save() {
    if (this.stockCount > this.totalPurchaseOrderItemCount || this.stockCount<=0) {
      this.snackbarService.openSnackbar("Enter valid stock count", ResponseMessageTypes.WARNING);
      return;
    }
    this.orderReceipt.orderReceiptItem.orderItemId = this.purchaseOrder.purchaseOrderItem.itemId;
    this.orderReceipt.orderReceiptItem.quantity = this.purchaseOrder.purchaseOrderItem.totalQuantity;
    this.orderReceipt.orderReceiptItem.orderReceiptItemId = this.purchaseOrder.purchaseOrderItem.orderReceiptItemId
      == '00000000-0000-0000-0000-000000000000' ? null : this.purchaseOrder.purchaseOrderItem.orderReceiptItemId;
    this.orderReceipt.inventoryStaffId = this.loggedUser["userId"];
    this.orderReceipt.createdUserId = this.loggedUser["userId"];
    this.orderReceipt.orderReceiptItem.createdUserId = this.loggedUser["userId"];
    this.orderReceipt.orderReceiptItem.isConsumable = true;
    this.orderReceipt.orderReceiptItem.consumableQuantity = this.stockCount;
    this.httpService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_RECEIPTS, this.orderReceipt).subscribe((response) => {
      if (response.isSuccess == true) {
        this.orderReceipt.orderReceiptItem.orderReceiptItemDetails = [];
        this.orderReceipt.orderReceiptBatchId = response.resources;
        if(this.screenType == 'job-selection')
           this.navigateToJobSelection();
        else
           this.navigateToEditPage();
      }
    });
  }



  receiptingViewNew() {
    this.router.navigate(['inventory/order-receipts/receipting/technician-receiving-goods-return-view'], {
      queryParams:
      {
        GoodsReturnId: this.orderReceipt.orderReferenceId,
        OrderTypeId: this.orderReceipt.orderTypeId,
        orderReceiptId: this.orderReceipt.orderReceiptId,
        isActiveNew: this.isActiveNew,
        screenType:this.screenType,
      }, skipLocationChange: true
    });
  }



  navigateToEditPage() {
    this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-goods-return-edit'], {
      queryParams:
      {
        id: this.orderReceipt.orderReferenceId,
        orderTypeId: this.orderReceipt.orderTypeId,
        batchId: this.orderReceipt.orderReceiptBatchId,
        screenType:this.screenType,
        isActiveNew: this.isActiveNew
      }
    });
  }

  cancel() {
    this.router.navigate(['inventory/order-receipts/receipting/technician-receiving-goods-return-edit'],
      {
        state: {
          data: this.orderReceipt,
          id: this.orderReceipt.orderReferenceId,
          orderTypeId: this.orderType,
          batchId: this.batchId ? this.batchId : '',
          isActiveNew: this.isActiveNew,
          screenType:this.screenType,
        }, skipLocationChange: true
      });
  }
  navigateToJobSelection (){
    this.router.navigate(['/inventory/job-selection'], { queryParams: { tab: 0 } });
  }
  ngAfterViewInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
  }
}
