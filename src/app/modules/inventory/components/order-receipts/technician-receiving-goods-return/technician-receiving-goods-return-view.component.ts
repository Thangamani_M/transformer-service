import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { Observable } from 'rxjs';
import { SerialnumberViewComponent } from '../intransit';
@Component({
  selector: 'app-technician-receiving-goods-return-view',
  templateUrl: './technician-receiving-goods-return-view.component.html',
  styleUrls: ['./technician-receiving-goods-return-view.component.scss']
})
export class TechnicianReceivingGoodsReturnViewComponent implements OnInit {
  goodsReturnId: string;
  batchNumber: any;
  TechnicianReturnReceiptingDetails: any = null;
  ReceiptStatus: string = "";
  ReceiptTye: string = "";
  IsLoaded: boolean = false;
  IsFaulty: boolean = false;
  OrderTypeId: string = "";
  barcodes: any;
  barcode: any;
  isActiveNew: boolean;
  screenType;
  constructor(private dialog: MatDialog, private httpService: CrudService, private router: Router,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private snackbarService: SnackbarService) {
    this.goodsReturnId = this.activatedRoute.snapshot.queryParams.GoodsReturnId;
    this.batchNumber = this.activatedRoute.snapshot.queryParams.BatchId;
    this.OrderTypeId = this.activatedRoute.snapshot.queryParams.OrderTypeId;
    this.isActiveNew = this.activatedRoute.snapshot.queryParams.isActiveNew;
    this.screenType = this.activatedRoute.snapshot.queryParams.screenType;

  }

  ngOnInit() {

    this.getReceiveById().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode == 200 && response.resources != null) {
        this.TechnicianReturnReceiptingDetails = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
        if (this.TechnicianReturnReceiptingDetails.pendingBatch != null){
          this.batchNumber = this.TechnicianReturnReceiptingDetails.pendingBatch.orderReceiptBatchId;
        }
      }
      else {
        this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    });

  }

  getReceiveById(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('GoodsReturnId', (this.goodsReturnId && this.goodsReturnId != undefined) ? this.goodsReturnId : '');
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDERRECEIVE_TECHNICIAN_GOODSRETURN, null, null, params);
  }

  getReceiveByIdAndBatch(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('GoodsReturnId', (this.goodsReturnId && this.goodsReturnId != undefined) ? this.goodsReturnId : '').set('BatchId', this.batchNumber ? this.batchNumber : '');
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDERRECEIVE_TECHNICIAN_GOODSRETURN, null, null, params);
  }

  navigateToEdit(): void {
    this.router.navigate(['inventory/order-receipts/receipting/technician-receiving-goods-return-edit'],
      {
        queryParams:
        {
          id: this.goodsReturnId,
          orderTypeId: this.OrderTypeId,
          batchId: this.batchNumber ? this.batchNumber : '',
          isActiveNew: this.isActiveNew,
          screenType:this.screenType
        }, skipLocationChange: true
      });
  }

  getserialnumber(orderItems, itemCode) {
    this.barcodes = orderItems.orderReceiptItemDetails.filter(b => b.isScanned == true);
    this.barcode = undefined;
    if (this.barcodes.length != 0) {
      this.barcode = Array.prototype.map.call(this.barcodes, function (item) { return item.barcode; }).join(",");
    }
    const dialogReff = this.dialog.open(SerialnumberViewComponent, { width: '700px', disableClose: true, data: { barcode: this.barcode, count: this.barcodes.length } });
    dialogReff.afterClosed().subscribe(result => {
      if (result) return;
      this.rxjsService.setDialogOpenProperty(false);
    });
  }
  navigateToJobSelection (){
    this.router.navigate(['/inventory/job-selection'], { queryParams: { tab: 0 } });
  }
}
