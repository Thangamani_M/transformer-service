import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { KeyValuePair, OrderReceipt, OrderReceiptingSubmitItems, OrderReceiptSubmit } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { DocumentTypes, InventoryModuleApiSuffixModels, OrderTypes } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';
import { Supplier } from '../../../models/Supplier';

@Component({
  selector: 'app-technician-receiving-goods-return-verify',
  templateUrl: './technician-receiving-goods-return-verify.component.html',
  styleUrls: ['./technician-receiving-goods-return-verify.component.scss']
})
export class TechnicianReceivingGoodsReturnVerifyComponent implements OnInit {
  @ViewChild('serialNumber', { static: false }) serilNumberField: ElementRef;

  userData: UserLogin;
  orderReferenceId: string;
  orderNumberControl = new FormControl();
  applicationResponse: IApplicationResponse;
  uxOderTypes: any;
  orderType: string = '';
  storageLocation: string = '';
  isNew: boolean = false;
  showSupplier: boolean = false;
  orderReceiptVerifyForm: FormGroup;
  keyValuePair: KeyValuePair;
  uxStorageLocations: any;
  filteredFaultys: any[] = [];
  faultyAutoFillForm: FormGroup;
  isLoading = false;
  supplierName: string = '';
  supplier = new Supplier();
  orderReceipt = new OrderReceipt();
  showBatchNo = false;
  orderTypes = OrderTypes;
  orderTypesOptions = [];
  receiptingType: any;
  batchId: string = '0';
  order: any = null;
  //mask validations
  public alphaNumericWithSpacePattern = { '0': { pattern: new RegExp('^[A-Za-z0-9 ]*$') } };
  isButtonDisabled: boolean = true;
  items = [];
  Items: Array<any> = [];
  itemList: OrderReceiptingSubmitItems;
  imageName: string;
  errorMessage: string;
  screenType;
  @ViewChild('ddl', null) myDiv: ElementRef<HTMLElement>;
  @ViewChild(SignaturePad, { static: false }) signaturePad: any;

  public signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 500,
    'canvasHeight': 80
  };

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute,

    private httpService: CrudService,
    private router: Router,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.orderReceipt = this.router.getCurrentNavigation().extras.state.data;
      this.orderType = this.orderReceipt.orderTypeId;
      this.batchId = this.orderReceipt.orderReceiptBatchId;
    }
    if (this.orderReceipt.orderReceiptId == undefined) {
      this.orderReceipt.orderReferenceId = this.activatedRoute.snapshot.queryParams.id;
      this.orderType = this.activatedRoute.snapshot.queryParams.orderTypeId;
      this.batchId = this.activatedRoute.snapshot.queryParams.batchId;
    }
    this.screenType = this.activatedRoute.snapshot.queryParams.screenType;
    this.store.pipe(select(loggedInUserData)).subscribe((userData) => {
      this.userData = userData;
    });

  }

  ngOnInit() {
    this.isButtonDisabled = false;

    this.orderReceiptVerifyForm = this.formBuilder.group({
      requestId: [''],
      orderTypeName: [''],
      orderNumber: [''],
      technicianName: [''],
      storageLocationName: [''],
      technicianLocation: [''],
      comment: [''],
    });

    if (this.orderReceipt.orderReferenceId != undefined)
      this.getFaulty(this.orderReceipt.orderReferenceId);
    else
      this.rxjsService.setGlobalLoaderProperty(false);
  }

  //Redirect to barcode scan page and pass related items
  barcodeScan(faultyItem, faultyDetails) {
    if (faultyDetails) {
      this.orderReceipt.orderNumber = faultyDetails.orderNumber;
      this.orderReceipt.orderReferenceId = faultyDetails.orderReferenceId;
      this.orderReceipt.warehouseId = this.userData.warehouseId;
      this.orderReceipt.inventoryStaffId = this.userData.userId;
      this.orderReceipt.supplierId = faultyDetails.supplierId;
      this.orderReceipt.orderReceiptId = faultyItem.orderReceiptId;
      let order = {
        itemName: faultyItem.itemName,
        itemCode: faultyItem.itemCode,
        purchaseOrderItem: faultyItem,
        orderReceipt: this.orderReceipt
      }
      this.router.navigate(['/inventory/order-receipts/receipting/technician-receiving-goods-return-item-scan'], { state: { data: order } });
    }
  }

  //Selected value to assign auto fill textbox
  displayFaultyNo(keyValuePair: KeyValuePair) {
    if (keyValuePair) { return keyValuePair.displayName; }
  }

  //Get purchase order with related items and suppliers
  getFaulty(pid) {
    if (pid && pid != null) {
      let pId = (typeof pid === 'object') ? pid.id : pid;
      let params;
      params = new HttpParams().set('GoodsReturnId', pId);

      this.httpService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.ORDERRECEIVE_TECHNICIAN_GOODSRETURN, null, null, params).subscribe({
          next: response => {
            this.order = response.resources;

            if (this.order != null) {
              this.orderNumberControl.setValue({ displayName: this.order.orderNumber });
              this.orderReceipt.orderReceiptId = this.order.orderReceiptId;
              this.orderReceipt.isVerified = this.order.isVerified;
              this.orderReceipt.orderTypeId = this.orderType;

              this.orderReceipt.orderReceiptBatchId = this.order.orderReceiptBatchId;
              this.orderReceiptVerifyForm.controls.requestId.setValue(this.order.requestId);
              this.orderReceiptVerifyForm.controls.orderTypeName.setValue(this.order.orderTypeName);
              this.orderReceiptVerifyForm.controls.orderNumber.setValue(this.order.goodsReturnNumber);
              this.orderReceiptVerifyForm.controls.technicianName.setValue(this.order.technicianName);
              this.orderReceiptVerifyForm.controls.technicianLocation.setValue(this.order.technicianLocation);
              this.orderReceiptVerifyForm.controls.storageLocationName.setValue(this.order.storageLocationName);
            }
            this.rxjsService.setGlobalLoaderProperty(false);
          }
        });
      if (pid.id !== undefined)
        this.orderReceipt.orderReferenceId = pid.id;
    }
  }

  //hide and show ui elements based on receipting type
  receiptingTypeChange(event) {
    let selectedReceiptingType = event;
    this.receiptingType = selectedReceiptingType.toLocaleLowerCase();
    //New
    if (selectedReceiptingType.toLocaleLowerCase() === 'new') {
      this.isNew = true;
      this.showSupplier = false;
      this.filteredFaultys = [];
      this.order = null;
      this.showBatchNo = false;
      this.orderNumberControl.setValue({ displayName: '' });
      this.orderReceipt.batchNumber = null;
    }
    //Return
    else if (selectedReceiptingType.toLocaleLowerCase() === 'return') {
      this.EnableDisableControls();
    }
    //Replacement
    else if (selectedReceiptingType.toLocaleLowerCase() === 'replacement') {
      this.EnableDisableControls();
    }//Faulty
    else if (selectedReceiptingType.toLocaleLowerCase() === 'faulty') {
      this.EnableDisableControls();
    }
  }

  private EnableDisableControls() {
    this.isNew = true;
    this.showSupplier = false;
    this.filteredFaultys = [];
    this.order = null;
    this.showBatchNo = false;
    this.orderNumberControl.setValue({ displayName: '' });
    this.orderReceipt.batchNumber = null;
  }

  private verifyDescrepancy() {
    if (this.orderReceipt.orderReceiptId != null) {
      //this.router.navigate(['/inventory/order-receipts/receipting/discrepancy/'], { queryParams: { id: this.orderReceipt.orderReceiptId } });
      this.orderReceipt.orderReceiptBatchId = this.batchId;
      this.router.navigate(['/inventory/order-receipts/receipting/discrepancy/'], { state: { data: this.orderReceipt } });
    }
    else {
      this.snackbarService.openSnackbar('Please check mandatory fields', ResponseMessageTypes.WARNING);
    }
  }

  drawComplete() {

  }

  clearPad() {
    this.signaturePad.clear();
  }

  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.resizeCanvas();
    this.signaturePad.clear();
  }

  SubmitOrderReceipt() {
    if (this.orderReceiptVerifyForm.invalid) {
      this.orderReceiptVerifyForm.markAllAsTouched();
      return;
    }

    this.isButtonDisabled = true;
    var orderReceiptSubmitDetails = new OrderReceiptSubmit();
    orderReceiptSubmitDetails.orderReceiptId = this.order.orderReceiptId;
    orderReceiptSubmitDetails.batchId = this.order.orderReceiptBatchId;
    orderReceiptSubmitDetails.documentType = DocumentTypes.SUPPLIERS_INVOICE;
    orderReceiptSubmitDetails.comment = this.orderReceiptVerifyForm.get('comment').value;
    orderReceiptSubmitDetails.orderTypeId = this.order.orderTypeId;
    orderReceiptSubmitDetails.orderType = 'Goods Return',
    orderReceiptSubmitDetails.inventoryStaffId = this.userData["userId"];
    orderReceiptSubmitDetails.createdUserId = this.userData["userId"];
    
    if (this.Items.length != 0 || this.order.orderReceiptId != null) {
      for (let item of this.Items) {
        this.itemList = new OrderReceiptingSubmitItems();
        this.itemList.itemId = item.itemId;
        this.itemList.itemCode = item.itemCode;
        this.itemList.itemName = item.itemName;
        this.itemList.receivedQuantity = item.receivedQty;
        this.itemList.varianceQuantity = item.varianceQuantity;
        orderReceiptSubmitDetails.itemList.push(this.itemList);
      }
      this.isButtonDisabled = true;
      const formData = new FormData();
      formData.append("orderReceiptSubmitDetails", JSON.stringify(orderReceiptSubmitDetails));
      if (this.signaturePad.signaturePad._data.length > 0) {
        this.imageName = this.order.goodsReturnNumber + "-signature.jpeg";
        const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
        const imageFile = new File([imageBlob], this.imageName, { type: 'image/jpeg' });
        formData.append('document_files[]', imageFile);
      }
      this.rxjsService.setGlobalLoaderProperty(true);
      this.httpService.fileUpload(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_RECEIPT_FINAL_SUBMIT, formData)
        .subscribe({
          next: response => {
            this.onSaveComplete(response);
            this.redirect();
          },
          error: err => {
            this.errorMessage = err;
            this.isButtonDisabled = false;
          }
        });
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }
  onSaveComplete(response): void {
    if (response.statusCode == 200) {
      //this..responseMessage(response);
    }
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  previousPage() {
    this.router.navigate(['inventory/order-receipts/receipting/technician-receiving-goods-return-edit'], { queryParams: { id: this.orderReceipt?.orderReferenceId, orderTypeId: this.orderReceipt?.orderTypeId, batchId: this.order?.orderReceiptBatchId,screenType:this.screenType } });
  }

  listPage() {
    this.router.navigate(['/inventory/order-receipts/receipting'], { queryParams: { tab: 1 }, skipLocationChange: true });
  }
  redirect(){

    if(this.screenType == 'job-selection')
       this.navigateToJobSelection();
    else
       this.listPage(); 
  }  
  view() {
    if (this.orderReceipt.orderReferenceId) {
      this.router.navigate(['inventory/order-receipts/receipting/technician-receiving-goods-return-view'], { queryParams: { GoodsReturnId: this.orderReceipt.orderReferenceId, OrderTypeId: this.orderReceipt.orderTypeId, orderReceiptId: this.orderReceipt.orderReceiptId,screenType:this.screenType }, skipLocationChange: true });
    }
  }
  onSubmit() {

  }
  navigateToJobSelection (){
    this.router.navigate(['/inventory/job-selection'], { queryParams: { tab: 0 } });
  }
}
