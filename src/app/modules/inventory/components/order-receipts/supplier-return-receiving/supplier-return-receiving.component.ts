import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-supplier-return-receiving',
  templateUrl: './supplier-return-receiving.component.html',
  styleUrls: ['./supplier-return-receiving.component.scss']
})
export class SupplierReturnReceivingComponent implements OnInit {
  orderReferenceId: string;
  batchNumber: any;
  supplierReturnDetails: any = null;
  ReceiptStatus: string = "";
  ReceiptTye: string = "";
  IsLoaded: boolean = false;
  IsPurchaseOrder: boolean = false;
  ispending = false;
  OrderTypeId: string = "";
  supplierReturnReceivingForm: FormGroup

  constructor(private httpService: CrudService, private router: Router,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private formBuilder: FormBuilder) {
    this.orderReferenceId = this.activatedRoute.snapshot.queryParams.orderReferenceId;
  }
  ngOnInit() {
    this.createForm();
    this.getSupplierReturnById().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess) {

        this.supplierReturnDetails = response.resources;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })

  }


  createForm() {
    this.supplierReturnReceivingForm = this.formBuilder.group({
      orderReceiptId: [this.orderReferenceId ? this.orderReferenceId : ''],
      orderReceiptActionTypeId: [1, Validators.required],
      comment: ['', Validators.required]
    });
  }

  getSupplierReturnById(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('SupReturnId', this.orderReferenceId);
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_RETURN_RECEIVING_RELATEDITMES, null, null, params);
  }

  save() {
    if (this.supplierReturnReceivingForm.invalid) {
      return
    }
    this.httpService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_RETURN_RECEIVING, this.supplierReturnReceivingForm.value).subscribe({

      next: response => this.router.navigate(['inventory/order-receipts/receipting'], { queryParams: { tab: 1 } }),
      error: err => err
    });
  }

  previous(): void {

    this.router.navigate(['inventory/order-receipts/receipting/supplier-return-receiving-verify'], { queryParams: { orderReferenceId: this.orderReferenceId, orderReceiptId: this.supplierReturnDetails.orderReceiptId } })
  }


}
