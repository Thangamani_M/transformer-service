import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-supplier-return-receiving-verify',
  templateUrl: './supplier-return-receiving-verify.component.html',
  styleUrls: ['./supplier-return-receiving-verify.component.scss']
})
export class SupplierReturnReceivingVerifyComponent implements OnInit {
  orderReferenceId: string;
  orderReceiptId: string;
  batchNumber: any;
  supplierReturnDetails: any = null;
  ReceiptStatus: string = "";
  ReceiptTye: string = "";
  IsLoaded: boolean = false;
  IsPurchaseOrder: boolean = false;
  ispending = false;
  OrderTypeId: string = "";
  supplierReturnReceivingForm: FormGroup
  itemFoundFlag: boolean
  itemExistFlag: boolean
  IsSubmitDisabled: Boolean = true
  supplierReceivedItem: any

  constructor(private httpService: CrudService, private router: Router,
    private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder, private rxjsService: RxjsService, private snackBar: SnackbarService) {
    this.orderReferenceId = this.activatedRoute.snapshot.queryParams.orderReferenceId;
    this.orderReceiptId = this.activatedRoute.snapshot.queryParams.orderReceiptId;
  }
  ngOnInit() {
    this.createForm();
    if (this.orderReceiptId) {
      this.getSupplierReturnPreviousById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.supplierReturnDetails = response.resources;
          this.supplierReturnReceivingForm.patchValue(response.resources)
          this.supplierReturnReceivingForm.get('orderNumber').setValue(response.resources.supReturnNumber)
          this.supplierReturnReceivingForm.get('orderReferenceId').setValue(this.orderReferenceId)
          this.supplierReturnReceivingForm.get('OrderReceiptBatchId').setValue(response.resources.orderReceiptBatchId)
          this.supplierReturnReceivingForm.get('OrderReceiptId').setValue(response.resources.orderReceiptId)
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    } else {
      this.getSupplierReturnById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess) {
          this.supplierReturnDetails = response.resources;
          this.supplierReturnReceivingForm.patchValue(response.resources)
          this.supplierReturnReceivingForm.get('orderNumber').setValue(response.resources.supReturnNumber)
          this.supplierReturnReceivingForm.get('orderReferenceId').setValue(this.orderReferenceId)
          this.supplierReturnReceivingForm.get('OrderReceiptBatchId').setValue(response.resources.orderReceiptBatchId)
          this.supplierReturnReceivingForm.get('OrderReceiptId').setValue(response.resources.orderReceiptId)
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      })
    }


  }

  createForm() {
    this.supplierReturnReceivingForm = this.formBuilder.group({
      warehouseId: [''],
      locationId: [''],
      inventoryStaffId: [''],
      orderNumber: [''],
      orderReferenceId: [''],
      OrderReceiptBatchId: [null],
      OrderReceiptId: [''],
      barCode: ['']
    });
  }

  getSupplierReturnById(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('SupReturnId', this.orderReferenceId);
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_RETURN_RECEIVING_RELATEDITMES, null, null, params);
  }

  getSupplierReturnPreviousById(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('SupReturnId', this.orderReferenceId).set('OrderReceiptId', this.orderReceiptId);
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_RETURN_RECEIVING_RELATEDITMES, null, null, params);
  }

  //verify and Scanning in Items
  getBarcode() {
    if (!this.supplierReturnReceivingForm.get('barCode').value)
      return;

    this.itemExistFlag = false;
    let itemFoundFlag = false;
    this.supplierReturnDetails.supplierReturnItems.forEach((item) => {
      item.supplierReturnItemDetails.forEach((itemDetail) => {
        if (itemDetail.barcode == this.supplierReturnReceivingForm.get('barCode').value || itemDetail.serialNumber == this.supplierReturnReceivingForm.get('barCode').value) {
          if (item.receivedBarcode != undefined && item.receivedBarcode.split(',').indexOf(itemDetail.barcode) > -1) {
            this.snackBar.openSnackbar("Barcode already verified!", ResponseMessageTypes.WARNING);
            this.itemExistFlag = true;
            return;
          }
        }
      });
    });

    if (this.itemExistFlag) {
      return;
    }

    this.supplierReturnDetails.supplierReturnItems.forEach((item) => {
      item.supplierReturnItemDetails.forEach((itemDetail) => {

        if (itemDetail.barcode == this.supplierReturnReceivingForm.get('barCode').value || itemDetail.serialNumber == this.supplierReturnReceivingForm.get('barCode').value) {
          item.receivedQty = item.receivedQty == undefined ? 1 : item.receivedQty += 1;
          item.isInWarranty = itemDetail.isInWarranty;
          if (item.receivedBarcode == undefined)
            item.receivedBarcode = itemDetail.barcode;
          else
            item.receivedBarcode += ',' + itemDetail.barcode;

          //remove the comma at beginning
          item.receivedBarcode = item.receivedBarcode.replace(/^,|,$/g, '');

          itemDetail.receivedBarcode = itemDetail.barcode;
          itemFoundFlag = true;
        }
      });
    });
    this.supplierReturnReceivingForm.get('barCode').setValue('');
    if (!itemFoundFlag)
      this.snackBar.openSnackbar("Barcode not maching with current Supplier Return Receiving!", ResponseMessageTypes.WARNING);
    this.EnableDisableSubmit();
  }

  EnableDisableSubmit() {
    if (this.supplierReturnDetails != undefined && this.supplierReturnDetails.supplierReturnItems != undefined) {
      this.supplierReturnDetails.supplierReturnItems.forEach(item =>
        item.supplierReturnItemDetails.forEach(element => {
          if (element.receivedBarcode != undefined)
            this.IsSubmitDisabled = false;
        })
      )
    }
  }

  saveSupplierReturn() {
    this.IsSubmitDisabled = true;

    if (this.supplierReturnReceivingForm.invalid) {
      return
    }


    this.supplierReturnDetails.supplierReturnItems.forEach(item => {
      item.supplierReceivedItems = item.supplierReturnItemDetails.filter(function (detail) {
        return detail.receivedBarcode;
      });
    });
    this.supplierReceivedItem = this.supplierReturnDetails.supplierReturnItems.filter(function (item) {
      return item.supplierReceivedItems.length != 0;
    });

    this.supplierReceivedItem = { supplierReceivedItems: this.supplierReceivedItem };
    const plantdata = { ...this.supplierReceivedItem, ...this.supplierReturnReceivingForm.value };
    this.httpService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_RETURN_RECEIVING, plantdata).subscribe({

      next: response => this.router.navigate(['/inventory/order-receipts/receipting/supplier-return-receiving'], { queryParams: { orderReferenceId: this.orderReferenceId } }),
      error: err => err
    });
  }

  navigateToEdit(): void {

    this.router.navigate(['inventory/order-receipts/receipting/supplier-return-receiving-verify'], { queryParams: { id: this.orderReferenceId, orderTypeId: this.OrderTypeId, batchId: this.batchNumber ? this.batchNumber : '' } })
  }


}


export class SupplierDetailsModal {
  constructor(supplierDetailsModal?: SupplierDetailsModal) {
    this.itemId = supplierDetailsModal ? supplierDetailsModal.itemId == undefined ? null : supplierDetailsModal.itemId : null;
    this.quantity = supplierDetailsModal ? supplierDetailsModal.quantity == undefined ? null : supplierDetailsModal.quantity : null;
  }
  itemId?: string;
  quantity?: number;
  supplierReceivedItems: SupplierReceivedItems[]
}


export class SupplierReceivedItems {
  constructor(supplierReceivedItems?: SupplierReceivedItems) {
    this.barcode = supplierReceivedItems ? supplierReceivedItems.barcode == undefined ? null : supplierReceivedItems.barcode : null;
    this.serialNumber = supplierReceivedItems ? supplierReceivedItems.serialNumber == undefined ? null : supplierReceivedItems.serialNumber : null;
    this.isInWarranty = supplierReceivedItems ? supplierReceivedItems.isInWarranty == undefined ? false : supplierReceivedItems.isInWarranty : false;
  }
  barcode?: string;
  serialNumber?: number;
  isInWarranty: boolean
}

