import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IApplicationResponse, ModulesBasedApiSuffix } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-supplier-return-receiving-view',
  templateUrl: './supplier-return-receiving-view.component.html',
  styleUrls: ['./supplier-return-receiving-view.component.scss']
})
export class SupplierReturnReceivingViewComponent implements OnInit {
  orderReceiptId: string;
  orderReferenceId: string;
  SupplierReturnRecevingViewDetail:any;
  batchNumber: any;
  supplierReturnDetails: any = null;
  ReceiptStatus: string = "";
  constructor(private httpService: CrudService, private router: Router,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService) {
    this.orderReceiptId = this.activatedRoute.snapshot.queryParams.orderReceiptId;
    this.orderReferenceId = this.activatedRoute.snapshot.queryParams.orderReferenceId;
  }
  ngOnInit() {
    this.getSupplierReturnById().subscribe((response: IApplicationResponse) => {
      if (response.isSuccess && response.statusCode === 200) {
        this.supplierReturnDetails = response.resources;
        this.SupplierReturnRecevingViewDetail = [
              { name: 'Supplier Return No', value: response.resources?.supReturnNumber },
              { name: 'Receiving Type', value: response.resources?.supplierReturnStatusName },
              { name: 'Supplier Name', value: response.resources?.supplierName },
              { name: 'Sub Location', value: response.resources?.subLocation },
              { name: 'Status', value: response.resources?.status, statusClass: response.resources?.cssClass },
              { name: 'Batch No', value: response.resources?.batchNumber },
        ]
        this.rxjsService.setGlobalLoaderProperty(false);
      }
    })
  }

  getSupplierReturnById(): Observable<IApplicationResponse> {
    let params = new HttpParams().set('OrderReceiptId', this.orderReceiptId);
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.SUPPLIER_RETURN_RECEIVING_VIEW, null, null, params);
  }

  navigateToEdit(): void {
    this.router.navigate(['inventory/order-receipts/receipting/supplier-return-receiving-verify'], { queryParams: { orderReferenceId: this.orderReferenceId, orderReceiptId: this.orderReceiptId } });
  }
}
