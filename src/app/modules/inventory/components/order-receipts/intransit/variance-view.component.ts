import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix } from '@app/shared';
import { CrudService } from '@app/shared/services';
import { InterBranchStockOrderItemDetail } from '@modules/inventory/models/interbranchStockOrderDetails.model';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-variance-view',
  templateUrl: './variance-view.component.html',
  styleUrls: ['./intransit-view.component.scss']
})
export class VarianceViewComponent implements OnInit {
  userData: UserLogin;
  pendinItems: any = null;
  items: any = null;
  varianceItems: any = null;
  InterBranchStockOrderItemDetails: InterBranchStockOrderItemDetail[] = [];
  InterBranchStockOrderItemDetail = new InterBranchStockOrderItemDetail();
  varianceForm: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private httpService: CrudService, private store: Store<AppState>,
    private dialog: MatDialog, private formBuilder: FormBuilder) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
  }

  ngOnInit(): void {
   
    this.varianceForm = this.formBuilder.group({
      comments: [''],
    });
    this.pendinItems = this.data.item.orderReceiptItemDetails;
    this.items = this.data.item;
    for (let item of this.pendinItems) {
      this.InterBranchStockOrderItemDetail.InterBranchStockOrderItemDetailId = item.interBranchStockOrderItemDetailId;
      this.InterBranchStockOrderItemDetail.InterBranchStockOrderItemId = item.interBranchStockOrderItemId;
      this.InterBranchStockOrderItemDetail.ModifiedUserId = this.userData.userId;
      this.InterBranchStockOrderItemDetails.push(this.InterBranchStockOrderItemDetail);
    }

  }
  valuechange(newValue) {
   
    let comments = this.varianceForm.controls.comments.value;
    let index = this.InterBranchStockOrderItemDetails.findIndex(item => item['InterBranchStockOrderItemDetailId'] == newValue.interBranchStockOrderItemDetailId);

    this.InterBranchStockOrderItemDetails[index].Comments = comments;

  }
  createVariance() {
   
    if (this.InterBranchStockOrderItemDetails.length == 0) {
      return;
    }

    this.httpService.update(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INTERBRANCHSTOCKORDER_DETAILS, this.InterBranchStockOrderItemDetails).subscribe((response) => {
      if (response.message != "Receipting  Please enter valid serial number") {
        this.dialog.closeAll();
      }
    });
  }

}
