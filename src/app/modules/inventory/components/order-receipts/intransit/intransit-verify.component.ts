import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderReceipt, OrderReceiptingSubmitItems, OrderReceiptSubmit } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { CrudService, RxjsService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { DocumentTypes, InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { select, Store } from '@ngrx/store';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';

@Component({
  selector: 'app-intransit-verify',
  templateUrl: './intransit-verify.component.html',
  styleUrls: ['./intransit-view.component.scss']
})
export class InTransitVerifyComponent implements OnInit {
  loggedUser: any;
  orderReceiptId: string;
  orderReceiptVerifyForm: FormGroup;
  applicationResponse: IApplicationResponse;
  errorMessage: string;
  OrderReceiptDetails: any = null;
  orderReceiptSubmitDetails: any;
  items = [];
  divHide: boolean = true;
  Items: Array<any> = [];
  itemList: OrderReceiptingSubmitItems;
  isButtonDisabled: boolean = true;
  orderReceipt = new OrderReceipt();
  batchId: string;
  imageName: string;
  isPrevious: boolean = false;
  warehouseId: any;
  @ViewChild(SignaturePad, { static: false }) signaturePad: any;

  public signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 500,
    'canvasHeight': 80
  };

  constructor(private router: Router, private activatedRoute: ActivatedRoute,

    private httpService: CrudService,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private store: Store<AppState>) {
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.orderReceipt = this.router.getCurrentNavigation().extras.state.data;
      this.batchId = this.orderReceipt.orderReceiptBatchId;
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData) => {
      this.loggedUser = userData;
    });
  }

  ngOnInit() {
    this.warehouseId = this.loggedUser["warehouseId"];
    this.isButtonDisabled = false;
    this.GetOrdeReceipt(this.orderReceipt.barcodeNumber, this.orderReceipt.rqNumber, this.warehouseId);
    this.orderReceiptVerifyForm = this.formBuilder.group({
      comment: ['', Validators.required]
    });
  }

  drawComplete() {

  }

  clearPad() {
    this.signaturePad.clear();
  }

  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.resizeCanvas();
    this.signaturePad.clear();
  }

  GetOrdeReceipt(BarCode: string, orderNumber: string, warehouseId: string) {

    let params = new HttpParams().set('BarCodeNumber', (BarCode && BarCode != undefined) ? BarCode : '').set('RQNumber', orderNumber ? orderNumber : '').set('WarehouseId', warehouseId ? warehouseId : '');
    this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_DETAILS, null, false, params).subscribe((response: IApplicationResponse) => {
      this.OrderReceiptDetails = response.resources;
      this.Items = this.OrderReceiptDetails.orderReceiptItem;
      this.orderReceiptVerifyForm.patchValue(this.OrderReceiptDetails);
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  SubmitOrderReceipt() {
    this.rxjsService.setGlobalLoaderProperty(true);
    this.isButtonDisabled = true;
    var orderReceiptSubmitDetails = new OrderReceiptSubmit();
    orderReceiptSubmitDetails.orderReceiptId = this.OrderReceiptDetails.orderReceiptId;
    orderReceiptSubmitDetails.batchId = this.batchId;
    orderReceiptSubmitDetails.documentType = DocumentTypes.SUPPLIERS_INVOICE;
    orderReceiptSubmitDetails.comment = this.orderReceiptVerifyForm.get('comment').value;
    orderReceiptSubmitDetails.orderTypeId = this.OrderReceiptDetails.orderTypeId;
    orderReceiptSubmitDetails.inventoryStaffId = this.loggedUser["userId"];
    orderReceiptSubmitDetails.createdUserId = this.loggedUser["userId"];
    orderReceiptSubmitDetails.orderType = 'In Transit';

    if (this.Items.length != 0 || this.OrderReceiptDetails.orderReceiptId != null) {
      for (let item of this.Items) {
        this.itemList = new OrderReceiptingSubmitItems();
        this.itemList.itemId = item.itemId;
        this.itemList.itemCode = item.itemCode;
        this.itemList.itemName = item.itemName;
        this.itemList.receivedQuantity = item.pickingQty;
        this.itemList.varianceQuantity = item.varianceQty;
        orderReceiptSubmitDetails.itemList.push(this.itemList);
      }
      this.isButtonDisabled = true;
      const formData = new FormData();
      formData.append("orderReceiptSubmitDetails", JSON.stringify(orderReceiptSubmitDetails));
      if (this.signaturePad.signaturePad._data.length > 0) {
        this.imageName = this.OrderReceiptDetails.orderNumber + "-signature.jpeg";
        const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
        const imageFile = new File([imageBlob], this.imageName, { type: 'image/jpeg' });
        formData.append('document_files[]', imageFile);
      }
      this.rxjsService.setGlobalLoaderProperty(true);
      this.httpService.fileUpload(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_RECEIPT_FINAL_SUBMIT, formData)
        .subscribe({
          next: response => {
            this.onSaveComplete(response);
            this.listPage();
          },
          error: err => {
            this.errorMessage = err;
            this.isButtonDisabled = false;
          }
        });
      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }
  onSaveComplete(response): void {
    if (response.statusCode == 200) {
    }
  }

  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }
  previousPage() {
    this.isPrevious = true;
    this.router.navigate(['inventory/order-receipts/receipting/intransit-add-edit'], { queryParams: { isPrevious: this.isPrevious, id: this.orderReceipt.orderReferenceId, barCodeNumber: this.orderReceipt.barcodeNumber, orderTypeId: this.orderReceipt.orderTypeId, batchId: this.OrderReceiptDetails.orderReceiptBatchId } });
  }
  listPage() {
    this.router.navigate(['/inventory/order-receipts/receipting'], { queryParams: { tab: 3 } });
  }
  view() {
    this.router.navigate(["inventory/order-receipts/receipting/intransit-view"], { queryParams: { id: this.orderReceipt.orderReferenceId }, skipLocationChange: true });
  }
}
