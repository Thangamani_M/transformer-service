import { DatePipe } from '@angular/common';
import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudType, IApplicationResponse, LoggedInUserModel, ModulesBasedApiSuffix, ResponseMessageTypes } from '@app/shared';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { combineLatest, Observable } from 'rxjs';
import { SerialnumberViewComponent } from '.';
import { loggedInUserData } from '@modules/others';
import { AppState } from '@app/reducers';
import { Store } from '@ngrx/store';
@Component({
  selector: 'app-intransit-view',
  templateUrl: './intransit-view.component.html',
  styleUrls: ['./intransit-view.component.scss']
})
export class IntransitViewComponent implements OnInit {
  InTransitViewDetail: any;
  primengTableConfigProperties: any;
  interbranchstockOrderId: string;
  OrderReceiptDetails: any = null;
  barcodes: any;
  barcode: any;
  ispending = false;
  batchId: any;
  warehouseId: any;
  receivingclerk: any;
  receivedOn: any;
  orderReceiptId;
  loggedInUserData;
  stockDetailListDialog: boolean = false;
  stockDetail;
  stockCodeSerialNum = [];
  constructor(private store: Store<AppState>, private dialog: MatDialog, private httpService: CrudService, private router: Router, private datePipe: DatePipe,
    private activatedRoute: ActivatedRoute, private rxjsService: RxjsService, private snackbarService: SnackbarService) {
    this.interbranchstockOrderId = this.activatedRoute.snapshot.queryParams.id;
    this.batchId = this.activatedRoute.snapshot.queryParams.batchId;
    this.warehouseId = this.activatedRoute.snapshot.queryParams.warehouseId;
    this.orderReceiptId = this.activatedRoute.snapshot.queryParams.orderReceiptId;
    this.primengTableConfigProperties = {
      tableCaption: "Inter Branch Transfer Receiving View",
      breadCrumbItems: [{ displayName: 'Inventory Management', relativeRouterUrl: '/inventory/dashboard' }, { displayName: 'Receiving List', relativeRouterUrl: '/inventory/order-receipts/receipting', queryParams: { tab: 3 } },
      { displayName: 'Inter Branch Transfer Receiving View', relativeRouterUrl: '' }],
      selectedTabIndex: 0,
      tableComponentConfigs: {
        tabsList: [
          {
            enableBreadCrumb: true,
            enableAction: true,
            enableEditActionBtn: false,
            enableClearfix: true,

          }]
      }
    }
    this.InTransitViewDetail = [
      {
        name: 'BASIC INFO', columns: [
          { name: 'Receiving ID', value: '' },
          { name: 'Request WarehouseName', value: '' },
          { name: 'Reference ID', value: '' },
          { name: 'Issue WarehouseName', value: '' },
          { name: 'Status', value: '' },
          { name: 'Courier Name', value: '' },
          { name: 'Receiving Clerk', value: '' },
          { name: 'Received On', value: '' }
        ]
      }]
  }

  ngOnInit() {
    this.combineLatestNgrxStoreData()
    if (this.batchId, this.warehouseId) {
      this.getReceiveById().subscribe((response: IApplicationResponse) => {
        if (response.isSuccess && response.statusCode === 200) {
          this.OrderReceiptDetails = response.resources;
          this.setInTransitViewDetail(response);

          if (response.isSuccess) {
            this.OrderReceiptDetails = response.resources;
            if (this.OrderReceiptDetails.itemDetails.length != 0) {
              this.receivingclerk = this.OrderReceiptDetails.itemDetails[0].receivingClerk;
              this.receivedOn = this.OrderReceiptDetails.itemDetails[0].modifiedDate;
            }

            if (this.OrderReceiptDetails.pendingBatch != null)
              this.batchId = this.OrderReceiptDetails.pendingBatch.orderReceiptBatchId;

            // if (this.OrderReceiptDetails.pendingItems.length == 0) {
            //   this.ispending = true;
            // }

            this.rxjsService.setGlobalLoaderProperty(false);
          }
          else {
            this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
            this.rxjsService.setGlobalLoaderProperty(false);
          }
          this.setInTransitViewDetail(response);
        }
        else {
          this.snackbarService.openSnackbar(response.message, ResponseMessageTypes.ERROR);
          this.rxjsService.setGlobalLoaderProperty(false);
        }
      });
    }
  }
  combineLatestNgrxStoreData() {
    combineLatest([
      this.store.select(loggedInUserData)]
    ).subscribe((response) => {
      this.loggedInUserData = new LoggedInUserModel(response[0]);
    });
  }
  setInTransitViewDetail(response) {
    this.InTransitViewDetail = [
      {
        name: 'BASIC INFO', columns: [
          { name: 'Receiving ID', value: response.resources?.receivingId },
          { name: 'Request Warehouse', value: response.resources?.requestWarehouseName },
          { name: 'IBT Request Number', value: response.resources?.orderNumber },
          { name: 'Issue Warehouse', value: response.resources?.issueWarehouseName },
          { name: 'Receiving Barcode', value: response.resources?.receivedBarCode },
          { name: 'Status', value: response.resources?.status, statusClass: response.resources?.cssClass },
          { name: 'Courier Name', value: response.resources?.couriername },
          { name: 'Receiving Clerk', value: response.resources?.receivingClerk !== undefined ? response.resources?.receivingClerk : '' },
          { name: 'Receiving Date', value: response.resources?.receivedDate !== undefined ? this.datePipe.transform(response.resources?.receivedDate, 'yyyy-MM-dd HH:mm:ss') : '' }
        ]

      },
    ]
  }
  getReceiveById(): Observable<IApplicationResponse> {
    //let params = new HttpParams().set('InterBranchStockOrderId', (this.interbranchstockOrderId && this.interbranchstockOrderId != undefined) ? this.interbranchstockOrderId : '').set('BatchId', this.batchId ? this.batchId : '').set('WarehouseId', this.warehouseId ? this.warehouseId : '');
    let params = new HttpParams().set('orderReceiptId', this.orderReceiptId).set('userId', this.loggedInUserData.userId);
    return this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.INTRANSIT_DETAILS_IBT, null, null, params, this.batchId);

  }
  onCRUDRequested(type: CrudType | string, row?: object, searchObj?: any): void {
    switch (type) {
      case CrudType.EDIT:
        this.navigateToEdit();
        break;
    }
  }
  navigateToEdit(): void {
    this.router.navigate(['inventory/order-receipts/receipting/intransit-add-edit'], { queryParams: { id: this.interbranchstockOrderId, barCodeNumber: this.OrderReceiptDetails.barCodeNumber, batchId: this.batchId, warehouseId: this.warehouseId }, skipLocationChange: true })
  }
  navigateVariance() {
    this.router.navigate(["/inventory/variance/variance-esclation"], {
      queryParams: {
        pickingBatch: this.OrderReceiptDetails?.orderNumber
      }, skipLocationChange: true
    });
  }
  getserialnumber(orderItems,itemCode?:any) {
    this.stockDetailListDialog = true;

    let stockCode = orderItems.serialNumber;
    this.stockCodeSerialNum = stockCode.split(",");
    this.stockDetail = orderItems;
    // this.barcode = undefined;
    // if (this.barcodes?.length != 0) {
    //   this.barcode = Array.prototype.map.call(this.barcodes, function (item) { return item.barcode; }).join(",");
    // }
    // const dialogReff = this.dialog.open(SerialnumberViewComponent, { width: '500px', disableClose: true, data: { barcode: this.barcode, count: this.barcodes.length } });
    // dialogReff.afterClosed().subscribe(result => {
    //   if (result) return;
    //   this.rxjsService.setDialogOpenProperty(false);
    // });
  }
}
