import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderReceiptInTransit, OrderReceiptItem, OrderReceiptItemDetail } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels, OrderTypes } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';
import { debounceTime, finalize, switchMap, tap } from 'rxjs/operators';
import { VarianceViewComponent } from '.';

@Component({
  selector: 'app-intransit-add-edit',
  templateUrl: './intransit-add-edit.component.html',
  styleUrls: ['./intransit-view.component.scss']
})
export class IntransitAddEditComponent implements OnInit {
  @ViewChild('serialNumber', { static: false }) serilNumberField: ElementRef;
  @ViewChild(SignaturePad, { static: false }) signaturePad: any;
  public signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 500,
    'canvasHeight': 80
  };

  receiptingType: any;
  intransitForm: FormGroup;
  orderTypes = OrderTypes;
  applicationResponse: IApplicationResponse;
  orderReceiptItemDetails: OrderReceiptItemDetail[] = [];
  filteredOrderNumbers: any[] = [];
  batchId: string='0';
  orderType: string = '';
  order: any;
  OrderReceiptDetails: any = null;
  orderNumberControl = new FormControl();
  isNew: boolean = false;
  showSupplier: boolean = false;
  isLoading = false;
  showBatchNo = false;
  userData:UserLogin;
  batchnumber:string;
  itemExistFlag = false;
  orderReceipt = new OrderReceiptInTransit();
  OrderReceipts: OrderReceiptInTransit;
  Items: any;
  barcode: any;
  orderReceiptItem:OrderReceiptItemDetail;
  ItemCopy: any;
  ispending=false;
  batchNumber: any;
  IsSubmitDisabled = true;
  batch:any;
  IsBatch = false;
  showFilterOption: boolean = false;
  barcodeNumber: any;
  orderNumber: any;
  consumerItems = [];
  isClicked: boolean = false;
  isSaveDraftDisabled:boolean=false;
  isVerifyDisabled:boolean=false;
  isPrevious:boolean=false;
  stockCodeErrorMessage : any ='';
  warehouseId:any;
  showStockCodeError : boolean = false;
  stockDetailDialog: boolean = false;
  stockDetails;
  orderSerialNumber=[];
  receivedQtyValue;
  screenType;
  showButton: boolean = false;
  constructor(private dialog: MatDialog,private snackBar: SnackbarService,private formBuilder: FormBuilder,private activatedRoute: ActivatedRoute,private httpService: CrudService, private router: Router, 
    private snackbarService: SnackbarService,
     private rxjsService: RxjsService,
    private store:Store<AppState>) {
      this.store.pipe(select(loggedInUserData)).subscribe((userData:UserLogin)=>{ if(!userData)return;
        this.userData = userData;
      });
      this.isPrevious = this.activatedRoute.snapshot.queryParams.isPrevious;
      this.barcodeNumber = this.activatedRoute.snapshot.queryParams.barCodeNumber;
      this.warehouseId = this.activatedRoute.snapshot.queryParams.warehouseId;
      this.screenType = this.activatedRoute.snapshot.queryParams.screenType;
      if (this.router && this.router.getCurrentNavigation().extras.state) {
        this.orderReceipt = this.router.getCurrentNavigation().extras.state.data;
        this.orderType = this.orderReceipt?.orderTypeId;
        this.batchId = this.orderReceipt?.orderReceiptBatchId;
        this.barcodeNumber = this.activatedRoute.snapshot.queryParams.barCodeNumber;
      }
      if(this.orderReceipt?.orderReceiptId == undefined){
        this.orderReceipt.orderReferenceId = this.activatedRoute.snapshot.queryParams.id;
        this.orderType = this.activatedRoute.snapshot.queryParams.orderTypeId;
        this.batchId = this.activatedRoute.snapshot.queryParams.batchId;
        this.barcodeNumber = this.activatedRoute.snapshot.queryParams.barCodeNumber;
      }
     }

  ngOnInit(): void {
    
    this.warehouseId=this.userData.warehouseId;
    this.isSaveDraftDisabled=true;
    if(this.isPrevious){
      this.isVerifyDisabled=false;
    }else{
      this.isVerifyDisabled=true;
    }

    this.rxjsService.setGlobalLoaderProperty(false);
    
    this.intransitForm = this.formBuilder.group({
      receivingId: [''],
      orderTypeName: [''],
      comments: [''],
      orderNumber: [''],
      supplierName: [''],
      technicianName: [''],
      storageLocationName: [''],
      barCode: [''],
      requestWarehouseName:[''],
      issueWarehouseName:[''],
      couriername:[''],
      pobarCode:[''],
    });

    

    if(this.barcodeNumber){
      this.intransitForm.controls.pobarCode.setValue(this.barcodeNumber);
      this.getIntrasitDetails(this.barcodeNumber,this.orderNumber,this.warehouseId);
    }


    this.intransitForm.get('orderNumber')
    .valueChanges.pipe(
      debounceTime(300),
      tap(() => (this.isLoading = true)),
      switchMap((value) => {
        if (!value) {
          this.isLoading = false;
          return this.filteredOrderNumbers;
        } else if (typeof value === 'object') {
          this.isLoading = false;
          return (this.filteredOrderNumbers = []);
        } else {
          value = value?.trim();
          return this.httpService
            .get(
              ModulesBasedApiSuffix.INVENTORY,
              InventoryModuleApiSuffixModels.INTER_STOCKORDER_DROPDOWN,
              null,
              true,
              prepareGetRequestHttpParams(null, null, {
                displayName: value,
                isAll: false,
              })
            )
            .pipe(finalize(() => (this.isLoading = false)));
        }
      })
    )
    .subscribe((results: any) => {
      if (results.isSuccess && results.resources.length > 0) {
        this.filteredOrderNumbers = results.resources;
        this.stockCodeErrorMessage = '';
        this.showStockCodeError = false;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      else{
        this.filteredOrderNumbers = [];
        this.stockCodeErrorMessage = 'Enter valid input';
        this.showStockCodeError = true;
        this.rxjsService.setGlobalLoaderProperty(false);
      }
      
    });

  }
  
  searchIntrasitDetails(scan?:any){
    this.barcodeNumber=this.intransitForm.controls.pobarCode.value?.trim();
    this.getIntrasitDetails(this.barcodeNumber,this.orderNumber,this.warehouseId);
  }

  onOrderSelected(orderNumber: any) {
    this.orderNumber=orderNumber;
    this.getIntrasitDetails(this.barcodeNumber,this.orderNumber,this.warehouseId);
  }
  stockDetail(index,data){
    this.stockDetailDialog= true;
    this.orderSerialNumber =  data?.receivingIbtItemDetails[0]?.receivedSerialNumber.split(',');
    this.stockDetails = data;
  }
  getIntrasitDetails(BarCode,orderNumber,warehouseId){
    if(BarCode !=""){
      this.orderReceipt.barcodeNumber = BarCode;
      this.orderReceipt.rqNumber = orderNumber;
      let params = new HttpParams().set('BarCodeNumber', (BarCode && BarCode != undefined) ? BarCode : '').set('RQNumber', orderNumber ? orderNumber : '').set('WarehouseId', warehouseId ? warehouseId : '').set('userId', this.userData.userId ? this.userData.userId : '');
      // let params = new HttpParams().set('BarCodeNumber', (BarCode && BarCode != undefined) ? BarCode : '');    
      if ('15' === OrderTypes.INTRANSIT) {
        this.httpService.get(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.RECEIVING_IBT_DETAILS,null, null, params).subscribe({
            next: response => {
              if(this.orderReceipt.IsSaveDraft==true){
                this.isSaveDraftDisabled=false;
              }
              
              this.applicationResponse = response;
              this.rxjsService.setGlobalLoaderProperty(false);
              if (response.statusCode == 200 && response.resources != null &&  response.isSuccess) {
                this.showButton=true;
                this.OrderReceiptDetails = response.resources;
                this.order = response.resources;
                this.batchnumber=this.batchId;
                this.rxjsService.setGlobalLoaderProperty(false);

                if (this.order != null) {
                  this.intransitForm.controls['pobarCode'].disable();
                  this.orderNumberControl.setValue({ displayName: this.order.orderNumber });
                  this.showSupplier = true;
                  this.isNew = true;
                  this.showBatchNo = false;
                  response.resources.receivingIbtItems.forEach(ele=>{
                    if(ele.receivingIbtItemDetails?.length >0)
                       ele.receivingIbtItemDetails[0].serialNumber=[];
                  })
                  this.orderReceipt = response.resources;
                  this.Items = response.resources.receivingIbtItems;
                  this.Items.forEach(ele=>{
                    ele.receivedQtyCopy =  ele.receivedQty;
                    ele.qtyToReceive =  ele.receivedQty;
                  });
                  this.ItemCopy = this.Items;

                  this.rxjsService.setGlobalLoaderProperty(false);
                  this.orderReceipt.batchNumber = this.batchnumber;//this.order.batchNumber
                  this.orderReceipt.orderReceiptId = this.order.orderReceiptId;
                  this.orderReceipt.isVerified = this.order.isVerified;
                  this.orderReceipt.orderTypeId = this.orderType;
                  this.orderReceipt.orderReceiptBatchId=this.order.orderReceiptBatchId;
                  this.intransitForm.controls.orderNumber.setValue(this.order.ibtRequestNumber);
                  this.intransitForm.controls.requestWarehouseName.setValue(this.order.fromWarehouseName);
                  this.intransitForm.controls.issueWarehouseName.setValue(this.order.toWarehouseName);
                  this.intransitForm.controls.couriername.setValue(this.order.courierName);
                  // this.batch= Array.prototype.map.call(this.order.orderReceiptItem, s => s.pickingQty).toString();
                  // let array = this.batch.split(',');
                  // var max = array.reduce(function(a, b) {return Math.max(a, b);});
                  // if(Number(max) ===0){
                  // this.IsBatch=true;
                  // this.orderReceipt.IsBatch=true;
                  // }else{
                  //   this.IsBatch=false;
                  //   this.orderReceipt.IsBatch=false;
                  // }
                  // let draftarray = []
                  // let list = this.order.orderReceiptItem;
                  // this.order.orderReceiptItem.forEach((value, index) => {
                  //   value.orderReceiptItemDetails.forEach((itemDetail) => {
                  //     draftarray.push(itemDetail.isDraft);
                  //   });
                    
                  // });
                  // if (draftarray.includes(true)){
                  //   this.isVerifyDisabled=false;
                  // }
                }

                this.rxjsService.setGlobalLoaderProperty(false);
              }
              else{
                this.showButton=false;
                this.order = null;
                this.snackBar.openSnackbar(this.applicationResponse.message = response?.message, ResponseMessageTypes.WARNING);
              }
              
            }
          });
      }
    }
    else{
      this.snackBar.openSnackbar("Barcode is required!", ResponseMessageTypes.WARNING);
    }
  }
  drawComplete() {

  }
  clearPad() {
    this.signaturePad.clear();
  }
  ngAfterViewInit() {
    this.signaturePad?.set('minWidth', 1);
    this.signaturePad?.resizeCanvas();
    this.signaturePad?.clear();
  }
  getPickerJobDetails(orderReceiptId) {
    if (orderReceiptId && orderReceiptId != null) {
      let orderNumber = (typeof orderReceiptId === 'object') ? orderReceiptId.id : orderReceiptId;
     
      let params = new HttpParams().set('InterBranchStockOrderId', orderReceiptId);
      return this.httpService.get(
        ModulesBasedApiSuffix.INVENTORY,
        InventoryModuleApiSuffixModels.INTERBRANCH_STOCK_DETAILS,null,null, params).subscribe({
          next: response => {
            if (response.statusCode == 200 && response.resources != null) {
              this.applicationResponse = response;
              this.showFilterOption = true;
              this.orderReceipt = response.resources;
              this.Items = this.orderReceipt.orderReceiptItem;
              this.ItemCopy = this.orderReceipt.orderReceiptItem;
              this.rxjsService.setGlobalLoaderProperty(false);
            }
            else {
              this.orderReceipt = new OrderReceiptInTransit();
              this.Items = new Array();
            }

          }
        });
      this.rxjsService.setGlobalLoaderProperty(false);
    }

  }
  
  getPurchaseOrder(intid) {
    if (intid && intid != null) {
      let pId = (typeof intid === 'object') ? intid.id : intid;
      
      let params = new HttpParams().set('InterBranchStockOrderId', (this.orderReceipt.orderReferenceId && this.orderReceipt.orderReferenceId != undefined) ? this.orderReceipt.orderReferenceId : '');    
      if (this.orderType === OrderTypes.INTRANSIT) {
        this.httpService.get(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.INTRANSIT_DETAILS,null, null, params).subscribe({
            next: response => {
              this.order = response.resources;
              this.batchnumber=this.batchId;
             
              if (this.order != null) {
                this.orderNumberControl.setValue({ displayName: this.order.orderNumber });
                this.showSupplier = true;
                this.isNew = true;
                this.showBatchNo = false;
               // this.supplierName = this.order.supplier.supplierName;
                this.orderReceipt.batchNumber = this.batchnumber;//this.order.batchNumber
                this.orderReceipt.orderReceiptId = this.order.orderReceiptId;
                this.orderReceipt.isVerified = this.order.isVerified;
                this.orderReceipt.orderTypeId = this.orderType;
                this.orderReceipt.orderReceiptBatchId=this.batchId;
                this.orderReceipt.orderReceiptId =  response.resources.orderReceiptId;
                //this.orderReceipt.pendingBatch = this.order['pendingBatch'];
                //this.batchId = this.orderReceipt.pendingBatch != null ? this.orderReceipt.pendingBatch.orderReceiptBatchId : '0';
                this.intransitForm.controls.requestId.setValue(this.order.requestId);
                this.intransitForm.controls.orderTypeName.setValue(this.order.orderTypeName);
                this.intransitForm.controls.orderNumber.setValue(this.order.orderNumber);
                this.intransitForm.controls.supplierName.setValue(this.order.supplierName);
                this.intransitForm.controls.technicianName.setValue(this.order.technicianName);
                this.intransitForm.controls.storageLocationName.setValue(this.order.storageLocationName);
                
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
      }
      
     
      // if(pid.id !== undefined)
      //       this.orderReceipt.orderReferenceId = pid.id;
    }

  }
  
  getpickerBarcode() {
    this.barcode = this.intransitForm.get('barCode').value;
    if (this.barcode === '' || this.barcode == null || !this.barcode){
      this.snackBar.openSnackbar(this.applicationResponse.message = "Please enter the Serial Number.", ResponseMessageTypes.WARNING);
      return;
    }
   

    this.itemExistFlag = false;
    let toastArr1 = []; let toastArr2 = [];let toastArr3 = [];
    this.Items?.forEach((item, index) => {
      item?.receivingIbtItemDetails?.forEach((itemDetail) => {
        let orderSerialNumber:any[];  let receivedSerialNumber:any[]; 
        orderSerialNumber = itemDetail?.orderSerialNumber?.split(',');
        receivedSerialNumber =  itemDetail?.receivedSerialNumber?.split(',');
        if (orderSerialNumber?.length >0 && (orderSerialNumber.includes(this.barcode)==false || orderSerialNumber.includes(this.barcode)==null)) {
         toastArr1.push(true);
        }
        if (receivedSerialNumber?.length >0 && receivedSerialNumber?.includes(this.barcode)) {
        //  this.snackBar.openSnackbar(this.applicationResponse.message = "Serial Number has been received.", ResponseMessageTypes.WARNING);
         // this.itemExistFlag = true;
          toastArr2.push(true);
        }
        if(orderSerialNumber.includes(this.barcode) && !receivedSerialNumber?.includes(this.barcode)) {
  
          if(itemDetail.serialNumber?.includes(this.barcode)){
            toastArr3.push(true);
          }else {
            itemDetail.serialNumber.push(this.barcode);
            this.Items[index].receivedQty = this.Items[index].receivedQty + 1;
          }
        }
      });
    });

    if (toastArr1.includes(true)) {
      this.snackBar.openSnackbar(this.applicationResponse.message = "Serial Number scanned does not exists.", ResponseMessageTypes.WARNING);
      return;
    }
    else if (toastArr2.includes(true)) {
      this.snackBar.openSnackbar(this.applicationResponse.message = "Serial Number has been already received.", ResponseMessageTypes.WARNING);
      return;
    }
    // else if (toastArr2.includes(true)) {
    //   this.snackBar.openSnackbar(this.applicationResponse.message = "Serial Number has been already scanned.", ResponseMessageTypes.WARNING);
    //   return;
    // }

    this.intransitForm.get('barCode').setValue('');
  }
  changeQty(value,index) {
    let total =   parseInt(this.Items[index].orderQty);
    if(parseInt(value) <= total  ){
      this.Items[index].qtyToReceive=value; 
      this.Items[index].receivedQty=value;
    } else {
      this.Items[index].qtyToReceive=null; 
      this.Items[index].receivedQty=0;
      this.snackbarService.openSnackbar("You can enter upto "+ total +" quantity only.",ResponseMessageTypes.WARNING);
    }
  }
  quantityToReceiveUpdate(value,index) {

    let tem = this.Items[index].receivedQty +  parseInt(value);
    if(tem > this.Items[index].orderQty) {
      this.Items[index].qtyToReceive = null;
      this.snackbarService.openSnackbar('Please enter correct quantity to receive. ', ResponseMessageTypes.WARNING);
      return;
    }else if(value){
      this.Items[index].qtyToReceive = value;
    //  this.Items[index].receivedQty = parseInt(value);
    }else if(!value || value==0){
      this.Items[index].qtyToReceive = null;
    }
   
  
  }
  notserializedQty(event,data){
    if(data.availableQty>=Number(event)){
    this.consumerItems=[];
    let Item=new OrderReceiptItem();
    Item.pickingQty=Number(event)-data.pickingQty;
    Item.varianceQty = parseInt(data.availableQty) - Number(event);
    let index = this.Items.findIndex(item => item.itemId == data.itemId);
    this.Items[index].varianceQty = Item.varianceQty;
    Item.isNotSerialized=true;
    Item.orderItemId=data.itemId;
    this.consumerItems.push(Item);
    this.orderReceipt.orderReceiptItem = this.orderReceipt.orderReceiptItem.filter(item => item.itemId !== data.itemId);
    this.isSaveDraftDisabled=false;
    this.isVerifyDisabled=false;
    }else{
      let Item=new OrderReceiptItem();
      let index = this.Items.findIndex(item => item.itemId == data.itemId);
      this.Items[index].varianceQty = 0;
      this.snackBar.openSnackbar(this.applicationResponse.message = "Please Enter Minimum or Equal to Issue Qty!", ResponseMessageTypes.WARNING);
    }
    
  }

  
  loadBarcodes() {
    this.itemExistFlag = false;
    let itemFoundFlag = false;
    this.Items.forEach((item: OrderReceiptItem) => {
      item.orderReceiptItemDetails.forEach((itemDetail: OrderReceiptItemDetail) => {
        if (itemDetail.itemId == item.itemId) {
          item.pickingQty = item.pickingQty == undefined ? 1 : item.pickingQty += 1;
          item.quantity = item.pickingQty;
          if (item.pickedBarcode == undefined)
            item.pickedBarcode = itemDetail.barcode;
          else
            item.pickedBarcode += ',' + itemDetail.barcode;

          //remove the comma at beginning
          item.pickedBarcode = item.pickedBarcode.replace(/^,|,$/g, '');

          itemDetail.pickedBarcode = itemDetail.barcode;
          itemFoundFlag = true;
        }
      });
    });

  }
  //Redirect to barcode scan page and pass related items
  barcodeScan(purchaseOrderItem, purchaseOrderDetails) {
    if (purchaseOrderDetails) {
      this.orderReceipt.orderNumber = purchaseOrderDetails.orderNumber;
      this.orderReceipt.orderReferenceId = purchaseOrderDetails.orderReferenceId;
      this.orderReceipt.warehouseId = this.userData.warehouseId;
      this.orderReceipt.inventoryStaffId = this.userData.userId;
      this.orderReceipt.supplierId = purchaseOrderDetails.supplierId;
      this.orderReceipt.orderReceiptId = purchaseOrderItem.orderReceiptId;
      let order = {
        itemName: purchaseOrderItem.itemName,
        itemCode: purchaseOrderItem.itemCode,
        purchaseOrderItem: purchaseOrderItem,
        orderReceipt: this.orderReceipt
      }
      this.router.navigate(['/inventory/order-receipts/receipting/item-scan'], { state: { data: order } });
    }

  }
  saveDraftOld(){
    this.isSaveDraftDisabled=true;
    this.orderReceipt.IsSaveDraft=true;
    this.orderReceipt.barcodeNumber=this.orderReceipt.barcodeNumber;
    this.orderReceipt.IsPrevious=this.isPrevious;
    this.orderReceipt.rqNumber=this.orderReceipt.rqNumber;
    this.orderReceipt.inventoryStaffId = this.userData.userId;
    this.orderReceipt.warehouseId = this.userData.warehouseId;
    this.orderReceipt.orderReferenceId = this.orderReceipt['interBranchStockOrderId'];
    this.orderReceipt.createdUserId = this.userData.userId;
    this.orderReceipt.orderReceiptItem = this.orderReceipt.orderReceiptItem.filter(function (item) {
      return item.pickingQty > 0;
    });

    if (this.orderReceipt.orderReceiptItem.length == 0) {
      this.snackbarService.openSnackbar('Please check mandatory fields', ResponseMessageTypes.ERROR);
      return;
    }      

    this.orderReceipt.orderReceiptItem.forEach(item => {
      item.orderReceiptItemDetails = item.orderReceiptItemDetails.filter(function (detail) {
        return detail.pickedBarcode != undefined;
      });
    }); 
    if(this.isClicked==false){
      this.orderReceipt.orderReceiptItem=[];
    }
    if(this.consumerItems.length==0){
      this.orderReceipt.orderReceiptItem = this.orderReceipt.orderReceiptItem.filter(item => item.isNotSerialized !== true);
    }
    this.orderReceipt.orderReceiptItem = this.orderReceipt.orderReceiptItem.filter(item => item.receivedQty !== item.availableQty);
    
    this.consumerItems.forEach(item => {
      this.orderReceipt.orderReceiptItem.push(item);
    });   

    this.httpService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_RECEIPTS_INTRANSIT, this.orderReceipt).subscribe((response) => {
      if (response.message != "Receipting  Please enter valid serial number") {
        this.orderReceipt.orderReceiptBatchId=response.resources;
        this.getIntrasitDetails(this.barcodeNumber,this.orderNumber,this.warehouseId);
      }
    });

  }
  savePickerJob() {
    
     let receivingIbtItems;let finalObject;
      receivingIbtItems  =  this.orderReceipt?.receivingIbtItems;
     let receivingIbtItemsOnj=[];
     this.Items?.forEach(ele=>{
        let obj =  {
          interBranchStockOrderId: this.orderReceipt['interBranchStockOrderId'],
          itemId: ele.itemId,
          receivedQty:ele?.isNotSerialized ? (ele?.receivedQty ? parseInt(ele?.receivedQty)  : 0) : ele?.receivingIbtItemDetails[0]?.serialNumber?.length,
          serialNumber: ele?.receivingIbtItemDetails[0]?.serialNumber?.length > 0?  ele?.receivingIbtItemDetails[0]?.serialNumber?.toString() : null,
          userId: this.userData.userId
        }
        receivingIbtItemsOnj.push(obj);
     });
     finalObject = {
      "interBranchStockOrderId": this.orderReceipt['interBranchStockOrderId'],
      "isVerify": true,
      "userId": this.userData.userId,
      "comments":this.intransitForm.get('comments').value,
      "items": receivingIbtItemsOnj
     }
     const formData = new FormData();
     formData.append("receivingDetails", JSON.stringify(finalObject));

     if (this.signaturePad.signaturePad._data.length > 0) {
       let imageName = "Signature" + ".jpeg";
       let imageBlob; imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
       const imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' });
       formData.append('document_files[]', imageFile);
     }else {
      this.snackbarService.openSnackbar("Please do enter the signature.",ResponseMessageTypes.ERROR);
      return;
     }
    

    this.httpService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RECEIVING_IBT_SAVE, formData).subscribe((response) => {
      response.message != "Receipting  Please enter valid serial number"
      if (response?.isSuccess && response?.statusCode==200 ) {
       // this.orderReceipt.orderReceiptBatchId=response.resources;
       // this.verifyDescrepancy();
        if(this.screenType == 'job-selection')
         this.navigateToJobSelection();
        else
         this.listPage();
      }
      this.rxjsService.setGlobalLoaderProperty(false);
    });
  }
  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }
  saveDraft() {
    let receivingIbtItems; let finalObject;
     receivingIbtItems  =  this.orderReceipt?.receivingIbtItems;
    let receivingIbtItemsOnj=[];
    this.Items?.forEach(ele=>{
       let obj =  {
         interBranchStockOrderId: this.orderReceipt['interBranchStockOrderId'],
         itemId: ele.itemId,
         receivedQty:ele?.isNotSerialized ? (ele?.receivedQty ? parseInt(ele?.receivedQty)  : 0) : ele?.receivingIbtItemDetails[0]?.serialNumber?.length,
         serialNumber: ele?.receivingIbtItemDetails[0]?.serialNumber?.length >0 ? ele?.receivingIbtItemDetails[0]?.serialNumber?.toString() : null,
         userId: this.userData.userId
       }
       receivingIbtItemsOnj.push(obj);
    });
    finalObject = {
      "interBranchStockOrderId": this.orderReceipt['interBranchStockOrderId'],
      "isVerify": false,
      "comments":this.intransitForm.get('comments').value,
      "userId": this.userData.userId,
      "items": receivingIbtItemsOnj
     }
     const formData = new FormData();
     formData.append("receivingDetails", JSON.stringify(finalObject));

     if (this.signaturePad.signaturePad._data.length > 0) {
       let imageName = "Signature" + ".jpeg";
       let imageBlob; imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
       const imageFile = new File([imageBlob], imageName, { type: 'image/jpeg' });
       formData.append('document_files[]', imageFile);
     }
  

   this.httpService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.RECEIVING_IBT_SAVE, formData).subscribe((response) => {
     response.message != "Receipting  Please enter valid serial number"
     if (response?.isSuccess && response?.statusCode==200) {
      // this.orderReceipt.orderReceiptBatchId=response.resources;
      // this.verifyDescrepancy();
      if(this.screenType == 'job-selection')
       this.navigateToJobSelection();
      else
        this.listPage();
     }
     this.rxjsService.setGlobalLoaderProperty(false);
   });
 }
  private verifyDescrepancy() {
    if (this.orderReceipt.orderReceiptId != null) {
      this.router.navigate(['inventory/order-receipts/receipting/intransit-verify/'], { state: { data: this.orderReceipt } });
    }
    else {
      this.snackbarService.openSnackbar('Please check mandatory fields', ResponseMessageTypes.ERROR);
    }
  }
  
  view() {
    this.router.navigate(["inventory/order-receipts/receipting/intransit-view"], { queryParams: { id: this.orderReceipt.orderReferenceId }, skipLocationChange: true });
  }
  cancel() {
    if(this.screenType == 'job-selection')
       this.navigateToJobSelection();
      else
        this.listPage();
  }
  listPage() {
    this.router.navigate(['/inventory/order-receipts/receipting'], { queryParams: { tab: 3 } });
  }
  navigateToJobSelection (){
    this.router.navigate(['/inventory/job-selection'], { queryParams: { tab: 0 } });
  }
  getvariance(event,data){

    if(event?.varianceQty!=0 && event.receivingIbtItems.length!=0){
      const dialogReff = this.dialog.open(VarianceViewComponent, 
      { width: '700px', disableClose: true,data: { item: event,pending: data } });
      dialogReff.afterClosed().subscribe(result => {
        if (result) return;
        this.rxjsService.setDialogOpenProperty(false);
      });
    }
    else{
      return;
    }
   
  }
}
