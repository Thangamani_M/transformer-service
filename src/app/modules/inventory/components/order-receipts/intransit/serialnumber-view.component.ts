
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-serialnumber-view',
  templateUrl: './serialnumber-view.component.html',
  styleUrls: ['./serialnumber-view.component.scss']
})
export class SerialnumberViewComponent implements OnInit {
  barcode: any;
  barcodes: any;
  count: any;
  printsection: boolean;
  printcodes: any[] = [];

  elementType = 'svg';
  value = 'someValue12340987';
  format = 'CODE128';
  lineColor = '#000000';
  width = 2;
  height = 100;
  displayValue = true;
  fontOptions = '';
  font = 'monospace';
  textAlign = 'center';
  textPosition = 'bottom';
  textMargin = 2;
  fontSize = 20;
  background = '#ffffff';
  margin = 10;
  marginTop = 10;
  marginBottom = 10;
  marginLeft = 10;
  marginRight = 10;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {

    this.printcodes = [];
    this.printsection = false;
    this.barcode = this.data.barcode;
    if (this.barcode != null) {
      this.count = this.data.count;
      this.barcodes = this.barcode.split(',');
    }

  }

  get values(): string[] {
    return this.value.split('\n');
  }

  onPrintBarCodeMultiple() {

    this.printcodes = [];
    this.printcodes = this.barcodes;
    this.print();
  }
  onPrintBarCode(code) {

    this.printcodes = [];
    this.printcodes.push(code);
    this.print();
  }


  print() {

    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
  <html>
    <head>
      <style>
      body{  width: 99%;}
        label { font-weight: 400;
                font-size: 13px;
                padding: 2px;
                margin-bottom: 5px;
              }
        table, td, th {
               border: 1px solid silver;
                }
                table td {
               font-size: 13px;
                }

                 table th {
               font-size: 13px;
                }
          table {
                border-collapse: collapse;
                width: 98%;
                }
            th {
                height: 26px;
                }
      </style>
    </head>
<body onload="window.print();window.close()">${printContents}</body>
  </html>`
    );
    popupWin.document.close();

  }

}
