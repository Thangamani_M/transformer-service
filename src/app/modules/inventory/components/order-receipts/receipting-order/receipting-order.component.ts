import { HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { KeyValuePair, OrderReceipt } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, prepareGetRequestHttpParams, prepareRequiredHttpParams } from '@app/shared';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { InventoryModuleApiSuffixModels, OrderTypes } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { UserLogin } from '@modules/others/models';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { debounceTime, finalize, switchMap, tap } from 'rxjs/operators';
import { Supplier } from '../../../models/Supplier';

@Component({
  selector: 'app-recepting-order',
  templateUrl: './receipting-order.component.html',
  styleUrls: ['./receipting-order.component.scss']
})
export class ReceiptingOrderComponent implements OnInit {
  @ViewChild('serialNumber', { static: false }) serilNumberField: ElementRef;

  userData: UserLogin;
  orderReferenceId: string;
  orderNumberControl = new FormControl();
  applicationResponse: IApplicationResponse;
  uxOderTypes: any;
  orderType: string = '';
  storageLocation: string = '';
  isNew: boolean = false;
  showSupplier: boolean = false;
  receiptingOrderForm: FormGroup;
  keyValuePair: KeyValuePair;
  uxStorageLocations: any;
  filteredPurchaseOrders: any[] = [];
  purchaseOrderAutoFillForm: FormGroup;
  isLoading = false;
  supplierName: string = '';
  order: any;
  supplier = new Supplier();
  orderReceipt = new OrderReceipt();
  showBatchNo = false;
  orderTypes = OrderTypes;
  orderTypesOptions = [];
  receivingForm: FormGroup;
  receiptingType: any;
  batchId: string = '0';
  poBarcode: string = '';
  isDraft: boolean = false;
  isVerifyDisabled = true;
  deleteConfirm: boolean = false
  //mask validations
  public alphaNumericWithSpacePattern = { '0': { pattern: new RegExp('^[A-Za-z0-9 ]*$') } };

  @ViewChild('ddl', null) myDiv: ElementRef<HTMLElement>;
  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private httpService: CrudService, private router: Router,
    private snackbarService: SnackbarService,
    private rxjsService: RxjsService,
    private store: Store<AppState>) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData: UserLogin) => {
      if (!userData) return;
      this.userData = userData;
    });
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.orderReceipt = this.router.getCurrentNavigation().extras.state.data;
      this.orderType = this.orderReceipt.orderTypeId;
      this.batchId = this.orderReceipt.orderReceiptBatchId;
    }
    if (this.orderReceipt.orderReceiptId == undefined) {
      this.orderReceipt.orderReferenceId = this.activatedRoute.snapshot.queryParams.id;
      this.orderType = this.activatedRoute.snapshot.queryParams.orderTypeId;
      this.batchId = this.activatedRoute.snapshot.queryParams.batchId;
    }

  }

  ngOnInit() {

    this.receivingForm = this.formBuilder.group({
      requestId: [''],
      orderTypeName: [''],
      orderNumber: [''],
      supplierName: [''],
      storageLocationName: [''],
      warehouseName: [''],
      poBarcode: [''],
    });

    if (this.orderReceipt.orderReferenceId != undefined) {
      this.getPurchaseOrder(this.orderReceipt.orderReferenceId);
    }

    this.orderTypesOptions = Object.entries(this.orderTypes).map(([key, value]) => ({ key, value }))
    //temporarly added this below code. we will be remove this code after implementation
    this.orderTypesOptions = this.orderTypesOptions.filter(function (obj) {
      return obj.key !== 'REPLACEMENT';
    });

    if (this.orderReceipt.orderReferenceId == undefined && this.orderType == this.orderTypes.NEW) {
      this.orderReceipt.orderTypeId = this.orderType;
      this.isLoading = false,
        this.rxjsService.setGlobalLoaderProperty(false);
      //return;
    }

    this.orderNumberControl.valueChanges.pipe(debounceTime(300), tap(() => this.isLoading = true),
      switchMap(value => {

        if (!value) {
          this.isLoading = false;
          return this.filteredPurchaseOrders;
        } else if (typeof value === 'object') {
          this.isLoading = false;
          return this.filteredPurchaseOrders = [];
        } else {
          if (this.orderReceipt.orderTypeId === OrderTypes.NEW) {
            let resources = this.getUXPurchaseOrderSearch(value);
            return resources;
          } else if (this.orderReceipt.orderTypeId.toLocaleLowerCase() == OrderTypes.RETURN) {
            return this.getSupReturnNumbersUX(value);
          } else {
            let resources = this.getUXRepairRequestSearch(value);
            return resources;
          }
        }
      }
      )).subscribe(results => {
        this.filteredPurchaseOrders = results.resources;
        // if (this.filteredPurchaseOrders.length == 0){
        //   this.snackbarService.openSnackbar("Enter valid PO number", 
        //   ResponseMessageTypes.WARNING);
        // }
      });
  }

  getSupReturnNumbersUX(value: any): Observable<any> {
    return this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_SUP_RETURN_NUMBER_SEARCH, null, true, prepareGetRequestHttpParams(null, null, { displayName: value }))
      .pipe(
        finalize(() => {
          this.isLoading = false,
            this.rxjsService.setGlobalLoaderProperty(false);
        }),
      );
  }

  getUXRepairRequestSearch(value: string): Observable<any> {
    return this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_REPAIRREQUEST_SEARCH, null, true, prepareGetRequestHttpParams(null, null, { searchText: value, orderTypeId: this.orderReceipt.orderTypeId }))
      .pipe(
        finalize(() => {
          this.isLoading = false,
            this.rxjsService.setGlobalLoaderProperty(false);
        }),
      );

  }

  getUXPurchaseOrderSearch(value: string): Observable<any> {
    return this.httpService.get(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_PURCHASEORDER_BY_WEREHOUSE, null, true, 
      prepareGetRequestHttpParams(null, null, { displayName: value, userId: this.userData.userId }))
      .pipe(
        finalize(() => {
          this.isLoading = false,
            this.rxjsService.setGlobalLoaderProperty(false);
        }),
      );

  }

  getUXOrderType(): void {

    this.httpService.dropdown(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_TYPES)
      .subscribe(resp => {
        this.applicationResponse = resp;
        this.uxOderTypes = this.applicationResponse.resources;
        this.rxjsService.setGlobalLoaderProperty(false);

      });
  }


  //Redirect to barcode scan page and pass related items
  barcodeScan(purchaseOrderItem, purchaseOrderDetails) {

    if (purchaseOrderDetails) {
      this.orderReceipt.orderNumber = purchaseOrderDetails.orderNumber;
      this.orderReceipt.orderReferenceId = purchaseOrderDetails.orderReferenceId;
      this.orderReceipt.warehouseId = this.userData.warehouseId;
      this.orderReceipt.inventoryStaffId = this.userData.userId;
      this.orderReceipt.supplierId = purchaseOrderDetails.supplierId;
      this.orderReceipt.orderReceiptId = purchaseOrderItem.orderReceiptId;
      let order = {
        itemName: purchaseOrderItem.itemName,
        itemCode: purchaseOrderItem.itemCode,
        purchaseOrderItem: purchaseOrderItem,
        orderReceipt: this.orderReceipt
      }
      if (purchaseOrderItem.isNotSerialized)
        this.router.navigate(['/inventory/order-receipts/receipting/item-not-serialized'], { state: { data: order } });
      else
        this.router.navigate(['/inventory/order-receipts/receipting/item-scan'], { state: { data: order } });
    }

  }


  //Selected value to assign auto fill textbox
  displayPurchaseOrderNo(keyValuePair: KeyValuePair) {
    if (keyValuePair) { return keyValuePair.displayName; }
  }

  //Get purchase order with related items and suppliers
  getPurchaseOrderByOrderNumber(poNumber) {
    let pid = this.filteredPurchaseOrders.find(
      (x) => x.displayName == poNumber
    ).id;
    this.batchId = null;
    this.getPurchaseOrder(pid);
  }

  //Get purchase order with related items by PO barcode
  iSpoBarcode: boolean = false;
  getPurchaseOrderByBarcode() {
    this.poBarcode = this.receivingForm.get('poBarcode').value;
    this.iSpoBarcode = true;
    this.getPurchaseOrder(this.poBarcode);
  }

  getPurchaseOrder(pid) {
    if (pid && pid != null) {
      let pId = (typeof pid === 'object') ? pid.id : pid;
      let params;
      params = new HttpParams().set('PurchaseOrderId', pId)
      .set('UserId', this.userData.userId);
      if (this.batchId != '0' && this.batchId != undefined)
        params = new HttpParams().set('PurchaseOrderId', pId)
        .set('BatchId', this.batchId)
        // .set('warehouseIds', this.userData.warehouseId)
        .set('UserId', this.userData.userId);
      if (this.iSpoBarcode) {
        params = new HttpParams().set('poBarcode', pId)
        // .set('warehouseIds', this.userData.warehouseId)
        .set('UserId', this.userData.userId);
        this.iSpoBarcode = false;
      }

      if (this.orderType === OrderTypes.NEW) {
        this.httpService.get(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.PURCHASEORDER_RELATEDITEMS, null, null, params).subscribe({
            next: response => {
              this.order = response.resources;
              if (this.order != null) {
                this.orderNumberControl.setValue(this.order.orderNumber);
                this.showSupplier = true;
                this.isNew = true;
                this.showBatchNo = false;
                this.supplierName = this.order.supplier.supplierName;
                this.orderReceipt.batchNumber = "1";//this.order.batchNumber
                this.orderReceipt.orderReceiptId = this.order.orderReceiptId;
                this.orderReceipt.isVerified = this.order.isVerified;
                this.orderReceipt.orderTypeId = this.orderType;
                this.orderReceipt.pendingBatch = this.order['pendingBatch'];
                this.batchId = this.orderReceipt.pendingBatch != null ? this.orderReceipt.pendingBatch.orderReceiptBatchId : '';
                this.orderReceipt.orderReceiptBatchId = this.batchId;
                this.receivingForm.controls.requestId.setValue(this.order.requestId);
                this.receivingForm.controls.orderTypeName.setValue(this.order.orderTypeName);
                this.receivingForm.controls.orderNumber.setValue(this.order.orderNumber);
                this.receivingForm.controls.supplierName.setValue(this.order.supplierName);
                this.receivingForm.controls.storageLocationName.setValue(this.order.storageLocationName);
                this.receivingForm.controls.warehouseName.setValue(this.order.warehouseName);
                this.receivingForm.controls.poBarcode.setValue(this.order.poBarcode);
                this.isDraft = this.orderReceipt.pendingBatch != null ? this.orderReceipt.pendingBatch.isDraft : false;
                if (this.order.orderItems.some(i => i.receivedQty > 0) == true)
                  this.isVerifyDisabled = false;
                else
                  this.isVerifyDisabled = true;
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
      }
      else if (this.orderReceipt.orderTypeId.toLocaleLowerCase() === OrderTypes.RETURN.toLocaleLowerCase()) {
        let httpParams = new HttpParams();
        this.httpService.get(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.SUPPLIER_RETURN_RELATEDITMES, null, null, httpParams.append("supreturnid", pId)).subscribe({
            next: response => {

              this.order = response.resources;

              if (this.order != null) {
                this.orderNumberControl.setValue(this.order.orderNumber);
                this.showSupplier = true;
                this.isNew = true;
                this.showBatchNo = false;
                this.supplierName = this.order.supplierName;
                this.orderReceipt.batchNumber = "1";//this.order.batchNumber;
                this.orderReceipt.orderTypeId = this.orderReceipt.orderTypeId;
                this.orderReceipt.orderReceiptId = this.order.orderReceiptId;
                this.orderReceipt.relatedOrderTypeId = this.order.relatedOrderTypeId;
                this.orderReceipt.isVerified = this.order.isVerified;
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
      }
      else {
        this.httpService.get(
          ModulesBasedApiSuffix.INVENTORY,
          InventoryModuleApiSuffixModels.REPAIRREQUEST_RELATEDITEMS, pId).subscribe({
            next: response => {
              this.order = response.resources;
              if (this.order != null) {
                this.orderNumberControl.setValue(this.order.orderNumber);
                this.showSupplier = false;
                this.isNew = true;
                this.showBatchNo = false;
                this.order.supplier = null;
                this.order.batchNumber = "1";
                this.orderReceipt.orderReceiptId = this.order.orderReceiptId;
                this.orderReceipt.isVerified = this.order.isVerified;
              }
              this.rxjsService.setGlobalLoaderProperty(false);
            }
          });
      }
      if (pid.id !== undefined)
        this.orderReceipt.orderReferenceId = pid.id;
    }

  }
  getreceivedetailsById(purchaseOrderId: string) {
    this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.PURCHASEORDER_RELATEDITEMS,
      purchaseOrderId,
      false,
      null
    ).subscribe((response: IApplicationResponse) => {
      if (response.resources) {

        this.order = response.resources;
        this.receivingForm.controls.requestId.setValue(this.order.requestId);
        this.receivingForm.controls.orderTypeName.setValue(this.order.orderTypeName);
        this.receivingForm.controls.orderNumber.setValue(this.order.orderNumber);
        this.receivingForm.controls.supplierName.setValue(this.order.supplierName);
        this.receivingForm.controls.storageLocationName.setValue(this.order.storageLocationName);
      }
      // this.rxjsService.setGlobalLoaderProperty(false);
    });
  }



  //Get storage location based on warehouse
  getUXstorageLocations() {


    const params = new HttpParams().set('warehouseid', this.userData.warehouseId)

    this.httpService.dropdown(
      ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.UX_LOCATION, params
    ).subscribe(resp => {
      this.uxStorageLocations = resp.resources;

      //Defalut storage location should be staging bay location
      this.orderReceipt.locationId = "72b3bff0-7425-475b-a554-7addbcc3e9fa";
      this.rxjsService.setGlobalLoaderProperty(false);
    });

  }

  //hide and show ui elements based on receipting type
  receiptingTypeChange(event) {

    let selectedReceiptingType = event;
    this.receiptingType = selectedReceiptingType.toLocaleLowerCase();
    //New
    if (selectedReceiptingType.toLocaleLowerCase() === 'new') {
      this.isNew = true;
      this.showSupplier = false;
      this.filteredPurchaseOrders = [];
      this.order = null;
      this.showBatchNo = false;
      this.orderNumberControl.setValue('');
      this.orderReceipt.batchNumber = null;
    }
    //Return
    else if (selectedReceiptingType.toLocaleLowerCase() === 'return') {
      this.EnableDisableControls();
    }
    //Replacement
    else if (selectedReceiptingType.toLocaleLowerCase() === 'replacement') {
      this.EnableDisableControls();
    }//Faulty
    else if (selectedReceiptingType.toLocaleLowerCase() === 'faulty') {
      this.EnableDisableControls();
    }

  }

  private EnableDisableControls() {
    this.isNew = true;
    this.showSupplier = false;
    this.filteredPurchaseOrders = [];
    this.order = null;
    this.showBatchNo = false;
    this.orderNumberControl.setValue('');
    this.orderReceipt.batchNumber = null;
  }
  public verifyDescrepancy() {

    if (this.orderReceipt.orderReceiptId != null) {
      //this.router.navigate(['/inventory/order-receipts/receipting/discrepancy/'], { queryParams: { id: this.orderReceipt.orderReceiptId } });
      this.orderReceipt.orderReceiptBatchId = this.batchId;
      this.router.navigate(['/inventory/order-receipts/receipting/discrepancy/'], { state: { data: this.orderReceipt } });
    }
    else {
      this.snackbarService.openSnackbar('Enter valid data', ResponseMessageTypes.ERROR);
    }

  }

  saveAsDraft() {
    let saveAsDraft = {
      OrderReceiptId: this.orderReceipt.orderReceiptId,
      OrderReceiptBatchId: this.batchId,
      CreatedUserId: this.userData.userId
    };
    this.httpService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_RECEIPTS_SAVE_AS_DRAFT, saveAsDraft).subscribe((response) => {
      if (response.isSuccess == true) {
        this.listPage();
      }
    });
  }

  delete() {
    this.deleteConfirm = true;
  }

  deleteReceptingOrder() {
    let body = {
      "ids": this.batchId,
      "ModifiedUserId": this.userData.userId
    }
    this.httpService.delete(ModulesBasedApiSuffix.INVENTORY,
      InventoryModuleApiSuffixModels.ORDER_RECEIPTS, null, prepareRequiredHttpParams(body)).subscribe({
        next: response => {
          this.deleteConfirm = false;
          if (response.isSuccess == true)
            this.listPage();
        }
      });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onClose() {

  }

  onSaveComplete(response): void {
    if (response.statusCode == 200) {
    }
  }

  view() {
    this.router.navigate(["inventory/order-receipts/receipting/view"], { queryParams: { PurchaseOrderId: this.orderReceipt.orderReferenceId, BatchId: this.orderReceipt.orderReceiptBatchId, OrderTypeId: this.orderReceipt.orderTypeId }, skipLocationChange: true });
  }
  listPage() {
    this.router.navigate(['/inventory/order-receipts/receipting'], { queryParams: { tab: 0 } });
  }
}
