import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LayoutModule, SharedModule } from '@app/shared';
import { MaterialModule } from '@app/shared/material.module';
import {
  DescrepancyAddEditComponent, IntransitAddEditComponent,
  InTransitVerifyComponent, IntransitViewComponent, OrderReceiptsRoutingModule, ReceiptingComponent,
  ReceiptingItemNotSerializedComponent, ReceiptingItemScanComponent, ReceiptingListComponent, ReceiptingOrderComponent,
  ReceiptingViewComponent, SerialnumberViewComponent, TechnicianReceivingComponent, TechnicianReceivingFaultyItemComponent, TechnicianReceivingFaultyItemModalComponent, TechnicianReceivingFaultyItemScanComponent, TechnicianReceivingFaultyItemVerifyComponent, TechnicianReceivingGoodsReturnComponent, TechnicianReceivingGoodsReturnItemScanComponent, TechnicianReceivingGoodsReturnStockCountComponent, TechnicianReceivingGoodsReturnVerifyComponent, TechnicianReceivingGoodsReturnViewComponent, TechnicianReceivingItemScanComponent, TechnicianReceivingVerifyComponent, TechnicianReceivingViewComponent, VarianceViewComponent
} from '@inventory/components/order-receipts';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { NgxBarcodeModule } from 'ngx-barcode';
// import { NgxBarCodePutModule } from 'ngx-barcodeput';
import { NgxPrintModule } from 'ngx-print';
import { SignaturePadModule } from 'ngx-signaturepad';
import { BarcodePrinterModule } from '../barcode-printer/barcode-printer/barcode-printer.module';
import { RadioSystemRemovalReceivingAddEditComponent } from './radio-system-removal-receiving/radio-system-removal-receiving-add-edit/radio-system-removal-receiving-add-edit.component';
import { RadioSystemRemovalReceivingScanComponent } from './radio-system-removal-receiving/radio-system-removal-receiving-scan/radio-system-removal-receiving-scan.component';
import { RadioSystemRemovalReceivingVerifyComponent } from './radio-system-removal-receiving/radio-system-removal-receiving-verify/radio-system-removal-receiving-verify.component';
import { RadioSystemRemovalReceivingViewComponent } from './radio-system-removal-receiving/radio-system-removal-receiving-view/radio-system-removal-receiving-view.component';
import { ReceiptingNewItemAddEditComponent } from './receipting-new-item/receipting-new-item-add-edit/receipting-new-item-add-edit.component';
import { ReceiptingNewItemScanComponent } from './receipting-new-item/receipting-new-item-scan/receipting-new-item-scan.component';
import { ReceiptingNewItemVerifyComponent } from './receipting-new-item/receipting-new-item-verify/receipting-new-item-verify.component';
import { ReceiptingNewItemViewComponent } from './receipting-new-item/receipting-new-item-view/receipting-new-item-view.component';
import { ReceivingTransferRequestAddEditComponent } from './receiving-transfer-request/receiving-transfer-request-add-edit/receiving-transfer-request-add-edit.component';
import { ReceivingTransferRequestVerifyComponent } from './receiving-transfer-request/receiving-transfer-request-verify/receiving-transfer-request-verify.component';
import { ReceivingTransferRequestViewComponent } from './receiving-transfer-request/receiving-transfer-request-view/receiving-transfer-request-view.component';
import { SupplierReturnReceivingVerifyComponent } from './supplier-return-receiving/supplier-return-receiving-verify.component';
import { SupplierReturnReceivingViewComponent } from './supplier-return-receiving/supplier-return-receiving-view.component';
import { SupplierReturnReceivingComponent } from './supplier-return-receiving/supplier-return-receiving.component';


@NgModule({
  declarations: [ReceiptingComponent, ReceiptingListComponent, ReceiptingItemScanComponent, ReceiptingOrderComponent, ReceiptingViewComponent,
    DescrepancyAddEditComponent, IntransitViewComponent, IntransitAddEditComponent, SupplierReturnReceivingViewComponent, SupplierReturnReceivingVerifyComponent,
    InTransitVerifyComponent, SupplierReturnReceivingComponent, SerialnumberViewComponent, TechnicianReceivingViewComponent, TechnicianReceivingComponent,
    TechnicianReceivingItemScanComponent, TechnicianReceivingVerifyComponent, TechnicianReceivingGoodsReturnViewComponent, TechnicianReceivingGoodsReturnComponent, TechnicianReceivingGoodsReturnItemScanComponent,
    TechnicianReceivingGoodsReturnVerifyComponent,  ReceiptingItemNotSerializedComponent, VarianceViewComponent, TechnicianReceivingGoodsReturnStockCountComponent,
    TechnicianReceivingFaultyItemScanComponent, TechnicianReceivingFaultyItemComponent, TechnicianReceivingFaultyItemVerifyComponent, TechnicianReceivingFaultyItemModalComponent, ReceiptingNewItemAddEditComponent, ReceiptingNewItemViewComponent, 
    ReceiptingNewItemScanComponent, ReceiptingNewItemVerifyComponent, RadioSystemRemovalReceivingAddEditComponent, RadioSystemRemovalReceivingScanComponent, RadioSystemRemovalReceivingViewComponent, RadioSystemRemovalReceivingVerifyComponent, ReceivingTransferRequestViewComponent, ReceivingTransferRequestVerifyComponent, ReceivingTransferRequestAddEditComponent
  ],
  imports: [
    CommonModule,
    OrderReceiptsRoutingModule,
    ReactiveFormsModule, FormsModule,
    LayoutModule,
    MaterialModule,
    SharedModule,
    SignaturePadModule,
    NgxBarcodeModule,
    NgxPrintModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BarcodePrinterModule,
    // NgxBarCodePutModule
  ],
  entryComponents: [ SerialnumberViewComponent, VarianceViewComponent, TechnicianReceivingFaultyItemModalComponent],
})
export class OrderReceiptsModule { }
