import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  DescrepancyAddEditComponent, IntransitAddEditComponent,
  InTransitVerifyComponent, IntransitViewComponent, ReceiptingComponent, ReceiptingItemNotSerializedComponent, ReceiptingItemScanComponent, ReceiptingListComponent, ReceiptingOrderComponent, ReceiptingViewComponent, TechnicianReceivingComponent, TechnicianReceivingFaultyItemComponent, TechnicianReceivingFaultyItemScanComponent, TechnicianReceivingFaultyItemVerifyComponent, TechnicianReceivingGoodsReturnComponent, TechnicianReceivingGoodsReturnItemScanComponent, TechnicianReceivingGoodsReturnStockCountComponent, TechnicianReceivingGoodsReturnVerifyComponent, TechnicianReceivingGoodsReturnViewComponent, TechnicianReceivingItemScanComponent, TechnicianReceivingVerifyComponent, TechnicianReceivingViewComponent
} from '@inventory/components/order-receipts';
import { RadioSystemRemovalReceivingAddEditComponent } from './radio-system-removal-receiving/radio-system-removal-receiving-add-edit/radio-system-removal-receiving-add-edit.component';
import { RadioSystemRemovalReceivingScanComponent } from './radio-system-removal-receiving/radio-system-removal-receiving-scan/radio-system-removal-receiving-scan.component';
import { RadioSystemRemovalReceivingVerifyComponent } from './radio-system-removal-receiving/radio-system-removal-receiving-verify/radio-system-removal-receiving-verify.component';
import { RadioSystemRemovalReceivingViewComponent } from './radio-system-removal-receiving/radio-system-removal-receiving-view/radio-system-removal-receiving-view.component';
import { ReceiptingNewItemAddEditComponent } from './receipting-new-item/receipting-new-item-add-edit/receipting-new-item-add-edit.component';
import { ReceiptingNewItemScanComponent } from './receipting-new-item/receipting-new-item-scan/receipting-new-item-scan.component';
import { ReceiptingNewItemVerifyComponent } from './receipting-new-item/receipting-new-item-verify/receipting-new-item-verify.component';
import { ReceiptingNewItemViewComponent } from './receipting-new-item/receipting-new-item-view/receipting-new-item-view.component';
import { ReceivingTransferRequestAddEditComponent } from './receiving-transfer-request/receiving-transfer-request-add-edit/receiving-transfer-request-add-edit.component';
import { ReceivingTransferRequestVerifyComponent } from './receiving-transfer-request/receiving-transfer-request-verify/receiving-transfer-request-verify.component';
import { ReceivingTransferRequestViewComponent } from './receiving-transfer-request/receiving-transfer-request-view/receiving-transfer-request-view.component';
import { SupplierReturnReceivingVerifyComponent } from './supplier-return-receiving/supplier-return-receiving-verify.component';
import { SupplierReturnReceivingViewComponent } from './supplier-return-receiving/supplier-return-receiving-view.component';
import { SupplierReturnReceivingComponent } from './supplier-return-receiving/supplier-return-receiving.component';
import { AuthenticationGuard as AuthGuard } from '@app/shared/services/authguards';

const routes: Routes = [
  {
    path: "receipting",
    component: ReceiptingComponent,
    data: { title: 'Receiving' },
    children: [
      { path: '', component: ReceiptingListComponent, pathMatch: 'full', canActivate: [AuthGuard], data: { title: 'Receiving' } },
      { path: 'item-scan', component: ReceiptingItemScanComponent, canActivate: [AuthGuard], data: { title: 'Receiving item scan' } },
      { path: 'item-not-serialized', component: ReceiptingItemNotSerializedComponent, canActivate: [AuthGuard], data: { title: 'Receiving non serialized item' } },
      { path: 'order', component: ReceiptingOrderComponent, canActivate: [AuthGuard], data: { title: 'New Receiving Add/Edit' } },
      { path: 'discrepancy/:id', component: DescrepancyAddEditComponent, canActivate: [AuthGuard], data: { title: 'New Receiving Verify' } },
      { path: 'discrepancy', component: DescrepancyAddEditComponent, canActivate: [AuthGuard], data: { title: 'New Receiving Verify' } },
      { path: 'intransit-view', component: IntransitViewComponent, canActivate: [AuthGuard], data: { title: 'Inter Branch Transfer Receiving' } },
      { path: 'view', component: ReceiptingViewComponent, canActivate: [AuthGuard], data: { title: 'New Receiving view' } },
      { path: 'intransit-add-edit', component: IntransitAddEditComponent, canActivate: [AuthGuard], data: { title: 'Inter Branch Transfer Receiving' } },
      { path: 'supplier-return-receiving-view', component: SupplierReturnReceivingViewComponent, canActivate: [AuthGuard], data: { title: 'Supplier Return Receiving View' } },
      { path: 'intransit-verify', component: InTransitVerifyComponent, canActivate: [AuthGuard], data: { title: 'Inter Branch Transfer Receiving' } },
      { path: 'supplier-return-receiving-verify', component: SupplierReturnReceivingVerifyComponent, canActivate: [AuthGuard], data: { title: 'Supplier Return Receiving Verify' } },
      { path: 'supplier-return-receiving', component: SupplierReturnReceivingComponent, canActivate: [AuthGuard], data: { title: 'Supplier Return Receiving' } },
      { path: 'technician-receiving-view', component: TechnicianReceivingViewComponent, canActivate: [AuthGuard], data: { title: 'Technician Receiving View' } },
      { path: 'technician-receiving-edit', component: TechnicianReceivingComponent, canActivate: [AuthGuard], data: { title: 'Technician Receiving' } },
      { path: 'technician-receiving-item-scan', component: TechnicianReceivingItemScanComponent, canActivate: [AuthGuard], data: { title: 'Technician Receiving Item Scan' } },
      { path: 'technician-receiving-verify', component: TechnicianReceivingVerifyComponent, canActivate: [AuthGuard], data: { title: 'Technician Receiving Verify' } },
      { path: 'technician-receiving-goods-return-view', component: TechnicianReceivingGoodsReturnViewComponent, canActivate: [AuthGuard], data: { title: 'Technician Receiving View' } },
      { path: 'technician-receiving-goods-return-edit', component: TechnicianReceivingGoodsReturnComponent, canActivate: [AuthGuard], data: { title: 'Technician Receiving' } },
      { path: 'technician-receiving-goods-return-item-scan', component: TechnicianReceivingGoodsReturnItemScanComponent, canActivate: [AuthGuard], data: { title: 'Technician Receiving Item Scan' } },
      { path: 'technician-receiving-goods-return-stock-count', component: TechnicianReceivingGoodsReturnStockCountComponent, canActivate: [AuthGuard], data: { title: 'Technician Receiving Stock Count' } },
      { path: 'technician-receiving-goods-return-verify', component: TechnicianReceivingGoodsReturnVerifyComponent, canActivate: [AuthGuard], data: { title: 'Technician Receiving Verify' } },

      { path: 'technician-receiving-faulty-item', component: TechnicianReceivingFaultyItemComponent, canActivate: [AuthGuard], data: { title: 'Technician Receiving Faulty Item' } },
      { path: 'technician-receiving-faulty-item-scan', component: TechnicianReceivingFaultyItemScanComponent, canActivate: [AuthGuard], data: { title: 'Technician Receiving Faulty Item Scan' } },
      { path: 'technician-receiving-faulty-item-verify', component: TechnicianReceivingFaultyItemVerifyComponent, canActivate: [AuthGuard], data: { title: 'Technician Receiving Faulty Item Scan' } },

      { path: 'new-item-add-edit', component: ReceiptingNewItemAddEditComponent, canActivate: [AuthGuard], data: { title: 'Receiving New Item' } },
      { path: 'new-item-view', component: ReceiptingNewItemViewComponent, canActivate: [AuthGuard], data: { title: 'Receiving New Item' } },
      { path: 'new-item-scan', component: ReceiptingNewItemScanComponent, canActivate: [AuthGuard], data: { title: 'Receiving New Item' } },
      { path: 'new-item-verify', component: ReceiptingNewItemVerifyComponent, canActivate: [AuthGuard], data: { title: 'Receiving New Item' } },

      { path: 'radio-removal-receiving-add-edit', component: RadioSystemRemovalReceivingAddEditComponent, canActivate: [AuthGuard], data: { title: 'Radio System Removal Receiving Add Edit' } },
      { path: 'radio-removal-receiving-view', component: RadioSystemRemovalReceivingViewComponent, canActivate: [AuthGuard], data: { title: 'Radio System Removal Receiving View' } },
      { path: 'radio-removal-receiving-scan', component: RadioSystemRemovalReceivingScanComponent, canActivate: [AuthGuard], data: { title: 'Radio System Removal Receiving Scan' } },
      { path: 'radio-removal-receiving-verify', component: RadioSystemRemovalReceivingVerifyComponent, canActivate: [AuthGuard], data: { title: 'Radio System Removal Receiving Verify' } },

      { path: 'receiving-transfer-request-add-edit', component: ReceivingTransferRequestAddEditComponent, canActivate: [AuthGuard], data: { title: 'Receiving Transfer Request' } },
      { path: 'receiving-transfer-request-view', component: ReceivingTransferRequestViewComponent, canActivate: [AuthGuard], data: { title: 'Receiving Transfer Request' } },
      { path: 'receiving-transfer-request-verify', component: ReceivingTransferRequestVerifyComponent, canActivate: [AuthGuard], data: { title: 'Receiving Transfer Request' } },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],

})

export class OrderReceiptsRoutingModule { }
