import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CreditNote, CreditNoteItemList, OrderReceipt } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { ResponseMessageTypes } from '@app/shared/enums';
import { CrudService, RxjsService, SnackbarService } from '@app/shared/services';
import { ModulesBasedApiSuffix } from '@app/shared/utils';
import { IApplicationResponse } from '@app/shared/utils/interfaces.utils';
import { DocumentTypes, InventoryModuleApiSuffixModels } from '@modules/inventory/shared';
import { loggedInUserData } from '@modules/others';
import { select, Store } from '@ngrx/store';
import { SignaturePad } from 'ngx-signaturepad/signature-pad';

@Component({
  selector: 'app-descrepancy-add-edit',
  templateUrl: './descrepancy-add-edit.component.html',
  styleUrls: ['./descrepancy-add-edit.component.scss']
})
export class DescrepancyAddEditComponent implements OnInit {
  loggedUser: any;
  orderReceiptId: string;
  creditNote: CreditNote | any;
  creditNoteForm: FormGroup;
  applicationResponse: IApplicationResponse;
  errorMessage: string;
  creditDetails: Array<CreditNote>;
  creditNoteDetails: any;
  items = [];
  divHide: boolean = true;
  Items: Array<any> = [];
  itemList: CreditNoteItemList;
  fileName: string;
  fileURL: string;
  isButtonDisabled: boolean = true;
  selectedFiles = new Array();
  totalFileSize = 0;
  maxFileSize = 104857600; //in Bytes 100MB
  imageName: string;
  orderReceipt = new OrderReceipt();
  batchId: string;
  curDate = new Date();
  primengTableConfigProperties;
  @ViewChild(SignaturePad, { static: false }) signaturePad: any;
  selectedTabIndex = 0;
  public signaturePadOptions: Object = {
    'minWidth': 1,
    'canvasWidth': 500,
    'canvasHeight': 80
  };

  constructor(private router: Router,
    private httpService: CrudService,
    private formBuilder: FormBuilder,
    private rxjsService: RxjsService,
    private snackbarService: SnackbarService, private store: Store<AppState>) {
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.orderReceipt = this.router.getCurrentNavigation().extras.state.data;
      this.batchId = this.orderReceipt.orderReceiptBatchId;
    }
    this.store.pipe(select(loggedInUserData)).subscribe((userData) => {
      this.loggedUser = userData;
    });
    this.primengTableConfigProperties = {
      tableCaption: "Receiving",
      selectedTabIndex: 0,
      breadCrumbItems: [{ displayName: 'Inventory Management - Receiving', relativeRouterUrl: '' }, { displayName: 'Receiving List', relativeRouterUrl: '/inventory/order-receipts/receipting', queryParams: { tab: 0 } }, { displayName: 'New Receiving View', relativeRouterUrl: "inventory/order-receipts/receipting/view", queryParams: { PurchaseOrderId: this.orderReceipt.orderReferenceId, BatchId: this.orderReceipt.orderReceiptBatchId, OrderTypeId: this.orderReceipt.orderTypeId } }, { displayName: 'New Receiving', relativeRouterUrl: '/inventory/order-receipts/receipting/order', state: { data: this.orderReceipt } }, { displayName: 'Verify', relativeRouterUrl: '' }],
      tableComponentConfigs: {
        tabsList: [
          {
            caption: 'Receiving',
            dataKey: 'orderReferenceId',
            enableBreadCrumb: true,
            enableExportCSV: false,
            enableExportExcel: false,
            enableExportCSVSelected: false,
            enableReset: false,
            enableAction: true
          }
        ]
      }
    }
  }

  ngOnInit() {
    this.isButtonDisabled = false;
    this.creditNote = new CreditNote();
    this.GetCreditNoteById(this.orderReceipt.orderReceiptId);
    this.creditNoteForm = this.formBuilder.group({
      comment: [this.creditNote.comment || '', Validators.required],
    });
  }

  drawComplete() {

  }

  clearPad() {
    this.signaturePad.clear();
  }

  ngAfterViewInit() {
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.resizeCanvas();
    this.signaturePad.clear();
  }

  GetCreditNoteById(OrderReceiptId: string) {
    let params;
    params = new HttpParams().set('OrderReceiptId', OrderReceiptId);
    if (this.batchId != '0')
      params = new HttpParams().set('OrderReceiptId', OrderReceiptId).set('BatchId', this.batchId);
    this.httpService.get(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CREDIT_NOTE, null, false, params).subscribe((response: IApplicationResponse) => {
      this.creditDetails = response.resources;
      this.creditNote = this.creditDetails[0];
      this.creditNoteForm.patchValue(this.creditNote);
      for (let i = 0; i < this.creditDetails.length; i++) {
        this.Items.push({ 'creditNoteItemId': this.creditDetails[i]['creditNoteItemId'], 'itemName': this.creditDetails[i]['itemName'], 'itemId': this.creditDetails[i]['itemId'], 'itemCode': this.creditDetails[i]['itemCode'], 'totalQty': this.creditDetails[i]['totalQty'], 'receivedQty': this.creditDetails[i]['receivedQty'], 'varianceQty': this.creditDetails[i]['varianceQty'], 'damagedQuantity': this.creditDetails[i]['damagedQuantity'] })
        if (parseInt(this.creditDetails[i]['varianceQty']) > 0 || parseInt(this.creditDetails[i]['damagedQuantity']) > 0) {
          this.creditNote.itemStatusName = "Partially Received";
        }
      }
    });
    this.rxjsService.setGlobalLoaderProperty(false);
  }

  onFileChange(event) {
    const supportedExtensions = ['jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx'];
    for (var i = 0; i < event.target.files.length; i++) {

      let selectedFile = event.target.files[i];
      const path = selectedFile.name.split('.');
      const extension = path[path.length - 1];
      if (supportedExtensions.includes(extension)) {
        this.selectedFiles.push(event.target.files[i]);
        this.totalFileSize += event.target.files[i].size;
      }
      else {
        this.snackbarService.openSnackbar("Allow to upload this file format only - jpeg, jpg, png, gif, pdf, doc, docx, xls, xlsx", ResponseMessageTypes.WARNING);
      }
    }
  }

  SaveCreditNote() {
    this.isButtonDisabled = true;
    if (this.totalFileSize > this.maxFileSize)
      return;

    var creditNotedetails = new CreditNote();
    if (this.creditNote.creditNoteId != undefined) {
    }
    else {
      creditNotedetails.InventoryStaffId = this.loggedUser["userId"];
      creditNotedetails.warehouseId = this.loggedUser["warehouseId"];
    }
    creditNotedetails.batchId = this.batchId;
    creditNotedetails.createdUserId = this.loggedUser["userId"];
    creditNotedetails.orderReceiptId = this.creditNote.orderReceiptId;
    creditNotedetails.comment = this.creditNoteForm.get('comment').value;
    creditNotedetails.documentType = DocumentTypes.SUPPLIERS_INVOICE;
    creditNotedetails.creditNoteURL = this.fileURL;
    creditNotedetails.orderTypeId = this.orderReceipt.orderTypeId;
    creditNotedetails.orderNumber = this.orderReceipt.orderNumber;
    if (this.Items.length != 0 || this.creditNote.orderReceiptId != null) {
      for (let item of this.Items) {
        this.itemList = new CreditNoteItemList();
        this.itemList.creditNoteItemId = item.creditNoteItemId;
        this.itemList.itemId = item.itemId;
        this.itemList.quantity = item.quantity;
        creditNotedetails.creditNoteItemsList.push(this.itemList);

        if (item.varianceQty > 0) {
          if (this.creditNoteForm.invalid || this.signaturePad.signaturePad._data.length == 0) {
            this.isButtonDisabled = false;
            this.snackbarService.openSnackbar("Please Enter comments and signature", ResponseMessageTypes.WARNING);
            return;
          }
        }
      }
      this.isButtonDisabled = true;
      const formData = new FormData();
      if (this.selectedFiles.length > 0) {
        for (const file of this.selectedFiles) {
          formData.append('document_files[]', file);
        }
      }
      formData.append("creditNoteDetails", JSON.stringify(creditNotedetails));
      if (this.signaturePad.signaturePad._data.length > 0) {
        this.imageName = this.creditNote.batchNumber + "-signature.jpeg";
        const imageBlob = this.dataURItoBlob(this.signaturePad.toDataURL().replace(/^data:image\/(png|jpg|pdf|jpeg);base64,/, ""));
        const imageFile = new File([imageBlob], this.imageName, { type: 'image/jpeg' });
        formData.append('document_files[]', imageFile);
      }
      this.rxjsService.setGlobalLoaderProperty(true);
      this.httpService.fileUpload(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.CREDIT_NOTE_FILE_UPLOAD, formData)
        .subscribe({
          next: response => {
            this.onSaveComplete(response);
            this.router.navigate(['/inventory/order-receipts/receipting']);
          },
          error: err => {
            this.errorMessage = err;
            this.isButtonDisabled = false;
          }
        });

      this.rxjsService.setGlobalLoaderProperty(false);
    }
  }
  onSaveComplete(response): void {
    if (response.statusCode == 200) {
    }
  }
  deleteFile(index) {
    this.totalFileSize -= this.selectedFiles[index].size;
    this.selectedFiles.splice(index, 1);
  }
  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }
  previousPage() {
    this.router.navigate(['/inventory/order-receipts/receipting/order'], { state: { data: this.orderReceipt } });

  }
  listPage() {
    this.router.navigate(['/inventory/order-receipts/receipting'], { queryParams: { tab: 0 } });
  }
  receiptingViewNew() {
    this.router.navigate(["inventory/order-receipts/receipting/view"], { queryParams: { PurchaseOrderId: this.orderReceipt.orderReferenceId, BatchId: this.orderReceipt.orderReceiptBatchId, OrderTypeId: this.orderReceipt.orderTypeId }, skipLocationChange: true });
  }
}
