import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderReceipt } from '@app/modules/inventory/models';
import { AppState } from '@app/reducers';
import { ModulesBasedApiSuffix, ResponseMessageTypes, SnackbarService } from '@app/shared';
import { CrudService, RxjsService } from '@app/shared/services';
import { InventoryModuleApiSuffixModels } from '@modules/inventory/shared/utils';
import { loggedInUserData } from '@modules/others';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-recepting-item-not-serialized',
  templateUrl: './receipting-item-not-serialized.component.html',
  styleUrls: ['./receipting-item-scan.component.scss']
})

export class ReceiptingItemNotSerializedComponent implements OnInit, AfterViewInit {
  purchaseOrder: any;
  itemName: string;
  stockCount: number;
  orderReceipt = new OrderReceipt();
  loggedUser: any;
  isbrudcrumb: string;
  totalPurchaseOrderItemCount: number;
  alreadyReceivedQty: number;
  isDraft: boolean = false;

  constructor(private router: Router, private rxjsService: RxjsService, private store: Store<AppState>,
    private httpService: CrudService, private snackbarService: SnackbarService) {
    this.store.pipe(select(loggedInUserData)).subscribe((userData) => {
      this.loggedUser = userData;
    });
    if (this.router && this.router.getCurrentNavigation().extras.state) {
      this.purchaseOrder = this.router.getCurrentNavigation().extras.state.data;
      this.itemName = this.purchaseOrder.itemName + " - " + this.purchaseOrder.itemCode;
      this.orderReceipt = this.purchaseOrder.orderReceipt;
      this.isDraft = this.orderReceipt.pendingBatch != null ? this.orderReceipt.pendingBatch.isDraft : false;
      this.totalPurchaseOrderItemCount = this.purchaseOrder.purchaseOrderItem.totalQuantity;
      this.alreadyReceivedQty = this.purchaseOrder.purchaseOrderItem.quantity - this.purchaseOrder.purchaseOrderItem.totalQuantity;
      this.stockCount = this.purchaseOrder.purchaseOrderItem.receivedQty;
    }
  }

  ngOnInit() {
  }

  cancel() {
    this.router.navigate(['/inventory/order-receipts/receipting/order'], { state: { data: this.orderReceipt } });
  }

  save() {

    if (this.stockCount > this.totalPurchaseOrderItemCount || this.stockCount <= 0) {
      this.snackbarService.openSnackbar("Enter valid stock count", ResponseMessageTypes.WARNING);
      return;
    }
    this.orderReceipt.orderReceiptItem.orderItemId = this.purchaseOrder.purchaseOrderItem.stockOrderItemId;
    this.orderReceipt.orderReceiptItem.quantity = this.purchaseOrder.purchaseOrderItem.totalQuantity;
    this.orderReceipt.orderReceiptItem.orderReceiptItemId = this.purchaseOrder.purchaseOrderItem.orderReceiptItemId;
    this.orderReceipt.inventoryStaffId = this.loggedUser["userId"];
    this.orderReceipt.createdUserId = this.loggedUser["userId"];
    this.orderReceipt.orderReceiptItem.createdUserId = this.loggedUser["userId"];
    this.orderReceipt.orderReceiptItem.isConsumable = true;
    this.orderReceipt.orderReceiptItem.consumableQuantity = this.stockCount;

    this.httpService.create(ModulesBasedApiSuffix.INVENTORY, InventoryModuleApiSuffixModels.ORDER_RECEIPTS, this.orderReceipt).subscribe((response) => {
      if (response.isSuccess == true) {
        this.orderReceipt.orderReceiptItem.orderReceiptItemDetails = [];
        this.orderReceipt.orderReceiptBatchId = response.resources;
        this.router.navigate(['/inventory/order-receipts/receipting/order'], { state: { data: this.orderReceipt } });
      }
    });
  }

  receiptingViewNew() {
    this.router.navigate(["inventory/order-receipts/receipting/view"], { queryParams: { PurchaseOrderId: this.orderReceipt.orderReferenceId, BatchId: this.orderReceipt.orderReceiptBatchId, OrderTypeId: this.orderReceipt.orderTypeId }, skipLocationChange: true });
  }

  receiptingNew() {
    this.router.navigate(['/inventory/order-receipts/receipting/order'], { state: { data: this.orderReceipt } });
  }

  ngAfterViewInit() {
    this.rxjsService.setGlobalLoaderProperty(false);
  }
}
